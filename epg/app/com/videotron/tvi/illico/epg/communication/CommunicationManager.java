package com.videotron.tvi.illico.epg.communication;

import java.rmi.Remote;
import java.rmi.NotBoundException;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.App;
import com.opencable.handler.cahandler.CAHandler;
import com.opencable.handler.nethandler.NetHandler;

public class CommunicationManager {

    private static final long LOOK_UP_RETRY_DELAY = 2000L;

    private XletContext xletContext;
    private static CommunicationManager instance = new CommunicationManager();

    public static CommunicationManager getInstance() {
        return instance;
    }

    static MonitorService monitorService;

    IxcLookupWorker lookupWorker;

    public void start(XletContext xletContext, DataUpdateListener l) {
        this.xletContext = xletContext;
        lookupWorker = new IxcLookupWorker(xletContext);
        lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME, l);
        lookupWorker.lookup("/1/1/", CAHandler.IXC_NAME, l);
        lookupWorker.lookup("/1/1/", NetHandler.IXC_NAME, l);
        lookupWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, l);
        lookupWorker.lookup("/1/2080/", SearchService.IXC_NAME, l);
        lookupWorker.lookup("/1/2040/", DaemonService.IXC_NAME, l);
        lookupWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME, l);
        lookupWorker.lookup("/1/2100/", StcService.IXC_NAME, l);
        lookupWorker.lookup("/1/2110/", NotificationService.IXC_NAME, l);
        lookupWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, l);
        lookupWorker.lookup("/1/1010/", ScreenSaverService.IXC_NAME, l);
        lookupWorker.lookup("/1/1020/", VODService.IXC_NAME, l);
        lookupWorker.lookup("/1/2200/", ISAService.IXC_NAME, l);
        if (App.R3_TARGET) {
            lookupWorker.lookup("/1/2085/", VbmService.IXC_NAME, l);
        }
        if (Environment.SUPPORT_DVR) {
            lookupWorker.lookup("/1/20A0/", PvrService.IXC_NAME, l);
        }
    }

    public void dispose() {
    }

    public static MonitorService getMonitorService() {
        if (monitorService == null) {
            monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        }
        return monitorService;
    }

}

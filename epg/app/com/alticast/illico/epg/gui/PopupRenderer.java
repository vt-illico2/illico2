package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.ui.Popup;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class PopupRenderer extends Renderer {
    protected DataCenter dataCenter = DataCenter.getInstance();

    protected Image[] icons = new Image[] {
        null,
        dataCenter.getImage("icon_g_check.png"),
        dataCenter.getImage("icon_noti_red.png"),
    };

    protected Image iButton = dataCenter.getImage("05_focus_dim.png");
    protected Image iFocus = dataCenter.getImage("05_focus.png");

    protected Image i_pop_sha = dataCenter.getImage("pop_sha.png");
    protected Image i_05_pop_glow_t = dataCenter.getImage("05_pop_glow_t.png");
    protected Image i_05_pop_glow_m = dataCenter.getImage("05_pop_glow_m.png");
    protected Image i_05_pop_glow_b = dataCenter.getImage("05_pop_glow_b.png");

    protected Image i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
    protected Image i_pop_high_350 = dataCenter.getImage("pop_high_350.png");

    protected Font fTitle = FontResource.BLENDER.getFont(24);
    protected Font fText = FontResource.BLENDER.getFont(20);
    protected Font fButton = FontResource.BLENDER.getFont(18);

    protected FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);
    protected FontMetrics fmText = FontResource.getFontMetrics(fText);

    protected Color cTitle = new Color(255, 203, 4);
    protected Color cText = Color.white;
    protected Color cButton = new Color(3, 3, 3);

    protected Color cPopup = new Color(23, 23, 23);

    protected String[] texts;

    protected int Y_GAP = 20;
    protected int TEXT_Y = 113;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void setMessage(String s) {
        texts = getSplitMessage(s);
    }

    public String[] getSplitMessage(String s) {
        return TextUtil.split(TextUtil.replace(s, "|", "\n"), fmText, getMessageWidth());
    }

    public int getMessageWidth() {
        return 320;
    }

    public void setMessage(String[] s) {
        texts = s;
    }

    public Color getBackground() {
        return Popup.DEFAULT_BACKGROUND;
    }

    public Rectangle getButtonFocusBounds(Popup popup) {
        int buttonSize = (popup.buttons != null) ? popup.buttons.length : 0;
        int focus = popup.getFocus();
        if (buttonSize == 1) {
            if (focus == 0) {
                return new Rectangle(403+1, 318+35, 154, 32);
            }
        } else if (buttonSize == 2) {
            if (focus == 0) {
                return new Rectangle(321+1, 318+35, 154, 32);
            } else if (focus == 1) {
                return new Rectangle(485+1, 318+35, 154, 32);
            }
        }
        return null;
    }

    public void paint(Graphics g, UIComponent c) {
        Popup popup = (Popup) c;
        int buttonSize = (popup.buttons != null) ? popup.buttons.length : 0;
        int height = buttonSize == 0 ? 224 : 259;

        // shadow & glow
        g.drawImage(i_pop_sha, 306, height + 143 - 4, 350, 79, c);

        g.drawImage(i_05_pop_glow_t, 276, 113, c);
        g.drawImage(i_05_pop_glow_m, 276, 193, 410, height - 100, c);
        g.drawImage(i_05_pop_glow_b, 276, 193 - 100 + height, c);

        // popup body
        g.setColor(cPopup);
        g.fillRect(306, 143, 350, height);

        g.drawImage(i_pop_gap_379, 285, 181, c);
        g.drawImage(i_pop_high_350, 306, 143, c);

        Image icon = icons[popup.icon];
        String s = popup.title;
        int width = fmTitle.stringWidth(s);
        int tx;
        if (icon != null) {
            int iw = icon.getWidth(c);
            width += iw + 5;
            tx = 481 - width / 2;
            g.drawImage(icon, tx, 151, c);
            tx += iw + 5;
        } else {
            tx = 481 - width / 2;
        }

        g.setColor(cTitle);
        g.setFont(fTitle);
        g.drawString(s, tx, 169);

        g.translate(306, 143);
        paintBody(g, popup);
        g.translate(-306, -143);

        paintButtons(g, popup);
    }

    protected int paintMessage(Graphics g, Popup p, String[] msg, int y) {
        if (msg != null) {
            g.setColor(cText);
            g.setFont(fText);
            for (int i = 0; i < msg.length; i++) {
                GraphicUtil.drawStringCenter(g, msg[i], 480-306, y);
                y = y + Y_GAP;
            }
        }
        return y;
    }

    protected void paintBody(Graphics g, Popup p) {
        if (texts != null && texts.length > 0) {
            int fs = fText.getSize();
            int y = TEXT_Y - ((texts.length - 1) * Y_GAP - fs) / 2 - 3;
            paintMessage(g, p, texts, y);
        }
    }

    protected void paintButtons(Graphics g, Popup popup) {
        int buttonSize = (popup.buttons != null) ? popup.buttons.length : 0;
        if (buttonSize == 1) {
            g.drawImage(iFocus, 403, 318+35, popup);
            g.setColor(cButton);
            g.setFont(fButton);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(popup.buttons[0]), 481, 339+35);
        } else if (buttonSize == 2) {
            int focus = popup.getFocus();
            g.drawImage(focus == 0 ? iFocus : iButton, 321, 318+35, popup);
            g.drawImage(focus == 1 ? iFocus : iButton, 485, 318+35, popup);
            g.setColor(cButton);
            g.setFont(fButton);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(popup.buttons[0]), 399, 339+35);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(popup.buttons[1]), 564, 339+35);
        }
    }


}

package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ParentalControl;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.util.Date;

public class ReminderChangePopupRenderer extends PopupRenderer {

    private Image iHd = dataCenter.getImage("icon_hd.png");
    //->Kenneth[2015.2.3] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd.png");

    private Image i_pop_sha = dataCenter.getImage("pop_sha.png");
    private Image i_pglow610_m = dataCenter.getImage("pglow610_m.png");
    private Image i_pglow610_b = dataCenter.getImage("pglow610_b.png");
    private Image i_pglow610_t = dataCenter.getImage("pglow610_t.png");
    private Image i_07_respop_title = dataCenter.getImage("07_respop_title.png");
    private Image i_07_respop_high = dataCenter.getImage("07_respop_high.png");
    private Image i_07_res_pgbg_b = dataCenter.getImage("07_res_pgbg_b.png");
    private Image i_07_res_pgbg_m = dataCenter.getImage("07_res_pgbg_m.png");
    private Image i_07_res_pgbg_t = dataCenter.getImage("07_res_pgbg_t.png");
    private Image i_07_res_line = dataCenter.getImage("07_res_line.png");

    private Font fChannel = FontResource.BLENDER.getFont(18);
    private Font fProgram = FontResource.BLENDER.getFont(20);
    private FontMetrics fmChannel = FontResource.getFontMetrics(fChannel);
    private FontMetrics fmProgram = FontResource.getFontMetrics(fProgram);

    private Program from;
    private Program to;

    private String msg1;
    private String msg2;

    private Color cPopupBg = new Color(33, 33, 33);

    public ReminderChangePopupRenderer(Program from, Program to) {
        this.from = from;
        this.to = to;

        msg1 = TextUtil.replace(dataCenter.getString("epg.reminder_change_popup_msg_1"),
                   "%%", Formatter.getCurrent().getLongDate(from.getStartTime()) );
        msg2 = dataCenter.getString("epg.reminder_change_popup_msg_2");
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void paint(Graphics g, UIComponent c) {
        Popup popup = (Popup) c;

        // shadow & glow
        g.drawImage(i_pop_sha, 207, 435, 550, 79, c);
        g.drawImage(i_pglow610_m, 177, 111, 610, 305, c);
        g.drawImage(i_pglow610_b, 177, 416, c);
        g.drawImage(i_pglow610_t, 177, 68, c);

        // popup body
        g.setColor(cPopupBg);
        g.fillRect(207, 98, 550, 341);
        g.drawImage(i_07_respop_title, 206, 136, c);
        g.drawImage(i_07_respop_high, 207, 98, c);

        // title
        g.setColor(cTitle);
        g.setFont(fTitle);
        GraphicUtil.drawStringCenter(g, popup.title, 478, 125);

        g.setColor(Color.white);
        g.setFont(fChannel);
        GraphicUtil.drawStringCenter(g, msg1, 473, 182);
        GraphicUtil.drawStringCenter(g, msg2, 473, 292);

        paintProgram(g, from, popup);
        g.translate(0, 109);
        paintProgram(g, to, popup);
        g.translate(0, -109 + 35);

        paintButtons(g, popup);
        g.translate(0, -35);
    }

    private void paintProgram(Graphics g, Program p, Popup c) {
        g.drawImage(i_07_res_pgbg_b, 224, 230, c);
        g.drawImage(i_07_res_pgbg_m, 224, 212, c);
        g.drawImage(i_07_res_pgbg_t, 224, 202, c);
        g.drawImage(i_07_res_line, 470, 214, c);

        g.setColor(Color.white);
        g.setFont(fChannel);
        Channel ch = p.getEpgChannel();
		//->Kenneth : 4K by June
        int definition = Channel.DEFINITION_SD;
        if (ch != null) {
            g.drawString(String.valueOf(ch.getNumber()), 237, 227);
            g.drawImage(ch.getLogo(), 276, 206, c);
			//->Kenneth : 4K by June
            definition = ch.getDefinition();
        }
        g.drawString(p.callLetter, 387, 227);
		//->Kenneth : 4K by June
        if (definition == Channel.DEFINITION_HD) {   // TODO - 4K
            g.drawImage(iHd, fmChannel.stringWidth(p.callLetter) + 387 + 3, 215, c);
        } else if (definition == Channel.DEFINITION_4K) {
            g.drawImage(iUhd, fmChannel.stringWidth(p.callLetter) + 387 + 3, 215, c);
        }
        g.setFont(fProgram);
        g.drawString(TextUtil.shorten(ParentalControl.getTitle(p), fmProgram, 252), 482, 227);
    }


}

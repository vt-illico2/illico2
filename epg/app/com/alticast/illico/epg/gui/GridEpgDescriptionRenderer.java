package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.ui.GridEpgDescription;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.ParentalControl;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class GridEpgDescriptionRenderer extends Renderer {

    public static final int X_OFFSET = 30;
    public static final int Y_OFFSET = 77;
    public static final int WIDTH = 574;
    private Rectangle bounds = new Rectangle(X_OFFSET, Y_OFFSET, WIDTH, 245);

    private DataCenter dataCenter = DataCenter.getInstance();

    private Image iDescBg = dataCenter.getImage("02_epg_detail_bg.png");
    private Image iRec = dataCenter.getImage("02_icon_REC.png");
    private Image iRemind = dataCenter.getImage("02_reminder_icon.png");
    private Image iSkipped = dataCenter.getImage("02_icon_skipped.png");
    private Image iHd = dataCenter.getImage("icon_hd.png");
    private Image iCC = dataCenter.getImage("icon_cc.png");
    private Image iLock = dataCenter.getImage("02_icon_con.png");
    //->Kenneth[2015.2.3] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd.png");

    private Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };

    private Font fTitle = FontResource.BLENDER.getFont(27);
    private Font fTime = FontResource.BLENDER.getFont(16);
    private Font fChannelDesc = FontResource.BLENDER.getFont(18);
    private FontMetrics fmChannelDesc = FontResource.getFontMetrics(fChannelDesc);
    private FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

    private Color cTitle = new Color(252, 202, 4);

    private Color cTime = new Color(195, 195, 195);
    private Color cChannelDesc = new Color(195, 195, 195);
    private Color cProgressBg = new Color(120, 120, 120);
    private Color cProgressFg = new Color(255, 212, 0);

    private String[] text;

    private static final int BAR_WIDTH = 120;

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
    }

    public void paint(Graphics g, UIComponent c) {
        GridEpgDescription ged = (GridEpgDescription) c;
        int iconWidth = 26;
		//->Kenneth : 4K by June
        if (ged.definition > 0) {   // TODO - 4K
            iconWidth += 26;
        }
        if (ged.isCaptioned) {
            iconWidth += 26;
        }

		//->Kenneth[2015.4.11] : R5 : 얘를 없애야 filter 위에 있는 구분선이 사라진다.
        //g.drawImage(iDescBg, 0, 27, c);
        g.setFont(fTitle);
        g.setColor(cTitle);
        Program p = ged.program;
        if (p == null) {
            return;
        }
        Formatter fm = Formatter.getCurrent();
        String s = ged.title;
        int ix = 23;
        if (ged.blockLevel != ParentalControl.OK) {
            g.drawImage(iLock, ix, 81-Y_OFFSET, c);
            ix += 27;
        }
        s = TextUtil.shorten(s, fmTitle, WIDTH-6-ix-iconWidth);
        g.drawString(s, ix, 24);

		//->Kenneth[2015.4.11] : R5 : 없어짐
        /*
        g.setFont(fChannelDesc);
        g.setColor(cChannelDesc);
        String desc = ged.channelDesc;
        g.drawString(desc, 24, 24+18);
        int screenData = ged.screenData;
        if (screenData > 0) {
            Util.drawScreenIcons(g, screenData, fmChannelDesc.stringWidth(desc) + 26, 29, c);
        }
        */

        if (Log.EXTRA_ON) {
            g.setFont(fChannelDesc);
            g.setColor(Color.green);
            g.drawString(new java.util.Date(p.startTime).toString(), 24, 34);
            g.drawString(new java.util.Date(p.endTime).toString(), 24, 52);
            g.drawString(p.getId() + " " + (p.isLive() ? "live" : "rerun") + " " + Genre.getName(p.getConvertedCategory()), 320, 48);
            g.drawString("(" + p.getType() + ") " + p.getSeriesId() + " " + p.getEpisodeTitle(), 320, 68);
        }

        ix = ix + fmTitle.stringWidth(s) + 5;
		//->Kenneth[2015.4.11] : R5 : HD/UHD 아이콘 없어짐
        /*
		//->Kenneth : 4K by June
        if (ged.definition == Constants.DEFINITION_HD) {   // TODO - 4K
            g.drawImage(iHd, ix, 10, c);
            ix += 26;
        } else if (ged.definition == Constants.DEFINITION_4K) {  
            g.drawImage(iUhd, ix, 10, c);
            ix += 28;
        }
        */
        Image im = ratingImages[p.getRatingIndex()];
        if (im != null) {
            g.drawImage(im, ix, 10, c);
            ix += 26;
        }
        if (ged.isCaptioned) {
            g.drawImage(iCC, ix, 10, c);
        }

        if (!(p instanceof FakeProgram || p instanceof NullProgram)) {
            int recX;
            long pst = p.getStartTime();
            long pet = p.getEndTime();
            //->Kenneth[2015.4.11] : R5 : screenData 없어지면서 y 좌표 조정
            int yGap = 17;

            g.setFont(fTime);
            g.setColor(cTime);
            if (p.isOnAir()) {
                g.drawString(fm.getTime(pst), 24, 68-yGap);
                s = fm.getTime(pet);
                g.drawString(s, 210, 68-yGap);

                g.setColor(cProgressBg);
                g.fillRect(79+4, 61-yGap, BAR_WIDTH, 6);

                int bar = (int) (BAR_WIDTH * (System.currentTimeMillis() - pst) / (pet - pst));
                bar = Math.max(0, Math.min(bar, BAR_WIDTH));
                g.setColor(cProgressFg);
                g.fillRect(79+4, 61-yGap, bar, 6);
                recX = 210 + 5;
            } else {
                s = fm.getTime(pst) + "  -  " + fm.getTime(pet);
                g.drawString(s, 24, 68-yGap);
                recX = 24 + 5;
            }
            ix = g.getFontMetrics().stringWidth(s) + recX;
            if (ReminderManager.getInstance().contains(p)) {
                g.drawImage(iRemind, ix, 53-yGap, c);
                ix += 22 + 3;
            }
            // recording icon
            switch (ged.recordingStatus) {
                case RecordingStatus.RECORDING:
                    g.drawImage(iRec, ix, 56-yGap, c);
                    ix += 34;
                    break;
                case RecordingStatus.SKIPPED:
                    g.drawImage(iSkipped, ix, 56-yGap, c);
                    ix += 34;
                    break;
            }
        }
//        if (ged.blockLevel == ParentalControl.ADULT_CONTENT) {
//            g.drawImage(dataCenter.getImageByLanguage("02_block_des.png"), 54-X_OFFSET, 158-Y_OFFSET, c);
//        }
    }
}


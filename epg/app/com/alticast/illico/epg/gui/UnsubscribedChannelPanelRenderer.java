package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.*;

public class UnsubscribedChannelPanelRenderer extends AbstractBlockedPanelRenderer {

    protected Color cMessage = Color.white;
    protected Font fMessage = FontResource.BLENDER.getFont(22);

    protected Color cNormal = new Color(241, 241, 241);
    protected Font fNormal = FontResource.BLENDER.getFont(24);

    protected Color cButton = new Color(3, 3, 3);
    protected Font fButton = FontResource.BLENDER.getFont(18);

    private Color[] cHighlights = new Color[] { new Color(255, 203, 0) };

    protected Color cOr = Color.white;
    protected Font fOr = FontResource.BLENDER.getFont(20);

    protected Color cInfo = new Color(172, 172, 172);
    protected Font fInfo = FontResource.BLENDER.getFont(16);

    protected Color cAbout = new Color(200, 200, 200);
    protected Font fAbout = FontResource.BLENDER.getFont(22);

    private Image i_Group_371 = dataCenter.getImage("Group_371.png");
    private Image i_chlogobg = dataCenter.getImage("chlogobg.png");

    private Image i_05_focus_258 = dataCenter.getImage("05_focus_258.png");

    private Image i_isa_pgsh_t = dataCenter.getImage("isa_pgsh_t.png");
    private Image i_isa_pgsh_b = dataCenter.getImage("isa_pgsh_b.png");
    private Image i_isa_pgbg_b = dataCenter.getImage("isa_pgbg_b.png");
    private Image i_isa_pgbg_m = dataCenter.getImage("isa_pgbg_m.png");
    private Image i_isa_pgbg_t = dataCenter.getImage("isa_pgbg_t.png");

    private Image i_02_icon_isa_bl = dataCenter.getImage("02_icon_isa_bl.png");

    private String title;
    private String line1;
    private String line2;
    private String or;
    private String goTo;
    private String callTo;
    private String url;
    private String tel;
    private String button;
    private String about;

    //->Kenneth[2016.8.29] CYO Bundling/A la carte 하면서 보니까 클릭 에니메이션 포지션이 이상함. 그래서 수정. 
    private static final Rectangle focusBounds = new Rectangle(440, 236, 258, 40);
    //private static final Rectangle focusBounds = new Rectangle(446+2, 249, 244, 32);
    //<-

    // R5
    private FontMetrics fmName = FontResource.getFontMetrics(fName);
    protected FontMetrics fmInfo = FontResource.getFontMetrics(fInfo);
    protected FontMetrics fmButton = FontResource.getFontMetrics(fButton);
    //protected Color cYellow = new Color(249, 194, 0);

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public Rectangle getButtonFocusBounds() {
        return focusBounds;
    }


    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
        UnsubscribedChannelPanel d = (UnsubscribedChannelPanel) c;
        Channel ch = d.channel;
        String fullName = ch.getFullName();

        about = dataCenter.getString("epg.about");

        //->Kenneth[2015.8.21] VDTRMASTER-5597 : 예전 테스트 코드로 인해 무조건 true 되던거 없앴음.
        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        boolean isa = d.isISAPurchasable();
        //boolean isa = d.isISASupported();
        //<-

        if (isa) {
            if (fullName == null) {
                title = dataCenter.getString("epg.subscribe_now_to");
            } else {
                title = TextUtil.replace(dataCenter.getString("epg.subscribe_now_to"), TextUtil.TEXT_REPLACE_MARKER, fullName);
            }
            line1 = dataCenter.getString("epg.subscribe_isa_text_1");
            line2 = dataCenter.getString("epg.subscribe_isa_text_2");
            button = dataCenter.getString("epg.subscribe_now");
        } else {
            if (fullName == null) {
                title = dataCenter.getString("epg.add_to_package");
            } else {
                title = TextUtil.replace(dataCenter.getString("epg.add_to_package"), TextUtil.TEXT_REPLACE_MARKER, fullName);
            }
            line1 = dataCenter.getString("epg.subscribe_cyo_text_1");
            line2 = dataCenter.getString("epg.subscribe_cyo_text_2");
            button = dataCenter.getString("epg.change_package");
        }
        or = dataCenter.getString("epg.or");
        goTo = dataCenter.getString("epg.go_to") + " ";
        callTo = dataCenter.getString("epg.call_to") + " ";
        url = dataCenter.getString("epg.URL");
        tel = dataCenter.getString("epg.PHONE_NUMBER");

    }

    protected void paintBackground(Graphics g, Channel ch, UIComponent c)  {
        g.setColor(cBg);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(i_12_ppop_sha, 101, 479, 759, 57, c);
        g.drawImage(i_pop_759_bg, 71, 26, c);

        Image logo = ch.getLogo();

        Util.drawChannelIcon(g, ch, 118, 64, c);
        g.setColor(cNumber);
        g.setFont(fNumber);
        g.drawString(String.valueOf(ch.getNumber()), 146, 83);
        if (ch.getDefinition() == Channel.DEFINITION_HD) { 
            g.drawImage(iHd, 190, 71, c);
        } else if (ch.getDefinition() == Channel.DEFINITION_4K) {
            g.drawImage(iUhd, 190, 71, c);
        }
        g.drawImage(logo, 226, 63, c);
        g.setFont(fName);
        String fullName = ch.getFullName();
        if (fullName != null && fullName.length() > 0) {
            g.drawString(fullName, 840-fmName.stringWidth(fullName), 83);
        }
    }

    public void paint(Graphics g, UIComponent c) {
        UnsubscribedChannelPanel d = (UnsubscribedChannelPanel) c;
        Channel ch = d.channel;
        paintBackground(g, ch, c);

        //->Kenneth[2015.8.21] VDTRMASTER-5597 : 예전 테스트 코드로 인해 무조건 true 되던거 없앴음.
        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        boolean isa = d.isISAPurchasable();
        //boolean isa = d.isISASupported();
        //<-

        if (isa) {
            g.drawImage(i_isa_pgsh_t, 182, 159, c);
            g.drawImage(i_isa_pgsh_b, 182, 272, c);
            g.drawImage(i_isa_pgbg_b, 181, 298, c);
            g.drawImage(i_isa_pgbg_m, 181, 168, 596, 130, c);
            g.drawImage(i_isa_pgbg_t, 181, 158, c);
        }

        g.drawImage(i_Group_371, 121, 360, c);
        g.drawImage(i_chlogobg, 200, 174, c);
        GraphicUtil.drawImageCenter(g, ch.getLogo(), 212+50, 218+15, c);

        g.setFont(fMessage);
        g.setColor(cMessage);
        GraphicUtil.drawStringCenter(g, title, 478, 138);

        g.setFont(fButton);
        g.setColor(cNormal);
        GraphicUtil.drawStringCenter(g, line1, 562, 197);
        GraphicUtil.drawStringCenter(g, line2, 562, 197+19);

        g.setFont(fOr);
        g.setColor(cOr);
        g.drawString(or, 202, 330);
        g.drawString(or, 520, 330);

        g.setFont(fInfo);
        g.setColor(cInfo);
        g.drawString(goTo, 225, 330);
        g.drawString(callTo, 546, 330);
        //->Kenneth[2015.4.22] R5 Julie 요청으로 white 로 바꿈
        g.setColor(Color.white);
        g.drawString(url, 225+fmInfo.stringWidth(goTo), 330);
        g.drawString(tel, 546+fmInfo.stringWidth(callTo), 330);

        g.drawImage(i_05_focus_258, 440, 236, c);

        g.setColor(cButton);
        g.setFont(fButton);

        if (isa) {
            int centerX = 562;
            int iconW = i_02_icon_isa_bl.getWidth(c);
            int buttonW = fmButton.stringWidth(button);
            int gap = 5;
            int totalW = iconW+buttonW+gap;
            int startX = centerX-totalW/2;
            g.drawImage(i_02_icon_isa_bl, startX, 242, c);
            g.drawString(button, startX+iconW+gap, 257);
        } else {
            GraphicUtil.drawStringCenter(g, button, 562, 257);
        }

        g.setFont(fAbout);
        g.setColor(cAbout);
        g.drawString(about, 122, 400);

        int screenData = d.screenData;
        if (screenData > 0) {
            Util.drawScreenIconsRight(g, screenData, 787+54, 386, c);
        }
    }

}

package com.alticast.illico.epg.gui;

import java.awt.*;
import java.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.alticast.illico.epg.ui.Popup;


public class HdmiSimpleNotiRenderer extends PopupRenderer {
    Image i_hdmi_vol_bg = dataCenter.getImage("hdmi_vol_bg.png");

    Color cGray = new Color(172, 172, 172);
    Color cWhite = Color.WHITE;

    Font fLarge = FontResource.BLENDER.getFont(24);
    Font fSmall = FontResource.BLENDER.getFont(19);

    String msg = "";

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    public Color getBackground() {
        return null;
    }

    public void paint(Graphics g, UIComponent c) {
        Popup p = (Popup) c;
        g.drawImage(i_hdmi_vol_bg, 0, 360, c);

        g.setColor(cWhite);
        g.setFont(fLarge);
        GraphicUtil.drawStringCenter(g, p.title, 480, 461);
        g.setColor(cGray);
        g.setFont(fSmall);
        GraphicUtil.drawStringCenter(g, msg, 480, 486);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString("count = " + com.alticast.illico.epg.navigator.VolumeController.hdmiCheckCount, 700, 440);
        }

    }

    public void prepare(UIComponent c) {
        //msg = dataCenter.getString("epg.hdmi_simple_msg") + dataCenter.getString("epg.hdmi_url");
        msg = dataCenter.getString("epg.hdmi_url");
        super.prepare(c);
    }

}

package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.*;

public abstract class AbstractBlockedPanelRenderer extends Renderer {

    public Rectangle bounds = new Rectangle(0, 0, 960, 540);
    protected DataCenter dataCenter = DataCenter.getInstance();

    protected Color cBg = new Color(12, 12, 12);

    protected Color cNumber = Color.white;
    protected Font fNumber = FontResource.BLENDER.getFont(23);
    protected Font fName = FontResource.BLENDER.getFont(20);

    protected Image i_12_ppop_sha = dataCenter.getImage("12_ppop_sha.png");
    protected Image i_pop_759_bg = dataCenter.getImage("pop_759_bg.png");
    protected Image iHd = dataCenter.getImage("icon_hd.png");
    //->Kenneth[2015.2.3] 4K
    protected Image iUhd = dataCenter.getImage("icon_uhd.png");
    private FontMetrics fmName = FontResource.getFontMetrics(fName);

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paintBackground(Graphics g, Channel ch, UIComponent c)  {
        g.setColor(cBg);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(i_12_ppop_sha, 101, 479, 759, 57, c);
        g.drawImage(i_pop_759_bg, 71, 26, c);

        Image logo = ch.getLogo();

        Util.drawChannelIcon(g, ch, 118, 64, c);
        g.setColor(cNumber);
        g.setFont(fNumber);
        g.drawString(String.valueOf(ch.getNumber()), 146, 83);
        if (ch.getDefinition() == Channel.DEFINITION_HD) { 
            g.drawImage(iHd, 190, 71, c);
        } else if (ch.getDefinition() == Channel.DEFINITION_4K) {
            g.drawImage(iUhd, 190, 71, c);
        }
        g.drawImage(logo, 226, 63, c);
        g.setFont(fName);
        String fullName = ch.getFullName();
        if (fullName != null && fullName.length() > 0) {
            g.drawString(fullName, 840-fmName.stringWidth(fullName), 83);
        }
    }
}

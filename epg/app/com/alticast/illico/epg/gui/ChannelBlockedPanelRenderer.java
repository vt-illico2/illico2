package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.*;

public class ChannelBlockedPanelRenderer extends AbstractBlockedPanelRenderer {

    protected Color cTitle = new Color(255, 203, 0);
    protected Font fTitle = FontResource.BLENDER.getFont(32);

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void paint(Graphics g, UIComponent c) {
        ChannelBlockedPanel d = (ChannelBlockedPanel) c;
        Channel ch = d.channel;
        paintBackground(g, ch, c);

        g.setFont(fTitle);
        g.setColor(cTitle);
        GraphicUtil.drawStringCenter(g, dataCenter.getString("epg.blocked_message_" + d.type), 478, 155);
    }

}

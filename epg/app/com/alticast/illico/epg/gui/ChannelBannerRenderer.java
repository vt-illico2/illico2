package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.ui.EpgChannelBanner;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.util.Date;

/**
 * ChannelBannerRenderer.
 *
 * @author June Park
 */
public class ChannelBannerRenderer extends Renderer {

    public static final int TITLE_WIDTH = 175;

    private DataCenter dataCenter = DataCenter.getInstance();

    private Rectangle bounds = new Rectangle(635, 0, 325, 306);
    private Font fClock = FontResource.BLENDER.getFont(17);
    private Font fName = FontResource.BLENDER.getFont(19);
    private Font fNumber  = FontResource.BLENDER.getFont(20);
    private Font fProgram  = FontResource.BLENDER.getFont(23);
    private Font fFilter  = FontResource.BLENDER.getFont(17);
    public FontMetrics fmName  = FontResource.getFontMetrics(fName);
    public FontMetrics fmProgram  = FontResource.getFontMetrics(fProgram);
    private FontMetrics fmClock = FontResource.getFontMetrics(fClock);

    private Color cClock = Color.white;
    private Color cName = new Color(189, 189, 189);
    private Color cNumber = Color.white;
    private Color cProgram = Color.black;
    private Color cAllFilter = new Color(225, 225, 225);
    private Color cFilter = new Color(0xffcb00);

    private Image iBackground = dataCenter.getImage("02_chbg.png");

    private Image iClock = dataCenter.getImage("clock.png");

    private Image iRec = dataCenter.getImage("02_icon_REC.png");
    private Image iSkipped = dataCenter.getImage("02_icon_skipped.png");
    public Image iHd = dataCenter.getImage("icon_hd_foc.png");
    public Image iCC = dataCenter.getImage("icon_cc_foc.png");
    public Image iHdNormal = dataCenter.getImage("icon_hd.png");
    public Image iBlockedFoc = dataCenter.getImage("02_icon_con_foc.png");
    //->Kenneth[2015.2.3] 4K
    public Image iUhd = dataCenter.getImage("icon_uhd.png");
    public Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g_foc.png"),
        dataCenter.getImage("icon_8_foc.png"),
        dataCenter.getImage("icon_13_foc.png"),
        dataCenter.getImage("icon_16_foc.png"),
        dataCenter.getImage("icon_18_foc.png")
    };

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    Color panel = new org.dvb.ui.DVBColor(200, 0, 0, 50);

    public synchronized void paint(Graphics g, UIComponent c) {
        Formatter formatter = Formatter.getCurrent();
        g.drawImage(iBackground, 0, 0, c);

        g.setColor(cClock);
        g.setFont(fClock);
        String clockString = formatter.getLongDate();
        GraphicUtil.drawStringRight(g, clockString, 910-635, 57);
        g.drawImage(iClock, 910-635 - fmClock.stringWidth(clockString) - 24, 44, c);

//        g.setColor(cClock);
//        g.setFont(fClock);
//        g.drawString(formatter.getLongDate(), 125, 56);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString("version = " + App.VERSION, 90, 265);
//            g.drawString("programs = " + Program.objectCount, 90, 285);
            g.drawString("next update = " + Log.DEBUG_DATE_FORMAT.format(new Date(EpgDataManager.nextUpdateTime)), 90, 285);
        }

        EpgChannelBanner cb = (EpgChannelBanner) c;
        g.setColor(cNumber);
        g.setFont(fNumber);
        g.drawString(cb.number, 107, 115);

        g.setColor(cName);
        g.setFont(fName);
        String s = cb.name;
        if (s != null && s.length() > 0) {
            //->Kenneth[2015.11.23] VDTRMASTER-5721
            g.drawString(TextUtil.shorten(s, fmName, 153), 107, 132+5);
            //<-
            //g.drawString(TextUtil.shorten(s, fmName, 153), 107, 132);
        }

        Channel ch = cb.channel;
        if (ch == null) {
            return;
        }
		//->Kenneth : 4K by June
        int def = ch.getDefinition();
        if (def == Channel.DEFINITION_HD) { 
            g.drawImage(iHdNormal, 147, 102, c);
        } else if (def == Channel.DEFINITION_4K) { 
            g.drawImage(iUhd, 147, 102, c);
        }
        g.setColor(cb.filtered ? cFilter : cAllFilter);
        g.setFont(fFilter);
        g.drawString(cb.currentFilter, 89, 87);

        Util.drawChannelIcon(g, ch, 83, 97, c);

        // logo
        if (cb.logo != null) {
            g.drawImage(cb.logo, 169+5, 94, c);
        }

        // recording icon
        switch (cb.recordingStatus) {
            case RecordingStatus.RECORDING:
                g.drawImage(iRec, 231, 224, c);
                break;
            case RecordingStatus.SKIPPED:
                g.drawImage(iSkipped, 231, 224, c);
                break;
        }

        int x;
        if (cb.parentalBlocked) {
            g.drawImage(iBlockedFoc, 87, 158, c);
            x = 109;
        } else {
            x = 89;
        }

        g.setColor(cProgram);
        String[] program = cb.program;
        if (program != null) {
            g.setFont(fProgram);
            for (int i = 0; i < program.length; i++) {
                g.drawString(program[i], x, 176 + i*20);
            }
        }
        Image[] icons = cb.icons;
        if (icons != null) {
            int ix = x + cb.iconX;
            int iy = cb.iconLine * 20 + 164;
            for (int i = 0; i < icons.length; i++) {
                if (icons[i] != null) {
                    g.drawImage(icons[i], ix, iy, c);
                    ix += 26;
                }
            }
        }

        if (cb.timeString != null) {
            g.setFont(fClock);
            g.drawString(cb.timeString, 90, 237);
        }
    }



}

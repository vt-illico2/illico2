package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.ui.MiniEpg;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.util.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Graphics;
import org.dvb.ui.DVBColor;
import org.davic.net.tuning.*;
import javax.tv.locator.*;

public class MiniEpgRenderer extends Renderer {

    private DataCenter dataCenter = DataCenter.getInstance();

    private Rectangle bounds = new Rectangle(0, 200, 669+17, 340);

    //->Kenneth[2015.2.16] 4K flat design tuning
    private Font fFilter     = FontResource.BLENDER.getFont(16);
    //private Font fFilter     = FontResource.BLENDER.getFont(18);
    private Font fClock      = FontResource.BLENDER.getFont(16);
    private Font fChannelNum = FontResource.BLENDER.getFont(24);
    private Font fText       = FontResource.BLENDER.getFont(18);
    private Font fBigText    = FontResource.BLENDER.getFont(22);
    private Font fChannelName = FontResource.BLENDER.getFont(22);
    private Font fNormalName = FontResource.BLENDER.getFont(20);
    private FontMetrics fmText = FontResource.getFontMetrics(fText);
    private FontMetrics fmClock = FontResource.getFontMetrics(fClock);
    private FontMetrics fmBigText = FontResource.getFontMetrics(fBigText);

    private Image iBackground = dataCenter.getImage("02_mini_bg.png");
    private Image iFoc = dataCenter.getImage("02_mini_foc.png");
    private Image iClock = dataCenter.getImage("clock.png");

    private Image iBlockedFoc    = dataCenter.getImage("02_icon_con_foc.png");
    private Image iRecFoc = dataCenter.getImage("02_mini_rec_foc.png");
    private Image iRec = dataCenter.getImage("02_icon_REC.png");
    private Image iRemindFoc = dataCenter.getImage("02_mini_remind_foc.png");
    private Image iRemind = dataCenter.getImage("02_reminder_icon_foc.png");

    private Image iSkipped = dataCenter.getImage("02_icon_skipped.png");
    private Image iCCFoc = dataCenter.getImage("icon_cc_foc.png");
    private Image iHdFoc = dataCenter.getImage("icon_hd_foc.png");
    private Image iHd = dataCenter.getImage("icon_hd.png");
    //->Kenneth[2015.2.3] 4K
    private Image iUhdFoc = dataCenter.getImage("icon_uhd_foc.png");
    private Image iUhd = dataCenter.getImage("icon_uhd.png");

    private Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g_foc.png"),
        dataCenter.getImage("icon_8_foc.png"),
        dataCenter.getImage("icon_13_foc.png"),
        dataCenter.getImage("icon_16_foc.png"),
        dataCenter.getImage("icon_18_foc.png")
    };
    private Image iArT = dataCenter.getImage("02_ars_t.png");
    private Image iArB = dataCenter.getImage("02_ars_b.png");
    private Image iArR = dataCenter.getImage("02_ars_r.png");

    private Color cAllFilter = new Color(222, 222, 222);
    private Color cFilter = new Color(0xffcb00);

    private Color cPlaying = new Color(176, 213, 255);
    private Color cClock = Color.white;

    private Color cNormalChannel = new Color(185, 185, 185);
    private Color cNormalText = new Color(131, 131, 131);
    private Color cNormalName = new Color(172, 172, 172);

    private Color cFocusChannel = Color.white;
    private Color cFocusText = new Color(5, 5, 5);

    private Color cProgressBackground = new Color(0xa18c25);
    private Color cProgressBar = Color.black;

    private static final int MAX_PROGRESS_WIDTH = 84;

    public MiniEpgRenderer() {
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void paint(Graphics g, UIComponent c) {
        MiniEpg epg = (MiniEpg) c;
        int focus = epg.getFocus();
        int size = epg.size;

        Formatter formatter = Formatter.getCurrent();
        g.drawImage(iBackground, 0, 0, c);
        g.drawImage(iFoc, 52, 131, c);

        String s = ChannelController.get(0).getCurrentFilterKey();
        g.setFont(fFilter);
        g.setColor(ChannelController.ALL_FILTER_KEY.equals(s) ? cAllFilter : cFilter);
        g.drawString(dataCenter.get("epg.filtered_by") + dataCenter.getString(s), 55, 50);

        Program cur = epg.leftPrograms[focus];
        if (cur != null) {
            g.setColor(cAllFilter);
            if (cur.isFuture()) {
                s = dataCenter.getString("epg.future_program") + " : " + formatter.getLongDayText(cur.startTime);
            } else if (cur.isPast()) {
                s = dataCenter.getString("epg.past_program") + " : " + formatter.getLongDayText(cur.startTime);
            } else {
                s = dataCenter.getString("epg.now_playing");
            }
            g.drawString(s, 63, 73);
        }

        //->Kenneth[2015.2.16] 4K flat design tuning
        g.drawImage(iClock, 251-5, 59, c);
        g.setColor(cClock);
        g.drawString(formatter.getTime(), 272-5, 72);
//        g.drawImage(iClock, 251, 59, c);
//        g.setColor(cClock);
//        g.drawString(formatter.getTime(), 272, 72);

        paint(g, epg, (focus - 1 + size) % size, 86);
        paint(g, epg, (focus + 1) % size, 234);
        paintCenter(g, epg, focus);

        g.drawImage(iArT, 277, 118, c);
        g.drawImage(iArB, 277, 227, c);
        if (!epg.miniDesc.isVisible() && cur != null && cur.next != null) {
            g.drawImage(iArR, 327, 168, c);
        }
    }

    /** y는 채널 로고 기준. */
    private void paint(Graphics g, MiniEpg c, int index, int y) {
        Program p = c.leftPrograms[index];

        g.setFont(fChannelNum);
        g.setColor(cNormalChannel);

        String channelNumber;
        Image channelLogo;
        String title;

        Channel ch = c.channelList.getChannelAt(index);
        channelNumber = Integer.toString(ch.getNumber());
        channelLogo = ch.getScaledLogo();
        title = ParentalControl.getTitle(p);

        Util.drawChannelIcon(g, ch, 59, y + 1, c);

        g.drawString(channelNumber, 84, y + 18);
        //g.drawImage(channelLogo, 141, y, c);
        g.drawImage(channelLogo, 171, y, c);
		//->Kenneth : 4K by June
        if (ch.getDefinition() == Channel.DEFINITION_HD) {   // TODO - 4K
            //g.drawImage(iHd, 288, y + 4, c);
            g.drawImage(iHd, 130, y + 4, c);
        } else if (ch.getDefinition() == Channel.DEFINITION_4K) { 
            //g.drawImage(iUhd, 288, y + 4, c);
            g.drawImage(iUhd, 130, y + 4, c);
        }

		//->Kenneth[2014.4.21] R5 : Call letter 안 보이게
        //g.setColor(cNormalName);
        //g.setFont(fNormalName);
        //g.drawString(ch.getName(), 225, y + 17);

        g.setFont(fText);
        g.setColor(cNormalText);
        g.drawString(TextUtil.shorten(title, fmText, 235), 85, y+37);
    }

    private void paintCenter(Graphics g, MiniEpg c, int index) {
        Program p = c.leftPrograms[index];

        String channelNumber;
        Image channelLogo;
        String title = c.title;
        String channelName;

        Channel ch = c.channelList.getChannelAt(index);
        Util.drawChannelIcon(g, ch, 59, 138, c);

        channelNumber = Integer.toString(ch.getNumber());
        channelLogo = ch.getScaledLogo();
		//->Kenneth[2014.4.21] R5 : Call letter 안 보이게
        //channelName = ch.getName();

        g.setFont(fChannelNum);
        g.setColor(cFocusChannel);
        g.drawString(channelNumber, 84, 156);

        //g.setFont(fChannelName);
        //g.drawString(channelName, 223, 156);

        //g.drawImage(channelLogo, 141, 137, c);
        g.drawImage(channelLogo, 171, 137, c);

        g.setFont(fClock);
        g.setColor(cFocusText);
        int iw = 0;
        Image rating = null;
        if (p != null) {
            if (p instanceof FakeProgram) {
                // does nothing
            } else {
                Formatter formatter = Formatter.getCurrent();
                long st = p.getStartTime();
                long et = p.getEndTime();
                int recX;
                String s;
                if (p.isOnAir()) {
                    s = formatter.getTime(st);
                    g.drawString(s, 84, 220);
                    recX = 90 + fmClock.stringWidth(s);

                    g.setColor(cProgressBackground);
                    g.fillRect(recX, 212, MAX_PROGRESS_WIDTH, 6);
                    g.setColor(cProgressBar);
                    g.fillRect(recX, 212, getProgressWidth(st, et), 6);
                    recX += MAX_PROGRESS_WIDTH + 6;

                    s = formatter.getTime(et);
                    g.drawString(s, recX, 220);
                    recX += fmClock.stringWidth(s) + 3;

//                    GraphicUtil.drawStringRight(g, formatter.getTime(st), 133, 220);
//                    g.drawString(s, 229, 220);
//                    g.setColor(cProgressBackground);
//                    g.fillRect(139, 212, MAX_PROGRESS_WIDTH, 6);
//                    g.setColor(cProgressBar);
//                    g.fillRect(139, 212, getProgressWidth(st, et), 6);
//                    recX = 229 + 5;
                } else {
                    s = formatter.getTime(st) + " - " + formatter.getTime(et);
                    g.drawString(s, 85, 220);
                    recX = fmClock.stringWidth(s) + 85 + 5;
                }

                int ix = recX;
                if (ReminderManager.getInstance().contains(p)) {
                    g.drawImage(iRemindFoc, 55, 133, c);
                    g.drawImage(iRemind, ix, 205, c);
                    ix += 14 + 3;
                }
                // recording icon
                switch (RecordingStatus.getInstance().getStatus(p)) {
                    case RecordingStatus.RECORDING:
                        g.drawImage(iRec, ix, 206, c);
                        g.drawImage(iRecFoc, 55, 133, c);
                        break;
                    case RecordingStatus.SKIPPED:
                        g.drawImage(iSkipped, ix, 206, c);
                        break;
                }

                rating = ratingImages[p.getRatingIndex()];
                if (rating != null) {
                    iw += 26;
                }
				//->Kenneth : 4K by June
                // TODO - 4K
                if (Util.getDefinition(ch, p) >= Constants.DEFINITION_HD) {
                    iw += 26;
                }
                if (p.isCaptioned()) {
                    iw += 26;
                }
            }
        }
        if (MiniEpg.blockLevel != ParentalControl.OK) {
            g.drawImage(iBlockedFoc, 61, 167, c);
        }

        g.setFont(fBigText);
        g.setColor(cFocusText);
        String[] text = TextUtil.split(title, fmBigText, 240, 2);
        g.drawString(text[0], 84, 184);
        int s1 = fmBigText.stringWidth(text[0]);
        int ix, iy;
        String t2;
        if (text.length > 1 || s1 + iw + 3 > 240) {
            // draw next line
            t2 = text.length > 1 ? text[1] : "";
            t2 = TextUtil.shorten(t2, fmBigText, 240 - iw - 3);
            g.drawString(t2, 84, 184+18);
            ix = 84 + fmBigText.stringWidth(t2) + 3;
            iy = 189;
        } else {
            ix = 84 + s1 + 3;
            iy = 189 - 18;
        }
		//->Kenneth : 4K by June
        // TODO - 4K
        // [2015.2.3] 왜 아래처럼 두번 그리는지 모르겠네. 아마도 채널정보에 하나 프로그램 정보에 하나 그려주는 것
        // 아닐까?
        if (ch.getDefinition() == Constants.DEFINITION_HD) {
            //g.drawImage(iHdFoc, 292, 143, c);
            g.drawImage(iHdFoc, 130, 143, c);
            if (p.getDefinition() == Constants.DEFINITION_HD) {
                g.drawImage(iHdFoc, ix, iy, c);
                ix += 26;
            }
        } else if (ch.getDefinition() == Constants.DEFINITION_4K) {
            //g.drawImage(iUhdFoc, 292, 143, c);
            g.drawImage(iUhdFoc, 130, 143, c);
        }
        /*
        if (ch.getDefinition() >= Constants.DEFINITION_HD) {
            g.drawImage(iHdFoc, 292, 143, c);
            if (p.getDefinition() >= Constants.DEFINITION_HD) {
                g.drawImage(iHdFoc, ix, iy, c);
                ix += 26;
            }
        }
        */
        if (rating != null) {
            g.drawImage(rating, ix, iy, c);
            ix += 26;
        }
        if (p.isCaptioned()) {
            g.drawImage(iCCFoc, ix, iy, c);
        }
    }


    private int getProgressWidth(long st, long et) {
        long cur = System.currentTimeMillis();
        if (cur <= st) {
            return 0;
        }
        if (cur >= et) {
            return MAX_PROGRESS_WIDTH;
        }
        return (int) ((cur - st) * MAX_PROGRESS_WIDTH / (et - st));
    }


}

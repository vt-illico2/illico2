package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.*;

public class DualUhdPipLimitPanelRenderer extends AbstractBlockedPanelRenderer {

    protected Color bgColor = Color.black;
    private Image blockIcon = dataCenter.getImage("icon_pip_block.png");

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paintBackground(Graphics g, Channel ch, UIComponent c)  {
    }

    public void paint(Graphics g, UIComponent c) {
        g.setColor(bgColor);
        g.fillRect(0, 0, 960, 540);
        g.drawImage(blockIcon, 438, 179, c);
    }
}

package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.ui.GridPrograms;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.Point;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

/**
 * GridProgramsRenderer.
 *
 * @author  June Park
 */
public class GridProgramsRenderer extends Renderer {

    public static final int X_OFFSET = 51;
    public static final int UI_WIDTH = 960 - X_OFFSET;

    public static final int TOTAL_WIDTH = 860;

    public static final int MIN_FOCUS_WIDTH = 250;

//    public static final int FOCUS_HEADER_HEIGHT = 23;
//    public static final int FOCUS_CONTENT_HEIGHT = 48;
//    public static final int FOCUS_HEIGHT = FOCUS_HEADER_HEIGHT + FOCUS_CONTENT_HEIGHT;

    public static final int HEADER_HEIGHT = 27;
    //->Kenneth[2015.2.10] 4K
    //public static final int FOCUS_HEIGHT = 48;
    public static final int FOCUS_HEIGHT = 47;

    public static final int CHANNEL_CELL_WIDTH = 260;
    public static final int PROGRAM_CELL_WIDTH = TOTAL_WIDTH - CHANNEL_CELL_WIDTH;
    public static final int NEXT_MARGIN = 0;

    public static final int CELL_COLUMN_COUNT = 4;
    public static final int WIDTH_PER_HOUR = (PROGRAM_CELL_WIDTH - NEXT_MARGIN) / GridPrograms.HOURS_IN_PAGE;
    public static final long TIME_PER_CELL = GridPrograms.PAGE_TIME_GAP / CELL_COLUMN_COUNT;

    public static final int H_GAP = (PROGRAM_CELL_WIDTH - NEXT_MARGIN) / CELL_COLUMN_COUNT;
    public static final int V_GAP = 37;

    public static final int FOCUS_Y_OFFSET = 247;

    private static final int TYPE_NORMAL = 0;
    private static final int TYPE_FOCUS_CHANNEL  = 1;
    private static final int TYPE_FOCUS_PROGRAM  = 2;

    private DataCenter dataCenter = DataCenter.getInstance();
//    private PvrContentManager pcm = PvrContentManager.getInstance();
    private RecordingStatus pcm = RecordingStatus.getInstance();
    private ReminderManager rmm = ReminderManager.getInstance();

    private Image iTimeBg = dataCenter.getImage("02_time_bg.png");
    private Image iFocusChannelBg = dataCenter.getImage("02_sel_bg.png");

    private Image iEffect1 = dataCenter.getImage("02_grid_ef01.png");
    private Image iFocusL = dataCenter.getImage("02_grid_foc01_l.png");
    private Image iFocusM = dataCenter.getImage("02_grid_foc01_m.png");
    private Image iFocusR = dataCenter.getImage("02_grid_foc01_r.png");

    private Image iRedFocusL = dataCenter.getImage("02_grid_foc02_l.png");
    private Image iRedFocusM = dataCenter.getImage("02_grid_foc02_m.png");
    private Image iRedFocusR = dataCenter.getImage("02_grid_foc02_r.png");

//    private Image iRedNormalL = dataCenter.getImage("02_grid_foc03_l.png");
//    private Image iRedNormalM = dataCenter.getImage("02_grid_foc03_m.png");
//    private Image iRedNormalR = dataCenter.getImage("02_grid_foc03_r.png");

    private Image iGreenFocusL = dataCenter.getImage("02_grid_foc04_l.png");
    private Image iGreenFocusM = dataCenter.getImage("02_grid_foc04_m.png");
    private Image iGreenFocusR = dataCenter.getImage("02_grid_foc04_r.png");

//    private Image iGreenNormalL = dataCenter.getImage("02_grid_foc05_l.png");
//    private Image iGreenNormalM = dataCenter.getImage("02_grid_foc05_m.png");
//    private Image iGreenNormalR = dataCenter.getImage("02_grid_foc05_r.png");


    private Image iArrowU = dataCenter.getImage("02_ars_t.png");
    private Image iArrowD = dataCenter.getImage("02_ars_b.png");
    private Image iArrowL = dataCenter.getImage("02_ars_l.png");
    private Image iArrowR = dataCenter.getImage("02_ars_r.png");
//    private Image iNow    = dataCenter.getImage("02_icon_now.png");
    private Image iNow2   = dataCenter.getImage("02_icon_now_2.png");

    private Image iBlockedFoc  = dataCenter.getImage("02_icon_con_foc.png");

    private Image iRec = dataCenter.getImage("02_icon_REC.png");
    private Image iSkipped = dataCenter.getImage("02_icon_skipped.png");
    private Image iRemind = dataCenter.getImage("02_reminder_icon_foc.png");
    private Image iHd = dataCenter.getImage("icon_hd_foc.png");
    private Image iHdNormal = dataCenter.getImage("icon_hd.png");
    private Image iCCFoc = dataCenter.getImage("icon_cc_foc.png");

    private Image iNoInfoL = dataCenter.getImage("02_noinfo_l.png");
    private Image iNoInfoM = dataCenter.getImage("02_noinfo_m.png");
    private Image iNoInfoR = dataCenter.getImage("02_noinfo_r.png");
    private Image iNoInfoFocL = dataCenter.getImage("02_noinfo_foc_l.png");
    private Image iNoInfoFocM = dataCenter.getImage("02_noinfo_foc_m.png");
    private Image iNoInfoFocR = dataCenter.getImage("02_noinfo_foc_r.png");

    //->Kenneth : 4K icon 추가
    private Image iUhd = dataCenter.getImage("icon_uhd_foc.png");
    private Image iUhdNormal = dataCenter.getImage("icon_uhd.png");

    //->Kenneth : R5 icon 추가
    private Image iSlash_clar = dataCenter.getImage("slash_clar.png");
    private Image iSlash_clar_foc = dataCenter.getImage("slash_clar_foc.png");
    // test channel logo
    private Image sample_logo = dataCenter.getImage("sample_logo.png");

    private Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g_foc.png"),
        dataCenter.getImage("icon_8_foc.png"),
        dataCenter.getImage("icon_13_foc.png"),
        dataCenter.getImage("icon_16_foc.png"),
        dataCenter.getImage("icon_18_foc.png")
    };

    private Image i_02_tri03_r = dataCenter.getImage("02_tri03_r.png");
    private Image i_02_tri03_l = dataCenter.getImage("02_tri03_l.png");
    private Image i_02_tri02_r = dataCenter.getImage("02_tri02_r.png");
    private Image i_02_tri02_l = dataCenter.getImage("02_tri02_l.png");
    private Image i_02_tri01_r = dataCenter.getImage("02_tri01_r.png");
    private Image i_02_tri01_l = dataCenter.getImage("02_tri01_l.png");
    private Image i_02_tri01_foc_l = dataCenter.getImage("02_tri01_foc_l.png");
    private Image i_02_tri01_foc_r = dataCenter.getImage("02_tri01_foc_r.png");

    private Image[] iTrL = { i_02_tri02_l, i_02_tri01_l, i_02_tri01_foc_l, i_02_tri03_l };
    private Image[] iTrR = { i_02_tri02_r, i_02_tri01_r, i_02_tri01_foc_r, i_02_tri03_r };

    private static Color cChBack = new DVBColor(0, 0, 0, 242);
    private static Color cPrBackTop = new DVBColor(179, 179, 178, 128);
    private static Color cPrBackBottom = new DVBColor(25, 25, 25, 230);

    private static Color[] cChannel = { new DVBColor(204, 204, 204, 179), new DVBColor(255, 255, 255, 255) };
    private static Color cFocusText = new Color(5, 5, 5);
    private static Color cText = new Color(131, 131, 131);
    private static Color cNullText = new Color(88, 88, 88);
    private static Color cNullFocusText = new Color(191, 191, 191);
    private static Color[] cProgramTitle = { cText, Color.white, cFocusText };
    private static Color[] cNullProgramTitle = { cNullText, cNullFocusText, cFocusText };

    private static int[] vertBarHeights = { V_GAP - 2, FOCUS_HEIGHT - 1};

    private static Color cTime = Color.black;

    private static Color[] cBar = { new DVBColor(72, 72, 71, 242), new DVBColor(183, 183, 184, 128) };
    private static Color cFocusChBack = new DVBColor(40, 40, 40, 242);
    private static Color cFocusPrBack = new Color(12, 12, 11);
    private static Color cDay = Color.black;
    private static Color cProgressBg = new Color(196, 170, 45);
    private static Color cProgressFg = Color.black;
    private static Color cInputNumber = new Color(252, 202, 4);
    private static Color cRedRing = new Color(123, 34, 34);
    private static Color cRedRingFoc = new Color(166, 53, 53);
    private static Color cGreenRing = new Color(52, 141, 38);

    private static Font fChannel = FontResource.BLENDER.getFont(22);
    private static Font fChannelName = FontResource.BLENDER.getFont(19);
    private static Font fTime = FontResource.BLENDER.getFont(19);
    private static Font fText = FontResource.BLENDER.getFont(20);
    private static Font fFocusText = FontResource.BLENDER.getFont(20);
    private static FontMetrics fmChannelName = FontResource.getFontMetrics(fChannelName);
    private static FontMetrics fmTime = FontResource.getFontMetrics(fTime);
    private static FontMetrics fmText = FontResource.getFontMetrics(fText);
    private static FontMetrics fmFocusText = FontResource.getFontMetrics(fFocusText);

    //->Kenneth[2015.2.10] 4K
    private static Color c30_30_30 = new Color(30, 30, 30);
    private static Color c45_45_45 = new Color(45, 45, 45);
    private static Color c50_50_50 = new Color(50, 50, 50);
    //->Kenneth[2015.5.3] 4K : 4월 28일 황성화씨 메일 요청에 의해
    private static Color c27_27_27 = new Color(27, 27, 27);
    private static Color c38_38_38 = new Color(38, 38, 38);

    private String noMore;

    private GridPrograms grid;
    private boolean loading;

    public Rectangle getPreferredBounds(UIComponent c) {
        this.grid = (GridPrograms) c;
        return new Rectangle(X_OFFSET, 1, UI_WIDTH, (grid.rowSize - 1) * V_GAP + FOCUS_HEIGHT + HEADER_HEIGHT);
    }

    public void prepare(UIComponent c) {
        noMore = dataCenter.getString("epg.no_data_title");
    }

    public void paint(Graphics g, UIComponent c) {
        loading = grid.loading;
        Formatter formatter = Formatter.getCurrent();
        // day & time
        g.drawImage(iTimeBg, 0, 0, c);

        long time = grid.pageTime;
        g.setFont(fTime);
        g.setColor(cDay);
        g.drawString(formatter.getLongDayText(time), 12, 19);

        g.setColor(cTime);
        int x = CHANNEL_CELL_WIDTH + 11;
        for (int i = 0; i < CELL_COLUMN_COUNT; i++) {
            g.drawString(formatter.getTime(time), x, 19);
            time = time + TIME_PER_CELL;
            x = x + H_GAP;
        }
        g.translate(0, HEADER_HEIGHT);
//HEADER_HEIGHT + V_GAP * grid.prevSize - V_GAP * grid.nextSize
//- V_GAP * grid.prevSize - HEADER_HEIGHT
        // program rows
        int row = 0;
        for (; row < grid.prevSize; row++) {
            paintChannel(g, c, row, TYPE_NORMAL);
            g.translate(0, V_GAP);
        }

        //->Kenneth[2015.2.12] cell 간격 2 pixel 로
        //g.drawImage(iFocusChannelBg, 0, 0, c);
        g.drawImage(iFocusChannelBg, 0, 1, c);

        paintChannel(g, c, row, TYPE_FOCUS_CHANNEL);
        //->Kenneth[2015.2.12] cell 간격 2 pixel 로
        //g.translate(0, FOCUS_HEIGHT);
        g.translate(0, FOCUS_HEIGHT+1);
        row++;
        for (; row < grid.rowSize; row++) {
            paintChannel(g, c, row, TYPE_NORMAL);
            g.translate(0, V_GAP);
        }
        g.translate(0, - V_GAP * grid.nextSize - FOCUS_HEIGHT);

        g.drawImage(iEffect1, 0, - V_GAP * grid.prevSize - HEADER_HEIGHT, c);

        if (!loading) {
            // now
            long cur = System.currentTimeMillis();
            if (grid.pageTime <= cur && cur <= time) {
                // time contains next page time.
                x = CHANNEL_CELL_WIDTH + (int) ((cur - grid.pageTime) * WIDTH_PER_HOUR / Constants.MS_PER_HOUR);
                g.drawImage(iNow2, x - 9, 21 - V_GAP * grid.prevSize - HEADER_HEIGHT, c);
            }

            // focused program
            paintFocus(g, c, grid.focusedEvent);
        } else {

        }
        g.translate(0, -V_GAP * grid.prevSize - HEADER_HEIGHT);
    }

    // 0 == normal, 1 == focus
    private void paintChannel(Graphics g, UIComponent c, int row, int type) {
        if (type == TYPE_NORMAL) {
            // -> 포커스 되지 않은 채널의 채널정보(왼쪽) BG
            g.setColor(cChBack);
            //g.setColor(Color.blue);
            //g.fillRect(0, 0, CHANNEL_CELL_WIDTH, V_GAP - 1);
            g.fillRect(0, 1, CHANNEL_CELL_WIDTH, V_GAP - 2);
            // 좌우에 1px 빈칸을 둔다.
            //->Kenneth : 4K flat design
//            g.setColor(cPrBackTop);
//            g.fillRect(CHANNEL_CELL_WIDTH + 1, 0, PROGRAM_CELL_WIDTH - 2, 20);
            // -> 포커스 되지 않은 채널의 프로그램정보(오른쪽) BG : 데이터가 있는 경우
            //g.setColor(cPrBackBottom);

            //g.setColor(c50_50_50);
            //g.fillRect(CHANNEL_CELL_WIDTH + 1, 0, PROGRAM_CELL_WIDTH - 2, V_GAP - 1);

            //->Kenneth[2015.5.3] 4K : 4월 28일 황성화씨 메일 요청에 의해
            g.setColor(c27_27_27);
            g.fillRect(CHANNEL_CELL_WIDTH + 1, 1, PROGRAM_CELL_WIDTH - 2, V_GAP - 2);
        }

        //->Kenneth[2015.5.3] 4K : 4월 28일 황성화씨 메일 요청에 의해
        g.setColor(c38_38_38);
        g.fillRect(CHANNEL_CELL_WIDTH, 0, 2, V_GAP+10);

        //->Kenneth[2015.9.1] 전반적인 null check 다시 함.
        if (grid == null) return;
        int index = grid.getIndexOfRow(row);
        if (index < 0) {
            return;
        }
        if (grid.channelList == null) return;
        Channel ch = grid.channelList.getChannelAt(index);
        if (ch == null) {
            if (Log.DEBUG_ON) Log.printDebug("paintChannel() : Channel is null");
            return;
        }

        int ty = type == TYPE_NORMAL ? 24 : 29;
        g.setFont(fChannel);

        if (type == TYPE_FOCUS_CHANNEL && grid.inputedCount > 0) {
            g.setColor(cInputNumber);
            g.drawChars(grid.inputedChars, 0, grid.inputedChars.length, 30, ty);
        //->Kenneth[2015.3.30] R5
        // chNameState == 1 : full name 보여줌
        } else if (type == TYPE_FOCUS_CHANNEL && grid.chNameState == 1) {
            g.setFont(fTime);
            g.setColor(Color.white);
            String fullName = ch.getFullName();
            //fullName = "This is channel full name sample for test";
            if (fullName != null && fullName.length() > 0) {
                fullName = TextUtil.shorten(fullName, fmTime, 200);
            }
            if (fullName != null) g.drawString(fullName, 30, ty);
        //->Kenneth[2015.3.30] R5
        // chNameState = 0 : 채널 no, logo, definition, icon 보여줌
        } else {
            Channel sister = ch.sister;

            //->Kenneth[2016.3.7] R7 : Ungrouped 에서는 sister 안 그림
            if (!EpgCore.isChannelGrouped) sister = null;
            //<-

            g.setFont(fTime);
            g.setColor(cChannel[type]);
            int chNum = ch.getNumber();
            String chNumStr = String.valueOf(chNum);

            // 채널 no
            //->Kenneth[2015.9.23] VDTRMASTER-5660
            // grouping 된 채널 뿐 아니라 다른 채널도 yellow color 로 그리도록 함
            int sisterDef = Channel.DEFINITION_SD;
            if (sister == null) {
                // focus 된 채널이고 sister 가 없으면 yellow 컬러로 그림
                if (type == TYPE_FOCUS_CHANNEL) {
                    g.setColor(cInputNumber);
                }
                g.drawString(chNumStr, 30, ty);
            } else {
                sisterDef = sister.getDefinition();
                int sisterNum = sister.getNumber();
                if (chNum < sisterNum) {
                    // HD filter 가 아닌 일반적인 경우
                    int tmpX = 30+fmTime.stringWidth(chNumStr)+5;
                    // 채널 구분이미지
                    g.drawImage(iSlash_clar_foc, tmpX, ty-11, c);
                    tmpX = tmpX+iSlash_clar_foc.getWidth(c)+5;

                    if (type == TYPE_FOCUS_CHANNEL) {
                        // focus 된 채널인 경우
                        if (sister.isSubscribed()) {
                            // sister 로 tune 되어야 하는 경우
                            g.drawString(chNumStr, 30, ty);
                            g.setColor(cInputNumber);
                            g.drawString(String.valueOf(sisterNum), tmpX, ty);
                        } else {
                            // sister 는 권한 없어서 SD 로 tune 되어야 하는 경우
                            g.setColor(cInputNumber);
                            g.drawString(chNumStr, 30, ty);
                            g.setColor(cChannel[type]);
                            g.drawString(String.valueOf(sisterNum), tmpX, ty);
                        }
                    } else {
                        // focus 된 채널이 아닌 경우 : 원래의 회색으로 그린다
                        g.drawString(chNumStr, 30, ty);
                        g.drawString(String.valueOf(sisterNum), tmpX, ty);
                    }
                //->Kenneth[2015.9.23] VDTRMASTER-5686
                // HD filter 의 경우는 sister 없는 것처럼 그려야 한다.
                // 이 지라는 5660 의 side effect 임
                } else {
                    // focus 된 채널이면 yellow 컬러로 그림
                    if (type == TYPE_FOCUS_CHANNEL) {
                        g.setColor(cInputNumber);
                    }
                    g.drawString(chNumStr, 30, ty);
                //<-
                }
            }
            /*
            g.drawString(chNumStr, 30, ty);
            int sisterDef = Channel.DEFINITION_SD;
            if (sister != null) {
                sisterDef = sister.getDefinition();
                int sisterNum = sister.getNumber();
                if (chNum < sisterNum) {
                    int tmpX = 30+fmTime.stringWidth(chNumStr)+5;
                    // 채널 구분이미지
                    g.drawImage(iSlash_clar_foc, tmpX, ty-11, c);
                    tmpX = tmpX+iSlash_clar_foc.getWidth(c)+5;
                    // sister 채널 no
                    if (type == TYPE_FOCUS_CHANNEL && sister.isSubscribed()) {
                        g.setColor(cInputNumber);
                    }
                    g.drawString(String.valueOf(sisterNum), tmpX, ty);
                }
            }
            */
            //<-

            // HD/UHD logo
            int def = ch.getDefinition();
            if (def < sisterDef) {
                def = sisterDef;
            }
            if (def == Channel.DEFINITION_4K) {   // 4K
                g.drawImage(iUhdNormal, 109, ty - 12, c);
            } else if (def == Channel.DEFINITION_HD) {   // HD
                g.drawImage(iHdNormal, 110, ty - 12, c);
            }
            // channel logo
            //int logoW = sample_logo.getWidth(c);
            //g.drawImage(sample_logo, 195-(logoW/2), ty - 21, c);
            Image logo = ch.getLogo();
            if (logo != null) {
                int logoW = logo.getWidth(c);
                g.drawImage(logo, 195-(logoW/2), ty - 21, c);
            }

            if (Log.EXTRA_ON) {
                g.setFont(FontResource.BLENDER.getFont(16));
                g.setColor(Color.green);
                //->Kenneth[2016.3.9] Call letter 를 full name 앞으로 옮기자
                g.drawString(ch.getId() + ", " + ch.getTypeString() + ", " + ch.getLanguage(), 135, ty - 13);
                //g.drawString(ch.getId() + ", " + ch.getTypeString() + ", " + ch.getLanguage()+", "+ch.getName(), 135, ty - 13);

                if (sister != null) {
                    g.drawString(sister.getNumber() + " " + sister.getName(), 50, ty-13);
                }
                //->Kenneth[2016.2.11] : Full name 도 그려주자
                String fullName = ch.getFullName();
                //->Kenneth[2016.3.9] Call letter 를 full name 앞으로 옮기자
                g.drawString(ch.getName()+"["+fullName+"]", 50, ty+13);
                //g.drawString("["+fullName+"]", 50, ty+13);
                //<-
            }
        }

        if (type == TYPE_FOCUS_CHANNEL) {
            Util.drawChannelIcon(g, ch, false, grid.iconType == 0, 5, ty - 18, c);
        } else {
            Util.drawChannelIcon(g, ch, false, true, 5, ty - 18, c);
        }

        if (!loading) {
            if (grid.leftPrograms == null) return;
            Program left = grid.leftPrograms[index];

            //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
            // Grid EPG 보여줄때 SD/HD 그룹핑 채널인 경우 채널은 SD 지만 프로그램 정보는 HD 로 바꿔 놓았음.
            // GridEpg.decideTargetChannel() 의 적용이 그것임. 그러나 그 side effect 로 recording -> B -> SD 로 녹화 채널 바꾸는
            // 경우 문제 발생. 또한 D 액션팝업, Grid program 에서 제대로 recording 되고 있는지 감지 못하는 경우 발생.
            // 따라서 Sister 프로그램에 대한 녹화 여부를 알게 해주는 API 를 추가적으로 사용함.
            // 아래의 경우 channel 을 전달하는 것음 없앰
            if (left != null) paintPrograms(g, left, type, ty);
            //if (left != null) paintPrograms(g, left, type, ty, ch.sister);
        }
    }

    //->Kenneth[2015.4.25] R5 : sisterCh 추가 : sister 채널에 대한 recording 이 있는지도 확인 필요
    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
    // Grid EPG 보여줄때 SD/HD 그룹핑 채널인 경우 채널은 SD 지만 프로그램 정보는 HD 로 바꿔 놓았음.
    // GridEpg.decideTargetChannel() 의 적용이 그것임. 그러나 그 side effect 로 recording -> B -> SD 로 녹화 채널 바꾸는
    // 경우 문제 발생. 또한 D 액션팝업, Grid program 에서 제대로 recording 되고 있는지 감지 못하는 경우 발생.
    // 따라서 Sister 프로그램에 대한 녹화 여부를 알게 해주는 API 를 추가적으로 사용함.
    // 아래의 경우 channel 을 전달하는 것음 없앰
    private void paintPrograms(Graphics g, Program left, int type, int fontY) {
        g.setFont(fText);
        if (left == null) {
            g.drawString(dataCenter.getString("epg.no_data_title"), CHANNEL_CELL_WIDTH + 5, fontY);
            return;
        }
        int h = vertBarHeights[type];
        long lastTime = grid.pageTime + GridPrograms.PAGE_TIME_GAP;
        Program p = left;
        int sx = 0;
        int ex = 0;
        while (true) {
            if (p == null) {
                if (ex == 0 || ex >= TOTAL_WIDTH) {
                    return;
                }
                sx = ex;
                ex = TOTAL_WIDTH;
                //->Kenneth[2015.4.6] R5 : Highlight 구현을 위해 Program 받도록 수정
                paintCell(g, true, sx, ex, noMore, h, type, fontY, false, false, p);
                //paintCell(g, true, sx, ex, noMore, h, type, fontY, false, false);
                ex = 0;
            } else {
                long pst = p.startTime;
                if (pst >= lastTime) {
                    return;
                }
                long pet = p.endTime;
                sx = getX(pst);
                ex = getX(pet);

                //->Kenneth[2015.4.6] R5 : Highlight 구현을 위해 Program 받도록 수정
                paintCell(g, p instanceof NullProgram, sx, ex, ParentalControl.getTitle(p), h, type, fontY,
                          pst < grid.pageTime, pet > lastTime, p);
                //paintCell(g, p instanceof NullProgram, sx, ex, ParentalControl.getTitle(p), h, type, fontY,
                //          pst < grid.pageTime, pet > lastTime);
                int cw = ex - sx;
                //->Kenneth[2015.4.25] R5 : sister recording 처리
                boolean drawRecordingRectangle = false;
                if (pcm.getStatus(p) == RecordingStatus.RECORDING) {
                    drawRecordingRectangle = true;
                }

                //->Kenneth[2016.3.7] R7 : Grouped 에서만 sister 체크
                if (EpgCore.isChannelGrouped) {
                    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
                    if (pcm.getSisterStatus(p) == RecordingStatus.RECORDING) {
                        drawRecordingRectangle = true;
                    }
                }
                //<-

                /*
                if (sisterCh != null) {
                    Program sisterProgram = EpgDataManager.getInstance().getProgram(sisterCh, pst);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridProgramsRenderer.paintPrograms() : sisterProgram = "+sisterProgram);
                    if (sisterProgram != null) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridProgramsRenderer.paintPrograms() : sisterProgram.RecordingStatus = "+pcm.getStatus(sisterProgram));
                        if (pcm.getStatus(sisterProgram) == RecordingStatus.RECORDING) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridProgramsRenderer.paintPrograms() : Draw Sister Recording Rectangle : "+sisterProgram);
                            drawRecordingRectangle = true;
                        }
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridProgramsRenderer.paintPrograms() : currentProgram = "+p);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridProgramsRenderer.paintPrograms() : currentProgram.recordingStatus = "+pcm.getStatus(p));
                }
                */
                if (drawRecordingRectangle) {
                    if (type == TYPE_NORMAL) {
                        // red normal
                        g.setColor(cRedRing);
                        //->Kenneth[2015.4.6] R5 : Highlights 구현중 recording 표시가 highlight 를 완전히 가리지 못함
                        // 그래서 33->35 로 해서 height 를 늘려줌
                        g.drawRect(sx+1, 1, cw-2, 35);
                        g.drawRect(sx+1+1, 1+1, cw-2-2, 35-2);
                        g.drawRect(sx+1+2, 1+2, cw-2-4, 35-4);
//                        g.drawImage(iRedNormalL, sx+2, 1, grid);
//                        g.drawImage(iRedNormalM, sx+12, 1, mw, 34, grid);
//                        g.drawImage(iRedNormalR, ex-11, 1, grid);
                    } else {
                        // red focus
                        if (cw >= 22) {
                            g.drawImage(iRedFocusL, sx+1, 0, grid);
                            g.drawImage(iRedFocusM, sx+11, 0, cw-22, 48, grid);
                            g.drawImage(iRedFocusR, ex-11, 0, grid);
                        } else {
                            g.setColor(cRedRingFoc);
                            g.drawRect(sx+1, 0, cw-2, 47);
                            g.drawRect(sx+1+1, 0+1, cw-2-2, 47-2);
                            g.drawRect(sx+1+2, 0+2, cw-2-4, 47-4);
                            g.drawRect(sx+1+3, 0+3, cw-2-6, 47-6);
                        }
                    }
                //->Kenneth[2016.3.7] : R7 Grouped 에서만 sister 체크
                //->Kenneth[2015.8.24] : R5 에서 sister program 도 체크할 것
                } else if (rmm.contains(p) || (EpgCore.isChannelGrouped && rmm.contains(p.getSisterProgram()))) {
                //} else if (rmm.contains(p) || rmm.contains(p.getSisterProgram())) {
                //} else if (rmm.contains(p)) {
                //<-
                //<-
                    if (type == TYPE_NORMAL) {
                        // green normal
                        g.setColor(cGreenRing);
                        g.drawRect(sx+1, 1, cw-2, 33);
                        g.drawRect(sx+1+1, 1+1, cw-2-2, 33-2);
                        g.drawRect(sx+1+2, 1+2, cw-2-4, 33-4);
//                        g.drawImage(iGreenNormalL, sx+2, 1, grid);
//                        g.drawImage(iGreenNormalM, sx+12, 1, ex-sx-23, 34, grid);
//                        g.drawImage(iGreenNormalR, ex-11, 1, grid);
                    } else {
                        // green focus
                        if (cw >= 24) {
                            g.drawImage(iGreenFocusL, sx+1, 0, grid);
                            g.drawImage(iGreenFocusM, sx+11, 0, cw-22, 48, grid);
                            g.drawImage(iGreenFocusR, ex-11, 0, grid);
                        } else {
                            g.setColor(cGreenRing);
                            g.drawRect(sx+1, 0, cw-2, 47);
                            g.drawRect(sx+1+1, 0+1, cw-2-2, 47-2);
                            g.drawRect(sx+1+2, 0+2, cw-2-4, 47-4);
                            g.drawRect(sx+1+3, 0+3, cw-2-6, 47-6);
                        }
                    }
                }

                p = p.next;
                if (p == null && ex < TOTAL_WIDTH) {
                    g.setColor(cBar[type]);
                    g.fillRect(ex, 0, 1, h);
                }
            }
            if (sx != CHANNEL_CELL_WIDTH) {
                //->Kenneth[2015.2.10] : 4K
                //g.setColor(cBar[type]);
                //->Kenneth[2015.5.3] 4K : 4월 28일 황성화씨 메일 요청에 의해
                g.setColor(c38_38_38);
                //g.fillRect(sx, 0, 1, h);
                if (type == TYPE_NORMAL) {
                    // 포커스 안된놈
                    g.fillRect(sx, 0, 2, h+2);
                } else {
                    // 포커스 된놈
                    g.fillRect(sx, 0, 2, h+2);
                }
            }
        }
    }

    //->Kenneth[2015.4.6] R5 : Highlight 구현을 위해 Program 받도록 수정
    private void paintCell(Graphics g, boolean isNull, int sx, int ex, String title, int h,
                    int type, int fontY, boolean contLeft, boolean contRight, Program program) {
        boolean normal = type == TYPE_NORMAL;
        //->Kenneth[2015.4.6] R5 : highlight 그리기
        boolean highlighted = Highlight.getInstance().needToHighlighted(program);
        int width = ex - sx;
        if (isNull) {
            if (normal) {
                //->Kenneth[2015.2.10] 4K : flat design 으로 인해 이미지 아닌 컬러로 bg 그린다
                g.setColor(c45_45_45);
                g.fillRect(sx+1, 1, width-2, 35);
                /*
                if (width > 21) {
                    //->Kenneth : Data is not available 의 BG 그려주는 부분.
                    // 보니까 위 아래 구분이 안되게 되어 있음 현재 이미지가.
                    // 코멘트만 추가하고 코드 수정은 하지 않았음.
                    g.drawImage(iNoInfoL, sx + 1, 0, grid);
                    g.drawImage(iNoInfoR, ex - 10, 0, grid);
                    g.drawImage(iNoInfoM, sx + 11, 0, width - 21, 36, grid);
                } else {
                    g.drawImage(iNoInfoM, sx + 1, 0, width, 36, grid);
                }
                */
            } else {
                if (width > 20) {
                    g.drawImage(iNoInfoFocL, sx + 1, 0, grid);
                    g.drawImage(iNoInfoFocR, ex - 10, 0, grid);
                    g.drawImage(iNoInfoFocM, sx + 11, 0, width - 21, 46, grid);
                } else {
                    g.drawImage(iNoInfoFocM, sx + 1, 0, width, 46, grid);
                }
            }
            g.setColor(cNullProgramTitle[type]);
            if (type == TYPE_NORMAL) {
                type = 3;
            }
        } else {
            //->Kenneth[2015.4.6] R5 : highlight 그리기
            //->Kenneth[2015.6.11] : 포커스 된 채널에 속한 프로그램도 highlight 되어야 함
            if (highlighted) {
                if (normal) {
                    // 선택되지 않은 채널의 highlight
                    g.setColor(GridEpgRenderer.HIGHLIGHT_BG_COLOR);
                    g.fillRect(sx+1, 1, width-1, 35);
                } else {
                    // 선택된 채널의 포커스 없는 프로그램의 highlight
                    if (program != null && program.equals(grid.focusedEvent)) {
                        // 포커스 된 프로그램은 highlight 그리지 않음
                    } else {
                        g.setColor(GridEpgRenderer.HIGHLIGHT_BG_COLOR);
                        g.fillRect(sx+1, 1, width-1, 46);
                        //g.fillRect(sx+1, 1, width-2, 46);
                    }
                }
            }
            g.setColor(cProgramTitle[type]);
        }
        int tw = width - 11;
        int tx = sx + 9;
        if (contLeft) {
            if (tw > 1) {
                g.drawImage(iTrL[type], tx, fontY - 10, grid);
            }
            tx += 13;
            tw -= 13;
        }
        if (contRight) {
            if (tw > 5) {
                g.drawImage(iTrR[type], ex - 11, fontY - 10, grid);
            }
            tw -= 13;
        }
        if (highlighted && normal) {
            g.setColor(GridEpgRenderer.HIGHLIGHT_FG_COLOR);
        }
        g.drawString(TextUtil.shorten(title, fmText, tw), tx, fontY);
    }

    private void paintFocus(Graphics g, UIComponent c, Program p) {
        int expandState = grid.expandState;

        int minWidth;
        if (expandState != 0) {
            minWidth = MIN_FOCUS_WIDTH;
        } else {
            minWidth = 25;  // left & right image width
        }

        long pst;
        long pet;
        if (p != null) {
            pst = p.getStartTime();
            pet = p.getEndTime();
        } else {
            pst = grid.pageTime;
            pet = grid.pageTime + GridPrograms.PAGE_TIME_GAP;
        }

        int x = getX(pst);
        int width = Math.max(getX(pet) - x, minWidth);

        if (x + width > TOTAL_WIDTH) {
            // focus right position is out of bounds
            width = Math.max(minWidth, TOTAL_WIDTH - x);
            x = TOTAL_WIDTH - width;
        }

        // yellow focus
        //->Kenneth[2015.2.10] 4K 
        // 포커스 된 노란 BG 가 왼쪽으로 좀 나와 보여서 수정
        //g.drawImage(iFocusL, x-5, -6, c);
        //g.drawImage(iFocusM, x+10, -6, width-20, 60, c);
        //g.drawImage(iFocusR, x+width-10, -6, c);
        g.drawImage(iFocusL, x-5+1, -6, c);
        g.drawImage(iFocusM, x+10, -6, width-20, 60, c);
        g.drawImage(iFocusR, x+width-10, -6, c);

        //->Kenneth[2016.3.7] : R7 Grouped 에서만 sister 체크
        //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
        if (pcm.getStatus(p) == RecordingStatus.RECORDING || (EpgCore.isChannelGrouped && pcm.getSisterStatus(p) == RecordingStatus.RECORDING)) {
        //if (pcm.getStatus(p) == RecordingStatus.RECORDING || pcm.getSisterStatus(p) == RecordingStatus.RECORDING) {
        //if (pcm.getStatus(p) == RecordingStatus.RECORDING) {
        //<-

            // red focus
            g.drawImage(iRedFocusL, x-1, -1, c);
            g.drawImage(iRedFocusM, x+9, -1, width-19, 48, c);
            g.drawImage(iRedFocusR, x+width-10, -1, c);
        } else if (rmm.contains(p)) {
            // green focus
            g.drawImage(iGreenFocusL, x-1, -1, c);
            g.drawImage(iGreenFocusM, x+9, -1, width-19, 48, c);
            g.drawImage(iGreenFocusR, x+width-10, -1, c);
        }

        String title;
        if (p == null) {
            title = dataCenter.getString("epg.no_data_title");
        } else {
            title = ParentalControl.getTitle(p);
        }

        if (expandState == 0) {
            //->Kenneth[2015.4.6] R5 : Highlight 구현을 위해 Program 받도록 수정
            // normal small focus
            paintCell(g, false, x, x + width, ParentalControl.getTitle(p), 0, TYPE_FOCUS_PROGRAM, 28,
                        pst < grid.pageTime, pet > grid.pageTime + GridPrograms.PAGE_TIME_GAP, p);
            //paintCell(g, false, x, x + width, ParentalControl.getTitle(p), 0, TYPE_FOCUS_PROGRAM, 28,
            //            pst < grid.pageTime, pet > grid.pageTime + GridPrograms.PAGE_TIME_GAP);

//            title = TextUtil.shorten(title, fmFocusText, width - 15);
//            g.setFont(fFocusText);
//            g.setColor(cFocusText);
//            g.drawString(title, x + 10, 28);
        } else if (expandState == 1 && p != null) {
            // expanded icon focus
            Formatter fm = Formatter.getCurrent();
            int ix = x + 12;
            if (ParentalControl.getLevel(p) != ParentalControl.OK) {
                g.drawImage(iBlockedFoc, ix, 22, c);
                ix += 20;
            }
            Channel ch = grid.getFocusedChannel();
			//->Kenneth : 4K by June
            int def = Util.getDefinition(ch, p);
            //if (def >= Constants.DEFINITION_HD) {  // TODO - 4K
            //    g.drawImage(iHd, ix, 26, c);
            //    ix += 26;
            //}
            if (def == Constants.DEFINITION_4K) { // 4K 
                g.drawImage(iUhd, ix, 26, c);
                ix += 28;
            } else if (def == Constants.DEFINITION_HD) { // HD 
                g.drawImage(iHd, ix, 26, c);
                ix += 26;
            }
            Image im = ratingImages[p.getRatingIndex()];
            if (im != null) {
                g.drawImage(im, ix, 26, c);
                ix += 26;
            }
            if (p.isCaptioned()) {
                g.drawImage(iCCFoc, ix, 26, c);
                ix += 26;
            }

            int xr = x + width - 12;
            // recording icon
            switch (grid.recordingStatus) {
                case RecordingStatus.RECORDING:
                    g.drawImage(iRec, xr - 31, 24, c);
                    xr -= 34;
                    break;
                case RecordingStatus.SKIPPED:
                    g.drawImage(iSkipped, xr - 31, 24, c);
                    xr -= 34;
                    break;
            }
            if (rmm.contains(p)) {
                g.drawImage(iRemind, xr - 17, 24, c);
            }


            g.setFont(fTime);
            g.setColor(cTime);
            //->Kenneth[2016.8.24] CYO Bundling/A la carte
            if (ch.isISAPurchasable()) {
            //if (ch.isPurchasable()) {
            //<-
                g.drawString(dataCenter.getString("epg.subscribe_now"), ix + 4, 26+12);
            }

            if (p.isOnAir()) {
                String s = fm.getTime(pst);
                g.drawString(s, x+12, 20);
                //->Kenneth[2015.4.11] 오른쪽으로 칸이 넘어가서 보기 싫으므로 수정
                g.drawString(fm.getTime(pet), x+width-57-12, 20);
                //g.drawString(fm.getTime(pet), x+width-57, 20);

                int barX = x + 12 + 5 + fmTime.stringWidth(s);
                //->Kenneth[2015.4.11] 오른쪽으로 칸이 넘어가서 보기 싫으므로 수정
                int barWidth = x + width - 57 - 5 - barX - 12;
                //int barWidth = x + width - 57 - 5 - barX;

                g.setColor(cProgressBg);
                g.fillRect(barX, 11, barWidth, 6);

                int bar = (int) (barWidth * (System.currentTimeMillis() - pst) / (pet - pst));
                bar = Math.max(0, Math.min(bar, barWidth));
                g.setColor(cProgressFg);
                g.fillRect(barX, 11, bar, 6);
            } else {
                g.drawString(fm.getTime(pst) + "  -  " + fm.getTime(pet), x+12, 20);
            }
        } else {
            // expanded text focus
            String[] s = TextUtil.split(title, fmFocusText, width - 15, 2);
            g.setFont(fFocusText);
            g.setColor(cFocusText);
            if (s.length == 1) {
                g.drawString(s[0], x + 10, 28);
            } else if (s.length >= 2) {
                g.drawString(s[0], x + 10, 19);
                g.drawString(s[1], x + 10, 39);
            }
        }

        // manual recording
        if (RecordingStatus.SHOW_MANUAL_RECORINGS_IN_GUIDE) {
            long firstTime = grid.pageTime;
            long lastTime = grid.pageTime + GridPrograms.PAGE_TIME_GAP;
            int sy = HEADER_HEIGHT - V_GAP * grid.prevSize - HEADER_HEIGHT;
            for (int row = 0; row < grid.rowSize; row++) {
                int index = grid.getIndexOfRow(row);
                if (Log.ALL_ON) Log.printDebug("");
                if (Log.ALL_ON) Log.printDebug("");
                if (Log.ALL_ON) Log.printDebug("paintFocus() ===============================================================");
                if (Log.ALL_ON) Log.printDebug("paintFocus() : row = "+row);
                if (Log.ALL_ON) Log.printDebug("paintFocus() : index = "+index);
                if (index < 0) {
                    //->Kenneth[2016.12.6] VDTRMASTER-6016 : Favatite filter 적용시 채널 갯수가 화면에 꽉차지 않는 경우
                    // 여기서 바로 continue 하지 말고 V_GAP 만큼 더해줘야 한다.
                    //->Kenneth[2016.12.12] VDTRMASTER-6022 때문에 긴급 패치 하면서 6016 수정은 잠시 막아둔다.
                    // 다음 릴리즈 할 때 아래 comment out 된 것을 풀어줘야 한다.
                    //->Kenneth[2016.12.16] VDTRMASTER-6016 : 6022 릴리즈 했으므로 다시 fix 한다. 
                    sy += V_GAP;
                    //<-
                    continue;
                }
                //->Kenneth[2015.8.31] 다른 로그 추적하다가 여기서 NullPointerException 나는 경우 있음.
                // grid.channelList 가 null 인 경우가 있음. 그래서 아래는 null check 하도록 합.
                Channel ch = null;
                if (grid.channelList != null) ch = grid.channelList.getChannelAt(index);

                if (Log.ALL_ON) Log.printDebug("paintFocus() : ch = "+ch);
                Color cRed;
                int ry, rh;
                if (row != grid.prevSize) {
                    cRed = cRedRing;
                    ry = sy + 1;
                    rh = 6;
                    sy += V_GAP;
                } else {
                    cRed = cRedRingFoc;
                    ry = sy - 1;
                    rh = 7;
                    sy += FOCUS_HEIGHT;
                }

                if (Log.ALL_ON) Log.printDebug("paintFocus() : sy = "+sy);
                if (Log.ALL_ON) Log.printDebug("paintFocus() : ry1 = "+ry);
                RangeList manual = null;
                if (ch != null) {
                    manual = RecordingStatus.getInstance().getManualRecordingTimes(ch);
                    //->Kenneth[2015.8.24] VDTRMASTER-5589
                    // R5 에서는 sister channel 도 확인해야 함.
                    try {

                        //->Kenneth[2016.3.7] R7 : Grouped 상태에서만 sister 체크
                        if (manual == null && EpgCore.isChannelGrouped && ch.getSisterChannel() != null) {
                        //if (manual == null && ch.getSisterChannel() != null) {
                        //<-

                            manual = RecordingStatus.getInstance().getManualRecordingTimes((Channel)ch.getSisterChannel());
                        }
                    } catch (Exception e) {
                        Log.print(e);
                    }
                }
                //<-
                if (manual != null) {
                    int size = manual.size();
                    for (int i = 0; i < size; i++) {
                        try {
                            Range r = (Range) manual.elementAt(i);
                            if (r.intersects(firstTime, lastTime)) {
                                int xs = getX(r.from);
                                int xe = getX(r.to);
                                g.setColor(cRed);
                                g.fillRect(xs, ry, xe-xs, rh);
                                if (Log.ALL_ON) Log.printDebug("paintFocus() : ry2 = "+ry);
                            }
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                }
            }
        }

        // arrow
        int ax = x + width/2 - 11;
        Point point;
        if (grid.navigable[GridPrograms.MOVE_UP]) {
            point = grid.arrowPositions[GridPrograms.MOVE_UP];
            point.x = ax;
            point.y = -14;
            g.drawImage(iArrowU, point.x, point.y, c);
            point.y += V_GAP * grid.prevSize + HEADER_HEIGHT;
        }
        if (grid.navigable[GridPrograms.MOVE_DOWN]) {
            point = grid.arrowPositions[GridPrograms.MOVE_DOWN];
            point.x = ax;
            point.y = 43;
            g.drawImage(iArrowD, point.x, point.y, c);
            point.y += V_GAP * grid.prevSize + HEADER_HEIGHT;
        }
        if (grid.navigable[GridPrograms.MOVE_LEFT]) {
            point = grid.arrowPositions[GridPrograms.MOVE_LEFT];
            point.x = x-14;
            point.y = 11;
            g.drawImage(iArrowL, point.x, point.y, c);
            point.y += V_GAP * grid.prevSize + HEADER_HEIGHT;
        }
        if (grid.navigable[GridPrograms.MOVE_RIGHT]) {
            point = grid.arrowPositions[GridPrograms.MOVE_RIGHT];
            point.x = x+width-2;
            point.y = 11;
            g.drawImage(iArrowR, point.x, point.y, c);
            point.y += V_GAP * grid.prevSize + HEADER_HEIGHT;
        }
    }

    private int getProgressWidth(Program p, int max) {
        int bar = (int) (max * (System.currentTimeMillis() - p.getStartTime()) / p.getDuration());
        return Math.max(0, Math.min(bar, max));
    }

    private int getX(long time) {
        long t = Math.max(0, time - grid.pageTime);
        int x = CHANNEL_CELL_WIDTH + (int) (t * WIDTH_PER_HOUR / Constants.MS_PER_HOUR);
        return Math.min(TOTAL_WIDTH, x);
    }

}

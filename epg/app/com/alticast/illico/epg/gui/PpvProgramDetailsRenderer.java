package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.ui.PpvProgramDetails;
import com.alticast.illico.epg.data.PpvProgram;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.Genre;
import com.alticast.illico.epg.ParentalControl;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class PpvProgramDetailsRenderer extends ProgramDetailsRenderer {
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("MM/dd HH:mm:ss");

    private Image iEnglish = dataCenter.getImage("icon_en.png");
    private Image iFrench = dataCenter.getImage("icon_fr.png");
    private Image iCC = dataCenter.getImage("icon_cc.png");

    private Color cDur = new Color(179, 179, 179);
    private Font fDur = FontResource.BLENDER.getFont(17);

    private Color cInfoName = new Color(182, 182, 182);
    private Font fInfoName = FontResource.BLENDER.getFont(16);

    private Color cInfo = new Color(235, 235, 235);
    private Font fInfo = FontResource.DINMED.getFont(15);

    private Color cPrice = new Color(242, 231, 207);
    private Font fPrice = FontResource.BLENDER.getFont(18);

    private Color cAvail = new Color(225, 195, 80);
    private Font fAvail = FontResource.BLENDER.getFont(17);

    private FontMetrics fmPrice = FontResource.getFontMetrics(fPrice);
    private FontMetrics fmDur = FontResource.getFontMetrics(fDur);

    public void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        PpvProgramDetails d = (PpvProgramDetails) c;
        PpvProgram p = d.ppv;

        if (Log.EXTRA_ON) {
            long end = p.endTime;
            g.setFont(fInfoName);
            g.setColor(Color.white);
            int cx = getX(System.currentTimeMillis(), end);
            g.drawLine(cx, 160, cx, 235);

            Program np = p.next;
            if (np != null && np instanceof PpvProgram) {
                PpvProgram n = (PpvProgram) np;
                drawWindow(g, Color.black, n.marketingWindowStartTime, n.marketingWindowEndTime, end, 165);
                drawWindow(g, Color.black, n.adWindowStartTime, n.adWindowEndTime, end, 180);
                drawWindow(g, Color.black, n.buyWindowStartTime, n.buyWindowEndTime, end, 195);
                drawWindow(g, Color.black, n.buyWindowStartTime, n.cancelWindowEndTime, end, 210);
                drawWindow(g, Color.black, n.startTime, n.endTime, end, 225);
            }

            drawWindow(g, Color.magenta, "mkt", p.marketingWindowStartTime, p.marketingWindowEndTime, end, 165);
            drawWindow(g, Color.yellow, "adv", p.adWindowStartTime, p.adWindowEndTime, end, 180);
            drawWindow(g, Color.green, "buy", p.buyWindowStartTime, p.buyWindowEndTime, end, 195);
            drawWindow(g, Color.cyan, "can", p.buyWindowStartTime, p.cancelWindowEndTime, end, 210);
            drawWindow(g, Color.orange, "evt", p.startTime, p.endTime, end, 225);

            g.setColor(Color.green);
            g.drawString("eid = " + p.eid + ", pkg_id = " + p.packageId, 315, 155);
        }

        if (d.blockLevel == ParentalControl.ADULT_CONTENT) {
            return;
        }

        Formatter f = Formatter.getCurrent();

        g.setColor(cPrice);
        g.setFont(fPrice);

        g.setColor(cPrice);
        g.setFont(fPrice);
        String s = f.getPrice(p.price) + "  |  ";
        g.drawString(s, 55, 189);
        int x = 55 + fmPrice.stringWidth(s);

        g.setColor(cAvail);
        g.setFont(fAvail);
        g.drawString(dataCenter.getString("ppv.available_until") + " : " + f.getLongDate(p.buyWindowEndTime), x, 189);

        g.setColor(cDur);
        g.setFont(fDur);
        s = f.getDurationText(p.getDuration()) + "  |  " + Genre.getName(p.getConvertedCategory()) + "  |  ";
        Image im;
        x = 55;
        int y = 209;
        g.drawString(s, x, y);
        x += fmDur.stringWidth(s);
        if (Constants.LANGUAGE_ENGLISH.equals(p.getLanguage())) {
            im = iEnglish;
            g.drawImage(im, x, y-12, c);
            x += im.getWidth(c) + 1;
        } else if (Constants.LANGUAGE_FRENCH.equals(p.getLanguage())) {
            im = iFrench;
            g.drawImage(im, x, y-12, c);
            x += im.getWidth(c) + 1;
        }
        if (p.isCaptioned()) {
            g.drawImage(iCC, x, y-12, c);
        }

        s = p.getDirector();
        if (s != null && s.length() > 0) {
            g.setColor(cInfoName);
            g.setFont(fInfoName);
            g.drawString(dataCenter.getString("epg.director"), 54, 237);
            g.setColor(cInfo);
            g.setFont(fInfo);
            g.drawString(s, 127, 237);
        }

        s = p.getActors();
        if (s != null && s.length() > 0) {
            g.setColor(cInfoName);
            g.setFont(fInfoName);
            g.drawString(dataCenter.getString("epg.actors"), 54, 257);
            g.setColor(cInfo);
            g.setFont(fInfo);
            g.drawString(p.getActors(), 127, 257);
        }
    }

    private void drawWindow(Graphics g, Color c, long from, long to, long maxTime, int y) {
        g.setColor(c);
        int l = getX(from, maxTime);
        int r = getX(to, maxTime);
        g.drawRect(l, y-1, r-l, 7);
    }

    private void drawWindow(Graphics g, Color c, String name, long from, long to, long maxTime, int y) {
        g.setColor(c);
        int l = getX(from, maxTime);
        int r = getX(to, maxTime);
        g.fillRect(l, y, r-l, 6);
        g.drawString(name + " " + timeFormat.format(new Date(from))
                        + " - " + timeFormat.format(new Date(to)), 663, y + 9);
    }

    private int getX(long time, long maxTime) {
        return 650 - (int) ((maxTime - time) * 50 / Constants.MS_PER_HOUR);
    }

}

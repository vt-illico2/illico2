package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.ui.VolumeBar;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import org.dvb.ui.DVBColor;

public class VolumeBarRenderer extends Renderer {

    DataCenter dataCenter = DataCenter.getInstance();

    Rectangle bounds = new Rectangle(0, 396, 960, 154);

    Font fNumber = FontResource.BLENDER.getFont(24);

    Color cBg = new DVBColor(0, 0, 0, 20 * 255 / 100);
    Color cNumber = Color.white;
    Color cMuted = new Color(124, 124, 124);

    Image iBg = dataCenter.getImage("volumebg.png");
    Image iBar = dataCenter.getImage("v_gauge.png");
    Image iBarMute = dataCenter.getImage("v_gauge_mute.png");
//    Image iBarEmpty = dataCenter.getImage("v_gauge_dim.png");

    Image iVolume = dataCenter.getImage("volume_txt.png");
    Image iMuteFr = dataCenter.getImage("mute_txt_fr.png");
    Image iMuteEn = dataCenter.getImage("mute_txt_en.png");

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void paint(Graphics g, UIComponent c) {
        VolumeBar v = (VolumeBar) c;
        g.drawImage(iBg, 0, 0, c);
        g.setColor(cBg);
        g.fillRect(375, 71, 239, 14);

        g.setFont(fNumber);
        Image im;
        if (v.isMuted) {
            im = iBarMute;
            g.drawImage(dataCenter.getImageByLanguage("mute_txt.png"), 296, 65, c);
            g.setColor(cMuted);
        } else {
            im = iBar;
            g.drawImage(iVolume, 296, 65, c);
            g.setColor(cNumber);
        }
        int cv = v.currentVolume;
        int max = v.maxLevel;
        int x = 376;
//        int i = 0;
        for (int i = 0; i < cv; i++) {
            g.drawImage(im, x, 72, c);
            x += 8;
        }
//        for (; i < max; i++) {
//            g.drawImage(iBarEmpty, x, 72, c);
//            x += 8;
//        }
        GraphicUtil.drawStringCenter(g, v.currentVolumeString, 640, 85);
    }

}

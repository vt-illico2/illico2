package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.data.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.*;

public class IncompatibilityChannelPanelRenderer extends AbstractBlockedPanelRenderer {

    protected Color cMessage = Color.white;
    protected Font fMessage = FontResource.BLENDER.getFont(22);

    protected Color cNormal = new Color(241, 241, 241);
    protected Font fNormal = FontResource.BLENDER.getFont(24);

    //->Kenneth[2015.10.13] DDC-114
    //private Color[] cHighlights = new Color[] { new Color(255, 203, 0) };
    protected Color yellowColor = new Color(252, 202, 4);
    //<-

    protected Color cOr = Color.white;
    protected Font fOr = FontResource.BLENDER.getFont(20);

    protected Color cInfo = new Color(172, 172, 172);
    //protected Color cYellow = new Color(249, 194, 0);
    protected Font fInfo = FontResource.BLENDER.getFont(16);
    protected FontMetrics fmInfo = FontResource.getFontMetrics(fInfo);
    private FontMetrics fmName = FontResource.getFontMetrics(fName);

    protected Color cAbout = new Color(200, 200, 200);
    protected Font fAbout = FontResource.BLENDER.getFont(22);

    private Image i_Group_371 = dataCenter.getImage("Group_371.png");
    private Image i_chlogobg = dataCenter.getImage("chlogobg.png");

    private Image icon_uhd = dataCenter.getImage("icon_uhd.png");

    private String title;
    private String or;
    private String goTo;
    private String callTo;
    private String url;
    private String tel;
    private String about;

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
        IncompatibilityChannelPanel d = (IncompatibilityChannelPanel) c;
        Channel ch = d.channel;
        String callLetter = ch.getCallLetter();

        about = dataCenter.getString("epg.about");
        title = dataCenter.getString("epg.uhd_title");

        or = dataCenter.getString("epg.or");
        goTo = dataCenter.getString("epg.go_to") + " ";
        callTo = dataCenter.getString("epg.call_to") + " ";
        url = dataCenter.getString("epg.URL");
        tel = dataCenter.getString("epg.PHONE_NUMBER");

    }

    protected void paintBackground(Graphics g, Channel ch, UIComponent c)  {
        g.setColor(cBg);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(i_12_ppop_sha, 101, 479, 759, 57, c);
        g.drawImage(i_pop_759_bg, 71, 26, c);

        Image logo = ch.getLogo();

        Util.drawChannelIcon(g, ch, 118, 64, c);
        g.setColor(cNumber);
        g.setFont(fNumber);
        g.drawString(String.valueOf(ch.getNumber()), 146, 83);
        g.drawImage(icon_uhd, 190, 71, c);
        g.drawImage(logo, 226, 63, c);
        g.setFont(fName);
        String fullName = ch.getFullName();
        if (fullName != null && fullName.length() > 0) {
            g.drawString(fullName, 840-fmName.stringWidth(fullName), 83);
        }
    }

    public void paint(Graphics g, UIComponent c) {
        IncompatibilityChannelPanel d = (IncompatibilityChannelPanel) c;
        Channel ch = d.channel;
        paintBackground(g, ch, c);

        g.drawImage(i_Group_371, 121, 360, c);
        g.drawImage(i_chlogobg, 185, 145, c);
        GraphicUtil.drawImageCenter(g, ch.getLogo(), 247, 204, c);

        g.setFont(fMessage);
        g.setColor(cMessage);
        g.drawString(title, 359, 179);

        //->Kenneth[2015.10.13] DDC-114
        g.setFont(fInfo);
        g.setColor(yellowColor);
        String reasonMsg = "";
        if (d.reasonCode == IncompatibilityChannelPanel.REASON_NO_UHD_PACKAGE) {
            reasonMsg = dataCenter.getString("epg.incomp_reason_no_uhd_package");
        } else if (d.reasonCode == IncompatibilityChannelPanel.REASON_EPG850) {
            reasonMsg = dataCenter.getString("epg.incomp_reason_epg850");
        } else if (d.reasonCode == IncompatibilityChannelPanel.REASON_EPG851) {
            reasonMsg = dataCenter.getString("epg.incomp_reason_epg851");
        }
        g.drawString(reasonMsg, 480-fmInfo.stringWidth(reasonMsg)/2, 295);
        //<-
        g.setFont(fOr);
        g.setColor(cOr);
        g.drawString(or, 202, 320);
        g.drawString(or, 520, 320);

        g.setFont(fInfo);
        g.setColor(cInfo);
        g.drawString(goTo, 225, 320);
        g.drawString(callTo, 546, 320);
        //->Kenneth[2015.4.22] R5 Julie 요청으로 white 로 바꿈
        g.setColor(Color.white);
        g.drawString(url, 225+fmInfo.stringWidth(goTo), 320);
        g.drawString(tel, 546+fmInfo.stringWidth(callTo), 320);

        g.setFont(fAbout);
        g.setColor(cAbout);
        g.drawString(about, 122, 400);
    }
}

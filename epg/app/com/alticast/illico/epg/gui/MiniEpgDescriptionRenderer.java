package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class MiniEpgDescriptionRenderer extends Renderer {

    private Rectangle bounds = new Rectangle(328, 131, 669+17-328, 154);

    private DataCenter dataCenter = DataCenter.getInstance();
    private Image iArrowRight = dataCenter.getImage("02_ars_r.png");
    private Image iBackground = dataCenter.getImage("02_mini_foc2.png");

//    private Image iBlockEn = dataCenter.getImage("02_block_des_mini_en.png");
//    private Image iBlockFr = dataCenter.getImage("02_block_des_mini_fr.png");

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
    }

    public void paint(Graphics g, UIComponent c) {
        g.drawImage(iBackground, 0, 0, c);
        if (((MiniEpgDescription) c).hasNext) {
            g.drawImage(iArrowRight, 669-328, 368-331, c);
        }

//        if (MiniEpg.blockLevel == ParentalControl.ADULT_CONTENT) {
//            g.drawImage(dataCenter.getImageByLanguage("02_block_des_mini.png"), 345-328, 349-331, c);
//        }
    }



}

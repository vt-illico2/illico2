package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.*;
import java.awt.*;
import java.util.*;
import org.dvb.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;

public class GridEpgRenderer extends FullScreenPanelRenderer {
    private static final Rectangle VIDEO_BOUNDS = new Rectangle(628, 84, 284, 160);

    private DataCenter dataCenter = DataCenter.getInstance();

    private Image iAvOut = dataCenter.getImage("02_av_out.png");

    //->Kenneth[2015.4.3] R5 : Flat design 에 의해서 없어진 놈
    //private Image iShadow = dataCenter.getImage("02_grid_sh.png");
    //private Image iShadow2 = dataCenter.getImage("02_grid_sh2.png");
    //private Image iShadow3 = dataCenter.getImage("02_grid_sh3.png");

    private Font fText = FontResource.BLENDER.getFont(18);
    private Font fFilter = FontResource.BLENDER.getFont(18);
    private Font fDebug = FontResource.BLENDER.getFont(14);
    private FontMetrics fmFilter = FontResource.getFontMetrics(fFilter);

    private Color cTitle = new Color(239, 239, 239);
    private Color cAllFilter = new Color(220, 220, 220);
    private Color cFilter = new Color(0xffcb00);

    private Color cBlue = new Color(24, 50, 205);
    private Color cGreen = new Color(24, 205, 50);

    private Color cGray = new Color(172, 172, 172);
    //->Kenneth[2015.9.3] R5 : bg color 바꿈
    //->Kenneth[2015.8.26] R5 : bg color 바꿈
    //->Kenneth[2015.8.19] R5 : bg color 바꿈
    //->Kenneth[2015.8.13] R5 : bg color 바꿈
    //->Kenneth[2015.7.27] R5 : bg color 바꿈
    //->Kenneth[2015.4.6] R5 : [2015.6.2] : bg color 바꿈
    public static final Color HIGHLIGHT_BG_COLOR = new Color(17, 107, 112);
    //public static final Color HIGHLIGHT_BG_COLOR = new Color(20, 75, 126);
    //public static final Color HIGHLIGHT_BG_COLOR = new Color(0, 27, 164);
    //public static final Color HIGHLIGHT_BG_COLOR = new Color(41, 81, 126);
    //public static final Color HIGHLIGHT_BG_COLOR = new Color(128, 128, 108);
    //public static final Color HIGHLIGHT_BG_COLOR = new Color(99, 116, 124);
    public static final Color HIGHLIGHT_FG_COLOR = new Color(255, 255, 255);

    private String[] text;

    private GridEpg gridEpg;

    //->Kenneth[2015.4.3] R5
    private Image filterIcon;
    private Image highlightIcon;

    //->Kenneth[2015.4.3] R5
    public void prepare(UIComponent c) {
        super.prepare(c);
        Hashtable btnTable = (Hashtable) SharedMemory.getInstance().get("Footer Img");
        filterIcon = (Image)btnTable.get(PreferenceService.BTN_A);
        highlightIcon = (Image)btnTable.get(PreferenceService.BTN_C);
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        gridEpg = (GridEpg) c;
        return Constants.SCREEN_BOUNDS;
    }

    public Rectangle getVideoBounds() {
        return VIDEO_BOUNDS;
    }

    //->Kenneth[2017.2.27] R7.3 : Highlight 와 filter 관련 animation 정보
    public static int filterAnimationStartX = 0;
    public static int filterAnimationWidth = 0;
    public static int highlightAnimationStartX = 0;
    public static int highlightAnimationWidth = 0;

    public void calculateAnimationPosition(String filterKey) {
        int startX = 50;
        int space = 10;
        String temp = dataCenter.getString("epg.my_filters");
        startX += fmFilter.stringWidth(temp)+space;
        startX += filterIcon.getWidth(gridEpg)+space-5;
        filterAnimationStartX = startX;
        temp = dataCenter.getString(filterKey);
        startX += fmFilter.stringWidth(temp);
        filterAnimationWidth = fmFilter.stringWidth(temp);
        int chSize = GridEpg.channelList.size() + GridEpg.channelList.getSisterSize();
        if (filterKey.equals(Rs.MENU_FILTER_HD.getKey())) {
            chSize = GridEpg.channelList.size();
        }
        if (!EpgCore.isChannelGrouped) {
            chSize = GridEpg.channelList.size();
        }
        temp = "("+chSize+")";
        startX += fmFilter.stringWidth(temp)+space;
        filterAnimationWidth += fmFilter.stringWidth(temp);
        startX += 2+space;
        startX += highlightIcon.getWidth(gridEpg)+space-5;
        String curHighlightName = Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight());
        if (Highlight.getInstance().isNone()) {
            curHighlightName = dataCenter.getString("epg.highlights");
        }
        highlightAnimationStartX = startX;
        highlightAnimationWidth = fmFilter.stringWidth(curHighlightName)+10;
    }
    //<-

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        //->Kenneth[2015.4.3] R5 : Flat design 에 의해서 없어진 놈
        /*
        if (gridEpg.type == GridEpg.TYPE_DEFAULT) {
            g.drawImage(iShadow2, 44, 247, c);
        } else {
            g.drawImage(iShadow3, 44, 75, c);
        }
        if (gridEpg.details == null) {
            g.drawImage(iShadow, 7, 425, c);
        }
        */

        //->Kenneth[2017.2.27] R7.3 : Highlight 와 filter 관련해서 GUI 수정됨
        int startY;
        if (gridEpg.type == GridEpg.TYPE_DEFAULT) {
            g.drawImage(iAvOut, 622, 84, c);
            startY = 242;
        } else {
            startY = 98;
        }
        int startX = 50;
        int space = 10;
        // "My Filters"
        String temp = dataCenter.getString("epg.my_filters");
        g.setColor(cAllFilter);
        g.drawString(temp, startX, startY);
        startX += fmFilter.stringWidth(temp)+space;
        // filter icon
        g.drawImage(filterIcon, startX, startY-15, c);
        startX += filterIcon.getWidth(c)+space-5;
        //filterAnimationStartX = startX;
        // filter name
        String filterKey = ChannelController.get(0).getCurrentFilterKey();
        temp = dataCenter.getString(filterKey);
        g.setColor(ChannelController.ALL_FILTER_KEY.equals(filterKey) ? cAllFilter : cFilter);
        g.drawString(temp, startX, startY);
        startX += fmFilter.stringWidth(temp);
        //filterAnimationWidth = fmFilter.stringWidth(temp);
        // channel size
        //->Kenneth[2015.7.16] R5 : VDTRMASTER-5506 sister 채널 갯수를 알 필요가 있음.
        int chSize = GridEpg.channelList.size() + GridEpg.channelList.getSisterSize();
        //->Kenneth[2015.7.20] R5 : HD 채널의 경우는 sister 채널 갯수를 더하지 않는다.
        if (filterKey.equals(Rs.MENU_FILTER_HD.getKey())) {
            chSize = GridEpg.channelList.size();
        }
        //->Kenneth[2016.3.7] R7 : Ungrouped 이면 sister 는 카운트 안한다.
        if (!EpgCore.isChannelGrouped) {
            chSize = GridEpg.channelList.size();
        }
        temp = "("+chSize+")";
        g.drawString(temp, startX, startY);
        startX += fmFilter.stringWidth(temp)+space;
        //filterAnimationWidth += fmFilter.stringWidth(temp);
        // "|" 
        g.setColor(cAllFilter);
        g.fillRect(startX, startY-15, 2, 20);
        startX += 2+space;
        // c icon
        g.drawImage(highlightIcon, startX, startY-15, c);
        startX += highlightIcon.getWidth(c)+space-5;
        // highlight BG
        String curHighlightName = Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight());
        if (!Highlight.getInstance().isNone()) {
            g.setColor(HIGHLIGHT_BG_COLOR);
            g.fillRect(startX, startY-14, fmFilter.stringWidth(curHighlightName)+10, 19);
        }
        //highlightAnimationStartX = startX;
        //highlightAnimationWidth = fmFilter.stringWidth(curHighlightName)+10;
        // highlight FG
        g.setColor(HIGHLIGHT_FG_COLOR);
        if (Highlight.getInstance().isNone()) {
            curHighlightName = dataCenter.getString("epg.highlights");
        }
        g.drawString(curHighlightName, startX+5, startY);
        /*
        int sy;
        if (gridEpg.type == GridEpg.TYPE_DEFAULT) {
            g.drawImage(iAvOut, 622, 84, c);
            sy = 242;
        } else {
            sy = 98;
        }

        //->Kenneth[2015.4.3] R5
        // filterIcon
        g.drawImage(filterIcon, 50, sy-15, c);

        String filterName = ChannelController.get(0).getCurrentFilterKey();
        g.setFont(fFilter);
        //->Kenneth[2015.4.3] R5 : x 좌표 변경
        //->Kenneth[2015.4.11] R5 : "Filter : " 라고 보이는 놈은 흰색 뒤는 filter 따라 다른 색으로 수정한다
        String filterHead = dataCenter.getString("epg.filtered_by");
        String filterTitle = dataCenter.getString(filterName);
        g.setColor(cAllFilter);
        g.drawString(filterHead, 78, sy);
        g.setColor(ChannelController.ALL_FILTER_KEY.equals(filterName) ? cAllFilter : cFilter);
        g.drawString(filterTitle, 78+fmFilter.stringWidth(filterHead), sy);
        //->Kenneth[2015.7.16] R5 : VDTRMASTER-5506 sister 채널 갯수를 알 필요가 있음.
        int chSize = GridEpg.channelList.size() + GridEpg.channelList.getSisterSize();
        //->Kenneth[2015.7.20] R5 : HD 채널의 경우는 sister 채널 갯수를 더하지 않는다.
        if (filterName.equals(Rs.MENU_FILTER_HD.getKey())) {
            chSize = GridEpg.channelList.size();
        }

        //->Kenneth[2016.3.7] R7 : Ungrouped 이면 sister 는 카운트 안한다.
        if (!EpgCore.isChannelGrouped) {
            chSize = GridEpg.channelList.size();
        }
        //<-

        //int chSize = GridEpg.channelList.size();
        //<-
        String chSizeStr = " ("+chSize+")";
        int chSizeStrLength = fmFilter.stringWidth(chSizeStr);
        int chSizeX = 78+fmFilter.stringWidth(filterHead)+fmFilter.stringWidth(filterTitle);
        g.drawString(chSizeStr, chSizeX, sy);

        //->Kenneth[2015.4.3] R5 : Highlights
        int hl_x = 311;
        if ((chSizeX+chSizeStrLength) > hl_x) {
            hl_x = chSizeX+chSizeStrLength+10;
        }
        g.drawImage(highlightIcon, hl_x, sy-15, c);
        String highlightTitle = dataCenter.getString("epg.highlights")+" : ";
        String curHighlightName = Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight());
        g.setColor(cAllFilter);
        g.drawString(highlightTitle, hl_x+28, sy);
        // highlight BG
        int bgX = hl_x+28+fmFilter.stringWidth(highlightTitle)+4;
        if (!Highlight.getInstance().isNone()) {
            g.setColor(HIGHLIGHT_BG_COLOR);
            //->Kenneth[2015.6.2] 왜 좌표가 안맞지? "-7" 을 해줌
            g.fillRect(bgX, sy-14, fmFilter.stringWidth(curHighlightName)+10, 19);
        }
        // highlight FG
        g.setColor(HIGHLIGHT_FG_COLOR);
        g.drawString(curHighlightName, bgX+5, sy);
        */
        //<-

        //->Kenneth[2015.4.7] R5 : Hihglight 가 보여지기 때문에 얘는 그리지 않음.
        /*
        if (gridEpg.type == GridEpg.TYPE_EXTENDED) {
            Channel ch = gridEpg.getFocusedChannel();
            if (ch != null) {
                int screenData = ch.getScreenData();
                if (screenData > 0) {
                    String s = dataCenter.getString("epg.discover_ch");
                    g.setColor(cGray);
                    g.drawString(s, 323, sy);
                    Util.drawScreenIcons(g, screenData, fmFilter.stringWidth(s) + 323 + 2, sy - 13, c);
                }
            }
        }
        */

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString("MS_URL=" + SharedMemory.getInstance().get(SharedDataKeys.MS_URL), 80, 70);
            g.drawString(CaManager.eventCount + " " + ChannelDatabaseBuilder.getInstance().toString(), 80, 84);

            g.setFont(fDebug);
            // EPG data
            Calendar cal = Calendar.getInstance();
            long cur = System.currentTimeMillis();
            cal.setTimeInMillis(cur);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            long t0 = cal.getTimeInMillis();

            int tx = 70;
            int ty = 476;
            int dg = 56;

            EpgDataManager edm = EpgDataManager.getInstance();
            RangeList epgChecker = edm.getEpgChecker();
            RangeList ppvChecker = edm.getPpvChecker();

            Enumeration en;
            en = epgChecker.elements();
            g.setColor(Color.red);
            try {
                while (en.hasMoreElements()) {
                    Range r = (Range) en.nextElement();
                    int x1 = (int) ((r.from - t0) * 56 / Constants.MS_PER_DAY) + tx;
                    int x2 = (int) ((r.to - t0) * 56 / Constants.MS_PER_DAY) + tx;
                    g.fillRect(x1, ty, x2 - x1, 5);
                }
            } catch (Exception ex) {
            }
            en = ppvChecker.elements();
            g.setColor(Color.blue);
            try {
                while (en.hasMoreElements()) {
                    Range r = (Range) en.nextElement();
                    int x1 = (int) ((r.from - t0) * 56 / Constants.MS_PER_DAY) + tx;
                    int x2 = (int) ((r.to - t0) * 56 / Constants.MS_PER_DAY) + tx;
                    g.fillRect(x1, ty + 5, x2 - x1, 5);
                }
            } catch (Exception ex) {
            }


            g.setColor(Color.gray);
            g.drawRect(tx, ty, dg * 15, 10);

            int x = tx;
            for (int i = 0; i < 15; i++) {
                g.drawLine(x, ty, x, ty+10);
                g.drawString(String.valueOf(i), x + 2, ty+9);
                x += dg;
            }

            //->Kenneth[2015.4.18] 여기에서 Grid 의 현재 위치 indicator(회색) 를 그린다. 
            long page = gridEpg.getPageTime();
            if (page > 0) {
                x = (int) ((page - t0) * 56 / Constants.MS_PER_DAY) + tx;
                g.fillRect(x, ty+10, (int) (GridPrograms.PAGE_TIME_GAP * 56 / Constants.MS_PER_DAY), 3);
            }

            g.setColor(Color.green);
            x = (int) ((cur - t0) * 56 / Constants.MS_PER_DAY) + tx;
            g.drawLine(x, ty+10, x, ty+13);

            g.setColor(Color.magenta);
            x = (int) ((EpgDataManager.fileEndTime - t0) * 56 / Constants.MS_PER_DAY) + tx;
            g.drawLine(x, ty+10, x, ty+13);

            g.setColor(Color.yellow);
            x = (int) ((EpgDataManager.firstTime - t0) * 56 / Constants.MS_PER_DAY) + tx;
            g.drawLine(x, ty+10, x, ty+13);
            x = (int) ((EpgDataManager.lastTime - t0) * 56 / Constants.MS_PER_DAY) + tx;
            g.drawLine(x, ty+10, x, ty+13);


        }

    }

}

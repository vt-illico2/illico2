package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.util.Util;
import com.alticast.illico.epg.ui.ProgramDetails;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.TvChannel;

public class ProgramDetailsRenderer extends OptionPanelRenderer {
    protected Image iEffect = dataCenter.getImage("02_gride_ef04.png");

    protected Image iBlocked  = dataCenter.getImage("02_icon_con.png");

    protected Image iRec = dataCenter.getImage("02_icon_REC.png");
    protected Image iRemind = dataCenter.getImage("02_reminder_icon.png");
    protected Image iSkipped = dataCenter.getImage("02_icon_skipped.png");
    protected Image iHd = dataCenter.getImage("icon_hd.png");
    protected Image iCC = dataCenter.getImage("icon_cc.png");
//    protected Image iISA1 = dataCenter.getImage("02_icon_isa_1.png");
    protected Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };
    //->Kenneth[2015.2.3] 4K
    protected Image iUhd = dataCenter.getImage("icon_uhd.png");
    //->Kenneth[2015.2.18] 4K : grid 에는 보였는데 여기서는 안보인다 그래서 넣음
    private Image iAvOut = dataCenter.getImage("02_av_out.png");
    //->Kenneth[2015.3.23] : 위에 iAvOut 을 PIP 가 보일때에만 그리기 위한 flag
    // PIP 가 안 그려지는 상황 (전체Grid 에서 INfo 로 이동시) 에서도 iAvOut 이 그려지는 상황 막음.
    boolean isPipRunning;

    protected Font fTitle = FontResource.BLENDER.getFont(27);
    protected Font fTime = FontResource.BLENDER.getFont(19);
    protected Font fInfo = FontResource.DINMED.getFont(15);
    protected Font fChannelName = FontResource.BLENDER.getFont(22);
    protected Font fChannelNumber = FontResource.BLENDER.getFont(23);
    protected Font fChannelFullName = FontResource.BLENDER.getFont(18);
    protected Font fStation = FontResource.BLENDER.getFont(16);
    protected Font fStationValue = FontResource.DINMED.getFont(15);
    protected FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);
    protected FontMetrics fmTime = FontResource.getFontMetrics(fTime);
    protected FontMetrics fmChannelFullName = FontResource.getFontMetrics(fChannelFullName);

    protected Color cTitle = new Color(252, 202, 4);

    protected Color cChannelNumber = Color.white;
    protected Color cTime = new Color(212, 212, 212);

    protected Color cProgressBg = new Color(35, 35, 35);
    protected Color cProgressFg = new Color(249, 195, 0);

    protected Color cInfo = new Color(230, 230, 230);
    protected Color cButton = new Color(230, 230, 230);
    protected Color cChannelName = new Color(210, 210, 210);
    protected Color cChannelFullName = new Color(210, 210, 210);
    protected Color cStation = new Color(182, 182, 182);
    protected Color cStationValue = new Color(243, 243, 243);

    //->Kenneth[2015.4.14] R5 : 위의 color, Font 등은 PpvProgramDetailsRenderer 에서 사용할지도 모르므로
    // 남겨두되 여기서 사용하는 것은 다시 정의해서 쓰도록 하자.
    private Color c172_172_172 = new Color(172, 172, 172);
    private Color c252_202_4 = new Color(252, 202, 4);
    private Color c182_182_182 = new Color(182, 182, 182);
    private Color c195_195_195 = new Color(195, 195, 195);
    private Color c120_120_120 = new Color(120, 120, 120);
    private Color c249_195_0 = new Color(249, 195, 0);
    private Font fBlender16 = FontResource.BLENDER.getFont(16);
    private Font fBlender18 = FontResource.BLENDER.getFont(18);
    private Font fBlender22 = FontResource.BLENDER.getFont(22);
    private Font fBlender23 = FontResource.BLENDER.getFont(23);
    private Font fBlender27 = FontResource.BLENDER.getFont(27);
    protected FontMetrics fmBlender18 = FontResource.getFontMetrics(fBlender18);
    protected FontMetrics fmBlender23 = FontResource.getFontMetrics(fBlender23);
    protected FontMetrics fmBlender27 = FontResource.getFontMetrics(fBlender27);

    private int screenData = -1;

    public void prepare(UIComponent c) {
        screenData = -1;

        //->Kenneth[2015.3.23] : 4K : ProgramDetailsRenderer 에서 갖다쓰기 위해 public 으로 바꿈.
        isPipRunning = ChannelBackground.getInstance().isShowing;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetailsRenderer.prepare () : isPipRunning = "+isPipRunning);

        super.prepare(c);
    }

    public void paint(Graphics g, UIComponent c) {
        //->Kenneth[2015.2.18] 4K : grid 에는 보였는데 여기서는 안보인다 그래서 넣음
        // 특이사항 : drawImage 코드를 super.paint 밑에 넣으면 이상하게 그려짐. 어디선가 translate 된 graphics 가
        // 되어버리는듯. 위에 그리면 반대로 안 보임
        super.paint(g, c);
        //g.setColor(Color.blue);
        //g.fillRect(622, 0, 298, 167);
        //g.drawImage(iAvOut, 622, 84, c);
        // y 좌표를 0 으로 해야지 위치가 맞음 주의.

        //->Kenneth[2015.3.23] : 위에 iAvOut 을 PIP 가 보일때에만 그리기
        if (isPipRunning) {
            g.drawImage(iAvOut, 622, 0, c);
        }
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        //->Kenneth[2015.4.14] R5 : 워낙 바뀔 게 많아서 아예 새로 그리기로 함.
        //->Kenneth[2015.6.11] R5 : channel icon 이 그려지고 안 그려지고 따라서 x 좌표 바뀜
        int baseY = -84;
        Formatter df = Formatter.getCurrent();
        ProgramDetails programDetails = (ProgramDetails) c;
        Channel channel = programDetails.channel;
        Program program = programDetails.program;
        boolean isFakeProgram = program instanceof FakeProgram;
        TvChannel sister = channel.getSisterChannel();

        //->Kenneth[2016.3.7] R7 : Ungrouped 이면 sister 체크 안함
        if (!EpgCore.isChannelGrouped) {
            sister = null;
        }
        //<-

        int definition = Util.getDefinition(channel, program);  
        boolean isCaptioned = program.isCaptioned();

        try {
            // 1. blocked icon / channel icon 
            boolean channelIconDisplayed = false;
            if (programDetails.blockLevel != ParentalControl.OK) {
                g.drawImage(iBlocked, 54, baseY+112, c);
                channelIconDisplayed = true;
            } else {
                channelIconDisplayed = Util.drawChannelIcon(g, channel, false, true, 52, baseY+112, c);
            }

            // 2. 채널 번호
            int startX = 83;
            //->Kenneth[2015.6.11] 만약 channel icon 이 안보이는 경우 앞으로 땡김
            if (!channelIconDisplayed) {
                startX = 54;
            }
            String chNumStr = channel.getNumber()+"";
            if (sister != null) {
                chNumStr = chNumStr + " | " + sister.getNumber();
            }
            g.setColor(c172_172_172);
            g.setFont(fBlender23);
            g.drawString(chNumStr, startX, baseY+130);
            int xPadding = 15;
            int tmpX = startX+fmBlender23.stringWidth(chNumStr)+xPadding;

            // 3. HD/UHD
            //->Kenneth[2015.7.8] VDTRMASTER-5485 : R5 부터 definition icon 안 그림
            /*
            definition = channel.getDefinition();  
            if (sister != null) {
                int sisterDef = sister.getDefinition();  
                if (sisterDef > definition) {
                    definition = sisterDef;
                }
            }
            if (definition == Constants.DEFINITION_HD) {
                g.drawImage(iHd, tmpX, baseY+117, c);
                tmpX += iHd.getWidth(c)+xPadding;
            } else if (definition == Constants.DEFINITION_4K) {
                g.drawImage(iUhd, tmpX, baseY+117, c);
                tmpX += iUhd.getWidth(c)+xPadding;
            }
            */

            // 4. 채널 로고
            if (programDetails.channelLogo != null) {
                g.drawImage(programDetails.channelLogo, tmpX, baseY+109, c);
                tmpX += programDetails.channelLogo.getWidth(c)+xPadding;
            }

            // 5. 채널 full name
            String chFullName = channel.getFullName();
            if (chFullName != null) {
                g.setColor(c172_172_172);
                g.setFont(fBlender23);
                g.drawString(chFullName, tmpX, baseY+130);
            }

            // 6. 프로그램명
            g.setColor(c252_202_4);
            g.setFont(fBlender27);
            String programTitle = programDetails.title;
            programTitle = TextUtil.shorten(programTitle, g.getFontMetrics(), 430);
            g.drawString(programTitle, 54, baseY+195);

            // 7. program data (900, mobile 등의 아이콘)
            if (channel != null) {
                if (screenData < 0) {
                    screenData = channel.getScreenData();
                }
                if (screenData > 0) {
                    if (channelIconDisplayed) {
                        //->Kenneth[2015.7.8] R5 : VDTRMASTER-5484 : 900 icon 을 안 그리는 경우도 있음.
                        Util.drawScreenIcons(g, screenData, 81, baseY+142, c, false);
                        //Util.drawScreenIcons(g, screenData, 81, baseY+142, c);
                    } else {
                        Util.drawScreenIcons(g, screenData, 81-29, baseY+142, c, false);
                        //Util.drawScreenIcons(g, screenData, 81-29, baseY+142, c);
                    }
                }
            }

            // 8. Rating image / CC image
            tmpX = 54+fmBlender27.stringWidth(programTitle)+15;
            Image im = ratingImages[program.getRatingIndex()];
            if (im != null) {
                g.drawImage(im, tmpX, baseY+181, c);
                tmpX += 26;
            }
            if (isCaptioned) {
                g.drawImage(iCC, tmpX, baseY+181, c);
            }

            // fake program 이면 "station"이라고 그리고 리턴.
            if (isFakeProgram) {
                g.setColor(c182_182_182);
                g.setFont(fBlender16);
                g.drawString(dataCenter.getString("epg.station"), 54, baseY+223);
                return;
            }

            // 9. program 프로그레스/시간
            tmpX = 54;
            g.setColor(c195_195_195);
            g.setFont(fBlender18);
            if (program.isOnAir()) {
                String startTime = df.getLongDayText(program.startTime) + "  |  " + df.getTime(program.startTime);
                g.drawString(startTime, tmpX, baseY+221);
                tmpX += fmBlender18.stringWidth(startTime) + 5;

                String endTime = df.getTime(program.endTime);
                g.drawString(endTime, tmpX + 125, baseY+221);

                g.setColor(c120_120_120);
                g.fillRect(tmpX, baseY+213, 120, 6);
                g.setColor(c249_195_0);
                g.fillRect(tmpX, baseY+213, getProgressWidth(program, 120), 6);

                tmpX = tmpX + 125 + 5 + fmBlender18.stringWidth(endTime);
            } else {
                String time = df.getLongDayText(program.startTime) + "  |  " + df.getTime(program.startTime) + "  -  " + df.getTime(program.endTime);
                g.drawString(time, tmpX, baseY+221);
                tmpX = tmpX + 5 + fmBlender18.stringWidth(time);
            }

            // 10. remider icon
            if (ReminderManager.getInstance().contains(program)) {
                g.drawImage(iRemind, tmpX, baseY+206, c);
                tmpX += 22 + 3;
            }

            // 11. recording icon
            switch (programDetails.recordingStatus) {
                case RecordingStatus.RECORDING:
                    g.drawImage(iRec, tmpX, baseY+209, c);
                    break;
                case RecordingStatus.SKIPPED:
                    g.drawImage(iSkipped, tmpX, baseY+209, c);
                    break;
            }

            // 12. Extra information
            if (com.videotron.tvi.illico.log.Log.EXTRA_ON) {
                g.setColor(Color.green);
                g.drawString("auth = " + channel.isAuthorized(), 54, baseY+245);
                g.drawString("has package = " + com.alticast.illico.epg.config.PreviewChannels.hasPackage(channel), 150, baseY+245);
            }
        } catch (Exception e) {
            Log.print(e);
        }

        //->Kenneth[2015.4.14] R5 : 워낙 바뀔 게 많아서 아예 새로 그리기로 함.
        // 따라서 예전 코드 comment out 시킴
        /* 
        g.drawImage(iEffect, 48, 142, c);

        Formatter df = Formatter.getCurrent();
        ProgramDetails d = (ProgramDetails) c;
        Channel ch = d.channel;
        Program p = d.program;
        boolean isFakeProgram = p instanceof FakeProgram;

		//->Kenneth : 4K by June
        int definition = Util.getDefinition(ch, p);   // TODO - 4K
        boolean isCap = p.isCaptioned();
        int iconWidth = 26;
		//->Kenneth : 4K by June
        if (definition == Constants.DEFINITION_HD) {
            iconWidth += 26;
        } else if (definition >= Constants.DEFINITION_4K) {
            iconWidth += 28;
        }
        if (isCap) {
            iconWidth += 26;
        }

        int ix = 54;
        if (d.blockLevel != ParentalControl.OK) {
            g.drawImage(iBlocked, ix, 113-84, c);
            ix += 27;
        }

        String s = d.title;
        g.setFont(fTitle);
        g.setColor(cTitle);
        s = TextUtil.shorten(s, g.getFontMetrics(),
                GridEpgDescriptionRenderer.X_OFFSET + GridEpgDescriptionRenderer.WIDTH-6-ix-iconWidth);
        g.drawString(s, ix, 48);
        ix += fmTitle.stringWidth(s) + 5;

		//->Kenneth : 4K by June
        // [2015.2.3]
        if (definition == Constants.DEFINITION_HD) {
            g.drawImage(iHd, ix, 34, c);
            ix += 26;
        } else if (definition == Constants.DEFINITION_4K) {
            g.drawImage(iUhd, ix, 34, c);
            ix += 28;
        }
        //if (definition > 0) {
        //    g.drawImage(iHd, ix, 34, c);
        //    ix += 26;
        //}
        Image im = ratingImages[p.getRatingIndex()];
        if (im != null) {
            g.drawImage(im, ix, 34, c);
            ix += 26;
        }
        if (isCap) {
            g.drawImage(iCC, ix, 34, c);
        }

        int y;
        if (!isFakeProgram) {
            g.setFont(fChannelFullName);
            g.setColor(cChannelFullName);
            g.drawString(d.fullName, 54, 67);

            g.setFont(fTime);
            g.setColor(cTime);

            if (ch != null) {
                if (screenData < 0) {
                    screenData = ch.getScreenData();
                }
                if (screenData > 0) {
                    Util.drawScreenIcons(g, screenData, fmChannelFullName.stringWidth(d.fullName) + 54 + 2, 54, c);
                }
            }

            ix = 56;
            if (p.isOnAir()) {
                s = df.getLongDayText(p.startTime) + "  |  " + df.getTime(p.startTime);
                g.drawString(s, ix, 94);
                ix += fmTime.stringWidth(s) + 5;

                s = df.getTime(p.endTime);
                g.drawString(s, ix + 125, 94);

                g.setColor(cProgressBg);
                g.fillRect(ix, 86, 120, 6);
                g.setColor(cProgressFg);
                g.fillRect(ix, 86, getProgressWidth(p, 120), 6);

                ix = ix + 125 + 5 + fmTime.stringWidth(s);
            } else {
                s = df.getLongDayText(p.startTime) + "  |  " + df.getTime(p.startTime) + "  -  " + df.getTime(p.endTime);
                g.drawString(s, ix, 94);
                ix = ix + 5 + fmTime.stringWidth(s);
            }
            if (ReminderManager.getInstance().contains(p)) {
                g.drawImage(iRemind, ix, 79, c);
                ix += 22 + 3;
            }
            // recording icon
            switch (d.recordingStatus) {
                case RecordingStatus.RECORDING:
                    g.drawImage(iRec, ix, 82, c);
                    break;
                case RecordingStatus.SKIPPED:
                    g.drawImage(iSkipped, ix, 82, c);
                    break;
            }
            if (com.videotron.tvi.illico.log.Log.EXTRA_ON) {
                g.setColor(Color.green);
                g.drawString("auth = " + ch.isAuthorized(), ix + 5, 94);
                g.drawString("has package = " + com.alticast.illico.epg.config.PreviewChannels.hasPackage(ch), ix + 5, 94+18);
            }
            y = 111;
        } else {
            if (d.fullName != null && d.fullName.length() > 0) {
                g.setColor(cStation);
                g.setFont(fStation);
                g.drawString(dataCenter.getString("epg.station"), 54, 193-84);
                g.setColor(cStationValue);
                g.setFont(fStationValue);
                g.drawString(d.fullName, 127, 193-84);
            }
            y = 111 - 49;
        }

        Util.drawChannelIcon(g, ch, false, true, 52, y, c);
        int sx = 83;
//        boolean isISA = ch.isPurchasable();
//        if (isISA) {
//            g.drawImage(iISA1, 81, y, c);
//            sx += 28;
//        }

        g.setColor(cChannelNumber);
        g.setFont(fChannelNumber);
        g.drawString(d.channelNumber, sx, y + 19);

        g.drawImage(d.channelLogo, sx+136-83, y - 2, c);

        g.setFont(fChannelName);
        g.setColor(cChannelName);
        g.drawString(d.channelName, sx+250-83, y + 19);

//        if (d.blockLevel == ParentalControl.ADULT_CONTENT) {
//            g.drawImage(dataCenter.getImageByLanguage("block_txt2.png"), 55, 269-84, c);
//        }
        */
    }

    protected int getProgressWidth(Program p, int max) {
        int bar = (int) (max * (System.currentTimeMillis() - p.getStartTime()) / p.getDuration());
        return Math.max(0, Math.min(bar, max));
    }
}

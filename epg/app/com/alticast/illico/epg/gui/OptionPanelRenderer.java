package com.alticast.illico.epg.gui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.Rs;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.menu.SubscribeMenuItem;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class OptionPanelRenderer extends Renderer {
    public static final int BUTTON_Y_GAP = 39;

    public Rectangle bounds = new Rectangle(0, 84, 960, 540 - 84);
    protected DataCenter dataCenter = DataCenter.getInstance();

    private Image iBackground = dataCenter.getImage("bg_detail.jpg");
    private Image iShadow = dataCenter.getImage("02_grid_btsh.png");

    private Image iAc1 = dataCenter.getImage("01_acbg_1st.png");
    private Image iAc2 = dataCenter.getImage("01_acbg_2nd.png");
    private Image iAcShadow = dataCenter.getImage("01_ac_shadow.png");
    private Image iAcGlow = dataCenter.getImage("01_acglow.png");
    private Image iAcMiddle = dataCenter.getImage("01_acbg_m.png");
    private Image iAcLine = dataCenter.getImage("01_acline.png");

    private Image iBullet = dataCenter.getImage("02_detail_ar.png");
    private Image iFocus = dataCenter.getImage("02_detail_bt_foc.png");

    private Image iSubNormal = dataCenter.getImage("02_icon_isa_ac.png");
    private Image iSubFocus = dataCenter.getImage("02_icon_isa_bl.png");
    
    //R5
    private Image iAc1_1 = dataCenter.getImage("01_acbg_1st_1.png");
    private Image iAc2_1 = dataCenter.getImage("01_acbg_2nd_1.png");
    private Image iAcMiddle_1 = dataCenter.getImage("01_acbg_m_1.png");
    private Image iAcLine_1 = dataCenter.getImage("01_acline_1.png");
    private Image iFocusDim = dataCenter.getImage("02_detail_bt_foc-dim.png");
    private Image iSimillar = dataCenter.getImage("icon_similar_f.png");
    private Image iSimillarDim = dataCenter.getImage("icon_similar_n.png");

    public static Font fButton = FontResource.BLENDER.getFont(18);
    public static Font fFocusedButton = FontResource.BLENDER.getFont(21);
    private static FontMetrics fmButton = FontResource.getFontMetrics(fButton);
    private static FontMetrics fmFocusedButton = FontResource.getFontMetrics(fFocusedButton);

    public static Color cButton = new Color(230, 230, 230);
    public static Color cFocusedButton = new Color(3, 3, 3);

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public Rectangle getButtonFocusBounds(OptionPanel c) {
        return getButtonFocusBounds(c.menu, c.getFocus());
    }

    public Rectangle getButtonFocusBounds(MenuItem menu, int focus) {
        if (menu == null) {
            return null;
        }
        int size = menu.size();
        if (size <= 0) {
            return null;
        }
        if (focus < 0) {
            return null;
        }
        int tx = 581 - 579 + 626 + 2;
        int ty = (3 - size + focus) * BUTTON_Y_GAP + 445 - 84 + 184 - 467 + 173;
        return new Rectangle(tx, ty, 284, 41);
    }

    public void paintButtons(Graphics g, UIComponent c, MenuItem menu) {
    	// R5
    	// VDTRMASTER-5534
    	OptionPanel opPanel = null;
    	if (c instanceof OptionPanel) {
    		opPanel = (OptionPanel) c;
    	}
    	
        int size = menu.size();
        if (size <= 0) {
            return;
        }
        int focus = c.getFocus();
        int h = size * BUTTON_Y_GAP;

        g.translate(0, (5 - size) * BUTTON_Y_GAP);
        
        //R5
        // VDTRMASTER-5534
        if (opPanel != null && opPanel.isDimmedBg()) {
        	g.drawImage(iAc1_1, 628, 173, c);
	        if (size > 1) {
	            g.drawImage(iAc2_1, 628, 216, c);
	            g.drawImage(iAcMiddle_1, 628, 252, 284, h - 77, c);
	        }
	        
	        int y = 213;
	        for (int i = 0; i < size - 1; i++) {
	            g.drawImage(iAcLine_1, 643, y, c);
	            y += BUTTON_Y_GAP;
	        }
        } else {
	        g.drawImage(iAc1, 628, 173, c);
	        if (size > 1) {
	            g.drawImage(iAc2, 628, 216, c);
	            g.drawImage(iAcMiddle, 628, 252, 284, h - 77, c);
	        }

	        g.drawImage(iAcGlow, 627, 154 + h, c);
	        g.drawImage(iAcShadow, 581, 166 + h, c);
	        
	        int y = 213;
	        for (int i = 0; i < size - 1; i++) {
	            g.drawImage(iAcLine, 643, y, c);
	            y += BUTTON_Y_GAP;
	        }
        }

        if (focus >= 0) {
        	// VDTRMASTER-5534
        	if (opPanel != null && opPanel.getDimmedFocus()) {
        		g.drawImage(iFocusDim, 626, 173 + focus * BUTTON_Y_GAP, c);
        	} else {
        		g.drawImage(iFocus, 626, 173 + focus * BUTTON_Y_GAP, c);
        	}
        }

        String s;
        int y = 199;
        for (int i = 0; i < size; i++) {
            MenuItem item = menu.getItemAt(i);
            s = dataCenter.getString(item.getKey());
            if (i == focus) {
                if (item instanceof SubscribeMenuItem) {
                    g.drawImage(iSubFocus, 639, y - 15, c);
                }
                
                // R5
                if (item.equals(Rs.MENU_COD) || item.equals(Rs.MENU_ADULT_COD)) {
                	g.drawImage(iSimillar, 636, y - 18, c);
                }
                
                g.setColor(cFocusedButton);
                g.setFont(fFocusedButton);
                s = TextUtil.shorten(s, fmFocusedButton, 238);
                g.drawString(s, 671, y + 1);
            } else {
                if (item instanceof SubscribeMenuItem) {
                    g.drawImage(iSubNormal, 640, y - 15, c);
                } else {
                	// R5
                	if (item.equals(Rs.MENU_COD) || item.equals(Rs.MENU_ADULT_COD)) {
                		g.drawImage(iSimillarDim, 636, y - 18, c);
                	} else {
                		g.drawImage(iBullet, 651, y - 8, c);
                	}
                }
                g.setColor(cButton);
                g.setFont(fButton);
                s = TextUtil.shorten(s, fmButton, 235);
                g.drawString(s, 673, y);
            }
            y += BUTTON_Y_GAP;
        }
        g.translate(0, -(5 - size) * BUTTON_Y_GAP);
    }

    public void paint(Graphics g, UIComponent c) {
        g.drawImage(iBackground, 0, 0, c);
        g.drawImage(iShadow, 0, 342, c);

        OptionPanel d = (OptionPanel) c;
        paintButtons(g, d, d.menu);
    }

}

package com.alticast.illico.epg.gui;

import java.awt.*;
import java.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.alticast.illico.epg.ui.Popup;


public class HdmiFullNotiRenderer extends PopupRenderer {
    public static final int MAX_TEXT_WIDTH = 800;

    int Y_GAP = 24;
    int topX = 268;

    Image i_07_pop_sha = dataCenter.getImage("07_pop_sha.png");
    Image i_large_noti_b = dataCenter.getImage("large_noti_b.png");
    Image i_large_noti_m = dataCenter.getImage("large_noti_m.png");
    Image i_large_noti_t = dataCenter.getImage("large_noti_t.png");
    Image i_01_hotkeybg = dataCenter.getImage("01_hotkeybg_2.png");

    Image i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
    Image i_btn_293_l = dataCenter.getImage("btn_293_l.png");

    Color cText = Color.white;
    Font fText = FontResource.BLENDER.getFont(21);
    FontMetrics fmText = FontResource.getFontMetrics(fText);

    Color cButton = Color.black;
    Font fButton = FontResource.BLENDER.getFont(18);
    FontMetrics fmButton = FontResource.getFontMetrics(fButton);

    Color cTitle = new Color(255, 203, 0);
    Font fTitle = FontResource.BLENDER.getFont(24);

    private Color[] cHighlights = new Color[] { new Color(255, 203, 0) };

    public HdmiFullNotiRenderer() {
        this(268);
    }

    public HdmiFullNotiRenderer(int topX) {
        this.topX = topX;
        TEXT_Y = topX + 125;
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    public Color getBackground() {
        return null;
    }

    public Rectangle getButtonFocusBounds(Popup c) {
        if (c.getFocus() == 0) {
            return new Rectangle(181+1, 435, 293, 32);
        } else {
            return new Rectangle(485+1, 435, 293, 32);
        }
    }

    public void paint(Graphics g, UIComponent c) {
        Popup p = (Popup) c;

        g.drawImage(i_07_pop_sha, 0, 79 + topX, 960, 171, c);
        g.drawImage(i_large_noti_t, 0, 0 + topX, c);
        g.drawImage(i_large_noti_m, 0, 70 + topX, 960, 290-topX+50, c);
        g.drawImage(i_large_noti_b, 0, 410, c);
        g.drawImage(i_01_hotkeybg, 236, 466, c);

        g.setColor(cTitle);
        g.setFont(fTitle);
        g.drawString(p.title, 79, 85 + topX);

        paintBody(g, p);

        String[] buttons = p.buttons;
        if (buttons.length == 2) {
            int focus = p.getFocus();
            g.setColor(cButton);
            g.setFont(fButton);
            g.drawImage(focus == 0 ? i_btn_293_foc : i_btn_293_l, 181, 435, c);
            g.drawImage(focus == 1 ? i_btn_293_foc : i_btn_293_l, 485, 435, c);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[0]), 330, 456);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[1]), 631, 456);
        }

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString("count = " + com.alticast.illico.epg.navigator.VolumeController.hdmiCheckCount, 700, 85 + topX);
        }
    }

    protected void paintBody(Graphics g, Popup p) {
        g.setColor(cText);
        g.setFont(fText);
        int fs = fText.getSize();

        int y = (510 + topX) / 2 - ((texts.length - 1) * Y_GAP - fs) / 2 - 3;
        for (int i = 0; i < texts.length; i++) {
            if (i < texts.length - 1) {
                GraphicUtil.drawStringCenter(g, texts[i], 480, y);
            } else {
                GraphicUtil.drawHighlightedTextCenter(g, texts[i], 480, y,
                        cText, cHighlights, new String[] { dataCenter.getString("epg.hdmi_url") });
            }
            y = y + Y_GAP;
        }
    }

    public int getMessageWidth() {
        return MAX_TEXT_WIDTH;
    }

}

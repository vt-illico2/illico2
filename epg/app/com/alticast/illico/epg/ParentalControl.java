package com.alticast.illico.epg;

import java.rmi.*;
import java.util.*;
import javax.tv.util.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;

/**
 * ParentalControl.
 */
public class ParentalControl implements DataUpdateListener {

    private static ParentalControl instance = new ParentalControl();

    public static ParentalControl getInstance() {
        return instance;
    }

    public static boolean enabled;
    public static int ratingIndex = Constants.RATING_INDEX_18 + 10;
    public static boolean hideAdult;

    private static long unblockDuration = 0;

    public static long hourUnit;

    public static final int OK             = 3;
    public static final int RATING_OVER    = 2;
    public static final int ADULT_CONTENT  = 1;

    public static final byte CLEAR            = ChannelController.CLEAR;
    public static final byte BLOCK_BY_RATING  = ChannelController.BLOCK_BY_RATING;
    public static final byte BLOCK_BY_CHANNEL = ChannelController.BLOCK_BY_CHANNEL;

    private static EpgDataManager edm = EpgDataManager.getInstance();

    private static UnblockTable unblockedChannels = new UnblockTable();
    private static UnblockTable unblockedPrograms = new UnblockTable();

    private ParentalControl() {
        DataCenter dc = DataCenter.getInstance();
        dc.addDataUpdateListener(RightFilter.PARENTAL_CONTROL, this);
        dc.addDataUpdateListener(RightFilter.BLOCK_BY_RATINGS, this);
        dc.addDataUpdateListener(RightFilter.HIDE_ADULT_CONTENT, this);
        dc.addDataUpdateListener(RightFilter.PIN_CODE_UNBLOCKS_CHANNEL, this);

        if (dc.getBoolean("CHANNEL_BLOCK_RELEASE_TIME_UNIT_MINUTE")) {
            hourUnit = Constants.MS_PER_MINUTE;
        } else {
            hourUnit = Constants.MS_PER_HOUR;
        }
    }

    public static byte checkBlock(Channel ch, Program p) {
        if (!enabled) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.checkBlock() returns CLEAR");
            return CLEAR;
        }
        if (ch.isBlocked() && !isUnblocked(ch)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.checkBlock() returns BLOCK_BY_CHANNEL");
            return BLOCK_BY_CHANNEL;
        }
        if (p == null) {
            p = edm.getCurrentProgram(ch);
        }
        if (p == null || p.getRatingIndex() < ratingIndex || isUnblocked(p)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.checkBlock() returns CLEAR -- 2");
            return CLEAR;
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.checkBlock() returns BLOCK_BY_RATING");
            return BLOCK_BY_RATING;
        }
    }

    public static byte checkBlock(Channel ch) {
        return checkBlock(ch, null);
    }

//    private void blockNow() {
//        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
//            ChannelController.get(i).blockIfNeeded();
//        }
//    }

    public void dataUpdated(String key, Object old, Object value) {
        if (RightFilter.PARENTAL_CONTROL.equals(key)) {
            enabled = Definitions.OPTION_VALUE_ON.equals(value);
//            if (enabled) {
//                blockNow();
//            }
            Log.printInfo("ParentalControl.enabled = " + enabled);

        } else if (RightFilter.BLOCK_BY_RATINGS.equals(key)) {
            int r = Program.getRatingIndex((String) value);
            if (r == Constants.RATING_INDEX_G) {
                r = Constants.RATING_INDEX_18 + 10;
            }
            ratingIndex = r;
            Log.printInfo("ParentalControl.ratingIndex = " + ratingIndex);

        } else if (RightFilter.HIDE_ADULT_CONTENT.equals(key)) {
            hideAdult = Definitions.OPTION_VALUE_YES.equals(value);
            Log.printInfo("ParentalControl.hideAdult = " + hideAdult);

        } else if (RightFilter.PIN_CODE_UNBLOCKS_CHANNEL.equals(key)) {
            long dur;
            if (Definitions.FOR_4_HOURS.equals(value)) {
                dur = 4 * hourUnit;
            } else if (Definitions.FOR_8_HOURS.equals(value)) {
                dur = 8 * hourUnit;
            } else {
                dur = 0;
            }
            Log.printInfo("ParentalControl.unblockDuration = " + unblockDuration + " -> " + dur);
            if (dur != unblockDuration) {
                if (dur == 0) {
                    // until channel is changed
                    unblockedChannels.clear();
                } else if (unblockDuration == 0) {
                    // for 4,8 hours - does nothing
                }
                unblockDuration = dur;
            }
        }
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController.get(i).recheckBlock();
        }
        EpgCore.getInstance().gridEpg.updateParentalControl();
        EpgCore.getInstance().miniEpg.updateParentalControl();
    }

    public void dataRemoved(String key) {
    }

    public static void reset() {
        unblockedChannels.clear();
        unblockedPrograms.clear();
    }

    public synchronized long channelUnblocked(Channel ch) {
        Log.printInfo(App.LOG_HEADER+"ParentalControl.channelUnblocked = " + unblockDuration + ", " + ch);
        long dur = unblockDuration;
        String name = ch.getName();
        unblockedChannels.remove(name);
        if (dur == 0) {
            unblockedChannels.put(name, new UnblockInThisChannel(name));
        } else {
            unblockedChannels.put(name, new UnblockForTime(name, dur));
        }
        return dur;
    }

    public synchronized void programUnblocked(Channel ch) {
        Log.printInfo(App.LOG_HEADER+"ParentalControl.programUnblocked = " + ch);
        programUnblocked(edm.getCurrentProgram(ch));
    }

    public synchronized void programUnblocked(AbstractProgram p) {
        Log.printInfo(App.LOG_HEADER+"ParentalControl.programUnblocked = " + p);
        if (p != null) {
            String id = p.getId();
            unblockedPrograms.remove(id);
            unblockedPrograms.put(id, new UnblockProgram(id, p));
        }
    }

    public static boolean isUnblocked(Channel ch) {
        return !enabled || unblockedChannels.containsKey(ch.getName());
    }

    public static boolean isUnblocked(AbstractProgram p) {
        return unblockedPrograms.containsKey(p.getId());
    }

    public static int getLevel(AbstractProgram p) {
        if (p == null || isUnblocked(p)) {
            return OK;
        }
        return getLevel(p.getRatingIndex());
    }

    public static int getLevel(int contentRating) {
        if (!enabled) {
            return OK;
        }
        if (hideAdult && contentRating >= Constants.RATING_INDEX_18) {
            return ADULT_CONTENT;
        }
        if (contentRating >= ratingIndex) {
            return RATING_OVER;
        } else {
            return OK;
        }
    }

    public static int getSettingLevel() {
        if (!enabled) {
            return Integer.MAX_VALUE;
        }
        return ratingIndex;
    }

    public static String getTitle(Program p) {
        if (p == null) {
            return "";
        } else if (!enabled || !hideAdult || p.getRatingIndex() < Constants.RATING_INDEX_18 || isUnblocked(p)) {
            return p.getTitle();
        } else {
            return DataCenter.getInstance().getString("epg.blocked_title");
        }
    }

    public String toString() {
        DataCenter dataCenter = DataCenter.getInstance();
        return dataCenter.getString(RightFilter.PARENTAL_CONTROL) + ", "
            + dataCenter.getString(RightFilter.BLOCK_BY_RATINGS) + "(" + ratingIndex + "), "
            + dataCenter.getString(RightFilter.HIDE_ADULT_CONTENT) + ", "
            + unblockedChannels + ", " + unblockedPrograms;
    }

    static class UnblockTable extends Hashtable {
        public Object remove(Object key) {
            UnblockOption o = (UnblockOption) super.remove(key);
            if (Log.DEBUG_ON) {
                Log.printDebug("UnblockTable.remove = " + o);
            }
            if (o != null) {
                o.stop();
            }
            return o;
        }

        public void clear() {
            // flush all images
            Enumeration keys = keys();
            while (keys.hasMoreElements()) {
                Object key = keys.nextElement();
                remove(key);
            }
            // actually this is not meaningful.
            super.clear();
        }
    }


    abstract static class UnblockOption {
        String name;
        public UnblockOption(String key) {
            name = key;
        }
        protected abstract void stop();
    }


    static class UnblockInThisChannel extends UnblockOption implements EpgChannelEventListener {

        public UnblockInThisChannel(String chName) {
            super(chName);
            ChannelController.get(0).addChannelEventListener(this);
        }

        public void selectionRequested(Channel ch, short type, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockInThisChannel.selectionRequested()");
            channelChanged(ch);
        }

        public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockInThisChannel.selectionBlocked()");
            channelChanged(ch);
        }

        public void serviceStopped(Channel lastChannel) {
        }

        public void showChannelInfo(Channel ch) {
        }

        public void programUpdated(Channel ch, Program p) {
        }

        private void channelChanged(Channel ch) {
            if (!name.equals(ch.getName())) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockInThisChannel.channelChanged("+ch+")");
                unblockedChannels.remove(name);
            }
        }

        protected void stop() {
            ChannelController.get(0).removeChannelEventListener(this);
        }

        public String toString() {
            return "InChannel[" + name + "]";
        }
    }

    static class UnblockForTime extends UnblockOption implements TVTimerWentOffListener {
        TVTimerSpec suspendTimer = new TVTimerSpec();
        long end;
        public UnblockForTime(String chName, long dur) {
            super(chName);
            suspendTimer.addTVTimerWentOffListener(this);
            this.end = dur + System.currentTimeMillis();

            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(suspendTimer);
            suspendTimer.setDelayTime(dur);
            try {
                timer.scheduleTimerSpec(suspendTimer);
            } catch (TVTimerScheduleFailedException ex) {
            }
        }

        protected void stop() {
            TVTimer.getTimer().deschedule(suspendTimer);
        }

        public void timerWentOff(TVTimerWentOffEvent e) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockForTime.timerWentOff() : "+name+" should be re-blocked.");
            unblockedChannels.remove(name);
            // block again
            for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
                ChannelController cc = ChannelController.get(i);
                synchronized (cc) {
                    Channel ch = cc.getCurrent();
                    if (ch != null && name.equals(ch.getName())) {
                        cc.blockIfNeeded();
                    }
                }
            }
        }

        public String toString() {
            long rem = (end - System.currentTimeMillis()) / 1000L;
            return "Time[" + (rem / 60) + ":" + (rem % 60) + "]";
        }
    }

    static class UnblockProgram extends UnblockOption implements EpgChannelEventListener, TVTimerWentOffListener {
        TVTimerSpec suspendTimer = new TVTimerSpec();
        AbstractProgram program;
        public UnblockProgram(String key, AbstractProgram p) {
            super(key);
            program = p;
            ChannelController.get(0).addChannelEventListener(this);
            suspendTimer.addTVTimerWentOffListener(this);
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(suspendTimer);
            suspendTimer.setAbsoluteTime(p.getEndTime());   // TODO - mediaTime?
            try {
                timer.scheduleTimerSpec(suspendTimer);
            } catch (TVTimerScheduleFailedException ex) {
            }
        }

        public void timerWentOff(TVTimerWentOffEvent e) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockProgram.timerWentOff()");
            unblockedPrograms.remove(name);
        }

        public void selectionRequested(Channel ch, short type, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockProgram.selectionRequested()");
            channelChanged(ch);
        }

        public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockProgram.selectionBlocked()");
            channelChanged(ch);
        }

        public void serviceStopped(Channel lastChannel) {
        }

        public void showChannelInfo(Channel ch) {
        }

        public void programUpdated(Channel ch, Program p) {
        }

        private void channelChanged(Channel ch) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.UnblockProgram.channelChanged("+ch+")");
            if (!program.getCallLetter().equals(ch.getName())) {
                unblockedPrograms.remove(name);
            }
        }

        protected void stop() {
            ChannelController.get(0).removeChannelEventListener(this);
            TVTimer.getTimer().deschedule(suspendTimer);
        }

        public String toString() {
            return "Program[" + Formatter.getCurrent().getTime(program.getEndTime()) + "]";
        }
    }


}

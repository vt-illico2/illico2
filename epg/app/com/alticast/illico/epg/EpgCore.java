package com.alticast.illico.epg;

import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.menu.*;
import com.alticast.illico.epg.effect.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.virtualchannel.VirtualChannelPage;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.sdv.SDVController;
import com.alticast.illico.epg.ppv.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.tuner.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.errormessage.*;
import com.videotron.tvi.illico.ixc.search.*;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.adapter.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.log.Log;
import javax.tv.xlet.XletContext;
import javax.tv.service.selection.*;
import org.ocap.service.AbstractService;
import javax.tv.service.Service;
import javax.tv.locator.Locator;
import org.ocap.net.OcapLocator;
import org.havi.ui.HScene;
import org.dvb.event.*;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordedService;
import org.ocap.hn.service.RemoteService;
import org.ocap.hardware.Host;
import java.rmi.RemoteException;
import java.util.Vector;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import com.opencable.handler.cahandler.CAHandler;
import com.opencable.handler.nethandler.NetHandler;
import com.alticast.ui.*;
import com.alticast.illico.epg.sdv.SdvDiagnostics;

/**
 * This class is the main class of EPG application.
 *
 * @author  June Park
 */
public final class EpgCore implements ServiceContextListener, UserEventListener,
                                      ServiceStateListener, DataUpdateListener,
                                      MenuListener, MenuOpenListener, EpgListener {

    public static final String PVR_APP_NAME = "PVR";

    public static String dncsName;

    /** XletContext. */
    private XletContext xletContext;

    private ServiceContext serviceContext;

    private ChannelDatabaseBuilder channelDatabaseBuilder;
    private ChannelController channelController;

    public MiniEpg miniEpg;
    public GridEpg gridEpg;

    private VideoResizer mainVideoResizer;
    //->Kenneth[2015.6.23] DDC-107 : private 에서 public 으로 바꿈
    public PipController pipController;
    //private PipController pipController;
    //<-

    private VideoPlane[] videoPlane;

    private MonitorService monitorService;
    //->Kenneth[2017.9.1] R7.4 : TEST 를 위해서 public 으로 함
    public PvrService pvrService;

    /** singleton instance. */
    private static EpgCore instance = new EpgCore();

    private int state = EpgService.STATE_NONE;

    private int monitorState;
    public static boolean isBlockedMode = false;
    public static boolean activeStbMode = true;
    public static boolean navigableStbMode = true;

    private String videoContext = "";
    public static boolean epgVideoContext = true;

    private UserEventRepository eventRepository;
    private boolean eventRegistered = false;

    private DataCenter dataCenter = DataCenter.getInstance();

    OptionScreen optionScreen = new WatchTvOptionScreen();
    //->Kenneth[2015.8.19] DDC-113
    OptionScreen audioDescriptionOption = null;
    //<-

    //->Kenneth[2016.7.15] R7.2 Zoom 처리
    OptionScreen zoomOption = null;
    //<-

    private EIDMappingTableUpdateAdapter eidListener = new EIDMappingTableUpdateAdapter();
    private ScreenSaverAdapter screenSaver = new ScreenSaverAdapter();
    private STBModeChangeAdapter stbModeListener = new STBModeChangeAdapter();
    private VideoContextAdapter videoContextListener = new VideoContextAdapter();
    private ISAServiceDataAdapter isaServiceDataListener = new ISAServiceDataAdapter();

    private LogLevelAdapter logListener = new LogLevelAdapter();

    private MonitorCAAuthorizationListener caListener = new MonitorCAAuthorizationAdapter();

    public ReadyChecker readyChecker = new ReadyChecker();

    //->Kenneth[2015.6.15] R5 : Channel on demand 갔다가 back 으로 상세 페이지 돌아와야 함.
    public static Channel channelBeforeGoingToVOD = null;
    public static Program programBeforeGoingToVOD = null;

    //->Kenneth[2016.3.7] R7 
    public static boolean isChannelGrouped = true;
    //<-

    /**
     * Returns the singleton instance of EpgCore.
     *
     * @return EpgCore instance.
     */
    public static EpgCore getInstance() {
        return instance;
    }

    /** Constructor. */
    private EpgCore() {
        eventRepository = new UserEventRepository("Epg.Keys");
        eventRepository.addKey(OCRcEvent.VK_INFO);
        eventRepository.addKey(OCRcEvent.VK_ENTER);
        eventRepository.addKey(OCRcEvent.VK_LEFT);
        eventRepository.addKey(OCRcEvent.VK_RIGHT);
        eventRepository.addKey(OCRcEvent.VK_UP);
        eventRepository.addKey(OCRcEvent.VK_DOWN);
        eventRepository.addKey(KeyCodes.FAV);
        eventRepository.addKey(KeyCodes.COLOR_D);
        eventRepository.addKey(KeyCodes.SEARCH);
        //->Kenneth[2015.6.6] R5 : little boy 키로 add/remove favorite channel 한다.
        eventRepository.addKey(KeyCodes.LITTLE_BOY);
        //<-
        //->Kenneth[2015.8.18] DDC-113
        eventRepository.addKey(KeyCodes.STAR);
        //<-

        //->Kenneth[2016.7.15] R7.2 zoom 키
        eventRepository.addKey(KeyCodes.ASPECT);
        //<-

        optionScreen.setBackground(null);
        optionScreen.setAutoCloseDelay(DataCenter.getInstance().getLong("WATCH_D_OPTION_HIDE_DELAY", 5000L));

        readyChecker.add(MonitorService.IXC_NAME);
        readyChecker.add(SDVController.INIT_RESULT);
        readyChecker.add(ChannelDatabase.DATA_KEY);
        readyChecker.add("EpgCore");
    }

    /** Called when application's initXlet. */
    public void init(XletContext context) {
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug("#######################################");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init()");
        if (Log.DEBUG_ON) Log.printDebug("#######################################");
        this.xletContext = context;
        Effect.init();

        Object time = DataCenter.getInstance().get("OOB_RELOAD_TIME");
        if (time != null) {
            String s = time.toString();
            int pos = s.indexOf(':');
            if (pos > 0) {
                try {
                    int hour = Integer.parseInt(s.substring(0, pos));
                    int min = Integer.parseInt(s.substring(pos + 1));
                    SharedMemory.getInstance().put(SharedDataKeys.OOB_RELOAD_TIME, new int[] { hour, min });
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }

        if (App.EMULATOR) {
            DataCenter.getInstance().put("DNCS_NAME", "LAB");
            dncsName = "LAB";
        }

        this.serviceContext = Environment.getServiceContext(0);
        serviceContext.addListener(this);

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call ParentalControl.getInstance()");
        ParentalControl.getInstance();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call EpgServiceImpl.start()");
        EpgServiceImpl.getInstance().start(xletContext);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call PromotionBanner.setEpgService()");
        PromotionBanner.getInstance().setEpgService(EpgServiceImpl.getInstance());

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call EpgDataManager.init()");
        EpgDataManager.getInstance().init();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Create MiniEpg()");
        miniEpg = new MiniEpg();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Create GridEpg()");
        gridEpg = new GridEpg();
        Effect.getRoot(miniEpg);

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call CommunicationManager.start()");
        CommunicationManager.getInstance().start(xletContext, this);

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call ContentionHandler.init()");
        ContentionHandler.getInstance().init();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call ChannelController.get(0)");
        channelController = ChannelController.get(0);

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call TunerManager.getInstance()");
        TunerManager.getInstance();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call PpvController.getInstance()");
        PpvController.getInstance();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call DiagWindow.getInstance()");
        DiagWindow.getInstance();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Create VideoPlane()");
        videoPlane = new VideoPlane[Environment.VIDEO_DEVICE_COUNT];
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Call VideoResizer.getInstance()");
        mainVideoResizer = VideoResizer.getInstance();
        videoPlane[0] = mainVideoResizer;
        if (Environment.SUPPORT_PIP) {
            try {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : Create PipController()");
                pipController = new PipController();
                videoPlane[1] = pipController;
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        for (int i = 0; i < videoPlane.length; i++) {
            if (videoPlane[i] != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start EpgCore.init() : ChannelController.setVideoPlane("+videoPlane[i]+").init()");
                ChannelController.get(i).setVideoPlane(videoPlane[i]);
                videoPlane[i].init();
            }
        }

        Thread t = new Thread("EpgCore.init") {
            public void run() {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call ChannelDatabaseBuilder.build()");
                channelDatabaseBuilder = ChannelDatabaseBuilder.getInstance();
                ChannelDatabase db = channelDatabaseBuilder.build();

                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call PowerModeHandler.init()");
                PowerModeHandler.getInstance().init();

                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call RecordingStatus.init()");
                RecordingStatus.getInstance().init();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call ChannelZapper.getInstance()");
                ChannelZapper.getInstance();

                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call readyChecker.check(EpgCore)");
                readyChecker.check("EpgCore");

                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.init.t.run() : Call VolumeController.getInstance()");
                VolumeController.getInstance();
            }
        };
        t.start();
        UserActivityChecker.getInstance();
        if (Log.DEBUG_ON) Log.printDebug("#######################################");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"End EpgCore.init()");
        if (Log.DEBUG_ON) Log.printDebug("#######################################");
        if (Log.DEBUG_ON) Log.printDebug("");
    }

    private void readyEpg() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg()");
        MonitorService monitor = (MonitorService) dataCenter.get(MonitorService.IXC_NAME);
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg() : Call monitor.iamReady()");
            monitor.iamReady(FrameworkMain.getInstance().getApplicationName());
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg() : Call HeapManager.start()");
        HeapManager.getInstance().start();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg() : Call SDVController.startMonitoring()");
        SDVController.getInstance().startMonitoring();

        //->Kenneth[2016.7.8] R7.2 sticky filter
        try {
            String lastChannelFilter = UpManager.getInstance().get(UpManager.LAST_CHANNEL_FILTER);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg() : lastChannelFilter = "+lastChannelFilter);
            if (Rs.MENU_FILTER_SUBSCRIBED.getKey().equals(lastChannelFilter)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.readyEpg() : Call applyFilter()");
                applyFilter(lastChannelFilter, this);
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-
    }

    /** Called when application's pauseXlet. */
    public void pause() {
    }

    /** Called when application's destroyXlet. */
    public void destroy() {
        enableEvents(false);
//        PromotionBanner.getInstance().stopMonitoring();
        EpgServiceImpl.getInstance().dispose();
    }

    public void stopAll() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stopAll()");
        miniEpg.stopWithoutAnimation();
    }

    private void enableEvents(boolean enable) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.enableEvents("+enable+")");
        EventManager em = EventManager.getInstance();
        synchronized (eventRepository) {
            if (enable) {
                if (!eventRegistered) {
                    em.addUserEventListener(this, eventRepository);
                }
                eventRegistered = true;
            } else {
                if (eventRegistered) {
                    em.removeUserEventListener(this);
                }
                eventRegistered = false;
            }
        }
    }

    public void changeState(int newState) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore: changeState = " + newState);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : optionScreen.stop()");
        optionScreen.stop();
        //->Kenneth[2015.8.18] DDC-113
        if (audioDescriptionOption != null) audioDescriptionOption.stop();
        //<-
        //->Kenneth[2016.7.15] R7.2 : Zoom 처리
        if (zoomOption != null) zoomOption.stop();
        //<-
        switch (newState) {
            case EpgService.STATE_GUIDE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() -- STATE_GUIDE");
                if (pipController != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call PipController.stop()");
                    pipController.stop();
                }
                miniEpg.stopWithoutAnimation();
                if (state != EpgService.STATE_GUIDE) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call GridEpg.start()");
                    gridEpg.start();
                }
                break;
            case EpgService.STATE_PIP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() -- STATE_PIP");
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call MiniEpg.stopWithoutAnimation()");
                miniEpg.stopWithoutAnimation();
                if (pipController != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call PipController.start()");
                    pipController.start();
                }
                break;
            case EpgService.STATE_NONE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() -- STATE_NONE");
                if (pipController != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call PipController.stop()");
                    pipController.stop();
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call MiniEpg.stopWithoutAnimation()");
                miniEpg.stopWithoutAnimation();
                if (state == EpgService.STATE_GUIDE) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call GridEpg.stop()");
                    gridEpg.stop();
                }
                break;
        }
        state = newState;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeState() : Call updateState()");
        updateState();
    }

    public int getEpgState() {
        return state;
    }

    public VideoPlane getVideoPlane(int videoIndex) {
        return videoPlane[videoIndex];
    }

    public VideoResizer getVideoResizer() {
        return mainVideoResizer;
    }

    /** ServiceContextListener implemetation. */
    public void receiveServiceContextEvent(ServiceContextEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.receiveServiceContextEvent : " + e);
        }
        Service s = e.getServiceContext().getService();
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgCore.receiveServiceContextEvent : service = " + s);
        }
        if (s != null && (s instanceof RecordedService || s instanceof RemoteService)) {
            if (e instanceof NormalContentEvent) {
                if (!App.R3_TARGET || PVR_APP_NAME.equals(getVideoContextName())) {
                    channelController.setLastRecording(s);
                    FrontPanelDisplay.getInstance().showText("PLAY");
                }
            }
//            } else {
//                FrontPanelDisplay.getInstance().showText(
//                    TextUtil.getRightAlignedText(Integer.toString(channelController.getCurrent().getNumber()),
//                    ChannelZapper.digit, ' '));
//            }
        } else {
            PpvController.getInstance().receiveServiceContextEvent(e);
            resetScreenSaverTimer();
        }
    }

    public void userEventReceived(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return;
        }
        int code = event.getCode();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived : code = " + code);
        }

        if (code == KeyCodes.FAV) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call processFavKey()");
            processFavKey();
            return;
        }
        //->Kenneth[2016.7.15] R7.2 : Zoom 처리
        if (code == KeyCodes.ASPECT) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call processZoomKey()");
            processZoomKey();
            return;
        }
        //<-

        boolean isWatching;
        if (!App.R3_TARGET) {
            isWatching = state == EpgService.STATE_NONE && monitorState != MonitorService.FULL_SCREEN_APP_STATE
                      || state == EpgService.STATE_PIP;
        } else {
            isWatching = monitorState != MonitorService.FULL_SCREEN_APP_STATE && epgVideoContext;
        }

        //->Kenneth[2015.6.6] R5 : little boy 키로 add/remove favorite channel 한다.
        if (code == KeyCodes.LITTLE_BOY) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() LITTLE_BOY");
            //->Kenneth[2015.7.16] VDTRMASTER-5490 : av watching, special video 상태가 아닌 경우만 동작하기
            // 이것 때문에 isWatching 이후로 little boy 처리하는 코드를 옮겼음.
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() LITTLE_BOY : isWatching = "+isWatching);
            if (!isWatching) {
                return;
            }
            //<-

            //->Kenneth[2016.4.15] VDTRMASTER-5800 : miniEpg 가 뜬 상태에서는 LITTLE_BOY 키가 miniEpg 의 채널에 적용되어야 함.
            //Channel ch = channelController.getCurrent();
            Channel ch = null;
            boolean differentWithCurrentChannel = false;
            try {
                if (miniEpg != null && miniEpg.isVisible()) {
                    ch = miniEpg.getFocusedChannel();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() LITTLE_BOY : ch from miniEpg = "+ch);
                    Channel curCh = channelController.getCurrent();
                    try {
                        if (!curCh.equals(ch)) {
                            differentWithCurrentChannel = true;
                        }
                    } catch (Exception e) {
                        Log.print(e);
                    }
                } else {
                    ch = channelController.getCurrent();
                }
            } catch (Exception e) {
                Log.print(e);
            }
            //<-

            if (ch == null) return;
            //->Kenneth[2015.10.3] VDTRMASTER-5677 : favorite 등록후 채널 package 빠지는 경우에도
            // remove 는 될 수 있어야 함. 권한 체크를 add 하는 경우에만 하도록 함.
            //->Kenneth[2015.9.1] VDTRMASTER-5578
            //if (!ch.isAuthorized()) return;
            //<-

            //->Kenneth[2017.2.27] R7.3 : TECH 채널은 favorite/block 채널이 될 수 없음
            if (ch.getType() == Channel.TYPE_TECH) return;
            //<-

            if (ch.isFavorite()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() LITTLE_BOY : Call removeFavoriteChannel()");
                //->Kenneth[2016.4.15] VDTRMASTER-5800 : miniEpg 가 뜬 상태에서는 LITTLE_BOY 키가 miniEpg 의 채널에 적용되어야 함.
                // 그러나 channel banner 가 계속 뜨는 것을 listener 를 넣지 않게 해서 막는다.
                if (differentWithCurrentChannel) {
                    removeFavoriteChannel(ch, null);
                } else {
                    removeFavoriteChannel(ch, this);
                }
                //removeFavoriteChannel(ch, this);
                //<-
            } else {
                if (!ch.isAuthorized()) return;
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() LITTLE_BOY : Call addFavoriteChannel()");
                //->Kenneth[2016.3.9] R7 : VBM 로그 

                //->Kenneth[2016.4.15] VDTRMASTER-5800 : miniEpg 가 뜬 상태에서는 LITTLE_BOY 키가 miniEpg 의 채널에 적용되어야 함.
                // 그러나 channel banner 가 계속 뜨는 것을 listener 를 넣지 않게 해서 막는다.
                if (differentWithCurrentChannel) {
                    addFavoriteChannel(ch, null, true);
                } else {
                    addFavoriteChannel(ch, this, true);
                }
                //addFavoriteChannel(ch, this, true);
                //<-

                //addFavoriteChannel(ch, this);
                //<-
            }
            //<-
            //->Kenneth[2015.6.11] R5 : mini epg 가 떠 있는 경우에 반영되어야 한다.
            if (miniEpg != null && miniEpg.isVisible()) {
                miniEpg.repaint();
            }
            return;
        }
        //<-

        if (!isWatching) {
            // R3 - PVR watching 에서 키가 skip 된다.
            if (code == KeyCodes.COLOR_D && !epgVideoContext) {
                //->Kenneth[2016.7.18] R7.2 zoom 키 처리. zoom option 이 떠 있으면 D 키 무시
                if (zoomOption != null && zoomOption.isStarted()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() ZoomOption is visible, so not to show OptionScreen");
                } else {
                    MenuItem videoMenu = createVideoOptions();
                    if (videoMenu != null) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call optionScreen.start() in !isWatching");
                        optionScreen.start(videoMenu, null, this, this);
                    }
                }
                /*
                MenuItem videoMenu = createVideoOptions();
                if (videoMenu != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call optionScreen.start() in !isWatching");
                    optionScreen.start(videoMenu, null, this, this);
                }
                */
                //<-


            //->Kenneth[2016.1.18] DDC-115
            // * keycode 를 전달하도록 한다.
            // Exit 의 경우 EpgCore 까지 전달되지 않으므로 UserActivityChecker 에서 바로 showAudioDescriptionOption 를
            // 불러준다.
            } else if (code == KeyCodes.STAR) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAudioDescriptionOption(STAT) in !isWatching");
                showAudioDescriptionOption(code);

            //->Kenneth[2017.4.26] VDTRMASTER-6125 : search 를 여기서 띄운다. 원래는 monitor 가 띄웠음.
            } else if (code == KeyCodes.SEARCH) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() : !isWatching but Call launchSearch(MONITOR)");
                launchSearch(SearchService.PLATFORM_MONITOR, true);
            //<-
            }
            /*
            //->Kenneth[2015.8.18] DDC-113
            } else if (code == KeyCodes.STAR) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAudioDescriptionOption() in !isWatching");
                showAudioDescriptionOption();
            }
            //<-
            */
            //<-
            return;
        }
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (!App.R3_TARGET || epgVideoContext) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call ChannelZapper.showCurrentInfo()");
                    ChannelZapper.getInstance().showCurrentInfo();
                }
                break;
            case OCRcEvent.VK_INFO:
            case OCRcEvent.VK_UP:
            case OCRcEvent.VK_DOWN:
            case OCRcEvent.VK_LEFT:
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call optionScreen.stop()");
                optionScreen.stop();
                //->Kenneth[2015.8.18] DDC-113
                if (audioDescriptionOption != null) audioDescriptionOption.stop();
                //<-
                //->Kenneth[2016.7.15] R7.2 : Zoom 처리
                if (zoomOption != null) zoomOption.stop();
                //<-
                if (checkPermission(App.NAME)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call miniEpg.start()");
                    miniEpg.start();
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAppErrorMessage(MOT501)");
                    showAppErrorMessage("MOT501", null, App.NAME);
                }
                break;
            case KeyCodes.COLOR_D:
                if (zoomOption != null && zoomOption.isStarted()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() ZoomOption is visible, so not to show OptionScreen");
                    break;
                }
                if (checkPermission(App.NAME)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showOption()");
                    showOption();
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAppErrorMessage(MOT501) -- 2");
                    showAppErrorMessage("MOT501", null, App.NAME);
                }
                break;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call optionScreen.stop()");
                optionScreen.stop();
                //->Kenneth[2015.8.18] DDC-113
                if (audioDescriptionOption != null) audioDescriptionOption.stop();
                //<-
                //->Kenneth[2016.7.15] R7.2 : Zoom 처리
                if (zoomOption != null) zoomOption.stop();
                //<-

                //->Kenneth[2017.4.26] VDTRMASTER-6125 : search 를 여기서 띄운다. 원래는 monitor 가 띄웠음.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call launchSearch(EPG)");
                launchSearch(SearchService.PLATFORM_EPG, true);
                //<-

                break;
            //->Kenneth[2016.1.18] DDC-115
            // * keycode 를 전달하도록 한다.
            // Exit 의 경우 EpgCore 까지 전달되지 않으므로 UserActivityChecker 에서 바로 showAudioDescriptionOption 를
            // 불러준다.
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAudioDescriptionOption(STAR)");
                showAudioDescriptionOption(code);
                break;
            /*
            //->Kenneth[2015.8.18] DDC-113
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.userEventReceived() Call showAudioDescriptionOption()");
                showAudioDescriptionOption();
                break;
            //<-
            */
            //<-
        }
    }

    private MenuItem createVideoOptions() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.createVideoOptions()");
        String vc = this.videoContext;

        MenuItem root;
        if ("PVR".equals(vc)) {
            root = new MenuItem(vc);
            root.add(Rs.MENU_PICTURE);
            root.add(Rs.MENU_INVOKE_CAPTION);
            root.add(Rs.MENU_AUDIO_DESC);
//            root.add(Rs.MENU_GO_TO_HELP);

        } else if ("VOD".equals(vc)) {
            root = new MenuItem(vc);
            root.add(Rs.MENU_PICTURE);
            root.add(Rs.MENU_INVOKE_CAPTION);
//            root.add(Rs.MENU_GO_TO_HELP);

        } else {
            return null;
        }
        return root;
    }
    //->Kenneth[2015.10.7] VDTRMASTER-5664 improvement
    // Fav 버튼이 눌렸을 때 curCh 다음의 next fav 채널을 돌려준다.
    // SD sister 채널은 skip 한다.
    // null 을 돌려주는 경우는 오직 등록된 favorate 채널이 없는 경우 뿐이다.
    // 아래에서 curCh 은 null 이 되면 안된다.
    // 추가 : removeHd 를 추가한 것은 Grid 에서의 동작과 AV 상태에서의 동작이 다르기 때문이다.
    // favorite 채널이 1, 2|202, 3 이 등록되어 있다면
    // Grid 에서는 1 -> 2 -> 3 을 순서가 되어야 하고 (removeHd 가 true)
    // HD 튜닝이 우선인 AV 상태에서는 1 -> 3 -> 202 가 되어야 하기 때문이다.(removeHd 가 false)
    public Channel getNextFavChannel(Channel curCh, boolean removeHd) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel("+curCh+", "+removeHd+")");
        Channel newCh = null;
        try {
            ChannelList list = ChannelDatabase.current.filter(Rs.MENU_FILTER_FAVOURITES.getKey());
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : size = "+list.size());
            if (list.size() == 0) {
                return null;
            }


            //->Kenneth[2015.10.18] VDTRMASTER-5699 : all channel 중의 favorite 이 아닌
            // filtered channel 중의 favorite 을 먼저 사용해야 한다.
            boolean isHdFiltered = false;
            try {
                ChannelList currentList = ChannelController.get(0).getChannelList();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : currentList() = "+currentList);
                if (ChannelController.get(0).getCurrentFilterKey().equals(Rs.MENU_FILTER_HD.getKey())) {
                    isHdFiltered = true;
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : isHdFiltered = "+isHdFiltered);
                ChannelList filteredFavList = currentList.filter(ChannelDatabase.favoriteFilter);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : filteredFavList.size() = "+filteredFavList.size());
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Dump of filteredFavList");
                filteredFavList.dump();
                if (filteredFavList.size() == 0) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Call applyFilter(ALL)");
                    EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_ALL.getKey(), this);
                } else {
                    list = filteredFavList;
                }
            } catch (Exception ex) {
                if (Log.DEBUG_ON) Log.print(ex);
            }
            //<-


            //->Kenneth[2016.3.7] R7 : Ungrouped 상태면 아무것도 하지 않는다.
            if (isChannelGrouped) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Channel Grouped!");
                list = list.cloneList();
                if (isHdFiltered) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : HD filter. Dump without removeSister");
                    list.dump();
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Dump before removeSister");
                    list.dump();
                    //->Kenneth[2015.10.7] VDTRMASTER-5695. 여기서는 subcription 따짐.
                    list.removeSister(removeHd, true);
                    //list.removeSister(removeHd);
                    //<-
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Dump after removeSister");
                    list.dump();
                }
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Channel Ungrouped! Dump()");
                list.dump();
            }
            /*
            list = list.cloneList();
            if (isHdFiltered) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : HD filter. Dump without removeSister");
                list.dump();
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Dump before removeSister");
                list.dump();
                //->Kenneth[2015.10.7] VDTRMASTER-5695. 여기서는 subcription 따짐.
                list.removeSister(removeHd, true);
                //list.removeSister(removeHd);
                //<-
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Dump after removeSister");
                list.dump();
            }
            */
            //<-

            int index = list.search(curCh);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : Current Index = "+index);
            if (index < 0) {
                index = 0;
            } else {
                index = (index + 1) % list.size();
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : New Index = "+index);
            newCh = list.getChannelAt(index);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() : newCh = "+newCh);
            if (newCh == null) {
                // 요건 사실상 발생하면 안되는 error 상황임
                newCh = curCh;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.getNextFavChannel() returns = "+newCh);
        return newCh;
    }
    //<-

    //->Kenneth[2016.7.15] R7.2 : Zoom 처리
    public void processZoomKey() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : monitorState = "+monitorState);
        if (monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : FULL_SCREEN_APP_STATE so return");
            return;
        }
        try {
            Channel currentCh = (Channel)channelController.getCurrentChannel();
            if (currentCh != null && videoContext != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : currentCh = "+currentCh);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : videoContext = "+videoContext);
                if (videoContext.equals("EPG")) {
                    if (currentCh.getType() == Channel.TYPE_VIRTUAL) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : Virtual Channel, so return");
                        return;
                    }
                    if (currentCh.getType() == Channel.TYPE_GALAXIE) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : Galaxie Channel, so return");
                        return;
                    }
                    if (currentCh.getType() == Channel.TYPE_RADIO) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : Radio Channel, so return");
                        return;
                    }
                    if (!currentCh.isSubscribed()) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processZoomKey() : unsubscribed Channel, so return");
                        return;
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        showZoomOption();
    }
    //<-

    //->Kenneth[2016.7.15] R7.2 : Zoom 처리
    public void showZoomOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption()");
        // 아래의 optionScreen 은 AV 상태에서의 option screen 임을 주의. EPG 의 각 화면별로 option screen 은 해당 클래스에서
        // 관리됨.
        boolean dOptionVisible = optionScreen.isStarted();
        if (audioDescriptionOption != null && audioDescriptionOption.isStarted()) {
            dOptionVisible = true;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : dOptionVisible = "+dOptionVisible);
        if (dOptionVisible) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : Do nothing.");
            return;
        }
        if (zoomOption == null || !zoomOption.isStarted()) {
            // option screen 을 보여준다.
            String currentSetting = (String)DataCenter.getInstance().get(PreferenceNames.VIDEO_ZOOM_MODE);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : currentSetting = "+currentSetting);
            //zoomOption = new OptionScreen();
            zoomOption = new OptionScreen(KeyCodes.ASPECT, PreferenceService.BTN_POUND, "close_options");
            zoomOption.setAutoCloseDelay(DataCenter.getInstance().getLong("WATCH_D_OPTION_HIDE_DELAY", 5000L));
            if (monitorState == MonitorService.TV_VIEWING_STATE || monitorState == MonitorService.PIP_STATE) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : setBackground(null)");
                zoomOption.setBackground(null);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : Call start()");
            zoomOption.start(Rs.MENU_PICTURE_BY_SHARP, null, this, this);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : Zoom Option is visible now.");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showZoomOption() : Call stop()");
            zoomOption.stop();
            zoomOption = null;
        }
    }

    public void processFavKey() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processFavKey()");
        //->Kenneth[2015.8.21] VDTRMASTER-5591
        // 정준이가 왜 STOPPED 인 경우 막아놨는지는 알 수 없음. 얘로 인해서 fav 버튼 동작 안하게 되므로
        // 일단 풀도록 한다
        /*
        if (channelController.getState() == ChannelController.STOPPED) {
            Log.printDebug("EpgCore.processFavKey: ChannelController STOPPED");
            return;
        }
        */
        //<-

        //->Kenneth[2015.10.7] VDTRMASTER-5664 improvement
        // getNextFavChannel 을 이용하도록 구현한다.
        Channel curCh = channelController.getCurrent();
        if (curCh == null) return;
        Channel nextFavCh = getNextFavChannel(curCh, false);
        if (nextFavCh == null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processFavKey() : showNoFavPopup()");
            PopupController.getInstance().showNoFavPopup();
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.processFavKey() : changeChannel("+nextFavCh+")");
            channelController.changeChannel(nextFavCh);
        }
        /*
        ChannelList list = ChannelDatabase.current.filter(Rs.MENU_FILTER_FAVOURITES.getKey());
        if (list.size() == 0) {
            PopupController.getInstance().showNoFavPopup();
            return;
        }
        int index = list.search(channelController.getCurrent());
        if (index < 0) {
            index = 0;
        } else {
            index = (index + 1) % list.size();
        }
        channelController.changeChannel(list.getChannelAt(index));
        */
        //<-
    }

    public void stateChanged(int newMonitorState) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged("+newMonitorState+")");
        this.monitorState = newMonitorState;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call updateState()");
        updateState();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call optionScreen.stop()");
        optionScreen.stop();
        //->Kenneth[2015.8.18] DDC-113
        if (audioDescriptionOption != null) audioDescriptionOption.stop();
        //<-
        //->Kenneth[2016.7.15] R7.2 : Zoom 처리
        if (zoomOption != null) zoomOption.stop();
        //<-
        if (monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call miniEpg.stopWithoutAnimation()");
            miniEpg.stopWithoutAnimation();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call ChannelBackground.pause()");
            ChannelBackground.getInstance().pause();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call EpgChannelBanner.stop()");
            EpgChannelBanner.getInstance().stop();
        } else if (monitorState == MonitorService.TV_VIEWING_STATE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stateChanged() : Call ChannelBackground.resume()");
            ChannelBackground.getInstance().resume();
        }
    }

    public int getMonitorState() {
        return monitorState;
    }

    public boolean isSpecialVideoState() {
        try {
            return monitorService.isSpecialVideoState();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }

    private void updateState() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.updateState()");
        boolean watch = monitorState != MonitorService.FULL_SCREEN_APP_STATE || state == EpgService.STATE_PIP;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.updateState() : Call ChannelZapper.setWatchTvState("+watch+")");
        ChannelZapper.getInstance().setWatchTvState(watch);
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.dataUpdated("+key+")");
        if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                monitorService.addInbandDataListener(ConfigManager.getInstance(), appName);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addStateListener(this, appName);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                this.monitorState = monitorService.getState();
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                eidListener.tableUpdated();
            } catch (Exception ex) {
                Log.print(ex);
            }
            try {
                monitorService.addEIDMappingTableUpdateListener(eidListener, appName);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addScreenSaverConfirmListener(screenSaver, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addSTBModeChangeListener(stbModeListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addCAResourceAuthorizationListener(caListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addVideoContextListener(videoContextListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                stbModeListener.modeChanged(monitorService.getSTBMode());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                videoContextListener.videoModeChanged(monitorService.getVideoContextName());
            } catch (RemoteException ex) {
                Log.print(ex);
            }

            PromotionBanner.getInstance().setMonitorService(monitorService);

        } else if (key.equals(PreferenceService.IXC_NAME)) {
            UpManager.getInstance().init((PreferenceService) value);
            //->Kenneth [2016.1.21] DDC-115
            // 위의 init 내에서 DataCenter 에 ONE_TIME_AUDIO_DESCRIPTION_OFF 의 현재 UP 에 저장된 값을 넣었음.
            // 따라서 그 값이 null 이나 "" 이면 아직 한번도 셋팅되지 않을 것이므로 (DataCenter 에서 꺼낼 수 있음)
            // 이 값을 YES 로 셋팅하고 audio description 을 OFF 시킨다.
            // 다음 부팅시에는 YES 가 올 것이므로 OFF 시키지 않을 것이다.
            String currentSetting = (String)DataCenter.getInstance().get(UpManager.ONE_TIME_AUDIO_DESCRIPTION_OFF);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.dataUpdated() : ONE_TIME_AUDIO_DESCRIPTION_OFF = "+currentSetting);
            if (currentSetting == null || "".equals(currentSetting)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.dataUpdated() : Call UpManager.set(ONE_TIME_AUDIO_DESCRIPTION_OFF, YES)");
                UpManager.getInstance().set(UpManager.ONE_TIME_AUDIO_DESCRIPTION_OFF, Definitions.OPTION_VALUE_YES);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.dataUpdated() : Call UpManager.set(DESCRIBED_AUDIO_DISPLAY, OFF)");
                UpManager.getInstance().set(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_OFF);
                Rs.MENU_AUDIO_DESC_OFF.setChecked(true);
                Rs.MENU_AUDIO_DESC_ON.setChecked(false);
            }
            //<-

        } else if (key.equals(PvrService.IXC_NAME)) {
            pvrService = (PvrService) value;
//            try {
//                pvrService.addContentsChangedListener(RecordingStatus.getInstance());
//            } catch (RemoteException ex) {
//                Log.print(ex);
//            }
        } else if (key.equals(CAHandler.IXC_NAME)) {
            CAHandler handler = (CAHandler) value;
            CaManager.getInstance().init(handler);
            PpvController.getInstance().init(handler);

        } else if (key.equals(NetHandler.IXC_NAME)) {
            NetManager.getInstance().init((NetHandler) value);

        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) value;
            try {
                int currentLevel = stcService.registerApp(App.NAME);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + App.NAME);
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(App.NAME, logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        } else if (key.equals(VbmService.IXC_NAME)) {
            EpgVbmController.getInstance().init((VbmService) value);

        } else if (key.equals(DaemonService.IXC_NAME)) {
            DaemonService daemonService = (DaemonService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                daemonService.addListener(appName, RemotePpvHandler.getInstance());
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        } else if (key.equals(SearchService.IXC_NAME)) {
            SearchService ss = (SearchService) value;
            gridEpg.setSearchService(ss);

        } else if (key.equals(NotificationService.IXC_NAME)) {
            ReminderManager.getInstance().init((NotificationService) value);

        } else if (key.equals(ISAService.IXC_NAME)) {
            ISAService is = (ISAService) value;
            try {
                isaServiceDataListener.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL, is.getISAServiceList(ISAService.ISA_SERVICE_TYPE_CHANNEL));
            } catch (Exception ex) {
                Log.print(ex);
            }
            try {
                isaServiceDataListener.updateISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW, is.getISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW));
            } catch (Exception ex) {
                Log.print(ex);
            }
            //->Kenneth[2016.8.22] ISA/CYO Bundling/A la carte
            try {
                isaServiceDataListener.updateISAExcludedChannelsAndTypes(is.getExcludedChannels(), is.getExcludedChannelTypes());
            } catch (Exception ex) {
                Log.print(ex);
            }
            //<-
            try {
                is.addISAServiceDataListener(isaServiceDataListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        }
        // use one time
        dataCenter.removeDataUpdateListener(key, this);
    }

    public void dataRemoved(String key)  {
    }

    /** record. */
    public void record(final Channel ch, final Program p) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.record() program = " + p + ", startTime = " + new Date(p.startTime));
        }
        if (p == null || p instanceof FakeProgram || p instanceof NullProgram) {
            return;
        }
        if (!supportDvr()) {
            Log.printInfo("EpgCore.record. not support DVR !");
            return;
        }
        //->Kenneth[2015.10.15] DDC-114
        int reason = IncompatibilityChannelPanel.resetReasonCode(ch);
        if (reason == IncompatibilityChannelPanel.REASON_NO_UHD_PACKAGE) {
            if (Log.DEBUG_ON) Log.printDebug("EpgCore.record() : Call showIncompatibilityErrorMessage()");
            EpgCore.getInstance().showIncompatibilityErrorMessage(reason);
            return ;
        }
        /*
        //->Kenneth[2015.2.3] 4K
        if (needToShowIncompatibilityPopup(ch)) {
            if (Log.DEBUG_ON) Log.printDebug("EpgCore.record() : Call showIncompatibilityErrorMessage()");
            showIncompatibilityErrorMessage();
            return ;
        }
        */
        //<-
        if (p instanceof PpvProgram) {
            if (!PpvController.getInstance().isPurchasedOrReserved((PpvProgram) p)
                        && !CaManager.getInstance().checkSubscriptionPurchased(ch)) {
                Log.printInfo("EpgCore.record. not purchased PPV !");
                return;
            }
        } else if (ch == null) {
            Log.printInfo("EpgCore.record. channel is null !");
            return;
        } else if (!ch.isRecordable()) {
            Log.printInfo("EpgCore.record. not recordable channel !");
            return;
        } else if (App.BLOCK_TO_RECORD_UNSUB_CHANNEL && !CaManager.getInstance().check(ch)) {
            Log.printInfo("EpgCore.record. unsubscribed channel !");
            boolean ret = showSubscriptionPopup(ch, ISAService.ENTRY_POINT_EPG_E01);
            if (!ret) {
                PopupController.getInstance().showRecordingUnsubPopup();
            }
            return;
        }
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.record() : Call pvrService.record("+p+")");
            pvrService.record(ch, p);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /** cancel record. */
    public void cancelRecord(Program p) {
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.cancelRecord() : Call pvrService.cancel("+p+")");
            pvrService.cancel(p);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }


    public void showErrorMessage(String errorCode, ErrorMessageListener l) {
        showErrorMessage(errorCode, l, null);
    }

    public void showAppErrorMessage(String errorCode, ErrorMessageListener l, String appName) {
        Hashtable table = new Hashtable();
        table.put("<APP_NAME>", appName);
        showErrorMessage(errorCode, l, table);
    }

    public void showErrorMessage(String errorCode, ErrorMessageListener l, Hashtable table) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore: showErrorMessage : " + errorCode);
        }
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            Log.printError("EpgCore: Not bound ErrorMessageService");
            return;
        }
        try {
            if (table != null) {
                svc.showErrorMessage(errorCode, l, table);
            } else {
                svc.showErrorMessage(errorCode, l);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void hideErrorMessage() {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore: hideErrorMessage");
        }
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            Log.printError("EpgCore: Not bound ErrorMessageService");
            return;
        }
        try {
            svc.hideErrorMessage();
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //->Kenneth[2015.2.28] 4K
    // requestor 가 main 이면 pip 가 DualUhdPipPanel 을 보여주고 있는지 확인해서 다시 tune 하고
    // pip 이면 main 에게 같은 일을 해준다
    // panel 이 보여질 조건이 지나갔는데도 계속 보여주기 때문에 이를 처리
    // [2015.3.17] 삼성에서 small PIP 에서는 항상 UHD 보이지 않는다고 함. 따라서 
    // 이 기능은 더 이상 필요 없으므로 사용 안함.
    /*
    public void checkDualUhdPipPopup(VideoPlane requestor) {
        Log.printDebug(App.LOG_HEADER+"EpgCore.checkDualUhdPipPopup("+requestor+")");
        if (pipController == null || mainVideoResizer == null) return;
        ChannelController targetCC = null;
        VideoPlane target = null;
        if (requestor instanceof PipController) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.checkDualUhdPipPopup() : Request From PIP");
            target = mainVideoResizer;
            targetCC = ChannelController.get(0);
        } else if (requestor instanceof VideoResizer) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.checkDualUhdPipPopup() : Request From MAIN");
            target = pipController;
            targetCC = ChannelController.get(1);
        } else {
            Log.printDebug(App.LOG_HEADER+"EpgCore.checkDualUhdPipPopup() : Request From ??");
        }
        if (target == null || targetCC == null) return;
        if (target.isDualUhdPipVisible()) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.checkDualUhdPipPopup() : Call targetCC.changeChannel("+targetCC.getCurrent()+")");
            targetCC.changeChannel(targetCC.getCurrent());
        }
    }
    */

    //->Kenneth[2015.10.13] DDC-114
    // EPG 가 팝업을 띄우는 경우는 EPG850, EPG851 과 관계 없다. 왜냐하면 IncompatibilityChannelPage 를 띄울 것이기
    // 때문이다. 따라서 팝업 띄우는 부분은 수정없이 그대로 유지 한다.
    //->Kenneth[2015.10.15] DDC-114 : reason code 를 받아서 그에 맞는 팝업 call 을 하도록 수정
    //->Kenneth[2015.2.2] 4K
    public void showIncompatibilityErrorMessage(int reason) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore: showIncompatibilityErrorMessage("+reason+")");
        }
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            Log.printError("EpgCore: Not bound ErrorMessageService");
            return;
        }
        try {
            if (reason == IncompatibilityChannelPanel.REASON_NO_UHD_PACKAGE) {
                svc.showCommonMessage("");
            } else if (reason == IncompatibilityChannelPanel.REASON_EPG850) {
                svc.showCommonMessage("EPG850");
            } else if (reason == IncompatibilityChannelPanel.REASON_EPG851) {
                svc.showCommonMessage("EPG851");
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //->Kenneth[2015.10.13] DDC-114
    // EPG850, 851 남기는 부분에서 Error message app 쪽으로 메시지 전달
    public void sendIncompatibilityErrorMessage(String msg) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"sendIncompatibilityErrorMessage("+msg+")");
        if (msg == null) return;
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            Log.printError("EpgCore: Not bound ErrorMessageService");
            return;
        }
        try {
            svc.getCommonMessage(msg);
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //<-

    //->Kenneth[2015.10.15] DDC-114 : IncompatibiltyChannelPage 의 resetReasonCode 를 사용하기로 한다.
    /*
    //->Kenneth[2015.2.2] 4K : incompatibilty popup 을 띄워야 하는 상황인지 체크한다.
    public boolean needToShowIncompatibilityPopup(Channel ch) {
        boolean result = false;
        if (!Environment.SUPPORT_UHD && ch != null && ch.getDefinition() == Channel.DEFINITION_4K) {
            result = true;
        } 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.needToShowIncompatibilityPopup() returns "+result);
        return result;
    }
    */
    //<-

    //->Kenneth[2016.3.9] R7 : VBM 로그를 D-option 통해서인지 little boy button 통해서인지 남겨야 함.
    public void addFavoriteChannel(Channel ch, EpgListener el, boolean byLittleBoy) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel("+ch+", "+byLittleBoy+")");
        if (el != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel() : Call EpgListener.notifyEpgEvent()");
            el.notifyEpgEvent(EpgListener.ADDED_TO_FAVOURITES, ch);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel() : Call UpManager.changeFavoriteChannel()");
        UpManager.getInstance().changeFavoriteChannel(ch, true);
        if (byLittleBoy) {
            EpgVbmController.getInstance().writeFavoriteAdded(ch.getCallLetter()+"_P");
        } else {
            EpgVbmController.getInstance().writeFavoriteAdded(ch.getCallLetter()+"_D");
        }
    }
    /*
    public void addFavoriteChannel(Channel ch, EpgListener el) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel("+ch+")");
        if (el != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel() : Call EpgListener.notifyEpgEvent()");
            el.notifyEpgEvent(EpgListener.ADDED_TO_FAVOURITES, ch);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addFavoriteChannel() : Call UpManager.changeFavoriteChannel()");
        UpManager.getInstance().changeFavoriteChannel(ch, true);
    }
    */
    //<-

    public void removeFavoriteChannel(Channel ch, EpgListener el) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.removeFavoriteChannel("+ch+")");
        if (el != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.removeFavoriteChannel() : Call EpgListener.notifyEpgEvent()");
            el.notifyEpgEvent(EpgListener.REMOVED_FROM_FAVOURITES, ch);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.removeFavoriteChannel() : Call UpManager.changeFavoriteChannel()");
        UpManager.getInstance().changeFavoriteChannel(ch, false);
    }

    public void addBlockedChannel(final Channel ch, final EpgListener el) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.addBlockedChannel("+ch+")");
        RightFilterListener rfl = new RightFilterListener() {
            // Kenneth
            //String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.block_pin"), '|');
            String[] explain = getBlockedChannelExplain(ch, true);
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    if (el != null) {
                        el.notifyEpgEvent(EpgListener.ADDED_TO_RESTRICTED, ch);
                    }
                    UpManager.getInstance().changeBlockedChannel(ch, true);
                    if (!ParentalControl.enabled) {
                        UpManager.getInstance().set(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
                    }
                    // block current channel
                    synchronized (channelController) {
                        if (ch.equals(channelController.getCurrentChannel())) {
                            channelController.blockIfNeeded();
                        }
                    }
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.CHANNEL_RESTRICTION }, null, null);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    //->Kenneth[2015.11.23] VDTRMASTER-5718 : 여러곳에서 사용할 수 있도록 함. ch null 추가.
    public String[] getBlockedChannelExplain(Channel ch, boolean isBlocking) {
    //->Kenneth [2015.4.21] : R5
    //->Kenneth [R4.1] : sister channel 이 있으면 그에 맞는 explain 을 만들어서 돌려준다
    //private String[] getBlockedChannelExplain(Channel ch, boolean isBlocking) {
        String[] explain = null;
        try {
            if (isBlocking) {
                explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.block_pin"), '|');
            } else {
                explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
            }
            if (ch != null) {
                String fullName = ch.getFullName();
                //->Kenneth[2017.8.23] R7.4 : FULL_NAME 위치가 3번째 라인으로 이동되었음.
                // 또한 ellisize 할 필요도 없어 보임. 혼자 한 라인 다 차지함.
                if (fullName == null || fullName.length() == 0) {
                    fullName = "";
                }
                explain[2] = TextUtil.replace(explain[2], "FULL_NAME", fullName);
                /*
                if (fullName == null || fullName.length() == 0) {
                    explain[1] = TextUtil.replace(explain[1], "FULL_NAME", "");
                } else {
                    //->Kenneth[2015.9.10] VDTRMASTER-5632 : fullName 이 너무 길면 자르자.
                    if (fullName.length() > 14) {
                        fullName = fullName.substring(0, 14)+"...";
                        if (Log.DEBUG_ON) Log.printDebug("EpgCore.getBlockedChannelExplain() : Ellipsized fullName = "+fullName);
                    }
                    //<-
                    explain[1] = TextUtil.replace(explain[1], "FULL_NAME", fullName);
                }
                */
                //<-
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return explain;
    }
    //<-

    public void removeBlockedChannel(final Channel ch, final EpgListener el) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.removeBlockedChannel("+ch+")");
        RightFilterListener rfl = new RightFilterListener() {
            //Kenneth
            //String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
            String[] explain = getBlockedChannelExplain(ch, false);
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    UpManager.getInstance().changeBlockedChannel(ch, false);
                    // unblock current channel
                    synchronized (channelController) {
                        if (ch.equals(channelController.getCurrentChannel())
                            && channelController.getState() == ParentalControl.BLOCK_BY_CHANNEL) {
                            channelController.releaseChannelBlock(ch);
                        }
                    }
                    if (el != null) {
                        el.notifyEpgEvent(EpgListener.REMOVED_FROM_RESTRICTED, ch);
                    }
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.CHANNEL_RESTRICTION }, null, null);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void goToPreference(final String appName) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.goToPreference("+appName+")");
        DataCenter dc = DataCenter.getInstance();
        if (Definitions.OPTION_VALUE_ON.equals(dc.get(RightFilter.PARENTAL_CONTROL))
                && Definitions.OPTION_VALUE_YES.equals(dc.get(RightFilter.PROTECT_SETTINGS_MODIFICATIONS))) {

            RightFilterListener rfl = new RightFilterListener() {
                String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.desc_go_pref"), '|');
                public void receiveCheckRightFilter(int response) {
                    if (response == PreferenceService.RESPONSE_SUCCESS) {
                        goToPreferenceImpl(appName);
                    }
                }
                public String[] getPinEnablerExplain() {
                    return explain;
                }
            };
            PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
            try {
                ps.checkRightFilter(rfl, App.NAME,
                        new String[] { RightFilter.PROTECT_SETTINGS_MODIFICATIONS }, null, null);
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            goToPreferenceImpl(appName);
        }
    }

    public ChannelList applyFilter(String filterName, EpgListener l) {
        return applyFilter(channelController, filterName, l);
    }

    public ChannelList applyFilter(ChannelController cc, String filterName, EpgListener l) {
        ChannelList list = ChannelDatabase.current.filter(filterName);
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"EpgCore.applyFilter. name = " + filterName + ", size = " + list.size());
        }
        if (list.size() == 0) {
            if (filterName.equals(Rs.MENU_FILTER_FAVOURITES.getKey())) {
                PopupController.getInstance().showNoFavPopup();
            } else {
                PopupController.getInstance().showConfirm(
                        dataCenter.getString("epg.no_filtered_popup_title"),
                        dataCenter.getString("epg.no_filtered_popup_msg"), null);
            }
            return list;
        }
        if (l == null && cc == channelController) {
            // listener가 null 인 경우 적절하게 설정해준다.
            if (state == EpgService.STATE_GUIDE) {
                l = gridEpg;
            } else {
                // MiniEpg가 떠 있어도 filter가 바뀔 경우는 channel banner에서 처리하는게 깔끔
                l = this;
            }
        }

        if (l != null) {
            l.notifyEpgEvent(FILTER_WILL_BE_CHANGED, filterName);
        }
        if (cc != null) {
            cc.setFilter(list, filterName);
        }
        gridEpg.filterChanged(list, filterName);
//        if (state == EpgService.STATE_NONE && monitorState != MonitorService.FULL_SCREEN_APP_STATE
//                || state == EpgService.STATE_PIP) {
//            if (checkPermission(App.NAME)) {
//                if (!miniEpg.isActive()) {
//                    miniEpg.start();
//                }
//            }
//            ChannelZapper.getInstance().showChannelInfo(channelController.getCurrent());
//        }
        EpgVbmController.getInstance().writeSelectedChannelFilter(filterName);
        if (l != null) {
            l.notifyEpgEvent(FILTER_CHANGED, filterName);
        }
        //->Kenneth[2016.7.8] R7.2 sticky filter
        Log.printDebug(App.LOG_HEADER+"EpgCore.applyFilter() : Call UpManager.set(LAST_CHANNEL_FILTER, "+filterName+")");
        UpManager.getInstance().set(UpManager.LAST_CHANNEL_FILTER, filterName);
        //<-
        return list;
    }

    //->Kenneth[2016.1.21] DDC-115
    // 이렇게 새로운 함수를 만드는 이유는
    // GridEpg 에서 option popup 이 Grid 위에 같이 떴다가 Grid 와 같이 사라지기 때문임.
    // 따라서 EPG 가 떠 있는 경우에는 일정 시간 흐른후에 showAudioDescriptionOption 를 호출해서
    // Exit 으로 인해서 EPG 가 사라지는 것을 기다린 후 option popup 이 보이길 바라기 때문이다.
    public static int lastKeyPressedBeforeExit = -1;
    //->Kenneth[2017.3.27] R7.3 : VDTRMASTER-6048 : boolean 값을 return 하도록 함수형태를 바꾼다.
    public boolean notifyExitKeyPressed(final int keyCode) {
    //public void notifyExitKeyPressed(final int keyCode) {
    //<-
        if (lastKeyPressedBeforeExit != KeyCodes.STAR) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : Previous Key is not STAR, so return.");
            return false;
        //-> Kenneth[2016.1.22] VDTRMASTER-5760 : STAR 키 이후에 다른 키가 오는 경우  
        // 그 키가 EXIT 이어서 팝업이 뜨던 안 뜨던 어떤 키던지 오면 STAR 키는 무효화 되어야 한다.
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : Invalidate STAR key. lastKeyPressedBeforeExit set to -1");
            lastKeyPressedBeforeExit = -1;
        //<-
        }
        try {
            if (monitorService != null && monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
                String activeApp = monitorService.getCurrentActivatedApplicationName();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : activeApp = "+activeApp);
                if (activeApp.equals("EPG")) {
                    // EPG 가 사라질 시간을 준 후에 호출하도록 하자.
                    new Thread() {
                        public void run() {
                            try {
                                Thread.sleep(800);
                            } catch (Exception e) {
                                Log.print(e);
                            }
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : Call showAudioDescriptionOption(EXIT) in THREAD.");
                            showAudioDescriptionOption(keyCode);
                        }
                    }.start();
                }
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : Call showAudioDescriptionOption(EXIT).");
                showAudioDescriptionOption(keyCode);
                //->Kenneth[2017.3.27] R7.3 : VDTRMASTER-6048 : special video 상태면 exit 을 consume 한다.
                if ("PVR".equals(getVideoContextName())) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyExitKeyPressed() : videoContext = PVR : So return true.");
                    return true;
                }
                //<-
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //->Kenneth[2017.3.27] R7.3 : VDTRMASTER-6048 : boolean 값을 return 하도록 함수형태를 바꾼다.
        return false;
        //<-
    }
    //<-

    //->Kenneth[2016.1.21] DDC-115
    // * 와 Exit 키가 순서대로 오는 경우만 option popup 을 띄운다.
    // * 이후에 Exit 이 3초 이내로 오는 경우만 동작해야 한다.
    long lastStarPressedTime = System.currentTimeMillis();
    long starTimeout = 3000L;
    public void showAudioDescriptionOption(int keyCode) {
        if (keyCode == KeyCodes.STAR) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption(STAR). Reset lastStarPressedTime");
            lastStarPressedTime = System.currentTimeMillis();
        } else if (keyCode == OCRcEvent.VK_EXIT) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption(EXIT)");
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : monitorState = "+monitorState);
        try {
            if (monitorService != null && monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
                String activeApp = monitorService.getCurrentActivatedApplicationName();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : activeApp = "+activeApp);
                if (!activeApp.equals("EPG")) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Do nothing.");
                    return;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        // 아래의 optionScreen 은 AV 상태에서의 option screen 임을 주의. EPG 의 각 화면별로 option screen 은 해당 클래스에서
        // 관리됨.
        boolean dOptionVisible = optionScreen.isStarted();
        //->Kenneth[2016.7.15] R7.2 : zoomOption 이 새로 생겨서 그에 대한 visible 체크도 해야함
        if (zoomOption != null && zoomOption.isStarted()) {
            dOptionVisible = true;
        }
        //<-
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : dOptionVisible = "+dOptionVisible);
        if (dOptionVisible) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Do nothing.");
            return;
        }
        if (audioDescriptionOption == null || !audioDescriptionOption.isStarted()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : AudioDescription Option is invisible now.");
            if (keyCode != OCRcEvent.VK_EXIT) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Not EXIT Key, so return.");
                return;
            }
            long gap = System.currentTimeMillis() - lastStarPressedTime;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : gap = "+gap);
            if (gap > starTimeout) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Timeout! So return.");
                return;
            }
            // option screen 을 보여준다.
            String currentSetting = (String)DataCenter.getInstance().get(PreferenceNames.DESCRIBED_AUDIO_DISPLAY);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : currentSetting = "+currentSetting);
            if (Definitions.OPTION_VALUE_ON.equals(currentSetting)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call UpManager.set(OFF)");
                UpManager.getInstance().set(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_OFF);
                Rs.MENU_AUDIO_DESC_OFF.setChecked(true);
                Rs.MENU_AUDIO_DESC_ON.setChecked(false);
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call UpManager.set(ON)");
                UpManager.getInstance().set(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_ON);
                Rs.MENU_AUDIO_DESC_ON.setChecked(true);
                Rs.MENU_AUDIO_DESC_OFF.setChecked(false);
            }
            audioDescriptionOption = new OptionScreen();
            audioDescriptionOption.setAutoCloseDelay(DataCenter.getInstance().getLong("WATCH_D_OPTION_HIDE_DELAY", 5000L));
            if (monitorState == MonitorService.TV_VIEWING_STATE || monitorState == MonitorService.PIP_STATE) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : setBackground(null)");
                audioDescriptionOption.setBackground(null);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call start()");
            audioDescriptionOption.start(Rs.MENU_AUDIO_DESC, null, this, this);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : AudioDescription Option is visible now.");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call stop()");
            audioDescriptionOption.stop();
            audioDescriptionOption = null;
            // * 로 인해서 option popup 이 사라진 경우에는 *, EXIT 아닌 EXIT 만 눌러도 다시 팝업이 보이는 것을
            // 방지하기 위해서 lastStarPressedTime 을 Timeout 만큼 과거로 만들어 준다. 
            // 그러면 반드시 * 와 EXIT 을 모두 다시 눌러야만 팝업이 뜬다.
            if (keyCode == KeyCodes.STAR) {
                lastStarPressedTime = lastStarPressedTime - starTimeout;
            }
        }
    }
    /*
    //->Kenneth[2015.8.18] DDC-113
    public void showAudioDescriptionOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption()");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : monitorState = "+monitorState);
        try {
            if (monitorService != null && monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
                String activeApp = monitorService.getCurrentActivatedApplicationName();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : activeApp = "+activeApp);
                if (!activeApp.equals("EPG")) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Do nothing.");
                    return;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        // 아래의 optionScreen 은 AV 상태에서의 option screen 임을 주의. EPG 의 각 화면별로 option screen 은 해당 클래스에서
        // 관리됨.
        boolean dOptionVisible = optionScreen.isStarted();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : dOptionVisible = "+dOptionVisible);
        if (dOptionVisible) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Do nothing.");
            return;
        }
        if (audioDescriptionOption == null || !audioDescriptionOption.isStarted()) {
            // option screen 을 보여준다.
            String currentSetting = (String)DataCenter.getInstance().get(PreferenceNames.DESCRIBED_AUDIO_DISPLAY);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : currentSetting = "+currentSetting);
            if (Definitions.OPTION_VALUE_ON.equals(currentSetting)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call UpManager.set(OFF)");
                UpManager.getInstance().set(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_OFF);
                Rs.MENU_AUDIO_DESC_OFF.setChecked(true);
                Rs.MENU_AUDIO_DESC_ON.setChecked(false);
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call UpManager.set(ON)");
                UpManager.getInstance().set(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_ON);
                Rs.MENU_AUDIO_DESC_ON.setChecked(true);
                Rs.MENU_AUDIO_DESC_OFF.setChecked(false);
            }
            audioDescriptionOption = new OptionScreen();
            audioDescriptionOption.setAutoCloseDelay(DataCenter.getInstance().getLong("WATCH_D_OPTION_HIDE_DELAY", 5000L));
            if (monitorState == MonitorService.TV_VIEWING_STATE || monitorState == MonitorService.PIP_STATE) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : setBackground(null)");
                audioDescriptionOption.setBackground(null);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call start()");
            audioDescriptionOption.start(Rs.MENU_AUDIO_DESC, null, this, this);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showAudioDescriptionOption() : Call stop()");
            audioDescriptionOption.stop();
            audioDescriptionOption = null;
        }
    }
    //<-
    */

    //->Kenneth[2016.7.19] R7.2 : DebugScreen 에서 현재 Active 한 app 을 가져갈수 있게 함수추가
    public String getCurrentActivatedApplicationName() {
        String appName = "";
        try {
            appName = monitorService.getCurrentActivatedApplicationName();
        } catch (Exception e) {
            Log.print(e);
        }
        return appName;
    }
    //<-

    // watch tv option
    private void showOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showOption()");
        Channel ch = channelController.getCurrent();
        Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
        MenuItem root = new MenuItem("WATCH_TV");
        if (p != null && p instanceof PpvProgram) {
            if (channelController.getState() == ChannelController.CLEAR) {
                root.add(Rs.MENU_CART);
            }
            root.add(Rs.MENU_HOW_TO_ORDER_OPTION);
            root.add(Rs.MENU_TUNE_TO_TRAILER);
            root.add(Rs.MENU_NEW_THIS_MONTH);
        }

        root.add(Rs.getRecordMenu(p));  // may be null
        //->Kenneth[2017.2.27] R7.3 : TECH 채널의 경우 favorite/block 채널에 포함될 수 없음
        if (ch.getType() != Channel.TYPE_TECH) {
            if (ch.isFavorite()) {
                root.add(Rs.MENU_REMOVE_FAV_CHANNEL);
            } else if (ch.isAuthorized()) {
                root.add(Rs.MENU_ADD_FAV_CHANNEL);
            }
        }
        //<-
//        root.add(ch.isFavorite() ? Rs.MENU_REMOVE_FAV_CHANNEL : Rs.MENU_ADD_FAV_CHANNEL);
        switch (ch.getType()) {
            case Channel.TYPE_RADIO:
            case Channel.TYPE_GALAXIE:
            case Channel.TYPE_VIRTUAL:
                break;
            default:
                root.add(Rs.MENU_PICTURE);
                break;
        }
        //->Kenneth[2017.2.27] R7.3 : TECH 채널의 경우 favorite/block 채널에 포함될 수 없음
        if (ch.getType() != Channel.TYPE_TECH) {
            root.add(ch.isBlocked() ? Rs.MENU_UNBLOCK_CHANNEL : Rs.MENU_BLOCK_CHANNEL);
        }
        //<-

        //->Kenneth[2017.3.16] R7.3 : D-option 에 search 추가됨
        root.add(Rs.MENU_SEARCH);
        //<-

        if (Environment.SUPPORT_PIP) {
            if (state == EpgService.STATE_PIP) {
                root.add(Rs.MENU_CLOSE_PIP);
            } else {
                root.add(Rs.MENU_LAUNCH_PIP);
            }
        }
        root.add(Rs.getMoreOptionsMenu(ch));
        root.add(Rs.MENU_GO_TO_HELP);
        root.add(Rs.getParentalControlMenu());
        optionScreen.start(root, null, this, this);
    }

    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected("+item+")");
        // TODO - ch, program이 추후에 바뀔수도 있음
        Channel ch = channelController.getCurrent();
        Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
        if (item instanceof UpMenuItem) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call UpMenuItem.selected()");
            ((UpMenuItem) item).selected();
            return;
        }
        if (item == Rs.MENU_RECORD || item == Rs.MENU_RECORD_PROGRAM) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call record()");
            record(ch, p);
        } else if (item == Rs.MENU_STOP_RECORDING || item == Rs.MENU_CANCEL_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call cancelRecord()");
            cancelRecord(p);
        } else if (item == Rs.MENU_ADD_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call addFavoriteChannel()");
            //->Kenneth[2016.3.9] R7 : VBM 로그 
            addFavoriteChannel(ch, this, false);
            //addFavoriteChannel(ch, this);
            //<-
        } else if (item == Rs.MENU_REMOVE_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call removeFavoriteChannel()");
            removeFavoriteChannel(ch, this);
        } else if (item == Rs.MENU_BLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call addBlockChannel()");
            addBlockedChannel(ch, this);
        } else if (item == Rs.MENU_UNBLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call removeBlockChannel()");
            removeBlockedChannel(ch, this);
        } else if (item.getKey().startsWith("filter.")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call applyFilter()");
            ChannelList list = applyFilter(item.getKey(), this);
        } else if (item == Rs.MENU_LAUNCH_PIP) {
            if (pipController != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call launchPip()");
                launchPip();
            }
        } else if (item == Rs.MENU_CLOSE_PIP) {
            if (pipController != null) {
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call exitToChannel()");
                    CommunicationManager.getMonitorService().exitToChannel();
                } catch (Exception ex) {
                    Log.print(ex);
                    pipController.stop();
                }
            }
        } else if (item.getKey().startsWith("menu.pc_")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call showParentalControlSettingPopup()");
            showParentalControlSettingPopup(item, "TV");
        } else if (item == Rs.MENU_GO_TO_PREFERENCE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call goToPreference(null)");
            goToPreference(null);
        } else if (item == Rs.MENU_GO_TO_HELP) {
//        	if ("VOD".equals(videoContext)) {
//        		VODService vod = (VODService) dataCenter.get(VODService.IXC_NAME);
//        		try {
//        			vod.gotoHelp();
//        		} catch (Exception e) {
//        			Log.print(e);
//        		}
//        	} else if ("PVR".equals(videoContext)) {
//        		startUnboundApp("Help", new String[] { MonitorService.REQUEST_APPLICATION_LAST_KEY, "PVR" , null} );
//            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call startUnboundApp(Help)");
        		startUnboundApp("Help", new String[] { MonitorService.REQUEST_APPLICATION_HOT_KEY, "TV", null} );
//        	}
        } else if (item == Rs.MENU_CART) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call showCart()");
            PpvController.getInstance().showCart();
        } else if (item == Rs.MENU_HOW_TO_ORDER_OPTION) {
            if (p instanceof PpvProgram) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call PpvController.tuneToChannel(HOW_TO_ORDER)");
                PpvController.getInstance().tuneToChannel((PpvProgram) p, PpvController.CHANNEL_HOW_TO_ORDER);
            }
        } else if (item == Rs.MENU_TUNE_TO_TRAILER) {
            if (p instanceof PpvProgram) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call PpvController.tuneToChannel(TRAILER)");
                PpvController.getInstance().tuneToChannel((PpvProgram) p, PpvController.CHANNEL_TRAILER);
            }
        } else if (item == Rs.MENU_NEW_THIS_MONTH) {
            if (p instanceof PpvProgram) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : Call PpvController.tuneToChannel(NEW_THIS_MONTH)");
                PpvController.getInstance().tuneToChannel((PpvProgram) p, PpvController.CHANNEL_NEW_THIS_MONTH);
            }
        //->Kenneth[2017.3.16] R7.3 : D-option 에 search 추가
        // 얘가 불리는 경우는 Live 상태에서 D-option 으로 접근한 경우이다.
        // Grid, Mini EPG 의 경우는 각각의 selected 에서 처리하도록 했음
        } else if (item == Rs.MENU_SEARCH) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.selected() : SEARCH");
            launchSearch(SearchService.PLATFORM_EPG, true, "D_LIVE");
        //<-
        }
    }

    public void canceled() {
    }

    public void opened(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.opened("+item+")");
        if (item instanceof CheckableMenu) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.opened() : Call CheckableMenu.resetCheckedChildren()");
            ((CheckableMenu) item).resetCheckedChildren();
        }
    }

    public void showParentalControlSettingPopup(final MenuItem item, final String appName) {
        final String key;
        if (item == Rs.MENU_PC_SUSPEND) {
            key = "epg.suspend_pin";
        } else if (item == Rs.MENU_PC_ACTIVATE) {
            key = "epg.desc_go_pref";
        } else if (item == Rs.MENU_PC_ENABLE) {
            key = "epg.restore_pin";
        } else {
            return;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showParentalControlSettingPopup("+key+")");
        RightFilterListener rfl = new RightFilterListener() {
            String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString(key), '|');
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    changeParentalControlSetting(item, appName);
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.PARENTAL_CONTROL }, null, null);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void changeParentalControlSetting(MenuItem item, String appName) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.changeParentalControlSetting("+item+")");
        DataCenter dc = DataCenter.getInstance();
        if (item == Rs.MENU_PC_ACTIVATE) {
            try {
                startUnboundApp("SETTINGS", new String[] {
                                    appName, PreferenceService.PARENTAL_CONTROLS } );
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else if (item == Rs.MENU_PC_SUSPEND) {
            String curSusDefTime = dc.getString(RightFilter.SUSPEND_DEFAULT_TIME);
            boolean result = false;
            if (Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS.equals(curSusDefTime)
                    || Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS.equals(curSusDefTime)) {
                result = UpManager.getInstance().set(RightFilter.PARENTAL_CONTROL, curSusDefTime);
            }
        } else if (item == Rs.MENU_PC_ENABLE) {
            UpManager.getInstance().set(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
        }
    }

    ////////////////////////////////////////////////////////////////////////
    // Start App
    ////////////////////////////////////////////////////////////////////////

    private void goToPreferenceImpl(String appName) {
        startUnboundApp("SETTINGS", new String[] {
                                    appName, PreferenceService.PROGRAM_GUIDE } );
    }

    public void launchPip() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.launchPip()");
        startUnboundApp("PIP", null);
    }

    public void launchSearch(String platform, boolean reset) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.launchSearch("+platform+", "+reset+")");
        try {
            SearchService ss = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
            if (ss != null) {
                ss.launchSearchApplication(platform, reset);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    //->Kenneth[2017.3.22] R7.3 : D-Option 에서의 Search 를 위해 추가된 함수
    public void launchSearch(String platform, boolean reset, String param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.launchSearch("+platform+", "+reset+", "+param+")");
        try {
            SearchService ss = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
            if (ss != null) {
                ss.launchSearchApplicationWithParam(platform, reset, param);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }
    //<-

    public void stopSearch() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.stopSearch()");
        try {
            SearchService ss = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
            if (ss != null) {
                ss.stopSearchApplication();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void startUnboundAppWithPin(final String appName, final String[] param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.startUnboundAppWithPin("+appName+")");
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);

        PinEnablerListener l = new PinEnablerListener() {
            public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    startUnboundApp(appName, param);
                }
            }
        };

        String[] msg = new String[]{
            DataCenter.getInstance().getString("epg.admin_pin1"),
            DataCenter.getInstance().getString("epg.admin_pin2")
        };

        try {
            ps.showPinEnabler(l, msg);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void startUnboundApp(String appName, String[] param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.startUnboundApp("+appName+")");
        try {
            monitorService.startUnboundApplication(appName, param);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    class VideoContextAdapter implements VideoContextListener {

        public void videoModeChanged(String videoName) throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.VideoContextAdapter.videoContextListener("+videoName+")");
            videoContext = videoName;
            epgVideoContext = videoName == null || videoName.equals(App.NAME);
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
    	public void logLevelChanged(int logLevel) throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.LogLevelAdapter.logLevelChanged("+logLevel+")");
            Log.setStcLevel(logLevel);
        }
    }

    class ScreenSaverAdapter implements ScreenSaverConfirmationListener {
        public boolean confirmScreenSaver() throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ScreenSaverAdapter.confirmScreenSaver()");
            if (state == EpgService.STATE_GUIDE) {
                FrameworkMain.getInstance().printSimpleLog("confirmScreenSaver = STATE_GUIDE");
                return true;
            }
            boolean watching = false;
            if (!ChannelBackground.getInstance().isRunning()) {
                // A/V 가 main 에 나오고 있는 경우
                if (!Environment.SUPPORT_DVR) {
                    return false;
                }
                watching = !ChannelController.get(0).isPaused();
                if (!watching) {
                    FrameworkMain.getInstance().printSimpleLog("confirmScreenSaver = ChannelController is paused");
                }
            } else {
                FrameworkMain.getInstance().printSimpleLog("confirmScreenSaver = running : " + ChannelBackground.getInstance().getCurrentPanel());
            }
            if (pipController != null) {
                watching = watching || pipController.isWatching();
            }
            return !watching;
        }
    }

    class ReadyChecker implements DataUpdateListener {
        private Vector vector = new Vector();

        public synchronized void add(String key) {
            vector.addElement(key);
            dataCenter.addDataUpdateListener(key, ReadyChecker.this);
        }

        synchronized void check(String key) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ReadyChecker.check("+key+")");
            vector.removeElement(key);
            dataCenter.removeDataUpdateListener(key, ReadyChecker.this);
            if (Log.DEBUG_ON) {
                Log.printDebug("EpgCore.ReadyChecker.size = " + vector.size());
            }
            if (vector.size() == 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ReadyChecker.check() : Call readyEpg()");
                readyEpg();
            }
        }

        public void dataUpdated(String key, Object old, Object value) {
            check(key);
        }

        public void dataRemoved(String key) {
        }

        public String toString() {
            if (vector.isEmpty()) {
                return "EPG is ready to start.";
            } else {
                return "EPG is waiting for ... : " + vector;
            }
        }
    }

    public boolean checkPermission(String appName) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.checkPermission("+appName+")");
        try {
            return monitorService.checkAppAuthorization(appName) != MonitorService.NOT_AUTHORIZED;
        } catch (Exception ex) {
        }
        return true;
    }

    public String getVideoContextName() {
        return videoContext;
    }

    public void resetScreenSaverTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.resetScreenSaverTimer()");
        try {
            monitorService.resetScreenSaverTimer();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public boolean showSubscriptionPopup(Channel channel, String entryPoint) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.showSubscriptionPopup()");
        try {
            ISAService is = (ISAService) dataCenter.get(ISAService.IXC_NAME);
            is.showSubscriptionPopup(channel.getCallLetter(), entryPoint);
            return true;
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }


    class STBModeChangeAdapter implements STBModeChangeListener {
        public void modeChanged(int newMode) throws RemoteException {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"EpgCore.STBModeChangeAdapter.modeChanged = " + newMode);
            }
            switch (newMode) {
                case MonitorService.STB_MODE_NORMAL:
                    activeStbMode = true;
                    navigableStbMode = true;
                    ChannelZapper.getInstance().start();
                    enableEvents(true);
                    if (Host.getInstance().getPowerMode() != Host.FULL_POWER) {
                        if (Definitions.DISPLAY_POWER_OFF_TIME.equals(dataCenter.get(PreferenceNames.DISPLAY_POWER_OFF))) {
                            FrontPanelDisplay.getInstance().showClock();
                        } else {
                            FrontPanelDisplay.getInstance().clear();
                        }
                    }
                    break;
                //R7.4 sykim VDTRMASTER-6146    
                case MonitorService.STB_MODE_STANDALONE:
                	navigableStbMode = false;
                	disableStbExceptEvent();
                    FrontPanelDisplay.getInstance().clear();
                    break;
                case MonitorService.STB_MODE_NO_SIGNAL:
                    navigableStbMode = false;
                    disableStb();
                    FrontPanelDisplay.getInstance().clear();
                    break;
                case MonitorService.STB_MODE_MANDATORY_BLOCKED:
                    isBlockedMode = true;
                    navigableStbMode = false;
                    disableStb();
                    FrontPanelDisplay.getInstance().showText("----");
                    break;
                default:
                    disableStb();
                    break;
            }
            navigableStbMode = activeStbMode || newMode == MonitorService.STB_MODE_BOOTING;
        }

        private void disableStb() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.STBModeChangeAdapter.disableStb()");
            activeStbMode = false;
            ChannelZapper.getInstance().stop();
            enableEvents(false);
        }
        
        //R7.4 sykim VDTRMASTER-6146
        private void disableStbExceptEvent() {
        	if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.STBModeChangeAdapter.disableStbExceptEvent()");
            activeStbMode = false;
            ChannelZapper.getInstance().stop();
            enableEvents(true);
        }
    }

    class EIDMappingTableUpdateAdapter implements EIDMappingTableUpdateListener, Runnable {

        String sdvPackage = DataCenter.getInstance().getString("SDV_PACKAGE_NAME");

        public synchronized void tableUpdated() throws RemoteException {
            String dncs = monitorService.getDncsName();
            if (dncs != null) {
                DataCenter.getInstance().put("DNCS_NAME", dncs);
                EpgCore.dncsName = dncs;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"EpgCore.EIDMappingTableUpdateAdapter.tableUpdated");
                Log.printDebug(App.LOG_HEADER+"dncsName = " + dncsName);
            }

            String eid = monitorService.getEidAtPackageNameEidMappingFile(sdvPackage);
            SdvDiagnostics.set(SdvDiagnostics.PACKAGE, sdvPackage + ", " + eid);
            checkSdvPackage();
        }

        public void checkSdvPackage()  {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.EIDMappingTableUpdateAdapter.checkSdvPackage() : Call t.run()");
            Thread t = new Thread(EIDMappingTableUpdateAdapter.this);
            t.start();
        }

        public void run() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.EIDMappingTableUpdateAdapter.run()");
            try {
                int result = monitorService.checkResourceAuthorization(sdvPackage);
                int reason;
                switch (result) {
                    case MonitorService.NOT_FOUND_ENTITLEMENT_ID:
                        reason = SDVController.EID_NOT_FOUND;
                        break;
                    case MonitorService.NOT_AUTHORIZED:
                        reason = SDVController.NOT_AUTHORIZED;
                        break;
                    default:
                        reason = SDVController.AUTHORIZED;
                        break;
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.EIDMappingTableUpdateAdapter.run() : call SDVController.setPackage("+reason+")");
                SDVController.getInstance().setPackage(reason);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void checkSdvPackage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.checkSdvPackage()");
        eidListener.checkSdvPackage();
    }

    public boolean supportDvr() {
        return Environment.SUPPORT_DVR && checkPermission(PVR_APP_NAME);
    }

    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyEpgEvent("+id+")");
        switch (id) {
            case ADDED_TO_FAVOURITES:
            case REMOVED_FROM_FAVOURITES:
            case ADDED_TO_RESTRICTED:
            case REMOVED_FROM_RESTRICTED:
            case FILTER_WILL_BE_CHANGED:
            case FILTER_CHANGED:
                ChannelZapper zapper = ChannelZapper.getInstance();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyEpgEvent() : Call ChannelZapper.showCurrentInfo()");
                zapper.showCurrentInfo();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.notifyEpgEvent() : Call ChannelZapper.getChannelBanner.notifyEpgEvent()");
                zapper.getChannelBanner().notifyEpgEvent(id, data);
                break;
        }
    }

    class WatchTvOptionScreen extends OptionScreen implements EpgChannelEventListener {

        public synchronized void start(MenuItem root, Point point, MenuListener l, MenuOpenListener ol) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.start()");
            channelController.addChannelEventListener(this);
            super.start(root, point, l, ol);
        }

        public synchronized void stop() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.stop()");
            channelController.removeChannelEventListener(this);
            super.stop();
        }

        public void selectionRequested(Channel ch, short type, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.selectionRequested("+ch+")");
            stop();
        }

        public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.selectionBlocked("+ch+")");
            stop();
        }

        public void serviceStopped(Channel ch) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.serviceStopped("+ch+")");
            stop();
        }

        public void showChannelInfo(Channel ch) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.showChannelInfo("+ch+")");
        }

        public void programUpdated(Channel ch, Program p) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.WatchTvOptionScreen.programUpdated("+ch+")");
        }

    }

    class MonitorCAAuthorizationAdapter implements MonitorCAAuthorizationListener {

        public void deliverEvent(long targetId, int targetType, boolean isAuthorized, String packageId) throws RemoteException {
            Log.printDebug(App.LOG_HEADER+"EpgCore.MonitorCAAuthorizationListener: " + packageId + ", " + isAuthorized);
            if (eidListener.sdvPackage.equals(packageId)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.MonitorCAAuthorizationAdapter.deliverEvent() : Call SDVController.packageUpdated()");
                SDVController.getInstance().packageUpdated(isAuthorized);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.MonitorCAAuthorizationAdapter.deliverEvent() : Call CaUpdateScreen.put()");
            CaUpdateScreen.put(packageId, targetId & 0xFFFFFFFF, isAuthorized);
        }
    }

    class ISAServiceDataAdapter implements ISAServiceDataListener {
        HashSet channels = new HashSet();
        HashSet previews = new HashSet();

	    public synchronized void updateISAServiceList(String isaServiceType, String[] serviceList) throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ISAServiceDataListener.updateISAServiceList("+isaServiceType+")");
            if (ISAService.ISA_SERVICE_TYPE_CHANNEL.equals(isaServiceType)) {
                channels = getServiceSet(serviceList);
                merge();
            } else if (ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW.equals(isaServiceType)) {
                previews = getServiceSet(serviceList);
                merge();
            }
        }

        //->Kenneth[2016.8.17] CYO Bundling/A la carte
        public synchronized void updateISAExcludedChannelsAndTypes(String[] excludedChannels, int[] excludedTypes) throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ISAServiceDataListener.updateISAExcludedChannelsAndTypes("+excludedChannels+", "+excludedTypes+")");
            if (excludedChannels != null) {
                for (int i = 0 ; i < excludedChannels.length ; i++) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"excludedChannels("+i+") = "+excludedChannels[i]);
                }
            }
            if (excludedTypes!= null) {
                for (int i = 0 ; i < excludedTypes.length ; i++) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"excludedTypes("+i+") = "+excludedTypes[i]);
                }
            }
            ConfigManager.getInstance().isaExcludedChannels = getServiceSet(excludedChannels);
            ConfigManager.getInstance().isaExcludedTypes = excludedTypes;
        }
        //<-

        protected HashSet getServiceSet(String[] serviceList) {
            HashSet set = new HashSet();
            if (serviceList != null) {
                for (int i = 0; i < serviceList.length; i++) {
                    set.add(serviceList[i]);
                }
            }
            return set;
        }

        private void merge() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgCore.ISAServiceDataListener.merge()");
            HashSet merged = new HashSet();
            merged.addAll(channels);
            merged.addAll(previews);
            ConfigManager.getInstance().isaChannels = merged;
        }

    }

}

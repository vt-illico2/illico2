package com.alticast.illico.epg.menu;

import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.ui.*;
import java.util.*;

public class SortMenu extends CheckableMenu {

    /** Create menu item with specified key and children. */
    public SortMenu(String key, MenuItem[] children) {
        super(key, children);
    }

    public void resetCheckedChildren() {
        ChannelComparator comparator = GridEpg.comparator;

        Enumeration en = items.elements();
        while (en.hasMoreElements()) {
            MenuItem item = (MenuItem) en.nextElement();
            boolean check;
            if (item == Rs.MENU_SORT_BY_NUMBER) {
                check = comparator == ChannelList.ASCENDING_BY_NUMBER;
            } else if (item == Rs.MENU_SORT_BY_NAME) {
                check = comparator == ChannelList.ASCENDING_BY_NAME;
            } else {
                check = false;
            }
            item.setChecked(check);
        }
    }

}


package com.alticast.illico.epg.menu;

import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.alticast.illico.epg.*;
import java.util.*;

public class UpMenu extends CheckableMenu {

    String upKey;

    public UpMenu(String key, String upKey, MenuItem[] children) {
        super(key, children);
        this.upKey = upKey;
    }

    public void resetCheckedChildren() {
        Object currentSetting = DataCenter.getInstance().get(upKey);
        if (Log.DEBUG_ON) {
            Log.printDebug("UpMenu.resetCheckedChildren: " + upKey + ", " + currentSetting);
        }

        Enumeration en = items.elements();
        while (en.hasMoreElements()) {
            Object item = en.nextElement();
            if (item instanceof UpMenuItem) {
                ((UpMenuItem) item).recheck(currentSetting);
            } else {
                ((MenuItem) item).setChecked(false);
            }
        }
    }

}


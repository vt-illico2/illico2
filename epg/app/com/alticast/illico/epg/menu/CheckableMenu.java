package com.alticast.illico.epg.menu;

import com.videotron.tvi.illico.ui.*;

public class CheckableMenu extends MenuItem {

    /** Create menu item with specified key and children. */
    public CheckableMenu(String key, MenuItem[] children) {
        super(key, children);
    }

    public void resetCheckedChildren() {
    }

}


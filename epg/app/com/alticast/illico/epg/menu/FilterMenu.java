package com.alticast.illico.epg.menu;

import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.navigator.*;
import java.util.*;

public class FilterMenu extends CheckableMenu {

    public FilterMenu(String key, MenuItem[] children) {
        super(key, children);
    }

    public void resetCheckedChildren() {
        String currentFilter = ChannelController.get(0).getCurrentFilterKey();
        if (Log.DEBUG_ON) {
            Log.printDebug("FilterMenu.resetCheckedChildren: " + currentFilter);
        }
        Enumeration en = items.elements();
        while (en.hasMoreElements()) {
            MenuItem item = (MenuItem) en.nextElement();
            item.setChecked(item.getKey().equals(currentFilter));
        }
    }

}


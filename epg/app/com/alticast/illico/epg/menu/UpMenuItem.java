package com.alticast.illico.epg.menu;

import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.*;

public class UpMenuItem extends MenuItem {

    String upKey;
    String upValue;

    public UpMenuItem(String key, String upKey, String upValue) {
        super(key);
        this.upKey = upKey;
        this.upValue = upValue;
    }
    
    //->Kenneth[2017.2.22] R7.3 : Highlight 를 Option 에서 처리
    public String getValue() {
        return upValue;
    }
    //<-

    public void selected() {
        UpManager.getInstance().set(upKey, upValue);
    }

    void recheck(Object currentSetting) {
        setChecked(upValue.equals(currentSetting));
    }

}


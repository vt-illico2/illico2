package com.alticast.illico.epg;

import java.util.*;
import com.opencable.handler.cahandler.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.ppv.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.util.*;
import org.ocap.hardware.pod.POD;

/**
 * Cablecard CANH와 통신하는 class.
 *
 */
public class CaManager implements CAAuthorizationListener {

    /**
     * EPG가 준비되기 전에 check를 허용할지 여부.
     * 부팅 초반에 CANH가 잘못된 결과를 주는 경우가 있어서 false로 사용중.
     */
    private static final boolean ENALBE_CHECK_BEFORE_EPG_READY = false;

    /** CANH 통신할 때 EID에 특정 bit을 enable 시켜야 한다. */
    public static final long EID_PREFIX = 0x0000000100000000L;

    /** Check API에 listener를 넣으면 추후의 변경을 알려주고 아니면 일회성 조회. */
    public static final boolean USE_LISTENER = true;

    // 상태 확인을 위한 fields
    public static int eventCount = USE_LISTENER ? 0 : -1;
    public static int checkCount = 0;
    public static int authCount = 0;
    public static int notAuthCount = 0;
    public static int failedCount = 0;

    CAHandler handler;

    /** CAHandler.getState() 참조. */
    int state = -1;
    /** CaManager사용이 준비 되었는지 여부. */
    boolean ready = false;
    /** 부팅 초반 전체 채널에 대한 check를 수행하는데 완료되었는지 여부. */
    boolean allChecked = false;
    /** 특수한 환경에서 CANH없이 테스트 하고자 하는 경우 사용되는 state. */
    public static final int CANH_BYPASS_MODE = -1818;

    /**
     * CANH가 잘못된 결과를 알려줘서 모든 또는 일부 채널이 미가입으로 알려주는 문제가 가끔 있다.
     * 이 경우 이 flag가 set 되며, 모든 채널을 zapping 하도록 처리.
     */
    public static boolean INVALID_CANH = false;

    /** For test. */
    public static final boolean CA_EMULATION = false;

    String apiVersion = "";
    String implVersion = "";

    /**
     * source ID 별 권한 result 를 저장.
     * key = Integer, value = Boolean
     */
    static Hashtable validMap = new Hashtable();

    /** 전체 full update를 위한 worker. */
    Worker checkWorker = new Worker("CaManager.CheckWorker", false);

    /** CaUpdateListener. */
    private Vector listeners = new Vector();

    private static CaManager instance = new CaManager();

    public static CaManager getInstance() {
        return instance;
    }

    private CAHandlerStateListener stateListener = new CAHandlerStateAdapter();

    public CaManager() {
        SharedMemory.getInstance().put(EpgService.DATA_KEY_CA_MAP, validMap);
    }

    public void init(CAHandler handler) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.init()"); 
        this.handler = handler;
        try {
            if (DataCenter.getInstance().getBoolean("CANH_BYPASS_MODE")) {
                setState(CANH_BYPASS_MODE);
            } else {
                handler.addStateListener(stateListener);
                synchronized (stateListener) {
                    setState(handler.getState());
                }
            }
            apiVersion = handler.getAPIVersion();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.init : apiVersion = "+apiVersion); 
            implVersion = handler.getImplementationVersion();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.init : implVersion = "+implVersion); 
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (App.EMULATOR) {
            validMap.put(new Integer(-7), Boolean.TRUE);
            validMap.put(new Integer(-14), Boolean.FALSE);
            validMap.put(new Integer(-28), Boolean.FALSE);
        }
    }

    /** Inband data를 받고 나서 이 API를 호출. */
    public void readyToCheck() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.readyToCheck()");
        if (!ENALBE_CHECK_BEFORE_EPG_READY) {
            if (!ready) {
                ready = true;
                checkAllChannels(true);
                PreviewChannels.recheckAuth();
            }
        }
        // ENALBE_CHECK_BEFORE_EPG_READY 일 때는, ChannelDB가 생기면 check 될 거다.
    }

    /** 체크 했는데 true 인지 체크. */
    public static boolean isSubscribed(Channel ch) {
        Boolean b = (Boolean) validMap.get(ch.sourceIdObject);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.isSubscribed("+ch+")");
        if (b != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.isSubscribed() returns "+b.booleanValue());
        }
        return b != null && b.booleanValue();
    }

    /** 체크 했는데 false 인지 체크. */
    public static boolean isUnsubscribed(Channel ch) {
        Boolean b = (Boolean) validMap.get(ch.sourceIdObject);
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"CaManager.isUnsubscribed("+ch+")");
        if (b != null) {
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"CaManager.isUnsubscribed() returns "+!b.booleanValue());
        }
        return b != null && !b.booleanValue();
    }

    /** Check 안했으면 check. */
    public boolean checkIfNeeded(Channel ch) {
        Integer sid = ch.sourceIdObject;
        Boolean b = (Boolean) validMap.get(sid);
        boolean auth;
        if (b == null) {
            auth = check(sid.intValue());
        } else {
            auth = b.booleanValue();
        }
        return auth;
    }

    public boolean check(Channel ch) {
        return check(ch.getSourceId());
    }

    /** CANH 통신하여 check 하고 valid map에 저장. */
    public boolean check(int sourceId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.check("+sourceId+")");
        if (state != CAHandler.CAH_READY) {
            if (Log.WARNING_ON) {
                Log.printWarning(App.LOG_HEADER+"CaManager.CAHandler in not ready. state = " + state);
            }
            return true;
        }
        if (sourceId <= 0) {
            return true;
        }
        long time = System.currentTimeMillis();
        checkCount++;
        try {
            boolean ret;
            if (CA_EMULATION) {
                if (Log.EXTRA_ON) {
                    ret = (sourceId/100) != 11 && (sourceId/100) != 12;
                } else {
                    ret = (sourceId/100) != 12;
                }
            } else {
                CAAuthorization ca;
                if (USE_LISTENER) {
                    ca = handler.getSourceAuthorization((short)sourceId, this);
                } else {
                    ca = handler.getSourceAuthorization((short)sourceId, null);
                }
                ret = ca.isAuthorized();
            }
            if (ret) {
                authCount++;
            } else {
                notAuthCount++;
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.check() calls updateMap()");
            updateMap(sourceId, ret);
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"CaManager.checkAuthorized : sid = " + sourceId + ", result = " + ret +
                                        ", time = " + (System.currentTimeMillis() - time));
            }
            return ret;
        } catch (Exception ex) {
            // 일반 채널은 default로 가입된 걸로 간주.
            failedCount++;
            Log.print(ex);
            return true;
        }
    }

    /**
     * 개별 PPV program을 구매하지 않고, PPV 채널 전체에 권한이 있는 경우.
     * 일반 고객 STB에서 이런 상황이 있지는 않겠지만, 개발용 박스는 가능.
     * 이 경우는 구매할 필요가 없으니 order 기능을 제공하지 않는다.
     */
    public boolean checkSubscriptionPurchased(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.checkSubscriptionPurchased("+ch+")");
        if (state != CAHandler.CAH_READY) {
            if (Log.WARNING_ON) {
                Log.printWarning(App.LOG_HEADER+"CaManager.CAHandler in not ready. state = " + state);
            }
            return false;
        }
        int sourceId = ch.getSourceId();
        try {
            CAAuthorization ca;
            if (USE_LISTENER) {
                ca = handler.getSourceAuthorization((short) sourceId, this);
            } else {
                ca = handler.getSourceAuthorization((short) sourceId, null);
            }
            boolean ret = ca.isSubscriptionPurchased();
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"CaManager.checkSubscriptionPurchased : sid = " + sourceId + ", result = " + ret);
            }
            return ret;
        } catch (Exception ex) {
            // PPV 이기 때문에 미구매/미가입 으로 간주.
            Log.print(ex);
            return false;
        }
    }

    /** Map을 update하고 변경이 있는 경우 listener로 알림. */
    private void updateMap(int sourceId, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.updateMap("+sourceId+", "+auth+")");
        Object old = validMap.put(new Integer(sourceId), auth ? Boolean.TRUE : Boolean.FALSE);
        if (!allChecked) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.updateMap() : !allChecked, so return.");
            return;
        }
        boolean changed = auth != (Boolean.FALSE != old);   // 최초 check 라서 old가 null 이면 true 취급
        if (!changed) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.updateMap() : !changed, so return.");
            return;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.updateMap() : Call ChannelController.notifyCaUpdated()");
        ChannelController.notifyCaUpdated(sourceId, auth);
        // 미가입 TECH 채널은 surfable channel list에서 제거하기 위해,
        // TECH 채널의 권한이 변경되면 channel db build를 다시 한다.
        // 2011-11-22, VT 요구사항.
        ChannelDatabase db = ChannelDatabase.current;
        if (db != null) {
            Channel ch = db.getChannelById(sourceId);
            if (ch != null) {
                if (ch.getType() == Channel.TYPE_TECH) {
                    Log.printDebug(App.LOG_HEADER+"CaManager.updateMap : tech ch = " + ch);
                    ChannelDatabaseBuilder.getInstance().buildLater(ChannelDatabaseBuilder.CHANGED_TECH_PERMISSION);
                }
            }
        }
    }

    private void setState(int newState) {
        this.state = newState;
    }

    /** fullUpdate 가 true이면 전체 강제 update, 아니면 check 안한 채널만. */
    public void checkAllChannels(boolean fullUpdate) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.checkAllChannels("+fullUpdate+")");
        if (!ENALBE_CHECK_BEFORE_EPG_READY && !ready) {
            Log.printDebug("CaManager.checkAllChannels: ready = " + ready);
            return;
        }
        if (fullUpdate) {
            checkWorker.clear();
        }
        checkWorker.push(new CheckAllThread(fullUpdate));
    }

    public CAAuthorization getResourceAuthorization(long eid, CAAuthorizationListener listener) throws Exception {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.getResourceAuthorization()");
        if (handler == null) {
            return null;
        }
        return handler.getResourceAuthorization(EID_PREFIX | eid, listener);
    }

/* 안쓰는 API
    public boolean checkResource(long eid, CAAuthorizationListener listener) {
        try {
            long id = EID_PREFIX | eid;
            CAAuthorization ret = handler.getResourceAuthorization(id, listener);
            Log.printDebug("CaManager.checkResource: " + id);
            return ret.isAuthorized();
        } catch (Exception ex) {
            Log.print(ex);
            return true;
        }
    }

    public boolean checkPpv(int sid, long eid) throws Exception {
        CAAuthorization auth = handler.getIppvAuthorization((short) sid, eid, this);
        boolean ret = auth.isIppvEventPurchased();
        if (Log.DEBUG_ON) {
            Log.printDebug("PpvController: isPurchased             = " + ret);
            Log.printDebug("PpvController: isAuthorized            = " + auth.isAuthorized());
            Log.printDebug("PpvController: isFreePreviewable       = " + auth.isFreePreviewable());
            Log.printDebug("PpvController: isSubscriptionPurchased = " + auth.isSubscriptionPurchased());
        }
        return ret;
    }
*/


    ////////////////////////////////////////////////////////////////////////
    // CAAuthorizationListener
    ////////////////////////////////////////////////////////////////////////

    /** 권한 변경시 호출 됨 */
    public void deliverEvent(CAAuthorizationEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.deliverEvent("+event+")");
        eventCount++;
        if (event == null) {
            return;
        }
        int type = event.getTargetType();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"CaManager.deliverEvent: targetType = " + type);
        }
        switch (type) {
        case CAAuthorization.SOURCE_TARGET: // channel
            {
            int sid = (int) event.getTargetId();
            boolean auth = event.isAuthorized();
            if (Log.DEBUG_ON) {
                Log.printDebug("CaManager.deliverEvent: targetId   = " + sid);
                Log.printDebug("CaManager.deliverEvent: authorized = " + auth);
            }
            FrameworkMain.getInstance().printSimpleLog("CaManager: targetId = " + sid + ", " + auth);
            updateMap(sid, auth);
            CaUpdateScreen.put(sid, auth);
            PpvController.getInstance().caUpdated(sid);
            notifyCaUpdated(sid, auth);
            }
            break;
//        case CAAuthorization.PPV_TARGET:
//            {
//            long eid = event.getTargetId() & ~EID_PREFIX;
//            boolean auth = event.isAuthorized();
//            if (Log.DEBUG_ON) {
//                Log.printDebug("CaManager.deliverEvent: targetId   = " + eid);
//                Log.printDebug("CaManager.deliverEvent: authorized = " + auth);
//            }
//            FrameworkMain.getInstance().printSimpleLog("CaManager: targetId = " + eid + ", " + auth);
//            CaUpdateScreen.put(CaUpdateScreen.PPV_EID, eid, auth);
//            PpvController.getInstance().caUpdated(eid, auth);
//            }
//            break;
        }
    }

    public void addListener(CaUpdateListener l) {
        synchronized (listeners) {
            if (!listeners.contains(l)) {
                listeners.addElement(l);
            }
        }
    }

    public void removeListener(CaUpdateListener l) {
        listeners.removeElement(l);
    }

    /** Channel 권한 변경시 listener 전달. */
    public void notifyCaUpdated(int sid, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.notifyCaUpdated()");
        synchronized (listeners) {
            Enumeration en = listeners.elements();
            while (en.hasMoreElements()) {
                try {
                    ((CaUpdateListener) en.nextElement()).caUpdated(sid, auth);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    public String toString() {
        return "CANH API version = " + apiVersion
             + ", Implementation version = " + implVersion
             + ", state = " + getStateString(state);
    }

    public static String getStateString(int state) {
        switch (state) {
            case CAHandler.CAH_INITIALIZING:
                return "CAH_INITIALIZING";
            case CAHandler.CAH_READY:
                return "CAH_READY";
            case CANH_BYPASS_MODE:
                return "CANH_BYPASS_MODE";
            default:
                return "UNKNOWN(" + state + ")";
        }
    }

    class CAHandlerStateAdapter implements CAHandlerStateListener {
        public synchronized void deliverEvent(CAHandlerStateEvent event) {
            if (Log.INFO_ON) {
                Log.printDebug(App.LOG_HEADER+"CaManager.CAHandlerStateEvent = " + event);
            }
            setState(event.getNewState());
        }
    }

    class CheckAllThread extends Thread {
        private boolean full;

        public CheckAllThread(boolean fullUpdate) {
            super("CaManager.CheckAllThread, " + fullUpdate);
            full = fullUpdate;
        }

        public void run() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.CheckAllThread.run()");
            ChannelDatabase db = ChannelDatabase.current;
            if (db == null) {
                return;
            }
            ChannelList list = db.getAllChannelList();
            int size = list.size();
            int count = 0;
            boolean changedTech = false;
            for (int i = 0; i < size; i++) {
                Channel ch = list.getChannelAt(i);
                int type = ch.getType();
                if (!(ch instanceof VirtualChannel) && type != Channel.TYPE_PPV) {
                    boolean old = !isUnsubscribed(ch);
                    boolean auth;
                    if (full) {
                        auth = check(ch);
                    } else {
                        auth = checkIfNeeded(ch);
                    }
                    if (auth != old && type == Channel.TYPE_TECH) {
                        changedTech = true;
                    }
                    if (!auth) {
                        count++;
                    }
                }
            }
            if (!allChecked) {
                int number = DataCenter.getInstance().getInt("KNOWN_CLEAR_CHANNEL_NUMBER");
                if (number > 0) {
                    Channel ch = db.getChannelByNumber(number);
                    if (ch != null && isUnsubscribed(ch)) {
                        Log.printWarning("CaManager: CANH tells ch #" + number + " is unsubscribed !");
                        INVALID_CANH = true;
                    }
                }
            }
            if (full) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.CheckAllThread.run() : Call ChannelController.notifyCaUpdated(0, true)");
                ChannelController.notifyCaUpdated(0, true);
            }
            // 미가입 tech 채널은 제공하지 않는다.
            if (changedTech) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CaManager.CheckAllThread.run() : Call buildLater()");
                ChannelDatabaseBuilder.getInstance().buildLater(ChannelDatabaseBuilder.CHANGED_TECH_PERMISSION);
            }
            allChecked = true;
            Log.printDebug("CaManager.checkAllChannels. result=" + count + "/" + size);
            FrameworkMain.getInstance().printSimpleLog("CaManager = " + count + "/" + size);
        }
    }

}

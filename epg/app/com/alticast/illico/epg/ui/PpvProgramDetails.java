package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.ppv.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.ixc.upp.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;
import com.opencable.handler.cahandler.*;

public class PpvProgramDetails extends ProgramDetails implements PpvListener, RightFilterListener {

    public PpvProgram ppv;

    private static final Byte LOCK = new Byte((byte) 0);
    private static CAAuthorization monitoring = null;

    private CAAuthorizationListener caaListener = new CAAuthorizationAdapter();

    public PpvProgramDetails() {
        setRenderer(new PpvProgramDetailsRenderer());
        scroll.setSize(519, 190-76);
        scroll.setRows(5);
        scroll.setBottomMaskImagePosition(0, 343-186-76);
        scroll.setTopMaskImage(DataCenter.getInstance().getImage("epg_text_sha_top1.png"));
        scroll.setTopMaskImagePosition(0, 9);
        scroll.setInsets(3, 5, 7, 0);
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.stop()");
        super.stop();
        ppv = null;
        setMonitoring(null);
    }

    public void start(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.start("+p+")");
        if (p instanceof PpvProgram) {
            ppv = (PpvProgram) p;
        }
        super.start(p);
    }

    protected void setData(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.setData("+p+")");
        super.setData(p);
        if (blockLevel == ParentalControl.ADULT_CONTENT) {
            scroll.setLocation(48, 186);
        } else {
            scroll.setLocation(48, 186+76-3);
        }
    }

    private boolean checkAuth(PpvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.checkAuth("+p+")");
        PpvController pc = PpvController.getInstance();
        CAAuthorization caa = pc.checkPurchased(channel, p, caaListener);
        boolean auth = false;
        try {
            auth = caa.isIppvEventPurchased();
        } catch (Exception ex) {
        }
        setMonitoring(caa);
        if (auth) {
            return true;
        }
        return PpvHistoryManager.getInstance().contains(p);
    }

    private static void setMonitoring(CAAuthorization caa) {
        synchronized (LOCK) {
            Log.printDebug(App.LOG_HEADER+"PpvProgramDetails: setMonitoring = " + caa);
            try {
                if (monitoring != null && monitoring != PpvController.getInstance().monitoring) {
                    monitoring.stopMonitoring();
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            monitoring = caa;
        }
    }

    public void selected(MenuItem item) {
        String key = item.getKey();

        DataCenter dc = DataCenter.getInstance();
        if (item == Rs.MENU_CLOSE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call GridEpg.hideDetails()");
            ((GridEpg)getParent()).hideDetails();
        //->Kenneth[2015.2.3] 4K
        } else if (item == Rs.MENU_MORE_INFO) {
            if (Log.DEBUG_ON) Log.printDebug("PpvProgramDetail.selected() : Call showIncompatibilityErrorMessage()--2");
            //->Kenneth[2015.10.15] DDC-114
            EpgCore.getInstance().showIncompatibilityErrorMessage(IncompatibilityChannelPanel.resetReasonCode(channel));
            //EpgCore.getInstance().showIncompatibilityErrorMessage();
            //<-
        } else if (item == Rs.MENU_TUNE_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call tuneToChannel()");
            tuneToChannel();
        } else if (item == Rs.MENU_SET_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call ReminderManager.remind()");
            ReminderManager.getInstance().remind(program, this);
            //->Kenneth[2015.9.18] : VDTRMASTER-5651 
            resetFooters();
            //<-
        } else if (item == Rs.MENU_REMOVE_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call ReminderManager.remove()");
            ReminderManager.getInstance().remove(program);
            updateStatus();
            //->Kenneth[2015.9.18] : VDTRMASTER-5651 
            resetFooters();
            //<-
        } else if (item == Rs.MENU_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call PpvController.showPurchasePopup()");
            PpvController.getInstance().showPurchasePopup(ppv, this);
        } else if (item == Rs.MENU_CANCEL_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call PpvController.showCancelPopup()");
            PpvController.getInstance().showCancelPopup(ppv, this);
        } else if (item == Rs.MENU_UNBLOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call PpvController.showPin()");
            ChannelAssetPage.showPin(this);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.selected() : Call GridEpg.selected()");
            ((GridEpg)getParent()).selected(item, channel, program, this, this);
        }
    }

    public void notifyPurchaseResult(int result) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.notifyPurchaseResult("+result+")");
        if (result == PpvListener.PURCHASE_SUCCESS) {
            updateStatus();
        }
    }

    public void notifyCancelResult(int result) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.notifyCancelResult("+result+")");
        if (result == PpvListener.CANCEL_SUCCESS) {
            updateStatus();
        }
    }

    protected MenuItem createMenu() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.createMenu()");
        MenuItem root = new MenuItem("PPV_DETAIL");

        //->Kenneth[2015.10.15] DDC-114
        int reason = IncompatibilityChannelPanel.resetReasonCode(channel);
        if (reason != IncompatibilityChannelPanel.REASON_UHD_COMPATIBLE) {
            root.add(Rs.MENU_MORE_INFO);
            return root;
        }
        /*
        //->Kenneth[2015.2.3] 4K
        if (EpgCore.getInstance().needToShowIncompatibilityPopup(channel)) {
            root.add(Rs.MENU_MORE_INFO);
            return root;
        }
        */

        //->Kenneth[2015.7.27] DDC-107 : 뒤늦게 발견. PPV 의 경우는 구매를 막아야 한다.
        if (channel.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD) {
            boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.createMenu() : canPlayUhd = "+canPlayUhd);
            if (!canPlayUhd) {
                root.add(Rs.MENU_MORE_INFO);
                return root;
            } 

            //->Kenneth[2015.8.6] VDTRMASTER-5563 : 4k 박스/4K 채널인 경우만 동작하도록 아래에서 여기로 위치 변경
            boolean hdcp22Supported = true;
            if (VideoOutputUtil.getInstance().existHdcpApi()) {
                hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.createMenu() : hdcp22Supported = "+hdcp22Supported);
                if (!hdcp22Supported) {
                    root.add(Rs.MENU_MORE_INFO);
                    return root;
                } 
            }
            //<-
        }

        /*
        //->Kenneth[2015.7.27] HDCP2.2
        boolean hdcp22Supported = true;
        if (VideoOutputUtil.getInstance().existHdcpApi()) {
            hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.createMenu() : hdcp22Supported = "+hdcp22Supported);
            if (!hdcp22Supported) {
                root.add(Rs.MENU_MORE_INFO);
                return root;
            } 
        }
        //<-
        */

        if (PpvController.hasPackageAuth) {
//            boolean ordered = PpvController.getInstance().isPurchasedOrReserved(ppv);
            boolean ordered = checkAuth(ppv);
            if (ordered) {
                if (PpvController.isCancelableTime(ppv)) {
                    if (ParentalControl.getLevel(ppv) != ParentalControl.OK) {
                        root.add(Rs.MENU_UNBLOCK);
                    } else {
                        root.add(Rs.MENU_CANCEL_ORDER);
                    }
                }
            } else {
                if (PpvController.isOrderableTime(ppv)) {
                    if (ParentalControl.getLevel(ppv) != ParentalControl.OK) {
                        root.add(Rs.MENU_UNBLOCK);
                    } else {
                        root.add(Rs.MENU_ORDER);
                    }
                }
            }
            root.add(Rs.MENU_TUNE_CHANNEL);

            //->Kenneth[2015.6.15] R5 : set a reminder 는 없앰
            //if (root.size() == 1 && ppv.isFuture()) {
            //    root.add(Rs.getReminderMenu(program));
//                if (ReminderManager.getInstance().contains(program)) {
//                    root.add(Rs.MENU_REMOVE_REMINDER);
//                } else {
//                    root.add(Rs.MENU_SET_REMINDER);
//                }
            //}
        }
        //->Kenneth[2015.6.6] R5 : close 는 없앰
        //root.add(Rs.MENU_CLOSE);
        return root;
    }

    public void receiveCheckRightFilter(int response) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.receiveCheckRightFilter("+response+")");
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.receiveCheckRightFilter() : Call ParentalControl.programUnblocked()");
            ParentalControl.getInstance().programUnblocked(ppv);
            if (ppv.isOnAir()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.receiveCheckRightFilter() : Call ChannelController.releaseChannelBlock()");
                ChannelController.get(0).releaseChannelBlock(channel);
            }
//            updateStatus();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.receiveCheckRightFilter() : Call start(ppv)");
            start(ppv);
        }
    }

    public String[] getPinEnablerExplain() {
        //->Kenneth[2015.11.23] VDTRMASTER-5718 : EpgCore 의 것을 사용한다.
        return EpgCore.getInstance().getBlockedChannelExplain(channel, false);
        //return TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
        //<-
    }

    ////////////////////////////////////////////////////////////////////////////
    // CAAuthorizationListener
    ////////////////////////////////////////////////////////////////////////////
    class CAAuthorizationAdapter implements CAAuthorizationListener {
        public void deliverEvent(CAAuthorizationEvent event) {
            Log.printInfo(App.LOG_HEADER+"PpvProgramDetails.CAAuthorizationAdapter.deliverEvent: event = " + event);
            if (event == null) {
                return;
            }
            int type = event.getTargetType();
            if (type != CAAuthorization.PPV_TARGET) {
                return;
            }
            long eid = event.getTargetId() & ~CaManager.EID_PREFIX;
            boolean auth = event.isAuthorized();
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.CAAuthorizationAdapter.deliverEvent: eid        = " + eid);
                Log.printDebug(App.LOG_HEADER+"PpvProgramDetails.CAAuthorizationAdapter.deliverEvent: authorized = " + auth);
            }
            updateStatus();
        }
    }

}

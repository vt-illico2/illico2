package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.navigator.ChannelZapper;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.sdv.mc.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.ui.Scrollable;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.epg.EpgDataListener;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.Point;
import java.awt.event.KeyEvent;
import org.ocap.ui.event.OCRcEvent;
import java.util.Calendar;
import java.util.Date;
import javax.tv.util.*;

public class GridPrograms extends EpgPanel implements TVTimerWentOffListener, EpgDataListenerInternal, Scrollable {
    private static final long serialVersionUID = 202109851251L;

    private static final long DEFAULT_EXPAND_TIME = 3000L;

    public static final int HOURS_IN_PAGE = 2;
    public static final long PAGE_TIME_GAP = HOURS_IN_PAGE * Constants.MS_PER_HOUR;

    public static final boolean ENABLE_ARROW_EFFECT = false;
    public static final boolean ENABLE_DATE_CHANGE_EFFECT = false;

    public int size;

    public int prevSize;
    public int nextSize;
    public int rowSize;

    public ChannelList channelList;

    public long firstTime;
    public long startTime;
    public long pageTime;

    public ProgramList[] programs;
    public Program[] leftPrograms;
    public Program focusedEvent;

    private long focusedTime;

    private boolean autoExpand = false;
    // 0 focus, 1 expanded icons, 2 expanded title
    public int expandState = 0;

    public boolean loading = false;
    private boolean leftFocus = true;

    public int iconType = 0;

    private GridProgramsRenderer r = new GridProgramsRenderer();

    // 9개 일때 5개 보다 많아 링이 된 경우
    private boolean isRing;

    public boolean navigable[] = new boolean[4];
    public Point arrowPositions[] = new Point[4];
    public static final int MOVE_UP = ArrowEffect.UP;
    public static final int MOVE_DOWN = ArrowEffect.DOWN;
    public static final int MOVE_LEFT = ArrowEffect.LEFT;
    public static final int MOVE_RIGHT = ArrowEffect.RIGHT;

    private TVTimerSpec expandTimer;
    private TVTimerSpec dcaTimer;
    //->Kenneth[2015.3.30] R5
    private TVTimerSpec chNameTimer;
    // 0:기본 display 1:채널fullname 만 보이는 상태
    public int chNameState;

    public int recordingStatus;

    public char[] inputedChars = new char[ChannelZapper.MAX_DIGIT];

    public int inputedCount;
    private int inputedNumber;

    private Effect nextDateEffect;
    private Effect prevDateEffect;

    private ArrowEffect arrowEffect;

    public GridPrograms(int prevChSize, int nextChSize) {
        this.prevSize = prevChSize;
        this.nextSize = nextChSize;
        this.rowSize = prevSize + nextSize + 1;
        setRenderer(r);

        dcaTimer = new TVTimerSpec();
        dcaTimer.setDelayTime(DataCenter.getInstance().getLong("DCA_STANDBY_DELAY", 2000L));
        dcaTimer.addTVTimerWentOffListener(this);

        expandTimer = new TVTimerSpec();
        expandTimer.setTime(DEFAULT_EXPAND_TIME);
        expandTimer.setRepeat(true);
        expandTimer.setAbsolute(false);
        expandTimer.addTVTimerWentOffListener(this);

        //->Kenneth[2015.3.30] R5
        chNameTimer = new TVTimerSpec();
        chNameTimer.setTime(DEFAULT_EXPAND_TIME);//3초
        chNameTimer.setRepeat(true);
        chNameTimer.setAbsolute(false);
        chNameTimer.addTVTimerWentOffListener(this);

        nextDateEffect = new ImageMovingEffect(this, DataCenter.getInstance().getImage("02_epg_date_l.png"), 15, false, new Rectangle(260, 0, 859-260, 27));
        prevDateEffect = new ImageMovingEffect(this, DataCenter.getInstance().getImage("02_epg_date_r.png"), 15, true, new Rectangle(260, 0, 859-260, 27));
        arrowEffect = new ArrowEffect(this);

        resetInputStack();

        for (int i = 0; i < arrowPositions.length; i++) {
            arrowPositions[i] = new Point();
        }
    }

    public void setAutoExpand(boolean autoExpand) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.setAutoExpand("+autoExpand+")");
        this.autoExpand = autoExpand;
    }

    private void startExpandTimer() {
        boolean timerEnabled;
        if (autoExpand) {
            timerEnabled = true;
        } else {
            Channel ch = getFocusedChannel();
            //->Kenneth[2016.8.24] CYO Bundling/A la carte
            timerEnabled = (ch != null && ch.isISAPurchasable());
            //timerEnabled = (ch != null && ch.isPurchasable());
            //<-
            if (!timerEnabled) {
                stopExpandTimer();
            }
        }
        Log.printDebug(App.LOG_HEADER+"startExpandTimer = " + timerEnabled);
        iconType = 0;
        if (timerEnabled) {
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(expandTimer);
            try {
                expandTimer = timer.scheduleTimerSpec(expandTimer);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    private void stopExpandTimer() {
        iconType = 0;
        TVTimer.getTimer().deschedule(expandTimer);
    }

    private void startDcaTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(dcaTimer);
        try {
            dcaTimer = timer.scheduleTimerSpec(dcaTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    private void stopDcaTimer() {
        TVTimer.getTimer().deschedule(dcaTimer);
    }

    private void resetInputStack() {
        if (inputedCount != 0) {
            inputedCount = 0;
            inputedNumber = 0;
            for (int i = 0; i < inputedChars.length; i++) {
                if (i < ChannelZapper.MAX_DIGIT - ChannelZapper.digit) {
                    inputedChars[i] = ' ';
                } else {
                    inputedChars[i] = '-';
                }
            }
        }
    }

    //->Kenneth[2015.3.30] R5
    private void startChannelNameTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.startChannelNameTimer()");
        TVTimer timer = TVTimer.getTimer();
        stopChannelNameTimer();
        try {
            chNameTimer = timer.scheduleTimerSpec(chNameTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
        repaint();
    }

    //->Kenneth[2015.3.30] R5
    private void stopChannelNameTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.stopChannelNameTimer()");
        chNameState = 0;
        TVTimer.getTimer().deschedule(chNameTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        TVTimerSpec spec = e.getTimerSpec();
        if (spec.equals(dcaTimer)) {
            focusChannelByNumber();
            GridEpg gridEpg = (GridEpg) getParent();
            if (gridEpg != null && gridEpg.type == GridEpg.TYPE_DEFAULT) {
                // program 변경
                gridEpg.desc.setProgram(focusedEvent);
                gridEpg.repaint();
            }
        } else if (spec.equals(expandTimer)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.timerWentOff() : expandTimer");
            iconType = (iconType + 1) % 2;
            if (focusedEvent instanceof FakeProgram) {
                repaint(0, 25 + GridProgramsRenderer.V_GAP * prevSize, 30, 40);
                return;
            }
            int ns;
            if (expandState == 2) {
                ns = 1;
            } else {
                ns = expandState + 1;
            }
            if (ns == 1) {
                recordingStatus = RecordingStatus.getInstance().getStatus(focusedEvent);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.timerWentOff() : recordingStatus = "+recordingStatus);
                //->Kenneth[2016.3.7] R7 : Group 상태에서만 sister 상태 체크
                if (EpgCore.isChannelGrouped) {
                    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
                    int sisterRecordingStatus = RecordingStatus.getInstance().getSisterStatus(focusedEvent);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.timerWentOff() : sisterRecordingStatus = "+sisterRecordingStatus);
                    if (sisterRecordingStatus == RecordingStatus.RECORDING) {
                        recordingStatus = sisterRecordingStatus;
                    }
                }
                //<-
            }
            expandState = ns;
            repaint();
//        } else if (spec.equals(iconChangeTimer)) {
//            iconType = (iconType + 1) % 2;
//            repaint(0, 25 + GridProgramsRenderer.V_GAP * prevSize, 30, 40);

        //->Kenneth[2015.3.30] R5
        } else if (spec.equals(chNameTimer)) {
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.timerWentOff() : chNameTimer");
            if (chNameState == 0) {
                chNameState = 1;
            } else {
                chNameState = 0;
            }
            repaint();
        }
    }

    public void start() {
        start(-1, -1, null, true);
    }

    public void start(long newPageTime, int newFocus, Program event, boolean needLoading) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start -- GridPrograms.start("+newFocus+", "+event+", "+needLoading+")");
        hideLoading();
        long cur = System.currentTimeMillis();
        startTime = cur - cur % (Constants.MS_PER_HOUR / 2);
        firstTime = startTime - PAGE_TIME_GAP;
        if (newPageTime == -1) {
            this.pageTime = startTime;
            focusedTime = cur;
        } else {
            if (pageTime != newPageTime) {
                focusedTime = newPageTime;
                this.pageTime = newPageTime;
            }
        }
        channelList = GridEpg.channelList;

        this.size = channelList.size();
        this.isRing = size >= rowSize;

        this.programs = new ProgramList[size];
        this.leftPrograms = new Program[size];

        EpgDataManager edm = EpgDataManager.getInstance();
        Program p;
        for (int i = 0; i < size; i++) {
            //->Kenneth[2016.3.7] R7 : decideTargetChannel 자체에서 grouping 체크하므로 여기선 아무일도 안한다.
            //->Kenneth[2015.4.6] R5 : SD/HD 그룹핑 때문에 SD 채널이라도 HD 프로그램을 가져야 한다.
            Channel target = GridEpg.decideTargetChannel(channelList.getChannelAt(i));
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.start() : ("+i+") : "+target);
            programs[i] = edm.getPrograms(target);
            //programs[i] = edm.getPrograms(channelList.getChannelAt(i));
            p = programs[i].search(pageTime, programs[i].start);
            if (p != null) {
                leftPrograms[i] = p;
            } else  {
                leftPrograms[i] = programs[i].start;
            }
        }

        if (newFocus == -1) {
            this.focus = channelList.getCurrentIndex();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.start() : getCurrentIndex()"+channelList.getCurrentIndex());
        } else {
            this.focus = newFocus;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.start() : new focus = "+focus+" : "+channelList.getCurrentChannel());
        if (focus < 0) {
            focus = 0;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.start() : isRing = "+isRing);
        if (!isRing) {
            int half = (rowSize + 1) / 2;
            if (size > half) {
                if (focus >= half || size - focus > half) {
                    focus = size - half;
                }
            }
        }

        if (event == null) {
            this.focusedEvent = programs[focus].search(focusedTime, leftPrograms[focus]);
        } else {
            this.focusedEvent = event;
            resetFocusedTime();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("GridPrograms.focus = " + focusedEvent);
        }
        updateNavigable();
        prepare();

        if (needLoading) {
            checkAvailable(pageTime);
        }
        if (ENABLE_ARROW_EFFECT) {
            arrowEffect.readyAbsoluteLocation();
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.start() : Call startChannelNameTimer()");
        startChannelNameTimer();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"End -- GridPrograms.start("+event+")");
    }

    public boolean hasMultiplePages() {
        return (size > rowSize);
    }

    public boolean checkAvailable(long time) {
        return checkAvailable(time, true);
    }

    private boolean checkAvailable(long time, boolean leftFocus) {
        long from = Math.min(time, EpgDataManager.lastTime);
        long to = from + PAGE_TIME_GAP;
        if (!EpgDataManager.getInstance().isAvailable(from, to)) {
            startLoading(from, to, leftFocus);
            repaint();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.checkAvailable() returns false");
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.checkAvailable() returns true");
        return true;
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.stop()");
        hideLoading();
        stopExpandTimer();
        stopDcaTimer();
        resetInputStack();
        programs = null;
        leftPrograms = null;
        channelList = null;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.stop() : Call startChannelNameTimer()");
        startChannelNameTimer();
    }

    public Channel getFocusedChannel() {
        ChannelList list = channelList;
        if (list == null) {
            return null;
        }
        //->Kenneth[2015.4.20] R5
        Channel ch = channelList.getChannelAt(focus);
        ch = GridEpg.decideTargetChannel(ch);
        return ch;
        //return channelList.getChannelAt(focus);
    }

    //->Kenneth[2015.10.7] getFocusedChannel() 은 sister 채널로 바꿔서 돌려준다.
    // 바꾸지 않는 경우도 필요하다.
    public Channel getFocusedChannelWithoutChange() {
        ChannelList list = channelList;
        if (list == null) {
            return null;
        }
        return channelList.getChannelAt(focus);
    }

    public int getIndexOfRow(int row) {
        if (isRing) {
            return (focus - prevSize + row + size) % size;
        } else {
            int index = focus - prevSize + row;
            if (index >= size) {
                return Integer.MIN_VALUE;
            }
            return index;
        }
    }

    public boolean handleKey(int code) {
        if (code >= KeyEvent.VK_0 && code <= KeyEvent.VK_9) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : Numeric Key");
            keyNumber(code - KeyEvent.VK_0);
            return true;
        }
        switch (code) {
            case KeyCodes.COLOR_C:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : COLOR_C");
                // TEST
                //->Kenneth[2015.4.6] R5 : C 버튼을 구현해야 되서 아래 extra 에서 테스트 하는 코드 삭제
                /*
                if (Log.EXTRA_ON) {
                    McChannelInfo info = null;
                    McData data = (McData) DataCenter.getInstance().get(MiniCarouselReader.MC_KEY);
                    if (data != null) {
                        int size = channelList.size();
                        Channel ch;
                        int index = focus;
                        for (int i = 1; i < size - 1; i++) {
                            index = (index + 1) % size;
                            ch = channelList.getChannelAt(index);
                            if (ch instanceof SdvChannel &&  data.get(ch.getSourceId()) == null) {
                                focus = index;
                                ChannelController.get(0).changeChannel(ch);
                                resetChannelFocus();
                                return false;
                            }
                        }
                    }
                }
                */
                return false;
            case OCRcEvent.VK_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_UP");
                if (navigable[MOVE_UP]) {
                    if (ENABLE_ARROW_EFFECT) {
                        arrowEffect.startFromRoot(arrowPositions[ArrowEffect.UP], ArrowEffect.UP);
                    }
                    keyUp(1);
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_DOWN");
                if (navigable[MOVE_DOWN]) {
                    if (ENABLE_ARROW_EFFECT) {
                        arrowEffect.startFromRoot(arrowPositions[ArrowEffect.DOWN], ArrowEffect.DOWN);
                    }
                    keyDown(1);
                }
                break;
            case OCRcEvent.VK_PAGE_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_PAGE_UP");
                GridEpg gridEpg = (GridEpg) getParent();
                if (gridEpg != null) {
                    gridEpg.footer.clickAnimationByKeyCode(code);
                }
                if (hasMultiplePages()) {
                    keyUp(rowSize);
                }
                break;
            case OCRcEvent.VK_PAGE_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_PAGE_DOWN");
                GridEpg gridEpg1 = (GridEpg) getParent();
                if (gridEpg1 != null) {
                    gridEpg1.footer.clickAnimationByKeyCode(code);
                }
                if (hasMultiplePages()) {
                    keyDown(rowSize);
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_LEFT");
                if (ENABLE_ARROW_EFFECT && navigable[MOVE_LEFT]) {
                    arrowEffect.startFromRoot(arrowPositions[ArrowEffect.LEFT], ArrowEffect.LEFT);
                }
                if (keyLeft()) {
                    if (ENABLE_DATE_CHANGE_EFFECT) {
                        prevDateEffect.startLater();
                    }
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_RIGHT");
                if (ENABLE_ARROW_EFFECT && navigable[MOVE_RIGHT]) {
                    arrowEffect.startFromRoot(arrowPositions[ArrowEffect.RIGHT], ArrowEffect.RIGHT);
                }
                if (keyRight()) {
                    if (ENABLE_DATE_CHANGE_EFFECT) {
                        nextDateEffect.startLater();
                    }
                }
                break;
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_ENTER");
                keyEnter();
                break;
            case OCRcEvent.VK_PREV_DAY:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_PREV_DAY");
                keyPrevDay();
                break;
            case OCRcEvent.VK_NEXT_DAY:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.handleKey() : VK_NEXT_DAY");
                keyNextDay();
                break;
            default:
                return false;
        }
        resetInputStack();
        stopDcaTimer();
        updateNavigable();
        return true;
    }

    public void keyNumber(int number) {
        stopDcaTimer();
        inputedCount++;
        for (int i = inputedChars.length - inputedCount; i < inputedChars.length - 1; i++) {
            inputedChars[i] = inputedChars[i + 1];
        }
        inputedChars[inputedChars.length - 1] = (char) ('0' + number);

        inputedNumber = inputedNumber * 10 + number;
        if (inputedCount >= ChannelZapper.digit) {
            focusChannelByNumber();
        } else {
            startDcaTimer();
        }
        repaint();
    }

    public void keyEnter() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyEnter()");
        if (inputedCount > 0) {
            stopDcaTimer();
            focusChannelByNumber();
            return;
        }
        if (!focusedEvent.isOnAir()) {
            Container p = getParent();
            if (p != null) {
                ((GridEpg) p).showDetails(focusedEvent);
            }
            return;
        }
        Channel target = getFocusedChannel();
        if (target == null) {
            try {
                CommunicationManager.getMonitorService().exitToChannel();
            } catch (Exception ex) {
                Log.print(ex);
            }
            return;
        }
        //->Kennet[2017.9.1] R7.4 : TEST : 테스트 중 아니면 꼭 막을것
        // grid 에서 채널 선택되면 해당 채널의 upcoming 레코딩을 다 지우는 테스트 코드임
        /*
        try {
            EpgCore.getInstance().pvrService.removeAllUpcomingRecordings(target.getName());
        } catch (Exception e) {
            Log.print(e);
        }
        */
        //<-

        //->Kenneth[2016.12.14] VDTRMASTER-6022 : Grid 화면에서 Free Preview 채널 누르면 ISA/CYO 팝업 대신에 해당 채널로
        // tune 되어야 한다. 아래 !target.isSubscribed() 를 R7.2 하면서 뺐더니 생기는 이슈임.
        // Detail 페이지에서 팝업이 보이는 것은 일단 관여치 않기로 한다. (conf call 에서 Alex 가 얘기한 것임)
        // 지금은 긴급 패치이니 필요한 경우 R7.3 미팅에서 처리하도록 한다.
        if (target.isISAPurchasable() && !target.isSubscribed()) {
        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        //if (target.isISAPurchasable()) {
        //if (target.isPurchasable() && !target.isSubscribed()) {
        //<-
        //<-
            EpgCore.getInstance().showSubscriptionPopup(target, ISAService.ENTRY_POINT_EPG_E01);
            //->Kenneth[2017.3.16] R7.3 : VBM 로그 
            EpgVbmController.getInstance().writeUnsubscriptionScreen(target, focusedEvent);
            //<-
        } else {
            Container p = getParent();
            if (p != null) {
                ((GridEpg) p).exitToChannel(target);
            } else {
                try {
                    CommunicationManager.getMonitorService().exitToChannel(target.getId());
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    private void focusChannelByNumber() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.focusChannelByNumber()");
        int no = inputedNumber;
        resetInputStack();
        Channel ch = ChannelDatabaseBuilder.getInstance().getDatabase().getChannelByNumber(no);
        //->Kenneth[2017.2.13] R7.3 : TECH 채널은 Grid/Mini EPG 에서 안보인다.
        if (ch != null && ch.getType() != Channel.TYPE_TECH) {
            setFocusedChannel(ch);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.focusChannelByNumber(). TECH channel, so do nothing");
        }
        //setFocusedChannel(ch);
        //<-
        repaint();
    }

    private void updateNavigable() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.updateNavigable()");
        expandState = 0;
        navigable[MOVE_UP] = (isRing || focus > 0);
        navigable[MOVE_DOWN] = (isRing || focus < size - 1);
        if (focusedEvent == null) {
            navigable[MOVE_LEFT] = false;
            navigable[MOVE_RIGHT] = false;
        } else {
            long pst = focusedEvent.getStartTime();
            navigable[MOVE_LEFT] = (pst > firstTime && focusedEvent.prev != null) || (pst < pageTime && pageTime > firstTime);
            long pet = focusedEvent.getEndTime();
            navigable[MOVE_RIGHT] = (pet < pageTime + PAGE_TIME_GAP && pet < EpgDataManager.lastTime)
                                    ||  pageTime + PAGE_TIME_GAP < EpgDataManager.lastTime;
        }
        startExpandTimer();
        repaint();
    }

    boolean setFocusedChannel(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.setFocusedChannel("+ch+")");
        if (ch == null) {
            return false;
        }
        GridEpg gridEpg = (GridEpg) getParent();
        int index = channelList.search(ch);
        if (index < 0) {
            if (gridEpg == null) {
                return false;
            }
            //->Kenneth[2015.4.25] R5 : sister channel 이 있는지 확인
            Channel sister = null;

            //->Kenneth[2016.3.7] R7 : Group 상태에서만 sister 체크
            if (EpgCore.isChannelGrouped) {
                try {
                    sister = (Channel)ch.getSisterChannel();
                } catch (Exception e) {
                    Log.print(e);
                }
                if (sister != null) {
                    index = channelList.search(sister);
                }
            }
            //<-

            if (index < 0) {
                gridEpg.selected(Rs.MENU_FILTER_TYPE_ALL);
                index = channelList.search(ch);
                if (index < 0) {
                    if (sister != null) {
                        index = channelList.search(sister);
                    }
                    if (index < 0) {
                        return false;
                    }
                }
            }
        }
        focus = index;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.setFocusedChannel() : Call resetChannelFocus()");
        resetChannelFocus();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.setFocusedChannel() : Call updateNavigable()");
        updateNavigable();
        if (gridEpg != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.setFocusedChannel() : Call GridEpg.channelFocusChanged()");
            gridEpg.channelFocusChanged();
        }
        return true;
    }

    public void keyUp(int amount) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyUp()");
        if (!navigable[MOVE_UP]) {
            return;
        }
        focus = (focus - amount + size) % size;
        resetChannelFocus();
    }

    public void keyDown(int amount) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyDown()");
        if (!navigable[MOVE_DOWN]) {
            return;
        }
        focus = (focus + amount) % size;
        resetChannelFocus();
    }

    private void resetChannelFocus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.resetChannelFocus()");
        if (leftPrograms[focus] != null) {
            focusedEvent = programs[focus].search(focusedTime, leftPrograms[focus]);
            if (focusedEvent == null) {
                if (leftPrograms[focus].getEndTime() <= focusedTime) {
                    focusedEvent = programs[focus].end;
                } else {
                    focusedEvent = leftPrograms[focus];
                }
            }
        }
        //->Kenneth[2015.3.30] R5
        startChannelNameTimer();
    }

    private synchronized void startLoading(long from, long to, boolean leftFocus) {
        this.loading = true;
        this.leftFocus = leftFocus;
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.showNotDelayLoadingAnimation(new Point(612, getY() + 27 + getHeight() / 2));
        } catch (Exception ex) {
            Log.print(ex);
        }
        EpgDataManager.getInstance().requestHttp(from, to);
    }

    private synchronized void hideLoading() {
        if (!loading) {
            return;
        }
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.hideLoadingAnimation();
        } catch (Exception ex) {
            Log.print(ex);
        }
        loading = false;
    }

    public boolean keyRight() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyRight()");
        if (loading) {
            return false;
        }
        if (focusedEvent == null) return false;
        long end = focusedEvent.getEndTime();
        if (!navigable[MOVE_RIGHT]) {
            DataCenter dc = DataCenter.getInstance();
            PopupController.getInstance().showConfirm(
                    dc.getString("epg.no_more_data_popup_title"),
                    dc.getString("epg.no_more_data_popup_msg"), null);
            return false;
        }

        boolean ret;
        if (end >= pageTime + PAGE_TIME_GAP) {
            ret = true;
            // move to next page
            pageTime = pageTime + PAGE_TIME_GAP;
            EpgDataManager.getInstance().epgViewChanged(pageTime, pageTime + PAGE_TIME_GAP);
            if (!checkAvailable(pageTime)) {
                return ret;
            }

            for (int i = 0; i < programs.length; i++) {
                Program p = programs[i].search(pageTime, leftPrograms[i]);
                if (p != null) {
                    leftPrograms[i] = p;
                } else {
                    leftPrograms[i] = programs[i].end;
                }
            }
            if (end == pageTime) {
                // focused program is over. select next.
                focusedEvent = focusedEvent.next;
            }
        } else {
            ret = false;
            // move next in the page
            focusedEvent = focusedEvent.next;
        }
        resetFocusedTime();
        repaint();
        return ret;
    }

    public boolean keyLeft() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyLeft()");
        if (loading) {
            return false;
        }
        if (!navigable[MOVE_LEFT]) {
            return false;
        }
        if (focusedEvent == null) {
            return false;
        }
        if (focusedEvent.prev != null || pageTime > startTime || focusedEvent.startTime < pageTime) {
            boolean ret;
            if (focusedEvent.contains(pageTime)) {
                ret = true;
                boolean toPast = pageTime < System.currentTimeMillis();
                pageTime = pageTime - PAGE_TIME_GAP;
                EpgDataManager.getInstance().epgViewChanged(pageTime, pageTime + PAGE_TIME_GAP);
                if (!toPast && !checkAvailable(pageTime, false)) {
                    return ret;
                }
                for (int i = 0; i < programs.length; i++) {
                    Program p = programs[i].search(pageTime, leftPrograms[i]);
                    if (p != null) {
                        leftPrograms[i] = p;
                    } else {
                        leftPrograms[i] = programs[i].start;
                    }
                }
                if (focusedEvent.getStartTime() != pageTime + PAGE_TIME_GAP) {
                    resetFocusedTime();
                    repaint();
                    return ret;
                }
            } else {
                ret = false;
            }
            focusedEvent = focusedEvent.prev;
            resetFocusedTime();
            repaint();
            return ret;
        } else {
            return false;
        }
    }

    private void keyPrevDay() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyPrevDay()");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(pageTime - 1));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long newPageTime = cal.getTime().getTime();
        if (newPageTime < startTime) {
            if (pageTime == startTime) {
                return;
            }
            newPageTime = startTime;
        }
        changeDay(newPageTime);
    }

    private void keyNextDay() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.keyNextDay()");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(pageTime));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        long newPageTime = cal.getTime().getTime();
        long curEndTime = programs[focus].end.getEndTime();
        if (curEndTime <= newPageTime) {
            return;
        }
        changeDay(newPageTime);
    }

    private void changeDay(long time) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.changeDay()");
        pageTime = time;
        EpgDataManager.getInstance().epgViewChanged(pageTime, pageTime + PAGE_TIME_GAP);
        if (!checkAvailable(time)) {
            return;
        }
        for (int i = 0; i < programs.length; i++) {
            Program p = programs[i].search(pageTime, leftPrograms[i]);
            if (p != null) {
                leftPrograms[i] = p;
            } else {
                leftPrograms[i] = programs[i].start;
            }
        }
        focusedEvent = leftPrograms[focus];
        focusedTime = time;
        GridEpg gridEpg = (GridEpg) getParent();
        if (gridEpg != null && gridEpg.type == GridEpg.TYPE_DEFAULT) {
            // program 변경
            gridEpg.desc.setProgram(focusedEvent);
            gridEpg.repaint();
        }
        repaint();
    }

    public void goToDay(int dayOffset) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridPrograms.goToDay("+dayOffset+")");
        // Note - 현재도 미래일때는 유지해줘야 한다.
        // 다시 원복 - VDTRMASTER-2317
        if (dayOffset == 0) {
            changeDay(startTime);
            return;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(pageTime));
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        cal.setTime(new Date(startTime));
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.add(Calendar.DAY_OF_MONTH, dayOffset);
        if (Log.DEBUG_ON) {
            Log.printDebug("GridPrograms.goToDay: " + dayOffset + ", " + cal.getTime());
        }
        long newPageTime = cal.getTime().getTime();
        if (newPageTime < startTime) {
            newPageTime = startTime;
        }
        changeDay(newPageTime);
    }

    private void resetFocusedTime() {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GridPrograms.resetFocusedTime : focusedEvent = " + focusedEvent);
        }
        long cur = System.currentTimeMillis();
        if (pageTime <= cur && cur < pageTime + PAGE_TIME_GAP) {
            if (focusedEvent.contains(cur)) {
                focusedTime = cur;
                return;
            }
        }
        if (focusedEvent.contains(pageTime)) {
            focusedTime = pageTime;
            return;
        }
        if (focusedEvent.contains(pageTime + PAGE_TIME_GAP - 1)) {
            focusedTime = pageTime + PAGE_TIME_GAP - 1;
            return;
        }
        focusedTime = focusedEvent.getStartTime() + focusedEvent.getDuration() / 2;
    }

//    public void dataAvaliable(RangeList list) {
    public void epgDataUpdated(byte type, long from, long to) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GridPrograms.epgDataUpdated: loading = " + loading + ", leftFocus = " + leftFocus);
        }
        if (!loading) {
            if (from < pageTime + PAGE_TIME_GAP && pageTime < to) {
                Log.printDebug("GridPrograms.epgDataUpdated: update leftPrograms = " + new Date(from));
                for (int i = 0; i < programs.length; i++) {
                    Program p = programs[i].search(pageTime, null);
                    if (p != null) {
                        leftPrograms[i] = p;
                    } else {
                        leftPrograms[i] = programs[i].start;
                    }
                }
                try {
                    focusedEvent = programs[focus].search(focusedTime, leftPrograms[focus]);
                } catch (Exception ex) {
                }
            }
            return;
        }
        GridEpg gridEpg = (GridEpg) getParent();
        if (gridEpg == null) {
            Log.printDebug("GridPrograms.epgDataUpdated: root is null");
            return;
        }
        if (EpgDataManager.getInstance().isAvailable(pageTime, pageTime + PAGE_TIME_GAP)) {
            for (int i = 0; i < programs.length; i++) {
                Program p = programs[i].search(pageTime, null);
                if (p != null) {
                    leftPrograms[i] = p;
                } else {
                    leftPrograms[i] = programs[i].start;
                }
            }
            if (focusedTime >= pageTime && focusedTime < pageTime + PAGE_TIME_GAP) {
                focusedEvent = programs[focus].search(focusedTime, leftPrograms[focus]);
            } else {
                if (leftFocus) {
                    focusedTime = pageTime;
                    focusedEvent = leftPrograms[focus];
                } else {
                    focusedTime = pageTime + PAGE_TIME_GAP - 1;
                    focusedEvent = programs[focus].search(focusedTime, leftPrograms[focus]);
                }
            }
            if (gridEpg.type == GridEpg.TYPE_DEFAULT) {
                gridEpg.desc.setProgram(focusedEvent);
            }
            updateNavigable();
            hideLoading();
            repaint();
        }
    }
}

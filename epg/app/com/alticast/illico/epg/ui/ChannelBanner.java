package com.alticast.illico.epg.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.effect.*;
import com.alticast.illico.epg.navigator.*;
import java.awt.*;
import javax.tv.util.*;
import java.util.*;
import org.dvb.ui.DVBBufferedImage;
//import com.alticast.ui.*;

/**
 * ChannelBanner.
 *
 * @author June Park
 */
public abstract class ChannelBanner extends UIComponent implements TVTimerWentOffListener, Runnable, EpgListener {

    private static final long serialVersionUID = 1209019842L;

    public static final ChannelBanner EMPTY = new EmptyChannelBanner();

    public Channel channel;
    public String name;
    public String number;
    public Image logo;
	//->Kenneth : 4K by June
    public int definition;

    protected TVTimer timer = TVTimer.getTimer();
    protected TVTimerSpec autoCloseTimer;
    protected static final long DEFAULT_CLOSE_TIME = 3000L;

    protected EpgDataManager dm = EpgDataManager.getInstance();

    public AnimatedIcon favIcon;
    public AnimatedIcon lockIcon;

    protected static Image defaultLogo;
    protected static Image defaultScaledLogo;

    //->Kenneth[2015.9.3] R5 : julie 메일에 의거 pvr/vod 시에 다른 icon 보이게 수정함.
    public static Image pvrLogo = DataCenter.getInstance().getImage("PVR_Icon.png");
    public static Image vodLogo = DataCenter.getInstance().getImage("VOD_Icon.png");
    //<-

    public ChannelBanner() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setDelayTime(DEFAULT_CLOSE_TIME);
        autoCloseTimer.addTVTimerWentOffListener(this);

        DataCenter dc = DataCenter.getInstance();
        Image iFav = dc.getImage("02_icon_fav.png");
        Image iLock = dc.getImage("02_icon_con.png");
        if (defaultLogo == null) {
            defaultLogo = dc.getImage("INFOV_logo.png");
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (defaultScaledLogo == null) {
            //->Kenneth[2015.9.3] Julie 메일에 따라서 외부에서도 사용할 수 있게 수정/추가
            defaultScaledLogo = getScaledLogo(defaultLogo);
            /*
            DVBBufferedImage di = new DVBBufferedImage(80, 24);
            Graphics g = di.getGraphics();
            g.drawImage(defaultLogo, 0, 0, 80, 24, null);
            g.dispose();
            defaultScaledLogo = di;
            */
            //<-
        }
        favIcon = new AnimatedIcon(iFav);
        lockIcon = new AnimatedIcon(iLock);
        favIcon.setContainer(this);
        lockIcon.setContainer(this);
    }

    //->Kenneth[2015.9.3] Julie 메일에 따라서 추가
    public static Image getScaledLogo(Image logo) {
        DVBBufferedImage di = new DVBBufferedImage(80, 24);
        Graphics g = di.getGraphics();
        g.drawImage(logo, 0, 0, 80, 24, null);
        g.dispose();
        return di;
    }
    //<-

    // returns channel
    public Channel startTuner(int videoIndex) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.startTuner("+videoIndex+")");
        if (App.R3_TARGET && videoIndex == 0) {
            String videoContext = EpgCore.getInstance().getVideoContextName();
            if (videoContext != null && videoContext.length() > 0 && !App.NAME.equals(videoContext)) {
                startVideoContext(videoContext);
                return null;
            }
        }
        Channel ch = ChannelController.get(videoIndex).getCurrent();
        start(ch);
        return ch;
    }

    /** while input. */
    public void start(String number, String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.start("+number+", "+name+")");
        timer.deschedule(autoCloseTimer);
        synchronized (this) {
            this.number = number;
            this.name = name;
            this.channel = null;
            this.definition = 0;
        }
        start();
        EventQueue.invokeLater(this);
    }

    public void start(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.start("+ch+")");
        if (ch != null) {
            start(ch, Integer.toString(ch.getNumber()));
        }
    }
    //->Kenneth[2015.9.3] Julie 메일에 따라서 추가
    public static String getDisplayableVideoContextName(String videoContext) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.getDisplayableVideoContextName("+videoContext+")");
        String s = "";
        if ("PVR".equals(videoContext)) {
            if (Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage())) {
                s = "ENREGISTREUR";
            } else {
                s = "RECORDER";
            }
        } else if ("VOD".equals(videoContext)) {
            /*
            Channel ch = ChannelDatabase.current.getChannelByNumber(dc.getInt("VOD_CHANNEL", 900));
            if (ch != null) {
                s = ch.getCallLetter();
            } else {
                if (Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage())) {
                    s = "ILLICO SUR DEMANDE";
                } else {
                    s = "ILLICO ON DEMAND";
                }
            }
            */
            if (Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage())) {
                s = "ILLICO SUR DEMANDE";
            } else {
                s = "ILLICO ON DEMAND";
            }
        }
        return s;
    }
    //<-

    // VOD, PVR 재생 등 video context 가 변경되면 호출된다.
    // Full Menu, Guide 등을 띄웠을 때 Channel Banner 내용을 변경할 필요가 있음
    protected void startVideoContext(String videoContext) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.startVideoContext("+videoContext+")");
        timer.deschedule(autoCloseTimer);
        DataCenter dc = DataCenter.getInstance();
        String s = videoContext;
        String n = "";
        //->Kenneth[2015.9.3] Julie 메일에 따라서 수정
        Image im = null;
        if ("PVR".equals(videoContext)) {
            im = pvrLogo;
        } else if ("VOD".equals(videoContext)) {
            im = vodLogo;
            n = "900";
        }
        s = getDisplayableVideoContextName(videoContext);
        //Image im = dc.getImage("INFOV_logo.png");
        /*
        if ("PVR".equals(videoContext)) {
            if (Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage())) {
                s = "ENP";
            }
        } else if ("VOD".equals(videoContext)) {
            n = "900";
            Channel ch = ChannelDatabase.current.getChannelByNumber(dc.getInt("VOD_CHANNEL", 900));
            if (ch != null) {
                s = ch.getCallLetter();
            } else {
                s = "VOD";
            }
        }
        */
        //<-
        int d = Constants.DEFINITION_SD;
		//->Kenneth : 4K by June
        Integer in = (Integer) SharedMemory.getInstance().get(videoContext + SharedDataKeys.VIDEO_DEFINITION);
        if (in != null) {
            d = in.intValue();
        } else {
            Boolean b = (Boolean) SharedMemory.getInstance().get(videoContext + SharedDataKeys.VIDEO_RESOLUTION);
            if (b != null) {
                d = b.booleanValue() ? 1 : 0;
            }
        }
        synchronized (this) {
            this.number = n;
            this.name = s;
            this.channel = null;
            this.logo = im;
            this.definition = d;
        }
        start();
        startTimer();
    }

    public void start(Channel ch, String number) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBanner.start("+ch+", "+number+")");
        timer.deschedule(autoCloseTimer);
        synchronized (this) {
            this.channel = ch;
            this.number = number;
            //->Kenneth[2015.4.20] R5
            this.name = ch.getFullName();
            //this.name = ch.getName();
            this.logo = ch.getLogo();
            this.definition = ch.getDefinition();
            readyData(ch);
        }
        start();
        startTimer();
    }

    public abstract boolean isActive();

    protected abstract void readyData(Channel ch);

    public void run() {
        startTimer();
    }

    protected void stopTimer() {
        timer.deschedule(autoCloseTimer);
    }

    protected void startTimer() {
        synchronized (autoCloseTimer) {
            timer.deschedule(autoCloseTimer);
            try {
                Log.printDebug(App.LOG_HEADER+"ChannelBanner.startTimer");
                autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    public abstract void timerWentOff(TVTimerWentOffEvent event);

    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        // does nothing
    }

}

class EmptyChannelBanner extends ChannelBanner {

    public boolean isActive() {
        return true;
    }

    protected void readyData(Channel ch) {
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
    }
}


package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;

import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public abstract class OptionPanel extends EpgPanel implements MenuListener {

    public int size;

    public MenuItem menu;
    public MenuItem options;
    protected ClickingEffect clickEffect;
    // R5
    boolean isDimmedBg;
    boolean useDimmedFocus;

    public OptionScreen optionScreen = new OptionScreen();

    public OptionPanel() {
    }

    protected void showOptionScreen() {
        optionScreen.start(options, null, this, EpgCore.getInstance());
    }

    protected void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(((OptionPanelRenderer)renderer).getButtonFocusBounds(this));
    }

    /** MenuListener. */
    public void canceled() {
    }

    public void updateStatus() {
    }

    // R5
    public void paintOptionPanel(Graphics g) {
    	((OptionPanelRenderer)renderer).paintButtons(g, this, menu);
    }
    
    // R5
    public void setDimmedBg(boolean dim) {
    	isDimmedBg = dim;
    }
    
    // R5
    public boolean isDimmedBg() {
    	return isDimmedBg;
    }
    
    // R5
    public void setDimmedFocus(boolean dim) {
    	useDimmedFocus = dim;
    }
    
    public boolean getDimmedFocus() {
    	return useDimmedFocus;
    }
}

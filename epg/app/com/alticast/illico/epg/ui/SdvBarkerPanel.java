package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.util.Date;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;

public class SdvBarkerPanel extends EpgPanel {

    public static final byte TYPE_ERROR = 0;
    public static final byte TYPE_KEEP_ALIVE    = 1;

    byte type;

    //->Kenneth[2017.9.27] R7.4 : Error Message 를 사용하기로 해서 완전히 바뀜
    short mid;
    short rid;
    String currentErrorCode;

    public SdvBarkerPanel(short mid, short rid, byte type) {
        if (Log.DEBUG_ON) Log.printDebug("SdvBarkerPanel("+mid+", "+rid+", "+type+")");
        this.type = type;
        this.mid = mid;
        this.rid = rid;
        String errorCode = getErrorCode(mid, rid);
        if (Log.DEBUG_ON) Log.printDebug("SdvBarkerPanel() : errorCode = "+errorCode);
        if (errorCode == null) return;
        currentErrorCode = errorCode;
        EpgCore.getInstance().showErrorMessage(errorCode, null);
    }

    // Error Message popup 이 닫히면서 호출됨.
    public void sdvErrorPopupClosed(String errorCode) {
        if (Log.DEBUG_ON) Log.printDebug("SdvBarkerPanel.sdvErrorPopupClosed("+errorCode+")");
        if (Log.DEBUG_ON) Log.printDebug("SdvBarkerPanel.sdvErrorPopupClosed() : currentErrorCode = "+currentErrorCode);
        ChannelBackground.getInstance().stopPanel();
        if (type == TYPE_KEEP_ALIVE) {
            if (Log.DEBUG_ON) Log.printDebug("SdvBarkerPanel.sdvErrorPopupClosed() : Call SDVSessionManager.actionKeepAlive()");
            SDVSessionManager.getInstance().actionKeepAlive();
        }
    }

    public String getErrorCode(short messageId, short responseId) {
        String errorCode = null;
        if (messageId == (short)0x0002 && responseId == (short)0x0001) {
            errorCode = "SDV001";
        } else if (messageId == (short)0x0002 && responseId == (short)0x8001) {
            errorCode = "SDV002";
        } else if (messageId == (short)0x0002 && responseId == (short)0x8002) {
            errorCode = "SDV003";
        } else if (messageId == (short)0x0002 && responseId == (short)0x8003) {
            errorCode = "SDV004";
        } else if (messageId == (short)0x0002 && responseId == (short)0x8004) {
            errorCode = "SDV005";
        } else if (messageId == (short)0x0002 && responseId == (short)0x8005) {
            errorCode = "SDV006";
        } else if (messageId == (short)0x0002 && responseId == (short)0x80FF) {
            errorCode = "SDV007";
        } else if (messageId == (short)0x0020 && responseId == (short)0x3000) {
            errorCode = "SDV008";
        } else if (messageId == (short)0x0020 && responseId == (short)0x3001) {
            errorCode = "SDV009";
        } else if (messageId == (short)0x0003 && responseId == (short)0x8001) {
            errorCode = "SDV010";
        } else if (messageId == (short)0x8005 && responseId == (short)0x0000) {
            errorCode = "SDV011";
        } else if (messageId == (short)0x8002 && responseId == (short)0x8001) {
            errorCode = "SDV012";
        } else if (messageId == (short)0x8002 && responseId == (short)0x8005) {
            errorCode = "SDV013";
        } else if (messageId == (short)0x8002 && responseId == (short)0x0001) {
            errorCode = "SDV014";
        } else if (messageId == (short)0x8002 && responseId == (short)0x80FF) {
            errorCode = "SDV015";
        } else if (messageId == (short)0x8002 && responseId == (short)0x8007) {
            errorCode = "SDV016";
        } else if (messageId == (short)0x0020 && responseId == (short)0x3002) {
            errorCode = "SDV017";
        }
        return errorCode;
    }

    public boolean handleKey(int code) {
        return false;
    }
    /*
    DataCenter dataCenter = DataCenter.getInstance();
    SdvBarkerPanelRenderer r = new SdvBarkerPanelRenderer();

    short mid;
    short rid;
    String errorCode = null;
    String titleKey;
    String buttonKey;
    String message;

    public SdvBarkerPanel() {
        setRenderer(r);
    }

    public SdvBarkerPanel(short mid, short rid, byte type) {
        setRenderer(r);

        this.type = type;

        this.mid = mid;
        this.rid = rid;

        SdvPopup sp = (SdvPopup) dataCenter.get(ConfigManager.SDV_POPUP_INSTANCE);
        if (sp != null) {
            SdvErrorMessage sem = sp.getMessage(mid, rid);
            if (sem != null) {
                errorCode = sem.errorCode;
                titleKey = sem.titleKey;
                buttonKey = sem.buttonKey;
            } else {
                Log.printDebug(App.LOG_HEADER+"SdvBarkerPanel: SdvErrorMessage is null");
            }
        } else {
            Log.printDebug(App.LOG_HEADER+"SdvBarkerPanel: SdvPopup is null");
        }
        if (errorCode == null) {
            Log.printWarning(App.LOG_HEADER+"SdvBarkerPanel: can't find title = " + Integer.toHexString(mid) + ", " + Integer.toHexString(rid));
            buttonKey = "sdv.close";
            titleKey = "sdv.problem";
        }

        SdvBarker sb = (SdvBarker) dataCenter.get(ConfigManager.SDV_BARKER_INSTANCE);
        if (sb != null) {
            message = sb.getMessage(mid, rid);
        } else {
            Log.printDebug(App.LOG_HEADER+"SdvBarkerPanel: SdvBarker is null");
        }
        if (message == null) {
            Log.printWarning(App.LOG_HEADER+"SdvBarkerPanel: can't find message = " + Integer.toHexString(mid) + ", " + Integer.toHexString(rid));
            message = dataCenter.getString("sdv.default_message");
        }

        r.setMessage(message);
    }


    public boolean handleKey(int code) {
        if (code == OCRcEvent.VK_ENTER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SdvBarkerPanel.handleKey() : VK_ENTER");
            ChannelBackground.getInstance().stopPanel();
            if (type == TYPE_KEEP_ALIVE) {
                SDVSessionManager.getInstance().actionKeepAlive();
            }
            return true;
        }
        return false;
    }


    class SdvBarkerPanelRenderer extends Renderer {

        protected Image[] icons = new Image[] {
            null,
            dataCenter.getImage("icon_g_check.png"),
            dataCenter.getImage("icon_noti_red.png"),
        };

        protected Image iButton = dataCenter.getImage("05_focus_dim.png");
        protected Image iFocus = dataCenter.getImage("05_focus.png");

        protected Image i_pop_sha = dataCenter.getImage("pop_sha.png");
        protected Image i_05_pop_glow_t = dataCenter.getImage("05_pop_glow_t.png");
        protected Image i_05_pop_glow_m = dataCenter.getImage("05_pop_glow_m.png");
        protected Image i_05_pop_glow_b = dataCenter.getImage("05_pop_glow_b.png");

        protected Image i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
        protected Image i_pop_high_350 = dataCenter.getImage("pop_high_350.png");

        protected Font fTitle = FontResource.BLENDER.getFont(24);
        protected Font fText = FontResource.BLENDER.getFont(20);
        protected Font fButton = FontResource.BLENDER.getFont(18);

        protected FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);
        protected FontMetrics fmText = FontResource.getFontMetrics(fText);

        protected Color cTitle = new Color(255, 203, 4);
        protected Color cText = Color.white;
        protected Color cButton = new Color(3, 3, 3);

        protected Color cPopup = new Color(23, 23, 23);

        protected String[] texts;

        protected int Y_GAP = 20;
        protected int TEXT_Y = 113;

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void prepare(UIComponent c) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SdvBarkerPanel.SdvBarkerPanelRenderer.prepare()");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public void setMessage(String s) {
            texts = getSplitMessage(s);
        }

        public String[] getSplitMessage(String s) {
            return TextUtil.split(TextUtil.replace(s, "|", "\n"), fmText, 320);
        }

        public void setMessage(String[] s) {
            texts = s;
        }

        public void paint(Graphics g, UIComponent c) {
            int height = 259;
            // shadow & glow
            g.drawImage(i_pop_sha, 306, height + 143 - 4, 350, 79, c);

            g.drawImage(i_05_pop_glow_t, 276, 113, c);
            g.drawImage(i_05_pop_glow_m, 276, 193, 410, height - 100, c);
            g.drawImage(i_05_pop_glow_b, 276, 193 - 100 + height, c);

            // popup body
            g.setColor(cPopup);
            g.fillRect(306, 143, 350, height);

            g.drawImage(i_pop_gap_379, 285, 181, c);
            g.drawImage(i_pop_high_350, 306, 143, c);

            String s = dataCenter.getString(titleKey);
            int tx = 481 - fmTitle.stringWidth(s) / 2;

            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(s, tx, 169);

            g.translate(306, 143);
            paintBody(g, c);
            g.translate(-306, -143);

            if (buttonKey != null) {
                g.drawImage(iFocus, 403, 318+35, c);
                g.setColor(cButton);
                g.setFont(fButton);
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttonKey), 481, 339+35);
            }

        }

        protected int paintMessage(Graphics g, UIComponent p, String[] msg, int y) {
            if (msg != null) {
                g.setColor(cText);
                g.setFont(fText);
                for (int i = 0; i < msg.length; i++) {
                    GraphicUtil.drawStringCenter(g, msg[i], 480-306, y);
                    y = y + Y_GAP;
                }
            }
            return y;
        }

        protected void paintBody(Graphics g, UIComponent p) {
            if (texts != null && texts.length > 0) {
                int fs = fText.getSize();
                int y = TEXT_Y - ((texts.length - 1) * Y_GAP - fs) / 2 - 3;
                paintMessage(g, p, texts, y);
            }
        }
    }
    */
}


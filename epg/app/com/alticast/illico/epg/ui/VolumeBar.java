package com.alticast.illico.epg.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.navigator.VolumeController;
import com.alticast.illico.epg.gui.VolumeBarRenderer;
import java.awt.Rectangle;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import java.util.*;
import com.alticast.ui.*;

public class VolumeBar extends UIComponent implements TVTimerWentOffListener {

    private static final long serialVersionUID = 8532398L;

    public int currentVolume;
    public boolean isMuted;
    public String currentVolumeString;

    private TVTimer timer = TVTimer.getTimer();
    private TVTimerSpec autoCloseTimer;
    private static final long DEFAULT_CLOSE_TIME = 3000L;

    private VolumeBarRenderer vbr = new VolumeBarRenderer();

    private LayeredUI ui;

    public int maxLevel;
    private int step;

    private long hideDelayNormal;
    private long hideDelayMute;

    public VolumeBar(int max, int increaseStep, int current) {
        this.maxLevel = max;
        this.step = increaseStep;

        this.currentVolume = current;
        this.currentVolumeString = String.valueOf(currentVolume);

        hideDelayNormal = DataCenter.getInstance().getLong("VOLUME_BAR_HIDE_DELAY", 3000L);
        hideDelayMute = DataCenter.getInstance().getLong("VOLUME_BAR_HIDE_DELAY_WHEN_MUTE", 10000L);

        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setDelayTime(hideDelayNormal);
        autoCloseTimer.addTVTimerWentOffListener(this);

        setRenderer(vbr);
        prepare();

        Rectangle r = getBounds();
        LayeredWindow window = new LayeredWindow();
        window.setSize(r.width, r.height);
        window.add(this);
        window.setVisible(true);
        setVisible(true);
        setLocation(0, 0);

        ui = WindowProperty.VOLUME_BAR.createLayeredWindow(window, r);
        ui.deactivate();
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VolumeBar.start()");
        ui.activate();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VolumeBar.stop()");
        ui.deactivate();
        VolumeController.getInstance().saveLevel();
    }

    public void setVolume(int relativeValue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VolumeBar.setVolume("+relativeValue+")");
        currentVolume = relativeValue;
        currentVolumeString = String.valueOf(currentVolume);
        start();
        startTimer();
    }

    public void setMuted(boolean mute) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VolumeBar.setMute("+mute+")");
        this.isMuted = mute;
        autoCloseTimer.setDelayTime(mute ? hideDelayMute : hideDelayNormal);
//        if (!mute) {
            startTimer();
            start();
//        } else {
//            start();
//        }
        repaint();
    }

    private void startTimer() {
        timer.deschedule(autoCloseTimer);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
//        if (!isMuted) {
            stop();
//        }
    }

}

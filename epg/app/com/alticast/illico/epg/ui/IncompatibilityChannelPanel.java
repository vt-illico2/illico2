package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.util.Date;
import java.util.Stack;
import java.awt.Color;
import org.ocap.ui.event.OCRcEvent;

public class IncompatibilityChannelPanel extends EpgPanel implements DataUpdateListener{
    //->Kenneth[2015.10.13] DDC-114 : Incompatibility page 가 보이는 이유를 화면에 보여줘야 한다.
    public static final int REASON_NO_UHD_PACKAGE = 0;
    public static final int REASON_EPG850 = 1;
    public static final int REASON_EPG851 = 2;
    public static final int REASON_UHD_COMPATIBLE = 3;

    public int reasonCode = REASON_UHD_COMPATIBLE;
    //<-

    public EpgConfig epgConfig;

    public Channel channel;
    private ScrollTexts scroll = new ScrollTexts(false);
    private ScrollTexts uhdFuncText = new ScrollTexts(false);
    public int screenData = -1;

    protected ClickingEffect clickEffect;

    IncompatibilityChannelPanelRenderer ren = new IncompatibilityChannelPanelRenderer();

    public IncompatibilityChannelPanel() {
        setRenderer(ren);
        // UHD functional text
        uhdFuncText.setForeground(Color.white);
        uhdFuncText.setRowHeight(17);
        uhdFuncText.setFont(FontResource.DINMED.getFont(14));
        uhdFuncText.setBounds(357, 196, 411, 58);
        uhdFuncText.setRows(3);
        uhdFuncText.setVisible(true);
        this.add(uhdFuncText);
        // 하단 설명
        scroll.setForeground(new Color(149, 149, 149));
        scroll.setRowHeight(17);
        scroll.setFont(FontResource.DINMED.getFont(16));
        scroll.setBounds(121, 413, 724, 56);
        scroll.setRows(2);
        scroll.setVisible(true);
        this.add(scroll);
        
        DataCenter.getInstance().addDataUpdateListener(ConfigManager.EPG_CONFIG_INSTANCE, this);
    }

    //DataUpdateListener implementation
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.dataUpdated("+key+")");
        setEpgConfig((EpgConfig)value);
    }

    //DataUpdateListener implementation
    public void dataRemoved(String key)  {
    }

    private void setEpgConfig(EpgConfig config) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.setEpgConfig("+config+")");
        this.epgConfig = config;
        if (epgConfig != null) {
            uhdFuncText.setContents(epgConfig.getUhdFuncText());
        }
    }

    //->Kenneth[2015.10.13] DDC-114 
    public static int resetReasonCode(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode("+ch+")");
        if (!Environment.SUPPORT_UHD && ch != null && ch.getDefinition() == Channel.DEFINITION_4K) {
            // 1. UHD package 가 없는 경우
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : returns REASON_NO_UHD_PACKAGE");
            return REASON_NO_UHD_PACKAGE;
        } 
        if (ch.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD) {
            boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : canPlayUhd = "+canPlayUhd);
            boolean hdcp22Supported = true;
            if (VideoOutputUtil.getInstance().existHdcpApi()) {
                hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : hdcp22Supported = "+hdcp22Supported);
            }
            if (canPlayUhd) {
                if (!hdcp22Supported) {
                    // 2. EPG 851
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : returns REASON_EPG851");
                    return REASON_EPG851;
                }
            } else {
                // 3. EPG 850
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : returns REASON_EPG850");
                return REASON_EPG850;
            }
        }
        // 4. default 로는 REASON_UHD_COMPATIBLE 
        // 사실 이게 필요한지는 모르겠지만 필터링이 안되는 경우는 팝업띄우지 않아도 되지 않을까? -> 외부에서 호출되므로 필요
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.resetReasonCode() : returns REASON_UHD_COMPATIBLE");
        return REASON_UHD_COMPATIBLE;
    }
    //<-

    public void setChannel(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.setChannel("+ch+")");
        this.channel = ch;
        screenData = ch.getScreenData();
        scroll.setContents(channel.getDescription());
        //->Kenneth[2015.10.13] DDC-114 
        // start() 가 아닌 setChannel() 에서 resetReasonCode()하는 이유는
        // 이미 incompatibilility panel 이 그려진 상태면 panel 새로 생성하지 않고 setChannel()하고 말기 때문.
        // start() 시에는 아래에서 당연히 setChannel()을 불러주고 있고.
        reasonCode = resetReasonCode(ch);
        //<-
    }

    public void start(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.start("+ch+")");
        setChannel(ch);
        setEpgConfig(ConfigManager.getInstance().epgConfig);
        super.start();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.stop()");
        super.stop();
    }

    public boolean handleKey(int code) {
        if (code == OCRcEvent.VK_ENTER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IncompatibilityChannelPanel.handleKey() : VK_ENTER");
        }
        return false;
    }
}

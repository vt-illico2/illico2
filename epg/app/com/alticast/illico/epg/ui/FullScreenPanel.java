package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.util.Date;
import java.util.Stack;

public abstract class FullScreenPanel extends EpgPanel implements ClockListener {

    public Stack stack = new Stack();

    public FullScreenPanel() {
    }

    public void start() {
        super.start();
        Clock.getInstance().addClockListener(this);
    }

    public void stop() {
        Clock.getInstance().removeClockListener(this);
        super.stop();
    }

    public void clockUpdated(Date date) {
        repaint();
    }

}

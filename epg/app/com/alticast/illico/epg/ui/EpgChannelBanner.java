package com.alticast.illico.epg.ui;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.navigator.ChannelZapper;
import com.alticast.illico.epg.gui.ChannelBannerRenderer;
import com.alticast.illico.epg.ParentalControl;
import com.alticast.illico.epg.util.Util;
import java.awt.Rectangle;
import java.awt.Image;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;
import java.util.*;
import com.alticast.ui.*;

/**
 * EpgChannelBanner.
 *
 * @author June Park
 */
public class EpgChannelBanner extends ChannelBanner implements LayeredKeyHandler {

    public String[] program;
    public String timeString;
    public int recordingStatus;
    public Image[] icons;
    public int iconX;
    public int iconLine;
    public String currentFilter;
    public boolean filtered = false;
    public boolean parentalBlocked;

    private ChannelBannerRenderer cbr = new ChannelBannerRenderer();

    private LayeredUI ui;
    private DataCenter dataCenter = DataCenter.getInstance();

    private RefreshEffect filterEffect;

    private static EpgChannelBanner instance = new EpgChannelBanner();

    public static EpgChannelBanner getInstance() {
        return instance;
    }

    private EpgChannelBanner() {
        setRenderer(cbr);
        prepare();

        Rectangle r = getBounds();
        LayeredWindow window = new LayeredWindow();

        window.setSize(r.width, r.height);
        window.add(this);
        window.setVisible(true);
        setVisible(true);
        setLocation(0, 0);

        ui = WindowProperty.CHANNEL_BANNER.createLayeredDialog(window, r, this);

        //->Kenneth[2015.6.11] R5
        favIcon.setLocation(83, 97);
        lockIcon.setLocation(83, 97);
        //favIcon.setLocation(86, 97);
        //lockIcon.setLocation(86, 97);

        filterEffect = new RefreshEffect(this, 16, DataCenter.getInstance().getImage("sweep.png"));
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.start()");
        ui.activate();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.stop()");
        ui.deactivate();
    }

    public boolean isActive() {
        return ui.isActive();
    }

    public boolean handleKeyEvent(UserEvent e) {
        switch (e.getCode()) {
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.handleKeyEvent() : VK_ENTER");
                ChannelZapper.getInstance().handleEnterKey();
                return true;
            case OCRcEvent.VK_EXIT:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.handleKeyEvent() : VK_EXIT");
                ChannelZapper.getInstance().handleExitKey();
                return true;
        }
        return false;
    }

    protected void readyData(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.readyData("+ch+")");
        Formatter formatter = Formatter.getCurrent();
        Program p = dm.getCurrentProgram(ch);
        if (p != null) {
            parentalBlocked = ParentalControl.getLevel(p) != ParentalControl.OK;
            int width = !parentalBlocked ? ChannelBannerRenderer.TITLE_WIDTH : ChannelBannerRenderer.TITLE_WIDTH - 20;
            program = TextUtil.split(ParentalControl.getTitle(p), cbr.fmProgram, width, 2);
            if (p instanceof FakeProgram || p instanceof NullProgram) {
                timeString = "";
            } else {
                timeString = formatter.getTime(p.getStartTime()) + " ~ "
                           + formatter.getTime(p.getEndTime());
            }
            // NOTE - 채널이 아닌 프로그램
            recordingStatus = RecordingStatus.getInstance().getStatus(p);

            Image rating = cbr.ratingImages[p.getRatingIndex()];
            int iw = 0;
			//->Kenneth : 4K by June
            int definition = Util.getDefinition(ch, p);
            icons = new Image[3];
            int iconIndex = 0;
			//->Kenneth : 4K by June
            //->Kenneth[2015.2.4] 4K
            if (definition == Constants.DEFINITION_HD) { 
                iw += 26;
                icons[iconIndex++] = cbr.iHd;
            } else if (definition == Constants.DEFINITION_4K) { 
                iw += 28;
                icons[iconIndex++] = cbr.iUhd;
            }
            if (rating != null) {
                iw += 26;
                icons[iconIndex++] = rating;
            }
            if (p.isCaptioned()) {
                iw += 26;
                icons[iconIndex++] = cbr.iCC;
            }
            if (iw > 0) {
                int sw = cbr.fmProgram.stringWidth(program[0]);
                if (program.length > 1 || sw + iw + 3 > width) {
                    if (program.length > 1) {
                        program[1] = TextUtil.shorten(program[1], cbr.fmProgram, width - iw - 3);
                        iconX = cbr.fmProgram.stringWidth(program[1]) + 3;
                        iconLine = 1;
                    } else {
                        iconX = 0;
                        iconLine = 1;
                    }
                } else {
                    iconX = sw + 3;
                    iconLine = 0;
                }
            } else {
                icons = null;
            }
        } else {
            recordingStatus = RecordingStatus.NOT_RECORDING;
            icons = null;
            program = null;
            timeString = null;
            parentalBlocked = false;
        }
        // TODO - 성능 향상
        String s = ChannelController.get(0).getCurrentFilterKey();
        setFilter(s);
    }

    private void setFilter(String filterKey) {
        Object shortKey = dataCenter.get(filterKey + ".short");
        if (shortKey != null) {
            currentFilter = dataCenter.get("epg.filtered_by") + shortKey.toString();
        } else {
            currentFilter = dataCenter.get("epg.filtered_by") + dataCenter.getString(filterKey);
        }
        filtered = !ChannelController.ALL_FILTER_KEY.equals(filterKey);
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgChannelBanner. timerWentOff");
        }
        stop();
    }

    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        if (!isActive()) {
            return;
        }
        stopTimer();
        switch (id) {
            case ADDED_TO_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : ADDED_TO_FAVOURITES");
                favIcon.showIcon();
                break;
            case REMOVED_FROM_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : REMOVED_FROM_FAVOURITES");
                favIcon.hideIcon();
                break;
            case ADDED_TO_RESTRICTED:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : ADDED_TO_RESTRICTED");
                lockIcon.showIcon();
                break;
            case REMOVED_FROM_RESTRICTED:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : REMOVED_FROM_RESTRICTED");
                lockIcon.hideIcon();
                break;
            case FILTER_WILL_BE_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : FILTER_WILL_BE_CHANGED");
                filterEffect.readyToStart(89-3, 87-17, 185, 21);
                break;
            case FILTER_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug("EpgChannelBanner.notifyEpgEvent() : FILTER_CHANGED");
                String filterKey = data.toString();
                setFilter(filterKey);
                filterEffect.start();
                break;
        }
        startTimer();
    }

}

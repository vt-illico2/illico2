package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.PopupController;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import java.awt.*;
import java.awt.event.KeyEvent;
import javax.tv.util.*;
import com.alticast.ui.*;


public class NoFavouritesPopup extends Popup {

    private TextBox message = new TextBox(TextBox.ALIGN_CENTER);

    public NoFavouritesPopup() {
        DataCenter dataCenter = DataCenter.getInstance();
        PopupRenderer r = new PopupRenderer();
        setRenderer(r);

        message.setFont(FontResource.BLENDER.getFont(18));
        message.setRowHeight(18);
        message.setForeground(Color.white);
        message.setBounds(320, 205, 324, 145);
        this.add(message);
        message.setContents(dataCenter.getString("epg.fav_empty_popup_msg"));

        this.title = dataCenter.getString("epg.fav_empty_popup_title");
        this.buttons = CONFIRM_BUTTONS;
    }

}

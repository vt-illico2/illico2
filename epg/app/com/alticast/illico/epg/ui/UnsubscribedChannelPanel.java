package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.isa.*;
import java.util.Date;
import java.util.Stack;
import java.awt.Color;
import org.ocap.ui.event.OCRcEvent;

public class UnsubscribedChannelPanel extends EpgPanel {

    public Channel channel;
    private ScrollTexts scroll = new ScrollTexts(false);
    private boolean isa;
    public int screenData = -1;

    protected ClickingEffect clickEffect;

    UnsubscribedChannelPanelRenderer ren = new UnsubscribedChannelPanelRenderer();

    public UnsubscribedChannelPanel() {
        setRenderer(ren);
        scroll.setForeground(new Color(149, 149, 149));
        scroll.setRowHeight(17);
        scroll.setFont(FontResource.DINMED.getFont(16));
        scroll.setBounds(121, 413, 724, 56);
        scroll.setRows(2);
        scroll.setVisible(true);

        this.add(scroll);
    }

    public void setChannel(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UnsubscribedChannelPanel.setChannel("+ch+")");
        this.channel = ch;
        screenData = ch.getScreenData();
        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        isa = ch.isISAPurchasable();
        //isa = ch.isISASupported();
        //<-
        scroll.setContents(channel.getDescription());
    }

    public void start(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UnsubscribedChannelPanel.start("+ch+")");
        setChannel(ch);
        super.start();
    }

    //->Kenneth[2016.8.24] CYO Bundling/A la carte
    public boolean isISAPurchasable() {
    //public boolean isISASupported() {
    //<-
        return isa;
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UnsubscribedChannelPanel.stop()");
        super.stop();
    }

    protected void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(ren.getButtonFocusBounds());
    }

    public boolean handleKey(int code) {
        if (code == OCRcEvent.VK_ENTER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UnsubscribedChannelPanel.handleKey() : VK_ENTER");
            startClickingEffect();
//            if (purchasable) {
                EpgCore.getInstance().showSubscriptionPopup(channel, ISAService.ENTRY_POINT_EPG_E03);
//            } else {
//                EpgCore.getInstance().startUnboundAppWithPin("Options", null);
//            }
            return true;
        }
        return false;
    }


}

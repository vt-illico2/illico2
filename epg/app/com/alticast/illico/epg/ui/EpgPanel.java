package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public abstract class EpgPanel extends ChannelBackgroundPanel {

    protected EpgFooter footer = new EpgFooter();

    public EpgPanel() {
        footer.setBounds(0, 488, 906, 25);
        this.add(footer);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug("["+this+"]EpgPanel.start()");
        prepare();
        setVisible(true);
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug("["+this+"]EpgPanel.stop()");
        setVisible(false);
    }

}

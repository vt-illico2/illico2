package com.alticast.illico.epg.ui;

import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.util.*;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;

public class EpgFooter extends Footer {

    static Hashtable keyNameToCode = new Hashtable();
    static {
        putKeyCode(PreferenceService.BTN_A, KeyCodes.COLOR_A);
        putKeyCode(PreferenceService.BTN_B, KeyCodes.COLOR_B);
        putKeyCode(PreferenceService.BTN_C, KeyCodes.COLOR_C);
        putKeyCode(PreferenceService.BTN_D, KeyCodes.COLOR_D);
        putKeyCode(PreferenceService.BTN_BACK, OCRcEvent.VK_LAST);
        putKeyCode(PreferenceService.BTN_EXIT, OCRcEvent.VK_EXIT);

        putKeyCode(PreferenceService.BTN_GUIDE, OCRcEvent.VK_GUIDE);
        putKeyCode(PreferenceService.BTN_INFO, OCRcEvent.VK_INFO);
        putKeyCode(PreferenceService.BTN_LIST, OCRcEvent.VK_LIST);
        putKeyCode(PreferenceService.BTN_MENU, OCRcEvent.VK_MENU);
        putKeyCode(PreferenceService.BTN_OK, OCRcEvent.VK_ENTER);
        putKeyCode(PreferenceService.BTN_PAGE, new int[] { OCRcEvent.VK_PAGE_UP, OCRcEvent.VK_PAGE_DOWN});
        putKeyCode(PreferenceService.BTN_SEARCH, KeyCodes.SEARCH);
        putKeyCode(PreferenceService.BTN_WIDGET, KeyCodes.WIDGET);
        //->Kenneth[2015.4.13] R5 
        putKeyCode(PreferenceService.BTN_PRO, KeyCodes.LITTLE_BOY);
    }

    Hashtable codeToIcon = new Hashtable();

    public EpgFooter() {
        super();
        setFont(FontResource.DINMED.getFont(15));
    }

    public EpgFooter(short align, boolean showDivider) {
        super(align, showDivider);
        setFont(FontResource.DINMED.getFont(15));
    }

    public Image addButton(String buttonKey, String textKey) {
        Image icon = super.addButton(buttonKey, textKey);
        if (icon != null) {
            int[] codes = (int[]) keyNameToCode.get(buttonKey);
            if (codes != null) {
                for (int i = 0; i < codes.length; i++) {
                    codeToIcon.put(new Integer(codes[i]), icon);
                }
            }
        }
        return icon;
    }

    public void reset() {
        super.reset();
        codeToIcon.clear();
    }

    public void clickAnimationByKeyCode(int keyCode) {
        if (isVisible()) {
            Image icon = (Image) codeToIcon.get(new Integer(keyCode));
            if (icon != null) {
                clickAnimation(icon);
            }
        }
    }

    private static void putKeyCode(String keyName, int code) {
        putKeyCode(keyName, new int[] { code });
    }

    private static void putKeyCode(String keyName, int[] codes) {
        keyNameToCode.put(keyName, codes);
    }


}

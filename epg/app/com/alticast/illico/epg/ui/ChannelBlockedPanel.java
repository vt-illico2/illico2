package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.effect.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.util.Date;
import java.util.Stack;
import java.util.Hashtable;
import java.awt.*;

public class ChannelBlockedPanel extends EpgPanel implements RightFilterListener {

    private static final int STATE_NONE = 0;
    private static final int STATE_PIN  = 1;

    public short type;
    public Channel channel;

    private TextBox message = new TextBox(TextBox.ALIGN_CENTER);
    private TextBox suspend = new TextBox(TextBox.ALIGN_CENTER);
    private TextBox desc = new TextBox(TextBox.ALIGN_CENTER);

    private ImageLabel icon;
    private Effect unlockEffect;

    public ChannelBlockedPanel(Channel ch, short type) {
        if (Log.DEBUG_ON) Log.printDebug("ChannelBlockedPanel("+ch+", "+type+")");
        setRenderer(new ChannelBlockedPanelRenderer());
        icon = new ImageLabel(DataCenter.getInstance().getImage("02_block_lock.png"));
        FrameworkMain.getInstance().getImagePool().waitForAll();
        icon.setLocation(426, 190);
        this.add(icon);
        unlockEffect = new LockEffect(icon, 15, 41, 0, -41);
        unlockEffect.setRequestor(this);

        message.setFont(FontResource.BLENDER.getFont(24));
        message.setForeground(Color.white);
        message.setBounds(100, 312, 760, 30);
        this.add(message);

        suspend.setFont(FontResource.BLENDER.getFont(19));
        suspend.setForeground(new Color(182, 182, 182));
        suspend.setRowHeight(20);
        suspend.setBounds(100, 360, 760, 70);
        suspend.setContents(DataCenter.getInstance().getString("epg.suspend_desc"));
        this.add(suspend);

        desc.setFont(FontResource.BLENDER.getFont(17));
        desc.setForeground(new Color(150, 150, 150));
        desc.setRowHeight(18);
        desc.setBounds(100, 420, 760, 70);
        desc.setContents(DataCenter.getInstance().getString("epg.modify_pc_desc"));
        this.add(desc);

        message.setContents(DataCenter.getInstance().getString("epg.press_b_unblock_" + type));
        footer.reset();
        this.channel = ch;
        this.type = type;

        state = STATE_NONE;
    }

    public void stop() {
        super.stop();
        hidePin();
    }

    public boolean handleKey(int code) {
        if (code == KeyCodes.COLOR_B) {
            if (Log.DEBUG_ON) Log.printDebug("ChannelBlockedPanel.handleKey() : COLOR_B");
            showPin();
            return true;
        }
        return false;
    }

    private synchronized void showPin() {
        if (Log.DEBUG_ON) Log.printDebug("ChannelBlockedPanel.showPin()");
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        if (ps == null) {
            return;
        }
        try {
            ps.checkRightFilter(this, App.NAME, new String[] { RightFilter.ALLOW_BYPASS_BLOCKED_CHANNEL }, null, null);
            state = STATE_PIN;
        } catch (Exception ex) {
        }
    }

    private synchronized void hidePin() {
        if (Log.DEBUG_ON) Log.printDebug("ChannelBlockedPanel.hidePin()");
        if (state != STATE_PIN) {
            return;
        }
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.hidePinEnabler();
        } catch (Exception ex) {
        }
        state = STATE_NONE;
    }

    public void receiveCheckRightFilter(int response) {
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            unlockEffect.startLater();
        }
    }

    public String[] getPinEnablerExplain() {
        //->Kenneth[2015.11.23] VDTRMASTER-5718 : EpgCore 의 것을 사용한다.
        return EpgCore.getInstance().getBlockedChannelExplain(channel, false);
        /*
        //->Kenneth[2015.8.21] VDTRMASTER-5604
        if (channel == null) {
            return TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
        }
        String[] explain = null;
        try {
            explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
            String fullName = channel.getFullName();
            if (fullName == null || fullName.length() == 0) {
                explain[1] = TextUtil.replace(explain[1], "FULL_NAME", "");
            } else {
                //->Kenneth[2015.10.9] VDTRMASTER-5694 : EpgCore 에는 5632 로 수정되었는데 여기에도 있네.
                if (fullName.length() > 14) {
                    fullName = fullName.substring(0, 14)+"...";
                    if (Log.DEBUG_ON) Log.printDebug("ChannelBlockedPanel.getPinEnablerExplain() : Ellipsized fullName = "+fullName);
                }
                //<-
                explain[1] = TextUtil.replace(explain[1], "FULL_NAME", fullName);
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return explain;
        //return TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
        //<-
        */
    }

    public boolean skipAnimation(Effect effect) {
        return false;
    }

    public void animationStarted(Effect effect) {
    }

    public void animationEnded(Effect effect) {
        if (type == ParentalControl.BLOCK_BY_CHANNEL) {
            long dur = ParentalControl.getInstance().channelUnblocked(channel);
            DataCenter dc = DataCenter.getInstance();
            PopupController.getInstance().showAutoClose(
                    dc.getString("epg.unblock_popup_title"),
                    dc.getString("epg.unblock_popup_msg_" + dur / ParentalControl.hourUnit),
                    null, null, Popup.ICON_CHECK);
        } else if (type == ParentalControl.BLOCK_BY_RATING) {
            ParentalControl.getInstance().programUnblocked(channel);
        }
        ChannelController.get(0).releaseChannelBlock(channel);
    }

}

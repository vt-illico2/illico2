package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import java.awt.*;
import java.util.*;
import java.awt.event.KeyEvent;
import javax.tv.util.*;
import com.alticast.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
public class TutorialPopup extends Popup {
    private TutorialPopupRenderer renderer = new TutorialPopupRenderer();
    private GridEpg gridEpg;
    private int focusIndex = 0;

    public TutorialPopup(GridEpg gridEpg) {
        this.gridEpg = gridEpg;
        setRenderer(renderer);
    }

    public void start() {
        focusIndex = 0;
        super.start();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TotorialPopup.handleKey("+code+")");
        try {
            switch (code) {
                case OCRcEvent.VK_LEFT:
                    focusIndex --;
                    if (focusIndex < 0) {
                        focusIndex = 0;
                    }
                    repaint();
                    break;
                case OCRcEvent.VK_RIGHT:
                    focusIndex ++;
                    if (focusIndex >= 3) {
                        focusIndex = 2;
                    }
                    repaint();
                    break;
                case OCRcEvent.VK_ENTER:
                    if (focusIndex == 0) {
                        stop();
                    } else if (focusIndex == 1) {
                        //->Kenneth[2015.6.11] : 주환 이슈에 보면 learn more 한 이후에는 다시 팝업 뜨면 안됨
                        Tutorial.getInstance().doNotDisplayAgain();
                        // help app 으로 이동
                        stop();
                        //EpgCore.getInstance().startUnboundApp("Help", new String[] { MonitorService.REQUEST_APPLICATION_HOT_KEY, "TV", null} );
                        //->Kenneth[2015.9.23] VDTRMASTER-5659 : help root 로 가도록 한다.
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TotorialPopup.handleKey() : go to Help");
                        EpgCore.getInstance().startUnboundApp("Help", new String[] {null, App.NAME, "TV", ""} );
                        /*
                        String categoryId = DataCenter.getInstance().getString("TUTORIAL_HELP_LINK_CATEGORY_ID");
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TotorialPopup.handleKey() : TUTORIAL_HELP_LINK_CATEGORY_ID = "+categoryId);
                        if (categoryId == null) {
                            EpgCore.getInstance().startUnboundApp("Help", new String[] {null, App.NAME, "TV", ""} );
                        } else {
                            EpgCore.getInstance().startUnboundApp("Help", new String[] {null, App.NAME, "TV", categoryId} );
                        }
                        */
                        //<-
                    } else if (focusIndex == 2) {
                        Tutorial.getInstance().doNotDisplayAgain();
                        stop();
                    }
                    break;
                case OCRcEvent.VK_EXIT:
                    stop();
                    break;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return true;
    }

    // Popup.startEffect()를 override 해서 animation 안 하게 한다.
    // 왜냐하면 Grid 위에 에러팝업 뜬 상태에서 tutorial 이 뜨면서 에러팝업 위에 animation
    // 그려진후 사라지는 현상이 있기 때문이다.
    public void startEffect() {
        setVisible(true);
    }

    // Renderer
    class TutorialPopupRenderer extends Renderer {
        DVBColor fullBgColor = new DVBColor(12, 12, 12, 230);
        Color titleColor = new Color(252, 202, 4);
        Color buttonNameColor = new Color(3, 3, 3);
        Font f18 = FontResource.BLENDER.getFont(18);
        Font f24 = FontResource.BLENDER.getFont(24);
        FontMetrics fm18 = FontResource.getFontMetrics(f18);
        FontMetrics fm24 = FontResource.getFontMetrics(f24);
        Image popupBg = DataCenter.getInstance().getImage("pop_759_bg.png");
        Image normalButtonBg = DataCenter.getInstance().getImage("05_focus_dim.png");
        Image focusButtonBg = DataCenter.getInstance().getImage("05_focus.png");
        String title;
        String firstButtonName;
        String secondButtonName;
        String thirdButtonName;
        Image tutorialImage;

        public void prepare(UIComponent c) {
            title = Tutorial.getInstance().getTitle();
            firstButtonName = Tutorial.getInstance().getFirstButtonName();
            secondButtonName = Tutorial.getInstance().getSecondButtonName();
            thirdButtonName = Tutorial.getInstance().getThirdButtonName();
            tutorialImage = Tutorial.getInstance().getTutorialImage();
//            title = "Title";
//            firstButtonName = "Close";
//            secondButtonName = "Learn more";
//            thirdButtonName = "Don't display again";
//            tutorialImage = focusButtonBg;
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paint(Graphics g, UIComponent c) {
            try {
                // full bg
                g.setColor(fullBgColor);
                g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
                // popup bg
                g.drawImage(popupBg, 71, 26, c);
                // title
                if (title != null) {
                    g.setColor(titleColor);
                    g.setFont(f24);
                    g.drawString(title, 479-fm24.stringWidth(title)/2, 84);
                }
                // tutorial popup
                if (tutorialImage != null) {
                    //->Kenneth[2015.9.23] VDTRMASTER-5657
                    g.drawImage(tutorialImage, 120, 107, 719, 300, c);
                    //g.drawImage(tutorialImage, 100, 107, 719, 300, c);
                    //<-
                }
                // 하단 버튼
                g.setColor(buttonNameColor);
                g.setFont(f18);
                int xGap = 164;
                for (int i = 0 ; i < 3 ; i++) {
                    Image btnBg = null;
                    if (focusIndex == i) {
                        btnBg = focusButtonBg;
                    } else {
                        btnBg = normalButtonBg;
                    }
                    g.drawImage(btnBg, 237+i*xGap, 422, c);
                    String name = null;
                    if (i == 0) {
                        name = firstButtonName;
                    } else if (i == 1) {
                        name = secondButtonName;
                    } else if (i == 2) {
                        name = thirdButtonName;
                    }
                    if (name != null) {
                        g.drawString(name, 315-fm18.stringWidth(name)/2+i*xGap, 443);
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }
}

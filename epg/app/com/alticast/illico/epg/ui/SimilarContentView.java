/**
 * 
 */
package com.alticast.illico.epg.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.Hashtable;
import java.util.Vector;

import org.dvb.event.UserEvent;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.DVBColor;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;
import org.havi.ui.event.HRcEvent;

import com.alticast.illico.epg.App;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.ChannelDatabase;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.similar.SimilarContentManager;
import com.alticast.illico.epg.data.similar.type.SError;
import com.alticast.illico.epg.data.similar.type.SHead;
import com.alticast.illico.epg.data.similar.type.SimilarContent;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.util.KeyCodes;
import org.ocap.ui.event.OCRcEvent;

/**
 * @author zestyman
 *
 */
public class SimilarContentView extends UIComponent implements Runnable, LayeredKeyHandler {

	public static final int MODE_INTRO = 0;
	public static final int MODE_OPEN = 1;
	public static final int MODE_CLOSE = 2;
	public static final String VOD_APP_NAME = "VOD";
	
	private final int NORMAL_FRAMES = 12;
	private final int INTRO_FRAMES = 8;
	
	private final long STEP_DELAY = 40;
	private final long SCROLL_DELAY = 175;
	
	
	/** SharedMemory key to store root window. */
    private static final String KEY_ROOT_WINDOW = "Effect.rootWindow";
	
	private OptionPanel opPanel;
	private boolean isAnimating;
	private int mode;
	private boolean hasFocus;

	private SimilarContent[] contents;
	private String[] shortenTitles = new String[5];
	private SHead head;

	private DVBBufferedImage background;
	private DVBBufferedImage contentDVBImage;
	private static DVBBufferedImage image;
	private static DVBBufferedImage offScreen;
	private Toolkit toolKit = Toolkit.getDefaultToolkit();

	private Rectangle mixClipBounds = new Rectangle(46, 263, 558, 210);
	private Rectangle landscapeClipBounds = new Rectangle(46, 326, 558, 144);
	private Rectangle adultClipBounds = new Rectangle(46, 332, 558, 144);
	private Rectangle clipBounds = mixClipBounds;

	private DVBAlphaComposite[] openAlpha;
	private DVBAlphaComposite[] closeAlpha;

	private int[] openPosY;
	private int[] closePosY;
	private int[] introPosY;
	private int[] landOpenPosY;
	private int[] landClosePosY;
	

	private int frames;
	private int frameCount;
	
	private boolean isRunningThread;
	private int scroll;
	private Rectangle scrollBounds = new Rectangle(0, 0, 0, 0);
	private Container root;
	private LayeredUI ui;
	private Channel ch;
	private Image chLogo;
	private boolean isAllLandscape;
	private boolean updatePos;
	private boolean isAdult;

	public SimilarContentView() {

		renderer = new SimilarContentRenderer();

		setRenderer(renderer);

		if (background == null) {
			SharedMemory sm = SharedMemory.getInstance();
			synchronized (sm) {
				background = getFullScreenImage(sm, "DVBBufferedImage.1");
				image      = getFullScreenImage(sm, "DVBBufferedImage.2");
	            offScreen  = getFullScreenImage(sm, "DVBBufferedImage.3");
			}
		}
		
		if (contentDVBImage == null) {
			SharedMemory sm = SharedMemory.getInstance();
			synchronized (sm) {
				contentDVBImage = getFullScreenImage(sm, "DVBBufferedImage.SIMILAR");
			}
		}

		openAlpha = new DVBAlphaComposite[NORMAL_FRAMES];
		for (int i = 0; i < NORMAL_FRAMES; i++) {
			if (i == 0) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.25f);
			} else if (i == 1) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.5f);
			} else if (i == 2) {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.82f);
			} else {
				openAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 1f);
			}

		}

		closeAlpha = new DVBAlphaComposite[NORMAL_FRAMES];
		for (int i = 0; i < NORMAL_FRAMES; i++) {
			if (i == 10) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.5f);
			} else if (i == 11) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.25f);
			} else if (i == 9) {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.82f);
			} else {
				closeAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 1f);
			}

		}

		int fcs = (NORMAL_FRAMES - 1) * (NORMAL_FRAMES - 1);
		int openDiffY = 291 - 414;
		int closeDiffY = 414 - 291;

		openPosY = new int[NORMAL_FRAMES];
		closePosY = new int[NORMAL_FRAMES];
		landOpenPosY = new int[NORMAL_FRAMES]; 
		landClosePosY = new int[NORMAL_FRAMES]; 
		introPosY = new int[INTRO_FRAMES];

		for (int i = 0; i < NORMAL_FRAMES; i++) {
			openPosY[i] = 414 + Math.round((float) openDiffY * i * i / fcs);
			closePosY[i] = 291 + Math.round((float) closeDiffY * i * i / fcs);
			landOpenPosY[i] = Math.round((float) 33 * i * i / fcs);
			landClosePosY[i] = landOpenPosY[i] - 33;
		}
		
		fcs = (INTRO_FRAMES - 1) * (INTRO_FRAMES - 1);
		int introDiffY = 414 - 482;
		for (int i = 0; i < INTRO_FRAMES; i++) {
			introPosY[i] = 482 + Math.round((float) introDiffY * i * i / fcs);
		}
		
		LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.add(this);
		
        ui = WindowProperty.SIMILARE_VIEW.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        
		setVisible(false);
		ui.activate();
	}

	public void init() {
		mode = MODE_INTRO;
		hasFocus = false;
		contents = null;
		head = null;
		isAllLandscape = false;
		updatePos = false;
		isAdult = false;
		prepare();
	}
	
	public void stop() {
		ui.deactivate();
		opPanel = null;
		
		WindowProperty.dispose(ui);
	}

	public void startAnimation(int mode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentView, startAnimation, mode=" + mode);
		}
		this.mode = mode;
		
		if (opPanel != null) {
			if (mode == MODE_OPEN && hasContents()) {
				opPanel.setDimmedBg(true);
			}
		}
		if (!hasContents() && !isAdult) {
			animationEnded();
			return;
		}
		
		if (!isAdult) {
			if (!updatePos) {
				// check all landscape
				isAllLandscape = true;
				for(int i = 0; i < contents.length; i++) {
					if (!contents[i].isChannelContent() && contents[i].getPoster() != null 
							&& contents[i].getPoster().getHeight(null) > 100) {
						isAllLandscape = false;
						break;
					} else if (!contents[i].isChannelContent() && contents[i].getPoster() == null) {
						isAllLandscape = false;
						break;
					}
				}
				
				if (isAllLandscape) {
					int fcs = (NORMAL_FRAMES - 1) * (NORMAL_FRAMES - 1);
					int openDiffY = 363 - 414;
					int closeDiffY = 414 - 363;

					for (int j = 0; j < NORMAL_FRAMES; j++) {
						openPosY[j] = 414 + Math.round((float) openDiffY * j * j / fcs);
						closePosY[j] = 363 + Math.round((float) closeDiffY * j * j / fcs);
						landOpenPosY[j] = 0;
						landClosePosY[j] = 0;
					}
					
					clipBounds = adultClipBounds;
					updatePos = true;
				}
			}
		} else {
			int fcs = (NORMAL_FRAMES - 1) * (NORMAL_FRAMES - 1);
			int openDiffY = 331 - 482;
			int closeDiffY = 482 - 331;

			for (int j = 0; j < NORMAL_FRAMES; j++) {
				openPosY[j] = 482 + Math.round((float) openDiffY * j * j / fcs);
				closePosY[j] = 331 + Math.round((float) closeDiffY * j * j / fcs);
				landOpenPosY[j] = 0;
				landClosePosY[j] = 0;
			}
			
			clipBounds = adultClipBounds;
		}
		
		if (EventQueue.isDispatchThread()) {
			run();
		} else {
			EventQueue.invokeLater(this);
		}
	}

	public void setHasFocus(boolean focus) {
		hasFocus = focus;
		if (contents != null) {
			this.focus = contents.length - 1;
		}
	}

	public boolean hasFocus() {
		return hasFocus;
	}

	public void animationStarted() {
		setVisible(false);
		
		if (mode == MODE_INTRO) {
			frames = INTRO_FRAMES;
		} else {
			frames = NORMAL_FRAMES;
		}
		frameCount = frames - 1;
	}

	public void animationEnded() {
		if (opPanel != null) {
			if (mode != MODE_OPEN || !hasContents()) {
				opPanel.setDimmedBg(false);
			}
		}
		setVisible(true);
		
		if (hasContents() && mode == MODE_OPEN) {
			new Thread("SCRELL_THREAD") {
				public void run() {
					isRunningThread = true;
					while (isRunningThread) {
						
						try {
							Thread.sleep(SCROLL_DELAY);
						} catch (InterruptedException e) {
						}
						
						if (scrollBounds.width != 0) {
							scroll++;
							repaint(scrollBounds.x, scrollBounds.y, scrollBounds.width, scrollBounds.height);
						}
					}
				};
			}.start();
		} else {
			isRunningThread = false;
		}
	}

	public boolean isAnimating() {
		return isAnimating;
	}

	public boolean isOpened() {
		return mode == MODE_OPEN;
	}

	public void resetScroll() {
		scroll = 0;
	}

	public void setElements(SimilarContent[] contents, Program p) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContent, setElements, contents=" + contents + ", length=" + 
					(contents != null ? contents.length : 0));
		}

		if (contents == null || contents.length == 0) {
			this.contents = contents;
			return;
		}
		
		SimilarContent sc = new SimilarContent();
		sc.makeChannelContent();
		
		ch = ChannelDatabase.current.getChannelByName(p.callLetter);
		chLogo = ch.getScaledLogo();
		
		Vector v = new Vector();
		v.add(sc);
		
		for (int i = 0; i < contents.length && i < 4; i++) {
            //-> [6.17] 쥴리와 상의 후 원복
            /*
			contents[i].retrieveImageDirect();
			if (contents[i].getPoster() != null) {
				v.add(contents[i]);
			}
            */
            v.add(contents[i]);
            //<-
		}
		
		this.contents = new SimilarContent[v.size()];
		v.toArray(this.contents);
		
		FontMetrics fm16 = FontResource.getFontMetrics(FontResource.BLENDER.getFont(16));
		for (int i = 0; i < this.contents.length; i++) {
			shortenTitles[i] = TextUtil.shorten(this.contents[i].getTitle(), fm16, 87);
		}
	}
	
    /*
	public void addChannelContent(Program p) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContent, addChannelContent");
		}
		SimilarContent sc = new SimilarContent();
		sc.makeChannelContent();
		
		ch = ChannelDatabase.current.getChannelByName(p.callLetter);
		chLogo = ch.getScaledLogo();
		
		Vector v = new Vector();
		v.add(sc);
		
		this.contents = new SimilarContent[v.size()];
		v.toArray(this.contents);
		
		FontMetrics fm16 = FontResource.getFontMetrics(FontResource.BLENDER.getFont(16));
		for (int i = 0; i < this.contents.length; i++) {
			shortenTitles[i] = TextUtil.shorten(this.contents[i].getTitle(), fm16, 87);
		}
	}
    */
	
	public boolean hasContents() {
		if (contents != null && contents.length > 0) {
			return true;
		}
		return false;
	}
	
	public boolean needButton() {
		if (contents != null || head != null) {
			return true;
		}
		return false;
	}

	public void setHead(SHead h) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentView, setHead, head=" + h);
		}
		head = h;
	}

	public void setOptionPanel(OptionPanel opPanel) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentView, setOptionPanel, opPanel=" + opPanel);
		}
		this.opPanel = opPanel;
	}
	
	public void setAdultMode(boolean adult) {
		if (Log.DEBUG_ON) {
			Log.printDebug("SimilarContentView, setAdultMode, adult=" + adult);
		}
		isAdult = true;
		contents = new SimilarContent[0];
	}
	
	public boolean isAdultMode() {
		return isAdult;
	}

	public boolean handleKeyEvent(UserEvent event) {
		Log.printDebug("SimilarContent, handleKeyEvent, event=" + event + ", hasFocus=" + hasFocus);
		
		if (event.getType() != KeyEvent.KEY_PRESSED || !hasFocus) {
			return false;
		}
		
		int keyCode = event.getCode();

		switch (keyCode) {
		case HRcEvent.VK_UP:
		case HRcEvent.VK_DOWN:
			return true;
		case HRcEvent.VK_LEFT:
			if (contents != null) {
				if (focus > 0) {
					focus--;
					resetScroll();
					repaint();
				}
			}
			return true;
		case HRcEvent.VK_RIGHT:
			if (contents != null) {
				if (focus < contents.length - 1) {
					focus++;
				} else {
					setHasFocus(false);
					opPanel.setDimmedFocus(false);
				}
				resetScroll();
				repaint();
			}
			return true;
		case HRcEvent.VK_ENTER:
			if (Log.DEBUG_ON) {
				Log.printDebug("SimilarContent, contents[" + focus + "].isChannelContent=" 
						+ contents[focus].isChannelContent());
			}
			
			if (contents[focus].isChannelContent()) {
				if (ch != null) {
					String callLetter = ch.getCallLetter();
                    //->Kenneth[2015.8.29] VDTRMASTER-5618 : VCDS 에서는 SD 만 쓰게 수정함. jira 참조.
                    try {
                        int def = ch.getDefinition();
                        Channel sisterCh = (Channel)ch.getSisterChannel();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SimilarContentView.handleKeyEvent(ENTER) : callLetter = "+callLetter);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SimilarContentView.handleKeyEvent(ENTER) : def = "+def);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SimilarContentView.handleKeyEvent(ENTER) : sisterCh = "+sisterCh);
                        if (def == Channel.DEFINITION_HD && sisterCh != null) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SimilarContentView.handleKeyEvent(ENTER) : sisterCh.definition = "+sisterCh.getDefinition());
                            String sisterCallLetter = sisterCh.getCallLetter();
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SimilarContentView.handleKeyEvent(ENTER) : change call_letter from "+callLetter+" to "+sisterCallLetter);
                            callLetter = sisterCallLetter;
                        }
                    } catch (Exception e) {
                        Log.print(e);
                    }
                    //<-
					
					if (Environment.EMULATOR) {
						EpgCore.getInstance().startUnboundApp(VOD_APP_NAME,
								new String[] { App.NAME, "CALL_CFCM"  });
					} else {
						EpgCore.getInstance().startUnboundApp(VOD_APP_NAME,
								new String[] { App.NAME, "CALL_" + callLetter });
					}
				}
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("SimilarContent, contents[" + focus + "].vodId=" + contents[focus].getVodId());
				}
				
				if (contents[focus].getVodId() != null) {
					EpgCore.getInstance().startUnboundApp(VOD_APP_NAME,
							new String[] { App.NAME, contents[focus].getVodId() });
				}
			}
			return true;
        //->Kenneth[2015.6.17] R5 : program details 에서 사용하는 키들을 막는다.
        case KeyCodes.COLOR_D:
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
        case KeyCodes.LITTLE_BOY:
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
            if (isOpened() && hasContents()) {
                return true;
            }
            break;
        //<-
		}

		return false;
	}
	
	private static DVBBufferedImage getFullScreenImage(SharedMemory sm, String key) {
		DVBBufferedImage buf = (DVBBufferedImage) sm.get(key);
		if (buf == null) {
			buf = new DVBBufferedImage(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			sm.put(key, buf);
		}
		return buf;
	}
	
	public Container getRoot(Component component) {
        root = (Container) SharedMemory.getInstance().get(KEY_ROOT_WINDOW);
        if (root != null) {
            return root;
        }
        if (component == null) {
            return null;
        }
        Container c = component.getParent();
        while (c != null) {
            root = c;
            c = c.getParent();
        }
        if (root != null) {
            SharedMemory.getInstance().put(KEY_ROOT_WINDOW, root);
        }
        return root;
    }

	public void run() {
		animationStarted();

		frameCount = 0;
		// make parent buffer.
		DVBGraphics bGraphics = (DVBGraphics) background.createGraphics();
		DVBGraphics iGraphics = (DVBGraphics) image.createGraphics();
		DVBGraphics oGraphics = (DVBGraphics) offScreen.createGraphics();
		DVBGraphics pGraphics = (DVBGraphics) getRoot(this).getGraphics();
		
		try {
			pGraphics.setDVBComposite(DVBAlphaComposite.Src);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }
		
		paintBackground(bGraphics);
		pGraphics.drawImage(background, 0, 0, null);
		isAnimating = true;
		do {
			long start = System.currentTimeMillis();
//			try {
//				oGraphics.setDVBComposite(DVBAlphaComposite.Src);
//            } catch (UnsupportedDrawingOperationException ex) {
//                Log.print(ex);
//            }
//			oGraphics.drawImage(background, 0, 0, null);
//			Log.printDebug("3");
//			paint(iGraphics);
//			Log.printDebug("4");
//			oGraphics.drawImage(image, 0, 0, null);
//			Log.printDebug("5");
//			oGraphics.setColor(Color.CYAN);
//			oGraphics.fillRect(100, 400, 20, 20);
//			pGraphics.drawImage(offScreen, 0, 0, null);
			
			try {
				pGraphics.setDVBComposite(DVBAlphaComposite.Src);
	        } catch (UnsupportedDrawingOperationException ex) {
	            Log.print(ex);
	        }
			pGraphics.drawImage(background, 0, 0, null);
			try {
				pGraphics.setDVBComposite(DVBAlphaComposite.SrcOver);
	        } catch (UnsupportedDrawingOperationException ex) {
	            Log.print(ex);
	        }
			paint(pGraphics);
//			oGraphics.drawImage(image, 0, 0, null);
//			Log.printDebug("5");
//			oGraphics.setColor(Color.CYAN);
//			oGraphics.fillRect(100, 400, 20, 20);
//			pGraphics.drawImage(offScreen, 0, 0, null);
			toolKit.sync();
			frameCount++;

			long end = System.currentTimeMillis();
			long diff = end - start;
			if (diff < STEP_DELAY) {
				try {
					Thread.sleep(STEP_DELAY - diff);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}

		} while (frameCount < frames);
		frameCount--;

		clear(bGraphics);
		clear(iGraphics);
		clear(oGraphics);
		clear(pGraphics);
		
		oGraphics.dispose();
		iGraphics.dispose();
		bGraphics.dispose();
		pGraphics.dispose();
		isAnimating = false;
		animationEnded();
	}

	private void clear(DVBGraphics g) {
		g.setClip(null);
		try {
			g.setDVBComposite(DVBAlphaComposite.Src);
		} catch (UnsupportedDrawingOperationException ex) {
			Log.print(ex);
		}
		g.clearRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		try {
			g.setDVBComposite(DVBAlphaComposite.SrcOver);
		} catch (UnsupportedDrawingOperationException ex) {
			Log.print(ex);
		}
	}
	
	/**
     * Paints background components to the graphics.
     *
     * @param g graphics context
     */
    private void paintBackground(Graphics g) {
        try {
            Component[] child = root.getComponents();

            int i = child.length - 1;
            Component c;
            while (i >= 0) {
                c = child[i];
                if (c != null && c.isVisible()) {
                    Point p = c.getLocation();
                    Graphics g1 = g.create();
                    g1.translate(p.x, p.y);
                    try {
                        c.paint(g1);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                    g1.dispose();
                }
                i--;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

	class SimilarContentRenderer extends Renderer {

		private DVBColor bgColor = new DVBColor(17, 17, 17, 230);
		private Color focuseColor = new Color(249, 194, 0);
		private Color titleColor = new Color(224, 224, 224);
		private Color explainColor = new Color(195, 195, 195);
		private Color channelColor = new Color(62, 62, 62);
		private Color cDimmed102 = new DVBColor(0, 0, 0, 102);
		private Color similarBgColor = new Color (25, 25, 25);
		private Color lineColor = new Color(48, 48, 48);
		private Image threeDIconImg, UHDIcon;
		private Image arrowLeftImg, arrowRightImg;
		private Image defaultImg;
		private Image i02ArsL, i02ArsLDim, icon900Img, iconRepeatImg, iconOKImg, iconAdultImg;

		private String errorCodeHeaderStr;
		private String availableCodStr;
		private String adultMsg1Str;
		private String adultMsg2Str;
		private String noSimilarContent;
		
		public Rectangle getPreferredBounds(UIComponent c) {
			return Constants.SCREEN_BOUNDS;
		}

		public void prepare(UIComponent c) {
			threeDIconImg = DataCenter.getInstance().getImage("icon_3d.png");
			UHDIcon = DataCenter.getInstance().getImage("icon_uhd.png");
			arrowLeftImg = DataCenter.getInstance().getImage("02_ars_l_s.png");
			arrowRightImg = DataCenter.getInstance().getImage("02_ars_r_s.png");
			defaultImg = DataCenter.getInstance().getImage("VOD_default_S_Portrait.png");
			i02ArsL = DataCenter.getInstance().getImage("02_ars_l.png");
			i02ArsLDim = DataCenter.getInstance().getImage("02_ars_l_dim.png");
			icon900Img = DataCenter.getInstance().getImage("icon_900.png");
			iconRepeatImg = DataCenter.getInstance().getImage("icon_repeat.png");
			Hashtable footerTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
			iconOKImg = (Image) footerTable.get(PreferenceService.BTN_OK);
			iconAdultImg = DataCenter.getInstance().getImage("02_adult_poster_s.png");

			errorCodeHeaderStr = DataCenter.getInstance().getString("menu.error_code");
			availableCodStr = DataCenter.getInstance().getString("menu.available_cod");
			adultMsg1Str = DataCenter.getInstance().getString("menu.adult_msg_1");
			adultMsg2Str = DataCenter.getInstance().getString("menu.adult_msg_2");
			noSimilarContent = DataCenter.getInstance().getString("menu.noSimilarContent");
		}

		protected void paint(Graphics g, UIComponent c) {
			if (isAnimating && mode != MODE_INTRO && hasContents()) {
				DVBGraphics gg = (DVBGraphics) g;

				DVBAlphaComposite origin = gg.getDVBComposite();
				try {
					if (mode == MODE_OPEN) {
						gg.setDVBComposite(openAlpha[frameCount]);
					} else {
						gg.setDVBComposite(closeAlpha[frameCount]);
					}
				} catch (UnsupportedDrawingOperationException e) {
					e.printStackTrace();
				}
				
				g.setColor(bgColor);
				g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

				try {
					gg.setDVBComposite(origin);
				} catch (UnsupportedDrawingOperationException e) {
					e.printStackTrace();
				}
			} else if (mode == MODE_OPEN && !isAnimating && hasContents()) {
				g.setColor(bgColor);
				g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
				
				if (!hasFocus && hasContents()) {
					g.drawImage(i02ArsL, 606, 422, c);
				}
			}
			
			if (hasContents() && !hasFocus && (mode != MODE_OPEN || isAnimating)) {
				g.drawImage(i02ArsLDim, 606, 422, c);
			}

			if (opPanel != null && hasContents()) {
				int moveX = opPanel.getBounds().x;
				int moveY = opPanel.getBounds().y;
				g.translate(moveX, moveY);
				opPanel.paintOptionPanel(g);
				g.translate(-moveX, -moveY);
			}

			Rectangle originClip = null;

			// Rectangle originClip = g.getClipBounds();
			// g.setClip(clipBounds);

			if (contents != null && contents.length > 0) {
				
				paintContents(contentDVBImage, c);
				g.drawImage(contentDVBImage, 0, 0, c);
				
			} else if (isAdult) {
				
				paintContents(contentDVBImage, c);
				g.drawImage(contentDVBImage, 0, 0, c);
				
			} else if (mode == MODE_OPEN && head != null && head.getError() != null) {
				g.setColor(similarBgColor);
				g.fillRect(55, 388, 537, 87);
				g.setColor(lineColor);
				g.fillRect(55, 388, 537, 1);
				SError error = head.getError();
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(titleColor);

//					String message = TextUtil.shorten(error.getMessage(), g.getFontMetrics(), 435);
				String[] message = TextUtil.split(error.getMessage(), g.getFontMetrics(), 400, 2);
				
				int posY = 431 - (message.length - 1) * 22;
				if (message.length == 2) {
					posY += 8;
				}
				
				for(int i = 0; i < message.length; i++) {
					g.drawString(message[i], 152, posY + i * 22);
				}

				String errorCodeStr = errorCodeHeaderStr + " [" + head.getError().getCode() + "]";
				g.setFont(FontResource.BLENDER.getFont(16));
				g.drawString(errorCodeStr, 152, posY + message.length * 22);
			} else if (mode == MODE_OPEN) {
				g.setColor(similarBgColor);
				g.fillRect(55, 388, 537, 87);
				g.setColor(lineColor);
				g.fillRect(55, 388, 537, 1);
				// no similar content found.
				g.setFont(FontResource.BLENDER.getFont(21));
				g.setColor(titleColor);
				GraphicUtil.drawStringCenter(g, noSimilarContent, 338, 443);
			}
			
			// for debug
			if (Log.EXTRA_ON) {
				g.setColor(Color.green);
				g.setFont(FontResource.DINMED.getFont(14));
				g.drawString(SimilarContentManager.debugStr, 280, 498);
			}
		}
		
		private void paintContents(DVBBufferedImage context, UIComponent c) {
			DVBGraphics gg = (DVBGraphics) context.getGraphics();
			
			clear(gg);
			
			if (contents != null && contents.length > 0) {
				Rectangle originClip = gg.getClipBounds();
				if ((originClip != null && originClip.height > 50) || isAnimating || mode != MODE_OPEN) {
					gg.setClip(clipBounds);
				} else {
					originClip = null;
				}
				
				int posY = 0;
				if (mode == MODE_INTRO) {
					posY = introPosY[frameCount];
				} else if (mode == MODE_OPEN) {
					posY = openPosY[frameCount];
				} else {
					posY = closePosY[frameCount];
				}
				
				if (mode == MODE_OPEN && !isAnimating) {
					gg.setColor(similarBgColor);
					gg.fillRect(clipBounds.x, posY - 27, clipBounds.width, clipBounds.height - 1);
					gg.setColor(lineColor);
					gg.fillRect(clipBounds.x, posY - 27, clipBounds.width, 1);
					
					gg.drawImage(icon900Img, 56, posY - 30 + 16, c);
					gg.drawImage(iconRepeatImg, 93, posY - 30 + 16, c);
					gg.setFont(FontResource.BLENDER.getFont(18));
					gg.setColor(explainColor);
					if (isAdult) {
						GraphicUtil.drawStringRight(gg, adultMsg1Str, 250, posY - 30 + 29);
						gg.drawImage(iconOKImg, 256, posY - 30 + 15, c);
						gg.drawString(adultMsg2Str, 283, posY - 30 + 29);
						gg.drawImage(iconAdultImg, 321, posY - 30 + 45, c);
					} else {
						gg.drawString(availableCodStr, 112, posY - 30 + 29);
					}
				} else {
					gg.setColor(similarBgColor);
					gg.fillRect(clipBounds.x, posY, clipBounds.width, clipBounds.height - 1);
					gg.setColor(lineColor);
					gg.fillRect(clipBounds.x, posY, clipBounds.width, 1);
				}

				posY += 12;
				for (int i = 0; i < contents.length; i++) {
					int gapX = i * 110;
					if (i == focus && hasFocus) {
						gg.setColor(focuseColor);
						
						if (contents[i].getPoster() != null) {
							int width = contents[i].getPoster().getWidth(c) + 4;
							int height = contents[i].getPoster().getHeight(c) + 4;
							if (contents[i].getPoster().getHeight(c) > 100) {
								gg.fillRect(58 + gapX, posY - 2, width, height);
							} else {
								if (!isAllLandscape) {
									int landPosY = posY - 2;
									if (mode == MODE_OPEN) {
										landPosY += landOpenPosY[frameCount];
									}
									gg.fillRect(57 + gapX, landPosY, width, height);
								} else {
									gg.fillRect(57 + gapX, posY - 2, width, height);
								}
								
							}
						} else if (contents[i].isChannelContent()) {
							if (isAllLandscape) {
								gg.fillRect(56 + gapX, posY - 2, 96, 73);
							} else {
								gg.fillRect(56 + gapX, posY - 2 + 33, 96, 73);
							}
						} else {
							gg.fillRect(58 + gapX, posY - 2, 94, 139);
						}
						
						// arrow
						int arrowY = 366;
						if (isAllLandscape) {
							arrowY = 399;
						}
						if (focus > 0) {
							gg.drawImage(arrowLeftImg, 46 + gapX, arrowY, c);
							gg.drawImage(arrowRightImg, 157 + gapX, arrowY, c);
						} else {
							gg.drawImage(arrowRightImg, 157 + gapX, arrowY, c);
						}
					}

					if (contents[i].getPoster() != null) {
						if (contents[i].getPoster().getHeight(c) > 100) {
							gg.drawImage(contents[i].getPoster(), 60 + gapX, posY, c);

							if ((mode != MODE_OPEN && !isAnimating) || mode == MODE_INTRO) {
								gg.setColor(cDimmed102);
								gg.fillRect(60 + gapX, posY, contents[i].getPoster().getWidth(c), contents[i]
										.getPoster().getHeight(c));
							}

							if (contents[i].getDefinitions().indexOf("UHD") > -1) {
								gg.drawImage(UHDIcon, 60 + gapX, posY + 118, c);
							} else if (contents[i].getDefinitions().indexOf("3D") > -1) {
								gg.drawImage(threeDIconImg, 60 + gapX, posY + 118, c);
							}
						} else {
							int landPosY = posY;
							if (mode == MODE_OPEN) {
								landPosY += landOpenPosY[frameCount];
							} else if (mode == MODE_CLOSE){
								landPosY -= landClosePosY[frameCount];
							}
							gg.drawImage(contents[i].getPoster(), 59 + gapX, landPosY, c);

							if ((mode != MODE_OPEN && !isAnimating) || mode == MODE_INTRO) {
								gg.setColor(cDimmed102);
								gg.fillRect(58 + gapX, landPosY, contents[i].getPoster().getWidth(c), contents[i]
										.getPoster().getHeight(c));
							}

							if (contents[i].getDefinitions().indexOf("UHD") > -1) {
								gg.drawImage(UHDIcon, 59 + gapX, landPosY + 20, c);
							} else if (contents[i].getDefinitions().indexOf("3D") > -1) {
								gg.drawImage(threeDIconImg, 59 + gapX, landPosY + 20, c);
							}
						}
					} else if (contents[i].isChannelContent()) {
						int landPosY = posY;
						if (mode == MODE_OPEN) {
							landPosY += landOpenPosY[frameCount];
						} else if (mode == MODE_CLOSE){
							landPosY -= landClosePosY[frameCount];
						}

						gg.setColor(channelColor);
						gg.fillRect(58 + gapX, landPosY, 92, 69);
						
						gg.drawImage(chLogo, 58 + gapX + 46 - chLogo.getWidth(null)/2, landPosY + 35 - chLogo.getHeight(null)/2, c);
						
					} else { // default bg
						gg.drawImage(defaultImg, 60 + gapX, posY, 90, 135, c);
					}

					int posX = 61 + gapX;

					if (i == focus && hasFocus) {
						gg.setColor(focuseColor);
					} else {
						gg.setColor(titleColor);
					}

					gg.setFont(FontResource.BLENDER.getFont(16));
					
					int titleGap = 153;
					if (isAllLandscape) {
						titleGap = 87;
					}
					if (i == focus && hasFocus && !isAnimating) {

						Rectangle ori = gg.getClipBounds();

						int cnt = 0;
						cnt = Math.max(scroll - 10, 0);

						if (contents[i].getTitle() != null) {
							if (shortenTitles[i].equals(contents[i].getTitle())) {
								gg.drawString(shortenTitles[i], posX, posY + titleGap);
							} else {
								scrollBounds.x = posX;
								scrollBounds.y = posY + titleGap - 12;
								scrollBounds.width = 88;
								scrollBounds.height = 20;

								gg.clipRect(scrollBounds.x, scrollBounds.y, scrollBounds.width, scrollBounds.height);
								try {
									gg.drawString(contents[i].getTitle().substring(cnt), posX, posY + titleGap);
								} catch (IndexOutOfBoundsException ex) {
									scroll = 0;
								}
							}
						}
						gg.setClip(ori);
					} else {
						if (!hasFocus) {
							scrollBounds.width = 0;
						}
						if (shortenTitles[i] != null) {
							gg.drawString(shortenTitles[i], posX, posY + titleGap);
						}
					}
				}

				gg.setClip(originClip);
				
			} else if (isAdult) {
				Rectangle originClip = gg.getClipBounds();
				if ((originClip != null && originClip.height > 50) || isAnimating || mode != MODE_OPEN) {
					gg.setClip(clipBounds);
				} else {
					originClip = null;
				}
				
				int posY = 0;
				if (mode == MODE_INTRO) {
					posY = introPosY[frameCount];
				} else if (mode == MODE_OPEN) {
					posY = openPosY[frameCount];
				} else {
					posY = closePosY[frameCount];
				}
				
				gg.setColor(similarBgColor);
				gg.fillRect(clipBounds.x, posY, clipBounds.width, clipBounds.height - 1);
				gg.setColor(lineColor);
				gg.fillRect(clipBounds.x, posY, clipBounds.width, 1);
				
				gg.drawImage(icon900Img, 56, posY + 16, c);
				gg.drawImage(iconRepeatImg, 93, posY + 16, c);
				gg.setFont(FontResource.BLENDER.getFont(18));
				gg.setColor(explainColor);
				
				FontMetrics fm = gg.getFontMetrics();
				int str1Width = fm.stringWidth(adultMsg1Str);
				int str2Width = fm.stringWidth(adultMsg2Str);
				int posX = 348 - (str1Width + str2Width + 33)/2;
				
				gg.drawString(adultMsg1Str, posX, posY + 29);
				posX += str1Width + 6;
				gg.drawImage(iconOKImg, posX, posY + 15, c);
				posX += 27;
				gg.drawString(adultMsg2Str, posX, posY + 29);
				
				gg.drawImage(iconAdultImg, 321, posY + 45, c);
			
				gg.setClip(originClip);
			} 
		}
	}
}

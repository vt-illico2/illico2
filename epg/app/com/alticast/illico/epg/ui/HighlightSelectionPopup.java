package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import java.awt.*;
import java.util.*;
import java.awt.event.KeyEvent;
import javax.tv.util.*;
import com.alticast.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
public class HighlightSelectionPopup extends Popup{
    private boolean selectionVisible = false;
    private HighlightSelectionPopupRenderer renderer = new HighlightSelectionPopupRenderer();
    private PopSelectValueUI selectPopup = new PopSelectValueUI();
    private GridEpg gridEpg;
    private boolean highlightChanged = false;

    public HighlightSelectionPopup(GridEpg gridEpg) {
        this.gridEpg = gridEpg;
        setRenderer(renderer);
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"HighlightSelectionPopup.handleKey("+code+")");
        try {
            switch (code) {
                case OCRcEvent.VK_UP:
                case OCRcEvent.VK_DOWN:
                    if (selectionVisible) {
                        selectPopup.handleKey(code);
                    }
                    break;
                case OCRcEvent.VK_ENTER:
                    if (selectionVisible) {
                        String selectedTitle = selectPopup.getSelectedValue();
                        String currentTitle =
                            Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight());
                        if (!selectedTitle.equals(currentTitle)) {
                            // 다른 highlight 를 선택
                            highlightChanged = true;
                            int selectedIndex = selectPopup.getSelectedIndex();
                            String newHighlight = Highlight.getInstance().getAllHighlights()[selectedIndex];
                            Highlight.getInstance().setHighlight(newHighlight);
                            renderer.prepare(this);
                        }
                        hideSelectionPopup();
                        //->Kenneth[2015.5.29] Julie 리뷰시 하나 선택하면 걍 닫히게 해달라고 했다고 함.
                        closePopup();
                    } else {
                        showSelectionPopup();
                    }
                    break;
                //case OCRcEvent.VK_LAST:
                case OCRcEvent.VK_EXIT:
                    if (selectionVisible) {
                        hideSelectionPopup();
                    } else {
                        closePopup();
                    }
                    break;
                case KeyCodes.COLOR_C:
                    closePopup();
                    break;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return true;
    }

    private void showSelectionPopup() {
        try {
            String[] all = Highlight.getInstance().getAllHighlights();
            String[] names = new String[all.length];
            for (int i = 0 ; i < names.length ; i++) {
                names[i] = Highlight.getInstance().getHighlightName(all[i]);
            }
            selectPopup.show(this, names,
                    Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight()), 
                    //->Kenneth[2015.9.17] VDTRMASTER-5648 : Julie 요구사항
                    DataCenter.getInstance().getString("epg.highlight_selection_title"), 519, 177, 5);
                    //DataCenter.getInstance().getString("epg.highlight_title"), 519, 177, 5);
                    //<-
            add(selectPopup, 0);
            repaint();
            selectionVisible = true;
        } catch (Exception e) {
            Log.print(e);
        }
    }

    private void hideSelectionPopup() {
        selectPopup.hide();
        selectionVisible = false;
        repaint();
    }

    private void closePopup() {
        stop();
        if (highlightChanged) {
            gridEpg.highlightChanged();
        }
    }

    // Renderer
    class HighlightSelectionPopupRenderer extends Renderer {
        DVBColor fullBgColor = new DVBColor(12, 12, 12, 230);
        DVBColor smallBgColor = new DVBColor(44, 44, 44, 255);
        DVBColor highlightColor = new DVBColor(255, 255, 255, 230);
        DVBColor smallDimmedColor = new DVBColor(0, 0, 0, 128);
        Color titleColor = new Color(252, 202, 4);
        Color descColor = new Color(229, 229, 229);
        Color closeColor = new Color(241, 241, 241);
        Font font24 = FontResource.BLENDER.getFont(24);
        Font font18 = FontResource.BLENDER.getFont(18);
        Font font17 = FontResource.BLENDER.getFont(17);
        String title;
        String desc1;
        String desc2;
        String closeText;
        String curHighlight;
        Image focusedIcon = DataCenter.getInstance().getImage("input_210_foc2.png");
        Image unfocusedIcon = DataCenter.getInstance().getImage("input_210_sc.png");
        Image closeIcon;

        public void prepare(UIComponent c) {
            Hashtable btnTable = (Hashtable) SharedMemory.getInstance().get("Footer Img");
            closeIcon = (Image)btnTable.get(PreferenceService.BTN_C);
            FrameworkMain.getInstance().getImagePool().waitForAll();
            title = DataCenter.getInstance().getString("epg.highlight_title");
            desc1 = DataCenter.getInstance().getString("epg.highlight_desc1");
            desc2 = DataCenter.getInstance().getString("epg.highlight_desc2");
            closeText = DataCenter.getInstance().getString("epg.close");
            curHighlight = Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight());
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paint(Graphics g, UIComponent c) {
            // full bg
            g.setColor(fullBgColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            // small bg
            g.setColor(smallBgColor);
            g.fillRect(306, 147, 350, 245);
            // title
            g.setColor(titleColor);
            g.setFont(font24);
            g.drawString(title, 334, 189);
            // desc
            g.setColor(descColor);
            g.setFont(font18);
            g.drawString(desc1, 337, 220);
            g.drawString(desc2, 337, 220+22);
            // popup dimmed 
            if (selectionVisible) {
                g.setColor(smallDimmedColor);
                g.fillRect(306, 147, 350, 245);
                // focus icon
                g.drawImage(unfocusedIcon, 335, 262, c);
            } else {
                // focus icon
                g.drawImage(focusedIcon, 335, 262, c);
            }
            // cur highlight 
            g.setColor(highlightColor);
            g.drawString(curHighlight, 348, 283);
            // close icon
            g.drawImage(closeIcon, 580, 360, c);
            g.setColor(closeColor);
            g.setFont(font17);
            g.drawString(closeText, 605, 375);
        }
    }
}

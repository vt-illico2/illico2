package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.PopupController;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import java.awt.*;
import java.awt.event.KeyEvent;
import javax.tv.util.*;
import com.alticast.ui.*;
import com.alticast.illico.epg.*;


public class Popup extends UIComponent implements TVTimerWentOffListener {

    public static final long DEFAULT_CLOSE_TIME = 5000L;

    public static final int ICON_NONE = 0;
    public static final int ICON_CHECK = 1;
    public static final int ICON_ERROR = 2;

    public int icon = ICON_NONE;

    public String[] buttons;
    public String title;

    public PopupListener listener;

    public static final String BUTTON_CANCEL = "epg.Cancel";
    public static final String BUTTON_OK = "epg.OK";
    public static final String BUTTON_YES = "epg.Yes";
    public static final String BUTTON_NO = "epg.No";

    public static final String[] QUESTION_BUTTONS = {
        BUTTON_OK, BUTTON_CANCEL
    };

    public static final String[] CONFIRM_BUTTONS = {
        BUTTON_OK
    };

    public  static final Color DEFAULT_BACKGROUND = new DVBColor(12, 12, 12, 204);

    protected TVTimerSpec autoCloseTimer;
    private long autoCloseDelay = 0;

    protected ClickingEffect clickEffect;
    protected Effect showingEffect;

    public Popup() {
        this.setVisible(true);

        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.addTVTimerWentOffListener(this);
        autoCloseDelay = 0;

        setBounds(Constants.SCREEN_BOUNDS);
        clickEffect = new ClickingEffect(this);
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Popup.stop()");
        stopTimer();
        PopupController.getInstance().stop(this);
    }

    public void setAutoCloseDelay(long delay) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Popup.setAutoCloseDelay("+delay+")");
        autoCloseDelay = delay;
        if (delay > 0) {
        }
    }

    public void startEffect() {
        if (showingEffect == null) {
            showingEffect = new MovingEffect(this, 8,
                            new Point(0, 40), new Point(0, 0),
                            MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        }
        showingEffect.start();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Popup.handleKey("+code+")");
        if (buttons == null || buttons.length <= 0) {
            switch (code) {
                case OCRcEvent.VK_UP:
                case OCRcEvent.VK_DOWN:
                case OCRcEvent.VK_LEFT:
                case OCRcEvent.VK_RIGHT:
                case KeyCodes.COLOR_A:
                case KeyCodes.COLOR_B:
                case KeyCodes.COLOR_C:
                case KeyCodes.COLOR_D:
                case OCRcEvent.VK_ENTER:
                case OCRcEvent.VK_LAST:
                case OCRcEvent.VK_CHANNEL_UP:
                case OCRcEvent.VK_CHANNEL_DOWN:
                case OCRcEvent.VK_RECORD:
                case OCRcEvent.VK_STOP:
                    return true;
                case OCRcEvent.VK_EXIT:
                    stopAndClose(false);
                    return true;
                default:
                    if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                        return true;
                    }
                    return false;
            }
        }
        startTimer();
        switch (code) {
            case OCRcEvent.VK_RIGHT:
                if (focus < buttons.length - 1) {
                    focus++;
                    repaint();
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (focus > 0) {
                    focus--;
                    repaint();
                }
                break;
            case OCRcEvent.VK_ENTER:
                startClickingEffect();
                if (listener != null) {
                    listener.notifySelected(this, buttons[focus]);
                }
                stop();
                break;
            case OCRcEvent.VK_LAST:
            case OCRcEvent.VK_EXIT:
                stopAndClose(false);
                break;
            case OCRcEvent.VK_UP:
            case OCRcEvent.VK_DOWN:
            case OCRcEvent.VK_INFO:
            case OCRcEvent.VK_GUIDE:
            case KeyCodes.COLOR_A:
            case KeyCodes.COLOR_B:
            case KeyCodes.COLOR_C:
            case KeyCodes.COLOR_D:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
            case OCRcEvent.VK_RECORD:
            case OCRcEvent.VK_STOP:
                return true;
            default:
                if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                    return true;
                }
                return false;
        }
        return true;
    }

    private void selected(String key) {
    }

    public void startTimer() {
        long delay = autoCloseDelay;
        if (delay <= 0) {
            return;
        }
        Log.printDebug("Popup.startAutoCloseTimer = " + delay);
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        autoCloseTimer.setDelayTime(delay);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    public void stopTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Popup.timerWentOff() : Call stopAndClose(true)");
        stopAndClose(true);
    }

    private synchronized void stopAndClose(boolean byTimer) {
        try {
            if (listener != null) {
                listener.notifyCanceled(this, byTimer);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        stop();
    }

    public Color getBackground() {
        if (renderer instanceof PopupRenderer) {
            return ((PopupRenderer) renderer).getBackground();
        } else {
            return DEFAULT_BACKGROUND;
        }
    }

    public boolean equals(Object o) {
        //->Kenneth[2015.4.16] tutorial popup 구현하다 아래에서 NullPointerException 나길래 처리함.
        try {
            if (o instanceof Popup) {
                Popup p = (Popup) o;
                if (p != null && title != null && p.title != null) {
                    return icon == p.icon && title.equals(p.title) && autoCloseDelay == p.autoCloseDelay
                        && renderer.getClass().equals(p.renderer.getClass());
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return false;
    }

    public boolean isLight() {
        return buttons == null || buttons.length <= 1;
    }

    public void startClickingEffect() {
        if (renderer instanceof PopupRenderer) {
            clickEffect.start(((PopupRenderer) renderer).getButtonFocusBounds(this));
        }
    }

    public void animationEnded(Effect effect) {
        if (effect == showingEffect) {
            this.setVisible(true);
            startTimer();
        }
    }

}

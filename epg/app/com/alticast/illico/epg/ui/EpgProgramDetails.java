package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.ixc.isa.ISAService;

import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public class EpgProgramDetails extends ProgramDetails {

    public EpgProgramDetails() {
        setRenderer(new ProgramDetailsRenderer());
    }

    public void start(Program p) {
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.start("+p+")");
        super.start(p);
    }

    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected()");
        String key = item.getKey();

        DataCenter dc = DataCenter.getInstance();
        if (item == Rs.MENU_CLOSE) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call hideDetails()");
            ((GridEpg)getParent()).hideDetails();
        } else if (item == Rs.MENU_VIEW_PROGRAM || item == Rs.MENU_TUNE_CHANNEL  || item == Rs.MENU_TUNE_IN) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call tuneToChannel()");
            tuneToChannel();
        } else if (item == Rs.MENU_RECORD || item == Rs.MENU_RECORD_PROGRAM) {
            //->Kenneth[2015.10.15] DDC-114 : EpgCore.record() 에서 판단해서 팝업 띄움.
            /*
            int reason = IncompatibilityChannelPanel.resetReasonCode(channel);
            if (reason == IncompatibilityChannelPanel.REASON_NO_UHD_PACKAGE) {
                if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetail.selected() : Call showIncompatibilityErrorMessage()--1");
                EpgCore.getInstance().showIncompatibilityErrorMessage(reason);
                return ;
            }
            */
            /*
            //->Kenneth[2015.2.3] 4K
            if (EpgCore.getInstance().needToShowIncompatibilityPopup(channel)) {
                if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetail.selected() : Call showIncompatibilityErrorMessage()--1");
                EpgCore.getInstance().showIncompatibilityErrorMessage();
                return ;
            }
            */
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.record()");
            EpgCore.getInstance().record(channel, program);
        //->Kenneth[2015.2.3] 4K
        } else if (item == Rs.MENU_MORE_INFO) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetail.selected() : Call showIncompatibilityErrorMessage()--2");
            //->Kenneth[2015.10.15] DDC-114
            EpgCore.getInstance().showIncompatibilityErrorMessage(IncompatibilityChannelPanel.resetReasonCode(channel));
            //EpgCore.getInstance().showIncompatibilityErrorMessage();
            //<-
        } else if (item == Rs.MENU_STOP_RECORDING || item == Rs.MENU_CANCEL_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.cancelRecord()");
            EpgCore.getInstance().cancelRecord(program);

            //->Kenneth[2015.3.7] R7 : Ungrouped 에서는 sister 레코딩 신경쓸 필요 없음.
            if (!EpgCore.isChannelGrouped) {
                return ;
            }
            //<-

            //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
            int recStatus = RecordingStatus.getInstance().getStatus(program);
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() : recStatus = "+recStatus);
            // cancelRecording 이 두번 불리우면 팝업 두번 뜸. 따라서 recording 중인 놈으로 이미 호출했으면
            // sister 인 놈은 호출하지 않음.
            if (recStatus != RecordingStatus.RECORDING) {
                if (RecordingStatus.getInstance().getSisterStatus(program) == RecordingStatus.RECORDING) {
                    if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.cancelRecord(sisterProgram)");
                    EpgCore.getInstance().cancelRecord(program.getSisterProgram());
                }
            }
            //<-
        } else if (item == Rs.MENU_ADD_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.addFavoriteChannel()");
            //->Kenneth[2016.3.9] R7 : VBM 로그 
            EpgCore.getInstance().addFavoriteChannel(channel, this, false);
            //EpgCore.getInstance().addFavoriteChannel(channel, this);
            //<-
        } else if (item == Rs.MENU_REMOVE_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.removeFavoriteChannel()");
            EpgCore.getInstance().removeFavoriteChannel(channel, this);
        } else if (item == Rs.MENU_BLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.addBlockedChannel()");
            EpgCore.getInstance().addBlockedChannel(channel, this);
        } else if (item == Rs.MENU_UNBLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.removeBlockedChannel()");
            EpgCore.getInstance().removeBlockedChannel(channel, this);
        } else if (item == Rs.MENU_SET_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call ReminderManager.remind()");
            ReminderManager.getInstance().remind(program, this);
            //->Kenneth[2015.4.14] : R5 
            resetFooters();
        } else if (item == Rs.MENU_REMOVE_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call ReminderManager.remove()");
            ReminderManager.getInstance().remove(program);
            updateStatus();
            //->Kenneth[2015.4.14] : R5 
            resetFooters();
        } else if (item == Rs.MENU_SUBSCRIBE_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.showSubscriptionPopup()");
            EpgCore.getInstance().showSubscriptionPopup(channel, ISAService.ENTRY_POINT_EPG_E02);
            //->Kenneth[2017.3.16] R7.3 : VBM 로그 
            EpgVbmController.getInstance().writeUnsubscriptionScreen(channel, program);
            //<-
        } else if (item == Rs.MENU_CHANGE_PACKAGE) {
//            EpgCore.getInstance().startUnboundAppWithPin("Options", null);
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call EpgCore.showSubscriptionPopup()");
            EpgCore.getInstance().showSubscriptionPopup(channel, ISAService.ENTRY_POINT_EPG_E02);
            //->Kenneth[2017.3.16] R7.3 : VBM 로그 
            EpgVbmController.getInstance().writeUnsubscriptionScreen(channel, program);
            //<-
        } else {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.selected() Call GridEpg.selected()");
            ((GridEpg)getParent()).selected(item, channel, program, this, this);
        }
    }


    protected MenuItem createMenu() {
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu()");
        MenuItem root = new MenuItem("GRID_DETAIL");
        //->Kenneth[2015.6.6] R5 : 로그 추가
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel = "+channel);
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : program = "+program);
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isSubscribed() = "+channel.isSubscribed());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isISAPurchasable() = "+channel.isISAPurchasable());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isBlocked() = "+channel.isBlocked());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isAuthorized() = "+channel.isAuthorized());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isAuthorizedByFreePreview() = "+channel.isAuthorizedByFreePreview());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.isFreePreview() = "+channel.isFreePreview());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : channel.getType() = "+channel.getTypeString());
        if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : PreviewChannels.hasPackage() = "+PreviewChannels.hasPackage(channel));

        //->Kenneth[2015.10.15] DDC-114
        int reason = IncompatibilityChannelPanel.resetReasonCode(channel);
        if (reason != IncompatibilityChannelPanel.REASON_UHD_COMPATIBLE) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : !UHD_COMPATIBLE, MENU_MORE_INFO");
            root.add(Rs.MENU_MORE_INFO);
            //->Kenneth[2015.6.6] : R5 
            root = addCOD(root);
            return root;
        }
        /*
        //->Kenneth[2015.2.3] 4K
        if (EpgCore.getInstance().needToShowIncompatibilityPopup(channel)) {
            root.add(Rs.MENU_MORE_INFO);
            //->Kenneth[2015.6.6] : R5 
            root = addCOD(root);
            return root;
        }
        */
        //<-

        //->Kenneth[2016.10.17] VDTRMASTER-5943, R7.2/CYO Bundling
        // Redio, Galaxie 채널은 FakeProgram 임.
        // 기존에는 FakeProgram 의 경우 MENU_TUNE_IN 기능밖에 없었지만
        // 새로 A la carte 가 생기면서 미가입 상태에 대한 처리가 필요해짐
        // 따라서 아래 조건문에서 isSubscribed() 인 경우만 MENU_TUNE_IN 으로 하도록 한다.
        // 그러면 !isSubscribed() 인 경우는 이후의 코드들에 의해서 처리될 것임.
        if (program instanceof FakeProgram && channel.isSubscribed()) {
        //if (program instanceof FakeProgram) {
        //<-
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : Subscribed FakeProgram, MENU_TUNE_IN");
            root.add(Rs.MENU_TUNE_IN);
            //->Kenneth[2015.4.16] R5 
            //root.add(Rs.MENU_CLOSE);
            //->Kenneth[2015.6.6] : R5 
            root = addCOD(root);
            return root;
        }

        //->Kenneth[2015.6.6] R5 : VDTRMASTER-5308 : ISA+Preview by Julie
        // 조건1 : ISA 채널 리스트에 있을 것. isISASupported()
        // 조건2 : Free Preview 채널 리스트에 있을 것. isFreePreview()
        // 조건3 : PPV 가 아닐것. -> Channel.isPurchable() 구현에 그렇게 되어 있길래 넣은 조건임
        // 조건4 : AV 가 보여질 것. isSubscribed()

        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        // 아래 조건식을 !isISAPurchasable 로 했음 유의. 
        if (!channel.isISAPurchasable() 
        //if (channel.isSubscribed() && channel.isISASupported() 
        //<-
                && channel.isFreePreview() && channel.getType() != Channel.TYPE_PPV) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : ISA+Preview Case");
            if (program.isPast()) {
                // 과거 프로그램
                root.add(Rs.MENU_TUNE_CHANNEL);
            } else if (program.isFuture()) {
                // 미래 프로그램
                root.add(Rs.MENU_TUNE_CHANNEL);
                root.add(Rs.getRecordMenu(program));
            } else {
                // 현재 프로그램
                root.add(Rs.MENU_VIEW_PROGRAM);
                root.add(Rs.getRecordMenu(program));
            }
            root = addCOD(root);
            return root;
        }

        if (!channel.isSubscribed() || channel.isAuthorizedByFreePreview()) {
            //->Kenneth[2016.8.24] CYO Bundling/A la carte
            if (channel.isISAPurchasable()) {
            //if (channel.isPurchasable()) {
            //<-
                if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : MENU_SUBSCRIBE_CHANNEL");
                root.add(Rs.MENU_SUBSCRIBE_CHANNEL);
            } else {
                if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : MENU_CHANGE_PACKAGE");
                root.add(Rs.MENU_CHANGE_PACKAGE);
            }
            //->Kenneth[2015.4.16] R5 
            //root.add(Rs.MENU_CLOSE);
            //->Kenneth[2015.6.6] : R5 
            root = addCOD(root);
            return root;
        }

        if (program.isPast()) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : MENU_TUNE_CHANNEL -- 1");
            root.add(Rs.MENU_TUNE_CHANNEL);
            //->Kenneth[2015.4.16] R5 
            //root.add(Rs.MENU_CLOSE);
            //->Kenneth[2015.6.6] : R5 
            root = addCOD(root);
            return root;
        }

        if (program.isFuture()) {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : MENU_TUNE_CHANNEL -- 2");
            root.add(Rs.MENU_TUNE_CHANNEL);
        } else {
            if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.createMenu() : MENU_VIEW_PROGRAM");
            root.add(Rs.MENU_VIEW_PROGRAM);
        }
        //->Kenneth[2015.6.22] R5 : VDTRMASTER-5480

        //->Kenneth[2015.3.7] R7 : Ungrouped 에서는 sister 레코딩 신경쓸 필요 없음.
        if (EpgCore.isChannelGrouped && RecordingStatus.getInstance().getSisterStatus(program) == RecordingStatus.RECORDING) {
        //if (RecordingStatus.getInstance().getSisterStatus(program) == RecordingStatus.RECORDING) {
        //<-

            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgProgramDetails.createMenu() : Use sister recording.");
            root.add(Rs.getRecordMenu(program.getSisterProgram()));
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgProgramDetails.createMenu() : Record Menu");
            root.add(Rs.getRecordMenu(program));
        }
        //root.add(Rs.getRecordMenu(program));
        //<-

        //->Kenneth[2015.4.14] R5 : reminder 가 B 키에 매핑되고 menu 에서 빠짐
        //root.add(Rs.getReminderMenu(program));

//        if (program.isFuture()) {
//            if (ReminderManager.getInstance().contains(program)) {
//                root.add(Rs.MENU_REMOVE_REMINDER);
//            } else {
//                root.add(Rs.MENU_SET_REMINDER);
//            }
//        }

        //->Kenneth[2015.4.15] : R5 : UI 상 close 는 필요 없어 보임
        //root.add(Rs.MENU_CLOSE);

        //->Kenneth[2015.6.6] : R5 
        root = addCOD(root);
        
        return root;
    }

    //->Kenneth[2015.6.6] R5 : 여러군데에서 처리해야 해서 함수로 뺌. 
    private MenuItem addCOD(MenuItem root) {
        if (has900Icon()) {
        	if (Log.DEBUG_ON) Log.printDebug("EpgProgramDetails.addCOD() : addCOD, channel.rating=" + 
        			 (channel != null ? channel.getRating() : -1));
        	if (channel != null && channel.getRating() >= Constants.RATING_INDEX_18) {
                root.add(Rs.MENU_ADULT_COD);
            } else if (sView.needButton()) { // contents가 잇는 경우에만 버튼이 노출 되도록 수정.
                root.add(Rs.MENU_COD);
            }
        }
        return root;
    }
}

package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.data.similar.SimilarContentManager;
import com.alticast.illico.epg.data.similar.type.SHead;
import com.alticast.illico.epg.data.similar.type.SimilarContent;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;

import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.awt.Point;
import java.rmi.RemoteException;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public abstract class ProgramDetails extends OptionPanel implements ReminderListener, EpgListener {

    public int blockLevel;
    public Program program;
    public int recordingStatus;
    public Channel channel;
    public String title;
    public String channelNumber;
    public String channelName;
    public String fullName; // radio는 station
    public Image channelLogo;
    //->Kenneth[2015.4.14] R5 : footers
    private Image iconLittleBoy;
    private String textReminder;

    protected ScrollTexts scroll = new ScrollTexts();
    
    // R5
    protected SimilarContentView sView = new SimilarContentView();

    //->Kenneth[2015.6.12] R5 : VBM 을 위한 sessionId
    public static long sessionId;

    //->Kenneth[2016.3.9] R7 : sView 가 slide up 되는 경우 VBM 로그를 남겨야 하는데 다만 
    // 여러번 중복해서 남기면 안되므로 상세 페이지 들어와서 slide up 된 적이 있는지 flag.
    private boolean sViewLaunched = false;

    public ProgramDetails() {
        /*
        DataCenter dc = DataCenter.getInstance();

        scroll.setBounds(48, 186, 519, 190);
        scroll.setFont(FontResource.DINMED.getFont(20));
        scroll.setForeground(Color.white);
        scroll.setRows(8);
        scroll.setRowHeight(21);
        scroll.setVisible(true);
        scroll.setInsets(8, 5, 8+7, 0);
        scroll.setBottomMaskImage(dc.getImage("epg_text_sha.png"));
        scroll.setBottomMaskImagePosition(0, 343-186-1);
        scroll.setTopMaskImage(dc.getImage("epg_text_sha_top2.png"));
        scroll.setTopMaskImagePosition(0, 6);
        this.add(scroll);
        */

        footer.setBounds(0, 404, 906, 25);
        this.add(footer);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.start()");
        //->Kenneth[2015.4.14] : R5
        resetFooters();

        prepare();
        setVisible(true);

        //->Kenneth[2015.6.12] R5 : VBM 을 위한 sessionId
        sessionId = System.currentTimeMillis();
        EpgVbmController.getInstance().writeAccessAssetPage(sessionId);
        //<-

        //->Kenneth[2016.3.9] R7 
        sViewLaunched = false;
    }

    //->Kenneth[2015.4.15] : R5 : Channel on demand 연동여부에 따라서 scroll text 크기 조절되어야 함.
    private void resetScrollText() {
        this.remove(scroll);
        scroll = new ScrollTexts();
        if (program instanceof PpvProgram) {
            // PPV 채널인 경우 : PpvProgramDetails 에 있는 내용 그대로 세팅한다
            scroll.setSize(519, 190-76);
            scroll.setRows(5);
            scroll.setBottomMaskImage(DataCenter.getInstance().getImage("epg_text_sha.png"));
            scroll.setBottomMaskImagePosition(0, 343-186-76);
            scroll.setTopMaskImage(DataCenter.getInstance().getImage("epg_text_sha_top1.png"));
            scroll.setTopMaskImagePosition(0, 9);
            scroll.setInsets(3, 5, 7, 0);
            scroll.setFont(FontResource.DINMED.getFont(20));

            //->Kenneth[2015.6.11] : R5 highlight 만 white 컬러를 가진다
            scroll.setForeground(new Color(183, 183, 183));
            //scroll.setForeground(Color.white);

            scroll.setRowHeight(21);
            scroll.setVisible(true);
        } else {
            //->Kenneth[2015.8.13] GUI tuning
            // PPV 채널이 아닌 경우
            if (has900Icon()) {
                // 하단에 channel on demand 포스터들이 있는 경우
                scroll.setBounds(48, 165, 519, 200-40);
                scroll.setRows(6);
                scroll.setBottomMaskImagePosition(0, 343-186-1-40);
            } else {
                // 하단에 channel on demand 포스터들이 없는 경우
                scroll.setBounds(48, 165, 519, 221);
                scroll.setRows(9);
                scroll.setBottomMaskImagePosition(0, 343-186-1+21);
            }
            /*
            // PPV 채널이 아닌 경우
            if (has900Icon()) {
                // 하단에 channel on demand 포스터들이 있는 경우
                scroll.setBounds(48, 186, 519, 200-40);
                scroll.setRows(6);
                scroll.setBottomMaskImagePosition(0, 343-186-1-40);
            } else {
                // 하단에 channel on demand 포스터들이 없는 경우
                scroll.setBounds(48, 186, 519, 200);
                scroll.setRows(8);
                scroll.setBottomMaskImagePosition(0, 343-186-1);
            }
            */
            //<-
            scroll.setFont(FontResource.DINMED.getFont(20));

            //->Kenneth[2015.6.11] : R5 highlight 만 white 컬러를 가진다
            scroll.setForeground(new Color(183, 183, 183));
            //scroll.setForeground(Color.white);

            scroll.setRowHeight(21);
            scroll.setVisible(true);
            scroll.setInsets(8, 5, 8+7, 0);
            scroll.setBottomMaskImage(DataCenter.getInstance().getImage("epg_text_sha.png"));
            scroll.setTopMaskImage(DataCenter.getInstance().getImage("epg_text_sha_top2.png"));
            scroll.setTopMaskImagePosition(0, 6);
        }
        this.add(scroll);
    }

    //->Kenneth[2015.4.15] : R5 : 해당 채널이 900 icon 을 가지고 있나 없나 알려줌.
    public boolean has900Icon() {
        if (channel == null) return false;
        int screenData = channel.getScreenData();
        if (screenData > 0) {
            if ((screenData & Channel.SCREEN_MASK_900) != 0) {
                return true;
            }
        }
        return false;
    }

    //->Kenneth[2015.7.19] R5 : VDTRMASTER-5514
    // Search 로부터 상세 페이지가 띄워진 것인지 돌려준다.
    private boolean fromSearch(){
        boolean result = false;
        try {
            //->Kenneth[2015.8.13] VDTRMASTER-5581
            // 상세 페이지 들어오는 경로가 startUnboundApplication 인 경우는 문제 없는데
            // Search 에서 GridEpg.SearchActionAdaptor 로 바로 띄우는 경우 monitor 의 params 
            // 가 mispatch 할 수 있음. 따라서 GridEpg.param 으로 체크하도록 함.
            String[] params = GridEpg.paramsForDetailPage;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.fromSearch() : paramsForDetailPage = "+params);
            //if (params == null) {
            //    MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            //    params = monitor.getParameter(App.NAME);
            //}

            // null 로 안 만들어 주면 Search 에서 오지 않은 경우에도 남아서 영향 끼침.
            //->Kenneth[2015.10.7] 여기서 null 로 하면 resetFooter 등에서 다시 fromSearch 부르면 false 만 리턴함.
            // 따라서 stop() 할 때 없애주는 것으로 옮김
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.fromSearch() : Set paramsForDetailPage = null");
            //GridEpg.paramsForDetailPage = null;

            //MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            //String[] params = monitor.getParameter(App.NAME);
            //<-
            if (params != null && params[0] != null) {
                String param = params[0];
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.fromSearch() params[0] = "+param);
                if ("Search".equals(param)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.fromSearch() returns true.");
                    return true;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.fromSearch() returns false.");
        return false;
    }

    //->Kenneth[2015.4.14] : R5 : Reminder 의 text 를 바꿔야 할 때도 있지만 안 보여줘야 할 때도 있기 때문에
    // 아예 footer 자체를 조작하는 것으로 바꿈
    protected void resetFooters() {
        footer.reset();
        //->Kenneth[2015.7.19] R5 : VDTRMASTER-5514
        if (fromSearch()) {
            footer.addLeftAlignedButton(PreferenceService.BTN_BACK, "epg.back");
        } else {
            footer.addLeftAlignedButton(PreferenceService.BTN_BACK, "epg.back_to_grid");
        }
        //footer.addLeftAlignedButton(PreferenceService.BTN_BACK, "epg.back_to_grid");
        //<-
        Image iconScroll = footer.addButton(PreferenceService.BTN_PAGE, "epg.scroll_description");
        try {
            if (channel != null) {
                if (channel.isFavorite()) {
                    this.iconLittleBoy = footer.addButton(PreferenceService.BTN_PRO, "epg.remove_fav");
                } else {
                    this.iconLittleBoy = footer.addButton(PreferenceService.BTN_PRO, "epg.add_fav");
                }
            }
        } catch (Exception e) {
        }
        try {
            MenuItem reminderMenu = Rs.getReminderMenu(program);
            if (reminderMenu == Rs.MENU_REMOVE_REMINDER) {
                this.textReminder = "menu.remove_reminder";
                footer.addButton(PreferenceService.BTN_B, textReminder);
            } else if (reminderMenu == Rs.MENU_SET_REMINDER) {
                this.textReminder = "menu.set_reminder";
                footer.addButton(PreferenceService.BTN_B, textReminder);
            } else if (reminderMenu == null) {
                this.textReminder = null;
            }
        } catch (Exception e) {
        }
        footer.addButton(PreferenceService.BTN_D, "epg.options");
        footer.linkWithScrollTexts(iconScroll, scroll);
        repaint();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.stop()");
        setVisible(false);
        program = null;
        // R5
        sView.stop();

        //->Kenneth[2016.3.9] R7 
        sViewLaunched = false;
        
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
			ls.hideLoadingAnimation();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
        //->Kenneth[2015.8.26] VDTRMASTER-5613
        if (optionScreen != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.stop() : Call optionScreen.stop()");
            optionScreen.stop();
        }
        //<-
        //->Kenneth[2015.10.7] fromSearch 에서 null 로 하면 resetFooter 등에서 다시 fromSearch 부르면 false 만 리턴함.
        // 따라서 stop() 할 때 없애주는 것으로 옮김
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.stop() : Set paramsForDetailPage = null");
        GridEpg.paramsForDetailPage = null;
        //<-
    }

    public synchronized void start(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.start("+p+")");
        setData(p);
        focus = 0;
        this.start();

        //->Kenneth[2015.6.15] R5 : Channel on demand 갔다가 back 으로 상세 페이지 돌아와야 함.
        // 그래서 현재 데이터 저장함
        EpgCore.channelBeforeGoingToVOD = channel;
        EpgCore.programBeforeGoingToVOD = program;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.start() : EpgCore.channelBeforeGoingToVOD = "+channel);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.start() : EpgCore.programBeforeGoingToVOD = "+program);
        //<-

		
        // R5
        sView.init();
        sView.setOptionPanel((OptionPanel)this);
        
//        sView.setAdultMode(true);
        
        if (has900Icon()) {
        	if (Log.DEBUG_ON) Log.printDebug("ProgramDetails.start, channel.rating=" + 
        			(channel != null ? channel.getRating() : -1));
        	if (channel != null && channel.getRating() >= Constants.RATING_INDEX_18) {
        		sView.setAdultMode(true);
        		menu = createMenu();
				size = menu.size();
        	} else {
        		new Thread("LOAD_SIMILAR") {
        			public void run() {
        				
        				LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance()
        						.get(LoadingAnimationService.IXC_NAME);
        				try {
        					ls.showLoadingAnimation(new Point(480, 270));
        				} catch (RemoteException e) {
        					e.printStackTrace();
        				}
        				SimilarContent[] contents = SimilarContentManager.getInstance().requestSimilarContents(program);
        				if (Log.DEBUG_ON) {
        					Log.printDebug("LOAD_SIMILAR, contents=" + contents);
        				}
        				if (contents != null) {
        					sView.setElements(contents, program);
        					menu = createMenu();
        					size = menu.size();
        				} else {
        					sView.setElements(null, program);
        					SHead head = SimilarContentManager.getInstance().getLastHead();
        					if (head != null && head.getError() != null) {
        						sView.setHead(head);
        						menu = createMenu();
            					size = menu.size();
        					}
        					// R5 - remove
//        					else {
//        						// in the case of has no content from back-end.
//        						sView.addChannelContent(program);
//        						menu = createMenu();
//            					size = menu.size();
//        					}
        				}
        				
        				try {
        					ls.hideLoadingAnimation();
        				} catch (RemoteException e) {
        					e.printStackTrace();
        				}
        				// R5
        				repaint();

        				if (contents != null) {
        					for (int i = 0; i < contents.length; i++) {
        						contents[i].retrieveImage((UIComponent)sView);
        					}
        					sView.startAnimation(SimilarContentView.MODE_INTRO);
        				}
        				
                        /*
        				if (sView.hasContents()) {
        					sView.startAnimation(SimilarContentView.MODE_INTRO);
        				}
                        */
        			}
        		}.start();
        	}
        }
        
        
    }

    protected void setData(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.setData("+p+")");
        //->Kenneth[2017.4.20] VDTRMASTER-6102 때문에 아래 call letter 바꿔치기를 다시 막는다.
        // 대신에 GridEpg.showDetails() 에서 grouped 인 상태에서도 program 바꿔치기하게 만든다. 

        //->Kenneth[2017.4.10] VDTRMASTER-6086 때문에 아래 call letter 바꿔치기를 다시 복구한다.
        // GridEpg 에서는 grouped 에서만 HD 채널로 바꿔주기 때문이다.
        // 더불어 subscription 체크하는 코드 추가한다.

        //->Kenneth[2017.1.23] VDTRMASTER-6019 때문에 아래 call letter 바꿔치기를
        // GridEpg.startDetails() 에서 하게 되므로 여기서는 Exchange 하는 코드 없앤다.

        //->Kenneth[2016.3.7] R7 : Search 는 바뀌지 않을 것이기 때문에 아래 SD -> HD 로 하는 코드는 
        // 일단 그대로 놔두기로 결정. 이후 요구사항이 있으면 ungrouped 에서는 안하게 수정하지 머.

        //->Kenneth[2015.9.18] VDTRMASTER-5643
        // Search 에서 오는 경우 무조건 SD call letter 가 날아옴.
        // 따라서 sister 가 있고 subscribed 된 거면 여기서 HD program 으로 바꿔치기함.
        // 주의사항 : sister program 을 구해서 바꿔치기 하려 했는데 검색결과에 나오는 program 중에는
        // 현재 EPG data 보다 먼 미래의 데이터가 포함되기도 한다. 이런 경우 sister program 을 구할 수 없음.
        // 따라서 program 바꿔치기 보다 call letter 만 HD 채널의 것으로 바꿔치기 한다.

        /*
        //->Kenneth[2015.10.7] VDTRMASTER-5643 reopen 됨. program 의 definition 정보는 신뢰하지 말자.
        if (fromSearch()) {
        //if (fromSearch() && !p.isHd()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.setData() : from Search and SD program");
            try {
                Channel ch = p.getEpgChannel();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.setData() : ch = "+ch);
                if (ch != null) {
                    Channel sisterCh = (Channel)ch.getSisterChannel();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.setData() : sisterCh = "+sisterCh);
                    //->Kenneth[2017.4.10] VDTRMASTER-6086 때문에 다시 복구하면서 추가로 subscription 체크하는 코드 넣는다.
                    if (sisterCh != null && sisterCh.getDefinition() == Channel.DEFINITION_HD && sisterCh.isSubscribed()) {
                    //if (sisterCh != null && sisterCh.getDefinition() == Channel.DEFINITION_HD) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.setData() : Exchange callletter from "+p.callLetter+" to "+sisterCh.getCallLetter());
                        p.callLetter = sisterCh.getCallLetter();
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        //<-
        */

        blockLevel = ParentalControl.getLevel(p);
        title = ParentalControl.getTitle(p);
        program = p;

        channel = p.getEpgChannel();
        channelName = channel.getName();
        if (p instanceof FakeProgram) {
            if (channel.getType() == Channel.TYPE_RADIO) {
                fullName = channel.radioFrequency;
            } else {
                fullName = "";
            }
        } else {
            fullName = channel.fullName;
        }
        channelNumber = Integer.toString(channel.getNumber());
        channelLogo = channel.getLogo();
        //->Kenneth[2015.4.15] : R5 : contructor 에 있던 놈을 여기에 넣음.
        resetScrollText();
        if (blockLevel == ParentalControl.ADULT_CONTENT) {
            scroll.setContents(TextUtil.replace(DataCenter.getInstance().getString("epg.modify_pc_desc2"), "|", "\n"));
        } else {
            //->Kenneth[2015.6.11] R5
            String header = Highlight.getInstance().getMyCategoryName(p); 
            if (header == null) {
                scroll.setContents(p.getFullDescription());
            } else {
                scroll.setContents(header + " - " +p.getFullDescription());
                scroll.setHeader(header);
            }
            title = p.getTitle();
        }

        updateStatus();
    }

    public boolean handleKey(int code) {

    	// R5
    	MenuItem item = null;
    	
        footer.clickAnimationByKeyCode(code);
        switch (code) {
            case KeyCodes.COLOR_D:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : COLOR_D");
                showOptionScreen();
                break;
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_ENTER");
                keyEnter(focus);
                break;
            case OCRcEvent.VK_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_UP");
                focus = (focus - 1 + size) % size;
                repaint();
                // R5
                item = menu.getItemAt(focus);
                if ((item == Rs.MENU_ADULT_COD || item == Rs.MENU_COD) && sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_CLOSE);
                } else if ((item == Rs.MENU_ADULT_COD || item == Rs.MENU_COD) && !sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_OPEN);
                    //->Kenneth[2016.3.9] R7
                    if (!sViewLaunched && program != null) {
                        EpgVbmController.getInstance().writeSimilarDisplayed(program.getId());
                        sViewLaunched = true;
                    }
                    //<-
                } else if (sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_CLOSE);
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_DOWN");
                focus = (focus + 1) % size;
                repaint();
                // R5
                item = menu.getItemAt(focus);
                if ((item == Rs.MENU_ADULT_COD || item == Rs.MENU_COD) && sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_CLOSE);
                } else if ((item == Rs.MENU_ADULT_COD || item == Rs.MENU_COD) && !sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_OPEN);
                    //->Kenneth[2016.3.9] R7
                    if (!sViewLaunched && program != null) {
                        EpgVbmController.getInstance().writeSimilarDisplayed(program.getId());
                        sViewLaunched = true;
                    }
                    //<-
                } else if (sView.isOpened()) {
                	sView.startAnimation(SimilarContentView.MODE_CLOSE);
                }
                break;
            case OCRcEvent.VK_PAGE_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_PAGE_UP");
                scroll.showPreviousPage();
                break;
            case OCRcEvent.VK_PAGE_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_PAGE_DOWN");
                scroll.showNextPage();
                break;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : SEARCH");
                if (optionScreen != null) {
                    optionScreen.stop();
                }
                return false;
            case KeyCodes.COLOR_A:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : COLOR_A");
                return true;
            case KeyCodes.COLOR_B:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : COLOR_B");
                //->Kenneth[2015.4.14] R5 : set/remove a reminder
                if (textReminder != null) {
                    if (textReminder.equals("menu.set_reminder")) {
                        ReminderManager.getInstance().remind(program, this);
                    } else if (textReminder.equals("menu.remove_reminder")) {
                        ReminderManager.getInstance().remove(program);
                        updateStatus();
                    }
                    resetFooters();
                }
                return true;
            case KeyCodes.LITTLE_BOY:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : LITTLE_BOY");
                //->Kenneth[2015.4.14] R5 : add/remove favorite
                Channel ch = channel;
                //->Kenneth[2015.10.3] VDTRMASTER-5677 : favorite 등록후 채널 package 빠지는 경우에도
                // remove 는 될 수 있어야 함. 권한 체크를 add 하는 경우에만 하도록 함.
                //if (!ch.isAuthorized()) return true;
                if (ch.isFavorite()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : LITTLE_BOY : Call removeFavoriteChannel()");
                    EpgCore.getInstance().removeFavoriteChannel(ch, this);
                } else {
                    if (!ch.isAuthorized()) return true;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : LITTLE_BOY : Call addFavoriteChannel()");
                    //->Kenneth[2016.3.9] R7 : VBM 로그 
                    EpgCore.getInstance().addFavoriteChannel(ch, this, true);
                    //EpgCore.getInstance().addFavoriteChannel(ch, this);
                    //<-
                }
                //<-
                resetFooters();
                return true;
            case OCRcEvent.VK_LEFT:
            	if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : VK_LEFT");
            	item = menu.getItemAt(menu.size() - 1);
            	if (item == Rs.MENU_COD && sView.hasContents()) {
            		focus = menu.size() - 1;
            		if (!sView.isOpened()) {
            			sView.startAnimation(SimilarContentView.MODE_OPEN);
                        //->Kenneth[2016.3.9] R7
                        if (!sViewLaunched && program != null) {
                            EpgVbmController.getInstance().writeSimilarDisplayed(program.getId());
                            sViewLaunched = true;
                        }
                        //<-
            		}
            		setDimmedFocus(true);
            		sView.setHasFocus(true);
            		repaint();
            	}
            	return true;
            //->Kenneth[2015.8.19] DDC-113
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : STAR");
                if (optionScreen != null && optionScreen.isStarted()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.handleKey() : STAR : return true");
                    return true;
                }
            //<-
            default:
                return false;
        }
        return true;
    }

    protected void keyEnter(int focus) {
        startClickingEffect();
        MenuItem item = menu.getItemAt(focus);
        // R5
        if (item == Rs.MENU_ADULT_COD) {
        	EpgCore.getInstance().startUnboundApp(SimilarContentView.VOD_APP_NAME,
					new String[] { App.NAME, "CHANNEL_ON_VOD" });
        } else if (item == Rs.MENU_COD) {
        	if (sView.hasContents()) {
	        	setDimmedFocus(true);
	        	sView.setHasFocus(true);
        	}
        } else {
        	selected(item);
        }
    }

    public void updateParentalControl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.updateParentalControl()");
        if (program == null) {
            return;
        }
        setData(program);
        repaint();
    }

    public synchronized void updateStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.updateStatus()");
        if (program == null) {
            return;
        }
        try {
            recordingStatus = RecordingStatus.getInstance().getStatus(program);

            //->Kenneth[2016.3.7] R7 : Group 상태에서만 sister 상태 체크
            if (EpgCore.isChannelGrouped) {
                //->Kenneth[2015.6.19/6.22] R5 : VDTRMASTER-5480
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.updateStatus() : recordingStatus = "+recordingStatus);
                int sisterRecordingStatus = RecordingStatus.getInstance().getSisterStatus(program);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.updateStatus() : sisterRecordingStatus = "+sisterRecordingStatus);
                if (sisterRecordingStatus == RecordingStatus.RECORDING) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.updateStatus() : recordingStatus : Use sister's recording status");
                    recordingStatus = sisterRecordingStatus;
                }
            }
            //<-

            //->Kenneth[2016.3.7] R7 : GridEpg.getOptionMenu 에서 자체 filtering 하기 때문에 여기선 그냥 true 로 호출해도 됨
            options = GridEpg.getOptionMenu(program, true);
            //options = GridEpg.getOptionMenu(program);

            menu = createMenu();
            size = menu.size();
            repaint();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    protected void showOptionScreen() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.showOptionScreen()");
        //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
        //->Kenneth[2016.3.7] R7 : GridEpg.getOptionMenu 에서 자체 filtering 하기 때문에 여기선 그냥 true 로 호출해도 됨
        options = GridEpg.getOptionMenu(program, true);
        //options = GridEpg.getOptionMenu(program);
        super.showOptionScreen();
    }

    protected abstract MenuItem createMenu();

    protected void tuneToChannel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.tuneToChannel()");
        Container c = getParent();
        if (c != null && c instanceof GridEpg) {
            GridEpg grid = (GridEpg) c;
            grid.hideDetails();
            grid.exitToChannel(channel);
        } else {
            try {
                CommunicationManager.getMonitorService().exitToChannel(channel.getId());
            } catch (Exception ex) {
                stop();
            }
        }
    }

    public void notifyReminderResult(Program p, boolean added) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.notifyReminderResult()");
        if (added) {
            updateStatus();
        }
    }

    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        //->Kenneth[2015.4.14] R5 : footer 추가로 구현함
        switch (id) {
            case ADDED_TO_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.notifyEpgEvent() : ADDED_TO_FAVOURITES");
                footer.changeText(iconLittleBoy, "epg.remove_fav");
                break;
            case REMOVED_FROM_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ProgramDetails.notifyEpgEvent() : REMOVED_FROM_FAVOURITES");
                footer.changeText(iconLittleBoy, "epg.add_fav");
                break;
        }
    }

}

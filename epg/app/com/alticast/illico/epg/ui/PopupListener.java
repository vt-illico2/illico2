package com.alticast.illico.epg.ui;

public interface PopupListener {

    void notifySelected(Popup popup, String key);

    void notifyCanceled(Popup popup, boolean byTimer);


}

package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.effect.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.menu.*;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.navigator.ChannelZapper;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import java.awt.event.KeyEvent;
import javax.tv.util.*;
import org.dvb.event.*;
import org.davic.resources.*;
import org.ocap.ui.event.*;
import org.dvb.event.UserEvent;
import java.awt.*;
import java.util.*;
import com.alticast.ui.*;

/**
 * Main UI Class of Grid EPG.
 *
 * @author June Park
 */
public class GridEpg extends FullScreenPanel implements LayeredKeyHandler, EpgListener, EpgDataListenerInternal, ReminderListener,
                                                        MenuListener, Scrollable, CaUpdateListener {

    private static final long serialVersionUID = 12985715871511L;

    public static final int TYPE_DEFAULT  = 0;
    public static final int TYPE_EXTENDED = 1;
    public int type = TYPE_DEFAULT;

    private static final int[] Y_POINTS = { 253, 105 };
    private GridPrograms[] grid = new GridPrograms[2];
    private GridPrograms currentGrid;

    private OptionScreen os;
    public GridEpgDescription desc = new GridEpgDescription();
//    private GridProgramDetails details = new GridProgramDetails();
    public ProgramDetails details;

    private EpgPanel focusedComponent;

    //->Kenneth[2015.4.3] R5 : GridEpgRenderer 에서 갖다 쓰기 위해 public 으로 바꿈
    public static ChannelList channelList;
    static ChannelComparator sortingComparator = ChannelList.ASCENDING_BY_NUMBER;
    public static ChannelComparator comparator = sortingComparator;    // fav filter가 걸리면 sortingComp와 달라질 수 있다.

    private static final String CHANGE_DATE_PREFIX = "menu.change_date.";

    LayeredUI ui;

    String[] param;

    boolean firstType = true;

    Point[] favPoint = new Point[grid.length];
    AnimatedIcon favIcon;
    AnimatedIcon lockIcon;
    RefreshEffect filterEffect;

    private Image iconGuide;

    //->Kenneth[2015.4.3] R5
    private Image iconHighlight;
    //->Kenneth[2015.4.11] R5 : little boy footer
    private Image iconLittleBoy;

    public SearchActionAdapter searchActionAdapter = new SearchActionAdapter();

    //->Kenneth[2015.6.12] R5 : VBM 을 위한 sessionId
    public long sessionId;

    public GridEpg() {
        stack = new Stack();
        stack.push("epg.tv_guide");
        setRenderer(new GridEpgRenderer());

        footer.setBounds(0, 488, 906, 25);

        grid[TYPE_DEFAULT] = new GridPrograms(2, 2);
        grid[TYPE_EXTENDED] = new GridPrograms(4, 4);
        grid[TYPE_EXTENDED].setAutoExpand(true);

        DataCenter dc = DataCenter.getInstance();
        Image iFav = dc.getImage("02_icon_fav.png");
        Image iLock = dc.getImage("02_icon_con.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();

        favIcon = new AnimatedIcon(iFav);
        lockIcon = new AnimatedIcon(iLock);
        favIcon.setContainer(this);
        lockIcon.setContainer(this);

        filterEffect = new RefreshEffect(this, 16, DataCenter.getInstance().getImage("sweep.png"));

        for (int i = 0; i < grid.length; i++) {
            int x = grid[i].getX();
            int y = Y_POINTS[i];
            grid[i].setLocation(x, y);
            favPoint[i] = new Point(x + 5, y + GridProgramsRenderer.HEADER_HEIGHT
                                         + GridProgramsRenderer.V_GAP * grid[i].prevSize + 12);
        }

        LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.add(this);
        window.setVisible(true);
        setLocation(0, 0);

        ui = WindowProperty.EPG_DEFAULT.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug("######################################");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start -- GridEpg.start()");
        if (Log.DEBUG_ON) Log.printDebug("######################################");
        if (ui.isActive()) {
            return;
        }
        EpgDataManager.getInstance().addListener(this);

        //->Kenneth[2015.4.3] R5
        footer.reset();
        //footer.addButton(PreferenceService.BTN_INFO, "epg.details");
        //this.iconGuide = footer.addButton(PreferenceService.BTN_GUIDE, "epg.extended_view");
        //->Kenneth[2015.4.13] R5
        this.iconGuide = footer.addLeftAlignedButton(PreferenceService.BTN_GUIDE, "epg.extended_view");
        //footer.addButton(PreferenceService.BTN_SEARCH, "epg.search");
        //Image iconScroll = footer.addButton(PreferenceService.BTN_PAGE, "epg.change_page");
        this.iconLittleBoy = footer.addButton(PreferenceService.BTN_PRO, "epg.add_fav");
        //->Kenneth[2017.2.27] R7.3
        footer.addButton(PreferenceService.BTN_A, "epg.channel_filters");
        //footer.addButton(PreferenceService.BTN_A, "epg.filters");
        //<-
        footer.addButton(PreferenceService.BTN_B, "epg.change_date");
        this.iconHighlight = footer.addButton(PreferenceService.BTN_C, "epg.highlights");
        footer.addButton(PreferenceService.BTN_D, "epg.options");
        //footer.linkWithScrollTexts(iconScroll, this);
        //->Kenneth[2017.2.22] R7.3 에서 없어짐
        //resetFooterHighlight();
        //->

        param = null;
        try {
            param = CommunicationManager.getMonitorService().getParameter(App.NAME);
        } catch (Exception ex) {
        }
        if (param != null) {
            if (Log.DEBUG_ON) {
                for (int i = 0; i < param.length; i++) {
                    Log.printDebug("GridEpg: param[" + i + "] = " + param[i]);
                }
            }
        }
        //->Kenneth[2015.3.30] R5 : sister 를 제거해야 하기 때문에 cloneList() 를 통해서 데이터를 가져옴.
        //ChannelController 가 가진 channel list 는 바뀌지 않아야 하기 때문임
        channelList = ChannelController.get(0).getChannelList().cloneList().sort(comparator);

        //->Kenneth[2017.2.13] R7.3 : TECH 채널은 GridEpg/MiniEpg 에 보이면 안된다.
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Call channelList.removeTech()");
        channelList.removeTech();
        //<-

        //->Kenneth[2015.8.26] : HDFilter 인지 체크하는 코드 앞으로 옮김 (jira 5614 때문)
        String currentFilterKey = ChannelController.get(0).getCurrentFilterKey();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : currentFilterKey = "+currentFilterKey);
        boolean isHdFilter = false;
        if (Rs.MENU_FILTER_HD.getKey().equals(currentFilterKey)) {
            isHdFilter = true;
        }


        //->Kenneth[2015.4.20] R5 : removeSister 하기 전에 channelList 의 currentIndex 를 Sister HD 로 되어 있다면 SD 로
        //바꿔줘야 함.
        //->Kenneth[2015.8.26] VDTRMASTER-5614 : HD 필터인 경우 아래에서 removeSister 하지 않으므로
        // 여기서도 SD index 로 바꿔놓지 말아야 한다.
        if (isHdFilter) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Do not reset index. It's HD filter");

        //->Kenneth[2016.3.7] R7 : Ungrouped 상태에서는 grouping 관련 아무일도 하지 않음.
        } else if (!EpgCore.isChannelGrouped) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Do not reset index. It's Ungrouped");
        //<-

        } else {
            try {
                Channel oldFocus = channelList.getCurrentChannel();
                if (oldFocus != null) {
                    Channel oldSister = (Channel)oldFocus.getSisterChannel();
                    if (oldSister != null) {
                        int oldFocusDef = oldFocus.getDefinition();
                        int oldSisterDef = oldSister.getDefinition();
                        if (oldFocusDef > oldSisterDef) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Bingo! Call setCurrentChannel("+oldSister+")");
                            channelList.setCurrentChannel(oldSister);
                        }
                    }
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        //->Kenneth[2015.8.4] : VDTRMASTER-5544 : HD Filter 의 경우 removeSister 하지 않는다
        if (isHdFilter) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Do not call removeSister. It's HD filter");

        //->Kenneth[2016.3.7] R7 : Ungrouped 상태에서는 grouping 관련 아무일도 하지 않음.
        } else if (!EpgCore.isChannelGrouped) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Do not call removeSister. It's Ungrouped");
        //<-

        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : channelList.size() before removeSister() = "+channelList.size());
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : channelList.getCurrentChannel() before removeSister() = "+channelList.getCurrentChannel());
            //->Kenneth[2015.10.7] VDTRMASTER-5695. 여기서는 subcription 안 따짐.
            channelList.removeSister(true, false);
            //channelList.removeSister(true);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : channelList.size() after removeSister() = "+channelList.size());
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : channelList.getCurrentChannel() after removeSister() = "+channelList.getCurrentChannel());
        }
        //<-
        //channelList = ChannelController.get(0).getChannelList().sort(comparator);

        firstType = true;

        if (Definitions.GRID_FORMAT_FULL.equals(DataCenter.getInstance().get(PreferenceNames.GRID_FORMAT))) {
            changeType(TYPE_EXTENDED, param);
            footer.changeText(iconGuide, "epg.normal_view");
        } else {
            changeType(TYPE_DEFAULT, param);
            footer.changeText(iconGuide, "epg.extended_view");
        }
        if (!App.R3_TARGET || EpgCore.epgVideoContext) {
            // TODO - 시작할때 block 걸려 있어도 상관 없나?
            ChannelController.get(0).changeChannel(-1);
        }

        PromotionBanner banner = PromotionBanner.getInstance();
        banner.startMonitoring();
        banner.setBounds(496, 34, 230, 40);
        this.add(banner);
        this.add(footer);
        prepare();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        resetFooterLittleBoy();
        //->Kenneth[2015.6.15] R5 
        if (backFromVOD()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : programBeforeGoingToVOD = "+EpgCore.programBeforeGoingToVOD);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : channelBeforeGoingToVOD = "+EpgCore.channelBeforeGoingToVOD);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : currentGrid = "+currentGrid);
            if (EpgCore.programBeforeGoingToVOD != null && EpgCore.channelBeforeGoingToVOD != null) {
                channelList.setCurrentChannel(EpgCore.channelBeforeGoingToVOD);
                if (currentGrid != null) {
                    currentGrid.setFocusedChannel(EpgCore.channelBeforeGoingToVOD);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : Go to Detail page directly");
                showDetails(EpgCore.programBeforeGoingToVOD);
                banner.setRotationTimer(true);
                CaManager.getInstance().addListener(this);
                sessionId = System.currentTimeMillis();
                super.start();
                ui.activate();
                return;
            }
        }
        //<-
        super.start();
        ui.activate();
        banner.setRotationTimer(true);
        CaManager.getInstance().addListener(this);
        //->Kenneth[2015.4.16] R5 
        //->Kenneth[2015.7.9] R5 : VDTRMASTER-5511 때문에 아래로 옮김 
        /*
        if (Tutorial.getInstance().needToDisplayTutorial()) {
            if (!backFromHelp()) {
                PopupController.getInstance().showTutorialPopup(this);
                Tutorial.getInstance().increaseViewCount();
            }
        }
        */
        //->Kenneth[2015.6.12] R5 : VBM 을 위한 sessionId
        sessionId = System.currentTimeMillis();

        //->Kenneth[2015.7.10] TANK : 하단의 timeout 시간을 조절하기 위한 flag
        boolean noFavPopupVisible = false;
        //->Kenneth[2015.6.30] TANK : filter 변경
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            String[] params = monitor.getParameter(App.NAME);
            if (params != null && params.length >= 2 && params[0] != null && params[1] != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.start() : params[0] = "+params[0]+", params[1] = "+params[1]);
                if ("Menu".equals(params[0])) {
                    if ("ALL".equals(params[1])) {
                        EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_ALL.getKey(), this);
                    } else if ("MY_PACKAGE".equals(params[1])) {
                        EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_SUBSCRIBED.getKey(), this);
                    } else if ("MY_FAVORITE".equals(params[1])) {
                        ChannelList allFavList = ChannelDatabase.current.filter(ChannelDatabase.favoriteFilter);
                        if (allFavList.size() == 0) {
                            PopupController.getInstance().showNoFavPopup();
                            noFavPopupVisible = true;
                        } else {
                            EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_FAVOURITES.getKey(), this);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-

        //->Kenneth[2015.8.24] R5 : VDTRMASTER-5610 Cisco 때문에 timeout 을 500ms 늘임
        //->Kenneth[2015.7.9] R5 : VDTRMASTER-5511 tutorial 이 좀 늦게 떠야함.
        if (Tutorial.getInstance().needToDisplayTutorial()) {
            if (!backFromHelp()) {
                try {
                    if (noFavPopupVisible) {
                        Thread.sleep(1300+500);
                    } else {
                        Thread.sleep(700+500);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                PopupController.getInstance().showTutorialPopup(this);
                Tutorial.getInstance().increaseViewCount();
            }
        }
        //<-
        if (Log.DEBUG_ON) Log.printDebug("######################################");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"End -- GridEpg.start()");
        if (Log.DEBUG_ON) Log.printDebug("######################################");
    }

    //->Kenneth[2015.4.18] R5 : help 로부터 EPG 가 호출된 것인지 돌려준다. 이때는 tutorial 띄우지 않음. 
    private boolean backFromHelp(){
        boolean result = false;
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            String[] params = monitor.getParameter(App.NAME);
            if (params != null && params[0] != null) {
                String param = params[0];
                if ("Help".equals(param)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.backFromHelp() returns true.");
                    return true;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.backFromHelp() returns false.");
        return false;
    }

    //->Kenneth[2015.6.15] R5 : VOD 로부터 EPG 가 호출된 것인지 돌려준다. 
    private boolean backFromVOD(){
        boolean result = false;
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            String[] params = monitor.getParameter(App.NAME);
            if (params != null && params.length > 1 && params[0] != null && params[1] != null) {
                if (params[0].equals(MonitorService.REQUEST_APPLICATION_LAST_KEY) && "VOD".equals(params[1])) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.backFromVOD() returns true.");
                    return true;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.backFromVOD() returns false.");
        return false;
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.stop()");
        CaManager.getInstance().removeListener(this);
        EpgDataManager.getInstance().removeListener(this);
        PromotionBanner banner = PromotionBanner.getInstance();
        banner.stopMonitoring();
        banner.setRotationTimer(false);
        hideOption();
        firstType = true;
        hideDetails();
        ui.deactivate();
        this.removeAll();
        if (currentGrid != null) {
            currentGrid.stop();
            currentGrid = null;
        }
        if (desc != null) {
            desc.stop();
        }
        channelList = null;
        EpgCore.getInstance().stopSearch();
        super.stop();
        EpgDataManager.getInstance().epgClosed();
        //->Kenneth[2016.7.19] R7.2 On-screen bebug 화면 지움
        DebugScreen.getInstance().dispose();
        //<-
    }

    //->Kenneth[2017.2.22] R7.3 에서 없어짐
    /*
    //->Kenneth[2015.4.3] R5 : 현재의 Highlight 에 따라서 footer 의 텍스트를 바꿔준다.
    private void resetFooterHighlight() {
        if (Highlight.getInstance().getCurrentHighlight().equals(Highlight.HIGHLIGHT_NONE)) {
            footer.changeText(iconHighlight, "epg.highlights");
        } else {
            footer.changeText(iconHighlight, "epg.clear_highlights");
        }
    }
    */

    //->Kenneth[2015.4.11] R5 : 현재의 favorite 에 따라서 footer 의 텍스트를 바꿔준다.
    private void resetFooterLittleBoy() {
        if (currentGrid == null) return;
        Channel curCh = currentGrid.getFocusedChannel();
        if (curCh == null) return;
        if (curCh.isFavorite()) {
            footer.changeText(iconLittleBoy, "epg.remove_fav");
        } else {
            footer.changeText(iconLittleBoy, "epg.add_fav");
        }
    }

    public long getPageTime() {
        GridPrograms cur = currentGrid;
        if (cur != null) {
            return cur.pageTime;
        }
        return 0;
    }

    private void changeType(int newType) {
        changeType(newType, null);
    }

    private synchronized void changeType(int newType, String[] param) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GridEpg.changeType : " + param);
        }
        this.type = newType;
        GridPrograms old = currentGrid;
        currentGrid = grid[type];
        if (old != null) {
            this.remove(old);
        }
        this.add(currentGrid);
        setFocusedComponent(null);
        boolean needDetail = startDetails(param);
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GridEpg.changeType.needDetail : " + needDetail);
        }

        favIcon.setLocation(favPoint[type]);
        lockIcon.setLocation(favPoint[type]);

        if (!needDetail) {
            if (old != null) {
                old.stop();
                currentGrid.start(old.pageTime, old.getFocus(), old.focusedEvent, true);
            } else {
                currentGrid.start();
            }
        }

        setFocusedComponent(currentGrid);
        if (type == TYPE_DEFAULT) {
            desc.setProgram(currentGrid.focusedEvent);
            desc.setVisible(true);
            this.add(desc);
        } else {
            this.remove(desc);
            desc.setProgram(null);
        }
        if (type == TYPE_EXTENDED) {
            EpgCore.getInstance().getVideoResizer().maximize();
            ChannelZapper.getInstance().setChannelBanner(ChannelBanner.EMPTY);
        } else {
            EpgCore.getInstance().getVideoResizer().resize(
                        ((GridEpgRenderer)renderer).getVideoBounds(),
                        VideoController.SHOW, VideoController.CHANNEL_INFO);
        }
        repaint();
        if (needDetail) {
            showDetails(currentGrid.focusedEvent);
        }
        EpgVbmController.getInstance().writeUsedEpgLayout(
                            type == TYPE_DEFAULT ? EpgVbmController.FULL_SCALED : EpgVbmController.FULL_COMPLETE);
    }

    //->Kenneth[2015.8.13] VDTRMASTER-5581
    public static String[] paramsForDetailPage = null;
    //<-
    private boolean startDetails(String[] param) {
        if (param == null || param.length < 3) {
            this.param = null;
            return false;
        }
        if (Log.DEBUG_ON) {
            for (int i = 0; i < param.length; i++) {
                Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails.param[" + i + "] = " + param[i]);
            }
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails.channelList = " + channelList);
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails.currentGrid = " + currentGrid);
        }
        //->Kenneth[2017.1.23] VDTRMASTER-6019
        // Search 에서 온 경우 sister HD 가 있으면 그 call letter 로 바꿔준다.
        // 여기에 이 코드가 추가되었으므로 ProgramDetails 에서 call letter exchange 하던 코드는 필요없으므로 막는다.
        // (중요) : 이 작업은 Ungrouped 에만 동작하도록 한다.
        // 왜냐하면 Grouped 상태의 channelList 는 sister 채널중 SD 의 것으로만으로 이루어져 있음 (Grid 자체가 채널넘버에
        // 따라 ordering 되어야 하기 때문). 따라서 HD 로 바꿔치기 하면 이후의 channelList 에서 해당 HD 채널을 찾을 수
        // 없어서 이상 동작함.
        try {
            if (!EpgCore.isChannelGrouped) {
                if (param != null && param[0] != null) {
                    String param0 = param[0];
                    if ("Search".equals(param0)) {
                        Channel chFromSearch = ChannelDatabase.current.getChannelByName(param[1]);
                        Channel sisterCh = (Channel)chFromSearch.getSisterChannel();
                        if (sisterCh != null && sisterCh.getDefinition() == Channel.DEFINITION_HD && sisterCh.isAuthorized()) {
                            param[1] = sisterCh.getCallLetter();
                            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : Exchange callLetter from "+chFromSearch.getCallLetter()+" to "+param[1]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-

        //->Kenneth[2015.8.13] VDTRMASTER-5581
        Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : Set paramsForDetailPage = param");
        paramsForDetailPage = param;
        //<-

        Channel ch = ChannelDatabase.current.getChannelByName(param[1]);
        if (ch == null) {
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : ChannelDatabase.current.getChannelByName() returns null, So return false");
            return false;
        }
        int index = channelList.search(ch);
        Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : index = "+index);
        if (index < 0) {
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : Call EpgCore.applyFilter(ALL)");
            EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_ALL.getKey(), this);
            index = channelList.search(ch);
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : index again = "+index);
            if (index < 0) {
                Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() returns false");
                return false;
            }
        }
        long time = Long.parseLong(param[2]);
        Program p = EpgDataManager.getInstance().getProgram(ch, time);
        Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : p = "+p);
        if (p == null) {
            p = EpgDataManager.getInstance().requestProgram(ch.getName(), time);
            Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : p again = "+p);
            if (p == null) {
                Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() returns false");
                return false;
            }
        }
        long cur = System.currentTimeMillis();
        long pageTime = cur - cur % (Constants.MS_PER_HOUR / 2) - GridPrograms.PAGE_TIME_GAP;
        while (pageTime + GridPrograms.PAGE_TIME_GAP <= time) {
            pageTime += GridPrograms.PAGE_TIME_GAP;
        }
        Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() : Call GridPrograms.start("+index+", "+p+")");
        currentGrid.start(pageTime, index, p, false);
        // start time
        Log.printDebug(App.LOG_HEADER+"GridEpg.startDetails() returns true");
        return true;
    }

    /** Filter 변경. */
    //->Kenneth[2015.8.21] VDTRMASTER-5593 : synchronized 를 없앰.
    // 이게 해법인지 솔직히 모르겠음. 이명구 책임이 잡은 dump 를 참조해서 코드를 따라가다 보니 다음과 같은
    // flow 가 생길 가능성 있음. 
    // GridEpg.start 
    // -> (synchronized) ChannelController.changeChannel()
    // -> EpgCore.applyFilter
    // -> (synchronized) GridEpg.filterChanged 에서 다시
    // -> EpgCore.applyFilter(All)
    // 여기에서 무한루프에 빠지거나 혹은
    // 이때 다른 루트로 changeChannel 이 발생하고 걔가 EpgCore.applyFilter 를 타고 들어오면 문제 생길 가능성 있음.
    // 그러나 정확한 시나리오를 파악하기 힘들고 재현 방법도 딱히 없는지라 일단 의심스러운
    // filterChanged 에서 synchronized 를 제거하고 릴리즈 해 보기로 함.
    // side effect 생길 가능성 존재함.
    public void filterChanged(ChannelList list, String filterName) {
    //public synchronized void filterChanged(ChannelList list, String filterName) {
    //<-
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Start : GridEpg.filterChanged("+filterName+")");

        //->Kenneth[2017.2.13] R7.3 : TECH 채널은 GridEpg/MiniEpg 에 보이면 안된다.
        // 아래 cloneList 하는 이유는 removeTech 를 여기에만 적용하기 위함이다.
        if (list != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : Call list.cloneList()");
            list = list.cloneList();
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : Call list.removeTech()");
        list.removeTech();
        //<-

        ChannelFilter filter = ChannelDatabase.getFilter(filterName);
        if (filter != null && filter instanceof ChannelComparator) {
            comparator = (ChannelComparator) filter;
        } else {
            comparator = sortingComparator;
        }
        //->Kenneth[2015.7.20] R5 : HD filter 의 경우 removeSister 하면 안된다. isHdFilter 추가함.
        boolean isHdFilter = false;
        if (Rs.MENU_FILTER_HD.getKey().equals(filterName)) {
            isHdFilter = true;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : isHdFilter = "+isHdFilter);
        ChannelList oldList = channelList;
        if (oldList != null) {
            if (list.size() > 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : Call setChannelList("+isHdFilter+")");
                setChannelList(list, comparator, isHdFilter);
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : Before call EpgCore.applyFilter(All)");
                ChannelList allList = EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_ALL.getKey(), this);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : After call EpgCore.applyFilter() : "+allList);
                if (allList != null && oldList.size() != allList.size()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.filterChanged() : Call setChannelList()");
                    setChannelList(allList, comparator);
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"End : GridEpg.filterChanged("+filterName+")");
    }

    /** Sorting 변경. */
    private void sort(ChannelComparator c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.sort("+c+")");
        sortingComparator = c;
        setChannelList(channelList, c);
        //->Kenneth[2016.3.9] R7 : sort 변경시 VBM 로그 남김
        if (c.equals(ChannelList.ASCENDING_BY_NUMBER)) {
            EpgVbmController.getInstance().writeSortingChanged("NUMBER");
        } else if (c.equals(ChannelList.ASCENDING_BY_NAME)) {
            EpgVbmController.getInstance().writeSortingChanged("NAME");
        }
        //<-
    }

    //->Kenneth[2015.7.20] R5 : HD filter 의 경우 removeSister 하면 안된다.
    private void setChannelList(ChannelList list, ChannelComparator c) {
        setChannelList(list, c, false);
    }
    //<-

    //->Kenneth[2015.7.19] R5 : VDTRMASTER-5512
    // SD/HD grouped 채널에서 filter 변경하면 오동작함. 이유는 grouped 채널인 경우 focusedEvent 는
    // HD 의 것을 사용하므로 채널을 HD 채널을 가리킴. 그러나 실제 index 는 SD 의 것이어야 함.
    // 왜냐하면 아래에서 removeSister 를 호출해서 HD 채널들을 지우게 되니까.
    // 따라서 sister 가 있고 그놈이 SD 이면 그 놈으로 채널 교체
    //->Kenneth[2015.7.20] R5 : HD filter 의 경우 removeSister 하면 안된다.
    private synchronized void setChannelList(ChannelList list, ChannelComparator c, boolean isHdFilter) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList()");
        ChannelList newList = list.sort(c);
        Channel curCh = (Channel) currentGrid.focusedEvent.getChannel();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : focusedEvent.getChannel() = "+curCh);

        //->Kenneth[2016.3.7] R7 : Ungrouped 에서는 sister 관련 동작 안하도록 아래 if 문 추가
        if (EpgCore.isChannelGrouped) {
        //<-

            try {
                Channel sisterCh = (Channel)curCh.getSisterChannel();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : sisterCh = "+sisterCh);
                if (sisterCh != null) {
                    int def = curCh.getDefinition();
                    int sisterDef = sisterCh.getDefinition();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : def = "+def);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : sisterDef = "+sisterDef);
                    if (def > sisterDef) {
                        // 여기서 HD -> SD 로 교체
                        curCh = sisterCh;
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        //->Kenneth[2015.9.8] VDTRMASTER-5622
        // 기존 코드는 newFocus 를 구한 후에 removeSister 를 했음. 그러나 위에 sort() 함수는 새로운 ChannelList 를
        // 생성하게 됨. 따라서 sister HD 채널이 모두 포함된 상태이므로 여기서 newFocus 를 구하면 안됨.
        // 따라서 removeSister 하는 부분을 newFocus 얻기 전에 하도록 수정한다.
        // 그리고 channelList 가 아닌 newList 에서 removeSister 하도록 한다.
        if (isHdFilter) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Don't Call removeSister()");

        //->Kenneth[2016.3.7] R7 : Ungrouped 에서는 sister 관련 동작 안하도록 한다.
        } else if (!EpgCore.isChannelGrouped) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Don't Call removeSister(). It's Ungrouped");
        //<-

        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Call removeSister()");
            //->Kenneth[2015.10.7] VDTRMASTER-5695. 여기서는 subcription 안 따짐.
            newList.removeSister(true, false);
            //newList.removeSister(true);
            //channelList.removeSister();
        }
        //<-
        int newFocus = newList.search(curCh);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : newFocus = "+newFocus);
        channelList = newList;
        /*
        if (isHdFilter) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Don't Call removeSister()");
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Call removeSister()");
            channelList.removeSister();
        }
        */
        comparator = c;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : GridPrograms.start()");
        currentGrid.start(currentGrid.pageTime, newFocus, null, true);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList() : Call channelFocusChanged()");
        channelFocusChanged();
    }
    /*
    private synchronized void setChannelList(ChannelList list, ChannelComparator c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setChannelList()");
        ChannelList newList = list.sort(c);
        int newFocus = newList.search((Channel) currentGrid.focusedEvent.getChannel());
        channelList = newList;
        //->Kenneth[2015.4.3] R5 : filter 변경후 라던지 하여간 channelList 를 셋할때 sister 지워줌
        channelList.removeSister();
        comparator = c;
        currentGrid.start(currentGrid.pageTime, newFocus, null, true);
        channelFocusChanged();
    }
    */
    //<-

    public boolean handleKeyEvent(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        return handleKey(event.getCode());
    }

    //->Kenneth[2015.4.20] R5 : 받은 채널에서 sister 가 있는지, 어느 것이 높은 resolution 인지
    // 가입은 되어 있는지 확인해서 target channel 이 원 채널이 될지, sister 가 될지 돌려준다.
    public static Channel decideTargetChannel(Channel src) {
        if (src == null) return src;

        //->Kenneth[2016.3.7] R7 : Ungrouped 에서는 동작하지 않는다.
        if (!EpgCore.isChannelGrouped) {
            return src;
        }
        //<-

        try {
            Channel sister = (Channel)src.getSisterChannel();
            if (sister == null) return src;
            int srcDef = src.getDefinition();
            int sisterDef = sister.getDefinition();
            if (srcDef == sisterDef) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.decideTargetChannel() : Strange!! Same Definition!!!");
            }
            if (srcDef >= sisterDef) return src;
            if (sister.isSubscribed()) {
                //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.decideTargetChannel() returns sister : "+sister);
                return sister;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return src;
    }


    void channelFocusChanged() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.channelFocusChanged()");
        if (type == TYPE_DEFAULT) {
            desc.setProgram(currentGrid.focusedEvent);
        }
        //->Kenneth[2015.4.11] R5  
        resetFooterLittleBoy();
        repaint();
    }

    public Channel getFocusedChannel() {
        return currentGrid.getFocusedChannel();
    }

    public boolean hasMultiplePages() {
        return currentGrid != null && currentGrid.hasMultiplePages();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey : " + code);
        }
        Program old = currentGrid.focusedEvent;
        if (focusedComponent.handleKey(code)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : focusedComponent returns true");
            if (currentGrid != null && old != currentGrid.focusedEvent) {
                channelFocusChanged();
            }
            return true;
        }
        //->Kenneth[2015.4.13] R5 : leftAlignedButton 에 대한 click animation 
        if (code == OCRcEvent.VK_GUIDE) {
            //Info 눌러 들어간 상세화면에서는 동작하면 안됨
            if (focusedComponent != details) {
                footer.clickAnimation(iconGuide);
            }
        } else {
            footer.clickAnimationByKeyCode(code);
        }
        switch (code) {
            case KeyCodes.LITTLE_BOY:
                Channel ch = currentGrid.getFocusedChannel();
                //->Kenneth[2015.10.3] VDTRMASTER-5677 : favorite 등록후 채널 package 빠지는 경우에도
                // remove 는 될 수 있어야 함. 권한 체크를 add 하는 경우에만 하도록 함.
                //if (!ch.isAuthorized()) return true;
                if (ch.isFavorite()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : LITTLE_BOY : Call removeFavoriteChannel()");
                    EpgCore.getInstance().removeFavoriteChannel(ch, this);
                } else {
                    if (!ch.isAuthorized()) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : LITTLE_BOY but !isAuthorized(), so return.");
                        return true;
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : LITTLE_BOY : Call addFavoriteChannel()");

                    //->Kenneth[2016.3.9] R7 : VBM 로그 
                    EpgCore.getInstance().addFavoriteChannel(ch, this, true);
                    //EpgCore.getInstance().addFavoriteChannel(ch, this);
                    //<-
                }
                //<-
                resetFooterLittleBoy();
                return true;
            case KeyCodes.COLOR_A:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : COLOR_A");
                hideOption();
                showFilterOption();
                EpgVbmController.getInstance().writeEpgRemoteKeySelection(EpgVbmController.FILTERS);
                return true;
            case KeyCodes.COLOR_B:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : COLOR_B");
                hideOption();
                showChangeDateOption();
                return true;
            case KeyCodes.COLOR_C:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : COLOR_C");
                //->Kenneth[2015.4.11] R5 : Info 눌러 들어간 상세화면에서는 동작하면 안됨
                if (focusedComponent == details) {
                    return true;
                }
                //->Kenneth[2017.2.22] R7.3 
                hideOption();
                showHighlightOption();
                /*
                //->Kenneth[2015.4.4] R5
                if (Highlight.getInstance().isNone()) {
                    // Highlight 팝업을 띄워야 함.
                    PopupController.getInstance().showHighlightSelectionPopup(this);
                } else {
                    // Clear Highlight 가 실행되어야 함
                    Highlight.getInstance().setHighlight(Highlight.HIGHLIGHT_NONE);
                    highlightChanged();
                }
                */
                //<-
                return true;
                //return PromotionBanner.getInstance().processKeyEvent();
            case KeyCodes.COLOR_D:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : COLOR_D");
                if (!currentGrid.loading) {
                    hideOption();
                    showOption(currentGrid.focusedEvent);
                    EpgVbmController.getInstance().writeEpgRemoteKeySelection(EpgVbmController.OPTIONS);
                }
                return true;
            case KeyCodes.RECORD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : RECORD");
                if (EpgCore.getInstance().supportDvr()) {
                    //->Kenneth[2015.10.15] DDC-114 : EpgCore.record() 에서 판단해서 팝업 띄움.
                    /*
                    //->Kenneth[2015.2.2] 4K
                    Channel curCh = currentGrid.getFocusedChannel();
                    if (EpgCore.getInstance().needToShowIncompatibilityPopup(curCh)) {
                        if (Log.DEBUG_ON) Log.printDebug("GridEpg.handleKey() : Call showIncompatibilityErrorMessage()");
                        EpgCore.getInstance().showIncompatibilityErrorMessage();
                        return true;
                    }
                    */
                    Channel curCh = currentGrid.getFocusedChannel();
                    EpgCore.getInstance().record(curCh, currentGrid.focusedEvent);
                    return true;
                } else {
                    return false;
                }
            case KeyCodes.STOP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : STOP");
                if (EpgCore.getInstance().supportDvr()) {
                    EpgCore.getInstance().cancelRecord(currentGrid.focusedEvent);

                    //->Kenneth[2015.3.7] R7 : Ungrouped 에서는 sister 레코딩 신경쓸 필요 없음.
                    if (!EpgCore.isChannelGrouped) {
                        return true;
                    }
                    //<-

                    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480 
                    int recStatus = RecordingStatus.getInstance().getStatus(currentGrid.focusedEvent);
                    if (Log.DEBUG_ON) Log.printDebug("GridEpg.handleKey() : recStatus = "+recStatus);
                    // cancelRecording 이 두번 불리우면 팝업 두번 뜸. 따라서 recording 중인 놈으로 이미 호출했으면
                    // sister 인 놈은 호출하지 않음.
                    if (recStatus != RecordingStatus.RECORDING) {
                        if (RecordingStatus.getInstance().getSisterStatus(currentGrid.focusedEvent) == RecordingStatus.RECORDING) {
                            if (Log.DEBUG_ON) Log.printDebug("GridEpg.handleKey() : Call EpgCore.cancelRecord(sisterProgram)");
                            EpgCore.getInstance().cancelRecord(currentGrid.focusedEvent.getSisterProgram());
                        }
                    }
                    //<-
                    return true;
                } else {
                    return false;
                }
//            case OCRcEvent.VK_PAGE_UP:
//                if (type == TYPE_DEFAULT) {
//                    desc.showPreviousPage();
//                    return true;
//                }
//                break;
//            case OCRcEvent.VK_PAGE_DOWN:
//                if (type == TYPE_DEFAULT) {
//                    desc.showNextPage();
//                    return true;
//                }
//                break;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : SEARCH");
                EpgCore.getInstance().launchSearch(SearchService.PLATFORM_EPG, true);
                hideOption();
                EpgVbmController.getInstance().writeEpgRemoteKeySelection(EpgVbmController.SEARCH);
                return true;
            case OCRcEvent.VK_GUIDE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : VK_GUIDE");
                // Note - change rules from new EPG UI
                if (focusedComponent instanceof GridPrograms) {
                    if (firstType) {
                        changeType((type + 1) % grid.length);
                        firstType = false;
                        footer.changeText(iconGuide, "epg.exit");
                    } else {
                        exitToChannel();
                    }
                }
                return true;
            case OCRcEvent.VK_EXIT:
            case OCRcEvent.VK_LIVE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : VK_EXIT/VK_LIVE");
                exitToChannel();
                return true;
            case KeyCodes.FAV:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : FAV");
                try {
                    //->Kenneth[2015.10.7] VDTRMASTER-5663 
                    // VDTRMASTER-5664 에 의해서 수정된 구현을 이용하도록 한다.
                    Channel curCh = currentGrid.getFocusedChannelWithoutChange();
                    if (curCh != null) {
                        Channel nextFavCh = EpgCore.getInstance().getNextFavChannel(curCh, true);
                        if (nextFavCh == null) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : FAV : showNoFavPopup()");
                            PopupController.getInstance().showNoFavPopup();
                        } else {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : FAV : setFocusedChannel("+nextFavCh+")");
                            currentGrid.setFocusedChannel(nextFavCh);
                            repaint();
                        }
                    }
                    /*
                    ChannelList allFavList = ChannelDatabase.current.filter(ChannelDatabase.favoriteFilter);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : allFavList.size() = "+allFavList.size());
                    if (allFavList.size() == 0) {
                        DataCenter dc = DataCenter.getInstance();
                        PopupController.getInstance().showNoFavPopup();
                        return true;
                    }
                    ChannelList filteredFavList = channelList.filter(ChannelDatabase.favoriteFilter);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : filteredFavList.size() = "+filteredFavList.size());
                    if (filteredFavList.size() == 0) {
                        // no fav channel in current list
                        ChannelList allList = EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_ALL.getKey(), this);
                        setChannelList(allList, comparator);
                        filteredFavList = allFavList;
                    }
                    int index = filteredFavList.search(currentGrid.getFocusedChannel());
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : current Ch = "+currentGrid.getFocusedChannel());
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : index = "+index);
                    if (index < 0) {
                        index = 0;
                    } else {
                        index = (index + 1) % filteredFavList.size();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : new index = "+index);
                    }
                    Channel newFocusedChannel = filteredFavList.getChannelAt(index);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey(FAV) : newFocusedChannel = "+newFocusedChannel);
                    currentGrid.setFocusedChannel(newFocusedChannel);
                    repaint();
                    */
                } catch (Exception e) {
                    Log.print(e);
                }
                return true;
            case OCRcEvent.VK_LAST:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : LAST");
                if (focusedComponent instanceof OptionPanel) {
                    boolean fromSearch = false;
                    String[] epgParam = this.param;
                    if (epgParam != null) {
                        if (Log.DEBUG_ON) {
                            for (int i = 0; i < param.length; i++) {
                                Log.printDebug("GridEpg.LAST.param[" + i + "] = " + param[i]);
                            }
                        }
                        if (epgParam.length > 0 && "Search".equals(epgParam[0])) {
                            fromSearch = true;
                        }
                    }
                    if (fromSearch) {
                        EpgCore.getInstance().launchSearch(SearchService.PLATFORM_EPG, false);
                        param = null;
                        //->Kenneth[2015.6.12] R5 : VBM 추가
                        EpgVbmController.getInstance().writeExitByBack(ProgramDetails.sessionId);
                    } else {
                        hideDetails();
                    }
                }
                return true;
            case OCRcEvent.VK_INFO:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : INFO");
                if (focusedComponent instanceof OptionPanel) {
                    hideDetails();
                } else {
                    if (!(currentGrid.focusedEvent instanceof NullProgram) && !currentGrid.loading) {
                        showDetails(currentGrid.focusedEvent);
                    }
                }
                return true;
//            case OCRcEvent.VK_PINP_MOVE:
//                if (Log.EXTRA_ON) {
//                    CaManager.getInstance().checkAllChannels(true);
//                    ChannelList list = EpgCore.getInstance().applyFilter(Rs.MENU_FILTER_TYPE_SDV.getKey(), this);
//                    if (list.size() > 0) {
//                        setChannelList(list, comparator);
//                    }
//                }
//                return false;
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : CH UP/DOWN");
//            case KeyCodes.FORWARD:
//            case KeyCodes.BACK:
//            case KeyCodes.REWIND:
//            case KeyCodes.FAST_FWD:
//            case KeyCodes.PAUSE:
//            case KeyCodes.PLAY:
                return (type != TYPE_DEFAULT);
            //->Kenneth[2015.8.19] DDC-113
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : STAR");
                if (os != null && os.isStarted()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.handleKey() : STAR : return true");
                    return true;
                }
            //<-
            default:
                return false;
        }
    }

    public synchronized void showDetails(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails("+p+")");
        //->Kenneth[2017.4.20] VDTRMASTER-6102 를 해결하기 위해서 grouped 인 경우는 여기에 아래 처리함.
        // ingrouped 인 경우는 startDetails 에서 처리했음.
        // 이렇게 각각 나누는 이유는 grouped 에 관계없이 startDetails 에서 행해지는 동작이 필요하기 때문임.
        // 그 동작이 이루어진 후(focused channel 등 설정) detail 화면으로 가기 바로 전에 
        // program 을 바꿔준다.
        try {
            if (EpgCore.isChannelGrouped) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : It's Grouped");
                if (paramsForDetailPage != null && paramsForDetailPage[0] != null) {
                    String param0 = paramsForDetailPage[0];
                    if ("Search".equals(param0)) {
                        Channel ch = p.getEpgChannel();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() ch = "+ch);
                        Channel sisterCh = (Channel)ch.getSisterChannel();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() sisterCh = "+ch);
                        if (sisterCh != null && sisterCh.getDefinition() == Channel.DEFINITION_HD && sisterCh.isAuthorized()) {
                            long time = Long.parseLong(paramsForDetailPage[2]);
                            Program sisterProgram = EpgDataManager.getInstance().getProgram(sisterCh, time);
                            Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : sisterProgram = "+sisterProgram);
                            if (sisterProgram == null) {
                                sisterProgram = EpgDataManager.getInstance().requestProgram(sisterCh.getName(), time);
                                Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : sisterProgram again = "+sisterProgram);
                                if (sisterProgram == null) {
                                    Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : Fail to exchange to sister program");
                                } else {
                                    Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : Exchange from ["+p+"] to ["+sisterProgram+"]");
                                    p = sisterProgram;
                                }
                            } else {
                                Log.printDebug(App.LOG_HEADER+"GridEpg.showDetails() : Exchange from ["+p+"] to ["+sisterProgram+"]");
                                p = sisterProgram;
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-

        footer.setVisible(false);
        currentGrid.setVisible(false);
        desc.setVisible(false);
        if (p instanceof PpvProgram) {
            details = new PpvProgramDetails();
            details.start((PpvProgram) p);
        } else {
            details = new EpgProgramDetails();
            details.start(p);
        }
        this.add(details, 0);
        setFocusedComponent(details);
        repaint();
    }

    private void showOption(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showOption("+p+")");
        os = new OptionScreen();
        //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
        os.start(getOptionMenu(p, true), null, this, EpgCore.getInstance());
        //os.start(getOptionMenu(p), null, this, EpgCore.getInstance());
    }

    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
    // Grid EPG 보여줄때 SD/HD 그룹핑 채널인 경우 채널은 SD 지만 프로그램 정보는 HD 로 바꿔 놓았음.
    // GridEpg.decideTargetChannel() 의 적용이 그것임. 그러나 그 side effect 로 recording -> B -> SD 로 녹화 채널 바꾸는
    // 경우 문제 발생. 또한 D 액션팝업, Grid program 에서 제대로 recording 되고 있는지 감지 못하는 경우 발생.
    // 따라서 D 버튼 옵션보여줄때 sister recording 을 같이 체크할지 알려줘야 한다.
    public static MenuItem getOptionMenu(Program p, boolean checkSisterRecording) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.getOptionMenu("+p+", "+checkSisterRecording+")");
        boolean isFuture = p.isFuture();
        MenuItem root = new MenuItem("GRID_EPG");
        //->Kenneth[2016.3.7] Note : R7 구현하는 중에 아래 isValid 안 쓰이고 있음 발견. 왜 그럴까?
        boolean isVaild = !(p instanceof FakeProgram) && !(p instanceof NullProgram);

        //->Kenneth[2016.3.7] R7 : Group 상태에서만 sister 상태 체크
        if (EpgCore.isChannelGrouped && checkSisterRecording && RecordingStatus.getInstance().getSisterStatus(p) == RecordingStatus.RECORDING) {
        //if (checkSisterRecording && RecordingStatus.getInstance().getSisterStatus(p) == RecordingStatus.RECORDING) {
        //<-

            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.getOptionMenu() : Use sister recording.");
            root.add(Rs.getRecordMenu(p.getSisterProgram()));
        } else {
            root.add(Rs.getRecordMenu(p));
        }
        //root.add(Rs.getRecordMenu(p));
        root.add(Rs.getReminderMenu(p));
        root.add(Rs.MENU_SORTING);

        //->Kenneth[2017.3.16] R7.3 : D-option 에 search 추가됨
        root.add(Rs.MENU_SEARCH);
        //<-

        Channel ch = p.getEpgChannel();
        //->Kenneth[2017.4.11] R7.3 : VDTRMASTER-6095 : miniEpg 에서 사용됨.
        if (ch != null && ch.getType() != Channel.TYPE_TECH) {
            if (ch.isFavorite()) {
                root.add(Rs.MENU_REMOVE_FAV_CHANNEL);
            } else if (ch.getType() == Channel.TYPE_PPV || ch.isAuthorized()) {
                root.add(Rs.MENU_ADD_FAV_CHANNEL);
            }
            root.add(p.isBlocked() ? Rs.MENU_UNBLOCK_CHANNEL : Rs.MENU_BLOCK_CHANNEL);
        }
        //<-
        root.add(Rs.MENU_GO_TO_PREFERENCE);
        root.add(Rs.MENU_GO_TO_HELP);
        root.add(Rs.getParentalControlMenu());
        root.add(Rs.getMoreOptionsMenu(p.getEpgChannel()));
//        root.add(isFuture ? Rs.MENU_TUNE_CHANNEL : Rs.MENU_VIEW_PROGRAM);
//        root.add(Rs.MENU_CHANGE_FILTER);
        return root;
    }

    private void hideOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.hideOption()");
        if (os != null) {
            os.stop();
            os = null;
        }
    }

    //->Kenneth[2017.2.22] R7.3 Highlight
    private void showHighlightOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showHighlightOption()");
        os = new OptionScreen(KeyCodes.COLOR_C, PreferenceService.BTN_C, "epg.close");
        os.setAutoCloseDelay(DataCenter.getInstance().getLong("WATCH_D_OPTION_HIDE_DELAY", 5000L));
        ((UpMenu)Rs.MENU_HIGHLIGHT).resetCheckedChildren();
        os.start(Rs.MENU_HIGHLIGHT, this);
    }
    //<-

    private void showChangeDateOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showChangeDateOption()");
        MenuItem date = new MenuItem("epg.change_date");
        DataCenter dc = DataCenter.getInstance();
        Formatter fm = Formatter.getCurrent();
        long time = System.currentTimeMillis();

        int i = 0;
        while (time + GridPrograms.PAGE_TIME_GAP <= EpgDataManager.lastTime) {
            String key = CHANGE_DATE_PREFIX + i;
            date.add(new MenuItem(key));
            String value = fm.getLongDayText(time);
            if (i == 0) {
                value = dc.get("epg.today") + " (" + value + ")";
            }
            dc.put(key, value);
            time += Constants.MS_PER_DAY;
            i++;
        }
        os = new OptionScreen(KeyCodes.COLOR_B, PreferenceService.BTN_B, "epg.close");
        os.start(date, this);
    }

    private void showFilterOption() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.showFilterOption()");
        os = new OptionScreen(KeyCodes.COLOR_A, PreferenceService.BTN_A, "epg.close");
        os.start(Rs.MENU_CHANGE_FILTER, null, this, EpgCore.getInstance());
    }

    public void selected(MenuItem item) {
        selected(item, currentGrid.getFocusedChannel(), currentGrid.focusedEvent, this, this);
    }

    public void selected(MenuItem item, Channel ch, Program p, EpgListener l, ReminderListener rl) {
        if (item == Rs.MENU_TUNE_CHANNEL || item == Rs.MENU_VIEW_PROGRAM) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call exitToChannel()");
            exitToChannel(ch);
        } else if (item == Rs.MENU_RECORD || item == Rs.MENU_RECORD_PROGRAM) {
            //->Kenneth[2015.10.15] DDC-114 : EpgCore.record() 에서 판단해서 팝업 띄움.
            /*
            int reason = IncompatibilityChannelPanel.resetReasonCode(channel);
            if (reason == IncompatibilityChannelPanel.REASON_NO_UHD_PACKAGE) {
                if (Log.DEBUG_ON) Log.printDebug("GridEpg.selected() : Call showIncompatibilityErrorMessage()");
                EpgCore.getInstance().showIncompatibilityErrorMessage(reason);
                return ;
            }
            */
            /*
            //->Kenneth[2015.2.3] 4K
            if (EpgCore.getInstance().needToShowIncompatibilityPopup(ch)) {
                if (Log.DEBUG_ON) Log.printDebug("GridEpg.selected() : Call showIncompatibilityErrorMessage()");
                EpgCore.getInstance().showIncompatibilityErrorMessage();
                return ;
            }
            */
            //<-
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.record()");
            EpgCore.getInstance().record(ch, p);
        } else if (item == Rs.MENU_STOP_RECORDING || item == Rs.MENU_CANCEL_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.cancelRecord()");
            EpgCore.getInstance().cancelRecord(p);

            //->Kenneth[2015.3.7] R7 : Ungrouped 에서는 sister 레코딩 신경쓸 필요 없음.
            if (!EpgCore.isChannelGrouped) {
                return ;
            }
            //<-

            //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
            int recStatus = RecordingStatus.getInstance().getStatus(p);
            if (Log.DEBUG_ON) Log.printDebug("GridEpg.selected() : recStatus = "+recStatus);
            // cancelRecording 이 두번 불리우면 팝업 두번 뜸. 따라서 recording 중인 놈으로 이미 호출했으면
            // sister 인 놈은 호출하지 않음.
            if (recStatus != RecordingStatus.RECORDING) {
                if (RecordingStatus.getInstance().getSisterStatus(p) == RecordingStatus.RECORDING) {
                    if (Log.DEBUG_ON) Log.printDebug("GridEpg.selected() Call EpgCore.cancelRecord(sisterProgram)");
                    EpgCore.getInstance().cancelRecord(p.getSisterProgram());
                }
            }
            //<-
        } else if (item == Rs.MENU_ADD_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.addFavoriteChannel()");
            //->Kenneth[2016.3.9] R7 : VBM 로그 
            EpgCore.getInstance().addFavoriteChannel(ch, l, false);
            //EpgCore.getInstance().addFavoriteChannel(ch, l);
            //<-
        } else if (item == Rs.MENU_REMOVE_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.removeFavoriteChannel()");
            EpgCore.getInstance().removeFavoriteChannel(ch, l);
        } else if (item == Rs.MENU_BLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.addBlockedChannel()");
            EpgCore.getInstance().addBlockedChannel(ch, l);
        } else if (item == Rs.MENU_UNBLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.removeBlockedChannel()");
            EpgCore.getInstance().removeBlockedChannel(ch, l);
        } else if (item.getKey().startsWith("filter.")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.applyFilter()");
            ChannelList list = EpgCore.getInstance().applyFilter(item.getKey(), l);
//            if (list.size() > 0) {
//                setChannelList(list, comparator);
//            }
        } else if (item == Rs.MENU_SORT_BY_NUMBER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call sort(BY_NUMBER)");
            sort(ChannelList.ASCENDING_BY_NUMBER);
        } else if (item == Rs.MENU_SORT_BY_NAME) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call sort(BY_NAME)");
            sort(ChannelList.ASCENDING_BY_NAME);
        } else if (item == Rs.MENU_GO_TO_PREFERENCE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call goToPreference()");
            EpgCore.getInstance().goToPreference(App.NAME);
        } else if (item == Rs.MENU_SET_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call ReminderManager.remind()");
            ReminderManager.getInstance().remind(p, rl);
        } else if (item == Rs.MENU_REMOVE_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call ReminderManager.remove()");
            ReminderManager.getInstance().remove(p);
        } else if (item.getKey().startsWith(CHANGE_DATE_PREFIX)) {
            try {
                int day = Integer.parseInt(item.getKey().substring(CHANGE_DATE_PREFIX.length()));
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call currentGrid.goToDay()");
                currentGrid.goToDay(day);
            } catch (Exception ex) {
            }
        } else if (item.getKey().startsWith("menu.pc_")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.showParentalControlSettingPopup()");
            EpgCore.getInstance().showParentalControlSettingPopup(item, App.NAME);
        //->Kenneth[2017.2.22]
        } else if (item.getKey().startsWith("epg.highlight")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.highlightChanged()");
            Highlight.getInstance().setHighlight(((UpMenuItem)item).getValue());
            highlightChanged();
        //<-
        } else if (item == Rs.MENU_GO_TO_HELP) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.startUnboundApp()");
            EpgCore.getInstance().startUnboundApp("Help", new String[] { MonitorService.REQUEST_APPLICATION_HOT_KEY, App.NAME, null} );
        //->Kenneth[2017.3.22] R7.3 : D-option 에 search 추가
        // VBM 로깅을 위해서 Grid/Mini/Live 별로 각각 selected 에 처리함
        } else if (item == Rs.MENU_SEARCH) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : SEARCH");
            EpgCore.getInstance().launchSearch(SearchService.PLATFORM_EPG, true, "D_GRID");
        //<-
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.selected() : Call EpgCore.selected()");
            EpgCore.getInstance().selected(item);
        }
    }

    public void canceled() {
    }

    public synchronized void hideDetails() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.hideDetails()");
        if (details == null) {
            return;
        }
        details.stop();
        this.remove(details);
        repaint();
        footer.setVisible(true);
        currentGrid.setVisible(true);
        desc.setVisible(true);
        setFocusedComponent(currentGrid);
        currentGrid.checkAvailable(currentGrid.pageTime);
        details = null;
        param = null;
    }

    private void setFocusedComponent(EpgPanel c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.setFocusedComponent()");
        focusedComponent = c;
    }

    public synchronized void updateParentalControl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.updateParentalControl()");
        if (desc != null) {
            desc.updateParentalControl();
        }
        if (details != null) {
            details.updateParentalControl();
        }
        if (ui.isActive()) {
            repaint();
        }
    }

    public synchronized void updateRecordingStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.updateRecordingStatus()");
        if (details != null) {
            details.updateStatus();
        }
        if (desc != null) {
            desc.updateRecordingStatus();
        }
        if (ui.isActive()) {
            repaint();
        }
    }

    public synchronized void updateRemotePpv() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.updateRemotePpv()");
        if (details != null) {
            details.updateStatus();
        }
        if (ui.isActive()) {
            repaint();
        }
    }

    public void setSearchService(SearchService ss) {
        try {
            ss.addSearchActionEventListener(SearchService.PLATFORM_EPG, searchActionAdapter);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public class SearchActionAdapter implements SearchActionEventListener {

        // SearchActionEventListener
        public void actionRequested(String platform, String[] action) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.SearchActionAdapter.actionRequested("+platform+")");
            if (action != null && action.length == 1 && "CLOSED".equals(action[0])) {
                return;
            }
            if (action != null) {
                for (int i = 0 ; i < action.length ; i++) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.SearchActionAdapter.actionRequested() : action["+i+"] = "+action[i]);
                }
            }
            hideOption();
            hideDetails();
            GridEpg.this.param = action;
            changeType(type, action);
        }
    }

    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        // TODO - repaint 안되어서 아이콘이 바로 안나오는 경우 있음.
        //        listener가 먼저 불리고 up가 바뀌어서 그런 듯.
        switch (id) {
            case ADDED_TO_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : ADDED_TO_FAVOURITES");
                favIcon.showIcon();
                //->Kenneth[2015.4.14] R5 
                footer.changeText(iconLittleBoy, "epg.remove_fav");
                repaint();
                break;
            case REMOVED_FROM_FAVOURITES:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : REMOVED_FROM_FAVOURITES");
                favIcon.hideIcon();
                //->Kenneth[2015.4.14] R5
                footer.changeText(iconLittleBoy, "epg.add_fav");
                repaint();
                break;
            case ADDED_TO_RESTRICTED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : ADDED_TO_RESTRICTED");
                lockIcon.showIcon();
                break;
            case REMOVED_FROM_RESTRICTED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : REMOVED_FROM_RESTRICTED");
                lockIcon.hideIcon();
                break;
            case FILTER_WILL_BE_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : FILTER_WILL_BE_CHANGED");
                //->Kenneth[2017.2.27] R7.3 : filter animation 위치 바뀜, 아래 FILTER_CHANGED 에서 ready 하도록 함
                //int y = (type == TYPE_DEFAULT) ? 242 - 17 : 98 - 17;
                //filterEffect.readyToStart(50, y, GridProgramsRenderer.CHANNEL_CELL_WIDTH, 21);
                //<-
                break;
            case FILTER_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyEpgEvent() : FILTER_CHANGED");
                if (currentGrid == focusedComponent) {
                    //->Kenneth[2017.2.27] R7.3 : filter animation 위치 바뀜
                    int y = (type == TYPE_DEFAULT) ? 242 - 17 : 98 - 17;
                    if (renderer instanceof GridEpgRenderer) {
                        ((GridEpgRenderer)renderer).calculateAnimationPosition(ChannelController.get(0).getCurrentFilterKey());
                    }
                    filterEffect.readyToStart(GridEpgRenderer.filterAnimationStartX, y, GridEpgRenderer.filterAnimationWidth, 21);
                    //<-
                    filterEffect.start();
                }
                break;
        }
    }

    //->Kenneth[2015.4.4] R5
    public void highlightChanged() {
        int y = (type == TYPE_DEFAULT) ? 242 - 17 : 98 - 17;
        //->Kenneth[2017.2.27] R7.3 : highlight animation 위치 바뀜
        //filterEffect.readyToStart(338, y, GridProgramsRenderer.CHANNEL_CELL_WIDTH, 21);
        if (renderer instanceof GridEpgRenderer) {
            ((GridEpgRenderer)renderer).calculateAnimationPosition(ChannelController.get(0).getCurrentFilterKey());
        }
        filterEffect.readyToStart(GridEpgRenderer.highlightAnimationStartX, y, GridEpgRenderer.highlightAnimationWidth, 21);
        //<-
        filterEffect.start();
        //->Kenneth[2015.5.29] R5 : VBM 추가
        EpgVbmController.getInstance().writeHighlightSelected(sessionId);
        EpgVbmController.getInstance().writeHighlightSelectedName(Highlight.getInstance().getHighlightName(Highlight.getInstance().getCurrentHighlight()));
        //->Kenneth[2017.2.22] R7.3 에서 없어짐
        //resetFooterHighlight();
        //<-
        EpgVbmController.getInstance().writeHighlightLoaded(sessionId);
    }

    public void notifyReminderResult(Program p, boolean added) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.notifyReminderResult()");
        if (added) {
            repaint();
        }
    }

    public void exitToChannel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.exitToChannel()");
        try {
            CommunicationManager.getMonitorService().exitToChannel();
        } catch (Exception ex) {
            Log.print(ex);
            stop();
        }
    }

    public void exitToChannel(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.exitToChannel("+ch+")");
        // view
        try {
//            if (App.R3_TARGET || true) {
                CommunicationManager.getMonitorService().exitToChannel(ch.getId());
//            } else {
//                CommunicationManager.getMonitorService().exitToChannel();
//            }
        } catch (Exception ex) {
            Log.print(ex);
            ChannelController.get(0).changeChannel(ch);
            stop();
        }
//        ChannelController.get(0).changeChannel(ch);
    }

    public void epgDataUpdated(byte type, long from, long to) {
        GridPrograms gp = currentGrid;
        if (gp != null) {
            gp.epgDataUpdated(type, from, to);
        } else {
            return;
        }
        if (gp.pageTime + GridPrograms.PAGE_TIME_GAP <= from) {
            return;
        }
        Log.printInfo(App.LOG_HEADER+"GridEpg.epgDataUpdated : updating current EPG data.");
        synchronized (this) {
// Note - 이걸 풀면 loading이 여러번 생김.
//        이걸 막으면 data update 시에 grid program 으로 focused event를 전달해야할 필요가 있음.
//        GridPrograms 내부에서 처리
//            setChannelList(channelList, comparator);
            if (details != null) {
                Program p = details.program;
                if (p != null && from < p.endTime && p.startTime < to) {
                    if (p.equals(gp.focusedEvent)) {
                        Log.printInfo("GridEpg.epgDataUpdated : updating program details.");
                        details.setData(gp.focusedEvent);
                        details.repaint();
                    } else {
                        Log.printInfo("GridEpg.epgDataUpdated : program changed, hiding details.");
                        hideDetails();
                    }
                }
            }
        }
    }

    public void caUpdated(int sid, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpg.caUpdated("+sid+", "+auth+") : Call updateRecordingStatus()");
        updateRecordingStatus();
    }

}

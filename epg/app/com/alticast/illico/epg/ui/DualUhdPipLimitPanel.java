package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.util.Date;
import java.util.Stack;
import java.awt.Color;
import org.ocap.ui.event.OCRcEvent;

public class DualUhdPipLimitPanel extends EpgPanel implements DataUpdateListener{
    public EpgConfig epgConfig;

    public Channel channel;
    private ScrollTexts pipLimitText = new ScrollTexts(false);
    public int screenData = -1;

    DualUhdPipLimitPanelRenderer ren = new DualUhdPipLimitPanelRenderer();

    public DualUhdPipLimitPanel() {
        setRenderer(ren);
        // UHD functional text
        pipLimitText.setForeground(Color.white);
        pipLimitText.setRowHeight(19);
        pipLimitText.setFont(FontResource.DINMED.getFont(15));
        pipLimitText.setBounds(300, 298, 360, 58);
        pipLimitText.setRows(3);
        pipLimitText.setVisible(true);
        pipLimitText.setCenterAlign(true);
        this.add(pipLimitText);
        
        DataCenter.getInstance().addDataUpdateListener(ConfigManager.EPG_CONFIG_INSTANCE, this);
    }

    //DataUpdateListener implementation
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.dateUpdated("+key+")");
        setEpgConfig((EpgConfig)value);
    }

    //DataUpdateListener implementation
    public void dataRemoved(String key)  {
    }

    private void setEpgConfig(EpgConfig config) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.setEpgConfig("+config+")");
        this.epgConfig = config;
        if (epgConfig != null) {
            pipLimitText.setContents(epgConfig.getUhdLimitText());
        }
    }

    public void setChannel(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.setChannel("+ch+")");
        this.channel = ch;
        screenData = ch.getScreenData();
    }

    public void start(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.start("+ch+")");
        setChannel(ch);
        setEpgConfig(ConfigManager.getInstance().epgConfig);
        super.start();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.stop()");
        super.stop();
    }

    public boolean handleKey(int code) {
        if (code == OCRcEvent.VK_ENTER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DualUhdPipLimitPanel.handleKey() : VK_ENTER");
        }
        return false;
    }
}

package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ui.MenuItem;
import java.awt.event.KeyEvent;
import java.awt.*;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import com.alticast.ui.*;


public class MiniEpg extends UIComponent implements TVTimerWentOffListener, EpgChannelEventListener,
                                                DataUpdateListener, LayeredKeyHandler, MenuListener,
                                                EpgListener, ReminderListener {

    private LayeredUI ui;
    public MiniEpgDescription miniDesc;

    private static final long serialVersionUID = 102385711508158L;

    private TVTimerSpec autoCloseTimer;
    private TVTimerSpec expandTimer;

    private boolean autoShowDesc;

    public int size;

    private static final long DEFAULT_CLOSE_TIME = 15000L;
    private static final long DEFAULT_EXPAND_TIME = 1000L;

    private long focusedTime;

    public ProgramList[] programs;
    public Program[] leftPrograms;
    public ChannelList channelList;

    private Effect showingEffect;
    private Effect hidingEffect;
    private RefreshEffect filterEffect;

    OptionScreen option;

    public boolean isFuture;
    public static int blockLevel;
    public String title;

    private EpgFooter footer = new EpgFooter(Footer.ALIGN_RIGHT, true);

    public MiniEpg() {
        DataCenter dataCenter = DataCenter.getInstance();
        dataCenter.addDataUpdateListener(ConfigManager.EPG_CONFIG_INSTANCE, this);
        dataCenter.addDataUpdateListener(PreferenceNames.AUTO_SHOW_DESCIRIPTION, this);

        footer.setBounds(317-240, 288, 240, 25);
        this.add(footer);

        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setDelayTime(DEFAULT_CLOSE_TIME);
        autoCloseTimer.addTVTimerWentOffListener(this);

        expandTimer = new TVTimerSpec();
        expandTimer.setDelayTime(DEFAULT_EXPAND_TIME);
        expandTimer.addTVTimerWentOffListener(this);

        miniDesc = new MiniEpgDescription();
        miniDesc.setVisible(true);
        setRenderer(new MiniEpgRenderer());
        this.add(miniDesc);

        Rectangle r = getBounds();
        LayeredWindow window = new MiniEpgWindow();
        window.setSize(r.width, r.height);
        window.add(this);
        window.setVisible(true);
        setVisible(false);

        ui = WindowProperty.MINI_EPG.createLayeredDialog(window, r, this);
        ui.deactivate();

        int x = getX();
        int y = getY();
        Point from = new Point(-200, 0);
        Point to = new Point(0, 0);
        showingEffect = new MovingEffect(this, 12,
                    from, to, MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        hidingEffect = new MovingEffect(this, 12,
                    to, from, MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
        setLocation(0, 0);

        filterEffect = new RefreshEffect(this, 16, DataCenter.getInstance().getImage("sweep.png"));

        updateConfig(ConfigManager.getInstance().epgConfig);
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.start()");
        footer.reset();
        footer.addButton(PreferenceService.BTN_GUIDE, "epg.full_guide");
        footer.addButton(PreferenceService.BTN_D, "epg.options");
        miniDesc.resetFooter();

        this.focusedTime = System.currentTimeMillis();

        channelList = ChannelController.get(0).getChannelList().sort(GridEpg.comparator);
        readyEpgData();
        this.focus = channelList.getCurrentIndex();
//        miniDesc.setState(MiniEpgDescription.STATE_NORMAL);

//        top = getProgram((focus - 1 + size) % size);
//        center = getProgram(focus);
//        bottom = getProgram((focus + 1) % size);

        miniDesc.setVisible(false);
        this.setVisible(false);
        ui.activate();
//        FrameworkMain.getInstance().addComponent(this);
        prepare();
        changeBlockLevel();
        showingEffect.start();
        ChannelController.get(0).addChannelEventListener(this);
        EpgVbmController.getInstance().writeUsedEpgLayout(EpgVbmController.MINI_EPG);
    }

    private void readyEpgData() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.readyEpgData()");
        this.size = channelList.size();
        this.programs = new ProgramList[size];
        this.leftPrograms = new Program[size];
        EpgDataManager edm = EpgDataManager.getInstance();
        for (int i = 0; i < size; i++) {
            programs[i] = edm.getPrograms(channelList.getChannelAt(i));
            leftPrograms[i] = programs[i].search(focusedTime);
        }
    }

    public boolean isActive() {
        return ui.isActive();
    }

    public void stopWithoutAnimation() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.stopWithoutAnimation()");
        ui.deactivate();
        stop();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.stop()");
        synchronized (footer) {
            if (option != null) {
                option.stop();
                option = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("MiniEpg.stop " + this.getParent());
        }
        stopTimer();
        ChannelController.get(0).removeChannelEventListener(this);
        if (!ui.isActive()) {
            return;
        }
        miniDesc.setVisible(false);
        if (isVisible()) {
            hidingEffect.startLater();
        }
    }

    public void showOption(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.showOption("+p+")");
        stopTimer();
        synchronized (footer) {
            option = new OptionScreen();
            //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
            option.start(GridEpg.getOptionMenu(p, false), null, this, EpgCore.getInstance());
            //option.start(GridEpg.getOptionMenu(p), null, this, EpgCore.getInstance());
        }
    }

    //->Kenneth[2016.4.15] VDTRMASTER-5800 : miniEpg 가 뜬 상태에서는 LITTLE_BOY 키가 miniEpg 의 채널에 적용되어야 함.
    public Channel getFocusedChannel() {
        if (channelList == null) return null;
        Channel ch = null;
        try {
            ch = channelList.getChannelAt(focus);
        } catch (Exception e) {
            Log.print(e);
        }
        return ch;
    }
    //<-

    public void selected(MenuItem item) {
        Program p = leftPrograms[focus];
        if (item == Rs.MENU_TUNE_CHANNEL || item == Rs.MENU_VIEW_PROGRAM) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call ChannelController.changeChannel()");
            ChannelController.get(0).changeChannel(leftPrograms[focus].getCallLetter());
            stopWithoutAnimation();
        } else if (item == Rs.MENU_RECORD || item == Rs.MENU_RECORD_PROGRAM) {
            //->Kenneth[2015.10.15] DDC-114 : EpgCore.record() 에서 판단해서 팝업 띄움.
            /*
            //->Kenneth[2015.2.3] 4K
            if (EpgCore.getInstance().needToShowIncompatibilityPopup(channelList.getChannelAt(focus))) {
                if (Log.DEBUG_ON) Log.printDebug("MiniEpg.selected() : Call showIncompatibilityErrorMessage()");
                EpgCore.getInstance().showIncompatibilityErrorMessage();
                return ;
            }
            */
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.record()");
            EpgCore.getInstance().record(channelList.getChannelAt(focus), p);
        } else if (item == Rs.MENU_STOP_RECORDING || item == Rs.MENU_CANCEL_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.cancelRecord()");
            EpgCore.getInstance().cancelRecord(p);
        } else if (item == Rs.MENU_ADD_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.addFavoriteChannel()");
            //->Kenneth[2016.3.9] R7 : VBM 로그 
            EpgCore.getInstance().addFavoriteChannel(channelList.getChannelAt(focus), this, false);
            //EpgCore.getInstance().addFavoriteChannel(channelList.getChannelAt(focus), this);
            //<-
        } else if (item == Rs.MENU_REMOVE_FAV_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.removeFavoriteChannel()");
            EpgCore.getInstance().removeFavoriteChannel(channelList.getChannelAt(focus), this);
        } else if (item == Rs.MENU_BLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.addBlockedChannel()");
            EpgCore.getInstance().addBlockedChannel(channelList.getChannelAt(focus), this);
        } else if (item == Rs.MENU_UNBLOCK_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.removeBlockedChannel()");
            EpgCore.getInstance().removeBlockedChannel(channelList.getChannelAt(focus), this);
        } else if (item.getKey().startsWith("filter.")) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.applyFilter()");
            ChannelList list = EpgCore.getInstance().applyFilter(item.getKey(), this);
            if (list.size() > 0) {
                setChannelList(list, channelList.getComparator());
            }
        } else if (item == Rs.MENU_SORT_BY_NUMBER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call setChannelList(BY_NUMBER)");
            setChannelList(channelList, ChannelList.ASCENDING_BY_NUMBER);
        } else if (item == Rs.MENU_SORT_BY_NAME) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call setChannelList(BY_NAME)");
            setChannelList(channelList, ChannelList.ASCENDING_BY_NAME);
        } else if (item == Rs.MENU_SET_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call ReminderManager.remind()");
            ReminderManager.getInstance().remind(p, this);
        } else if (item == Rs.MENU_REMOVE_REMINDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call ReminderManager.remove()");
            ReminderManager.getInstance().remove(p);
        //->Kenneth[2017.3.22] R7.3 : D-option 에 search 추가
        // VBM 로깅을 위해서 Grid/Mini/Live 별로 각각 selected 에 처리함
        } else if (item == Rs.MENU_SEARCH) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : SEARCH");
            EpgCore.getInstance().launchSearch(SearchService.PLATFORM_EPG, true, "D_MINI");
        //<-
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.selected() : Call EpgCore.selected()");
            EpgCore.getInstance().selected(item);
        }
        option = null;
        startTimer();
    }

    public void canceled() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.canceled()");
        option = null;
        startTimer();
    }

    /** filter를 바꾸거나 sorting을 적용 했을 때 */
    private void setChannelList(ChannelList list, ChannelComparator c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.setChannelList()");
        ChannelList newList = list.sort(c);
        int newFocus = newList.search(channelList.getChannelAt(focus));
        focus = Math.max(0, newFocus);
        channelList = newList;
        // mini epg에서 sorting을 변경하면 EPG에도 적용한다.
        GridEpg.comparator = c;
        readyEpgData();
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        return handleKey(e.getCode());
    }

    public boolean handleKey(int code) {
        if (option == null) {
            startTimer();
        }
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_ENTER");
                stop();
                ChannelController.get(0).changeChannel(leftPrograms[focus].getCallLetter());
                return true;
            case OCRcEvent.VK_INFO:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_INFO");
                if (autoShowDesc) {
                    stop();
                } else {
                    if (miniDesc.isVisible()) {
                        miniDesc.setVisible(false);
                    } else {
                        showDesc();
                    }
                }
                return true;
            case OCRcEvent.VK_EXIT:
            case OCRcEvent.VK_LIVE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_EXIT or VK_LIVE");
                stop();
                return true;
            case OCRcEvent.VK_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_UP");
                keyUp();
                focusMoved();
                return true;
            case OCRcEvent.VK_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_DOWN");
                keyDown();
                focusMoved();
                return true;
            case OCRcEvent.VK_LEFT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_LEFT");
                keyLeft();
                focusMoved();
                return true;
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_RIGHT");
                keyRight();
                focusMoved();
                return true;
            case OCRcEvent.VK_PAGE_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_PAGE_UP");
                if (miniDesc.isVisible()) {
                    miniDesc.footer.clickAnimationByKeyCode(code);
                    miniDesc.showPreviousPage();
                }
                return true;
            case OCRcEvent.VK_PAGE_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_PAGE_DOWN");
                if (miniDesc.isVisible()) {
                    miniDesc.footer.clickAnimationByKeyCode(code);
                    miniDesc.showNextPage();
                }
                return true;
            case KeyCodes.COLOR_A:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : COLOR_A");
                // block to launch mini menu
                return true;
            case KeyCodes.COLOR_D:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : COLOR_D");
                footer.clickAnimationByKeyCode(code);
                showOption(leftPrograms[focus]);
                return true;
            case KeyCodes.RECORD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : RECORD");
                if (EpgCore.getInstance().supportDvr()) {
                    //->Kenneth[2015.10.15] DDC-114 : EpgCore.record() 에서 판단해서 팝업 띄움.
                    /*
                    //->Kenneth[2015.2.2] 4K
                    Channel curCh = channelList.getChannelAt(focus);
                    if (EpgCore.getInstance().needToShowIncompatibilityPopup(curCh)) {
                        if (Log.DEBUG_ON) Log.printDebug("MiniEpg.handleKey() : Call showIncompatibilityErrorMessage()");
                        EpgCore.getInstance().showIncompatibilityErrorMessage();
                        return true;
                    }
                    */
                    Channel curCh = channelList.getChannelAt(focus);
                    EpgCore.getInstance().record(curCh, leftPrograms[focus]);
                    return true;
                } else {
                    return false;
                }
            case KeyCodes.STOP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : STOP");
                if (EpgCore.getInstance().supportDvr()) {
                    EpgCore.getInstance().cancelRecord(leftPrograms[focus]);
                    return true;
                } else {
                    return false;
                }
            case OCRcEvent.VK_GUIDE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : VK_GUIDE");
                footer.clickAnimationByKeyCode(code);
                return false;
            //->Kenneth[2015.8.10] VDTRMASTER-5572
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : SEARCH - close Mini EPG");
                stopWithoutAnimation();
                return false;
            //<-
            //->Kenneth[2015.8.19] DDC-113
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : STAR");
                if (option != null && option.isStarted()) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : STAR : return true");
                    return true;
                }
            //<-
            //->Kenneth[2016.9.22] R7.2 VDTRMASTER-5868
            case KeyCodes.ASPECT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.handleKey() : ASPECT. Return true");
                return true;
            //<-
            default:
                return false;
        }
    }

    private void keyUp() {
        focus = (focus - 1 + size) % size;
        repaint();
    }

    private void keyDown() {
        focus = (focus + 1) % size;
        repaint();
    }

    public void keyRight() {
        Program current = leftPrograms[focus];
        if (current.next != null) {
            current = current.next;
            focusedTime = current.getStartTime();
            for (int i = 0; i < programs.length; i++) {
                Program p = programs[i].search(focusedTime, leftPrograms[i]);
                if (p != null) {
                    leftPrograms[i] = p;
                } else {
                    leftPrograms[i] = programs[i].end;
                }
            }
            repaint();
        }
    }

    public void keyLeft() {
        Program current = leftPrograms[focus];
        if (current.prev != null) {
            current = current.prev;
            focusedTime = current.getStartTime();
            for (int i = 0; i < programs.length; i++) {
                Program p = programs[i].search(focusedTime, leftPrograms[i]);
                if (p != null) {
                    leftPrograms[i] = p;
                } else {
                    leftPrograms[i] = programs[i].start;
                }
            }
            repaint();
        }
    }

    private void focusMoved() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.focusMoved()");
        synchronized (miniDesc) {
//            miniDesc.setState(MiniEpgDescription.STATE_NORMAL);
//            miniDesc.repaint();
            miniDesc.setVisible(false);
        }
        changeBlockLevel();
        startTimer();
    }

    private void changeBlockLevel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.changeBlockLevel()");
        Program p = leftPrograms[focus];
        blockLevel = ParentalControl.getLevel(p);
        title = ParentalControl.getTitle(p);
    }

    private void startTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.startTimer()");
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }

        timer.deschedule(expandTimer);
        synchronized (miniDesc) {
//            if (miniDesc.getState() == MiniEpgDescription.STATE_NORMAL) {
            if (!miniDesc.isVisible() && autoShowDesc) {
                try {
                    expandTimer = timer.scheduleTimerSpec(expandTimer);
                } catch (TVTimerScheduleFailedException e) {
                    Log.print(e);
                }
            }
        }
    }

    private void stopTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.stopTimer()");
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        timer.deschedule(expandTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        TVTimerSpec spec = event.getTimerSpec();
        if (spec.equals(autoCloseTimer)) {
            synchronized (footer) {
                if (isVisible() && option == null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.timerWentOff() : Call stop()");
                    stop();
                }
            }
        } else if (spec.equals(expandTimer)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.timerWentOff() : Call showDesc()");
            showDesc();
        }
    }

    private void showDesc() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.showDesc()");
        synchronized (miniDesc) {
            String desc = leftPrograms[focus].getFullDescription();
            if (desc == null || desc.length() == 0) {
                return;
            }
            if (blockLevel == ParentalControl.ADULT_CONTENT) {
                miniDesc.setDescription(TextUtil.replace(DataCenter.getInstance().getString("epg.modify_pc_desc2"), "|", "\n"));
            } else {
                miniDesc.setDescription(desc);
            }
            miniDesc.hasNext = leftPrograms[focus].next != null;
            miniDesc.setVisible(true);
            miniDesc.repaint();
        }
    }

    public void animationStarted(Effect effect) {
    }

    public void animationEnded(Effect effect) {
        if (effect == showingEffect) {
            this.setVisible(true);
            startTimer();
        } else if (effect == hidingEffect) {
//            FrameworkMain.getInstance().removeComponent(this);
            synchronized (this) {
                ui.deactivate();
                this.setVisible(false);
                programs = null;
                leftPrograms = null;
                channelList = null;
            }
        }
    }

    public void selectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"MiniEpg.selectionRequested");
        }
        channelChanged(ch);
    }

    public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"MiniEpg.selectionBlocked");
        }
        channelChanged(ch);
    }

    public void serviceStopped(Channel ch) {
    }

    public void showChannelInfo(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.showChannelInfo()");
        channelChanged(ch);
    }

    public void programUpdated(Channel ch, Program p) {
    }

    private void channelChanged(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.channelChanged("+ch+")");
        synchronized (miniDesc) {
            miniDesc.setVisible(false);
        }
        int index = channelList.search(ch);
        if (index < 0) {
            // 전체 채널로 filter
            channelList = ChannelDatabase.current.getAllChannelList().sort(GridEpg.comparator);
            focus = channelList.search(ch);
            readyEpgData();
        } else {
            focus = index;
        }
        focusMoved();
        repaint();
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (ConfigManager.EPG_CONFIG_INSTANCE.equals(key)) {
            updateConfig((EpgConfig) value);
        } else if (PreferenceNames.AUTO_SHOW_DESCIRIPTION.equals(key)) {
            autoShowDesc = Definitions.OPTION_VALUE_YES.equals(value);
        }
    }

    private void updateConfig(EpgConfig config) {
        if (config == null) {
            return;
        }
        autoCloseTimer.setTime((long) (config.miniEpgDisplayTime * Constants.MS_PER_SECOND));
        expandTimer.setTime((long) (config.miniEpgInfoDisplayTime * Constants.MS_PER_SECOND));
    }

    public void dataRemoved(String key)  {
    }

    public void updateRecordingStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.updateRecordingStatus()");
        if (ui.isActive()) {
            repaint();
        }
    }

    public synchronized void updateParentalControl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.updateParentalControl()");
        if (ui.isActive()) {
            focusMoved();
            repaint();
        }
    }


    /** EpgListener */
    public void notifyEpgEvent(int id, Object data) {
        switch (id) {
            case FILTER_WILL_BE_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.notifyEpgEvent() : FILTER_WILL_BE_CHANGED");
                filterEffect.readyToStart(55-3, 50-17, 300, 21);
                break;
            case FILTER_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.notifyEpgEvent() : FILTER_CHANGED");
                filterEffect.start();
                break;
        }
    }

    public void notifyReminderResult(Program p, boolean added) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.notifyReminderResult()");
        if (added) {
            repaint();
        }
    }

    class MiniEpgWindow extends LayeredWindow {
        public void notifyShadowed() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniEpg.MiniEpgWindow.notifyShadowed()");
            LayeredUIInfo[] infos = LayeredUIManager.getInstance().getAllLayeredUIInfos();
            if (infos == null) {
                return;
            }
            int priority = WindowProperty.MINI_EPG.getPriority();
            String name = WindowProperty.MINI_EPG.getName();
            for (int i = 0; i < infos.length; i++) {
                LayeredUIInfo info = infos[i];
                if (info == null) {
                    continue;
                }
                if (info.getPriority() == priority && !name.equals(info.getName()) && info.isActive()) {
                    if (MiniEpg.this.isVisible()) {
                        stopWithoutAnimation();
                    }
                }
            }
        }
    }

}

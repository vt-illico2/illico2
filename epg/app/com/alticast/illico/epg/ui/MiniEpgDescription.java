package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.ixc.upp.*;
import org.havi.ui.event.HRcEvent;
import java.awt.Color;
import java.awt.Image;


public class MiniEpgDescription extends UIComponent {

    private static final long serialVersionUID = 2159871293893L;

    MiniEpgDescriptionRenderer descRenderer = new MiniEpgDescriptionRenderer();
    ScrollTexts scroll = new ScrollTexts();

    public boolean hasNext;

    EpgFooter footer = new EpgFooter(Footer.ALIGN_RIGHT, false);

    public MiniEpgDescription() {
        setRenderer(descRenderer);

        footer.setBounds(331-240, 103, 240, 25);
        this.add(footer);

        scroll.setBounds(descRenderer.getPreferredBounds(this));
        scroll.setLocation(0, 0);
        scroll.setInsets(10, 15, 64, 24);
        scroll.setForeground(new Color(39, 39, 39));
        scroll.setFont(FontResource.DINMED.getFont(18));
        scroll.setRows(4);
        scroll.setRowHeight(19);
        scroll.setVisible(true);
        scroll.setBottomMaskImage(DataCenter.getInstance().getImage("02_mini_desbg.png"));
        scroll.setBottomMaskImagePosition(0, 64);
        scroll.setTopMaskImage(DataCenter.getInstance().getImage("02_mini_desbg_t.png"));
        scroll.setTopMaskImagePosition(0, 1);
        this.add(scroll);
    }

    public void resetFooter() {
        footer.reset();
        Image scrollIcon = footer.addButton(PreferenceService.BTN_PAGE, "epg.scroll_description");
        footer.linkWithScrollTexts(scrollIcon, scroll);
    }

    public void setDescription(String text) {
        scroll.setContents(text);
    }

    public void showPreviousPage() {
        scroll.showPreviousPage();
    }

    public void showNextPage() {
        scroll.showNextPage();
    }


}

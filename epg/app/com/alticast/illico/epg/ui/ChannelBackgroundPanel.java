package com.alticast.illico.epg.ui;

import com.videotron.tvi.illico.framework.UIComponent;

public abstract class ChannelBackgroundPanel extends UIComponent {

    public ChannelBackgroundPanel() {
    }

    public boolean isRunning() {
        return true;
    }

    public void pause() {
    }
}

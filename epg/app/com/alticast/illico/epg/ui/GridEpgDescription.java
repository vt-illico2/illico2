package com.alticast.illico.epg.ui;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.ScrollTexts;
import java.awt.Color;
import java.awt.event.KeyEvent;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import org.havi.ui.event.HRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public class GridEpgDescription extends UIComponent {

    private static final long serialVersionUID = 3251098125398L;

    public Program program;
    public int recordingStatus;
    public String channelDesc;
    public String title;

    ScrollTexts scroll = new ScrollTexts(false);

    public int blockLevel;
	//->Kenneth : 4K by June
    public int definition;
    public boolean isCaptioned;
    public int screenData;

    public GridEpgDescription() {
        setRenderer(new GridEpgDescriptionRenderer());
        //->Kenneth[2015.4.11] : R5 : screenData 없어지면서 y 좌표 조정
        scroll.setBounds(24, 73-11, 540, 58);
        scroll.setForeground(new Color(183, 183, 183));
        scroll.setFont(FontResource.DINMED.getFont(18));
        scroll.setRows(3);
        scroll.setRowHeight(19);
        scroll.setVisible(true);
        scroll.setBottomMaskImage(DataCenter.getInstance().getImage("12_des_sh01.png"));
        scroll.setBottomMaskImagePosition(-54, 28);
        scroll.setTopMaskImage(DataCenter.getInstance().getImage("12_des_sh01_top.png"));
        scroll.setTopMaskImagePosition(-54, 4);
        this.add(scroll);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.start()");
        super.start();
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.stop()");
        program = null;
    }

    public void setProgram(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.setProgram("+p+")");
        if (p == null) return;
        int oldLevel = this.blockLevel;
        blockLevel = ParentalControl.getLevel(p);
        if (program != p || oldLevel != blockLevel) {
            program = p;
            if (p != null) {
                Channel channel = p.getEpgChannel();
				//->Kenneth : 4K by June
                definition = Util.getDefinition(channel, p);
                isCaptioned = p.isCaptioned();
                channelDesc = Util.getChannelDescription(channel);
                screenData = channel.getScreenData();
            } else {
				//->Kenneth : 4K by June
                definition = Constants.DEFINITION_SD;
                isCaptioned = false;
            }
            //->Kenneth[2015.2.3] 4K 에 대한 판단은 channel 의 정보만을 이용한다. (요구사항임)
            // 위의 코드는 혹시 몰라서 그대로 두고 4K 인 것만 다시 아래에서 처리
            Channel channel = p.getEpgChannel();
            if (channel != null && channel.getDefinition() == Constants.DEFINITION_4K) {
                definition = Constants.DEFINITION_4K;
            }

            if (blockLevel == ParentalControl.ADULT_CONTENT) {
                scroll.setContents(TextUtil.replace(DataCenter.getInstance().getString("epg.modify_pc_desc2"), "|", "\n"));
                title = DataCenter.getInstance().getString("epg.blocked_title");
            } else {
                if (p != null) {
                    //->Kenneth[2015.6.11] R5
                    String header = Highlight.getInstance().getMyCategoryName(p); 
                    if (header == null) {
                        scroll.setContents(p.getFullDescription());
                    } else {
                        scroll.setContents(header + " - " +p.getFullDescription());
                        scroll.setHeader(header);
                    }
                    title = p.getTitle();
                } else {
                    scroll.setContents("");
                    title = "";
                }
            }
        }
        updateRecordingStatus();
    }

    public void showPreviousPage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.showPreviousPage()");
        scroll.showPreviousPage();
    }

    public void showNextPage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.showNextPage()");
        scroll.showNextPage();
    }

    public void updateRecordingStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.updateRecordingStatus()");
        recordingStatus = RecordingStatus.getInstance().getStatus(program);
        //->Kenneth[2016.3.7] R7 : Group 상태에서만 sister 상태 체크
        if (EpgCore.isChannelGrouped) {
            //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
            int sisterRecordingStatus = RecordingStatus.getInstance().getSisterStatus(program);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.updateRecordingStatus() : sisterRecordingStatus = "+sisterRecordingStatus);
            if (sisterRecordingStatus == RecordingStatus.RECORDING) {
                recordingStatus = sisterRecordingStatus;
            }
            //<-
        }
        //<-
        repaint();
    }

    public synchronized void updateParentalControl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GridEpgDescription.updateParentalControl()");
        if (program != null) {
            setProgram(program);
        }
    }
}

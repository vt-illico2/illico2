package com.alticast.illico.epg.data;

import javax.tv.locator.Locator;
import javax.tv.service.*;
import javax.tv.service.navigation.*;
import org.ocap.net.*;
import javax.tv.service.selection.ServiceContext;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;

public class RealChannel extends Channel {

    protected ServiceDetails details;

    public RealChannel(Service service) {
        super(service);
        this.name = service.getName();
		//->Kenneth : 4K by June
        this.definition = 0;

        if (service instanceof ServiceNumber) {
            this.number = ((ServiceNumber) service).getServiceNumber();
        } else {
            details = getServiceDetails(service);
            if (details instanceof ServiceNumber) {
                this.number = ((ServiceNumber) details).getServiceNumber();
            } else {
                this.number = 0;
            }
        }

        try {
            LocatorFilter locatorFilter = new LocatorFilter(new Locator[] { service.getLocator() });
            if (!locatorFilter.accept(service)) {
                this.type = TYPE_HIDDEN;
                return;
            }
        } catch (Exception ex) {
        }

        String upperName = name.toUpperCase();
        if (name == null || name.length() == 0 || number <= 0) {
            this.type = TYPE_UNKNOWN;
        } else if (upperName.startsWith("TEST")) {
            this.type = TYPE_TECH;
        } else if (upperName.startsWith("GAL") || upperName.startsWith("MU") && upperName.endsWith("A")) {
            this.type = TYPE_GALAXIE;
        } else if (upperName.length() > 3 && (upperName.startsWith("IND") || upperName.startsWith("VCC"))) {
            this.type = TYPE_PPV;
        } else {
            this.type = TYPE_NORMAL;
        }
    }

    protected void setType(short newType) {
        if (type == TYPE_HIDDEN) {
            Log.printWarning("RealChannel: HIDDEN channel !");
            return;
        }
        if (newType == TYPE_SDV) {
            Log.printWarning("RealChannel: skipped to change type to SDV");
            return;
        }
        this.type = newType;
    }

    public int getId() {
        return getSourceId();
    }

    private ServiceDetails getServiceDetails(Service s) {
        if (s instanceof ServiceDetails) {
            return (ServiceDetails) s;
        }
        ServiceDetailsRequestor requestor = new ServiceDetailsRequestor();

        synchronized (requestor) {
            try {
                s.retrieveDetails(requestor);
                requestor.wait(1000L);
            } catch (InterruptedException ex) {
            }
        }
        return requestor.serviceDetails;

    }

    public boolean select(ChannelController cc, boolean auth) {
        super.select(cc, auth);
        // 미가입이라도 일반 채널은 그냥 보내는게 좋겠다.
//        if (auth) {
        startSdvSession(cc, true);
        return true;
//        }
    }

    class ServiceDetailsRequestor implements SIRequestor {

        private ServiceDetails serviceDetails;

        public void notifyFailure(SIRequestFailureType reason) {
            synchronized (ServiceDetailsRequestor.this) {
                serviceDetails = null;
                ServiceDetailsRequestor.this.notifyAll();
            }
        }

        public void notifySuccess(SIRetrievable[] result) {
            synchronized (ServiceDetailsRequestor.this) {
                try {
                    serviceDetails = (ServiceDetails) result[0];
                } catch (Exception ex) {
                }
                ServiceDetailsRequestor.this.notifyAll();
            }
        }
    }
}

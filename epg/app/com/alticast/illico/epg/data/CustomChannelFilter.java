package com.alticast.illico.epg.data;

import java.util.*;
import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ixc.upp.*;

public class CustomChannelFilter implements ChannelFilter, ChannelComparator, DataUpdateListener {

    private ChannelFilter[] filters = null;

    private Hashtable map = new Hashtable();

    private static final Object[] TABLE = {
        Definitions.FAVOURITES_CHANNEL_FILTER_SUBSCRIBED_CHANNELS,  Rs.MENU_FILTER_SUBSCRIBED,
        Definitions.FAVOURITES_CHANNEL_FILTER_MY_FAVOURITES,        Rs.MENU_FILTER_FAVOURITES,
        Definitions.FAVOURITES_CHANNEL_FILTER_ENTERTAINMENT,        Rs.MENU_FILTER_GENRE[0],
        Definitions.FAVOURITES_CHANNEL_FILTER_LIFESTYLE,            Rs.MENU_FILTER_GENRE[1],
        Definitions.FAVOURITES_CHANNEL_FILTER_SPORTS,               Rs.MENU_FILTER_GENRE[2],
        Definitions.FAVOURITES_CHANNEL_FILTER_NEWS,                 Rs.MENU_FILTER_GENRE[3],
        Definitions.FAVOURITES_CHANNEL_FILTER_MOVIES,               Rs.MENU_FILTER_GENRE[4],
        Definitions.FAVOURITES_CHANNEL_FILTER_DOCUMENTARIES,        Rs.MENU_FILTER_GENRE[5],
        Definitions.FAVOURITES_CHANNEL_FILTER_FAMILY,               Rs.MENU_FILTER_GENRE[6],
        Definitions.FAVOURITES_CHANNEL_FILTER_KIDS,                 Rs.MENU_FILTER_GENRE[7],
        Definitions.FAVOURITES_CHANNEL_FILTER_MUSIC,                Rs.MENU_FILTER_GENRE[8],
//        Definitions.FAVOURITES_CHANNEL_FILTER_FRENCH_CHANNEL,       Rs.MENU_FILTER_FRENCH,
//        Definitions.FAVOURITES_CHANNEL_FILTER_ENGLISH_CHANNEL,      Rs.MENU_FILTER_ENGLISH,
        Definitions.FAVOURITES_CHANNEL_FILTER_HD_CHANNELS,          Rs.MENU_FILTER_HD,
        Definitions.FAVOURITES_CHANNEL_FILTER_ALL_CHANNELS,         Rs.MENU_FILTER_TYPE_ALL,
        Definitions.FAVOURITES_CHANNEL_FILTER_PPV_CHANNELS,         Rs.MENU_FILTER_TYPE_PPV,
//        Definitions.FAVOURITES_CHANNEL_FILTER_GALAXIE_RADIO_CHANNELS, Rs.MENU_FILTER_TYPE_GAL_RADIO,
//        Definitions.FAVOURITES_CHANNEL_FILTER_SPORTSMAX_CHANNELS, Rs.MENU_FILTER_TYPE_SPORTSMAX,
//        Definitions.FAVOURITES_CHANNEL_FILTER_TECHNICIAN,           Rs.MENU_FILTER_TECHNICIAN
    };

    public CustomChannelFilter() {
        DataCenter dc = DataCenter.getInstance();
        dc.addDataUpdateListener(PreferenceNames.FAVOURITE_CHANNEL_FILTER, this);
        Object o = dc.get(PreferenceNames.FAVOURITE_CHANNEL_FILTER);
        if (o != null) {
            setFilter((String) o);
        }

        for (int i = 0; i < TABLE.length; i += 2) {
            map.put(TABLE[i], TABLE[i + 1]);
        }

    }

    public void dataUpdated(String key, Object old, Object value) {
        setFilter((String) value);
    }

    public void dataRemoved(String key) {
    }

    private void setFilter(String line) {
        if (Log.DEBUG_ON) {
            Log.printDebug("CustomChannelFilter: setFilter = " + line);
        }
        Vector newFilters = new Vector();
        if (line != null) {
            String[] s = TextUtil.tokenize(line, PreferenceService.PREFERENCE_DELIMETER);
            for (int i = 0; i < s.length; i++) {
                MenuItem mi = (MenuItem) map.get(s[i]);
                if (mi != null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("CustomChannelFilter: menu[" + i + "] = " + mi.getKey());
                    }
                    Object o = ChannelDatabase.filterTable.get(mi.getKey());
                    if (o != null) {
                        newFilters.add(o);
                    }
                }
            }
        }
        ChannelFilter[] array = new ChannelFilter[newFilters.size()];
        newFilters.copyInto(array);
        filters = array;
    }

    public boolean accept(Channel channel) {
        return getFilterIndex(channel) >= 0;
    }

    private int getFilterIndex(Channel channel) {
        ChannelFilter[] array = filters;
        if (array == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("CustomChannelFilter: getFilterIndex = " + channel + " returns -1");
            }
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i].accept(channel)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("CustomChannelFilter: getFilterIndex = " + channel + " returns " + i);
                }
                return i;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("CustomChannelFilter: getFilterIndex = " + channel + " returns -1");
        }
        return -1;
    }

    public int compare(Channel a, Channel b) {
        int ai = getFilterIndex(a);
        int bi = getFilterIndex(b);
        if (ai == bi) {
            if (filters != null && ai >= 0 && ai < filters.length
                    && filters[ai] != null && filters[ai] instanceof ChannelComparator) {
                return ((ChannelComparator) filters[ai]).compare(a, b);
            }
            return a.getNumber() - b.getNumber();
        } else {
            return ai - bi;
        }
    }


}


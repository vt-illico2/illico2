package com.alticast.illico.epg.data;

public interface ReminderListener {

    public void notifyReminderResult(Program p, boolean added);

}

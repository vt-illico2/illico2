package com.alticast.illico.epg.data;

import java.awt.Image;
import java.util.Date;
import java.util.Calendar;
import java.util.Hashtable;
import java.text.SimpleDateFormat;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Formatter;

public abstract class Program implements TvProgram, Comparable, AbstractProgram {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd_HH:mm:ss");
    public String callLetter;
    public long startTime;
    public long endTime;

    public Program prev;
    public Program next;

//    public static int objectCount = 0;

//    public Program() {
//        objectCount++;
//    }

//    protected void finalize() throws Throwable {
//        super.finalize();
//        objectCount--;
//    }

    /**
     * Gets a call letter of Channel.
     * @return call letter.
     */
    public String getCallLetter() {
        return callLetter;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getDuration() {
        return endTime - startTime;
    }

    public boolean contains(long time) {
        return startTime <= time && time < endTime;
    }

    public boolean isOnAir() {
        return contains(System.currentTimeMillis());
    }

    public boolean isFuture() {
        return System.currentTimeMillis() < startTime;
    }

    public boolean isPast() {
        return endTime <= System.currentTimeMillis();
    }

    public String toString() {
        return callLetter + "/" + sdf.format(new Date(startTime)) + " - " + sdf.format(new Date(endTime))
                          + "/(Title)" + getTitle() + "/(EpisodeTitle)" + getEpisodeTitle() 
                          + "/(ProgramId)" +getId() + "/(SeriesId)" + getSeriesId();
    }

    public boolean equals(Object o) {
        if (o instanceof Program) {
            Program p = (Program) o;
            return startTime == p.getStartTime()
                && endTime == p.getEndTime()
                && getId().equals(p.getId())
                && callLetter.equals(p.getCallLetter())
                && getTitle().equals(p.getTitle());
        } else {
            return false;
        }
    }

    //->Kenneth[2015.6.19] : R5 : SD/HD grouping 으로 인해 빈번히 사용되는 API 추가
    public Program getSisterProgram() {
        Channel ch = getEpgChannel();
        if (ch == null || ch.sister == null) {
            return null;
        }
        return EpgDataManager.getInstance().getProgram(ch.sister, startTime);
    }

    public abstract boolean isCaptioned();

    public abstract boolean isLive();

	//->Kenneth : 4K by June
    /**
     * Determines whether this program is HD or not.
     * @return true if this Program is HD; false otherwise.
     */
    public final boolean isHd() {
        return getDefinition() >= DEFINITION_HD;
    }

	//->Kenneth : 4K by June
    public abstract int getDefinition();

    public abstract String getFullDescription();

    public abstract String getActors();

    public abstract String getDirector();

    public abstract String getTitle();

    public abstract String getId();

    public abstract String getSeriesId();

    public abstract String getEpisodeTitle();

    public abstract int getRatingIndex();

    public abstract int getType();

    public abstract int getCategory();

    public abstract int getSubCategory();


    public abstract String getProductionYear();

    public int getConvertedCategory() {
        int c = getCategory();
        if (c == 15) {
            // movie
            int sub = getSubCategory();
            if (sub > 0) {
                return sub;
            } else {
                return c;
            }
        } else {
            return c;
        }
    }

    public String getRating() {
        return Constants.PARENTAL_RATING_NAMES[getRatingIndex()];
    }

    public static int getRatingIndex(String rating) {
        for (int i = 0; i < Constants.PARENTAL_RATING_NAMES.length; i++) {
            if (Constants.PARENTAL_RATING_NAMES[i].equals(rating)) {
                return i;
            }
        }
        return 0;
    }

    public TvChannel getChannel() {
        return getEpgChannel();
    }

    public Channel getEpgChannel() {
        return ChannelDatabaseBuilder.getInstance().getDatabase().getChannelByName(callLetter);
    }

    public int getChannelId() {
        Channel ch = getEpgChannel();
        if (ch != null) {
            return ch.getId();
        } else {
            return -1;
        }
    }

    public boolean isFavorite() {
        Channel ch = getEpgChannel();
        return ch != null && ch.isFavorite();
    }

    public boolean isBlocked() {
        Channel ch = getEpgChannel();
        return ch != null && ch.isBlocked();
    }

    public boolean isAuthorized() {
        Channel ch = getEpgChannel();
        return ch != null && ch.isAuthorized();
    }

    /** Comparable. */
    public int compareTo(Object o) {
        if (o instanceof Program) {
            long nst = ((Program) o).startTime;
            if (nst == this.startTime) {
                return 0;
            }
            return this.startTime > nst ? 1 : -1;
        } else {
            return -1;
        }
    }

}

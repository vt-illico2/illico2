package com.alticast.illico.epg.data;

import javax.tv.service.Service;
import javax.tv.service.SIManager;
import javax.tv.locator.Locator;
import org.ocap.net.OcapLocator;
import org.davic.net.InvalidLocatorException;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import javax.tv.service.selection.ServiceContext;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.sdv.SDVController;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.alticast.illico.epg.sdv.SdvDiagnostics;
import com.alticast.illico.epg.sdv.mc.*;
import com.alticast.illico.epg.CaManager;

public class SdvChannel extends Channel {

    static SIManager siManager = SIManager.createInstance();
    Locator locator;
    public static Locator DUMMY_SDV_LOCATOR;
    {
        try {
            DUMMY_SDV_LOCATOR = new OcapLocator(0);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public SdvChannel(String name, int number, int sid) {
        super(sid);
        this.type = TYPE_SDV;
        this.name = name;
        this.number = number;
        locator = DUMMY_SDV_LOCATOR;
    }

    protected void setType(short newType) {
        if (type != newType) {
            Log.printWarning("SdvChannel: skipped to change type = " + newType);
        }
    }

    public int getId() {
        return getSourceId();
    }

    public boolean setLocator(int freq, int pn, int mod) {
        try {
            setLocator(new OcapLocator(freq, pn, mod));
            return true;
        } catch (InvalidLocatorException ex) {
            Log.print(ex);
            return false;
        }
    }

    public void setLocator(Locator loc) {
        if (loc == null) {
            return;
        }
        this.locator = loc;
        if (Log.DEBUG_ON) {
            Log.printDebug("SdvChannel.setLocator = " + loc.toExternalForm());
        }
        try {
            this.service = siManager.getService(loc);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public Locator getLocator() {
        return locator;
    }

    public synchronized boolean select(ChannelController cc, boolean auth) {
        if (!ChannelController.CHECK_CA_BEFORE_CHANGE_CHANNEL) {
            auth = CaManager.getInstance().check(sourceId);
        }
        if (!auth || !SDVController.hasSdvPackage) {
            if (!ChannelController.STOP_BEFORE_SELECT) {
                cc.getServiceContext().stop();
            }
            return false;
        }

        // 현재 알고 있는 정보로 tune 한다.
        McChannelInfo info = null;
        McData data = (McData) DataCenter.getInstance().get(MiniCarouselReader.MC_KEY);
        if (data != null) {
            info = data.get(sourceId);
        }
        int freq;
        boolean selected;
        if (info != null) {
            freq = (int) info.mCarrierFrequency;
            setLocator(freq, info.mProgramNumber, (int) info.mModulationMode);
            super.select(cc, auth);
            SdvDiagnostics.increase(SdvDiagnostics.CACHE_HITS);
            selected = true;
        } else {
            SdvDiagnostics.increase(SdvDiagnostics.CACHE_MISSES);
            if (!ChannelController.STOP_BEFORE_SELECT) {
                // 앞에서 stop 되지 않았으므로
                cc.getServiceContext().stop();
            }
            if (data == null) {
                MiniCarouselReader.getInstance().forceTune(cc.getNetworkInterface());
            }
            // TODO - cache miss 시나리오 - sc event가 오지 않아 문제가 있다.
            // TODO - 이미 레코딩 중인 것이 있는 경우 바로 select 하자. mc 가 늦게 변해서 info는 없다.
//            setLocator(DUMMY_SDV_LOCATOR);
//            freq = 0;
            selected = false;
        }
        startSdvSession(cc, selected);
        return true;
    }

    public void reselect(ChannelController cc, int freq, int pn, int mod) {
        OcapLocator loc = null;
        try {
            loc = new OcapLocator(freq, pn, mod);
        } catch (InvalidLocatorException ex) {
            Log.print(ex);
            return;
        }
        if (Log.INFO_ON) {
            Log.printInfo("SdvChannel.reselect = " + loc);
        }
        McData data = (McData) DataCenter.getInstance().get(MiniCarouselReader.MC_KEY);
        if (data != null) {
            synchronized (this) {
                McChannelInfo info = data.get(sourceId);
                Log.printWarning("SdvChannel.reselect : updating MC cache");
                if (info != null) {
                    Log.printWarning(" before = " + info);
                    info.mCarrierFrequency = (long) freq;
                    info.mProgramNumber = pn;
                    info.mModulationMode = (short) mod;
                    Log.printWarning(" after = " + info);
                }
            }
        }
        setLocator(loc);
        if (cc.getState() == ChannelController.CLEAR) {
            super.select(cc, true);
        } else {
            if (Log.INFO_ON) {
                Log.printInfo("SdvChannel.reselect : cc is not clear = " + cc);
            }
        }
        if (!loc.equals(locator)) {
            SdvDiagnostics.increase(SdvDiagnostics.CACHE_OVERRIDES);
        }
    }

}

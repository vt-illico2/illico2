package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.log.Log;
import java.util.*;

public class FakeProgramList extends ProgramList {

    public FakeProgramList(Channel ch) {
        super(new FakeProgram(ch));
        current = start;
    }

    public void resetChannel(Channel ch) {
        ((FakeProgram) start).resetChannel(ch);
    }

    public void append(Program p) {
    }

    public void addAll(ProgramList list) {
    }

    public void add(Program p) {
    }

    public Program getCurrent() {
        return current;
    }

    public Program resetStart(long oldestTime) {
        return resetCurrent();
    }

    public Program resetCurrent() {
        start.startTime = EpgDataManager.firstTime;
        start.endTime = EpgDataManager.storedEndTime;
        return start;
    }

    public Program search(long time, Program startPoint) {
        return search(time);
    }

    public Program search(long time) {
        if (time < start.startTime) {
            start.startTime = time;
            return start;
        }
        if (end.endTime <= time) {
            end.endTime = time;
            return end;
        }
        return current;
    }

    public boolean fill(long to) {
        if (end.endTime < to) {
            end.endTime = to;
            return true;
        }
        return false;
    }

    public void printList() {
        if (Log.DEBUG_ON) {
            Program p = start;
            while (p != null) {
                Log.printDebug("TypedProgramList: " + p + "   " + p.prev);
                p = p.next;
            }
        }
    }

    public String toString() {
        return "TypedProgramList(" + callLetter + ")";
    }

}




package com.alticast.illico.epg.data;

import java.rmi.RemoteException;
import com.alticast.illico.epg.App;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;

public class ReminderOption implements NotificationOption {

    String name;
    Program program;
    int type;
    long startTime;
    long displayTime;
    int popupType;
    int action;

    public ReminderOption(Program p, int type, long delay) {
        program = p;
        startTime = p.getStartTime();
        displayTime = startTime;
        popupType = PROGRAM_REMINDER_POPUP;
        action = ACTION_TUNE_CHANNEL;
        if (p instanceof PpvProgram) {
            PpvProgram ppv = (PpvProgram) p;
            name = "PPV";
            this.type = type;
            if (type == ReminderManager.TYPE_PPV_ORDERABLE) {
                displayTime = ppv.marketingWindowStartTime + delay;
                popupType = PPV_ORDER_REMINDER_POPUP;
            } else if (type == ReminderManager.TYPE_PPV_ORDERED) {
                popupType = PPV_REMINDER_POPUP;
//                action = ACTION_NONE;
            }
        } else {
            name = App.NAME;
            this.type = ReminderManager.TYPE_PROGRAM;
        }
    }

    public String getApplicationName() {
        return name;
    }

    public int getCategories() {
        return 0;
    }

    public long getDisplayTime() {
        return displayTime;
    }

    public String getMessage() {
        return program.getTitle();
    }

    public String getMessageID() {
        return null;
    }

    public int getNotificationAction() {
        return action;
    }

    public String[] getNotificationActionData() {
        String call = program.getCallLetter();
        return new String[] { call, program.getRating(),
                String.valueOf(type), call + '|' + startTime };
    }

    public int getNotificationPopupType() {
        return popupType;
    }

    public long getRemainderDelay() {
        return 0;
    }

    public int getRemainderTime() {
        return 0;
    }

    public String[][] getButtonNameAction() {
        return null;
    }

    public String getLargePopupSubMessage() {
        return null;
    }

    public String getLargePopupMessage() {
        return null;
    }

    public String getSubMessage() {
        return null;
    }

    public boolean isCountDown() {
        return type != ReminderManager.TYPE_PPV_ORDERABLE;
    }

    public int getDisplayCount() {
        return 0;
    }

    public boolean isViewed() {
        return false;
    }

    public long getCreateDate() {
        return 0;
    }

}

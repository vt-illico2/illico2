package com.alticast.illico.epg.data;

/**
 * This class provides EPG functionality for EPG only.
 *
 * @author  June Park
 */
public interface EpgDataListenerInternal {

    byte EPG_DATA = 1;
    byte PPV_DATA = 2;

    void epgDataUpdated(byte type, long from, long to);

}

package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

public class FakeProgram extends Program {

    private String title;
    private String language;

    public FakeProgram(Channel ch) {
        this.callLetter = ch.getName();
        this.startTime = EpgDataManager.firstTime;
        this.endTime = Math.max(EpgDataManager.storedEndTime, startTime + EpgDataManager.DEFAULT_DURATION);
        resetChannel(ch);
    }

    /** MS conifg 변경에 의해 정보가 바뀌었을때 호출. */
    public void resetChannel(Channel ch) {
        String s;
        switch (ch.getType()) {
            case Channel.TYPE_GALAXIE:
                s = ch.fullName;
                if (s != null && s.length() > 0) {
                    //Kenneth [2014.10.2] : Galaxie Rebranding 때문에 헤드(Galaxie -) 삭제
                    //title = "Galaxie - " + s;
                    title = s;
                } else {
                    //title = "Galaxie - " + ch.getCallLetter();
                    title = ch.getCallLetter();
                }
                break;
            case Channel.TYPE_RADIO:
                s = ch.fullName;
                if (s != null && s.length() > 0) {
                    title = "Radio - " + s;
                } else {
                    title = "Radio - " + ch.getCallLetter();
                }
                break;
        }
        language = ch.getLanguage();
    }

    public int getType() {
        return TYPE_UNKNOWN;
    }

	//->Kenneth : 4K by June
    public int getDefinition() {
        return DEFINITION_SD;
    }

    public boolean isCaptioned() {
        return false;
    }

    public boolean isLive() {
        return false;
    }

    public boolean isInteractive() {
        return false;
    }

    public boolean isSapEnabled() {
        return false;
    }

    public int getStarRating() {
        return 0;
    }

    /**
     * Gets a language of this Program.
     * @return language.
     */
    public String getLanguage() {
        return language;
    }

    public String getTitle() {
        return title;
    }

    /**
     * Gets an unique identifier of Program.
     * @return program id.
     */
    public String getId() {
        return callLetter + "|" + startTime;
    }

    /**
     * Gets an audio type of Program.
     * @return audio type.
     */
    public int getAudioType() {
        return AUDIO_UNKNOWN;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getCategory() {
        return 0;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getSubCategory() {
        return 0;
    }

    public int getRatingIndex() {
        return 0;
    }

    public String getProductionYear() {
        return "";
    }

    public String getCountry() {
        return "";
    }

    public String getFullDescription() {
        Channel ch = getEpgChannel();
        if (ch != null) {
            return ch.getDescription();
        }
        return "";
    }

    public String getDirector() {
        return "";
    }

    public String getActors() {
        return "";
    }

    public String getSeriesId() {
        return "";
    }

    public String getEpisodeTitle() {
        return "";
    }

    public long getPpvEid() {
        return 0L;
    }

}

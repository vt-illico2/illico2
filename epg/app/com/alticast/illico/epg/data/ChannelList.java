package com.alticast.illico.epg.data;

import java.util.Vector;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.*;

/**
 * <code>ChannelList</code> represents the abstraction of channel ring.
 * Channel ring is the circular channel list which user navigates forward or
 * backward. All channels in channel ring is sorted in ascending order by
 * channel number.
 *
 * @author June Park
 * @since Charles CW Kwak, 2004-05-15
 */
public class ChannelList {

    public static final ChannelComparator ASCENDING_BY_NAME = new ChannelNameComparator(true);
    public static final ChannelComparator DESCENDING_BY_NAME = new ChannelNameComparator(false);
    public static final ChannelComparator ASCENDING_BY_NUMBER = new ChannelNumberComparator(true);
    public static final ChannelComparator DESCENDING_BY_NUMBER = new ChannelNumberComparator(false);

    private int currentIndex = 0;

    private Channel[] channels;
    private ChannelComparator comparator;


    public ChannelList() {
        init(new Channel[0], ASCENDING_BY_NUMBER);
    }

    /**
     * Creates a new channel ring with ring name.
     *
     * @param name the ring name.
     */
    public ChannelList(Vector vector) {
        Channel[] array = new Channel[vector.size()];
        vector.copyInto(array);
        init(array, ASCENDING_BY_NUMBER);
    }

    /**
     * Creates a new channel ring with ring name.
     *
     * @param name the ring name.
     */
    public ChannelList(Channel[] array) {
        this(array, ASCENDING_BY_NUMBER);
    }

    /**
     * Creates a new channel ring with ring name and channel list.
     *
     * @param name the ring name.
     * @param sortedChannels the array of {@link com.knc.ma.nav.service.Channel}
     *      objects. This array has to be sorted. Sorting criteria is channel
     *      number and all channel ring has to be sorted by same criteria.
     */
    private ChannelList(Channel[] array, ChannelComparator c) {
        init(array, c);
    }

    private void init(Channel[] array, ChannelComparator c) {
        this.comparator = c;
        this.channels = array;
        sort(channels, c);
    }

    /**
     * Returns the channel list as the array of Channel objects.
     * <p><b>NOTE: </b>It returns the copy of array.</p>
     * @return the channel list as the array of Channel objects contained in
     * 		this channel ring. It returns zero-sized array when it
     * 		does not have any channels.
     */
    public Channel[] getChannels() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.getChannels() : FULL channels are copied()");
        int count = channels.length;
        Channel[] list = new Channel[count];
        System.arraycopy(channels, 0, list, 0, count);
        return list;
    }

    /**
     * Returns the first channel of channel ring.
     * @return the first channel object. null if there is no channels in
     * 		the ring or channel ring is null.
     */
    public Channel getFirstChannel() {
        return getChannelAt(0);
    }

    /**
     * Returns the last channel of channel ring.
     * @return the last channel object. null if there is no channels in
     * 		the ring or channel ring is null.
     */
    public Channel getLastChannel() {
        return getChannelAt(channels.length - 1);
    }

    /**
     * Returns the current channel of channel ring.
     * @return the current channel object.
     */
    public Channel getCurrentChannel() {
        return getChannelAt(currentIndex);
    }

    public Channel getChannelAt(int index) {
        if (index >= 0 && index < channels.length) {
            return channels[index];
        } else {
            return null;
        }
    }

    //->Kenneth[2017.2.13] R7.3 : Tech 채널은 GridEpg, MiniEpg 에서 안보여야 됨
    public void removeTech() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeTech()");
        if (channels == null) return;
        try {
            Channel curCh = getCurrentChannel();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeTech: curCh = "+curCh);
            // TECH 채널이 현재 채널인 경우 (Grid 와 Mini EPG 에서만 불리운다는 가정하에 아래 처리함)
            // Julie 요구사항에 따라서 TECH 가 아닌 가장 가까운 next channel 을 현재 채널로 한다.
            if (curCh.getType() == Channel.TYPE_TECH) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeTech: curCh is TECH. So find next non-TECH channel");
                boolean isTech = true;
                while (isTech) {
                    currentIndex = getNextIndex();
                    Channel ch = channels[currentIndex];
                    if (ch.getType() != Channel.TYPE_TECH) {
                        curCh = ch;
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeTech: new curCh = "+curCh);
                        isTech = false;
                    }
                }
            }
            Vector buf = new Vector();
            int size = channels.length;
            for (int i = 0 ; i < size ; i++) {
                if (channels[i].getType() != Channel.TYPE_TECH) {
                    buf.addElement(channels[i]);
                    continue;
                }
            }
            channels = new Channel[buf.size()];
            System.arraycopy(buf.toArray(), 0, channels, 0, channels.length);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeTech() : Call setCurrentChannel()");
            boolean result = setCurrentChannel(curCh);
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //<-

    //->Kenneth[2015.10.7] VDTRMASTER-5695. subscription 여부를 따지도록 하는 코드 추가
    //->Kenneth[2015.10.7] VDTRMASTER-5664 때문에 removeHd 를 받는 방식으로 바꿈. 채널 넘버 낮은 쪽(SD) 를 지워햐 하는
    // 경우도 생김.
    //->Kenneth[2015.3.30] R5 : SD/HD grouping 을 위해서 sister 채널(채널 넘버 높은쪽)을 지운다.
    public void removeSister(boolean removeHd, boolean checkSubscription) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister("+removeHd+", "+checkSubscription+")");
        if (channels == null) return;
        //->Kenneth[2015.6.6] 채널수가 줄게 되므로 다시 찾아야 한다.
        Channel curCh = getCurrentChannel();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : curCh = "+curCh);
        //<-

        Vector buf = new Vector();
        try {
            int size = channels.length;
            for (int i = 0 ; i < size ; i++) {
                Channel ch = channels[i];
                Channel sister = (Channel)ch.getSisterChannel();
                if (sister == null) {
                    buf.addElement(ch);
                    continue;
                }
                int chNum = ch.getNumber();
                int sisterNum = sister.getNumber();
                //->Kenneth[2015.10.16] VDTRMASTER-5695. subscription 여부를 따지도록 하는 코드 추가
                if (checkSubscription) {
                    boolean chSubscribed = ch.isSubscribed();
                    boolean sisterSubscribed = sister.isSubscribed();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : chSubscribed = "+chSubscribed);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : sisterSubscribed = "+sisterSubscribed);
                    if (chSubscribed && !sisterSubscribed) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : "+chNum+":"+sisterNum+" -> "+chNum+" subscribed and selected");
                        buf.addElement(ch);
                        continue;
                    } else if (!chSubscribed) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : "+chNum+" is skipped due to unsubscription");
                        continue;
                    } 
                }
                //<-
                if (removeHd) {
                    if (chNum < sisterNum) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : "+chNum+":"+sisterNum+" -> "+chNum+" selected");
                        buf.addElement(ch);
                        continue;
                    }
                } else {
                    if (chNum > sisterNum) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister : "+sisterNum+":"+chNum+" -> "+chNum+" selected");
                        buf.addElement(ch);
                        continue;
                    }
                }
            }
            channels = new Channel[buf.size()];
            for (int i = 0 ; i < channels.length ; i++) {
                channels[i] = (Channel)buf.elementAt(i);
            }

            //->Kenneth[2015.6.6] 채널수가 줄게 되므로 currentChannel, currentIndex 재설정
            if (curCh == null) return;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister() : Call setCurrentChannel()");
            boolean result = setCurrentChannel(curCh);
            if (!result) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister() : setCurrentChannel() returned false");
                Channel sister = (Channel)curCh.getSisterChannel();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister() : sister = "+sister);
                if (sister != null) {
                    result = setCurrentChannel(sister);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.removeSister() : setCurrentChannel(sister) returned "+result);
                }
            }
            //<-
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //->Kenneth[2017.9.7] : 아래 요구사항 철회. 따라서 이전 코드로 돌린다.
    //->Kenneth[2017.8.21] R7.4 : Virtual 이면서 sister 가 있는 경우도 skip 해야됨.
    // 기존의 코드가 recursively 호출되어야 하기 때문에 여기에 새로 통합해서 함수를 만든다.
    /*
    public Channel getPreviousChannel() {
        return findChannel(false);
    }

    public Channel getNextChannel() {
        return findChannel(true);
    }

    public Channel findChannel(boolean isNext) {
        int index = 0;
        if (isNext) {
            index = getNextIndex();
        } else {
            index = getPreviousIndex();
        }
        Channel ch = null;
        try {
            if (isNext) {
                // Next 채널 찾기
                while (true) {
                    ch = getChannelAt(index);
                    if (ch.getType() == Channel.TYPE_VIRTUAL && ch.getSisterChannel() != null) {
                        // virtual 채널인 sister 가 있으면 skip
                        index ++;
                        if (index == channels.length) {
                            index = 0;
                        }
                        continue;
                    }
                    if (!Environment.SUPPORT_UHD && ch.getDefinition() == Channel.DEFINITION_4K) {
                        // UHD 박스가 아닌데 UHD 채널인 경우는 skip
                        index ++;
                        if (index == channels.length) {
                            index = 0;
                        }
                        continue;
                    }
                    break;
                }
            } else {
                // Previous 채널 찾기
                while (true) {
                    ch = getChannelAt(index);
                    if (ch.getType() == Channel.TYPE_VIRTUAL && ch.getSisterChannel() != null) {
                        // virtual 채널인 sister 가 있으면 skip
                        index --;
                        if (index < 0) {
                            index = channels.length - 1;
                        }
                        continue;
                    }
                    if (!Environment.SUPPORT_UHD && ch.getDefinition() == Channel.DEFINITION_4K) {
                        // UHD 박스가 아닌데 UHD 채널인 경우는 skip
                        index --;
                        if (index < 0) {
                            index = channels.length - 1;
                        }
                        continue;
                    }
                    break;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return ch;
    }
    */
    public Channel getPreviousChannel() {
        //->Kenneth[2015.3.6] 4K : Non UHD STB 에서는 재핑시 UHD 채널을 skip 해야 함.
        if (Environment.SUPPORT_UHD) {
            return getChannelAt(getPreviousIndex());
        }
        return findNonUhdChannel(false);
        //return getChannelAt(getPreviousIndex());
    }

    public Channel getNextChannel() {
        //->Kenneth[2015.3.6] 4K : Non UHD STB 에서는 재핑시 UHD 채널을 skip 해야 함.
        if (Environment.SUPPORT_UHD) {
            return getChannelAt(getNextIndex());
        }
        return findNonUhdChannel(true);
        //return getChannelAt(getNextIndex());
    }

    //->Kenneth[2015.3.6] 4K : Non UHD STB 에서는 재핑시 UHD 채널을 skip 해야 함.
    private Channel findNonUhdChannel(boolean isNext) {
        int index = 0;
        if (isNext) {
            index = getNextIndex();
        } else {
            index = getPreviousIndex();
        }
        Channel ch = getChannelAt(index);
        try {
            if (ch.getDefinition() != Channel.DEFINITION_4K) {
                return ch;
            }
            if (isNext) {
                while (true) {
                    index ++;
                    if (index == channels.length) {
                        index = 0;
                    }
                    ch = getChannelAt(index);
                    if (ch.getDefinition() != Channel.DEFINITION_4K) {
                        break;
                    }
                }
            } else {
                while (true) {
                    index --;
                    if (index < 0) {
                        index = channels.length - 1;
                    }
                    ch = getChannelAt(index);
                    if (ch.getDefinition() != Channel.DEFINITION_4K) {
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return ch;
    }
    //<-

    public int getCurrentIndex() {
        return currentIndex;
    }

    private int getPreviousIndex() {
        int size = channels.length;
        if (currentIndex >= 0) {
            return (currentIndex - 1 + size) % size;
        } else {
            return (-currentIndex - 1 - 1 + size) % size;
        }
    }

    private int getNextIndex() {
        int size = channels.length;
        if (currentIndex >= 0) {
            return (currentIndex + 1) % size;
        } else {
            return (-currentIndex - 1) % size;
        }
    }

    public boolean resetCurrentChannel(int number) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelList.resetCurrentChannel: no=" + number + ", i=" + currentIndex);
            Log.printDebug(App.LOG_HEADER+"ChannelList.resetCurrentChannel: ch="+getChannelAt(currentIndex));
        }
        for (int i = 0; i < channels.length; i++) {
            Channel ch = channels[i];
            if (ch != null && ch.getNumber() == number) {
                currentIndex = i;
                if (Log.DEBUG_ON) {
                    Log.printDebug(App.LOG_HEADER+"ChannelList.resetCurrentChannel: new index = " + i);
                    Log.printDebug(App.LOG_HEADER+"ChannelList.resetCurrentChannel: ch="+getChannelAt(currentIndex));
                }
                return true;
            }
        }
        return false;
    }

    public boolean setCurrentChannel(Channel ch) {
        Log.printWarning(App.LOG_HEADER+"ChannelList.setCurrentChannel("+ch+")");
        if (channels.length == 0) {
            return false;
        }
        if (ch == null) {
            Log.printWarning(App.LOG_HEADER+"ChannelList.setCurrentChannel: channel is null");
            return false;
        }
        int index = getNextIndex();
        if (ch.equals(getChannelAt(index))) {
            currentIndex = index;
            return true;
        }
        index = getPreviousIndex();
        if (ch.equals(getChannelAt(index))) {
            currentIndex = index;
            return true;
        }
        currentIndex = search(ch);
        return (currentIndex >= 0);
    }

    /**
     * Test if it contains the given channel. It regards two channels with
     * same channel number are same.
     * @param ch channel to test if it contains
     * @return true if it contains the channel with same number as that of
     * 		the given channel. (Both major number and minor number have to be
     * 		same); false otherwise.
     */
    public boolean contains(Channel ch) {
        return search(ch) >= 0;
    }

    /**
     * Returns the count of channels which it contains.
     * @return the count of channels which it contains
     */
    public int size() {
        return channels.length;
    }

    //->Kenneth[2015.7.16] R5 : VDTRMASTER-5506 sister 채널 갯수를 알 필요가 있음.
    public int getSisterSize() {
        if (channels == null || channels.length == 0) return 0;
        int sisterCount = 0;
        try {
            for (int i = 0 ; i < channels.length ; i++) {
                Channel ch = channels[i];
                if (ch == null) continue;
                if (ch.getSisterChannel() != null) {
                    sisterCount ++;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return sisterCount;
    }
    //<-

    public ChannelComparator getComparator() {
        return comparator;
    }

    /**
     * Returns the index of the channel of given major and minor number. This
     * index is valid in the Channel array by getChannel()
     *
     * @param majorNum the major channel number
     * @param minorNum the minor channel number
     * @return the index of channel in the Channel array which obtained from
     *		getChannels(); the negative integer if it does not exist.
     *		The negative integer may be meaningful. It is the negative number
     *		of proper position of the given channel plus 1.
     *		(= -1 * (proper index + 1))
     */
    public int search(Channel target) {
        if (target == null) {
            return -1;
        }
        int low = 0;
        int high = channels.length - 1;

        while (low <= high) {
            int mid = (low + high) >> 1;
            int compare = comparator.compare(target, channels[mid]);
            if (compare > 0) {
                low = mid + 1;
            } else if (compare < 0) {
                high = mid - 1;
            } else {
                return mid;
            }
        }
        return -(low + 1); // key not found.
    }

    /**
     * Sorts the specified channels by the criteria defined in concrete
     * {@link com.knc.ma.nav.service.Channel} class.
     * @param channels the array of channel
     */
    public static void sort(Channel[] channels, ChannelComparator c) {
        sort(channels, c, 0, channels.length - 1);
    }

    private static void sort(Channel[] array, ChannelComparator c, int l, int r) {
        if (l >= r) {
            return;
        }

        int i = l - 1;
        int j = r;
        Channel temp = null;

        while (true) {
            while (c.compare(array[++i], array[r]) < 0);
            while (j > 0 && c.compare(array[--j], array[r]) > 0);
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            if (j <= i) {
                break;
            }
        }

        array[j] = array[i];
        array[i] = array[r];
        array[r] = temp;
        sort(array, c, l, i - 1);
        sort(array, c, i + 1, r);
    }


    //->Kenneth[2015.10.7] 필요에 의해 추가
    public void dump() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.dump() : START =======================================================");
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"*** currentIndex = "+currentIndex);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"");
            if (channels != null) {
                for (int i = 0 ; i < channels.length ; i++) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"*** "+i+"th : "+channels[i]);
                }
            }
        } catch (Exception e){
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.dump() : END =======================================================");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"");
    }

    /**
     * Returns the new sorted list.
     */
    public ChannelList sort(ChannelComparator c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.sort() : CREATE new ChannelList()");
        ChannelList newList = new ChannelList(getChannels(), c);
        Channel ch = getCurrentChannel();
        if (ch == null) {
            ch = getNextChannel();
        }
        newList.setCurrentChannel(ch);
        return newList;
    }

    public ChannelList filter(ChannelFilter filter) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.filter("+filter+")");
        Vector v = new Vector(channels.length);
        Channel c;
        for (int i = 0; i < channels.length; i++) {
            c = channels[i];
            if (filter.accept(c)) {
                //->Kenneth[2017.4.21] : R7.3 : VDTRMASTER-6097 : my package filter 의 경우 TECH 채널 제거
                if (filter.equals(ChannelDatabase.subscribedFilter) && c.getType() == Channel.TYPE_TECH) {
                    continue;
                }
                //<-
                v.addElement(c);
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.filter() : CREATE new ChannelList()");
        ChannelList newList = new ChannelList(v);
        Channel cur = getCurrentChannel();
//        if (cur != null) {
        boolean chExisting = newList.setCurrentChannel(cur);
//        } else {
//            int index = newList.search(getNextChannel());
//            if (index >= 0) {
//
//            }
//        }
        return newList;
    }

    public ChannelList cloneList() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.cloneList() : FULL channels are copied()");
        Channel[] ch = new Channel[channels.length];
        System.arraycopy(channels, 0, ch, 0, ch.length);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelList.cloneList() : CREATE new ChannelList()");
        ChannelList newList = new ChannelList(ch, comparator);
        newList.currentIndex = currentIndex;
        return newList;
    }

}

class ChannelNameComparator implements ChannelComparator {

    private boolean isAscending;

    public ChannelNameComparator(boolean ascending) {
        this.isAscending = ascending;
    }

    public int compare(Channel a, Channel b) {
        //->Kenneth[2016.2.11] VDTRMASTER-5732 : Call Letter 가 아닌 full name 으로 소팅하기
        int x = a.getFullName().compareTo(b.getFullName());
        /*
        int x = a.getName().compareTo(b.getName());
        */
        //<-
        if (x == 0) {
            // 같은 이름을 가지고 있는 경우 때문에 추가.
            x = a.getNumber() - b.getNumber();
        }
        if (!isAscending) {
            x = -x;
        }
        return x;
    }
}

class ChannelNumberComparator implements ChannelComparator {

    private boolean isAscending;

    public ChannelNumberComparator(boolean ascending) {
        this.isAscending = ascending;
    }

    public int compare(Channel a, Channel b) {
        int x = a.getNumber() - b.getNumber();
        if (!isAscending) {
            x = -x;
        }
        return x;
    }
}

package com.alticast.illico.epg.data;

import java.awt.Image;
import java.util.Hashtable;
import javax.tv.service.Service;
import org.ocap.net.OcapLocator;
import javax.tv.locator.Locator;
import javax.tv.service.selection.ServiceContext;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.alticast.illico.epg.util.Util;
import com.alticast.illico.epg.CaManager;
import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.log.Log;

public abstract class Channel implements TvChannel {
    protected Service service;
    public final int sourceId;
    public final Integer sourceIdObject;

    protected String name;
    protected int number;
    protected short type;

	//->Kenneth : 4K by June
    protected int definition;
    protected int rating;
    protected String language;
    protected byte genre;

//    public boolean isAuthorized = true;
//    public boolean isFavorite;
//    public boolean isBlocked;

    public String friendlyName = "";
    public String fullName = "";
    public String radioFrequency = "";
    public String[] descriptions = { "", "" };

    public Channel sister = null;

    public static final int SCREEN_MASK_WEB    = 0x01;
    public static final int SCREEN_MASK_MOBILE = 0x02;
    public static final int SCREEN_MASK_900    = 0x04;
    public static final int SCREEN_MASK_TABLET = 0x08;

    //->Kenneth[2017.3.23] R7.3 : APD disabled 
    public boolean apdDisabled;
    //<-

    protected Channel(int sid) {
        this.sourceId = sid;
        this.sourceIdObject = new Integer(sid);
    }

    public Channel(final Service svc) {
        this(svc != null ? ((OcapLocator) svc.getLocator()).getSourceID() : 0);
        this.service = svc;
    }

    protected void setType(short newType) {
        this.type = newType;
    }

    protected void setChannelInfo(ChannelInfo info) {
        if (info == null) {
            return;
        }
        setType((short) info.type);
		//->Kenneth : 4K by June
        this.definition = info.definition;
        this.genre = (byte) info.genreId;
        this.language = info.language;
        this.friendlyName = info.friendlyName;
        this.fullName = info.fullName;
        this.radioFrequency = info.radioFrequency;
        this.descriptions = info.descriptions;
        this.rating = info.rating;
        //->Kenneth[2017.3.23] R7.3 : APD disabled 
        this.apdDisabled = info.apdDisabled;
        //<-
    }

    public int getScreenData() {
        ChannelInfoData cid = ConfigManager.getInstance().channelInfo;
        if (cid != null) {
            Hashtable table = cid.channelTable;
            if (table != null) {
                ChannelInfo ci = (ChannelInfo) table.get(name);
                if (ci != null) {
                    return ci.screenData;
                }
            }
        }
        return 0;
    }

    public abstract int getId();

    public int getNumber() {
        return number;
    }

    public int getSourceId() {
        return sourceId;
    }

    public Integer getSourceIdObject() {
        return sourceIdObject;
    }

    public short getType() {
        return type;
    }

    public final String getCallLetter() {
        return name;
    }

    public final String getFullName() {
        //->Kenneth[2016.2.11] VDTRMASTER-5732 : Full Name 이 null 이 안되게 하자
        // 그래야지 ChannelList.compare() 에서 full name 비교할때 오류를 막을수 있음.
        if (fullName == null) return "";
        //<-
        return fullName;
    }

    public final String getName() {
        return name;
    }

    public String getLanguage() {
        return language;
    }

    public int getRating() {
        return rating;
    }

    public byte getGenre() {
        return genre;
    }

	//->Kenneth : 4K by June
    public boolean isHd() {
        return definition >= DEFINITION_HD;
    }

    public int getDefinition() {
        return definition;
    }

    public int compareTo(Channel ch) {
        return number - ch.getNumber();
    }

    public boolean equals(Object o) {
        if (o != null && o instanceof Channel) {
            Channel ch = (Channel) o;
            // 가끔 같은 sid를 가진 채널이 있으면 indexing에 문제가 생긴다.
            return ch.getId() == getId() && ch.getNumber() == number;
        }
        return false;
    }

    public boolean isFavorite() {
        return ChannelDatabase.favoriteFilter.accept(this);
    }

    public boolean isBlocked() {
        return ChannelDatabase.blockedFilter.accept(this);
    }

    /** 미가입이 아닌지. (CANH 문제로 결과를 제대로 주지 않은 경우는 일단 가입으로 간주.) */
    public boolean isAuthorized() {
        return !CaManager.isUnsubscribed(this);
    }

    public boolean isSubscribed() {
        return CaManager.getInstance().checkIfNeeded(this);
    }

    //->Kenneth[2016.8.24] 설명 추가 : 이 함수의 의미는 AV 는 Preview 이기 때문에 볼 수는 있지만
    // 실제 가입된 채널은 아니라는 의미. 즉 AV 는 볼 수 있되 ISA 가입 유도해야 하는 대상임.
    public boolean isAuthorizedByFreePreview() {
        return isSubscribed() && !PreviewChannels.hasPackage(this);
    }

    public Image getLogo() {
        return ChannelLogos.getLogo(name);
    }

    public Image getScaledLogo() {
        return ChannelLogos.getScaledLogo(name);
    }

    public String toString() {
        StringBuffer buf = new StringBuffer(70);
        buf.append("[Channel: no=").append(number).append(", name=")
           .append(name).append(", sid=").append(getSourceId())
           .append(", type=").append(getTypeString()).append("]");
        return buf.toString();
    }

    public Service getService() {
        return service;
    }

    public boolean select(ChannelController cc, boolean auth) {
        cc.select(service);
        return true;
    }

    public Locator getLocator() {
        return service.getLocator();
    }

    public org.davic.net.Locator getDavicLocator() {
        Locator loc = getLocator();
        if (loc instanceof org.davic.net.Locator) {
            return (org.davic.net.Locator) loc;
        } else if (sourceId > 0) {
            try {
                return new OcapLocator(sourceId);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return null;
    }

    public String getLocatorString() {
        Locator locator = getLocator();
        if (locator != null) {
            return locator.toExternalForm();
        } else {
            return null;
        }
    }

    public String getDescription() {
        int li = Util.getLanguageIndex(FrameworkMain.getInstance().getCurrentLanguage());
        if (descriptions != null && li >= 0 && li < descriptions.length) {
            return descriptions[li];
        } else {
            return "";
        }
    }

    public TvChannel getSisterChannel() {
        return sister;
    }

    //->Kenneth[2017.3.23] R7.3 APD
    public boolean apdDisabled() {
        return apdDisabled;
    }
    //<-

    public boolean isRecordable() {
        switch (type) {
            case TYPE_NORMAL:
            case TYPE_SPORTSMAX:
            case TYPE_SDV:
            case TYPE_PPV:
                return true;
            default:
                return false;
        }
    }

    public boolean isSurfableType() {
        switch (type) {
            case TYPE_UNKNOWN:
            case TYPE_ABSTRACT:
            case TYPE_DATA:
            case TYPE_NOT_OFFERED:
            case TYPE_HIDDEN:
                return false;
            case TYPE_TECH:
                return isAuthorized();
            default:
                return true;
        }
    }

    public boolean isAppChannel() {
        return false;
    }

    public boolean isPresenting(ChannelController cc) {
        return isPresenting(cc.getServiceContext());
    }

    public boolean isPresenting(ServiceContext sc) {
        if (service != null) {
            return service.equals(sc.getService());
        } else {
            return false;
        }
    }

    public boolean isFreePreview() {
        return ConfigManager.getInstance().freePreviews.contains(name);
    }

    //->Kenneth[2016.8.23] CYO Bundling/A la carte
    // A la carte 의 추가로 인해서 기존의 로직을 적용할 수 없음.
    // 그래서 isISAPurchasable 과 isISAPremium 를 정의한다.
    // - isISAPurchasable 의 경우 미가입이고, PPV 아니고, Preview 패키지 없고
    //   exluded channel 에 포함되지 않고 excluded type 에도 포함되지 않은 놈임.
    // - isISAPremium 의 경우 기존의 isISASupported 의 동일한 구현이나 의미를 명확하게 하기 위해
    //   rename 한다. true 인 경우 기존의 ISA 채널임. 
    //
    // 사용법은 먼저 isISAPurchasable() 으로 ISA 구매가능여부 (기존의 ISA + A la carte) 확인한 후
    // isISAPremium() 으로 기존의 ISA 인지 A la carte 인지 구분한다.
    // ==> (중요) 구현하다 보니 ISA Premium 인지는 체크할 필요 없음. 왜냐하면 
    // premium 팝업을 보여주느냐, A la carte 팝업을 보여주느냐는 EPG 가 아닌 ISA 앱에서 처리할 것이기
    // 때문임. 따라서 isISAPremium()은 삭제한다.
    public boolean isISAPurchasable() {
        // PPV 이면 isa 구매 안됨
        if (type == Channel.TYPE_PPV) return false;
        // UNKNOWN, ABSTRACT, HIDDEN, VIRTUAL, TECH, DATA, NOT_OFFERED, NO_PROGRAM 의 경우 ISA 구매 안됨
        if (type == Channel.TYPE_UNKNOWN
                || type == Channel.TYPE_ABSTRACT
                || type == Channel.TYPE_HIDDEN
                || type == Channel.TYPE_VIRTUAL
                || type == Channel.TYPE_TECH
                || type == Channel.TYPE_DATA
                || type == Channel.TYPE_NOT_OFFERED
                || type == Channel.TYPE_NO_PROGRAM) return false;
        // 시청가능하며 preview 패키지가 있는 경우는 구매 필요 없음.
        if (isAuthorized() && PreviewChannels.hasPackage(this)) return false;
        // excluded channel 에 포함되는 경우는 ISA 구매 안됨
        if (ConfigManager.getInstance().isaExcludedChannels.contains(name)) return false;
        // excluded type 에 포함되는 경우는 ISA 구매 안됨
        if (ConfigManager.getInstance().isISAExcludedType(type)) return false;

        // 위의 필터 통과했으면 ISA 구매 가능
        return true;
    }

    //public boolean isISAPremium() {
    //    return ConfigManager.getInstance().isaChannels.contains(name);
    //}
    /*
    public boolean isISASupported() {
//        return number >= 50 && number < 70 || name.startsWith("T");
        return ConfigManager.getInstance().isaChannels.contains(name);
    }

    public boolean isPurchasable() {
        return type != Channel.TYPE_PPV && isISASupported() && (!isAuthorized() || !PreviewChannels.hasPackage(this));
    }
    */
    //<-

    public void startSdvSession(ChannelController cc, boolean selected) {
        try {
            SDVSessionManager.getInstance().startChannel(cc, this, selected);
        } catch (Throwable t) {
            Log.print(t);
        }
    }

    public String getTypeString() {
        return getTypeString(type);
    }

    public static String getTypeString(int type) {
        switch (type) {
            case TYPE_ABSTRACT:
                return "ABSTRACT";
            case TYPE_NORMAL:
                return "NORMAL";
            case TYPE_RADIO:
                return "RADIO";
            case TYPE_GALAXIE:
                return "GALAXIE";
            case TYPE_PPV:
                return "PPV";
            case TYPE_SPORTSMAX:
                return "SPORTSMAX";
            case TYPE_VIRTUAL:
                return "VIRTUAL";
            case TYPE_TECH:
                return "TECH";
            case TYPE_DATA:
                return "DATA";
            case TYPE_SDV:
                return "SDV";
            case TYPE_NOT_OFFERED:
                return "NOT_OFFERED";
            case TYPE_HIDDEN:
                return "HIDDEN";
            //->Kenneth[2015.4.18] : R5 : No Program type 추가됨.
            case TYPE_NO_PROGRAM:
                return "NO_PROGRAM";
            //->Kenneth[2017.8.24] : R7.4 : Camera type 추가
            case TYPE_CAMERA:
                return "CAMERA";
            default:
                return "UNKNOWN";
        }
    }

}

/**
 * 
 */
package com.alticast.illico.epg.data.similar.type;

import org.json.simple.JSONObject;

/**
 * @author zestyman
 *
 */
public class SHead extends BaseJson {
	
	private String jvmName;
	private long cacheTimeStamp;
	private int totalElementCount;
	private int returnedElementCount;
	private int nextFrom;
	
	private SError error;

	public String getJvmName() {
		return jvmName;
	}

	public long getCacheTimeStamp() {
		return cacheTimeStamp;
	}

	public int getTotalElementCount() {
		return totalElementCount;
	}

	public int getReturnedElementCount() {
		return returnedElementCount;
	}

	public int getNextFrom() {
		return nextFrom;
	}
	
	public SError getError() {
		return error;
	}

	/* (non-Javadoc)
	 * @see com.videotron.tvi.illico.vod.data.similar.type.BaseJson#parseJson(java.lang.Object)
	 */
	public void parseJson(Object obj)  throws Exception {
		JSONObject jObj = (JSONObject) obj;
		
		// handle in case of Error
		Object eObj = jObj.get("error");
		
		if (eObj != null) {
			error = new SError();
			error.parseJson(eObj);
		}
		
		jvmName = getString(jObj, "JVMName");
		cacheTimeStamp = getLong(jObj, "cacheTimeStamp");
		
		if (error == null) {
			totalElementCount = (int) getLong(jObj, "totalElementCount");
			returnedElementCount =  (int) getLong(jObj, "returnedElementCount");
			nextFrom = (int) getLong(jObj, "nextFrom");
		}
	}

}

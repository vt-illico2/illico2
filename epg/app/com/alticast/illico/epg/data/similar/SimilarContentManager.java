/**
 * 
 */
package com.alticast.illico.epg.data.similar;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.similar.type.SBody;
import com.alticast.illico.epg.data.similar.type.SHead;
import com.alticast.illico.epg.data.similar.type.SimilarContent;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.alticast.illico.epg.util.*;

/**
 * @author zestyman
 *
 */
public class SimilarContentManager {

	private static SimilarContentManager instance;
	private SHead lastHead;
	private SimilarContent[] similarArr;
	private Object lock = new Object();
	private Thread thread;

	public static String debugStr = "Similar : ";

	private SimilarContentManager() {

	}

	public static SimilarContentManager getInstance() {
		if (instance == null) {
			instance = new SimilarContentManager();
		}

		return instance;
	}

	public SHead getLastHead() {
		return lastHead;
	}

	public SimilarContent[] requestSimilarContents(final Program p) {
		
		thread = new Thread() {
			public void run() {
				long serverStartTime, serverEndTime, parsingEndTime;
				similarArr = null;
				lastHead = null;

				String[] proxyIPs = (String[]) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_PROXY_IP);
				int[] proxyPorts = (int[]) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_PROXY_PORT);
				String backUrl = (String) SharedMemory.getInstance().get(SearchService.KEY_SEARCH_URL);

				if (Log.DEBUG_ON) {
					Log.printDebug("SimilarContentManager, requestSimilarContents, proxyIPs" + proxyIPs);
					if (proxyIPs != null) {
						for (int i = 0; i < proxyIPs.length; i++) {
							Log.printDebug("SimilarContentManager, requestSimilarContents, proxyIPs[" + i + "]=" + proxyIPs[i]);
						}
					}
					Log.printDebug("SimilarContentManager, requestSimilarContents, proxyPorts" + proxyPorts);
					if (proxyPorts != null) {
						for (int i = 0; i < proxyPorts.length; i++) {
							Log.printDebug("SimilarContentManager, requestSimilarContents, proxyPorts[" + i + "]="
									+ proxyPorts[i]);
						}
					}
					Log.printDebug("SimilarContentManager, requestSimilarContents, backUrl" + backUrl);
				}
				
				// "http://172.26.93.197/illicoservice/stateless/search/vod/similar?vodId=";
				String services = (String)SharedMemory.getInstance().get(VODService.KEY_SVOD_LIST);
				
				if (Environment.EMULATOR) {
					backUrl = "http://dev04stb.int.videotron.com:80/illicoservice/stateless/search";
					services = "S_1";
				}
                //String URL_HEADER = backUrl + "/linear/similar/vod?channelCallLetters=";	
				// VDTRMASTER-5737
				String URL_HEADER = null;
				if (backUrl.endsWith("/")) {
					URL_HEADER = backUrl + "linear/similar/vod?channelCallLetters=";
				} else {
					URL_HEADER = backUrl + "/linear/similar/vod?channelCallLetters=";
				}
                //<-
				String URL_MIDDLE = "&epgId=";
				String URL_SUBFIX = "&language=" + FrameworkMain.getInstance().getCurrentLanguage() + "&services="
						+ services;
				try {
					serverStartTime = System.currentTimeMillis();
					byte[] src = null;
					// VDTRMASTER-5600
					String targetURL = URL_HEADER + URLEncoder.encode(p.callLetter) + URL_MIDDLE + p.getId() + URL_SUBFIX;
//					String targetURL = URL_HEADER + "DJR" + URL_MIDDLE + "648580XE" + URL_SUBFIX;
					if (Environment.EMULATOR) {
//						src = BinaryReader.read(new File("resource/similar.txt"));
						if (proxyIPs != null && proxyPorts != null) {
							src = getBytes(proxyIPs, proxyPorts, targetURL);
						} else {
							src = getBytes(targetURL);
						}
					} else {
						if (proxyIPs != null && proxyPorts != null) {
							src = getBytes(proxyIPs, proxyPorts, targetURL);
						} else {
							src = getBytes(targetURL);
						}
					}
					// byte[] src = URLRequestor.getBytes("10.247.209.97", 8000,
					// URL_HEADER + contentId + URL_SUBFIX, null);
					serverEndTime = System.currentTimeMillis();
					String jsonStr = new String(src, "UTF-8");

					if (Log.DEBUG_ON) {
						Log.printDebug("SimilarContentManager, requestSimilarContents, jsonStr=" + jsonStr);
					}

					JSONParser jsonParser = new JSONParser();

					JSONObject jObj = (JSONObject) jsonParser.parse(jsonStr);

					SHead head = new SHead();
					head.parseJson(jObj.get("head"));
					lastHead = head;
					if (head.getError() != null) {
						similarArr = null;
					} else {
						SBody body = new SBody();
						body.parseJson(jObj.get("body"));

						similarArr = body.getSimilarContents();
					}
					parsingEndTime = System.currentTimeMillis();

					debugStr = "Similar : requestTime=" + (serverEndTime - serverStartTime) + ", parsingTime="
							+ (parsingEndTime - serverEndTime);
				} catch (IOException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				} catch (ParseException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				} catch (InterruptedException e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					
					return;
				} catch (Exception e) {
					Log.print(e);
					debugStr = "Similar : " + e.getMessage();
					similarArr = new SimilarContent[0];
				}
				
				synchronized (lock) {
					lock.notifyAll();
				}
			}
		};
		
		thread.start();
		
		synchronized (lock) {
			try {
				lock.wait(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		if (thread != null && thread.isAlive() && similarArr == null && lastHead == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("SimilarContentManager, requestSimilarContents, call interrupt()");
			}
			similarArr = new SimilarContent[0];
			thread.interrupt();
			thread = null;
		}

		return similarArr;
	}
	

    // TEST CODE
    /*
    private static void testUrl() {
        URL url = null;
        String path = "http://aut07cyoapi.int.videotron.com/client/residentiel/illico/Login.action?request_locale=fr_CA&macId=xxxxxx&version=99";
        DebugScreen.getInstance().addLog(path);
        URLConnection con = null;
        try {
        	url = new URL(path);
            con = url.openConnection();
            if (Log.INFO_ON) {
                if (con instanceof HttpURLConnection) {
                    HttpURLConnection hc = (HttpURLConnection) con;
                    DebugScreen.getInstance().addLog(hc.getResponseCode()+"");
                    DebugScreen.getInstance().addLog(hc.getResponseMessage());
                }
            }
        } catch (Exception e) {
            Log.print(e);
            DebugScreen.getInstance().addLog(e.toString());
        } finally {
        }
    }
    */
	
	/**
     * Gets bytes from URL.
     *
     * @param proxyHost IP or DNS of proxy server, if null don't use proxy
     * @param proxyPort port of proxy server
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public static byte[] getBytes(String proxyHost, int proxyPort, String locator) throws IOException {
        String path = locator;
        if (Log.INFO_ON) {
            Log.printInfo("SimilarContentManager, getBytes path = " + path);
        }
        URL url = null;
        if (proxyHost != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes proxy:port = " + proxyHost + ":" + proxyPort);
            }
        	url = new URL("HTTP", proxyHost, proxyPort, path);
        } else {
        	url = new URL(path);
        }
        //->Kenneth[2016.7.19] R7.2 : On-screen debug
        DebugScreen.getInstance().addLog(url.toString());
        //testUrl();
        //<-
        URLConnection con = url.openConnection();
        if (Log.INFO_ON) {
            if (con instanceof HttpURLConnection) {
                HttpURLConnection hc = (HttpURLConnection) con;
                Log.printInfo("SimilarContentManager, getBytes response = " + hc.getResponseCode());
                Log.printInfo("SimilarContentManager, getBytes : " + hc.getResponseMessage());
            }
        }

        BufferedInputStream bis = null;
        if (con instanceof HttpURLConnection && ((HttpURLConnection) con).getResponseCode() >= 400) {
        	if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes, open errorStream");
            }
        	bis = new BufferedInputStream(((HttpURLConnection) con).getErrorStream());
        } else {
        	if (Log.DEBUG_ON) {
                Log.printDebug("SimilarContentManager, getBytes, open normalStream");
            }
        	bis = new BufferedInputStream(con.getInputStream());
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[2048];
        int c;
        while ((c = bis.read(buf)) != -1) {
            baos.write(buf, 0, c);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("URLRequestor.getBytes read bytes = " + baos.size());
        }
        if (con instanceof HttpURLConnection) {
            ((HttpURLConnection) con).disconnect();
        }
        bis.close();
        return baos.toByteArray();
    }

    /**
     * Gets bytes from URL.
     *
     * @param proxyHosts IP or DNS of proxy server, if null don't use proxy
     * @param proxyPorts port of proxy server
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public byte[] getBytes(String[] proxyHosts, int[] proxyPorts, String locator) throws IOException {
        if (proxyHosts == null || proxyHosts.length == 0) {
            return getBytes(null, 0, locator);
        }
        for (int i = 0; i < proxyHosts.length; i++) {
            try {
                return getBytes(proxyHosts[i], proxyPorts[i], locator);
            } catch (IOException ex) {
                Log.printWarning(ex.getClass() + " / " + proxyHosts[i] + ":" + proxyPorts[i]);
            }
        }
        throw new IOException("no avaliable proxy");
    }

    /**
     * Gets bytes from URL.
     *
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public byte[] getBytes(String locator) throws IOException {
    	return getBytes(null, 0, locator);
    }
}

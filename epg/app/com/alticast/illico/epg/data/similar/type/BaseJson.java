package com.alticast.illico.epg.data.similar.type;

import org.json.simple.JSONObject;

import com.videotron.tvi.illico.util.TextUtil;

public abstract class BaseJson {

	abstract public void parseJson(Object obj) throws Exception;
	
	protected String getString(JSONObject obj, String key) {
		if (obj != null) {
			Object value = obj.get(key);
			if (value != null) {
				return (String) value;
			}
		}
		
		return TextUtil.EMPTY_STRING;
	}
	
	protected long getLong(JSONObject obj, String key) {
		if (obj != null) {
			Object value = obj.get(key);
			if (value != null) {
				return ((Number) value).longValue();
			}
		}
		
		return 0;
	}
	
	protected boolean getBoolean(JSONObject obj, String key) {
		if (obj != null) {
			Object value = obj.get(key);
			if (value != null) {
				if (value instanceof String) {
					return Boolean.valueOf((String)value).booleanValue();
				} else {
					return ((Boolean) value).booleanValue();
				}
			}
		}
		return false;
	}
}

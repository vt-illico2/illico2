/**
 * 
 */
package com.alticast.illico.epg.data.similar.type;

import java.awt.Image;
import java.lang.reflect.Method;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.alticast.illico.epg.ui.ProgramDetails;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.alticast.illico.epg.util.DebugScreen;

/**
 * This class represent Similar content
 * 
 * @author zestyman
 *
 */
public class SimilarContent extends BaseJson {

	private String vodId = TextUtil.EMPTY_STRING;
	private String seasonNo = TextUtil.EMPTY_STRING;

	private SType type;
	private SGenre[] genres;

	private String definitions = TextUtil.EMPTY_STRING;
	private String rating = TextUtil.EMPTY_STRING;
	private String title = TextUtil.EMPTY_STRING;
	private String originalTitle = TextUtil.EMPTY_STRING;
	private String contentImage = TextUtil.EMPTY_STRING;
	private String languages = TextUtil.EMPTY_STRING;
	private String channelCallLetter = TextUtil.EMPTY_STRING;
	private String channelName = TextUtil.EMPTY_STRING;
	private boolean channelFreePreviewIndicator;
	private boolean freeContentIndicator;
	private boolean payPerViewContentIndicator;
	private boolean clubContentIndicator;
	private boolean isChannelContent;
	
	private Image poster;
	
	public String getVodId() {
		return vodId;
	}
	
	public void makeChannelContent() {
		isChannelContent = true;
		title = DataCenter.getInstance().getString("menu.all_content");
	}
	
	public boolean isChannelContent() {
		return isChannelContent;
	}
	
	public String getSeasonNo() {
		return seasonNo;
	}

	public SType getType() {
		return type;
	}

	public SGenre[] getGenres() {
		return genres;
	}

	public String getDefinitions() {
		return definitions;
	}

	public String getRating() {
		return rating;
	}

	public String getTitle() {
		return title;
	}

	public String getOriginalTitle() {
		return originalTitle;
	}

	public String getContentImage() {
		return contentImage;
	}

	public String getLanguages() {
		return languages;
	}

	public String getChannelCallLetter() {
		return channelCallLetter;
	}

	public String getChannelName() {
		return channelName;
	}

	public boolean isChannelFreePreviewIndicator() {
		return channelFreePreviewIndicator;
	}

	public boolean isFreeContentIndicator() {
		return freeContentIndicator;
	}

	public boolean isPayPerViewContentIndicator() {
		return payPerViewContentIndicator;
	}

	public boolean isClubContentIndicator() {
		return clubContentIndicator;
	}
	
	public Image getPoster() {
		return poster;
	}
	
    //->Kenneth[2016.8.26] R7.2 On-screen debug
    private String posterUrl = null;
    //<-
	public void retrieveImage(final UIComponent ui) {
		if (contentImage != null && contentImage != TextUtil.EMPTY_STRING) {
			new Thread("LOAD_SIMILAR_IMAGE_" + vodId) {
				public void run() {
//					poster = CatalogueDatabase.getInstance().getImage(contentImage);
					
					if (Log.DEBUG_ON) {
						Log.printDebug("SimilarContent, retrieveImage, contentImage=" + contentImage);
					}
			        try {
			            Object obj = SharedMemory.getInstance().get(SharedDataKeys.VOD_POSTER_IMAGE);
			            if (obj == null || contentImage == null) {
			            	poster = null;
			            }
			            Method m = obj.getClass().getMethod("getImage", new Class[]{String.class, String.class});
			            if (m != null) {
			                poster = (Image) m.invoke(obj, new Object[]{null, contentImage});
			            }
			        } catch (Exception e) {
			            Log.print(e);
			        }
			        if (Log.DEBUG_ON) {
						Log.printDebug("SimilarContent, retrieveImage, result: " + poster);
					}
			        ui.repaint();

                    //->Kenneth[2016.8.26] R7.2 On-screen debug
                    if (posterUrl == null) {
                        Object strObj = SharedMemory.getInstance().get(SharedDataKeys.VOD_POSTER_URL);
                        if (strObj != null) {
                            posterUrl = (String)strObj;
                        }
                    }
                    if (posterUrl == null && posterUrl.length() > 0) {
                        DebugScreen.getInstance().addLog(contentImage);
                    } else {
                        DebugScreen.getInstance().addLog(posterUrl + contentImage);
                    }
                    //<-
				};
			}.start();
		}
	}
	
	public void retrieveImageDirect() {
		if (contentImage != null && contentImage != TextUtil.EMPTY_STRING) {
					
			if (Log.DEBUG_ON) {
				Log.printDebug("SimilarContent, retrieveImage, contentImage=" + contentImage);
			}
	        try {
	            Object obj = SharedMemory.getInstance().get(SharedDataKeys.VOD_POSTER_IMAGE);
	            if (obj == null || contentImage == null) {
	            	poster = null;
	            }
	            Method m = obj.getClass().getMethod("getImage", new Class[]{String.class, String.class});
	            if (m != null) {
	                poster = (Image) m.invoke(obj, new Object[]{null, contentImage});
	            }
	        } catch (Exception e) {
	            Log.print(e);
	        }
	        if (Log.DEBUG_ON) {
				Log.printDebug("SimilarContent, retrieveImage, result: " + poster);
			}
		}
	}

	public void parseJson(Object obj)  throws Exception {
		JSONObject jObj = (JSONObject) obj;

		vodId = getString(jObj, "vodId");
		seasonNo = getString(jObj, "seasonNo");
		type = new SType();
		type.parseJson(jObj.get("type"));

		JSONArray genreArr = (JSONArray) jObj.get("genres");
		genres = new SGenre[genreArr.size()];
		for (int i = 0; i < genreArr.size(); i++) {
			genres[i] = new SGenre();
			genres[i].parseJson(genreArr.get(i));
		}

		definitions = getString(jObj, "definitions");
		rating = getString(jObj, "rating");
		title = getString(jObj, "title");
		originalTitle = getString(jObj, "originalTitle");
		contentImage = getString(jObj, "contentImageName");
		languages = getString(jObj, "languages");
		channelCallLetter = getString(jObj, "channelCallLetter");
		channelName = getString(jObj, "channelName");
		channelFreePreviewIndicator = getBoolean(jObj, "channelFreePreviewIndicator");
		freeContentIndicator = getBoolean(jObj, "freeContentIndicator");
		payPerViewContentIndicator = getBoolean(jObj, "payPreviewContentIndicator");
		clubContentIndicator = getBoolean(jObj, "clubContentIndicator");
	}
}

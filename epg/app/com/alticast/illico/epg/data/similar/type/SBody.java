/**
 * 
 */
package com.alticast.illico.epg.data.similar.type;

import org.json.simple.JSONArray;

/**
 * @author zestyman
 *
 */
public class SBody extends BaseJson {
	
	private SimilarContent[] contents;
	
	public SimilarContent[] getSimilarContents() {
		return contents;
	}

	public void parseJson(Object obj) throws Exception {
		if (obj == null) {
			return;
		}
		
		JSONArray jArray = (JSONArray) obj;
		
		if (jArray != null && jArray.size() > 0) {
			contents = new SimilarContent[jArray.size()];
			for (int i = 0; i < jArray.size(); i++) {
				contents[i] = new SimilarContent();
				
				contents[i].parseJson(jArray.get(i));
			}
		}
	}

}

package com.alticast.illico.epg.data;

import java.util.*;
import javax.tv.locator.LocatorFactory;
import javax.tv.service.Service;
import javax.tv.service.SIManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.*;

public class EmulatedChannel extends Channel {

    private final int id;

    public static Vector getList() {
        Vector vector = new Vector();
        Log.printDebug(App.LOG_HEADER+"EmulatedChannel.getList() :  Read emulated_channel_map.txt");
        String[] s = TextReader.read("./emulated_channel_map.txt");
        Log.printDebug(App.LOG_HEADER+"EmulatedChannel.getList() :  s = "+s);
        if (s != null) {
            for (int i = 0; i < s.length; i++) {
                try {
                    Log.printDebug(App.LOG_HEADER+"EmulatedChannel.getList() : s["+i+"] = "+s[i]);
                    String[] tk = TextUtil.tokenize(s[i], '|');
                    int number = Integer.parseInt(tk[0]);
                    vector.addElement(new EmulatedChannel(Integer.parseInt(tk[0]), tk[1], tk[2]));
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        return vector;
    }

    public EmulatedChannel(int no, String cn, String locStr) {
        super(-no);
        this.number = no;
        this.type = TYPE_NORMAL;
        this.name = cn;
        if (name.startsWith("IND") || name.startsWith("VCC")) {
            this.type = TYPE_PPV;
        } else if (name.startsWith("GAL")) {
            this.type = TYPE_GALAXIE;
        } else if (name.startsWith("RADIO")) {
            this.type = TYPE_RADIO;
        } else if (name.startsWith("VIR")) {
            this.type = TYPE_VIRTUAL;
        }
		//->Kenneth : 4K by June
        if (name.endsWith("HD")) {
            this.definition = 1;
        } else {
            this.definition = 0;
        }
        this.id = -number;
        this.service = createService(locStr);
    }

    public int getId() {
        return id;
    }

    private static Service createService(String locStr) {
        LocatorFactory lf = LocatorFactory.getInstance();
        Service s = null;
        try {
            s = SIManager.createInstance().getService(lf.createLocator(locStr));
        } catch (Exception ex) {
            Log.print(ex);
        }
        return s;
    }


}

package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.framework.DataCenter;

public class NullProgram extends Program {

    static DataCenter dataCenter = DataCenter.getInstance();

    public NullProgram(String channelName) {
        this.callLetter = channelName;
        this.startTime = EpgDataManager.firstTime;
        this.endTime = Math.max(EpgDataManager.storedEndTime, startTime + EpgDataManager.DEFAULT_DURATION);
    }

    public NullProgram(String channelName, long from, long to) {
        this.callLetter = channelName;
        this.startTime = from;
        this.endTime = to;
    }

    public int getType() {
        return TYPE_UNKNOWN;
    }

	//->Kenneth : 4K by June
    public int getDefinition() {
        return DEFINITION_SD;
    }

    public boolean isCaptioned() {
        return false;
    }

    public boolean isLive() {
        return false;
    }

    public boolean isInteractive() {
        return false;
    }

    public boolean isSapEnabled() {
        return false;
    }

    public int getStarRating() {
        return 0;
    }

    /**
     * Gets a language of this Program.
     * @return language.
     */
    public String getLanguage() {
        return Constants.LANGUAGE_ENGLISH;
    }

    public String getTitle() {
        return dataCenter.getString("epg.no_data_title");
    }

    /**
     * Gets an unique identifier of Program.
     * @return program id.
     */
    public String getId() {
        return callLetter + startTime;
    }

    /**
     * Gets an audio type of Program.
     * @return audio type.
     */
    public int getAudioType() {
        return AUDIO_UNKNOWN;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getCategory() {
        return 0;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getSubCategory() {
        return 0;
    }

    public int getRatingIndex() {
        return 0;
    }

    public String getProductionYear() {
        return "";
    }

    public String getCountry() {
        return "";
    }

    public String getFullDescription() {
        Channel ch = getEpgChannel();
        if (ch != null) {
            return ch.getDescription();
        }
        return "";
    }

    public String getDirector() {
        return "";
    }

    public String getActors() {
        return "";
    }

    public String getSeriesId() {
        return "";
    }

    public String getEpisodeTitle() {
        return "";
    }

    public long getPpvEid() {
        return 0L;
    }

}

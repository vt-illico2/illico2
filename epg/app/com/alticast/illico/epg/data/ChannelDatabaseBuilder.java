package com.alticast.illico.epg.data;

import javax.tv.service.SIManager;
import javax.tv.service.transport.Transport;
import javax.tv.service.transport.ServiceDetailsChangeListener;
import javax.tv.service.transport.ServiceDetailsChangeEvent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.upp.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.util.*;
import javax.tv.util.*;
import java.util.Vector;
import java.util.Date;

/**
 * This class builds the channel database, and monitors the updates of channel
 * line up from head-end.
 *
 * @author June Park
 */
public final class ChannelDatabaseBuilder implements ServiceDetailsChangeListener,
                                                TVTimerWentOffListener, DataUpdateListener {

    // Channel DB update를 유발한 reason
    public static final short BOOT                           = 0;
    public static final short CHANGED_CHANNEL_INFO           = 1;
    public static final short CHANGED_VIRTUAL_CHANNEL_MAP    = 2;
    public static final short CHANGED_SDV_SAM                = 3;
    public static final short CHANGED_HUB_ID                 = 4;
    public static final short CHANGED_SERVICE_DETAILS        = 5;
    public static final short CHANGED_SDV_PERMISSION         = 6;
    public static final short CHANGED_TECH_PERMISSION        = 7;

    private static final String[] REASON_STRINGS = {
        "BOOT",
        "CHANNEL_INFO",
        "VIRTUAL_CHANNEL_MAP",
        "SDV_SAM",
        "HUB_ID",
        "SERVICE_DETAILS",
        "SDV_PERMISSION",
        "TECH_PERMISSION"
    };

    private static ChannelDatabaseBuilder instance = new ChannelDatabaseBuilder();
    private Vector listeners = new Vector(4);

    // 최근에 build한 DB
    private ChannelDatabase db;
    // Channel db가 변경되면 많은 채널에서 ServiceDetailsChangeListener가 발생한다.
    // 따라서 1초 타이머를 이용해서 build를 한번만 또는 최소한으로 하도록 한다.
    private TVTimerSpec updateTimer;
    // build 하는 background thread
    private SingleThreadRunner runner;

    // MS/OC configurations
    private ChannelInfoData channelInfo;
    private VirtualChannelMap virtualChannelInfo;
    private SdvSam sdvSam;

    private long lastBuildTime;
    private short reason = BOOT;
    private int version = 0;

    private int tsLen = 0;

    private static final long UPDATE_WAIT_TIME = 1000L;

    public static ChannelDatabaseBuilder getInstance() {
        return instance;
    }

    private ChannelDatabaseBuilder() {
        Runnable r = new Runnable() {
            public void run() {
                build();
            }
        };
        runner = new SingleThreadRunner(r, "ChannelDatabaseBuilder.build");

        updateTimer = new TVTimerSpec();
        updateTimer.setDelayTime(UPDATE_WAIT_TIME);
        updateTimer.setRepeat(false);
        updateTimer.addTVTimerWentOffListener(this);

        Transport[] transports = SIManager.createInstance().getTransports();
        if (transports != null || transports.length > 0) {
            tsLen = transports.length;
            Log.printWarning("ChannelDatabaseBuilder : transports = " + transports.length);
            for (int i = 0; i < tsLen; i++) {
                transports[i].addServiceDetailsChangeListener(this);
            }
        } else {
            if (Log.WARNING_ON) {
                Log.printWarning("cannot get transport from SIManager.");
            }
        }
        DataCenter dc = DataCenter.getInstance();
        dc.addDataUpdateListener(ConfigManager.CHANNEL_INFO_INSTANCE, this);
        dc.addDataUpdateListener(ConfigManager.VIRTUAL_CHANNEL_MAP_INSTANCE, this);
        dc.addDataUpdateListener(ConfigManager.SDV_SAM_INSTANCE, this);
        dc.addDataUpdateListener(NetManager.HUB_ID_KEY, this);
        channelInfo = (ChannelInfoData) dc.get(ConfigManager.CHANNEL_INFO_INSTANCE);
        virtualChannelInfo = (VirtualChannelMap) dc.get(ConfigManager.VIRTUAL_CHANNEL_MAP_INSTANCE);
        sdvSam = (SdvSam) dc.get(ConfigManager.SDV_SAM_INSTANCE);
    }

    public synchronized ChannelDatabase build() {
        if (Log.INFO_ON) {
            Log.printInfo("ChannelDatabase.build start");
        }
        ChannelDatabase database = new ChannelDatabase(channelInfo, virtualChannelInfo, sdvSam);
        this.db = database;
        ChannelDatabase.current = db;
        lastBuildTime = System.currentTimeMillis();
        version++;
        DataCenter.getInstance().put(ChannelDatabase.DATA_KEY, database);
        if (Log.INFO_ON) {
            Log.printInfo("ChannelDatabase.build end : " + this);
            Log.printInfo("ChannelDatabase.size = " + database.size());
        }
        CaManager.getInstance().checkAllChannels(false);
        return database;
    }

    public void buildLater(short reason) {
        this.reason = reason;
        runner.start();
    }

    /** Returns the ChannelDatabase */
    public ChannelDatabase getDatabase() {
        return db;
    }

    public void notifyChange(ServiceDetailsChangeEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ServiceDetailsChangeEvent : " + event);
            Log.printDebug("  details : " + event.getServiceDetails().getLongName());
            Log.printDebug("  name : " + event.getServiceDetails().getService().getName());
        }
        startTimer();
    }

    private void startTimer() {
        synchronized (updateTimer) {
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(updateTimer);
            try {
                timer.scheduleTimerSpec(updateTimer);
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        buildLater(CHANGED_SERVICE_DETAILS);
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (ConfigManager.CHANNEL_INFO_INSTANCE.equals(key)) {
            channelInfo = (ChannelInfoData) value;
            buildLater(CHANGED_CHANNEL_INFO);

        } else if (ConfigManager.VIRTUAL_CHANNEL_MAP_INSTANCE.equals(key)) {
            virtualChannelInfo = (VirtualChannelMap) value;
            buildLater(CHANGED_VIRTUAL_CHANNEL_MAP);

        } else if (ConfigManager.SDV_SAM_INSTANCE.equals(key)) {
            sdvSam = (SdvSam) value;
            buildLater(CHANGED_SDV_SAM);

        } else if (NetManager.HUB_ID_KEY.equals(key)) {
            buildLater(CHANGED_HUB_ID);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelDatabaseBuilder : dataUpdated = " + REASON_STRINGS[reason]);
        }
    }

    public void dataRemoved(String key)  {
    }

    public String toString() {
        return "ChannelDatabaseBuilder[ver=" + version + " ; " + tsLen
                                + ", reason=" + REASON_STRINGS[reason]
                                + ", built=" + new Date(lastBuildTime) + "]";
    }

}

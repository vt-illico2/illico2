package com.alticast.illico.epg.data;

import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.sdv.SdvDiagnostics;
import com.alticast.illico.epg.sdv.SDVController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import java.util.Hashtable;
import java.util.HashSet;
import java.util.Vector;
import java.rmi.RemoteException;
import java.util.Enumeration;
import javax.tv.service.SIManager;
import javax.tv.service.Service;
import javax.tv.service.navigation.ServiceList;
import javax.tv.service.navigation.ServiceFilter;
import org.ocap.service.AbstractService;
import org.ocap.net.OcapLocator;
import javax.tv.locator.Locator;

public class ChannelDatabase extends ServiceFilter {
    public static ChannelDatabase current;

    public static final String DATA_KEY = "ChannelDatabase";

    private static FavoriteFilter ff = new FavoriteFilter(PreferenceNames.FAVOURITE_CHANNEL);
    public static ChannelFilter favoriteFilter = ff;
    public static ChannelFilter blockedFilter = new UpFilter(RightFilter.CHANNEL_RESTRICTION);
    public static ChannelFilter subscribedFilter = new SubscribedFilter();
//    public static ChannelComparator favoriteComparator = ff;

    public static Hashtable filterTable = new Hashtable();

    static {
        filterTable.put(Rs.MENU_FILTER_FAVOURITES.getKey(), favoriteFilter);

        for (int i = 0; i < Rs.MENU_FILTER_GENRE.length; i++) {
            String key = Rs.MENU_FILTER_GENRE[i].getKey();
            filterTable.put(key, new GenreFilter(key));
        }
        filterTable.put(Rs.MENU_FILTER_TYPE_ALL.getKey(), new AllFilter());
        filterTable.put(Rs.MENU_FILTER_TYPE_PPV.getKey(), new TypeFilter(Channel.TYPE_PPV));
        filterTable.put(Rs.MENU_FILTER_TYPE_SDV.getKey(), new TypeFilter(Channel.TYPE_SDV));
//        filterTable.put(Rs.MENU_FILTER_TYPE_SPORTSMAX.getKey(), new TypeFilter(Channel.TYPE_SPORTSMAX));
//        filterTable.put(Rs.MENU_FILTER_TYPE_GAL_RADIO.getKey(), new TypeFilter(Channel.TYPE_GALAXIE, Channel.TYPE_RADIO));

//        filterTable.put(Rs.MENU_FILTER_FRENCH.getKey(), new LanguageFilter(Constants.LANGUAGE_FRENCH));
        filterTable.put(Rs.MENU_FILTER_ENGLISH.getKey(), new LanguageFilter(Constants.LANGUAGE_ENGLISH));
        filterTable.put(Rs.MENU_FILTER_FRENCH.getKey(), new LanguageFilter(Constants.LANGUAGE_FRENCH));
        filterTable.put(Rs.MENU_FILTER_INTERNATIONAL.getKey(), new LanguageFilter(Constants.LANGUAGE_INTERNATIONAL));
//        filterTable.put(Rs.MENU_FILTER_MY_LANGUAGE.getKey(), new MyLanguageFilter());

        filterTable.put(Rs.MENU_FILTER_HD.getKey(), new HdFilter());
//        filterTable.put(Rs.MENU_FILTER_SD.getKey(), new SdFilter());
        filterTable.put(Rs.MENU_FILTER_SUBSCRIBED.getKey(), subscribedFilter);
//        filterTable.put(Rs.MENU_FILTER_UNSUBSCRIBED.getKey(), new UnsubscribedFilter());
//        filterTable.put(Rs.MENU_FILTER_CUSTOM.getKey(), new CustomChannelFilter());
    }

    /** Contains the all invalid channels. */
    private ChannelList invalidChannels;

    /** Contains the all channels including virtual channels. */
    private ChannelList allChannels;
    /** favorite Channels. */
    private ChannelList favoriteChannels;
//    /** subscribed Channels. */
//    private ChannelList subscribedChannels;

    /** Contains the all channel list. Key will be sourceId as an Integer and value will be a Channel. */
    private Hashtable idMap;     // id, channel
    private Hashtable numberMap; // number, channel
    private Hashtable nameMap;   // name, Channel

    private Channel dataChannel;

    public final boolean loadedAll;

    public ChannelDatabase(ChannelInfoData infoData, VirtualChannelMap vcInfoData, SdvSam sdvSam) {
        // for emulator
        if (App.EMULATOR) {
            Log.printDebug(App.LOG_HEADER+"ChannelDatabase: It's EMULATOR, so use EmulatedChannel");
            Vector surfableList = new Vector();
            idMap = new Hashtable();
            numberMap = new Hashtable();
            nameMap = new Hashtable();
            EmulatedChannel c = null;
            Vector emList = EmulatedChannel.getList();
            int size = emList.size();
            for (int i = 0; i < size; i++){
                c = (EmulatedChannel) emList.elementAt(i);
                surfableList.add(c);
                putChannelMap(c);
                if (infoData != null) {
                    c.setChannelInfo((ChannelInfo) infoData.channelTable.get(c.getName()));
                }
            }

            VirtualChannel vc = new VirtualChannel(c.getService(), "VOD", 900);
            vc.appName = "VOD";
            surfableList.add(vc);
            putChannelMap(vc);

            // check sister channel
            if (infoData != null) {
                for (int i = 0; i < infoData.hdName.length; i++) {
                    Channel hd = getChannelByName(infoData.hdName[i]);
                    Channel sd = getChannelByName(infoData.sdName[i]);
                    if (hd != null && sd != null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(App.LOG_HEADER+"ChannelDatabase: sister = " + hd + ", " + sd);
                        }
                        hd.sister = sd;
                        sd.sister = hd;
                    }
                }
            }

            allChannels = new ChannelList(surfableList);
            storeChannels(allChannels);
            favoriteChannels = allChannels.filter(favoriteFilter);
            loadedAll = true;
            return;
        }

        // 1. build service list
        SIManager siManager = SIManager.createInstance();
        ServiceList serviceList = null;
        while (true) {
            serviceList = siManager.filterServices(this);
            if (serviceList != null && serviceList.size() > 0) {
                break;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"ChannelDatabase: no service. wait 3 sec.");
            }
            try {
                Thread.sleep(3000L);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        try {
            serviceList = serviceList.sortByNumber();
        } catch (Exception ex) {
            Log.print(ex);
        }

        CaManager caManager = CaManager.getInstance();

        int sid = DataCenter.getInstance().getInt("DATA_CHANNEL_SOURCE_ID");

        int realSize = serviceList.size();
        Vector tempList = new Vector(realSize * 2);

        // SDV channels
        int sdvTotal = 0;
        int sdvCount = 0;
        int hubId = NetManager.getInstance().getHubId();
        int sdvSize = 0;
        if (sdvSam != null && sdvSam.channels != null && hubId > 0) {
            sdvSize = sdvSam.channels.length;
            for (int i = 0; i < sdvSize; i++) {
                SdvChannelInfo si = sdvSam.channels[i];
                if (hubId == si.hubId) {
                    sdvTotal++;
                    SdvChannel sc = new SdvChannel(si.callLetter, si.number, si.sourceId);
                    tempList.addElement(sc);
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"ChannelDatabase: found " + sdvSize + " SDV channels.");
            }
        }
// NOTE - SDV 테스트 용... - june 반드시 제거
//if (Log.EXTRA_ON && App.VERSION.indexOf("beta") != -1) {
//    SdvChannel sc = new SdvChannel("VANEN", 1000, 99999);
//    tempList.addElement(sc);
//}

        // real channels
        for (int i = 0; i < realSize; i++) {
            Service service = serviceList.getService(i);
            RealChannel rc = new RealChannel(service);
            tempList.addElement(rc);
            if (rc.getSourceId() == sid) {
                dataChannel = rc;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelDatabase: found " + realSize + " real channels.");
        }

        if (dataChannel == null) {
            dataChannel = (Channel) tempList.elementAt(0);
        }

        int virtualSize = 0;
        // virtual channels
        if (vcInfoData != null) {
            Service dataService = dataChannel.getService();
            virtualSize = vcInfoData.channels.length;
            for (int i = 0; i < virtualSize; i++) {
                VirtualChannelInfo vi = vcInfoData.channels[i];
                VirtualChannel vc = new VirtualChannel(dataService, vi.callLetter, vi.number);
                tempList.addElement(vc);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"ChannelDatabase: found " + virtualSize + " virtual channels.");
            }
        }

        // build channel rings
        int tempSize = tempList.size();
        Vector surfableList = new Vector(tempSize);
        idMap = new Hashtable(tempSize);
        numberMap = new Hashtable(tempSize);
        nameMap = new Hashtable(tempSize);

        Vector invalidList = new Vector(tempSize);

        Enumeration en = tempList.elements();
        while (en.hasMoreElements()) {
            Channel ch = (Channel) en.nextElement();
            boolean valid = false;
            if (numberMap.get(new Integer(ch.getNumber())) != null) {
                if (Log.INFO_ON) {
                    Log.printInfo(App.LOG_HEADER+"ChannelDatabase: duplicated channel number = " + ch);
                }
// 임시로 이 조건은 풀어둔다.
//            } else if (nameMap.get(ch.getName()) != null) {
//                if (Log.INFO_ON) {
//                    Log.printInfo("ChannelDatabase: duplicated call letter = " + ch);
//                }
//            } else if (idMap.get(new Integer(ch.getId())) != null) {
//                if (Log.INFO_ON) {
//                    Log.printInfo("ChannelDatabase: duplicated id = " + ch);
//                }
            } else {
                if (ch instanceof SdvChannel) {
                    sdvCount++;
                }
                if (infoData != null) {
                    ch.setChannelInfo((ChannelInfo) infoData.channelTable.get(ch.getName()));
                }
                if (ch.isSurfableType()) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"ChannelDatabase: OK = " + ch);
                    }
                    surfableList.add(ch);
                    putChannelMap(ch);
                    valid = true;
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"ChannelDatabase: not surfable type = " + ch);
                    }
                }
            }
            if (!valid) {
                invalidList.add(ch);
            }
        }

        // check sister channel
        if (infoData != null) {
            for (int i = 0; i < infoData.hdName.length; i++) {
                Channel hd = getChannelByName(infoData.hdName[i]);
                Channel sd = getChannelByName(infoData.sdName[i]);
                if (hd != null && sd != null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"ChannelDatabase: sister = " + hd + ", " + sd);
                    }
                    hd.sister = sd;
                    sd.sister = hd;
                }
            }
        }

        SdvDiagnostics.set(SdvDiagnostics.SDV_CHANNELS, sdvCount + "/" + sdvTotal);

        allChannels = new ChannelList(surfableList);
        storeChannels(allChannels);

        Channel maxChannel = allChannels.getLastChannel();
        if (maxChannel != null && maxChannel.getNumber() >= 1000) {
            ChannelZapper.digit = 4;
        } else {
            ChannelZapper.digit = 3;
        }
        invalidChannels = new ChannelList(invalidList);

        favoriteChannels = allChannels.filter(favoriteFilter);

        loadedAll = realSize > 0 && sdvSize > 0 && virtualSize > 0;
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelDatabase: allChannels.size = " + allChannels.size() + " : loadedAll = " + loadedAll);
        }
    }

/*
    private void _setChannelInfo(ChannelInfoData infoData) {
        if (infoData == null || infoData.channelTable == null) {
            return;
        }
        Hashtable table = infoData.channelTable;
        for (int i = 0; i < allChannels.size(); i++) {
            Channel ch = allChannels.getChannelAt(i);
            ChannelInfo info = (ChannelInfo) table.get(ch.getName());
            if (info != null) {
                ch.setChannelInfo(info);
            }
            ch.sister = null;
        }
        for (int i = 0; i < infoData.hdName.length; i++) {
            Channel hd = getChannelByName(infoData.hdName[i]);
            Channel sd = getChannelByName(infoData.sdName[i]);
            if (hd != null && sd != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("ChannelDatabase: sister = " + hd + ", " + sd);
                }
                hd.sister = sd;
                sd.sister = hd;
            }
        }
    }
*/

    private void storeChannels(ChannelList list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelDatabase.storeChannel()");
        int size = list.size();
        //->zestyman[2015.03.24] R5
        Object[][] array = new Object[size][9];
        for (int i = 0; i < size; i++) {
            Channel ch = list.getChannelAt(i);
            array[i][0] = new Integer(ch.getNumber());
            array[i][1] = ch.getName();
            array[i][2] = ch.getLogo();
            array[i][3] = ch.isRecordable() ? Boolean.TRUE : Boolean.FALSE;
            array[i][4] = new Integer(ch.getType());
            array[i][5] = new Integer(ch.getId());
            //->zestyman[2015.03.24] R5
            array[i][6] = ch.getFullName();
            //->zestyman[2015.10.05] R5
            // VDTRMASTER-5670 & VDTRMASTER-5680
            if (ch.getSisterChannel() != null) {
            	try {
					array[i][7] = ch.getSisterChannel().getCallLetter();
				} catch (RemoteException e) {
					Log.print(e);
				}
            } else {
            	array[i][7] = null;
            }
            // ->
            array[i][8] = new Integer(ch.getDefinition());
        }
        SharedMemory.getInstance().put(EpgService.DATA_KEY_CHANNELS, array);
    }

    private void putChannelMap(Channel c) {
        idMap.put(new Integer(c.getId()), c);
        numberMap.put(new Integer(c.getNumber()), c);
        nameMap.put(c.getName(), c);
    }

    public Channel getDataChannel() {
        return dataChannel;
    }

    public ChannelList getAllChannelList() {
        return allChannels;
    }

    public ChannelList getFavoriteChannelList() {
        return favoriteChannels;
    }

    public ChannelList getInvalidChannelList() {
        return invalidChannels;
    }

    public ChannelList filter(ChannelFilter filter) {
        return allChannels.filter(filter);
    }

    public ChannelList filter(String filterName) {
        ChannelFilter filter = getFilter(filterName);
        if (filter == null) {
            return new ChannelList();
        }
        return allChannels.filter(filter);
    }

    public static ChannelFilter getFilter(String filterName) {
        return (ChannelFilter) filterTable.get(filterName);
    }

    public Channel getChannelById(int id) {
        return (Channel) idMap.get(new Integer(id));
    }

    public Channel getChannelByNumber(int number) {
        return (Channel) numberMap.get(new Integer(number));
    }

    public Channel getChannelByName(String name) {
        return (Channel) nameMap.get(name);
    }

    // ServiceFilter
    public boolean accept(Service service) {
        return !(service instanceof AbstractService);
    }

    void resetFavorite() {
        favoriteChannels = allChannels.filter(favoriteFilter);
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController.get(i).resetFavorite(favoriteChannels);
        }
    }

    public int size() {
        return allChannels.size();
    }

    public Channel find(Service s) {
        if (s == null) {
            return null;
        }
        Locator loc = s.getLocator();
        if (loc instanceof OcapLocator) {
            OcapLocator ol = (OcapLocator) loc;
            int sid = ol.getSourceID();
            if (sid != -1) {
                return getChannelById(sid);
            }
        }
        return null;
    }

}

class AllFilter implements ChannelFilter {
    public boolean accept(Channel channel) {
        return true;
    }
}

class TypeFilter implements ChannelFilter {
    int type1;
    int type2;

    public TypeFilter(int chType) {
        type1 = chType;
        type2 = chType;
    }

    public TypeFilter(int t1, int t2) {
        type1 = t1;
        type2 = t2;
    }

    public boolean accept(Channel channel) {
        int chType = channel.getType();
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
        return type1 == chType || type2 == chType;
    }
}

class GenreFilter implements ChannelFilter {
    String menuKey;

    public GenreFilter(String key) {
        menuKey = key;
    }

    public boolean accept(Channel channel) {
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
        return menuKey.endsWith("genre." + channel.getGenre());
    }
}

//class MyLanguageFilter implements ChannelFilter {
//    public boolean accept(Channel channel) {
//        return FrameworkMain.getInstance().getCurrentLanguage().equals(channel.getLanguage());
//    }
//}


class UpFilter implements ChannelFilter, DataUpdateListener {

    String upName;
    HashSet channelSet;
    Vector channelVector;

    public UpFilter(String name) {
        upName = name;
        DataCenter dataCenter = DataCenter.getInstance();
        dataCenter.addDataUpdateListener(name, this);
        init(dataCenter.getString(name));
    }

    private synchronized void init(String line) {
        HashSet set = new HashSet();
        Vector vector = new Vector();
        if (line != null) {
            String[] tokens = TextUtil.tokenize(line, PreferenceService.PREFERENCE_DELIMETER);
            for (int i = 0; i < tokens.length; i++) {
                set.add(tokens[i]);
                vector.addElement(tokens[i]);
            }
        }
        channelSet = set;
        channelVector = vector;
    }

    public boolean accept(Channel channel) {
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
        return channelSet.contains(channel.getName());
    }

    public void dataUpdated(String key, Object old, Object value) {
        init((String) value);
    }

    public void dataRemoved(String key) {
        init(null);
    }
}

//class FavoriteFilter extends UpFilter implements ChannelComparator {
class FavoriteFilter extends UpFilter {
    public FavoriteFilter(String name) {
        super(name);
    }

//    public int compare(Channel a, Channel b) {
//        int diff = channelVector.indexOf(a.getName()) - channelVector.indexOf(b.getName());
//        if (diff == 0) {
//            return a.getNumber() - b.getNumber();
//        } else {
//            return diff;
//        }
//    }

    public void dataUpdated(String key, Object old, Object value) {
        if (value.equals(old)) {
            // UP 변경시 DataCenter에도 넣고, UP에 의한 update도 불리기에 수정.
            return;
        }
        super.dataUpdated(key, old, value);
        if (ChannelDatabase.current != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"ChannelDatabase.FavoriteFilter.dataUpdated() : Call resetFavorite()");
            }
            ChannelDatabase.current.resetFavorite();
        }
    }
}


class LanguageFilter implements ChannelFilter {
    String language;
    public LanguageFilter(String languageCode) {
        language = languageCode;
    }
    public boolean accept(Channel channel) {
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
        return language.equals(channel.getLanguage());
    }
}

class HdFilter implements ChannelFilter {
    public boolean accept(Channel channel) {
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
		//->Kenneth : 4K by June
        return channel.getDefinition() > Constants.DEFINITION_SD;
    }
}

class SdFilter implements ChannelFilter {
    public boolean accept(Channel channel) {
        //->Kenneth[2017.1.10] R7.3 TECH 는 제외
        if (channel.getType() == Channel.TYPE_TECH) return false;
        //<-
		//->Kenneth : 4K by June
        return channel.getDefinition() == Constants.DEFINITION_SD;
    }
}

class SubscribedFilter implements ChannelFilter {
    CaManager ca = CaManager.getInstance();
    public boolean accept(Channel channel) {
        switch (channel.getType()) {
            case Channel.TYPE_PPV:
            case Channel.TYPE_VIRTUAL:
                return true;
            case Channel.TYPE_SDV:
                return SDVController.hasSdvPackage && channel.isAuthorized();
            default:
                return channel.isAuthorized();
        }
    }
}


//class UnsubscribedFilter implements ChannelFilter {
//    CaManager ca = CaManager.getInstance();
//    public boolean accept(Channel channel) {
//        return !ca.isAuthorized(channel);
//    }
//}

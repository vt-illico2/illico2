package com.alticast.illico.epg.data;

import java.awt.Image;
import java.util.Date;
import java.util.Calendar;
import java.util.Hashtable;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.Constants;

public class PpvProgram extends EpgProgram {

    protected static Hashtable indicatorPool = new Hashtable();

    public static final String INDICATOR_IND = "IND";
    public static final String INDICATOR_VCC = "VCC";

    public final long eid;
    public final float price;
    public final String indicator;

    public final String packageId;
    public final boolean rightToCopy;

    public final long freeCastStartTime;
    public final long freeCastEndTime;

    public final long buyWindowStartTime;
    public final long buyWindowEndTime;

    public final long cancelWindowEndTime;

    public final long adWindowStartTime;
    public final long adWindowEndTime;

    public final long marketingWindowStartTime;
    public final long marketingWindowEndTime;

    PpvProgram(EpgProgram e, String line, char[] c, int l, int p) {
        infoMap = e.infoMap;
        callLetter = e.callLetter;
        startTime = e.startTime;
        endTime = e.endTime;
        id = e.id;
        title = e.title;
//        languageId = e.languageId;
        seriesId = e.seriesId;
        episodeTitle = e.episodeTitle;
        year = e.year;
        country = e.country;
        description = e.description;
        actors = e.actors;

        long v = 0;
        try {
            v = Long.parseLong(getString(c, l, p - l));
        } catch (Exception ex) {
        }
        eid = v;
        l = p + 1;

        p = line.indexOf(DELIM, l);
        price = getFloat(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        String str = getString(c, l, p - l);
        Object o = indicatorPool.get(str);
        if (o != null) {
            indicator = (String) o;
        } else {
            indicatorPool.put(str, str);
            indicator = str;
        }
        l = p + 1;

        p = line.indexOf(DELIM, l);
        packageId = getString(c, l, p - l);
        l = p + 1;  // fixed 12 length

        p = line.indexOf(DELIM, l);
        rightToCopy = c[l] != '0';
        l = p + 1;  // fixed 1 length

        p = line.indexOf(DELIM, l);
        freeCastStartTime = getGmtTime(c, l, p - l);
        l = p + 1;  // fixed 14 length

        p = line.indexOf(DELIM, l);
        freeCastEndTime = freeCastStartTime + getSecond(c, l, p - l);
        l = p + 1;  // fixed 6 length

        p = line.indexOf(DELIM, l);
        buyWindowStartTime = getGmtTime(c, l, p - l);
        l = p + 1;  // fixed 14 length

        p = line.indexOf(DELIM, l);
        buyWindowEndTime = buyWindowStartTime + getSecond(c, l, p - l);
        l = p + 1;  // fixed 6 length

        p = line.indexOf(DELIM, l);
        cancelWindowEndTime = startTime + getSecond(c, l, p - l);
        l = p + 1;  // fixed 6 length

        p = line.indexOf(DELIM, l);
        adWindowStartTime = getGmtTime(c, l, p - l);
        l = p + 1;  // fixed 14 length

        p = line.indexOf(DELIM, l);
        adWindowEndTime = adWindowStartTime + getSecond(c, l, p - l);
        l = p + 1;  // fixed 6 length

        p = line.indexOf(DELIM, l);
        marketingWindowStartTime = getGmtTime(c, l, p - l);
        l = p + 1;  // fixed 14 length

        p = line.indexOf(DELIM, l);
        int end = p == -1 ? c.length : p;
        marketingWindowEndTime = marketingWindowStartTime + getSecond(c, l, end - l);
        l = p + 1;  // fixed 6 length
    }

    private static float getFloat(char[] c, int offset, int len) {
        int major = 0;
        float minor = 0.0f;
        int divider = 0;
        for (int i = offset; i < offset + len; i++) {
            if (c[i] == '.' || c[i] == ',') {
                divider = 10;
            } else if (divider == 0) {
                major = major * 10 + (c[i] - '0');
            } else {
                minor = minor + ((float) (c[i] - '0')) / divider;
                divider = divider * 10;
            }
        }
        return minor + major;
    }

    protected static long getSecond(char[] c, int offset, int len) {
        int val = 0;
        int mp = 1;
        for (int i = offset + len - 1; i >= offset; i--) {
            val = val + (c[i] - '0') * mp;
            mp = mp * 10;
        }
        return val * 1000L;
    }

    public long getPpvEid() {
        return eid;
    }

}

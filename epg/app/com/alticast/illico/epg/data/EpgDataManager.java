package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.epg.ui.GridPrograms;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.config.*;
import java.util.*;
import java.util.zip.*;
import java.io.*;
import javax.tv.util.*;
import java.text.SimpleDateFormat;

import org.dvb.dsmcc.DSMCCObject;
import org.ocap.system.*;
import org.ocap.hardware.*;
import org.ocap.hardware.pod.*;

public class EpgDataManager implements TVTimerWentOffListener, DataUpdateListener {

    /** File encoding. */
    private static final String ENCODING = "8859_1";

    /** 1일 단위 zip file안에는 6시간 단위로 text file이 4개 존재. */
    private static final long DATA_FILE_TIME_GAP = Constants.MS_PER_DAY / 4;
    /** EPG가 받아들일 수 있는 최대 한도. 15일 후 data까지. */
    public static final int MAX_DAYS = 15;
    // MS에서 넘어오는 항목들
    public static int limitDaysNavigation = MAX_DAYS;
    public static int daysNavigation = MAX_DAYS;    // EPG UI에서 제공하는 최대
    public static int daysBootup = MAX_DAYS;        // 부팅시 load할 amount
    public static int daysCannotRemove = 2;         // not using
    public static int purgingMethodeGrid;           // not using
    public static int purgingMethodDescriptions;    // not using

    // HTTP로 가져온 경우 현재 보고있는 window (2시간 기준) 좌우 몇시간만 유지할지.
    public static long FUTURE_BLOC_PERIOD = 4 * Constants.MS_PER_HOUR;
    public static long PAST_BLOC_PERIOD = 4 * Constants.MS_PER_HOUR;

    // EPG UI는 가능하면 과거 1 page도 제공한다.
    private static final long START_TIME_OFFSET = GridPrograms.PAGE_TIME_GAP;
    // 편성이 존재하지 않는 가상 program의 duration. 어차피 임시라서 최대로 제공하면 편하다.
    public static final long DEFAULT_DURATION = MAX_DAYS * Constants.MS_PER_DAY;

    // not using
    private static final int NEVER_PURGE = 1;
    private static final int ASKED_PURGE = 2;
    private static final int AUTO_PURGE  = 3;

    // 이 class가 제공하는 시작지점. Guide가 제공하는 첫페이지 - 1 페이지.
    public static long firstTime = 1234567890123L;
    // 이 class가 제공하는 끝지점. UI에서 최대로 갈수있는 지점까지.
    public static long lastTime = firstTime;
    // 저장된 끝지점
    public static long storedEndTime = 1;
    // File로 저장된 끝지점
    public static long fileEndTime = 1;
    public static long nextUpdateTime = 0;

    private DataWorker worker = new DataWorker("EpgDataManager.Worker");

    static TimeZone curTimeZone;
    static TimeZone gmtTimeZone;

    private final Object TIMER_LOCK = new Character('x');

    private static final String ENTRY_FORMAT = "yyyyMMdd-HHmm";
    private static final String LINE_FORMAT = "yyyyMMddHHmm";

    /** key = String (channel name), value = ProgramList. */
    private Hashtable listTable = new Hashtable(1000);

    /** EPG data 수신상황을 check하기 위한 DataChecker. */
    private DataChecker epgChecker = new DataChecker(EpgDataListener.EPG_DATA);
    /** PPVR data 수신상황을 check하기 위한 DataChecker. */
    private DataChecker ppvChecker = new DataChecker(EpgDataListener.PPV_DATA);
    /** 각 채널별 현재 program을 관리하기 위한 timer. */
    protected TVTimerSpec updateTimer = new TVTimerSpec();

    private Vector listenersIxc = new Vector();
    private Vector listenersInternal = new Vector();

    // HTTP proxy for EPG Server.
    private String[] proxyHost = new String[] { null };
    private int[] proxyPort = new int[1];

    private static EpgDataManager instance = new EpgDataManager();

    public static EpgDataManager getInstance() {
        return instance;
    }

    private EpgDataManager() {
        if (App.EMULATOR) {
            daysNavigation = 5;
        }
        resetFirstTime();
        updateTimer.addTVTimerWentOffListener(this);
        curTimeZone = TimeZone.getDefault();
        gmtTimeZone = createGmtTimeZone();

        EpgProgram.gmtTimeZone = gmtTimeZone;

        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.curTimeZone = " + curTimeZone);
            Log.printDebug("EpgDataManager.gmtTimeZone = " + gmtTimeZone);
        }

        DataCenter dc = DataCenter.getInstance();

        for (int i = 0; i < MAX_DAYS; i++) {
            dc.addDataUpdateListener("EPG_DAY_" + i, this);
            dc.addDataUpdateListener("PPV_DAY_" + i, this);
        }
        dc.addDataUpdateListener(ConfigManager.EPG_CONFIG_INSTANCE, this);
        dc.addDataUpdateListener(ChannelDatabase.DATA_KEY, this);
    }


    public void init() {
        // load application props
        DataCenter dc = DataCenter.getInstance();

        if ("true".equalsIgnoreCase(dc.getString("PROXY_USE"))) {
            proxyHost = TextUtil.tokenize(dc.getString("PROXY_HOST"), "|");
            String[] ports = TextUtil.tokenize(dc.getString("PROXY_PORT"), "|");
            proxyPort = new int[ports.length];
            for (int i = 0; i < ports.length; i++) {
                proxyPort[i] = Integer.parseInt(ports[i]);
            }
        }

        FUTURE_BLOC_PERIOD = dc.getInt("FUTURE_BLOC_PERIOD", 4) * Constants.MS_PER_HOUR;
        PAST_BLOC_PERIOD = dc.getInt("PAST_BLOC_PERIOD", 4) * Constants.MS_PER_HOUR;
        limitDaysNavigation = dc.getInt("LIMIT_DAYS_NAVIGATION", MAX_DAYS * 2);

        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager: limitDaysNavigation = " + limitDaysNavigation + ", " + dc.getInt("LIMIT_DAYS_NAVIGATION"));
        }

        updateConfig(ConfigManager.getInstance().epgConfig);

        if (Log.EXTRA_ON) {
            // 이하 GMT를 위한 테스트 코드
            Log.printDebug("EpgDataManager: DST check = " + new SimpleTimeZone(0, "aaa"));
            try {
                String[] s = TimeZone.getAvailableIDs();
                for (int i = 0; i < s.length; i++) {
                    Log.printDebug("EpgDataManager:" + i + "] TimeZone = " + s[i] + " " + TimeZone.getTimeZone(s[i]));
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            Calendar ccc = Calendar.getInstance();
            TimeZone z1 = ccc.getTimeZone();
            TimeZone z2 = TimeZone.getDefault();
            TimeZone z3 = TimeZone.getDefault();
            Log.printDebug("EpgDataManager: DST check = " + (z3 == z2));
            Log.printDebug("EpgDataManager: DST check = " + z1);
            Log.printDebug("EpgDataManager: DST check = " + z2);

            ccc.setTimeInMillis(System.currentTimeMillis());
            Log.printDebug("EpgDataManager: DST check Hour = " + ccc.get(Calendar.HOUR));
            ccc.setTimeZone(new SimpleTimeZone(0, "bbb"));
            Log.printDebug("EpgDataManager: DST check Hour = " + ccc.get(Calendar.HOUR));

            Calendar cal = Calendar.getInstance();
            Log.printDebug("EpgDataManager: DST check 0 = " + cal.getTimeZone());
            cal.set(Calendar.MONTH, 2);
            cal.set(Calendar.DAY_OF_MONTH, 9);
            cal.set(Calendar.HOUR_OF_DAY, 1);
            long ll = cal.getTimeInMillis();
            Log.printDebug("EpgDataManager: DST check 1 = " + cal.getTime());
            Log.printDebug("EpgDataManager: DST check 1a = " + new Date(ll));
            cal.add(Calendar.HOUR_OF_DAY, 1);
            Log.printDebug("EpgDataManager: DST check 2 = " + cal.getTime());
            Log.printDebug("EpgDataManager: DST check 2a = " + new Date(ll + Constants.MS_PER_HOUR));

            cal.set(Calendar.HOUR_OF_DAY, 2);
            Log.printDebug("EpgDataManager: DST check 3 = " + cal.getTime());

            cal.setTimeInMillis(ll);
            cal.add(Calendar.DAY_OF_MONTH, 1);
            long diff = cal.getTimeInMillis() - ll;
            Log.printDebug("EpgDataManager: DST check diff = " + (diff) + " " + (diff / Constants.MS_PER_HOUR) + "h");



            cal.set(Calendar.MONTH, 10);
            cal.set(Calendar.DAY_OF_MONTH, 7);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            ll = cal.getTimeInMillis();
            Log.printDebug("EpgDataManager: DST check _ date 1 = " + new Date(ll));
            ll = ll + Constants.MS_PER_HOUR;
            Log.printDebug("EpgDataManager: DST check _ date 2 = " + new Date(ll));
            ll = ll + Constants.MS_PER_HOUR;
            Log.printDebug("EpgDataManager: DST check _ date 3 = " + new Date(ll));


            Log.printDebug("EpgDataManager: DST check 3a= " + cal.getTime() + " " + cal.getTime().getTime());
            cal.add(Calendar.HOUR_OF_DAY, 1);
            Log.printDebug("EpgDataManager: DST check 3 = " + cal.getTime() + " " + cal.getTime().getTime());
            cal.setTimeInMillis(cal.getTimeInMillis() + Constants.MS_PER_HOUR);
            Log.printDebug("EpgDataManager: DST check 4 = " + cal.getTime());
        }
    }

    /** firstTime과 lastTime을 재설정. */
    private static void resetFirstTime() {
        long cur = System.currentTimeMillis();
        long gridStartTime = cur - cur % (Constants.MS_PER_HOUR / 2);
        firstTime = gridStartTime - START_TIME_OFFSET;

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(cur);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        cal.add(Calendar.DAY_OF_MONTH, daysNavigation);
        lastTime = cal.getTimeInMillis();
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager: firstTime = " + new Date(firstTime));
            Log.printDebug("EpgDataManager: lastTime = " + cal.getTime());
        }
    }

    private static TimeZone createGmtTimeZone() {
        TimeZone tz = TimeZone.getTimeZone("GMT");
        if (tz != null) {
            return tz;
        }
        tz = TimeZone.getTimeZone("UTC");
        if (tz != null) {
            return tz;
        }
        return new SimpleTimeZone(0, "custom_gmt");
    }

    public ProgramList getPrograms(Channel ch) {
        if (ch == null) {
            return null;
        }
        ProgramList list = (ProgramList) listTable.get(ch.getName());
        switch (ch.getType()) {
            case Channel.TYPE_GALAXIE:
            case Channel.TYPE_RADIO:
                if (list == null || !(list instanceof FakeProgramList)) {
                    list = new FakeProgramList(ch);
                    listTable.put(ch.getName(), list);
                }
                return list;
            default:
                if (list == null) {
                    list = new ProgramList(ch.getName());
                    listTable.put(ch.getName(), list);
                }
                return list;
        }
    }

    public Program getCurrentProgram(Channel ch) {
        ProgramList list = getPrograms(ch);
        if (list != null) {
            if (list.current == null) {
                list.resetCurrent();
            }
            return list.current;
        } else {
            return null;
        }
    }

    public Program getProgram(Channel ch, long time) {
        ProgramList list = getPrograms(ch);
        if (list == null) {
            return null;
        }
        return list.search(time);
    }

    public Vector findPrograms(ChannelFilter cFilter, ProgramFilter pFilter) {
        return findPrograms(ChannelDatabaseBuilder.getInstance().getDatabase().filter(cFilter), pFilter);
    }

    public Vector findPrograms(ChannelList list, ProgramFilter filter) {
        Vector vector = new Vector();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            findPrograms(list.getChannelAt(i), filter, vector);
        }
        return vector;
    }

    private Vector findPrograms(Channel ch, ProgramFilter filter, Vector vector) {
        if (vector == null) {
            vector = new Vector();
        }
        ProgramList list = getPrograms(ch);
        if (list != null) {
            Program p = list.start;
            while (p != null) {
                if (filter.accept(p)) {
                    vector.add(p);
                }
                p = p.next;
            }
        }
        return vector;
    }

    public Vector filterPrograms(Vector list, ProgramFilter filter) {
        Vector vector = new Vector();
        Enumeration en = list.elements();
        while (en.hasMoreElements()) {
            Program p = (Program) en.nextElement();
            if (filter.accept(p)) {
                vector.addElement(p);
            }
        }
        return vector;
    }

    private void startTimer(final long time) {
        synchronized (TIMER_LOCK) {
            TVTimer.getTimer().deschedule(updateTimer);
            updateTimer.setAbsoluteTime(time);
            nextUpdateTime = time;
            try {
                updateTimer = TVTimer.getTimer().scheduleTimerSpec(updateTimer);
            } catch (Throwable ex) {
                Log.print(ex);
                worker.resetCurrent();
            }
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        worker.resetCurrent();
    }

    public void startDownloading() {
        worker.startDownloading();
    }

    /** 부팅 또는 IB update 되고 나서 불림. */
    public void readPrograms() {
        worker.readyToRead();
    }

    private boolean addPrograms(List list) {
        if (list == null) {
            return false;
        }
        long time = System.currentTimeMillis();
        int size = list.size();
        if (Log.INFO_ON) {
            Log.printInfo("EpgDataManager.addPrograms : list.size = " + size);
        }
        if (size == 0) {
            return false;
        }

        boolean success = true;
        Hashtable table = new Hashtable();
        // List<String> to Hashtable<String, List<Program>>
        // epg data line을 하나씩 읽어서 채널별 array list를 만든다.
        String line;
        Program p;
        for (int i = size - 1; i >= 0; i--) {
            line = (String) list.remove(i);
            try {
                p = EpgProgram.create(line);
                if (firstTime < p.endTime) {
                    List programList = (List) table.get(p.callLetter);
                    if (programList == null) {
                        programList = new ArrayList();
                        table.put(p.callLetter, programList);
                    }
                    programList.add(p);
                }
            } catch (Exception ex) {
                Log.print(ex);
                success = false;
            }
        }

        // Hashtable<String, List<Program>> to List<String> and List<ProgramList>
        // 채널별 ProgramList를 만든다
        int tableSize = table.size();
        List channels = new ArrayList(tableSize + 1);
        List programs = new ArrayList(tableSize + 1);
        Enumeration en = table.keys();
        while (en.hasMoreElements()) {
            String callLetter = (String) en.nextElement();
            channels.add(callLetter);
            programs.add(new ProgramList((List) table.get(callLetter)));
        }
        table.clear();
        table = null;

        // List<Program> to ProgramList
        // 기존 ProgramList와 병합
        ProgramList oldList;
        ProgramList newList;
        int channelSize = channels.size();
        // 같은 thread에서 수행되기 때문에 LOCK 제거했음
        for (int i = channelSize - 1; i >= 0; i--) {
            String callLetter = (String) channels.remove(i);
            newList = (ProgramList) programs.remove(i);
            oldList = (ProgramList) listTable.get(callLetter);
            if (oldList == null) {
                listTable.put(callLetter, newList);
                newList.resetStart(firstTime);
            } else {
                oldList.addAll(newList);
            }
        }
        if (Log.INFO_ON) {
            Log.printInfo("EpgDataManager.addPrograms : parsing time = " + (System.currentTimeMillis() - time));
        }
        return success;
    }

    public boolean isValid(long time) {
        return firstTime <= time && time < lastTime;
    }

    public boolean isAvailable(long from, long to) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.isAvailable : " + new Date(from) + ", " + new Date(to));
        }
        boolean checkEpg = epgChecker.contains(from, to);
        boolean checkPpv = ppvChecker.contains(from, to);
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.isAvailable result : " + checkEpg + ", " + checkPpv);
        }
        return (checkEpg && checkPpv);
    }

    public void dumpChecker() {
//        Log.printDebug("EpgDataManager.programs = " + Program.objectCount);
        Log.printDebug("EpgDataManager.daysNavigation   = " + daysNavigation);
        Log.printDebug("EpgDataManager.daysBootup       = " + daysBootup);
        Log.printDebug("EpgDataManager.daysCannotRemove = " + daysCannotRemove);

        epgChecker.dump();
        ppvChecker.dump();
    }

    public void requestHttp(long from, long to) {
        worker.addHttp(from, to);
    }

    public EpgProgram requestProgram(String name, long start) {
        Vector v = requestLines(name, start, start + Constants.MS_PER_MINUTE, false);
        if (v == null || v.size() == 0) {
            return null;
        }
        return EpgProgram.create((String) v.elementAt(0));
    }

    public Vector requestPrograms(String name, String programId, String seriesId, long from) {
        Vector vector = new Vector();
        Vector lines = requestLines(name, programId, seriesId, from);
        if (lines == null || lines.size() == 0) {
            return vector;
        }
        Enumeration en = lines.elements();
        while (en.hasMoreElements()) {
            vector.addElement(EpgProgram.create(en.nextElement().toString()));
        }
        return vector;
    }

    public Vector requestProgramsByTitle(String callLetter, String title, long from) {
        Vector vector = new Vector();
        Vector lines = requestLines(callLetter, from, from + DEFAULT_DURATION, false);
        if (lines == null || lines.size() == 0) {
            return vector;
        }
        Enumeration en = lines.elements();
        while (en.hasMoreElements()) {
            EpgProgram p = EpgProgram.create(en.nextElement().toString());
            if (p.getTitle().equals(title)) {
                vector.addElement(p);
            }
        }
        return vector;
    }

    private Vector requestLines(String name, String programId, String seriesId, long from) {
        SimpleDateFormat curFormatter = new SimpleDateFormat(LINE_FORMAT);

        ArrayList list = new ArrayList();
        Hashtable param = new Hashtable();
        param.put("start_date", curFormatter.format(new Date(from)));
        if (seriesId != null) {
            param.put("series_id", seriesId);
        } else if (programId != null) {
            param.put("program_id", programId);
        } else {
            Log.printWarning("EpgDataManager: no programId/seriesId specified.");
            return null;
        }

        if (name != null) {
            param.put("channel_call_letter", TextUtil.urlEncode(name));
        }
        if (EpgCore.dncsName != null) {
            param.put("dncs", EpgCore.dncsName);
        }

        DataCenter dc = DataCenter.getInstance();
        String url = "http://" + dc.getString("EPG_SERVICE_DNS") + ":" + dc.getString("EPG_SERVICE_PORT")
                        + dc.getString("EPG_CONTEXT_ROOT") + "/" + dc.getString("SERVICE_GET_PROGRAM_DETAILS");
        Vector lines = null;
        try {
            lines = URLRequestor.getLines(proxyHost, proxyPort, url, param);
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }
        if (lines == null || lines.size() == 0) {
            if (Log.WARNING_ON) {
                Log.printWarning("EpgDataManager: No data from HTTP");
            }
            return null;
        }
        Object first = lines.remove(0);
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager: HTTP result : " + first);
            Log.printDebug("EpgDataManager: HTTP lines : " + lines.size());
        }
        return lines;
    }

    private Vector requestLines(String name, long from, long to, boolean showError) {
        SimpleDateFormat curFormatter = new SimpleDateFormat(LINE_FORMAT);

        ArrayList list = new ArrayList();
        Hashtable param = new Hashtable();
        to = Math.min(lastTime, to);
        param.put("start_date", curFormatter.format(new Date(from)));
        param.put("end_date",   curFormatter.format(new Date(to)));
        if (name != null) {
            param.put("channel_call_letters", TextUtil.urlEncode(name));
        }
        if (EpgCore.dncsName != null) {
            param.put("dncs", EpgCore.dncsName);
        }

        DataCenter dc = DataCenter.getInstance();
        String url = "http://" + dc.getString("EPG_SERVICE_DNS") + ":" + dc.getString("EPG_SERVICE_PORT")
                        + dc.getString("EPG_CONTEXT_ROOT") + "/" + dc.getString("SERVICE_GET_GUIDE_DETAILS");
        Vector lines = null;
        try {
            lines = URLRequestor.getLines(proxyHost, proxyPort, url, param);
            //->Kenneth[2016.7.19] R7.2 : On-screen debug
            Log.printDebug("EpgDataManager: DebugScreen.addLog after getLines()");
            DebugScreen.getInstance().addLog(URLRequestor.lastURLStr);
            //<-
        } catch (Exception ex) {
            Log.print(ex);
            if (showError) {
                EpgCore.getInstance().showErrorMessage("EPG801", null);
            }
            //->Kenneth[2016.7.19] R7.2 : On-screen debug
            Log.printDebug("EpgDataManager: DebugScreen.addLog after catching Exception");
            DebugScreen.getInstance().addLog(URLRequestor.lastURLStr);
            //<-
            return null;
        }
        if (lines == null || lines.size() == 0) {
            if (Log.WARNING_ON) {
                Log.printWarning("EpgDataManager: No data from HTTP");
            }
            return null;
        }
        Object first = lines.remove(0);
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager: HTTP result : " + first);
            Log.printDebug("EpgDataManager: HTTP lines : " + lines.size());
        }
        if (showError && first != null) {
            String fs = first.toString();
            String[] tk = TextUtil.tokenize(fs, '|');
            if (tk != null && tk.length > 0) {
                if (!tk[0].equals("EPG000")) {
                    EpgCore.getInstance().showErrorMessage(tk[0], null);
                }
            }
        }
        return lines;
    }


    public Vector findSeries(String callLetter, String seriesId, long from, long to) {
        if (Log.INFO_ON) {
            Log.printInfo("EpgDataManager.findSeries: " + callLetter + ", " + seriesId + ", " + new Date(from));
        }
        Vector vector = new Vector();
        if (seriesId == null) {
            return vector;
        }
        ProgramList list = (ProgramList) listTable.get(callLetter);
        if (list == null) {
            return vector;
        }

        Program p = list.search(from);
        if (p == null) {
            p = list.start;
        }
        while (p != null && p.startTime < to) {
            if (seriesId.equals(p.getSeriesId())) {
                vector.addElement(p);
            }
            p = p.next;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.findSeries: found = " + vector.size());
        }
//        Collections.sort(vector, new SeriesComparator(callLetter));
        return vector;
    }

    public Vector findEpisodes(String callLetter, final String seriesId, final String programId) {
        ProgramFilter filter = new ProgramFilter() {
            public boolean accept(Program p) {
                return programId.equals(p.getId()) && seriesId.equals(p.getSeriesId());
            }
        };
        Vector vector = findPrograms(ChannelDatabase.subscribedFilter, filter);
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.findEpisodes : " + vector.size());
        }
        Collections.sort(vector, new SeriesComparator(callLetter));
        return vector;
    }

//    public Vector findPrograms(String callLetter, String title, long from) {
//        Vector vector = new Vector();
//        ProgramList list = (ProgramList) listTable.get(callLetter);
//        if (list == null) {
//            return vector;
//        }
//        Program p = list.start;
//        while (p != null) {
//            if (p.startTime > from && title.equals(p.getTitle())) {
//                vector.add(p);
//            }
//            p = p.next;
//        }
//        return vector;
//    }

    public Vector findProgramsById(String callLetter, String pid, long from, long to) {
        Vector vector = new Vector();
        ProgramList list = (ProgramList) listTable.get(callLetter);
        if (list == null || pid == null || "".equals(pid)) {
            return vector;
        }
        Program p = list.search(from);
        if (p == null) {
            p = list.start;
        }
        while (p != null && p.startTime < to) {
            if (pid.equals(p.getId())) {
                vector.add(p);
            }
            p = p.next;
        }
        return vector;
    }

    public Vector findProgramsByTitle(String callLetter, String title, long from, long to) {
        Vector vector = new Vector();
        ProgramList list = (ProgramList) listTable.get(callLetter);
        if (list == null || title == null || "".equals(title)) {
            return vector;
        }
        Program p = list.search(from);
        if (p == null) {
            p = list.start;
        }
        while (p != null && p.startTime < to) {
            if (title.equals(p.getTitle())) {
                vector.add(p);
            }
            p = p.next;
        }
        return vector;
    }

    public void addListener(EpgDataListenerInternal l) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.addListener (internal) : " + l);
        }
        synchronized (listenersInternal) {
            if (!listenersInternal.contains(l)) {
                listenersInternal.addElement(l);
            }
        }
    }

    public void removeListener(EpgDataListenerInternal l) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.removeListener (internal) : " + l);
        }
        synchronized (listenersInternal) {
            listenersInternal.removeElement(l);
        }
    }

    public void addListener(EpgDataListener l) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.addListener IXC : " + l);
        }
        synchronized (listenersIxc) {
            if (!listenersIxc.contains(l)) {
                listenersIxc.addElement(l);
            }
        }
    }

    public void removeListener(EpgDataListener l) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.removeListener IXC : " + l);
        }
        synchronized (listenersIxc) {
            listenersIxc.removeElement(l);
        }
    }

    private void notifyListenersIxc(byte type, long from, long to) {
        if (Log.INFO_ON) {
            Log.printInfo("EpgDataManager.notifyListeners (IXC): "
                        + type + ", " + new Date(from) + " - " + new Date(to));
        }

        EpgDataListener[] array;
        int size = 0;
        synchronized (listenersIxc) {
            size = listenersIxc.size();
            if (size > 0) {
                array = new EpgDataListener[size];
                listenersIxc.copyInto(array);
            } else {
                return;
            }
        }

        for (int i = 0; i < size; i++) {
            try {
                array[i].epgDataUpdated(type, from, to);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void notifyListenersInternal(byte type, long from, long to) {
        if (Log.INFO_ON) {
            Log.printInfo("EpgDataManager.notifyListeners (internal): "
                        + type + ", " + new Date(from) + " - " + new Date(to));
        }
        EpgDataListenerInternal[] array;
        int size = 0;
        synchronized (listenersInternal) {
            size = listenersInternal.size();
            if (size > 0) {
                array = new EpgDataListenerInternal[size];
                listenersInternal.copyInto(array);
            } else {
                return;
            }
        }
        for (int i = 0; i < size; i++) {
            try {
                array[i].epgDataUpdated(type, from, to);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    /** update channel descriptions */
    private void updateChannelInfo(ChannelDatabase db) {
        ChannelList list = db.getAllChannelList();
        for (int i = list.size() - 1; i >= 0; i--) {
            Channel ch = list.getChannelAt(i);
            if (ch != null) {
                Object o = listTable.get(ch.getName());
                if (o != null && o instanceof FakeProgramList) {
                    ((FakeProgramList) o).resetChannel(ch);
                }
            }
        }
    }

    public RangeList getEpgChecker() {
        return epgChecker;
    }

    public RangeList getPpvChecker() {
        return ppvChecker;
    }

    public String toString() {
        return "EpgDataManager[Queue=" + worker.size() + ", Channels=" + listTable.size() + "]";
    }

    /////////////////////////////////////////////////////////////////////////
    // DataUpdateListener
    /////////////////////////////////////////////////////////////////////////

    public void dataUpdated(final String key, Object old, final Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgDataManager.dataUpdated("+key+", "+value+")");
        if (key.equals(ConfigManager.EPG_CONFIG_INSTANCE)) {
            // EPG config from MS
            updateConfig((EpgConfig) value);
            return;
        } else if (key.equals(ChannelDatabase.DATA_KEY)) {
            // Channel DB 정보가 변경되면 channel 부가 정보도 변경될 수 있다.
            // Radio 채널들 부가정보에 따라 표시 이름이 달라지는 채널도 있음.
            updateChannelInfo((ChannelDatabase) value);
            return;
        }
        File f = (File) value;
        boolean isPpv;
        if (key.startsWith("EPG_DAY_")) {
            isPpv = false;
        } else if (key.startsWith("PPV_DAY_")) {
            isPpv = true;
        } else {
            return;
        }
        int number = Integer.parseInt(key.substring(8));
        if (number < daysBootup) {
            worker.addZip(f, number, isPpv);
        }
    }

    public void dataRemoved(String key)  {
    }

    /** MS config를 저장. */
    private void updateConfig(EpgConfig config) {
        if (Log.DEBUG_ON) {
            Log.printDebug("EpgDataManager.updateConfig : " + config);
        }
        if (config == null) {
            return;
        }

        daysNavigation = Math.min(config.daysNavigation, limitDaysNavigation);
        DataCenter dc = DataCenter.getInstance();
        dc.put("EPG_SERVICE_DNS", config.dnsName);
        dc.put("EPG_SERVICE_PORT", String.valueOf(config.port));
        dc.put("EPG_CONTEXT_ROOT", config.contextRoot);

        daysBootup = config.daysBootup;
        daysCannotRemove = config.daysCannotRemove;
        purgingMethodeGrid = config.purgingMethodeGrid;
        purgingMethodDescriptions = config.purgingMethodDescriptions;

        proxyHost = config.proxyHosts;
        proxyPort = config.proxyPorts;

        for (int i = 0; proxyHost != null && i < proxyHost.length; i++) {
            Log.printDebug("proxyHost[" + i + "] = " + proxyHost[i]);
        }

        resetFirstTime();
    }

    /** Guide 종료할 때 File 이외의 모든 프로그램 종료. */
    public void epgClosed() {
        worker.epgClosed();
    }

    /** View가 변경되면 HTTP로 가져온 data중 정해진 범위 밖의 것은 제거. */
    public void epgViewChanged(long from, long to) {
        worker.epgViewChanged(from, to);
    }

    /////////////////////////////////////////////////////////////////////////
    // Zip, HTTP, Flush
    /////////////////////////////////////////////////////////////////////////

    /** Zip file안의 text file을 읽어 line별 String으로 임시저장. */
    class ZipSegment extends ArrayList {
        long from, to;

        public ZipSegment(long start, long end) {
            super(4096);
            from = start;
            to = end;
        }
    }

    class DataWorker extends Worker {
        /** boot 완료되면 true로 set. true인 경우 바로 unzip. false일 때는 모았다가 unzip. */
        boolean checkBoot = false;
        /** 모두 load후에 한번에 unzip 하기 위한 flag. */
        boolean ready = true;

        DataWorker(String name) {
            super(name);
        }

        /** Inband load 전에 호출됨. */
        public synchronized void startDownloading() {
            Log.printDebug("EpgDataManager.startDownloading: checkBoot = " + checkBoot);
// NOTE - memory를 아끼기 위해 필요 없는 채널을 모두 없애는 기능을
//        넣어보려고 했지만 사용하지 않기로 해서 comment 처리
//            if (checkBoot) {
//                flushExtraChannels();
//            }
            ready = false;
        }

        /** Zip File이 load되면 호출. */
        public synchronized void addZip(File file, int number, boolean isPpv) {
            String name = file.getName();
            QueueEntry ze = find(file);
            if (ze != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EpgDataManager: found same zip: " + name);
                }
                return;
            }
            // push를 쓰면 바로 notify가 가서 풀려버리기 때문에
            // addElement를 쓰고 ready 이후에 notify 별도 호출.
            addElement(new ZipQueueEntry(file, number, isPpv));
            Collections.sort(this);
            if (ready) {
                DataWorker.this.notify();
            } else {
                Log.printInfo("EpgDataManager: need to wait until downloading");
            }
        }

        /** HTTP로 load가 필요할때 호출. */
        public synchronized void addHttp(long from, long to) {
            if (!epgChecker.contains(from, to) || !ppvChecker.contains(from, to)) {
                // load 되어어있지 않은 경우.
                // 보통 UI에서 들어오는 요청이니 맨 앞으로 넣어준다.
                insertElementAt(new HttpQueueEntry(from, to), 0);
            }
            DataWorker.this.notify();
        }

        /** EPG 닫힐 때. */
        public synchronized void epgClosed() {
            addElement(new EpgViewQueueEntry());
            if (ready) {
                DataWorker.this.notify();
            }
        }

        /** EPG view 이동할 때. */
        public synchronized void epgViewChanged(long from, long to) {
            // 이전 action이 아지 처리되지 않았을때, 뒤늦게 처리되면 문제.
            for (int i = size() - 1; i >= 0; i--) {
                Object o = elementAt(i);
                if (o instanceof EpgViewQueueEntry) {
                    if (((EpgViewQueueEntry) o).to != 0) {
                        removeElementAt(i);
                    }
                }
            }
            addElement(new EpgViewQueueEntry(from, to));
            if (ready) {
                DataWorker.this.notify();
            }
        }

        /** Reset current program. */
        public synchronized void resetCurrent() {
            addElement(new ResetCurrentQueueEntry());
            if (ready) {
                DataWorker.this.notify();
            }
        }

//        public synchronized void flushExtraChannels() {
//            addElement(new FlushExtraChannelsQueueEntry());
//            if (ready) {
//                DataWorker.this.notify();
//            }
//        }

        /** Listener 전달 목적. */
        public synchronized void epgDataUpdated(byte type, long from, long to) {
            // 이전 event 찾아내서 지우고 합쳐서 다시 queue에 밀어넣기
            NotifyListenerQueueEntry target = null;
            for (int i = 0; i < size(); i++) {
                Object o = elementAt(i);
                if (o instanceof NotifyListenerQueueEntry && ((NotifyListenerQueueEntry) o).type == type) {
                    target = (NotifyListenerQueueEntry) o;
                    break;
                }
            }
            if (target != null) {
                removeElement(target);
                Log.printInfo("EpgDataManager: merging = " + target);
            } else {
                target = new NotifyListenerQueueEntry(type);
            }
            target.add(from, to);
            addElement(target);
            if (ready) {
                DataWorker.this.notify();
            }
        }

        /**
         * 전에 OOB, IB 둘중 하나만 load 해보려고 했는데,
         * 지금은 둘 다 load하고 서로 다른 file은 equals가 false를 return.
         */
        private QueueEntry find(File file) {
     	    for (int i = 0 ; i < elementCount; i++) {
                QueueEntry ze = (QueueEntry) elementData[i];
                if (ze.equals(file)) {
                    return ze;
                }
            }
            return null;
        }

        /** IB 수신이 끝나면 load 시작. */
        public synchronized void readyToRead() {
            Log.printInfo("EpgDataManager: readyToRead");
            if (!checkBoot) {
                // 부팅 이전이라면 daysBootup 까지만 unzip
                int days = daysBootup;
                Log.printInfo("EpgDataManager.readyToRead : " + days);
                for (int i = size() - 1; i >= 0; i--) {
                    Object o = elementAt(i);
                    if (o instanceof ZipQueueEntry) {
                        ZipQueueEntry z = (ZipQueueEntry) o;
                        if (z.number >= days) {
                            Log.printInfo("EpgDataManager: removing entry = " + z);
                            removeElementAt(i);
                        }
                    }
                }
            }
            addElement(new DetachQueueEntry());
            DataWorker.this.notify();
            checkBoot = true;
            ready = true;
        }

    }

    /** DataWorker에 add되는 entry. */
    abstract class QueueEntry implements Comparable, Runnable {
        long created;
        public QueueEntry() {
            created = System.currentTimeMillis();
        }
    }

    /**
     * OC files -> sorted list of byte[]
     */
    class ZipQueueEntry extends QueueEntry {
        boolean isPpv;
        int number;
        String name;
        String absolutePath;
        File file;

        public ZipQueueEntry(File file, int n, boolean ppv) {
            this.isPpv = ppv;
            this.file = file;
            number = n;
            name = file.getName();
            absolutePath = file.getAbsolutePath();
        }

        public void run() {
            if (file instanceof DSMCCObject) {
                DSMCCObject obj = (DSMCCObject) file;
                if (!obj.isLoaded()) {
                    // 보통 이 작업은 IB adapter에서 처리되어 넘어온다.
                    // IB load 이후 detach 전에 tuner를 놓는데, 이게 괜찮다고 하지만,
                    // cache 상황에 따라 unload 될 수도 있다는 생각에...
                    try {
                        obj.synchronousLoad();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            }
            try {
                readProgramsFromZip();
            } catch (Throwable t) {
                // OutOfMemory 등 발생 가능
                Log.printError("EPG501 : failed to parse");
                Log.print(t);
            }
            if (file instanceof DSMCCObject) {
                try {
                    ((DSMCCObject) file).unload();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }

        /** zip file 읽어서 EpgDataManager에 저장. */
        private void readProgramsFromZip() {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(file);
            } catch (Throwable ex) {
                Log.printError("EPG501 : failed to open file : " + ex);
                Log.print(ex);
            }
            if (fis == null) {
                return;
            }

            SimpleDateFormat entryFormatter = new SimpleDateFormat(ENTRY_FORMAT);
            // time zone setting이 잘못 되었을때 대비한다. 보통은 -4, -5 return.
            long timeZoneOffset = entryFormatter.getTimeZone().getRawOffset();

            long time = System.currentTimeMillis();
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry entry = null;
            try {
                while ((entry = zis.getNextEntry()) != null) {
                    // zip file안의 entry 이름 규칙은 epg-20140403-1200 (GMT아님)
                    String entryName = entry.getName();
                    long from = 0;
                    long to = 0;
                    try {
                        from = entryFormatter.parse(entryName.substring(4)).getTime();
                        to = from + DATA_FILE_TIME_GAP; // 6시간
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                    Log.printDebug("EpgDataManager.june: timeZoneOffset = " + timeZoneOffset);
                    Log.printDebug("EpgDataManager.june: firstTime = " + new Date(firstTime));
                    Log.printDebug("EpgDataManager.june: to = " + new Date(to));
                    Log.printDebug("EpgDataManager.june: check = " + (to + 5 * Constants.MS_PER_HOUR + timeZoneOffset <= firstTime));
                    if (to + 5 * Constants.MS_PER_HOUR + timeZoneOffset <= firstTime) {
                        if (Log.INFO_ON) {
                            Log.printInfo("EpgDataManager.getProgramsFromZip : skipped old entry : " + entryName);
                            Log.printInfo(" from = " + new Date(from));
                            Log.printInfo(" to = " + new Date(to));
                            Log.printInfo(" timeZoneOffset = " + timeZoneOffset);
                            Log.printInfo(" firstTime = " + new Date(firstTime));
                        }
                        continue;
                    }
                    ZipSegment zs = new ZipSegment(from, to);   // ArrayList

                    InputStreamReader reader;
                    try {
                        reader = new InputStreamReader(zis, ENCODING);
                    } catch (UnsupportedEncodingException ex) {
                        reader = new InputStreamReader(zis);
                    }
                    // Line을 ZipSegment에 저장
                    BufferedReader r = new BufferedReader(reader);
                    String str;
                    while ((str = r.readLine()) != null) {
                        zs.add(str);
                    }
                    // ZipSegment를 parse & store
                    boolean success = addPrograms(zs);
                    if (success) {
                        fileEndTime = Math.max(fileEndTime, zs.to);
                        Range range = new Range(zs.from, zs.to);
                        if (isPpv) {
                            ppvChecker.add(range);
                        } else {
                            epgChecker.add(range);
                        }
                    }
                    zs = null;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("EpgDataManager.getProgramsFromZip : " + entryName);
                    }
                }
            } catch (Throwable e) {
                Log.printError("EPG501 : failed to unzip : free_heap=" + Runtime.getRuntime().freeMemory());
                Log.print(e);
            } finally {
                try {
                    zis.close();
                } catch (Exception e) {
                    Log.print(e);
                }
                try {
                    fis.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (Log.INFO_ON) {
                Log.printInfo("EpgDataManager.getProgramsFromZip: read time = " + (System.currentTimeMillis() - time));
            }
        }

        /** Comparable. */
        public int compareTo(Object o) {
            // 날짜순 정렬, PPV 우선
            if (o instanceof ZipQueueEntry) {
                ZipQueueEntry z = (ZipQueueEntry) o;
                int diff = number - z.number;
                if (diff == 0) {
                    return isPpv ? -1 : 1;
                } else {
                    return diff;
                }
            } else {
                return -1;
            }
        }

        public boolean equals(Object o) {
//            if (o instanceof File) {
//                File f = (File) o;
//                return name.equals(f.getName()) && f.length() == data.length;
//            }
            return o == this;
        }

        public String toString() {
            return "ZipQueueEntry[" + name + ", " /*+ data.length + ", "*/
                        + (isPpv ? "PPV" : "EPG") + ", " + absolutePath + "]";
        }
    }

    /**
     * HTTP request
     */
    class HttpQueueEntry extends QueueEntry {
        long from;
        long to;

        public HttpQueueEntry(long from, long to) {
            this.from = from;
            this.to = to;
        }

        public void run() {
            if (!epgChecker.contains(from, to) || !ppvChecker.contains(from, to)) {
                List list = requestLines(null, from, to, true);
                addPrograms(list);
            }
            ppvChecker.add(from, to);
            epgChecker.add(from, to);
        }

        public int compareTo(Object o) {
            return 0;
        }

        public String toString() {
            return "HttpQueueEntry[" + new Date(from) + " - " + new Date(to) + "]";
        }
    }

    /**
     * Channel별 현재 program을 찾아서 유지.
     * EPG 특성상 현재 program이 필요한 경우가 많은데, 그때그때 찾으면 느리기 때문에 미리 준비.
     */
    class ResetCurrentQueueEntry extends QueueEntry {

        public void run() {
            long readyStartTime = System.currentTimeMillis();   // for 시간측정
            long closestEndTime = Long.MAX_VALUE;       // 다음 check할 time
            resetFirstTime();       // 과거데이터 삭제를 위해 firstTime 재설정
            Enumeration en = listTable.elements();
            // absoulte timer를 사용해도 오차로 인해 원하는시간 500ms 이전에 호출될 수 있다.
            // 이 경우 이전 프로그램이 선택되기 때문에, 반올림한 시간을 사용.
            long currentTime = Clock.getRoundedMillis();
            while (en.hasMoreElements()) {
                ProgramList list = (ProgramList) en.nextElement();
                list.resetStart(firstTime);
                Program cur = list.resetCurrent(currentTime);
                if (cur != null) {
                    closestEndTime = Math.min(cur.endTime, closestEndTime);
                }
            }
            if (closestEndTime == Long.MAX_VALUE) {
                // 3분 후에 다시 시도 - 데이터가 없거나 잘못된 경우.
                if (Log.WARNING_ON) {
                    Log.printWarning("EpgDataManager: reset current failed.");
                }
                closestEndTime = System.currentTimeMillis() + 3 * Constants.MS_PER_MINUTE;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("EpgDataManager: reset current elapsed time = "
                                + (System.currentTimeMillis() - readyStartTime) + " " + new Date(closestEndTime));
            }
            startTimer(closestEndTime);
        }

        public int compareTo(Object o) {
            // 제일 늦게 해도 된다.
            return 1;
        }

        public String toString() {
            return "ResetCurrentQueueEntry";
        }
    }

    /**
     * EPG view에 따라 HTTP로 가져온 데이터를 룰에 따라 적절히 지운다.
     */
    class EpgViewQueueEntry extends QueueEntry {
        // 모두 0 이면 fileEndTime 이후를 지운다.
        long from;
        long to;

        /** EPG를 닫는 경우. */
        public EpgViewQueueEntry() {
        }

        /** EPG view를 옮긴 경우. */
        public EpgViewQueueEntry(long from, long to) {
            this.from = from;
            this.to = to;
        }

        /** Comparable. */
        public int compareTo(Object o) {
            if (o instanceof ZipQueueEntry) {
                return 1;
            }
            return 0;
        }

        /** 원하는 영역을 제거. */
        private void flush(long newFrom, long newTo) {
            if (!epgChecker.intersects(newFrom, newTo) && !ppvChecker.intersects(newFrom, newTo)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EpgViewQueueEntry: flush[" + new Date(newFrom) + " - " + new Date(newTo) + "] : skipped");
                }
                return;
            }
            Enumeration en = listTable.elements();
            while (en.hasMoreElements()) {
                ProgramList list = (ProgramList) en.nextElement();
                list.flush(newFrom, newTo);
            }
            ppvChecker.remove(newFrom, newTo);
            epgChecker.remove(newFrom, newTo);
            if (Log.DEBUG_ON) {
                Log.printDebug("EpgViewQueueEntry: flush[" + new Date(newFrom) + " - " + new Date(newTo) + "] : finished");
            }
        }

        /** 원하는 지점 이후 모두. */
        private void flushAfter(long last) {
            long newFrom = last;
            long newTo = newFrom + newFrom + 300 * Constants.MS_PER_DAY;
            if (!epgChecker.intersects(newFrom, newTo) && !ppvChecker.intersects(newFrom, newTo)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("EpgViewQueueEntry: flushAfter[" + new Date(newFrom) + "] : skipped");
                }
                return;
            }
            Enumeration en = listTable.elements();
            while (en.hasMoreElements()) {
                ProgramList list = (ProgramList) en.nextElement();
                list.resetLast(newFrom);
            }
            ppvChecker.remove(newFrom, newTo);
            epgChecker.remove(newFrom, newTo);
            if (Log.DEBUG_ON) {
                Log.printDebug("EpgViewQueueEntry: flushAfter[" + new Date(newFrom) + "] : finished");
            }
        }

        public void run() {
            if (to == 0) {
                // EPG를 닫으면 file에서 가져온 이후는 모두 삭제
                flushAfter(fileEndTime);
            } else {
                // 보고있는 화면 기준으로 삭제 영역 설정
                long newFrom = from - PAST_BLOC_PERIOD;
                long newTo = to + FUTURE_BLOC_PERIOD;
                if (fileEndTime < newFrom) {
                    flush(fileEndTime, newFrom);
                }
                if (fileEndTime < newTo) {
                    flushAfter(newTo);
                }
            }
        }

        public String toString() {
            if (to == 0) {
                return "EpgViewQueueEntry[close]";
            } else {
                return "EpgViewQueueEntry[" + new Date(from) + " - " + new Date(to) + "]";
            }
        }
    }

/*
    class FlushExtraChannelsQueueEntry extends QueueEntry {
        public int compareTo(Object o) {
            return 1;
        }

        public void run() {
            ArrayList toBeRemoved = new ArrayList();
            Enumeration en = listTable.keys();
            while (en.hasMoreElements()) {
                try {
                    String s = (String) en.nextElement();
                    if (s != null && ChannelDatabase.current.getChannelByName(s) == null) {
                        toBeRemoved.add(s);
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            Log.printInfo("FlushExtraChannelsQueueEntry: found " + toBeRemoved.size());
            for (int i = 0; i < toBeRemoved.size(); i++) {
                ProgramList list = (ProgramList) listTable.remove(toBeRemoved.get(i));
                if (list != null) {
                    list.flushAllNext(list.start);
                }
            }
        }

        public String toString() {
            return "FlushExtraChannelsQueueEntry";
        }
    }
*/

    /**
     * EPG file 처리 이후에 Detach 수행.
     * Tuner를 놓아도 detach를 하지 않으면 OC cache에 남아있어서 access할 수 있다.
     *
     * 이전 구현은 detach 이후에 tuner를 놓았는데, 최대한 빨리 수행하기 위해,
     * byte[]에 zip file을 copy 해두고 detach 하는 방법을 사용했었음.
     * 그러다보니 memory 소모가 더 컸고, OOM 가능성을 줄이기 위해 이 방법으로 수정.
     */
    class DetachQueueEntry extends QueueEntry {
        /** Comparable. */
        public int compareTo(Object o) {
            return 1;
        }

        public void run() {
            DataAdapterManager.getInstance().getInbandAdapter().detach();
        }

        public String toString() {
            return "DetachQueueEntry";
        }
    }

    /**
     * EpgDataListener 전달.
     */
    class NotifyListenerQueueEntry extends QueueEntry {
        byte type;
        RangeList rangeList = new RangeList();

        public NotifyListenerQueueEntry(byte dataType) {
            type = dataType;
        }

        public void add(long from, long to) {
            rangeList.add(from, to);
        }

        /** Comparable. */
        public int compareTo(Object o) {
            return 1;
        }

        public void run() {
            for (int i = 0; i < rangeList.size(); i++) {
                Range range = (Range) rangeList.elementAt(i);
                try {
                    notifyListenersIxc(type, range.from, range.to);
                } catch (Throwable t) {
                    Log.print(t);
                }
            }
        }

        public String toString() {
            return "NotifyListenerQueueEntry[type=" + type + "]";
        }
    }

    /**
     * Listener 처리를 지원하는 RangeList.
     */
    class DataChecker extends RangeList {
        byte type;

        public DataChecker(byte dataType) {
            type = dataType;
        }

        public void add(Range range) {
            synchronized (DataChecker.this) {
                super.add(range);
                storedEndTime = Math.max(storedEndTime, range.to);
                Enumeration en = listTable.elements();
                while (en.hasMoreElements()) {
                    ProgramList list = (ProgramList) en.nextElement();
                    list.fill(range.to);
                }
            }
            long cur = System.currentTimeMillis();
            if (range.from <= cur && cur <= range.to) {
                timerWentOff(null);
            }
            notifyListenersInternal(type, range.from, range.to);
            worker.epgDataUpdated(type, range.from, range.to);
            dump();
        }
    }

}

class SeriesComparator implements Comparator {
    String callLetter;
    Channel channel;
	//->Kenneth : 4K by June
    int definition;

    SeriesComparator(String callLetter) {
        this.callLetter = callLetter;
        channel = ChannelDatabase.current.getChannelByName(callLetter);
        if (channel != null) {
			//->Kenneth : 4K by June
            definition = channel.getDefinition();
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof SeriesComparator) {
            return ((SeriesComparator) obj).channel == channel;
        }
        return false;
    }

    public int compare(Object o1, Object o2) {
        Program a = (Program) o1;
        Program b = (Program) o2;
        // check same channel
        boolean aSame = callLetter.equals(a.callLetter);
        boolean bSame = callLetter.equals(b.callLetter);
        if (aSame && bSame) {
            return a.compareTo(b);
        }
        if (aSame && !bSame) {
            return -1;
        }
        if (!aSame && bSame) {
            return 1;
        }
        // check same definition
        Channel aCh = a.getEpgChannel();
        Channel bCh = b.getEpgChannel();
		//->Kenneth : 4K by June
        if (aCh != null && bCh != null) {
            int aDef = aCh.getDefinition();
            int bDef = bCh.getDefinition();
            if (aDef == bDef) {
                return a.compareTo(b);
            }
            return (aDef == definition) ? -1 : 1;
        }
        return aCh != null ? -1 : 1;
    }
}


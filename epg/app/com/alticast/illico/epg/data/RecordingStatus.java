package com.alticast.illico.epg.data;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.sdv.mc.*;
import com.alticast.illico.epg.tuner.*;
import com.alticast.illico.epg.util.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import java.util.*;
import java.io.Serializable;
import javax.tv.service.selection.*;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterface;
import javax.tv.locator.*;
import javax.tv.service.*;
import org.ocap.net.OcapLocator;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.dvb.service.selection.DvbServiceContext;

public class RecordingStatus implements RecordingChangedListener, RecordingAlertListener {

    public static final boolean SHOW_MANUAL_RECORINGS_IN_GUIDE = App.R3_TARGET;

    public static final int NOT_RECORDING = 0;
    public static final int RECORDING     = 1;
    public static final int SKIPPED       = 2;

    /** 진행중 states */
    private static final int[] IN_PROGRESS_STATES = {
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE
    };

    /** PENDING states */
    private static final int[] PENDING_STATES = {
        OcapRecordingRequest.PENDING_NO_CONFLICT_STATE,
        OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE
    };

    private HashSet inProgressChannels = new HashSet();
    private ArrayList inProgressChannelList = new ArrayList();
    // key = RecordingRequest, value = InProgressItem
    private Hashtable inProgressRecordings = new Hashtable();
    // key = call + start_time, value = RecordingRequest
    private Hashtable scheduledTable = new Hashtable();

    // Key = call, value = Hashset<RecordingRequest>
    private Hashtable manualRecordingTable = new Hashtable();
    // Key = call, value = RangeList
    private Hashtable manualTimeTable = new Hashtable();

    OcapRecordingManager manager;

    private Byte REMOVED_LOCK = new Byte((byte) 0);

    private ChannelChangeWaitItem waitItem = null;

    private static RecordingStatus instance = new RecordingStatus();

    public static RecordingStatus getInstance() {
        return instance;
    }

    private RecordingStatus() {
    }

    public void init() {
        manager = (OcapRecordingManager) RecordingManager.getInstance();
        if (manager == null) {
            Log.printWarning("RecordingStatus.init: RecordingManager is null.");
            return;
        }
        manager.addRecordingChangedListener(this);

        RecordingAlertListener mcRead = new RecordingAlertListener() {
            public void recordingAlert(RecordingAlertEvent e) {
                MiniCarouselReader.getInstance().forceTune();
            }
        };

        manager.addRecordingAlertListener(mcRead, 90 * Constants.MS_PER_SECOND);   // 90초
        manager.addRecordingAlertListener(this, 50 * Constants.MS_PER_SECOND);     // 50초
        synchronized (this) {
            RecordingList list = manager.getEntries();
            if (list == null) {
                return;
            }
            int size = list.size();
            Log.printInfo("RecordingStatus.init: size = " + size);
            for (int i = 0; i < size; i++) {
                try {
                    RecordingRequest r = list.getRecordingRequest(i);
                    entryChanged(r, RecordingChangedEvent.ENTRY_ADDED, 0, r.getState());
                } catch (Exception ex) {
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("RecordingStatus.init: inProgressRecordings.size = " + inProgressRecordings.size());
            Log.printDebug("RecordingStatus.init: scheduledTable.size = " + scheduledTable.size());
        }
    }

    /** RecordingAlertListener. */
    public void recordingAlert(RecordingAlertEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert("+e+")");
        RecordingRequest req = e.getRecordingRequest();
        if (req == null) {
            return;
        }

        if (ChannelDatabase.current == null) {
            return;
        }
        Locator locator = null;

        String callLetter = AppData.getString(req, AppData.CHANNEL_NAME);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert() : callLetter = "+callLetter);
        int sid = AppData.getInteger(req, AppData.SOURCE_ID);
        Channel ch = null;
        if (callLetter != null) {
            ch = ChannelDatabase.current.getChannelByName(callLetter);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert() : ch = "+ch);
        if (ch == null) {
            // call letter에 해당하는 channel 이 없어진 경우
            ch = ChannelDatabase.current.getChannelById(sid);
            if (ch != null) {
                // source ID로 찾아서 같은 것이 있으면 이름이 바뀐것으로 간주
                AppData.set(req, AppData.CHANNEL_NAME, ch.getName());
                Log.printError("RecordingStatus: new channel name = " + ch.getName());
            } else {
                // 없으면 놔둔다. 나중에 fail 될 듯.
                Log.printError("RecordingStatus: channel not found");
            }
        } else {
            int newSid = ch.getSourceId();
            if (newSid != sid) {
                // channel에 해당하는 source ID가 바뀐 경우
                if (ch instanceof SdvChannel) {
                    try {
                        locator = new OcapLocator(newSid);
                    } catch (Exception ex) {
                        locator = SdvChannel.DUMMY_SDV_LOCATOR;
                    }
                } else {
                    locator = ch.getLocator();
                }
                AppData.set(req, AppData.SOURCE_ID, new Integer(newSid));
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert() : new source_id = "+newSid);
                sid = newSid;
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert() : source_id is Good");
            }
        }
/*
        if (ch != null && ch instanceof SdvChannel) {
            Log.printInfo("RecordingStatus: SDV channel = " + ch);
            McData data = (McData) DataCenter.getInstance().get(MiniCarouselReader.MC_KEY);
            if (data != null) {
                McChannelInfo info = data.get(sid);
                if (info != null) {
                    Log.printInfo("RecordingStatus: setting tuning information from MC to SDV recording");
                    try {
                        locator = new OcapLocator((int) info.mCarrierFrequency, info.mProgramNumber, (int) info.mModulationMode);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            }
//            if (sdvLocator == null) {
//                try {
//                    sdvLocator = ChannelDatabase.current.getDataChannel().getLocator();
//                } catch (Exception ex) {
//                    Log.print(ex);
//                }
//            }
        }
*/
        if (locator != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingAlert() : Call reschedule(req, locator)");
            reschedule(req, locator);
        }
    }

    private void reschedule(RecordingRequest req, Locator loc) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.reschedule("+req+", "+loc+")");
        RecordingSpec spec = req.getRecordingSpec();
        if (spec != null && spec instanceof LocatorRecordingSpec) {
            LocatorRecordingSpec oldSpec = (LocatorRecordingSpec) spec;
            try {
                Locator[] locators = new Locator[] { loc };
                LocatorRecordingSpec newSpec = new LocatorRecordingSpec(locators,
                        oldSpec.getStartTime(), oldSpec.getDuration(), oldSpec.getProperties());
                req.reschedule(newSpec);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    /** RecordingChangedListener. */
    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingChanged("+e+")");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.recordingChanged() : Call entryChanged()");
        entryChanged(e.getRecordingRequest(), e.getChange(), e.getOldState(), e.getState());
    }

    //->Kenneth[2015.10.27] : 자세한 log 찍기 위해서 추가
    private String getStateStr(int state) {
        String stateStr = "";
        switch (state) {
            case OcapRecordingRequest.CANCELLED_STATE:
                stateStr = "CANCELLED_STATE";
                break;
            case OcapRecordingRequest.TEST_STATE:
                stateStr = "TEST_STATE";
                break;
            case OcapRecordingRequest.COMPLETED_STATE:
                stateStr = "COMPLETED_STATE";
                break;
            case OcapRecordingRequest.DELETED_STATE:
                stateStr = "DELETED_STATE";
                break;
            case OcapRecordingRequest.FAILED_STATE:
                stateStr = "FAILED_STATE";
                break;
            case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                stateStr = "IN_PROGRESS_INCOMPLETE_STATE";
                break;
            case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                stateStr = "IN_PROGRESS_INSUFFICIENT_SPACE_STATE";
                break;
            case OcapRecordingRequest.IN_PROGRESS_STATE:
                stateStr = "IN_PROGRESS_STATE";
                break;
            case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                stateStr = "IN_PROGRESS_WITH_ERROR_STATE";
                break;
            case OcapRecordingRequest.INCOMPLETE_STATE:
                stateStr = "INCOMPLETE_STATE";
                break;
            case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                stateStr = "PENDING_WITH_CONFLICT_STATE";
                break;
            case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
                stateStr = "PENDING_NO_CONFLICT_STATE";
                break;
        }
        return stateStr;
    }
    //<-

    private synchronized void entryChanged(RecordingRequest r, int change, int oldState, int newState) {
        Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged : " + change + ", " + getStateStr(oldState) + " -> " + getStateStr(newState));
        switch (change) {
            case RecordingChangedEvent.ENTRY_ADDED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : ENTRY_ADDED");
                if (isScheduled(newState)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processScheduled(r, true)");
                    processScheduled(r, true);
                }
                if (isInProgress(newState)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processInProgress(r, true)");
                    processInProgress(r, true);
                }
                if (newState == OcapRecordingRequest.FAILED_STATE || newState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processFailed(r)");
                    processFailed(r);
                }
//                if (newState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
//                    processWithError(r, true);
//                }
                break;
            case RecordingChangedEvent.ENTRY_DELETED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : ENTRY_DELETED");
                if (isScheduled(oldState)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processScheduled(r, false)");
                    processScheduled(r, false);
                }
                if (isInProgress(oldState)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processInProgress(r, false)");
                    processInProgress(r, false);
                }
//                if (oldState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
//                    processWithError(r, false);
//                }
                break;
            case RecordingChangedEvent.ENTRY_STATE_CHANGED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : ENTRY_CHANGED");
                boolean oldSche = isScheduled(oldState);
                boolean newSche = isScheduled(newState);
                if (oldSche != newSche) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processScheduled(r, "+newSche+")");
                    processScheduled(r, newSche);
                }
                boolean oldProg = isInProgress(oldState);
                boolean newProg = isInProgress(newState);
                if (oldProg != newProg || !newProg) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processInProgress(r, "+newProg+")");
                    processInProgress(r, newProg);
                }
                if (newState == OcapRecordingRequest.FAILED_STATE || newState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call processFailed(r)");
                    processFailed(r);
                }
//                boolean oldError = oldState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE;
//                boolean newError = newState == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE;
//                if (oldError != newError) {
//                    processWithError(r, newError);
//                }
                break;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call gridEpg.updateRecordingStatus()");
        EpgCore.getInstance().gridEpg.updateRecordingStatus();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.entryChanged() : Call miniEpg.updateRecordingStatus()");
        EpgCore.getInstance().miniEpg.updateRecordingStatus();
    }

    private void processInProgress(RecordingRequest r, boolean add) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress("+r+", "+add+")");
        if (add) {
            InProgressItem ip = new InProgressItem(r);
            Object old = inProgressRecordings.put(r, ip);
            if (old == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call SDVSessionManager.recordingStarted(r)");
                SDVSessionManager.getInstance().recordingStarted(r);
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Duplicated Recording");
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call TunerManager.recordingStarted(r, ip.networkInterface)");
            TunerManager.getInstance().recordingStarted(r, ip.networkInterface);
        } else {
            InProgressItem ip = (InProgressItem) inProgressRecordings.remove(r);
            if (ip != null) {
                synchronized (REMOVED_LOCK) {
                    Integer sid = getSourceId(r);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call SDVSessionManager.recordingStopped(r)");
                    SDVSessionManager.getInstance().recordingStopped(r);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call TunerManager.recordingStopped()");
                    TunerManager.getInstance().recordingStopped(r, ip != null ? ip.networkInterface : null);
                    if (waitItem != null) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call waitItem.tuneIfPossible()");
                        boolean check = waitItem.tuneIfPossible(sid.intValue());
                        if (check) {
                            waitItem = null;
                        }
                    }
                }
            }
        }

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call ChannelZapper.setRecordingCount()");
        ChannelZapper.getInstance().setRecordingCount(getRecordingCount());

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processInProgress() : Call updateInProgressChannels()");
        updateInProgressChannels();

        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"RecordingStatus: inProgressRecordings.size = " + inProgressRecordings.size() + ", added = " + add);
            Log.printDebug(App.LOG_HEADER+"RecordingStatus: inProgressChannels.size = " + inProgressChannels.size());
        }
//        FrameworkMain.getInstance().printSimpleLog("inProgressRecordings.size = " + inProgressRecordings.size());
    }

    private synchronized void updateInProgressChannels() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.updateInProgressChannels()");
        HashSet newChannels = new HashSet();
        ArrayList newChannelList = new ArrayList();
        for (Iterator iter = inProgressRecordings.values().iterator(); iter.hasNext();) {
            InProgressItem item = (InProgressItem) iter.next();
            newChannels.add(new Integer(item.sourceId));
            newChannelList.add(new Integer(item.number));
        }
        this.inProgressChannels = newChannels;
        Collections.sort(newChannelList);
        this.inProgressChannelList = newChannelList;
    }

    public Channel getNextRecordingChannel(Channel ch) {
        ArrayList list = inProgressChannelList;
        int pos = Collections.binarySearch(list, new Integer(ch.getNumber()));
        if (pos < 0) {
            pos = (-pos - 1) % list.size();
        } else {
            pos = (pos + 1) % list.size();
        }
        int number = ((Integer) inProgressChannelList.get(pos)).intValue();
        return ChannelDatabase.current.getChannelByNumber(number);
    }

    public Channel getPreviousRecordingChannel(Channel ch) {
        ArrayList list = inProgressChannelList;
        int pos = Collections.binarySearch(list, new Integer(ch.getNumber()));
        int size = list.size();
        if (pos < 0) {
            pos = (-pos - 2 + size) % size;
        } else {
            pos = (pos - 1 + size) % size;
        }
        int number = ((Integer) inProgressChannelList.get(pos)).intValue();
        return ChannelDatabase.current.getChannelByNumber(number);
    }

    private void processScheduled(RecordingRequest r, boolean add) {
        Object id = AppData.get(r, AppDataKeys.PROGRAM_ID);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processScheduled("+id+", "+add+")");
        if (id == null || id.equals("")) {
            // manual recording
            if (SHOW_MANUAL_RECORINGS_IN_GUIDE) {
                String call = AppData.getString(r, AppDataKeys.CHANNEL_NAME);
                if (call.equals("")) {
                    return;
                }
                HashSet set = (HashSet) manualRecordingTable.get(call);
                if (set == null) {
                    if (add) {
                        set = new HashSet();
                        manualRecordingTable.put(call, set);
                    } else {
                        return;
                    }
                }
                if (add) {
                    set.add(r);
                } else {
                    set.remove(r);
                }
                Object[] array = set.toArray();
                RangeList rangeList = new RangeList();
                for (int i = 0; i < array.length; i++) {
                    RecordingRequest rr = (RecordingRequest) array[i];
                    long from = AppData.getScheduledStartTime(rr);
                    long dur = AppData.getScheduledDuration(rr);
                    rangeList.add(from, from + dur);
                }
                manualTimeTable.put(call, rangeList);
                if (Log.DEBUG_ON) {
                    Log.printDebug("RecordingStatus: time slots for manual recorings of " + call);
                    rangeList.dump();
                }
            }
            return;
        }
        String key = AppData.getString(r, AppDataKeys.CHANNEL_NAME) + AppData.getLong(r, AppDataKeys.PROGRAM_START_TIME);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processScheduled() : "+key+", "+add);
        if (add) {
            scheduledTable.put(key, r);
        } else {
            Object value = scheduledTable.remove(key);
            if (value == null) {
                Set set = scheduledTable.entrySet();
                Iterator it = set.iterator();
                Map.Entry entry;
                Object removeKey = null;
                while (it.hasNext()) {
                    entry = (Map.Entry) it.next();
                    if (r.equals(entry.getValue())) {
                        removeKey = entry.getKey();
                        break;
                    }
                }
                if (removeKey != null) {
                    scheduledTable.remove(removeKey);
                }
            }
        }
    }

    private void processFailed(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.processFailed() : Call SDVSessionManager.recordingFailed(r)");
        SDVSessionManager.getInstance().recordingFailed(r);
    }

//    private void processWithError(RecordingRequest r, boolean added) {
//        Log.printDebug("RecordingStatus.processWithError: " + added);
//    }

    public boolean cancelPpv(AbstractProgram p) {
        Log.printDebug(App.LOG_HEADER+"RecordingStatus.cancelPpv("+p+")");
        if (p == null) {
            return false;
        }
        RecordingRequest req = (RecordingRequest) scheduledTable.get(p.getCallLetter() + p.getStartTime());
        if (req == null) {
            return false;
        }
        boolean recorded = false;
        try {
            recorded = ((LeafRecordingRequest) req).getService() != null;
        } catch (Exception ex) {
        }
        try {
            if (recorded) {
                ((LeafRecordingRequest) req).stop();
            } else {
                req.delete();
            }
            return true;
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
    }

    public RecordingRequest getRecordingRequest(int reqId) {
        if (manager != null) {
            return manager.getRecordingRequest(reqId);
        }
        return null;
    }

    private RecordingRequest getContent(Program p) {
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getContent("+p+")");
        if (p == null) {
            return null;
        }
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getContent() : "+p.getCallLetter() + p.getStartTime());
        return (RecordingRequest) scheduledTable.get(p.getCallLetter() + p.getStartTime());
    }

    public boolean contains(Program p) {
        return getContent(p) != null;
    }

    public int getStatus(Program p) {
        RecordingRequest req = getContent(p);
        if (req == null) {
            return NOT_RECORDING;
        }
        if (AppData.getBoolean(req, AppDataKeys.UNSET)) {
            return SKIPPED;
        } else {
            return RECORDING;
        }
    }

    //->Kenneth[2015.6.19] R5 : VDTRMASTER-5480
    // Grid EPG 보여줄때 SD/HD 그룹핑 채널인 경우 채널은 SD 지만 프로그램 정보는 HD 로 바꿔 놓았음.
    // GridEpg.decideTargetChannel() 의 적용이 그것임. 그러나 그 side effect 로 recording -> B -> SD 로 녹화 채널 바꾸는
    // 경우 문제 발생. 또한 D 액션팝업, Grid program 에서 제대로 recording 되고 있는지 감지 못하는 경우 발생.
    // 따라서 Sister 프로그램에 대한 녹화 여부를 알게 해주는 API 를 추가적으로 사용함.
    public int getSisterStatus(Program p) {
        if (p == null) {
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getSisterStatus() returns NOT_RECORDING -- 1");
            return NOT_RECORDING;
        }

        //->Kenneth[2016.3.7] R7 : Ungrouped 이면 체크안함
        if (!EpgCore.isChannelGrouped) {
            return NOT_RECORDING;
        }
        //<-

        Program sisterProgram = p.getSisterProgram();
        if (sisterProgram == null) {
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getSisterStatus() returns NOT_RECORDING -- 2");
            return NOT_RECORDING;
        }

        RecordingRequest req = getContent(sisterProgram);
        if (req == null) {
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getSisterStatus() returns NOT_RECORDING -- 3");
            return NOT_RECORDING;
        }
        if (AppData.getBoolean(req, AppDataKeys.UNSET)) {
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getSisterStatus() returns SKIPPED");
            return SKIPPED;
        } else {
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.getSisterStatus() returns RECORDING");
            return RECORDING;
        }
    }

    public int getRecordingCount() {
        return inProgressRecordings.size();
    }

    public Vector getInProgressRecordings() {
        return new Vector(inProgressRecordings.keySet());
    }

    public RangeList getManualRecordingTimes(Channel ch) {
        return (RangeList) manualTimeTable.get(ch.getName());
    }

    public boolean isRecording(Channel ch) {
        if (ch == null) {
            return false;
        }
        return inProgressChannels.contains(ch.getSourceIdObject());
    }

    public Channel getNearestRecording(Channel ch) {
        try {
            Enumeration en = inProgressRecordings.keys();
            RecordingRequest r = null;
            int id = 0;
            while (en.hasMoreElements()) {
                RecordingRequest req = (RecordingRequest) en.nextElement();
                int rid = req.getId();
                if (id < rid) {
                    id = rid;
                    r = req;
                }
            }
            if (r != null) {
                int cn = AppData.getInteger(r, AppData.CHANNEL_NUMBER);
                return ChannelDatabase.current.getChannelByNumber(cn);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    public boolean isRecording(ChannelController cc) {
        InProgressItem item = getRecording(cc);
        if (item == null) {
            return false;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("RecordingStatus: found recording for cc");
            Log.printDebug(" cc  = " + cc);
            Log.printDebug(" rec = " + item);
        }
        return true;
    }

    public int getRecordingSourceIdWithout(ChannelController cc) {
        NetworkInterface ni = cc != null ? cc.getNetworkInterface() : null;
        for (Iterator iter = inProgressRecordings.values().iterator(); iter.hasNext();) {
            InProgressItem item = (InProgressItem) iter.next();
            if (item.networkInterface != ni) {
                return item.sourceId;
            }
        }
        return -1;
    }

    private InProgressItem getRecording(ChannelController cc) {
        return getRecording(cc.getNetworkInterface());
    }

    private InProgressItem getRecording(NetworkInterface ni) {
        if (ni == null) {
            return null;
        }
        for (Iterator iter = inProgressRecordings.values().iterator(); iter.hasNext();) {
            InProgressItem item = (InProgressItem) iter.next();
            if (item.networkInterface == ni) {
                return item;
            }
        }
        return null;
    }

    public RecordingRequest getRecordingRequest(NetworkInterface ni) {
        InProgressItem ipi = getRecording(ni);
        if (ipi == null) {
            return null;
        }
        return ipi.request;
    }

    private boolean isInProgress(int state) {
        switch (state) {
            case OcapRecordingRequest.IN_PROGRESS_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                return true;
            default:
                return false;
        }
    }

    private boolean isScheduled(int state) {
        switch (state) {
            case OcapRecordingRequest.IN_PROGRESS_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
            case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
            case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
            case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                return true;
            default:
                return false;
        }
    }

    /**
     * Recording Event 타이밍에 따라 갈 수 있음에도 못 가는 경우가 있음.
     * true를 return 하면 progress 에서 뭔가 빠졌다는 뜻.
     */
    public synchronized boolean revalidate() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.revalidate()");
        ArrayList changedRecordings = new ArrayList();
        try {
            Enumeration en = inProgressRecordings.keys();
            RecordingRequest r = null;
            int id = 0;
            while (en.hasMoreElements()) {
                RecordingRequest req = (RecordingRequest) en.nextElement();
                int state = -1;
                try {
                    state = req.getState();
                } catch (Exception ex) {
                }
                if (!isInProgress(state)) {
                    changedRecordings.add(req);
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        int size = changedRecordings.size();
        if (size == 0) {
            Log.printDebug("RecordingStatus.revalidate : did nothing");
            return false;
        } else {
            for (int i = 0; i < size; i++) {
                RecordingRequest req = (RecordingRequest) changedRecordings.get(i);
                processInProgress(req, false);
            }
            Log.printDebug("RecordingStatus.revalidate = " + size);
            return true;
        }
    }

    private ChannelController findUnselectedController(HashSet selectedChannels) {
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController cc = ChannelController.get(i);
            if (cc.getState() != ChannelController.STOPPED) {
                Channel ch = cc.getCurrent();
                if (ch == null) {
                    return cc;
                }
                Integer sid = new Integer(ch.getSourceId());
                if (!selectedChannels.contains(sid)) {
                    return cc;
                }
            }
        }
        return null;
    }

    public synchronized void changeChannelByRecordingChange(int recording, HashSet selectedChannels) {
        final ChannelController cc = findUnselectedController(selectedChannels);
        if (cc == null) {
            Log.printInfo("RecordingStatus.changeChannelByRecordingChange: OK to record. does nothing.");
            return;
        }
        Log.printInfo("RecordingStatus.changeChannelByRecordingChange: target cc = " + cc);

        RecordingRequest rr = manager.getRecordingRequest(recording);
        int sid = getSourceId(rr).intValue();
        final Channel ch = ChannelDatabase.current.getChannelById(sid);

        synchronized (REMOVED_LOCK) {
            boolean ok = TunerManager.getInstance().checkTune(cc, ch);
            if (ok) {
                Log.printInfo("RecordingStatus.changeChannelByRecordingChange : OK");
                cc.changeChannel(ch);
            } else {
                Log.printInfo("RecordingStatus.changeChannelByRecordingChange : wait");
                waitItem = new ChannelChangeWaitItem(cc, ch);
            }
        }
    }

    public RecordingRequest replace(RecordingRequest req, LocatorRecordingSpec locSpec, Locator newLocator, int sdvStatus) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingStatus.replace("+req+", "+locSpec+", "+newLocator+", "+sdvStatus+")");
        try {
            LocatorRecordingSpec newSpec = new LocatorRecordingSpec(new Locator[] { newLocator },
                locSpec.getStartTime(), locSpec.getDuration(), locSpec.getProperties());

            String[] oldKeys = req.getKeys();
            Serializable[] values;
            String[] keys;
            if (oldKeys == null) {
                keys = new String[1];
                values = new Serializable[1];
            } else {
                keys = new String[oldKeys.length + 1];
                values = new Serializable[oldKeys.length + 1];
                for (int i = 0; i < oldKeys.length; i++) {
                    keys[i] = oldKeys[i];
                    values[i] = req.getAppData(oldKeys[i]);
                }
            }
            keys[keys.length - 1] = AppData.SDV_STATUS;
            values[keys.length - 1] = new Integer(sdvStatus);
            req.delete();
            RecordingRequest newRecording = manager.record(newSpec, keys, values);
            return newRecording;
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    ////////////////////////////////////////////////////////////////////////
    // AppData get methods
    ////////////////////////////////////////////////////////////////////////

    public static Integer getSourceId(RecordingRequest req) {
        Integer integer = null;
        try {
            integer = (Integer) AppData.get(req, AppDataKeys.SOURCE_ID);
        } catch (Exception ex) {
        }
        if (integer == null) {
            integer = new Integer(0);
        }
        return integer;
    }

    public boolean isBuffering(Channel ch) {
        BufferingRequest[] brs = manager.getBufferingRequests();
        if (brs != null) {
            for (int i = 0; i < brs.length; i++) {
                if (brs[i] != null && ch.getService().equals(brs[i].getService())) {
                    return true;
                }
            }
        }
        return false;
    }

    class InProgressItem {
        int sourceId;
        int number;
        RecordingRequest request;
        Locator locator;
        NetworkInterface networkInterface;

        RecordingSpec spec;

        public InProgressItem(RecordingRequest r) {
            request = r;
            spec = r.getRecordingSpec();
            sourceId = getSourceId(r).intValue();
            number = AppData.getInteger(r, AppData.CHANNEL_NUMBER);
            networkInterface = findNetworkInterface();
            if (networkInterface == null) {
                Log.printError("RecordingStatus: not found NI = " + r.getId());
            } else {
                Log.printInfo("RecordingStatus: NI = " + networkInterface
                        + ", index = " + ChannelController.getNetworkInterfaceIndex(networkInterface));
            }
        }

        public NetworkInterface findNetworkInterface() {
            NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
            Object niIndex = request.getAppData("NI index");
            if (niIndex == null) {
                niIndex = request.getAppData("NI_index");
            }
            if (niIndex != null) {
                int index = -1;
                if (niIndex instanceof Integer) {
                    index = ((Integer) niIndex).intValue();
                } else {
                    try {
                        index = Integer.parseInt(niIndex.toString());
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
                Log.printWarning("RecordingStatus: found NI from appData = " + niIndex);
                if (index >= 0 && index < nis.length) {
                    return nis[index];
                }
            }

            Locator[] locators;
            if (spec instanceof ServiceContextRecordingSpec) {
                ServiceContext sc = ((ServiceContextRecordingSpec) spec).getServiceContext();
                if (sc instanceof DvbServiceContext) {
                    return ((DvbServiceContext) sc).getNetworkInterface();
                } else {
                    Log.printWarning("RecordingStatus: not found NI : SC is not DvbServiceContext");
                    return null;
                }
            } else if (spec instanceof LocatorRecordingSpec) {
                locators = ((LocatorRecordingSpec) spec).getSource();
                if (locators.length == 0) {
                    Log.printWarning("RecordingStatus: not found NI : no locator");
                    return null;
                }
            } else if (spec instanceof ServiceRecordingSpec) {
                Service service = ((ServiceRecordingSpec) spec).getSource();
                if (service == null) {
                    Log.printWarning("RecordingStatus: not found NI : no service");
                    return null;
                }
                try {
                    locators = LocatorFactory.getInstance().transformLocator(service.getLocator());
                } catch (InvalidLocatorException ex) {
                    Log.print(ex);
                    return null;
                }
            } else {
                Log.printWarning("RecordingStatus: not found NI : unknown RecordingSpec");
                return null;
            }
            if (locators.length == 0 || locators[0] == null || !(locators[0] instanceof OcapLocator)) {
                Log.printWarning("RecordingStatus: not found NI : not OcapLocator");
                return null;
            }
            OcapLocator locator = (OcapLocator) locators[0];
            int freq = locator.getFrequency();
            String locatorString = locator.toExternalForm();

            NetworkInterface ni;

            // NI 중 같은 freq를 가지고 있는 것을 찾는다.
            // freq 까지밖에 찾을 수 없는 단점이 있다.
            ArrayList reserved = new ArrayList();
            for (int i = 0; i < nis.length; i++) {
                ni = nis[i];
                if (ni.isReserved()) {
                    Locator loc = ni.getLocator();
                    if (loc instanceof OcapLocator && ((OcapLocator) loc).getFrequency() == freq) {
                        reserved.add(ni);
                    }
                }
            }
            int size = reserved.size();
            if (size == 1) {
                Log.printInfo("RecordingStatus: found NI - reserved only.");
                return (NetworkInterface) reserved.get(0);
            } else if (size == 0) {
                Log.printWarning("RecordingStatus: no reserved NI.");
                return null;
            }
            // 두 NI가 같은 freq를 가지고 있는 경우... CC를 검색한다.

            // CC의 NI가 같은 locator를 가지고 있는 것을 찾는다.
            // CC의 NI가 null이 아니면 tune 되어있다는 얘기고 service를 얻어내면
            // tuned NI의 정확한 locator를 알 수 있다.
            ArrayList ccNiList = new ArrayList();
            ArrayList ccAllList = new ArrayList();
            for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
                ChannelController cc = ChannelController.get(i);
                ni = cc.getNetworkInterface();
                if (ni != null) {
                    ccAllList.add(ni);
                    Service ccService = cc.getService();
                    if (ccService != null) {
                        Locator ccLoc = ccService.getLocator();
                        Locator[] ccLocators = null;
                        try {
                            ccLocators = LocatorFactory.getInstance().transformLocator(ccLoc);
                            if (locatorString.equals(ccLocators[0].toExternalForm())) {
                                ccNiList.add(ni);
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            }
            size = ccNiList.size();
            Log.printDebug("RecordingStatus: finding NI... ccNiList.size = " + size);
            if (size == 1) {
                Log.printInfo("RecordingStatus: found NI - watching only.");
                return (NetworkInterface) ccNiList.get(0);
            } else if (size == 0) {
                // 2개가 같은 freq, CC는 같은게 없는 경우.
                for (int i = 0; i < nis.length; i++) {
                    // cc에 의해 reserve되지 않은 NI.
                    if (!ccAllList.contains(nis[i])) {
                        Log.printWarning("RecordingStatus: found NI - used only TSB (BWP).");
                        return nis[i];
                    }
                }
                Log.printWarning("RecordingStatus: not found NI - no free CC");
                return null;
            } else {
                for (Iterator iter = inProgressRecordings.values().iterator(); iter.hasNext();) {
                    InProgressItem item = (InProgressItem) iter.next();
                    if (item.networkInterface != null) {
                        ccNiList.remove(item.networkInterface);
                    }
                }
                if (ccNiList.size() > 0) {
                    Log.printInfo("RecordingStatus: found NI. target size = " + ccNiList.size());
                    return (NetworkInterface) ccNiList.get(0);
                }
                Log.printWarning("RecordingStatus: not found NI - all tuners are recording?");
                return null;
            }
        }

        public boolean equals(Object o) {
            if (o instanceof Integer) {
                return ((Integer) o).intValue() == sourceId;
            } else if (o instanceof RecordingRequest) {
                return request.equals(o);
            } else if (o instanceof InProgressItem) {
                return request.equals(((InProgressItem) o).request);
            } else {
                return super.equals(o);
            }
        }

        public int hashCode() {
            return sourceId;
        }

        public String toString() {
            return "InProgressItem[" + sourceId + ", " + networkInterface + "]";
        }
    }

    class ChannelChangeWaitItem {
        ChannelController channelController;
        Channel targetChannel;
        int oldSourceId;

        public ChannelChangeWaitItem(ChannelController cc, Channel target) {
            this.channelController = cc;
            Channel ch = cc.getCurrent();
            if (ch != null) {
                this.oldSourceId = ch.getSourceId();
            }
            this.targetChannel = target;
        }

        public boolean tuneIfPossible(int sourceId) {
            if (oldSourceId == sourceId) {
                Log.printInfo("ChannelChangeWaitItem: tuning...");
                channelController.changeChannel(targetChannel);
                return true;
            } else {
                Log.printInfo("ChannelChangeWaitItem: different sid = " + oldSourceId + ", " + sourceId);
                return false;
            }
        }


    }

}



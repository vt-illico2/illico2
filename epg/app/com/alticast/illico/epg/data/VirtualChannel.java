package com.alticast.illico.epg.data;

import javax.tv.service.Service;
import com.alticast.illico.epg.config.ChannelInfo;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.videotron.tvi.illico.log.Log;

public class VirtualChannel extends Channel {

    private final int id;
    public String appName;
    public String param;

    public VirtualChannel(Service service, String name, int number) {
        super(service);
        this.number = number;
        this.type = TYPE_VIRTUAL;
        this.name = name;
        this.id = -number;
    }

    public int getId() {
        return id;
    }

    protected void setChannelInfo(ChannelInfo info) {
        if (info == null) {
            return;
        }
        super.setChannelInfo(info);
        this.appName = info.appName;
        this.param = info.param;
    }

    public boolean isAppChannel() {
        return appName != null;
    }

    public boolean select(ChannelController cc, boolean auth) {
        super.select(cc, auth);
        startSdvSession(cc, true);
        return true;
    }

}

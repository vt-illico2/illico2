package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
public class Highlight {
    public static final String PREF_KEY = "GRID_HIGHLIGHT";

    public static final String HIGHLIGHT_NONE = "HIGHLIGHT_NONE";
    public static final String HIGHLIGHT0 = "HIGHLIGHT0";
    public static final String HIGHLIGHT1 = "HIGHLIGHT1";
    public static final String HIGHLIGHT2 = "HIGHLIGHT2";
    public static final String HIGHLIGHT3 = "HIGHLIGHT3";
    public static final String HIGHLIGHT4 = "HIGHLIGHT4";
    public static final String HIGHLIGHT5 = "HIGHLIGHT5";
    public static final String HIGHLIGHT6 = "HIGHLIGHT6";
    public static final String HIGHLIGHT7 = "HIGHLIGHT7";
    public static final String HIGHLIGHT8 = "HIGHLIGHT8";

    private String curHighlight = HIGHLIGHT_NONE;
    private String[] allHighlights;
    private int[][] mappingCategories;
    private static Highlight instance;
    private PreferenceService ps;
    protected Highlight() {
        init();
    }

    private void init() {
        allHighlights = new String[10];
        allHighlights[0] = HIGHLIGHT_NONE;
        allHighlights[1] = HIGHLIGHT0;
        allHighlights[2] = HIGHLIGHT1;
        allHighlights[3] = HIGHLIGHT2;
        allHighlights[4] = HIGHLIGHT3;
        allHighlights[5] = HIGHLIGHT4;
        allHighlights[6] = HIGHLIGHT5;
        allHighlights[7] = HIGHLIGHT6;
        allHighlights[8] = HIGHLIGHT7;
        allHighlights[9] = HIGHLIGHT8;

        try {
            // Preference 에 저장된 값을 가져와서 초기값으로 삼는다.
            ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
            String stored = ps.getPreferenceValue(PREF_KEY);
            if (Log.DEBUG_ON) Log.printDebug("Highlight.init() : stored Highlight = "+stored);
            if (stored != null && stored.length() > 0) {
                curHighlight = stored;
            }
            // Propertis 파일에서 category id 매핑을 가져온다.
            mappingCategories = new int[allHighlights.length][];
            for (int i = 0 ; i < allHighlights.length ; i++) {
                String[] tmp = TextUtil.tokenize(DataCenter.getInstance().getString(allHighlights[i]), '|');
                if (tmp == null || tmp.length == 0) continue;
                int[] tmpInt = new int[tmp.length];
                for (int j = 0 ; j < tmp.length ; j++) {
                    tmpInt[j] = Integer.parseInt(tmp[j]);
                    if (Log.DEBUG_ON) Log.printDebug("Highlight.init() : Category mapping : "+allHighlights[i]+" -> "+tmpInt[j]);
                }
                mappingCategories[i] = tmpInt;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Highlight getInstance() {
        if (instance == null) {
            instance = new Highlight();
        }
        return instance;
    }

    public String getCurrentHighlight() {
        return curHighlight;
    }

    public String getHighlightName(String highlight) {
        System.out.println("getHighlightName : "+highlight);
        if (highlight == null) return null;
        String toc = DataCenter.getInstance().getString("epg."+highlight.toLowerCase());
        System.out.println("getHighlightName returns "+toc);
        return toc;
    }

    public boolean isNone() {
        if (curHighlight == null) return true;
        if (HIGHLIGHT_NONE.equals(curHighlight)) return true;
        return false;
    }

    //->Kenneth[2015.6.11] 
    // 프로그램 상세 정보 보여줄때 앞에 category 에 맞는 highlight name 을 앞에 붙여줘야 함.
    public String getMyCategoryName(Program p) {
        if (p == null) return null;
        if (mappingCategories == null || mappingCategories.length == 0) return null;
        try {
            int myCat = p.getCategory();
            System.out.println("getMyCategoryName : my category is "+myCat);
            int index = -1;
            for (int i = 0 ; i < mappingCategories.length ; i++) {
                int [] cats = mappingCategories[i];
                if (cats == null || cats.length == 0) continue;
                for (int j = 0 ; j < cats.length ; j++) {
                    if (cats[j] == myCat) {
                        index = i;
                        break;
                    }
                }
            }
            if (index == -1) {
                return null;
            }
            String myHighlightName = getHighlightName(allHighlights[index]);
            return myHighlightName;
        } catch (Exception e) {
            Log.print(e);
        }
        return null;
    }

    public boolean needToHighlighted(Program p) {
        if (p == null) return false;
        if (mappingCategories == null || mappingCategories.length == 0) return false;
        if (curHighlight.equals(HIGHLIGHT_NONE)) return false;
        try {
            int cat = p.getCategory();
            int index = 0;
            for (int i = 0 ; i < allHighlights.length ; i++) {
                if (allHighlights[i].equals(curHighlight)) {
                    index = i;
                    break;
                }
            }
            int [] cats = mappingCategories[index];
            for (int i = 0 ; i < cats.length ; i++) {
                if (cats[i] == cat) {
                    return true;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return false;
    }

    public void setHighlight(String highlight) {
        if (Log.DEBUG_ON) Log.printDebug("Highlight.setHighlight("+highlight+")");
        if (highlight == null) return;
        try {
            ps.setPreferenceValue(PREF_KEY, highlight);
            curHighlight = highlight;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] getAllHighlights() {
        return allHighlights;
    }
}

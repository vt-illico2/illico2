package com.alticast.illico.epg.data;

import java.awt.Image;
import java.util.Date;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.TimeZone;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.Constants;

public class EpgProgram extends Program {
    public static TimeZone gmtTimeZone;

    // info bit map
    // llcccccc (language - 2bit, category - 6bit)
    // ddssssss (definition - 2bit, sub category - 6bit) //->Kenneth : 4K by June
    // rrraattt (rating - 3bit, audio type - 2bit, program type - 3bit)
    // sssbbbb_ (star rating - 3bit, boolean values - 5bit) //->Kenneth : 4K by June

//->Kenneth : 4K by June
//    protected static final int HD_MASK = 0x01;
    protected static final int CAPTION_MASK = 0x02;
    protected static final int LIVE_MASK = 0x04;
    protected static final int INTERACTIVE_MASK = 0x08;
    protected static final int SAP_MASK = 0x10;

    protected static final char DELIM = '|';

    protected static Hashtable channelNamePool = new Hashtable();

    int infoMap;
    //String callLetter;  // 0
                        // 1 (channel definition -> not use)
    //long startTime;     // 2
    //long endTime;       // 3 (dur)
    String id;          // 4
    String title;       // 5
                        // 6-   type, def, audio, caption, live, interactive, star rating
//    int languageId;    // -6
                        // 7    category
                        // 8    subCategory
    String seriesId;    // 9
    char[] episodeTitle;// 10
//    String episodeId;   // 11
                        // 12   rating
                        // 13   sap
    char[] year;        // 14
    char[] country;     // 15
    char[] description; // 16
    char[] actors;      // 17

    protected EpgProgram() {
    }

    public static EpgProgram create(String line) {
        EpgProgram temp = new EpgProgram();
        char[] c = line.toCharArray();
        int lastPoint = temp.parseLine(line, c);
        if (lastPoint == 0) {
            return temp;
        }
        int p = line.indexOf(DELIM, lastPoint);
        return new PpvProgram(temp, line, c, lastPoint, p);
    }

    protected int parseLine(String line, char[] c) {
        int l = 0;
        int p;
        p = line.indexOf(DELIM, l);
        String str = getString(c, l, p - l);
        Object o = channelNamePool.get(str);
        if (o != null) {
            callLetter = (String) o;
        } else {
            channelNamePool.put(str, str);
            callLetter = str;
        }
        l = p + 1;

        p = line.indexOf(DELIM, l);
        // skip 1. channel definition
        l = p + 1;

        p = line.indexOf(DELIM, l);
        startTime = getGmtTime(c, l, p - l);
        l = p + 1;  // fixed 14 length

        p = line.indexOf(DELIM, l);
        long dur = getDuration(c, l, p - l);
        endTime = startTime + dur;
        l = p + 1;  // fixed 6 length

        p = line.indexOf(DELIM, l);
        if (p > l) {
            id = getString(c, l, p - l);
        } else {
            id = callLetter + startTime;
        }
        l = p + 1;

        p = line.indexOf(DELIM, l);
        title = getString(c, l, p - l);

        l = p + 1;
        // type //->Kenneth : 4K by June
        infoMap |= (c[l++] - '0') << 8;
        // definition //->Kenneth : 4K by June
        infoMap |= (c[l++] - '0') << 22;
        // audioType //->Kenneth : 4K by June
        infoMap |= (c[l++] - '0') << 11;
        if (c[l++] == '1') {
            infoMap |= CAPTION_MASK;
        }
        if (c[l++] == '1') {
            infoMap |= LIVE_MASK;
        }
        if (c[l++] == '1') {
            infoMap |= INTERACTIVE_MASK;
        }
        // star rating
        infoMap |= (c[l++] - '0') << 5;

        int languageId = Math.max(0, Math.min(c[l++] - '0', Constants.LANGUAGES.length - 1));
        infoMap |= (languageId & 0x03) << 30;

        l++;    // skip delim
        p = line.indexOf(DELIM, l);
        if (p != l) {
            infoMap |= getInt(c, l, p - l) << 24;   // category
        }
        l = p + 1;
        p = line.indexOf(DELIM, l);
        if (p != l) {
            infoMap |= getInt(c, l, p - l) << 16;   // subcategory
        }
        l = p + 1;

        p = line.indexOf(DELIM, l);
        seriesId = getString(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        episodeTitle = getChars(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
//        episodeId = getString(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        String rating = getString(c, l, p - l);
        infoMap |= getRatingIndex(rating) << 13;
        l = p + 1;

        p = line.indexOf(DELIM, l);
        if (c[l++] == '1') {
            infoMap = infoMap | SAP_MASK;
        }
        l = p + 1;

        p = line.indexOf(DELIM, l);
        year = getChars(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        country = getChars(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        description = getChars(c, l, p - l);
        l = p + 1;

        p = line.indexOf(DELIM, l);
        if (p == -1) {
            actors = getChars(c, l, c.length - l);
        } else {
            actors = getChars(c, l, p - l);
        }
        l = p + 1;

        return l;
    }

    protected static char[] getChars(char[] c, int offset, int len) {
        if (len == 0) {
            return null;
        }
        char[] ret = new char[len];
        System.arraycopy(c, offset, ret, 0, len);
        return ret;
    }

    protected static String getString(char[] c, int offset, int len) {
        if (len == 0) {
            return "";
        }
        return new String(c, offset, len);
    }

    protected static long getGmtTime(char[] c, int offset, int len) {
        if (len < 14) {
            return 0;
        }
        Calendar gmtCalendar = Calendar.getInstance(gmtTimeZone);
        gmtCalendar.set(Calendar.YEAR, getInt(c, offset + 0, 4));
        gmtCalendar.set(Calendar.MONTH, getInt(c, offset + 4, 2) - 1);
        gmtCalendar.set(Calendar.DAY_OF_MONTH, getInt(c, offset + 6, 2));
        gmtCalendar.set(Calendar.HOUR_OF_DAY, getInt(c, offset + 8, 2));
        gmtCalendar.set(Calendar.MINUTE, getInt(c, offset + 10, 2));
        gmtCalendar.set(Calendar.SECOND, getInt(c, offset + 12, 2));
        gmtCalendar.set(Calendar.MILLISECOND, 0);
        return gmtCalendar.getTime().getTime();
    }

    protected static long getDuration(char[] c, int i, int len) {
        if (len < 6) {
            return 0;
        }
        long val = (c[i++] - '0') * 10 + (c[i++] - '0');
        val = val * 60; // 60 min
        val = val + (c[i++] - '0') * 10 + (c[i++] - '0');
        val = val * 60; // 60 sec
        val = val + (c[i++] - '0') * 10 + (c[i++] - '0');
        return val * 1000L; // 1000 ms
    }

    private static int getInt(char[] c, int offset, int len) {
        int val = 0;
        int pow = 1;
        for (int i = offset + len - 1; i >= offset; i--) {
            val = val + (c[i] - '0') * pow;
            pow = pow * 10;
        }
        return val;
    }

    /**
     * Gets a call letter of Channel.
     * @return call letter.
     */
    public String getCallLetter() {
        return callLetter;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public long getDuration() {
        return endTime - startTime;
    }

    /**
     * Gets a type of Channel.
     * @return Channel type.
     */
    public int getType() {
        return (infoMap >> 8) & 0x07;
    }

	//->Kenneth : 4K by June
    public int getDefinition() {
        return (infoMap >> 22) & 0x03;
    }

    /**
     * Determines whether this program is provided with closed caption.
     * @return true if closed captioned; false otherwise.
     */
    public boolean isCaptioned() {
        return (infoMap & CAPTION_MASK) != 0;
    }

    public boolean isLive() {
        return (infoMap & LIVE_MASK) != 0;
    }

    public boolean isInteractive() {
        return (infoMap & INTERACTIVE_MASK) != 0;
    }

    public boolean isSapEnabled() {
        return (infoMap & SAP_MASK) != 0;
    }

    public int getStarRating() {
        return (infoMap >> 5) & 0x07;
    }

    /**
     * Gets a language of this Program.
     * @return language.
     */
    public String getLanguage() {
        return Constants.LANGUAGES[getLanguageIndex()];
    }

    public int getLanguageIndex() {
        return (infoMap >> 30) & 0x03;
    }

    public String getTitle() {
        return title;
    }

    /**
     * Gets an unique identifier of Program.
     * @return program id.
     */
    public String getId() {
        return id;
    }

    /**
     * Gets an audio type of Program.
     * @return audio type.
     */
    public int getAudioType() {
        return (infoMap >> 11) & 0x03;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getCategory() {
        return (infoMap >> 24) & 0x3F;
    }

    /**
     * Gets a genre of this Program.
     * @return genre.
     */
    public int getSubCategory() {
        return (infoMap >> 16) & 0x3F;
    }

    public int getRatingIndex() {
        return (infoMap >> 13) & 0x07;
    }

    public String getProductionYear() {
        return year == null ? "" : new String(year);
    }

    public String getCountry() {
        return country == null ? "" : new String(country);
    }

    public String getFullDescription() {
//        return description;
        return description == null ? "" : new String(description);
    }


    public String getActors() {
        return actors == null ? "" : new String(actors);
    }

    public String getDirector() {
        return "";
    }

    public String getSeriesId() {
        return seriesId;
    }

    public String getEpisodeTitle() {
        return episodeTitle == null ? "" : new String(episodeTitle);
    }

    public long getPpvEid() {
        return 0L;
    }

}

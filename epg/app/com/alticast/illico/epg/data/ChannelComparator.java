package com.alticast.illico.epg.data;

public interface ChannelComparator {

    /**
     * Compares two channels to check whether the first entry should be placed
     * ahead of the second entry in the iterator list.
     * @param a the first entry to compare.
     * @param b the second entry to compare.
     * @return positive integer if the first argument should be placed ahead of
     * the second argument; negative integer if the second argument should be
     * placed ahead of the first entry; zero if the current order should be
     * retained.
     */
    public int compare(Channel a, Channel b);

}

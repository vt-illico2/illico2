package com.alticast.illico.epg.data;

public interface ProgramFilter {

    /**
     * Tests if a particular program passes this filter.
     * @param program Program to be evaluated against the filtering algorithm.
     * @return true if entry satisfies the filtering algorithm; false otherwise.
     */
    public boolean accept(Program program);

}

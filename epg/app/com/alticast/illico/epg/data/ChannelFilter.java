package com.alticast.illico.epg.data;

public interface ChannelFilter {

    /**
     * Tests if a particular channel passes this filter.
     * @param channel Channel to be evaluated against the filtering algorithm.
     * @return true if entry satisfies the filtering algorithm; false otherwise.
     */
    public boolean accept(Channel channel);

}

package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ppv.PpvHistory;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import java.util.*;

public class ReminderManager implements NotificationListener {

    public static final int TYPE_PROGRAM       = 1;
    public static final int TYPE_PPV_ORDERABLE = 3;
    public static final int TYPE_PPV_ORDERED   = 4;

    NotificationService service;

    // for type 1 & 4
    // key = ch + start_time, value = [app_name, id]
    Hashtable programTable = new Hashtable();

    // key = ch + start_time, value = [app_name, id]
    Hashtable orderableTable = new Hashtable();

    private static ReminderManager instance = new ReminderManager();

    public static ReminderManager getInstance() {
        return instance;
    }

    public void init(NotificationService ns) {
        this.service = ns;
        storeDateToTable(App.NAME);
        storeDateToTable("PPV");
    }

    private void storeDateToTable(String name) {
        String[] ids = null;
        try {
             ids = service.setRegisterNotificaiton(name, this);
        } catch (Exception ex) {
            Log.print(ex);
        }
        for (int i = 0; ids != null && i < ids.length; i++) {
            String id = ids[i];
            try {
                NotificationOption o = service.getNotificationDetail(id);
                String[] data = o.getNotificationActionData();
                String key;
                if (data.length > 3) {
                    key = data[3];
                } else {
                    key = data[0] + '|' + o.getDisplayTime();
                }
                Hashtable table;
                if (data.length > 2 && String.valueOf(TYPE_PPV_ORDERABLE).equals(data[2])) {
                    Log.printDebug("ReminderManager: case orderable");
                    table = orderableTable;
                } else {
                    Log.printDebug("ReminderManager: case program");
                    table = programTable;
                }
                table.put(key, new String[] { name, id });
                if (Log.INFO_ON) {
                    Log.printInfo("ReminderManager: added " + name + " = " + key + ", " + id);
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void remind(Program p, ReminderListener l) {
        remind(p, TYPE_PROGRAM, l);
    }

    public void remindOrderable(PpvProgram p, ReminderListener l) {
        remind(p, TYPE_PPV_ORDERABLE, l);
    }

    public void remindOrdered(PpvProgram p, ReminderListener l) {
        remind(p, TYPE_PPV_ORDERED, l);
    }

    // for program
    private void remind(final Program p, final int type, final ReminderListener l) {
        if (!p.isFuture()) {
            Log.printInfo("ReminderManager: already started");
            return;
        }
        if (service == null) {
            Log.printError("ReminderManager: Not found NotificationService");
            return;
        }
        Log.printInfo("ReminderManager.remind = " + p + ", " + type);

        final Program dup = type != TYPE_PPV_ORDERABLE ? getProgram(p.getStartTime()) : null;

        if (dup == null) {
            remindImpl(p, type, l);
        } else {
            Log.printDebug("ReminderManager.remind: check duplicate = " + p);
            try {
                if (dup.getEpgChannel().equals(p.getEpgChannel())) {
                    remove(dup);
                    remindImpl(p, type, l);
                    return;
                }
            } catch (Exception ex) {
            }
            ReminderChangePopupRenderer r = new ReminderChangePopupRenderer(dup, p);
            String[] buttons = new String[] { Popup.BUTTON_YES, Popup.BUTTON_NO };

            PopupListener pl = new PopupListener() {
                public void notifySelected(Popup popup, String key) {
                    if (Popup.BUTTON_YES.equals(key)) {
                        remove(dup);
                        remindImpl(p, type, l);
                    }
                }
                public void notifyCanceled(Popup p, boolean byTimer) {
                }
            };
            PopupController.getInstance().showCustomPopup(
                    DataCenter.getInstance().getString("epg.reminder_change_popup_title"),
                    buttons, r, pl);
        }
    }

    private boolean remindImpl(Program p, int type, ReminderListener l) {
        boolean result;
        try {
            long delay = service.getDisplayTime();
            ReminderOption o = new ReminderOption(p, type, delay);
            String id = service.setNotify(o);
            String[] value = new String[] { o.getApplicationName(), id };
            String key = o.getNotificationActionData()[3];
            Log.printWarning("ReminderManager.remindImpl: key = " + key + ", " + id + ", " + type);
            if (type == TYPE_PPV_ORDERABLE) {
                orderableTable.put(key, value);
            } else {
                programTable.put(key, value);
            }
            result = true;
        } catch (Exception ex) {
            Log.print(ex);
            result = false;
        }
        if (l != null) {
            l.notifyReminderResult(p, result);
        }
        return result;
    }

    public boolean removeOrderable(PpvProgram o) {
        return removeImpl(o, orderableTable);
    }

    public boolean remove(AbstractProgram o) {
        return removeImpl(o, programTable);
    }

    public boolean removeImpl(AbstractProgram o, Hashtable table) {
        if (service == null) {
            Log.printWarning("ReminderManager: Not found NotificationService");
            return false;
        }
        String[] value = getContent(o, table);
        if (value == null) {
            Log.printWarning("ReminderManager.remove: Not found = " + o);
            return false;
        }
        String name = value[0];
        String id = value[1];
        Log.printInfo("ReminderManager.remove: " + o + ", name = " + name + ", id = " + id);
        try {
            service.removeNotify(id);
            table.remove(o.getCallLetter() + '|' + o.getStartTime());
            return true;
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
    }

//    public boolean remove(Object o) {
//        if (service == null) {
//            Log.printWarning("ReminderManager: Not found NotificationService");
//            // TODO - error
//            return false;
//        }
//        String id = getContent(o);
//        if (id == null) {
//            Log.printWarning("ReminderManager.remove: Not found ID = " + id);
//            // TODO - error
//            return false;
//        }
//        Log.printInfo("ReminderManager: remove " + o + ", id = " + id);
//        try {
//            service.removeNotify(id);
//            if (o instanceof Program) {
//                Program p = (Program) o;
//                if (p instanceof PpvProgram) {
//                    ppvTable.remove(p.getCallLetter() + '|' + p.getStartTime());
//                } else {
//                    programTable.remove(p.getCallLetter() + '|' + p.getStartTime());
//                }
//            } else if (o instanceof PpvHistory) {
//                PpvHistory h = (PpvHistory) o;
//                ppvTable.remove(h.channelName + '|' + h.startTime);
//            } else {
//                return false;
//            }
//            return true;
//        } catch (Exception ex) {
//            Log.print(ex);
//            return false;
//        }
//    }

    /** 해당 시간에 reminder 걸려있는 Program return. */
    private Program getProgram(long time) {
        Enumeration en = programTable.keys();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            try {
                int pos = key.lastIndexOf('|');
                if (pos >= 0) {
                    if (time == Long.parseLong(key.substring(pos+1))) {
                        String name = key.substring(0, pos);
                        Program p = EpgDataManager.getInstance().getProgram(
                                ChannelDatabase.current.getChannelByName(name), time);
                        if (p != null) {
                            return p;
                        }
                    }
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return null;
    }

    public boolean containsOrderable(PpvProgram p) {
        return getContent(p, orderableTable) != null;
    }

    public boolean contains(Program p) {
        return getContent(p, programTable) != null;
    }

    private String[] getContent(AbstractProgram p, Hashtable table) {
        if (p == null) {
            return null;
        }
        return (String[]) table.get(p.getCallLetter() + '|' + p.getStartTime());
    }

    private void removeFromTable(Hashtable table, String id) {
        Set set = table.entrySet();
        Iterator it = set.iterator();

        Vector toBeRemoved = new Vector();
        Map.Entry entry;
        while (it.hasNext()) {
            entry = (Map.Entry) it.next();
            String[] value = (String[]) entry.getValue();
            if (value != null && value.length > 1 && id.equals(value[1])) {
                toBeRemoved.addElement(entry.getKey());
                Log.printInfo("ReminderManager: to be removed = " + entry);
            }
        }
        Enumeration en = toBeRemoved.elements();
        while (en.hasMoreElements()) {
            table.remove(en.nextElement());
        }
    }

    /**
     * Event is started.
     * @param id messageID.
     * @throws RemoteException
     */
    public void action(String id) {
        Log.printInfo("ReminderManager: action = " + id);
        removeFromTable(programTable, id);
        removeFromTable(orderableTable, id);
    }

    /**
     * Event is canceled.
     * @param id
     * @throws RemoteException
     */
    public void cancel(String id) {
        Log.printInfo("ReminderManager: cancel = " + id);
        removeFromTable(programTable, id);
        removeFromTable(orderableTable, id);
    }

    public void directShowRequested(String platform, String[] action) {
        Log.printInfo("ReminderManager: directShowRequested.");
    }



}

package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Clock;
import java.util.*;

public class ProgramList {

    protected final String callLetter;

    public Program start;
    public Program end;

    Program current;

    /** Creates Program list with unsorted programs. size > 0 */
    public ProgramList(List list) {
        int size = list.size();
        Program[] array = new Program[size];
        array = (Program[]) list.toArray(array);
//        list.copyInto(array);
        Arrays.sort(array);

        start = array[0];
        Program p = start;
        this.callLetter = p.callLetter;
        for (int i = 1; i < size; i++) {
            Program x = array[i];
            if (p.endTime == x.startTime) {
                p.next = x;
                x.prev = p;
                p = x;
            } else if (p.endTime < x.startTime) {
                NullProgram np = new NullProgram(callLetter, p.endTime, x.startTime);
                p.next = np;
                np.next = x;
                x.prev = np;
                np.prev = p;
                p = x;
            } else {
                // skip
                if (false && Log.WARNING_ON) {
                    Log.printWarning("ProgramList: overlapped program: " + x);
                }
            }
        }
        end = p;
    }

    public ProgramList(String callLetter) {
        this(new NullProgram(callLetter));
    }

    protected ProgramList(Program p) {
        start = p;
        end = p;
        this.callLetter = p.callLetter;
    }

    /** 뒤에 붙인다. */
    public void append(Program p) {
        if (end == null) {
            start = p;
            end = p;
        } else {
            end.next = p;
            p.prev = end;
            end = p;
        }
    }

    /** 한꺼번에 붙인다. */
    public void addAll(ProgramList list) {
        addAll(list.start, list.end);
    }

    private synchronized void addAll(Program ls, Program le) {
//        Program ls = list.start;
//        Program le = list.end;
        long st = ls.startTime;
        long et = le.endTime;

        if (end.endTime == st) {
            // 끝에
            end.next = ls;
            ls.prev = end;
            end = le;
        } else if (end.endTime < st) {
            // 끝에 공백이 있다.
            if (end instanceof NullProgram) {
                // 기존 공백 늘이기
                end.next = ls;
                ls.prev = end;
                end.endTime = st;
            } else {
                // 공백 삽입
                NullProgram np = new NullProgram(callLetter, end.endTime, st);
                end.next = np;
                np.next = ls;
                ls.prev = np;
                np.prev = end;
            }
            end = le;
        } else if (et == start.startTime) {
            // 앞에
            le.next = start;
            start.prev = le;
            start = ls;
        } else if (et < start.startTime) {
            // 앞에 공백 존재
            if (start instanceof NullProgram) {
                // 기존 공백 늘이기
                le.next = start;
                start.prev = le;
                start.startTime = et;
            } else {
                // 공백 삽입
                NullProgram np = new NullProgram(callLetter, et, start.startTime);
                start.prev = np;
                np.prev = le;
                le.next = np;
                np.next = start;
            }
            start = ls;
        } else {
            // 겹치는 케이스
            // sp = new program's start와 중복되는 program
            Program sp = start;
            while (sp != null) {
                if (st < sp.endTime) {
                    break;
                }
                sp = sp.next;
            }
            // ep = new program's end와 중복되는 program
            Program ep = end;
            while (ep != null) {
                if (et > ep.startTime) {
                    break;
                }
                ep = ep.prev;
            }

            if (sp.startTime < st) {
                // 앞에 공백이 생겨셔 만든다.
                NullProgram np = new NullProgram(callLetter, sp.startTime, st);
                np.next = ls;
                ls.prev = np;
                ls = np;
                st = np.startTime;
            }
            Program spp = sp.prev;
            sp.prev = null; // gc
            if (spp == null) {
                // 맨 앞으로 갔다.
                start = ls;
            } else if (spp instanceof NullProgram && ls instanceof NullProgram) {
                // 앞에도 Null이니 ls는 버리자. => spp를 버리자
                ls.startTime = spp.startTime;
                Program sppp = spp.prev;
                if (sppp != null) {
                    sppp.next = ls;
                    ls.prev = sppp;
                } else {
                    start = ls;
                }
                spp.prev = null;
            } else {
                spp.next = ls;
                ls.prev = spp;
            }

            if (et < ep.endTime) {
                // 뒤에 공백이 생겨서 만든다.
                NullProgram np = new NullProgram(callLetter, et, ep.endTime);
                np.prev = le;
                le.next = np;
                le = np;
                et = np.endTime;
            }
            Program epn = ep.next;
//            flushAllNext(ep);
            ep.next = null; // gc
            if (epn == null) {
                // 맨 뒤로 갔다.
                end = le;
            } else if (epn instanceof NullProgram && le instanceof NullProgram) {
                // 뒤에도 Null이니 le는 버리자.
                epn.startTime = le.startTime;
                epn.prev = le.prev;
                if (le.prev != null) {
                    le.prev.next = epn;
                } else {
                    start = epn;  // 이럴 조건은 없다. list size > 0 이기 때문에, 앞에서 맞추고 들어오기 때문
                }
            } else {
                epn.prev = le;
                le.next = epn;
            }
            // for gc
            Program gp = sp;
            while (gp != null) {
                sp = gp;
                gp = gp.next;
                sp.prev = null;
                sp.next = null;
            }
        }
        long cur = System.currentTimeMillis();
        if (st <= cur && cur < et) {
            resetCurrent();
        }
    }

    void flushAllPrev(Program p) {
        Program x = p.prev;
        while (x != null) {
            p.prev = null;
            x.next = null;
            p = x;
            x = p.prev;
        }
    }

    void flushAllNext(Program p) {
        Program x = p.next;
        while (x != null) {
            p.next = null;
            x.prev = null;
            p = x;
            x = p.next;
        }
    }

    // TODO -reset 하기 전에 미묘하게 늦을 수 있다. !!!
    Program getCurrent() {
        // NOTE - 프로그램이 없는 경우 계속 호출되는 부담을 고려. 어쩔 수 없다.
        if (current == null) {
            resetCurrent();
        }
        return current;
    }

    public synchronized Program resetStart(long oldestTime) {
        if (oldestTime < start.startTime) { // data 앞에 공백이 있는 경우
            if (start instanceof NullProgram) {
                start.startTime = oldestTime;
            } else {
                NullProgram np = new NullProgram(callLetter, oldestTime, start.startTime);
                np.next = start;
                start.prev = np;
                start = np;
            }
            return start;
        }
        Program p = start;
        while (p != null && p.endTime <= oldestTime) {
            p = p.next;
        }
        if (p != null) {
            start = p;
            flushAllPrev(start);
//            start.prev = null;
        }
        return p;
    }

    public synchronized Program resetLast(long lastTime) {
        if (lastTime < start.startTime) {
            // 이런 일이 일어나면 안됨...
            return start;
        }
        boolean filled = fill(lastTime);
        if (filled) {
            // 모자라서 채웠으니 필요 없다.
            return end;
        }
        Program p = search(lastTime - 1, end);
        if (p == null) {
            // 이런 일이 일어나면 안됨...
            return end;
        }
        end = p;
        flushAllNext(end);
        return end;
    }

    public synchronized void flush(long st, long et) {
        if (end.endTime <= st) {
            return;
        } else if (et <= start.startTime) {
            return;
        } else {
            // 겹치는 케이스
            // sp = new program's start와 중복되는 program
            Program sp = start;
            while (sp != null) {
                if (st < sp.endTime) {
                    break;
                }
                sp = sp.next;
            }
            // ep = new program's end와 중복되는 program
            Program ep = end;
            while (ep != null) {
                if (et > ep.startTime) {
                    break;
                }
                ep = ep.prev;
            }

            Program spp;
            if (sp.startTime < st) {
                spp = sp;
                sp = sp.next;
            } else {
                spp = sp.prev;
            }
//            if (spp == null) {
//                start = new NullProgram(st, et);
//            } else {
//
//            }

            Program epn;
            if (et < ep.endTime) {
                epn = ep;
                ep = ep.prev;
            } else {
                epn = ep.next;
            }

            if (sp == null || ep == null || ep.endTime <= sp.startTime) {
                return;
            }

            NullProgram np = new NullProgram(callLetter, sp.startTime, ep.endTime);
            sp.prev = null;
            if (spp == null) {
                start = np;
            } else {
                spp.next = np;
                np.prev = spp;
            }
            ep.next = null;
            if (epn == null) {
                end = np;
            } else {
                epn.prev = np;
                np.next = epn;
            }
            // for gc
            Program gp = sp;
            while (gp != null) {
                gp.prev = null;
                sp = gp;
                gp = gp.next;
                sp.next = null;
            }
        }
        long cur = System.currentTimeMillis();
        if (st <= cur && cur < et) {
            resetCurrent();
        }
    }

    public Program resetCurrent() {
        return resetCurrent(Clock.getRoundedMillis());
    }

    public synchronized Program resetCurrent(long time) {
        // 앞 프로그램이 순서대로 존재한다는 가정이 필요.
        Program p = start;
        while (p != null && !p.contains(time)) {
            p = p.next;
        }
        current = p;
        return p;
    }

    public Program search(long time, Program startPoint) {
        if (startPoint == null) {
            startPoint = start;
        }

        Program p = startPoint;
        if (p.endTime <= time) {
            while (p != null && !p.contains(time)) {
                p = p.next;
            }
            return p;
        } else if (p.startTime > time) {
            while (p != null && !p.contains(time)) {
                p = p.prev;
            }
            return p;
        } else {
            return p;
        }
    }

    public Program search(long time) {
        if (current == null || current instanceof NullProgram) {
            return search(time, start);
        } else {
            return search(time, current);
        }
    }

    public synchronized boolean fill(long to) {
        if (end.endTime < to) {
            if (end instanceof NullProgram) {
                end.endTime = to;
            } else {
                NullProgram np = new NullProgram(callLetter, end.endTime, to);
                end.next = np;
                np.prev = end;
                end = np;
            }
            return true;
        }
        return false;
    }

    public void printList() {
        if (Log.DEBUG_ON) {
            Log.printDebug("========== ProgramList from start ======");
            Program p = start;
            while (p != null) {
                Log.printDebug("ProgramList: " + p);
                p = p.next;
            }
        }
    }

    public String toString() {
        return "ProgramList(" + callLetter + ")";
    }
}



package com.alticast.illico.epg.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Date;

import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.guide.ProgramEvent;

import org.ocap.net.OcapLocator;
import org.ocap.shared.dvr.*;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.pvr.*;

/**
 * 녹화물 AppData 관련
 *
 * @author  June Park
 */
public class AppData implements AppDataKeys {

    public static final Integer TO_BE_DELETED = new Integer(Integer.MIN_VALUE);

    public static void set(RecordingRequest req, String key, Serializable value) {
        try {
            req.addAppData(key, value);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static void remove(RecordingRequest req, String key) {
        try {
            req.removeAppData(key);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static Serializable get(RecordingRequest req, String name) {
        return req.getAppData(name);
    }

    public static int getInteger(RecordingRequest req, String name) {
        try {
            return ((Integer) req.getAppData(name)).intValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static long getLong(RecordingRequest req, String name) {
        try {
            return ((Long) req.getAppData(name)).longValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getString(RecordingRequest req, String name) {
        try {
            return req.getAppData(name).toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean getBoolean(RecordingRequest req, String name) {
        try {
            return ((Boolean) req.getAppData(name)).booleanValue();
        } catch (Exception ex) {
            return false;
        }
    }

    public static long getScheduledStartTime(RecordingRequest req) {
        if (req == null) {
            return 0;
        }
        try {
            RecordingSpec spec = req.getRecordingSpec();
            if (spec instanceof LocatorRecordingSpec) {
                return ((LocatorRecordingSpec) spec).getStartTime().getTime();
            } else if (spec instanceof ServiceContextRecordingSpec) {
                return ((ServiceContextRecordingSpec) spec).getStartTime().getTime();
            } else if (spec instanceof ServiceRecordingSpec) {
                return ((ServiceRecordingSpec) spec).getStartTime().getTime();
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public static long getScheduledDuration(RecordingRequest req) {
        if (req == null) {
            return 0;
        }
        try {
            RecordingSpec spec = req.getRecordingSpec();
            if (spec instanceof LocatorRecordingSpec) {
                return ((LocatorRecordingSpec) spec).getDuration();
            } else if (spec instanceof ServiceContextRecordingSpec) {
                return ((ServiceContextRecordingSpec) spec).getDuration();
            } else if (spec instanceof ServiceRecordingSpec) {
                return ((ServiceRecordingSpec) spec).getDuration();
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public static int getFailedReason(RecordingRequest req) {
        int reason = getInteger(req, FAILED_REASON);
        if (reason != 0) {
            return reason;
        }
        LeafRecordingRequest r = (LeafRecordingRequest) req;
        // 실패 원인
        String exception = null;
        try {
            // RecordingFailedException
            RecordingFailedException ex = (RecordingFailedException) r.getFailedException();
            if (ex != null) {
                reason = ex.getReason();
                AppData.set(r, AppData.FAILED_REASON, new Integer(reason));
                return reason;
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public static boolean isSingleProgram(int type) {
        switch (type) {
            case TvProgram.TYPE_MOVIE:
            case TvProgram.TYPE_EVENT:
            case TvProgram.TYPE_UNKNOWN:
                return true;
            default:
                return false;
        }
    }

    public static void print(RecordingRequest req) {
        if (Log.DEBUG_ON) {
            String[] keys = req.getKeys();
            if (keys == null || keys.length <= 0) {
                Log.printDebug("AppData: no appdata keys");
            } else {
                for (int i = 0; i < keys.length; i++) {
                    Log.printDebug("AppData: appdata[" + i + "] : " + keys[i] +  " = " + get(req, keys[i]));
                }
            }
        }
    }

}

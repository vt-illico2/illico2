package com.alticast.illico.epg.data;

import com.videotron.tvi.illico.framework.DataCenter;

public class Genre {

    private static DataCenter dataCenter = DataCenter.getInstance();

    public static String getName(int code) {
        String s = dataCenter.getString("Categories." + code);
        if (s.length() > 0) {
            return s;
        } else {
            return dataCenter.getString("Categories.0");
        }
    }

}

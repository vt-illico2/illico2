package com.alticast.illico.epg.data;

public interface AbstractProgram  {

    public String getCallLetter();

    public long getStartTime();

    public long getEndTime();

    public int getRatingIndex();

    public String getId();

    public boolean contains(long time);

}

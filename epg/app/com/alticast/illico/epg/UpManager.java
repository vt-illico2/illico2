package com.alticast.illico.epg;

import java.rmi.*;
import java.util.*;

import com.alticast.illico.epg.navigator.VolumeController;
import com.alticast.illico.epg.data.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.*;

/**
 * UpManager.
 */
public class UpManager implements PreferenceListener {
    //->Kenneth [2016.1.21] DDC-115
    // 처음의 딱 한 번만 audio description 을 off 시켰는지 여부 판단하는 flag
    // 다음 부팅부터는 audio description 을 off 시키지 않아야 하기 때문에 추가함.
    public static final String ONE_TIME_AUDIO_DESCRIPTION_OFF = "ONE_TIME_AUDIO_DESCRIPTION_OFF";
    //<-

    //Kenneth[2016.7.8] : R7.2 sticky filter
    public static final String LAST_CHANNEL_FILTER = "LAST_CHANNEL_FILTER";
    //<-

    private static final String[] EPG_NAMES = {
        PreferenceNames.LANGUAGE,
        PreferenceNames.FAVOURITE_CHANNEL,
        RightFilter.CHANNEL_RESTRICTION,
        PreferenceNames.PIP_POSITION,
        PreferenceNames.PIP_SIZE,
        PreferenceNames.POWER_ON_CHANNEL,
        PreferenceNames.GRID_FORMAT,
        PreferenceNames.FAVOURITE_CHANNEL_FILTER,
        PreferenceNames.AUDIO_VOLUME_CONTROL,
        PreferenceNames.DISPLAY_POWER_OFF,
        PreferenceNames.AUTO_SHOW_DESCIRIPTION,
        PreferenceNames.CC_DISPLAY,
        PreferenceNames.VIDEO_ZOOM_MODE,
        PreferenceNames.DESCRIBED_AUDIO_DISPLAY,
        PreferenceNames.SAP_LANGUAGE_AUDIO,
        RightFilter.PIN_CODE_UNBLOCKS_CHANNEL,
        RightFilter.PARENTAL_CONTROL,
        RightFilter.BLOCK_BY_RATINGS,
        RightFilter.HIDE_ADULT_CONTENT,
        RightFilter.SUSPEND_DEFAULT_TIME,
        RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
        VolumeController.KEY_HDMI_CHECK_COUNT,
        //->Kenneth [2015.2.5] 4K
        // monitor 에서 Preference(세팅앱) 에 값 넣으면 그것을 받기 위한 코드
        MonitorService.SUPPORT_UHD,
        //<-
        //->Kenneth [2016.1.21] DDC-115
        ONE_TIME_AUDIO_DESCRIPTION_OFF,
        //<-

        //->Kenneth [2016.3.7] R7
        PreferenceNames.SD_HD_CHANNELS_GROUPING,
        //<-

        //->Kenneth [2016.5.27] Log capture tool 때문에 추가
        // Log capture 하려는 모든 App 은 아래의 preference 를 등록하여야 한다.
        // 그에 관해 event 받는 것은 framework 의 Log.java 에 구현되어 있다.
        Log.LOG_CAPTURE_APP_NAME,
        //<-

        //Kenneth[2016.7.8] : R7.2 sticky filter
        LAST_CHANNEL_FILTER,
        //<-

        //Kenneth[2017.2.22] : R7.3 Highlight 를 UpMenu 로 관리
        Highlight.PREF_KEY,
        //<-
    };

    private PreferenceService service;

    private static UpManager instance = new UpManager();

    public static UpManager getInstance() {
        return instance;
    }

    public UpManager() {
        LastChannelSaveHandler.getInstance();
    }

    public void init(PreferenceService ps) {
        this.service = ps;
        String[] values = null;
        try {
            values = service.addPreferenceListener(this,
                FrameworkMain.getInstance().getApplicationName(), EPG_NAMES, null);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (values == null || values.length < EPG_NAMES.length) {
            return;
        }
        for (int i = 0; i < EPG_NAMES.length; i++) {
            if (values[i] != null) {
                Log.printInfo("UpManager.init: " + EPG_NAMES[i] + " = " + values[i]);
                DataCenter.getInstance().put(EPG_NAMES[i], values[i]);
                //->Kenneth[2016.3.7] R7
                if (PreferenceNames.SD_HD_CHANNELS_GROUPING.equals(EPG_NAMES[i])) {
                    if (Definitions.SD_HD_CHANNEL_GROUPED.equals(values[i])) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.init() : set isChannelGrouped = true");
                        EpgCore.isChannelGrouped = true;
                    } else if (Definitions.SD_HD_CHANNEL_UNGROUPED.equals(values[i])) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.init() : set isChannelGrouped = false");
                        EpgCore.isChannelGrouped = false;
                    }
                }
                //<-
            }
        }
    }

    public boolean set(String name, String value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.set("+name+", "+value+")");
        try {
            return service.setPreferenceValue(name, value);
        } catch (Exception ex) {
            Log.print(ex);
           return false;
        }
    }

    //Kenneth[2016.7.8] : R7.2 sticky filter
    // get 하는 것도 만든다. LAST_CHANNEL_FILTER 을 부팅하면서 꺼내오기 위해서임.
    public String get(String name) {
        String value = null;
        try {
            value = service.getPreferenceValue(name);
        } catch (Exception e) {
            Log.print(e);
        }
        return value;
    }
    //<- 

    /** PreferenceListener. */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.receiveUpdatedPreference("+name+", "+value+")");
        if (value != null) {
            DataCenter.getInstance().put(name, value);
            //->Kenneth [2015.2.5] 4K
            if ("SUPPORT_UHD".equals(name)) {
                if ("true".equals(value)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.receiveUpdatedPreference() : set SUPPORT_UHD = true");
                    Environment.SUPPORT_UHD = true;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.receiveUpdatedPreference() : set SUPPORT_UHD = false");
                    Environment.SUPPORT_UHD = false;
                }
            }

            //->Kenneth[2016.3.7] R7
            if (PreferenceNames.SD_HD_CHANNELS_GROUPING.equals(name)) {
                if (Definitions.SD_HD_CHANNEL_GROUPED.equals(value)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.receiveUpdatedPreference() : set isChannelGrouped = true");
                    EpgCore.isChannelGrouped = true;
                } else if (Definitions.SD_HD_CHANNEL_UNGROUPED.equals(value)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.receiveUpdatedPreference() : set isChannelGrouped = false");
                    EpgCore.isChannelGrouped = false;
                }
            }
            //<-
        }
    }

    public boolean changeFavoriteChannel(Channel ch, boolean add) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.changeFavoriteChannel("+ch+", "+add+")");
        return changeChannelList(PreferenceNames.FAVOURITE_CHANNEL, ch.getName(), add);
    }

    public boolean changeBlockedChannel(Channel ch, boolean add) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.changeBlockedChannel("+ch+", "+add+")");
        return changeChannelList(RightFilter.CHANNEL_RESTRICTION, ch.getName(), add);
    }

    private boolean changeChannelList(String upKey, String name, boolean add) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UpManager.changeChannelList("+name+", "+add+")");
        DataCenter dataCenter = DataCenter.getInstance();
        String list = dataCenter.getString(upKey);
        String newList = add ? addItem(list, name) : removeItem(list, name);
        dataCenter.put(upKey, newList);
        //->Kenneth [R4.1] sister block
        if (upKey.equals(RightFilter.CHANNEL_RESTRICTION)) {
            try {
                if (add) {
                    Log.printDebug(App.LOG_HEADER+"Call PreferenceService.addBlockChannel("+name+")");
                    service.addBlockChannel(name);
                } else {
                    Log.printDebug(App.LOG_HEADER+"Call PreferenceService.removeBlockChannel("+name+")");
                    service.removeBlockChannel(name);
                }
                return true;
            } catch (Exception e) {
                Log.print(e);
                return false;
            }
        //->Kenneth[2015.6.2] R5 : 우정 조언에 따라 여기에 추가
        } else if (upKey.equals(PreferenceNames.FAVOURITE_CHANNEL)) {
            try {
                if (add) {
                    Log.printDebug(App.LOG_HEADER+"Call PreferenceService.addFavoriteChannel("+name+")");
                    service.addFavoriteChannel(name);
                } else {
                    Log.printDebug(App.LOG_HEADER+"Call PreferenceService.removeFavoriteChannel("+name+")");
                    service.removeFavoriteChannel(name);
                }
                return true;
            } catch (Exception e) {
                Log.print(e);
                return false;
            }//<-
        }
        return set(upKey, newList);
    }

    public static String addItem(String list, String str) {
        if (list == null || list.length() == 0) {
            return str;
        }
        return list + PreferenceService.PREFERENCE_DELIMETER + str;
    }

    public static String removeItem(String list, String str) {
        if (list == null || list.length() == 0 || list.equals(str)) {
            return "";
        }
        String l = PreferenceService.PREFERENCE_DELIMETER + str;
        String r = str + PreferenceService.PREFERENCE_DELIMETER;
        int p = list.indexOf(l);
        if (p >= 0) {
            return list.substring(0, p) + list.substring(p + l.length());
        }
        p = list.indexOf(r);
        if (p >= 0) {
            return list.substring(0, p) + list.substring(p + r.length());
        }
        p = list.indexOf(str);
        if (p >= 0) {
            return list.substring(0, p) + list.substring(p + str.length());
        }
        return list;
    }

}

package com.alticast.illico.epg;

import java.util.*;
import javax.tv.util.*;
import com.opencable.handler.nethandler.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.adapter.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.ui.*;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import java.awt.*;
import java.awt.event.*;
import org.dvb.event.*;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingManager;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;
import org.dvb.user.*;

public class UserActivityChecker implements LayeredKeyHandler, TVTimerWentOffListener, ChannelEventListener {

    TVTimerSpec luaTimer;

    boolean enabled = false;
    boolean scheduled = false;

    public static long lastUserActivity;
    private int sourceId;
    private boolean action = false;

    private int[] keyCodes;
    private int diagInputStep = 0;

    //->Kenneth[2015.10.9] : **00 대신할 키코드를 추가함. '*' 가 audio description 으로 사용되면서 불편해짐.
    // 258000 에 맞는 키코드를 사용하자.
    //private int[] keyCodes2 = {50, 53, 56, 48, 48, 48};
    // 25558444000 에 맞는 키코드를 사용하자. (리모컨에서 alti 누른 것과 동일)
    private int[] keyCodes2 = {50, 53, 53, 53, 56, 52, 52, 52, 48, 48, 48};
    private int diagInputStep2 = 0;
    //<-

    private static UserActivityChecker instance = new UserActivityChecker();

    public static UserActivityChecker getInstance() {
        return instance;
    }

    private UserActivityChecker() {
        LayeredUI handler = WindowProperty.SDV_USER_ACTIVITY.createLayeredKeyHandler(this);
        handler.activate();

        luaTimer = new TVTimerSpec();
        luaTimer.setDelayTime(Constants.MS_PER_MINUTE);
        luaTimer.setRepeat(false);
        luaTimer.addTVTimerWentOffListener(this);

        if (Environment.EMULATOR) {
            keyCodes = new int[] { OCRcEvent.VK_F9, OCRcEvent.VK_F9, OCRcEvent.VK_0, OCRcEvent.VK_0 };
        } else {
            try {
                String[] keys = TextUtil.tokenize(DataCenter.getInstance().getString("DIAG_KEY_CODES"), ',');
                int[] array = new int[keys.length];
                for (int i = 0; i < keys.length; i++) {
                    array[i] = Integer.parseInt(keys[i]);
                }
                keyCodes = array;
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (keyCodes == null || keyCodes.length == 0) {
                keyCodes = new int[] { OCRcEvent.VK_ASTERISK };
            }
        }
    }

    public boolean handleKeyEvent(UserEvent e) {
        //->Kenneth [R4.1] : Call RebootChecker
        RebootChecker.getInstance().checkKeyEvent(e);
        //<-
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        lastUserActivity = e.getWhen();
        if (enabled) {
            action = true;
            if (!scheduled) {
                synchronized (this) {
                    Log.printInfo("UserActivityChecker.send LUA by key event");
                    SDVController.getInstance().sendUserActivity(sourceId, lastUserActivity);
                    resetTimer();
                }
            }
        }
        int code = e.getCode();
        if (code == keyCodes[diagInputStep]) {
            diagInputStep++;
            if (diagInputStep >= keyCodes.length) {
                diagInputStep = 0;
                DiagWindow.getInstance().start(true);
            }
        } else {
            diagInputStep = 0;
        }
        //->Kenneth[2015.10.9]
        //->Kenneth[2016.7.4] Lab 과 Qualif 에서만 동작하게 함. 보안의 이유로
        //->Kenneth[2017.3.29] VDTRMASTER-5703 테스트를 위해서 잠시 lab/qualif 체크 막음.
        //if ("LAB".equals(EpgCore.dncsName) || "QUALIF".equals(EpgCore.dncsName)) {
            if (code == keyCodes2[diagInputStep2]) {
                diagInputStep2++;
                if (diagInputStep2 >= keyCodes2.length) {
                    diagInputStep2 = 0;
                    DiagWindow.getInstance().start(false);
                }
            } else {
                diagInputStep2 = 0;
            }
        //}
        //<-

        //->Kenneth[2016.1.18] DDC-115 : EXIT 의 키의 경우 EpgCore 에 바로 보냄.
        // EpgCore 까지는 Exit 키가 전달되지 않으므로 여기서 바로 호출함.
        if (code == OCRcEvent.VK_EXIT) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UserActivityChecker : Call EpgCore.notifyExitKeyPressed(EXIT)");
            //->Kenneth[2017.3.27] R7.3 : VDTRMASTER-6048 : boolean 값을 return 하도록 함수형태를 바꾼다.
            boolean exitConsumed = EpgCore.getInstance().notifyExitKeyPressed(code);
            if (exitConsumed) {
                DebugScreen.getInstance().checkKeyCode(code);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"UserActivityChecker : return true");
                return true;
            }
            //EpgCore.getInstance().notifyExitKeyPressed(code);
            //<-
        } else {
            EpgCore.lastKeyPressedBeforeExit = code;
        }
        //<-

        //->Kenneth[2016.7.19] R7.2 : on-screen debug 
        DebugScreen.getInstance().checkKeyCode(code);
        //<-
        return false;
    }

    public void setTimerDelay(long delay) {
        luaTimer.setDelayTime(delay);
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            ChannelController.get(0).addChannelEventListener(this);
        }
    }

    private synchronized void resetTimer() {
        Log.printInfo(App.LOG_HEADER+"UserActivityChecker.resetTimer");
        if (lastUserActivity == 0) {
            Log.printInfo(App.LOG_HEADER+"UserActivityChecker: skip timer");
            return;
        }
        // timer
        TVTimer.getTimer().deschedule(luaTimer);
        try {
            TVTimer.getTimer().scheduleTimerSpec(luaTimer);
        } catch (Exception ex) {
        }
        scheduled = true;
        action = false;
    }

    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        Log.printInfo(App.LOG_HEADER+"UserActivityChecker.timerWentOff");
        scheduled = false;
        if (action) {
            Log.printInfo(App.LOG_HEADER+"UserActivityChecker.send LUA by timer");
            SDVController.getInstance().sendUserActivity(sourceId, lastUserActivity);
            resetTimer();
        }
    }

    public void selectionRequested(int id, short type) {
        sourceId = Math.abs(id);
        lastUserActivity = System.currentTimeMillis();
        resetTimer();
        action = true;
    }

    public void selectionBlocked(int id, short type) {
        sourceId = Math.abs(id);
        lastUserActivity = System.currentTimeMillis();
        resetTimer();
        action = true;
    }

}

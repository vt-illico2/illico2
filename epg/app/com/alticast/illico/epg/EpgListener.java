package com.alticast.illico.epg;

public interface EpgListener {

    int ADDED_TO_RESTRICTED     = 0;
    int REMOVED_FROM_RESTRICTED = 1;
    int ADDED_TO_FAVOURITES     = 2;
    int REMOVED_FROM_FAVOURITES = 3;
    int FILTER_WILL_BE_CHANGED  = 4;
    int FILTER_CHANGED          = 5;

    void notifyEpgEvent(int id, Object data);

}

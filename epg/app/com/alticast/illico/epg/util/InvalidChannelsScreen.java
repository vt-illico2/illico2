package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.App;
import com.alticast.illico.epg.EpgCore;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;

public class InvalidChannelsScreen extends DiagScreen {

    public static final int ROW_SIZE = 22;
    public static final int PAGE_SIZE = ROW_SIZE * 3;
    private static final int ROW_GAP = 19;
    private static final int COL_GAP = 300;
    private static final int Y1 = 83;
    private static final int[] X_POS = { 50, 78, 120, 180, 225 };
    private static final String[] LABLES = { "", "No.", "Name", "SID", "Type"};

    ChannelList list;

    public InvalidChannelsScreen(Color background) {
        super("Invalid Channels", background);
    }

    public void updateScreen() {
        ChannelDatabase db = ChannelDatabase.current;
        ChannelList list = null;
        if (db != null) {
            if (App.EMULATOR) {
                list = db.getAllChannelList();
            } else {
                list = db.getInvalidChannelList();
            }
        }
        this.list = list;
        if (list != null) {
            totalPage = (list.size() - 1) / PAGE_SIZE + 1;
        }
        currentPage = 0;
    }

    public int getCount() {
        if (list != null) {
            return list.size();
        } else {
            return 0;
        }
    }

    public boolean showNextPage() {
        if (list != null) {
            totalPage = (list.size() - 1) / PAGE_SIZE + 1;
        }
        int nextPage = currentPage + 1;
        if (nextPage < totalPage) {
            currentPage = nextPage;
            repaint();
            return true;
        }
        return false;
    }

    public boolean showPreviousPage() {
        if (currentPage > 0) {
            currentPage--;
            repaint();
            return true;
        }
        return false;
    }

    public ArrayList getLogs() {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                Channel ch = list.getChannelAt(i);
                arrayList.add("" + ch.getNumber() + DELIMITER + ch.getName() + DELIMITER + ch.getSourceId() + DELIMITER + ch.getTypeString());
            }
        }
        return arrayList;
    }

    protected void paintBody(Graphics g) {
        int y = Y1;
        g.setColor(Color.cyan);
        for (int c = 0; c < 3; c++) {
            for (int x = 1; x < X_POS.length; x++) {
                g.drawString(LABLES[x], X_POS[x] + c * COL_GAP, y);
            }
        }
        if (list == null) {
            return;
        }

        int i = PAGE_SIZE * currentPage;
        int sx = 0;
        for (int c = 0; c < 3; c++) {
            y = Y1 + ROW_GAP;
            for (int r = 0; r < ROW_SIZE; r++) {
                if (i >= getCount()) {
                    return;
                }
                paintChannel(g, list.getChannelAt(i), i, sx, y);
                y += ROW_GAP;
                i++;
            }
            sx += COL_GAP;
        }
    }

    private void paintChannel(Graphics g, Channel ch, int i, int sx, int y) {
        int x = 0;
        g.setColor(Color.green);
        g.drawString(String.valueOf(i + 1), sx + X_POS[x++], y);
        g.setColor(Color.white);
        g.drawString(String.valueOf(ch.getNumber()), sx + X_POS[x++], y);
        g.drawString("" + ch.getName(), sx + X_POS[x++], y);
        g.drawString(String.valueOf(ch.getSourceId()), sx + X_POS[x++], y);
        g.drawString(ch.getTypeString(), sx + X_POS[x++], y);
    }

}


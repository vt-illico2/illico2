package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.EpgCore;
import com.videotron.tvi.illico.ixc.vbm.*;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;

public class VbmLogScreen extends DiagScreen {
    LinkedList list;

    public VbmLogScreen(Color background) {
        super("Action Logs", background);
        list = new FixedSizeList(22);
        SharedMemory.getInstance().put(SharedDataKeys.VBM_DEBUG_LOGS, list);
        list.addFirst("test log - EPG");
    }

    public int getCount() {
        if (list != null) {
            return list.size();
        } else {
            return -2;
        }
    }

    protected void paintBody(Graphics g) {
        if (list == null) {
            return;
        }
        int y = 95;
        int size = getCount();
        int lenOfLong = Long.toString(System.currentTimeMillis()).length();
        for (int i = 0; i < size && i < 22; i++) {
            String s = null, d = null, tmp = null;
            try {
                s = list.get(i).toString();
            } catch (Exception ex) {
                s = "";
            }
            StringTokenizer tok = new StringTokenizer(s, "/");
            tok.nextToken(); // remove MID
            try {
            	tmp = tok.nextToken(); // try convert by second value
	            if (lenOfLong == tmp.length()) {
	            	d = new Date(Long.parseLong(tmp)).toString();
	            }
            } catch (Exception ex) {}
            if (d == null) {
            	try {
            		tmp = tok.nextToken(); // try convert by third value
    	            if (lenOfLong == tmp.length()) {
    	            	d = new Date(Long.parseLong(tmp)).toString();
    	            }
                } catch (Exception ex) {}
            }
            paintLine(g, i, s, d, y);
            y += 20;
        }
    }

    private void paintLine(Graphics g, int index, String s, String d, int y) {
        g.setColor(Color.green);
        g.drawString(String.valueOf(index), 50, y);
        g.setColor(Color.white);
        g.drawString(s, 80, y);
        if (d != null) {
        	g.drawString(d, 570, y);
        }
    }
}


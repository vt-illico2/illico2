package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.EpgCore;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;

public class AdapterDebugScreen extends DiagScreen {
    Hashtable updateStatusTable;
    String key;

    public AdapterDebugScreen(String key, String title, Color background) {
        super(title, background);
        this.key = key;
        this.title = title;
        updateStatusTable = (Hashtable) SharedMemory.getInstance().get(key);

        if (updateStatusTable != null) {
            totalPage = (updateStatusTable.size() - 1) / ROW_SIZE + 1;
        }
        currentPage = 0;
    }

    public int getCount() {
        if (updateStatusTable != null) {
            return updateStatusTable.size();
        } else {
            return 0;
        }
    }

    public boolean showNextPage() {
        if (updateStatusTable != null) {
            totalPage = (updateStatusTable.size() - 1) / ROW_SIZE + 1;
        }
        int nextPage = currentPage + 1;
        if (nextPage < totalPage) {
            currentPage = nextPage;
            repaint();
            return true;
        }
        return false;
    }

    public boolean showPreviousPage() {
        if (currentPage > 0) {
            currentPage--;
            repaint();
            return true;
        }
        return false;
    }

    public ArrayList getLogs() {
        ArrayList list = new ArrayList();
        Enumeration en = updateStatusTable.elements();
        while (en.hasMoreElements()) {
            Object[] info = (Object[]) en.nextElement();
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < info.length; i++) {
                sb.append(info[i]);
                sb.append(DELIMITER);
            }
            list.add(sb.toString());
        }
        return list;
    }

    protected void paintBody(Graphics g) {
        Object[] error = (Object[]) SharedMemory.getInstance().get(Log.LAST_ERROR_LOG);
        if (error != null) {
            paintLine(g, Color.orange, Color.orange, error, 100 + 20 * ROW_SIZE);
        }

        if (updateStatusTable == null) {
            return;
        }
        int y = 95;
        int skipCount = currentPage * ROW_SIZE;
        int row = 0;
        Enumeration en = updateStatusTable.elements();
        while (en.hasMoreElements() && row < ROW_SIZE) {
            Object[] info = (Object[]) en.nextElement();
            if (skipCount > 0) {
                skipCount--;
                continue;
            }
            paintLine(g, Color.yellow, Color.white, info, y);
            row++;
            y += 20;
        }
    }

    private void paintLine(Graphics g, Color c1, Color c2, Object[] info, int y) {
        g.setColor(c1);
        String name = (String) info[0];
        int pos = name.indexOf("VBM");
        if (pos > 0) {
            StringBuffer sb = new StringBuffer();
            sb.append(name.substring(0, pos));
            sb.append("Logging");
            sb.append(name.substring(pos + 3));
            name = sb.toString();
        }
        g.drawString(name, 50, y);
        g.setColor(Color.white);
        g.drawString("c = " + info[1], 260, y);
        g.setColor(Color.yellow);
//        g.drawString((String) info[2], 310, y);
        GraphicUtil.drawStringRight(g, (String) info[2], 417, y);
        g.setColor(c2);
        g.drawString((String) info[3], 425, y);
    }
}


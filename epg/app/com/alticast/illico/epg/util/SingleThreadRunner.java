package com.alticast.illico.epg.util;

import java.util.Date;
import com.videotron.tvi.illico.log.Log;

public class SingleThreadRunner implements Runnable {

    private Runnable target;
    private String threadName;

    private Thread current;
    private boolean hasNext;

    public SingleThreadRunner(Runnable runnable, String name) {
        this.target = runnable;
        this.threadName = name;
    }

    public synchronized void start() {
        if (current != null) {
            hasNext = true;
            if (Log.DEBUG_ON) {
                Log.printDebug("SingleThreadRunner: thread is running.");
            }
        } else {
            startNewThread();
        }
    }

    private void startNewThread() {
        current = new Thread(this, threadName + "_" + new Date());
        current.start();
        hasNext = false;
    }

    public void run() {
        try {
            target.run();
        } catch (Exception ex) {
        }
        synchronized (this) {
            current = null;
            if (hasNext) {
                startNewThread();
            }
        }
    }

}


package com.alticast.illico.epg.util;

import java.util.*;
import com.videotron.tvi.illico.log.Log;

public class RangeList extends Vector {

    public void add(long from, long to) {
        add(new Range(from, to));
    }

    public synchronized void remove(long from, long to) {
        ArrayList toBeRemoved = new ArrayList();
        ArrayList toBeAdded = new ArrayList();
        for (int i = 0; i < elementCount; i++) {
            Range r = (Range) elementData[i];
            if (r.intersects(from, to)) {
                boolean flushBefore = from <= r.from;
                boolean flushAfter = r.to <= to;
                if (flushBefore && flushAfter) {
                    toBeRemoved.add(r);
                } else if (flushBefore) {
                    r.from = to;
                } else if (flushAfter) {
                    r.to = from;
                } else {
                    long oldTo = r.to;
                    r.to = from;
                    toBeAdded.add(new Range(to, oldTo));
                }
            }
        }
        for (int i = 0; i < toBeRemoved.size(); i++) {
            removeElement(toBeRemoved.get(i));
        }
        int addSize = toBeAdded.size();
        for (int i = 0; i < addSize; i++) {
            addElement(toBeAdded.get(i));
        }
        if (addSize > 0) {
            Collections.sort(this);
        }
    }

    public void addRangeList(RangeList list) {
        Enumeration en = list.elements();
        while (en.hasMoreElements()) {
            add((Range) en.nextElement());
        }
    }

    public void add(Range range) {
        synchronized (this) {
            addElement(range);
            Collections.sort(this);
            Range r = (Range) elementData[elementCount - 1];
            Range l;
            for (int i = elementCount - 2; i >= 0; i--) {
                l = (Range) elementData[i];
                if (l.to >= r.from) {
                    l.to = Math.max(l.to, r.to);
                    removeElementAt(i + 1);
                }
                r = l;
            }
        }
    }

    public synchronized boolean contains(long from, long to) {
        for (int i = 0; i < elementCount; i++) {
            Range r = (Range) elementData[i];
            if (r.contains(from, to)) {
                return true;
            }
        }
        return false;
    }

    public synchronized boolean intersects(long from, long to) {
        for (int i = 0; i < elementCount; i++) {
            Range r = (Range) elementData[i];
            if (r.intersects(from, to)) {
                return true;
            }
        }
        return false;
    }

    public synchronized long getLastTime() {
        if (elementCount > 0) {
            return ((Range) elementData[elementCount-1]).to;
        }
        return 0;
    }

    public void dump() {
        if (!Log.DEBUG_ON) {
            return;
        }
        Log.printDebug("RangeList : size = " + size());
        for (int i = 0; i < size(); i++) {
            Log.printDebug(String.valueOf(elementAt(i)));
        }
    }


}

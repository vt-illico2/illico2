package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.adapter.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.sdv.mc.*;
import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.tv.util.*;
import org.dvb.user.*;
import org.dvb.event.*;
import com.alticast.ui.*;
import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;

//import org.ocap.hardware.*;
//import org.ocap.hardware.device.*;
//import org.dvb.media.*;

public class DiagWindow extends LayeredWindow implements Runnable, LayeredKeyHandler, TVTimerWentOffListener {

    LayeredUI ui;

    private DiagScreen[] screens;
    private DiagScreen current;
    private int currentScreenIndex = -1;

    private TVTimerSpec updateTimer = new TVTimerSpec();

    private static DiagWindow instance = new DiagWindow();
    //->Kenneth[2015.10.9]
    FileNavigator fileNav = new FileNavigator(new DVBColor(0, 0, 0, 128));
    boolean byPublic = true;

    public static DiagWindow getInstance() {
        return instance;
    }

    private DiagWindow() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);

        ui = WindowProperty.DEBUG_SCREEN.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();

        //->Kenneth[2015.10.28] VDTRMASTER-5717 getLogs 를 통해서 diag 화면이 떠 있는 것과 상관없이
        // 데이터를 긁어갈 수 있으므로 Contructor 에서 생성해야 한다. 따라서 comment out 된 걸 풀기로 한다.
        screens = new DiagScreen[] {
            new AdapterDebugScreen("BuiltInFileAdapter.updateStatusTable", "App List", new DVBColor(150, 0, 0, 128)),
            new AdapterDebugScreen("OobAdapter.updateStatusTable", "OOB OC", new DVBColor(0, 150, 0, 128)),
            new AdapterDebugScreen("InbandAdapter.updateStatusTable", "IB OC", new DVBColor(0, 0, 150, 128)),
            SdvDiagnostics.getInstance(),
            new SdvSessionStatus(),
            new InvalidChannelsScreen(new DVBColor(130, 0, 130, 128)),
            new CaUpdateScreen(new DVBColor(0, 130, 130, 128)),
            new ImagePoolScreen(new DVBColor(130, 130, 0, 128)),
            new LayeredUIScreen(new DVBColor(150, 150, 150, 128)),
            new VbmLogScreen(new DVBColor(0, 150, 70, 128)),
            new GeneralLogScreen(new DVBColor(70, 0, 150, 128), SharedDataKeys.COMPANION_DEBUG_LOGS),
        };

        updateTimer.setDelayTime(5000L);
        updateTimer.setRegular(false);
        updateTimer.setRepeat(true);
        updateTimer.addTVTimerWentOffListener(this);
    }

//    private static final String[] PORT_NAMES = {
//        "AV_OUTPUT_PORT_TYPE_RF",
//        "AV_OUTPUT_PORT_TYPE_BB",
//        "AV_OUTPUT_PORT_TYPE_SVIDEO",
//        "AV_OUTPUT_PORT_TYPE_1394",
//        "AV_OUTPUT_PORT_TYPE_DVI",
//        "AV_OUTPUT_PORT_TYPE_COMPONENT_VIDEO",
//        "AV_OUTPUT_PORT_TYPE_HDMI",
//        "AV_OUTPUT_PORT_TYPE_INTERNAL"
//    };


    public synchronized void start(boolean byPublic) {
        if (ui.isActive()) {
            return;
        }
        Log.printInfo("DiagWindow.start");
        this.byPublic = byPublic;
        EventQueue.invokeLater(this);
        TVTimer.getTimer().deschedule(updateTimer);
        try {
            updateTimer = TVTimer.getTimer().scheduleTimerSpec(updateTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }

//        Enumeration en = Host.getInstance().getVideoOutputPorts();
//        int index = 0;
//        while (en.hasMoreElements()) {
//            VideoOutputPort port = (VideoOutputPort) en.nextElement();
//            VideoOutputSettings settings = (VideoOutputSettings) port;
//            Log.printDebug("port[" + index + "] = " + port + " ; " + port.status() + " ; " + PORT_NAMES[port.getType()] + " ; " + settings.isDisplayConnected());
//            VideoOutputConfiguration[] configs = settings.getSupportedConfigurations();
//            for (int j = 0; j < configs.length; j++) {
//                VideoOutputConfiguration c = configs[j];
//                Log.printDebug(" config[" + j + "] = " + getString(c));
//            }
//            VideoOutputConfiguration c = settings.getOutputConfiguration();
//            Log.printDebug(" current = " + getString(c));
//            index++;
//        }
    }

//    private String getARString(int ar) {
//        switch (ar) {
//            case VideoFormatControl.ASPECT_RATIO_4_3:
//                return "ASPECT_RATIO_4_3";
//            case VideoFormatControl.ASPECT_RATIO_16_9:
//                return "ASPECT_RATIO_16_9";
//            case VideoFormatControl.ASPECT_RATIO_2_21_1:
//                return "ASPECT_RATIO_2_21_1";
//        }
//        return "ASPECT_RATIO_UNKNOWN";
//    }
//
//    private String getScanModeString(int sm) {
//        switch (sm) {
//            case VideoResolution.SCANMODE_INTERLACED:
//                return "i";
//            case VideoResolution.SCANMODE_PROGRESSIVE:
//                return "p";
//        }
//        return "?";
//    }
//
//    private String getString(VideoOutputConfiguration c) {
//        if (c instanceof FixedVideoOutputConfiguration) {
//            FixedVideoOutputConfiguration f = (FixedVideoOutputConfiguration) c;
//            VideoResolution r = f.getVideoResolution();
//            return f.getName() + "; " + r.getPixelResolution() + getScanModeString(r.getScanMode()) + " ; " + getARString(r.getAspectRatio()) + " ; " + r.getRate();
//        } else {
//            return c.getName();
//        }
//    }

    public void run() {
        DiagScreen.opaque = false;
        //->Kenneth[2015.11.2] 매번 생성해 버리면 맨마지막 companion log 화면에서 기존 데이터를 날려버림.
        // 따라서 contructor 에서 생성한 놈을 그대로 사용하도록 한다.
        // 또한 fileNav 는 여기서 필요한 경우만 생성하도록 한다.
        if (byPublic) {
            if (screens.length == 12) {
                DiagScreen[] newScreens = new DiagScreen[11] ;
                System.arraycopy(screens, 0, newScreens, 0, 11);
                screens = newScreens;
            }
            showScreen(0);
        } else {
            if (screens.length == 11) {
                DiagScreen[] newScreens = new DiagScreen[12] ;
                System.arraycopy(screens, 0, newScreens, 0, 11);
                fileNav = new FileNavigator(new DVBColor(0, 0, 0, 128));
                newScreens[11] = fileNav;
                screens = newScreens;
            }
            showScreen(11);
        }
        /*
        if (byPublic) {
            screens = new DiagScreen[] {
                new AdapterDebugScreen("BuiltInFileAdapter.updateStatusTable", "App List", new DVBColor(150, 0, 0, 128)),
                new AdapterDebugScreen("OobAdapter.updateStatusTable", "OOB OC", new DVBColor(0, 150, 0, 128)),
                new AdapterDebugScreen("InbandAdapter.updateStatusTable", "IB OC", new DVBColor(0, 0, 150, 128)),
                SdvDiagnostics.getInstance(),
                new SdvSessionStatus(),
                new InvalidChannelsScreen(new DVBColor(130, 0, 130, 128)),
                new CaUpdateScreen(new DVBColor(0, 130, 130, 128)),
                new ImagePoolScreen(new DVBColor(130, 130, 0, 128)),
                new LayeredUIScreen(new DVBColor(150, 150, 150, 128)),
                new VbmLogScreen(new DVBColor(0, 150, 70, 128)),
                new GeneralLogScreen(new DVBColor(70, 0, 150, 128), SharedDataKeys.COMPANION_DEBUG_LOGS)
            };
            showScreen(0);
        } else {
            screens = new DiagScreen[] {
                new AdapterDebugScreen("BuiltInFileAdapter.updateStatusTable", "App List", new DVBColor(150, 0, 0, 128)),
                new AdapterDebugScreen("OobAdapter.updateStatusTable", "OOB OC", new DVBColor(0, 150, 0, 128)),
                new AdapterDebugScreen("InbandAdapter.updateStatusTable", "IB OC", new DVBColor(0, 0, 150, 128)),
                SdvDiagnostics.getInstance(),
                new SdvSessionStatus(),
                new InvalidChannelsScreen(new DVBColor(130, 0, 130, 128)),
                new CaUpdateScreen(new DVBColor(0, 130, 130, 128)),
                new ImagePoolScreen(new DVBColor(130, 130, 0, 128)),
                new LayeredUIScreen(new DVBColor(150, 150, 150, 128)),
                new VbmLogScreen(new DVBColor(0, 150, 70, 128)),
                new GeneralLogScreen(new DVBColor(70, 0, 150, 128), SharedDataKeys.COMPANION_DEBUG_LOGS),
                fileNav
            };
            showScreen(11);
        }
        */
        //<-
        ui.activate();
    }

    public synchronized void stop() {
        Log.printInfo("DiagWindow.stop");
        ui.deactivate();
        removeAll();
        currentScreenIndex = -1;
        current = null;
        TVTimer.getTimer().deschedule(updateTimer);
        //->Kenneth[2015.10.9]
        if (fileNav != null) {
            fileNav.stop();
            // 11.2 nullify 함
            fileNav = null;
        }
        //<-
    }

    private void showScreen(int index) {
        if (index < 0 || index >= screens.length) {
            stop();
            return;
        }
        currentScreenIndex = index;
        removeAll();
        current = screens[currentScreenIndex];
        current.start();
        current.updateScreen();
        this.add(current);
        this.repaint();
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        if (Log.ALL_ON) {
            Log.printInfo("DiagWindow.handleKeyEvent = " + code);
        }
        //->Kenneth[2015.10.9]
        if (current instanceof FileNavigator) {
            boolean consumed = ((FileNavigator)current).handleKey(code);
            if (consumed) {
                return true;
            }
        }
        //<-

        // for debug
        switch (code) {
            case OCRcEvent.VK_PAGE_UP:
                logLevelUp();
                return false;
            case OCRcEvent.VK_PAGE_DOWN:
                logLevelDown();
                return false;
            case OCRcEvent.VK_F8:
                if (Environment.EMULATOR) {
                    DataAdapterManager.getInstance().getOobAdapter().reloadLocalFiles();
                    ChannelDatabaseBuilder.getInstance().buildLater(ChannelDatabaseBuilder.BOOT);
                }
                return false;
            case OCRcEvent.VK_F9:
            case OCRcEvent.VK_ASTERISK:
            //case OCRcEvent.VK_PINP_UP:
            case OCRcEvent.VK_0://->Kenneth[2015.10.9] 추가
                if (!current.showNextPage()) {
                    showScreen(currentScreenIndex + 1);
                }
                return false;
            //case OCRcEvent.VK_PINP_DOWN:
            case OCRcEvent.VK_8://->Kenneth[2015.10.9] 추가
                if (!current.showPreviousPage()) {
                    showScreen(currentScreenIndex - 1);
                }
                return false;
            //->Kenneth[2015.10.19]
            case OCRcEvent.VK_PINP_UP:
                loadOobFromDump();
                return false;
            //<-
            case OCRcEvent.VK_DISPLAY_SWAP:
                if (currentScreenIndex == screens.length - 1) {
                    showScreen(0);
                } else {
                    showScreen(screens.length - 1);
                }
                return false;
            case OCRcEvent.VK_STOP:
                stop();
                return false;
            case OCRcEvent.VK_PINP_MOVE:
                TunerManager.dumpBuffers();
                if (current instanceof SdvDiagnostics) {
                    SDVController.getInstance().sendInit();

//                    SDVController.getInstance().sendUserActivity(1, System.currentTimeMillis() - 1000L);
//
//                    try {
//                        byte[] buf = (App.NAME + App.VERSION).getBytes();
//                        java.net.DatagramPacket packet = new java.net.DatagramPacket(buf, buf.length, java.net.InetAddress.getByName("10.247.167.214"), 23000);
//                        SDVManager.getInstance().socket.send(packet);
//                    } catch (Exception ex) {
//                        Log.print(ex);
//                    }

                } else if (current instanceof SdvSessionStatus) {
                    MiniCarouselReader.getInstance().forceTune();
//                    TunerManager.test();

                } else if (current instanceof ImagePoolScreen)  {
                    ImagePoolScreen ips = (ImagePoolScreen) current;
                    ips.paintBuffer = !ips.paintBuffer;
                    this.repaint();

                } else if (currentScreenIndex == 1) {
//                    DataAdapterManager.getInstance().getOobAdapter().start();
                    Hashtable table = (Hashtable) SharedMemory.getInstance().get(SharedDataKeys.OOB_ADAPTER_TABLE);
                    if (table != null) {
                        Iterator it = table.entrySet().iterator();
                        Map.Entry entry;
                        while (it.hasNext()) {
                            entry = (Map.Entry) it.next();
                            String name = (String) entry.getKey();
                            ActionListener al = (ActionListener) entry.getValue();
                            //->Kenneth[2015.10.19]
                            ActionEvent ae = new ActionEvent(this, 0, OobAdapter.COMMAND_DUMP);
                            //ActionEvent ae = new ActionEvent(this, 0, OobAdapter.COMMAND_RESET);
                            //<-
                            al.actionPerformed(ae);
                            Log.printDebug("OOB manual update = " + name + " : " + al);
                        }
                    }

//                } else if (currentScreenIndex == 2) {
//                    ActionListener al = (ActionListener) SharedMemory.getInstance().get("IB_TEST");
//                    if (al != null) {
//                        ActionEvent ae = new ActionEvent(this, 0, OobAdapter.COMMAND_RESET);
//                        al.actionPerformed(ae);
//                        Log.printDebug("IB manual update : " + al);
//                    }

                } else if (current instanceof InvalidChannelsScreen) {
                    VolumeController.getInstance().setHdmiCheckCount(0);
                } else {
                    HeapManager.getInstance().gc(true);
//                    ChannelDatabaseBuilder.getInstance().buildLater(ChannelDatabaseBuilder.BOOT);
                }
                return false;

            //->Kenneth[2017.9.28] R7.4 : SDV 팝업 테스트를 위한 코드 추가
            // SdvSessionStatus 페이지여야 하고 현재 채널이 sdv 타입의 채널에 튠되어 있어야 한다.
            case KeyCodes.COLOR_C:
                if (current instanceof SdvSessionStatus) {
                    VideoPlane vp = ChannelController.get(0).getVideoPlane();
                    vp.startSdvError((short)0x0002, (short)0x0001, ChannelController.get(0).getCurrent().getSourceId(), true);
                }
                return false;
            case KeyCodes.COLOR_B:
                if (current instanceof SdvSessionStatus) {
                    VideoPlane vp = ChannelController.get(0).getVideoPlane();
                    vp.startKeepAlive();
                }
                return false;
            //<-

//            case KeyCodes.COLOR_C:
//                byte[] b = new byte[1024 * 1024];
//                com.videotron.tvi.illico.util.SharedMemory.getInstance().put("CONSUME_" + new java.util.Date(), b);
//                    VideoPlane vp = ChannelController.get(0).getVideoPlane();
//                    vp.startSdvError((short)2, (short)1, 1543, true);
//                CaManager.getInstance().checkAllChannels(true);

//                MiniCarouselReader.getInstance().forceTune();
//
//                Session ss = SDVSessionManager.getInstance().getSession(1);
//                ss.request = -12345;
//                ss.sourceId = 12345;
//                ss.channel = ChannelController.get(0).getCurrent();
//                ss.status = Session.STATUS_PENDING;

//                new Thread() {
//                    public void run() {
//                        try {
//                            PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
//                            boolean result = pvrService.requestTuner();
//                            Log.printDebug("pvrService.requestTuner = " + result);
//                        } catch (Exception ex) {
//                            Log.print(ex);
//                        }
//                    }
//                }.start();
//                SDVController.getInstance().showKeepAlive();
//                break;
//            case KeyCodes.COLOR_B:
//                    VideoPlane vp2 = ChannelController.get(1).getVideoPlane();
//                    vp2.startSdvError((short)2, (short)1, 1543, true);
//                ChannelController.get(0).getVideoPlane().startSdvError(SDVConstants.SDVMessageID_SDVProgramSelectIndication, (short) 0x8001, ChannelController.get(0).getCurrent().getSourceId(), true);
//                return false;
            case OCRcEvent.VK_MUTE:
                DiagScreen.opaque = !DiagScreen.opaque;
                this.repaint();
                return false;
        }
        return false;
    }

    //->Kenneth [2015.10.19] dump 파일로 부터 oob update 시킴
    private void loadOobFromDump() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DiagWindow.loadOobFromDump()");
        try {
            Hashtable table = (Hashtable) SharedMemory.getInstance().get(SharedDataKeys.OOB_ADAPTER_TABLE);
            if (table != null) {
                Iterator it = table.entrySet().iterator();
                Map.Entry entry;
                while (it.hasNext()) {
                    entry = (Map.Entry) it.next();
                    String name = (String) entry.getKey();
                    ActionListener al = (ActionListener) entry.getValue();
                    ActionEvent ae = new ActionEvent(this, 0, OobAdapter.COMMAND_LOAD_FROM_DUMP);
                    al.actionPerformed(ae);
                    if (Log.DEBUG_ON) Log.printDebug("OOB manual update from Dump = " + name + " : " + al);
                }
            }
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
    }
    //<-

    private void logLevelUp() {
        int cl = Log.getConsoleLevel();
        int nl = cl;
        if (cl <= Log.LEVEL_OFF) {
            nl = Log.LEVEL_ERROR;
        } else if (cl <= Log.LEVEL_ERROR) {
            nl = Log.LEVEL_WARNING;
        } else if (cl <= Log.LEVEL_WARNING) {
            nl = Log.LEVEL_INFO;
        } else if (cl <= Log.LEVEL_INFO) {
            nl = Log.LEVEL_DEBUG;
        } else if (cl <= Log.LEVEL_DEBUG) {
            nl = Log.LEVEL_EXTRA;
        } else if (cl <= Log.LEVEL_EXTRA) {
            nl = Log.LEVEL_ALL;
        } else {
            return;
        }
        if (cl != nl) {
            GeneralPreferenceUtil.write(Log.UP_NAME_CONSOLE_LEVEL, String.valueOf(nl + 8));
            repaint();
        }
    }

    private void logLevelDown() {
        int cl = Log.getConsoleLevel();
        int nl = cl;
        if (cl >= Log.LEVEL_ALL) {
            nl = Log.LEVEL_EXTRA;
        } else if (cl >= Log.LEVEL_EXTRA) {
            nl = Log.LEVEL_DEBUG;
        } else if (cl >= Log.LEVEL_DEBUG) {
            nl = Log.LEVEL_INFO;
        } else if (cl >= Log.LEVEL_INFO) {
            nl = Log.LEVEL_WARNING;
        } else if (cl >= Log.LEVEL_WARNING) {
            nl = Log.LEVEL_ERROR;
        } else if (cl >= Log.LEVEL_ERROR) {
            nl = Log.LEVEL_OFF;
        } else {
            return;
        }
        if (cl != nl) {
            GeneralPreferenceUtil.write(Log.UP_NAME_CONSOLE_LEVEL, String.valueOf(nl + 8));
            this.repaint();
        }
    }

    private static final int ICON_X = 860;
    private static final int ICON_Y = 38;
    private static final int WIDTH = 12;

    public void paint(Graphics g) {
        super.paint(g);
        int x = ICON_X;
        for (int i = screens.length - 1; i >= 0; i--) {
            if (i == currentScreenIndex) {
                g.setColor(Color.white);
                g.fillRect(x, ICON_Y, WIDTH, WIDTH);
            } else {
                g.setColor(screens[i].getBackground());
                g.fillRect(x, ICON_Y, WIDTH, WIDTH);
                g.setColor(Color.white);
                g.drawRect(x, ICON_Y, WIDTH-1 , WIDTH-1);
            }
            x -= WIDTH + 3;
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        try {
            DiagScreen sc = current;
            if (sc != null) {
                sc.updateScreen();
            }
        } catch (Exception ex) {
        }
        repaint();
    }

    public byte[] getLogs() {
        String[] names = {
            "AppList",
            "OOB_OC",
            "IB_OC",
            "Invalid_Channels",
            "CANH",
            "Image_Pool",
            "LayeredUI_List",
            "SDV_Diagnostics",
        };
        DiagScreen[] targets = {
            screens[0],
            screens[1],
            screens[2],
            screens[5],
            screens[6],
            screens[7],
            screens[8],
            screens[3],
        };

        ArrayList[] lists = new ArrayList[targets.length];
        for (int i = 0; i < lists.length; i++) {
            targets[i].updateScreen();
            lists[i] = targets[i].getLogs();
        }

        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n");
        sb.append("<root>\n");
        for (int i = 0; i < lists.length; i++) {
            sb.append("  <");
            sb.append(names[i]);
            sb.append("><![CDATA[\n");
            if (lists[i] != null) {
                sb.append(new Date());
                sb.append('\n');
                Iterator it = lists[i].iterator();
                while (it.hasNext()) {
                    sb.append(it.next());
                    sb.append('\n');
                }
            }
            sb.append("]]></");
            sb.append(names[i]);
            sb.append(">\n");
        }
        sb.append("</root>\n");

        return sb.toString().getBytes();
    }

}

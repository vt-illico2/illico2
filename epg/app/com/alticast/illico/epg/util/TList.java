package com.alticast.illico.epg.util;

import java.awt.*;
/**
 * @author TANPANG2
 */
public class TList extends Component{
    private int lineCount = 0 ;

    private int dataCount = 0 ;

    private int lineIndex = 0 ;

    private int startIndex = 0 ;

    private Object[] data = null ;

    private boolean isSelected = false ;

    private TRenderer render = null ;

	private boolean isRotatable = false;

	private boolean hasPage = true;

    public TList(int lineCount) {
        this.lineCount = lineCount ;
    }

    public void reset() {
        lineIndex = 0;
        startIndex = 0;
    }

	public void setPaging(boolean hasPage) {
		this.hasPage = hasPage;
	}

	public void setRotatable(boolean isRotatable) {
		this.isRotatable = isRotatable;
	}

    public void setRenderer(TRenderer r) {
        render = r ;
    }

    public void setData(Object[] data) {
        this.data = data ;
        if (data == null) {
            dataCount = 0;
        } else {
            dataCount = data.length;
        }
    }

    public void setLineCount(int lineCount) {
        this.lineCount = lineCount;
    }

    public Object[] getData() {
        return data ;
    }

    public void setSelection(boolean select) {
        this.isSelected = select ;
    }

    public boolean getSelection() {
        return isSelected ;
    }

    public int getDataIndex() {
        return startIndex+lineIndex ;
    }

    public boolean tryDown() {
        if (data == null) return false;
        if ((startIndex+lineIndex) == dataCount-1) return false;
        if (lineIndex == lineCount-1) return false;
        return true;
    }

    public boolean tryUp() {
        if (lineIndex == 0) return false;
        return true;
    }

    public void setDataIndex(int index) {
        if (data == null) return;
        if (index >= dataCount) return;
        int tmp = index/lineCount;
        startIndex = tmp*lineCount;
        lineIndex = index%lineCount;
    }

    public int getLineIndex() {
        return lineIndex;
    }

    public void setLineIndex(int index) {
        lineIndex = index;
    }

    public Object getSelectedData() {
        if (data == null) return null;
        return data[getDataIndex()];
    }

    public int getTotalPage() {
        int page = 0;
        if (lineCount >= dataCount) {
            page = 1;
        } else {
            if (dataCount%lineCount == 0) {
                page = dataCount/lineCount;
            } else {
                page = dataCount/lineCount+1;
            }
        }
        return page;
    }

    private int height = -1;
    public void setHeight(int height) {
        this.height = height;
    }

    public int getCurrentPage() {
        int page = 0;
        if (lineCount >= dataCount) {
            page = 1;
        } else {
            page = startIndex/lineCount+1;
        }
        return page;
    }

    public void pageUp() {
        if (data == null || !isSelected) return ;
		if (!hasPage) return;
        if (lineCount >= dataCount) return;
		if (!isRotatable && startIndex == 0) {
			return;
		}
        startIndex = startIndex - lineCount;
        if (startIndex < 0) {
            int rest = dataCount%lineCount;
            if (rest == 0) {
                startIndex = dataCount - lineCount;
            } else {
                startIndex = lineCount*(dataCount/lineCount);
                if (lineIndex >= rest) {
                    lineIndex = rest-1;
                }
            }
        }
    }

    public void pageDown() {
        if (data == null || !isSelected) return ;
		if (!hasPage) return;
        if (lineCount >= dataCount) return;
		if (!isRotatable && (startIndex+lineCount) >= dataCount) {
			return;
		}
        startIndex = startIndex + lineCount;
        if (startIndex >= dataCount) {
            startIndex = 0;
        }
        if (startIndex + lineIndex >= dataCount) {
            lineIndex = dataCount%lineCount-1;
        }
    }

    public void lineUp() {
    	if (data == null) return ;
		if (hasPage) {
			if (!isRotatable && ((lineIndex+startIndex) == 0)) {
				return;
			}
			lineIndex --;
			if (lineIndex < 0) {
				if (lineCount >= dataCount) {
					lineIndex = dataCount-1;
				} else {
					if (startIndex == 0) {
						if (dataCount%lineCount == 0) {
							startIndex = dataCount - lineCount;
							lineIndex = lineCount-1;
						} else {
							startIndex = lineCount*(dataCount/lineCount);
							lineIndex = dataCount%lineCount-1;
						}
					} else {
						startIndex = startIndex - lineCount;
						lineIndex = lineCount - 1;
					}
				}
			}
		} else {
			if (lineIndex > 0) {
				lineIndex --;
			} else {
				if (startIndex > 0) {
					startIndex --;
				}
			}
		}
    }

    public void lineDown() {
    	if (data == null) return ;
		if (hasPage) {
			if (!isRotatable && (startIndex+lineIndex) >= (dataCount-1)) {
				return;
			}
			lineIndex ++;
			if (lineCount >= dataCount) {
				if (lineIndex == dataCount) {
					lineIndex = 0;
				}
			} else {
				if (startIndex+lineIndex >= dataCount) {
					lineIndex = 0;
					startIndex = 0;
					return;
				}
				if (lineIndex == lineCount) {
					lineIndex = 0;
					startIndex = startIndex+lineCount;
					if (startIndex >= dataCount) {
						startIndex = 0;
					}
				}
			}
		} else {
			if ((lineIndex+startIndex) == dataCount-1) return;
			if (lineIndex < lineCount-1) {
				lineIndex++;
			} else {
				startIndex ++;
			}
		}
    }

    public Object[] getDisplayedData() {
        Object[] result = new Object[lineCount];
        for (int i = 0 ; i < lineCount ; i++) {
            if (data == null || data.length == 0) {
                result[i] = null;
            } else {
                if (startIndex+i < dataCount) {
                    result[i] = data[startIndex+i];
                } else {
                    result[i] = null;
                }
            }
        }
        return result;
    }

    public void paint(Graphics g) {
        int x = 0;
        int y = 0;
        int w = render.getSize().width;
        int h ;
        if (height == -1) {
            h = render.getSize().height;
        } else {
            h = height;
        }

        for (int i = 0 ; i < lineCount ; i++) {
            if (i == lineIndex && isSelected) {
                render.setSelection(true);
            } else {
                render.setSelection(false);
            }
            if (data == null || data.length == 0) {
                render.setData(null);
            } else {
                if (startIndex+i < dataCount) {
                    render.setData(data[startIndex+i]);
                } else {
                    render.setData(null);
                }
            }
            g.translate(x, h*i);
            render.paint(g);
            g.translate(x, -h*i);
        }
    }
}

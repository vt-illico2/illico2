package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.EpgCore;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;
import org.dvb.application.*;
import com.alticast.ui.*;

public class LayeredUIScreen extends DiagScreen {

    public static final int ROW_SIZE = 22;
    private static final int ROW_GAP = 19;
    private static final int COL_GAP = 480;
    private static final int[] X_POS = { 50, 110, 270 };

    LayeredUI[] array;

    public LayeredUIScreen(Color background) {
        super("LayeredUI List", background);
    }

    public void updateScreen() {
        array = LayeredUIManager.getInstance().getAllLayeredUIs();
    }

    public int getCount() {
        LayeredUI[] ui = array;
        return (ui != null) ? ui.length : 0;
    }

    public ArrayList getLogs() {
        ArrayList arrayList = new ArrayList();
        LayeredUI[] ui = array;
        if (ui != null) {
            for (int i = 0; i < ui.length; i++) {
                LayeredUI a = ui[i];
                arrayList.add("" + a.getPriority() + DELIMITER + a.isActive() + DELIMITER + a.getName() + DELIMITER + getBoundsString(a));
            }
        }
        return arrayList;
    }

    protected void paintBody(Graphics g) {
        LayeredUI[] ui = array;
        if (ui == null) {
            return;
        }
        int size = ui.length;
        int sx = 0;
        int i = 0;
        for (int c = 0; c < 2; c++) {
            int y = 70 + ROW_GAP;
            for (int r = 0; r < ROW_SIZE; r++) {
                i++;
                if (i >= size) {
                    return;
                }
                paintUI(g, ui[i], sx, y);
                y += ROW_GAP;
            }
            sx += COL_GAP;
        }
    }

    private void paintUI(Graphics g, LayeredUI a, int sx, int y) {
        g.setColor(Color.green);
        g.drawString(String.valueOf(a.getPriority()), sx + X_POS[0], y);
        g.setColor(a.isActive() ? Color.cyan : Color.orange);
        g.drawString("" + a.getName(), sx + X_POS[1], y);

        g.setColor(Color.white);
        g.drawString(getBoundsString(a), sx + X_POS[2], y);
    }

    private String getBoundsString(LayeredUI a) {
        Rectangle r = a.getBounds();
        if (r == null || r.isEmpty()) {
            return "-";
        } else {
            StringBuffer sb = new StringBuffer(20);
            sb.append('[');
            sb.append(r.x);
            sb.append(',');
            sb.append(r.y);
            sb.append(' ');
            sb.append(r.width);
            sb.append('x');
            sb.append(r.height);
            sb.append(']');
            return sb.toString();
        }

    }
}

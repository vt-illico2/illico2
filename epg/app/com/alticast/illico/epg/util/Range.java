package com.alticast.illico.epg.util;

import java.util.*;

public class Range implements Comparable {
    public long from;
    public long to;

    public Range(long f, long t) {
        from = f;
        to = t;
    }

    public boolean contains(long f, long t) {
        return from <= f && t <= to;
    }

    public boolean intersects(long f, long t) {
        return from < t && f < to;
    }

    public int compareTo(Object o) {
        if (o instanceof Range) {
            long diff = from - ((Range) o).from;
            if (diff == 0) {
                return 0;
            } else if (diff > 0) {
                return 1;
            } else {
                return -1;
            }
        }
        return 0;
    }

    public String toString() {
        return "Range : " + new Date(from) + ", " + new Date(to);
    }

}

package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ppv.*;
import com.alticast.illico.epg.*;
import java.awt.*;
import java.util.*;
import java.text.*;
import javax.tv.util.*;

public class CaUpdateScreen extends DiagScreen {

    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    Font fDebug = FontResource.BLENDER.getFont(14);

    public static final int ROW_SIZE = 20;
    private static final int ROW_GAP = 19;
    private static final int COL_GAP = 300;
    private static final int Y0 = 83;
    private static final int Y1 = Y0 + ROW_GAP;
    private static final int Y2 = Y1 + ROW_GAP;
    private static final int[] X_POS = { 78, 147, 160, 220 };
    private static final String[] LABLES = { "Time", "", "ID", "Package"};
    public static final String SOURCE_ID = "SourceID";
    public static final String PPV_EID = "PPV EID";

    static FixedSizeList list = new FixedSizeList(66);

    static int count;

    public CaUpdateScreen(Color background) {
        super("CANH update callbacks", background);
        if (Environment.EMULATOR) {
            for (int i = 0; i < 20; i++) {
                put(-7, false);
                put(12345, true);
                put("PK_FSLKFJA", 123142, true);
                put("PK_XCDFSLFJ", 324821, false);
            }
        }
    }

    public static void put(int sid, boolean auth) {
        put(SOURCE_ID, sid, auth);
    }

    public static void put(String message, long eid, boolean auth) {
        synchronized (list) {
            count++;
            list.addFirst(new ListItem(message, eid, auth));
        }
    }

    public int getCount() {
        return count;
    }

    public ArrayList getLogs() {
        ArrayList arrayList = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            arrayList.add(it.next().toString());
        }
        return arrayList;
    }

    protected void paintBody(Graphics g) {
        g.setColor(Color.white);
        g.drawString(CaManager.getInstance().toString(), 50, Y0);

        int y = Y2;
        g.setColor(Color.cyan);
        for (int c = 0; c < 3; c++) {
            for (int x = 0; x < X_POS.length; x++) {
                g.drawString(LABLES[x], X_POS[x] + c * COL_GAP, y);
            }
        }
        if (list == null) {
            return;
        }

        int i = 0;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            ListItem item = (ListItem) it.next();
            int r = i / ROW_SIZE;
            int c = i % ROW_SIZE;
            item.paint(g, count - i, r * COL_GAP, Y2 + ROW_GAP + c * ROW_GAP);
            i++;
        }
        g.setFont(fDebug);
        g.setColor(Color.yellow);
        g.drawString("" + PpvController.getInstance().monitoring, -130, Y1);
    }

    static class ListItem {
        String date;
        String content;
        long eid;
        boolean auth;

        public ListItem(String s, long eid, boolean auth) {
            date = timeFormat.format(new Date());
            content = s;
            this.eid = eid;
            this.auth = auth;
        }

        public void paint(Graphics g, int index, int x, int y) {
            int c = 0;
            g.setColor(Color.green);
            GraphicUtil.drawStringRight(g, String.valueOf(index), x + X_POS[c] - 5, y);
            g.setColor(Color.white);
            g.drawString(date, x + X_POS[c++], y);
            g.setColor(auth ? Color.yellow : Color.red);
            g.drawString(auth ? "O" : "X", x + X_POS[c++], y);
            g.setColor(Color.white);
            g.drawString(String.valueOf(eid), x + X_POS[c++], y);
            if (content != null) {
                g.setColor(Color.yellow);
                getContent();
                g.drawString(content, x + X_POS[c++], y);
            }
        }

        private String getContent() {
            if (content == SOURCE_ID) {
                ChannelDatabase db = ChannelDatabase.current;
                if (db != null) {
                    Channel ch = db.getChannelById((int) eid);
                    if (ch != null) {
                        content = "Ch #" + ch.getNumber() + " " + ch.getName();
                    }
                }
            }
            return content;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            sb.append(date);
            sb.append(DELIMITER);
            sb.append(auth ? 'O' : 'X');
            sb.append(DELIMITER);
            sb.append(eid);
            sb.append(DELIMITER);
            sb.append(getContent());
            return sb.toString();
        }
    }

}


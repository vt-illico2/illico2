package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.alticast.illico.epg.EpgCore;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;
import org.dvb.application.*;

public class ImagePoolScreen extends DiagScreen {

    public static final int ROW_SIZE = 22;
    private static final int ROW_GAP = 19;
    private static final int COL_GAP = 480;
    private static final int[] X_POS = { 50, 150 };

    Hashtable table = new Hashtable();

    public boolean paintBuffer = false;

    public ImagePoolScreen(Color background) {
        super("ImagePool Status", background);
    }

    public void start() {
        paintBuffer = false;
        super.start();
    }

    public void updateScreen() {
        AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration en = appsDB.getAppAttributes(new CurrentServiceFilter());
        while (en.hasMoreElements()) {
            AppAttributes at = (AppAttributes) en.nextElement();
            if (at != null) {
                String name = at.getName();
                Object o = SharedMemory.getInstance().get(name + "." + SharedDataKeys.IMAGE_POOL_STATUS);
                if (o != null && o instanceof Runnable) {
                    ((Runnable) o).run();
                    table.put(name, o);
                }
            }
        }
    }

    public int getCount() {
        return table.size();
    }

    public ArrayList getLogs() {
        ArrayList list = new ArrayList();
        for (Iterator it = table.entrySet().iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            list.add("" + entry.getKey() + DELIMITER + entry.getValue());
        }
        return list;
    }

    protected void paintBody(Graphics g) {
        if (paintBuffer) {
            Image im = Effect.getOffScreenImage();
            if (im != null) {
                g.drawImage(im, 0, 0, this);
            } else {
                Log.printWarning("ImagePoolScreen: Effect image is null");
            }
            g.setColor(Color.white);
            g.drawString("" + im, 50, 500);
            return;
        }
        int i = 0;
        for (Iterator it = table.entrySet().iterator(); it.hasNext();) {
            Map.Entry entry = (Map.Entry) it.next();
            int r = i / ROW_SIZE;
            int c = i % ROW_SIZE;
            int x = r * COL_GAP;
            int y = 70 + ROW_GAP + c * ROW_GAP;
            String name = "" + entry.getKey();
            if ("VBM".equals(name)) {
                name = "Logging";
            }
            g.setColor(Color.yellow);
            g.drawString(name, x + X_POS[0], y);
            g.setColor(Color.white);
            g.drawString("" + entry.getValue(), x + X_POS[1], y);
            i++;
        }

    }


}
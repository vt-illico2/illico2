package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.EpgCore;
import com.videotron.tvi.illico.ixc.vbm.*;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;

public class GeneralLogScreen extends DiagScreen {
    LinkedList list;

    public GeneralLogScreen(Color background, String sharedDataKey) {
        super(sharedDataKey, background);
        list = new FixedSizeList(22);
        SharedMemory.getInstance().put(sharedDataKey, list);
    }

    public int getCount() {
        if (list != null) {
            return list.size();
        } else {
            return -2;
        }
    }

    protected void paintBody(Graphics g) {
        if (list == null) {
            return;
        }
        int y = 95;
        int size = getCount();
        for (int i = 0; i < size && i < 22; i++) {
            String s = null, d = null, tmp = null;
            try {
                s = list.get(i).toString();
            } catch (Exception ex) {
                s = "";
            }
            paintLine(g, i, s, d, y);
            y += 20;
        }
    }

    private void paintLine(Graphics g, int index, String s, String d, int y) {
        g.setColor(Color.green);
        g.drawString(String.valueOf(index), 50, y);
        g.setColor(Color.white);
        g.drawString(s, 80, y);
        if (d != null) {
        	g.drawString(d, 570, y);
        }
    }
}


package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.tv.util.*;
import org.dvb.application.*;
import com.alticast.ui.*;
import java.io.*;
import org.ocap.ui.event.*;
import com.videotron.tvi.illico.framework.*;

public class FileNavigator extends DiagScreen implements TVTimerWentOffListener {
    //private static final String LOG_ROOT = System.getProperty("dvb.persistent.root");
    private static final String LOG_ROOT = Log.getLogicalStorageVolume();
    Font f16 = FontResource.BLENDER.getFont(16);
    FontMetrics fm16 = FontResource.getFontMetrics(f16);
    Font f14 = FontResource.BLENDER.getFont(14);
    FontMetrics fm14 = FontResource.getFontMetrics(f14);
    private File baseDir;
    private File curDir;
    private File openedFile;
    TList contentList;
    ContentRenderer conRenderer;
    TextBox textBox;
    private TVTimerSpec updateTimer = new TVTimerSpec();
    DeletePopup deletePopup;
    boolean deletePopupVisible = false;
    AppNamesPopup appNamesPopup;
    boolean appNamesPopupVisible = false;

    public FileNavigator(Color background) {
        super("File Navigator", background);

		conRenderer = new ContentRenderer();

    	contentList = new TList(20);
    	contentList.setBounds(147, 100, 627, 400);
    	contentList.setSelection(true);
    	contentList.setHeight(20);
    	contentList.setRotatable(false);
    	contentList.setRenderer(conRenderer);
    	add(contentList);

        updateTimer.setDelayTime(1000L);
        updateTimer.setRegular(false);
        updateTimer.setRepeat(true);
        updateTimer.addTVTimerWentOffListener(this);

        init();
    }

    private void init() {
        try {
            /*
            baseDir = new File(LOG_ROOT);
            File logFile = new File(LOG_ROOT+"/log/", "JAVA_LOG.txt");
            if (logFile != null && logFile.exists()) {
                cd(new File(LOG_ROOT+"/log/"));
            } else {
                cd(baseDir);
            }
            */
            //baseDir = new File(System.getProperty("dvb.persistent.root"));
            baseDir = new File(LOG_ROOT);
            cd(baseDir);
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
    }

    public void start() {
        stop();
        init();
        try {
            updateTimer = TVTimer.getTimer().scheduleTimerSpec(updateTimer);
            DataCenter.getInstance().addDataUpdateListener(Log.LOG_CAPTURE_APP_NAME, new AppNameUpdater());
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    class AppNameUpdater implements DataUpdateListener {
        public void dataUpdated(String key, Object old, Object value) {
            try {
                if (curDir != null && LOG_ROOT != null) {
                    File logDir = new File(LOG_ROOT+"/log/");
                    File logRootDir = new File(LOG_ROOT);
                    if (logDir.equals(curDir) || logRootDir.equals(curDir)) {
                        if (value != null && value instanceof String) {
                            String valueStr = (String)value;
                            if ("DELETE_FILE".equals(valueStr)) {
                                Object selectedObj = contentList.getSelectedData();
                                File selected = null;
                                if (selectedObj != null && (selectedObj instanceof File)) {
                                    selected = (File)selectedObj;
                                    selected.delete();
                                }
                            }
                        }
                        cd(curDir);
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }

        public void dataRemoved(String key) {
        }
    }

    public void stop() {
        contentList.setData(null);
        if (textBox != null) {
            remove(textBox);
            textBox = null;
        }
        TVTimer.getTimer().deschedule(updateTimer);
        deletePopupVisible = false;
        System.gc();
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        try {
            if (textBox == null) {
                repaint();
            }
        } catch (Exception ex) {
        }
    }

    private void cd(File target) {
        if (target == null) return;
        if (!target.canRead()) return;
        try {
            File[] files = target.listFiles();
            Object[] data = null;
            if (files == null || files.length == 0) {
                data = new Object[1];
                data[0] = target.getParentFile();
            } else {
                data = new Object[files.length+1];
                data[0] = target.getParentFile();
                System.arraycopy(files, 0, data, 1, files.length);
            }
            if (data[0] == null) data[0] = "ROOT";
            curDir = target;
            contentList.setData(data);
            contentList.reset();
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
        repaint();
    }

    private void openFile(File target) {
        if (Log.DEBUG_ON) Log.printDebug("FileNavigator.openFile("+target+")");
        if (target == null) return;
        if (!target.canRead()) {
            if (Log.DEBUG_ON) Log.printDebug("FileNavigator.openFile() : Cannot read");
            return;
        }
        int size = (int)target.length();
        if (size == 0 || size > 1000000) {
            if (Log.DEBUG_ON) Log.printDebug("FileNavigator.openFile() : Invalid Size = "+size);
            return;
        }
        String content = null;
        FileReader reader = null;
        try {
            reader = new FileReader(target);
            char[] chars = new char[(int) target.length()];
            reader.read(chars);
            content = new String(chars);
            content = TextUtil.urlDecode(content);
            reader.close();
            reader = null;
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        } finally {
            if(reader != null){
                try {
                    reader.close();
                    reader = null;
                } catch (Exception ex) {
                    if (Log.DEBUG_ON) Log.print(ex);
                }
            }
        }
        if (content == null) {
            if (Log.DEBUG_ON) Log.printDebug("FileNavigator.openFile() : content is null");
            return;
        }
        openedFile = target;
        textBox = new TextBox(24);
        //->Kenneth[2016.11.17] HD TV 가 아닌 곳에서는 잘림. 그래서 safety zone 을 둠
        textBox.setBounds(50, 95, 960-100, 400);
        //textBox.setBounds(20, 95, 960-40, 450);
        textBox.setLineHeight(18);
        textBox.setTextColor(new Color(252, 202, 4));
        textBox.setBackground(new Color(30, 30, 30));
        textBox.setFont(f14, fm14);
        textBox.setRotatable(true);
        add(textBox, 0);
        textBox.setText(content);
        repaint();
    }

    public void updateScreen() {
    }

    public int getCount() {
        return 0;
    }

    public boolean handleKey(int keyCode) {
        Object selectedObj = contentList.getSelectedData();
        File selected = null;
        if (selectedObj != null && (selectedObj instanceof File)) {
            selected = (File)selectedObj;
        }
        try {
            if (deletePopupVisible) {
                if (keyCode == KeyEvent.VK_ENTER) {
                    // 파일 지우고 팝업 닫기
                    try {
                        if (selected != null) {
                            /*
                            File logDir = new File(LOG_ROOT+"/log/");
                            File parentDir = selected.getParentFile();
                            if (parentDir != null && parentDir.equals(logDir)) {
                                // 지우려는 파일이 로그 파일이면 cache 중지시킴
                                Log.turnOffLogCache();
                            }
                            selected.delete();
                            cd(curDir);
                            */
                            UpManager.getInstance().set(Log.LOG_CAPTURE_APP_NAME, "DELETE_FILE");
                        }
                    } catch (Exception e){
                        if (Log.DEBUG_ON) Log.print(e);
                    }
                    hideDeletePopup();
                } else if (keyCode == OCRcEvent.VK_EXIT) {
                    hideDeletePopup();
                }
                return true;
            }
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
        /*
        boolean logChanged = Log.checkLogOnOff(keyCode, "EPG");
        if (logChanged) {
            cd(curDir);
            return true;
        }
        */
        try {
            switch (keyCode) {
                case KeyEvent.VK_UP:
                    if (appNamesPopupVisible) {
                        appNamesPopup.up();
                        return true;
                    }
                    if (textBox == null) {
                        contentList.lineUp();
                    } else {
                        textBox.pageUp();
                    }
                    repaint();
                    return true;
                case KeyEvent.VK_DOWN:
                    if (appNamesPopupVisible) {
                        appNamesPopup.down();
                        return true;
                    }
                    if (textBox == null) {
                        contentList.lineDown();
                    } else {
                        textBox.pageDown();
                    }
                    repaint();
                    return true;
                case KeyEvent.VK_ENTER:
                    if (appNamesPopupVisible) {
                        hideAppNamesPopup();
                        // upp 에 세팅
                        String appName = appNamesPopup.getSelectedAppName();
                        UpManager.getInstance().set(Log.LOG_CAPTURE_APP_NAME, appNamesPopup.getSelectedAppName());
                        return true;
                    }
                    if (textBox != null) return false;
                    if (selected != null) {
                        if (selected.equals(curDir.getParentFile())) {
                            // 상위로 이동
                            cd(selected);
                        } else {
                            if (selected.isDirectory()) {
                                // 폴더인 경우
                                cd(selected);
                            } else {
                                // 파일인 경우
                                openFile(selected);
                            }
                        }
                    }
                    return true;
                case KeyEvent.VK_PAGE_UP:
                    if (appNamesPopupVisible) {
                        return true;
                    }
                    if (textBox == null) return false;
                    textBox.pageUp();
                    repaint();
                    return true;
                case KeyEvent.VK_PAGE_DOWN:
                    if (appNamesPopupVisible) {
                        return true;
                    }
                    if (textBox == null) return false;
                    textBox.pageDown();
                    repaint();
                    return true;
                case OCRcEvent.VK_EXIT:
                    if (appNamesPopupVisible) {
                        hideAppNamesPopup();
                        return true;
                    }
                    if (textBox == null) {
                        DiagWindow.getInstance().stop();
                    } else {
                        this.remove(textBox);
                        textBox = null;
                        repaint();
                    }
                    return true;
                case KeyCodes.COLOR_A:
                    // flash root 로 이동
                    cd(new File(System.getProperty("dvb.persistent.root")));
                    return true;
                case KeyCodes.COLOR_B:
                    // log root 로 이동
                    cd(baseDir);
                    return true;
                case KeyCodes.COLOR_C:
                    if (appNamesPopupVisible) {
                        return true;
                    }
                    // 파일 지우기
                    if (textBox != null) return false;
                    if (selected != null) {
                        if (!selected.isDirectory() && LOG_ROOT != null) {
                            // 파일인 경우 : 보안을 위해서 log 파일만 지우도록 하자
                            File logDir = new File(LOG_ROOT+"/log/");
                            //File oobDumpDir = new File(LOG_ROOT+"/dump_OOB/");
                            File parentDir = selected.getParentFile();
                            if (parentDir != null && parentDir.equals(logDir)) {
                                showDeletePopup(selected);
                                /*
                            } else if (curDir != null && oobDumpDir.exists() && oobDumpDir.equals(curDir)) {
                                showDeletePopup(selected);
                                */
                            }
                        }
                    }
                    return true;
                case KeyCodes.COLOR_D:
                    // log 키 
                    if (textBox != null) return true;
                    if (appNamesPopupVisible) {
                        hideAppNamesPopup();
                    } else {
                        showAppNamesPopup();
                    }
                    return true;
            }
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
        return false;
    }

    private void showDeletePopup(File selected) {
        deletePopupVisible = true;
        deletePopup = new DeletePopup();
        deletePopup.setTarget(selected);
        deletePopup.setBounds(0, 0, 960, 540);
        this.add(deletePopup, 0);
        repaint();
    }

    private void hideDeletePopup() {
        deletePopupVisible = false;
        this.remove(deletePopup);
        repaint();
    }

    private void showAppNamesPopup() {
        appNamesPopupVisible = true;
        appNamesPopup = new AppNamesPopup();
        appNamesPopup.setBounds(0, 0, 960, 540);
        this.add(appNamesPopup, 0);
        repaint();
    }

    private void hideAppNamesPopup() {
        appNamesPopupVisible = false;
        this.remove(appNamesPopup);
        repaint();
    }

    protected void paintBody(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(147, 100, 627, 400);
        if (textBox == null) {
            g.setFont(f16);
            String logInfo = "";

            g.setColor(Color.yellow);
            logInfo = "Press [A] to move FLASH_ROOT / Press [B] to move LOG_ROOT / Press [C] to delete a log / Press [D] to catch a log";
            /*
            try {
                File file = new File(LOG_ROOT+"/log/", "JAVA_LOG.txt");
                if (file.exists()) {
                    g.setColor(Color.yellow);
                    logInfo = "Log Size = ("+file.length()+")bytes ["+LOG_ROOT+"/log/JAVA_LOG.txt]. ";
                } else {
                    g.setColor(Color.blue);
                    logInfo = "Log file is NOT existing : ["+LOG_ROOT+"/log/JAVA_LOG.txt].";
                }
            } catch (Exception e) {
            }
            */
            g.drawString(logInfo, 50, 90);
        } else {
            g.setColor(Color.green);
            g.setFont(f16);
            String pageInfo = openedFile.toString()+" [PAGE : "+textBox.getCurrentPage()+"/"+textBox.getTotalPage()+"]";
            g.drawString(pageInfo, 50, 90);
        }
    }

    class DeletePopup extends Container {
        File target;
        public void setTarget(File target) {
            this.target = target;
        }

        public void paint(Graphics g) {
            g.setColor(Color.yellow);
            g.fillRect(280, 150, 400, 250);
            g.setColor(Color.black);
            g.setFont(f16);
            String msg1 = "Do you really want to remove it?";
            String msg2 = "Then press OK, otherwise EXIT.";
            if (target != null) {
                g.drawString(msg1, 350, 250);
                g.drawString(msg2, 350, 250+22);
            }
        }
    }

	class ContentRenderer extends TRenderer {
        public ContentRenderer() {
            setBounds(0, 0, 627, 20);
        }

        public void paint(Graphics g) {
            if (data == null) {
                return;
            }
            File file = null;
            g.setFont(f16);
            g.setColor(Color.white);
            try {
                file = (File)data;
            } catch (ClassCastException e) {
                // Root 인 경우에는 String 을 넣어두었음.
                if (isSelected) {
                    g.fillRect(0, 0, 627, 20);
                    g.setColor(Color.black);
                }
                g.drawString("/", 20, 16);
                return;
            }
            if (isSelected) {
                g.fillRect(0, 0, 627, 20);
                g.setColor(Color.black);
            }
            if (file.isDirectory()) {
                // 폴더
                if (file.equals(curDir.getParentFile())) {
                    // parent
                    if (isSelected) {
                        g.setColor(Color.black);
                    } else {
                        g.setColor(Color.white);
                    }
                    g.drawString("../", 20, 16);
                } else {
                    // 하위 폴더
                    g.drawString(file.toString(), 20, 16);
                    g.setColor(Color.green);
                    g.drawString("/", 20+fm16.stringWidth(file.toString()), 16);
                }
            } else {
                // 파일
                if (!file.canRead()) {
                    g.setColor(Color.red);
                }
                String fileName = file.toString();
                fileName = TextUtil.urlDecode(fileName);
                g.drawString(fileName, 20, 16);
                if (isSelected) {
                    g.setColor(Color.green);
                } else {
                    g.setColor(Color.yellow);
                }
                g.drawString(" ("+file.length()+"bytes)", 20+fm16.stringWidth(fileName), 16);
            }
        }
	}

    class AppNamesPopup extends Container {
        String[] appNames = {"STOP_LOGGING", "EPG", "PVR", "VOD", "Options", "ISA", "Profile", "Monitor", "Daemon",
            "Companion", "Particip_La_Voix"};
        int focusIndex = 0;
        public void reset() {
            focusIndex = 0;
            repaint();
        }

        public void up() {
            focusIndex --;
            if (focusIndex < 0) {
                focusIndex = appNames.length-1;
            }
            repaint();
        }

        public void down() {
            focusIndex ++;
            if (focusIndex == appNames.length) {
                focusIndex = 0;
            }
            repaint();
        }

        public String getSelectedAppName() {
            return appNames[focusIndex];
        }

        public void paint(Graphics g) {
            g.setColor(Color.yellow);
            g.fillRect(280, 150, 400, 320);
            g.setFont(f16);
            g.setColor(Color.blue);
            g.drawString("SELECT an application to start logging", 330, 228-44);
            g.drawString("Press [EXIT] or [D] to exit this popup without logging", 330, 228-22);
            for (int i = 0 ; i < appNames.length ; i++) {
                if (i == focusIndex) {
                    g.setColor(Color.red);
                } else {
                    g.setColor(Color.black);
                }
                g.drawString("["+appNames[i]+"]", 350, 228+19*i);
            }
        }
    }
}

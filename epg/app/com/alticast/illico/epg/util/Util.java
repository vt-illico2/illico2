package com.alticast.illico.epg.util;

import java.awt.*;
import javax.media.Time;
import javax.tv.service.selection.*;
import org.ocap.service.*;
import com.alticast.illico.epg.ParentalControl;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.PreviewChannels;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.framework.DataCenter;

public class Util {

    private static DataCenter dataCenter = DataCenter.getInstance();

    private static Image iFav       = dataCenter.getImage("02_icon_fav.png");
    private static Image iBlocked   = dataCenter.getImage("02_icon_con.png");
    private static Image iUnBlocked = dataCenter.getImage("02_icon_con_un.png");
    private static Image iUnsub     = dataCenter.getImage("02_icon_unsub.png");
    private static Image iUnsubFoc  = dataCenter.getImage("02_icon_unsub_foc.png");
    private static Image iFreePreview = dataCenter.getImage("02_icon_free.png");
    private static Image iISA       = dataCenter.getImage("02_icon_isa.png");

    private static Image iWeb = dataCenter.getImage("icon_web.png");
    private static Image iMob = dataCenter.getImage("icon_mob.png");
    private static Image i900 = dataCenter.getImage("icon_900.png");
    private static Image iTabEn = dataCenter.getImage("icon_tab_en.png");
    private static Image iTabFr = dataCenter.getImage("icon_tab_fr.png");
    //->Kenneth[2015.6.17] R5 : 900 아이콘 뒤에 repeat 아이콘그려져야 함.
    private static Image iRepeat = dataCenter.getImage("icon_repeat.png");

    private static final String[] SELECTION_FAILED_REASON = {
        "UNKNOWN",
        "INTERRUPTED",
        "CA_REFUSAL",
        "CONTENT_NOT_FOUND",
        "MISSING_HANDLER",
        "TUNING_FAILURE",
        "INSUFFICIENT_RESOURCES",
    };

    private static final String[] PRESENTATION_TERMINATED_REASON = {
        "UNKNOWN",
        "SERVICE_VANISHED",
        "TUNED_AWAY",
        "RESOURCES_REMOVED",
        "ACCESS_WITHDRAWN",
        "USER_STOP",
    };

    private static final String[] ALTERNATIVE_CONTENT_ERROR_REASON = {
        "RATING_PROBLEM",
        "CA_REFUSAL",
        "CONTENT_NOT_FOUND",
        "MISSING_HANDLER",
        "TUNING_FAILURE"
    };

    public static String toString(ServiceContextEvent e) {
        String str = null;
        if (e instanceof SelectionFailedEvent) {
            SelectionFailedEvent fe = (SelectionFailedEvent) e;
            int reason = fe.getReason();
            if (reason >= 0 && reason < SELECTION_FAILED_REASON.length) {
                str = SELECTION_FAILED_REASON[reason];
            }
        } else if (e instanceof PresentationTerminatedEvent) {
            PresentationTerminatedEvent te = (PresentationTerminatedEvent) e;
            int reason = te.getReason();
            if (reason >= 0 && reason < PRESENTATION_TERMINATED_REASON.length) {
                str = PRESENTATION_TERMINATED_REASON[reason];
            }
        } else if (e instanceof AlternativeContentErrorEvent) {
            AlternativeContentErrorEvent ae = (AlternativeContentErrorEvent) e;
            int reason = ae.getReason() - 100;
            if (reason >= 0 && reason < ALTERNATIVE_CONTENT_ERROR_REASON.length) {
                str = ALTERNATIVE_CONTENT_ERROR_REASON[reason];
            }
        }
        if (str != null) {
            return e + ", reason = " + str;
        }
        return e.toString();
    }

    public static void drawChannelIcon(Graphics g, Channel ch, int x, int y, Component c) {
        drawChannelIcon(g, ch, false, true, x, y, c);
    }

    public static void drawChannelIcon(Graphics g, Channel ch, boolean hasFocus, int x, int y, Component c) {
        drawChannelIcon(g, ch, false, true, x, y, c);
    }

    //->Kenneth[2015.6.11] R5 : channel icon 이 그려졌는지 아닌지 판단할 필요가 있어서 boolean 리턴하게 함
    public static boolean drawChannelIcon(Graphics g, Channel ch, boolean hasFocus, boolean first, int x, int y, Component c) {
        if (ch.getType() != Channel.TYPE_PPV && (!ch.isAuthorized() || !PreviewChannels.hasPackage(ch))) {
            // 미가입
            if (ch.isFreePreview()) {
                //->Kenneth[2016.8.24] CYO Bundling/A la carte
                if (!first && ch.isISAPurchasable()) {
                //if (!first && ch.isISASupported()) {
                //<-
                    g.drawImage(iISA, x, y, c);
                    return true;
                } else {
                    g.drawImage(iFreePreview, x, y, c);
                    return true;
                }
            } else {
                //->Kenneth[2016.8.24] CYO Bundling/A la carte
                if (first && ch.isISAPurchasable()) {
                //if (first && ch.isISASupported()) {
                //<-
                    g.drawImage(iISA, x, y, c);
                    return true;
                } else {
                    g.drawImage(hasFocus ? iUnsubFoc : iUnsub, x, y, c);
                    return true;
                }
            }
        } else if (ch.isBlocked()) {
            if (ParentalControl.isUnblocked(ch)) {
                g.drawImage(iUnBlocked, x-5, y, c);
                return true;
            } else {
                g.drawImage(iBlocked, x, y, c);
                return true;
            }
        } else if (ch.isFavorite()) {
            g.drawImage(iFav, x, y, c);
            return true;
        } else if (ch.isFreePreview()) {
            g.drawImage(iFreePreview, x, y, c);
            return true;
        }
        return false;
    }

    //->Kenneth[2015.7.8] R5 : VDTRMASTER-5484 : 900 icon 을 안 그리는 경우도 있음.
    public static void drawScreenIcons(Graphics g, int screenData, int x, int y, Component c, boolean draw900) {
        if ((screenData & Channel.SCREEN_MASK_900) != 0 && draw900) {
            g.drawImage(i900, x, y, c);
            g.drawImage(iRepeat, x+37, y, c);
            x += 57;
        }
    /*
    public static void drawScreenIcons(Graphics g, int screenData, int x, int y, Component c) {
        if ((screenData & Channel.SCREEN_MASK_900) != 0) {
            g.drawImage(i900, x, y, c);
            g.drawImage(iRepeat, x+37, y, c);
            x += 57;
        }
        */
        //<-
        if ((screenData & Channel.SCREEN_MASK_WEB) != 0) {
            g.drawImage(iWeb, x, y, c);
            x += 37-3;
        }
        if ((screenData & Channel.SCREEN_MASK_MOBILE) != 0) {
            g.drawImage(iMob, x, y, c);
            x += 53-3;
        }
        if ((screenData & Channel.SCREEN_MASK_TABLET) != 0) {
            Image im = dataCenter.getImageByLanguage("icon_tab.png");
            if (im != null) {
                g.drawImage(im, x, y, c);
            }
        }
    }

    public static void drawScreenIconsRight(Graphics g, int screenData, int x, int y, Component c) {
        if ((screenData & Channel.SCREEN_MASK_TABLET) != 0) {
            Image im = dataCenter.getImageByLanguage("icon_tab.png");
            if (im != null) {
                x -= (im.getWidth(c) - 3);
                g.drawImage(im, x, y, c);
            }
        }
        if ((screenData & Channel.SCREEN_MASK_MOBILE) != 0) {
            x -= 53-3;
            g.drawImage(iMob, x, y, c);
        }
        if ((screenData & Channel.SCREEN_MASK_WEB) != 0) {
            x -= 37-3;
            g.drawImage(iWeb, x, y, c);
        }
        if ((screenData & Channel.SCREEN_MASK_900) != 0) {
            x -= 15;
            g.drawImage(iRepeat, x, y, c);
            x -= 34-3+5;
            g.drawImage(i900, x, y, c);
        }
    }

    public static int getLanguageIndex(String language) {
        for (int i = Constants.LANGUAGES.length - 1; i >= 0; i--) {
            if (Constants.LANGUAGES[i].equals(language)) {
                return i;
            }
        }
        return 0;
    }

    public static String toHexEncodedString(String src) {
        try {
            byte[] b = src.getBytes("UTF-8");
            StringBuffer buf = new StringBuffer(b.length * 2 + 2);
            for (int i = 0; i < b.length; i++) {
                String s = Integer.toHexString(b[i] & 0xFF);
                if (s.length() == 1) {
                    buf.append("0");
                }
                buf.append(s);
            }
            return buf.toString();
        } catch (Exception ex) {
            return src;
        }
    }

    public static int parseHexString(String s) {
        if (s == null || s.length() <= 0) {
            return 0;
        }
        String x = s;
        if (x.length() > 2 && x.startsWith("0x")) {
            x = x.substring(2);
        }
        int val = 0;
        try {
            val = Integer.parseInt(x, 16);
        } catch (Exception ex) {
        }
        return val;
    }

    public static long getTime(Time time) {
        return ((long) time.getSeconds() * 1000L)
                + (time.getNanoseconds() / 1000000L) % 1000L;
    }

    public static String getPpvIndicator(Channel ch) {
        if (ch == null) {
            return null;
        }
        return getPpvIndicator(ch, EpgDataManager.getInstance().getCurrentProgram(ch));
    }

    public static String getPpvIndicator(Channel ch, Program p) {
        if (p != null && p instanceof PpvProgram) {
            return ((PpvProgram) p).indicator;
        }
        if (ch == null) {
            return null;
        }
        String call = ch.getName();
        if (call.startsWith("IN")) {
            return PpvProgram.INDICATOR_IND;
        } else if (call.startsWith("VC")) {
            return PpvProgram.INDICATOR_VCC;
        } else {
            return null;
        }
    }

    public static String getChannelDescription(Channel channel) {
        switch (channel.getType()) {
            case Channel.TYPE_GALAXIE:
                return "";
            case Channel.TYPE_RADIO:
                return channel.radioFrequency;
            default:
                return channel.fullName;
        }
    }

	//->Kenneth : 4K by June
    // TODO - 4K
    public static int getDefinition(Channel ch, Program p) {
        if (ch == null) {
            if (p == null) {
                return Constants.DEFINITION_SD;
            } else {
                return p.getDefinition();
            }
        } else {
            int cDef = ch.getDefinition();
            /* Kenneth[2015.2.3]
            if (cDef >= Constants.DEFINITION_4K || p == null) {
                return cDef;
            } else {
                return Math.min(cDef, p.getDefinition());
            }
            */
            //->Kenneth[2015.7.9] : 4K 에 대한 판단은 channel 에 있는 정보만을 이용한다.
            /*
            if (cDef >= Constants.DEFINITION_4K) {
                return cDef;
            } else {
                if (p != null) return Math.min(cDef, p.getDefinition());
            }
            */
            return cDef;
        }
    }

}

package com.alticast.illico.epg.util;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.EpgCore;
import java.awt.*;
import java.util.*;
import javax.tv.util.*;

//->Kenneth[2015.10.9] : FileNavigator 때문에 Container 로 바꾼다.
//public abstract class DiagScreen extends Component {
public abstract class DiagScreen extends Container {

    public static final char DELIMITER = '|';

    public static final int ROW_SIZE = 20;
    private static final int GRAPHICS_TOTAL_KB = 95*1024;

    protected static Font font = FontResource.BLENDER.getFont(18);
    protected static Color cGreen = new Color(120, 255, 100);

    protected int currentPage;
    protected int totalPage;
    protected String title;

    public static boolean opaque = false;

    public DiagScreen(String title, Color c) {
        this(title, Color.orange, c);
    }

    public DiagScreen(String title, Color fg, Color bg) {
        this.title = title;
        setForeground(fg);
        setBackground(bg);
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void start() {
        currentPage = 0;
    }

    public boolean showPreviousPage() {
        return false;
    }

    public boolean showNextPage() {
        return false;
    }

    public void updateScreen() {
    }

    public abstract int getCount();

    public final void paint(Graphics g) {
        g.setColor(opaque ? Color.black : getBackground());
        g.fillRect(0, 0, 960, 540);

        g.setFont(font);
        g.setColor(cGreen);
        g.drawString(EpgCore.dncsName + " : " + Environment.VENDOR_NAME + " : " + title
                        + " : size = " + getCount()
                        + (totalPage > 1 ? (" : (page " + (currentPage + 1) + "/" + totalPage + ") : ") : " : ")
                        + new Date(), 50, 45);

        g.setColor(Color.orange);
        Runtime runtime = Runtime.getRuntime();
        int jTotal = (int) (runtime.totalMemory() / 1024);
        int jUsing = jTotal - (int) (runtime.freeMemory() / 1024);
        g.drawString("using java heap : " + jUsing + "/" + jTotal + " KB (" + (jUsing * 100 / jTotal) + "%)", 50, 65);

        g.setColor(Color.yellow);
        int gUsing = (GraphicsMemory.getUsingGraphicsMemory() / 1024);
        g.drawString("using graphics memory : " + gUsing + "/" + GRAPHICS_TOTAL_KB
                + " KB (" + (gUsing * 100 / GRAPHICS_TOTAL_KB) + "%)", 350, 65);

        g.setColor(Color.orange);
        g.drawString("console Log = " + Log.toLevelString(Log.getConsoleLevel()), 720, 65);

        paintBody(g);
        super.paint(g);
    }

    protected abstract void paintBody(Graphics g);

    public ArrayList getLogs() {
        return null;
    }

}

package com.alticast.illico.epg.util;

import java.awt.*;
import java.util.*;
/**
 * TextBox
 *
 * @author TANPANG2
 */
public class TextBox extends Container{
    public static final int LEFT = 0;
    public static final int CENTER = 1;
    private int curAlign = LEFT;
    private static String CR = "\n";
    private static final String BLANK = "<BLANK>";
    private TList list = null;
    private String text = null;
    private Font font = null;
    private FontMetrics metrics = null;
    private Color fColor = null;
    private int lineHeight = 0;
    private int lineCount = 0;
    private int width = 0;
    private int height = 0;
    private Vector vector = null;

    TextBoxRenderer renderer = null;
    private String[] textArr = null;

	private boolean spaceTokenized = true;

    public TextBox(int lineCount) {
        this.lineCount = lineCount;
        this.width = width;
        this.height = height;
        list = new TList(lineCount);
        list.setSelection(true);
        renderer = new TextBoxRenderer();
        list.setRenderer(renderer);
        add(list);
        vector = new Vector();
    }

	public void setReturn(String cr) {
		CR = cr;
	}

    public void reset() {
        list.reset();
    }

    public void setText(String text) {
        this.text = text;
        textArr = processText();
        list.setData(textArr);
        list.reset();
    }

	public void setText(String text, boolean spaceTokenized) {
		this.spaceTokenized = spaceTokenized;
		setText(text);
	}

    public String getText() {
        return text;
    }

    public void setAlign(int align) {
        curAlign = align;
    }

    public String[] processText() {
        if (text == null) return null;
        try {
            StringTokenizer tokenizer = new StringTokenizer(text, CR, false);
            int n = tokenizer.countTokens();

            String [] strs = new String[n];
            for (int i = 0 ; i < strs.length ; i++) {
                String str = tokenizer.nextToken();
                if (str == null) continue;
                if (str.startsWith("<BLANK>")) {
                    vector.addElement(" ");
                    continue;
                }
				if (spaceTokenized) {
					StringTokenizer st = new StringTokenizer(str, " ", true);
					int count = st.countTokens();
					StringBuffer aLine = new StringBuffer();
					for (int j = 0 ; j < count ; j++) {
						String word = st.nextToken();
						int lineLength = metrics.stringWidth(aLine.toString());
						int wordLength = metrics.stringWidth(word);
						if ((lineLength + wordLength) > width) {
							vector.addElement(aLine.toString().trim());
							aLine = new StringBuffer(word);
						} else {
							aLine.append(word);
						}
					}
					if (aLine.length() > 0) {
						vector.addElement(aLine.toString().trim());
						aLine = null;
					}
				} else {
					int length = str.length();
					int startIndex = 0;
					for (int j = 0 ; j < length ; j++) {
						String tmp = str.substring(startIndex, j+1);
						if (j == length-1) {
							String end = str.substring(startIndex, length);
							if (metrics.stringWidth(end) > width) {
								vector.addElement(
									end.substring(0, end.length()-1));
								vector.addElement(
									end.substring(end.length()-1, end.length()));
							} else {
								vector.addElement(end);
							}
							break;
						}
						if (metrics.stringWidth(tmp) > width) {
							vector.addElement(str.substring(startIndex, j));
							startIndex = j;
						}
					}
				}
            }
            String [] result = new String[vector.size()];
            for (int i = 0 ; i < result.length ; i++) {
                result[i] = (String)vector.elementAt(i);
            }
            vector.removeAllElements();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setLineCount(int lineCount) {
        this.lineCount = lineCount;
        list.setLineCount(lineCount);
    }

    public void setLineHeight(int lineHeight) {
        this.lineHeight = lineHeight;
        renderer.setSize(width, lineHeight);
    }

    public void setFont(Font font, FontMetrics metrics) {
        this.font = font;
        this.metrics = metrics;
        if (text != null) {
            textArr = processText();
            list.setData(textArr);
            list.reset();
        }
    }

    public String[] getTextArr() {
        return textArr;
    }

    public void setTextColor(Color fColor) {
        this.fColor = fColor;
    }

    public void setBounds(int x, int y, int width, int height) {
        super.setBounds(x, y, width, height);
        this.width = width;
        this.height = height;
        list.setBounds(0, 0, width, height);
        renderer.setSize(width, lineHeight);
    }

    public int getTotalPage() {
        return list.getTotalPage();
    }

    public int getCurrentPage() {
        return list.getCurrentPage();
    }

	public void setRotatable(boolean isRotatable) {
		list.setRotatable(isRotatable);
	}

    public void pageUp() {
        list.pageUp();
    }

    public void pageDown() {
        list.pageDown();
    }

    public void setBackground(Color c) {
        super.setBackground(c);
        renderer.setBackground(c);
    }

    class TextBoxRenderer extends TRenderer {
        public void paint(Graphics g) {
            Color bgColor = getBackground();
            int x = 0;
            int y = 0;
            int w = getBounds().width;
            int h = getBounds().height;
            if (bgColor != null) {
                g.setColor(bgColor);
                g.fillRect(x, y, w, h);
            }
            if (data == null) return;
            g.setFont(font);
            g.setColor(fColor);
            int startX = 0;
            if (curAlign == CENTER) startX = w/2-metrics.stringWidth((String)data)/2;
            g.drawString((String)data
                , startX
                , metrics.getHeight()-2);
        }
    }
}

package com.alticast.illico.epg;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Environment;
import com.alticast.illico.epg.data.EpgDataManager;
import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

/**
 * This class is the initial class of EPG Xlet.
 *
 * @author  June Park
 */
public class App implements Xlet, ApplicationConfig {
    public static final String LOG_HEADER = "T_EPG : ";

    /** 예전 R2, R3 기능이 다를때 구분하기 위한 flag. */
    public static final boolean R3_TARGET = true;

    public static final boolean EMULATOR = Environment.EMULATOR;

    public static final boolean MANY_TUNER = Environment.TUNER_COUNT > 2;

    /** 미가입채널은 녹화 불가능하게 설정. */
    public static final boolean BLOCK_TO_RECORD_UNSUB_CHANNEL = true;

    public static final String NAME = "EPG";
    //Kenneth : Version of R4.1 and E*
    //public static final String VERSION = "R4_1.2.0";
    //public static final String VERSION = "R5.1.0_06";
    //public static final String VERSION = "R5.1.0_beta";//6.1 최초 릴리즈 버전
    //public static final String VERSION = "R5.1.0_beta_01";//6.5 GUI 조금 수정
    //public static final String VERSION = "R5.1.0_beta_02";//6.6 VDTR-5308 (ISA+Preview)|주환 이슈 수정
    //public static final String VERSION = "R5.1.0_beta_03";//6.11 주환 이슈 수정
    //public static final String VERSION = "R5.1.0_beta_04";//6.12 VBM 수정
    //public static final String VERSION = "R5.1.0_beta_05";//6.15 주환 이슈 수정
    //public static final String VERSION = "R5.1.0_beta_08";//6.15 Back 처리
    //public static final String VERSION = "R5.1.0_beta_09";//6.16 Back 처리
    //public static final String VERSION = "R5.1.0_beta_10";//6.17 Back 처리
    //public static final String VERSION = "R5.1.4";//6.17 official 첫 릴리즈/VDTRMASTER-5431, 5434, 5413, 5308
    //public static final String VERSION = "R5.2.x";//2015.6.18 : 5409 workaround 건 게 삼성에서 다시 문제됨(5481). 따라서 없앰.
    //public static final String VERSION = "R5.2.x_02";//2015.6.19 : Jira 5480, 5483, 5481
    //public static final String VERSION = "R5.2.x_04";//2015.6.22 : Jira 5480
    //public static final String VERSION = "R5.2.x_05";//2015.6.24 : DDC-107 analog blocking
    //public static final String VERSION = "R5.2.x_06";//2015.6.25 : DDC-107(Companion 관련)
    //public static final String VERSION = "R5.2.x_07";//2015.6.30 : Tank filter 적용
    //public static final String VERSION = "R5.2.x_08";//2015.7.8 : VDTRMASTER-5484 900icon 안그림, 5485 def icon 안그림
    // 2015.7.9] Julie 로부터 앞으로는 HD 구분하는데 프로그램 정보는 쓰지 않겠다고 confirm 받았음. Tank 리뷰 미팅시에.
    // 따라서 앞으로 관련된 이슈들 나오거나 개발사항이 있으면 그 기준을 적용한다.
    //public static final String VERSION = "R5.2.x_09";//2015.7.9 : VDTRMASTER-5511
    //public static final String VERSION = "R5.2.x_10";//2015.7.10 : VDTRMASTER-5511, timeout 시간을 no fav popup 이 뜨는 경우는 늘임.
    //public static final String VERSION = "R5.2.x_11";//2015.7.14 : R5 rebranding
    //public static final String VERSION = "R5.2.x_12";//2015.7.14 : VDTRMASTER-5486
    //public static final String VERSION = "R5.2.x_13";//2015.7.16 : VDTRMASTER-5490/5506
    //public static final String VERSION = "R5.2.x_14";//2015.7.17 : VTPERISCOPE-179
    //public static final String VERSION = "R5.2.x_15";//2015.7.19 : VDTRMASTER-5512/5514
    //public static final String VERSION = "R5.2.x_16";//2015.7.20 : VDTRMASTER-5513(얘는 위의 hd 구분할때 program 안 쓰는 결정에 의해서 자연스레 없어진 듯. 따로 코드 수정한 부분 없음)+hd filter 의 경우 removeSister 하지 않도록함.
    //public static final String VERSION = "R5.2.13";//2015.7.20 : VT Official Release
    //public static final String VERSION = "R5.3.x_01";//2015.7.23 : D option BG 바뀜 (flat design)
    //public static final String VERSION = "R5.3.x_02";//2015.7.27 : rebraning 적용 버전
    //public static final String VERSION = "R5.3.x_03";//2015.7.27 : highlight bg color 변경
    //public static final String VERSION = "R5.3.x_04";//2015.7.27 : HDCP2.2 + ppvProgramDetail 에 DDC-107 의 경우 구매할수 없도록 More Info 만 보이게 함.
    //public static final String VERSION = "R5.3.x_05";//2015.7.28 : rebranding
    //public static final String VERSION = "R5.3.x_06";//2015.7.29 : VDTRMASTER-5534
    //public static final String VERSION = "R5.3.x_07";//2015.7.29 : Companion 연동부도 HDCP2.2 (EpgServiceImpl)
    //public static final String VERSION = "R5.3.x_08";//2015.7.30 : special video 상태에서는 hot plug 처리 안함
    //public static final String VERSION = "R5.3.1";//2015.7.31 : VT Official Release
    //public static final String VERSION = "R5.4.x_01";//2015.8.4 : VDTRMASTER-5544 - GridEpg 시작할 때 HD filter 면 removeSister 하지 않는다.
    //public static final String VERSION = "R5.4.x_02";//2015.8.5 : VDTRMASTER-5543 - ppv 에서 menu(위젯도) 눌렸을 D 팝업 지우게함.
    //public static final String VERSION = "R5.4.x_03";//2015.8.6 : VDTRMASTER-5563 - PpvProgramDetails 수정
    //public static final String VERSION = "R5.4.3";//2015.8.10 : VT Official Release
    //public static final String VERSION = "R5.5.x_01";//2015.8.10 : VDTRMASTER-5572
    //public static final String VERSION = "R5.5.x_02";//2015.8.13 : VDTRMASTER-5580, 5581, Highlight BG 바꿈+programDetails 의 scorll y, height 수정(Nicolas, Chandi)
    //public static final String VERSION = "R5.5.x_03";//2015.8.18 : DDC-113
    //public static final String VERSION = "R5.5.x_04";//2015.8.20 : DDC-113, VDTRMASTER-5600 by Woojung
    //public static final String VERSION = "R5.5.x_05";//2015.8.21 : VDTRMASTER-5591/5604/5597/5593
    //public static final String VERSION = "R5.5.x_06";//2015.8.24 : VDTRMASTER-5589, 5610
    //public static final String VERSION = "R5.5.x_07";//2015.8.26 : VDTRMASTER-5614,5613
    //public static final String VERSION = "R5.5.x_08";//2015.8.28 : VDTRMASTER-5618
    //public static final String VERSION = "R5.5.13";//2015.8.28 : 5번째 VT Official Release
    //public static final String VERSION = "R5.6.x_01";//2015.9.1 : VDTRMASTER-5571, 5578, 5587
    //public static final String VERSION = "R5.6.x_02";//2015.9.3 : Highlight bg 바꿈,GUI수정(julie 요구사항), VDTRMASTER-5620
    //public static final String VERSION = "R5.6.x_03";//2015.9.8 : VDTRMASTER-5622
    //public static final String VERSION = "R5.6.x_03";//2015.9.10 : VDTRMASTER-5632
    //public static final String VERSION = "R5.6.x_04";//2015.9.10 : VDTRMASTER-5629
    //public static final String VERSION = "R5.6.7";//2015.9.10 : 6번째 VT Official Release
    //public static final String VERSION = "R5.7.x_01";//2015.9.17 : VDTRMASTER-5648(julie 요구사항, TOC 변경)
    //public static final String VERSION = "R5.7.x_02";//2015.9.18 : VDTRMASTER-5651, 5643
    //public static final String VERSION = "R5.7.x_03";//2015.9.23 : VDTRMASTER-5657, 5660, 5659(TUTORIAL_HELP_LINK_CATEGORY_ID 가 application.prop 에는 있지만 사용하지 않도록 했음)
    //public static final String VERSION = "R5.7.7";//2015.9.25 : 7번째 VT Official Release (추가로 VDTRMASTER-5662 eh 수정했음. TOC 만 바뀜)
    //public static final String VERSION = "R5.8.x_01";//2015.10.3 : VDTRMASTER-5677
    //public static final String VERSION = "R5.8.x_02";//2015.10.6 : VDTRMASTER-5670/5680(우정수정:이건 EPG 가 원인이 아니므로 릴리즈 할 때는 표시하지 않기로 한다.)
    //public static final String VERSION = "R5.8.x_03";//2015.10.6 : VDTRMASTER-5664(improvement), 5663, 5643(reopen된놈)
    //public static final String VERSION = "R5.8.4";//2015.10.7 : 8번째 VT Official Release
    //public static final String VERSION = "R5.8.5";//2015.10.8 : 8번째 VT Official Release : 5686 추가해서 다시 릴리즈(5660의 side effect)
    //public static final String VERSION = "R5.9.x_01";//2015.10.9 : VDTRMASTER-5694
    //public static final String VERSION = "R5.9.x_02";//2015.10.9 : Diag 화면 수정. 258000 을 사용하게 함.
    //public static final String VERSION = "R5.9.x_03";//2015.10.14 : DDC-114 테스트 버전
    //public static final String VERSION = "R5.9.x_04";//2015.10.15 : DDC-114 테스트 버전
    //public static final String VERSION = "R5.9.x_05";//2015.10.16 : VDTRMASTER-5695
    //public static final String VERSION = "R5.9.x_06";//2015.10.18 : VDTRMASTER-5699
    //public static final String VERSION = "R5.9.3";//2015.10.18 : 9번째 VT Official Release
    //public static final String VERSION = "R5.10.x_01";//2015.10.28 : VDTRMASTER-5717
    //public static final String VERSION = "R5.10.1";//2015.10.29 : 10번째 VT Official Release. getLogs 때문에 quick patch


    //->Kenneth[2016.1.20] DDC-115 때문에 수정한 fix 들 제외하고 R5.11.0 을 릴리즈해야하는 상황임
    // 따라서 다음 릴리즈 혹은 R7 관련 branch 만들때 아래 fix 를 다시 포함시켜야 한다.
    //
    // 5737 : SimilarContentManager 에 DDC-115 로 되어 있는 놈 rollback 해야함
    // 5721 : ChannelBannerRenderer 에 DDC-115 로 되어 있는 놈 rollback 해야함.
    // 5718 : ppv/ChannelAssetPage 에 DDC-115 로 되어 있는 놈 rollback 해야함.
    //
    //public static final String VERSION = "R5.11.x_01";//2015.10.29 : 258000 테스트 버전
    //public static final String VERSION = "R5.11.x_02";//VDTRMASTER-5737 우정이 수정해서 commit한 것임.
    //public static final String VERSION = "R5.11.x_03";//VDTRMASTER-5721, 5718

    //public static final String VERSION = "R5.11.0";//2016.1.21 : DDC-115
    //public static final String VERSION = "R5.12.1";//2016.1.22 : VDTRMASTER-5760
    //public static final String VERSION = "R5.13.x_1";//2016.2.11 : DDC-115 때문에 rollback 했던 지라들 다시 복귀./VDTRMASTER-5737,5721, 5718
    //public static final String VERSION = "R5.13.x_2";//2016.2.11 : VDTRMASTER-5732(full name sorting)
    //public static final String VERSION = "R5.13.4";//2016.2.26 : VT 공식 릴리즈
    //public static final String VERSION = "R7.1.x_03";//2016.3.7 : R7 개발/테스트
    //public static final String VERSION = "R7.1.x_04";//2016.3.21 : R7 개발/테스트
    //public static final String VERSION = "R7.1.0";//2016.3.31 : R7 1st VT 공식 릴리즈
    //public static final String VERSION = "R7.2.0";//2016.4.8 : R7 2nd VT 공식 릴리즈 (R7 rebranding)
    //public static final String VERSION = "R7.3.1";//2016.4.15 : VDTRMASTER-5800
    //public static final String VERSION = "R7.4.x";//2016.6.1 : LOG_CAPTURE_APP_NAME 기능 추가, file navi 개선
    //public static final String VERSION = "R7_2.1.x_1";//2016.7.8 : R7.2 sticky filter 구현
    //public static final String VERSION = "R7_2.1.x_2";//2016.7.15 : R7.2 zoom 키 구현 (TOC/menu 파일 수정)
    //public static final String VERSION = "R7_2.1.x_3";//2016.7.19 : R7.2 On-screen debug (DebugScreen 추가)
    //public static final String VERSION = "R7_2.1.x_4";//2016.7.28 : R7.2 Tutorial Versioning
    //public static final String VERSION = "R7_2.1.x_5";//2016.8.4 : R7.2 Tutorial Versioning - data 연동
    //public static final String VERSION = "R7_2.1.x_6";//2016.8.17 : CYO Bundling/A la carte
    //public static final String VERSION = "(R7.2+CYO).1.0";//2016.9.16 : R7.2/CYO Bundling 공식 첫 릴리즈
    //public static final String VERSION = "(R7.2+CYO).2.x";//2016.9.22 : VDTRMASTER-5868, 5866
    //public static final String VERSION = "(R7.2+CYO).2.2";//2016.10.4 : 2번째 릴리즈
    //public static final String VERSION = "(R7.2+CYO).3.x";//2016.10.7 : VDTRMASTER-5922
    //public static final String VERSION = "(R7.2+CYO).3.1";//2016.10.11 : 3번째 릴리즈
    //public static final String VERSION = "(R7.2+CYO).4.x";//2016.10.17 : VDTRMASTER-5943(상세화면 옵션 버튼이슈)
    //public static final String VERSION = "(R7.2+CYO).4.1";//2016.10.18 : 4번째 릴리즈

    // VDTRMASTER-6022 가 긴급 패치 되면서 6016 의 경우 다음에 릴리즈 하도록 한다. 따라서 6016 수정한 부분은 지우고 릴리즈 함.
    //public static final String VERSION = "(R7.2+CYO).5.x_01";//2016.12.6 : VDTRMASTER-6016
    //public static final String VERSION = "(R7.2+CYO).5.1";//2016.12.14 : VDTRMASTER-6022 (URGENT : Free Preview 이슈)

    //public static final String VERSION = "(R7.2+CYO).6.x"; //VDTRMASTER-6016 다시 수정함
    //public static final String VERSION = "(R7.3).1.x"; //VDTRMASTER-6016, 6019 수정 
    //public static final String VERSION = "(R7.3).1.x_01"; // R7.3 sprint#1 : Tech 채널 없애기
    //public static final String VERSION = "(R7.3).1.x"; // R7.3 Highlight 팝업 바꾸기
    //public static final String VERSION = "(R7.3).1.x_02"; // R7.3 Highlight None 일 때 TOC 반영
    //public static final String VERSION = "(R7.3).1.x_03"; // 2017.3.16 : R7.3 D-option 에 search 추가
    //public static final String VERSION = "(R7.3).1.x_04"; // 2017.3.22 : R7.3 D-option 에 search 추가한 것 VBM 
    //public static final String VERSION = "(R7.3).1.x_05"; // 2017.3.23 : R7.3 APD disabled 추가
    //public static final String VERSION = "(R7.3).1.x_06"; // 2017.3.24 : R7.3 highlight 순서 변경
    //public static final String VERSION = "(R7.3).1.x_07"; // 2017.3.27 : R7.3 VDTRMASTER-6003, 6048
    //public static final String VERSION = "(R7.3).1.x_07"; // 2017.3.27 : R7.3 grid 에서 unsubscribed 팝업 뜰때도 vbm 로그 남김
    //public static final String VERSION = "(R7.3).1.4"; // 2017.3.30 : R7.3 1st 릴리즈
    //public static final String VERSION = "(R7.3).2.x"; // 2017.4.10 : VDTRMASTER-6086, 6095
    //public static final String VERSION = "(R7.3).2.2"; // 2017.4.11 : 2번째 릴리즈
    //public static final String VERSION = "(R7.3).3.x"; // 2017.4.20 : VDTRMASTER-6102, 6097
    //public static final String VERSION = "(R7.3).3.2"; // 2017.4.21 : 3번째 릴리즈
    //public static final String VERSION = "(R7.4).0.1"; // 2017.8.17 :internal release VDTRMASTER-6146
    //public static final String VERSION = "(R7.4).1.x_01"; // 2017.8.17 :VDTRMASTER-6180 : epg.txt 수정했음
    //public static final String VERSION = "(R7.4).1.x_02"; // 2017.8.21 : oob.prop 에서 SDV_Barker_Messages.txt 를 EPG_DataOOB 에서 받음. (MS 가 관리)
    //public static final String VERSION = "(R7.4).1.x_03"; // 2017.8.21 : ChannelList 의 getNextChannel(), getPreviousChannel() 수정 : virtual 이면서 sister 있는 경우도 skip
    //public static final String VERSION = "(R7.4).1.x_04"; // 2017.8.23 : epg.txt 의 block_pin/unblock_pin 의 메시지 바꿈
    //public static final String VERSION = "(R7.4).1.x_05"; // 2017.8.24 : epg.txt 메시지 수정에 따라서 EpgCore.getBlockedChannelExplain() 수정함
    //public static final String VERSION = "(R7.4).1.x_06"; // 2017.8.24 : camera type 추가. camera 채널에서 MOT500 발생시 MOT510 으로 변화
    //public static final String VERSION = "(R7.4).1.x_07"; // 2017.9.6 : block/unblock pin TOC 재수정
    //public static final String VERSION = "(R7.4).2.2"; // 2017.9.7 : R7.4 2번째 릴리즈 (첫번째는 상용형이 R7.3 branch)에서 했음. VDTRMASTER-6180, 6024 : ChannelList 에서 getNextChannel() getPreviousChannel() 수정한거 DDC 안하기로 해서 이전 코드로 되돌림 
    //public static final String VERSION = "(R7.4).3.x"; // 2017.9.26 : SDV_Barker_Messages.txt 를 더이상 사용하지 않고 epg_config_oob 통해서 tuning 정보를, Error Message 를 통해서 barker 띄우기로 함
    //public static final String VERSION = "(R7.4).3.1"; // 2017.10.2 : R7.4 3번째 릴리즈: VDTRMASTER-6024
    //public static final String VERSION = "(R7.4).4.1"; // 2017.10.12 : R7.4 4번째 릴리즈 : VDTRMASTER-6219 : frequency 에 1000000 해주기 빠져있었음.
    public static final String VERSION = "(R7.4).5.1"; // 2017.10.13 : R7.4 5번째 릴리즈 : VDTRMASTER-6218 : running 모드로 바뀔 때 delay 주기



    /** XletContext. */
    private XletContext xletContext;

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     *
     * @param ctx The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
    }

    /** Initialize application. Called by Framework. */
    public void init() {
        ConfigManager.getInstance();
        EpgDataManager.getInstance();
        EpgCore.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     *
     * @throws XletStateChangeException is thrown if the Xlet cannot start
     *                                  providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
        EpgCore.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     *
     * @param unconditional If unconditional is true when this method is called,
     *    requests by the Xlet to not enter the destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue
     *    to execute (Not enter the Destroyed state). This exception is ignored
     *    if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
        EpgCore.getInstance().destroy();
    }

    ////////////////////////////////////////////////////////////////////////////
    // ApplicationConfig implementation
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the version of Application.
     *
     * @return version string.
     */
    public String getVersion() {
        return VERSION;
    }

    /**
     * Returns the name of Application.
     *
     * @return application name.
     */
    public String getApplicationName() {
        return NAME;
    }

    /**
     * Returns the Application's XletContext.
     *
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

}

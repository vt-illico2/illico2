package com.alticast.illico.epg.tuner;

import org.davic.net.*;
import org.davic.net.tuning.*;
import org.davic.resources.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.resource.*;
import org.ocap.service.*;
import org.ocap.net.*;
import org.davic.net.tuning.*;
import org.davic.mpeg.sections.SectionFilterGroup;
import java.util.*;
import javax.tv.service.Service;
import javax.tv.service.selection.*;
//import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.epg.communication.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;

/**
 * ContentionHandler
 *
 * @author  June Park
 */
public class ContentionHandler implements ResourceContentionHandler, ResourceClient,
                                      ResourceStatusListener {

    private ResourceContentionManager manager;

    private static ContentionHandler instance = new ContentionHandler();

    public static ContentionHandler getInstance() {
        return instance;
    }

    public static Vector savedBuffers = new Vector();

    private Vector warningRecordings = new Vector();

    private ContentionHandler() {
        manager = ResourceContentionManager.getInstance();
        manager.setResourceContentionHandler(this);
    }

    public void init() {
        NetworkInterfaceManager.getInstance().addResourceStatusEventListener(this);
    }

    public void dispose() {
        NetworkInterfaceManager.getInstance().removeResourceStatusEventListener(this);
    }

    /** ResourceContentionHandler */
    public ResourceUsage[] resolveResourceContention(ResourceUsage newRequest, ResourceUsage[] current) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContention()");
        // Note - current의 순서는 tuner id 순이라는데...
        if (Log.DEBUG_ON) {
            Thread.dumpStack();
            TunerManager.dumpBuffers();
            Log.printDebug("ContentionHandler.resolveResourceContention = " + newRequest);
            Log.printDebug("ContentionHandler: currentReservations.size = " + current.length);
            for (int i = 0; i < current.length; i++) {
                ResourceUsage r = current[i];
                Log.printDebug("ContentionHandler:  cur[" + i + "] = " + r);
            }
            if (newRequest instanceof ApplicationResourceUsage) {
                try {
                    ResourceUsage ru = (ApplicationResourceUsage) newRequest;
                    Log.printDebug("ContentionHandler: ApplicationResourceUsage = " + ru.getAppID());
                    String[] names = ru.getResourceNames();
                    for (int j = 0; j < names.length; j++) {
                        ResourceProxy proxy = ru.getResource(names[j]);
                        Log.printDebug("ContentionHandler: ARU[" + j + "] = " + proxy);
                        Log.printDebug("ContentionHandler: ARU[" + j + "].name = " + names[j]);
                        if (proxy != null) {
                            Log.printDebug("ContentionHandler: ARU[" + j + "].client = " + proxy.getClient());
                        }
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        ResourceUsage[] ret = resolveResourceContentionImpl(newRequest, current);
        if (Log.DEBUG_ON) {
            if (ret != null) {
                Log.printDebug("ContentionHandler.resolveResourceContention returns...");
                for (int i = 0; i < ret.length; i++) {
                    Log.printDebug("ContentionHandler:  ret[" + i + "] = " + ret[i]);
                }
            } else {
                Log.printDebug("ContentionHandler.resolveResourceContention returns null");
            }
        }
        if (ret != null && ret.length > Environment.TUNER_COUNT
                        && ret[ret.length - 1] instanceof TimeShiftBufferResourceUsage) {
            TimeShiftBufferResourceUsage tsbru = (TimeShiftBufferResourceUsage) ret[ret.length - 1];
            int index = -1;
            for (int i = 0; i < current.length; i++) {
                if (current[i] == tsbru) {
                    index = i;
                    break;
                }
            }
            if (index >= 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContention() : Call TunerManager.notifyBufferWillBeDropped("+index+")");
                TunerManager.getInstance().notifyBufferWillBeDropped(tsbru.getService(), index);
            }
        }
        return ret;
    }

    private ResourceUsage[] resolveResourceContentionImpl(ResourceUsage newRequest, ResourceUsage[] current) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl("+newRequest+")");
        // recording only
        if (newRequest instanceof RecordingResourceUsage) {
            boolean allScheduledRecording = true;
            boolean allRecordingRelated = true;
            for (int i = 0; i < current.length; i++) {
                if (!(current[i] instanceof RecordingResourceUsage)) {
                    allScheduledRecording = false;
                    TunerUsage tu = new TunerUsage(current[i]);
                    if (tu.recordingRequest == null) {
                        allRecordingRelated = false;
                    }
                }
            }
            if (allScheduledRecording) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl() : allScheduledRecording");
                return processNormal(newRequest, current);
            } else if (allRecordingRelated) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl() : allRecordingRelated");
                return current;
            }
        }

        ResourceUsage[] ret = null;
        // new request is from ServiceContext
        if (newRequest instanceof ServiceContextResourceUsage) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl() : ServiceContextResourceUsage");
            ServiceContextResourceUsage scru = (ServiceContextResourceUsage) newRequest;
            ServiceContext sc = scru.getServiceContext();
            ret = processNormal(newRequest, current, sc);
            if (ret != null) {
                int size = Math.min(ret.length, Environment.TUNER_COUNT);
                for (int i = 0; i < size; i++) {
                    if (ret[i] == scru) {
                        return ret;
                    }
                }
                int index = Environment.getIndex(sc);
                if (index >= 0) {
                    ChannelController cc = ChannelController.get(index);
                    cc.contentionProblem = true;
                }
            }
            return ret;
        }

        // check tuner related
        boolean tunerRelated = false;
        String[] names = newRequest.getResourceNames();
        for (int i = names.length; i >= 0; i--) {
            if (names[i - 1].equals("org.davic.net.tuning.NetworkInterfaceController")) {
                tunerRelated = true;
                break;
            }
        }
        if (!tunerRelated) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl() : not related with tuner");
            return null;
        }

        ret = processNormal(newRequest, current);
        if (Log.DEBUG_ON) {
            Log.print(Log.LEVEL_DEBUG, ret);
        }
//        if (ret.length > 0) {
//            SharedMemory sm = SharedMemory.getInstance();
//            sm.put("ResourceUsage0", ret[0]);
//            if (ret.length > 1) {
//                sm.put("ResourceUsage1", ret[1]);
//            }
//        }

        if (newRequest instanceof RecordingResourceUsage && ret.length >= Environment.TUNER_COUNT + 1) {
            // 녹화로 들어왔고, drop된 것이 sc인지 check.
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveResourceContentionImpl() : RecordingResourceUsage");
            final TunerUsage tu = new TunerUsage(ret[Environment.TUNER_COUNT]);
            if (tu.serviceContext != null) {
                if (Log.INFO_ON) {
                    int ti = Environment.getIndex(tu.serviceContext);
                    Log.printInfo("ContentionHandler: ServiceContext[" + ti + "] will be stopped.");
                }
                RecordingRequest req = ((RecordingResourceUsage) newRequest).getRecordingRequest();
                new ServiceContextDroppedHandler(tu.serviceContext, req).init();
            }
        }

        return ret;
    }

    private ResourceUsage[] processNormal(ResourceUsage newRequest, ResourceUsage[] current) {
        return processNormal(newRequest, current, null);
    }

    private ResourceUsage[] processNormal(ResourceUsage newRequest, ResourceUsage[] current, ServiceContext sc) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.processNormal()");

        ArrayList list = new ArrayList();
        for (int i = 0; i < current.length; i++) {
            list.add(new TunerUsage(current[i], sc));
        }
        list.add(new TunerUsage(newRequest));
        Collections.sort(list);

        ResourceUsage[] ret = new ResourceUsage[list.size()];
        for (int i = 0; i <= current.length; i++) {
            TunerUsage tu = (TunerUsage) list.get(i);
            if (Log.DEBUG_ON) {
                Log.printDebug(" TunerUsage[" + i + "] = " + tu);
            }
            ret[i] = tu.source;
        }
        return ret;
    }

    /** Recording 만 가지고 conflict 가 발생했을때 처리 */
    private ResourceUsage[] resolveRecordings(RecordingResourceUsage newRequest,
                                                    ResourceUsage[] current) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveRecordings()");
        RecordingRequest r;
        ArrayList list = new ArrayList();
        for (int i = 0; i < current.length; i++) {
            list.add(current[i]);
        }
        int pos = list.size() - 1;
        for (; pos >= 0; pos--) {
            r = ((RecordingResourceUsage) list.get(pos)).getRecordingRequest();
            if (!AppData.getBoolean(r, AppData.UNSET)) {
                break;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ContentionHandler.resolveRecordings: newRequest's position = " + (pos+1));
        }
        list.add(pos+1, newRequest);

        ResourceUsage[] ret = new ResourceUsage[list.size()];
        ret = (ResourceUsage[]) list.toArray(ret);
        return ret;
    }

    /** ResourceContentionHandler */
    public void resourceContentionWarning(ResourceUsage newRequest, ResourceUsage[] current) {
        if (Log.DEBUG_ON) {
            Thread.dumpStack();
            Log.printDebug(App.LOG_HEADER+"ContentionHandler.resourceContentionWarning = " + newRequest);
            Log.print(Log.LEVEL_DEBUG, current);
        }
        if (!(newRequest instanceof RecordingResourceUsage)) {
            return;
        }

        RecordingResourceUsage rru = (RecordingResourceUsage) newRequest;
        RecordingRequest req = rru.getRecordingRequest();

        try {
            // 어차피 confilct 생겨서 녹화 못한다.
            if (req.getState() == LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                return;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }

        // 1 watching + 2 scheduled rec 이 동시에 들어올 경우,
        // cur + new 만 체크해서는 warning 으로 판단할 수 없다.
        // 따라서 이전에 들어왔던 new rec 을 모두 같이 묶어서 검사한다.
        ArrayList list = new ArrayList();
        validateWarningRecordings();
        synchronized (this) {
            warningRecordings.addElement(rru);
            int size = warningRecordings.size();
            for (int i = 0; i < size; i++) {
                list.add(new TunerUsage((RecordingResourceUsage) warningRecordings.elementAt(i)));
            }
        }
        // 모두 묶어서 sort
        for (int i = 0; i < current.length; i++) {
            list.add(new TunerUsage(current[i]));
        }
        int totalSize = list.size();
        Collections.sort(list);

        // drop 될 usage 중에 service context가 있는지 검사해서 있으면 팝업
        int videoIndex = -1;
        for (int i = Environment.TUNER_COUNT; i < totalSize; i++) {
            TunerUsage tu = (TunerUsage) list.get(i);
            if (Log.DEBUG_ON) {
                Log.printDebug("TunerUsage[" + i + "] = " + tu);
            }
            if (tu.recordingRequest == null && tu.serviceContext != null) {
                videoIndex = Math.max(videoIndex, Environment.getIndex(tu.serviceContext));
            }
        }

//        TunerUsage[] tu = new TunerUsage[current.length];
//        int videoIndex = -1;
//        int count = 0;
//        for (int i = 0; i < tu.length; i++) {
//            tu[i] = new TunerUsage(current[i]);
//            if (tu[i].recordingRequest == null) {
//                if (tu[i].serviceContext == null) {
//                    count++;
//                } else {
//                    videoIndex = Math.max(videoIndex, Environment.getIndex(tu[i].serviceContext));
//                }
//            }
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("ContentionHandler.resourceContentionWarning: count = " + count);
//        }
//        if (count >= Environment.TUNER_COUNT) {
//            return;
//        }
        if (videoIndex < 0) {
            Log.printDebug("ContentionHandler.resourceContentionWarning: no service context without recording");
            return;
        }
        try {
            PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
            if (pvrService != null) {
                pvrService.showChannelAutoTuneNotice(videoIndex, req.getId());
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /** 이전에 들어왔던 new rec 들 중에 이미 지나간 것은 제외. */
    private synchronized void validateWarningRecordings() {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ContentionHandler.validateWarningRecordings : before size = " + warningRecordings.size());
        }
        Vector newRecordings = new Vector();
        Enumeration en = warningRecordings.elements();
        while (en.hasMoreElements()) {
            try {
                RecordingResourceUsage rru = (RecordingResourceUsage) en.nextElement();
                RecordingRequest req = rru.getRecordingRequest();
                int state = req.getState();
                if (state == LeafRecordingRequest.PENDING_NO_CONFLICT_STATE) {
                    if (AppData.getScheduledStartTime(req) > System.currentTimeMillis()) {
                        newRecordings.addElement(rru);
                    }
                }
            } catch (Exception ex) {
            }
        }
        warningRecordings = newRecordings;
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ContentionHandler.validateWarningRecordings : after size = " + warningRecordings.size());
        }
    }

    /*
    public boolean checkFreeTuner() {
        try {
            Channel ch = ChannelDatabase.current.getDataChannel();
            return checkFreeTuner(ch.getDavicLocator());
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean checkFreeTuner(Locator locator) {
        NetworkInterfaceController nic = new NetworkInterfaceController(this);
        try {
            nic.reserveFor(locator, null);
            try {
                nic.release();
            } catch (Exception ex) {
            }
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    public synchronized NetworkInterface reserveFor(Locator loc) {
        NetworkInterfaceController nic = new NetworkInterfaceController(this);
        try {
            nic.reserveFor(loc, null);
            return nic.getNetworkInterface();
        } catch (Exception ex) {
        }
        return null;
    }
    */

    /** ResourceClient */
    public boolean requestRelease(ResourceProxy proxy, Object data) {
        if (proxy instanceof NetworkInterfaceController) {
            // Does nothing here.
            return true;
        } else if (proxy instanceof SectionFilterGroup) {
            // Never release the resource (section filter group)
            return false;
        }
        return true;
    }

    public void release(ResourceProxy proxy) {
    }

    public void notifyRelease(ResourceProxy proxy) {
    }


    /** ResourceStatusListener */
    public void statusChanged(ResourceStatusEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ResourceStatusListener.statusChanged = " + event);
        }
    }

    // recording에 의해 service context의 tuner가 뺏길 경우
    // channel이 black이 되기 때문에 해당 recording 채널로 이동 시켜준다.
    class ServiceContextDroppedHandler implements ServiceContextListener {
        ServiceContext serviceContext;
        RecordingRequest recordingRequest;
        int videoIndex;

        ServiceContextDroppedHandler(ServiceContext sc, RecordingRequest req) {
            serviceContext = sc;
            recordingRequest = req;
            videoIndex = Environment.getIndex(serviceContext);
        }

        public void init() {
            if (videoIndex >= 0) {
                serviceContext.addListener(this);
            }
        }

        public void receiveServiceContextEvent(ServiceContextEvent e) {
            if (e == null) {
                return;
            }
            if (e instanceof PresentationTerminatedEvent) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent(PresentationTerminatedEvent)");
                serviceContext.removeListener(this);
                int reason = ((PresentationTerminatedEvent) e).getReason();
                if (reason == PresentationTerminatedEvent.RESOURCES_REMOVED) {
                    if (Log.DEBUG_ON)
                        Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : RESOURCES_REMOVED");
                    MonitorService ms = CommunicationManager.getMonitorService();
                    try {
                        String appName = ms.getCurrentActivatedApplicationName();
                        boolean appVideoContext = App.R3_TARGET && !EpgCore.epgVideoContext;
                        boolean inAppState = (appName != null && !appName.equals("EPG")) || appVideoContext;

                        int sid = AppData.getInteger(recordingRequest, AppData.SOURCE_ID);
                        if (inAppState) {
                            if (appVideoContext) {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : Call releaseVideoContext()");
                                ms.releaseVideoContext();
                            }
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : Call exitToChannel()");
                            ms.exitToChannel(sid);
                        } else {
                            ChannelController cc = ChannelController.get(videoIndex);
                            boolean isPending = cc.isPending();
                            if (isPending) {
                                try {
                                    PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
                                    if (pvrService != null) {
                                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : Call pvrService.cancelSaveBuffer()");
                                        pvrService.cancelSaveBuffer(videoIndex);
                                    }
                                } catch (Exception ex) {
                                    Log.print(ex);
                                }
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : Call stopChannel()");
                                ChannelController.get(videoIndex).stopChannel();
                            }
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent() : Call changeChannel()");
                            ChannelController.get(videoIndex).changeChannel(sid);
                        }
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            } else if (e instanceof NormalContentEvent) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ContentionHandler.ServiceContextDroppedHandler.receiveServiceContextEvent(NormalContentEvent)");
                serviceContext.removeListener(this);
            }
        }

    }
}


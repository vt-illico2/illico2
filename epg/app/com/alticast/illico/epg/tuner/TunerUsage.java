package com.alticast.illico.epg.tuner;

import java.rmi.RemoteException;
import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContext;

import org.davic.resources.ResourceProxy;
import org.dvb.application.AppID;
import org.ocap.dvr.RecordingResourceUsage;
import org.ocap.dvr.SharedResourceUsage;
import org.ocap.dvr.TimeShiftBufferResourceUsage;
import org.ocap.net.OcapLocator;
import org.ocap.resource.ApplicationResourceUsage;
import org.ocap.resource.ResourceUsage;
import org.ocap.service.ServiceContextResourceUsage;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.shared.dvr.RecordedService;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.hn.service.RemoteService;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.alticast.illico.epg.data.AppData;
import com.alticast.illico.epg.App;

/**
 * Tuner ResourceUsage 관리
 *
 * @author  June Park
 */
public class TunerUsage implements Comparable {

    public static final int RECORDING       = 0x20;
    public static final int SERVICE_CONTEXT = 0x10;
    public static final int TIME_SHIFT      = 0x08;
    public static final int APPLICATION     = 0x04;
    public static final int UNKNOWN         = 0x02;

    private static final int AID_VOD     = 0x1020;
    private static final int AID_PVR     = 0x20a0;
    private static final int AID_EPG     = 0x2026;
    private static final int AID_MONITOR = 0x0001;

    ResourceUsage[] resources;
    int bitMap;
    int priority;

    ServiceContext serviceContext;
    RecordingRequest recordingRequest;
    Service bufferService;
    int aid;

    public int recordingType = -1;

    private int sourceID = -1;

    public ResourceUsage source;

    public TunerUsage(ResourceUsage r) {
        this(r, null);
    }

    public TunerUsage(ResourceUsage r, ServiceContext sc) {
        source = r;
        if (r instanceof org.ocap.dvr.SharedResourceUsage) {
            init(((org.ocap.dvr.SharedResourceUsage) r).getResourceUsages(), sc);
        } else if (r instanceof org.ocap.resource.SharedResourceUsage) {
            init(((org.ocap.resource.SharedResourceUsage) r).getResourceUsages(), sc);
        } else {
            init(new ResourceUsage[] { r }, sc);
        }
    }

    private void init(ResourceUsage[] r, ServiceContext sc) {
        if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.init() : saved buffer");
        this.resources = r;
        if (r == null) {
            this.resources = new ResourceUsage[0];
            return;
        }
        if (r.length == 0) {
            return;
        }

        boolean toBeDropped = false;
        ResourceUsage ru = null;
        for (int i = 0; i < r.length; i++) {
            ru = r[i];
            if (sourceID == -1) {
                sourceID = getSourceID(ru);
            }
            if (ru instanceof ServiceContextResourceUsage) {
                if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.init() : ServiceContextResourceUsage");
                serviceContext = ((ServiceContextResourceUsage) ru).getServiceContext();
                bitMap = bitMap | SERVICE_CONTEXT;
            } else if (ru instanceof RecordingResourceUsage) {
                if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.init() : RecordingResourceUsage");
                recordingRequest = ((RecordingResourceUsage) ru).getRecordingRequest();
                if (AppData.getBoolean(recordingRequest, AppData.UNSET)) {
                    toBeDropped = true;
                }
                bitMap = bitMap | RECORDING;
            } else if (ru instanceof TimeShiftBufferResourceUsage) {
                if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.init() : TimeShiftBufferResourceUsage");
                bufferService = ((TimeShiftBufferResourceUsage) ru).getService();
                bitMap = bitMap | TIME_SHIFT;
                // 특별한 경우
                if (ContentionHandler.savedBuffers.contains(bufferService)) {
                    Log.printInfo("TunerUsage : saved buffer");
                    bitMap += 2;
                }
            } else if (ru instanceof ApplicationResourceUsage) {
                if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.init() : ApplicationResourceUsage");
                bitMap = bitMap | APPLICATION;
                AppID appId = ru.getAppID();
                aid = appId.getAID();

                if (Log.DEBUG_ON) {
                    try {
                        Log.printDebug("ApplicationResourceUsage = " + appId);
                        String[] names = ru.getResourceNames();
                        ResourceProxy[] proxy = new ResourceProxy[names.length];
                        for (int j = 0; j < names.length; j++) {
                            proxy[j] = ru.getResource(names[j]);
                            Log.printDebug("ARU[" + j + "] = " + proxy[j]);
                            Log.printDebug("ARU[" + j + "].name = " + names[j]);
                            if (proxy[j] != null) {
                                Log.printDebug("ARU[" + j + "].client = " + proxy[j].getClient());
                            }
                        }
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            } else {
                Log.printWarning("TunerUsage : unknown usage = " + ru);
                bitMap = bitMap | UNKNOWN;
            }
            if (toBeDropped) {
                priority = bitMap - 0xFFFFFF;
            } else {
                priority = bitMap;
                if (aid != 0) {
                    switch (aid) {
                        case AID_MONITOR:
                            priority = Math.max(priority, TIME_SHIFT + 1);
                            break;
                        case AID_VOD:
                            priority = Integer.MAX_VALUE;
                            break;
                        case AID_EPG:
                        case AID_PVR:
                            priority = Math.max(priority, SERVICE_CONTEXT - 1);
                            break;
                    }
                } else if (sc != null && bitMap == (SERVICE_CONTEXT | TIME_SHIFT) && serviceContext == sc) {
                    Log.printInfo("TunerUsage : same sc");
                    priority = TIME_SHIFT + 1;
                }
            }
        }
    }

    public String toString() {
        return "TunerUsage <" + Integer.toBinaryString(bitMap) + "> sid=" + sourceID + ", pr=" + priority;
    }

    public void print() {
        Log.printWarning("bitMap = " + Integer.toBinaryString(bitMap) + ", sid = " + sourceID);
        if (resources.length == 0) {
            Log.printWarning("no resources.");
        } else {
            for (int i = 0; i < resources.length; i++) {
                Log.printWarning(i + "] = " + resources[i]);
            }
        }
    }

    /** Free Tuner 인지 여부 */
    public boolean isFree() {
        // 없거나, TimeShiftBuffer로만 사용될 경우
        return (bitMap | TIME_SHIFT) == TIME_SHIFT;
    }

    /** 현재 tuner로 사용중인 모든 작업을 중지한다. */
    public void discard() {
        if (Log.DEBUG_ON) Log.printInfo(App.LOG_HEADER+"TunerUsage.discard()");
//        if (serviceContext != null && serviceContext.equals(
//                ChannelController.getController(false).getServiceContext())) {
//            // PIP를 중지 요청한 경우 PIP Stop
//            if (Epg.gridView.isActive()) { // 편성표때문에 PIP가 뜬 경우
//                Epg.gridView.stop();
//            } else {
//                Epg.pipView.stop();
//            }
//        }
        // 제한채널로 이동한 경우 tuner conflict가 일어나지 않아
        // 중지되지 않기 때문에 강제로 중지하도록 구현
        if (recordingRequest != null && recordingRequest instanceof LeafRecordingRequest) {
            try {
                ((LeafRecordingRequest) recordingRequest).stop();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        // TunerUsage를 하나 만들어 비교해본다
        TunerUsage t = null;
        if (obj instanceof TunerUsage) {
            t = (TunerUsage) obj;
        } else if (obj instanceof ResourceUsage) {
            t = new TunerUsage((ResourceUsage)obj);
        }
        if (t == null) {
            return false;
        }

        if (t.sourceID != this.sourceID) {
            // 다른 채널
            return false;
        }
        if (t.recordingRequest != null && this.recordingRequest != null) {
            // 같은 채널 녹화중
            return true;
        }
        if (t.serviceContext != null) {
            // service context 비교
            return t.serviceContext.equals(this.serviceContext);
        }
        return t.bitMap == this.bitMap;
    }

    public static int getSourceID(ResourceUsage ru) {
        if (ru == null) {
            return -1;
        }
        // TODO - another SharedResourceUsage
        if (ru instanceof SharedResourceUsage) {
            ResourceUsage[] r = ((SharedResourceUsage) ru).getResourceUsages();
            for (int i = 0; i < r.length; i++) {
                int sid = getSourceID(r[i]);
                if (sid != -1) {
                    return sid;
                }
            }
            return -1;
        }
        if (ru instanceof ServiceContextResourceUsage) {
            return getSourceID(
                    ((ServiceContextResourceUsage) ru).getRequestedService() );
        }
        if (ru instanceof RecordingResourceUsage) {
            return getSourceID(
                    ((RecordingResourceUsage) ru).getRecordingRequest() );
        }
        if (ru instanceof TimeShiftBufferResourceUsage) {
            return getSourceID(
                    ((TimeShiftBufferResourceUsage) ru).getService() );
        }
        return -1;
    }

    public static int getSourceID(RecordingRequest recordingRequest) {
        return AppData.getInteger(recordingRequest, AppData.SOURCE_ID);
    }

    public static int getSourceID(Service service) {
        if (service == null || service instanceof RecordedService || service instanceof RemoteService) {
            return -1;
        }
        Locator loc = service.getLocator();
        if (loc != null && loc instanceof OcapLocator) {
            return ((OcapLocator) loc).getSourceID();
        }
        return -1;
    }

    public int compareTo(Object o) {
        if (o instanceof TunerUsage) {
            TunerUsage t = (TunerUsage) o;
            int diff = t.priority - priority;
            if (diff != 0) {
                return diff;
            }
            if (recordingRequest != null) {
                if (t.recordingRequest == null) {
                    return -1;
                }
                int thisValue = 0;
                int otherValue = 0;
                if (AppData.getBoolean(recordingRequest, AppData.UNSET)) {
                    thisValue += 100;
                }
                if (AppData.getBoolean(t.recordingRequest, AppData.UNSET)) {
                    otherValue += 100;
                }
                if (thisValue != otherValue) {
                    return thisValue - otherValue;
                }
                if (AppData.getBoolean(recordingRequest, AppData.SUPPRESS_CONFLICT)) {
                    thisValue = 10;
                }
                if (AppData.getBoolean(t.recordingRequest, AppData.SUPPRESS_CONFLICT)) {
                    otherValue = 10;
                }
                if (thisValue != otherValue) {
                    return thisValue - otherValue;
                }

//                if (App.MANY_TUNER) {
//                    // gateway에서는 최신 recording을 최하위로
//                    long thisCreationTime = AppData.getLong(recordingRequest, AppData.CREATION_TIME);
//                    long otherCreationTime = AppData.getLong(t.recordingRequest, AppData.CREATION_TIME);
//                    if (thisCreationTime > otherCreationTime) {
//                        return 1;
//                    } else if (thisCreationTime < otherCreationTime) {
//                        return -1;
//                    }
//                }

                // by PVR-F0160
                // start time check
                long a1 = AppData.getScheduledStartTime(recordingRequest);
                long a2 = AppData.getScheduledStartTime(t.recordingRequest);
                if (a1 != a2) {
                    return a1 < a2 ? -1 : 1;
                }
                // movie first
                int t1 = AppData.getInteger(recordingRequest, AppData.PROGRAM_TYPE);
                int t2 = AppData.getInteger(t.recordingRequest, AppData.PROGRAM_TYPE);
                if (t1 == TvProgram.TYPE_MOVIE && t2 != TvProgram.TYPE_MOVIE) {
                    return -1;
                }
                if (t1 != TvProgram.TYPE_MOVIE && t2 == TvProgram.TYPE_MOVIE) {
                    return 1;
                }
                // series
                String s1 = AppData.getString(recordingRequest, AppData.GROUP_KEY);
                String s2 = AppData.getString(t.recordingRequest, AppData.GROUP_KEY);
                return s2.length() - s1.length();
            }
            if (serviceContext != null) {
                if (t.serviceContext == null) {
                    return -1;
                }
                return Environment.getIndex(serviceContext) - Environment.getIndex(t.serviceContext);
            }
            return 0;
        } else {
            return -1;
        }
    }

}

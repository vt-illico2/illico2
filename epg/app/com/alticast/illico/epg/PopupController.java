package com.alticast.illico.epg;

import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import javax.tv.util.*;
import com.alticast.ui.*;

public class PopupController extends LayeredWindow implements LayeredKeyHandler {

    LayeredUI ui;

    Popup current;

    private static PopupController instance = new PopupController();

    public static PopupController getInstance() {
        return instance;
    }

    private PopupController() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);

        ui = WindowProperty.EPG_POPUP.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
    }

    public void paint(Graphics g) {
        Popup p = current;
        Color bg = null;
        if (p != null) {
            bg = p.getBackground();
            if (bg != null) {
                g.setColor(bg);
                g.fillRect(0, 0, 960, 540);
            }
        }
        super.paint(g);
    }

    public synchronized void start(Popup popup) {
        if (current != null) {
            if (current.isLight() || current.equals(popup)) {
                Log.printInfo("PopupController.start: replace current popup");
                current.stopTimer();
                this.remove(current);
                // todo notify cancled;
            } else {
                current.setVisible(false);
            }
        }
        popup.setVisible(false);
        popup.prepare();
        this.add(popup, 0);
        current = popup;
        ui.activate();
        repaint();
        popup.startEffect();
    }

    public synchronized void stopAll() {
        this.removeAll();
        ui.deactivate();
    }

    public synchronized void stop(Popup popup) {
        this.remove(popup);
        if (getComponentCount() == 0) {
            current = null;
            ui.deactivate();
        } else {
            current = (Popup) getComponent(0);
            current.setVisible(true);
            current.startTimer();
            repaint();
        }
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        Popup p = current;
        if (p != null) {
            return p.handleKey(e.getCode());
        }
        return false;
    }

    public void showQuestion(String title, String message, PopupListener l) {
        show(Popup.ICON_NONE, title, message, Popup.QUESTION_BUTTONS, null, l, false);
    }

    public void showConfirm(String title, String message, PopupListener l) {
        showConfirm(Popup.ICON_NONE, title, message, l);
    }

    public void showConfirm(int icon, String title, String message, PopupListener l) {
        showAutoClose(title, message, Popup.CONFIRM_BUTTONS, l, icon);
    }

    public void showAutoClose(String title, String message, String[] buttons, PopupListener l, int icon) {
        show(icon, title, message, buttons, null, l, true);
    }

    // one button but not autoclose
    public void showNotice(String title, String message, PopupListener l) {
        show(Popup.ICON_NONE, title, message, Popup.CONFIRM_BUTTONS, null, l, false);
    }

    public void showCustomPopup(String title, String[] buttons, PopupRenderer r, PopupListener l) {
        show(Popup.ICON_NONE, title, null, buttons, r, l, buttons == null);
    }

    public void showCustomPopup(String title, String[] buttons, PopupRenderer r, PopupListener l, long autoCloseDelay) {
        show(Popup.ICON_NONE, title, null, buttons, r, l, autoCloseDelay);
    }

    public void show(int icon, String title, String message, String[] buttons, PopupRenderer r, PopupListener l, boolean autoclose) {
        show(icon, title, message, buttons, r, l, autoclose ? Popup.DEFAULT_CLOSE_TIME : 0);
    }

    public void show(int icon, String title, String message, String[] buttons, PopupRenderer r, PopupListener l, long autocloseDelay) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PopupController.show = " + r);
        }
        Popup p = new Popup();
        p.icon = icon;
        p.title = title;
        p.buttons = buttons;
        p.setAutoCloseDelay(autocloseDelay);
        p.listener = l;
        if (r == null) {
            r = new PopupRenderer();
        }
        if (message != null) {
            r.setMessage(message);
        }
        p.setRenderer(r);
        start(p);
    }

    public void showNoFavPopup() {
        start(new NoFavouritesPopup());
    }

    public void showRecordingUnsubPopup() {
        DataCenter dataCenter = DataCenter.getInstance();
        String msg = dataCenter.getString("epg.rec_unsub_msg");
        msg = TextUtil.replace(msg, "%1", dataCenter.getString("epg.URL"));
        msg = TextUtil.replace(msg, "%2", dataCenter.getString("epg.PHONE_NUMBER"));

        String title = dataCenter.getString("epg.rec_unsub_title");
        showConfirm(title, msg, null);
    }

    //->Kenneth[2015.4.4] R5 : Highlight selection 팝업을 보여준다.
    // GridEpg 위에 add 해도 Video 뚫리고 하기 때문에 별개의 LayeredWindow 인
    // PopupController 에서 그리도록 한다.
    public void showHighlightSelectionPopup(GridEpg gridEpg) {
        start(new HighlightSelectionPopup(gridEpg));
    }

    //->Kenneth[2015.4.16] R5 : Tutorial popup 을 보여준다
    // GridEpg 위에 add 해도 Video 뚫리고 하기 때문에 별개의 LayeredWindow 인
    // PopupController 에서 그리도록 한다.
    public void showTutorialPopup(GridEpg gridEpg) {
        start(new TutorialPopup(gridEpg));
    }
}

package com.alticast.illico.epg.navigator;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import javax.tv.service.Service;

import org.dvb.event.EventManager;
import org.dvb.event.UserEvent;
import org.dvb.event.UserEventListener;
import org.dvb.event.UserEventRepository;
import org.dvb.ui.DVBColor;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.framework.DataCenter;

/**
 * <code>ChannelZapper</code>
 *
 * @since Charles CW Kwak, 2007. 1. 9
 */
// TODO [cwkwak] Though it's not designed to be accessed by monitor submodule
//      start(), pause(), and enableDCA() is called.
public class ChannelZapper implements EpgChannelEventListener {

    /* state */
    private int state = STATE_NONE;
    private static final int STATE_NONE = 0;
    private static final int STATE_INITED = 1;
    private static final int STATE_STARTED = 2;

    private InputController inputController = new InputController();
    private ZapController zapController = new ZapController();

    private boolean dcaEnabled = true;
    private boolean isWatchTvState = true;

    /** channel number display related. */
    private FrontPanelDisplay fpd = FrontPanelDisplay.getInstance();

    private Byte selectionLock = new Byte((byte) 0);

    private ChannelController channelController = ChannelController.get(0);

    private ChannelBanner epgBanner = EpgChannelBanner.getInstance();
    private ChannelBanner banner = epgBanner;

    private TVTimer timer = TVTimer.getTimer();

    public static boolean isFullPower = true;

    // for zapper
    public static final long MAX_EVENT_LATENCY = 100L; // 0.1 sec
    public static final long SCZ_WAITING_PERIOD = 500L; // 0.5 sec

    // for input
    public static long DCA_STANDBY_DELAY; // 2.5 sec - 요구사항 변경 1.5초로 변경함.
    public static final long DCA_WRONG_CHANNEL_DISPLAY_TIME = 1000L; // 1 sec

    public static final int MAX_DIGIT = 4;
    public static int digit = MAX_DIGIT;

    // gateway를 위해 conflict popup 대신 recording 중인 채널 loop에서 channel up/down 하는 기능
    public static boolean inLoop = false;

    private static ChannelZapper instance = new ChannelZapper();

    public static ChannelZapper getInstance() {
        return instance;
    }

    private ChannelZapper() {
        DCA_STANDBY_DELAY = DataCenter.getInstance().getLong("DCA_STANDBY_DELAY", 2000L);
        state = STATE_INITED;
        channelController.addChannelEventListener(this);
        isFullPower = Host.getInstance().getPowerMode() == Host.FULL_POWER;
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.start()");
        if (state != STATE_STARTED) {
            state = STATE_STARTED;
            enableDCA(true);
            zapController.start();
        }
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.stop()");
        state = STATE_INITED;
        zapController.stop();
        enableDCA(false);
    }

    public synchronized void dispose() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.dispose()");
        state = STATE_NONE;
        enableDCA(false);
        zapController.stop();
    }

    public ChannelBanner getChannelBanner() {
        return this.banner;
    }

    public synchronized void setChannelBanner(ChannelBanner cb) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelZapper.setChannelBanner: param = " + cb);
        }
        ChannelBanner target;
        if (cb == null) {
            target = epgBanner;
        } else {
            target = cb;
        }
        if (!isWatchTvState && target == epgBanner) {
            this.banner = ChannelBanner.EMPTY;
        } else {
            this.banner = target;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelZapper.setChannelBanner: banner = " + banner);
        }
    }

    public synchronized void setWatchTvState(boolean watch) {
        if (isWatchTvState == watch) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelZapper.setWatchTvState = " + watch);
        }
        this.isWatchTvState = watch;
        if (watch) {
            if (banner == ChannelBanner.EMPTY) {
                this.banner = epgBanner;
            }
        } else {
            if (banner == epgBanner) {
                banner = ChannelBanner.EMPTY;
            }
        }
        zapController.enableLastKey(watch);
    }

    public void enableDCA(boolean enable) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.enableDCA("+enable+")");
        this.dcaEnabled = enable;
        if (dcaEnabled) {
            inputController.start();
        } else {
            inputController.stop();
        }
    }

    public void showCurrentInfo() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.showCurrentInfo()");
        Channel ch = banner.startTuner(0);
        if (ch != null) {
            fpd.showText(TextUtil.getRightAlignedText(Integer.toString(ch.getNumber()), digit, ' '));
        }
    }

    public void selectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.selectionRequested("+ch+")");
        showChannelInfo(ch);
    }

    public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.selectionBlocked("+ch+")");
        showChannelInfo(ch);
    }

    public void serviceStopped(Channel ch) {
    }

    public void showChannelInfo(Channel ch) {
        String num = Integer.toString(ch.getNumber());
        banner.start(ch, num);
        fpd.showText(TextUtil.getRightAlignedText(num, digit, ' '));
    }

    public void programUpdated(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.programUpdated("+p+")");
        if (banner.isActive()) {
            String num = Integer.toString(ch.getNumber());
            banner.start(ch);
        }
    }

    private void showChannelInfo(String number, String name) {
        display(TextUtil.getRightAlignedText(number, digit, '-'), name);
    }

    private void showUnknownChannelInfo(String number) {
        display(TextUtil.getRightAlignedText(number, digit, ' '), "???");
    }

    private void display(String number, String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.display("+name+")");
        banner.start(number, name);
        fpd.showText(number);
    }

    public void hideChannelBar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.hideChannelBar()");
        banner.stop();
    }

    public void handleEnterKey() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.handleEnterKey()");
        inputController.keyEnter();
    }

    public void handleExitKey() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.handleExitKey()");
        inputController.keyExit();
    }

    public void setRecordingCount(int recordingCount) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.setRecordingCount("+recordingCount+")");
        int tunerCount = Environment.TUNER_COUNT;
        if (tunerCount > 2) {
            inLoop = (tunerCount - recordingCount) < 2;
        }
    }

    class ZapController implements UserEventListener, TVTimerWentOffListener {
        /* timer related (Channel up/down) */
        private final UserEventRepository channelKeys;
        private final UserEventRepository onlyChannelKeys;

        private TVTimerSpec timerSpecChNav = null; // for channel navigation
        private boolean isTimerChNavScheduled = false;

        /* Rapid channel event handling */
        private Channel channelToTune = null;
        private boolean hasChannelToStop = false;

        private boolean lastKeyEnabled = true;

        public ZapController() {
            channelKeys = new UserEventRepository("ChannelZapper.channelKeys");
            channelKeys.addKey(OCRcEvent.VK_CHANNEL_UP);
            channelKeys.addKey(OCRcEvent.VK_CHANNEL_DOWN);
            channelKeys.addKey(OCRcEvent.VK_LAST);

            onlyChannelKeys = new UserEventRepository("ChannelZapper.onlyChannelKeys");
            onlyChannelKeys.addKey(OCRcEvent.VK_CHANNEL_UP);
            onlyChannelKeys.addKey(OCRcEvent.VK_CHANNEL_DOWN);

            timerSpecChNav = new TVTimerSpec();
            timerSpecChNav.setDelayTime(SCZ_WAITING_PERIOD);
            timerSpecChNav.addTVTimerWentOffListener(this);
        }

        public synchronized void start() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.start()");
            EventManager em = EventManager.getInstance();
            em.addUserEventListener(this, channelKeys);
        }

        public synchronized void stop() {
            EventManager em = EventManager.getInstance();
            em.removeUserEventListener(this);
        }

        private synchronized void enableLastKey(boolean enable) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.enableLastKey("+enable+")");
            this.lastKeyEnabled = enable;
        }

        private void keyPressed(int code, long when) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.keyPressed() : code=" + code + ", when=" + when + " " + (System.currentTimeMillis() - when));
            }
            inputController.reset();
            if (!isFullPower) {
                return;
            }

            synchronized (selectionLock) {
                Channel ch;
                switch (code) {
                    case OCRcEvent.VK_CHANNEL_UP:
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.keyPressed() : VK_CHANNEL_UP");
                        ch = channelController.getNextChannel();
                        if (inLoop) {
                            ChannelController another = ChannelController.get(1);
                            if (another.getState() != ChannelController.STOPPED) {
                                boolean checkTune = TunerManager.getInstance().checkTune(channelController, ch);
                                if (!checkTune) {
                                    if (Log.DEBUG_ON) Log.printInfo("ChannelZapper: cannot tune to channel");
                                    ch = RecordingStatus.getInstance().getNextRecordingChannel(ch);
                                    if (ch.equals(another.getCurrent())) {
                                        ch = RecordingStatus.getInstance().getNextRecordingChannel(ch);
                                    }
                                }
                            }
                        }
                        break;
                    case OCRcEvent.VK_CHANNEL_DOWN:
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.keyPressed() : VK_CHANNEL_DOWN");
                        ch = channelController.getPreviousChannel();
                        if (inLoop) {
                            ChannelController another = ChannelController.get(1);
                            if (another.getState() != ChannelController.STOPPED) {
                                boolean checkTune = TunerManager.getInstance().checkTune(channelController, ch);
                                if (!checkTune) {
                                    if (Log.DEBUG_ON) Log.printInfo("ChannelZapper: cannot tune to channel");
                                    ch = RecordingStatus.getInstance().getPreviousRecordingChannel(ch);
                                    if (ch.equals(another.getCurrent())) {
                                        ch = RecordingStatus.getInstance().getPreviousRecordingChannel(ch);
                                    }
                                }
                            }
                        }
                        break;
                    case OCRcEvent.VK_LAST:
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.keyPressed() : VK_LAST");
                        if (lastKeyEnabled) {
                            if (App.R3_TARGET && channelController.lastRecording != null) {
                                EpgCore.getInstance().startUnboundApp(EpgCore.PVR_APP_NAME, new String[] { MonitorService.REQUEST_APPLICATION_LAST_KEY });
                                return;
                            }
                            ch = channelController.getLastWatchedChannel();
                        } else {
                            return;
                        }
                        break;
                    default:
                        return;
                }
                // TODO : default 채널로 tune되지 않은 상태라면 first channel로
                // 이동하게 되므로 default 채널로 가도록 할 필요가 있음
                selectChannel(ch, when);
            }
        }

        private void selectChannel(Channel ch, long when) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.selectChannel("+ch+")");
            if (ch == null || !isFullPower) {
                return;
            }
            if (channelController.isPending()) {
                return;
            }
            boolean tuned;
            if (isTimerChNavScheduled) {
                // stop intermediate channel
                if (hasChannelToStop) {
                    // stop tuned channel
                    hasChannelToStop = false;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.selectChannel() : Call sc.stop()");
                    channelController.getServiceContext().stop();
                }

                // changeChannel() may take some time, so deschedule timer before
                // calling it.
                descheduleTimerChNav();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.selectChannel() : Call ChannelController.changeChannel()");
                tuned = channelController.changeChannel(ch, false, true, true);
                channelToTune = ch;
            } else {
                // stop tuned channel
                hasChannelToStop = true;

                // changeChannel() may take some time, so deschedule timer before
                // calling it.
                descheduleTimerChNav();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.selectChannel() : Call ChannelController.changeChannel()");
                tuned = channelController.changeChannel(ch, true, true, true);
//                inputController.scheduleTimerChBar(inputController.TIMER_HIDE_CHANNEL_BAR);
                channelToTune = null;
            }
            scheduleTimerChNav();
            if (!tuned && !hasChannelToStop) {
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelZapper.zapController.selectChannel() : Call sc.select()");
                    channelController.getServiceContext().select(channelController.getCurrent().getService());
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }

        private void descheduleTimerChNav() {
            if (Log.DEBUG_ON) {
                Log.printDebug("descheduleTimerChNav");
            }
            if (isTimerChNavScheduled) {
                timer.deschedule(timerSpecChNav);
                isTimerChNavScheduled = false;
            }
        }

        private void scheduleTimerChNav() {
            if (Log.DEBUG_ON) {
                Log.printDebug("scheduleTimerChNav");
            }
            // The only case is TIMER_CHN_TUNE
            try {
                timer.scheduleTimerSpec(timerSpecChNav);
                isTimerChNavScheduled = true;
            }
            catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }

        public void userEventReceived(UserEvent event) {
            if (event.getType() == KeyEvent.KEY_PRESSED) {
                keyPressed(event.getCode(), event.getWhen());
            }
        }

        /** Channel Timer */
        public void timerWentOff(TVTimerWentOffEvent event) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"timerWentOff(): channelToTune="
                    + ((channelToTune == null) ? "null" : channelToTune.toString()));
            }
            /* channel navigation (up/down/back) related */
            if (!isFullPower) {
                descheduleTimerChNav();
                channelToTune = null;
                return;
            }

            synchronized (selectionLock) {
                if (channelToTune != null) {
                    Channel ch = channelController.getCurrent();
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"changeChannel() by timer. ch=" + ch);
                    }
                    if (channelToTune == ch) {
                        // stop tuned channel
                        hasChannelToStop = true;
                        Log.printDebug(App.LOG_HEADER+"changeChannel() : call ChannelController.changeChannel()"); 
                        channelController.changeChannel(ch, true, true, true);
                    } else {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(App.LOG_HEADER+"changeChannel() REMARK: cancelled. (req="
                                + channelToTune + ", cur=" + ch);
                        }
                    }
                    channelToTune = null;
                }
                descheduleTimerChNav();
            }
        }
    }

    class InputController implements UserEventListener, TVTimerWentOffListener {
        private static final int TIMER_NONE = 0;
        private static final int TIMER_DCA_INPUT_STANDBY = 1;
        private static final int TIMER_DCA_INPUT_WRONG = 2;
//        private static final int TIMER_HIDE_CHANNEL_BAR = 3;

        private int timerModeChBar = TIMER_NONE;
        private boolean isTimerChBarScheduled = false;
        private TVTimerSpec timerSpecChBar;

        private int inputCount = 0;
        private String inputedString = "";
        private int inputedNumber = 0;

        private UserEventRepository numberKeys;

        public InputController() {
            numberKeys = new UserEventRepository("ChannelZapper.numberKeys");
            numberKeys.addAllNumericKeys();
            numberKeys.addKey(OCRcEvent.VK_ENTER);

            timerSpecChBar = new TVTimerSpec();
            timerSpecChBar.addTVTimerWentOffListener(this);
        }

        public synchronized void start() {
            EventManager em = EventManager.getInstance();
            em.addUserEventListener(this, numberKeys);
        }

        public synchronized void stop() {
            EventManager em = EventManager.getInstance();
            em.removeUserEventListener(this);
        }

        public void reset() {
            resetKeyInputStack();
            // descheduleTimerChBar()를 항상 호출하면, 자칫 OSD를 지우는 timer도 해제될
            // 수 있기 때문에 mode를 확인하고 지우도록 함.
            if (timerModeChBar == TIMER_DCA_INPUT_STANDBY) {
                descheduleTimerChBar();
            }
        }

        private void resetKeyInputStack() {
            inputCount = 0;
            inputedNumber = 0;
            inputedString = "";
        }

        public void userEventReceived(UserEvent event) {
            if (event.getType() == KeyEvent.KEY_PRESSED) {
                int code = event.getCode();
                //->Kenneth[2015.10.15] : log util 추가
                //Log.checkLogOnOff(code, "EPG");
                //<-
                if (code == OCRcEvent.VK_ENTER) {
                    keyEnter();
                } else {
                    keyNumber(code - KeyEvent.VK_0);
                }
            }
        }

        private void scheduleTimerChBar(int timerMode) {
            if (Log.DEBUG_ON) {
                Log.printDebug("scheduleTimerChBar(" + timerMode + ")");
            }
            timerModeChBar = timerMode;
            switch (timerModeChBar) {
                /* DCA related */
                case TIMER_DCA_INPUT_STANDBY :
                    // wait 2.5 seconds for next number input
                    timerSpecChBar.setDelayTime(DCA_STANDBY_DELAY);
                    break;
                case TIMER_DCA_INPUT_WRONG :
                    timerSpecChBar.setDelayTime(DCA_WRONG_CHANNEL_DISPLAY_TIME);
                    break;
            }

            try {
                timer.scheduleTimerSpec(timerSpecChBar);
                isTimerChBarScheduled = true;
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }

        private void descheduleTimerChBar() {
            if (Log.DEBUG_ON) {
                Log.printDebug("descheduleTimerChBar(" + timerModeChBar + ")");
            }
            if (isTimerChBarScheduled) {
                timer.deschedule(timerSpecChBar);
                isTimerChBarScheduled = false;
                timerModeChBar = TIMER_NONE;
            }
        }

        private void decideAndShowChannel(int inputedNumber) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.decideAndShowChannel("+inputedNumber+")");
            synchronized (selectionLock) {
                if (!isFullPower) {
                    return;
                }
                Channel curChannel = channelController.getCurrent();
                Channel targetChannel = ChannelDatabaseBuilder.getInstance().getDatabase().getChannelByNumber(inputedNumber);

                if (targetChannel == null) {
                    // delay removing inputed wrong channel number by
                    // 0.5 seconds
                    // [AmuRO-2007.11.16] 채널없음 표시
                    showUnknownChannelInfo(String.valueOf(inputedNumber));
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ChannelZapper: not found");
                    }
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.decideAndShowChannel().run() : Call scheduleTimerChBar()");
                            scheduleTimerChBar(TIMER_DCA_INPUT_WRONG);
                        }
                    });
                } else {
                    boolean same = false;
                    // audio portal이 없으므로 무조건 아래처럼 처리
                    if (targetChannel.equals(curChannel) && (!App.R3_TARGET || EpgCore.epgVideoContext)) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(App.LOG_HEADER+"Target channel and current channel are same. Ignore...");
                        }
                        resetKeyInputStack();
                        showChannelInfo(curChannel);
                    } else {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.decideAndShowChannel() : Call zapController.descheduleTimerChNav()");
                        zapController.descheduleTimerChNav();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.decideAndShowChannel() : Call ChannelController.changeChannel()");
                        channelController.changeChannel(targetChannel, true, true, true);
//                        scheduleTimerChBar(TIMER_HIDE_CHANNEL_BAR);
                    }
                }
            }
        }

        private void keyEnter() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.keyEnter()");
            if (inputCount > 0) {
                descheduleTimerChBar();
                synchronized (selectionLock) {
                    // To show 3rd DCA input
                    final int chnNum = inputedNumber;
                    resetKeyInputStack();
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            decideAndShowChannel(chnNum);
                        }
                    });
                }
            }
        }

        private void keyExit() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.keyExit()");
            descheduleTimerChBar();
            if (inputCount > 0) {
                resetKeyInputStack();
                showChannelInfo(channelController.getCurrent());
            }
        }

        private void keyNumber(int number) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.keyNumber("+number+")");
            descheduleTimerChBar();
            // [cwkwak] Without this lock, channel number may be longer than 3
            // because this function can be called by two different thread,
            // EDT and private prefetch() function call thread.
            synchronized (selectionLock) {
                inputCount++;
                inputedNumber = inputedNumber * 10 + number;
                inputedString += Integer.toString(number);
                if (inputCount == digit) {
                    showChannelInfo(inputedString, "");
//                    scheduleTimerInEDT(TIMER_HIDE_CHANNEL_BAR);

                    // To show 3rd DCA input
                    final int chnNum = inputedNumber;
                    resetKeyInputStack();
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"InputController.keyNumber() : decideAndShowChannel("+chnNum+")");
                            decideAndShowChannel(chnNum);
                        }
                    });
                } else {
                    // Second, show inputed channel number and set timer for standby
                    showChannelInfo(inputedString, "");
                    scheduleTimerInEDT(TIMER_DCA_INPUT_STANDBY);
                }
            }
        }

        private void scheduleTimerInEDT(final int timerMode) {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    scheduleTimerChBar(timerMode);
                }
            });
        }

        public void timerWentOff(TVTimerWentOffEvent e) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"InputController.timerWentOff(): timerMode=" + timerModeChBar);
            }

            /* DCA related */
            switch (timerModeChBar) {
                case TIMER_DCA_INPUT_STANDBY :
                    // decideAndShowChannel() will deschedule timer.
                    if (dcaEnabled) {
                        decideAndShowChannel(inputedNumber);
                    } else {
                        hideChannelBar();
                    }
                    resetKeyInputStack();
                    break;
                case TIMER_DCA_INPUT_WRONG :
                    if (isFullPower && state == STATE_STARTED) {
                        // [cwkwak] show current channel number only if it's TV
                        // viewing state and resized channel zapper is enabled.
                        resetKeyInputStack();
                        showCurrentInfo();
                    } else {
                        hideChannelBar();
                    }
                    break;
//                case TIMER_HIDE_CHANNEL_BAR :
//                    descheduleTimerChBar();
//                    hideChannelBar();
//                    break;
            }
        }

    }

}

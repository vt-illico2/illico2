package com.alticast.illico.epg.navigator;

import java.awt.*;
import javax.media.Player;
import javax.tv.media.AWTVideoSize;
import javax.tv.media.AWTVideoSizeControl;
import javax.tv.service.selection.InvalidServiceComponentException;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextFactory;
import javax.tv.service.selection.PresentationChangedEvent;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextListener;
import javax.tv.xlet.XletContext;
import java.util.Vector;
import java.util.Enumeration;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.epg.VideoResizeListener;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import java.rmi.RemoteException;
import org.dvb.event.UserEvent;
import org.dvb.media.VideoTransformation;
import org.dvb.ui.*;
import org.ocap.system.ScaledVideoManager;
import org.ocap.service.AlternativeContentErrorEvent;
import javax.tv.util.TVTimerWentOffEvent;
import com.alticast.ui.*;

/**
 * <code>VideoPlane</code> is utility of resizing video pane.
 *
 * @author June Park
 */
public abstract class VideoPlane extends Container {

//    private static final Color tuneFailBg = Color.black;
//    private static final Color tuneFailBgDebug = new DVBColor(0, 0, 255, 128);

    private Vector listeners = new Vector();

    protected ServiceContext serviceContext;
    protected AWTVideoSizeControl vsc;

    // june - todo remove
//    private boolean clear = true;

//    protected BackgroundPlane background = new BackgroundPlane();
    // june - TODO fix
//    protected VideoImage videoImage = new VideoImage();

    protected final VideoImage background;

    protected int videoIndex;

    protected String logPrefix;

    public VideoPlane(int index) {
        this.videoIndex = index;
        logPrefix = App.LOG_HEADER+"VideoPlane[" + index + "]";
        this.serviceContext = Environment.getServiceContext(index);
        this.vsc = getVideoSizeControl();
        this.background = createBackground();
        this.add(background);
        background.setVisible(true);
        this.setBounds(Constants.SCREEN_BOUNDS);
    }

    public VideoImage createBackground() {
        return new VideoImage();
    }

    public void init() {
        background.init(videoIndex);
    }

    //->Kenneth[2015.2.28] 4K 
    public boolean isDualUhdPipVisible() {
        return background.isDualUhdNotSupportedState();
    }

    protected AWTVideoSizeControl getVideoSizeControl() {
        if (vsc != null) {
            return vsc;
        }
        AWTVideoSizeControl control;
        ServiceContentHandler[] handlers = serviceContext.getServiceContentHandlers();
        if (handlers == null) {
            return null;
        }
        for (int i = 0; i < handlers.length; i++) {
            if (handlers[i] != null && handlers[i] instanceof Player) {
                Player player = (Player) handlers[i];
                return (AWTVideoSizeControl) player.getControl("javax.tv.media.AWTVideoSizeControl");
            }
        }
        if (Log.WARNING_ON) {
            Log.printWarning(logPrefix + ": can't find AWTVideoSizeControl.");
        }
        return null;
    }

    /**
     * Return the current video size changed by this object.
     */
    public Rectangle getCurrentVideoSize() {
        return getVideoSizeControl().getSize().getDestination();
    }

    public synchronized void stop() {
        background.stop();
    }

    public abstract void conflictResolved(Channel ch);

    public synchronized boolean resize(Rectangle r) {
        if (r == null || Constants.SCREEN_BOUNDS.equals(r) || r.isEmpty()) {
            maximize();
            return true;
        }
        resizeVideo(r);
        background.start(r);
        return false;
    }

    public synchronized void maximize() {
        stop();
        resizeVideo(Constants.SCREEN_BOUNDS);
    }

    /**
     * Resize video pane as specified.
     *
     * @param dst
     * @return	Return a copy of the rectangle representing where the video is
     * 		to be displayed, in the coordinate system of the screen.
     */
    private boolean resizeVideo(Rectangle dst) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".resize = " + dst);
        }
        if (vsc == null) {
            Log.printWarning(logPrefix + ".resize: cannot get AWTVideoSizeControl !");
            return false;
        }
        Rectangle src = new Rectangle(vsc.getSourceVideoSize());
        AWTVideoSize size = new AWTVideoSize(src, dst);
        if (dst.width > 0 && dst.height > 0) {
            size = vsc.checkSize(size);
        }
        boolean result = vsc.setSize(size);
        if (result) {
            int x = dst.x;
            int y = dst.y;
            int w = dst.width;
            int h = dst.height;
            Enumeration en = listeners.elements();
            while (en.hasMoreElements()) {
                try {
                    ((VideoResizeListener) en.nextElement()).mainScreenResized(x, y, w, h);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        return result;
    }

    public void addVideoResizeListener(VideoResizeListener l) throws RemoteException {
        synchronized (listeners) {
            listeners.addElement(l);
        }
    }

    public void removeVideoResizeListener(VideoResizeListener l) throws RemoteException {
        synchronized (listeners) {
            listeners.removeElement(l);
        }
    }

    public void startUnsubscribed(Channel ch) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": startUnsubscribed " + ch);
        }
        background.startUnsubscribed(ch);
    }

    //->Kenneth[2015.1.30] 4K
    public void startIncompatibility(Channel ch) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": startIncompatibility" + ch);
        }
        background.startIncompatibility(ch);
    }

    //->Kenneth[2015.2.28] 4K
    public void startDualUhdPip(Channel ch) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": startDualUhdPip" + ch);
        }
        background.startDualUhdPip(ch);
    }

    public void startKeepAlive() {
        background.startKeepAlive();
    }

    public boolean startSdvError(short messageId, short responseId, int sourceId, boolean byForce) {
        return background.startSdvError(messageId, responseId, sourceId, byForce);
    }

    /** Video area.
    class BackgroundPlane extends Component implements EpgChannelEventListener {
        private DataCenter dataCenter = DataCenter.getInstance();

        private static final int PIP_BLOCK   = 0;
        private static final int PIP_GALAXIE = 1;
        private static final int PIP_PPV_IND = 2;
        private static final int PIP_PPV_VCC = 3;
        private static final int PIP_RADIO   = 4;
        private static final int PIP_UNSUB   = 5;
        private static final int PIP_VIRTUAL = 6;

        private String[] prefix = {
            "02_pip_block_b",
            "02_pip_block_g",
            "02_pip_block_ppv01",
            "02_pip_block_ppv02",
            "02_pip_block_r",
            "02_pip_block_un",
            "02_pip_block_v"
        };

        private String[] suffixBySize = { ".png", "_b.png" };

        private int currentPrefix;
        private int currentSize;

        private int fontSize = 19;

        private Image currentImage;
        private String text = "";
        private String text2 = "";

        private Color cText = new Color(191, 191, 191);

        protected boolean isShowing = false;

        // june - todo remove
        private boolean isUnsubscribed = false;


        public synchronized void start(Rectangle r) {
            setBounds(r);
            ChannelController cc = ChannelController.get(videoIndex);
            Channel ch = cc.getCurrent();
            cc.addChannelEventListener(BackgroundPlane.this);
            // 미가입 처리
            if (isUnsubscribed) {
                updateImage(PIP_UNSUB, currentSize, dataCenter.getString("pip.text_unsub"));
            } else if (ch != null) {
                setImage(ch, ch.getType(), cc.getState());
            }
            isShowing = true;
        }

        public synchronized void stop() {
            ChannelController.get(videoIndex).removeChannelEventListener(BackgroundPlane.this);
            updateImage(-1, 0, "");
            isShowing = false;
        }

        public void setBounds(Rectangle r) {
            super.setBounds(r);
            fontSize = (r.width + 1270) / 80;
            int newSize = (r.width <= 300) ? 0 : 1;
            updateImage(currentPrefix, newSize, text);
        }

        private synchronized void updateImage(int px, int size, String desc) {
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix + ": updateImage = " + px + ", " + size);
            }
            this.text = desc;
            if (px == currentPrefix && size == currentSize) {
                return;
            }
            if (currentImage != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ": flush image");
                }
                Image temp = currentImage;
                currentImage = null;
                temp.flush();
            }
            currentPrefix = px;
            currentSize = size;
            if (currentPrefix != -1) {
                String path = DataAdapterManager.IMAGE_RESOURCE_PATH + prefix[px] + suffixBySize[size];
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ": create image = " + path);
                }
                currentImage = Toolkit.getDefaultToolkit().createImage(path);
            }
            repaint();
        }

        private void setImage(Channel ch, short type, byte ccState) {
            switch (ccState) {
                case ChannelController.BLOCK_BY_RATING:
                    updateImage(PIP_BLOCK, currentSize, dataCenter.getString("pip.text_blocked_program"));
                    return;
                case ChannelController.BLOCK_BY_CHANNEL:
                    updateImage(PIP_BLOCK, currentSize, dataCenter.getString("pip.text_blocked_channel"));
                    return;
//                case ChannelController.STOPPED:
//                    return;
            }
            switch (type) {
                case Channel.TYPE_GALAXIE:
                    text2 = ch.fullName;
                    updateImage(PIP_GALAXIE, currentSize, dataCenter.getString("pip.text_galaxie"));
                    return;
                case Channel.TYPE_RADIO:
                    updateImage(PIP_RADIO, currentSize, dataCenter.getString("pip.text_radio"));
                    return;
                case Channel.TYPE_VIRTUAL:
                    updateImage(PIP_VIRTUAL, currentSize, "");
                    return;
                case Channel.TYPE_PPV:
                    Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
                    if (p != null && p instanceof PpvProgram) {
                        String indicator = ((PpvProgram) p).indicator;
                        if (PpvProgram.INDICATOR_IND.equals(indicator)) {
                            updateImage(PIP_PPV_IND, currentSize, dataCenter.getString("pip.text_IND"));
                            return;
                        } else if (PpvProgram.INDICATOR_VCC.equals(indicator)) {
                            updateImage(PIP_PPV_VCC, currentSize, dataCenter.getString("pip.text_VCC"));
                            return;
                        }
                    }
                    String call = ch.getName();
                    if (call.startsWith("IN")) {
                        updateImage(PIP_PPV_IND, currentSize, dataCenter.getString("pip.text_IND"));
                    } else if (call.startsWith("VC")) {
                        updateImage(PIP_PPV_VCC, currentSize, dataCenter.getString("pip.text_VCC"));
                    } else {
                        updateImage(PIP_VIRTUAL, currentSize, ch.fullName);
                    }
                    return;
            }
            updateImage(-1, currentSize, "");
        }

        public void selectionRequested(Channel ch, short type, boolean auth) {
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix + ": selectionRequested = " + ch);
            }
            setImage(ch, type, ChannelController.CLEAR);
        }

        public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix + ": selectionBlocked = " + blockType + ", " + ch);
            }
            setImage(ch, type, blockType);
        }

        public void serviceStopped(Channel ch) {
        }

        public void showChannelInfo(Channel ch) {
        }

        public void programUpdated(Channel ch, Program p) {
        }

        public synchronized void startUnsubscribed(int sid) {
            isUnsubscribed = true;

            if (isShowing) {
                updateImage(PIP_UNSUB, currentSize, dataCenter.getString("pip.text_unsub"));
            }
        }

        public synchronized void stopUnsubscribed() {
            isUnsubscribed = false;

            if (isShowing) {
                if (currentPrefix == PIP_UNSUB) {
                    updateImage(-1, currentSize, "");
                }
            }
        }

        public boolean isClear() {
            return currentImage == null;
        }

        public void repaint() {
            if (isShowing) {
                super.repaint();
            }
        }

        public void paint(Graphics g) {
            int cp = currentPrefix;
            if (clear && (cp == PIP_PPV_IND || cp == PIP_PPV_VCC)
                    || App.R3_TARGET && videoIndex == 0 && !EpgCore.epgVideoContext) {
                paintTrans(g);
                return;
            }
            Image im = currentImage;
            if (im != null) {
                paintInfo(g, im);
            } else if (!clear) {
                if (Log.EXTRA_ON) {
                    g.setColor(tuneFailBgDebug);
                } else {
                    g.setColor(tuneFailBg);
                }
                g.fillRect(0, 0, getWidth(), getHeight());
            } else {
                paintTrans(g);
            }
        }

        private void paintTrans(Graphics g) {
            DVBGraphics dg = (DVBGraphics) g;
            DVBAlphaComposite old = dg.getDVBComposite();
            try {
                dg.setDVBComposite(DVBAlphaComposite.Src);
            } catch (UnsupportedDrawingOperationException ex) {
            }
            dg.clearRect(0, 0, getWidth(), getHeight());
            try {
                dg.setDVBComposite(old);
            } catch (UnsupportedDrawingOperationException ex) {
            }
        }

        private void paintInfo(Graphics g, Image image) {
            int w = getWidth();
            int h = getHeight();
            g.setColor(tuneFailBg);
            g.fillRect(0, 0, w, h);
            g.drawImage(image, 0, 0, w, h, this);
            String s = this.text;
            if (s == null || s.length() == 0) {
                return;
            }
            int y;
            int x = w / 2;
            int fs = fontSize;

            int cp = currentPrefix;
            g.setColor(cText);
            switch (cp) {
                case PIP_GALAXIE:
                case PIP_RADIO:
                    if (fs == 19) {
                        fs = 17;
                    }
                    x -= 4;
                    y = (80 * h - 2507) / 141;
                    if (cp == PIP_GALAXIE) {
                        y = y + 14;
                        g.setFont(FontResource.BLENDER.getFont(Math.max(fs - 3, 17)));
                        g.drawString(text2, x + 1, y);
                        y = y - 24;
                    }
                    g.setFont(FontResource.BLENDER.getFont(fs));
                    g.drawString(s, x, y);
                    break;
                default:
                    y = (38 * h - 957) / 47;
                    g.setFont(FontResource.BLENDER.getFont(fs));
                    GraphicUtil.drawStringCenter(g, s, x, y);
                    break;
            }

        }
    }

 */
}

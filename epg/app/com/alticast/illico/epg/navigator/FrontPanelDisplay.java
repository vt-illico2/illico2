package com.alticast.illico.epg.navigator;

import java.util.ArrayList;
import javax.tv.util.*;

import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.ocap.hardware.frontpanel.FrontPanelManager;
import org.ocap.hardware.frontpanel.TextDisplay;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
import com.alticast.illico.epg.*;

/**
 * <code>FrontPanelDisplay</code>
 *
 * @author Charles CW Kwak
 * @since 2007. 1. 23
 */
public class FrontPanelDisplay implements ResourceClient, TVTimerWentOffListener {
    private FrontPanelManager fpm = FrontPanelManager.getInstance();

    private static FrontPanelDisplay instance = null;

    private TVTimerSpec displayTimer;

    private long delay;

    public static synchronized FrontPanelDisplay getInstance() {
        if (instance == null) {
            instance = new FrontPanelDisplay();
        }
        return instance;
    }

    private FrontPanelDisplay() {
        displayTimer = new TVTimerSpec();
        delay = DataCenter.getInstance().getLong("FRONT_PANEL_CHANGE_DELAY");
        if (delay > 0) {
            displayTimer.setDelayTime(delay);
        }
        displayTimer.addTVTimerWentOffListener(this);
    }

    public void showText(String str) {
        displayText(str);
        startTimer();
    }

    public void clear() {
        stopTimer();
        displayText("    ");
    }

    public synchronized void showClock() {
        if (!EpgCore.navigableStbMode) {
            Log.printInfo("FrontPanelDisplay.showClock : skipped");
            if (!EpgCore.isBlockedMode) {
                clear();
            }
            return;
        }
        stopTimer();
        Log.printDebug(App.LOG_HEADER+"FrontPanelDisplay.showClock");
        if (fpm.reserveTextDisplay(this)) {
            TextDisplay led = fpm.getTextDisplay();
            boolean french = Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage());
            try {
                led.setClockDisplay(french ? TextDisplay.TWENTYFOUR_HOUR_CLOCK : TextDisplay.TWELVE_HOUR_CLOCK,
                    led.getBlinkSpec(), led.getScrollSpec(), led.getBrightSpec(), led.getColorSpec());
            } catch (Throwable ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("FrontPanelDisplay.displayText : fail to reserve front panel");
        }
    }

    // all synchronized methods call this method
    private synchronized void displayText(String str) {
//        if (!EpgCore.activeStbMode) {
//            Log.printInfo("FrontPanelDisplay.displayText : skipped");
//            return;
//        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"FrontPanelDisplay.displayText = " + str);
        }

        if (fpm.reserveTextDisplay(this)) {
            TextDisplay led = fpm.getTextDisplay();
            try {
                led.setTextDisplay(new String[] { str }, led.getBlinkSpec(),
                    led.getScrollSpec(), led.getBrightSpec(), led.getColorSpec());
            } catch (Throwable ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("FrontPanelDisplay.displayText : fail to reserve front panel");
        }
    }

    private synchronized void startTimer() {
        if (delay <= 0) {
            return;
        }
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(displayTimer);
        try {
            displayTimer = timer.scheduleTimerSpec(displayTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    private synchronized void stopTimer() {
        TVTimer.getTimer().deschedule(displayTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        showClock();
    }

    /** Resoure Client */
    public void notifyRelease(ResourceProxy proxy) {
    }

    public void release(ResourceProxy proxy) {
    }

    public boolean requestRelease(ResourceProxy proxy, Object requestData) {
        return true;
    }
}

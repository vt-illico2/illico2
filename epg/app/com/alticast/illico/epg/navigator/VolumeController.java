package com.alticast.illico.epg.navigator;

import java.awt.*;
import java.util.Enumeration;
import java.awt.event.*;
import com.videotron.tvi.illico.log.Log;
import org.dvb.event.*;
import org.dvb.ui.*;
import org.dvb.user.*;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.hardware.*;
import org.ocap.hardware.device.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.GeneralPreferenceUtil;
import com.videotron.tvi.illico.util.Constants;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;

/**
 * <code>VolumeController</code>
 *
 * @author June Park
 */
public class VolumeController implements UserEventListener, DataUpdateListener, VideoOutputPortListener {

    private static final String UP_NAME_VOLUME_LEVEL = "Volume Level";

    public static final String KEY_HDMI_CHECK_COUNT = "HDMI_check_count";

    public static final int MAX_VOLUME = 30;
    public static final int INCREASE_STEP = 1;

    public int currentVolume;

    private UserEventRepository keyEvents;

    /** AudioOutputPort. */
    private AudioOutputPort audioOutput = null;
    /** VideoOutputPort. */
    private VideoOutputPort hdmiPort = null;
    private VideoOutputSettings hdmiSettings = null;

    private boolean hdmiConnected = false;
    public static int hdmiCheckCount = 0;

    private static final int MAX_HDMI_CHECK_COUNT = 3;

    /** VolumeBar UI. */
    private VolumeBar volumeBar;

    private boolean enabled = true;

    /** the singleton instance. */
    private static VolumeController instance = new VolumeController();

    /** Returns the singleton instance. */
    public static VolumeController getInstance() {
        return instance;
    }

    /** Constructor. */
    private VolumeController() {
        HostSettings hostSettings = (HostSettings) Host.getInstance();
        Enumeration en = hostSettings.getAudioOutputs();
        if (en != null && en.hasMoreElements()) {
            audioOutput = (AudioOutputPort) en.nextElement();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("VolumeController: AudioOutputPort = " + audioOutput);
        }

        en = Host.getInstance().getVideoOutputPorts();
        while (en.hasMoreElements()) {
            VideoOutputPort port = (VideoOutputPort) en.nextElement();
            if (port.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
                hdmiPort = port;
                hdmiSettings = (VideoOutputSettings) port;
                if (Log.DEBUG_ON) {
                    Log.printDebug("VolumeController: HDMI VideoOutputPort = " + port);
                }
                break;
            }
        }

        if (hdmiSettings != null) {
            hdmiSettings.addListener(this);
            setHdmiConnected(hdmiSettings.isDisplayConnected());
        }

        this.currentVolume = Math.max(0, (int) (getVolume() * MAX_VOLUME));
        volumeBar = new VolumeBar(MAX_VOLUME, INCREASE_STEP, currentVolume);

        if (audioOutput != null) {
//            setMute(false);
            audioOutput.setMuted(false);
            keyEvents = new UserEventRepository("EPG.VolumeKeys");
            keyEvents.addKey(OCRcEvent.VK_VOLUME_UP);
            keyEvents.addKey(OCRcEvent.VK_VOLUME_DOWN);
            keyEvents.addKey(OCRcEvent.VK_MUTE);
            EventManager.getInstance().addUserEventListener(this, keyEvents);

            hostSettings.setSystemVolumeKeyControl(false);
            hostSettings.setSystemMuteKeyControl(false);
        }

        DataCenter dc = DataCenter.getInstance();
        dc.addDataUpdateListener(PreferenceNames.AUDIO_VOLUME_CONTROL, this);
        dc.addDataUpdateListener(KEY_HDMI_CHECK_COUNT, this);
        Object up = dc.get(PreferenceNames.AUDIO_VOLUME_CONTROL);
        if (up != null) {
            dataUpdated(PreferenceNames.AUDIO_VOLUME_CONTROL, null, up);
        }
        up = dc.get(KEY_HDMI_CHECK_COUNT);
        if (up != null) {
            dataUpdated(KEY_HDMI_CHECK_COUNT, null, up);
        }
    }

    public float getVolume() {
        if (audioOutput != null) {
            return audioOutput.getLevel();
        }
        return -1f;
    }

    private void setCurrentVolume(int vol) {
        if (audioOutput != null) {
            vol = Math.min(MAX_VOLUME, Math.max(0, vol));
            audioOutput.setLevel((float) vol / MAX_VOLUME);
            this.currentVolume = vol;
            volumeBar.setVolume(currentVolume);
        }
    }

    public boolean isMuted() {
        if (audioOutput != null) {
            return audioOutput.isMuted();
        }
        return true;
    }

    public void setMute(boolean mute) {
        if (audioOutput != null) {
            audioOutput.setMuted(mute);
            volumeBar.setMuted(mute);
        }
    }

    public void toggleMute() {
        setMute(!isMuted());
    }

    public void userEventReceived(UserEvent e) {
        if (!enabled || e.getType() != KeyEvent.KEY_PRESSED) {
            return;
        }
//        if (hdmiConnected) {
//            if (hdmiCheckCount < MAX_HDMI_CHECK_COUNT) {
//                showHdmiFullNoti();
//            } else {
//                showHdmiSimpleNoti();
//            }
//            return;
//        }
        int code = e.getCode();
        switch (code) {
            case OCRcEvent.VK_VOLUME_UP:
                if (isMuted()) {
                    setMute(false);
                }
                setCurrentVolume(currentVolume + INCREASE_STEP);
                break;
            case OCRcEvent.VK_VOLUME_DOWN:
                if (isMuted()) {
                    setMute(false);
                }
                setCurrentVolume(currentVolume - INCREASE_STEP);
                break;
            case OCRcEvent.VK_MUTE:
                toggleMute();
                break;
            default:
                return;
        }
    }

    private synchronized void setHdmiConnected(boolean connected) {
        if (Log.DEBUG_ON) {
            Log.printDebug("VolumeController: HDMI connection = " + (connected ? "ON" : "OFF"));
        }
        this.hdmiConnected = connected;
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (PreferenceNames.AUDIO_VOLUME_CONTROL.equals(key)) {
            if (Definitions.AUDIO_VOLUME_CONTROL_TV.equals(value)) {
                enabled = false;
                setMute(false);
                setCurrentVolume(MAX_VOLUME);
            } else if (Definitions.AUDIO_VOLUME_CONTROL_STB.equals(value)) {
                enabled = true;
                setCurrentVolume(loadLevel());
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("VolumeController: up value = " + value + ", enabled = " + enabled);
            }
        } else if (KEY_HDMI_CHECK_COUNT.equals(key)) {
            int count = 0;
            try {
                count = Integer.parseInt(value.toString());
            } catch (Exception ex) {
                Log.print(ex);
            }
            hdmiCheckCount = count;
        }
    }

    public void dataRemoved(String key)  {
    }

    public void saveLevel() {
        GeneralPreferenceUtil.write(UP_NAME_VOLUME_LEVEL, String.valueOf(currentVolume));
    }

    private int loadLevel() {
        String value = GeneralPreferenceUtil.read(UP_NAME_VOLUME_LEVEL);
        int level = MAX_VOLUME;
        if (value != null) {
            try {
                level = Integer.parseInt(value);
            } catch (NumberFormatException ex) {
            }
        }
        return level;
    }

    public void setHdmiCheckCount(int count) {
        hdmiCheckCount = Math.max(count, MAX_HDMI_CHECK_COUNT);
        UpManager.getInstance().set(KEY_HDMI_CHECK_COUNT, String.valueOf(count));
    }

    private void showHdmiFullNoti() {
        HdmiFullNotiRenderer r = new HdmiFullNotiRenderer(268-30);
        final String[] buttons = new String[] { Popup.BUTTON_OK, "epg.dont_show_again" };

        PopupListener pl = new PopupListener() {
            public void notifySelected(Popup popup, String key) {
                if (Popup.BUTTON_OK.equals(key)) {
                    setHdmiCheckCount(hdmiCheckCount + 1);
                } else if (buttons[1].equals(key)) {
                    setHdmiCheckCount(MAX_HDMI_CHECK_COUNT);
                }
            }
            public void notifyCanceled(Popup p, boolean byTimer) {
                if (!byTimer) {
                    setHdmiCheckCount(hdmiCheckCount + 1);
                }
            }
        };
        DataCenter dataCenter = DataCenter.getInstance();
        r.setMessage(dataCenter.getString("epg.hdmi_msg"));
        long autoCloseDelay = dataCenter.getInt("HDMI_VOLUME_FULL_NOTI_DURATION", 60) * Constants.MS_PER_SECOND;
        PopupController.getInstance().showCustomPopup(
                dataCenter.getString("epg.hdmi_title"),
                buttons, r, pl, autoCloseDelay);
    }

    private void showHdmiSimpleNoti() {
        HdmiSimpleNotiRenderer r = new HdmiSimpleNotiRenderer();
        DataCenter dataCenter = DataCenter.getInstance();
        long autoCloseDelay = dataCenter.getInt("HDMI_VOLUME_SIMPLE_NOTI_DURATION", 5) * Constants.MS_PER_SECOND;
        PopupController.getInstance().showCustomPopup(
                dataCenter.getString("epg.hdmi_simple_title"),
                null, r, null, autoCloseDelay);

    }

    //////////////////////////////////////////////////////////////////////////
    // VideoOutputPortListener
    //////////////////////////////////////////////////////////////////////////

    public void configurationChanged(VideoOutputPort source, VideoOutputConfiguration oldConfig, VideoOutputConfiguration newConfig) {
    }

    public void connectionStatusChanged(VideoOutputPort source, boolean status) {
        setHdmiConnected(status);
    }

    public void enabledStatusChanged(VideoOutputPort source, boolean status) {
    }

}

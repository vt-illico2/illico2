package com.alticast.illico.epg.navigator;

import com.alticast.ui.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.navigator.EpgChannelEventListener;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.radio.RadioChannelPage;
import com.alticast.illico.epg.virtualchannel.VirtualChannelPage;
import com.alticast.illico.epg.ppv.PpvController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.awt.event.KeyEvent;
import org.dvb.event.*;
import org.ocap.service.AlternativeContentErrorEvent;
import javax.tv.service.selection.*;
import java.awt.*;
import org.dvb.ui.*;
//->Kenneth[2015.4.10] PIP 상태 알기 위해 추가
import com.videotron.tvi.illico.ixc.monitor.*;

/**
 * PIP 던 Main 이던 Video영역 대신 graphic으로 가리는 부분을 위한 Component.
 * Galaxie, Radio, Virtual, 미가입, PPV 등등.
 * Resize 된 영역을 위해서 주로 사용하고, Main 화면이 전체화면이 가릴 경우는 ChannelBackground 에서 처리
 */
public class VideoImage extends Component implements EpgChannelEventListener, ServiceContextListener {
    private DataCenter dataCenter = DataCenter.getInstance();

    private static final Color tuneFailBg = Color.black;
    private static final Color tuneFailBgDebug = new DVBColor(0, 0, 255, 128);

    protected static final int PIP_CLEAR   = -1;
    protected static final int PIP_SDV     = 0;
    protected static final int PIP_GALAXIE = 1;
    protected static final int PIP_PPV_IND = 2;
    protected static final int PIP_PPV_VCC = 3;
    protected static final int PIP_RADIO   = 4;
    protected static final int PIP_UNSUB   = 5;
    protected static final int PIP_VIRTUAL = 6;
    protected static final int PIP_BLOCK_PROGRAM   = 7;
    protected static final int PIP_BLOCK_CHANNEL   = 8;
    protected static final int PIP_SDV_KEEP_ALIVE  = 9;
    protected static final int PIP_UNSUB_ISA       = 10;
    //->Kenneth [2015.1.30] : 4K 추가 : UHD 박스가 아닌데 UHD 채널에 접근시 나타나는 페이지
    protected static final int PIP_NOT_SUPPORT_UHD = 11;
    //->Kenneth [2015.2.28] : 4K 추가 : UHD 박스인데 PIP 로 동시에 UHD 채널을 표시할 경우 보이는 페이지
    protected static final int PIP_DUAL_UHD_NOT_SUPPORTED = 12;

    protected int state = PIP_CLEAR;

    // type 별 image 이름
    private String[] prefix = {
        "02_pip_block_un",
        "02_pip_block_g",
        "02_pip_block_ppv01",
        "02_pip_block_ppv02",
        "02_pip_block_r",
        "02_pip_block_un",
        "02_pip_block_v",
        "02_pip_block_b",
        "02_pip_block_b",
        "02_pip_block_un",
        "02_pip_isa",
        //->Kenneth [2015.1.30] : 4K 
        "02_pip_block_uhd"
        //->Kenneth [2015.2.28] : 4K 
        ,"02_pip_block_uhd"
    };

    // type 별 TOC
    private String[] texts = {
        "pip.text_sdv",
        "pip.text_galaxie",
        "pip.text_IND",
        "pip.text_VCC",
        "pip.text_radio",
        "pip.text_unsub",
        null,
        "pip.text_blocked_program",
        "pip.text_blocked_channel",
        "pip.text_sdv",
        "pip.text_isa",
        //->Kenneth [2015.1.30] : 4K 
        null
        //->Kenneth [2015.2.28] : 4K 
        , null
    };

    // _b 는 큰 PIP 영역을 위해
    private String[] suffixBySize = { ".png", "_b.png" };

    private int currentSize;    // 0 or 1

    private int fontSize = 19;

    private Image currentImage;
    private String text = "";
    private String text2 = "";

    protected Channel channel;

    private static final Color cText = new Color(191, 191, 191);

    //->Kenneth[2015.3.23] : 4K : ProgramDetailsRenderer 에서 갖다쓰기 위해 public 으로 바꿈.
    public boolean isShowing = false;
    protected boolean clear = false;

    int videoIndex;
    ServiceContext serviceContext;
    ChannelController channelController;

    String logPrefix;

    //->Kenneth [2015.2.4] : 4K 
    private String[] uhdShortFuncText;

    //->Kenneth [2015.2.28] : 4K 
    private String[] uhdLimitText;

    public void init(int videoIndex) {
        this.videoIndex = videoIndex;
        logPrefix = App.LOG_HEADER+"VideoImage[" + videoIndex + "]";
        channelController = ChannelController.get(videoIndex);
        channelController.addChannelEventListener(this);
        serviceContext = channelController.getServiceContext();
        serviceContext.addListener(this);
    }

    public void receiveServiceContextEvent(ServiceContextEvent e) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.receiveServiceContextEvent("+e+")");
        clear = (e instanceof PresentationChangedEvent) && !(e instanceof AlternativeContentErrorEvent)
                || (e instanceof NormalContentEvent);
        if (clear) {
            synchronized (this) {
                if (state == PIP_UNSUB || state == PIP_UNSUB_ISA || state == PIP_SDV) {
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.receiveServiceContextEvent() Call setState(PIP_CLEAR)");
                    setState(PIP_CLEAR);
                }
            }
        }
        if (isShowing) {
            repaint();
        }
    }

    public synchronized void start(Rectangle r) {
        if (this instanceof ChannelBackground) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.start("+r+") : This is ChannelBackground");
        } else {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.start("+r+")");
        }
        Channel ch = channelController.getCurrent();
        this.setBounds(r);
        isShowing = true;
    }

    //->Kenneth [2015.2.4] : 4K 
    private void resetUhdShortFuncText(int size) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdShortFuncText()");
        if (ConfigManager.getInstance().epgConfig == null) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdShortFuncText() : epgConfig = null so return");
            return;
        }
        String src = ConfigManager.getInstance().epgConfig.getUhdShortFuncText();
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdShortFuncText() src = "+src);
        if (src == null) {
            return;
        }
        if (size == 0) {
            // 작은 PIP
            uhdShortFuncText = TextUtil.split(src, FontResource.getFontMetrics(FontResource.BLENDER.getFont(16)), getWidth() - 10);
        } else {
            // 큰 PIP
            uhdShortFuncText = TextUtil.split(src, FontResource.getFontMetrics(FontResource.BLENDER.getFont(20)), getWidth() - 10);
        }
    }

    //->Kenneth [2015.2.28] : 4K 
    private void resetUhdLimitText(int size) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdLimitText()");
        if (ConfigManager.getInstance().epgConfig == null) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdLimitText() : epgConfig = null so return");
            return;
        }
        String src = ConfigManager.getInstance().epgConfig.getUhdLimitText();
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.resetUhdLimitText() src = "+src);
        if (src == null) {
            return;
        }
        if (size == 0) {
            // 작은 PIP
            uhdLimitText = TextUtil.split(src, FontResource.getFontMetrics(FontResource.BLENDER.getFont(16)), getWidth() - 10);
        } else {
            // 큰 PIP
            uhdLimitText = TextUtil.split(src, FontResource.getFontMetrics(FontResource.BLENDER.getFont(20)), getWidth() - 10);
        }
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.stop()");
        flushImage();
        isShowing = false;
    }

    public void setBounds(Rectangle r) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.setBounds("+r+")");
        super.setBounds(r);
        fontSize = (r.width + 1270) / 80;
        //->Kenneth [2015.3.21] : 4K : weather 에 보이는 video width 가 322 이므로 이에 맞게 그리도록 함.
        //int newSize = (r.width <= 300) ? 0 : 1;
        int newSize = (r.width <= 330) ? 0 : 1;

        //->Kenneth [2015.2.4] : 4K 
        resetUhdShortFuncText(newSize);
        resetUhdLimitText(newSize);
        updateImage(newSize);
    }

    private void flushImage() {
        synchronized (tuneFailBg) {
            if (currentImage != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + " : flush image");
                }
                Image temp = this.currentImage;
                this.currentImage = null;
                temp.flush();
            }
        }
    }

    private void updateImage(int size) {
        int state = this.state;
        synchronized (tuneFailBg) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+logPrefix + " : updateImage = " + state + ", " + size);
            }
            flushImage();
            currentSize = size;
            if (state != PIP_CLEAR) {
                String textKey = texts[state];
                if (state == PIP_UNSUB_ISA) {
                    this.text = "";
                    this.text2 = null;
                    String str = dataCenter.getString(textKey);
                    String[] splits = TextUtil.split(str, FontResource.getFontMetrics(FontResource.BLENDER.getFont(fontSize - 2)), getWidth() - 10);
                    if (splits.length > 0) {
                        this.text = splits[0];
                    }
                    if (splits.length > 1) {
                        this.text2 = splits[1];
                    }
                } else {
                    if (textKey != null) {
                        this.text = dataCenter.getString(textKey);
                    } else {
                        this.text = "";
                    }
                }
                //->Kenneth [2015.1.30] : 4K : null 체크 추가 
                if (prefix[state] != null) {
                    String path = DataAdapterManager.IMAGE_RESOURCE_PATH + prefix[state] + suffixBySize[size];
                    if (Log.DEBUG_ON) {
                        Log.printDebug(logPrefix + " : create image = " + path);
                    }
                    currentImage = Toolkit.getDefaultToolkit().createImage(path);
                }
            } else {
                this.text = "";
            }
            repaint();
        }
    }

    public void selectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.selectionRequested("+ch+", "+type+", "+auth+")");
        if (this instanceof ChannelBackground) Log.printDebug("(This is ChannelBackground)");
        changeState(ch, type, ChannelController.CLEAR, auth);
    }

    public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.selectionBlocked("+ch+", "+type+", "+blockType+", "+auth+")");
        if (this instanceof ChannelBackground) Log.printDebug("(This is ChannelBackground)");
        changeState(ch, type, blockType, auth);
    }

    public synchronized void serviceStopped(Channel ch) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.serviceStopped("+ch+")");
        if (this instanceof ChannelBackground) Log.printDebug("(This is ChannelBackground)");
        this.channel = ch;
        setState(PIP_CLEAR);
    }

    public void showChannelInfo(Channel ch) {
    }

    public void programUpdated(Channel ch, Program p) {
    }

    protected synchronized void changeState(Channel ch, short type, byte ccState, boolean auth) {
        if (this instanceof ChannelBackground) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"(ChannelBackground)VideoImage.changeState("+ch+", "+type+", "+ccState+", "+auth+")");
        } else {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState("+ch+", "+type+", "+ccState+", "+auth+")");
        }
        text2 = null;
        this.channel = ch;
        switch (ccState) {
            case ChannelController.BLOCK_BY_RATING:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : BLOCK_BY_RATING");
                if (type != Channel.TYPE_PPV) {
                    if (!auth) {
                        //->Kenneth[2016.8.24] CYO Bundling/A la carte
                        setState(ch.isISAPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
                        //setState(ch.isPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
                        //<-
                        return;
                    }
                }
                setState(PIP_BLOCK_PROGRAM);
                return;
            case ChannelController.BLOCK_BY_CHANNEL:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : BLOCK_BY_CHANNEL");
                // unsubcribed channel is prior to blocked channel
                if (type != Channel.TYPE_PPV) {
                    if (!auth) {
                        //->Kenneth[2016.8.24] CYO Bundling/A la carte
                        setState(ch.isISAPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
                        //setState(ch.isPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
                        //<-
                        return;
                    }
                }
                setState(PIP_BLOCK_CHANNEL);
                return;
        }
        switch (type) {
            case Channel.TYPE_GALAXIE:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_GALAXIE");
                setState(PIP_GALAXIE);
                text2 = ch.fullName;
                return;
            case Channel.TYPE_RADIO:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_RADIO");
                setState(PIP_RADIO);
                text2 = ch.fullName;
                return;
            case Channel.TYPE_VIRTUAL:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_VIRTUAL");
                setState(PIP_VIRTUAL);
                return;
            case Channel.TYPE_PPV:
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_PPV");
                //->Kenneth[2015.10.15] DDC-114
                int reason = IncompatibilityChannelPanel.resetReasonCode(ch);
                if (reason != IncompatibilityChannelPanel.REASON_UHD_COMPATIBLE) {
                    //->Kenneth[2015.3.8] channel selection 된 뒤에 incompatibility page 가 뜨니까 그 사이에 PPV 화면이
                    // 한참 동안 보이게 됨. 그래서 setState 하기 전에 체크해서 PPV 화면이 안 보이도록 함
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_PPV : Call set(PIP_NOT_SUPPORT_UHD)");
                    setState(PIP_NOT_SUPPORT_UHD);
                    return;
                }
                /*
                //->Kenneth[2015.3.8] channel selection 된 뒤에 incompatibility page 가 뜨니까 그 사이에 PPV 화면이
                // 한참 동안 보이게 됨. 그래서 setState 하기 전에 체크해서 PPV 화면이 안 보이도록 함
                if (EpgCore.getInstance().needToShowIncompatibilityPopup(ch)) {
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : TYPE_PPV : Call set(PIP_NOT_SUPPORT_UHD)");
                    setState(PIP_NOT_SUPPORT_UHD);
                    return;
                }
                */
                //<-
                Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
                if (p != null && p instanceof PpvProgram) {
                    String indicator = ((PpvProgram) p).indicator;
                    if (PpvProgram.INDICATOR_IND.equals(indicator)) {
                        setState(PIP_PPV_IND);
                        return;
                    } else if (PpvProgram.INDICATOR_VCC.equals(indicator)) {
                        setState(PIP_PPV_VCC);
                        return;
                    }
                }
                String call = ch.getName();
                if (call.startsWith("IN")) {
                    setState(PIP_PPV_IND);
                } else if (call.startsWith("VC")) {
                    setState(PIP_PPV_VCC);
                } else {
                    setState(PIP_VIRTUAL);
                }
                return;
        }
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.changeState() : Call setState(PIP_CLEAR)");
        setState(PIP_CLEAR);
    }

    //->Kenneth[2015.2.28] 4K 
    public boolean isDualUhdNotSupportedState() {
        if (state == PIP_DUAL_UHD_NOT_SUPPORTED) return true;
        return false;
    }

    protected synchronized void setState(int newState) {
        int oldState = this.state;
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".setState : " + getStateStr(oldState) + " -> " + getStateStr(newState));
        }
        if (this instanceof ChannelBackground) Log.printDebug("(This is ChannelBackground)");
        this.state = newState;
        if (isShowing) {
            if (oldState != newState) {
                updateImage(currentSize);
            } else {
                repaint();
            }
        }
    }

    public final void startUnsubscribed(Channel ch) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.startUnsubscribed("+ch+")");
        //->Kenneth[2016.8.24] CYO Bundling/A la carte
        setState(ch.isISAPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
        //setState(ch.isPurchasable() ? PIP_UNSUB_ISA : PIP_UNSUB);
        //<-
    }

    //->Kenneth[2015.1.30] 4K
    public final void startIncompatibility(Channel ch) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.startIncompatibility("+ch+")");
        //->Kenneth[2015.6.3] R5 : channel 에 받은 ch 를 지정.
        channel = ch;
        setState(PIP_NOT_SUPPORT_UHD);
    }

    //->Kenneth[2015.2.28] 4K
    public final void startDualUhdPip(Channel ch) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.startDualUhdPip("+ch+")");
        setState(PIP_DUAL_UHD_NOT_SUPPORTED);
    }

    protected synchronized void startKeepAlive() {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.startKeepAlive()");
        setState(PIP_SDV_KEEP_ALIVE);
    }

    public synchronized boolean startSdvError(short messageId, short responseId, int sourceId, boolean byForce) {
        Log.printDebug(logPrefix + ".startSdvBarker: " + sourceId);
        if (state != PIP_CLEAR && state != PIP_SDV) {
            return false;
        }
        if (channel == null || !(channel instanceof SdvChannel) || channel.getSourceId() != sourceId) {
            Log.printDebug(logPrefix + ".startSdvBarker: case 1: " + channel);
            return false;
        }
        if (!byForce && channelController.isPresenting() && channel.isPresenting(channelController)) {
            Log.printDebug(logPrefix + ".startSdvBarker: case 2: " + byForce);
            return false;
        }
        setState(PIP_SDV);
        return true;
    }

    public boolean isClear() {
        return currentImage == null;
    }

    public void repaint() {
        if (isShowing) {
            super.repaint();
        }
    }

    //->Kenneth [2015.2.4] : 4K 추가
    private String getStateStr(int stateInt) {
        String stateStr = "UNKNOWN";
        switch (stateInt) {
            case PIP_CLEAR:
                stateStr = "PIP_CLEAR";
                break;
            case PIP_SDV:
                stateStr = "PIP_SDV";
                break;
            case PIP_GALAXIE:
                stateStr = "PIP_GALAXIE";
                break;
            case PIP_PPV_IND:
                stateStr = "PIP_PPV_IND";
                break;
            case PIP_PPV_VCC:
                stateStr = "PIP_PPV_VCC";
                break;
            case PIP_RADIO:
                stateStr = "PIP_RADIO";
                break;
            case PIP_UNSUB:
                stateStr = "PIP_UNSUB";
                break;
            case PIP_VIRTUAL:
                stateStr = "PIP_VIRTUAL";
                break;
            case PIP_BLOCK_PROGRAM:
                stateStr = "PIP_BLOCK_PROGRAM";
                break;
            case PIP_BLOCK_CHANNEL:
                stateStr = "PIP_BLOCK_CHANNEL";
                break;
            case PIP_SDV_KEEP_ALIVE:
                stateStr = "PIP_SDV_KEEP_ALIVE";
                break;
            case PIP_UNSUB_ISA:
                stateStr = "PIP_UNSUB_ISA";
                break;
            case PIP_NOT_SUPPORT_UHD:
                stateStr = "PIP_NOT_SUPPORT_UHD";
                break;
            //->Kenneth [2015.2.28] : 4K 
            case PIP_DUAL_UHD_NOT_SUPPORTED:
                stateStr = "PIP_DUAL_UHD_NOT_SUPPORTED";
                break;
        }
        return stateStr;
    }

    public void paint(Graphics g) {
        int cs = this.state;
        if (clear && (cs == PIP_PPV_IND || cs == PIP_PPV_VCC)
                || App.R3_TARGET && videoIndex == 0 && !EpgCore.epgVideoContext) {
            paintTrans(g);
            return;
        }
        Image im = currentImage;
        if (im != null) {
            paintInfo(g, im);
        } else if (!clear) {
            if (Log.EXTRA_ON) {
                g.setColor(tuneFailBgDebug);
            } else {
                g.setColor(tuneFailBg);
            }
            g.fillRect(0, 0, getWidth(), getHeight());
        } else {
            paintTrans(g);
        }
    }

    private void paintTrans(Graphics g) {
        DVBGraphics dg = (DVBGraphics) g;
        DVBAlphaComposite old = dg.getDVBComposite();
        try {
            dg.setDVBComposite(DVBAlphaComposite.Src);
        } catch (UnsupportedDrawingOperationException ex) {
        }
        //->Kenneth[2015.2.17] 4K tuning
        // 작은 pip 화면 보면 1px 정도 메인 video 가 살짝 보이는 현상이 있어서 2 px 씩 작게 뚫음.
        //->Kenneth[2015.4.10] PIP 는 해결되는데 메뉴, pvr, 기타 scaled video 보이는 곳에는
        // 하단 정보 bar 와 video 영역이 불일치하는 문제 발생
        // 따라서 실제 2개의 video 가 보이는 PIP 상태에만 적용하고 (1px 로 줄임)
        // 그렇지 않은 일반 scaled video 에서는 예전대로 한다.
        if (EpgCore.getInstance().getMonitorState() == MonitorService.PIP_STATE) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.paintTrans(PIP : smaller)");
            dg.clearRect(1, 1, getWidth()-2, getHeight()-2);
        } else {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoImage.paintTrans(Non PIP : bigger)");
            dg.clearRect(0, 0, getWidth(), getHeight());
        }
        try {
            dg.setDVBComposite(old);
        } catch (UnsupportedDrawingOperationException ex) {
        }
    }

    private void paintInfo(Graphics g, Image image) {
        int w = getWidth();
        int h = getHeight();
        g.setColor(tuneFailBg);
        g.fillRect(0, 0, w, h);
        g.drawImage(image, 0, 0, w, h, this);
        String s = this.text;
        int y;
        int x = w / 2;
        int fs = fontSize;

        int cp = state;
        g.setColor(cText);
        switch (cp) {
            case PIP_GALAXIE:
            case PIP_RADIO:
                if (s == null || s.length() == 0) {
                    return;
                }
                if (fs == 19) {
                    fs = 17;
                }
                x -= 4;
                y = (80 * h - 2507) / 141;
//                if (cp == PIP_GALAXIE) {
                    y = y + 14;
                    g.setFont(FontResource.BLENDER.getFont(Math.max(fs - 3, 15)));
                    String s2 = TextUtil.shorten(text2, g.getFontMetrics(), w - x - 5);
                    g.drawString(s2, x + 1, y);
                    y = y - 24;
//                }
                g.setFont(FontResource.BLENDER.getFont(fs));
                g.drawString(s, x, y);
                break;
            //->Kenneth [2015.2.4] : 4K 추가
            case PIP_NOT_SUPPORT_UHD:
                try {
                    if (uhdShortFuncText == null || uhdShortFuncText.length == 0) {
                        return;
                    }
                    y = (38 * h - 957) / 47;
                    if (currentSize == 0) {
                        // 작은 PIP (Grid 등)
                        g.setFont(FontResource.BLENDER.getFont(16));
                        y = y - 25;
                        for (int i = 0 ; i < uhdShortFuncText.length ; i++) {
                            if (i == 3) break;
                            String aLine = uhdShortFuncText[i];
                            if (aLine != null) {
                                GraphicUtil.drawStringCenter(g, aLine, x, y+(18*i));
                            }
                        }
                    } else {
                        // 큰 PIP (Menu 등)
                        g.setFont(FontResource.BLENDER.getFont(20));
                        y = y - 30;
                        for (int i = 0 ; i < uhdShortFuncText.length ; i++) {
                            if (i == 3) break;
                            String aLine = uhdShortFuncText[i];
                            if (aLine != null) {
                                GraphicUtil.drawStringCenter(g, aLine, x, y+(22*i));
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.print(e);
                }
                break;
            //->Kenneth [2015.2.28] : 4K 추가
            case PIP_DUAL_UHD_NOT_SUPPORTED:
                try {
                    if (uhdLimitText == null || uhdLimitText.length == 0) {
                        return;
                    }
                    y = (38 * h - 957) / 47;
                    if (currentSize == 0) {
                        // 작은 PIP (Grid 등)
                        g.setFont(FontResource.BLENDER.getFont(16));
                        y = y - 25;
                        for (int i = 0 ; i < uhdLimitText.length ; i++) {
                            if (i == 3) break;
                            String aLine = uhdLimitText[i];
                            if (aLine != null) {
                                GraphicUtil.drawStringCenter(g, aLine, x, y+(18*i));
                            }
                        }
                    } else {
                        // 큰 PIP (Menu 등)
                        g.setFont(FontResource.BLENDER.getFont(20));
                        y = y - 30;
                        for (int i = 0 ; i < uhdLimitText.length ; i++) {
                            if (i == 3) break;
                            String aLine = uhdLimitText[i];
                            if (aLine != null) {
                                GraphicUtil.drawStringCenter(g, aLine, x, y+(22*i));
                            }
                        }
                    }
                } catch (Exception e) {
                    Log.print(e);
                }
            default:
                if (s == null || s.length() == 0) {
                    return;
                }
                y = (38 * h - 957) / 47;
                if (cp == PIP_UNSUB_ISA) {
                    fs -= 2;
                    g.setFont(FontResource.BLENDER.getFont(fs));
                    if (text2 != null) {
                        y = y + 4;
                        GraphicUtil.drawStringCenter(g, this.text2, x, y);
                        y = y - 20;
                    }
                } else {
                    g.setFont(FontResource.BLENDER.getFont(fs));
                }
                GraphicUtil.drawStringCenter(g, s, x, y);
                break;
        }

    }
}

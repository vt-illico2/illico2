package com.alticast.illico.epg.navigator;

import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.sdv.*;

import javax.tv.service.selection.*;
import java.util.*;
import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContext;
import javax.tv.locator.Locator;
import javax.tv.util.*;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterface;
import org.ocap.hardware.*;
import org.ocap.dvr.*;
import org.ocap.net.OcapLocator;
import org.ocap.shared.media.*;
import org.dvb.service.selection.DvbServiceContext;

public class ProgramUpdateChecker implements TVTimerWentOffListener, EpgDataListenerInternal {

    ChannelController cc;
    String logPrefix;

    Channel channel;
    Program program;
    Program viewingProgram;

    TVTimerSpec updateTimer = new TVTimerSpec();
    TVTimerSpec tsbTimer = new TVTimerSpec();

    private static EpgDataManager edm = EpgDataManager.getInstance();

    public ProgramUpdateChecker(ChannelController channelController) {
        this.cc = channelController;
        logPrefix = App.LOG_HEADER+"ProgramUpdateChecker[" + cc.getVideoIndex() + "]";
        updateTimer.addTVTimerWentOffListener(this);
        edm.addListener(this);

        tsbTimer.setTime(500);
        tsbTimer.setRepeat(true);
        tsbTimer.setAbsolute(false);
        tsbTimer.setRegular(false);
        tsbTimer.addTVTimerWentOffListener(new RefreshTimerListener());
    }

    public void enableMediaTimeChecker(boolean tsb) {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(tsbTimer);
        if (tsb) {
            try {
                tsbTimer = timer.scheduleTimerSpec(tsbTimer);
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

    public void updateViewingProgram() {
        long mediaTime = cc.getMediaTime();
        Program old = viewingProgram;
        if (old != null && old.contains(mediaTime)) {
            return;
        } else if (program != null && program.contains(mediaTime)) {
            viewingProgram = program;
        } else {
            viewingProgram = edm.getProgram(channel, mediaTime);
        }
        if (viewingProgram != null && !viewingProgram.equals(old)) {
            cc.viewingProgramChanged(channel, viewingProgram);
        }
    }

    public void setProgram(Channel ch) {
        setProgram(ch, edm.getCurrentProgram(ch));
        enableMediaTimeChecker(false);
    }

    private void setProgram(Channel ch, Program p) {
        synchronized (cc) {
            if (p != null) {
                if (!ch.equals(channel) || program == null || program.startTime < p.startTime) {
                    program = p;
                    startTimer(p.getEndTime());
                }
            }
            channel = ch;
        }
        updateViewingProgram();
    }

    private synchronized void startTimer(long time) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".startTimer = " + new Date(time));
        }
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(updateTimer);
        updateTimer.setAbsoluteTime(time + 600L);
        try {
            timer.scheduleTimerSpec(updateTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent ev) {
        long time = Clock.getRoundedTime(System.currentTimeMillis(), Constants.MS_PER_SECOND);
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".timerWentOff = " + new Date(time));
        }
        synchronized (cc) {
            Program np = edm.getProgram(channel, time);
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix + " new =  " + np + ", old = " + program);
            }
            setProgram(channel, np);
            cc.programUpdated(channel, np);
        }
    }

    public void epgDataUpdated(byte type, long from, long to) {
        synchronized (cc) {
            Channel ch = channel;
            if (program == null || from > program.startTime || program.startTime >= to || ch == null) {
                return;
            }
            if (ch.getType() == Channel.TYPE_PPV) {
                if (type != EpgDataListener.PPV_DATA) {
                    return;
                }
            } else {
                if (type == EpgDataListener.PPV_DATA) {
                    return;
                }
            }
            Program np = edm.getProgram(ch, program.startTime);
            if (program.equals(np)) {
                Log.printInfo(logPrefix + ".epgDataUpdated: same program = " + np);
                return;
            }
            Log.printInfo(logPrefix + ".epgDataUpdated: changed program = " + np);
            setProgram(ch, np);
            cc.programUpdated(channel, np);
        }
    }

    class RefreshTimerListener implements TVTimerWentOffListener {
        public void timerWentOff(TVTimerWentOffEvent event) {
            updateViewingProgram();
        }
    }

}

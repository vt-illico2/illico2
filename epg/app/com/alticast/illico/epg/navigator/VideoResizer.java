package com.alticast.illico.epg.navigator;

import java.awt.*;
import javax.media.Player;
import javax.tv.media.AWTVideoSize;
import javax.tv.media.AWTVideoSizeControl;
import javax.tv.service.selection.InvalidServiceComponentException;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextFactory;
import javax.tv.service.selection.PresentationChangedEvent;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextListener;
import javax.tv.xlet.XletContext;
import java.util.Vector;
import java.util.Enumeration;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.ParentalControl;
import com.alticast.illico.epg.ChannelBackground;
import java.rmi.RemoteException;
import org.dvb.event.UserEvent;
import org.dvb.media.VideoTransformation;
import org.dvb.ui.*;
import org.ocap.system.ScaledVideoManager;
import javax.tv.util.TVTimerWentOffEvent;
import com.alticast.ui.*;

/**
 * <code>VideoResizer</code> is utility of resizing video pane.
 *
 * @author June Park
 */
public class VideoResizer extends VideoPlane implements VideoController {

    protected short display = DO_NOT_SHOW;
    protected byte infoOption = PROGRAM_INFO;

    private static final int INFO_HEIGHT = 30;
    //->Kenneth[2015.2.12] : 줄리 요청에 의해서 더 어둡게 50% -> 70%
    //private static final Color infoBg = new DVBColor(0, 0, 0, 150);
    private static final Color infoBg = new DVBColor(0, 0, 0, 180);

    private LayeredUI ui;

    private VideoInfo info = new VideoInfo();

    private static VideoResizer instance = new VideoResizer();

    public static VideoResizer getInstance() {
        return instance;
    }

    private VideoResizer() {
        super(0);
        this.add(info, 0);
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);

        LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.add(this);

        try {
            // 초기에 한번만 설정하자.
            ScaledVideoManager.getInstance().setInitialBackground(
                                    serviceContext, new VideoTransformation());
        } catch (Throwable t) {
            Log.print(t);
        }

        ui = WindowProperty.EPG_SCALED_VIDEO.createLayeredWindow(window, Constants.SCREEN_BOUNDS);
        ui.deactivate();
    }

    public VideoImage createBackground() {
        return ChannelBackground.getInstance();
    }

    public synchronized void stop() {
        ui.deactivate();
        super.stop();
        info.setVisible(false);
        setDisplayOption(DO_NOT_SHOW);
        ChannelZapper.getInstance().setChannelBanner(null);
        if (EpgCore.getInstance().getMonitorState() != MonitorService.FULL_SCREEN_APP_STATE) {
            ChannelBackground.getInstance().resume();
        }
    }

    public void conflictResolved(Channel ch) {
    }

    public void resize(int x, int y, int w, int h, short display) {
        resize(x, y, w, h, display, PROGRAM_INFO);
    }

    public void resize(int x, int y, int w, int h, short display, byte info) {
        resize(new Rectangle(x, y, w, h), display, info);
    }

    public synchronized void resize(Rectangle r, short display) {
        resize(r, display, PROGRAM_INFO);
    }

    public synchronized void resize(Rectangle r, short display, byte infoOption) {
        Log.printInfo(App.LOG_HEADER+"VideoResizer.resize: " + r + ", " + infoOption);
        boolean maximized = super.resize(r);
        if (maximized) {
            return;
        }
        ChannelBackground.getInstance().pause();
        setInfoOption(infoOption);
        setDisplayOption(display);
        info.setBounds(r.x, r.y + r.height - INFO_HEIGHT, r.width, INFO_HEIGHT);
        if (display == DO_NOT_SHOW) {
            info.setVisible(false);
        } else {
            ChannelZapper.getInstance().setChannelBanner(info);
            info.startTuner(videoIndex);
        }
        ui.activate();
    }

    protected void setDisplayOption(short displayOption) {
        this.display = displayOption;
    }

    protected void setInfoOption(byte info) {
        this.infoOption = info;
    }

    /** Information area. */
    class VideoInfo extends ChannelBanner {
        VideoInfoRenderer vir = new VideoInfoRenderer();
        Image scaledLogo;
        String title;
		//->Kenneth : 4K by June
        int videoDefinition;

        public VideoInfo() {
            setRenderer(vir);
        }

        public void start() {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfo.start()");
            setVisible(true);
            repaint();
        }

        public void stop() {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfo.stop()");
            setVisible(false);
        }

        public boolean isActive() {
            return isVisible();
        }

        protected void startVideoContext(String videoContext) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfo.startVideoContext("+videoContext+")");
            super.startVideoContext(videoContext);
            if (infoOption == PROGRAM_INFO && display != DO_NOT_SHOW) {
                title = "" + SharedMemory.getInstance().get(videoContext + SharedDataKeys.VIDEO_PROGRAM_NAME);
            } else {
                title = name;
            }
			//->Kenneth : 4K by June
            videoDefinition = definition;
            scaledLogo = defaultScaledLogo;
        }

        protected void readyData(Channel ch) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfo.readyData("+ch+")");
            if (infoOption == PROGRAM_INFO && display != DO_NOT_SHOW) {
                Program p = dm.getCurrentProgram(ch);
                title = ParentalControl.getTitle(p);
                //->Kenneth[2015.6.19] R5 : VDTRMASTER-5483 : 그냥 채널의 정보만을 사용하도록 하자.
                videoDefinition = definition;
                /*
				//->Kenneth : 4K by June
                if (definition != Constants.DEFINITION_4K) {
                    videoDefinition = Math.min(definition, p.getDefinition());
                //->Kenneth[2015.5.8] VDTRMASTER-5414 : 우정이가 추가한 내용임
                } else {
                    videoDefinition = definition;
                }
                */
            } else {
                title = name;
				//->Kenneth : 4K by June
                videoDefinition = definition;
            }
            scaledLogo = ch.getScaledLogo();
        }

        public void timerWentOff(TVTimerWentOffEvent event) {
            if (display != SHOW) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfo.timerWentOff() : Call stop()");
                stop();
            }
        }
    }

    class VideoInfoRenderer extends Renderer {
        Color cText = new Color(204, 204, 204);
        Font fChannel = FontResource.BLENDER.getFont(20);
        Font fProgram = FontResource.BLENDER.getFont(19);
        Image iHd = DataCenter.getInstance().getImage("icon_hd.png");
        //->Kenneth[2015.2.2] 4K : TV Feed 의 데이터는 여기서 그린다는 것 명심
        Image iUhd = DataCenter.getInstance().getImage("icon_uhd.png");

        FontMetrics fmProgram = FontResource.getFontMetrics(fProgram);

        public void prepare(UIComponent c) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"VideoResizer.VideoInfoRenderer.prepare()");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void paint(Graphics g, UIComponent c) {
            //->Kenneth[2015.4.11] : R5 : 여러개 수정함
            VideoInfo cb = (VideoInfo) c;
            int width = cb.getWidth();
            int height = cb.getHeight();

            g.setColor(infoBg);
            g.fillRect(0, 0, width, height);

            Channel ch = cb.channel;
            if (ch != null) {
                Util.drawChannelIcon(g, ch, 3, 3, c);
            }

            g.setColor(cText);
            g.setFont(fChannel);
            // R5
            g.drawString(cb.number, 26+10, 20);
            //->Kenneth[2015.9.4] Julie 요청에 의해
            String videoContext = EpgCore.getInstance().getVideoContextName();//EPG, PVR, VOD
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"VideoInfoRenderer : videoContext = "+videoContext);
            if ("PVR".equals(videoContext)) {
                if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"VideoInfoRenderer : Draw pvrLogo");
                g.drawImage(ChannelBanner.pvrLogo, 150, 1, 80, 24, c);
            } else if ("VOD".equals(videoContext)) {
                if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"VideoInfoRenderer : Draw vodLogo");
                g.drawImage(ChannelBanner.vodLogo, 150, 1, 80, 24, c);
            } else {
                if (cb.scaledLogo != null) {
                    // R5
                    if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"VideoInfoRenderer : Draw ChannelBanner.scaledLogo");
                    g.drawImage(cb.scaledLogo, 150, 1, c);
                }
            }
            /*
            if (cb.scaledLogo != null) {
                // R5
                g.drawImage(cb.scaledLogo, 150, 1, c);
            }
            */
            //<-
            /* R5
            g.setFont(fProgram);
            int sw = cb.definition > 0 ? width - 156 - 32 : width - 156 - 5;    // TODO - 4K
            String s = TextUtil.shorten(cb.title, fmProgram, sw);
            g.drawString(s, 156, 19);
            */
            // R5
			//->Kenneth : 4K by June
            switch (cb.videoDefinition) {
                case Channel.DEFINITION_4K: // TODO
                    g.drawImage(iUhd, 80, 8, c);
                    break;
                case Channel.DEFINITION_HD:
                    g.drawImage(iHd, 80, 8, c);
                    break;
            }
        }
    }

}

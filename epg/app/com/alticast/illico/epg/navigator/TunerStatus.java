package com.alticast.illico.epg.navigator;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.tuner.*;
import com.alticast.illico.epg.sdv.mc.*;
import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.monitor.*;

import javax.tv.util.*;
import javax.tv.locator.*;
import javax.tv.service.*;
import org.davic.net.tuning.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.hardware.Host;

/**
 * TunerStatus
 *
 * @author June Park
 */
public class TunerStatus implements NetworkInterfaceListener, TVTimerWentOffListener {
    // for SDV session

    private static final int TUNER_ID_INDEX = 6;
    /** original session ID from MAC address. */
    private byte[] sessionID;
    private String sessionIdString;

    NetworkInterface ni;
//    Channel channel;
    String logPrefix;
    int tunerIndex;
    int freq;
    RecordingRequest req;
    BufferingRequest br;

    TVTimerSpec bufferFlushTimer = new TVTimerSpec();

    long startTime;

    private static boolean isShownErrorPopup = false;
    private static final Short MOT500_LOCK = new Short((short) 0);

    /** Constructor. */
    public TunerStatus(NetworkInterface networkInterface, int index) {
        this.ni = networkInterface;
        ni.addNetworkInterfaceListener(this);
        this.tunerIndex = index;
        this.logPrefix = App.LOG_HEADER+"TunerStatus[" + index + "]";
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": created with " + ni);
        }
        // session id
        String macAddr = Host.getInstance().getReverseChannelMAC();
        if (Log.DEBUG_ON) {
            Log.printDebug("TunerStatus: getReverseChannelMAC = " + macAddr);
        }
        this.sessionID = new byte[10];
        int len = macAddr.length();
        int pos = 0;
        for (int i = 0; i < 10; i++) {
            if (pos + 2 <= len) {
                sessionID[i] = (byte) Short.parseShort(macAddr.substring(pos, pos+2), 16);
                pos += 3;
            } else {
                break;
            }
        }
        sessionID[6] = (byte) index;

        StringBuffer sb = new StringBuffer(40);
        for (int i = 0; i < 7; i++) {
            byte b = sessionID[i];
            sb.append(Character.forDigit(b >> 4 & 0xf, 16));
            sb.append(Character.forDigit(b & 0xf, 16));
            sb.append('.');
        }
        sessionIdString = sb.toString().toUpperCase();

        bufferFlushTimer.setRepeat(false);
        bufferFlushTimer.setAbsolute(false);
        bufferFlushTimer.addTVTimerWentOffListener(this);
    }

    private static long getKeepAliveDuration() {
        Object o = SharedMemory.getInstance().get(PvrService.DATA_KEY_TSB_KEEP_ALIVE_DURATION);
        if (o != null && o instanceof Long) {
            return ((Long) o).longValue();
        } else {
            return 0L;
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // Control APIs
    /////////////////////////////////////////////////////////////////////////

    public synchronized void setRecording(RecordingRequest r) {
        req = r;
        if (req == null) {
            Log.printDebug(logPrefix + ": recording stopped");
            cancelBuffering();
        }
    }

//    public synchronized void _setChannel(Channel ch) {
//        channel = ch;
//    }

    public synchronized BufferingRequest setBuffering(BufferingRequest br) {
        stopCancelBufferingTimer();
        if (br == null) {
            this.br = null;
            return null;
        }
        Service service = br.getService();
        if (service == null) {
            this.br = br;
            startTime = System.currentTimeMillis();
            return br;
        }
        BufferingRequest old = null;
        BufferingRequest[] brs = TunerManager.manager.getBufferingRequests();
        for (int i = 0; i < brs.length; i++) {
            if (brs[i] == br) {
                this.br = br;
                startTime = System.currentTimeMillis();
                return br;
            } else if (service.equals(brs[i].getService())) {
                old = brs[i];
            }
        }
        Log.printWarning(logPrefix + ": old BufferingRequest = " + old);
        return old;
    }

    public synchronized void startCancelBufferingTimer() {
        if (br != null) {
            this.br = br;
        }
        TVTimer timer = stopCancelBufferingTimer();
        long dur = getKeepAliveDuration();
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": start buffer flush timer = " + dur);
        }
        if (dur <= 0) {
            return;
        }
        bufferFlushTimer.setTime(dur);
        try {
            bufferFlushTimer = timer.scheduleTimerSpec(bufferFlushTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    public synchronized TVTimer stopCancelBufferingTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(bufferFlushTimer);
        return timer;
    }

    public synchronized void cancelBuffering() {
        Log.printDebug(logPrefix + ".cancelBuffering = " + br);
        BufferingRequest br = this.br;
        if (br != null) {
            ChannelController cc = getChannelController();
            if (cc != null) {
                Log.printWarning(logPrefix + ".cancelBuffering is ignored !!");
                return;
            }
            try {
                ((OcapRecordingManager) RecordingManager.getInstance()).cancelBufferingRequest(br);
                ContentionHandler.savedBuffers.remove(br.getService());
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ".canceled = " + br);
                    TunerManager.dumpBuffers();
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            this.br = null;
        }
    }

    public synchronized void cancelBuffering(Service service) {
        Log.printDebug(logPrefix + ".cancelBuffering");
        BufferingRequest br = this.br;
        if (br != null) {
            Service thisService = br.getService();
            if (service != null && service.equals(thisService)) {
                Log.printDebug(logPrefix + ".cancelBuffering : same service");
                cancelBuffering();
            } else {
                Log.printDebug(logPrefix + ".cancelBuffering : service mismatch = " + service);
            }
        }
    }


    /////////////////////////////////////////////////////////////////////////
    // Listeners
    /////////////////////////////////////////////////////////////////////////

    /** NetworkInterfaceListener. */
    public void receiveNIEvent(NetworkInterfaceEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".receiveNIEvent = " + e);
        }
		if (e instanceof NetworkInterfaceTuningOverEvent) {
            NetworkInterfaceTuningOverEvent ntoe = (NetworkInterfaceTuningOverEvent) e;
            int status = ntoe.getStatus();
            if (status == NetworkInterfaceTuningOverEvent.SUCCEEDED) {
                freq = getFrequency();
                if (EpgCore.navigableStbMode) {
                    hideErrorMessage();
                }
            } else if (status == NetworkInterfaceTuningOverEvent.FAILED) {
                if (EpgCore.navigableStbMode) {
                    switch (EpgCore.getInstance().getMonitorState()) {
                        case MonitorService.PIP_STATE:
                        case MonitorService.TV_VIEWING_STATE:
                            showErrorMessage();
                            break;
                    }
                }
            }
        }
    }

    private int getFrequency() {
        if (!ni.isReserved()) {
            return 0;
        }
        Locator loc = ni.getLocator();
        if (loc != null) {
            String locStr = loc.toString();
            if (locStr.startsWith("ocap://f=0x")) {
                int index = locStr.indexOf('.');
                if (index > 11) {
                    try {
                        return (Integer.parseInt(locStr.substring(11, index), 16) / 1000000);
                    } catch (Exception ex) {
                    }
                }
            }
        }
        return 0;
    }

    /////////////////////////////////////////////////////////////////////////
    // Status APIs
    /////////////////////////////////////////////////////////////////////////

    public boolean isReserved() {
        return ni.isReserved();
    }

    public ChannelController getChannelController() {
        return ChannelController.getChannelController(ni);
    }

    public UsageInfo getUsage() {
        return new UsageInfo();
    }

    class UsageInfo {
        boolean watchingRecording = false;
        String string;

        public UsageInfo() {
            string = getString();
        }

        public String toString() {
            return string;
        }

        public String getString() {
            if (!ni.isReserved()) {
                return null;
            }
            String name = null;
            int number = 0;

            ChannelController cc = getChannelController();
            StringBuffer sb = new StringBuffer();
            if (cc != null) {
                sb.append("CC[");
                sb.append(cc.getVideoIndex());
                sb.append(']');
                Channel ch = cc.getCurrent();
                if (ch != null) {
                    name = ch.getName();
                    number = ch.getNumber();
                }
                watchingRecording = true;
            }
            RecordingRequest r = req;
            if (r != null) {
                watchingRecording = true;
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append("REC[");
                try {
                    sb.append(r.getId());
                } catch (Exception ex) {
                }
                sb.append(']');
                if (name == null) {
                    name = (String) AppData.get(r, AppData.CHANNEL_NAME);
                    number = AppData.getInteger(r, AppData.CHANNEL_NUMBER);
                }
            }
            if (br != null) {
                if (sb.length() > 0) {
                    sb.append(", ");
                }
                sb.append("TSB");
            }
            if (sb.length() == 0) {
                return "other";
            }
            if (name != null) {
                sb.append(", ch#");
                sb.append(number);
                sb.append(' ');
                sb.append(name);
            }
            return sb.toString();
        }
    }

    public String getStatus() {
        Locator loc = ni.getLocator();
        String locStr;
        if (loc != null) {
            locStr = loc.toString();
            if (locStr.startsWith("ocap://f=0x")) {
                int index = locStr.indexOf('.');
                if (index > 11) {
                    try {
                        locStr = locStr + " (" + (Integer.parseInt(locStr.substring(11, index), 16) / 1000000) + "MHz)";
                    } catch (Exception ex) {
                    }
                }
            }
        } else {
            locStr = "N/A";
        }
        String usage = getUsage().toString();
        return (ni.isReserved() ? "reserved" : "free") + ", " + locStr + (usage == null ? "" : " " + getUsage());
    }

    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".timerWentOff = " + br);
            TunerManager.dumpBuffers();
        }
        cancelBuffering();
    }


    ////////////////////////////////////////////////////////////////////////
    // Weak Signal
    ////////////////////////////////////////////////////////////////////////

	/**
	 * Shows the message popup using the ErrorMessage.
	 */
	private void showErrorMessage() {
        //->Kenneth[2015.2.4] 에뮬에서 팝업 자꾸 떠서 여기서 막음
        if (Environment.EMULATOR) {
            return;
        }
        UsageInfo usage = getUsage();
        Log.printError("TunerUnlocked : tuner=" + tunerIndex + " : freq=" + freq + " : " + getUsage());
        if (usage.watchingRecording
                && !EpgCore.getInstance().isSpecialVideoState()
                && Host.getInstance().getPowerMode() != Host.LOW_POWER) {
            synchronized (MOT500_LOCK) {
                isShownErrorPopup = true;
                //->Kenneth[2017.8.24] R7.4 : camera 채널인 경우 mot510 으로 변환
                String errorId = "MOT500";
                try {
                    ChannelController cc = getChannelController();
                    if (cc.getCurrent().getType() == Channel.TYPE_CAMERA) {
                        errorId = "MOT510";
                    }
                } catch (Exception e) {
                    Log.print(e);
                }
                EpgCore.getInstance().showErrorMessage(errorId, null);
                //EpgCore.getInstance().showErrorMessage("MOT500", null);
                //<-
            }
        }
	}

	/**
	 * Hides the popup.
	 */
	private void hideErrorMessage() {
        synchronized (MOT500_LOCK) {
            if (isShownErrorPopup) {
                Log.printError("TunerLocked : tuner=" + tunerIndex + " : freq=" + freq);
                isShownErrorPopup = false;
                EpgCore.getInstance().hideErrorMessage();
            }
        }
	}

}


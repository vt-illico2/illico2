package com.alticast.illico.epg.navigator;

import java.awt.*;
import java.awt.event.KeyEvent;

import javax.media.Player;
import javax.tv.media.AWTVideoSize;
import javax.tv.media.AWTVideoSizeControl;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;
import org.ocap.system.ScaledVideoManager;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.App;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.ParentalControl;
import com.alticast.illico.epg.UpManager;
import com.alticast.illico.epg.data.ChannelDatabase;
import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.data.EpgDataManager;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.RecordingStatus;
import com.alticast.illico.epg.ui.ChannelBanner;
import com.alticast.illico.epg.ui.EpgFooter;
import com.alticast.illico.epg.tuner.*;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;

/**
 * PipController.
 *
 * @author June Park
 */
public class PipController extends VideoPlane implements LayeredKeyHandler, EpgChannelEventListener, TVTimerWentOffListener {

    private ChannelController channelController;
    private DataCenter dataCenter = DataCenter.getInstance();
    private EpgDataManager edm = EpgDataManager.getInstance();

    private static final byte QUARTER      = 16;
    private static final byte HALF         = 32;
    private byte size = QUARTER;

    private static final int TOP_LEFT     = 0;
    private static final int TOP_RIGHT    = 1;
    private static final int BOTTOM_RIGHT = 2;
    private static final int BOTTOM_LEFT  = 3;
    private int position = TOP_LEFT;

    private static final Rectangle[] RECTANGLES = {
        new Rectangle(52, 40, 284, 160),
        new Rectangle(624, 40, 284, 160),
        new Rectangle(624, 319, 284, 160),
        new Rectangle(52, 319, 284, 160),
    };
    private static final Rectangle HALF_MAIN = new Rectangle(52, 122-20, 422, 238);
    private static final Rectangle HALF_PIP = new Rectangle(486, 122-20, 422, 238);

    private static final String[] UP_POSITION_NAMES = {
        Definitions.PIP_POSITION_TOP_LEFT,
        Definitions.PIP_POSITION_TOP_RIGHT,
        Definitions.PIP_POSITION_BOTTOM_RIGHT,
        Definitions.PIP_POSITION_BOTTOM_LEFT,
    };

    // timer
    private static final long DEFAULT_CLOSE_TIME = 5000L;
    private TVTimerSpec autoCloseTimer;

    private LayeredUI ui;

    private MovingEffect showingEffectTop;
    private MovingEffect hidingEffectTop;
    private MovingEffect showingEffectBottom;
    private MovingEffect hidingEffectBottom;

    private Channel currentChannel;
    private String[] pipProgramText;
    private String halfProgramText;
    private Image currentLogo;

    private EpgFooter footerHalf = new EpgFooter();
    private EpgFooter footerQuarter = new EpgFooter();

    private PipPanel panel = new PipPanel();
    private PipBorder border = new PipBorder();
    private PipDetail detail = new PipDetail();

    private boolean firstTime = true;

    private boolean pending = false;

    private boolean swapEnabled = true;

    public PipController() {
        super(1);
        channelController = ChannelController.get(1);

        LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);

        ui = WindowProperty.EPG_PIP.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
        window.add(this);
        window.add(border);
        window.add(detail);
        window.add(panel);

        Point fromTop = new Point(0, -100);
        Point fromBottom = new Point(0, 100);
        Point to = new Point(0, 0);
        showingEffectTop = new MovingEffect(detail, 10,
                    fromTop, to, MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        showingEffectTop.setClipBounds(0, 0, detail.getWidth(), detail.getHeight());
        hidingEffectTop = new MovingEffect(detail, 10,
                    to, fromTop, MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
        hidingEffectTop.setClipBounds(0, 0, detail.getWidth(), detail.getHeight());

        showingEffectBottom = new MovingEffect(detail, 10,
                    fromBottom, to, MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        showingEffectBottom.setClipBounds(0, 0, detail.getWidth(), detail.getHeight());
        hidingEffectBottom = new MovingEffect(detail, 10,
                    to, fromBottom, MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
        hidingEffectBottom.setClipBounds(0, 0, detail.getWidth(), detail.getHeight());

        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setDelayTime(DEFAULT_CLOSE_TIME);
        autoCloseTimer.addTVTimerWentOffListener(this);

        footerHalf.setBounds(0, 488, 906, 25);
        footerQuarter.setBounds(0, 488, 906, 25);
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.start()");
        if (ui.isActive()) {
            return;
        }
        background.setVisible(false);
        detail.setVisible(false);
        border.setVisible(false);
        channelController.addChannelEventListener(this);

        if (App.R3_TARGET) {
            swapEnabled = EpgCore.epgVideoContext;
        } else {
            swapEnabled = true;
        }

        footerHalf.reset();
        if (swapEnabled) {
            footerHalf.addButton(PreferenceService.BTN_A, "epg.pip_swap");
        }
        footerHalf.addButton(PreferenceService.BTN_B, "epg.pip_size");
        footerHalf.addButton(PreferenceService.BTN_D, "epg.options");

        footerQuarter.reset();
        if (swapEnabled) {
            footerQuarter.addButton(PreferenceService.BTN_A, "epg.pip_swap");
        }
        footerQuarter.addButton(PreferenceService.BTN_B, "epg.pip_size");
        footerQuarter.addButton(PreferenceService.BTN_C, "epg.pip_position");
        footerQuarter.addButton(PreferenceService.BTN_D, "epg.options");

        Rectangle bgBounds;
        if (Definitions.PIP_SIZE_SIDE_BY_SIDE.equals(dataCenter.get(PreferenceNames.PIP_SIZE))) {
            size = HALF;
            bgBounds = HALF_PIP;
        } else {
            size = QUARTER;
            Object pos = dataCenter.get(PreferenceNames.PIP_POSITION);
            position = TOP_LEFT;
            for (int i = 0; i < UP_POSITION_NAMES.length; i++) {
                if (UP_POSITION_NAMES[i].equals(pos)) {
                    position = i;
                    break;
                }
            }
            bgBounds = RECTANGLES[position];
            border.resetLocation(bgBounds);
            detail.resetLocation(bgBounds);
        }
        footerHalf.setVisible(size == HALF);
        footerQuarter.setVisible(size == QUARTER);

        if (firstTime) {
            // Note: 초기에 한번만 이게 유효하다.
            try {
                ScaledVideoManager.getInstance().setInitialComponent(
                                    serviceContext, null, bgBounds);
                firstTime = false;
            } catch (Throwable t) {
                Log.print(t);
            }
            background.start(bgBounds);
        } else {
            // 그리고 첫 service selection 없이 resize하면 main이 resize된다.
            resize(bgBounds);
        }
        Channel ch = channelController.getCurrent();
        if (ch == null) {
            ch = ChannelController.get(0).getCurrent();
        }
//        Channel first = ChannelController.get(0).getCurrent();

        if (Log.DEBUG_ON) {
            Log.printDebug("PipController.start: " + channelController);
        }

        boolean ableToTune = TunerManager.getInstance().checkTune(channelController, ch);

        if (!ableToTune) {
            int sid = RecordingStatus.getInstance().getRecordingSourceIdWithout(ChannelController.get(0));
            if (sid != -1) {
                Channel anotherCh = ChannelDatabase.current.getChannelById(sid);
                if (anotherCh != null) {
                    if (Log.INFO_ON) {
                        Log.printInfo(App.LOG_HEADER+"PipController.start: PIP staring channel changed.");
                        Log.printInfo(App.LOG_HEADER+"PipController.start:  from = " + ch);
                        Log.printInfo(App.LOG_HEADER+"PipController.start:  to   = " + anotherCh);
                    }
                    ch = anotherCh;
                } else {
                    Log.printWarning(App.LOG_HEADER+"PipController.start: can't find channel by source id = " + sid);
                }
            } else {
                Log.printWarning(App.LOG_HEADER+"PipController.start: no free recording.");
            }
        } else {

        }
        pending = channelController.changeChannel(ch) == false;
        panel.setVisible(false);
        ui.activate();
        if (!pending) {
            startImpl();
        } else {
            Log.printWarning(App.LOG_HEADER+"PipController.start: pending !!!");
        }
    }

    private void startImpl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.startImpl()");
        background.setVisible(true);
        if (size == HALF) {
            EpgCore.getInstance().getVideoResizer().resize(HALF_MAIN, VideoController.DO_NOT_SHOW);
            ChannelZapper.getInstance().setChannelBanner(panel);
            panel.startTuner(0);
            border.setVisible(false);
        } else {
            EpgCore.getInstance().getVideoResizer().maximize();
            border.setVisible(true);
        }
        showInfo();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.stop()");
        if (!ui.isActive()) {
            return;
        }
        Log.printInfo("PipController.stop");
        super.stop();
        channelController.removeChannelEventListener(this);
        channelController.stopChannel();
//        channelController.setCurrent(null);
        ui.deactivate();
        stopTimer();
//        hideInfo();

        // save up
        UpManager um = UpManager.getInstance();
        String ss = (size == HALF) ? Definitions.PIP_SIZE_SIDE_BY_SIDE : Definitions.PIP_SIZE_OVERLAY;
        if (!ss.equals(dataCenter.get(PreferenceNames.PIP_SIZE))) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.stop() : Call UpManager.set("+ss+"");
            um.set(PreferenceNames.PIP_SIZE, ss);
        }
        ss = UP_POSITION_NAMES[position];
        if (!ss.equals(dataCenter.get(PreferenceNames.PIP_POSITION))) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.stop() : Call UpManager.set("+ss+"");
            um.set(PreferenceNames.PIP_POSITION, ss);
        }
    }

    public boolean isRunning() {
        return ui.isActive();
    }

    public boolean isWatching() {
        return isRunning() && background.isClear();
    }

    public void conflictResolved(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.conflictResolved("+ch+")");
        if (pending) {
            if (ch == null) {
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.conflictResolved() : Call exitToChannel()");
                    CommunicationManager.getMonitorService().exitToChannel();
                } catch (Exception ex) {
                    Log.print(ex);
                    stop();
                }
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.conflictResolved() : Call startImpl()");
                startImpl();
            }
        }
    }

    private void showInfo() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.showInfo()");
        synchronized (panel) {
            if (!panel.isVisible()) {
                panel.setVisible(true);
            }
            if (size == QUARTER && !detail.isVisible()) {
                if (position == TOP_LEFT || position == TOP_RIGHT) {
                    showingEffectTop.start();
                } else {
                    showingEffectBottom.start();
                }
            }
            startTimer();
        }
    }

    private void hideInfo() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.hideInfo()");
        synchronized (panel) {
            if (!ui.isActive()) {
                return;
            }
            stopTimer();
            if (size == HALF) {
                return;
            }
            if (detail.isVisible()) {
                if (position == TOP_LEFT || position == TOP_RIGHT) {
                    hidingEffectTop.start();
                } else {
                    hidingEffectBottom.start();
                }
            }
            if (panel.isVisible()) {
                panel.setVisible(false);
            }
        }
    }

    // layered key event
    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent()");
        int code = e.getCode();
        if (panel.isVisible()) {
            if (size == HALF) {
                footerHalf.clickAnimationByKeyCode(code);
            } else if (size == QUARTER) {
                footerQuarter.clickAnimationByKeyCode(code);
            }
        }
        switch (code) {
            case OCRcEvent.VK_EXIT:
            case KeyCodes.PIP:
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call exitToChannel()");
                    CommunicationManager.getMonitorService().exitToChannel();
                } catch (Exception ex) {
                    Log.print(ex);
                    stop();
                }
                return true;
            case KeyCodes.PIP_MOVE:
            case KeyCodes.COLOR_C:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call move()");
                move();
                break;
            case KeyCodes.COLOR_B:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call changeSize()");
                changeSize();
                break;
            case KeyCodes.COLOR_A:
            case OCRcEvent.VK_DISPLAY_SWAP:
                if (!swapEnabled) {
                    return false;
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call swap()");
                swap();
                break;
            case OCRcEvent.VK_UP:
            case OCRcEvent.VK_PINP_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call channelUp()");
                channelUp();
                break;
            case OCRcEvent.VK_DOWN:
            case OCRcEvent.VK_PINP_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call channelDown()");
                channelDown();
                break;
            default:
                return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.handleKeyEvent() : Call showInfo()");
        showInfo();
        repaint();
        return true;
    }

    private void move() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.move()");
        synchronized (panel) {
            if (size != HALF) {
                position = (position + 1) % RECTANGLES.length;
                border.resetLocation(RECTANGLES[position]);
                detail.resetLocation(RECTANGLES[position]);
                resize(RECTANGLES[position]);
            }
        }
    }

    private void changeSize() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.changeSize()");
        synchronized (panel) {
            if (size == HALF) {
                size = QUARTER;
                resize(RECTANGLES[position]);
                EpgCore.getInstance().getVideoResizer().maximize();
                border.resetLocation(RECTANGLES[position]);
                detail.resetLocation(RECTANGLES[position]);
                border.setVisible(true);
            } else {
                size = HALF;
                resize(HALF_PIP);
                EpgCore.getInstance().getVideoResizer().resize(HALF_MAIN, VideoController.DO_NOT_SHOW);
                ChannelZapper.getInstance().setChannelBanner(panel);
                panel.startTuner(0);
                border.setVisible(false);
                detail.setVisible(false);
            }
            footerHalf.setVisible(size == HALF);
            footerQuarter.setVisible(size == QUARTER);
        }
    }

    private void swap() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.swap()");
        ChannelController.swap(ChannelController.get(0), channelController);
    }

    private void channelUp() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.channelUp()");
        Channel ch = channelController.getNextChannel();
        if (Environment.TUNER_COUNT > 2) {
            boolean checkTune = TunerManager.getInstance().checkTune(channelController, ch);
            if (!checkTune) {
                Log.printInfo(App.LOG_HEADER+"PipController: cannot tune to channel");
                ch = RecordingStatus.getInstance().getNextRecordingChannel(ch);
                if (ch.equals(ChannelController.get(0).getCurrent())) {
                    ch = RecordingStatus.getInstance().getNextRecordingChannel(ch);
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.channelUp() : Call ChannelController.changeChange("+ch+")");
        channelController.changeChannel(ch);
    }

    private void channelDown() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.channelDown()");
        Channel ch = channelController.getPreviousChannel();
        if (Environment.TUNER_COUNT > 2) {
            boolean checkTune = TunerManager.getInstance().checkTune(channelController, ch);
            if (!checkTune) {
                Log.printInfo(App.LOG_HEADER+"PipController: cannot tune to channel");
                ch = RecordingStatus.getInstance().getPreviousRecordingChannel(ch);
                if (ch.equals(ChannelController.get(0).getCurrent())) {
                    ch = RecordingStatus.getInstance().getPreviousRecordingChannel(ch);
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.channelDown() : Call ChannelController.changeChannel("+ch+")");
        channelController.changeChannel(ch);
    }

    public Channel getCurrentChannel() {
        return currentChannel;
    }

    private void startTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    private void stopTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        hideInfo();
    }

    public void selectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.selectionRequested() : Call channelChanged(ch)");
        channelChanged(ch);
    }

    public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.selectionBlocked() : Call channelChanged(ch)");
        channelChanged(ch);
    }

    public void serviceStopped(Channel ch) {
    }

    public void showChannelInfo(Channel ch) {
    }

    public void programUpdated(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.programUpdated("+p+")");
        if (p != null) {
            halfProgramText = TextUtil.shorten(ParentalControl.getTitle(p), panel.pir.fmTitle, 390);
            //Kenneth->[2015.7.14] VDTRMASTER-5486
            pipProgramText = TextUtil.split(ParentalControl.getTitle(p), panel.pir.fmTitle, 220, 2);
            //pipProgramText = TextUtil.split(ParentalControl.getTitle(p), panel.pir.fmTitle, 250, 2);
            //<-
        } else {
            halfProgramText = "";
            pipProgramText = new String[0];
        }
        repaint();
    }

    private void channelChanged(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.channelChanged("+ch+")");
        currentChannel = ch;
        currentLogo = ch.getLogo();
        Program p = edm.getCurrentProgram(ch);
        programUpdated(ch, p);
    }

    //////////////////////////////////////////////////////////////////////
    // A/V area
    //////////////////////////////////////////////////////////////////////
    class PipBorder extends Component {
        Image iBorder = dataCenter.getImage("07_avbg.png");

        public PipBorder() {
            setSize(292, 168);
        }

        public void resetLocation(Rectangle r) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipBorder.resetLocation("+r+")");
            setLocation(r.x - 4, r.y - 4);
        }

        public void paint(Graphics g) {
            g.drawImage(iBorder, 0, 0, this);
        }
    }

    //////////////////////////////////////////////////////////////////////
    // Channel and program information
    //////////////////////////////////////////////////////////////////////
    //->Kenneth[2015.9.3] : 얘는 half 모드가 아닐때 animation 되어 튀어 나오는 상세정보를 그리는 부분.
    class PipDetail extends UIComponent {

        boolean top;

        public PipDetail() {
            setRenderer(new PipDetailRenderer());
            setSize(292, 104);
        }

        public void resetLocation(Rectangle r) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipDetail.resetLocation("+r+")");
            if (position == TOP_LEFT || position == TOP_RIGHT) {
                top = true;
                setLocation(r.x - 4, r.y + 204 - 40);
            } else {
                top = false;
                setLocation(r.x - 4, r.y - 83-20);
            }
        }

        public void animationEnded(Effect ef) {
            if (ef == showingEffectTop || ef == showingEffectBottom) {
                setVisible(true);
            } else if (ef == hidingEffectTop || ef == hidingEffectBottom) {
                setVisible(false);
            }
        }
    }

    class PipDetailRenderer extends Renderer {

        Font fButton = FontResource.BLENDER.getFont(17);
        Font fNumber = FontResource.BLENDER.getFont(23);
        Font fTitle = FontResource.BLENDER.getFont(19);
        FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

        Color cButton = new Color(241, 241, 241);
        Color cText = Color.white;
        Color cTitle = new Color(223, 223, 223);
        Color cFullName = new Color(180, 180, 180);

        Image iHd = dataCenter.getImage("icon_hd.png");
        Image iArU = dataCenter.getImage("02_ars_t.png");
        Image iArD = dataCenter.getImage("02_ars_b.png");
        Image iBgT = dataCenter.getImage("07_infobg.png");
        //Image iBgB = dataCenter.getImage("07_infobg_b.png");
        //->Kenneth[2015.2.4] 4K
        Image iUhd = dataCenter.getImage("icon_uhd.png");

        public void prepare(UIComponent c) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipDetailRenderer.prepare()");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void paint(Graphics g, UIComponent c) {
            // 아래의 두 값은 릴리즈된 플립북 좌표계산용으로 일시적인 것임
            int baseX = 48;
            int baseY = 204;
            PipDetail pd = (PipDetail) c;
            // bg
            //g.drawImage(pd.top ? iBgT : iBgB, 0, 0, c);
            g.drawImage(iBgT, 0, 0, c);

            // arrow
            g.drawImage(iArU, 4, 1, c);
            g.drawImage(iArD, 4, 14, c);

            Channel ch = currentChannel;
            // channel icon
            Util.drawChannelIcon(g, ch, 80-baseX, 211-baseY, c);

            // channel num
            g.setColor(cText);
            g.setFont(fNumber);
            g.drawString(String.valueOf(ch.getNumber()), 107-baseX, 229-baseY);

            // hd/uhd
            if (ch.getDefinition() == Channel.DEFINITION_HD) {
                g.drawImage(iHd, 148-baseX, 216-baseY, c);
            } else if (ch.getDefinition() == Channel.DEFINITION_4K) {
                g.drawImage(iUhd, 148-baseX, 216-baseY, c);
            }

            // channel icon
            if (currentLogo != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController : Draw currentLogo = "+currentLogo);
                g.drawImage(currentLogo, 181-baseX, 207-baseY, c);
            }

            // fullName
            String fullName = ch.getFullName();
            if (fullName != null) {
                g.setColor(cFullName);
                g.setFont(fButton);
                g.drawString(fullName, 107-baseX, 250-baseY);
            }

            // 프로그램 
            g.setColor(cTitle);
            g.setFont(fTitle);
            int yGap = 18;
            for (int i = 0; i < pipProgramText.length; i++) {
                g.drawString(pipProgramText[i], 107-baseX, 280-baseY+yGap*i);
            }
        }
    }

    //////////////////////////////////////////////////////////////////////
    // PIP Background UI
    //////////////////////////////////////////////////////////////////////
    //->Kenneth[2015.9.3] : 얘는 half 모드일 때 그리는 부분
    class PipPanel extends ChannelBanner {

        PipPanelRenderer pir = new PipPanelRenderer();
        String title;

        public PipPanel() {
            setRenderer(pir);
            setVisible(false);
            add(footerHalf);
            add(footerQuarter);
        }

        public void start() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanel.start()");
            setVisible(true);
            repaint();
        }

        public void stop() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanel.stop()");
            setVisible(false);
        }

        public boolean isActive() {
            return isVisible();
        }

        protected void startVideoContext(String videoContext) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanel.startVideoContext("+videoContext+")");
            super.startVideoContext(videoContext);
            title = "" + SharedMemory.getInstance().get(videoContext + SharedDataKeys.VIDEO_PROGRAM_NAME);
        }

        protected void readyData(Channel ch) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanel.readyData("+ch+")");
            Program p = edm.getCurrentProgram(ch);
            if (p != null) {
                title = TextUtil.shorten(ParentalControl.getTitle(p), pir.fmTitle, 415);
            } else {
                title = "";
            }
        }

        public void timerWentOff(TVTimerWentOffEvent event) {
        }
    }

    class PipPanelRenderer extends Renderer {
        Image iShadow = dataCenter.getImage("02_pip_keybg.png");
        Image iShadowUp = dataCenter.getImage("07_pipshadow_up.png");
        Image iShadowDown = dataCenter.getImage("07_pipshadow_down.png");
        Image iFullBg = dataCenter.getImage("pipbg.jpg");

        Image iArU = dataCenter.getImage("02_ars_t.png");
        Image iArD = dataCenter.getImage("02_ars_b.png");

        Image iHd = dataCenter.getImage("icon_hd.png");
        Image iUhd = dataCenter.getImage("icon_uhd.png");

        Font fName = FontResource.BLENDER.getFont(23);
        Font fNumber = FontResource.BLENDER.getFont(23);
        Font fTitle = FontResource.BLENDER.getFont(19);
        Font fFullName = FontResource.BLENDER.getFont(17);
        FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

        Color cText = Color.white;
        Color cTitle = new Color(223, 223, 223);
        Color cFullName = new Color(180, 180, 180);

        public void prepare(UIComponent c) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.prepare()");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void paint(Graphics g, UIComponent c) {
            PipPanel p = (PipPanel) c;
            if (size == HALF) {
                paintHalf(g, p);
            } else {
                g.drawImage(iShadow, 2, 369, c);
                paintPip(g, p, position);
            }
        }

        //->Kenneth[2015.9.2] VDTRMASTER-5620
        // NullPointerException 이 발생. null 처리 강화.
        private void paintHalf(Graphics g, PipPanel c) {
            g.drawImage(iFullBg, 0, 0, c);
//            clearRect(g, HALF_MAIN.x, HALF_MAIN.y, HALF_MAIN.width, HALF_MAIN.height);

            // arrow
            g.drawImage(iArU, 486, 368-20, c);
            g.drawImage(iArD, 486, 381-20, c);

            Channel mainCh = c.channel;
            Channel pipCh = currentChannel;

            // channel icon
            if (mainCh != null) {
                Util.drawChannelIcon(g, mainCh, 53, 352, c);
            }
            if (pipCh != null) {
                Util.drawChannelIcon(g, pipCh, 509, 352, c);
            }

            // channel number
            g.setColor(cText);
            g.setFont(fNumber);
            g.drawString(c.number, 80, 390-20);
            if (pipCh != null) g.drawString(String.valueOf(pipCh.getNumber()), 536, 390-20);

            // hd/uhd
            if (mainCh != null) {
                if (mainCh.getDefinition() == Channel.DEFINITION_HD) {
                    g.drawImage(iHd, 121, 357, c);
                } else if (mainCh.getDefinition() == Channel.DEFINITION_4K) {
                    g.drawImage(iUhd, 121, 357, c);
                }
            }

            if (pipCh != null) {
                if (pipCh.getDefinition() == Channel.DEFINITION_HD) {
                    g.drawImage(iHd, 577, 357, c);
                } else if (pipCh.getDefinition() == Channel.DEFINITION_4K) {
                    g.drawImage(iUhd, 577, 357, c);
                }
            }

            // channel logo
            //->Kenneth[2015.9.3] Julie 메일에 의거.
            String videoContext = EpgCore.getInstance().getVideoContextName();//EPG, PVR, VOD
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.paintHalf() : videoContext = "+videoContext);
            if ("PVR".equals(videoContext)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.paintHalf() : Draw pvrLogo");
                g.drawImage(ChannelBanner.pvrLogo, 154, 348, c);
            } else if ("VOD".equals(videoContext)) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.paintHalf() : Draw vodLogo");
                g.drawImage(ChannelBanner.vodLogo, 154, 348, c);
            } else {
                if (c.logo != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.paintHalf() : Draw c.logo = "+c.logo);
                    g.drawImage(c.logo, 154, 348, c);
                }
            }
            /*
            if (c.logo != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController : Draw c.logo = "+c.logo);
                g.drawImage(c.logo, 154, 348, c);
            }
            */
            //<-
            if (currentLogo != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PipController.PipPanelRenderer.paintHalf() : Draw currentLogo = "+currentLogo);
                g.drawImage(currentLogo, 610, 348, c);
            }

            // full name
            g.setFont(fFullName);
            g.setColor(cFullName);
            //->Kenneth[2015.9.3] Julie 메일에 의거.
            String fName = ChannelBanner.getDisplayableVideoContextName(videoContext);
            if ("".equals(fName)) {
                if (mainCh != null && mainCh.getFullName() != null) {
                    g.drawString(mainCh.getFullName(), 80, 391);
                }
            } else {
                g.drawString(fName, 80, 391);
            }
            /*
            if (mainCh != null && mainCh.getFullName() != null) {
                g.drawString(mainCh.getFullName(), 80, 391);
            }
            */
            //<-
            if (pipCh != null && pipCh.getFullName() != null) {
                g.drawString(pipCh.getFullName(), 536, 391);
            }

            // program name
            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(c.title, 80, 421);
            g.drawString(halfProgramText, 536, 421);
        }

        private void paintPip(Graphics g, UIComponent c, int pos) {
            int x = RECTANGLES[pos].x;
            int y = RECTANGLES[pos].y;
            switch (pos) {
                case TOP_LEFT:
                    g.drawImage(iShadowUp, 0, 0, c);
                    break;
                case TOP_RIGHT:
                    g.drawImage(iShadowUp, 614, 0, c);
                    break;
                case BOTTOM_RIGHT:
                    g.drawImage(iShadowDown, 614, 155, c);
                    break;
                case BOTTOM_LEFT:
                    g.drawImage(iShadowDown, 0, 155, c);
                    break;
            }
        }
    }

}

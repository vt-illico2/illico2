package com.alticast.illico.epg.navigator;

import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ppv.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.virtualchannel.*;
import com.alticast.illico.epg.tuner.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.sdv.*;

import javax.tv.service.selection.*;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContext;
import javax.tv.locator.Locator;
import javax.tv.util.*;
import javax.media.*;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterface;
import org.ocap.hardware.*;
import org.ocap.dvr.*;
import org.ocap.net.OcapLocator;
import org.ocap.shared.media.*;
import org.ocap.shared.dvr.RecordedService;
import org.dvb.service.selection.DvbServiceContext;
import org.ocap.hn.service.RemoteService;

public class ChannelController implements ChannelContext, DataUpdateListener,
                                          TimeShiftListener, ControllerListener,
                                          //->Kenneth[2015.6.23] DDC-107
                                          VideoOutputListener,
                                          //<-
                                          ServiceContextListener {
    public static final boolean CHANGE_CHANNEL_WHEN_FILTER_CHANGED = true;
    public static final boolean STOP_BEFORE_SELECT = false;
    public static final boolean CHECK_CA_BEFORE_CHANGE_CHANNEL = false;

    private static final byte NEED_TO_CHECK_PERMISSION = (byte) 100;

    public static String ALL_FILTER_KEY = Rs.MENU_FILTER_TYPE_ALL.getKey();

    protected byte state = STOPPED;

    protected Channel pending;

    protected int videoIndex;
    protected VideoPlane videoPlane;
    protected ServiceContext serviceContext;
    protected DvbServiceContext dvbServiceContext;
    protected TimeShiftProperties tsp;
    protected Player player;

    protected boolean isLiveMode = true;
    protected boolean bufferFound = false;

    protected long playingPosition = 0;
    protected float currentRate = 1.0f;

    protected ChannelDatabase channelDatabase;

    public Service lastRecording;

    protected Channel currentChannel;
    protected Channel lastChannel;
    protected Channel lastValidChannel;

    protected ChannelList allChannelList;
    protected ChannelList currentChannelList;
    protected ChannelList zappingChannelList;

    protected String currentFilter;

    protected String logPrefix;

    protected ChannelEventList listeners = new ChannelEventList();

    protected ProgramUpdateChecker programUpdateChecker;

    protected ServiceContextEvent lastEvent;

    private LogStateListener logStateListener;

    private static CaManager caManager = CaManager.getInstance();
    private static TunerManager tunerManager;

    private static ChannelController[] controllers;

    public boolean contentionProblem = false;

    static {
        controllers = new ChannelController[Environment.VIDEO_DEVICE_COUNT];
        for (int i = 0; i < controllers.length; i++) {
            controllers[i] = new ChannelController(i);
        }
        tunerManager = TunerManager.getInstance();
    }

    public static ChannelController get(int index) {
        return controllers[index];
    }

    protected ChannelController(int index) {
        videoIndex = index;
        if (index == 0) {
            logPrefix = App.LOG_HEADER+"ChannelController[MAIN]";
        } else if (index == 1) {
            logPrefix = App.LOG_HEADER+"ChannelController[PIP]";
        } else {
            logPrefix = App.LOG_HEADER+"ChannelController[" + index + "]";
        }
        serviceContext = Environment.getServiceContext(index);

        if (serviceContext != null) {
            if (serviceContext instanceof TimeShiftProperties) {
                tsp = (TimeShiftProperties) serviceContext;
                tsp.addTimeShiftListener(this);
            }
            if (serviceContext instanceof DvbServiceContext) {
                dvbServiceContext = (DvbServiceContext) serviceContext;
            }
            ServiceContentHandler[] handlers = serviceContext.getServiceContentHandlers();
            for (int i = 0; i < handlers.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ": handlers[" + i + "] = " + handlers[i]);
                }
                if (handlers[i] instanceof Player) {
                    player = (Player) handlers[i];
                    break;
                }
            }
            if (player == null) {
                Log.printWarning(logPrefix + ": player not found.");
            } else {
                player.addControllerListener(this);
            }
            serviceContext.addListener(this);
        }
        programUpdateChecker = new ProgramUpdateChecker(this);

        setChannelDatabase(ChannelDatabaseBuilder.getInstance().getDatabase());
        DataCenter.getInstance().addDataUpdateListener(ChannelDatabase.DATA_KEY, this);
        //->Kenneth[2015.6.23] DDC-107
        VideoOutputUtil.getInstance().addVideoOutputListener(this);
        //<-
    }

    public VideoPlane getVideoPlane() {
        return this.videoPlane;
    }

    public void setVideoPlane(VideoPlane videoPlane) {
        this.videoPlane = videoPlane;
    }

    public void dispose() {
    }

    /** 가입 채널이 변경된 경우. */
    public static void notifyCaUpdated(int sid, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.notifyCaUpdated("+sid+", "+auth+")");
        for (int i = 0; i < controllers.length; i++) {
            ChannelController cc = controllers[i];
            synchronized (cc) {
                cc.resetZappingChannels(cc.currentChannelList);
                Channel ch = cc.currentChannel;
                if (sid != 0 && ch != null) {
                    if (cc.state == CLEAR) {
                        if (ch instanceof SdvChannel && ch.getSourceId() == sid) {
                            cc.stopChannel();
                            cc.changeChannel(ch);
                        }
                    } else if (cc.state != STOPPED) {
                        if (ch.getSourceId() == sid) {
                            cc.stopChannel();
                            cc.changeChannel(ch);
                        }
                    }
                }
            }
        }
    }

    public String getLogPrefix() {
        return logPrefix;
    }

    public int getVideoIndex() {
        return videoIndex;
    }

    public ServiceContext getServiceContext() {
        return serviceContext;
    }

    public Service getService() {
        return serviceContext.getService();
    }

    public boolean isPresenting() {
        return getNetworkInterface() != null;
    }

    public static ChannelController getChannelController(NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.getChannelController("+ni+")");
        if (ni == null) {
            return null;
        }
        for (int i = 0; i < controllers.length; i++) {
            if (ni.equals(controllers[i].getNetworkInterface())) {
                return controllers[i];
            }
        }
        return null;
    }

    public NetworkInterface getNetworkInterface() {
        if (dvbServiceContext != null) {
            return dvbServiceContext.getNetworkInterface();
        } else {
            return null;
        }
    }

    public int getNetworkInterfaceIndex() {
        return getNetworkInterfaceIndex(getNetworkInterface());
    }

    public static int getNetworkInterfaceIndex(NetworkInterface ni) {
        if (ni == null) {
            return -1;
        }
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        for (int i = 0; i < nis.length; i++) {
            if (ni.equals(nis[i])) {
                return i;
            }
        }
        return -1;
    }

    protected long getMediaTime() {
        return Util.getTime(player.getMediaTime());
    }

    protected void setMediaTime(long ms) {
        player.setMediaTime(new Time(ms * 1000000L));
    }

    protected void setChannelDatabase(ChannelDatabase db) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setChannelDatabase("+db+")");
        if (db == null) {
            return;
        }
        channelDatabase = db;
        allChannelList = channelDatabase.getAllChannelList().cloneList();
        ChannelList curList = allChannelList;
        if (currentFilter != null) {
            ChannelList filtered = db.filter(currentFilter);
            if (filtered.size() > 0 && curList.contains(currentChannel)) {
                curList = filtered;
            }
        }
        setChannelList(curList);
    }

    public Channel getPreviousChannel() {
        return zappingChannelList.getPreviousChannel();
    }

    public Channel getNextChannel() {
        return zappingChannelList.getNextChannel();
    }

    public Channel getLastWatchedChannel() {
        return lastChannel;
    }

    /** filter 가 바뀌었을 때, DB가 바뀌었을 때. */
    private void setChannelList(ChannelList list) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setChannelList("+list+")");
        synchronized (logPrefix) {
            if (currentChannel != null) {
                list.resetCurrentChannel(currentChannel.getNumber());
                Channel nc = list.getCurrentChannel();
                if (currentChannel.equals(nc)) {
                    currentChannel = nc;
                }
            }
            currentChannelList = list;
            resetZappingChannels(list);
        }
    }

    private void resetZappingChannels(ChannelList list) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".resetZappingChannels("+list+")");
        synchronized (logPrefix) {
            ChannelList zapping;
            if (CaManager.INVALID_CANH) {
                zapping = list;
            } else {
                zapping = list.filter(ChannelDatabase.subscribedFilter);
                if (zapping.size() <= 1) {
                    zapping = allChannelList.filter(ChannelDatabase.subscribedFilter);
                    if (zapping.size() <= 1) {
                        zapping = list;
                    }
                }
            }
            Channel ch = currentChannel;
            if (ch != null) {
                zapping.resetCurrentChannel(ch.getNumber());
            }
            zappingChannelList = zapping;
        }
    }

    public void setFilter(ChannelList list, String filterName) {
        Log.printDebug(logPrefix + ".setFilter : " + filterName + ", " + list.size() + ", " + currentChannel);
        if (list.size() == allChannelList.size()) {
            // TODO - 수정 필요 (filter 걸린 채널이 전체 채널일때?)
            this.currentFilter = null;
        } else {
            this.currentFilter = filterName;
        }
        setChannelList(list);
        if (CHANGE_CHANNEL_WHEN_FILTER_CHANGED) {
            //->Kenneth[2016.3.8] R7 : Grouped 상태에서 sister 가 있으며 가입된 HD 채널로 tune 한다.
            try {
                if (currentChannel != null && list.search(currentChannel) < 0) {
                    TvChannel target = list.getChannelAt(0);
                    if (EpgCore.isChannelGrouped && target.getDefinition() == Channel.DEFINITION_SD) {
                        TvChannel sister = target.getSisterChannel();
                        if (sister != null && sister.getDefinition() == Channel.DEFINITION_HD && sister.isSubscribed()) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.setFilter() : Call changeChannel(Sister)");
                            target = sister;
                        }
                    }
                    changeChannel(target);
                }
            } catch (Exception e) {
                Log.print(e);
            }
            /*
            if (currentChannel != null && list.search(currentChannel) < 0) {
                changeChannel(list.getChannelAt(0));
            }
            */
            //<-
        }
    }

    public String getCurrentFilter() {
        return currentFilter;
    }

    public String getCurrentFilterKey() {
        String s = currentFilter;
        if (s != null) {
            return s;
        } else {
            return ALL_FILTER_KEY;
        }
    }

    public ChannelList getChannelList() {
        return currentChannelList;
    }

    public void changeChannel(int id) {
        Service currentService = getService();
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".changeChannel: " + id + ", " + currentService);
        }
        if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
            if (Log.INFO_ON) {
                Log.printInfo(logPrefix + ".changeChannel: skip in LOW_POWER.");
            }
            return;
        }
        Channel ch;
        if (id == -1) {
            if (lastValidChannel != null && !lastValidChannel.isAppChannel()) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : Case 1");
                ch = lastValidChannel;
            } else {
                if (currentChannel != null) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : Case 1");
                    ch = currentChannel;
                } else {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : Case 2");
                    ch = getDefaultChannel();
                }
                if (ch.isAppChannel()) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : Case 3");
                    lastValidChannel = currentChannelList.getChannelAt(0);
                    ch = lastValidChannel;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : Case 4");
                    lastValidChannel = ch;
                }
            }
            if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : ch = "+ch);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : ch.getService() = "+ch.getService());
            if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : state = "+state);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".changeChannel : sc = "+serviceContext);
            if (serviceContext != null) Log.printDebug(logPrefix + ".changeChannel : sc.getService() = "+serviceContext.getService());
            if (state != STOPPED && ch.isPresenting(serviceContext)) {
                // skip if watching state
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ".changeChannel : skip -1 while active state (same service)");
                }
                return;
            }
            boolean ableToTune = tunerManager.checkTune(this, ch);
            if (!ableToTune) {
                int sid = RecordingStatus.getInstance().getRecordingSourceIdWithout(null);
                if (sid != -1) {
                    Channel anotherCh = ChannelDatabase.current.getChannelById(sid);
                    if (anotherCh != null) {
                        if (Log.INFO_ON) {
                            Log.printInfo(logPrefix + ".changeChannel -1: staring channel changed.");
                            Log.printInfo(logPrefix + ".changeChannel -1:  from = " + ch);
                            Log.printInfo(logPrefix + ".changeChannel -1:  to   = " + anotherCh);
                        }
                        ch = anotherCh;
                    } else {
                        Log.printWarning(logPrefix + ".changeChannel -1: can't find channel by source id = " + sid);
                    }
                } else {
                    Log.printWarning(logPrefix + ".changeChannel -1: no free recording.");
                }
            }
        } else {
            ch = channelDatabase.getChannelById(id);
        }
        changeChannel(ch);
    }

    public void changeChannel(String name) {
        changeChannel(channelDatabase.getChannelByName(name));
    }

    public void changeChannelByNumber(int number) {
        changeChannel(channelDatabase.getChannelByNumber(number));
    }

    public void changeChannel(TvChannel ch) {
        changeChannel(((Channel) ch), true, true, true);
    }

    public boolean changeChannel(Channel ch) {
        return changeChannel(ch, true, true, true);
    }

    public boolean isPending() {
        return pending != null;
    }

    public synchronized void processPending(Channel ch) {
        Log.printInfo(logPrefix + ".processPending : " + ch);
        if (ch != null) {
            changeChannel(ch, true, true, false);
        } else {
            pending = null;
            ChannelZapper.getInstance().showCurrentInfo();
        }
        videoPlane.conflictResolved(ch);
    }

    public boolean changeChannel(Channel ch, boolean select, boolean checkPermission, boolean checkTuner) {
        byte permission = checkPermission ? NEED_TO_CHECK_PERMISSION : CLEAR;
        return changeChannel(ch, select, permission, checkTuner);
    }

    //->Kenneth[2015.6.24] DDC-107
    public void updateVideoOutputStatus() {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus()");
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : currentChannel = "+currentChannel);
        if (currentChannel == null) return;
        //->Kenneth[2015.7.30] PVR 이나 VOD play 중에는 EPG 에서 처리하지 않고 그들이 처리하도록 한다.
        boolean isSpecialVideoState = EpgCore.getInstance().isSpecialVideoState();
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : isSpecialVideoState = "+isSpecialVideoState);
        if (isSpecialVideoState) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : PVR or VOD playing. So do nothing.");
            return;
        }
        //<-
        if (currentChannel.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD) {
            //->Kenneth[2015.7.17] VTPERISCOPE-179 : PIP 의 경우는 다시 tune 하지 않는다. standby <-> running 시에
            // 기대와는 다르게 hot plug 이벤트 발생함. 
            if (videoIndex == 0) {
                //->Kenneth[2015.9.10] VDTRMASTER-5629
                // full_screen_app 상태이고 그 app 이 VOD 나 PVR 이면 새로 tune 하지 않도록 한다.
                if (EpgCore.getInstance().getMonitorState() == MonitorService.FULL_SCREEN_APP_STATE) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : FULL_SCREEN_APP_STATE");
                    String activeApp = null;
                    try {
                        activeApp = CommunicationManager.getMonitorService().getCurrentActivatedApplicationName();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : activeApp = "+activeApp);
                    if ("VOD".equals(activeApp) || "PVR".equals(activeApp)) {
                        if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : return without changeChannel()");
                        return;
                    }
                }
                //<-
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : Call changeChannel(currentChannel)");
                changeChannel(currentChannel);
            }
            //if (Log.DEBUG_ON) Log.printDebug(logPrefix+"updateVideoOutputStatus() : Call changeChannel(currentChannel)");
            //changeChannel(currentChannel);
            //<-
        }
    }
    //<-

    //->Kenneth[2015.7.27] DDC-107 과 HDCP2.2 때문에 공통코드 여기로 옮김
    private void preventUhdWatching(Channel ch) {
        try {
            // 아래 코드를 보면 실제 채널 selection 은 하지 않음을 눈여겨 보자.
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call setEnableOfAnalogueOutputPorts(true) --- 1");
            VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call startIncompatibility(ch) --- 1");
            videoPlane.startIncompatibility(ch);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call setState(STOPPED) --- 1");
            setState(STOPPED);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call setCurrent("+ch+") --- 1");
            setCurrent(ch);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call serviceContext.stop() --- 1");
            serviceContext.stop();
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call listeners.fireShowChannelInfo("+ch+") --- 1");
            listeners.fireShowChannelInfo(ch);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call programUpdateChecker.setProgram("+ch+") --- 1");
            programUpdateChecker.setProgram(ch);
            if (videoIndex == 1) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".preventUhdWatching() : Call pipController.selectionRequested() --- 1");
                EpgCore.getInstance().pipController.selectionRequested(ch, ch.getType(), true);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //<-

    // false를 return 하면 pending
    synchronized boolean changeChannel(final Channel ch, boolean select, byte permission, boolean checkTuner) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".changeChannel : " + ch + ", " + select + ", cur=" + currentChannel+", permission = "+permission+", checkTune = "+checkTuner);
        }
        playingPosition = 0;
        // Note: ch == current channel로 같이 들어올 가능성이 있다. (channel block)
        if (ch == null) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true");
            return true;
        }
        if (checkTuner) {
            if (pending != null && state != STOPPED) {
                Log.printInfo(logPrefix + ".changeChannel is pending : " + pending);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true");
                return true;
            }
        } else {
            pending = null;
        }

        //->Kenneth[2015.6.23] DDC-107
        if (ch.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD) {
            boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : canPlayUhd = "+canPlayUhd);
            //->Kenneth[2015.7.27] HDCP2.2
            boolean hdcp22Supported = true;
            if (VideoOutputUtil.getInstance().existHdcpApi()) {
                hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : hdcp22Supported = "+hdcp22Supported);
            }
            //<-
            if (canPlayUhd) {
                // HDMI 가 옳게 연결되어 있으므로 analog blocking 을 하도록 한다.
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setEnableOfAnalogueOutputPorts(false)");
                VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(false);
                //->Kenneth[2015.7.27] HDCP2.2
                if (!hdcp22Supported) {
                    preventUhdWatching(ch);
                    Log.printError("EPG851");
                    //->Kenneth[2015.10.13] DDC-114
                    EpgCore.getInstance().sendIncompatibilityErrorMessage("EPG851");
                    //<-
                    return true;
                }
                //<-
            } else {
                // HDMI 가 제대로 연결되지 않았음. analog blocking 을 풀고 incompatibility page 를 띄움
                preventUhdWatching(ch);
                /*
                // 아래 코드를 보면 실제 채널 selection 은 하지 않음을 눈여겨 보자.
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setEnableOfAnalogueOutputPorts(true) --- 1");
                VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call startIncompatibility(ch) --- 1");
                videoPlane.startIncompatibility(ch);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setState(STOPPED) --- 1");
                setState(STOPPED);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setCurrent("+ch+") --- 1");
                setCurrent(ch);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call serviceContext.stop() --- 1");
                serviceContext.stop();
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireShowChannelInfo("+ch+") --- 1");
                listeners.fireShowChannelInfo(ch);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call programUpdateChecker.setProgram("+ch+") --- 1");
                programUpdateChecker.setProgram(ch);
                if (videoIndex == 1) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call pipController.selectionRequested() --- 1");
                    EpgCore.getInstance().pipController.selectionRequested(ch, ch.getType(), true);
                }
                */
                Log.printError("EPG850");
                //->Kenneth[2015.10.13] DDC-114
                EpgCore.getInstance().sendIncompatibilityErrorMessage("EPG850");
                //<-
                return true;
            }
        } else {
            // Analog blocking 을 푼다.
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setEnableOfAnalogueOutputPorts(true) --- 2");
            VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
        }
        //<-

        //  같은 채널이면 unblock 된 걸 다시 block 하지 말자.
        //                clear 면 return 하고, stopped 면 select 하자.
        boolean same = ch.equals(currentChannel) && (!App.R3_TARGET || EpgCore.epgVideoContext);
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : same == "+same);
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : state == "+state);

        if (same) {
            // 이미 보고 있는데, 같은 채널을 또 선택할 필요 없다.
            // CLEAR => BLOCK 인 경우는 blockIfNeeded를 이용.

            // STOPPED => CLEAR인 경우에는 다시 막힐 수 있는 문제가 있다.
            // STOPPED 도 풀어주면 VOD 갔다온 경우등 문제가 생길지도...
            switch (state) {
                case CLEAR:
//                    if (ch instanceof SdvChannel) {
//                        Log.printInfo(logPrefix + ".changeChannel: skipped to select same SDV channel !");
//                        return true;
//                    }
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : permission = CLEAR");
                    permission = CLEAR;
                    break;
//                    Log.printDebug(logPrefix + ".changeChannel skipped same channel.");
//                    return true;
                case BLOCK_BY_RATING:
                    if (permission != CLEAR && ch.getType() != Channel.TYPE_PPV) {
                        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true -- 1");
                        return true;
                    }
                    //->Kenneth[2015.6.2] VDTRMASTER-5434 : Parental rating 을 사용하도록 setting 에서 설정하고 rating
                    // 걸린 (12세, 18세 등) 4K PPV 채널로 튜닝하면 Incomp~ page 보임. Grid 띄움. 같은
                    // 채널 tune 하면 incom~page 가 아닌 ppv 구매 페이지가 바로 보임(버그). 따라서 여기서 return 시켜서 이후에
                    // incomp~page 를 stop()시키는 코드까지 안 가게 만듦.
                    if (ch.getDefinition() == Channel.DEFINITION_4K && !Environment.SUPPORT_UHD && ch.getType() == Channel.TYPE_PPV) {
                        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : 4K Channel/Non 4K STB/Same PPV channel/Blocked_by_rating. So return true");
                        return true;
                    }
                    //<-
                    break;
                case BLOCK_BY_CHANNEL:
                    if (permission != CLEAR) {
                        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true -- 2");
                        return true;
                    }
                    break;
            }
            // 여기서 pending 체크할 필요 없나? -> 같은 채널이니 필요 없는 듯.
        } else if (checkTuner && !tunerManager.requestTune(this, currentChannel, ch, true)) {
            pending = ch;
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return false");
            return false;
        }

        if (App.R3_TARGET && videoIndex == 0 && !EpgCore.epgVideoContext) {
            try {
                CommunicationManager.getMonitorService().exitToChannel(ch.getId());
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true");
                return true;
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        if (STOP_BEFORE_SELECT) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call serviceContext.stop()");
            serviceContext.stop();
        } else {
            // zapping중에 다시 들어오면 위의 check에 의해 무시된다.
        }
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setState(STOPPED)");
        setState(STOPPED);
//        isLiveMode = true;
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setCurrent("+ch+")");
        setCurrent(ch);

        this.contentionProblem = false;

        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : select == "+select);
        if (select) {
            int id = ch.getId();
            short type = ch.getType();
            int sid = ch.getSourceId();

            boolean auth;
            if (CHECK_CA_BEFORE_CHANGE_CHANNEL) {
                auth = caManager.check(sid);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : auth -- 1 = "+auth);
            } else {
                // 나중에 SC event 받고 체크할 것
                auth = true;
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : auth = true");
            }

            byte blockType;
            if (permission == NEED_TO_CHECK_PERMISSION) {
                blockType = ParentalControl.checkBlock(ch);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : blockType -- 1 = "+blockType);
            } else {
                blockType = permission;
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : blockType -- 2 = "+blockType);
            }

            if (blockType == ParentalControl.CLEAR) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call tunerManager.readyPlayingPosition()");
                tunerManager.readyPlayingPosition(this, ch);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionRequested()");
                listeners.fireSelectionRequested(ch, type, auth);
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call ch.select()");
                boolean selected = ch.select(this, auth);
                if (!selected) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call videoPlane.startUnsubscribed()");
                    videoPlane.startUnsubscribed(ch);
                }
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionRequestedIxc()");
                listeners.fireSelectionRequestedIxc(id, type);

                if (contentionProblem) {
                    String threadName = "contentionProblem";
                    Thread currentThread = Thread.currentThread();
                    if (currentThread == null || threadName.equals(currentThread.getName())) {
                        // Tuner 문제가 있을 경우 contention problem thread에서 수행하는 change channel이
                        // 다시 contention problem을 유발해서 무한 반복하는 문제가 발견되었다.
                        Log.printWarning(logPrefix + ".contentionProblem : still with conflict. Tuner may have a problem !");
                        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true");
                        return true;
                    }
                    Log.printWarning(logPrefix + ".contentionProblem");
                    Thread t = new Thread(threadName) {
                        public void run() {
                            // 2nd recording이 생기는 timing에 의해
                            // tuner manager에서는 OK 되었지만, contention에서 drop 된 경우
                            // 완벽하지 않지만 대비코드를 넣었음.
                            Channel newCh = RecordingStatus.getInstance().getNearestRecording(ch);
                            Log.printInfo(logPrefix + ".contentionProblem : newCh = " + newCh);
                            if (newCh == null) {
                                newCh = lastChannel;
                            }
                            if (newCh != null) {
                                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel().t.run() : Call changeChannel()");
                                changeChannel(newCh);
                            }
                        }
                    };
                    t.start();
                }
            } else {
                if (!CHECK_CA_BEFORE_CHANGE_CHANNEL) {
                    // 미가입이 우선이라 띄워줘야 한다.
                    auth = caManager.check(sid);
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : caManager.check("+sid+") returns "+auth);
                }

                //->Kenneth[2015.6.3] VDTRMASTER-5434 : 위의 경우는 걍 return 시키고
                // 여기서는 명시적으로 incompatibility page 를 실행시킨다
                // 아래 조건 식에 state != CLEAR 인 것 주의
                if (ch.getDefinition() == Channel.DEFINITION_4K && !Environment.SUPPORT_UHD 
                        && ch.getType() == Channel.TYPE_PPV && state != CLEAR) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : 4K Channel/Non 4K STB/PPV channel/NOT Clear. So call videoPlane.startIncompatibility()");
                    videoPlane.startIncompatibility(ch);
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionBlocked()");
                    listeners.fireSelectionBlocked(ch, type, blockType, auth);
                }
                //<-
                //if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionBlocked()");
                //listeners.fireSelectionBlocked(ch, type, blockType, auth);
                if (!STOP_BEFORE_SELECT) {
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call serviceContext.stop()");
                    serviceContext.stop();
                }
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call ch.startSdvSesstion()");
                ch.startSdvSession(this, true);

                //->Kenneth[2015.6.3] VDTRMASTER-5434 : 위의 경우는 걍 return 시키고
                // 여기서는 명시적으로 incompatibility page 를 실행시킨다
                // 아래 조건 식에 state != CLEAR 인 것 주의
                if (ch.getDefinition() == Channel.DEFINITION_4K && !Environment.SUPPORT_UHD 
                        && ch.getType() == Channel.TYPE_PPV && state != CLEAR) {
                } else {
                            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionBlockedIxc()");
                            listeners.fireSelectionBlockedIxc(id, type);
                }
                //<-
                //if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireSelectionBlockedIxc()");
                //listeners.fireSelectionBlockedIxc(id, type);
            }
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call setState("+blockType+")");
            setState(blockType);

        } else {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call listeners.fireShowChannelInfo("+ch+")");
            listeners.fireShowChannelInfo(ch);
        }
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : Call programUpdateChecker.setProgram("+ch+")");
        programUpdateChecker.setProgram(ch);
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".changeChannel() : return true -- last");
        return true;
    }

    public synchronized void stopChannel() {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".stopChannel");
        }
        TunerManager.getInstance().stopChannel(this);
        serviceContext.stop();
        // tune out
        SDVSessionManager.getInstance().stopChannel(this);
        listeners.fireServiceStopped(currentChannel);
        setState(STOPPED);
//        isLiveMode = true;
        pending = null;
        lastRecording = null;
    }

    public synchronized void releaseChannelBlock(Channel channel) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".releaseChannelBlock");
        }
        if (channel != null && channel.equals(currentChannel)) {
            changeChannel(channel, true, false, true);
        }
    }

    public synchronized void programUpdated(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".programUpdated("+p+")");
        recheckBlock(ch, p);
    }

    public void viewingProgramChanged(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".viewingProgramChanged = " + p);
        if (!isLiveMode) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix + ".viewingProgramChanged() : Call sendChannelLog(false)");
            sendChannelLog(false);
        }
    }

    public void recheckBlock() {
        recheckBlock(currentChannel, programUpdateChecker.program);
    }

    private synchronized void recheckBlock(Channel ch, Program p) {
        if (state == STOPPED || ch == null || !ch.equals(currentChannel)) {
            return;
        }
        listeners.fireProgramUpdated(ch, p);
        byte blockType = ParentalControl.checkBlock(ch, p);
        Log.printInfo(logPrefix + ".recheckBlock: cur state = " + getStateString(state) + ", new state = " + getStateString(blockType));
        if (state == blockType) {
            Log.printDebug(logPrefix + ".recheckBlock: same state !");
            return;
        }
        if (blockType == ParentalControl.CLEAR) {
            releaseChannelBlock(ch);
        } else {
            block(ch, blockType);
        }
    }


    public synchronized void blockIfNeeded() {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".blockIfNeeded()");
        Channel ch = currentChannel;
        if (ch == null) {
            return;
        }
        byte blockType = ParentalControl.checkBlock(ch);
        if (blockType != state && blockType != ParentalControl.CLEAR) {
            block(ch, blockType);
        }
    }

    private void block(Channel ch, byte blockType) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".block("+ch+", "+blockType+")");
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".block() : Call serviceContext.stop()");
        serviceContext.stop();
        short chType = ch.getType();
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".block() : Call listeners.fireSelectionBlocked()");
        listeners.fireSelectionBlocked(ch, chType, blockType, ch.isSubscribed());
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".block() : Call listeners.fireSelectionBlockedIxc()");
        listeners.fireSelectionBlockedIxc(ch.getId(), chType);
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".block() : Call listeners.setState()");
        setState(blockType);
    }

    protected void setCurrent(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setCurrent("+ch+")");
        if (ch == currentChannel) {
            return;
        }
        Service lr = lastRecording;
        if (lr != null) {
            if (!lr.equals(getService())) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ": lastRecording will be null");
                }
                lastRecording = null;
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ": keep lastRecording");
                }
            }
        }
        this.lastChannel = currentChannel;
        this.currentChannel = ch;
        if (ch != null) {
            if (!ch.isAppChannel()) {
                this.lastValidChannel = ch;
            }
            zappingChannelList.setCurrentChannel(ch);
            if (!currentChannelList.setCurrentChannel(ch)) {
                EpgCore.getInstance().applyFilter(this, Rs.MENU_FILTER_TYPE_ALL.getKey(), null);
            }
        }
    }

    public synchronized void setLastRecording(Service s) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setLastRecordings("+s+")");
        lastRecording = s;
        lastChannel = null;
    }

    public TvChannel getCurrentChannel() {
        return getCurrent();
    }

    public Channel getCurrent() {
        return currentChannel;
    }

    public byte getState() {
        return state;
    }

    public void setState(byte newState) {
        byte oldState = state;
        this.state = newState;
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".setState : " + getStateString(oldState) + " -> " + getStateString(newState));
        }
//        if (logStateListener == null) {
//            boolean oldClear = oldState == CLEAR;
//            boolean newClear = newState == CLEAR;
//
//        }
//            if (newClear) {
//                sendChannelLog();
//            } else {
//                sendStoppedLog();
//            }
//        }
    }

    public static void swap(ChannelController cc1, ChannelController cc2) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap()");
        synchronized (cc1) {
            synchronized (cc2) {
                int index1 = cc1.getNetworkInterfaceIndex();
                Channel c1 = cc1.getCurrent();
                Channel c2 = cc2.getCurrent();
                byte p1 = cc1.getState();
                byte p2 = cc2.getState();

                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc1.stopChannel()");
                cc1.stopChannel();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc2.stopChannel()");
                cc2.stopChannel();
                // 같은 채널인 경우 실제로 tuner가 바뀌지 않아
                // recording이 하나 물려있는 경우 이동 문제를 유발할 수 있다.
                if (index1 == 0) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc2.changeChannel(c1, true, p1, false)");
                    cc2.changeChannel(c1, true, p1, false);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc1.changeChannel(c2, true, p2, false)");
                    cc1.changeChannel(c2, true, p2, false);
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc1.changeChannel(c2, true, p2, false)");
                    cc1.changeChannel(c2, true, p2, false);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.swap() : Call cc2.changeChannel(c1, true, p1, false)");
                    cc2.changeChannel(c1, true, p1, false);
                }
            }
        }
    }

    public void select(Service s) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".select()");
        try {
            serviceContext.select(s);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".dataUpdated()");
        // channel db update
        ChannelDatabase db = (ChannelDatabase) value;
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".dataUpdated() : Call setChannelDatabase()");
        setChannelDatabase(db);
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".dataUpdated() : Call resetFavorite()");
        resetFavorite(db.getFavoriteChannelList());
    }

    public void dataRemoved(String key) {
    }

    /** 선호채널 설정이 변경된 경우. */
    public void resetFavorite(ChannelList favoriteChannels) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".resetFavorite()");
        if (Rs.MENU_FILTER_FAVOURITES.getKey().equals(getCurrentFilterKey())) {
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix+".resetFavorite: current filter is fav.");
            }
            // 현재 filter가 선호채널인 경우, update 필요
            if (favoriteChannels == null || favoriteChannels.size() <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix+".resetFavorite: and no fav channels.");
                }
                setFilter(allChannelList, null);
                EpgCore.getInstance().applyFilter(null, Rs.MENU_FILTER_TYPE_ALL.getKey(), null);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix+".resetFavorite: first fav = " + favoriteChannels.getChannelAt(0));
                }
                // TODO - 이미 fav filter가 걸려있는데, 또 걸 필요가 없다.
                //        UI 는 skip 하고 그냥 거는 방법을 생각해보자.
                String filterName = Rs.MENU_FILTER_FAVOURITES.getKey();
                setFilter(favoriteChannels.cloneList(), filterName);
                EpgCore.getInstance().applyFilter(null, filterName, null);
            }
        }
    }

    public void addChannelEventListener(ChannelEventListener l) {
        listeners.add(l);
    }

    public void removeChannelEventListener(ChannelEventListener l) {
        listeners.remove(l);
    }

    public void addChannelEventListener(EpgChannelEventListener l) {
        listeners.add(l);
    }

    public void removeChannelEventListener(EpgChannelEventListener l) {
        listeners.remove(l);
    }

    protected Channel getDefaultChannel() {
        String name = DataCenter.getInstance().getString(PreferenceNames.POWER_ON_CHANNEL);
        boolean isLast = Definitions.LAST_CHANNEL.equals(name);
        Channel ch = null;
        if (isLast) {
            if (currentChannel != null) {
                return currentChannel;
            }
            name = LastChannelSaveHandler.getInstance().load();
        }
        if (name != null) {
            ch = channelDatabase.getChannelByName(name);
        }
        if (ch == null) {
            ch = currentChannelList.getChannelAt(0);
        }
        return ch;
    }

    //->Kenneth[2015.9.1] VDTRMASTER-5571
    // 이거 상당히 까다로운 이슈임. UHD package 가 없는 user/stb 인 경우 해당 채널로 tuning 하면
    // 먼저 NormalContentEvent 가 오고 그 이후에 PresenataionTerminatedEvent 가 ACCESS_WITHDRAWN 으로 날아옴.
    // 이명구 책임에게 확인 결과 AV 를 빠리 터뜨리기 위해 그런거라고 하는데.. 하여간 이 때 두번째 이벤트에서
    // service 를 꺼내면 null 이므로 현재 채널과 같은 service 에 대한 event 라는 것을 확인할 길이 없음.(확인안되니
    // 무시하는 방어 코드를 넣을 수도 없음) null 이 오는 것은 spec 에 맞는 행동임.
    // 따라서 일단 엄밀히 조건 체크해서 무시하도록 workaround 를 걸어보자
    // 1. 가장 마지막 event 를 기억할 것
    // 2. PresentationTerminatedEvent.ACCESS_WITHDRAWN 이 왔을때 다음의 조건이면 무시하자
    //   2-1. 마지막 event 가 NormalContentEvent 이며 그 이벤트의 service 가 현재 채널의 service 와 같을 것.
    //   2-2. 현재 채널이 UHD 채널이고 incompatibility 화면이 그려져 있는 상태일것.
    //   2-3. SUPPORT_UHD 가 false 일것.
    //   2-4. PVR/VOD play 중이 아닐 것.
    private ServiceContextEvent lastSCEvent = null;
    //<-
    public void receiveServiceContextEvent(ServiceContextEvent e) {
        lastEvent = e;
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".receiveServiceContextEvent = " + Util.toString(e));
        }
        FrameworkMain.getInstance().printSimpleLog("ScEvt : " + logPrefix + " " + Util.toString(e));
        Service s = e.getServiceContext().getService();
        boolean tuned = e instanceof NormalContentEvent;
        TunerManager.getInstance().receiveServiceContextEvent(this, tuned, s);
        if (tuned) {
            if (playingPosition > 0 && !(s instanceof RecordedService || s instanceof RemoteService)) {
                if (Log.INFO_ON) {
                    Log.printInfo(logPrefix + ".playingPosition = " + playingPosition);
                }
                boolean toSet = true;
                try {
                    TimeShiftControl control = (TimeShiftControl) player.getControl("org.ocap.shared.media.TimeShiftControl");
                    long bufferStart = Util.getTime(control.getBeginningOfBuffer());
                    long cur = System.currentTimeMillis();
                    toSet = bufferStart <= playingPosition || (cur -  bufferStart) > Constants.MS_PER_MINUTE;
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (toSet) {
                    player.setMediaTime(new Time(playingPosition * 1000000L));
                } else {
                    Log.printInfo(logPrefix + ".playingPosition : buffer might be stopped !");
                }
            }
            // Note - Event가 늦게 오는 경우도 있어서 이걸 막는다.
//            synchronized (this) {
//                if (currentChannel != null && !currentChannel.getService().equals(s)) {
//                    state = STOPPED;
//                }
//            }
        }
        //->Kenneth[2015.4.27] : VDTRMASTER-5409 : 채널 이벤트가 왔는데 아래처럼 currentChannel 을 그냥 써버리면 안됨.
        // EPG 가아닌 PVR, VOD 가 발생시킨 채널 이벤트일수 있기 때문에.
        // 따라서 위에 정준이가 comment out 시킨 코드를 부분적으로 살리기로 함.
        boolean serviceMatched = true;
        if (currentChannel != null && !currentChannel.getService().equals(s)) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : currentChannel = "+currentChannel);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Service from currentChannel = "+currentChannel.getService());
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Service from Event = "+s);
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Service is NOT matched.");
            serviceMatched = false;
        }
        //<-

        //->Kenneth[2015.6.4] : VDTRMASTER-5409 : 다시 오픈됨. 왜 Cisco 박스에서만 발생할까 --ㅋ
        // 하여간 PVR play, VOD play 시에 incompatibility page 가 보이는 상태면 stop 시킨다.
        //->Kenneth[2015.6.18] : 이 수정으로 인해 삼성박스에서 PVR 에 가면 incompatibility page 보였다가 사라지는 현상
        // Rafic 과 Mustapha 가 리포트함. 따라서 없애기로 함.
        //if (!serviceMatched) {
        //    ChannelBackground chBg = ChannelBackground.getInstance();
        //    ChannelBackgroundPanel currentBgPanel = chBg.getCurrentPanel();
        //    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : currentBgPanel = "+currentBgPanel);
        //    if (currentBgPanel != null && (currentBgPanel instanceof IncompatibilityChannelPanel)) {
        //        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Call ChannelBackground.stopPanel()");
        //        chBg.stopPanel();
        //    }
        //}
        //<-

        Channel ch = currentChannel;
        if (ch == null || s != null && (s instanceof RecordedService || s instanceof RemoteService)) {
            // TODO - RecordedService 의 normal은 이전 상태를 기억하자.
            // TODO - 미가입 -> list play - > EXIT 에서 문제가 발생
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Quit!");
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }
        boolean sent = false;
        if (tuned) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Call sendChannelLog(true)");
            sent = sendChannelLog(true);
        }
        if (!sent) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Call sendStoppedLog()");
            sendStoppedLog();
        }

        //->Kenneth[2015.2.28] 4K
        // [2015.3.17] 삼성에서 small PIP 에서는 항상 UHD 보이지 않는다고 함. 따라서 
        // 이 기능은 더 이상 필요 없으므로 사용 안함.
        //if (Log.DEBUG_ON) Log.printDebug("ChannelController.receiveServiceContextEvent() : Call EpgCore.checkDualUhdLimit()");
        //EpgCore.getInstance().checkDualUhdPipPopup(videoPlane);

        //->Kenneth[2015.1.30] 4K
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : definition = "+ch.getDefinition());
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : SUPPORT_UHD = "+Environment.SUPPORT_UHD);
        if (ch.getDefinition() == Channel.DEFINITION_4K && !Environment.SUPPORT_UHD && serviceMatched) {
            //->Kenneth[2015.6.2] VDTRMASTER-5413 : PIP 화면의 경우 incompatibility 가 아닌 아래의 dual uhd pip 가
            //불리워야 함. 따라서 Main AV 의 경우만 incompatibility 를 보이게 한다.
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : videoIndex = "+videoIndex);
            if (videoIndex == 0) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Call videoPlane.startIncompatibility()");
                videoPlane.startIncompatibility(ch);
                lastSCEvent = e;//->Kenneth[2015.9.1]
                return;
            }
        }

        //->Kenneth[2015.2.28] 4K
        if (checkDualUhdLimit(e, ch) && serviceMatched) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Call videoPlane.startDualUhdPip()");
            videoPlane.startDualUhdPip(ch);
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }

        if (CHECK_CA_BEFORE_CHANGE_CHANNEL) {
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }
        if (ch.getType() == Channel.TYPE_PPV) {
            // PPV 채널은 별도의 UI를 그려줘야 한다.
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }
        if (ch instanceof SdvChannel) {
            // SDV 채널은 미리 체크 한다.
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }
        if (!EpgCore.activeStbMode) {
            lastSCEvent = e;//->Kenneth[2015.9.1]
            return;
        }
        //->Kenneth[2015.9.1] VDTRMASTER-5571
        if (lastSCEvent != null && (lastSCEvent instanceof NormalContentEvent)) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : lastSCEvent = "+lastSCEvent);
            if (!Environment.SUPPORT_UHD && currentChannel != null && currentChannel.getDefinition() == Channel.DEFINITION_4K) {
                if ((e instanceof PresentationTerminatedEvent)) {
                    int reason = ((PresentationTerminatedEvent)e).getReason();
                    if (reason == PresentationTerminatedEvent.ACCESS_WITHDRAWN) {
                        // 사실 special video 인지 체크할 필요는 없을 것 같지만.. 왜냐하면 그 경우라고 해서 cyo panel 이
                        // 뜰 필요는 없으니까.. 그래도 일단 엄밀히 체크하는 코드 놔둠.
                        boolean isSpecialVideoState = EpgCore.getInstance().isSpecialVideoState();
                        if (!isSpecialVideoState) {
                            ChannelBackground chBg = ChannelBackground.getInstance();
                            ChannelBackgroundPanel currentBgPanel = chBg.getCurrentPanel();
                            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : currentBgPanel = "+currentBgPanel);
                            if (currentBgPanel != null && (currentBgPanel instanceof IncompatibilityChannelPanel)) {
                                lastSCEvent = e;//->Kenneth[2015.9.1]
                                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".receiveServiceContextEvent() : Decide to ignore this event. So not to call checkUnsubscribed()");
                                return;
                            }
                        }
                    }
                }
            }
        }
        //<-
        checkUnsubscribed(ch, tuned, e);
        lastSCEvent = e;//->Kenneth[2015.9.1]
    }

    //->Kenneth[2015.2.28] 4K : PIP 상황에서 Dual UHD limit 메시지를 보여줄지 결정
    // 1. 받은 이벤트가 PresenationTerminatedEvent.ACCESS_WITHDRAWN 일것. 이명구 책임의 가이드에 따라.
    // 2. SUPPORT_UHD 여야 할 것
    // 3. PIP 모드여야 할것.
    // 4. tune 된 채널이 UHD 일것.
    // 엄밀히 얘기하면 다른 Main 혹은 Pip 화면 역시 UHD 인지 확인해야 하는데 그것까지는 하지 않음.
    //////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////
    // [2015.3.17] 삼성의 다음과 같은 통보로 내용이 확 바뀜
    // 1. 작은 PIP 에는 언제나 UHD 가 보이지 않은 (UHD decoder 는 Main 에만 dedicated 됨)
    // 2. 작은 PIP 에 UHD 안 보일 때 아무런 이벤트 주지 않음.
    // 따라서 PresentationTerminatedEvent 를 받을 것을 기대하면 안됨.
    // 또한 Main 화면에 메시지가 보이는 경우는 없음. 오직 small PIP 에만 보일 것임.
    private boolean checkDualUhdLimit(ServiceContextEvent e, Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit("+e+", "+ch+")");
        /* 채널 이벤트 체크하는 부분 없앰.
        if (e == null || !(e instanceof PresentationTerminatedEvent)) {
            if (Log.DEBUG_ON) Log.printDebug("ChannelController.checkDualUhdLimit() returns false -- 1");
            return false;
        }
        int reason = ((PresentationTerminatedEvent)e).getReason();
        if (reason != PresentationTerminatedEvent.ACCESS_WITHDRAWN) {
            if (Log.DEBUG_ON) Log.printDebug("ChannelController.checkDualUhdLimit() returns false -- 2");
            return false;
        }
        */
        int monitorState = EpgCore.getInstance().getMonitorState();
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit() : Monitor State = "+monitorState);
        // 1. PIP 가 아니면 아무 일 않는다.
        if (monitorState != MonitorService.PIP_STATE) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit() returns false -- NON PIP");
            return false;
        }
        // 2. UHD 채널로 튠한게 아니면 아무 일 않는다.
        if (ch == null || ch.getDefinition() != Channel.DEFINITION_4K) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit() returns false -- NON UHD CHANNEL");
            return false;
        }
        // 3. Small PIP 가 아니면 아무 일 않는다.
        // 참고 : VideoResizer(ChannelBackground) 가 Main, PipController 가 Small PIP 를 처리한다.
        if (videoPlane instanceof VideoResizer) {
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit() returns false -- NON SMALL PIP");
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkDualUhdLimit() returns true");
        return true;
    }

    private boolean checkUnsubscribed(Channel ch, boolean tuned, ServiceContextEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkUnsubscribed("+ch+", "+tuned+", "+e+")");
        if (tuned && CaManager.isSubscribed(ch)) {
            return true;
        }
        int sid = ch.getSourceId();
        boolean auth = caManager.check(sid);
        // Note - 위에서 PPV는 무조건 true를 return 하고 있다.
        if (!tuned) {
            int pteReason = -1;
            if (e instanceof PresentationTerminatedEvent) {
                pteReason = ((PresentationTerminatedEvent) e).getReason();
                if (pteReason == PresentationTerminatedEvent.USER_STOP) {
                    Log.printInfo("checkUnsubscribed: skipped PresentationTerminatedEvent.USER_STOP");
                    return true;
                }
            } else if (e instanceof SelectionFailedEvent) {
                int sfeReason = ((SelectionFailedEvent) e).getReason();
                if (sfeReason == SelectionFailedEvent.INTERRUPTED) {
                    Log.printInfo("checkUnsubscribed: skipped SelectionFailedEvent.INTERRUPTED");
                    return true;
                }
            }
            if (!auth || pteReason == PresentationTerminatedEvent.ACCESS_WITHDRAWN) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".checkUnsubscribed() : Call videoPlane.startUnsubscribed()");
                videoPlane.startUnsubscribed(ch);
            }
        }
        return auth;
    }

    /** TimeShiftListener implemetation */
    public void receiveTimeShiftevent(TimeShiftEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": receiveTimeShiftevent = " + e);
        }
        int reason = e.getReason();
        if (reason == TimeShiftEvent.TIME_SHIFT_BUFFER_FOUND) {
            bufferFound = true;
        } else if (reason == TimeShiftEvent.NO_TIME_SHIFT_BUFFER) {
            bufferFound = false;
        }
//        isLiveMode = true;
    }

    /** ControllerListener implemetation */
    public void controllerUpdate(ControllerEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": controllerUpdate = " + e);
        }
        float oldRate = currentRate;
        if (e instanceof RateChangeEvent) {
            currentRate = ((RateChangeEvent) e).getRate();
            boolean oldPaused = Math.abs(oldRate) < 0.01f;
            boolean curPaused = Math.abs(currentRate) < 0.01f;
            if (oldPaused != curPaused) {
                sendPausedLog(curPaused);
            }
        }
        if (e instanceof LeavingLiveModeEvent) {
            isLiveMode = false;
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".controllerUpdate() : Call sendChannelLog(false)");
            sendChannelLog(false);
            if (logStateListener != null) {
                programUpdateChecker.enableMediaTimeChecker(true);
            }
        } else if (e instanceof EnteringLiveModeEvent) {
            isLiveMode = true;
            if (Log.DEBUG_ON) Log.printDebug(logPrefix+".controllerUpdate() : Call sendChannelLog(false)");
            sendChannelLog(false);
            if (logStateListener != null) {
                programUpdateChecker.enableMediaTimeChecker(false);
            }
        }
        if (e instanceof MediaTimeSetEvent) {
            programUpdateChecker.updateViewingProgram();
        }
    }

    public boolean isPaused() {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".isPaused()");
        if (player == null) {
            return false;
        }
        float rate = player.getRate();
        return (rate > -0.01f && rate < 0.01f );
    }

    public synchronized void setLogStateListener(LogStateListener l) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setLogStateListener("+l+")");
        logStateListener = l;
        if (l != null) {
            if (!isLiveMode) {
                programUpdateChecker.enableMediaTimeChecker(true);
            }
            synchronized (this) {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setLogStateListener() : Call sendChannelLog(false)");
                boolean sent = sendChannelLog(false);
                if (!sent) {
                    sendStoppedLog();
                    if (Log.DEBUG_ON) Log.printDebug(logPrefix+".setLogStateListener() : Call sendStoppedLog()");
                } else {
                    if (isPaused()) {
                        sendPausedLog(true);
                    }
                }
            }
        } else {
            programUpdateChecker.enableMediaTimeChecker(false);
        }
    }

    public boolean sendChannelLog(boolean forced) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".sendChannelLog()");
        LogStateListener l = logStateListener;
        //->Kenneth[2016.3.11] 로그 추가
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.sendChannelLog() : l = "+l); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.sendChannelLog() : forced = "+forced); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.sendChannelLog() : state = "+state); 
        if (l == null) {
            return true;
        }
        //<-
        if (!forced && state != CLEAR) {
            return false;
        }
        Channel ch = currentChannel;
        if (ch == null || !ch.isRecordable()) {
            return false;
        }
        if (forced) {
            sendPausedLog(false);
        }
        long mediaTime = getMediaTime();
        long cur =  System.currentTimeMillis();
        if (mediaTime < cur - 2 * Constants.MS_PER_HOUR) {
            Log.printWarning(logPrefix + ":mediaTime is too small = " + mediaTime);
            mediaTime = cur;
        }
        Program p = EpgDataManager.getInstance().getProgram(ch, mediaTime);

        Hashtable t = new Hashtable();
        t.put(LogStateListener.KEY_STATE, "epg");
        t.put(LogStateListener.KEY_CHANNEL_ID, new Integer(ch.getNumber()));
        t.put(LogStateListener.KEY_PROGRAM_ID, p.getId());
        t.put(LogStateListener.KEY_TITLE, p.getTitle());
        t.put(LogStateListener.KEY_START_TIME, new Long(p.getStartTime()));
        t.put(LogStateListener.KEY_END_TIME, new Long(p.getEndTime()));
        t.put(LogStateListener.KEY_TIMESHIFT, Boolean.valueOf(!isLiveMode));
        t.put(LogStateListener.KEY_RECORDING, Boolean.valueOf(RecordingStatus.getInstance().getStatus(p) == RecordingStatus.RECORDING));
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.sendChannelLog() : t = "+t); 
        try {
            if (l != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.sendChannelLog() : Call l.logStateChanged()"); 
                l.logStateChanged(t);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }

    private void sendPausedLog(boolean paused) {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".sendPauseLog()");
        LogStateListener l = logStateListener;
        if (l != null) {
            try {
                l.playbackPaused(paused);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void sendStoppedLog() {
        if (Log.DEBUG_ON) Log.printDebug(logPrefix+".sendStoppedLog()");
        LogStateListener l = logStateListener;
        if (l != null) {
            try {
                l.playbackStopped();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private String getStateString(short s) {
        switch (s) {
            case STOPPED:
                return "STOPPED";
            case CLEAR:
                return "CLEAR";
            case BLOCK_BY_RATING:
                return "BLOCK_BY_RATING";
            case BLOCK_BY_CHANNEL:
                return "BLOCK_BY_CHANNEL";
            default:
                return "UNKNOWN";
        }
    }

    public String toString() {
        return "state=" + getStateString(state) + ", "
                + (isLiveMode ? "Live" : "TSB") + ", " + currentChannel
                + ", all=" + (allChannelList == null ? -1 : allChannelList.size())
                + ", cur=" + (currentChannelList == null ? -1 : currentChannelList.size())
                + ", zap=" + (zappingChannelList == null ? -1 : zappingChannelList.size());
    }
}

/** for fast dispatching */
class ChannelEventList {

    ChannelEventListener[] arrayIxc = new ChannelEventListener[0];
    EpgChannelEventListener[] arrayEpg = new EpgChannelEventListener[0];

    public synchronized void add(ChannelEventListener l) {
        if (indexOf(arrayIxc, l) != -1) {
            return;
        }
        int size = arrayIxc.length;
        ChannelEventListener[] newArray = new ChannelEventListener[size+1];
        System.arraycopy(arrayIxc, 0, newArray, 0, size);
        newArray[size] = l;
        arrayIxc = newArray;
    }

    public synchronized void remove(ChannelEventListener l) {
        int index = indexOf(arrayIxc, l);
        if (index == -1) {
            return;
        }
        int size = arrayIxc.length;
        ChannelEventListener[] newArray = new ChannelEventListener[size-1];
        if (index > 0) {
            System.arraycopy(arrayIxc, 0, newArray, 0, index);
        }
        if (index < size - 1) {
            System.arraycopy(arrayIxc, index + 1, newArray, index, size - index - 1);
        }
        arrayIxc = newArray;
    }

    public synchronized void add(EpgChannelEventListener l) {
        if (indexOf(arrayEpg, l) != -1) {
            return;
        }
        int size = arrayEpg.length;
        EpgChannelEventListener[] newArray = new EpgChannelEventListener[size+1];
        System.arraycopy(arrayEpg, 0, newArray, 0, size);
        newArray[size] = l;
        arrayEpg = newArray;
    }

    public synchronized void remove(EpgChannelEventListener l) {
        int index = indexOf(arrayEpg, l);
        if (index == -1) {
            return;
        }
        int size = arrayEpg.length;
        EpgChannelEventListener[] newArray = new EpgChannelEventListener[size-1];
        if (index > 0) {
            System.arraycopy(arrayEpg, 0, newArray, 0, index);
        }
        if (index < size - 1) {
            System.arraycopy(arrayEpg, index + 1, newArray, index, size - index - 1);
        }
        arrayEpg = newArray;
    }

    private int indexOf(Object[] array, Object elem) {
        for (int i = 0; i < array.length; i++) {
            if (elem.equals(array[i])) {
                return i;
            }
        }
        return -1;
    }

    public void fireSelectionRequestedIxc(int id, short type) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireSelectionRequestedIxc()");
        ChannelEventListener[] target = arrayIxc;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            if (Log.DEBUG_ON) {
                Log.printDebug("IxcChannelEvent " + i + "] = " + target[i]);
            }
            try {
                target[i].selectionRequested(id, type);
            } catch (Exception ex) {
            }
        }
    }

    public void fireSelectionBlockedIxc(int id, short type) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireSelectionBlockedIxc()");
        ChannelEventListener[] target = arrayIxc;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            try {
                target[i].selectionBlocked(id, type);
            } catch (Exception ex) {
            }
        }
    }

    public void fireSelectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireSelectionRequested()");
        EpgChannelEventListener[] target = arrayEpg;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            if (Log.DEBUG_ON) {
                Log.printDebug("EpgChannelEvent " + i + "] = " + target[i]);
            }
            try {
                target[i].selectionRequested(ch, type, auth);
            } catch (Exception ex) {
            }
        }
    }

    public void fireSelectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireSelectionBlocked()");
        EpgChannelEventListener[] target = arrayEpg;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            try {
                target[i].selectionBlocked(ch, type, blockType, auth);
            } catch (Exception ex) {
            }
        }
    }

    public void fireServiceStopped(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireSelectionStopped()");
        EpgChannelEventListener[] target = arrayEpg;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            try {
                target[i].serviceStopped(ch);
            } catch (Exception ex) {
            }
        }
    }

    public void fireShowChannelInfo(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireShowChannelInfo()");
        EpgChannelEventListener[] target = arrayEpg;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            try {
                target[i].showChannelInfo(ch);
            } catch (Exception ex) {
            }
        }
    }

    public void fireProgramUpdated(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelController.ChannelEventList.fireProgramUpdated()");
        EpgChannelEventListener[] target = arrayEpg;
        int size = target.length;
        for (int i = 0; i < size; i++) {
            try {
                target[i].programUpdated(ch, p);
            } catch (Exception ex) {
            }
        }
    }

}

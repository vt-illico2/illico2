package com.alticast.illico.epg.navigator;

import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.Program;

/**
 * This class provides EPG functionality for other applications through IXC.
 *
 * @author  June Park
 */
public interface EpgChannelEventListener {

    void selectionRequested(Channel ch, short type, boolean subscribed);

    void selectionBlocked(Channel ch, short type, byte blockType, boolean subscribed);

    void serviceStopped(Channel lastChannel);

    void showChannelInfo(Channel ch);

    void programUpdated(Channel ch, Program p);

}

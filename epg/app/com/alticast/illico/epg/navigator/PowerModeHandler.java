package com.alticast.illico.epg.navigator;

import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.ChannelDatabase;
import com.alticast.illico.epg.data.ChannelDatabaseBuilder;
import com.alticast.illico.epg.data.ChannelList;
import com.alticast.illico.epg.ppv.*;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.util.DiagWindow;
import com.alticast.illico.epg.virtualchannel.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.ui.*;

import javax.tv.service.selection.PresentationChangedEvent;
import java.util.Vector;
import java.util.Enumeration;
import javax.tv.service.selection.ServiceContext;
import javax.tv.locator.Locator;
import java.awt.event.KeyEvent;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterface;
import org.ocap.hardware.*;
import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;

public class PowerModeHandler implements PowerModeChangeListener, LayeredKeyHandler {

    LayeredUI handler;

    private static final PowerModeHandler instance = new PowerModeHandler();

    public static PowerModeHandler getInstance() {
        return instance;
    }

    private PowerModeHandler() {
        handler = WindowProperty.LOW_POWER_KEY_BLOCKER.createLayeredKeyHandler(this);
    }

    public void init() {
        Host.getInstance().addPowerModeChangeListener(this);
        activateHandler(Host.getInstance().getPowerMode());
    }

    public void dispose() {
        Host.getInstance().removePowerModeChangeListener(this);
    }

    private synchronized void activateHandler(int newPowerMode) {
        if (newPowerMode == Host.LOW_POWER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.activateHandler(LOW_POWER)");
            handler.activate();
            ChannelZapper.isFullPower = false;
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.activateHandler(FULL_POWER)");
            handler.deactivate();
            ChannelZapper.isFullPower = true;
        }
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (App.R3_TARGET) {
            switch (e.getCode()) {
                case HRcEvent.VK_POWER:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.handleKeyEvent(VK_POWER) returns false");
                    return false;
                case HRcEvent.VK_0:
                    if (e.getType() == KeyEvent.KEY_PRESSED) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.handleKeyEvent(VK_0) : Call Host.setPowerMode(FULL_POWER)");
                        Host.getInstance().setPowerMode(Host.FULL_POWER);
                    }
                    return true;
                default:
                    return true;
            }
        } else {
            return e.getCode() != HRcEvent.VK_POWER;
        }
    }

    public void powerModeChanged(int newPowerMode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged("+newPowerMode+")");
        activateHandler(newPowerMode);
        if (newPowerMode == Host.LOW_POWER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call ParentalControl.reset()");
            ParentalControl.reset();
            if (EpgCore.activeStbMode) {
                Object setting = DataCenter.getInstance().get(PreferenceNames.DISPLAY_POWER_OFF);
                if (Definitions.DISPLAY_POWER_OFF_TIME.equals(setting)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call FrontPanelDisplay.showClock()");
                    FrontPanelDisplay.getInstance().showClock();
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call FrontPanelDisplay.clear()");
                    FrontPanelDisplay.getInstance().clear();
                }

                ChannelController cc = ChannelController.get(0);

                //->Kenneth[2016.7.8] R7.2 sticky filter
                boolean isStickyFilter = false;
                String currentChannelFilter = cc.getCurrentFilterKey();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : currentChannelFilter = "+currentChannelFilter);
                if (Rs.MENU_FILTER_SUBSCRIBED.getKey().equals(currentChannelFilter)) {
                    isStickyFilter = true;
                }

                //->Kenneth[2016.9.22] VDTRMASTER-5866 : sticky filter 적용시에도 power on channel 로 설정되어 있는
                // 채널이 적용되어야 한다. 따라서 sticky filter 는 setFilter 여부만 관여하게 하고
                // 나머지 세팅은 그대로 따른다.
                // 이후 고려사항 : getDefaultChannel() 이 unsubscribed 채널을 돌려줄 경우에 대한 처리가 필요할 수도 있음.
                synchronized (cc) {
                    if (!isStickyFilter) {
                        cc.setFilter(cc.allChannelList, null);
                    }
                    Channel defaultChannel = cc.getDefaultChannel();
                    cc.currentChannel = defaultChannel;
                    cc.lastValidChannel = null;
                    cc.lastChannel = null;
                    cc.lastRecording = null;
                    cc.allChannelList.setCurrentChannel(defaultChannel);
                    cc.zappingChannelList.setCurrentChannel(defaultChannel);
                    //->Kenneth[2016.10.7] VDTRMASTER-5922 : mini EPG 의 focus 가 standby 갔다 오면 LCW 를 가리킴
                    // Mini EPG 가 사용하는 ChannelController 의 currentChannelList 의 채널도 바꿔줘야 함.
                    cc.currentChannelList.setCurrentChannel(defaultChannel);
                    //<-
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"current channel changed due to power off : " + cc.currentChannel);
                    }
                }
                /*
                if (!isStickyFilter) {
                    synchronized (cc) {
                        cc.setFilter(cc.allChannelList, null);
                        Channel defaultChannel = cc.getDefaultChannel();
                        cc.currentChannel = defaultChannel;
                        cc.lastValidChannel = null;
                        cc.lastChannel = null;
                        cc.lastRecording = null;
                        cc.allChannelList.setCurrentChannel(defaultChannel);
                        cc.zappingChannelList.setCurrentChannel(defaultChannel);
                        if (Log.DEBUG_ON) {
                            Log.printDebug(App.LOG_HEADER+"current channel changed due to power off : " + cc.currentChannel);
                        }
                    }
                }
                */
                /*
                synchronized (cc) {
                    cc.setFilter(cc.allChannelList, null);
                    Channel defaultChannel = cc.getDefaultChannel();
                    cc.currentChannel = defaultChannel;
                    cc.lastValidChannel = null;
                    cc.lastChannel = null;
                    cc.lastRecording = null;
                    cc.allChannelList.setCurrentChannel(defaultChannel);
                    cc.zappingChannelList.setCurrentChannel(defaultChannel);
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"current channel changed due to power off : " + cc.currentChannel);
                    }
                }
                */
                //<-
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call DiagWindow.stop()");
            DiagWindow.getInstance().stop();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call PopupController.stopAll()");
            PopupController.getInstance().stopAll();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged() : Call EpgCore.stopAll()");
            EpgCore.getInstance().stopAll();
        }
    }

}

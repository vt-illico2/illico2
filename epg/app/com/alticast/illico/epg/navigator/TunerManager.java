package com.alticast.illico.epg.navigator;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.tuner.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import org.ocap.hn.service.RemoteService;

import javax.tv.locator.Locator;
import javax.tv.service.selection.ServiceContext;
import org.davic.net.tuning.*;
import org.davic.resources.*;
import org.dvb.service.selection.DvbServiceContext;
import org.ocap.dvr.*;
import org.ocap.dvr.SharedResourceUsage;
import org.ocap.storage.ExtendedFileAccessPermissions;
import org.ocap.shared.dvr.*;
import org.ocap.resource.*;
import org.ocap.service.*;
import org.ocap.dvr.event.*;
import org.ocap.si.*;

import javax.tv.util.*;
import javax.tv.service.Service;
import java.util.*;

/**
 * TunerManager
 *
 * @author June Park
 */
public abstract class TunerManager {

    protected static final long TSB_DURATION = Constants.MS_PER_HOUR / Constants.MS_PER_SECOND;
    protected static ExtendedFileAccessPermissions defaultPermission
            = new ExtendedFileAccessPermissions(true, true, true, true, true, true, null, null);

    static Hashtable mediaTimeTable = new Hashtable();

    static RecordingStatus recordingStatus = RecordingStatus.getInstance();

    static OcapRecordingManager manager = (OcapRecordingManager) RecordingManager.getInstance();

    private TunerStatus[] tuners;

    private static final TunerManager instance = createInstance();

    public static TunerManager getInstance() {
        return instance;
    }

    private static TunerManager createInstance() {
        if (Environment.TUNER_COUNT >= 2) {
            return new DualTunerManager();
        } else {
            return new SingleTunerManager();
        }
    }

    protected TunerManager() {
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        tuners = new TunerStatus[nis.length];
        for (int i = 0; i < nis.length; i++) {
            tuners[i] = new TunerStatus(nis[i], i);
        }
    }

    public int size() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.size() returns "+tuners.length);
        return tuners.length;
    }

    public TunerStatus getTunerStatus(int i) {
        return tuners[i];
    }

    public TunerStatus find(NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.find("+ni+")");
        if (ni == null) {
            return null;
        }
        for (int i = 0; i < tuners.length; i++) {
            if (tuners[i].ni == ni) {
                return tuners[i];
            }
        }
        Log.printWarning("TunerManager.find : not found TunerStatus = " + ni);
        return null;
    }

    /////////////////////////////////////////////////////////////////////////
    // Conflict APIs
    /////////////////////////////////////////////////////////////////////////

    public abstract boolean requestTune(ChannelController cc, Channel current, Channel next, boolean keepBuffer);

    public abstract boolean checkTune(ChannelController cc, Channel next);

    /////////////////////////////////////////////////////////////////////////
    // Control APIs
    /////////////////////////////////////////////////////////////////////////

    public synchronized void startChannel(ChannelController cc, Channel ch) {
    }

    public synchronized void stopChannel(ChannelController cc) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.stopChannel("+cc+")");
        startCancelBufferingTimer(cc);
    }

    protected abstract void readyPlayingPosition(ChannelController cc, Channel ch);

    public void recordingStarted(RecordingRequest r, NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStarted("+r+", "+ni+")");
        TunerStatus ts = find(ni);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStarted() : ts = "+ts);
        if (ts != null) {
            ts.setRecording(r);
            ChannelController cc = ts.getChannelController();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStarted() : cc = "+cc);
            if (cc != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStarted() : Call sendChannelLog(false)");
                cc.sendChannelLog(false);
            }
        }
    }

    public void recordingStopped(RecordingRequest r, NetworkInterface ni) {
        Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStopped("+r+", "+ni+")");
        TunerStatus ts = find(ni);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStopped() : ts = "+ts);
        if (ts != null) {
            ts.setRecording(null);
            ChannelController cc = ts.getChannelController();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStopped() : cc = "+cc);
            if (cc != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerManager.recordingStopped() : Call sendChannelLog(false)");
                cc.sendChannelLog(false);
            }
        }
    }

    public void notifyBufferWillBeDropped(Service s, int index) {
        Log.printDebug(App.LOG_HEADER+"TunerManager.notifyBufferWillBeDropped : " + index);
        try {
            tuners[index].cancelBuffering(s);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    protected ExtendedFileAccessPermissions createExtendedFileAccessPermissions(NetworkInterface ni) {
        return new CustomExtendedFileAccessPermissions(ni);
    }

    public void receiveServiceContextEvent(ChannelController cc, boolean tuned, Service s) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent("+tuned+", "+s+")");
        BufferingRequest br = null;
        if (tuned && manager != null) {
            if (!(s instanceof RecordedService || s instanceof RemoteService)) {
                try {
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call dumpBuffers()");
                    dumpBuffers();
                    NetworkInterface ni = cc.getNetworkInterface();
                    if (ni != null) {
                        br = BufferingRequest.createInstance(s, TSB_DURATION, TSB_DURATION, createExtendedFileAccessPermissions(ni));
                        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call requestBuffering()");
                        manager.requestBuffering(br);
                        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call dumpBuffer()");
                        dumpBuffers();
                        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call find("+ni+")");
                        TunerStatus ts = find(ni);
                        if (ts != null) {
                            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call setBuffering()");
                            BufferingRequest nbr = ts.setBuffering(br);
                            if (nbr != null) {
                                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call ContentionHandler.saveBuffers.remove()");
                                ContentionHandler.savedBuffers.remove(s);
                            }
                            if (Environment.TUNER_COUNT > 2) {
                                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.receiveServiceContextEvent() : Call keepOnlyTwoBuffers()");
                                keepOnlyTwoBuffers(ni);
                            }
                        }
                    } else {
                        Log.printInfo("TunerManager.receiveServiceContextEvent: NI is null");
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        // TODO - udpate
//        TunerStatus ts = find(ni);
//        if (ts != null) {
//            ts.update(cc);
//        }
    }

    public static NetworkInterface getNetworkInterface(BufferingRequest br) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.getNetworkInterface("+br+")");
        ExtendedFileAccessPermissions efap = br.getExtendedFileAccessPermissions();
        if (efap != null && efap instanceof CustomExtendedFileAccessPermissions) {
            return ((CustomExtendedFileAccessPermissions) efap).ni;
        }
        return null;
    }

    protected void keepOnlyTwoBuffers(NetworkInterface ni) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.keepOnlyTwoBuffers("+ni+")");
        BufferingRequest[] br = manager.getBufferingRequests();
        if (br == null || br.length <= 2) {
            return;
        }
        BufferingRequest target = null;
        BufferingRequestCompareKey[] array = new BufferingRequestCompareKey[br.length];
        for (int i = 0; i < br.length; i++) {
            array[i] = new BufferingRequestCompareKey(br[i]);
        }
        Arrays.sort(array);
        if (array[0].creationTime < Long.MAX_VALUE) {
            TunerStatus ts = find(array[0].ni);
            if (ts != null) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.keepOnlyTwoBuffers() : Call ts.cencelBuffering()");
                ts.cancelBuffering();
            } else {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.keepOnlyTwoBuffers() : Call manager.cancelBufferingRequest()");
                manager.cancelBufferingRequest(array[0].br);
            }
        } else {
            Log.printWarning(App.LOG_HEADER+"TunerManager: can't pick BufferingRequest to cancel");
        }
    }

    protected void startCancelBufferingTimer(ChannelController cc) {
        startCancelBufferingTimer(cc, false);
    }

    protected void startCancelBufferingTimer(ChannelController cc, boolean save) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.startCancelBufferingTimer("+cc+", "+save+")");
        NetworkInterface ni = cc.getNetworkInterface();
        if (ni != null) {
            TunerStatus ts = find(ni);
            if (ts != null) {
                if (save) {
                    BufferingRequest br = ts.br;
                    if (br != null) {
                        ContentionHandler.savedBuffers.addElement(br.getService());
                    }
                }
                ts.startCancelBufferingTimer();
            }
        }
    }

    public synchronized void allocateTunerToRecording(int id) {
        Log.printDebug(App.LOG_HEADER+"TunerManager.allocateTunerToRecording = " + id);
        for (int i = 0; i < tuners.length; i++) {
            TunerStatus ts = tuners[i];
            if (ts.getChannelController() == null) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.allocateTunerToRecording() : Call ts.cencelBuffering()");
                ts.cancelBuffering();
            }
        }
    }

    public static void dumpBuffers() {
        if (manager == null || !Log.DEBUG_ON) {
            return;
        }
        BufferingRequest[] br = manager.getBufferingRequests();
        Log.printDebug("TunerManager.dumpBuffers: size = " + (br == null ? -1 : br.length));
        for (int i = 0; i < br.length; i++) {
            if (br[i] != null && br[i].getService() != null) {
                Locator loc = br[i].getService().getLocator();
                Log.printDebug("BufferingRequest[" + i + "] = " + br[i] + ", " + loc + ", " + br[i].getExtendedFileAccessPermissions());
            }
        }
    }

    class BufferingRequestCompareKey implements Comparable {
        BufferingRequest br;
        long creationTime;
        NetworkInterface ni;
        public BufferingRequestCompareKey(BufferingRequest br) {
            this.br = br;
            ExtendedFileAccessPermissions efap = br.getExtendedFileAccessPermissions();
            if (efap instanceof CustomExtendedFileAccessPermissions) {
                CustomExtendedFileAccessPermissions c = (CustomExtendedFileAccessPermissions) efap;
                ni = c.ni;
                if (ChannelController.getChannelController(ni) != null) {
                    creationTime = Long.MAX_VALUE;
                } else if (ContentionHandler.savedBuffers.contains(br.getService())) {
                    creationTime = Long.MAX_VALUE - 10;
                } else {
                    creationTime = c.creationTime;
                }
            }
        }

        public int compareTo(Object o) {
            if (o instanceof BufferingRequestCompareKey) {
                BufferingRequestCompareKey a = (BufferingRequestCompareKey) o;
                if (this.creationTime < a.creationTime) {
                    return -1;
                } else if (this.creationTime > a.creationTime) {
                    return 1;
                }
            }
            return 0;
        }

    }
}



class CustomExtendedFileAccessPermissions extends ExtendedFileAccessPermissions {
    long creationTime;
    NetworkInterface ni;
    public CustomExtendedFileAccessPermissions(NetworkInterface ni) {
        super(true, true, true, true, true, true, null, null);
        creationTime = System.currentTimeMillis();
        this.ni = ni;
    }
}

class DualTunerManager extends TunerManager {

    public boolean requestTune(ChannelController cc, Channel current, Channel next, boolean keepBuffer) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune("+cc+", "+current+", "+next+", "+keepBuffer+")");
        // check tuner
        boolean result = checkTune(cc, next);
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerManager: checkTune = " + result);
        }
        if (!result) {
            Channel old = current;
            if (!recordingStatus.isRecording(old)) {
                old = recordingStatus.getNearestRecording(old);
                if (old == null) {
                    old = current;
                } else {
                    if (EpgCore.epgVideoContext) {
                        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : Call cc.changeChanne(old, true, true, false)");
                        cc.changeChannel(old, true, true, false);
                    }
                }
            }
            if (recordingStatus.revalidate()) {
                result = checkTune(cc, next);
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : checkTune returns "+result);
            }
            if (!result) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : new ChannelChangeNotice(cc, old, next).start()");
                new ChangeChannelNotice(cc, old, next).start();
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : returns false -- 1");
                return false;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerManager: cc = " + cc.isLiveMode + ", " + cc.bufferFound + ", " + cc);
        }
        // check TSB
        if (cc.isLiveMode || !cc.bufferFound || current == null || cc.getState() == ChannelController.STOPPED) {
//            readyPlayingPosition(cc, next);
            startCancelBufferingTimer(cc);
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : returns true -- 2");
            return true;
        }

        if (checkBufferKeepable(cc, current, next)) {
//            readyPlayingPosition(cc, next);
            mediaTimeTable.put(current.getSourceIdObject(), new Long(cc.getMediaTime()));
            startCancelBufferingTimer(cc, true);
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : returns true -- 3");
            return true;
        } else if (!keepBuffer) {
            startCancelBufferingTimer(cc, true);
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : returns true -- 4");
            return true;
        } else {
            new SaveTimeshiftBuffer(cc, current, next).start();
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.requestTune() : returns false -- 5");
            return false;
        }
    }

    public boolean checkTune(ChannelController cc, Channel next) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune("+cc+", "+next+")");
        int count = recordingStatus.getRecordingCount();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerManager: rec count = " + count);
        }
        int free = Environment.TUNER_COUNT - count;
        if (free >= 2) {
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 1");
            return true;
        }
        if (free == 1) {
            ChannelController another = getAnother(cc);

            if (another.getState() == ChannelController.STOPPED) {
                if (EpgCore.epgVideoContext || another != ChannelController.get(0)) {
                    // another가 stop 되어있으면 그넘이 녹화중이거나 그넘의 튜너를 뺏으면 되는데
                    // stopped 상태더라도 VOD 재생으로 tuner를 못 뺏는 경우가 있다.

                    // special video context가 아닌 경우나, another가 main 이 아닌 경우는 무시
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 2");
                    return true;
                }
                // special video context가 sc를 점유하는지 check, 위 조건에 의해 sc index는 0
                ServiceContext sc = Environment.getServiceContext(0);
                if (sc instanceof DvbServiceContext) {
                    DvbServiceContext dsc = (DvbServiceContext) sc;
                    NetworkInterface ni = dsc.getNetworkInterface();
                    Log.printDebug("DualTunerManager.checkTune : 1 free, SpecialVideo, ni = " + ni);
                    if (ni == null) {
                        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 3");
                        return true;
                    }
                }

            }
            if (recordingStatus.isRecording(next)) {
                // another가 next라면 빈 tuner가 무조건 있다고 볼 수 있음.
                // 아니면 기존 next recording으로 이 sc를 이용할 수 있음.
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 4");
                return true;
            }
            // another가 기존 recording에 묶였으면 free tuner가 있음
            if (recordingStatus.isRecording(another)) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 5");
                return true;
            }
            if (cc.getState() != ChannelController.STOPPED) {
                Channel ccc = cc.getCurrent();
                if (ccc != null && ccc.equals(another.getCurrent())) {
                    // 같은 채널인데 현재 cc에 rec이 물려있는 경우 swap 해보자.
                    ChannelController.swap(cc, another);
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 6");
                    return true;
                }
            }
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns false -- 7");
            return false;
        } else {
            if (!recordingStatus.isRecording(next)) {
                // 모두 녹화중에는 녹화중이지 않은 다른 채널로 못간다.
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns false -- 8");
                return false;
            }
            // 녹화중인 채널로 가려고 한다.
            ChannelController another = getAnother(cc);
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : another = "+another);
            if (another.getState() == ChannelController.STOPPED) {
                // 다른게 stop 되어있으면 갈 수 있다. 가려는 채널은 cc가 없음.
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns true -- 9");
                return true;
            }
            // another는 현재 살아있다. 아마도 어떤 채널을 녹화 중.
            if (Environment.TUNER_COUNT <= 2) {
                if (cc.getState() != ChannelController.STOPPED) {
                    // another가 살아있고, cc도 살아있는 경우,
                    // cc가 이미 녹화중인 어디선가 next로 가는 것이니 갈 수 없다.
                    // next가 이미 another에 의해 점유된 경우.
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns false -- 10");
                    return false;
                }
                // another 살아있고, cc는 죽어있는 경우, another와 같은 채널로는 못간다.
                boolean tmp = !next.equals(another.getCurrent());
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns "+tmp+" -- 11");
                return tmp;
            } else {
                // gateway의 경우 cc가 살아있었어도 다른데로 갈 수 있다.
                boolean tmp = !next.equals(another.getCurrent());
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.DualTunerManager.checkTune() : returns "+tmp+" -- 12");
                return tmp;
            }
        }
    }

    private boolean checkBufferKeepable(ChannelController cc, Channel current, Channel next) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable("+cc+", "+current+", "+next+")");
        ChannelController another = getAnother(cc);
        int tunerCount = Environment.TUNER_COUNT;
        if (tunerCount <= 2) {
            if (another.getState() != ChannelController.STOPPED) {
                // PIP 켜져 있으면 못 남긴다.
                Log.printInfo("TunerManager: watching buffer can't be retained by another CC = " + another);
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns false -- 1");
                return false;
            }
            if (recordingStatus.getRecordingCount() == 0) {
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns false -- 2");
                return true;
            }
            // currnet => 이미 녹화에 buffer가 붙어있는 경우.
            // next => 별도의 tuner가 필요 없다.
            boolean isRecording = (recordingStatus.isRecording(current) || recordingStatus.isRecording(next));
            if (!isRecording) {
                Log.printInfo("TunerManager: watching buffer can't be retained by recordings.");
            }
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns "+isRecording+" -- 3");
            return isRecording;
        } else {
            int recCount = recordingStatus.getRecordingCount();
            int requiredCount = recCount + 2;  // recording + current + next
            boolean pipRecording = false;
            if (another.getState() != ChannelController.STOPPED) {
                pipRecording = recordingStatus.isRecording(another);
                if (recCount <= 0 || !pipRecording) {
                    requiredCount++;    // pip가 녹화중이지 않은채로 켜져있으면 필요 count 증가.
                }
            }
            Log.printDebug("TunerManager: required count = " + requiredCount);
            if (requiredCount <= tunerCount) {  // have enough tuners
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns true -- 4");
                return true;
            }
            if (pipRecording) {
                Channel pipCh = another.getCurrent();
                if (current.equals(pipCh)) {
                    boolean tmp = recordingStatus.isRecording(next);
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns "+tmp+" -- 5");
                    return tmp;
                } else if (next.equals(pipCh)) {
                    boolean tmp = recordingStatus.isRecording(current);
                    if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns "+tmp+" -- 6");
                    return tmp;
                }
            }
            boolean tmp = recordingStatus.isRecording(current) || recordingStatus.isRecording(next);
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.checkBufferKeepable() returns "+tmp+" -- 7");
            return tmp;
        }
    }

    private ChannelController getAnother(ChannelController cc) {
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController target = ChannelController.get(i);
            if (cc != target) {
                return target;
            }
        }
        return null;
    }

    public synchronized void stopChannel(ChannelController cc) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.stopChannel()");
        if (cc.getState() != ChannelController.STOPPED && cc.bufferFound && !cc.isLiveMode) {
            Channel current = cc.getCurrent();
            if (current != null) {
                long time = cc.getMediaTime();
                Log.printInfo("TunerManager: saving playing position = " + time + " : " + current);
                mediaTimeTable.put(current.getSourceIdObject(), new Long(time));
            }
        }
        super.stopChannel(cc);
    }

    protected void readyPlayingPosition(ChannelController cc, Channel ch) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.readyPlayingPosition()");
        Long time = (Long) mediaTimeTable.remove(ch.getSourceIdObject());
        if (time != null) {
            Log.printInfo("TunerManager: reading playing position = " + time);
            cc.playingPosition = time.longValue();
        }
    }


    class SaveTimeshiftBuffer extends Thread {
        Channel cur;
        Channel next;
        ChannelController cc;

        public SaveTimeshiftBuffer(ChannelController cc, Channel cur, Channel next) {
            super("SaveTimeshiftBuffer");
            this.cc = cc;
            this.cur = cur;
            this.next = next;
        }

        public void run() {
            boolean result = true;
            try {
                PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.SaveTimeshiftBuffer.run() : Call pvrService.confirmSaveBuffer()");
                result = pvrService.confirmSaveBuffer(cc.getVideoIndex(), cur, next);
            } catch (Throwable ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.SaveTimeshiftBuffer.run() : Call cc.processPending)");
            cc.processPending(result ? next : null);
        }

    }

    class ChangeChannelNotice extends Thread {
        Channel cur;
        Channel next;
        ChannelController cc;

        public ChangeChannelNotice(ChannelController cc, Channel cur, Channel next) {
            super("ChangeChannelNotice");
            this.cc = cc;
            this.cur = cur;
            this.next = next;
        }

        public void run() {
            boolean result = true;
            try {
                PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
                if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.ChangeChannelNotice.run() : Call pvrService.confirmChangeChannel()");
                result = pvrService.confirmChangeChannel(cc.getVideoIndex(), cur, next);
            } catch (Throwable ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"TunerManager.ChangeChannelNotice.run() : Call cc.processPending)");
            cc.processPending(result ? next : null);
        }
    }

}


class SingleTunerManager extends TunerManager {

    // always no conflict
    public boolean requestTune(ChannelController cc, Channel current, Channel next, boolean keepBuffer) {
        return true;
    }

    public boolean checkTune(ChannelController cc, Channel next) {
        return true;
    }

    protected void readyPlayingPosition(ChannelController cc, Channel ch) {
    }

}


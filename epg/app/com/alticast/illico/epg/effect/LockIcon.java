package com.alticast.illico.epg.effect;

import java.awt.*;
import com.videotron.tvi.illico.framework.effect.*;


public class LockIcon extends Component implements AnimationRequestor {

    Image image;

    LockEffect showingEffect;
    LockEffect hidingEffect;

    public LockIcon(Image im) {
        this.image = im;
        int w = image.getWidth(this);
        int h = image.getHeight(this);
        setSize(w, h);
        showingEffect = new LockEffect(this, 15, 9, -8, 0);
        hidingEffect = new LockEffect(this, 15, 9, 0, -8);
    }

    /** p = icon's origin */
    public void showIcon(Point p, Container c) {
        setLocation(p);
        c.add(this);
        showingEffect.start();
    }

    public void hideIcon(Point p, Container c) {
        setLocation(p);
        c.add(this);
        hidingEffect.start();
    }

    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

    public void animationStarted(Effect effect) {
    }

    public void animationEnded(Effect effect) {
        this.setVisible(false);
        this.getParent().remove(this);
    }

    public boolean skipAnimation(Effect effect) {
        return false;
    }

}


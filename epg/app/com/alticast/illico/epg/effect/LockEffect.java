package com.alticast.illico.epg.effect;

import java.awt.*;
import org.dvb.ui.*;
import com.videotron.tvi.illico.framework.effect.*;

public class LockEffect extends ScaleEffect {

    private int topHeight;
    private int maxMoveLeft;
    private int maxMoveRight;

    private int[] tx1;
    private int[] ty1;
    private int[] tx2;
    private int[] ty2;
    private int[] bx1;
    private int[] by1;
    private int[] bx2;
    private int[] by2;

    private static ScaleFactor LOCK_FACTOR = new ScaleFactor(
        1d, 1.5d, 1d,  0.2d, 0.5d, 0.7d, 0.8d,  0d, 1d);

    public LockEffect(Component c, int frameCount, int topHeight, int from, int to) {
        super(c, frameCount, LOCK_FACTOR);
        this.topHeight = topHeight;

        double topHeightRatio = (double) topHeight / imageHeight;

        tx1 = new int[frameCount];
        ty1 = new int[frameCount];
        tx2 = new int[frameCount];
        ty2 = new int[frameCount];
        bx1 = new int[frameCount];
        by1 = new int[frameCount];
        bx2 = new int[frameCount];
        by2 = new int[frameCount];

        int moveLen = to - from;
        int oldMaxWidth = maxWidth;
        int oldMaxHeight = maxHeight;

        int maxMoveLeft = 0;
        int maxMoveRight = 0;

        double[] offset = new double[frameCount];

        for (int i = 0; i < frameCount; i++) {
            double step = (double) i / (frameCount - 1);
            offset[i] = ((getTopOffsetFactor(step) * moveLen) + from) * scale[i];
            maxMoveLeft = Math.max(maxMoveLeft, -(int) (pointX[i] + offset[i]));
            maxMoveRight = Math.max(maxMoveRight, (int) (pointX[i] + offset[i] + widths[i]) - maxWidth);
        }
        for (int i = 0; i < frameCount; i++) {
            tx1[i] = (int) (pointX[i] + offset[i] + maxMoveLeft);
            tx2[i] = tx1[i] + widths[i];
            ty1[i] = pointY[i];
            ty2[i] = by1[i] = ty1[i] + (int) (scale[i] * topHeight);

            bx1[i] = pointX[i] + maxMoveLeft;
            bx2[i] = bx1[i] + widths[i];
            by2[i] = pointY[i] + heights[i];
        }

        clipBoundsXOffset = (imageWidth - maxWidth) / 2 - maxMoveLeft;
        maxWidth = maxWidth + maxMoveLeft + maxMoveRight;
        setClipBounds(clipBoundsXOffset, clipBoundsYOffset, maxWidth, maxHeight);
    }

    private double getTopOffsetFactor(double x) {
        if (x < factor.scaleUpFrom) {
            return 0;
        }
        if (x <= factor.scaleUpUntil) {
            return (x - factor.scaleUpFrom) / (factor.scaleUpUntil - factor.scaleUpFrom);
        }
        return 1;
    }

    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        // top
        g.drawImage(image, tx1[frame], ty1[frame], tx2[frame], ty2[frame],
                           0, 0, imageWidth, topHeight, null);
        // bottom
        g.drawImage(image, bx1[frame], by1[frame], bx2[frame], by2[frame],
                           0, topHeight, imageWidth, imageHeight, null);
    }


}


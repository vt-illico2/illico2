package com.alticast.illico.epg.effect;

import java.awt.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.framework.UIComponent;


public class AnimatedIcon extends Component implements AnimationRequestor {

    Image image;
    UIComponent container;

    public ScaleEffect showingEffect;
    public ScaleEffect hidingEffect;

    public AnimatedIcon(Image im) {
        this.image = im;
        int w = image.getWidth(this);
        int h = image.getHeight(this);
        setSize(w, h);
        showingEffect = new ScaleEffect(this, 15, ScaleFactor.ICON_SHOWING);
        hidingEffect = new ScaleEffect(this, 15, ScaleFactor.ICON_HIDING);
        showingEffect.updateBackgroundBeforeStart(true);
        hidingEffect.updateBackgroundBeforeStart(true);
    }

    public void setContainer(UIComponent c) {
        container = c;
    }

    /** p = icon's origin */
    public void showIcon() {
        container.add(this);
        showingEffect.start();
    }

    public void hideIcon() {
        container.add(this);
        hidingEffect.start();
    }

    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

    public void animationStarted(Effect effect) {
        container.animationStarted(effect);
    }

    public void animationEnded(Effect effect) {
        container.animationEnded(effect);
        this.setVisible(false);
        this.getParent().remove(this);
    }

    public boolean skipAnimation(Effect effect) {
        return container.skipAnimation(effect);
    }

}


package com.alticast.illico.epg;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.vbm.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import java.rmi.*;
import java.util.*;
import javax.tv.util.*;

public class EpgVbmController extends VbmController implements EpgChannelEventListener, TVTimerWentOffListener {

    public static final long MID_USED_EPG_LAYOUT          = 1004000001L;
    public static final long MID_EPG_REMOTE_KEY_SELECTION = 1004000002L;
    public static final long MID_SELECTED_CHANNEL_FILTER  = 1004000003L;

    public static final long MID_CHANNEL_TUNE_IN    = 1004000115L;
    public static final long MID_CHANNEL_TUNE_OUT   = 1004000116L;
    public static final long MID_WATCHED_PROGRAM    = 1004000117L;
    public static final long MID_PPV_ORDER_CHANNEL  = 1004000118L;
    public static final long MID_PPV_ORDER_PROGRAM  = 1004000119L;
    public static final long MID_PPV_ORDER_TIME     = 1004000120L;

    //->Kenneth[2015.5.29] R5 : VBM 추가
    public static final long MID_ACCESS_ASSET_PAGE  = 1004000121L;//프로그램 상세 페이지 들어가면 언제나.
    public static final long MID_HIGHLIGHT_SELECTED_NAME = 1004000122L;//팝업에서 특정 하이라이트 선택시(name).
    public static final long MID_HIGHLIGHT_SELECTED = 1004000123L;//팝업에서 특정 하이라이트 선택시(session id).
    public static final long MID_HIGHLIGHT_LOADED   = 1004000124L;//팝업에서 특정 하이라이트 선택시 가장 마지막으로(session id)
    public static final long MID_EXIT_BY_BACK       = 1004000125L;//Back 키로 Search 로 돌아가는 경우

    //->Kenneth[2016.3.9] R7 
    public static final long MID_FAVORITE_ADDED     = 1004000126L;//favorite 채널 등록시 (_P by person button, _D by D-option)
    public static final long MID_SORTING_CHANGED    = 1004000127L;//D->Sort 에서 옵션 변경시 Sort by channel number or by channel name 
    public static final long MID_SIMILAR_DISPLAYED  = 1004000128L;//프로그램 상세페이지에서 similar 패널이 slide up 되는 경우
    //<-

    //->Kenneth[2017.3.16] R7.3 
    public static final long MID_UNSUBSCRIPTION_SCREEN = 1004000129L;//EPG detail 에서 subscribe to this channel 눌러서 ISA 팝업이 뜨거나  channel Up/Down, DCA 등을 통해서 unsubscribed background panel 이 그려지는 경우
    //<-

    public static final String MINI_EPG = "Mini";
    public static final String FULL_SCALED = "Full_Scaled";
    public static final String FULL_COMPLETE = "Full_Complete";

    public static final String FILTERS = "Filters";
    public static final String SEARCH  = "Search";
    public static final String OPTIONS = "Options";

    long channelSession = 0;
    String callLetter = null;
    TVTimerSpec sessionStartTimer = new TVTimerSpec();

    protected static EpgVbmController instance = new EpgVbmController();

    public static EpgVbmController getInstance() {
        return instance;
    }

    protected EpgVbmController() {
        sessionStartTimer.setDelayTime(10 * Constants.MS_PER_SECOND);
        sessionStartTimer.addTVTimerWentOffListener(this);
    }

    protected void checkEmpty() {
        boolean oldEnabled = ENABLED;
        super.checkEmpty();
        if (oldEnabled != ENABLED) {
            if (ENABLED) {
                ChannelController.get(0).addChannelEventListener(this);
            } else {
                ChannelController.get(0).removeChannelEventListener(this);
            }
        }
    }

    //->Kenneth[2016.3.9] R7
    public void writeFavoriteAdded(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeFavoriteAdded("+name+")");
        write(MID_FAVORITE_ADDED, DEF_MEASUREMENT_GROUP, name);
    }

    public void writeSortingChanged(String option) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeSortingChanged("+option+")");
        write(MID_SORTING_CHANGED, DEF_MEASUREMENT_GROUP, option);
    }

    public void writeSimilarDisplayed(String programId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeSimilarDisplayed("+programId+")");
        write(MID_SIMILAR_DISPLAYED, DEF_MEASUREMENT_GROUP, programId);
    }
    //<-

    //->Kenneth[2015.5.29] R5
    public void writeAccessAssetPage(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeAccessAssetPage("+appSession+")");
        write(MID_ACCESS_ASSET_PAGE, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }

    public void writeHighlightSelectedName(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeHighlightSelectedName("+name+")");
        write(MID_HIGHLIGHT_SELECTED_NAME, DEF_MEASUREMENT_GROUP, name);
    }

    public void writeHighlightSelected(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeHighlightSelected("+appSession+")");
        write(MID_HIGHLIGHT_SELECTED, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }

    public void writeHighlightLoaded(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeHighlightLoaded("+appSession+")");
        write(MID_HIGHLIGHT_LOADED, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }

    public void writeExitByBack(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeExitByBack("+appSession+")");
        write(MID_EXIT_BY_BACK, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }
    //<-

    //->Kenneth[2017.3.16] R7.3
    public void writeUnsubscriptionScreen(Channel channel, Program program) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeUnsubscriptionScreen("+channel+", "+program+")");
        String value = channel.getCallLetter()+"|"+program.getCategory();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeUnsubscriptionScreen() : value = "+value);
        write(MID_UNSUBSCRIPTION_SCREEN, DEF_MEASUREMENT_GROUP, value);
    }
    //<-

    public void writeUsedEpgLayout(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeUsedEpgLayout("+name+")");
        write(MID_USED_EPG_LAYOUT, DEF_MEASUREMENT_GROUP, name);
    }

    public void writePpvOrder(AbstractProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writePpvOrder("+p+")");
        if (ENABLED) {
            String group = String.valueOf(System.currentTimeMillis());
            write(MID_PPV_ORDER_CHANNEL, group, p.getCallLetter());
            write(MID_PPV_ORDER_PROGRAM, group, p.getId());
            write(MID_PPV_ORDER_TIME, group, String.valueOf(p.getStartTime()));
        }
    }

    public void writeEpgRemoteKeySelection(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeEpgRemoteKeySelection("+name+")");
        write(MID_EPG_REMOTE_KEY_SELECTION, DEF_MEASUREMENT_GROUP, name);
    }

    public void writeSelectedChannelFilter(String filterKey) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeSelectedChannelFilter("+filterKey+")");
        if (ENABLED) {
            write(MID_SELECTED_CHANNEL_FILTER, DEF_MEASUREMENT_GROUP, DataCenter.getInstance().getString(filterKey + "_en"));
        }
    }

    private void writeChannelTuneIn(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeChannelTuneIn("+name+")");
        channelSession = System.currentTimeMillis();
        callLetter = name;
        write(MID_CHANNEL_TUNE_IN, String.valueOf(channelSession), name);
    }

    private void writeChannelTuneOut(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.writeChannelTuneOut("+name+")");
        if (channelSession == 0) {
            return;
        }
        write(MID_CHANNEL_TUNE_OUT, String.valueOf(channelSession), name);
        channelSession = 0;
        callLetter = null;
    }

    // listener
    public void selectionRequested(Channel ch, short type, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.selectionRequested("+ch+")");
        writeChannelTuneOut(callLetter);
        callLetter = ch.getName();
        startTimer();
    }

    public void selectionBlocked(Channel ch, short type, byte blockType, boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.selectionBlocked("+ch+")");
        writeChannelTuneOut(callLetter);
    }

    public void serviceStopped(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.serviceStopped("+ch+")");
        writeChannelTuneOut(ch.getName());
    }

    public void showChannelInfo(Channel ch) {
    }

    public void programUpdated(Channel ch, Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.programUpdated("+p+")");
        if (channelSession != 0 && ch.getName().equals(callLetter)) {
            write(MID_WATCHED_PROGRAM, String.valueOf(channelSession), String.valueOf(p.getId()));
        }
    }

    private synchronized void startTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(sessionStartTimer);
        try {
            sessionStartTimer = timer.scheduleTimerSpec(sessionStartTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    private synchronized void stopTimer() {
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgVbmController.timerWentOff()");
        if (callLetter != null) {
            writeChannelTuneIn(callLetter);
            try {
                Channel ch = ChannelDatabase.current.getChannelByName(callLetter);
                if (ch != null) {
                    Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
                    if (p != null) {
                        write(MID_WATCHED_PROGRAM, String.valueOf(channelSession), String.valueOf(p.getId()));
                    }
                }
            } catch (Exception ex) {
                Log.printDebug(ex);
            }
        }
    }
}


package com.alticast.illico.epg.virtualchannel;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.adapter.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.*;
import java.io.*;
import java.util.*;
import java.awt.*;
import javax.tv.util.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.ui.DVBColor;

public class VirtualChannelPage extends FullScreenPanel implements TVTimerWentOffListener {

    private Channel channel;
    private VirtualChannelData.ChannelData data;

    private TVTimerSpec downloadTimer;
    private TVTimerSpec tuneTimer;

    private long VIRTUAL_IMAGE_REQUEST_DELAY;
    private long VIRTUAL_APP_LAUNCH_DELAY;

    Image brand;
    Image image;

    int page = 0;
    int category = 0;

    int languageIndex = 0;
    boolean downloading = true;

    public VirtualChannelPage() {
        setRenderer(new VirtualChannelPageRenderer());
//        mediaTracker = new MediaTracker(this);

        VIRTUAL_IMAGE_REQUEST_DELAY = DataCenter.getInstance().getLong("VIRTUAL_IMAGE_REQUEST_DELAY", 1000L);
        VIRTUAL_APP_LAUNCH_DELAY = DataCenter.getInstance().getLong("VIRTUAL_APP_LAUNCH_DELAY", 2000L);

        downloadTimer = new TVTimerSpec();
        downloadTimer.setDelayTime(VIRTUAL_IMAGE_REQUEST_DELAY);
        downloadTimer.addTVTimerWentOffListener(this);

        tuneTimer = new TVTimerSpec();
        tuneTimer.setDelayTime(VIRTUAL_APP_LAUNCH_DELAY);
        tuneTimer.addTVTimerWentOffListener(this);

        this.add(footer);
    }

    public synchronized void start(Channel ch) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.start: delay = " + VIRTUAL_IMAGE_REQUEST_DELAY);
        }
        page = 0;
        category = 0;
        channel = ch;
        footer.reset();

        if (ch instanceof VirtualChannel) {
            VirtualChannel vc = (VirtualChannel) ch;
            String appName = vc.appName;
            Log.printInfo("VirtualChannelPage.start: appName = " + appName);
            if (appName != null) {
                try {
                    tuneTimer = TVTimer.getTimer().scheduleTimerSpec(tuneTimer);
                } catch (TVTimerScheduleFailedException ex) {
                    timerWentOff(new TVTimerWentOffEvent(TVTimer.getTimer(), tuneTimer));
                }
                super.start();
                return;
            }
        }

        VirtualChannelData vd = ConfigManager.getInstance().virtualChannelData;
        if (vd != null) {
            String curLan = FrameworkMain.getInstance().getCurrentLanguage();
            for (int i = 0; i < vd.languageTypes.length; i++) {
                if (curLan.equalsIgnoreCase(vd.languageTypes[i])) {
                    languageIndex = i;
                    break;
                }
            }
            if (languageIndex >= vd.languageTypes.length) {
                languageIndex = 0;
            }

//            VirtualChannelData.Category[] categories = vd.getData(ch);
            VirtualChannelData.ChannelData cd = vd.getData(ch);
            if (cd != null) {
                this.data = cd;
                if (data.categories.length > 1) {
                    footer.addButton(PreferenceService.BTN_A, "epg.change_category");
                }
                try {
                    downloadTimer = TVTimer.getTimer().scheduleTimerSpec(downloadTimer);
                } catch (TVTimerScheduleFailedException ex) {
                    timerWentOff(new TVTimerWentOffEvent(TVTimer.getTimer(), downloadTimer));
                }
            }
        }
        super.start();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.stop()");
        hideLoading();
        channel = null;
        data = null;
        TVTimer.getTimer().deschedule(downloadTimer);
        TVTimer.getTimer().deschedule(tuneTimer);
        if (brand != null) {
            brand.flush();
            brand = null;
        }
        if (image != null) {
            image.flush();
            image = null;
        }
        FrameworkMain.getInstance().getImagePool().remove("vir_default_01.png");
        super.stop();
    }

    public boolean handleKey(int code) {
        switch (code) {
            case OCRcEvent.VK_LEFT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.handleKey() : VK_LEFT");
                if (downloading) {
                    return true;
                }
                if (data != null && page > 0) {
                    page--;
                    startDownload();
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.handleKey() : VK_RIGHT");
                if (downloading) {
                    return true;
                }
                if (data != null && page < data.categories[category].pages.length - 1) {
                    page++;
                    startDownload();
                }
                break;
            case KeyCodes.COLOR_A:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.handleKey() : COLOR_A");
                /* 아래처럼 굳이 처리 안해도 default 로 false 된다.
                //->Kenneth[2015.7.16] Tank : A 키가 portal 을 띄우는데 씌여야 함. 따라서 이 페이지에
                // A footer 가 안보이는 경우 false 를 리턴해서 Portal 띄우는데 쓰이도록 한다.
                if (data == null || data.categories == null || data.categories.length <= 1) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.handleKey() : COLOR_A return false");
                    return false;
                }
                //<-
                */
                footer.clickAnimation(0);
                if (downloading) {
                    return true;
                }
                if (data != null && data.categories.length > 1) {
                    category = (category + 1) % data.categories.length;
                    page = 0;
                    startDownload();
                }
                break;
            case OCRcEvent.VK_EXIT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.handleKey() : VK_EXIT");
                ChannelController cc = ChannelController.get(0);
                cc.changeChannel(cc.getLastWatchedChannel());
                return true;
            default:
                return false;
        }
        repaint();
        return true;
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (channel == null) {
            return;
        }
        if (e.getTimerSpec() == tuneTimer) {
            synchronized (this) {
                if (channel instanceof VirtualChannel) {
                    VirtualChannel vc = (VirtualChannel) channel;
                    if (vc.appName != null) {
                        String[] params = null;
                        if (vc.param != null) {
                            params = TextUtil.tokenize(vc.param, "|");
                        }
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.timerWentOff() : Call EpgCore.startUnboundApp()");
                        EpgCore.getInstance().startUnboundApp(vc.appName, params);
                    }
                }
            }
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.timerWentOff() : Call startDownload()");
            startDownload();
        }
    }

    private synchronized void startDownload() {
        downloading = true;
        showLoading();
        new DownloadThread(data, data.categories[category]).start();
    }

    private synchronized void showLoading() {
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.showLoadingAnimation(new Point(480, 300));
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private synchronized void hideLoading() {
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.hideLoadingAnimation();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    class DownloadThread extends Thread {

        VirtualChannelData.ChannelData cd;
        VirtualChannelData.Category ct;

        public DownloadThread(VirtualChannelData.ChannelData data, VirtualChannelData.Category ct) {
            this.cd = data;
            this.ct = ct;
        }

        public void run() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VirtualChannelPage.DownloadThread.run()");
            String url = SharedMemory.getInstance().get(SharedDataKeys.MS_URL)
                       + DataCenter.getInstance().getString("VIRTUAL_IMAGE_PATH");
            String brandPath = url + data.brand;
            if (channel == null) {
                return;
            }
            if (brand == null) {
                byte[] b = null;
                try {
                    b = URLRequestor.getBytes(brandPath, null);
                } catch (Exception ex) {
                }
                if (channel != null && b != null) {
                    Image im = Toolkit.getDefaultToolkit().createImage(b);
                    MediaTracker mt = new MediaTracker(VirtualChannelPage.this);
                    mt.addImage(im, 0);
                    try {
                        mt.waitForAll();
                    } catch (InterruptedException ex) {
                    }
                    mt.removeImage(im);
                    synchronized (VirtualChannelPage.this) {
                        brand = im;
                    }
                }
            }

            String path = url + ct.pages[page].imagePath[languageIndex];
            synchronized (VirtualChannelPage.this) {
                if (image != null) {
                    Image temp = image;
                    image = null;
                    temp.flush();
                }
            }
            if (channel == null) {
                return;
            }
            byte[] b = null;
            try {
                b = URLRequestor.getBytes(path, null);
            } catch (Exception ex) {
            }
            if (channel != null) {
                Image im;
                if (b != null) {
                    im = Toolkit.getDefaultToolkit().createImage(b);
                    MediaTracker mt = new MediaTracker(VirtualChannelPage.this);
                    mt.addImage(im, 0);
                    try {
                        mt.waitForAll();
                    } catch (InterruptedException ex) {
                    }
                    mt.removeImage(im);
                } else {
                    im = DataCenter.getInstance().getImage("vir_default_01.png");
                    FrameworkMain.getInstance().getImagePool().waitForAll();
                }
                synchronized (VirtualChannelPage.this) {
                    image = im;
                }
            }
            hideLoading();
            downloading = false;
            repaint();
        }
    }

    class VirtualChannelPageRenderer extends FullScreenPanelRenderer {

//        Image[] iTitles = new Image[] {
//            dataCenter.getImage("02_menu_none.png"),
//            dataCenter.getImage("02_menu_01.png"),  // g
//            dataCenter.getImage("02_menu_02.png"),  // r
//            dataCenter.getImage("02_menu_03.png"),  // b
//        };
        Image i_02_vir_shadow01 = dataCenter.getImage("02_vir_shadow01.png");
        Image i_02_vir_shadow02 = dataCenter.getImage("02_vir_shadow02.png");
        Image iArL = dataCenter.getImage("02_ars_l.png");
        Image iArR = dataCenter.getImage("02_ars_r.png");
        Image i_01_hotkeybg = dataCenter.getImage("01_hotkeybg.png");

        Color cCategoryNormal = new Color(161, 160, 160);
        Color cPageBg = new Color(252, 202, 5);
        Color cTotalPage = new Color(110, 110, 110);

        Color cDefaultText = new Color(182, 182, 182);

        Color cBar = new DVBColor(187, 189, 192, 77);

        Font fTitle = FontResource.BLENDER.getFont(23);
        Font fPage = FontResource.BLENDER.getFont(16);

        Font fBar = FontResource.BLENDER.getFont(18);

        Font fCategoryFocus = FontResource.BLENDER.getFont(24);
        Font fCategoryNormal = FontResource.BLENDER.getFont(22);

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        private void paintDefault(Graphics g, int x, int y, UIComponent c) {
            DataCenter dataCenter = DataCenter.getInstance();
            Image im = dataCenter.getImage("vir_default_01.png");
            if (im != null && im.getWidth(c) > 0 && im.getHeight(c) > 0) {
                g.drawImage(im, x, y, c);
                String s = dataCenter.getString("epg.virtual_default");
                g.setFont(fCategoryNormal);
                g.setColor(Color.black);
                GraphicUtil.drawStringCenter(g, s, 473, y + 208);
                g.setColor(cDefaultText);
                GraphicUtil.drawStringCenter(g, s, 472, y + 207);
            }
        }

        protected void paint(Graphics g, UIComponent c) {
            super.paint(g, c);
            Channel ch = channel;
            if (ch != null && ch instanceof VirtualChannel && ((VirtualChannel) ch).appName != null) {
                return;
            }

            Image im = brand;
            if (im != null) {
                g.drawImage(brand, 51, 76, c);
            }

            VirtualChannelData.ChannelData cd = data;
            if (cd == null) {
                paintDefault(g, 75, 172, c);
                return;
            }
            VirtualChannelData.Category[] array = cd.categories;
            if (array == null) {
                paintDefault(g, 75, 172, c);
                return;
            }
            VirtualChannelData.Category ct = array[category];
            g.setFont(fTitle);
            if (array.length == 1) {
                g.setColor(ct.color);
                g.fillRect(76, 128, 805, 38);
                g.drawImage(i_02_vir_shadow02, 0, 128, c);
                g.setColor(Color.black);
                GraphicUtil.drawStringCenter(g, ct.title[languageIndex], 480, 154);

                im = image;
                if (im != null) {
                    if (im == DataCenter.getInstance().getImage("vir_default_01.png")) {
                        paintDefault(g, 75, 172, c);
                    } else {
                        g.drawImage(im, 75, 172, c);
                    }
                }

                if (page > 0) {
                    g.drawImage(iArL, 57, 308, c);
                }
                if (page < ct.pages.length - 1) {
                    g.drawImage(iArR, 884, 308, c);
                }
            } else {
                g.setColor(ct.color);
                g.fillRect(75, 166, 806, 31);
                g.drawImage(i_02_vir_shadow01, 0, 166, c);
                g.setColor(Color.black);
                GraphicUtil.drawStringCenter(g, ct.title[languageIndex], 480, 189);

                im = image;
                if (im != null) {
                    if (im == DataCenter.getInstance().getImage("vir_default_01.png")) {
                        paintDefault(g, 75, 204, c);
                    } else {
                        g.drawImage(im, 75, 204, c);
                    }
                }

                if (page > 0) {
                    g.drawImage(iArL, 57, 324, c);
                }
                if (page < ct.pages.length - 1) {
                    g.drawImage(iArR, 884, 324, c);
                }
                g.drawImage(i_01_hotkeybg, 367, 466, c);

                int x = 77;
                for (int i = 0; i < array.length; i++) {
                    VirtualChannelData.Category item = array[i];
                    String s = item.title[languageIndex];
                    if (i == category) {
                        g.setFont(fCategoryFocus);
                        g.setColor(item.color);
                    } else {
                        g.setFont(fCategoryNormal);
                        g.setColor(cCategoryNormal);
                    }
                    g.drawString(s, x, 151);
                    x += g.getFontMetrics().stringWidth(s) + 15;
                    if (i != array.length - 1) {
                        g.setFont(fBar);
                        g.setColor(cBar);
                        g.drawString("|", x, 150);
                        x += 16;
                    }
                }
            }
            if (ct.pages.length > 1) {
                g.setColor(cPageBg);
                g.fillRect(850, 458, 31, 17);
                g.setFont(fPage);
                g.setColor(Color.black);
                g.drawString(String.valueOf(page + 1), 854, 471);
                g.setColor(cTotalPage);
                GraphicUtil.drawStringRight(g, "/" + ct.pages.length, 877, 471);
            }

        }
    }
}

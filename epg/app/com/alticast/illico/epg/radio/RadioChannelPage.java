package com.alticast.illico.epg.radio;

import java.awt.*;
import javax.tv.util.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.ui.DVBColor;
import com.alticast.illico.epg.App;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;

public class RadioChannelPage extends FullScreenPanel implements TVTimerWentOffListener {

    private static RadioChannelPage instance = new RadioChannelPage();
    public static RadioChannelPage getInstance() {
        return instance;
    }

    private Channel channel;
    private Image image;
    private String[] desc;

    private MediaTracker mediaTracker;
    private TVTimerSpec downloadTimer;

    RadioChannelPageRenderer ren = new RadioChannelPageRenderer();

    private RadioChannelPage() {
        setRenderer(ren);
        mediaTracker = new MediaTracker(this);

        downloadTimer = new TVTimerSpec();
        downloadTimer.setDelayTime(DataCenter.getInstance().getLong("RADIO_IMAGE_REQUEST_DELAY", 2000L));
        downloadTimer.addTVTimerWentOffListener(this);
    }

    public synchronized void start(Channel ch) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"RadioChannelPage.start: " + ch);
        }
        channel = ch;
        setImage(null);
        TVTimer.getTimer().deschedule(downloadTimer);
        try {
            downloadTimer = TVTimer.getTimer().scheduleTimerSpec(downloadTimer);
        } catch (TVTimerScheduleFailedException ex) {
            timerWentOff(null);
        }
        desc = TextUtil.split(ch.getDescription(), ren.fmDescription, 490);
        super.start();
    }

    public synchronized void stop() {
        TVTimer.getTimer().deschedule(downloadTimer);
        channel = null;
        setImage(null);
        super.stop();
    }

    private synchronized void imageLoaded(Channel ch, Image im) {
        if (ch != null && ch != channel) {
            return;
        }
        if (im != null) {
            // ch can be null
            setImage(im);
        } else {
            setImage(DataCenter.getInstance().getImage("default_Radio.jpg"));
        }
        prepare();
        repaint();
    }

    private void setImage(Image im) {
        synchronized (this) {
            if (image != null) {
                image.flush();
            }
            image = im;
        }
        if (im != null) {
            mediaTracker.addImage(im, 1818);
            try {
                mediaTracker.waitForID(1818);
            } catch (Exception ex) {
            }
            mediaTracker.removeImage(im);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        new ImageDownloadThread(channel).start();
    }

    class ImageDownloadThread extends Thread {

        Channel targetChannel;

        public ImageDownloadThread(Channel ch) {
            targetChannel = ch;
        }

        public void run() {
            Image im = null;
            String name = TextUtil.urlEncode(targetChannel.getCallLetter()) + "_radio.jpg";
            String path = SharedMemory.getInstance().get(SharedDataKeys.MS_URL)
                        + DataCenter.getInstance().getString("RADIO_IMAGE_PATH") + name;
            byte[] b = null;
            try {
                b = URLRequestor.getBytes(path, null);
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (b != null) {
                im = Toolkit.getDefaultToolkit().createImage(b);
            }
            imageLoaded(targetChannel, im);
        }
    }

    public boolean handleKey(int code) {
        if (code == OCRcEvent.VK_ENTER) {
            Log.printDebug(App.LOG_HEADER+"RadioChannelPage: keyEnter");
            MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            if (ms != null) {
                try {
                    ms.startScreenSaver();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            return false;
        }
        return false;
    }

    class RadioChannelPageRenderer extends FullScreenPanelRenderer {

        Font fChannel = FontResource.BLENDER.getFont(26);
        Font fDescription = FontResource.BLENDER.getFont(18);
        Font fFrequency = FontResource.BLENDER.getFont(16);
        Font fFrequencyValue = FontResource.DINMED.getFont(15);
        Font fFullName = FontResource.BLENDER.getFont(27);

        FontMetrics fmDescription = FontResource.getFontMetrics(fDescription);

        Color cChannelNumber = Color.white;
        Color cChannelName = new Color(222, 222, 222);
        Color cDescription = new Color(200, 200, 200);
        Color cFrequency = new Color(185, 185, 185);
        Color cFrequencyValue = new DVBColor(255, 255, 255, 230);
        Color cFullName = new Color(252, 202, 4);

        public void prepare(UIComponent c) {
            super.prepare(c);
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paintBackground(Graphics g, UIComponent c) {
        }

        protected void paint(Graphics g, UIComponent c) {
            Image im = image;
            if (im == null || im.getWidth(c) <= 0) {
                if (App.EMULATOR) {
                    super.paint(g, c);
                }
                return;
            }
            g.drawImage(im, 0, 0, c);
            super.paint(g, c);

            Channel ch = channel;

            g.setFont(fChannel);
            g.setColor(cChannelNumber);
            g.drawString(String.valueOf(ch.getNumber()), 437, 161);
            g.setColor(cChannelName);
            g.drawString(ch.getName(), 499, 161);

            g.setFont(fFullName);
            g.setColor(cFullName);
            g.drawString(ch.fullName, 439, 207);

            g.setFont(fDescription);
            g.setColor(cDescription);
            int y = 227;
            for (int i = 0; i < desc.length; i++) {
                g.drawString(desc[i], 439, y);
                y += 20;
            }

            String fq = ch.radioFrequency;
            if (fq != null && fq.length() > 0) {
                y += 17;
                g.setFont(fFrequency);
                g.setColor(cFrequency);
                g.drawString(dataCenter.getString("epg.frequency"), 439, y);

                g.setFont(fFrequencyValue);
                g.setColor(cFrequencyValue);
                g.drawString(ch.radioFrequency, 533, y);
            }
        }
    }

}

package com.alticast.illico.epg.ppv;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;
import java.util.*;

public class OtherShowTimesPanel extends ListOptionPanel implements ReminderListener, RightFilterListener, PpvListener {

    public Vector infoList;

    public PpvProgram program;

    private MenuItem MENU_BACK_TO_DETAILS = new MenuItem("ppv.back_to_details");

    public OtherShowTimesPanel() {
        super("ppv.showtimes", 7, null);
        setRenderer(new OtherShowTimesPanelRenderer());
    }

    public void start(PpvProgram p, Vector list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.start("+p+")");
        program = p;

        focus = 0;
        listSize = list.size();
        listFocus = (listSize > 0) ? 0 : -1;
        this.list = list;

        infoList = new Vector(listSize);
        for (int i = 0; i < listSize; i++) {
            Object[] info = new Object[3];
            PpvProgram pp = (PpvProgram) list.elementAt(i);
            info[0] = String.valueOf(pp.getEpgChannel().getNumber());
            info[1] = Boolean.valueOf(PpvController.isOrderableTime(pp));
            info[2] = Boolean.valueOf(PpvController.getInstance().isPurchasedOrReserved(pp));
            infoList.addElement(info);
        }
        MenuItem root = new MenuItem("PPV_MORE_SHOWTIMES");
        if (listSize > 0) {
            root.add(Rs.MENU_ORDER);
            focus = -1;
        }
        root.add(MENU_BACK_TO_DETAILS);
        menu = root;
        size = menu.size();

        updateButton();
        this.start();
    }

    public void updateStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.updateStatus()");
        for (int i = 0; i < listSize; i++) {
            Object[] info = (Object[]) infoList.elementAt(i);
            PpvProgram pp = (PpvProgram) list.elementAt(i);
            info[1] = Boolean.valueOf(PpvController.isOrderableTime(pp));
            info[2] = Boolean.valueOf(PpvController.getInstance().isPurchasedOrReserved(pp));
        }
        updateButton();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.handleKey("+code+")");
        int lastListFocus = listFocus;
        boolean ret = super.handleKey(code);
        if (ret && listFocus != lastListFocus) {
            updateButton();
        }
        return ret;
    }

    private void updateButton() {
        if (listSize > 0) {
            PpvProgram p = (PpvProgram) list.elementAt(listFocus);
            Object[] info = (Object[]) infoList.elementAt(listFocus);
            if (ParentalControl.getLevel(p) != ParentalControl.OK) {
                menu.setItemAt(Rs.MENU_UNBLOCK, 0);
            } else if (Boolean.TRUE.equals(info[2])) {
                menu.setItemAt(Rs.MENU_CANCEL_ORDER, 0);
            } else if (Boolean.TRUE.equals(info[1]) && PpvController.hasPackageAuth) {
                menu.setItemAt(Rs.MENU_ORDER, 0);
            } else if (ReminderManager.getInstance().containsOrderable(p)) {
                menu.setItemAt(Rs.MENU_REMOVE_ALERT, 0);
            } else {
                menu.setItemAt(Rs.MENU_SET_ALERT, 0);
            }
        }
    }

    public void selected(MenuItem item) {
        if (item == MENU_BACK_TO_DETAILS) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_BACK_TO_DETAILS");
            Container c = getParent();
            if (c != null && c instanceof OtherShowTimesPage) {
                PpvController.getInstance().closePanel((OtherShowTimesPage) c);
            }
        } else if (item == Rs.MENU_SET_ALERT) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_SET_ALERT");
            final PpvProgram program = (PpvProgram) list.elementAt(listFocus);

            PpvPopupRenderer r = new PpvPopupRenderer(program, "ppv.reminder_confirm_msg_1", false);
            r.setTopMessage(DataCenter.getInstance().getString("ppv.reminder_confirm_msg_0"));
            String[] buttons = new String[] { Popup.BUTTON_YES, Popup.BUTTON_NO };

            PopupListener l = new PopupListener() {
                public void notifySelected(Popup p, String key) {
                    if (Popup.BUTTON_YES.equals(key)) {
                        ReminderManager.getInstance().remindOrderable(program, OtherShowTimesPanel.this);
                        updateStatus();
                    }
                }
                public void notifyCanceled(Popup p, boolean byTimer) {
                }
            };

            PopupController.getInstance().showCustomPopup(
                    DataCenter.getInstance().getString("ppv.set_alert_popup_title"),
                    buttons, r, l);


        } else if (item == Rs.MENU_REMOVE_ALERT) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_REMOVE_ALERT");
            final PpvProgram program = (PpvProgram) list.elementAt(listFocus);

            PpvPopupRenderer r = new PpvPopupRenderer(program, "ppv.remove_alert_confirm", false);
            String[] buttons = new String[] { Popup.BUTTON_YES, Popup.BUTTON_NO };

            PopupListener l = new PopupListener() {
                public void notifySelected(Popup p, String key) {
                    if (Popup.BUTTON_YES.equals(key)) {
                        ReminderManager.getInstance().removeOrderable(program);
                        updateStatus();
                    }
                }
                public void notifyCanceled(Popup p, boolean byTimer) {
                }
            };

            PopupController.getInstance().showCustomPopup(
                    DataCenter.getInstance().getString("ppv.remove_alert_popup_title"),
                    buttons, r, l);

        } else if (item == Rs.MENU_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_ORDER");
            PpvController.getInstance().showPurchasePopup((PpvProgram) list.elementAt(listFocus), this);
        } else if (item == Rs.MENU_CANCEL_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_CANCEL_ORDER");
            PpvController.getInstance().showCancelPopup((PpvProgram) list.elementAt(listFocus), this);
        } else if (item == Rs.MENU_UNBLOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.selected() : MENU_UNBLOCK");
            ChannelAssetPage.showPin(this);
        }
    }

    public void receiveCheckRightFilter(int response) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.receiveCheckRightFilter("+response+")");
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            ParentalControl.getInstance().programUnblocked(program);
            updateButton();
            if (program.isOnAir()) {
                ChannelController.get(0).releaseChannelBlock(PpvController.getInstance().currentChannel);
            }
            repaint();
        }
    }

    public String[] getPinEnablerExplain() {
        //->Kenneth[2015.11.23] VDTRMASTER-5718 : EpgCore 의 것을 사용한다.
        return EpgCore.getInstance().getBlockedChannelExplain(PpvController.getInstance().currentChannel, false);
        //return TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
        //<-
    }

    public void notifyReminderResult(Program p, boolean added) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.notifyReminderResult()");
        if (added) {
            updateStatus();
        }
    }

    public void notifyPurchaseResult(int result) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.notifyPurchaseResult()");
        if (result == PpvListener.PURCHASE_SUCCESS) {
            updateStatus();
        }
    }

    public void notifyCancelResult(int result) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OtherShowTimesPanel.notifyCancelResult()");
        if (result == PpvListener.CANCEL_SUCCESS) {
            updateStatus();
        }
    }
}

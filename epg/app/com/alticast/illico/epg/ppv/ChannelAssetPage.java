package com.alticast.illico.epg.ppv;

import com.opencable.handler.cahandler.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import java.util.*;
import javax.tv.util.*;

public class ChannelAssetPage extends FullScreenPanel
                              implements MenuListener, TVTimerWentOffListener {

//    // 현재 구매 가능
//    public static final int NOW_PLAYING                        = 10;
//    // 현재 구매 가능 + 미래 구매 프로그램 있음 (2시간 이후)
//    public static final int NOW_PLAYING_WITH_ALREADY_PURCHASED = 15;
//    // 현재 이미 시작, 미래 구매 가능
//    public static final int ALREADY_BEGUN                      = 25;
//    // 현재 이미 시작, 미래 구매 불가
//    public static final int NOT_AVAILABLE_YET                  = 30;
//    // 현재 이미 시작, 미래 구매 프로그램 있음 (2시간 이후)
//    public static final int ALREADY_PURCHASED                  = 40;
//    // 현재 이미 시작, 미래 시작 대기
//    public static final int COUNT_DOWN                         = 50;
//
//    // 현재 이미 시작 + 다음 프로그램 없음
//    public static final int ALREADY_BEGUN_NO_FUTURE            = 1;
//    // 현재 없음 + 다음 (아마도 구매 가능)
//    public static final int FUTURE                             = 2;

    private static Image brand_ind = DataCenter.getInstance().getImage("brandzone_ind.png");
    private static Image brand_vcc = DataCenter.getInstance().getImage("brandzone_vcc.png");

    public Channel channel;
    public Program program;

    public PpvProgram current;
    public PpvProgram next;
    public boolean currentOrderable;
    public boolean nextOrderable;

    public PpvProgram top;
    public PpvProgram bottom;

    public boolean countDown;
    public boolean hasFutureOrder;

    ScrollTexts scroll = new ScrollTexts();
    OptionScreen optionScreen = new OptionScreen();
    ClickingEffect clickEffect;

    public MenuItem root;
    int size;

    public long nextUpdateTime;

    TVTimerSpec secTimer = new TVTimerSpec();
    TVTimerSpec updateTimer = new TVTimerSpec();

    static Hashtable bannerCache = new Hashtable();
    static int ppvVersion = Integer.MIN_VALUE / 2;

    ChannelAssetPageRenderer pageRenderer = new ChannelAssetPageRenderer();

    boolean running = false;

    public ChannelAssetPage() {
        setRenderer(pageRenderer);
        stack.push("ppv.pay_per_view");

        secTimer.setDelayTime(500L);
        secTimer.setRegular(true);
        secTimer.setRepeat(true);
        secTimer.addTVTimerWentOffListener(this);

        updateTimer.addTVTimerWentOffListener(this);

        DataCenter dc = DataCenter.getInstance();

        scroll.setForeground(new Color(182, 182, 182));
        scroll.setRowHeight(19);
        scroll.setFont(FontResource.DINMED.getFont(15));
        scroll.setInsets(4, 8, 10, 0);
        scroll.setBounds(145, 388, 608-145, 459-388);
        scroll.setRows(6);
        scroll.setBottomMaskImage(pageRenderer.iBottomSha);
        scroll.setBottomMaskImagePosition(0, 105);
        scroll.setTopMaskImagePosition(0, 0);
        scroll.setVisible(true);
        this.add(scroll);

        footer.setBounds(0, 488, 906, 25);
        this.add(footer);
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start()");
        footer.reset();
        Image iconScroll = footer.addButton(PreferenceService.BTN_PAGE, "epg.scroll_description");
        footer.addButton(PreferenceService.BTN_SEARCH, "epg.search");
        footer.addButton(PreferenceService.BTN_D, "epg.options");
        footer.linkWithScrollTexts(iconScroll, scroll);

        super.start();
        updateDesc();
        repaint();
        running = true;
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.stop()");
        optionScreen.stop();
        super.stop();
        channel = null;
        current = null;
        next = null;
        top = null;
        bottom = null;
        program = null;
        TVTimer.getTimer().deschedule(secTimer);
        TVTimer.getTimer().deschedule(updateTimer);
        focus = 0;
        running = false;
    }

    public boolean isRunning() {
        return running;
    }

    public void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.pause()");
        optionScreen.stop();
    }

    public synchronized boolean start(Channel ch, Program program) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start("+ch+", "+program+")");
        if (program == null) {
            return false;
        }
        Program cp = program;
        while (cp != null && !(cp instanceof PpvProgram)) {
            cp = cp.next;
        }
        if (cp == null) {
            return false;
        }
        PpvController pc = PpvController.getInstance();
        PpvProgram p = (PpvProgram) cp;

        this.channel = ch;
        this.program = program;

        long cur = System.currentTimeMillis();

        boolean isCurrent = p.startTime <= cur;

        boolean auth;
        if (isCurrent) {
            if (App.EMULATOR) {
                auth = PpvHistoryManager.getInstance().contains(p);
            } else {
                CAAuthorization caa = pc.checkPurchased(ch, p, pc);
                try {
                    auth = caa.isIppvEventPurchased();
                } catch (Exception ex) {
                    auth = false;
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : auth = "+auth);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : Call PpvController.setMonitoring(CAAuthorization)");
                PpvController.getInstance().setMonitoring(caa);
            }
            if (auth) {
                Log.printInfo(App.LOG_HEADER+"ChannelAssetPage.start : purchased !");
                return false;
            }

            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : case current program");
            // 현재
            current = p;
            next = null;
            Program np = p.next;
            while (np != null && !(np instanceof PpvProgram)) {
                np = np.next;
            }
            if (np != null) {
                next = (PpvProgram) np;
            }

            currentOrderable = PpvController.isOrderableTime(current);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : currentOrderable = "+currentOrderable);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : next PpvProgram = "+next);
            if (currentOrderable) {
                // 현재 구매 가능 - next 표시할 필요 없음
                top = null;
                bottom = current;
            } else if (next == null) {
                // 현재 구매 불가 - next 도 없음
                top = null;
                bottom = current;
            } else {
                top = current;
                bottom = next;
                // 현재 구매 불가 - next 있음
                nextOrderable = PpvController.isOrderableTime(next);
                if (App.EMULATOR) {
                    auth = PpvHistoryManager.getInstance().contains(next);
                } else {
                    auth = checkAuth(ch, next);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : auth = "+auth);
            }
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : case next program");
            // 미래
            current = null;
            next = p;
            top = null;
            bottom = next;
            nextOrderable = PpvController.isOrderableTime(next);
            auth = checkAuth(ch, next);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : auth = "+auth);
        }
        this.hasFutureOrder = bottom != null && PpvHistoryManager.getInstance().hasFutureOrder(ch.getCallLetter(), bottom.endTime);
        this.countDown = auth;  // next에 auth 인 경우만 이리로 올 수 있다.

        setUpdateTimer();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : Call setLayout()");
        setLayout();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : Call start()");
        start();
        if (bottom != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.start() : Call readyBanner()");
            readyBanner(bottom.indicator);
        }
        return true;
    }

    private boolean checkAuth(Channel ch, PpvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.checkAuth("+ch+", "+p+")");
        PpvController pc = PpvController.getInstance();
        CAAuthorization caa = pc.checkPurchased(ch, p, pc);
        boolean auth;
        try {
            auth = caa.isIppvEventPurchased();
        } catch (Exception ex) {
            auth = false;
        }
        auth = auth || PpvHistoryManager.getInstance().contains(p);
        pc.setMonitoring(caa);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.checkAuth() returns "+auth);
        return auth;
    }

    void setLayout() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.setLayout()");
        TVTimer.getTimer().deschedule(secTimer);
        MenuItem[] menus;
        if (countDown) {
            if (isBlocked(bottom)) {
                menus = new MenuItem[] { Rs.MENU_UNBLOCK };
            } else {
                menus = new MenuItem[] { Rs.MENU_OTHER_SHOWTIME, Rs.MENU_CANCEL_ORDER };
            }
            try {
                TVTimer.getTimer().scheduleTimerSpec(secTimer);
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            if (isBlocked(bottom)) {
                menus = new MenuItem[] { Rs.MENU_UNBLOCK, Rs.MENU_HOW_TO_ORDER };
            } else {
                if (bottom == current ? currentOrderable : nextOrderable) {
                    menus = new MenuItem[] { Rs.MENU_ORDER, Rs.MENU_OTHER_SHOWTIME, Rs.MENU_HOW_TO_ORDER };
                } else {
                    menus = new MenuItem[] { Rs.MENU_OTHER_SHOWTIME, Rs.MENU_HOW_TO_ORDER };
                }
            }
        }

        scroll.showFirstPage();
        if (top == null) {
            scroll.setBounds(145, 330, 608-145, 459-330);
            scroll.setRows(6);
            scroll.setBottomMaskImagePosition(0, 96);
            scroll.setTopMaskImage(pageRenderer.iTopSha2);
        } else {
            scroll.setBounds(145, 388, 608-145, 459-388);
            scroll.setRows(3);
            scroll.setBottomMaskImagePosition(0, 38);
            scroll.setTopMaskImage(pageRenderer.iTopSha);
        }
        updateDesc();
        MenuItem newRoot = new MenuItem("PPV_PAGE");
        for (int i = 0; i < menus.length; i++) {
            if (PpvController.hasPackageAuth || menus[i] != Rs.MENU_ORDER) {
                 newRoot.add(menus[i]);
            }
        }

        int oldFocus = focus;
        this.root = newRoot;
        this.size = root.size();
        if (focus < 0 && top == null) {
            focus = 0;
        } else if (focus >= size) {
            focus = size - 1;
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelAssetPage.setLayout: top = " + top);
            Log.printDebug("ChannelAssetPage.setLayout: bottom = " + bottom);
            Log.printDebug("ChannelAssetPage.setLayout: current = " + current);
            Log.printDebug("ChannelAssetPage.setLayout: next = " + next);
            Log.printDebug("ChannelAssetPage.setLayout: focus = " + oldFocus + " -> " + focus);
        }
    }

    private boolean isBlocked(PpvProgram p) {
        return ParentalControl.getLevel(p) != ParentalControl.OK;
    }

    private void updateDesc() {
        if (bottom != null && ParentalControl.getLevel(bottom) != ParentalControl.ADULT_CONTENT) {
            scroll.setContents(bottom.getFullDescription());
        } else {
            scroll.setContents(" ");
        }
    }

    static void updateToUnblockMenu(MenuItem[] menus) {
        if (menus != null) {
            for (int i = 0; i < menus.length; i++) {
                if (menus[i] == Rs.MENU_ORDER || menus[i] == Rs.MENU_CANCEL_ORDER) {
                    menus[i] = Rs.MENU_UNBLOCK;
                }
            }
        }
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey("+code+")");
        switch (code) {
            case KeyCodes.COLOR_D:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : COLOR_D");
                footer.clickAnimationByKeyCode(code);
                optionScreen.start(Rs.PPV_OPTIONS, null, this, EpgCore.getInstance());
                break;
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_ENTER");
                keyEnter(focus);
                break;
            case OCRcEvent.VK_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_UP");
                if (focus == 0 && top != null) {
                    focus = -1;
                } else if (focus == -1) {
                    focus = size - 1;
                } else {
                    focus = (focus - 1 + size) % size;
                }
                repaint();
                break;
            case OCRcEvent.VK_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_DOWN");
                if (focus == -1) {
                    focus = 0;
                } else if (focus == size - 1 && top != null) {
                    focus = -1;
                } else {
                    focus = (focus + 1) % size;
                }
                repaint();
                break;
            case OCRcEvent.VK_PAGE_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_PAGE_UP");
                footer.clickAnimationByKeyCode(code);
                scroll.showPreviousPage();
                break;
            case OCRcEvent.VK_PAGE_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_PAGE_DOWN");
                footer.clickAnimationByKeyCode(code);
                scroll.showNextPage();
                break;
            case OCRcEvent.VK_LEFT:
            case OCRcEvent.VK_RIGHT:
                // block this key
                break;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : SEARCH");
                footer.clickAnimationByKeyCode(code);
                return false;
            case OCRcEvent.VK_EXIT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : VK_EXIT");
                ChannelController cc = ChannelController.get(0);
                cc.changeChannel(cc.getLastWatchedChannel());
                return true;
            //->Kenneth[2015.8.5] VDTRMASTER-5543 : menu 눌렸을때 D 팝업 지워야 함.(위젯도)
            case KeyCodes.MENU:
            case KeyCodes.WIDGET:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.handleKey() : MENU/WIDGET : Call optionScreen.stop()");
                optionScreen.stop();
                return false;
                //<-
            default:
                return false;
        }
        return true;
    }

    public void keyEnter(int focus) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.keyEnter("+focus+")");
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(((ChannelAssetPageRenderer) renderer).getButtonFocusBounds(focus));

        if (focus == -1) {
            if (ParentalControl.getLevel(top) != ParentalControl.OK) {
                showPin(new PpvProgramRightFilterListener(top));
            } else {
                PpvController.getInstance().showOtherShowTimes(top);
            }
        } else {
            selected(root.getItemAt(focus));
        }
    }

    /** MenuListener. */
    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected("+item+")");
        String key = item.getKey();

        DataCenter dc = DataCenter.getInstance();
        if (item == Rs.MENU_OTHER_SHOWTIME) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_OTHER_SHOWTIME");
            PpvController.getInstance().showOtherShowTimes(bottom);
        } else if (item == Rs.MENU_CART) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_CART");
            PpvController.getInstance().showCart();
        } else if (item == Rs.MENU_HOW_TO_ORDER_OPTION || item == Rs.MENU_HOW_TO_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_HOW_TO_ORDER_OPTION || MENU_HOW_TO_ORDER");
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_HOW_TO_ORDER);
        } else if (item == Rs.MENU_TUNE_TO_TRAILER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_TUNE_TO_TRAILER");
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_TRAILER);
        } else if (item == Rs.MENU_NEW_THIS_MONTH) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_NEW_THIS_MONTH");
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_NEW_THIS_MONTH);
        } else if (item == Rs.MENU_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_ORDER");
            if (App.EMULATOR) {
                // Note - test code
                PpvHistoryManager.getInstance().addToHistory(bottom, false);
            }
            PpvController.getInstance().showPurchasePopup(bottom, null);
        } else if (item == Rs.MENU_CANCEL_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_CANCEL_ORDER");
            PpvController.getInstance().showCancelPopup(bottom, null);
        } else if (item == Rs.MENU_UNBLOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.selected() : MENU_UNBLOCK");
            showPin(new PpvProgramRightFilterListener(bottom));
        }
    }

    public void canceled() {
    }

    private void setUpdateTimer() {
        long cur = System.currentTimeMillis();
        nextUpdateTime = Math.min(getClosestNextTime(current, cur), getClosestNextTime(next, cur));
        if (Log.DEBUG_ON) {
            Log.printDebug("ChannelAssetPage: next update time = " + new Date(nextUpdateTime));
        }
        updateTimer.setAbsoluteTime(nextUpdateTime + 600L);
        try {
            TVTimer.getTimer().scheduleTimerSpec(updateTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private long getClosestNextTime(PpvProgram p, long from) {
        long time = Long.MAX_VALUE;
        if (p != null) {
            long target = p.startTime;
            if (from < target) {
                time = Math.min(time, target);
            }
            target = p.endTime;
            if (from < target) {
                time = Math.min(time, target);
            }
            target = p.marketingWindowStartTime;
            if (from < target) {
                time = Math.min(time, target);
            }
            target = p.buyWindowEndTime;
            if (from < target) {
                time = Math.min(time, target);
            }
        }
        return time;
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.timerWentOff()");
        if (e.getTimerSpec() == secTimer) {
            // NOTE - count down rect
            repaint(800, 288, 130, 37);
        } else {
            // 다음 페이지 - TODO 테스트
            if (!updateProgram()) {
                Channel ch = channel;
                Program p = program;
                PpvController.getInstance().hideAssetPage();
                if (ch != null && p != null && ParentalControl.getLevel(p) != ParentalControl.OK) {
                    ChannelBackground.getInstance().stopPanel();
                    ChannelBackground.getInstance().showBlockedPanel(ch, ChannelController.BLOCK_BY_RATING);
                }
            }
        }
    }

    public synchronized boolean updateProgram() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.updateProgram()");
        if (channel != null) {
            Program p = EpgDataManager.getInstance().getProgram(channel, Clock.getRoundedMillis());
            return start(channel, p);
        }
        return false;
    }

    ////////////////////////////////////////////////////////////////////////////
    // RightFilter
    ////////////////////////////////////////////////////////////////////////////


    public static void showPin(RightFilterListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.showPin()");
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        if (ps == null) {
            return;
        }
        try {
            ps.checkRightFilter(l, App.NAME, new String[] { RightFilter.ALLOW_BYPASS_BLOCKED_CHANNEL }, null, null);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    class PpvProgramRightFilterListener implements RightFilterListener {
        PpvProgram target;

        //String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');

        public PpvProgramRightFilterListener(PpvProgram p) {
            target = p;
        }

        public void receiveCheckRightFilter(int response) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.PpvProgramRightFilterListener.receiveCheckRightFilter("+response+")");
            if (response == PreferenceService.RESPONSE_SUCCESS) {
                ParentalControl pc = ParentalControl.getInstance();
                pc.programUnblocked(target);
                setLayout();
//                setType(type);
                if (target.isOnAir()) {
                    ChannelController.get(0).releaseChannelBlock(PpvController.getInstance().currentChannel);
                }
            }
        }

        public String[] getPinEnablerExplain() {
            //->Kenneth[2015.11.23] VDTRMASTER-5718 : EpgCore 의 것을 사용한다.
            return EpgCore.getInstance().getBlockedChannelExplain(PpvController.getInstance().currentChannel, false);
            //<-
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Banner
    ////////////////////////////////////////////////////////////////////////////

    private void readyBanner(final String indicator) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.readyBanner("+indicator+")");
        final String path = SharedMemory.getInstance().get(SharedDataKeys.MS_URL)
                + DataCenter.getInstance().getString(indicator + "_BANNER_PATH");
        if (path == null || path.length() == 0) {
            return;
        }
        Thread t = new Thread() {
            public void run() {
                boolean versionChanged = false;
                try {
                    int version = CommunicationManager.getMonitorService().getInbandDataVersion(MonitorService.ppv_banner);
                    versionChanged = ppvVersion != version;
                    if (Log.DEBUG_ON) {
                        Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.readyBanner: version = " + version + ", " + ppvVersion);
                    }
                    ppvVersion = version;
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (versionChanged) {
                    synchronized (bannerCache) {
                        Enumeration en = bannerCache.elements();
                        while (en.hasMoreElements()) {
                            Image old = (Image) en.nextElement();
                            if (old != null) {
                                old.flush();
                            }
                        }
                        bannerCache.clear();
                    }
                } else {
                    if (bannerCache.get(indicator) != null) {
                        return;
                    }
                }

                byte[] b = null;
                try {
                    b = URLRequestor.getBytes(path, null);
                } catch (Exception ex) {
                }
                if (b != null) {
                    Image im = Toolkit.getDefaultToolkit().createImage(b);
                    synchronized (bannerCache) {
                        Image old = (Image) bannerCache.get(indicator);
                        if (old != null) {
                            old.flush();
                        }
                        bannerCache.put(indicator, im);
                    }
                    MediaTracker mt = new MediaTracker(ChannelAssetPage.this);
                    mt.addImage(im, 1818);
                    try {
                        mt.waitForID(1818);
                    } catch (Exception ex) {
                    }
                    mt.removeImage(im);
                    repaint();
                }
            }
        };
        t.start();
    }

    public Image getBannerImage() {
        if (bottom == null) {
            return null;
        }
        String indicator = bottom.indicator;
        Image im = (Image) bannerCache.get(indicator);
        if (im != null) {
            return im;
        }
        if (PpvProgram.INDICATOR_IND.equals(indicator)) {
            return brand_ind;
        } else if (PpvProgram.INDICATOR_VCC.equals(indicator)) {
            return brand_vcc;
        }
        return null;
    }

}

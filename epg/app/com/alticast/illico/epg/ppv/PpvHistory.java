package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.ui.*;
import org.dvb.event.*;
import java.io.*;
import java.util.*;

public class PpvHistory implements AbstractProgram, Comparable {

    public String channelNumber;
    public String channelName;
    public long eid;
    public String packageId;
    public long startTime;
    public long endTime;
    public String title;
    public float price;
	//->Kenneth : 4K by June
    public int definition;
    public long cancelableTime;
    public int rating;
    public String programId;
    public long buyWindowStartTime;
    public boolean purchased = true;

    public PpvProgram program;

    private PpvHistory(String line) {
        String[] s = TextUtil.tokenize(line, '|');

        channelNumber = s[0];
        channelName = s[1];
        eid = Long.parseLong(s[2]);
        packageId = s[3];

        startTime = Long.parseLong(s[4]);
        endTime = Long.parseLong(s[5]);
        title = s[6];
        price = Float.parseFloat(s[7]);
		//->Kenneth : 4K by June
        try {
            definition = Integer.parseInt(s[8]);
        } catch (Exception ex) {
            if ("true".equalsIgnoreCase(s[8])) {
                definition = 1;
            }
        }
        cancelableTime = Long.parseLong(s[9]);
        try {
            rating = Integer.parseInt(s[10]);
        } catch (Exception ex) {
        }
        try {
            programId = s[11];
        } catch (Exception ex) {
            programId = channelName + startTime;
        }
        try {
            buyWindowStartTime = Long.parseLong(s[12]);;
        } catch (Exception ex) {
        }
        try {
            purchased = !("false".equalsIgnoreCase(s[13]));
        } catch (Exception ex) {
        }
    }

    private PpvHistory(PpvProgram p, boolean isPurchased) {
        program = p;
        Channel ch = p.getEpgChannel();

        channelNumber = String.valueOf(ch.getNumber());
        channelName = ch.getName();
        eid = p.eid;
        packageId = p.packageId;

        startTime = p.startTime;
        endTime = p.endTime;
        title = p.getTitle();
        price = p.price;
		//->Kenneth : 4K by June
        //->Kenneth[2015.2.4] 4K
        if (ch.getDefinition() == Constants.DEFINITION_4K) {
            definition = Constants.DEFINITION_4K;
        } else {
            definition = p.getDefinition();
        }
        cancelableTime = p.cancelWindowEndTime;
        rating = p.getRatingIndex();
        programId = p.getId();
        buyWindowStartTime = p.buyWindowStartTime;
        purchased = isPurchased;
    }

    public static PpvHistory create(String line) {
        try {
            return new PpvHistory(line);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

//    public static PpvHistory create(PpvProgram p) {
//        return create(p, true);
//    }

    public static PpvHistory create(PpvProgram p, boolean isPurchased) {
        try {
            return new PpvHistory(p, isPurchased);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    public String toString() {
        return channelNumber + "|" + channelName + '|' + eid + '|' + packageId
                + '|' + startTime + '|' + endTime + '|' + title + '|' + price + '|' + definition
                + '|' + cancelableTime + '|' + rating + '|' + programId + '|' + buyWindowStartTime + '|' + purchased;
    }

    public PpvProgram getProgram() {
        Program p = EpgDataManager.getInstance().getProgram(
                    ChannelDatabase.current.getChannelByName(channelName), startTime);
        if (p instanceof PpvProgram) {
            return (PpvProgram) p;
        } else {
            return null;
        }
    }

    public boolean equals(Object o) {
        if (o instanceof PpvHistory) {
            PpvHistory h = (PpvHistory) o;
            return channelName.equals(h.channelName) && eid == h.eid && startTime == h.startTime;
        }
        return false;
    }

    public String getCallLetter() {
        return channelName;
    }

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public int getRatingIndex() {
        return rating;
    }

    public String getId() {
        return programId;
    }

    public boolean contains(long time) {
        return startTime <= time && time < endTime;
    }

    /** Comparable. */
    public int compareTo(Object o) {
        if (o instanceof PpvHistory) {
            PpvHistory h = (PpvHistory) o;
            if (this.startTime > h.startTime) {
                return 1;
            } else if (this.startTime < h.startTime) {
                return -1;
            } else {
                return channelNumber.compareTo(h.channelNumber);
            }
        } else {
            return -1;
        }
    }

}

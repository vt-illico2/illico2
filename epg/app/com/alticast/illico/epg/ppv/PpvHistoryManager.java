package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import java.util.*;
import java.io.*;
import javax.tv.util.*;

public class PpvHistoryManager implements EpgDataListenerInternal, TVTimerWentOffListener {
    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty("dvb.persistent.root");
    private static final String DIRECTORY = REPOSITORY_ROOT + "/ppv/";
    private static final String FILE_NAME = DIRECTORY + "history.txt";

    private Vector history = new Vector();
    // EPG 데이터 변경에 의해 Program이 변경되었는데, 못 찾으면 임시로 여기로 옮긴다.
    private Vector changedHistory = new Vector();

    private TVTimerSpec updateTimer = new TVTimerSpec();
    public static long nextUpdateTime;
    public static long lastUpdateTime;
    public static int lastUpdateCount;

    private static final long BUY_MARGIN = Constants.MS_PER_MINUTE;
    private static final long MIN_UPDATE_PERIOD = 10 * Constants.MS_PER_MINUTE;

    private static PpvHistoryManager instance = new PpvHistoryManager();

    public static PpvHistoryManager getInstance() {
        return instance;
    }

    private PpvHistoryManager() {
        EpgDataManager.getInstance().addListener(this);
        updateTimer.addTVTimerWentOffListener(this);
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.init()");
        File file = new File(DIRECTORY);
        if (!file.exists()) {
            if (Log.DEBUG_ON) {
                Log.printDebug(file + "is not exist, so mkdirs");
            }
            file.mkdirs();
        }
        history = readHistory();
        startTimer(System.currentTimeMillis() + MIN_UPDATE_PERIOD);
        resetUpdateTimer();
    }

    private Vector readHistory() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.readHistory()");
        String[] s = TextReader.read(new File(FILE_NAME));
        Vector v = new Vector();
        for (int i = 0; s != null && i < s.length; i++) {
            PpvHistory h = PpvHistory.create(s[i]);
            if (h != null) {
                v.addElement(h);
            }
        }
        Log.printWarning("PpvHistoryManager.readHistory: size = " + v.size());
        return v;
    }

    private synchronized void writeHistory() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.writeHistory()");
        PpvHistory[] array = new PpvHistory[history.size()];
        history.copyInto(array);
        TextWriter.write(array, new File(FILE_NAME));
        resetUpdateTimer();
        PpvController.getInstance().updateCurrentCart();
    }

    public Vector getAllHistory() {
        return history;
    }

    public Vector getCurrentHistory() {
        Vector v = new Vector();
        long cur = System.currentTimeMillis();
        Enumeration en = history.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = ((PpvHistory) en.nextElement());
            if ((h.startTime > cur || h.cancelableTime > cur) && !changedHistory.contains(h)) {
                v.addElement(h);
            }
        }
        Collections.sort(v);
        return v;
    }

    public Vector getPastHistory() {
        Vector v = new Vector();
        long cur = System.currentTimeMillis();
        Enumeration en = history.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = ((PpvHistory) en.nextElement());
            if (h.startTime <= cur) {
                v.addElement(h);
            }
        }
        return v;
    }

    public boolean contains(PpvProgram p) {
        PpvHistory h = PpvHistory.create(p, true);
        if (h == null) {
            return false;
        }
        return history.contains(h);
    }

    protected synchronized void addToHistory(PpvProgram p, boolean inBuyWindow) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.addToHistory("+p+", "+inBuyWindow+")");
        PpvHistory h = PpvHistory.create(p, inBuyWindow);
        if (h == null) {
            Log.printWarning(App.LOG_HEADER+"PpvHistoryManager.add: fail to create PpvHistory : " + p);
            return;
        }
        if (history.contains(h)) {
            Log.printWarning(App.LOG_HEADER+"PpvHistoryManager.add: duplicated PpvHistory : " + h + ", " + p);
            return;
        }
        history.addElement(h);
        writeHistory();
    }

    /** from Remote PVR. */
    protected synchronized void notifyRemotePurchase(Vector programs) {
        int count = 0;
        Enumeration en = programs.elements();
        while (en.hasMoreElements()) {
            PpvProgram p = (PpvProgram) en.nextElement();
            // Reminder 추가.
            ReminderManager.getInstance().remindOrdered(p, null);
            boolean purchased = PpvController.getInstance().isPurchased(p);
            Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.notifyRemotePurchase: " + purchased + ", " + p);

            PpvHistory h = PpvHistory.create(p, purchased);
            if (h != null && !history.contains(h)) {
                history.addElement(h);
                count++;
            }
        }
        Log.printInfo(App.LOG_HEADER+"PpvHistoryManager.notifyRemotePurchase: added " + count + " / " + programs.size());
        if (count > 0) {
            writeHistory();
        }
    }

    protected void removeFromHistory(PpvProgram p) {
        PpvHistory h = PpvHistory.create(p, true);
        removeFromHistory(h);
    }

    protected synchronized void removeFromHistory(PpvHistory h) {
        Log.printInfo(App.LOG_HEADER+"PpvHistoryManager.removeFromHistory = " + h);
        boolean removed = history.removeElement(h);
        if (removed) {
            writeHistory();
        } else {
            Log.printWarning("PpvHistoryManager.removeFromHistory: not found PpvHistory : " + h);
        }
    }

    protected synchronized void notifyRemoteCancel(String packageId) {
        Vector toBeRemoved = new Vector();
        Enumeration en = history.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = (PpvHistory) en.nextElement();
            if (h.packageId.equals(packageId)) {
                toBeRemoved.add(h);
            }
        }
        int count = toBeRemoved.size();
        Log.printInfo(App.LOG_HEADER+"PpvHistoryManager.notifyRemoteCancel: found " + count + " PpvHistory with pid=" + packageId);
        if (count <= 0) {
            return;
        }
        en = toBeRemoved.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = (PpvHistory) en.nextElement();
            history.remove(h);
            RecordingStatus.getInstance().cancelPpv(h);
            ReminderManager.getInstance().remove(h);
        }
        writeHistory();
    }

    public boolean hasFutureOrder(String channelName, long minStartTime) {
        Enumeration en = history.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = (PpvHistory) en.nextElement();
            if (channelName.equals(h.channelName) && h.startTime >= minStartTime) {
                Log.printDebug("PpvHistoryManager.hasFutureOrder: " + h);
                return true;
            }
        }
        Log.printDebug("PpvHistoryManager.hasFutureOrder: not found.");
        return false;
    }

    private PpvProgram findProgramByHistory(PpvHistory history) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.findProgramByHistory("+history+")");
        EpgDataManager edm = EpgDataManager.getInstance();
        PackageIdFilter filter = new PackageIdFilter(history.packageId);
        ChannelList allList = ChannelDatabaseBuilder.getInstance().getDatabase().getAllChannelList();
        ChannelList ppvList = allList.filter(filter);
        Vector vector = edm.findPrograms(ppvList, filter);
        if (vector != null && vector.size() > 0) {
            return (PpvProgram) vector.elementAt(0);
        } else {
            return null;
        }
    }

    class PackageIdFilter implements ProgramFilter, ChannelFilter {
        private String packageId;

        private PackageIdFilter(String packageId) {
            this.packageId = packageId;
        }

        public boolean accept(Program p) {
            if (p instanceof PpvProgram) {
                return packageId.equals(((PpvProgram) p).packageId);
            }
            return false;
        }

        public boolean accept(Channel channel) {
            return channel.getType() == Channel.TYPE_PPV;
        }
    }

    /** EpgDataListener. */
    public void epgDataUpdated(byte type, long from, long to) {
        if (type != PPV_DATA) {
            return;
        }
        Log.printInfo(App.LOG_HEADER+"PpvHistoryManager.epgDataUpdated: before = " + changedHistory.size());
        boolean toSave = false;
        Vector newChanged = new Vector();
        for (int i = changedHistory.size() - 1; i >= 0; i--) {
            PpvHistory h = (PpvHistory) changedHistory.elementAt(i);
            Log.printInfo("PpvHistoryManager: changedHistory[" + i + "] = " + h);
            PpvHistory nh = findNewProgram(h);
            if (nh != null) {
                history.removeElement(h);
                history.addElement(nh);
                ReminderManager.getInstance().remove(h);
                if (nh.program != null) {
                    ReminderManager.getInstance().remindOrdered(nh.program, null);
                }
                toSave = true;
            } else {
                newChanged.addElement(h);
            }
        }

        for (int i = history.size() - 1; i >= 0; i--) {
            PpvHistory h = (PpvHistory) history.elementAt(i);
            if (from <= h.startTime && h.startTime < to && !changedHistory.contains(h)) {
                Log.printInfo("PpvHistoryManager: history[" + i + "] = " + h);
                PpvProgram p = h.getProgram();
                if (p == null || !h.packageId.equals(p.packageId) || h.startTime != p.startTime || !h.title.equals(p.getTitle())) {
                    PpvHistory nh = findNewProgram(h);
                    if (nh != null) {
                        history.setElementAt(nh, i);
                        ReminderManager.getInstance().remove(h);
                        if (nh.program != null) {
                            ReminderManager.getInstance().remindOrdered(nh.program, null);
                        }
                        toSave = true;
                    } else {
                        newChanged.addElement(h);
                    }
                } else {
                    Log.printInfo("PpvHistoryManager: history[" + i + "] not changed");
                }
            }
        }
        synchronized (this) {
            if (toSave) {
                writeHistory();
            }
            changedHistory = newChanged;
        }
        Log.printInfo("PpvHistoryManager.epgDataUpdated: after = " + changedHistory.size());
    }

    private PpvHistory findNewProgram(PpvHistory h) {
        PpvProgram p = findProgramByHistory(h);
        if (p != null) {
            Log.printInfo("PpvHistoryManager: found program = " + p);
            return PpvHistory.create(p, h.purchased);
        } else {
            Log.printInfo("PpvHistoryManager: not found program = " + p);
            return null;
        }
    }

    private void stopTimer(long time) {
        synchronized (FILE_NAME) {
            TVTimer.getTimer().deschedule(updateTimer);
        }
    }

    private void startTimer(long time) {
        synchronized (FILE_NAME) {
            TVTimer.getTimer().deschedule(updateTimer);
            nextUpdateTime = time;
            if (Log.DEBUG_ON) {
                Log.printDebug("PpvHistoryManager: nextUpdateTime = " + new Date(nextUpdateTime));
            }
            updateTimer.setAbsoluteTime(nextUpdateTime);
            try {
                TVTimer.getTimer().scheduleTimerSpec(updateTimer);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void resetUpdateTimer() {
        long time = System.currentTimeMillis() + MIN_UPDATE_PERIOD;
        Vector currentHistory = getCurrentHistory();
        Enumeration en = currentHistory.elements();
        while (en.hasMoreElements()) {
            PpvHistory h = (PpvHistory) en.nextElement();
            if (!h.purchased) {
                time = Math.min(time, h.buyWindowStartTime + BUY_MARGIN);
            }
        }
        if (time <= System.currentTimeMillis()) {
            time = System.currentTimeMillis() + Constants.MS_PER_MINUTE;
        }
        startTimer(time);
    }

    public void timerWentOff(TVTimerWentOffEvent ev) {
        updatePurchasedHistory();
    }

    public boolean updatePurchasedHistory(PpvHistory h) {
        if (h.purchased) {
            return true;
        }
        updatePurchasedHistory();
        return h.purchased;
    }

    public synchronized void updatePurchasedHistory() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvHistoryManager.updatePurchasedHistory()");
        lastUpdateTime = System.currentTimeMillis();
        Vector currentHistory = getCurrentHistory();
        Enumeration en = currentHistory.elements();
        int count = 0;
        int failed = 0;
        while (en.hasMoreElements()) {
            PpvHistory h = (PpvHistory) en.nextElement();
            if (!h.purchased && h.buyWindowStartTime < System.currentTimeMillis()) {
                boolean purchased = PpvController.getInstance().isPurchased(h);
                if (purchased) {
                    Log.printError("PpvHistoryManager: already purchased");
                    count++;
                    h.purchased = true;
                } else {
                    boolean result = PpvController.getInstance().purchase(h);
                    if (result) {
                        h.purchased = true;
                        count++;
                    } else {
                        history.removeElement(h);
                        ReminderManager.getInstance().remove(h);
                        failed++;
                    }
                }
            }
        }
        lastUpdateCount = count;
        synchronized (FILE_NAME) {
            if (count > 0 || failed > 0) {
                writeHistory();
            } else {
                resetUpdateTimer();
            }
        }
    }

}


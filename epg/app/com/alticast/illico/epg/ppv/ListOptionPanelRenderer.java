package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.Program;
import com.videotron.tvi.illico.log.Log;
import org.dvb.ui.DVBColor;
import org.ocap.event.UserEvent;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import com.alticast.illico.epg.*;

public abstract class ListOptionPanelRenderer extends OptionPanelRenderer {
    protected static final int LIST_Y_GAP = 32;

    private Image iBg;
    private Image iShT;
    private Image iShB;
    private Image iListFocus;
    private Image iListFocusDim;
    private Image iListLine;

    private Image iArrowUp = dataCenter.getImage("02_ars_t.png");
    private Image iArrowDown = dataCenter.getImage("02_ars_b.png");

    private Color cTitle = new Color(214, 182, 61);
    private Color cTitleShadow = new Color(46, 46, 45);
    private Font fTitle = FontResource.BLENDER.getFont(20);

    private Color cHeader = new Color(193, 193, 193);
    private Font fHeader = FontResource.BLENDER.getFont(18);

    protected abstract void paintList(Graphics g, ListOptionPanel o, int index, boolean focused);

    public void prepare(UIComponent c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanelRenderer.prepare()");
        ListOptionPanel o = (ListOptionPanel) c;
        switch (((ListOptionPanel) c).rowSize) {
        case 9:
            iBg = dataCenter.getImage("ppvlistbg02.png");
            iShT = dataCenter.getImage("07_list_sh_t.png");
            iShB = dataCenter.getImage("07_list_sh_b.png");
            iListFocus = dataCenter.getImage("06_list_foc.png");
            iListFocusDim = dataCenter.getImage("06_list_foc_dim.png");
            iListLine = dataCenter.getImage("06_listline_2.png");
            break;
        case 7:
            iBg = dataCenter.getImage("ppvlistbg01.png");
            iShT = dataCenter.getImage("07_list_sh2_t.png");
            iShB = dataCenter.getImage("07_list_sh2_b.png");
            iListFocus = dataCenter.getImage("06_list_foc2.png");
            iListFocusDim = dataCenter.getImage("06_list_foc2_dim.png");
            iListLine = dataCenter.getImage("06_listline.png");
            break;
        }
        super.prepare(c);
    }

    public void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        ListOptionPanel o = (ListOptionPanel) c;
        int listFocus = o.listFocus;
        int listSize = o.listSize;
        int row = o.rowSize;
        int offset = 9 - row;
        int ty = offset * LIST_Y_GAP;
        int focus = o.getFocus();

        g.drawImage(iBg, 0, ty + 40, c);

        String s = dataCenter.getString(o.titleKey);
        g.setFont(fTitle);
        g.setColor(cTitleShadow);
        g.drawString(s, 68, ty + 66);
        g.setColor(cTitle);
        g.drawString(s, 67, ty + 65);

        if (o.header != null) {
            g.setFont(fHeader);
            g.setColor(cHeader);
            GraphicUtil.drawStringRight(g, dataCenter.getString(o.header), 595, 65);
        }

        ty = offset * LIST_Y_GAP + 111;
        for (int i = 0; i < row - 1; i++) {
            g.drawImage(iListLine, 68, ty, o);
            ty = ty + LIST_Y_GAP;
        }

        ty = offset * LIST_Y_GAP + 78;
        int firstIndex;
        int over = listSize - row;
        int move = listFocus - o.middle;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }
        if (listFocus >= 0) {
            Image im = focus < 0 ? iListFocus : iListFocusDim;
            g.drawImage(im, 53, ty + (listFocus - firstIndex) * LIST_Y_GAP, c);
        }

        g.translate(0, ty);
        int size = Math.min(row, listSize);
        int index = firstIndex;
        for (int i = 0; i < size; i++) {
            paintList(g, o, index, index == listFocus);
            index++;
            g.translate(0, LIST_Y_GAP);
        }
        g.translate(0, -LIST_Y_GAP * size - ty);

        if (firstIndex > 0) {
            g.drawImage(iShT, 54, 76 + offset * LIST_Y_GAP, o);
            g.drawImage(iArrowUp, 321, 69 + offset * LIST_Y_GAP, o);
        }

        if (firstIndex + row < listSize) {
            g.drawImage(iShB, 54, 310, o);
            g.drawImage(iArrowDown, 321, 361, o);
        }
    }


}

package com.alticast.illico.epg.ppv;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.util.Util;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;

public class ChannelAssetPageRenderer extends FullScreenPanelRenderer {

    Image iBg1 = dataCenter.getImage("ppv_bg01.jpg");
    Image iBg2 = dataCenter.getImage("ppv_bg02.jpg");
    Image iEnglish = dataCenter.getImage("icon_en.png");
    Image iFrench = dataCenter.getImage("icon_fr.png");
    Image iCC = dataCenter.getImage("icon_cc.png");
    Image iHd = dataCenter.getImage("icon_hd.png");
    //->Kenneth[2015.2.4] 4K
    Image iUhd = dataCenter.getImage("icon_uhd.png");
    Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };

    Image i_02_detail_bt_foc = dataCenter.getImage("02_detail_bt_foc.png");
    Image i_acbg_up = dataCenter.getImage("acbg_up.png");
    Image i_02_detail_ar = dataCenter.getImage("02_detail_ar.png");
    Image i_ppv_barbg = dataCenter.getImage("ppv_barbg.png");
    Image i_Group_368 = dataCenter.getImage("Group_368.png");
    Image i_ppvbar_sep = dataCenter.getImage("ppvbar_sep.png");
    Image i_icon_or_ppv = dataCenter.getImage("icon_or_ppv.png");

    Image iBlocked  = dataCenter.getImage("02_icon_con.png");
    Image iBlockedDim  = dataCenter.getImage("02_icon_con_dim.png");

    public Image iBottomSha = dataCenter.getImage("ppv_txtsha.png");
    public Image iTopSha  = dataCenter.getImage("ppv_txtsha_top.png");
    public Image iTopSha2 = dataCenter.getImage("ppv_txtsha_top2.png");

    static int[] LINE_Y1 = { 208, 286 };
    static int[] LINE_Y2 = { 295, 358 };

    static Color cChNum = new Color(253, 253, 253);
    static Font fChNum = FontResource.BLENDER.getFont(31);

    static Color cNow1 = new Color(186, 151, 7);
    static Color cNowTop = new Color(124, 124, 124);
    static Font fNow = FontResource.BLENDER.getFont(18);

    static Color cTitleTop = new Color(124, 124, 124);
    static Color cTitle = new Color(255, 203, 0);
    static Font fTitle = FontResource.BLENDER.getFont(29);
    static FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

    static Color cTime = new Color(200, 200, 200);
    static Font fTime = FontResource.BLENDER.getFont(19);

    static Color cDur = new Color(161, 160, 160);
    static Font fDur = FontResource.BLENDER.getFont(18);
    static FontMetrics fmDur = FontResource.getFontMetrics(fDur);

    static Color cMsg = new Color(252, 202, 4);
    static Font fMsg = FontResource.BLENDER.getFont(18);
    static Font fMsgCountDown = FontResource.BLENDER.getFont(17);

    static Color cCountDown = Color.white;
    static Font fCountDown = FontResource.BLENDER.getFont(30);

    static Color cToOrder = new Color(110, 110, 110);
    static Font fToOrder = FontResource.BLENDER.getFont(18);

    static Color cAlreadyBegunTop = new Color(124, 124, 124);
    static Color cAlreadyBegun = new Color(161, 160, 160);
    static Font fAlreadyBegun = FontResource.BLENDER.getFont(18);

    static Color cFutureOrder = new Color(161, 133, 17);
    static Font fFutureOrder = FontResource.BLENDER.getFont(18);

    ChannelAssetPage page;
    OptionPanelRenderer opRenderer = new OptionPanelRenderer();

    public ChannelAssetPageRenderer() {
    }

    public void prepare(UIComponent c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelAssetPageRenderer.prepare()");
        page = (ChannelAssetPage) c;
        super.prepare(c);
    }

    public Rectangle getButtonFocusBounds(int focus) {
        if (focus == -1) {
            return new Rectangle(626, 213, 284, 41);
        } else {
            Rectangle r = opRenderer.getButtonFocusBounds(page.root, focus);
            r.y = r.y + 84;
            return r;
        }
    }

    protected void paintBackground(Graphics g, UIComponent c) {
        if (page.top == null) {
            g.drawImage(iBg1, 0, 0, c);
        } else {
            g.drawImage(iBg2, 0, 0, c);
        }
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        Formatter f = Formatter.getCurrent();
        String s;

        PpvProgram top = page.top;
        PpvProgram bottom = page.bottom;
        PpvProgram current = page.current;
        Channel channel = page.channel;
        //->Kenneth[2015.8.31] VDTRMASTER-5566의 원인 찾다가
        // top, bottom, current, channel 이 모두 처음에는 null 이다가 이후에 null 아니게 되는 경우 있음 발견.
        // 따라서 일일이 null 체크하는 코드를 paint 에 전체적으로 추가한다.
        // 5566 의 원인은 아니었음.
        //<-
        Image im = page.getBannerImage();
        if (im != null) {
            g.drawImage(im, 0, 101, c);
        }
        g.setColor(cChNum);
        g.setFont(fChNum);
        if (channel != null) g.drawString(String.valueOf(channel.getNumber()), 54, 137);
        if (channel != null) g.drawImage(channel.getLogo(), 110, 112, c);

        g.translate(0, 84);
        opRenderer.paintButtons(g, c, page.root);
        g.translate(0, -84);

        if (page.hasFutureOrder) {
            g.setColor(cFutureOrder);
            g.setFont(fFutureOrder);
            GraphicUtil.drawStringRight(g, dataCenter.getString("ppv.msg_already_ordered"), 910, 91);
        }

        int[] lineY;
        if (top == null) {
            lineY = LINE_Y1;
            g.setFont(fNow);
            g.setColor(cNow1);
        } else {
            lineY = LINE_Y2;

            boolean topBlock = ParentalControl.getLevel(top) != ParentalControl.OK;
            s = dataCenter.getString(topBlock ? "ppv.unblock_content" : "ppv.see_other_showtimes");
            g.drawImage(i_acbg_up, 581, 153, c);
            if (c.getFocus() == -1) {
                g.drawImage(i_02_detail_bt_foc, 626, 197, c);
                g.setFont(OptionPanelRenderer.fFocusedButton);
                g.setColor(OptionPanelRenderer.cFocusedButton);
                g.drawString(s, 673, 224);
            } else {
                g.drawImage(i_02_detail_ar, 651, 212, c);
                g.setFont(OptionPanelRenderer.fButton);
                g.setColor(OptionPanelRenderer.cButton);
                g.drawString(s, 673, 224);
            }

            if (!page.currentOrderable) {
                g.drawImage(i_icon_or_ppv, 155, 235, c);
                g.setColor(cAlreadyBegunTop);
                g.setFont(fAlreadyBegun);
                g.drawString(dataCenter.getString("ppv.msg_no_longer"), 182, 251);
            }

            g.setColor(cTitleTop);
            g.setFont(fTitle);
            int ix = 154;
            if (ParentalControl.getLevel(top) != ParentalControl.OK) {
                g.drawImage(iBlockedDim, ix, 224-19, c);
                ix += 27;
            }
            s = TextUtil.shorten(ParentalControl.getTitle(top), fmTitle, 460 + 154 - ix);
            g.drawString(s, ix, 224);

            g.setColor(cNowTop);
            g.setFont(fNow);
            g.drawString(dataCenter.getString("ppv.now_playing_1"), 55, 220);
            g.drawString(dataCenter.getString("ppv.now_playing_2"), 55, 240);
            if (Log.EXTRA_ON) {
                g.setColor(Color.green);
                if (top.buyWindowStartTime > 0) {
                    s = "eid = " + top.eid + ", buy_window : " + f.getTime(top.buyWindowStartTime)
                        + " ~ " + f.getTime(top.buyWindowEndTime)
                        + ", " + f.getTime(top.cancelWindowEndTime);
                } else {
                    s = "eid = " + top.eid + ", buy_window = 0";
                }
                g.drawString(s, 175, 200);
                g.drawString(ChannelController.get(0).toString(), 55, 75);
            }
//            if (type == ChannelAssetPage.ALREADY_BEGUN) {
//                g.translate(0, 23);
//            }

            g.setColor(cNow1);
        }
        PpvProgram p = bottom;

        int y = lineY[0];
        s = bottom == current ? "ppv.now_playing" : "ppv.next_program";
        g.drawString(dataCenter.getString(s + "_1"), 55, y);
        g.drawString(dataCenter.getString(s + "_2"), 55, y + 20);

        if (Log.EXTRA_ON) {
            g.setFont(fMsgCountDown);
            g.setColor(Color.green);
            if (p != null) {
                if (p.buyWindowStartTime > 0) {
                    s = "eid = " + p.eid + ", buy_window = " + f.getTime(p.buyWindowStartTime)
                        + " ~ " + f.getTime(p.buyWindowEndTime)
                        + ", " + f.getTime(p.cancelWindowEndTime);
                } else {
                    s = "eid = " + p.eid + ", buy_window = 0";
                }
            }
            g.drawString(s, 175, y-20);
            g.drawString("next update = " + f.getTime(page.nextUpdateTime), 770, 78);
            g.drawString("package permission = " + PpvController.hasPackageAuth, 730, 98);
        }

        if (p != null) {
            g.setColor(cTitle);
            g.setFont(fTitle);
            int x = 154;
            if (ParentalControl.getLevel(p) != ParentalControl.OK) {
                g.drawImage(iBlocked, x, y + 3 - 19, c);
                x += 27;
            }
            s = TextUtil.shorten(ParentalControl.getTitle(p), fmTitle, 460 + 154 - x);
            g.drawString(s, x, y + 3);

            y = lineY[1];
            x = 154;
            g.setColor(cTime);
            g.setFont(fTime);
            if (p == current) {
                s = f.getTime(p.startTime) + " - " + f.getTime(p.endTime);
            } else {
                s = f.getLongDayText(p.startTime) + ", " + f.getTime(p.startTime);
            }
            s += "  |  " + f.getPrice(p.price) + "  |  " + dataCenter.getString("ppv.available_until") + " : " + f.getTime(p.buyWindowEndTime);
            g.drawString(s, x, y);
            y += 25;

            g.setColor(cDur);
            g.setFont(fDur);
            s = f.getDurationText(p.getDuration()) + "  |  " + Genre.getName(p.getConvertedCategory()) + "  |  ";
            g.drawString(s, x, y);
            x += fmDur.stringWidth(s);

            //->Kenneth : 4K by June
            //->Kenneth[2015.2.4] 4K
            if (channel != null) {
                int def  = Util.getDefinition(channel, p);
                if (def == Constants.DEFINITION_HD) {  // TODO - 4K
                    im = iHd;
                    g.drawImage(im, x, y - 12, c);
                    x += im.getWidth(c) + 1;
                } else if (def == Constants.DEFINITION_4K) {  // TODO - 4K
                    im = iUhd;
                    g.drawImage(im, x, y - 12, c);
                    x += im.getWidth(c) + 1;
                }
            }
            im = ratingImages[p.getRatingIndex()];
            if (im != null) {
                g.drawImage(im, x, y - 12, c);
                x += im.getWidth(c) + 1;
            }
            if (Constants.LANGUAGE_ENGLISH.equals(p.getLanguage())) {
                im = iEnglish;
                g.drawImage(im, x, y-12, c);
                x += im.getWidth(c) + 1;
            } else if (Constants.LANGUAGE_FRENCH.equals(p.getLanguage())) {
                im = iFrench;
                g.drawImage(im, x, y-12, c);
                x += im.getWidth(c) + 1;
            }
            if (p.isCaptioned()) {
                g.drawImage(iCC, x, y-12, c);
            }
        }

        if (page.countDown) {
            y = 279;
            g.drawImage(i_ppv_barbg, 580, y, c);
            g.drawImage(i_ppvbar_sep, 715, y + 12, c);
            g.setColor(cMsg);
            g.setFont(fMsgCountDown);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("ppv.msg_ordered_1"), 655, y+24);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("ppv.msg_ordered_2"), 655, y+41);
            g.setFont(fMsg);
            g.setColor(cDur);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("ppv.starts_in"), 761, y+33);

            g.setFont(fCountDown);
            g.setColor(cCountDown);
            if (p != null) {
                long dur = Math.max(0, p.startTime - System.currentTimeMillis());
                g.drawChars(getDuration(dur), 0, 8, 811, y+37);
            }
        } else {
            String key = null;
            if (bottom == current) {
                if (!page.currentOrderable) {
                    key = "ppv.msg_no_longer";
                }
            } else {
                if (!page.nextOrderable) {
                    key = "ppv.msg_too_early";
                }
            }
            if (key != null) {
                y = lineY[0] + 29;
                g.drawImage(i_icon_or_ppv, 155, y - 16, c);
                g.setColor(cAlreadyBegun);
                g.setFont(fAlreadyBegun);
                g.drawString(dataCenter.getString(key), 182, y);
            }
        }

        g.setFont(fToOrder);
        g.setColor(cToOrder);
        g.drawString(dataCenter.getString("to_order"), 55, 503);
    }

    private char[] countDown = new char[8];
    {
        countDown[2] = ':';
        countDown[5] = ':';
    }

    /** Returns "01:23:45". */
    private char[] getDuration(long duration) {
        StringBuffer sb = new StringBuffer(10);
        int sec = (int) (duration / Constants.MS_PER_SECOND);
        int min = sec / 60;
        sec = sec % 60;
        int hour = min / 60;
        min = min % 60;

        countDown[0] = (char) (hour / 10 + '0');
        countDown[1] = (char) (hour % 10 + '0');

        countDown[3] = (char) (min / 10 + '0');
        countDown[4] = (char) (min % 10 + '0');

        countDown[6] = (char) (sec / 10 + '0');
        countDown[7] = (char) (sec % 10 + '0');

        return countDown;
    }


}

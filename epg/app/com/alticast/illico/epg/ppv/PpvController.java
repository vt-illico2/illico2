package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.search.*;
import com.videotron.tvi.illico.ixc.loadinganimation.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.alticast.ui.*;
import com.opencable.handler.cahandler.*;
import org.dvb.event.*;
import org.ocap.service.*;
import java.io.*;
import java.util.*;
import java.awt.Point;
import java.awt.event.KeyEvent;
import javax.tv.service.selection.*;
import javax.tv.service.Service;

public class PpvController extends ChannelBackgroundPanel implements CAAuthorizationListener {

    private static final boolean NEED_ORDER_CONFIRM_POPUP = false;

    private static final long EID_PREFIX = CaManager.EID_PREFIX;

    public static final int CHANNEL_HOW_TO_ORDER   = 0;
    public static final int CHANNEL_TRAILER        = 1;
    public static final int CHANNEL_NEW_THIS_MONTH = 2;

    private static final String[] CHANNEL_KEY_SUFFIX = {
        "_HOW_TO_ORDER_CHANNEL",
        "_TRAILER_CHANNEL",
        "_NEW_THIS_MONTH_CHANNEL",
    };

    FullScreenPanel over;
    ChannelAssetPage current;
    ChannelAssetPage page;

    Channel currentChannel; // sc event에서 처리할 것
    boolean subscribed;
    CAHandler handler;

    Program currentProgram;

    public CAAuthorization monitoring = null;

    public static boolean hasPackageAuth = true;

    private static PpvController instance = new PpvController();

    public static PpvController getInstance() {
        return instance;
    }

    private PpvController() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);

        page = new ChannelAssetPage();

        DataCenter dataCenter = DataCenter.getInstance();
        String phone = dataCenter.getString("PPV_PHONE_NUMBER");
        dataCenter.put("to_order_en", TextUtil.replace(dataCenter.getString("ppv.to_order_en"), "%%", phone));
        dataCenter.put("to_order_fr", TextUtil.replace(dataCenter.getString("ppv.to_order_fr"), "%%", phone));
        dataCenter.put("to_cancel_en", TextUtil.replace(dataCenter.getString("ppv.to_cancel_en"), "%%", phone));
        dataCenter.put("to_cancel_fr", TextUtil.replace(dataCenter.getString("ppv.to_cancel_fr"), "%%", phone));

        PpvHistoryManager.getInstance();
    }

    public void init(CAHandler handler) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.init("+handler+")");
        this.handler = handler;
        PpvHistoryManager.getInstance().init();
    }

    private boolean resetPackageAuth() {
        MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        try {
            int ret = ms.getIPPVDeactivationPackageAuthority();
            // Note: AUTHORIZED => deactivate, NOT_AUTHORIZED => active
            hasPackageAuth = (ret != MonitorService.AUTHORIZED);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.resetPackageAuth() returns "+hasPackageAuth);
        return hasPackageAuth;
    }

    private synchronized boolean startAssetPage(Program p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.startAssetPage("+p+")");
        if (current != null && p.equals(page.program) && currentChannel != null && currentChannel.equals(page.channel)) {
            Log.printDebug("PpvController.startAssetPage: same page");
            return true;
        }
        if (!EpgCore.getInstance().checkPermission(App.NAME)) {
            EpgCore.getInstance().showAppErrorMessage("MOT501", null, App.NAME);
            return false;
        }

        resetPackageAuth();
        if (page.start(currentChannel, p)) {
            this.add(page);
            this.current = page;
            this.repaint();
            return true;
        } else {
            return false;
        }
    }

    public void showOtherShowTimes(final PpvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showOtherShowTimes("+p+")");
        Thread t = new Thread("showOtherShowTimes") {
            public void run() {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showOtherShowTimes().t.run()");
                showLoading();
                Vector list = findOtherShowTimes(p);
                showOtherShowTimesImpl(p, list);
                hideLoading();
            }
        };
        t.start();
    }

    private synchronized void showOtherShowTimesImpl(PpvProgram p, Vector list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showOtherShowTimesImpl("+p+")");
        OtherShowTimesPage ots = new OtherShowTimesPage();
        ots.start(p, list);
        this.add(ots, 0);
        if (over != null) {
            this.remove(over);
        }
        this.over = ots;
        this.repaint();
    }

    public synchronized void showCart() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showCart() : current = "+current);
        CartPage cp = new CartPage();
        this.add(cp, 0);
        if (over != null) {
            this.remove(over);
        }
        this.over = cp;
        cp.start(PpvHistoryManager.getInstance().getCurrentHistory());
        this.repaint();
    }

    public synchronized void closePanel(FullScreenPanel p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.closePanel("+p+")");
        if (p != null && over == p) {
            this.over = null;
            p.stop();
            this.remove(p);
            this.repaint();
        }
    }

    synchronized void hideAssetPage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.hideAssetPage()");
        page.stop();
        this.remove(page);
        this.current = null;
        this.repaint();
    }

    public boolean isRunning() {
        return current != null || over != null;
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.stop()");
        closePanel(over);
        hideAssetPage();
        this.currentChannel = null;
        this.currentProgram = null;
        setMonitoring(null);
    }

    public synchronized void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.pause()");
        if (current != null) {
            current.pause();
        }
    }

    /** Too late to order. */
    public boolean checkOrderableTime(PpvProgram p) {
        if (System.currentTimeMillis() < p.buyWindowEndTime) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.checkOrderableTime() returns true");
            return true;
        }
        PpvPopupRenderer r = new PpvPopupRenderer(p, "ppv.too_late_order_msg", true);
        PopupController.getInstance().showCustomPopup(
                DataCenter.getInstance().getString("ppv.too_late_order_title"),
                null, r, null);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.checkOrderableTime() returns false");
        return false;
    }

    public void tuneToChannel(int infoType) {
        tuneToChannel(page.bottom, infoType);
    }

    public void tuneToChannel(PpvProgram p, int infoType) {
        String indicator;
        if (p != null) {
            indicator = p.indicator;
        } else {
            indicator = Util.getPpvIndicator(currentChannel);
        }
        tuneToChannel(indicator, infoType);
    }

    private void tuneToChannel(String indicator, int infoType) {
        if (indicator == null) {
            return;
        }
        int number = DataCenter.getInstance().getInt(indicator + CHANNEL_KEY_SUFFIX[infoType]);
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"PpvController.tuneToChannel = " + number
                        + ", " + indicator + CHANNEL_KEY_SUFFIX[infoType]);
        }
        ChannelController.get(0).changeChannelByNumber(number);
    }

    // 구매취소 step 1
    public void showCancelPopup(final Object o, final PpvListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showCancelPopup()");
        PpvPopupRenderer r;
        boolean isCancelable;
        if (o instanceof PpvProgram) {
            r = new PpvPopupRenderer((PpvProgram) o, null, true);
            isCancelable = isCancelableTime((PpvProgram) o);
        } else if (o instanceof PpvHistory) {
            r = new PpvPopupRenderer((PpvHistory) o, null, true);
            isCancelable = isCancelableTime((PpvHistory) o);
        } else {
            return;
        }
        if (!isCancelable) {
            r.setMessage(DataCenter.getInstance().getString("ppv.too_late_cancel_msg"));
            PopupController.getInstance().showCustomPopup(
                    DataCenter.getInstance().getString("ppv.too_late_cancel_title"),
                    null, r, null);
            return;
        }

        r.setTopMessage(DataCenter.getInstance().getString("ppv.cancel_order_msg"));
        final String[] buttons = new String[] { Popup.BUTTON_YES, Popup.BUTTON_NO };

        PopupListener popupListener = new PopupListener() {
            public void notifySelected(Popup p, String key) {
                if (buttons[0].equals(key)) {
                    showCancelPinCode(o, l);
                }
            }
            public void notifyCanceled(Popup p, boolean byTimer) {
            }
        };

        PopupController.getInstance().showCustomPopup(
                DataCenter.getInstance().getString("ppv.cancel_order_popup_title"),
                buttons, r, popupListener);
    }

    // 구매취소 step 2 - PIN 입력
    private void showCancelPinCode(final Object o, final PpvListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showCancelPinCode()");
        RightFilterListener rfl = new RightFilterListener() {
            String[] explain = new String[] {
                DataCenter.getInstance().getString("ppv.pin_cancel_msg1"),
                DataCenter.getInstance().getString("ppv.pin_cancel_msg2")
            };
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    int ret = cancelImpl(o);
                    if (ret == CAHandler.IPPV_CANCEL_SUCCESS) {
                        showCancelFinishedPopup(o, l, ret);
                    } else {
                        if (l != null) {
                            l.notifyCancelResult(ret);
                        }
                    }
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.ALLOW_PURCHASE }, null, Definitions.RATING_G);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
        try {
            rfl.receiveCheckRightFilter(PreferenceService.RESPONSE_SUCCESS);
        } catch (Exception ex) {
        }
    }

    // 구매취소 step3 - 확인 팝업
    private void showCancelFinishedPopup(final Object o, final PpvListener l, final int ret) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showCancelFinishedPopup()");
        PpvPopupRenderer r;
        if (o instanceof PpvProgram) {
            r = new PpvPopupRenderer((PpvProgram) o, "ppv.cancel_finished_msg_1", true);
        } else if (o instanceof PpvHistory) {
            r = new PpvPopupRenderer((PpvHistory) o, "ppv.cancel_finished_msg_1", true);
        } else {
            return;
        }
        r.setTopMessage(DataCenter.getInstance().getString("ppv.cancel_finished_msg_0"));
        PopupListener popupListener = new PopupListener() {
            public void notifySelected(Popup p, String key) {
            }
            public void notifyCanceled(Popup p, boolean byTimer) {
                if (l != null) {
                    l.notifyCancelResult(ret);
                }
            }
        };
        PopupController.getInstance().showCustomPopup(
                DataCenter.getInstance().getString("ppv.cancel_finished_popup_title"),
                null, r, popupListener);
    }

    // 실제 취소 수행
    private int cancelImpl(Object o) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.cancelImpl()");
        long eid;
        PpvHistory history;
        boolean purchased;
        if (o instanceof PpvHistory) {
            history = (PpvHistory) o;
            eid = history.eid;
            purchased = PpvHistoryManager.getInstance().updatePurchasedHistory(history);
        } else if (o instanceof PpvProgram) {
            PpvProgram p = (PpvProgram) o;
            purchased = isPurchased(p);
            history = PpvHistory.create(p, true);
            eid = p.eid;
        } else {
            return -1;
        }

        int ret = -1;
        try {
            long id = EID_PREFIX | eid;
            ret = handler.cancelIppvEvent(id);
            Log.printInfo("PpvController: cancel: eid = " + Long.toHexString(id) + ", return = " + ret + ", purchased = " + purchased);
            if (ret == CAHandler.IPPV_CANCEL_SUCCESS || !purchased) {
                ret = CAHandler.IPPV_CANCEL_SUCCESS;
                PpvHistoryManager.getInstance().removeFromHistory(history);
                ReminderManager.getInstance().remove(history);
                RecordingStatus.getInstance().cancelPpv(history);
                // 아직 시작 안함
                // TODO - order confirm popup - timer
                updateAssetPage();
                return ret;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        showErrorMessage(ret);
//        Hashtable dynamic = new Hashtable();
//        dynamic.put("<ERROR_CODE>", errorCodeToString(ret));
//        EpgCore.getInstance().showErrorMessage("PPV603", null, dynamic);
        return ret;
    }

    // 구매 step 1
    public void showPurchasePopup(final PpvProgram ppv, final PpvListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showPurchasePopup()");
        resetPackageAuth();
        if (!hasPackageAuth) {
            EpgCore.getInstance().showAppErrorMessage("MOT501", null, "PPV");
            return;
        }
        // TODO - 늦은거
        if (!checkOrderableTime(ppv)) {
            return;
        }

        if (!NEED_ORDER_CONFIRM_POPUP) {
            showPurchasePinCode(ppv, l);
            return;
        }

        PpvPopupRenderer r = new PpvPopupRenderer(ppv, "ppv.purchase_popup_msg", true);
        final String[] buttons = new String[] { Popup.BUTTON_YES, Popup.BUTTON_NO };

        PopupListener popupListener = new PopupListener() {
            public void notifySelected(Popup p, String key) {
                if (buttons[0].equals(key)) {
                    showPurchasePinCode(ppv, l);
                }
            }
            public void notifyCanceled(Popup p, boolean byTimer) {
            }
        };

        PopupController.getInstance().showCustomPopup(
                DataCenter.getInstance().getString("ppv.purchase_popup_title"),
                buttons, r, popupListener);
    }

    // 구매 step 2
    private void showPurchasePinCode(final PpvProgram p, final PpvListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showPurchasePinCode()");
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        if (ps == null) {
            Log.printError("PpvController.purchase: not found PreferenceService.");
            // TODO - error
            return;
        }
        RightFilterListener rfl = new RightFilterListener() {
            String[] explain = new String[] {
                DataCenter.getInstance().getString("ppv.pin_msg1"),
                DataCenter.getInstance().getString("ppv.pin_msg2")
            };
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    if (!checkOrderableTime(p)) {
                        return;
                    }
                    int ret = purchaseImpl(p);
                    if (ret == CAHandler.IPPV_SUCCESS) {
                        showPurchaseFinishedPopup(p, l, ret);
                    } else {
                        if (l != null) {
                            l.notifyPurchaseResult(ret);
                        }
                    }
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.ALLOW_PURCHASE }, null, Definitions.RATING_G);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void showPurchaseFinishedPopup(final PpvProgram p, final PpvListener l, final int ret) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showPurchaseFinishedPopup()");
        PpvPopupRenderer r;
        String titleKey = "ppv.purchase_popup_title";
        if (p.isOnAir()) {
            int min = (int) Math.round(((double) (System.currentTimeMillis() - p.startTime)) / Constants.MS_PER_MINUTE);
            String s = String.valueOf(Math.max(1, min));
            s = TextUtil.replace(DataCenter.getInstance().getString("ppv.purchase_finished_msg_1"),
                             TextUtil.TEXT_REPLACE_MARKER, s);
            r = new PpvPopupRenderer(p, null, true);
            r.setMessage(s);
        } else {
            r = new PpvPopupRenderer(p, "ppv.purchase_finished_msg_2", true);
        }
        PopupListener popupListener = new PopupListener() {
            public void notifySelected(Popup p, String key) {
            }
            public void notifyCanceled(Popup p, boolean byTimer) {
                if (l != null) {
                    l.notifyPurchaseResult(ret);
                }
            }
        };
        PopupController.getInstance().showCustomPopup(
                DataCenter.getInstance().getString(titleKey),
                null, r, popupListener);
    }

    private int purchaseImpl(PpvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.purchaseImpl("+p+")");
        int ret = -1;
        try {
            boolean inBuyWindow = p.buyWindowStartTime <= System.currentTimeMillis();
            if (inBuyWindow) {
                ret = requestPurchase(p.eid);
            } else {
                // in marketing
                Log.printInfo("PpvController.purchase: case pre-purcase");
                ret = CAHandler.IPPV_SUCCESS;
            }
            if (ret == CAHandler.IPPV_SUCCESS) {
                PpvHistoryManager.getInstance().addToHistory(p, inBuyWindow);
                ReminderManager.getInstance().remindOrdered(p, null);
                EpgVbmController.getInstance().writePpvOrder(p);
                long cur = System.currentTimeMillis();
                if (cur < p.startTime) {
                    // 아직 시작 안함
                    // TODO - order confirm popup - timer
                    synchronized (this) {
                        if (current != null) {
                            if (current != page) {
                                closePanel(current);
                            }
                            updateAssetPage();
                        }
                    }
                } else {
                    // 이미 시작
                    // TODO - order confirm popup - timer
                    hideAssetPage();
                }
                return ret;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        showErrorMessage(ret);
        return ret;
    }

    /** 미리구매 -> 구매 */
    public boolean purchase(PpvHistory h) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.purchase("+h+")");
        int ret = requestPurchase(h.eid);
        if (ret == CAHandler.IPPV_SUCCESS) {
            EpgVbmController.getInstance().writePpvOrder(h);
            return true;
        } else {
            showErrorMessage(ret);
            return false;
        }
    }

    private void showErrorMessage(int retCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showErrorMessage("+retCode+")");
        Hashtable dynamic = new Hashtable();
        dynamic.put("<ERROR_CODE>", errorCodeToString(retCode));
//        String code = retCode == CAHandler.IPPV_MAX_EVENTS ? "PPV601" : "PPV604";
        String code = "PPV" + retCode;
        EpgCore.getInstance().showErrorMessage(code, null, dynamic);
        Log.printError(code + " : " + errorCodeToString(retCode));
    }

    private int requestPurchase(long eid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.requestPurchase("+eid+")");
        long id = EID_PREFIX | eid;
        int ret = -1;
        try {
            ret = handler.purchaseIppvEvent(id, null);
        } catch (Exception ex) {
            Log.print(ex);
            if (App.EMULATOR) {
                ret = CAHandler.IPPV_SUCCESS;
            }
        }
        Log.printInfo("PpvController.purchase: eid = " + Long.toHexString(id) + ", return = " + ret);
        return ret;
    }

    private static String errorCodeToString(int errorCode) {
        switch (errorCode) {
            case CAHandler.IPPV_ALREADY_AUTHORIZED:
                return "IPPV_ALREADY_AUTHORIZED";
            case CAHandler.IPPV_CREDIT_FAILURE:
                return "IPPV_CREDIT_FAILURE";
            case CAHandler.IPPV_IMPULSE_DISABLED:
                return "IPPV_IMPULSE_DISABLED";
            case CAHandler.IPPV_MAX_EVENTS:
                return "IPPV_MAX_EVENTS";
            case CAHandler.IPPV_NO_PERSISTENT_MEMORY:
                return "IPPV_NO_PERSISTENT_MEMORY";
            case CAHandler.IPPV_NOMEM:
                return "IPPV_NOMEM";
            case CAHandler.IPPV_OUTSIDE_PURCHASE_WINDOW:
                return "IPPV_OUTSIDE_PURCHASE_WINDOW";
            case CAHandler.IPPV_PIN_FAILED:
                return "IPPV_PIN_FAILED";
            case CAHandler.IPPV_PURCHASE_PENDING:
                return "IPPV_PURCHASE_PENDING";
            case CAHandler.IPPV_PURCHASE_UNAVAILABLE:
                return "IPPV_PURCHASE_UNAVAILABLE";
            case CAHandler.IPPV_BAD_EID:
                return "IPPV_BAD_EID";
            case CAHandler.IPPV_SUCCESS:
                return "IPPV_SUCCESS";
            case CAHandler.IPPV_CANCEL_BAD_EID:
                return "IPPV_CANCEL_BAD_EID";
            case CAHandler.IPPV_CANCEL_FAILED:
                return "IPPV_CANCEL_FAILED";
            case CAHandler.IPPV_CANCEL_SUCCESS:
                return "IPPV_CANCEL_SUCCESS";
            default:
                return String.valueOf(errorCode);
        }
    }

    public static boolean isOrderableTime(PpvProgram p) {
        long cur = System.currentTimeMillis();
//        return p.buyWindowStartTime <= cur && cur < p.buyWindowEndTime;
        return p.eid != 0 && p.marketingWindowStartTime <= cur && cur < p.buyWindowEndTime;
    }

    public static boolean isCancelableTime(PpvProgram p) {
        return System.currentTimeMillis() < p.cancelWindowEndTime;
    }

    public static boolean isCancelableTime(PpvHistory h) {
        return System.currentTimeMillis() < h.cancelableTime;
    }

    public boolean isPurchased(PpvProgram p) {
        return isPurchased(p.getEpgChannel(), p);
    }

    public boolean isPurchased(PpvHistory h) {
        try {
            Channel ch = ChannelDatabase.current.getChannelByName(h.channelName);
            if (ch != null) {
                return checkPurchased(ch.getSourceId(), h.eid);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }

    public boolean isPurchased(Channel ch, PpvProgram p) {
        if (ch != null) {
            return checkPurchased(ch.getSourceId(), p.eid);
        } else {
            return false;
        }
    }

    public CAAuthorization checkPurchased(Channel ch, PpvProgram p, CAAuthorizationListener l) {
        if (ch == null) {
            return null;
        }
        short sid = (short) ch.getSourceId();
        long id = EID_PREFIX | p.eid;
        try {
            CAAuthorization auth = handler.getIppvAuthorization(sid, id, l);
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"PpvController: isPurchased             = " + auth.isIppvEventPurchased());
                Log.printDebug(App.LOG_HEADER+"PpvController: isAuthorized            = " + auth.isAuthorized());
                Log.printDebug(App.LOG_HEADER+"PpvController: isFreePreviewable       = " + auth.isFreePreviewable());
                Log.printDebug(App.LOG_HEADER+"PpvController: isSubscriptionPurchased = " + auth.isSubscriptionPurchased());
            }
            return auth;
        } catch (Exception ex) {
        }
        return null;
    }

    public boolean isPurchasedOrReserved(PpvProgram p) {
        return isPurchased(p) || PpvHistoryManager.getInstance().contains(p);
    }

    private boolean checkPurchased(int sid, long eid) {
        if (eid == 0) {
            Log.printDebug("PpvController.checkPurchased: eid is zero !");
            return false;
        }
        try {
            long id = EID_PREFIX | eid;
            CAAuthorization auth = handler.getIppvAuthorization((short) sid, id, null);
            boolean ret = auth.isIppvEventPurchased();
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"PpvController: isPurchased             = " + ret);
                Log.printDebug(App.LOG_HEADER+"PpvController: isAuthorized            = " + auth.isAuthorized());
                Log.printDebug(App.LOG_HEADER+"PpvController: isFreePreviewable       = " + auth.isFreePreviewable());
                Log.printDebug(App.LOG_HEADER+"PpvController: isSubscriptionPurchased = " + auth.isSubscriptionPurchased());
            }
            return ret;
        } catch (Exception ex) {
            Log.print(ex);
            Log.printError("PPV602 : " + ex);
            EpgCore.getInstance().showErrorMessage("PPV602", null);
        }
        return false;
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.handleKey("+code+")");
        switch (code) {
            case KeyCodes.SEARCH:
                //->Kenneth[2015.8.13] VDTRMASTER-5580
                // PLATFORM_PPV 로 Search 검색후 Live 결과 이동시 GridEpg.SearchActionAdapter.actionRequested 로 
                // PPV 상세를 띄우려 하고 이 경우 GridEpg 는 이미 stop 된 상태라서 channelList 는 null인 상태라
                // NullPointerException 이 발생. PLATFORM_MONITOR 로 넣어주면 startUnboundApplication 통해서
                // 뜨기 때문에 channelList 가 초기화 되어서 null 이 아니게 되어서 정상 동작.
                // PpvController 에서 Search 키를 처리 안하도록 해 봐도 monitor 에서 channel type 찾아내서
                // PLATFORM_PPV 로 넣어주기 때문에 소용 없음.
                // Grid 가 뜬 상태에서 Search 가는 경우 등이나 PLATFORM_EPG 등으로 쓰는 것이 유효하고
                // PPV 채널로 이동한 상태에서는 아래처럼 PLATFORM_MONITOR 로 처리해 주기로 함.
                EpgCore.getInstance().launchSearch(SearchService.PLATFORM_MONITOR, true);
                //EpgCore.getInstance().launchSearch(SearchService.PLATFORM_PPV, true);
                //<-
                return true;
            //->Kenneth[2015.8.19] DDC-113
            case KeyCodes.STAR:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.handleKey() : STAR : return true");
                return true;
            //<-
            case KeyCodes.FAST_FWD:
            case KeyCodes.REWIND:
            case KeyCodes.PLAY:
            case KeyCodes.PAUSE:
            case KeyCodes.LIVE:
            case KeyCodes.STOP:
            case KeyCodes.RECORD:
            case KeyCodes.FORWARD:
                if (current != null) {
                    // skip video keys in PPV asset page.
                    return true;
                } else {
                    return false;
                }
        }
        if (over != null) {
            return over.handleKey(code);
        }
        if (current != null) {
            return current.handleKey(code);
        }
        return false;
    }

    public synchronized boolean start(Channel ch) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.start("+ch+")");
        // Note: Rating은 PPV에서 자체 처리 해야 한다.
        currentChannel = ch;

        this.subscribed = CaManager.getInstance().checkSubscriptionPurchased(ch);
        if (subscribed) {
            // 구매 되어서 start를 안했으면
            Log.printInfo(App.LOG_HEADER+"PpvController: not started. already has package !");
            return false;
        }

        Program p = EpgDataManager.getInstance().getProgram(ch, Clock.getRoundedMillis());
        if (p == null) {
            Log.printDebug(App.LOG_HEADER+"PpvController: no program. skip to start.");
            return false;
        }
        currentProgram = p;
        if (!startAssetPage(p)) {
            // 구매 되어서 start를 안했으면
            Log.printDebug(App.LOG_HEADER+"PpvController: not started.");
            return false;
        } else {
            return true;
        }
    }

    public synchronized void caUpdated(int sid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.caUpdated("+sid+")");
        if (currentChannel != null && currentChannel.getSourceId() == sid) {
            updateAssetPage();
        }
    }

    private synchronized void updateAssetPage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.updateAssetPage()");
        // TODO - 현재 보고 있는 채널에 대한 update 고려
        if (current != null) {
            boolean show = page.updateProgram();
            if (!show) {
                hideAssetPage();
            }
        }
    }

    public void setMonitoring(CAAuthorization caa) {
        synchronized (CHANNEL_KEY_SUFFIX) {
            Log.printDebug(App.LOG_HEADER+"PpvController: setMonitoring = " + caa);
            try {
                if (monitoring != null) {
                    monitoring.stopMonitoring();
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            monitoring = caa;
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // CAAuthorizationListener
    ////////////////////////////////////////////////////////////////////////////

    public void deliverEvent(CAAuthorizationEvent event) {
        Log.printInfo(App.LOG_HEADER+"ChannelAssetPage.deliverEvent: event = " + event);
        if (event == null) {
            return;
        }
        int type = event.getTargetType();
        if (type != CAAuthorization.PPV_TARGET) {
            return;
        }
        long eid = event.getTargetId() & ~EID_PREFIX;
        boolean auth = event.isAuthorized();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.deliverEvent: eid        = " + eid);
            Log.printDebug(App.LOG_HEADER+"ChannelAssetPage.deliverEvent: authorized = " + auth);
        }
        synchronized (this) {
            updateAssetPage();
        }
    }

    public void receiveServiceContextEvent(ServiceContextEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.receiveServiceContextEvent("+e+")");
        Channel ch = currentChannel;
        if (ch == null) {
            return;
        }
        Service s = e.getServiceContext().getService();
        if (s != null && !s.equals(ch.getService())) {
            Log.printDebug(App.LOG_HEADER+"PpvController: currentChannel is not this channel.");
            return;
        }
        if (e instanceof SelectionFailedEvent || e instanceof PresentationTerminatedEvent
                    || e instanceof AlternativeContentErrorEvent) {
            if (Log.DEBUG_ON) {
                int reason;
                if (e instanceof SelectionFailedEvent) {
                    reason = ((SelectionFailedEvent) e).getReason();
                } else if (e instanceof PresentationTerminatedEvent) {
                    reason = ((PresentationTerminatedEvent) e).getReason();
                } else if (e instanceof AlternativeContentErrorEvent) {
                    reason = ((AlternativeContentErrorEvent) e).getReason();
                } else {
                    reason = -1;
                }
                Log.printDebug(App.LOG_HEADER+"PpvController:  reason = " + reason);
            }
            Program p = EpgDataManager.getInstance().getProgram(ch, Clock.getRoundedMillis());
            if (p == null) {
                return;
            }
            startAssetPage(p);
        } else if (e instanceof PresentationChangedEvent) {
            if (e instanceof NormalContentEvent) {
                if (!subscribed) {
                    updateAssetPage();
                    // 권한 없으면 normal event를 skip 하자. 깜빡임 방지.
                    // SC event가 Normal, Failed 차례로 오기 때문에.
                    Log.printInfo("PpvController: no permission ! skip to hide UI !");
                    return;
                }
            }
            hideAssetPage();
        }
    }

    private Vector findOtherShowTimes(PpvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.findOtherShowTimes("+p+")");
        EpgDataManager edm = EpgDataManager.getInstance();
        OtherShowTimeFilter filter = new OtherShowTimeFilter(p);
        Vector vector = edm.requestPrograms(null, p.getId(), null, System.currentTimeMillis());
        vector = edm.filterPrograms(vector, new OtherShowTimeFilterFromHttp());
        vector = edm.filterPrograms(vector, filter);

        if (vector == null || vector.size() == 0) {
            ChannelList allList = ChannelDatabaseBuilder.getInstance().getDatabase().getAllChannelList();
            ChannelList ppvList = allList.filter(filter);
            vector = edm.findPrograms(ppvList, filter);
        }

        Collections.sort(vector, filter);
        return vector;
    }

    class OtherShowTimeFilter implements ProgramFilter, ChannelFilter, Comparator {
        private String programName;
        private long startTime;

        private OtherShowTimeFilter(PpvProgram p) {
            programName = p.getTitle();
            startTime = p.startTime;
        }

        public boolean accept(Program p) {
            return programName.equals(p.getTitle())
                && startTime != p.startTime
                && p.isFuture()
                && p instanceof PpvProgram
                && ((PpvProgram)p).eid != 0;
        }

        public boolean accept(Channel channel) {
            return channel.getType() == Channel.TYPE_PPV;
        }

        public int compare(Object o1, Object o2) {
            Program p1 = (Program) o1;
            Program p2 = (Program) o2;
            long s1 = p1.startTime;
            long s2 = p2.startTime;
            if (s1 == s2) {
                return p1.callLetter.compareTo(p2.callLetter);
            }
            if (s1 > s2) {
                return 1;
            } else {
                return -1;
            }
        }
    }

    class OtherShowTimeFilterFromHttp implements ProgramFilter {
        public boolean accept(Program p) {
            Channel ch = p.getEpgChannel();
            return ch != null && ch.getType() == Channel.TYPE_PPV;
        }
    }

    void notifyRemotePurchase(Vector programs) {
        Log.printInfo(App.LOG_HEADER+"PpvController.notifyRemotePurchase");
        PpvHistoryManager.getInstance().notifyRemotePurchase(programs);
        EpgCore.getInstance().gridEpg.updateRemotePpv();
        updateAssetPage();
    }

    void notifyRemoteCancel(String packageId) {
        Log.printInfo(App.LOG_HEADER+"PpvController.notifyRemoteCancel = " + packageId);
        PpvHistoryManager.getInstance().notifyRemoteCancel(packageId);
        EpgCore.getInstance().gridEpg.updateRemotePpv();
        updateAssetPage();
    }

    synchronized void updateCurrentCart() {
        Log.printInfo(App.LOG_HEADER+"PpvController.updateCurrentCart = " + over);
        if (over != null && over instanceof CartPage) {
            CartPage cp = (CartPage) over;
            cp.start(PpvHistoryManager.getInstance().getCurrentHistory());
            this.repaint();
        }
    }

    private synchronized void showLoading() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.showLoading()");
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.showNotDelayLoadingAnimation(new Point(480, 300));
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private synchronized void hideLoading() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PpvController.hideLoading()");
        LoadingAnimationService ls = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            ls.hideLoadingAnimation();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

}

package com.alticast.illico.epg.ppv;

import com.opencable.handler.cahandler.CAHandler;

public interface PpvListener {

    public static final int PURCHASE_SUCCESS = CAHandler.IPPV_SUCCESS;
    public static final int CANCEL_SUCCESS = CAHandler.IPPV_CANCEL_SUCCESS;

    public void notifyPurchaseResult(int result);

    public void notifyCancelResult(int result);

}

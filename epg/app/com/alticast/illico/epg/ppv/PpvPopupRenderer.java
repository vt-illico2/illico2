package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;

public class PpvPopupRenderer extends PopupRenderer {

    private Image iHd = dataCenter.getImage("icon_hd_ppv.png");
    //->Kenneth[2015.2.4] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd_ppv.png");

    private Color cTime = new Color(200, 200, 200);
    private Font fTime = FontResource.BLENDER.getFont(16);

    private Color cPrice = new Color(242, 231, 207);
    private Font fPrice = FontResource.BLENDER.getFont(18);
    private FontMetrics fmPrice = FontResource.getFontMetrics(fPrice);

    private String name;
    private String price;
    private String time;
	//->Kenneth : 4K by June
    private int definition;

    private String[] topMessage;

    public PpvPopupRenderer(PpvProgram p, String msgKey, boolean showChannel) {
		//->Kenneth : 4K by June
        this(p.getTitle(), p.startTime, p.endTime, p.price, p.getDefinition(),
                showChannel ? String.valueOf(p.getEpgChannel().getNumber()) : (String) null,
                showChannel ? p.getCallLetter() : (String) null,
                msgKey);
        // TODO - channel HD
    }

    public PpvPopupRenderer(PpvHistory h, String msgKey, boolean showChannel) {
		//->Kenneth : 4K by June
        this(h.title, h.startTime, h.endTime, h.price, h.definition,
                showChannel ? h.channelName : (String) null,
                showChannel ? h.channelNumber : (String) null,
                msgKey);
    }

	//->Kenneth : 4K by June
    private PpvPopupRenderer(String name, long start, long end, float price, int definition,
                            String no, String cl, String msgKey) {
        fText = FontResource.BLENDER.getFont(18);
        fmText = FontResource.getFontMetrics(fText);
        TEXT_Y = 163;
        Y_GAP = 18;

        this.name = name;
        Formatter fm = Formatter.getCurrent();
        time = fm.getDayText(start) + "  " + fm.getTime(start) + " ~ " + fm.getTime(end);

        if (no != null) {
            this.price = fm.getPrice(price) + " | " + no + " " + cl;
        } else {
            this.price = fm.getPrice(price);
        }
        this.definition = definition;
        if (msgKey != null) {
            setMessage(dataCenter.getString(msgKey));
        } else {
            setMessage(new String[0]);
        }
    }

    public void setTopMessage(String s) {
        topMessage = getSplitMessage(s);
    }

    protected void paintBody(Graphics g, Popup p) {
        int transY = 0;
        if (topMessage != null && topMessage.length > 0) {
            if (texts == null || texts.length == 0) {
                transY += 20;
            }
            transY += (paintMessage(g, p, topMessage, 70) - 60);
            g.translate(0, transY);
        }

        g.setColor(cTitle);
        g.setFont(fTitle);
        GraphicUtil.drawStringCenter(g, name, 175, 73);

        g.setColor(cTime);
        g.setFont(fTime);
        GraphicUtil.drawStringCenter(g, time, 175, 93);

        g.setColor(cPrice);
        g.setFont(fPrice);

        int width = g.getFontMetrics().stringWidth(price);
        int tx;
        //->Kenneth[2015.2.4] 4K
		//->Kenneth : 4K by June
        //->Kenneth[2015.5.19] VDTRMASTER-5431 : PPV 팝업에서는 definition 안 그리기로함.
        // 따라서 아래 조건식에서 else 부분의 코드만 사용한다.
        tx = 175 - width / 2;
        /*
        if (definition == Constants.DEFINITION_HD) { 
            int iw = iHd.getWidth(p);
            width += iw + 2;
            tx = 175 - width / 2;
            g.drawImage(iHd, tx, 99, p);
            tx += iw + 5;
        } else if (definition == Constants.DEFINITION_4K) { 
            int iw = iUhd.getWidth(p);
            width += iw + 2;
            tx = 175 - width / 2;
            g.drawImage(iUhd, tx, 99, p);
            tx += iw + 5;
        } else {
            tx = 175 - width / 2;
        }
        */
        g.drawString(price, tx, 116);

        if (transY == 0) {
            super.paintBody(g, p);
        } else {
            g.translate(0, -transY);
            paintMessage(g, p, texts, 193);
        }
    }

}

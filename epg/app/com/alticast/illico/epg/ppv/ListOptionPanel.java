package com.alticast.illico.epg.ppv;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import java.util.Vector;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;

public abstract class ListOptionPanel extends OptionPanel {

    public int listFocus = -1;
    public int listSize;

    public Vector list;

    public int rowSize;
    public int middle;

    public String titleKey;
    public String header;

    public ListOptionPanel(String titleKey, int row, String header) {
        this.titleKey = titleKey;
        this.header = header;
        this.rowSize = row;
        this.middle = row / 2;
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey("+code+")");
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey() : VK_ENTER");
                if (focus >= 0) {
                    if (size > 0) {
                        startClickingEffect();
                        selected(menu.getItemAt(focus));
                    }
                } else {
                    focus = 0;
                }
                break;
            case OCRcEvent.VK_UP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey() : VK_UP");
                if (focus >= 0) {
                    if (size > 0) {
                        focus = (focus - 1 + size) % size;
                    }
                } else if (listFocus > 0) {
                    listFocus--;
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey() : VK_DOWN");
                if (focus >= 0) {
                    if (size > 0) {
                        focus = (focus + 1) % size;
                    }
                } else if (listFocus < listSize - 1) {
                    listFocus++;
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey() : VK_LEFT");
                if (focus >= 0 && listSize > 0) {
                    focus = -1;
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListOptionPanel.handleKey() : VK_RIGHT");
                if (focus == -1) {
                    focus = 0;
                }
                break;
            default:
                return false;
        }
        repaint();
        return true;
    }

    public Object getFocusedItem() {
        try {
            return list.elementAt(listFocus);
        } catch (Exception ex) {
            return null;
        }
    }
}

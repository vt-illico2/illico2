package com.alticast.illico.epg.ppv;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;
import java.util.*;
import javax.tv.util.*;

public class CartPanel extends ListOptionPanel implements TVTimerWentOffListener, RightFilterListener /*, PpvListener*/ {

    private MenuItem MENU_BACK_TO_CHANNEL = new MenuItem("ppv.back_to_channel");

    String[] titles;

    TVTimerSpec secTimer = new TVTimerSpec();

    boolean adultUnblocked = false;

    public CartPanel() {
        super("ppv.cart", 9, "ppv.starts_in");
        setRenderer(new CartPanelRenderer());

        secTimer.setDelayTime(500L);
        secTimer.setRegular(true);
        secTimer.setRepeat(true);
        secTimer.addTVTimerWentOffListener(this);
    }

    public void start(Vector list) {
        focus = 0;
        listSize = list.size();
        listFocus = (listSize > 0) ? 0 : -1;
        this.list = list;

        updateTitles();

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.start("+listSize+")");

        MenuItem root = new MenuItem("PPV_CART");
        if (PpvController.getInstance().current != null) {
            root.add(MENU_BACK_TO_CHANNEL);
        }
        TVTimer.getTimer().deschedule(secTimer);
        if (listSize > 0) {
            root.add(Rs.MENU_CANCEL_ORDER);
            focus = -1;
            try {
                TVTimer.getTimer().scheduleTimerSpec(secTimer);
            } catch (Exception ex) {
            }
        }
        menu = root;
        size = menu.size();
        updateButton();
        this.start();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.handleKey("+code+")");
        int lastListFocus = listFocus;
        boolean ret = super.handleKey(code);
        if (ret && listFocus != lastListFocus) {
            updateButton();
        }
        return ret;
    }

    public void stop() {
        TVTimer.getTimer().deschedule(secTimer);
    }

    private void updateTitles() {
        titles = new String[list.size()];
        for (int i = 0; i < titles.length; i++) {
            updateTitle(i);
        }
    }

    private void updateTitle(int i) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.updateTitle("+i+")");
        titles[i] = TextUtil.shorten(getTitle((PpvHistory) list.elementAt(i)), CartPanelRenderer.fmTitle, 123);
    }

    public String getTitle(PpvHistory h) {
        if (!adultUnblocked && ParentalControl.getLevel(h) == ParentalControl.ADULT_CONTENT) {
            return DataCenter.getInstance().getString("epg.blocked_title");
        } else {
            return h.title;
        }
    }

    private void updateButton() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.updateButton()");
        if (listSize > 0) {
            int pos = menu.size() - 1;
            if (pos >= 0) {
                PpvHistory h = (PpvHistory) list.elementAt(listFocus);
                if (ParentalControl.getLevel(h) == ParentalControl.OK) {
                    menu.setItemAt(Rs.MENU_CANCEL_ORDER, pos);
                } else {
                    menu.setItemAt(Rs.MENU_UNBLOCK, pos);
                }
            }
        }
    }

    public void selected(MenuItem item) {
        if (item == MENU_BACK_TO_CHANNEL) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.selected() : MENU_BACK_TO_CHANNEL");
            Container c = getParent();
            if (c != null && c instanceof CartPage) {
                PpvController.getInstance().closePanel((CartPage) c);
            }
        } else if (item == Rs.MENU_CANCEL_ORDER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.selected() : MENU_CANCEL_ORDER");
            PpvController.getInstance().showCancelPopup((PpvHistory) list.elementAt(listFocus), null);
        } else if (item == Rs.MENU_UNBLOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.selected() : MENU_UNBLOCK");
            ChannelAssetPage.showPin(this);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        repaint(506, 163-84, 105, 287);
    }

    public void receiveCheckRightFilter(int response) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPanel.receiveCheckRightFilter("+response+")");
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            PpvHistory h = (PpvHistory) list.elementAt(listFocus);
            if (ParentalControl.getLevel(h.getRatingIndex()) == ParentalControl.ADULT_CONTENT) {
                adultUnblocked = true;
            }
            ParentalControl.getInstance().programUnblocked(h);
            try {
                PpvController.getInstance().page.setLayout();
            } catch (Exception ex) {
                Log.print(ex);
            }
            updateTitle(listFocus);
            updateButton();
            repaint();
            if (h.contains(System.currentTimeMillis())) {
                String cl = h.getCallLetter();
                Channel ch = ChannelController.get(0).getCurrent();
                if (ch != null && ch.getName().equals(cl)) {
                    ChannelController.get(0).releaseChannelBlock(ch);
                }
            }

        }
    }

    public String[] getPinEnablerExplain() {
        //->Kenneth[2015.11.23] VDTRMASTER-5718 : EpgCore 의 것을 사용한다.
        return EpgCore.getInstance().getBlockedChannelExplain(ChannelController.get(0).getCurrent(), false);
        //return TextUtil.tokenize(DataCenter.getInstance().getString("epg.unblock_pin"), '|');
        //<-
    }

//    public void notifyPurchaseResult(int result) {
//    }
//
//    public void notifyCancelResult(int result) {
//        if (result == PpvListener.CANCEL_SUCCESS) {
//            list.removeElementAt(listFocus);
//            start(list);
//        }
//    }


}

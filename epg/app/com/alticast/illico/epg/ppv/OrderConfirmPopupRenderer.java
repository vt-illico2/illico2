package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.videotron.tvi.illico.log.Log;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.util.Date;
import com.alticast.illico.epg.*;

// 사용 안함?
public class OrderConfirmPopupRenderer extends PopupRenderer {

    private Image iHd = dataCenter.getImage("icon_hd_ppv.png");
    private Image iSd = dataCenter.getImage("icon_sd.png");
    //->Kenneth[2015.2.4] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd_ppv.png");

    private Font fProgramTitle = FontResource.BLENDER.getFont(22);
    private Color cProgramTitle = new Color(250, 207, 0);
    private Color cTime = new Color(195, 195, 195);
    private Font fTime = FontResource.BLENDER.getFont(16);
    private Color cPrice = new Color(242, 231, 207);
    private Color cChannel = Color.white;
    private Font fChannel = FontResource.BLENDER.getFont(18);
    private FontMetrics fmChannel = FontResource.getFontMetrics(fChannel);
    private Font fMessage = FontResource.BLENDER.getFont(17);
    private FontMetrics fmMessage = FontResource.getFontMetrics(fMessage);
    private Color cMessage = new Color(194, 192, 192);

    private String name;
    private String time;
    private Image iDef;
    private float price;
    private String channel;

    public OrderConfirmPopupRenderer(String name, String time,
                int definition, float price, String channel, String message) {
        this.name = name;
        this.time = time;
        //->Kenneth[2015.2.4] 4K
        //this.iDef = definition > 0 ? iHd : iSd; // TODO - 4K
        if (definition == Constants.DEFINITION_SD) {
            this.iDef = iSd;
        } else if (definition == Constants.DEFINITION_HD) {
            this.iDef = iHd;
        } else if (definition == Constants.DEFINITION_4K) {
            this.iDef = iUhd;
        }
        this.price = price;
        this.channel = channel;
        this.texts = TextUtil.split(message, fmMessage, 330);
    }

    public void prepare(UIComponent c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OrderConfirmPopupRenderer.prepare()");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paintBody(Graphics g, Popup p) {
        g.setColor(cProgramTitle);
        g.setFont(fProgramTitle);
        GraphicUtil.drawStringCenter(g, name, 175, 70);

        g.setColor(cTime);
        g.setFont(fTime);
        GraphicUtil.drawStringCenter(g, time, 175, 90);
        g.drawString("|", 174, 111);

        g.setFont(fChannel);
        Formatter f = Formatter.getCurrent();
        String s = f.getPrice(price);
        int x = 163 - fmChannel.stringWidth(s);
        g.drawString(s, x, 112);
        x = x - iDef.getWidth(p) - 2;
        g.drawImage(iDef, x, 100, p);
        g.setColor(cChannel);
        g.drawString(channel, 181, 112);

        g.setColor(cMessage);
        g.setFont(fMessage);
        int y = 143 - texts.length * 18 / 2 ;
        for (int i = 0; i < texts.length; i++) {
            GraphicUtil.drawStringCenter(g, texts[i], 175, y);
            y = y + 18;
        }
    }

}

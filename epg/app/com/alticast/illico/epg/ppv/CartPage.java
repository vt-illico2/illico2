package com.alticast.illico.epg.ppv;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Container;
import java.awt.Image;
import org.ocap.ui.event.OCRcEvent;
import org.dvb.event.*;
import org.davic.resources.*;
import java.util.*;

public class CartPage extends FullScreenPanel implements MenuListener {

    CartPanel panel;

    OptionScreen optionScreen = new OptionScreen();

    public CartPage() {
        setRenderer(new FullScreenPanelRenderer());
        stack.push("ppv.pay_per_view");
        stack.push("ppv.upcoming_purchases");

        panel = new CartPanel();
        this.add(panel);
    }

    public void start(Vector list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.start()");
        footer.reset();
        footer.addButton(PreferenceService.BTN_BACK, "epg.back");
        footer.addButton(PreferenceService.BTN_SEARCH, "epg.search");
        footer.addButton(PreferenceService.BTN_D, "epg.options");
        panel.start(list);
        this.start();
    }

    public void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.pause()");
        optionScreen.stop();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.handleKey("+code+")");
        switch (code) {
            case OCRcEvent.VK_LAST:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.handleKey() : VK_LAST");
                footer.clickAnimationByKeyCode(code);
                PpvController.getInstance().closePanel(this);
                return true;
            case OCRcEvent.VK_EXIT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.handleKey() : VK_EXIT");
                PpvController.getInstance().closePanel(this);
                return true;
            case KeyCodes.COLOR_D:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.handleKey() : COLOR_D");
                footer.clickAnimationByKeyCode(code);
                optionScreen.start(Rs.PPV_OPTIONS, null, this, EpgCore.getInstance());
                return true;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.handleKey() : SEARCH");
                footer.clickAnimationByKeyCode(code);
                break;
        }
        return panel.handleKey(code);
    }

    /** MenuListener. */
    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.selected("+item+")");
        if (item == Rs.MENU_HOW_TO_ORDER_OPTION || item == Rs.MENU_HOW_TO_ORDER) {
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_HOW_TO_ORDER);
        } else if (item == Rs.MENU_TUNE_TO_TRAILER) {
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_TRAILER);
        } else if (item == Rs.MENU_NEW_THIS_MONTH) {
            PpvController.getInstance().tuneToChannel(PpvController.CHANNEL_NEW_THIS_MONTH);
        } else if (item == Rs.MENU_CART) {
//            PpvController.getInstance().showCart();
        }
    }

    public void canceled() {
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"CartPage.stop()");
        optionScreen.stop();
        panel.stop();
        super.stop();
    }

}

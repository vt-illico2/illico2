package com.alticast.illico.epg.ppv;

import java.util.Hashtable;
import java.util.Vector;
import java.rmi.RemoteException;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.daemon.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;

public class RemotePpvHandler implements RemoteRequestListener {
    public static final int PPV000 = 0;
    public static final int PPV001 = -1;
    public static final int PPV002 = -2;
    public static final int PPV003 = -3;

    private static RemotePpvHandler instance = new RemotePpvHandler();

    public static RemotePpvHandler getInstance() {
        return instance;
    }

    private RemotePpvHandler() {
    }

    /**
     * Pass request to host application.
     * @param path path to the operation to execute by host application.
     * @param body parameters(parma1=value1&param2=value2..)
     * @return response to request
     * @throws RemoteException during the execution of a remote method call
     */
    public byte[] request(String path, byte[] body) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePpvHandler.request("+path+")");
        if (path == null) {
            if (body == null) {
                Log.printWarning("RemotePpvHandler.request : no path, no bytes");
            } else {
                Log.printWarning("RemotePpvHandler.request : no path, " + body.length + " bytes");
            }
            return null;
        }
        Log.printInfo("RemotePpvHandler.request : " + path);
        if ("GetLogs".equalsIgnoreCase(path)) {
            return DiagWindow.getInstance().getLogs();
        }

        int ret = process(path, body);
        return ("result=" + ret).getBytes();
    }

    private int process(String path, byte[] body) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePpvHandler.process("+path+")");
        if (!"NotifyRPPVPurchase".equalsIgnoreCase(path)) {
            return PPV001;
        }
        Hashtable table = getBody(body);
        String packageId = (String) table.get("packageId");
        if (packageId == null || packageId.length() == 0) {
            return PPV003;
        }

        String type = (String) table.get("type");

        if ("CANCEL".equalsIgnoreCase(type)) {
            PpvController.getInstance().notifyRemoteCancel(packageId);
        } else if ("PURCHASE".equalsIgnoreCase(type)) {
            findAndAddToHistory(packageId);
        } else {
            Log.printInfo("RemotePpvHandler : invalid type = " + type);
            return PPV003;
        }
        return PPV000;
    }

    private Hashtable getBody(byte[] body) {
        Hashtable table = new Hashtable();
        String[] params = TextUtil.tokenize(new String(body), '&');
        for (int i = 0; params != null && i < params.length; i++) {
            String s = params[i];
            int pos = s.indexOf('=');
            if (pos != -1) {
                table.put(s.substring(0, pos), s.substring(pos + 1));
            }
        }
        return table;
    }

    private void findAndAddToHistory(String packageId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePpvHandler.findAndAddToHistory("+packageId+")");
        PackageIdFilter filter = new PackageIdFilter(packageId);
        Vector v = EpgDataManager.getInstance().findPrograms(filter, filter);
        if (v.size() > 0) {
            PpvController.getInstance().notifyRemotePurchase(v);
        } else {
            Log.printWarning("RemotePpvHandler : not found = " + packageId);
            // TODO - 추가 못한것, flash에 적어놓고 기다리면서 나중에라도 찾아보자
        }
    }

    class PackageIdFilter implements ProgramFilter, ChannelFilter {
        private String packageId;

        private PackageIdFilter(String pid) {
            packageId = pid;
        }

        public boolean accept(Program p) {
            return (p instanceof PpvProgram)
                && packageId.equals(((PpvProgram) p).packageId);
        }

        public boolean accept(Channel channel) {
            return channel.getType() == Channel.TYPE_PPV;
        }
    }

}


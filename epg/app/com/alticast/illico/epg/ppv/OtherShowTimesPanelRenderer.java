package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;

public class OtherShowTimesPanelRenderer extends ListOptionPanelRenderer {
    Image iEnglish = dataCenter.getImage("icon_en.png");
    Image iFrench = dataCenter.getImage("icon_fr.png");
    Image iCC = dataCenter.getImage("icon_cc.png");
    Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };

    private Color cMsg = new Color(236, 211, 143);
    private Font fLarge = FontResource.BLENDER.getFont(18);
    private Font fSmall = FontResource.BLENDER.getFont(17);

    private Color cTime = new Color(245, 245, 245);
    private Color cDate = new Color(182, 182, 182);
    private Color cChannel = new Color(160, 160, 160);

    private Color cPrice = new Color(242, 231, 207);
    private Font fPrice = FontResource.BLENDER.getFont(19);

//    private Image iOrder = dataCenter.getImage("icon_order.png");
    private Image iHd = dataCenter.getImage("icon_hd.png");
    private Image iHdFoc = dataCenter.getImage("icon_hd_foc.png");
    private Image iBlocked  = dataCenter.getImage("02_icon_con.png");
    //->Kenneth[2015.2.4] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd.png");
    private Image iUhdFoc = dataCenter.getImage("icon_uhd_foc.png");

    protected void paintList(Graphics g, ListOptionPanel o, int index, boolean focused) {
        Formatter fm = Formatter.getCurrent();
        Color c1, c2, c3;
        if (focused) {
            c1 = c2 = c3 = Color.black;
        } else {
            c1 = cTime;
            c2 = cDate;
            c3 = cChannel;
        }
        OtherShowTimesPanel m = (OtherShowTimesPanel) o;
        PpvProgram p = (PpvProgram) m.list.elementAt(index);
        g.setFont(fLarge);
        g.setColor(c2);
        GraphicUtil.drawStringCenter(g, fm.getDayText(p.startTime), 100, 23);

        g.setColor(c1);
        g.setFont(fSmall);
        GraphicUtil.drawStringRight(g, fm.getTime(p.startTime) + " -", 223, 23);
        GraphicUtil.drawStringRight(g, fm.getTime(p.endTime), fm.is24HourFormat() ? 288-25 : 288, 23);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString(p.buyWindowStartTime <= System.currentTimeMillis() ? "o" : "x", 60, 23);
            //->Kenneth[2017.3.27] log 추가
            g.drawString("id = "+p.getId(), 80, 35);
            //<-
        }

        g.setColor(c3);
        Object[] info = (Object[]) m.infoList.elementAt(index);
        GraphicUtil.drawStringRight(g, (String) info[0], 344, 23);

        g.drawString(String.valueOf(p.callLetter), 356, 23);
		//->Kenneth : 4K by June
        //->Kenneth[2015.2.4] 4K
        if (p.getDefinition() == Constants.DEFINITION_HD) { 
            Image im = focused ? iHdFoc : iHd;
            g.drawImage(im, 424, 11, o);
        } else if (p.getDefinition() == Constants.DEFINITION_4K) { 
            Image im = focused ? iUhdFoc : iUhd;
            g.drawImage(im, 424, 11, o);
        }
        g.setColor(c2);
        String key;
        if (Boolean.TRUE.equals(info[2])) {
            key = "ppv.purchased";
        } else if (Boolean.TRUE.equals(info[1])) {
            key = "ppv.available";
        } else {
            key = "ppv.coming_soon";
        }
        GraphicUtil.drawStringCenter(g, dataCenter.getString(key), 510, 23);
    }

    public void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        OtherShowTimesPanel o = (OtherShowTimesPanel) c;
        Formatter f = Formatter.getCurrent();

        PpvProgram p = o.program;
        // code from ChannelAssetPageRenderer
        int x = 56;
        int y = 40;
        g.setColor(ChannelAssetPageRenderer.cTitle);
        g.setFont(ChannelAssetPageRenderer.fTitle);

        if (ParentalControl.getLevel(p) != ParentalControl.OK) {
            g.drawImage(iBlocked, x, y - 19, c);
            x += 27;
        }
        String s = ParentalControl.getTitle(p);
        g.drawString(s, x, y);

        x += 5 + ChannelAssetPageRenderer.fmTitle.stringWidth(s);
		//->Kenneth : 4K by June
        //->Kenneth[2015.2.4] 4K
        if (p.getDefinition() == Constants.DEFINITION_HD) {
            g.drawImage(iHd, x, y - 15, c);
            x += 25;
        } else if (p.getDefinition() == Constants.DEFINITION_4K) {
            g.drawImage(iUhd, x, y - 15, c);
            x += 27;
        }
        Image im = ratingImages[p.getRatingIndex()];
        if (im != null) {
            g.drawImage(im, x, y - 15, c);
        }

        g.setColor(cPrice);
        g.setFont(fPrice);
        s = f.getPrice(p.price);
        g.drawString(s, 56, 147-84);

        g.setColor(ChannelAssetPageRenderer.cDur);
        g.setFont(ChannelAssetPageRenderer.fDur);
        s = Genre.getName(p.getConvertedCategory()) + "  |  ";

        x = 57;
        y = 167-84;
        g.drawString(s, x, y);
        x += ChannelAssetPageRenderer.fmDur.stringWidth(s);
        if (Constants.LANGUAGE_ENGLISH.equals(p.getLanguage())) {
            im = iEnglish;
            g.drawImage(im, x, y-12, c);
            x += im.getWidth(c) + 1;
        } else if (Constants.LANGUAGE_FRENCH.equals(p.getLanguage())) {
            im = iFrench;
            g.drawImage(im, x, y-12, c);
            x += im.getWidth(c) + 1;
        }
        if (p.isCaptioned()) {
            g.drawImage(iCC, x, y-12, c);
        }

        g.setFont(ChannelAssetPageRenderer.fToOrder);
        g.setColor(ChannelAssetPageRenderer.cToOrder);
        g.drawString(dataCenter.getString("to_order"), 55, 419);
    }

}

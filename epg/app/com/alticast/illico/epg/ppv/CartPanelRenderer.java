package com.alticast.illico.epg.ppv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.ParentalControl;
import org.dvb.ui.DVBColor;
import java.awt.Color;
import java.awt.Image;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.Graphics;
import java.util.Date;

public class CartPanelRenderer extends ListOptionPanelRenderer {
    Image iEnglish = dataCenter.getImage("icon_en.png");
    Image iFrench = dataCenter.getImage("icon_fr.png");
    Image iCC = dataCenter.getImage("icon_cc.png");
    Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };

    static Color cMsg = new Color(236, 211, 143);
    static Font fLarge = FontResource.BLENDER.getFont(18);
    static Font fSmall = FontResource.BLENDER.getFont(17);
    static FontMetrics fmTitle = FontResource.getFontMetrics(fLarge);

    private Color cTime = new Color(245, 245, 245);
    private Color cDur = new Color(182, 182, 182);
    private Color cChannel = new Color(160, 160, 160);

    private Color cTitleInfo = new Color(255, 203, 0);
    private Color cChannelInfo = new Color(224, 224, 224);
    private Font fTitleInfo = FontResource.BLENDER.getFont(24);
    private Font fChannelInfo = FontResource.BLENDER.getFont(20);
    private FontMetrics fmTitleInfo = FontResource.getFontMetrics(fTitleInfo);

//    private Image iOrder = dataCenter.getImage("icon_order.png");
    private Image iHd = dataCenter.getImage("icon_hd.png");
    private Image iHdFoc = dataCenter.getImage("icon_hd_foc.png");
    private Image iBlocked = dataCenter.getImage("02_icon_con.png");
    //->Kenneth[2015.2.4] 4K
    private Image iUhd = dataCenter.getImage("icon_uhd.png");
    private Image iUhdFoc = dataCenter.getImage("icon_uhd_foc.png");

    protected void paintList(Graphics g, ListOptionPanel o, int index, boolean focused) {
        long cur = System.currentTimeMillis();
        Formatter fm = Formatter.getCurrent();
        Color c1, c2, c3;
        if (focused) {
            c1 = c2 = c3 = Color.black;
        } else {
            c1 = cTime;
            c2 = cDur;
            c3 = cChannel;
        }
        CartPanel m = (CartPanel) o;
        PpvHistory h = (PpvHistory) m.list.elementAt(index);
        g.setFont(fLarge);
        g.setColor(c1);
        g.drawString(m.titles[index], 69, 23);

        g.setColor(c3);
        GraphicUtil.drawStringRight(g, h.channelNumber, 224, 23);
        g.drawString(h.channelName, 235, 23);

        g.setColor(c2);
        GraphicUtil.drawStringCenter(g, fm.getDayText(h.startTime), 324, 23);

        g.setColor(c1);
        g.setFont(fSmall);
        GraphicUtil.drawStringRight(g, fm.getTime(h.startTime) + " -", 435, 23);
        GraphicUtil.drawStringRight(g, fm.getTime(h.endTime), fm.is24HourFormat() ? 501-25 : 501, 23);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString(h.purchased ? "o" : "x", 60, 23);
        }

        g.setColor(c2);
        g.setFont(fLarge);
        long startsIn = Math.max(0, h.startTime - cur);
        String str;
        if (startsIn == 0) {
            str = dataCenter.getString("ppv.started");
        } else {
            str = fm.getCountDownDuration(startsIn);
        }
        GraphicUtil.drawStringRight(g, str, 600, 23);
    }

    public void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        CartPanel o = (CartPanel) c;
        Formatter f = Formatter.getCurrent();

        g.setFont(ChannelAssetPageRenderer.fToOrder);
        g.setColor(ChannelAssetPageRenderer.cToOrder);
        g.drawString(dataCenter.getString("to_cancel"), 55, 419);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            String s;
            if (PpvHistoryManager.lastUpdateTime > 0) {
                s = new Date(PpvHistoryManager.lastUpdateTime).toString();
            } else {
                s = "N/A";
            }
            g.drawString("Last Update = " + s + ",  Update Count = " + PpvHistoryManager.lastUpdateCount, 50, 18);
            g.drawString("Next Update = " + new Date(PpvHistoryManager.nextUpdateTime), 50, 38);
        }

        PpvHistory h = (PpvHistory) o.getFocusedItem();
        if (h == null) {
            return;
        }
        g.translate(0, -84);
        int end = 911;
        boolean blocked = ParentalControl.getLevel(h) != ParentalControl.OK;
		//->Kenneth : 4K by June
        int def = h.definition;
        if (def == Constants.DEFINITION_HD) { 
            end -= 26;
        } else if (def == Constants.DEFINITION_4K) {
            end -= 28;
        }
        int ix;
        if (blocked) {
            g.drawImage(iBlocked, 630, 127, c);
            ix = 658;
        } else {
            ix = 633;
        }
        g.setFont(fTitleInfo);
        g.setColor(cTitleInfo);
        String s = TextUtil.shorten(o.getTitle(h), fmTitleInfo, end - ix);
        g.drawString(s, ix, 145);
        ix += fmTitleInfo.stringWidth(s) + 5;
		//->Kenneth : 4K by June
        //->Kenneth[2015.2.4] 4K
        if (def == Constants.DEFINITION_HD) { 
            g.drawImage(iHd, ix, 131, c);
            ix += 26;
        } else if (def == Constants.DEFINITION_4K) {
            g.drawImage(iUhd, ix, 131, c);
            ix += 28;
        }
        Image im = ratingImages[h.rating];
        if (im != null) {
            g.drawImage(im, ix, 131, c);
        }
        ix = 633;
        g.setFont(fChannelInfo);
        g.setColor(cChannelInfo);
        g.drawString(h.channelNumber, 633, 172);
        g.drawString(h.channelName, 671, 172);
        g.translate(0, 84);
    }

}

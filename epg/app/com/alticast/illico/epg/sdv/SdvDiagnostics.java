package com.alticast.illico.epg.sdv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.epg.App;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.util.DiagScreen;
import com.alticast.ui.*;
import java.awt.*;
import org.dvb.ui.DVBColor;
import java.util.*;
import java.text.*;

public class SdvDiagnostics extends DiagScreen {

    static SdvDiagnostics screen = new SdvDiagnostics();
    public static final String DATE_PATTERN = "MM/dd@HH:mm:ss.SSS";

    // client
    public static final int AUTHORIZED   = 0;
    public static final int SERVICE_GP   = 1;
    public static final int HUB_ID       = 2;
    public static final int SDV_CHANNELS = 3;
    public static final int PACKAGE      = 4;
    // server
    public static final int STATUS = 5;
    public static final int TIME = 6;
    public static final int PRI_IP_PORT = 7;
    public static final int SEC_IP_PORT = 8;
    // protocols
    public static final int SELIND_RX = 9;
    public static final int SELRESP_TX = 10;
    public static final int QRYREQ_RX = 11;
    public static final int QRYCONF_TX = 12;
    public static final int EVIND_RX = 13;
    public static final int EVRESP_TX = 14;
    public static final int EVIND_TX = 15;
    public static final int LUA_REP_TX = 16;
    public static final int TOTAL_TX_RX = 17;
    public static final int INITREQ_TX = 18;
    public static final int INITCONF_RX = 19;
    public static final int INITCONFFAILS_RX = 20;
    public static final int SELREQ_TX = 21;
    public static final int SDV_SELREQ_TX = 22;
    public static final int SELCONF_RX = 23;
    public static final int SELCONFFAILS_RX = 24;
    public static final int SELCONFTIMEOUT_RX = 25;
    // mini carousel
    public static final int MC_STATUS = 26;
    public static final int DEF_FREQ = 27;
    public static final int TVP_TV_ID = 28;
    public static final int LOAD_TIME = 29;
    public static final int VERSION = 30;
    public static final int SIZE = 31;
    public static final int NUM_ENTRIES = 32;
    public static final int CACHE_HITS = 33;
    public static final int CACHE_MISSES = 34;
    public static final int CACHE_OVERRIDES = 35;
    public static final int LOAD_COUNT = 36;
    public static final int LOAD_FAILURES = 37;
    public static final int LAST_LOAD_ERR = 38;
    public static final int ERR_TIME = 39;
    public static final int LAST_LOAD_ATTEMPT = 40;
    // timing params for MC
    public static final int MESSAGE_RESPONSE_TIMEOUT = 41;
    public static final int MESSAGE_REQUEST_RETRY_INTERVAL = 42;
    public static final int BANDWIDTH_RECLAIM_UI_TIMEOUT = 43;
    public static final int MINI_CAROUSEL_READ_INTERVAL = 44;
    public static final int LUA_REPORTING_INTERVAL = 45;
    public static final int MESSAGE_REQUEST_MAX_RETRIES_COUNT = 46;
    public static final int GLOBAL_TIMEOUT = 47;

    public static final int LUA_TX_TIME     = 48;
    public static final int LAST_SEND_ERROR = 49;
    public static final int CHANNELS_MC = 50;

    private static int[] values = new int[52];
    private static String[] strings = new String[52];

    public static final String[] LABLES = {
        "Authorized:",
        "Service Gp:",
        "Hub ID:",
        "Channels:",
        "Package:",
        "Status:",
        "Time:",
        "Pri Ip-Port:",
        "Sec Ip-Port:",
        "SelInd Rx:",
        "SelResp Tx:",
        "QryReq Rx:",
        "QryConf Tx:",
        "EvInd Rx:",
        "EvResp Tx:",
        "EvInd Tx:",
        "LUA Rep Tx:",
        "Total Tx/Rx:",
        "InitReq Tx:",
        "InitConf Rx:",
        "InitConfFails Rx:",
        "SelReq Tx:",
        "SDV SelReq Tx:",
        "SelConf Rx:",
        "SelConfFails Rx:",
        "SelConfTimeout Rx:",
        "Status:",
        "Def Freq:",
        "Tvp/Tv Id:",
        "Load Time:",
        "Version:",
        "Size:",
        "Num Entries:",
        "Cache Hits:",
        "Cache misses:",
        "Cache Overrides:",
        "Load Count:",
        "Load Failures:",
        "Last Load Err:",
        "Err Time:",
        "Last Load Attempt:",
        "MessageResponseTimeout:",
        "MessageRequestRetryInterval:",
        "BandwidthReclaimUITimeout:",
        "MiniCarouselReadInterval:",
        "LuaReportingInterval:",
        "MessageRequestMaxRetriesCount:",
        "Global Timeout:",
        "LUA Tx Time:",
        "Last Send Error:",
        "Channels MC:",
    };

    public static void increase(int field) {
        values[field] = values[field] + 1;
        strings[field] = null;
        checkRepaint();
    }

    public static void set(int field, int value) {
        values[field] = value;
        strings[field] = null;
        checkRepaint();
    }

    public static void set(int field, long value) {
        set(field, (int) value);
    }

    public static void set(int field, String value) {
        strings[field] = value;
        checkRepaint();
    }

    public static void resetTime(int field) {
        resetTime(field, null);
    }

    public static void resetTime(int field, String msg) {
        String s = new SimpleDateFormat(DATE_PATTERN).format(new Date());
        if (msg == null) {
            strings[field] = s;
        } else {
            strings[field] = msg + " : " + s;
        }
        checkRepaint();
    }

    public static SdvDiagnostics getInstance() {
        return screen;
    }

    private SdvDiagnostics() {
        super("SDV Diagnostics", new DVBColor(0, 0, 0, 160));
    }

    private static void checkRepaint() {
        if (screen.getParent() != null) {
            screen.repaint();
        }
    }

    public int getCount() {
        return 1;
    }

    public ArrayList getLogs() {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < LABLES.length; i++) {
            try {
                String s = strings[i];
                if (s == null) {
                    s = String.valueOf(values[i]);
                }
                arrayList.add(LABLES[i] + DELIMITER + s);
            } catch (Exception ex) {
            }
        }
        return arrayList;
    }

    ////////////////////////////////////////////////////////////////////////

    private static final int ROW_GAP = 21;
    private static final int Y1 = 85;
    private static final int Y2 = Y1 + ROW_GAP * 6;
    private static final int Y3 = Y2 + ROW_GAP * 5;
    private static final int X1 = 500;
    private static final int X2 = 150;
    private static final int X3 = 280;
    private static final int X4 = 830;

    Font sdvFont = FontResource.BLENDER.getFont(20);
    Color c1 = Color.yellow;
    Color c2 = Color.orange;
    Color c3 = new Color(120, 255, 100);

    public void paintBody(Graphics g) {
        g.setFont(sdvFont);
        g.setColor(Color.cyan);
        g.drawString("Mini Carousel", X1 - 55, Y1);
        g.drawString("Client - " + App.NAME + " " + App.VERSION, X2 - 90, Y1);
        g.drawString("Server - " + EpgCore.dncsName, X2 - 45, Y2);
        g.drawString("Timing Parameters", X3 - 140, Y3);
        g.drawString("Protocols", X4 - 65, Y1);

        int y;
        // client
        y = Y1 + ROW_GAP;
        for (int i = AUTHORIZED; i <= PACKAGE; i++) {
            draw(g, c1, i, X2, y);
            y += ROW_GAP;
        }
        // server
        y = Y2 + ROW_GAP;
        for (int i = STATUS; i <= SEC_IP_PORT; i++) {
            draw(g, c1, i, X2, y);
            y += ROW_GAP;
        }
        // params
        y = Y3 + ROW_GAP;
        for (int i = MESSAGE_RESPONSE_TIMEOUT; i <= GLOBAL_TIMEOUT; i++) {
            draw(g, c3, i, X3, y);
            y += ROW_GAP;
        }
        draw(g, c2, CHANNELS_MC, X2, y);
        g.drawString(SDVManager.getInstance().getThreadStatus(), 60, y + ROW_GAP);

        // mc
        y = Y1 + ROW_GAP;
        for (int i = MC_STATUS; i <= LAST_LOAD_ATTEMPT; i++) {
            draw(g, c2, i, X1, y);
            y += ROW_GAP;
        }
        y += ROW_GAP;
        draw(g, c1, LUA_TX_TIME, X1, y);
        y += ROW_GAP;
        draw(g, c1, LAST_SEND_ERROR, X1, y);

        // protocol
        y = Y1 + ROW_GAP;
        for (int i = SELIND_RX; i <= SELCONFTIMEOUT_RX; i++) {
            draw(g, c1, i, X4, y);
            y += ROW_GAP;
        }
    }

    private void draw(Graphics g, Color c, int field, int x, int y) {
        String s = strings[field];
        if (s == null) {
            s = String.valueOf(values[field]);
        }
        g.setColor(c);
        GraphicUtil.drawStringRight(g, LABLES[field], x, y);
        g.setColor(Color.white);
        g.drawString(s, x + 7, y);
    }

}

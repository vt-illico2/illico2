package com.alticast.illico.epg.sdv;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.sdv.ccp.SDBEventIndication;
import com.alticast.illico.epg.sdv.ccp.SDBEventResponse;
import com.alticast.illico.epg.sdv.ccp.SDBInitConfirm;
import com.alticast.illico.epg.sdv.ccp.SDBInitRequest;
import com.alticast.illico.epg.sdv.ccp.SDBMessage;
import com.alticast.illico.epg.sdv.ccp.SDBProgramSelectConfirm;
import com.alticast.illico.epg.sdv.ccp.SDBProgramSelectIndication;
import com.alticast.illico.epg.sdv.ccp.SDBProgramSelectRequest;
import com.alticast.illico.epg.sdv.ccp.SDBProgramSelectResponse;
import com.alticast.illico.epg.sdv.ccp.SDBQueryConfirm;
import com.alticast.illico.epg.sdv.ccp.SDBQueryRequest;
import com.alticast.illico.epg.sdv.ccp.SDBUserActivityReport;
import com.alticast.illico.epg.sdv.ccp.TransactionID;
import com.alticast.illico.epg.UserActivityChecker;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;

/**
 * This singleton class manages all Switched Digital Video (SDV) related operations and data. It takes care of
 * registering with the SDV server, sending messages, and receiving messages from the SDV server. It also manages the
 * current set of source ID -> tuning triplet mappings.
 * @author Jay Tracy
 */
public class SDVManager {
    private static final int RECEIVE_BUFFER_SIZE = 2048;
    private static final int READ_TIME_OUT = 1000 * 17;

    /** Init request not yet sent to SDV server */
    public static final int STATE_UNKNOWN = 0;
    /** Sent init request, but no response received yet */
    public static final int STATE_PENDING = 1;
    /** Sent init request, and received confirm from SDV server */
    public static final int STATE_READY = 2;
    /** Exceeded init retry count and still no response from SDV server */
    public static final int STATE_UNAVAILABLE = 3;

    // the singleton instance
    private static SDVManager s_instance = new SDVManager();

    // the DatagramSocket used to receive SDV messages
    // Note: need a separate socket as you cannot send / receive on the
    // same socket on the OCAP stack on the 8300.
    private DatagramSocket socket;

    // used to manage the message receive thread
    private ReceiveThread receiveThread;

    private SDVEventListener listener;

    private InetAddress serverAddress;
    private int serverPort;
    private int clientPort;
    private int serviceGroupId;

    // indicates the state of the SDVManager
    private int m_state; // defaults to STATE_UNITIALIZED

    private int threadCount = 0;
    private long lastCreatedTime = 0;
    private long receivedBytes = 0;

    private Double THREAD_RESET_LOCK = new Double(18.1818d);

    /**
     * Returns the singleton instance of SDVManager.
     * @return the singleton instance of SDVManager.
     */
    public static SDVManager getInstance() {
        return s_instance;
    }

    /**
     * Constructor, although this is a singleton, the class is public so that the class factory can create an instance
     * @see getInstance
     */
    private SDVManager() {
        setState(STATE_UNKNOWN);
    }

    public void setAddress(String ip, int port, int sgId) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"SDVManager.setAddress = " + ip + " / " + port);
        }
        InetAddress server = null;
        try {
            server = InetAddress.getByName(ip);
        } catch (UnknownHostException e) {
            Log.printError(e);
            return;
        }
        if (server.equals(serverAddress) && port == serverPort && sgId == serviceGroupId) {
            Log.printInfo("SDVManager.setAddress: skip same address.");
            return;
        }
        synchronized (this) {
            this.serviceGroupId = sgId;
            this.serverAddress = server;
            if (port != serverPort) {
                // port chnaged
                this.serverPort = port;
                this.clientPort = port;
                resetReceiveThread(true);
            } else {
                SDVController.getInstance().sendInit();
            }
        }
    }

    public void resetReceiveThread(boolean sendInit) {
        Log.printDebug(App.LOG_HEADER+"SDVManager.resetReceiveThread = " + sendInit);
        synchronized (THREAD_RESET_LOCK) {
            if (receiveThread == null) {
                receiveThread = new ReceiveThread(sendInit);
                receiveThread.start();
            } else {
                receiveThread.reset(sendInit);
            }
        }
    }

    class ReceiveThread extends Thread {
        boolean init;
        long currentVersion;

        public ReceiveThread(boolean sendInit) {
            super("SDV_ReceiveThread");
            reset(sendInit);
        }

        public void reset(boolean sendInit) {
            init = sendInit;
            threadCount++;
            lastCreatedTime = System.currentTimeMillis();
        }

        public void run() {
            while (true) {
                try {
                    processLoop();
                } catch (Throwable t) {
                    Log.print(t);
                }
            }
        }

        private void processLoop() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.SDVReceiveThread.processLoop()"); 
            // send the initialization message for each tuner ID
            byte[] receiveBuffer = new byte[RECEIVE_BUFFER_SIZE];

            DatagramSocket serverSocket = null;
            while (serverSocket == null) {
                Log.printInfo("SDVReceiveThread : creating socket.");
                try {
                    serverSocket = new DatagramSocket(clientPort);
                    serverSocket.setSoTimeout(READ_TIME_OUT);
                } catch (Throwable t) {
                    Log.print(t);
                }
                if (serverSocket == null) {
                    try {
                        Thread.sleep(1000L);
                    } catch (Exception ex) {
                    }
                }
            }
            Log.printInfo("SDVReceiveThread : socket = " + serverSocket);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.SDVReceiveThread.processLoop() : socket = "+serverSocket); 
            synchronized (SDVManager.this) {
                currentVersion = lastCreatedTime;
                socket = serverSocket;
            }

            if (init) {
                Thread thread = new Thread("SDV_Send_Init") {
                    public void run() {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.SDVReceiveThread.processLoop().SDV_Send_Init.run()"); 
                        SDVController.getInstance().sendInit();
                    }
                };
                thread.start();
            }

            while (currentVersion == lastCreatedTime) {
                // Receive message in a UDP datagram
                DatagramPacket packet = new DatagramPacket(receiveBuffer, RECEIVE_BUFFER_SIZE);
                Log.printInfo("SDVReceiveThread : waiting packet");
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.SDVReceiveThread.processLoop() : waiting packet"); 
                try {
                    serverSocket.receive(packet);
                    // handle message
                    byte[] messageBytes = packet.getData();
                    int len = packet.getLength();
                    receivedBytes += len;
                    if (Log.INFO_ON) {
                        Log.printInfo(App.LOG_HEADER+"SDVReceiveThread : packet received : length = " + len + ", offset = " + packet.getOffset());
                    }
                    try {
                        // Let's go ahead and get the latest non-connection
                        // related header
                        handleMessage(messageBytes);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                } catch (Throwable t) {
                    if (Log.EXTRA_ON) {
                        Log.printDebug(App.LOG_HEADER+"SDVReceiveThread : read timeout - Waiting packet...");
                    }
                }
            }
            Log.printWarning(App.LOG_HEADER+"SDVReceiveThread : reset. closing socket.");
            try {
                serverSocket.close();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }


    // Determines the type of message
    protected SDBMessage handleMessage(byte[] buf) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage()"); 
        SDBMessage sdvMessage = null;
        short messageType = (short) (((buf[2] & 0xFF) << 8) | (buf[3] & 0xFF));
        DataInputStream dis = new DataInputStream(new ByteArrayInputStream(buf));
        short result = 0;
        Log.printInfo("We just broke out of the receive thread" + (messageType & 0xFFFF));
        switch (messageType) {
        case SDVConstants.SDVMessageID_SDVInitConfirm:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage() : SDVInitConfirm"); 
            sdvMessage = new SDBInitConfirm();
            result = sdvMessage.parse(dis);

            if (Log.INFO_ON) {
                Log.printInfo(sdvMessage.toString());
            }

            // pellos
            Log.printWarning(" result = " +  result);
            result = SDVConstants.FORMAT_OK;

            SdvDiagnostics.increase(SdvDiagnostics.INITCONF_RX);
            if (result == SDVConstants.FORMAT_OK) {
                setState(STATE_READY);
                SdvDiagnostics.resetTime(SdvDiagnostics.TIME);
                UserActivityChecker.getInstance().setEnabled(true);
                DataCenter.getInstance().put(SDVController.INIT_RESULT, "Success");
            } else {
                initFailed();
            }
            // m_currRetryCount = 0;
            break;

        case SDVConstants.SDVMessageID_SDVProgramSelectConfirm:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage() : SDVProgramSelectConfirm"); 
            sdvMessage = new SDBProgramSelectConfirm();
            sdvMessage.parse(dis);

            SDBProgramSelectConfirm confirmMessage = (SDBProgramSelectConfirm) sdvMessage;
            if (Log.INFO_ON) {
                Log.printInfo("SDVProgramSelectConfirm confirm message = " + confirmMessage);
            }
            // Diag 설정은 Controller로 이동
            break;

        case SDVConstants.SDVMessageID_SDVProgramSelectIndication:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage() : SDVProgramSelectIndication"); 
            sdvMessage = new SDBProgramSelectIndication();
            sdvMessage.parse(dis);

            if (Log.INFO_ON) {
                Log.printInfo("SDVProgramSelectIndication message = " + sdvMessage);
            }
            SdvDiagnostics.increase(SdvDiagnostics.SELIND_RX);
            break;

        case SDVConstants.SDVMessageID_SDVQueryRequest:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage() : SDVQueryRequest"); 
            sdvMessage = new SDBQueryRequest();
            sdvMessage.parse(dis);
            SdvDiagnostics.increase(SdvDiagnostics.QRYREQ_RX);
            break;

        case SDVConstants.SDVMessageID_SDVEventIndication:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.handleMessage() : SDVEventIndication"); 
            sdvMessage = new SDBEventIndication();
            sdvMessage.parse(dis);
            if (Log.INFO_ON) {
                Log.printInfo("SDVEventIndication Message Parse Complete " + sdvMessage);
            }
            SdvDiagnostics.increase(SdvDiagnostics.EVIND_RX);
            break;

//        case SDVConstants.SDVMessageID_SDVEventResponse:
        default:
            return null;
        }

        Log.printInfo("sdvMessage = " + sdvMessage.getClass());
        RetryHandler.getInstance().messageReceived(sdvMessage);
        if (listener != null) {
            listener.receiveMessage(sdvMessage, result);
        }
        return sdvMessage;
    }

    public void setSDVEventListener(SDVEventListener l) {
        listener = l;
    }

    /**
     * This method actually sends a message to the SDV Server (UDP datagram)
     * @param msg - the message being sent to the server (not serialized)
     * @param tid - a reference to the transaction ID that should be used or     null if the next transactionID should be
     *            used
     */
    public boolean sendMessage(SDBMessage msg, TransactionID tid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.sendMessage()"); 
        if (!SDVController.hasSdvPackage) {
            Log.printWarning("SDVManager: not authroized !");
        }

        if (msg instanceof SDBInitRequest) {
            if (m_state != STATE_READY) {
                setState(STATE_PENDING);
            }
        } else {
            if (m_state == STATE_UNAVAILABLE) {
                Log.printWarning("SDVManager: need to send init request !");
                SDVController.getInstance().sendInit();
                return false;
            } else if (m_state != STATE_READY) {
                Log.printWarning("SDVManager: not ready to send message !");
                return false;
            }
        }
        if (tid == null) {
            tid = new TransactionID();
        }
        msg.setTransactionID(tid);

        if (Log.INFO_ON) {
            Log.printInfo(msg.toString());
        }

        byte buf[] = msg.toByteArray();

        if (Log.INFO_ON) {
            Log.printInfo("SDVManager: serverAddress = " + serverAddress);
            Log.printInfo("\t\tIPAddress() = " + serverAddress.getHostAddress());
            Log.printInfo("\t\tPort() = " + serverPort);
        }

        DatagramPacket packet = new DatagramPacket(buf, buf.length, serverAddress, serverPort);

        try {
            if (Log.DEBUG_ON) {
                dumpBytes(buf, msg.getMessageFieldSegments());
                Log.printDebug("SDVManager: send before");
            }
            synchronized (this) {
                RetryHandler.getInstance().messageSent(msg);
                if (socket != null) {
                    socket.send(packet);
                } else {
                    Log.printInfo("SDVManager: socket is null");
                    return false;
                }
            }
            Log.printInfo("SDVManager: send after");
            return true;
        } catch (Exception e) {
            Log.print(e);
            SdvDiagnostics.resetTime(SdvDiagnostics.LAST_SEND_ERROR, e.getMessage());
        }
        return false;
    } // sendMessage()

    /**
     * Utility - dump byte array to sysout
     */
    private void dumpBytes(byte[] buf, int[] fieldLength) {
        dumpBytes(buf, buf.length, fieldLength);
    }

    /**
     * Utility - dump byte array to sysout
     */
    private void dumpBytes(byte[] buf, int bufLength, int[] fieldLength) {
        if (!Log.DEBUG_ON) {
            return;
        }
        int index = 0;
        int fieldLengthLength = fieldLength.length;
        int rowCount = 0;
        int whichField = 0;
        int currentFieldLength = fieldLength[whichField];
        String str = "";

        Log.printDebug("\t\t       0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F");

        while (index < bufLength) {
            rowCount = 0;
            str = "\t\t0x";
            if ((index & 0xFF) < 16)
                str += "0";
            str += Integer.toHexString(index & 0xFF) + "  ";
            while (index < bufLength && rowCount < 16) {
                if ((buf[index] < 16) && (buf[index] >= 0))
                    str += "0";
                str += Integer.toHexString((int) buf[index++] & 0xFF);
                currentFieldLength--;
                if (currentFieldLength == 0) {
                    str += ".";
                    if ((whichField + 1) < fieldLengthLength)
                        currentFieldLength = fieldLength[++whichField];
                } else
                    str += " ";

                rowCount++;
            }
            Log.printDebug(str);
        }

    } // dumpBytes(byte buf[], int[] fieldLength)

    /**
     * Returns the current state of the SDVManager with respect to server registration.
     * @return the current state of the SDVManager. Valid return values are:
     */
    public int getState() {
        return m_state;
    }

    private void setState(int s) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.setState("+s+")"); 
        m_state = s;
        SdvDiagnostics.set(SdvDiagnostics.STATUS, stateToString());
    }

    public void initFailed() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVManager.initFailed()"); 
        setState(STATE_UNAVAILABLE);
        SdvDiagnostics.increase(SdvDiagnostics.INITCONFFAILS_RX);
        DataCenter dc = DataCenter.getInstance();
        Object result = dc.get(SDVController.INIT_RESULT);
        if (result == null) {
            dc.put(SDVController.INIT_RESULT, "Failed");
        }
    }

    private String stateToString() {
        String val = new String();
        switch (m_state) {
            case STATE_PENDING:
                return "Pending";
            case STATE_READY:
                return "Ready";
            case STATE_UNAVAILABLE:
                return "Unavailable";
        }
        return "Unknown";
    }

    public String getThreadStatus() {
        return App.LOG_HEADER+"SDVManager : SDV Receive Thread : " + new Date(lastCreatedTime) + ", count = " + threadCount + ", received = " + receivedBytes + " Bytes";
    }

}

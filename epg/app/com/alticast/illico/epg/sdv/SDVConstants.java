package com.alticast.illico.epg.sdv;

public class SDVConstants {
	public static final int DSMCC_HEADER_LENGTH = 12;

	public static final byte MPEG2_DSMCC_Protocol_ID = 0x11;

	public static final byte DSMCCMessageType_Reserved = 0x0;
	public static final byte DSMCCMessageType_UN_Config = 0x1;
	public static final byte DSMCCMessageType_UN_Session = 0x2;
	public static final byte DSMCCMessageType_Download = 0x3;
	public static final byte DSMCCMessageType_SDV_CCP = 0x4;
	public static final byte DSMCCMessageType_Network_PassThru = 0x5;

	public static final byte DSMCCTransactionID_Assigned_By_Client = 0x0;
	public static final byte DSMCCTransactionID_Assigned_By_Server = 0x1;
	public static final byte DSMCCTransactionID_Assigned_By_Network = 0x2;

	// Message IDs
	public static final short SDVMessageID_Reserved = (short) 0x0000;
	public static final short SDVMessageID_SDVProgramSelectRequest = (short) 0x0001;
	public static final short SDVMessageID_SDVProgramSelectConfirm = (short) 0x0002;
	public static final short SDVMessageID_SDVProgramSelectIndication = (short) 0x0003;
	public static final short SDVMessageID_SDVProgramSelectResponse = (short) 0x0004;

	public static final short SDVMessageID_SDVUserActivityReport = (short) 0x8000;
	public static final short SDVMessageID_SDVInitRequest = (short) 0x8001;
	public static final short SDVMessageID_SDVInitConfirm = (short) 0x8002;
	public static final short SDVMessageID_SDVQueryRequest = (short) 0x8003;
	public static final short SDVMessageID_SDVQueryConfirm = (short) 0x8004;
	public static final short SDVMessageID_SDVEventIndication = (short) 0x8005;
	public static final short SDVMessageID_SDVEventResponse = (short) 0x8006;

	public static final int SDVProgramSelectRequest_TunerUseMask_Scheduled_Recording = 0x1;
	public static final int SDVProgramSelectRequest_TunerUseMask_Reserved = 0x62;
	public static final int SDVProgramSelectRequest_TunerUseMask_PPV = 0x4;
	public static final int SDVProgramSelectRequest_TunerUseMask_Display_Endpoint = 0x1C;

	public static final int EventDescriptorTag_DisplayBarkerMessage = 0;
	public static final int EventDescriptorTag_TunerUseChanged = 1;

	public static final byte TunerUseScheduledRecordingMask = 0x01;
	public static final byte TunerUsePPVMask = 0x04;
	public static final byte TunerUseMainTVMask = 0x08;
	public static final byte TunerUsePIPMask = 0x10;

	public static final byte QAM64 = 0x08;
	public static final byte QAM256 = 0x10;

	public static final byte ChannelServiceTypeStatic = 0x02;
	public static final byte ChannelServiceTypeDynamic = 0x10;
	public static final byte ChannelServiceTypeReclaim = 0x2A;

	public static final short FORMAT_OK = 0x0000;
	public static final short FORMAT_ERROR = 0x0001;

	public static final short SDV_ACK = 0x0000; // ???

	public static final short SDV_CVCT_PID = 0x1FEE;

	public static final short SDV_CVCT_HeaderID = (byte) 0xC9;
	public static final short SDV_Section_Number_Offset = 6;

	public static final byte IPResourceDescriptorTag = 3;
	public static final byte SDVConfigParamsDescriptorTag = 4;

}

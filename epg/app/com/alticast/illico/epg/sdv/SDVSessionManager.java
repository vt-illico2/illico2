package com.alticast.illico.epg.sdv;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.ccp.*;
import com.alticast.illico.epg.sdv.mc.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import javax.tv.service.selection.*;
import java.util.*;
import org.dvb.event.*;
import org.ocap.hardware.Host;
import org.ocap.net.OcapLocator;
import org.ocap.shared.dvr.*;
import org.ocap.dvr.*;
import javax.tv.locator.Locator;


public class SDVSessionManager {
    private static final int TUNER_ID_INDEX = 6;

    public static final int SDV_RECORDING_PENDING = 1;
    public static final int SDV_RECORDING_OK      = 2;

    SDVController con = SDVController.getInstance();

    Session[] sessions;

    /** original session ID from MAC address. */
    private byte[] originalSessionID;

    private static SDVSessionManager instance = new SDVSessionManager(Environment.TUNER_COUNT);

    public static SDVSessionManager getInstance() {
        return instance;
    }

    protected SDVSessionManager(int tunerCount) {
        sessions = new Session[tunerCount];
        originalSessionID = createSessionId();
        for (int i = 0; i < tunerCount; i++) {
            sessions[i] = new Session(createSessionId(i));
        }
    }

    public int getTunerId(ChannelController cc, Channel ch) {
        return 0;
    }

    public Session getSession(int tunerId) {
        return sessions[tunerId];
    }

    public Session getSession(byte[] sessionId) {
        return sessions[sessionId[TUNER_ID_INDEX]];
    }

    public byte[] getSessionId() {
        return originalSessionID;
    }

//    public byte[] getSessionId(ChannelController cc, Channel ch) {
//        return getSessionId(getTunerId(cc, ch));
//    }

    public byte[] createSessionId(int tunerId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.createSessionId("+tunerId+")");
        byte[] id = new byte[10];
        System.arraycopy(originalSessionID, 0, id, 0, 10);
        id[TUNER_ID_INDEX] = (byte) tunerId;
        return id;
    }

    private static byte[] createSessionId() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.createSessionId()");
        String macAddr = Host.getInstance().getReverseChannelMAC();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"SDVSessionManager: getReverseChannelMAC = " + macAddr);
        }
        byte[] sessionID = new byte[10];

        int len = macAddr.length();
        int pos = 0;
        for (int i = 0; i < 10; i++) {
            if (pos + 2 <= len) {
                sessionID[i] = (byte) Short.parseShort(macAddr.substring(pos, pos+2), 16);
                pos += 3;
            } else {
                break;
            }
        }
        return sessionID;
    }

    private Session getSession(ChannelController cc) {
        for (int i = 0; i < sessions.length; i++) {
            if (sessions[i].channelController == cc) {
                return sessions[i];
            }
        }
        return null;
    }

    private Session getIdleSession() {
        for (int i = 0; i < sessions.length; i++) {
            if (sessions[i].status == Session.STATUS_IDLE) {
                return sessions[i];
            } else if (sessions[i].isExpired()) {
                sessions[i].stopAll();
                return sessions[i];
            }
        }
        return null;
    }


    private Session getNotRecordingSession() {
        ArrayList list = new ArrayList(4);
        for (int i = 0; i < sessions.length; i++) {
            if (!sessions[i].isRecording()) {
                list.add(sessions[i]);
            }
        }
        int size = list.size();
        if (size > 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"SDVSessionManager.getNotRecordingSession: size = " + size);
            }
            Session ss = (Session) list.get(0);
            int nis = ss.getDisplay();
            if (nis == 0) {
                return ss;
            }
            // 둘다 걸린 경우..
            for (int i = 1; i < size; i++) {
                Session ss2 = (Session) list.get(i);
                int nis2 = ss2.getDisplay();
                if (nis2 == 0) {
                    return ss;
                } else if (nis2 > nis) {
                    ss = ss2;
                    nis = nis2;
                }
            }
            return ss;
        } else {
            return null;
        }
    }

    private Session getPendingRecordingSession() {
        for (int i = 0; i < sessions.length; i++) {
            if (sessions[i].request <= 0) {
                return sessions[i];
            }
        }
        return null;
    }

    public synchronized Session startChannel(ChannelController cc, Channel ch, boolean selected) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.startChannel("+ch+", "+selected+")");
        if (!SDVController.hasSdvPackage) {
            return null;
        }
        Session old = getSession(cc);
        if (old != null && ch.equals(old.channel) && old.status == Session.STATUS_READY && cc.getState() == ChannelController.CLEAR) {
            // 같은 채널 skip
            Log.printDebug("SDVSessionManager: same channel in this sesssion = " + old);
            return old;
        }
        Session ns = null;
        int sid = ch.getSourceId();
        boolean recording = false;
        // recording만 하고 있는 session 체크
        for (int i = 0; i < sessions.length; i++) {
            Session ss = sessions[i];
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"SDVSessionManager: check[" + i + "] = " + ss.request + ", " + ss.sourceId + ", " + ss.channelController);
            }
            if (ss.isRecording() && ss.sourceId == sid && ss.channelController == null) {
                ns = ss;
                recording = true;
            }
        }
        if (ns == null) {
            if (old != null && !old.isRecording()) {
                ns = old;
            } else {
                ns = getIdleSession();
                if (ns == null) {
                    Log.printError("SDVSessionManager: can't allocate session = " + ch);
                    dumpSessions();
                    updateSessions();
                    ns = old;
                    if (ns == null) {
                        Log.printError("SDVSessionManager: old session is null");
                        ns = getPendingRecordingSession();
                        if (ns == null) {
                            ns = sessions[0];
                        }
                    }
                }
            }
        }
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"SDVSessionManager.startChannel(): target = " + ns);
        }

        if (old != ns && old != null) {
            old.channelController = null;
            if (!old.isRecording()) {
                sendTuneOut(old);
            } else {
                sendTunerUseChange(old);
            }
        }

        ns.channelController = cc;
        ns.channel = ch;
        if (ns.isRecording()) {
            sendTunerUseChange(ns);
            ns.setChannel(0, ch, Session.STATUS_READY);
            if (!selected) {
                cc.select(ch.getService());
            }
        } else {
            ns.request = 0;
            int videoIndex = cc.getVideoIndex();
            byte tunerUse = getTunerUse(false, false, videoIndex + 1, false);
            sendProgramRequest(ns, ch, tunerUse);
            ns.setChannel(0, ch, (ch instanceof SdvChannel) ? Session.STATUS_PENDING : Session.STATUS_READY);
        }
        return ns;

//        // TODO - 일단은 무조건 보내보자....
//        if (old == null) {
//            old = getIdleSession();
//            if (old == null) {
//                Log.printError("SDVSessionManager: can't allocate session = " + ch);
//                return;
//            }
//            old.channelController = cc;
//        }
//        int tunerIndex = cc.getTunerIndex();
//        byte tunerUse = getTunerUse(false, false, tunerIndex + 1, false);
//
//        SDBProgramSelectRequest programSelectRequest = new SDBProgramSelectRequest(ch, tunerUse, SDVController.serviceGroupID);
//        programSelectRequest.setSessionID(old.getSessionId());
//        con.push(programSelectRequest);
//        old.setChannel(0, ch, (ch instanceof SdvChannel) ? Session.STATUS_PENDING : Session.STATUS_READY);
    }

    // for test
    public void sendTuneOutAll() {
        for (int i = 0; i < sessions.length; i++) {
            sendTuneOut(sessions[i]);
        }
    }

    public void dumpSessions() {
        if (Log.DEBUG_ON) {
            Log.printDebug("SDVSessionManager.dumpSessions");
            for (int i = 0; i < sessions.length; i++) {
                sessions[i].dump();
            }
        }
    }

    public synchronized void stopChannel(ChannelController cc) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.stopChannel()");
        if (!SDVController.hasSdvPackage) {
            return;
        }
        Session old = getSession(cc);
        if (old == null) {
            Log.printInfo("SDVSessionManager: no session for " + cc);
            return;
        }
        if (!old.isRecording()) {
            sendTuneOut(old);
        } else {
            old.channelController = null;
            sendTunerUseChange(old);
        }
    }

    public synchronized void actionKeepAlive() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.actionKeepAlive()");
        ChannelController cc = ChannelController.get(0);
        Session ss = getSession(cc);
        if (ss != null && ss.status == Session.STATUS_UNAVAILABLE) {
            synchronized (cc) {
                Channel ch = cc.getCurrent();
                cc.stopChannel();
                cc.changeChannel(ch);
            }
        }
    }

    /** Returns locator. */
    public String requestSdvRecording(int sourceId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.requestSdvRecording("+sourceId+")");
        if (!SDVController.hasSdvPackage) {
            return null;
        }
        Object ret = null;
        try {
            ret = getLocatorString(sourceId);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (ret != null && ret instanceof String) {
            return (String) ret;
        } else {
            String str = getLocatorFromMc(sourceId);
            Log.printError("SDVSessionManager: locator from MC = " + str);
            if (str != null) {
                return str;
            }
            if (ret != null) {
                return ret.toString();
            }
            return null;
        }
    }


    private Object getLocatorString(int sourceId) {
        dumpSessions();
//        updateSessions();
        Session ss = findSessionForRecording(sourceId);
        if (ss != null) {
            // tuner use change
            synchronized (this) {
//                ss.request = r;
                ss.request = -sourceId;
                sendTunerUseChange(ss);
                if (ss.channel != null) {
                    return ss.channel.getLocatorString();
                }
            }
        }
        ss = getIdleSession();
        if (ss == null) {
            Log.printWarning("SDVSessionManager: finding another session for recording SDV.");
            ss = getNotRecordingSession();
            if (ss == null) {
                Log.printError("SDVSessionManager: can't allocate session for recording SDV = " + sourceId);
                return new Integer(RecordingFailedException.INSUFFICIENT_RESOURCES);
            }
            ss.previousController = ss.channelController;
            ss.channelController = null;
        } else {
            ss.previousController = null;
        }
        Log.printDebug("SDVSessionManager: session for sdv recording = " + ss);
        ChannelDatabase db = ChannelDatabase.current;
        if (db == null) {
            return null;
        }
        Channel ch = db.getChannelById(sourceId);
        if (ch == null) {
            return null;
        }

        synchronized (ss) {
            synchronized (this) {
                ss.waitStartTime = System.currentTimeMillis();
                ss.request = -sourceId;
                ss.sourceId = sourceId;
                ss.channel = ch;
                ss.status = Session.STATUS_PENDING;
                // program select request
                SDBProgramSelectRequest msg = new SDBProgramSelectRequest(ch, ss.getTunerUse(), SDVController.serviceGroupID);
                msg.setSessionID(ss.getSessionId());
                con.push(msg);
            }
            try {
                ss.wait(Constants.MS_PER_MINUTE);
            } catch (InterruptedException ex) {
                Log.print(ex);
            }
        }
        Log.printDebug("Session status = " + Session.getStatusString(ss.status) + " " + ss.channelController);

        String ret = null;
        if (ss.channel != null && ss.channel.getSourceId() == sourceId && ss.status == Session.STATUS_READY) {
            ret = ss.channel.getLocatorString();
        }
        if (ret == null || ret.equals(SdvChannel.DUMMY_SDV_LOCATOR.toExternalForm())) {
            Log.printError("SDVSessionManager: failed to recording sdv channel");
            ss.stopAndRecover();
            return new Integer(RecordingFailedException.OUT_OF_BANDWIDTH);
        }
        if (ss.channel != null) {
            return ss.channel.getLocatorString();
        } else {
            return null;
        }
    }

    private String getLocatorFromMc(int sid) {
        try {
            // 현재 알고 있는 정보로 tune 한다.
            McData data = (McData) DataCenter.getInstance().get(MiniCarouselReader.MC_KEY);
            if (data != null) {
                McChannelInfo info = data.get(sid);
                if (info != null) {
                    OcapLocator locator = new OcapLocator((int) info.mCarrierFrequency, info.mProgramNumber, (int) info.mModulationMode);
                    return locator.toExternalForm();
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    public static byte getTunerUse(boolean recording, boolean ppv, int display, boolean tuneFailed) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.getTunerUse("+recording+", "+ppv+", "+display+", "+tuneFailed+")");
        int value = 0;
        value = value | (0xFF & (recording ? 1 : 0));
        value = value | ((0xFF & (ppv ? 1 : 0)) << 2);
        value = value | ((0xFF & display) << 3);
        value = value | ((0xFF & (tuneFailed ? 1 : 0)) << 7);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.getTunerUse() -- "+Integer.toBinaryString(value));
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.getTunerUse() returns (byte)"+((byte)(0xFF & value)));
        return (byte) (0xFF & value);
    }

    public void recordingStarted(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.recordingStarted("+r+")");
        if (!SDVController.hasSdvPackage) {
            return;
        }
        if (AppData.getInteger(r, AppData.SDV_STATUS) == SDV_RECORDING_OK) {
            return;
        }
        int sid = RecordingStatus.getSourceId(r).intValue();
        if (Log.DEBUG_ON) {
            Log.printDebug("SDVSessionManager.recordingStarted: sid = " + sid);
        }
        dumpSessions();
        ChannelDatabase db = ChannelDatabase.current;
        if (db == null) {
            return;
        }
        Channel ch = db.getChannelById(sid);
        if (ch == null) {
            return;
        }
        int rid = r.getId();
        boolean isSdv = ch.getType() == Channel.TYPE_SDV;
        for (int i = 0; i < sessions.length; i++) {
            int sReq = sessions[i].request;
            if (sReq == rid) {
                return;
            }
            if (isSdv && sReq == -sid) {
                sessions[i].request = rid;
                return;
            }
        }

        // SDV가 아닌 경우만, 또는 pending session이 없는 경우.
        synchronized (this) {
            Session ss = findSessionForRecording(sid);
            if (Log.DEBUG_ON) {
                Log.printDebug("SDVSessionManager: recordingStarted = " + ss);
            }
            if (ss != null) {
                // tuner use change
                ss.request = rid;
                sendTunerUseChange(ss);
                return;
            }
            ss = getIdleSession();
            if (ss == null) {
                Log.printError("SDVSessionManager: can't allocate session for recording = " + sid);
                return;
            }
            ss.request = rid;
            ss.sourceId = sid;
            ss.channel = ch;
            ss.status = Session.STATUS_READY;
            // program select request
            SDBProgramSelectRequest msg = new SDBProgramSelectRequest(ch, ss.getTunerUse(), SDVController.serviceGroupID);
            msg.setSessionID(ss.getSessionId());
            con.push(msg);
        }
    }

    public synchronized void recordingStopped(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.recordingStopped("+r+")");
        if (AppData.getInteger(r, AppData.SDV_STATUS) == SDV_RECORDING_PENDING) {
            return;
        }
        Log.printInfo("SDVSessionManager: recording stopped");
        for (int i = 0; i < sessions.length; i++) {
            Session ss = sessions[i];
            if (ss.request == r.getId()) {
                if (ss.channelController == null) {
                    // tune out
                    sendTuneOut(ss);
                } else {
                    // tuner change
                    ss.request = 0;
                    sendTunerUseChange(ss);
                }
                ss.previousController = null;
            }
        }
    }

    // Note - dummy sdv가 실패한 경우는 넘어가고, 그렇지 않은 경우만 처리한다.
    //        즉 RecordManager의 경우와 반대
    public synchronized void recordingFailed(RecordingRequest req) {
    }

    public synchronized void notifyRequestedSdvRecordingFailed(int sid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.notifyRequestedSdvRecordingFailed("+sid+")");
        for (int i = 0; i < sessions.length; i++) {
            Session ss = sessions[i];
            if (ss.request == -sid) {
                if (ss.channelController == null) {
                    ChannelController cc = ss.previousController;
                    if (cc != null) {
                        ss.stopAndRecover();
                    } else {
                        // tune out
                        sendTuneOut(ss);
                    }
                } else {
                    // tuner change
                    ss.request = 0;
                    sendTunerUseChange(ss);
                }
            }
        }
    }

    private synchronized Session findSessionForRecording(int sid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.findSessionForRecording("+sid+")");
        ArrayList list = new ArrayList(4);
        Session ss;
        for (int i = 0; i < sessions.length; i++) {
            ss = sessions[i];
            if (ss.sourceId == sid && ss.channelController != null && !ss.isRecording()) {
                list.add(ss);
            }
        }
        return pickSession(list);
    }

    private Session pickSession(ArrayList list) {
        int size = list.size();
        if (size > 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"SDVSessionManager.pickSession: size = " + size);
            }
            Session ss = (Session) list.get(0);
            int nis = ss.channelController.getNetworkInterfaceIndex();
            // 둘다 걸린 경우..
            for (int i = 1; i < size; i++) {
                Session ss2 = (Session) list.get(i);
                int nis2 = ss2.channelController.getNetworkInterfaceIndex();
                if (nis2 < nis) {
                    ss = ss2;
                    nis = nis2;
                }
            }
            return ss;
        } else {
            return null;
        }
    }

    private void updateSessions() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.updateSessions()");
        for (int i = 0; i < sessions.length; i++) {
            sessions[i].update();
        }
    }

    private void sendTuneOut(Session ss) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.sendTuneOut("+ss+")");
        byte tunerUse = getTunerUse(false, false, 0, false);
        SDBProgramSelectRequest msg = new SDBProgramSelectRequest(null, tunerUse, SDVController.serviceGroupID);
        msg.setSessionID(ss.getSessionId());
        con.push(msg);
        ss.stopAll();
    }

    private void sendTunerUseChange(Session ss) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.sendTunerUseChange("+ss+")");
        ss.previousController = null;
        SDBEventIndication msg = new SDBEventIndication(ss.sourceId, ss.getTunerUse());
        msg.setSessionID(ss.getSessionId());
        con.push(msg);
    }

    private void sendProgramRequest(Session ss, Channel ch, byte tunerUse) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVSessionManager.sendProgramRequest("+ss+", "+ch+", "+tunerUse+")");
        SDBProgramSelectRequest programSelectRequest = new SDBProgramSelectRequest(ch, tunerUse, SDVController.serviceGroupID);
        programSelectRequest.setSessionID(ss.getSessionId());
        con.push(programSelectRequest);
    }

}

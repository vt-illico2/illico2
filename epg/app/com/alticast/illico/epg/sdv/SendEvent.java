package com.alticast.illico.epg.sdv;

import java.util.Calendar;

import com.alticast.illico.epg.sdv.ccp.SDBMessage;
import com.alticast.illico.epg.sdv.ccp.TransactionID;
import com.videotron.tvi.illico.log.Log;

/**
 * @author pellos SDVProgramSelectRequest -> sourceID, tunerUse, serviceGroupID SDVUserActivityReport -> sourceID,
 *         lastUserActivity SDVInitRequest -> serviceGroupID SDVQueryConfirm -> response, sourceID, tunerUse,
 *         serviceGroupID, lastUserActivity SDVEventResponse -> response
 */
public class SendEvent implements Runnable {

    private SDBMessage message;
    private TransactionID transaction;

    public SendEvent(SDBMessage msg) {
        this(msg, null);
    }

    public SendEvent(SDBMessage msg, TransactionID tid) {
        this.message = msg;
        this.transaction = tid;
    }

    public void run() {
        boolean sent = SDVManager.getInstance().sendMessage(message, transaction);
        if (sent) {
            SDVController.getInstance().notifyMessageSent(message);
        } else {
            SDVController.getInstance().notifyMessageSentFailed(message);
        }
    }
}

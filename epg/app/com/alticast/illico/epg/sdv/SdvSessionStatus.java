package com.alticast.illico.epg.sdv;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.DiagScreen;
import com.alticast.illico.epg.navigator.*;
import com.alticast.ui.*;
import java.awt.*;
import org.dvb.ui.DVBColor;
import java.util.*;
import java.text.*;
import org.davic.net.tuning.*;
import javax.tv.locator.*;
import org.ocap.shared.dvr.*;

public class SdvSessionStatus extends DiagScreen {

    // session info
    public static final int NAME = 0;
    public static final int SESSION_ID = 1;
    public static final int SAMSVCID_TYPE = 2;
    public static final int SOURCE_ID = 3;
    public static final int ACT_TIME = 4;
    public static final int RETRIES_RESENDS = 5;
    public static final int RETUNES = 6;
    public static final int TUNER_STATUS = 7;
    public static final int TUNER_USE = 8;
    public static final int TV_REC_RSRC = 9;
    public static final int SDV_FREQ = 10;
    public static final int LAST_CCP_ERR = 11;
    public static final int SESSION_ERR_TIME = 12;

    public static final String[] LABLES = {
        "Name - Status:",
        "Session Id:",
        "SamsvcId/type:",
        "Source ID:",
        "Act Time:",
        "Retries/Resends:",
        "Retunes:",
        "Tuner Status:",
        "Tuner Use:",
        "Tv/Rec Rsrc:",
        "SDV Freq:",
        "Last CCP Err:",
        "Err Time:",
    };

    Session[] sessions;
    String mac;

    public SdvSessionStatus() {
        super("SDV Session & Channels", new DVBColor(0, 0, 0, 160));
        sessions = SDVSessionManager.getInstance().sessions;
        byte[] addr = SDVSessionManager.getInstance().getSessionId();
        StringBuffer sb = new StringBuffer(40);
        for (int i = 0; i < 6; i++) {
            byte b = addr[i];
            sb.append(Character.forDigit(b >> 4 & 0xf, 16));
            sb.append(Character.forDigit(b & 0xf, 16));
            sb.append('.');
        }
        sb.append("??");
        mac = sb.toString().toUpperCase();
    }

    public int getCount() {
        return sessions.length;
    }

    ////////////////////////////////////////////////////////////////////////

    private static final int ROW_GAP = 19;
    private static final int COL_GAP = 200;

    private static final int Y1 = 85;
    private static final int X1 = 200;
    private static final int X2 = 210;
    private static final int X3 = 600;

    Font font = FontResource.BLENDER.getFont(20);
    Color c1 = Color.yellow;
    Color c2 = Color.orange;
    Color c3 = new Color(120, 255, 100);

    public void paintBody(Graphics g) {
        if (Environment.TUNER_COUNT <= 2) {
            paintBodyNormal(g);
        } else {
            paintBodyMany(g);
        }
    }

    private void paintBodyMany(Graphics g) {
        int y = Y1;
        int x = X2;
        g.setColor(Color.cyan);
        g.drawString("SessionID = " + mac, 55, y);
        y += ROW_GAP;

        // NI
        g.setColor(Color.yellow);
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        TunerManager tm = TunerManager.getInstance();
        for (int i = 0; i < tm.size(); i++) {
            g.drawString("NI[" + i + "]: " + tm.getTunerStatus(i).getStatus(), 55, y);
            y += ROW_GAP;
        }
        // SdvSession
        g.setColor(Color.orange);
        for (int i = 0; i < sessions.length; i++) {
            Session ss = sessions[i];
            g.drawString("Session[" + i + "]: " + ss.getStatus(SdvSessionStatus.NAME)
                                         + ", " + ss.getStatus(SdvSessionStatus.SOURCE_ID)
                                         + ", " + ss.getStatus(SdvSessionStatus.TUNER_USE), 55, y);
            y += ROW_GAP;
        }

        // channel controller
        g.setColor(Color.cyan);
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController cc = ChannelController.get(i);
            g.drawString(cc.toString(), 55, y);
            y += ROW_GAP;
        }
        // GC
        g.setColor(Color.green);
        g.drawString(HeapManager.getInstance().getStatus(), 55, y);
        y += ROW_GAP;

        g.setColor(Color.orange);
        g.drawString(getMonitorStatus(), 55, y);
        y += ROW_GAP;

        g.setColor(Color.cyan);
        g.drawString(EpgCore.getInstance().readyChecker + " ; " + EpgDataManager.getInstance() + " ; " + ChannelController.get(0).lastRecording, 55, y);
        y += ROW_GAP;

        g.setColor(Color.yellow);
        g.drawString(SDVManager.getInstance().getThreadStatus(), 55, y);
        y += ROW_GAP;

        g.setColor(Color.green);
        StringBuffer sb = new StringBuffer();
        Vector vector = RecordingStatus.getInstance().getInProgressRecordings();
        int size = vector.size();
        sb.append("Recordings in progress (");
        sb.append(size);
        sb.append(") : ");
        for (int i = 0; i < size; i++) {
            RecordingRequest req = (RecordingRequest) vector.elementAt(i);
            int state = -1;
            int id = 0;
            try {
                state = req.getState();
                id = req.getId();
            } catch (Exception ex) {
            }
            sb.append('#');
            sb.append(id);
            sb.append('(');
            sb.append(state);
            sb.append(')');
            sb.append(',');
            sb.append(' ');
        }
        g.drawString(sb.toString(), 55, y);
    }

    private void paintBodyNormal(Graphics g) {
        g.setColor(Color.orange);
        int y = Y1 + ROW_GAP;
        for (int i = 0; i < LABLES.length; i++) {
            GraphicUtil.drawStringRight(g, LABLES[i], X1, y);
            y += ROW_GAP;
        }
        int x = X2;
        for (int i = 0; i < sessions.length; i++) {
            g.setColor(Color.cyan);
            y = Y1;
            g.drawString("Session " + i, x, y);
            y += ROW_GAP;
            paintSession(g, sessions[i], x, y);
            x += COL_GAP;
        }
        y = Y1 + ROW_GAP + ROW_GAP / 3 + LABLES.length * ROW_GAP;
        // channel controller
        g.setColor(Color.cyan);
        for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
            ChannelController cc = ChannelController.get(i);
            g.drawString(cc.toString(), 55, y);
            y += ROW_GAP;
        }
        // NI
        g.setColor(Color.yellow);
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        TunerManager tm = TunerManager.getInstance();
        for (int i = 0; i < tm.size(); i++) {
            g.drawString("NetworkInterface[" + i + "]: " + tm.getTunerStatus(i).getStatus(), 55, y);
            y += ROW_GAP;
        }
        // GC
        g.setColor(Color.green);
        g.drawString(HeapManager.getInstance().getStatus(), 55, y);
        y += ROW_GAP;

        g.setColor(Color.orange);
        g.drawString(getMonitorStatus(), 55, y);
        y += ROW_GAP;

        g.setColor(Color.cyan);
        g.drawString(EpgCore.getInstance().readyChecker + " ; " + EpgDataManager.getInstance() + " ; " + ChannelController.get(0).lastRecording, 55, y);
        y += ROW_GAP;

        g.setColor(Color.yellow);
        g.drawString(SDVManager.getInstance().getThreadStatus(), 55, y);

        y = Y1;
        x = X3;
        g.setColor(Color.green);
        Vector vector = RecordingStatus.getInstance().getInProgressRecordings();
        int size = vector.size();
        g.drawString("Recordings in progress (" + size + ")", x, y);
        g.setColor(Color.orange);
        for (int i = 0; i < size; i++) {
            y += ROW_GAP;
            RecordingRequest req = (RecordingRequest) vector.elementAt(i);
            int state = -1;
            int id = 0;
            try {
                state = req.getState();
                id = req.getId();
            } catch (Exception ex) {
            }
            g.setColor(Color.white);
            g.drawString("ID=" + id + ", " + getStateString(state), x, y);
            y += ROW_GAP;
            g.setColor(Color.yellow);
            g.drawString("CH #" + AppData.get(req, AppData.CHANNEL_NUMBER) + " " + AppData.get(req, AppData.CHANNEL_NAME) + " " + AppData.getString(req, AppData.TITLE), x, y);
            y += ROW_GAP;
            g.setColor(Color.orange);
            long from = AppData.getScheduledStartTime(req);
            long to = from + AppData.getScheduledDuration(req);
            Formatter fm = Formatter.getCurrent();
            g.drawString("time: " + fm.getTime(from) + " - " + fm.getTime(to) + ",  tuner=" + AppData.get(req, "NI index"), x, y);
            y += ROW_GAP;
        }
    }
/*
    private static String toString(NetworkInterface ni) {
        ChannelController cc = ChannelController.getChannelController(ni);
        String link = "";
        if (cc != null) {
            link = " <> CC[" + cc.getTunerIndex() + "]";
        }
        Locator loc = ni.getLocator();
        String locStr;
        if (loc != null) {
            locStr = loc.toString();
            if (locStr.startsWith("ocap://f=0x")) {
                int index = locStr.indexOf('.');
                if (index > 11) {
                    try {
                        locStr = locStr + " (" + (Integer.parseInt(locStr.substring(11, index), 16) / 1000000) + "MHz)";
                    } catch (Exception ex) {
                    }
                }
            }
        } else {
            locStr = "N/A";
        }
        return (ni.isReserved() ? "reserved" : "free") + ", " + locStr + link;
    }
*/
    private void paintSession(Graphics g, Session ss, int x, int sy) {
        g.setColor(Color.white);
        int y = sy;
        for (int i = 0; i < LABLES.length; i++) {
            g.drawString(ss.getStatus(i), x, y);
            y += ROW_GAP;
        }
    }

    private String getMonitorStatus() {
        StringBuffer sb = new StringBuffer(80);
        sb.append("Monitor Status: ");
        MonitorService ms = CommunicationManager.getMonitorService();
        if (ms == null) {
            sb.append("not bound IXC");
        } else {
            try {
                int mode = ms.getSTBMode();
                switch (mode) {
                    case MonitorService.STB_MODE_NORMAL:
                        sb.append("STB_MODE_NORMAL");
                        break;
                    case MonitorService.STB_MODE_STANDALONE:
                        sb.append("STB_MODE_STANDALONE");
                        break;
                    case MonitorService.STB_MODE_NO_SIGNAL:
                        sb.append("STB_MODE_NO_SIGNAL");
                        break;
                    case MonitorService.STB_MODE_MANDATORY_BLOCKED:
                        sb.append("STB_MODE_MANDATORY_BLOCKED");
                        break;
                    case MonitorService.STB_MODE_BOOTING:
                        sb.append("STB_MODE_BOOTING");
                        break;
                    default:
                        sb.append("STB_MODE_UNKNOWN");
                        break;
                }
                sb.append(" ; ");
            } catch (Throwable ex) {
            }

            try {
                int state = ms.getState();
                switch (state) {
                    case MonitorService.TV_VIEWING_STATE:
                        sb.append("TV_VIEWING_STATE");
                        break;
                    case MonitorService.FULL_SCREEN_APP_STATE:
                        sb.append("FULL_SCREEN_APP_STATE");
                        break;
                    case MonitorService.PIP_STATE:
                        sb.append("PIP_STATE");
                        break;
                    default:
                        sb.append("UNKNOWN_STATE");
                        break;
                }
                sb.append(" ; ");
            } catch (Throwable ex) {
            }

            try {
                sb.append("O,A,V [ ");
                sb.append(ms.getCurrentOverlappingApplicationName());
                sb.append(" , ");
                sb.append(ms.getCurrentActivatedApplicationName());
                sb.append(" , ");
                sb.append(EpgCore.getInstance().getVideoContextName());
                sb.append(" ]");
            } catch (Throwable ex) {
            }
        }
        return sb.toString();
    }

    public static String getStateString(int state) {
        switch (state) {
            case LeafRecordingRequest.COMPLETED_STATE:
                return "COMPLETED_STATE";
            case LeafRecordingRequest.DELETED_STATE:
                return "DELETED_STATE";
            case LeafRecordingRequest.FAILED_STATE:
                return "FAILED_STATE";
            case LeafRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                return "IN_PROGRESS_INCOMPLETE_STATE";
            case LeafRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                return "IN_PROGRESS_INSUFFICIENT_SPACE_STATE";
            case LeafRecordingRequest.IN_PROGRESS_STATE:
                return "IN_PROGRESS_STATE";
            case LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                return "IN_PROGRESS_WITH_ERROR_STATE";
            case LeafRecordingRequest.INCOMPLETE_STATE:
                return "INCOMPLETE_STATE";
            case LeafRecordingRequest.PENDING_NO_CONFLICT_STATE:
                return "PENDING_NO_CONFLICT_STATE";
            case LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                return "PENDING_WITH_CONFLICT_STATE";
            default:
                return "UNKNOWON_STATE (" + state + ")";

        }
    }

}

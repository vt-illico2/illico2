package com.alticast.illico.epg.sdv.mc;

public class SdvConfigParams {
	public long mLuaReportingInterval;
	public long mMiniCarouselReadInterval;
	public long mMessageResponseTimeout;
	public long mMessageRequestMaxRetriesCount;
	public long mMessageRequestRetryInterval;
	public long mBandwidthReclaimUiTimeout;
}

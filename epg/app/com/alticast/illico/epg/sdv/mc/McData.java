package com.alticast.illico.epg.sdv.mc;

import java.util.Hashtable;
import com.videotron.tvi.illico.log.Log;
import java.io.UnsupportedEncodingException;

public class McData {

    private static final int MAX_CHANNEL_TO_STRING = 20;

//    public static McData EMPTY_MC = new McData();

    // key = sid, value = channelInfo
    public Hashtable channelTable = new Hashtable();

//	public ChannelInfo[] channelInfos;
//	private IpResource ipResource;
	public SdvConfigParams sdvConfigParams;
//	public long serviceGroupId;
    public int transportStreamId;
    public int version;
    public int bytes;

    public int channels;

    public String ip;
    public int port;

    public String ip1;
    public String ip2;

    public String string;

    private McData() {
    }

    public McData(McSection[] sections) {
        ip1 = sections[0].ipResource.mIpAddrPrimary;
        ip2 = sections[0].ipResource.mIpAddrSecondary;
        if (ip1 == null || ip1.length() == 0) {
            ip = ip2;
        } else {
            ip = ip1;
        }
        port = sections[0].ipResource.mIpPort;

        sdvConfigParams = sections[0].mSdvConfigParams;
        transportStreamId = sections[0].transportStreamId;
        version = sections[0].version;

        StringBuffer sb = new StringBuffer();
        int count = 0;
        for (int i = 0; i < sections.length; i++) {
            bytes = bytes + sections[i].size;
            McChannelInfo[] infos = sections[i].channelInfos;
            if (infos != null) {
                channels = channels + infos.length;
                for (int j = 0; j < infos.length; j++) {
                    if (infos[j] != null) {
                        channelTable.put(new Integer(infos[j].mSourceId), infos[j]);
                        if (count < MAX_CHANNEL_TO_STRING) {
                            sb.append(infos[j].mSourceId);
                            count++;
                            if (count == MAX_CHANNEL_TO_STRING) {
                                sb.append("...");
                            } else {
                                sb.append(",");
                            }
                        }
                    }
                }
            }
        }
        string = sb.toString();
    }

    public McChannelInfo get(int sourceId) {
        return (McChannelInfo) channelTable.get(new Integer(sourceId));
    }

    public String toString() {
        return string;
    }
}

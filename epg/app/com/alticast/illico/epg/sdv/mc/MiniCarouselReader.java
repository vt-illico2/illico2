package com.alticast.illico.epg.sdv.mc;

import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.sdv.*;
import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;

import org.davic.mpeg.sections.*;
import org.davic.net.tuning.*;
import org.davic.net.Locator;
import org.davic.resources.*;
import javax.tv.util.*;
import org.ocap.net.*;
import java.util.*;

/**
 * This class read Mini Carousel from Section Filter.
 *
 * @author  June Park
 */
//[2014.11.06]Kenneth+June : Energy Star tuner off 때문에 정준이 엄청 수정많이한 클래스임. 
public class MiniCarouselReader implements SectionFilterListener, ResourceClient,
                                    NetworkInterfaceListener, TVTimerWentOffListener,
                                    ResourceStatusListener {

    // TODO - 2 tuner에 대한 고려를 해보자. NI[0] 대신 2 NI에 모두 SF를 걸 필요가 있을까?

    public static final String MC_KEY = "MiniCarousel";
    public static final String MINI_CAROUSEL_FREQUENCIES = "MINI_CAROUSEL_FREQUENCIES";
    public static final String MINI_CAROUSEL_MODULATION = "MINI_CAROUSEL_MODULATION";

    private static final int MC_PID = 0x1FEE;
    private static final int MC_TID = 0xC9;

    private static final boolean LOAD_WITHOUT_PACKAGE = true;

    private static final long DEFAULT_MC_TUNE_TIMEOUT = 60 * Constants.MS_PER_SECOND;
    public static long mcTuneTimeout = DEFAULT_MC_TUNE_TIMEOUT;

    private static final long SECTION_FILTER_TIMEOUT = 10 * Constants.MS_PER_SECOND;

    private boolean enabled = true;
    private SectionFilterGroup sfGroup;
    private TableSectionFilter sectionFilter;
//  private NetworkInterface networkInterface;
    private TVTimerSpec reloadTimer;

    private int serviceGroup = 0;

    private static MiniCarouselReader instance = new MiniCarouselReader();

    private ServiceGroupDiscovery sdg;

    public static TuningInfo tunedInfo;

    NetworkInterfaceController nic = new NetworkInterfaceController(this);

    private NetworkInterface readNI;    // 성공적으로 MC를 읽은 NI
    private Locator readLocator;

    private long reloadDelay = Constants.MS_PER_MINUTE;
    private long lastLoadTime;

    private static final Short FILTER_LOCK = new Short((short) 987);
    private static final Byte TIMER_LOCK = new Byte((byte) 123);

    public static MiniCarouselReader getInstance() {
        return instance;
    }

    private MiniCarouselReader() {
        reloadTimer = new TVTimerSpec();
        reloadTimer.setDelayTime(reloadDelay);
        reloadTimer.setRepeat(true);
        reloadTimer.setRegular(false);
        reloadTimer.addTVTimerWentOffListener(this);

        mcTuneTimeout = DataCenter.getInstance().getLong("MINI_CAROUSEL_TUNE_TIMEOUT");
        if (mcTuneTimeout <= 0) {
            mcTuneTimeout = DEFAULT_MC_TUNE_TIMEOUT;
        }

        sfGroup = new SectionFilterGroup(1);
        sectionFilter = sfGroup.newTableSectionFilter();
        sectionFilter.addSectionFilterListener(this);

        SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "Init");
    }

    private TuningInfo[] getTuningInfos() {
        //->Kenneth[2017.9.26] R7.4 : SdvBarker 는 더이상 사용하지 않음. epg_config_oob.txt 에 있는 tuning 정보 사용
        // 또한 Alex 요구사항에 의해서 DNCS 가 같은 정보만 돌려줘야 함. 따라서 application.prop 에 있는 애들은 사용하지
        // 않도록 한다. 
        if (Log.DEBUG_ON) Log.printDebug("MiniCarouselReader.getTuningInfos()");
        EpgConfig epgConfig = ConfigManager.getInstance().epgConfig;
        if (epgConfig == null) return null;
        Vector mcTuningInfos = epgConfig.mcTuningInfos;
        if (Log.DEBUG_ON) Log.printDebug("MiniCarouselReader.getTuningInfos() : mcTuningInfos = "+mcTuningInfos);
        if (mcTuningInfos == null) return null;
        Vector list = new Vector();
        int size = mcTuningInfos.size();
        if (Log.DEBUG_ON) Log.printDebug("MiniCarouselReader.getTuningInfos() : size = "+size);
        if (Log.DEBUG_ON) Log.printDebug("MiniCarouselReader.getTuningInfos() : EpgCore.dncsName = "+EpgCore.dncsName);
        if (size == 0 || EpgCore.dncsName == null) return null;
        for (int i = 0 ; i < size ; i++) {
            TuningInfo tmp = (TuningInfo)mcTuningInfos.get(i);
            if (EpgCore.dncsName.equalsIgnoreCase(tmp.dncs)) {
                if (Log.DEBUG_ON) Log.printDebug("MiniCarouselReader.getTuningInfos() : added = "+tmp);
                list.addElement(tmp);
            }
        }
        TuningInfo[] info = new TuningInfo[list.size()];
        list.copyInto(info);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader: TuningInfo size = " + info.length);
        return info;
        /*
        Vector list = new Vector();

        SdvBarker sb = (SdvBarker) DataCenter.getInstance().waitForData(ConfigManager.SDV_BARKER_INSTANCE,
                                                Constants.MS_PER_MINUTE);
        TuningInfo[] tuningInfos = null;
        if (sb != null) {
            TuningInfo[] sbInfos = sb.tuningInfos;
            if (sbInfos != null) {
                for (int i = 0; i < sbInfos.length; i++) {
                    list.addElement(sbInfos[i]);
                }
            }
        }
        int mod = DataCenter.getInstance().getInt(MINI_CAROUSEL_MODULATION);
        String[] freq = TextUtil.tokenize(DataCenter.getInstance().getString("MINI_CAROUSEL_FREQUENCIES"), ',');
        if (freq != null) {
            for (int i = 0; i < freq.length; i++) {
                try {
                    int f = Integer.parseInt(freq[i]) * 1000000;
                    TuningInfo ti = new TuningInfo(f, mod);
                    if (!list.contains(ti)) {
                        list.addElement(ti);
                    }
                } catch (Exception ex) {
                }
            }
        }
        TuningInfo[] info = new TuningInfo[list.size()];
        list.copyInto(info);
        Log.printDebug(App.LOG_HEADER+"MiniCarouselReader: TuningInfo size = " + info.length);
        return info;
        */
    }

    /**
     * Starts mc discovery. When it ended, MC will be stored into DataCenter.
     */
    public void selectHomeChannel() {
        if (Log.DEBUG_ON) {
            Log.printDebug("############################################################");
            Log.printDebug(App.LOG_HEADER+"Start --> MiniCarouselReader.selectHomeChannel");
            Log.printDebug("############################################################");
        }
        if (App.EMULATOR) {
            processFailed();
            return;
        }
        SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "Waiting OC");
        TuningInfo[] tuningInfos = getTuningInfos();

        if (LOAD_WITHOUT_PACKAGE || SDVController.hasSdvPackage) {
            SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "SgDiscovery");

            McData data = null;
            if (tuningInfos != null) {
                for (int i = 0; i < tuningInfos.length; i++) {
                    SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "SgDiscovery " + (i+1) + "/" + tuningInfos.length);
                    this.sdg = new ServiceGroupDiscovery(tuningInfos[i]);
                    data = sdg.read(mcTuneTimeout);
                    if (data != null) {
                        tunedInfo = tuningInfos[i];
                        break;
                    }
                }
            }
            this.sdg = null;
            if (data == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader: failed to discovery MC.");
                processFailed();
            }
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader : Call processFailded(No Permission)");
            processFailed("No Permission");
        }
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader : NetworkInterfaceController.release()");
            nic.release();
        } catch (NotOwnerException ex) {
            Log.print(ex);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (tunedInfo == null && tuningInfos.length > 0) {
            tunedInfo = tuningInfos[0];
        }
        if (tunedInfo != null) {
            SharedMemory.getInstance().put(SharedDataKeys.EPG_MC_FREQ, new Integer(tunedInfo.frequency));
        }

        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        for (int i = 0; i < nis.length; i++) {
            nis[i].addNetworkInterfaceListener(this);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("############################################################");
            Log.printDebug(App.LOG_HEADER+"End --> MiniCarouselReader.selectHomeChannel");
            Log.printDebug("############################################################");
        }
    }

    private boolean attachAndFilter(NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.attachAndFilter("+ni+")");
        if (!attach(ni)) {
            return false;
        }
        if (!startFiltering(ni)) {
            return false;
        }
        return true;
    }

    private boolean attach(NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.attach");
        try {
            sfGroup.attach(ni.getCurrentTransportStream(), this, new Object());
//            int index = ChannelController.getNetworkInterfaceIndex(ni);
//            SdvDiagnostics.set(SdvDiagnostics.TVP_TV_ID, index);
            return true;
        } catch (Exception ex) {
            Log.print(ex);
            Log.printWarning("MiniCarouselReader: attach failed.");
            return false;
        }
    }

    private boolean startFiltering(NetworkInterface ni) {
        synchronized (FILTER_LOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.startFiltering");
            try {
                sectionFilter.setTimeOut(SECTION_FILTER_TIMEOUT);
                sectionFilter.startFiltering(ni, MC_PID, MC_TID);
                return true;
            } catch (Exception ex) {
                Log.print(ex);
                Log.printWarning("MiniCarouselReader: failed to startFiltering.");
                return false;
            }
        }
    }

    private void stopFiltering() {
        if (sectionFilter != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.stopFiltering()");
            sectionFilter.stopFiltering();
        }
    }

    public void sectionFilterUpdate(SectionFilterEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug("##################################################");
            Log.printDebug(App.LOG_HEADER+"Start --> MiniCarouselReader.sectionFilterUpdate = " + e);
            Log.printDebug("##################################################");
        }
        NetworkInterface ni = null;
        if (e instanceof VersionChangeDetectedEvent) {
            SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "McpDiscovery");
            if (enabled) {
                stopFiltering();
                startTimer();
//                startFiltering();
            }
        } else if (e instanceof TimeOutEvent) {
            // does nothing
        } else if (e instanceof EndOfFilteringEvent) {
            TableSectionFilter sectionFilter = (TableSectionFilter) e.getSource();
            if (sdg != null) {
                SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "SgDiscovery - read");
            } else {
                SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "McpDiscFileRead");
            }
            SdvDiagnostics.resetTime(SdvDiagnostics.LAST_LOAD_ATTEMPT);
            String errorMessage = handleSections(sectionFilter);
            if (errorMessage != null) {
                loadFailed(errorMessage, true);
                startTimer();
//                startFiltering();
            } else {
                Object data = e.getAppData();
                if (data != null && data instanceof NetworkInterface) {
                    readNI = (NetworkInterface) data;
                    readLocator = readNI.getLocator();
                }
                // no error
                if (enabled) {
                    Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.sectionFilterUpdate() : Call startTimer()");
                    startTimer();
                }
            }
        } else {
            return;
        }
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.sectionFilterUpdate() : Call nic.release()");
            nic.release();
        } catch (NotOwnerException ex) {
            Log.print(ex);
        } catch (Exception ex) {
            Log.print(ex);
        }
        printTunerState();
        if (Log.DEBUG_ON) {
            Log.printDebug("##################################################");
            Log.printDebug(App.LOG_HEADER+"End --> MiniCarouselReader.sectionFilterUpdate = " + e);
            Log.printDebug("##################################################");
        }
    }

    private String handleSections(TableSectionFilter filter) {
        try {
            Section[] sections = filter.getSections();
            if (sections == null || sections.length == 0) {
                return "NoSections";
            }
            McSection[] mcSections = new McSection[sections.length];
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"MiniCarouselReader: mcSections.length = " + sections.length);
            }
            for (int i = 0; i < sections.length; i++) {
                if (sections[i] != null) {
                    try {
                        mcSections[i] = new McSection(sections[i].getData());
                    } catch (NoDataAvailableException ex) {
                        return "NoDataAvailable[" + i + "]";
                    }
                } else {
                    return "NoSection[" + i + "]";
                }
            }

            McData data = new McData(mcSections);

            DataCenter.getInstance().put(MC_KEY, data);
            int tsId = data.transportStreamId;
            if (tsId == 0) {
                return "SGMismatch";
            }
            int oldServiceGroup = serviceGroup;
            serviceGroup = tsId;

            SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, "CacheReady");

            lastLoadTime = System.currentTimeMillis();
            SdvDiagnostics.resetTime(SdvDiagnostics.LOAD_TIME);
            SdvDiagnostics.increase(SdvDiagnostics.LOAD_COUNT);

            SdvDiagnostics.set(SdvDiagnostics.VERSION, data.version);
            SdvDiagnostics.set(SdvDiagnostics.SIZE, data.bytes + " Bytes");
            SdvDiagnostics.set(SdvDiagnostics.NUM_ENTRIES, data.channels);
            SdvDiagnostics.set(SdvDiagnostics.CHANNELS_MC, data.toString());
            SdvDiagnostics.set(SdvDiagnostics.SERVICE_GP, data.transportStreamId);
            SdvDiagnostics.set(SdvDiagnostics.PRI_IP_PORT, data.ip1 + "-" + data.port);
            SdvDiagnostics.set(SdvDiagnostics.SEC_IP_PORT, data.ip2 + "-" + data.port);

            if (data.sdvConfigParams != null) {
                SdvConfigParams p = data.sdvConfigParams;
                SdvDiagnostics.set(SdvDiagnostics.MESSAGE_RESPONSE_TIMEOUT, p.mMessageResponseTimeout + "ms");
                SdvDiagnostics.set(SdvDiagnostics.MESSAGE_REQUEST_RETRY_INTERVAL, p.mMessageRequestRetryInterval + "ms");
                SdvDiagnostics.set(SdvDiagnostics.BANDWIDTH_RECLAIM_UI_TIMEOUT, p.mBandwidthReclaimUiTimeout);
                SdvDiagnostics.set(SdvDiagnostics.MINI_CAROUSEL_READ_INTERVAL, p.mMiniCarouselReadInterval + "s");
                SdvDiagnostics.set(SdvDiagnostics.LUA_REPORTING_INTERVAL, p.mLuaReportingInterval + "s");
                SdvDiagnostics.set(SdvDiagnostics.MESSAGE_REQUEST_MAX_RETRIES_COUNT, p.mMessageRequestMaxRetriesCount);

                long globalTimeout = p.mMessageResponseTimeout +
                    p.mMessageRequestMaxRetriesCount * (p.mMessageResponseTimeout + p.mMessageRequestRetryInterval);
                SdvDiagnostics.set(SdvDiagnostics.GLOBAL_TIMEOUT, globalTimeout + "ms");

                UserActivityChecker.getInstance().setTimerDelay(p.mLuaReportingInterval * Constants.MS_PER_SECOND);
                setReloadInterval(p.mMiniCarouselReadInterval * Constants.MS_PER_SECOND);

                RetryHandler.getInstance().setConfig(p);
            }
            SDVController.getInstance().setMcData(data);

            if (oldServiceGroup != 0 && oldServiceGroup != serviceGroup) {
                loadFailed("SGMismatch", false);
            }
            return null;
        } catch (Exception ex) {
            return ex.toString();
        }
    }

    private void loadFailed(String reason, boolean failed) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.loadFailed("+reason+", "+failed+")");
        if (failed) {
            SdvDiagnostics.increase(SdvDiagnostics.LOAD_FAILURES);
        }
        SdvDiagnostics.set(SdvDiagnostics.LAST_LOAD_ERR, reason);
        SdvDiagnostics.resetTime(SdvDiagnostics.ERR_TIME);
    }

    public void receiveNIEvent(NetworkInterfaceEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.receiveNIEvent = " + e);
        }
        printTunerState();
        if (!enabled) {
            return;
        }
        if (e instanceof NetworkInterfaceTuningOverEvent) {
            NetworkInterface ni = (NetworkInterface) e.getSource();
            NetworkInterfaceTuningOverEvent nitoe = (NetworkInterfaceTuningOverEvent) e;
            if (nitoe.getStatus() == NetworkInterfaceTuningOverEvent.SUCCEEDED) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.receiveNIEvent : call attachAndFilter(ni)");
                attachAndFilter(ni);
            }
        }
    }

    private boolean checkRecentlyLoaded() {
        return System.currentTimeMillis() < lastLoadTime + reloadDelay + Constants.MS_PER_MINUTE;
    }

    private void processFailed() {
        processFailed("Failed");
    }

    private void processFailed(String message) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.processFailed("+message+")");
        DataCenter dc = DataCenter.getInstance();
        if (dc.get(MC_KEY) == null) {
            SdvDiagnostics.set(SdvDiagnostics.MC_STATUS, message);
            dc.put(SDVController.INIT_RESULT, message);
        }
    }

    private void startTimer() {
        if (!enabled) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.startTimer = " + reloadTimer.getTime());
        }
        synchronized (TIMER_LOCK) {
            stopTimer();
            TVTimer timer = TVTimer.getTimer();
            try {
                timer.scheduleTimerSpec(reloadTimer);
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

    private void stopTimer() {
        synchronized (TIMER_LOCK) {
            TVTimer.getTimer().deschedule(reloadTimer);
        }
    }

    private void setReloadInterval(long delayTime) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.setReloadInterval("+delayTime+")");
        delayTime = Math.max(delayTime, 30 * Constants.MS_PER_SECOND);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader : delayTime => "+delayTime+"ms");
        if (reloadDelay != delayTime) {
            this.reloadDelay = delayTime;
            stopTimer();
            reloadTimer.setTime(delayTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.setReloadInterval() : call startTimer()");
            startTimer();
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (enabled) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.timerWentOff() : Call processRead()");
            processRead();
        }
    }

    public void statusChanged(ResourceStatusEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug("MiniCarouselReader.ResourceStatusEvent = " + e);
        }
//        if (e instanceof NetworkInterfaceReservedEvent) {
//            reservedNI = (NetworkInterface) e.getSoruce();
//        } else if (e instanceof NetworkInterfaceReleasedEvent) {
//            reservedNI = null;
//        }
    }

    public boolean requestRelease(ResourceProxy proxy, Object data) {
        if (proxy instanceof NetworkInterfaceController) {
            // Does nothing here.
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.requestRelease() returns true");
            return true;
        } else if (proxy instanceof SectionFilterGroup) {
            // Never release the resource (section filter group)
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.requestRelease() returns false");
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.requestRelease() returns true");
        return true;
    }

    public void release(ResourceProxy proxy) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.release()");
    }

    public void notifyRelease(ResourceProxy proxy) {
    }

    private void processRead() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.processRead()");
        NetworkInterface ni = readNI;
        Locator loc = readLocator;
        if (ni != null && ni.isReserved() && loc != null && loc.equals(ni.getLocator())) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.processRead: using existing tuner");
            boolean result = attachAndFilter(ni);
            if (result) {
                return;
            }
        }
        if (getFreeTuner() != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.processRead() : Call forceTune()");
            forceTune();
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.processRead: no free tuner");
        }
    }

    public void forceTune() {
        forceTune(null);
    }

    public void forceTune(NetworkInterface ni) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.forceTune("+ni+")");
        TuningInfo info = tunedInfo;
        if (info == null) {
            if (Log.DEBUG_ON) Log.printError("MiniCarouselReader.forceTune: not found freq");
            return;
        }
        final OcapLocator loc = info.createLocator();
        Thread t = new Thread("MiniCarouselReader.forceTune") {
            public void run() {
                try {
                    printTunerState();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.forceTune() in Thread : Call nic.reserveFor("+loc+")");
                    nic.reserveFor(loc, null);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.forceTune() in Thread : Call nic.tune("+loc+")");
                    nic.tune(loc);
                    printTunerState();
                } catch (NoFreeInterfaceException ex) {
                    Log.printDebug(ex);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        };
        t.start();
    }

    //Kenneth[2014.11.06] : removed due to not using anymore. It's modified during Evergy star turning off tuner.
    /*
    private boolean forceTuneWithFreeTuner() {
        if (getFreeTuner() != null) {
            forceTune();
            return true;
        } else {
            return false;
        }
    }
    */

    //Kenneth[2014.11.10] tuner 상태를 찍는 로그 method 추가
    private void printTunerState() {
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug("###################");
        if (Log.DEBUG_ON) Log.printDebug("###################");
        if (Log.DEBUG_ON) Log.printDebug("PRINT TUNER STATE");
        if (Log.DEBUG_ON) Log.printDebug("###################");
        if (Log.DEBUG_ON) Log.printDebug("###################");
        if (Log.DEBUG_ON) Log.printDebug("");
        try {
            NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
            for (int i = 0; i < nis.length; i++) {
                NetworkInterface ni = nis[i];
                if (Log.DEBUG_ON) Log.printDebug("+++++++++++++++++++++++++++++++++++++++");
                if (Log.DEBUG_ON) Log.printDebug(i+"th tuner : "+ni);
                if (Log.DEBUG_ON) Log.printDebug("isReserved() : "+ni.isReserved());
                if (Log.DEBUG_ON) Log.printDebug("getLocator(): "+ni.getLocator());
                if (ni == null) continue;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug("+++++++++++++++++++++++++++++++++++++++");
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug("###################");
        if (Log.DEBUG_ON) Log.printDebug("###################");
    }

    private NetworkInterface getFreeTuner() {
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        for (int i = 0; i < nis.length; i++) {
            NetworkInterface ni = nis[i];
            if (ni != null && !ni.isReserved()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.getFreeTuner() returns "+ni);
                return ni;
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.getFreeTuner() returns null");
        return null;
    }

    class ServiceGroupDiscovery implements NetworkInterfaceListener, ResourceClient {
//        NetworkInterfaceController nic;
        TuningInfo info;

        public ServiceGroupDiscovery(TuningInfo ti) {
            this.info = ti;
        }

        public McData read(long timeout) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.read("+timeout+")");
            int freq = info.frequency;
            SdvDiagnostics.set(SdvDiagnostics.DEF_FREQ, freq / 1000000);

            NetworkInterface ni;
//            nic = new NetworkInterfaceController(ServiceGroupDiscovery.this);
            try {
                printTunerState();
                OcapLocator loc = info.createLocator();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.read() : Call nic.reserveFor("+loc+")");
                nic.reserveFor(loc, null);
                ni = nic.getNetworkInterface();
                ni.addNetworkInterfaceListener(ServiceGroupDiscovery.this);

                synchronized (this) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ServiceGroupDiscovery.tune = " + loc.toExternalForm());
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.read() : Call nic.tune("+loc+")");
                    nic.tune(loc);
                    try {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.read() : Call wait("+timeout+")");
                        wait(timeout);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
                printTunerState();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.read() : after wait()");
                ni.removeNetworkInterfaceListener(ServiceGroupDiscovery.this);
                // discovery중에는 Empty를 넣지 않고 처리하도록 수정하자.
                return (McData) DataCenter.getInstance().get(MC_KEY);
            } catch (Exception ex) {
                Log.print(ex);
                return null;
            }
        }

        public void receiveNIEvent(NetworkInterfaceEvent e) {
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.receiveNIEvent = " + e);
                printTunerState();
            }
            if (e instanceof NetworkInterfaceTuningOverEvent) {
                NetworkInterface ni = (NetworkInterface) e.getSource();
                NetworkInterfaceTuningOverEvent nitoe = (NetworkInterfaceTuningOverEvent) e;
                synchronized (this) {
                    boolean success = false;
                    int status = nitoe.getStatus();
                    if (status == NetworkInterfaceTuningOverEvent.SUCCEEDED) {
                        success = attachAndFilter(ni);
                    } else {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.receiveNIEvent: not SUCCEEDED.");
                    }
                    if (!success) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MiniCarouselReader.ServiceGroupDiscovery.receiveNIEvent: notifyAll()");
                        notifyAll();
                    }
                }
            }
        }

        public boolean requestRelease(ResourceProxy proxy, Object data) {
            // Does nothing here.
            return true;
        }

        public void release(ResourceProxy proxy) {
        }

        public void notifyRelease(ResourceProxy proxy) {
        }

    }

}



package com.alticast.illico.epg.sdv.mc;

public class McChannelInfo {
	public String mShortName;
	public short mModulationMode;
	public long mCarrierFrequency;
	public int mChannelTsId;
	public int mProgramNumber;
	public int mServiceType;
	public int mSourceId;

    public String toString() {
        return "McChannelInfo[" +
                "mod = " + mModulationMode + ", " +
                "freq = " + mCarrierFrequency + ", " +
                "tsid = " + mChannelTsId + ", " +
                "pn = " + mProgramNumber + ", " +
                "sType = " + mServiceType + ", " +
                "name = " + mShortName + ", " +
                "sid = " + mSourceId + "]";
    }
}

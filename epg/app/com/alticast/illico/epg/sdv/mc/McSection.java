package com.alticast.illico.epg.sdv.mc;

import com.videotron.tvi.illico.log.Log;
import java.io.UnsupportedEncodingException;

class McSection {

	McChannelInfo[] channelInfos;
	IpResource ipResource;
	SdvConfigParams mSdvConfigParams;
	ServiceGroupId serviceGroupId;
    int transportStreamId;
    int version;
    int size;

	public McSection(byte[] sectionData) {
        size = sectionData.length;

		channelInfos = null;
		ipResource = null;
		mSdvConfigParams = null;
		serviceGroupId = null;

        transportStreamId = parseInt16(sectionData, 3);
        version = (sectionData[5] & 0x3E) >> 1;

		int index = 9;		// pointing num_channels_in_section field

		int channelCount = sectionData[index];

        if (Log.DEBUG_ON) {
		    Log.printDebug("McSection.McSection: channelCount = " + channelCount
                                    + ", transportStreamId = " + transportStreamId);
        }
		index++;

		channelInfos = new McChannelInfo[channelCount];
		for (int i = 0; i < channelCount; i++) {
			channelInfos[i] = parseChannelInfo(sectionData, index);
			index += 32;		// Information for one channel is 32 byte long.
		}

		int mcDescLen = (((int)sectionData[index] & 0x00000003) << 8) |
			((int)sectionData[index + 1] & 0x000000FF);
		index += 2;

		int tmpDescLen;
		while (mcDescLen > 0) {
			tmpDescLen = 2 + sectionData[index + 1];
			switch (sectionData[index]) {
			case 0x03:
				// tmpDescLen should be 12.
				ipResource = parseIpResource(sectionData, index);
				break;
			case 0x04:
				// tmpDescLen should be 28.
				mSdvConfigParams = parseSdvConfigParams(sectionData, index);
				break;
			case 0x06:
				// tmpDescLen should be 6.
				serviceGroupId = parseServiceGroupId(sectionData, index);
				break;
			default:
				// Unknown descriptor. Just ignores it.
				break;
			}
			index += tmpDescLen;
			mcDescLen -= tmpDescLen;
		}
	}

	private static McChannelInfo parseChannelInfo(byte[] data, int idx) {

		McChannelInfo channelInfo = new McChannelInfo();

        try {
		    channelInfo.mShortName = new String(data, idx, 14, "UTF-16BE");
        } catch (UnsupportedEncodingException ex) {
		    channelInfo.mShortName = new String(data, idx, 14);
        }
		channelInfo.mModulationMode = data[idx + 17];
		channelInfo.mCarrierFrequency = parseLong32(data, idx + 18);
		channelInfo.mChannelTsId = parseInt16(data, idx + 22);
		channelInfo.mProgramNumber = parseInt16(data, idx + 24);
		channelInfo.mServiceType = data[idx + 27] & 0x3F;		// [TBD]
		channelInfo.mSourceId = parseInt16(data, idx + 28);

        Log.printDebug(channelInfo);
		return channelInfo;
	}

	private static IpResource parseIpResource(byte[] data, int idx) {

		IpResource ipResource = new IpResource();

		ipResource.mIpPort = parseInt16(data, idx + 2);
		ipResource.mIpAddrPrimary = parseIpAddress(data, idx + 4);
		ipResource.mIpAddrSecondary = parseIpAddress(data, idx + 8);

        if (Log.DEBUG_ON) {
    		Log.printDebug("McSection.parseIpResource: " +
                "IpPort = " + ipResource.mIpPort + ", " +
                "IpAddrPrimary = " + ipResource.mIpAddrPrimary + ", " +
                "IpAddrSecondary = " + ipResource.mIpAddrSecondary);
        }
		return ipResource;
	}

	private static SdvConfigParams parseSdvConfigParams(byte[] data, int idx) {

		SdvConfigParams params = new SdvConfigParams();

		params.mLuaReportingInterval = parseLong32(data, idx + 4);
		params.mMiniCarouselReadInterval = parseLong32(data, idx + 8);
		params.mMessageResponseTimeout = parseLong32(data, idx + 12);
		params.mMessageRequestMaxRetriesCount = parseLong32(data, idx + 16);
		params.mMessageRequestRetryInterval = parseLong32(data, idx + 20);
		params.mBandwidthReclaimUiTimeout = parseLong32(data, idx + 24);

        if (Log.DEBUG_ON) {
    		Log.printDebug("McSection.parseSdvConfigParams: " +
                "mLuaReportingInterval = " + params.mLuaReportingInterval + ", " +
                "mMiniCarouselReadInterval = " + params.mMiniCarouselReadInterval + ", " +
                "mMessageResponseTimeout = " + params.mMessageResponseTimeout + ", " +
                "mMessageRequestMaxRetriesCount = " + params.mMessageRequestMaxRetriesCount + ", " +
                "mMessageRequestRetryInterval = " + params.mMessageRequestRetryInterval + ", " +
                "mBandwidthReclaimUiTimeout = " + params.mBandwidthReclaimUiTimeout);
        }
		return params;
	}

	private static ServiceGroupId parseServiceGroupId(byte[] data, int idx) {

		ServiceGroupId serviceGroupId = new ServiceGroupId();
		serviceGroupId.serviceGroupId = parseLong32(data, idx + 2);

        if (Log.DEBUG_ON) {
    		Log.printDebug("McSection.parseServiceGroupId: " +
                "serviceGroupId = " + serviceGroupId.serviceGroupId);
        }
		return serviceGroupId;
	}

	private static int parseInt16(byte[] data, int idx) {

		int result =
			(((int)data[idx] & 0x000000FF) << 8) |
			((int)data[idx+1] & 0x000000FF);

		return result;
	}

	private static int parseInt32(byte[] data, int idx) {

		int result =
			(((int)data[idx] & 0x000000FF) << 24) |
			(((int)data[idx + 1] & 0x000000FF) << 16) |
			(((int)data[idx + 2] & 0x000000FF) << 8) |
			((int)data[idx + 3] & 0x000000FF);

		return result;
	}

	private static long parseLong32(byte[] data, int idx) {

		long result =
			(((long)data[idx] & 0x00000000000000FF) << 24) |
			(((long)data[idx + 1] & 0x00000000000000FF) << 16) |
			(((long)data[idx + 2] & 0x00000000000000FF) << 8) |
			((long)data[idx + 3] & 0x00000000000000FF);

		return result;
	}

    private static String parseIpAddress(byte[] data, int idx) {
        return new String(
            ((int) data[idx] & 0x000000FF) + "." +
            ((int) data[idx + 1] & 0x000000FF) + "." +
            ((int) data[idx + 2] & 0x000000FF) + "." +
            ((int) data[idx + 3] & 0x000000FF)
        );
    }


}

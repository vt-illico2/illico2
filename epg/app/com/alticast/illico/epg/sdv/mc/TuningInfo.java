package com.alticast.illico.epg.sdv.mc;

import org.ocap.net.OcapLocator;
import com.videotron.tvi.illico.log.Log;

public class TuningInfo {
    //->Kenneth[2017.9.26] R7.4 : DNCS 정보 추가
    public String dncs = "";
    //<-
	public int frequency;
	public int modulation;

    public TuningInfo(int freq, int mod) {
        frequency = freq;
        modulation = mod;
    }

    //->Kenneth[2017.9.26] R7.4 : DNCS 정보 추가
    public TuningInfo(String dncs, int freq, int mod) {
        this.dncs = dncs;
        frequency = freq;
        modulation = mod;
    }

    public String toString() {
        return "TuningInfo [DNCS = "+dncs+" : Frequency = "+frequency+" : Modulation = "+modulation+"]";
    }
    //<-

    public boolean equals(Object obj) {
        if (obj instanceof TuningInfo) {
            TuningInfo ti = (TuningInfo) obj;
            return ti.frequency == frequency && ti.modulation == modulation;
        } else {
            return false;
        }
    }

    public OcapLocator createLocator() {
        OcapLocator locator = null;
        try {
            locator = new OcapLocator(frequency, modulation);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return locator;
    }
}

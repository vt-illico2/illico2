package com.alticast.illico.epg.sdv;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.Enumeration;

import org.ocap.hardware.Host;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.ccp.*;
import com.alticast.illico.epg.sdv.mc.McData;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import javax.tv.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.tv.service.selection.*;
import org.dvb.event.*;
import com.opencable.handler.cahandler.*;

public class SDVController implements SDVEventListener, TVTimerWentOffListener {

    public static final boolean USE_TIMER = true;

    public static final int AUTHORIZED     = 0;
    public static final int NOT_AUTHORIZED = 1;
    public static final int EID_NOT_FOUND  = 2;

    public static final String[] PACKAGE_STATUS = {
        "Yes", "No", "no EID"
    };

    public static final String INIT_RESULT = "SdvInitResult";

    public static boolean hasSdvPackage = true;
    public static int serviceGroupID = 0;
    private byte version = 0;

    int sentCount;
    int recvCount;

    McData mcData;

    Worker sendQueue = new Worker("SdvMessageSendQueue");

    TVTimerSpec recheckTimer;

    private static SDVController instance = new SDVController();

    private SDVController() {
        SDVManager.getInstance().setSDVEventListener(this);

        if (USE_TIMER) {
            recheckTimer = new TVTimerSpec();
            recheckTimer.setDelayTime(2 * Constants.MS_PER_MINUTE);
            recheckTimer.setRepeat(true);
            recheckTimer.setRegular(false);
            recheckTimer.addTVTimerWentOffListener(this);
        }
    }

    public static SDVController getInstance() {
        return instance;
    }

    public synchronized void setMcData(McData data) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.setMcData("+data+")");
        serviceGroupID = data.transportStreamId;
        SDVManager.getInstance().setAddress(data.ip, data.port, serviceGroupID);
        this.mcData = data;
    }

//    private synchronized boolean showBarker(String message, int sourceId, boolean byForce) {
//        Log.printInfo("SDVController.showBarker");
//        return ChannelBackground.getInstance().startSdvBarker(message, sourceId, byForce);
//    }

    private synchronized void showKeepAlive(ChannelController cc) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.showKeepAlive("+cc+")");
        cc.getVideoPlane().startKeepAlive();
    }

    public void sendInit() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.sendInit()");
        SDBInitRequest initRequest = new SDBInitRequest(serviceGroupID, version++);
        initRequest.setSessionID(SDVSessionManager.getInstance().getSessionId());
        push(initRequest);
    }

//    public void sendProgramSelect(ChannelController cc, Channel ch) {
//
//    public void startChannel(ChannelController cc, Channel ch) {
//        SDVSessionManager.getInstance().startChannel(cc, ch);
//
//        int tunerIndex = cc.getTunerIndex();
//        byte tunerUse = getTurerUse(false, false, tunerIndex + 1, false);
//        SDBProgramSelectRequest programSelectRequest = new SDBProgramSelectRequest(ch, tunerIndex, tunerUse, serviceGroupID);
//        programSelectRequest.setSessionID(SDVSessionManager.getInstance().getSessionId(cc, ch));
//        push(programSelectRequest);
//    }
//
//    public void stopChannel(ChannelController cc) {
//        // TODO - tune out
//        // check recording...
//    }


    public void sendUserActivity(int sid, long when) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.sendUserActivity("+sid+")");
        SDBMessage msg = new SDBUserActivityReport(sid, (int) (when / 1000L));
        msg.setSessionID(SDVSessionManager.getInstance().getSessionId());
        push(msg);
    }

    void push(SDBMessage msg) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.push("+msg+")");
        sendQueue.push(new SendEvent(msg));
    }

    void push(SDBMessage msg, TransactionID tid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.push("+msg+", "+tid+")");
        sendQueue.push(new SendEvent(msg, tid));
    }


    public void receiveMessage(SDBMessage message, short response) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"receiveMessage ==================================");
            Log.printInfo("message = " + (message.getMessageID() & 0xFFFF));
            Log.printInfo("response = " + response);
            Log.printInfo("==================================");
            Log.printInfo("\n");
            Log.printInfo("");
        }
//        SDVEvent event = null;

        if (message instanceof SDBProgramSelectConfirm) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.receiveMessage() : SDBProgramSelectConfirm");
            SdvDiagnostics.increase(SdvDiagnostics.SELCONF_RX);
            SDBProgramSelectConfirm m = (SDBProgramSelectConfirm) message;
            short res = m.getResponse();
            int sid = m.getSourceID();
            byte[] id = message.getSessionID();
            Session ss = SDVSessionManager.getInstance().getSession(id);
//res = (short) 0x80FF;

            if (res == SDBProgramSelectConfirm.rspOK) {
//                int tunerIndex = id[6];
//                if (tunerIndex < 0 || tunerIndex >= Environment.TUNER_COUNT) {
//                    tunerIndex = 0;
//                }
//                ChannelController cc = ChannelController.get(tunerIndex);
                if (ss != null) {
                    int freq = m.getTuningFrequency();
                    ChannelController cc = ss.channelController;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("SDVController.receiveMessage: cc = " + cc);
                    }
                    boolean done = false;
                    if (cc != null) {
                        synchronized (cc) {
                            Channel ch = cc.getCurrent();
                            if (ch != null && ch instanceof SdvChannel && ch.getSourceId() == sid) {
                                ((SdvChannel) ch).reselect(cc,
                                        freq, m.getMPEGProgramNumber(), m.getModulationFormat());
                                ss.setChannel(freq, ch, Session.STATUS_READY);
                                done = true;
                            } else {
                                Log.printInfo("SDVController.receiveMessage: different request = " + ch + ", " + sid);
                            }
                        }
                    }
                    if (!done) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("SDVController.receiveMessage: session.request = " + ss.request);
                        }
                        if (ss.request == -sid) {
                            ss.status = Session.STATUS_READY;
                            ss.freq = freq / 1000000;
//                            ss.request = 0;
                            Channel ch = ss.channel;
                            if (ch != null && ch.getSourceId() == sid && ch instanceof SdvChannel) {
                                ((SdvChannel) ch).setLocator(freq, m.getMPEGProgramNumber(), m.getModulationFormat());
                            }
                        }
                    }
                    ss.resetRecording(freq, (int) m.getMPEGProgramNumber(), (int) m.getModulationFormat(), sid);
                }
            } else {
                SdvDiagnostics.increase(SdvDiagnostics.SELCONFFAILS_RX);
                if (ss != null) {
                    if (ss.channelController != null) {
                        VideoPlane vp = ss.channelController.getVideoPlane();
                        vp.startSdvError(m.getMessageID(), res, sid, true);
    //                    vp.startSdvError((int) m.getMessageID(), (int) res, sid, false);
                    }
                    ss.status = Session.STATUS_UNAVAILABLE;
                }
//                showBarker(getBarkerMessage(m.getMessageID(), res), sid, false);
                switch (res) {
                    case SDBProgramSelectConfirm.rspUnknownClient:
                    case SDBProgramSelectConfirm.rspInvalidSG:
                        sendInit();
                        break;
                }
            }
            if (ss != null) {
                synchronized (ss) {
                    ss.notifyAll();
                }
            }
        } else if (message instanceof SDBProgramSelectIndication) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.receiveMessage() : SDBProgramSelectIndication");
            SDBProgramSelectIndication m = (SDBProgramSelectIndication) message;
            byte[] id = message.getSessionID();
            int sid = m.getCurrentSourceID();
            short reason = m.getReason();
            Session ss = SDVSessionManager.getInstance().getSession(id);
            ChannelController cc = null;
            if (ss != null) {
                cc = ss.channelController;
            }
            if (cc != null) {
                synchronized (cc) {
                    Channel ch = cc.getCurrent();
                    if (ch != null && ch instanceof SdvChannel && ch.getSourceId() == sid) {
                        switch (reason) {
                            case SDBProgramSelectIndication.FORCE_TUNE:
                                ((SdvChannel) ch).reselect(cc,
                                        m.getTuningFrequency(), m.getMPEGProgramNumber(), m.getModulationFormat());
                                break;
                            case SDBProgramSelectIndication.PROG_UNAVAIL:
                                // Barker를 보여주는 대신 session status를 바꾸기로 했다.
                                // ChannelController.get(tunerIndex).getVideoPlane().startSdvError(m.getMessageID(), reason, sid, true);
                                ss.status = Session.STATUS_UNAVAILABLE;
                                break;
                        }
                    }
                }
            }
            SDBProgramSelectResponse resMsg = new SDBProgramSelectResponse(SDBProgramSelectResponse.rspOk);
            resMsg.setSessionID(id);
            push(resMsg);

        } else if (message instanceof SDBQueryRequest) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.receiveMessage() : SDBQueryRequest");
            Channel ch = ChannelController.get(0).getCurrent();
            int sid = ch != null ? ch.getSourceId() : 0;
            SDBQueryConfirm msg = new SDBQueryConfirm((byte) 0x0000, sid, getTunerUse(0), serviceGroupID,
                    (int) (UserActivityChecker.lastUserActivity / 1000L));
            msg.setSessionID(message.getSessionID());
            msg.setMessageLength((short) 0x000C);
            push(msg);

        } else if (message instanceof SDBEventIndication) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.receiveMessage() : SDBEventIndication");
            byte[] id = message.getSessionID();
            ChannelController cc = null;
            Session session = SDVSessionManager.getInstance().getSession(id);
            if (session != null) {
                cc = session.channelController;
            }
            SDBEventResponse msg = new SDBEventResponse();
            msg.setSessionID(id);
            push(msg);

            if (cc != null) {
                showKeepAlive(cc);
            }

            // TODO - Tuner Use 보내는 메시지가 빠졌음
            // TODO - 구현
//            event = new SDVEvent(SDVConstants.SDVMessageID_SDVEventResponse, message.getSessionID());
//            event.setServiceGroupID(serviceGroupID);
//            event.setMessage(message);
//            eh.putEvent(event);
        }
        recvCount++;
        SdvDiagnostics.set(SdvDiagnostics.TOTAL_TX_RX, sentCount + "/" + recvCount);
    }

    public void notifyMessageSent(SDBMessage message) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent("+message+")");
        sentCount++;
        SdvDiagnostics.set(SdvDiagnostics.TOTAL_TX_RX, sentCount + "/" + recvCount);

        switch (message.getMessageID()) {
            case SDVConstants.SDVMessageID_SDVEventIndication:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVEventIndication");
                SdvDiagnostics.increase(SdvDiagnostics.EVIND_TX);
                break;
            case SDVConstants.SDVMessageID_SDVProgramSelectRequest:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVProgramSelectRequest");
                SdvDiagnostics.increase(SdvDiagnostics.SELREQ_TX);
                if (message instanceof SDBProgramSelectRequest && ((SDBProgramSelectRequest) message).isSdvChannel()) {
                    SdvDiagnostics.increase(SdvDiagnostics.SDV_SELREQ_TX);
                }
                break;
            case SDVConstants.SDVMessageID_SDVUserActivityReport:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVUserActivityReport");
                SdvDiagnostics.increase(SdvDiagnostics.LUA_REP_TX);
                SdvDiagnostics.resetTime(SdvDiagnostics.LUA_TX_TIME);
                break;
            case SDVConstants.SDVMessageID_SDVInitRequest:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVInitRequest");
                SdvDiagnostics.increase(SdvDiagnostics.INITREQ_TX);
                break;
            case SDVConstants.SDVMessageID_SDVQueryConfirm:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVQueryConfirm");
                SdvDiagnostics.increase(SdvDiagnostics.QRYCONF_TX);
                break;
            case SDVConstants.SDVMessageID_SDVEventResponse:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVEventResponse");
                SdvDiagnostics.increase(SdvDiagnostics.EVRESP_TX);
                break;
            case SDVConstants.SDVMessageID_SDVProgramSelectResponse:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyMessageSent() : SDVMessageID_SDVProgramSelectResponse");
                SdvDiagnostics.increase(SdvDiagnostics.SELRESP_TX);
                break;
            }
    }

    public void notifyMessageSentFailed(SDBMessage msg) {
//        switch (msg.getMessageID()) {
//            case SDVConstants.SDVMessageID_SDVProgramSelectRequest:
//                SDBProgramSelectRequest m = (SDBProgramSelectRequest) msg;
//                Session ss = SDVSessionManager.getInstance().getSession(m.getSessionID());
//                if (ss == null) {
//                    return;
//                }
//                boolean show = showBarker(getBarkerMessage(m.getMessageID(), 0), m.getSourceID(), false);
//                if (show) {
//                    ss.status = Session.STATUS_UNAVAILABLE;
//                }
//                break;
//        }
    }

    public void notifyTimeout(SDBMessage msg) {
        Log.printError("SDVController.notifyTimeout = " + msg);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.notifyTimeout("+msg+")");
        switch (msg.getMessageID()) {
            case SDVConstants.SDVMessageID_SDVInitRequest:
                SDVManager.getInstance().initFailed();
                break;
            case SDVConstants.SDVMessageID_SDVProgramSelectRequest:
                SDBProgramSelectRequest m = (SDBProgramSelectRequest) msg;
                SdvDiagnostics.increase(SdvDiagnostics.SELCONFTIMEOUT_RX);
                // select timeout 인 경우에는 보여주지 말자. -> 보여주자.
                byte[] id = msg.getSessionID();
                Session ss = SDVSessionManager.getInstance().getSession(id);
                if (ss != null) {
                    boolean show = false;
                    if (ss.channelController != null) {
                        VideoPlane vp = ss.channelController.getVideoPlane();
                        show = vp.startSdvError((short) 0x0020, (short) 0x3002, m.getSourceID(), false);
                    }
                    if (show)  {
                        ss.status = Session.STATUS_UNAVAILABLE;
                    }
                    synchronized (ss) {
                        ss.notifyAll();
                    }
                }
        }
        SDVManager.getInstance().resetReceiveThread(false);
    }

//    private String getBarkerMessage(int messageId, int responseId) {
//        SdvBarker barker = (SdvBarker) DataCenter.getInstance().get(ConfigManager.SDV_BARKER_INSTANCE);
//        String msg = null;
//        if (barker != null) {
//            msg = barker.getMessage(messageId, responseId);
//        }
//        if (msg == null) {
//            msg = DataCenter.getInstance().getString("sdv.default_msg");
//        }
//        return msg;
//    }

    public byte getTunerUse(int videoIndex) {
        int b = 0;
        Channel ch = ChannelController.get(videoIndex).getCurrent();
        if (ch == null) {
//            b = b | 0x10000000;
        } else {
            b = b | ((videoIndex + 1) << 3);
            if (ch.getType() == Channel.TYPE_PPV) {
                b = b | 0x0100;
            }
            if (RecordingStatus.getInstance().isRecording(ch)) {
                b = b | 0x01;
            }
        }
        return (byte) b;
    }

    public synchronized void setPackage(int reason) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.setPackage("+reason+")");
        SdvDiagnostics.set(SdvDiagnostics.AUTHORIZED, PACKAGE_STATUS[reason]);
        boolean newAuth = reason != NOT_AUTHORIZED;
        boolean oldAuth = hasSdvPackage;
        hasSdvPackage = newAuth;
        if (!oldAuth && newAuth) {
            sendInit();
            Thread t = new Thread() {
                public void run() {
                    try {
                        Thread.sleep(2000L);
                    } catch (Exception ex) {
                    }
                    for (int i = 0; i < Environment.VIDEO_DEVICE_COUNT; i++) {
                        try {
                            ChannelController cc = ChannelController.get(i);
                            synchronized (cc) {
                                Channel ch = cc.getCurrent();
                                if (ch != null && ch instanceof SdvChannel) {
                                    cc.changeChannel(ch);
                                }
                            }
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                }
            };
            t.start();
        }
        if (oldAuth != newAuth) {
            ChannelController.notifyCaUpdated(0, true);
        }
    }

    public void packageUpdated(boolean auth) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.packageUpdated("+auth+")");
        setPackage(auth ? AUTHORIZED : NOT_AUTHORIZED);
    }

    public void startMonitoring() {
        if (USE_TIMER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.startMonitoring()");
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(recheckTimer);
            try {
                recheckTimer = timer.scheduleTimerSpec(recheckTimer);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SDVController.timerWentOff()");
        EpgCore.getInstance().checkSdvPackage();
    }
}

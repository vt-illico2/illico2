package com.alticast.illico.epg.sdv;

import com.alticast.illico.epg.sdv.ccp.SDBMessage;

public interface SDVEventListener {
	 public void receiveMessage(SDBMessage message, short response);
}

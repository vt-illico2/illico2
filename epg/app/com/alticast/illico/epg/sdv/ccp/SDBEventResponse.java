package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVEventResponse message.
 * <br><br>
 * This message is sent by the SDV Client to confirm reception of an
 * SDBEventIndication message from the SDV Server.  However, if the
 * Client originates the SDBEventIndication message, the Server does
 * not need to send a confirmation to the Client. In other words,
 * Client events are not acknowledged by the Server.
 * <br><br>
 *
 * @author dkimble
 *
 */

public class SDBEventResponse extends SDBMessage {

    final short SDVEventResponseMsgLength = 2;

	public static final short rspOk = (short) 0x0000;
	public static final short rspFormatError = (short) 0x0001;

    private short m_response;

    private int[] messageFieldSegments = new int[] { 2 };


   /**
    * constructor
    */
   public SDBEventResponse() {
       this(rspOk);
   } // SDVEventResponse()

   /**
    * constructor with params
    *
    * @param response See the description of {@link #setResponse(short response)}
    * for details on this field.
    *
    */
   public SDBEventResponse(short response) {
      super(SDVConstants.SDVMessageID_SDVEventResponse);
      if (Log.INFO_ON) {
    	  Log.printInfo("SDVEventResponse");
    	  Log.printInfo("\t\tresponse = " + response);
      }

      m_response = response;

      setMessageLength(SDVEventResponseMsgLength);

   } // SDVEventResponse(short response)

   /**
    * Sets the response code for this SDVEventResponse message.
    * <br><br>
    *
    * Values are as follows:
    * <TABLE>
    * <TBODY>
    * <THEAD>
    * <TH>Code
    * <TH>Value
    * <TR><TD>rspOK<TD>0x0000
    * <TR><TD>rspFormatError<TD>0x0001
    * </TBODY>
    * </TABLE>
    *
    * @param response code
    */
   public void setResponse(short response) {
	   if (Log.INFO_ON) {
		   Log.printInfo("SDVEventResponse.setResponse(): response = " + response);
	   }

      // handle illegal response codes
      if ((response < 0) || (response > 1)) {
         Log.printWarning("SDVEventResponse.setResponse() - ERROR: illegal response value = "
                     + response);
         return;
      }

      m_response = response;

   } // setResponse(short response)

   /**
    * Returns the response code for this SDVProgramSelectResponse message.
    *
    * See the description of {@link #setResponse(short response)} for details on
    * this field.
    *
    * @return response
    */
   public long getResponse() {
      return m_response;
   }

   /**
    * Returns an int array which describes the sizes of all fields
    * in this message.
    * <br><br>
    * This allows a hex dump display of this message to include
    * visual cues (e.g. ".") at field boundaries.
    *
    * @return sizes of all fields in this message
    */
   public int[] getMessageFieldSegments() {
      int superNumFields = super.getMessageFieldSegments().length;
      int[] combinedArray = new int[superNumFields
            + messageFieldSegments.length];

      System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
            superNumFields);
      System.arraycopy(messageFieldSegments, 0, combinedArray,
            superNumFields, messageFieldSegments.length);

      return combinedArray;

   } // getMessageFieldSegments()

   /**
    * Serializes this message for transmission to the SDV server (via UDP)
    *
    * @return a byte array containing the entire message, including DSM-CC header
    * and the sessionID from {@link SDBMessage}
    */
   public byte[] toByteArray() {
      byte[] rc = null;
      byte[] s = super.toByteArray();
      ByteArrayOutputStream bo = new ByteArrayOutputStream();
      DataOutputStream out = new DataOutputStream(bo);

      try {
         out.write(s);

         out.writeShort(m_response);

         rc = bo.toByteArray();

         out.close();
         out = null;
      } catch (IOException e) {
         Log.printWarning("SDVEventResponse.toByteArray(): " + e.getMessage());
        Log.print(e);
      }
      finally
      {
         if (out != null)
         {
            try
            {
               out.close();
               out = null;
            }
            catch (IOException ioe)
            {
               // do nothing
            }
         }
      }

      return rc;

   } // toByteArray()

   /**
    * Parses an incoming SDVEventResponse message.
    *
    * @param dis a DataInputStream object containing the SDVEventResponse message
    * @return SDVEventResponse
    */
   public short parse(DataInputStream dis) {
	   if(Log.INFO_ON)
	   {
		   Log.printInfo("SDVEventResponse.parse()");
	   }

      short ret = super.parse(dis);

      if (ret == SDVConstants.FORMAT_OK) {
         try {
            m_response = dis.readShort();
         } catch (IOException e) {
            Log.printWarning("SDVEventResponse.parse(): " + e.getMessage());
            Log.print(e);
         }
      } // if(ret == SDVConstants.FORMAT_OK)

      return ret;

   } // parse(DataInputStream in)

   /**
    * Returns a string describing the contents of this message in human readable
    * form.
    *
    * @return String version of this object
    */
   public String toString() {
      String rc;

      rc = "\n\tSDVEventResponse\n";

      rc += super.toString();

      rc += "\t\tResponse               = " + m_response;

      return rc;

   } // toString()

   /**
    * Returns the size of this message, including DSM-CC header and the
    * sessionID from {@link SDBMessage}
    *
    * @return size of this message
    */
   public int size() {
//      return super.size() + SDVEventResponseMsgLength;
      return 12;
   }

}

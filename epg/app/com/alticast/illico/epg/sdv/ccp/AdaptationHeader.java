package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;

public class AdaptationHeader {

	public static final byte CONDITIONAL_ACCESS = (byte) 0x01;
	public static final byte USER_ID = (byte) 0x02;

	private byte m_adaptationType = (byte) 0xFF; // user type
	private byte[] m_adaptationData = { (byte) 0x00, (byte) 0x00, (byte) 0x00,
			(byte) 0x03, (byte) 0x00, (byte) 0x00, (byte) 0x00, (byte) 0x2D };

//	public AdaptationHeader() {
//	}

	public AdaptationHeader(byte[] ba) {
		m_adaptationData = ba;
	}

//	public void setData(byte[] ba) {
//		m_adaptationData = ba;
//	}

	public int getLength() {
		if (m_adaptationData != null) {
			return m_adaptationData.length + 1;
		} else {
			return 0;
		}
	}

	public byte[] toByteArray() {
		byte[] rc = null;
		ByteArrayOutputStream bo = new ByteArrayOutputStream();

		try {
			bo.write(m_adaptationType);
			bo.write(m_adaptationData);

			rc = bo.toByteArray();

			bo.close();
			bo = null;
		} catch (Exception ex) {
			Log.printWarning(ex);
		} finally {
			if (bo != null) {
				try {
					bo.close();
					bo = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;
	}

	public String toString() {
		byte[] ba = this.toByteArray();
		String rc = "Adaptation Header Data = ";

		for (int i = 0; i < ba.length; i++) {
			rc += "0x" + Integer.toHexString(ba[i]) + " ";
		}
		return rc;
	}

}

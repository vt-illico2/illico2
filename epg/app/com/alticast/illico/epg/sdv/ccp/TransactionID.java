package com.alticast.illico.epg.sdv.ccp;

import com.videotron.tvi.illico.log.Log;

public class TransactionID {

	public static final int TRANSACTION_ID_CLIENT_ORIGINATOR = 0x0000;
	public static final int TRANSACTION_ID_SERVER_ORIGINATOR = 0x4000;
	public static final int TRANSACTION_ID_NETWORK_ORIGINATOR = 0x8000;

	private int m_transactionID = 0;
	private static int s_newTransactionID = 0;
	private int m_originator = 0;

	/**
	 * Contains the unique TID for a DSMCC message from a particular client.
	 *
	 */
	public TransactionID() {
		m_transactionID = createID();
	}

	/**
	 * Constructor with initialization.
	 *
	 * Called by DSMCCMessage parse and setTransactinID
	 *
	 * @param id
	 */
	public TransactionID(int id) {
		m_transactionID = id;
	}

	/**
	 *
	 * createID ======== Creates a new unique TID.
	 *
	 * @return The new ID.
	 */
	private synchronized static int createID() {
		if (s_newTransactionID < Integer.MAX_VALUE) {
			s_newTransactionID++;
		} else {
			// too large wrap the transaction id
			s_newTransactionID = 0;
		}
		return s_newTransactionID;
	}

	/**
	 * getID ===== Returns the TID for this message. Called by DSMCCMessage
	 * DSMCCMessenger
	 *
	 * @return The TID.
	 */
	public int getID() {
		return m_transactionID;
	}

	/**
	 * toString
	 *
	 * @return A human readable version of this object.
	 */
	public String toString() {
		return Integer.toString(m_transactionID);
	}

	/**
	 *
	 * toByteArray ======== Serializes the TIC and returns the four byte
	 * representation of the TID. Called by UNClientSesssionSetupRequest.
	 *
	 * @return 4 byte array containing the TID.
	 */
	public byte[] toByteArray() {
		byte[] id = new byte[4];
		int tid = m_transactionID;

		for (int i = 0; i < 4; i++) {
			id[3 - i] = (byte) (tid & 0xFF);
			tid >>= 8;
		}

		return id;
	}

    public int hashCode() {
        return m_transactionID;
    }

    public boolean equals(Object o) {
        if (o instanceof TransactionID) {
            return ((TransactionID) o).m_transactionID == m_transactionID;
        } else if (o instanceof Integer) {
            return ((Integer) o).intValue() == m_transactionID;
        } else {
            return super.equals(o);
        }
    }

}

package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;
import com.alticast.illico.epg.sdv.Session;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.SdvChannel;
import com.alticast.illico.epg.navigator.ChannelController;

/**
 * This class encapsulates the SDVProgramSelectRequest message. <br>
 * <br>
 * The SDV Client sends this message to the SDV Server when the subscriber tunes
 * to any channel, SDV or non-SDV. <br>
 * <br>
 * This message is sent with sourceID of 0 when the subscriber turns off the STB
 * while it was tuned to an SDV channel or when recording ends. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBProgramSelectRequest extends SDBMessage {

	final short SDVProgramSelectRequestMsgLength = 14;

	private final short m_reservedShort = (short) 0xFFFF;
	private Channel m_channel;
    private boolean isSdv;
	private int m_sourceID = 0;
	// private data
	private final short m_privateDataLength = 6;
	private byte m_tunerUse = 0;
	private final byte m_reservedByte = (byte) 0xFF;
	private int m_serviceGroupID = 0;

	private int[] messageFieldSegments = new int[] { 2, 4, 2, 1, 1, 4 };

	/**
	 * constructor
	 */
	public SDBProgramSelectRequest() {
		super(SDVConstants.SDVMessageID_SDVProgramSelectRequest);
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectRequest");
		}

		setMessageLength((short) size());

	} // SDVProgramSelectRequest()

	/**
	 * constructor with parameters
	 *
	 * @param sourceID
	 *            the sourceID to which SDV client is requesting to tune
	 * @param tunerUse
	 *            how the tuner associated with this message will be used. See
	 *            the description of {@link #setTunerUse(byte tunerUse)} for
	 *            details this parameter.
	 * @param serviceGroupID
	 *            the service group this SDV client is in
	 */
	public SDBProgramSelectRequest(Channel ch, byte tunerUse, int serviceGroupID) {
		super(SDVConstants.SDVMessageID_SDVProgramSelectRequest);
        int sourceID = ch != null ? ch.getSourceId() : 0;
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectRequest");
			Log.printInfo("\t\tsourceID = " + sourceID + " = 0x"
					+ Integer.toHexString(sourceID));
			Log.printInfo("\t\ttunerUse = " + tunerUse);
			Log.printInfo("\t\tserviceGroupID = " + serviceGroupID);
		}
        m_channel = ch;
        isSdv = ch instanceof SdvChannel;
		m_sourceID = sourceID;
		m_tunerUse = tunerUse;
		m_serviceGroupID = serviceGroupID;

		setMessageLength((short) size());

	} // SDVProgramSelectRequest(int sourceID, byte tunerUse, int
		// serviceGroupID)

	/**
	 * Sets the sourceID to which SDV client is requesting to tune.
	 *
	 * @param sourceID
	 */
	public void setSourceID(int sourceID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectRequest.setSourceID(): sourceID = "
					+ sourceID + " = 0x" + Integer.toHexString(sourceID));
		}

		// TODO - should check for illegal sourceID
		m_sourceID = sourceID;

	} // setSourceID(int sourceID)

	/**
	 * @return the sourceID to which SDV client is requesting to tune.
	 */
	public int getSourceID() {
		return m_sourceID;
	}

    public Channel getChannel() {
        return m_channel;
    }

    public boolean isSdvChannel() {
        return isSdv;
    }

    public boolean isExpired() {
        if (m_channel == null) {
            return true;
        }
        Session ss = SDVSessionManager.getInstance().getSession(getSessionID());
        if (ss == null) {
            return true;
        }

        if ((m_tunerUse & 0x01) == 0x00) {
            // not recording
            ChannelController cc = ss.channelController;
            if (cc == null) {
                return true;
            }
            return !m_channel.equals(cc.getCurrent());
        } else {
            return false;
        }
    }

	/**
	 * @return the length of the private data section - this should always be 6
	 *         for an SDVProgramSelectRequest message.
	 */
	public short getPrivateDataLength() {
		return m_privateDataLength;
	}

	/**
	 * Specifies how the tuner associated with this message will be used. <br>
	 * <br>
	 * bit 0 Tuner is being used for a scheduled recording.<br>
	 * 1 = yes<br>
	 * 0 = no<br>
	 * bit 1 Reserved<br>
	 * bit 2 Tuner is being used for Pay-Per-View content.<br>
	 * 1 = yes<br>
	 * 0 = no<br>
	 * bits 3-5 Specifies the display endpoint for the tuner.<br>
	 * 0 = NONE<br>
	 * 1 = MAIN TV<br>
	 * 2 = PIP<br>
	 * values: 3,4,5,6,7 are reserved.<br>
	 *
	 * @param tunerUse
	 *            tuner use associated with this SDVProgramSelectRequest
	 */
	public void setTunerUse(byte tunerUse) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectRequest.setTunerUse()");
			Log.printInfo("\t" + tunerUseToString(tunerUse));
		}

		m_tunerUse = tunerUse;

	} // setTunerUse(byte tunerUse)

	/**
	 * Returns the tuner use for the tuner associated with this message. <br>
	 * <br>
	 * See the description of {@link #setTunerUse(byte tunerUse)} for details on
	 * tuner use.
	 *
	 * @return tuner use
	 */
	public byte getTunerUse() {
		return m_tunerUse;
	}

	/**
	 * Sets the service group this SDV client is in.
	 *
	 * @param serviceGroupID
	 */
	public void setServiceGroupID(int serviceGroupID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectRequest.setServiceGroupID(): serviceGroupID = "
							+ serviceGroupID);
		}

		// TODO - should check for illegal serviceGroupID
		m_serviceGroupID = serviceGroupID;

	} // setServiceGroupID(int serviceGroupID)

	/**
	 * @return the serviceGroup this SDV client is in.
	 */
	public int getServiceGroupID() {
		return m_serviceGroupID;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 *
	 * @return int array which describes the sizes of all fields in this
	 *         message.
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Serializes this message for transmission to the SDV server (via UDP)
	 *
	 * @return a byte array containing the entire message, including DSM-CC
	 *         header and the sessionID from {@link SDBMessage}
	 */
	public byte[] toByteArray() {
		byte[] rc = new byte[0];
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_reservedShort);
			out.writeInt(m_sourceID);
			out.writeShort(m_privateDataLength);
			out.writeByte(m_tunerUse);
			out.writeByte(m_reservedByte);
			out.writeInt(m_serviceGroupID);

			rc = bo.toByteArray();

			out.close();
			out = null;
		} catch (IOException e) {
			Log.printWarning("SDVProgramSelectRequest.toByteArray(): "
					+ e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	} // toByteArray()

	/**
	 * @return a string describing the contents of this message in human
	 *         readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVProgramSelectRequest\n";

		rc += super.toString();

		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedShort & 0xFFFF) + "\n";
		rc += "\t\tsourceID               = " + m_sourceID + "\n";
		rc += "\t\tPrivate Data Length    = " + m_privateDataLength + "\n";
		rc += tunerUseToString(m_tunerUse);
		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedByte & 0xFF) + "\n";
		rc += "\t\tService Group ID       = " + m_serviceGroupID;

		return rc;

	} // toString()

	/**
	 * Returns a string representation of the tunerUse defined in this
	 * SDVProgramSelectRequest message. <br>
	 * <br>
	 * See the description of {@link #setTunerUse(byte tunerUse)} for details on
	 * tuner use.
	 *
	 * @param tunerUse
	 * @return string representation of the tunerUse defined in this
	 *         SDVProgramSelectRequest message.
	 */
	public String tunerUseToString(byte tunerUse) {
		String rc;

		rc = "\t\tTuner Use:\n";
		rc += "\t\t\tIs tuner being used for scheduled recording: ";
		if ((tunerUse & 0x1) == 0)
			rc += "NO\n";
		else
			rc += "YES\n";
		rc += "\t\t\tIs tuner being used for PPV content:         ";
		if ((tunerUse & 0x4) == 0)
			rc += "NO\n";
		else
			rc += "YES\n";
		rc += "\t\t\tThe display endpoint for the tuner is:       ";
		switch ((tunerUse >> 3) & 0x7) {
		case 0:
			rc += "NONE";
			break;
		case 1:
			rc += "MAIN TV";
			break;
		case 2:
			rc += "PIP";
			break;
		}
		rc += "\n";

		return rc;

	} // tunerUseToString(byte tunerUse)

	/**
	 * @return the size of this message, including DSM-CC header and the
	 *         sessionID from {@link SDBMessage}
	 */
	public int size() {
		// return super.size() + SDVProgramSelectRequestMsgLength;
		return 24;
	}

}

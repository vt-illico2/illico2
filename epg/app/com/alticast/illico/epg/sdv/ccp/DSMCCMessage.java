
package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class is an implementation of the ISO/IEC 13818-6 DSM-CC message header
 * as described in Table 2-1 in chapter 2 of that spec.
 *
 * @author dkimble
 *
 */

public abstract class DSMCCMessage {
	// number of bytes before the variable length adaptation field - if present.
	public static final int INVARIANT_HEADER_SIZE = 12;

	// This will always be 0x11 indicating this is an MPEG-2 DSMCC message.
	public static final byte MPEG2_DSMCC_MESSAGE = (byte) 0x11;

	private byte m_protocolDiscriminator = MPEG2_DSMCC_MESSAGE;

	// This will always be 0x04 indicating this is an SDV CC msg
	//
	public static final byte SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE = (byte) 0x04;

	private byte m_dsmccType = SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE;

	// SDV message type
	protected short m_messageID;

	// A unique number for this particular message but with a definite format.
	private TransactionID m_transactionID = null;

	// Always 0xFF per ISO/IEC1318-6:1998(E)
	private byte m_reserved = (byte) 0xFF;

	// The length of the adaptation header if it is included. Else 0.
	private byte m_adaptationLength = (byte) 0;

	// The length of the entire message from adaptation length forward.
	private short m_messageLength;

	// The adaptation header -- if included.
	private AdaptationHeader m_adaptationHeader = null;

	private int[] messageFieldSegments = new int[] { 1, 1, 2, 4, 1, 1, 2 };

	/**
	 * constructor
	 */
	public DSMCCMessage(short id) {
        m_messageID = id;
	}

	/**
	 * Returns the protocol discriminator - this should always be
	 * MPEG2_DSMCC_MESSAGE = 0x11.
	 *
	 * @return protocol discriminator
	 */
	public byte getProtocolDiscriminator() {
		return m_protocolDiscriminator;
	}

	/**
	 * Returns the DSMCC msg type - this should always be
	 * SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE = 0x04.
	 *
	 * @return DSMCC type
	 */
	public byte getDSMCCType() {
		return m_dsmccType;
	}

	/**
	 * Sets the message header message ID.
	 *
	 * @param id
	 *            Message ID
	 */
//	public void setMessageID(short id) {
//		m_messageID = id;
//	}

	/**
	 * Returns the message ID.
	 *
	 * @return message ID
	 */
	public short getMessageID() {
		return m_messageID;
	}

	/**
	 * Sets the tranactionID on this message. <br>
	 * <br>
	 * This method will generate a new internal transaction ID.
	 */
	public void setTransactionID() {
		m_transactionID = new TransactionID();
	}

	/**
	 * Sets the tranactionID on this message.
	 *
	 * @param id
	 *            the TransactionID object to use.
	 */
	public void setTransactionID(TransactionID id) {
		m_transactionID = id;
	}

	/**
	 * Returns the TransactionID object for this message.
	 *
	 * @return transactionID
	 */
	public TransactionID getTransactionID() {
		return m_transactionID;
	}

	/**
	 * setAdaptationLength ======== Sets the adaptation header length. The
	 * default is 0 if this method is never called. Per SA, no adaptation data
	 * is required even though the STB currently sends some.
	 *
	 * @param len
	 */
	public void setAdaptationLength(byte len) {
		m_adaptationLength = len;
	}

	/**
	 * getAdaptationLength ======== Getter for the adaptation header length.
	 *
	 * @return The adaptation header length.
	 */
	public byte getAdaptationLength() {
		return m_adaptationLength;
	}

	public void setMessageLength(short messageLength) {
		m_messageLength = messageLength;
	}

	/**
	 * getMessageLength ======== Returns the total message length of this
	 * message
	 *
	 * @return message length
	 */
	public int getMessageLength() {
		return m_messageLength;
	}

	/**
	 *
	 * setAdaptationHeader ======== Sets the adaptation header for this message.
	 * Note that the default is to have no adaptation header.
	 *
	 * @param he
	 *            The AdaptationHeader to add to this header. See 2.1 of the
	 *            DSM-CC spec.
	 */
	public void setAdaptationHeader(AdaptationHeader he) {
		m_adaptationHeader = he;
		if (he != null) {
			m_adaptationLength = (byte) he.getLength();
		} else {
			m_adaptationLength = 0;
		}
	}

	/**
	 * Returns an int array which describes the sizes of all fields in the
	 * DSM-CC header. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 *
	 * @return int array which describes the sizes of all fields in the DSM-CC
	 *         header.
	 */
	public int[] getMessageFieldSegments() {
		return messageFieldSegments;

	} // getMessageFieldSegments()

	/**
	 * Serializes the DSM-CC header
	 *
	 * @return a byte array containing the DSM-CC header
	 */
	public byte[] toByteArray() {
		byte[] rc = null;
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.writeByte(m_protocolDiscriminator);
			out.writeByte(m_dsmccType);
			out.writeShort(m_messageID);
			out.writeInt(m_transactionID.getID());
			out.writeByte(m_reserved);
			out.writeByte(m_adaptationLength);
			out.writeShort(m_messageLength);

			rc = bo.toByteArray();

			out.close();
			out = null;

		} catch (IOException e) {
			Log.printWarning("DSMCCMessage.toByteArray(): " + e.getMessage());
            Log.print(e);
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;
	}

	/**
	 * parse -------
	 *
	 * @param dis
	 *            DataInputStream from a Datagram
	 *
	 * @return FORMAT_ERROR or FORMAT_OK
	 */
	public short parse(DataInputStream dis) {
		try {
			m_protocolDiscriminator = dis.readByte();
			if (m_protocolDiscriminator != MPEG2_DSMCC_MESSAGE)
				return SDVConstants.FORMAT_ERROR;

			m_dsmccType = dis.readByte();
			if (m_dsmccType != SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE)
				return SDVConstants.FORMAT_ERROR;

			m_messageID = dis.readShort();

			m_transactionID = new TransactionID(dis.readInt());

			m_reserved = dis.readByte();
			if ((m_reserved & 0xFF) != 0xFF)
				return SDVConstants.FORMAT_ERROR;

			m_adaptationLength = dis.readByte();

			m_messageLength = dis.readShort();

			if (m_adaptationLength != 0) {
				byte[] adaptBytes = new byte[m_adaptationLength];
				dis.read(adaptBytes, 0, m_adaptationLength);
				m_adaptationHeader = new AdaptationHeader(adaptBytes);
			}
		} catch (IOException e) {
			Log.printWarning("DSMCCMessage.parse(): " + e.getMessage());
            Log.print(e);
		}
		return SDVConstants.FORMAT_OK;
	}

	/**
	 * Returns a string describing the contents of this DSMCC header in human
	 * readable form.
	 *
	 * @return String version of this object
	 */
	public String toString() {
		String rc;

		rc = "\t\tDSM-CC header\n";
		rc += "\t\t\tProtocol Discriminator = 0x"
				+ Integer.toHexString(m_protocolDiscriminator)
				+ " = "
				+ ((m_protocolDiscriminator != MPEG2_DSMCC_MESSAGE) ? "ERROR - NOT "
						: "") + "MPEG DSM-CC message\n";
		rc += "\t\t\tDSMCC Type             = 0x"
				+ Integer.toHexString(m_dsmccType)
				+ " = "
				+ ((m_dsmccType != SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE) ? "ERROR - NOT "
						: "") + "SDV_CHANNEL_CHANGE_PROTOCOL_MESSAGE\n";
		rc += "\t\t\tMessage ID             = 0x"
				+ Integer.toHexString(m_messageID & 0xFFFF) + "\n";
		rc += "\t\t\tTransaction ID         = " + m_transactionID + "\n";
		rc += "\t\t\tReserved               = 0x"
				+ Integer.toHexString(m_reserved & 0xFF) + "\n";
		rc += "\t\t\tAdaptation Length      = " + m_adaptationLength + "\n";
		rc += "\t\t\tMessage Length         = " + m_messageLength + "\n";

		if (m_adaptationLength != 0 && m_adaptationHeader != null) {
			rc += m_adaptationHeader.toString() + "\n";
		}

		return rc;
	}

	/**
	 * Returns the size of the DSMCC header and adaptation field of this
	 * message.
	 *
	 * @return size of this DSMCC header and adaptation field
	 */
	public int size() {
		return INVARIANT_HEADER_SIZE + m_adaptationLength;
	}
}

package com.alticast.illico.epg.sdv.ccp;

import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVInitConfirm message. <br>
 * <br>
 * The SDV SERVER sends this message to the SDV CLIENT in response to
 * SDBInitRequest. The SDV SERVER will send this message with failure response
 * code if the signaled service group is not a recognized element of the SDV
 * topology. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBInitConfirm extends SDBMessage {

	final short SDVInitConfirmMsgLength = 2;

	private short m_response;
	private final short rspOK = 0x0000;
	private final short rspInvalidSG = (short) 0x8001;
	private final short rspSrvCapacityExceeded = (short) 0x8002;
	private final short rspFormatError = (short) 0x0001;
	private final short rspVerNotSupported = (short) 0x8007;
	private final short rspUnknownError = (short) 0x80FF;

	private int[] messageFieldSegments = new int[] { 2 };

	/**
	 * constructor
	 */
	public SDBInitConfirm() {
		super(SDVConstants.SDVMessageID_SDVInitConfirm);
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitConfirm");
		}

	} // SDVInitConfirm()


	// test Pellos
	private int m_serviceGroupID = 0;
	private byte m_version;
	public SDBInitConfirm(int serviceGroupID, byte version) {
		super(SDVConstants.SDVMessageID_SDVInitConfirm);
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitRequest");
			Log.printInfo("\t\tserviceGroupID = " + serviceGroupID);
			Log.printInfo("\t\tversion = " + version);
		}

		m_serviceGroupID = serviceGroupID;
		m_version = version;

		setMessageLength((short) size());

	}

	// test Pellos
	public void setResponse(short res) {
		m_response = res;
	}

/**
	 * Returns the response code from the previous SDVInitRequest message.
	 * <br><br>
	 *
	 * Values are as follows:
	 * <TABLE>
	 * <TBODY>
	 * <THEAD>
	 * <TH>Code
	 * <TH>Value
	 * <TR><TD>rspOK<TD>0x0000
	 * <TR><TD>rspInvalidSG<TD>0x8001
	 * <TR><TD>rspSrvCapacityExceeded<TD>0x8005
	 * <TR><TD>rspFormatError<TD>0x0001
	 * <TR><TD>rspVerNotSupported<TD>0x8007
	 * <TR><TD>rspUnknownError<TD>0x80FF
	 * </TBODY>
	 * </TABLE>
	 */
	public long getResponse() {
		return m_response;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Parses an incoming SDVInitConfirm message.
	 *
	 * @param dis
	 *            a DataInputStream object containing the SDVInitConfirm message
	 */
	public short parse(DataInputStream dis) {
		short ret = super.parse(dis);

		if (ret == SDVConstants.FORMAT_OK) {
			try {
				m_response = dis.readShort();
				if (m_response != SDVConstants.FORMAT_OK) {
					Log.printWarning("SDVInitConfirm.parse(): ERROR - m_response = "
									+ responseToString(m_response));
					return SDVConstants.FORMAT_ERROR;
				}
			} catch (IOException e) {
				Log.printWarning("SDVInitConfirm.parse(): " + e.getMessage());
                Log.print(e);
			}
		} // if(ret == SDVConstants.FORMAT_OK)

		return ret;

	} // parse(DataInputStream in)

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVInitConfirm\n";

		rc += super.toString();

		rc += "\t\tResponse               = 0x"
				+ Integer.toHexString(m_response & 0xFFFF) + " = "
				+ responseToString(m_response) + "\n";

		return rc;

	} // toString()

	/**
	 * Returns a string representation of the response code.
	 *
	 * See the description of {@link #getResponse()} for details on this field.
	 */
	public String responseToString(short response) {
		switch (response) {
		case rspOK:
			return "rspOK";
		case rspInvalidSG:
			return "rspInvalidSG";
		case rspSrvCapacityExceeded:
			return "rspSrvCapacityExceeded";
		case rspFormatError:
			return "rspFormatError";
		case rspVerNotSupported:
			return "rspVerNotSupported";
		case rspUnknownError:
			return "rspUnknownError";
		default:
			return "ERROR - ILLEGAL RESPONSE CODE!";
		}

	} // responseToString()

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 */
	public int size() {
		return super.size() + SDVInitConfirmMsgLength;
	}

}

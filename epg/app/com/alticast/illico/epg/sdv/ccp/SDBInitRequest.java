package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVInitRequest message. <br>
 * <br>
 * This message is sent from the SDV CLIENT to the SDV SERVER. This
 * initialization message shall be sent sent on power button push or when
 * set-top configuration data change. <br>
 * <br>
 * The SDV CLIENT shall re-send this message to the SDV SERVER, in case of any
 * scenarios listed below:<br>
 * 1. The Controlling SDV SERVER IP address or listening port parameters change
 * in the Dynamic Channel Map (DCM).<br>
 * 2. The Service Group ID changes in the DCM.<br>
 * 3. SDV SERVER sends response code RspSeSGNotFound in SDBProgramSelectConfirm
 * Message. <br>
 * <br>
 * Note that if the secondary SDV Server takes over following a protection
 * switch or failure of the primary SDV Server, the SDV Client does not need to
 * reinitialize with the secondary. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBInitRequest extends SDBMessage {

	final short SDVInitRequestMsgLength = 10;

	private short m_reservedShort = (short) 0xFFFF;
	private int m_serviceGroupID = 0;
	private byte m_version;
	private byte m_reservedByte = (byte) 0xFF;
	private final short m_numDesc = 0;

	private int[] messageFieldSegments = new int[] { 2, 4, 1, 1, 2 };

	/**
	 * constructor
	 */
	public SDBInitRequest() {
		super(SDVConstants.SDVMessageID_SDVInitRequest);
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitRequest");
		}

		setMessageLength((short) size());

	} // SDVInitRequest()

	/**
	 * constructor with parameters
	 *
	 * @param serviceGroupID
	 *            the service group this SDV client is in
	 * @param version
	 *            the version of this SDV client
	 */
	public SDBInitRequest(int serviceGroupID, byte version) {
		super(SDVConstants.SDVMessageID_SDVInitRequest);
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitRequest");
			Log.printInfo("\t\tserviceGroupID = " + serviceGroupID);
			Log.printInfo("\t\tversion = " + version);
		}

		m_serviceGroupID = serviceGroupID;
		m_version = version;

		setMessageLength((short) size());

	} // SDVInitRequest(int serviceGroupID, byte version)

	/**
	 * Sets the service group this SDV client is in.
	 *
	 * @param serviceGroupID
	 */
	public void setServiceGroupID(int serviceGroupID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitRequest.setServiceGroupID(): serviceGroupID = "
					+ serviceGroupID);
		}

		// TODO - should check for illegal serviceGroupID
		m_serviceGroupID = serviceGroupID;

	} // setServiceGroupID(int serviceGroupID)

	/**
	 * Returns the serviceGroup this SDV client is in.
	 *
	 * @return serviceGroupID
	 */
	public int getServiceGroupID() {
		return m_serviceGroupID;
	}

	/**
	 * Sets the version of this SDV client
	 *
	 * @param version
	 */
	public void setVersion(byte version) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVInitRequest.setVersion(): version = " + version);
		}

		// TODO - should check for illegal version
		m_version = version;

	} // setVersion(byte version)

	/**
	 * Returns the current version of this SDV client
	 *
	 * @return version
	 */
	public byte getVersion() {
		return m_version;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 *
	 * @return int array which describes the sizes of all fields in this message
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Serializes this message for transmission to the SDV server (via UDP)
	 *
	 * @return a byte array containing the entire message, including DSM-CC
	 *         header and the sessionID from {@link SDBMessage}
	 */
	public byte[] toByteArray() {
		byte[] rc = null;
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s); // includes sessionID

			out.writeShort(m_reservedShort);
			out.writeInt(m_serviceGroupID);
			out.writeByte(m_version);
			out.writeByte(m_reservedByte);
			out.writeShort(m_numDesc);

			rc = bo.toByteArray();

			out.close();
			out = null;

		} catch (IOException e) {
			Log.printWarning("SDVInitRequest.toByteArray(): " + e.getMessage());
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	} // toByteArray()

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 *
	 * @return String version of this object
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVInitRequest\n";

		rc += super.toString();

		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedShort & 0xFFFF) + "\n";
		rc += "\t\tService Group ID       = " + m_serviceGroupID + "\n";
		rc += "\t\tVersion                = " + m_version + "\n";
		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedByte & 0xFF) + "\n";
		rc += "\t\tNumDesc                = " + m_numDesc;

		return rc;

	} // toString()

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 *
	 * @return size of this message
	 */
	public int size() {
		// return super.size() + SDVInitRequestMsgLength;
		return 19;

	} // size()

	// TODO create descriptor class
	/*
	 * public Class D { private short dataTag; private short dataLength; private
	 * byte[] data; }
	 */
}

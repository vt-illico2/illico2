package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVEventIndication message. <br>
 * <br>
 * This message allows the SDV Server to send event indications to the SDV Client, and vice versa. For example, the SDV
 * Server uses this message to instruct the SDV Client to display a barker or message to inform the user that the
 * program is no longer available or to ask whether the subscriber is still watching a given program. <br>
 * <br>
 * @author dkimble
 */

public class SDBEventIndication extends SDBMessage {

    final short SDVEventIndicationMsgLength = 12;

    private short m_reservedShort1;
    // private data
    private short m_privateDataLength;
    private short m_reservedShort2;
    private int m_sourceID;
    private byte m_eventDescriptorTag;
    private byte m_attributeValueLength;
    private byte[] m_attributeValue;

    private int[] messageFieldSegments = new int[] {2, 2, 2, 4, 1, 1};

    /**
     * constructor
     */
    public SDBEventIndication() {
        super(SDVConstants.SDVMessageID_SDVEventIndication);
    } // SDVEventIndication()

    /**
     * constructor with parameters
     * @param sourceID
     * @param eventDescriptorTag
     * @param attributeValueLength
     * @param attributeValue
     */
    public SDBEventIndication(int sourceID, byte eventDescriptorTag, byte attributeValueLength, byte[] attributeValue) {
        super(SDVConstants.SDVMessageID_SDVEventIndication);
        m_sourceID = sourceID;
        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication");
            Log.printInfo("\t\teventDescriptorTag = " + eventDescriptorTag);
            Log.printInfo("\t\tattributeValueLength = " + attributeValueLength);
            String str = "\t\tattributeValue = 0x";
            for (int i = 0; i < attributeValueLength; i++) {
                if ((attributeValue[i] & 0xFF) < 16)
                    str += "0";
                str += Integer.toHexString(attributeValue[i] & 0xFF) + " ";
            }
            Log.printInfo(str);
        }

    } // SDVEventIndication()

    public SDBEventIndication(int sourceID, byte tunerUse) {
        super(SDVConstants.SDVMessageID_SDVEventIndication);
        setMessageLength((short) size());

        m_sourceID = sourceID;
        m_privateDataLength = 10;
        m_eventDescriptorTag = (byte) 0x01;
        m_attributeValueLength = 2;
        m_attributeValue = new byte[2];
        m_attributeValue[0] = tunerUse;
        m_attributeValue[1] = (byte) 0xFF;

        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication");
            Log.printInfo("\t\teventDescriptorTag = " + m_eventDescriptorTag);
            Log.printInfo("\t\tattributeValueLength = " + m_attributeValueLength);
            String str = "\t\tattributeValue = 0x";
            for (int i = 0; i < m_attributeValueLength; i++) {
                if ((m_attributeValue[i] & 0xFF) < 16)
                    str += "0";
                str += Integer.toHexString(m_attributeValue[i] & 0xFF) + " ";
            }
            Log.printInfo(str);
        }
    }

    // test pellos
    public void setPrivateDataLength(short len) {
        m_privateDataLength = len;
    }

    /**
     * Returns the first reserved short in this SDVEventIndication message. <br>
     * This should always be 0xFFFF.
     * @return first reserved short in this SDVEventIdication message (always 0xFFFF)
     */
    public short getReservedShort1() {
        return m_reservedShort1;
    }

    /**
     * Returns the length of the private data section - this should always be 12 plus the length of the attributeValue
     * (as defined by the length field).
     * @return private data length
     */
    public short getPrivateDataLength() {
        return m_privateDataLength;
    }

    /**
     * Returns the second reserved short in this SDVEventIndication message. <br>
     * This should always be 0xFFFF.
     * @return second reserved short in this SDVEventIdication message (always 0xFFFF)
     */
    public short getReservedShort2() {
        return m_reservedShort2;
    }

    /**
     * Sets the sourceID to which SDV client is currently tuned.
     * @param sourceID
     */
    public void setSourceID(int sourceID) {
        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication.setSourceID(): sourceID = " + sourceID);
        }

        // TODO - should check for illegal sourceID
        m_sourceID = sourceID;

    } // setSourceID(int sourceID)

    /**
     * @return sourceID to which SDV client is currently tuned.
     */
    public int getSourceID() {
        return m_sourceID;
    }

    /**
     * Sets the eventDescriptor tag for this message.
     * <TABLE>
     * <TBODY> <THEAD>
     * <TH>tag
     * <TH>meaning
     * <TR>
     * <TD>0
     * <TD>display barker
     * <TR>
     * <TD>1
     * <TD>tuner use </TBODY>
     * </TABLE>
     * @param eventDescriptorTag
     */
    public void setEventDescriptorTag(byte eventDescriptorTag) {
        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication.setEventDescriptorTag(): eventDescriptorTag = " + eventDescriptorTag);
        }
        m_eventDescriptorTag = eventDescriptorTag;
    }

    /**
     * Returns the eventDescriptor tag for this message. <br>
     * <br>
     * See the description of {@link #setEventDescriptorTag(byte eventDescriptorTag)} for details on on this field.
     * @return event descriptor tag
     */
    public byte getEventDescriptorTag() {
        return m_eventDescriptorTag;
    }

    /**
     * @param attributeValueLength Sets the length of the attributeValue.
     */
    public void setAttributeValueLength(byte attributeValueLength) {
        m_attributeValueLength = attributeValueLength;
    }

    /**
     * @return the length of the attributeValue.
     */
    public byte getAttributeValueLength() {
        return m_attributeValueLength;
    }

    /**
     * @param attributeValue Sets the attributeValue.
     */
    public void setAttributeValue(byte[] attributeValue) {
        m_attributeValue = attributeValue;
    }

    /**
     * @return attributeValue.
     */
    public byte[] getAttributeValue() {
        return m_attributeValue;
    }

    /**
     * Returns an int array which describes the sizes of all fields in this message. <br>
     * <br>
     * This allows a hex dump display of this message to include visual cues (e.g. ".") at field boundaries.
     * @return int array which describes the sizes of all fields in this message
     */
    public int[] getMessageFieldSegments() {
        int superNumFields = super.getMessageFieldSegments().length;
        int[] combinedArray = new int[superNumFields + messageFieldSegments.length];

        System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0, superNumFields);
        System.arraycopy(messageFieldSegments, 0, combinedArray, superNumFields, messageFieldSegments.length);

        return combinedArray;

    } // getMessageFieldSegments()

    /**
     * Serializes this message for transmission to the SDV server (via UDP)
     * @return a byte array containing the entire message, including DSM-CC header and the sessionID from
     *         {@link SDBMessage}
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        byte[] s = super.toByteArray();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(bo);

        try {
            out.write(s);

            out.writeShort(m_reservedShort1);
            out.writeShort(m_privateDataLength);
            out.writeShort(m_reservedShort2);
            out.writeInt(m_sourceID);
            out.writeByte(m_eventDescriptorTag);
            out.writeByte(m_attributeValueLength);
            for (int i = 0; i < m_attributeValueLength; i++) {
                out.writeByte(m_attributeValue[i]);
            }

            rc = bo.toByteArray();

            out.close();
            out = null;

        } catch (IOException e) {
            Log.printWarning("SDVEventIndication:toByteArray(): " + e.getMessage());
            Log.print(e);
        } finally {
            if (out != null) {
                try {
                    out.close();
                    out = null;
                } catch (IOException ioe) {
                    // do nothing
                }
            }
        }
        return rc;

    } // toByteArray()

    /**
     * Parses an incoming SDVEventIndication message.
     * @param dis a DataInputStream object containing the SDVEventIndication message
     * @return incoming SDVEventIndication message
     */
    public short parse(DataInputStream dis) {
        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication:parse()");
        }

        short ret = super.parse(dis);

        if (ret == SDVConstants.FORMAT_OK) {
            try {
                m_reservedShort1 = dis.readShort();
                m_privateDataLength = dis.readShort();
                m_reservedShort2 = dis.readShort();
                m_sourceID = dis.readInt();
                m_eventDescriptorTag = dis.readByte();
                m_attributeValueLength = dis.readByte();
                m_attributeValue = new byte[m_attributeValueLength];
                for (int i = 0; i < m_attributeValueLength; i++)
                    m_attributeValue[i] = dis.readByte();
            } catch (IOException e) {
                Log.printWarning("SDVEventIndication:parse(): " + e.getMessage());
                Log.print(e);
            }
        } // if(ret == SDVConstants.FORMAT_OK)
        else {
            Log.printWarning("SDVEventIndication:parse() error, super class failed parse.");
        }
        if (Log.INFO_ON) {
            Log.printInfo("SDVEventIndication:parse(), SourceID: " + m_sourceID);
        }
        return ret;

    } // parse(DataInputStream in)

    /**
     * Returns a string describing the contents of this message in human readable form.
     * @return String version of this object
     */
    public String toString() {
        String rc;

        rc = "\n\tSDVEventIndication\n";

        rc += super.toString();

        rc += "\t\t1st Reserved short     = 0x" + Integer.toHexString(m_reservedShort1 & 0xFFFF) + "\n";
        rc += "\t\tPrivate Data Length    = " + m_privateDataLength + "\n";
        rc += "\t\t2nd Reserved short     = 0x" + Integer.toHexString(m_reservedShort2 & 0xFFFF) + "\n";
        rc += "\t\tsourceID               = " + m_sourceID + "\n";
        rc += "\t\tEvent descriptor tag   = 0x" + Integer.toHexString(m_eventDescriptorTag & 0xFF) + "\n";
        rc += "\t\tAttribute value length   = " + m_attributeValueLength;
        rc += "\t\tattribute value = \n\t\t\t\t\t0x ";
        for (int i = 0; i < m_attributeValueLength; i++) {
            if ((m_attributeValue[i] & 0xFF) < 16)
                rc += "0";
            rc += Integer.toHexString(m_attributeValue[i] & 0xFF) + " ";
        }
        if (m_eventDescriptorTag == 0) {
        }

        return rc;

    } // toString()

    /**
     * Returns a string representation of the tunerUse defined in this SDVEventIndication message. <br>
     * <br>
     * @param tunerUse
     * @return String representation of the tunerUser
     */
    public String tunerUseToString(byte tunerUse) {
        String rc;

        rc = "\t\t\t\tTuner Use:\n";
        rc += "\t\t\t\t\tIs tuner being used for scheduled recording: ";
        if ((tunerUse & 0x1) == 0)
            rc += "NO\n";
        else
            rc += "YES\n";
        rc += "\t\t\t\t\tIs tuner being used for PPV content:         ";
        if ((tunerUse & 0x4) == 0)
            rc += "NO\n";
        else
            rc += "YES\n";
        rc += "\t\t\t\t\tThe display endpoint for the tuner is:       ";
        switch ((tunerUse >> 3) & 0x7) {
        case 0:
            rc += "NONE";
            break;
        case 1:
            rc += "MAIN TV";
            break;
        case 2:
            rc += "PIP";
            break;
        }
        rc += "\n";

        return rc;

    } // tunerUseToString(byte tunerUse)

    /**
     * Returns the size of this message, including DSM-CC header and the sessionID from {@link SDBMessage}
     * @return size of this message
     */
    public int size() {
//        return super.size() + SDVEventIndicationMsgLength;
        return 24;
    }
}

package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the sessionID portion of this SDV message.
 * @author dkimble
 */

public abstract class SDBMessage extends DSMCCMessage {

    private byte[] m_sessionID = new byte[10];

    private int[] messageFieldSegments = new int[] {6, 3, 1};

    /**
     * constructor
     * @param messageID
     */
    public SDBMessage(short messageID) {
        super(messageID);
    }

    /**
     * Sets the sessionID from a byte array passed in. <br>
     * <br>
     * The session ID consists of 6 bytes of the MAC address the SDV client is running on, concatenated with three bytes
     * of 0, concatenatetd with 1 byte of tunerID
     * @param id sessionID
     */
    public void setSessionID(byte[] id) {
        // Session ID always has 10 bytes.
        System.arraycopy(id, 0, m_sessionID, 0, 10);
    }

    /**
     * Returns a byte array containing the session ID. <br>
     * <br>
     * The session ID consists of 6 bytes of the MAC address the SDV client is running on, concatenated with three bytes
     * of 0, concatenatetd with 1 byte of tunerID
     * @return byte array containing the session ID
     */
    public byte[] getSessionID() {
        return m_sessionID;
    }

    /**
     * Returns an int array which describes the sizes of the three fields in the sessionID. <br>
     * <br>
     * This allows a hex dump display of this message to include visual cues (e.g. ".") at field boundaries.
     * @return int array which describes the sizes of the three fields in the sessionID.
     */
    public int[] getMessageFieldSegments() {
        int superNumFields = super.getMessageFieldSegments().length;
        int[] combinedArray = new int[superNumFields + messageFieldSegments.length];

        System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0, superNumFields);
        System.arraycopy(messageFieldSegments, 0, combinedArray, superNumFields, messageFieldSegments.length);

        return combinedArray;

    } // getMessageFieldSegments()

    /**
     * Serializes the sessionID part of this SDV message.
     * @return serialized version of the sessionID
     */
    public byte[] toByteArray() {
        byte[] rc = null;
        byte[] s = super.toByteArray();
        ByteArrayOutputStream bo = new ByteArrayOutputStream();

        try {
            bo.write(s);
            bo.write(m_sessionID);

            rc = bo.toByteArray();

            bo.close();
            bo = null;

        } catch (IOException e) {
            Log.printWarning("SDVMessage.toByteArray(): " + e.getMessage());
            Log.print(e);
        } finally {
            if (bo != null) {
                try {
                    bo.close();
                    bo = null;
                } catch (IOException ioe) {
                    // do nothing
                }
            }
        }

        return rc;

    } // toByteArray()

    /**
     * Parses the sessionID part of this SDV message.
     * @param dis A DataInputStream object containing the SDV message.
     * @return sessionID of the SDV message
     */
    public short parse(DataInputStream dis) {
        short ret = super.parse(dis);

        if (ret == SDVConstants.FORMAT_OK) {
            try {
                dis.read(m_sessionID, 0, m_sessionID.length);
            } catch (IOException e) {
                Log.printWarning("SDVMessage.parse(): " + e.getMessage());
                Log.print(e);
                ret = SDVConstants.FORMAT_ERROR;
            }
        } // if(ret == SDVConstants.FORMAT_OK)

        return ret;
    }

    /**
     * Returns a string describing the messageID and sessionID parts of this SDV message in human readable form.
     * @return String version of this object
     */
    public String toString() {
        String rc;

        rc = super.toString();

        rc += "\t\tSDV header\n";

        rc += "\t\t\tmessageID = " + messageIDToString(super.m_messageID) + "\n";

        rc += sessionIDToString(m_sessionID);

        return rc;

    } // toString()

    /**
     * Returns a string representation of the messageID. <br>
     * <br>
     * @param messageID
     * @return String version of the messageID
     */
    public String messageIDToString(short messageID) {
        switch (messageID) {
        case SDVConstants.SDVMessageID_Reserved:
            return "SDVMessageID_Reserved";

        case SDVConstants.SDVMessageID_SDVProgramSelectRequest:
            return "SDVMessageID_SDVProgramSelectRequest";

        case SDVConstants.SDVMessageID_SDVProgramSelectConfirm:
            return "SDVMessageID_SDVProgramSelectConfirm";

        case SDVConstants.SDVMessageID_SDVProgramSelectIndication:
            return "SDVMessageID_SDVProgramSelectIndication";

        case SDVConstants.SDVMessageID_SDVProgramSelectResponse:
            return "SDVMessageID_SDVProgramSelectResponse";

        case SDVConstants.SDVMessageID_SDVUserActivityReport:
            return "SDVMessageID_SDVUserActivityReport";

        case SDVConstants.SDVMessageID_SDVInitRequest:
            return "SDVMessageID_SDVInitRequest";

        case SDVConstants.SDVMessageID_SDVInitConfirm:
            return "SDVMessageID_SDVInitConfirm";

        case SDVConstants.SDVMessageID_SDVQueryRequest:
            return "SDVMessageID_SDVQueryRequest";

        case SDVConstants.SDVMessageID_SDVQueryConfirm:
            return "SDVMessageID_SDVQueryConfirm";

        case SDVConstants.SDVMessageID_SDVEventIndication:
            return "SDVMessageID_SDVEventIndication";

        case SDVConstants.SDVMessageID_SDVEventResponse:
            return "SDVMessageID_SDVEventResponse";

        default:
            return "UNKNOWN";
        }

    } // messageIDToString(short messageID) {

    /**
     * Returns a string representation of the sessionID
     * @param sessionID
     * @return String version of the sessionID
     */
    public String sessionIDToString(byte[] sessionID) {
        String rc;

        rc = "\t\t\tSessionID\n";
        rc += "\t\t\t\tMACAddress = ";
        for (int i = 0; i < 6; i++) {
            if (i > 0)
                rc += ":";
            rc += Integer.toHexString(sessionID[i] & 0xFF);
        }
        rc += "\n\t\t\t\tTunerID = " + sessionID[6] + "\n";

        return rc;

    } // sessionIDToString(byte[] sessionID)

    /**
     * Returns the size of the DSM-CC header and the sessionID.
     * @return size of the DSMCC header and sessionID
     */
    public int size() {
        return super.size() + m_sessionID.length;
    }

}

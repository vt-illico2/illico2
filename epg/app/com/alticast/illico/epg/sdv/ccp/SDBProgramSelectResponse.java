package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVProgramSelectResponse message. <br>
 * <br>
 * This message is sent from the Client to the SDV Server in response to a
 * SDBProgramSelectIndication message. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBProgramSelectResponse extends SDBMessage {

	final short SDVProgramSelectResponseMsgLength = 4;

	public static final short rspOk = (short) 0x0000;
	public static final short rspFormatError = (short) 0x0001;
	public static final short rspNoSession = (short) 0x0002;

	public static final short rspForceTune = (short) 0x8000;
	public static final short rspProgNotAvail = (short) 0x8001;

	private short m_response;
	// private final short m_privateDataLength = 10;
	private final short m_privateDataLength = 0x0000;

	private int[] messageFieldSegments = new int[] { 2, 2 };

	/**
	 * constructor
	 */
	public SDBProgramSelectResponse() {
		super(SDVConstants.SDVMessageID_SDVProgramSelectResponse);
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectResponse");
		}
		setMessageLength((short) size());
        setAdaptationLength((byte) 0x00);
	}

	// test Pellos
	public void setMassageLength() {
		setMessageLength((short) size());
	}

	/**
	 * constructor with params
	 *
	 * @param response
	 *            See the description of {@link #setResponse(short response)}
	 *            for details on this field.
	 */
	public SDBProgramSelectResponse(short response) {
		super(SDVConstants.SDVMessageID_SDVProgramSelectResponse);
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectResponse");
			Log.printInfo("\t\tresponse = " + response);
		}

		m_response = response;

		setMessageLength((short) size());
        setAdaptationLength((byte) 0x00);

	} // SDVProgramSelectResponse(short response)

	/**
	 * Sets the response code for this SDVProgramSelectResponse message. <br>
	 * <br>
	 *
	 * Values are as follows:
	 * <TABLE>
	 * <TBODY> <THEAD>
	 * <TH>Code
	 * <TH>Value
	 * <TR>
	 * <TD>rspForceTuen
	 * <TD>0x8000
	 * <TR>
	 * <TD>rspProgNotAvil
	 * <TD>0x8001 </TBODY>
	 * </TABLE>
	 *
	 * @param response
	 *            code
	 */
	public void setResponse(short response) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectResponse.setResponse(): response = "
					+ response);
		}

		// handle illegal response codes
		// if ((response < 0) || (response > 2)) {
		// Log.printWarning("SDVProgramSelectResponse.setResponse() - ERROR: illegal response value = "
		// + response);
		// return;
		// }

		m_response = response;

	} // setResponse(short response)

	/**
	 * Returns the response code for this SDVProgramSelectResponse message.
	 *
	 * See the description of {@link #setResponse(short response)} for details
	 * on this field.
	 *
	 * @return response
	 */
	public long getResponse() {
		return m_response;
	}

	/**
	 * Returns the length of the private data section of this message - this
	 * should always be 10 for a SDVProgramSelectResponse.
	 *
	 * @return private data length
	 */
	public short getPrivateDataLength() {
		return m_privateDataLength;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 *
	 * @return int array which describes the sizes of all fields in this message
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Serializes this message for transmission to the SDV server (via UDP)
	 *
	 * @return a byte array containing the entire message, including DSM-CC
	 *         header and the sessionID from {@link SDBMessage}
	 */
	public byte[] toByteArray() {
		byte[] rc = null;
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_response);
			out.writeShort(m_privateDataLength);

			rc = bo.toByteArray();

			out.close();
			out = null;

		} catch (IOException e) {
			Log.printWarning("SDVProgramSelectResponse.toByteArray(): "
					+ e.getMessage());
            Log.print(e);
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	} // toByteArray()

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 *
	 * @return String version of this object
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVProgramSelectResponse\n";

		rc += super.toString();

		rc += "\t\tResponse               = " + m_response + "\n";
		rc += "\t\tPrivate Data Length    = " + m_privateDataLength;

		return rc;

	} // toString()

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 *
	 * @return size of this message
	 */
	public int size() {
		// return super.size() + SDVProgramSelectResponseMsgLength;
		return 12;
	}

}

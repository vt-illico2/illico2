package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

public class SDBUserActivityReport extends SDBMessage {
	final short SDVUserActivityReportMsgLength = 8;

	private short m_reserved = (short) 0xFFFF;
	private int m_sourceID = 0;
	private int m_timeLastUserActivity = 0;

	private int[] messageFieldSegments = new int[] { 2, 4, 4 };

	/**
	 * plain vanilla constructor
	 */
	public SDBUserActivityReport() {
		super(SDVConstants.SDVMessageID_SDVUserActivityReport);
		if (Log.INFO_ON) {
			Log.printInfo("SDVUserActivityReport");
		}

		setMessageLength((short) size());

	} // SDVUserActivityReport()

	/**
	 * constructor with params
	 *
	 * @param sourceID
	 * @param timeLastUserActivity
	 *
	 */
	public SDBUserActivityReport(int sourceID, int timeLastUserActivity) {
		super(SDVConstants.SDVMessageID_SDVUserActivityReport);
		if (Log.INFO_ON) {
			Log.printInfo("SDVUserActivityReport");
			Log.printInfo("\t\tsourceID = " + sourceID);
			Log.printInfo("\t\ttimeLastUserActivity = " + timeLastUserActivity);
		}

		m_sourceID = sourceID;
		m_timeLastUserActivity = timeLastUserActivity;

		setMessageLength((short) size());

	} // SDVUserActivityReport(int sourceID, int timeLastUserActivity)

	/**
	 * setSourceID =========== Sets the sourceID currently being watched.
	 *
	 * @param sourceID
	 */
	public void setSourceID(int sourceID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVUserActivityReport.setSourceID() - sourceID = "
					+ sourceID);
		}

		// TODO - should check for illegal sourceID
		m_sourceID = sourceID;

	} // setSourceID(int sourceID)

	/**
	 * getSourceID ===========
	 *
	 * @return the sourceID currently being watched
	 */
	public int getSourceID() {
		return m_sourceID;

	} // getSourceID()

	/**
	 * getMessageFieldSegments =======================
	 *
	 * @return int array of field sizes for debugging
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * toByteArray =========== Serializes this message for transmission to the
	 * SDV server
	 *
	 * @return A byte array containing the entire message.
	 */
	public byte[] toByteArray() {
		byte[] rc = null;
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_reserved);
			out.writeInt(m_sourceID);
			out.writeInt(m_timeLastUserActivity);

			rc = bo.toByteArray();

			out.close();
			out = null;
		} catch (IOException e) {
			Log.printWarning("SDVUserActivityReport.toByteArray(): "
					+ e.getMessage());
            Log.print(e);
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;
	}

	/**
	 * @return A string describing the contents of this SDVUserActivityReport
	 *         msg in human readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVUserActivityReport\n";

		rc += super.toString();

		rc += "\t\tReserved                = 0x"
				+ Integer.toHexString(m_reserved & 0xFFFF) + "\n";
		rc += "\t\tSourceID                = " + m_sourceID + "\n";
		rc += "\t\tTime Last User Activity = " + m_timeLastUserActivity + "\n";

		return rc;
	}

	/**
	 * size ====
	 *
	 * @return the size of the final message that is to be serialized.
	 */
	public int size() {
		// return super.size() + SDVUserActivityReportMsgLength;
		return 20;
	}

	/**
	 * @return the m_timeLastUserActivity
	 */
	public int getLastUserActivity() {
		return m_timeLastUserActivity;
	}

	/**
	 * @param lastUserActivity
	 *            the m_timeLastUserActivity to set
	 */
	public void setLastUserActivity(int lastUserActivity) {
		m_timeLastUserActivity = lastUserActivity;
	}

}

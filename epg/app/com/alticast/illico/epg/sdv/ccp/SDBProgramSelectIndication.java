package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVProgramSelectIndication message.
 * <br><br>
 * This message is sent by the SDV Server to the SDV Client when to force-
 * tune users to another frequency or when the program is no longer available.
 * <br><br>
 *
 * @author dkimble
 *
 */

public class SDBProgramSelectIndication extends SDBMessage {
   public static final int FORCE_TUNE = 0xffff8000;
   public static final int PROG_UNAVAIL = 0xffff8001;

	final short SDVProgramSelectIndicationMsgLength = 22;

	private short m_reason;
	private int m_sourceID;
	// private data
	private short m_privateDataLength;
	private byte m_reservedByte;
	private byte m_modulationFormat;
	private int m_tuningFrequency;
	private short m_reservedShort;
	private short m_MPEGProgramNumber;
	private int m_currentSourceID;

	private int[] messageFieldSegments = new int[] {2, 4, 2, 1, 1, 4, 2, 4};

	// reason code
	private final short rsnOK = 0x0000;
	private final short rsnNormal = 0x0001;
	private final short rsnSeEntitlementFailure = 0x0002;
	private final short forceTune = (short) 0x8000;
	private final short progNotAvail = (short) 0x8001;

	// modulation format
	private final byte mfUnknownFormat = 0x00;
	private final byte mfQAM16 = 0x06;
	private final byte mfQAM32 = 0x07;
	private final byte mfQAM64 = 0x08;
	private final byte mfQAM128 = 0x0C;
	private final byte mfQAM256 = 0x10;

	/**
	 * constructor
	 */
	public SDBProgramSelectIndication() {
		super(SDVConstants.SDVMessageID_SDVProgramSelectIndication);
		if(Log.INFO_ON)
		{
			Log.printInfo("SDVProgramSelectIndication");
		}

	} // SDVProgramSelectIndication()

	// test Pellos
	public void setMassageLength(){
		setMessageLength((short) size());
	}

	// test Pellos
	public void setReason(short reason) {
		m_reason = reason;
	}

	// test Pellos
	public void setSourceID(int src) {
		m_sourceID = src;
	}

	// test Pellos
	public void setPrivateDataLength(short privateDataLength) {
		m_privateDataLength = privateDataLength;
	}

	// test Pellos
	public void setModulationFormat(byte modulF) {
		m_modulationFormat = modulF;
	}

	// test Pellos
	public void setCurrentSourceID(int curSourceId) {
		m_currentSourceID = curSourceId;
	}

	/**
	 * Returns the reason code from this SDVProgramSelectRequest message.
	 * <br><br>
	 * Values are as follows:
	 * <TABLE>
	 * <TBODY>
	 * <THEAD>
	 * <TH>Code
	 * <TH>Value
	 * <TR><TD>rsnOK<TD>0x0000
	 * <TR><TD>rsnNormal<TD>0x0001
	 * <TR><TD>rsSeEntitlementFailure<TD>0x0002
	 * <TR><TD>Reserved<TD>0x0003 - 0x7FFF
	 * <TR><TD>private use<TD>0x8000 - 0xFFFF
	 * <TR><TD>Force-tune<TD>0x8000
	 * <TR><TD>ProgNotAvail<TD>0x8001
	 * </TBODY>
	 * </TABLE>
	 */
	public short getReason() {
		return m_reason;
	}

	/**
	 * Returns the sourceID from this SDVProgramSelectIndication message.
	 */
	public int getSourceID() {
		return m_sourceID;
	}

	/**
	 * Returns the length of the private data section -
	 * this should always be 14 for a SDVProgramSelectIndication message
	 */
	public short getPrivateDataLength() {
		return m_privateDataLength;
	}

	/**
	 * Returns the reserved byte in this SDVProgramSelectIndication message.  This should always be 0xFF.
	 */
	public byte getReservedByte() {
		return m_reservedByte;
	}

	/**
	 * Returns modulation mode to use to tune to the sourceID specified in this SDVProgramSelectIndication message.
	 *
	 * <TABLE>
	 * <TBODY>
	 * <THEAD>
	 * <TH>Value
	 * <TH>Modulation
	 * <TR><TD>0x00<TD>Unknown modulation format
	 * <TR><TD>0x01-0x05<TD>Reserved
	 * <TR><TD>0x06<TD>QAM 16 modulation format
	 * <TR><TD>0x07<TD>QAM 32 modulation format
	 * <TR><TD>0x08<TD>QAM 64 modulation format
	 * <TR><TD>0x09-0x0B<TD>Reserved
	 * <TR><TD>0x0C<TD>QAM 128 modulation format
	 * <TR><TD>0x0D-0x0F<TD>Reserved
	 * <TR><TD>0x10<TD>QAM 256 modulation format
	 * <TR><TD>0x11-0xFF<TD>Reserved
	 * </TBODY>
	 * </TABLE>
	 */
	public byte getModulationFormat() {
		return m_modulationFormat;
	}

	/**
	 * Returns tuning frequency (in Hz) to use to tune to the SDV sourceID specified
	 * in this SDVProgramSelectIndication message
	 */
	public int getTuningFrequency() {
		return m_tuningFrequency;
	}

	/**
	 * Returns the reserved short in this SDVProgramSelectIndication message.  This should always be 0xFFFF.
	 */
	public short getReservedShort() {
		return m_reservedShort;
	}

	/**
	 * Returns MPEG program number to use to tune to the SDV sourceID specified
	 * in this SDVProgramSelectIndication message
	 */
	public short getMPEGProgramNumber() {
		return m_MPEGProgramNumber;
	}

	/**
	 * Returns what the SDV believes is the sourceID currently tuned to by the SDV client.
	 * <br><br>
	 * The SDV Client shall ignore this message if it is not tuned to sourceId when it receives this message.
	 */
	public int getCurrentSourceID() {
		return m_currentSourceID;
	}

	/**
	 * Returns an int array which describes the sizes of all fields
	 * in this message.
	 * <br><br>
	 * This allows a hex dump display of this message to include
	 * visual cues (e.g. ".") at field boundaries.
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Parses an incoming SDVProgramSelectIndication message.
	 *
	 * @param dis a DataInputStream object containing the SDVProgramSelectIndication message
	 */
	public short parse(DataInputStream dis) {
		if(Log.INFO_ON)
		{
			Log.printInfo("SDVProgramSelectIndication.parse()");
		}

		short ret = super.parse(dis);

		if (ret == SDVConstants.FORMAT_OK) {
			try {
				m_reason = dis.readShort();
				m_sourceID = dis.readInt();
				m_privateDataLength = dis.readShort();
				m_reservedByte = dis.readByte();
				m_modulationFormat = dis.readByte();
				m_tuningFrequency = dis.readInt();
				m_MPEGProgramNumber = dis.readShort();
                m_reservedShort = dis.readShort();
				m_currentSourceID = dis.readInt();
			} catch (IOException e) {
				Log.printWarning("SDVProgramSelectIndication.parse(): " + e.getMessage());
                Log.print(e);
			}
		} // if(ret == SDVConstants.FORMAT_OK)

		return ret;

	} // parse(DataInputStream in)

	/**
	 * Returns a string describing the contents of this message in human readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVProgramSelectIndication\n";

		rc += super.toString();

		rc += "\t\tReason       = 0x" + Integer.toHexString(m_reason)
				+ " = " + reasonToString(m_reason) + "\n";
		rc += "\t\tsourceID               = " + m_sourceID + "\n";
		rc += "\t\tPrivate Data Length    = " + m_privateDataLength + "\n";
		rc += "\t\tReserved               = 0x" + Integer.toHexString(m_reservedByte & 0xFF) + "\n";
		rc += "\t\tModulation Format      = 0x"
				+ Integer.toHexString(m_modulationFormat) + " = "
				+ modulationFormatToString(m_modulationFormat) + "\n";
		rc += "\t\tTuning Frequency       = " + m_tuningFrequency + "Hz\n";
		rc += "\t\tMPEG Program Number    = " + m_MPEGProgramNumber + "\n";
		rc += "\t\tReserved               = 0x" + Integer.toHexString(m_reservedShort & 0xFFFF) + "\n";
		rc += "\t\tm_currentSourceID      = " + m_currentSourceID + "\n";

		return rc;

	} // toString()

	/**
	 * Returns a string representation of the reason code
	 *
	 * See the description of {@link #getReason()} for details on this field.
	 */
	public String reasonToString(short reason) {
		switch (reason) {
		case rsnOK:
			return "rsnOK";
		case rsnNormal:
			return "rsnNormal";
		case rsnSeEntitlementFailure:
			return "rspSeEntitlementFailure";
		case forceTune:
			return "forceTune";
		case progNotAvail:
			return "progNotAvail";
		default:
			return "ERROR - ILLEGAL REASON CODE!";
		}

	} // reasonToString()

	/**
	 * Returns a string representation of the modulation format.
	 *
	 * See the description of {@link #getModulationFormat()} for details on this field.
	 */
	public String modulationFormatToString(short modulationFormat) {
		switch (modulationFormat) {
		case mfUnknownFormat:
			return "Unknown modulation format";
		case mfQAM16:
			return "QAM 16";
		case mfQAM32:
			return "QAM 32";
		case mfQAM64:
			return "QAM 64";
		case mfQAM128:
			return "QAM 128";
		case mfQAM256:
			return "QAM 256";
		default:
			return "ERROR - ILLEGAL MODULATION FORMAT!";
		}

	} // modulationFormatToString()

	/**
	 * Returns the size of this message, including DSM-CC header and the sessionID from {@link SDBMessage}
	 */
	public int size() {
		return super.size() + SDVProgramSelectIndicationMsgLength;
	}

	// pellos Test
	public byte[] toByteArray() {
		byte[] rc = new byte[0];
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

//			out.writeShort(m_reservedShort);
			out.writeShort(m_reason);
			out.writeInt(m_sourceID);

			out.writeShort(m_privateDataLength);
			out.writeByte((byte)0xFF);
			out.writeByte(m_modulationFormat);
			out.writeInt(m_tuningFrequency);
			out.writeShort(m_MPEGProgramNumber);
			// reserved
			out.writeShort((short)0xFFFF);
			out.writeInt(m_currentSourceID);

			rc = bo.toByteArray();

			out.close();
			out = null;
		} catch (IOException e) {
			Log.printWarning("SDVProgramSelectRequest.toByteArray(): "
					+ e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	}

}

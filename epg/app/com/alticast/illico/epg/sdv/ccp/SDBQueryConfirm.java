package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVQueryConfirm message. <br>
 * <br>
 * This message is sent from the SDV Client to the Server in response to a
 * SDVQueryRequest message. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBQueryConfirm extends SDBMessage {

	final short SDVQueryConfirmMsgLength = 18;

	private short m_response;
	private int m_sourceID = 0;
	// private datan
	private final short m_privateDataLength = 10;
	private byte m_tunerUse = 0;
	private byte m_reservedByte = (byte) 0xFF;
	private int m_serviceGroupID = 0;
	private int m_timeLastUserActivity = 0;

	private int[] messageFieldSegments = new int[] { 2, 4, 2, 1, 1, 4, 4 };

	/**
	 * constructor
	 */
	public SDBQueryConfirm() {
		super(SDVConstants.SDVMessageID_SDVQueryConfirm);
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm");
		}

		setMessageLength((short) size());

	} // SDVQueryConfirm()

	/**
	 * constructor with params
	 *
	 * @param response
	 * @param sourceID
	 * @param tunerUse
	 * @param serviceGroupID
	 * @param timeLastUserActivity
	 *
	 */
	public SDBQueryConfirm(short response, int sourceID, byte tunerUse,
			int serviceGroupID, int timeLastUserActivity) {
		super(SDVConstants.SDVMessageID_SDVQueryConfirm);
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm");
			Log.printInfo("\t\tresponse = " + response);
			Log.printInfo("\t\tsourceID = " + sourceID);
			Log.printInfo("\t\ttunerUse = " + tunerUse);
			Log.printInfo("\t\tserviceGroupID = " + serviceGroupID);
			Log.printInfo("\t\ttimeLastUserActivity = " + timeLastUserActivity);
		}

		m_response = response;
		m_sourceID = sourceID;
		m_tunerUse = tunerUse;
		m_serviceGroupID = serviceGroupID;
		m_timeLastUserActivity = timeLastUserActivity;

		setMessageLength((short) size());

	} // SDVQueryConfirm(short response, int sourceID, byte tunerUse, int
		// serviceGroupID, int timeLastUserActivity)

/**
    * Sets the response code for this SDVQueryConfirm message.
    * <br><br>
    *
    * Values are as follows:
    * <TABLE>
    * <TBODY>
    * <THEAD>
    * <TH>Code
    * <TH>Value
    * <TR><TD>rspOK<TD>0x0000
    * <TR><TD>rspFormatError<TD>0x0001
    * <TR><TD>rspNoSession<TD>0x0002
    * <TR><TD>reserved<TD>0x0008 - 0x7FFF
    * <TR><TD>private use<TD>0x8000 - 0xFFFF
    * </TBODY>
    * </TABLE>
    *
    * @param response code
    */
	public void setResponse(short response) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm()setResponse(): response = "
					+ response);
		}

		// handle illegal response codes
		if ((response < 0) || (response > 2)) {
			Log.printWarning("SDVQueryConfirm.setResponse() - ERROR: illegal response value = "
							+ response);
			return;
		}

		m_response = response;

	} // setResponse(short response)

	/**
	 * Returns the response code for this SDVQueryConfirm message.
	 *
	 * See the description of {@link #setResponse(short response)} for details
	 * on this field.
	 *
	 * @return response
	 */
	public int getResponse() {
		return m_response;
	}

	/**
	 * Sets the sourceID to which SDV client is currently tuned.
	 *
	 * @param sourceID
	 */
	public void setSourceID(int sourceID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm.setSourceID() - sourceID = "
					+ sourceID);
		}

		// TODO - should check for illegal sourceID
		m_sourceID = sourceID;

	} // setSourceID(int sourceID)

	/**
	 * Returns the sourceID to which SDV client is currently tuned.
	 *
	 * @return sourceID
	 */
	public int getSourceID() {
		return m_sourceID;

	} // getSourceID()

	/**
	 * Returns the length of the private data section of this message - this
	 * should always be 10 for a SDVQueryConfirm message.
	 *
	 * @return length of the private data section of this message
	 */
	public short getPrivateDataLength() {
		return m_privateDataLength;
	}

	/**
	 * Specifies how the tuner associated with this message is being be used. <br>
	 * <br>
	 * bit 0 Tuner is being used for a scheduled recording.<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 1 = yes<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 0 = no<br>
	 * bit 1 Reserved<br>
	 * bit 2 Tuner is being used for Pay-Per-View content.<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 1 = yes<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 0 = no<br>
	 * bits 3-5 Specifies the display endpoint for the tuner.<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 0 = NONE<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 1 = MAIN TV<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp 2 = PIP<br>
	 * &nbsp;&nbsp;&nbsp;&nbsp values: 3,4,5,6,7 are reserved.<br>
	 *
	 * @param tunerUse
	 *            set the tunerUse
	 */
	public void setTunerUse(byte tunerUse) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm.setTunerUse()");
			Log.printInfo("\t" + tunerUseToString(tunerUse));
		}

		m_tunerUse = tunerUse;

	} // setTunerUse(byte tunerUse)

	/**
	 * Returns the tuner use for the tuner associated with this message. <br>
	 * <br>
	 * See the description of {@link #setTunerUse(byte tunerUse)} for details on
	 * tuner use.
	 *
	 * @return tuner use
	 */
	public byte getTunerUse() {
		return m_tunerUse;

	} // getTunerUse()

	/**
	 * Sets the service group this SDV client is in.
	 *
	 * @param serviceGroupID
	 *            set the serviceGroupID
	 */
	public void setServiceGroupID(int serviceGroupID) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryConfirm.setServiceGroupID(): serviceGroupID = "
							+ serviceGroupID);
		}

		// TODO - should check for illegal serviceGroupID
		m_serviceGroupID = serviceGroupID;

	} // setServiceGroupID(int serviceGroupID)

	/**
	 * Returns the serviceGroup this SDV client is in.
	 *
	 * @return serviceGroupID
	 */
	public int getServiceGroupID() {
		return m_serviceGroupID;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 *
	 * @return int array containing the sizes of all fields in this message
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Serializes this message for transmission to the SDV server (via UDP)
	 *
	 * @return a byte array containing the entire message, including DSM-CC
	 *         header and the sessionID from {@link SDBMessage}
	 */
	public byte[] toByteArray() {
		byte[] rc = null;
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_response);
			out.writeInt(m_sourceID);
			out.writeShort(m_privateDataLength);
			out.writeByte(m_tunerUse);
			out.writeByte(m_reservedByte);
			out.writeInt(m_serviceGroupID);
			out.writeInt(m_timeLastUserActivity);

			rc = bo.toByteArray();

			out.close();
			out = null;

		} catch (IOException e) {
			Log.printWarning("SDVQueryConfirm.toByteArray(): " + e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;
	}

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 *
	 * @return String version of this object
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVQueryConfirm\n";

		rc += super.toString();

		rc += "\t\tResponse                = 0x"
				+ Integer.toHexString(m_response & 0xFFFF) + "\n";
		rc += "\t\tSourceID                = " + m_sourceID + "\n";
		rc += "\t\tPrivate Data Length     = " + m_privateDataLength + "\n";
		rc += tunerUseToString(m_tunerUse);
		rc += "\t\tReserved                = 0x"
				+ Integer.toHexString(m_reservedByte & 0xFF) + "\n";
		rc += "\t\tService Group ID        = " + m_serviceGroupID + "\n";
		rc += "\t\tTime Last User Activity = " + m_timeLastUserActivity + "\n";

		return rc;
	}

	/**
	 * Returns a string representation of the tunerUse defined in this
	 * SDVQueryConfirm message. <br>
	 * <br>
	 * See the description of {@link #setTunerUse(byte tunerUse)} for details on
	 * tuner use.
	 *
	 * @param tunerUse
	 * @return String representation of the tunerUse
	 */
	public String tunerUseToString(byte tunerUse) {
		String rc;

		rc = "\t\tTuner Use:\n";
		rc += "\t\t\tIs tuner being used for scheduled recording: ";
		if ((tunerUse & 0x1) == 0)
			rc += "NO\n";
		else
			rc += "YES\n";
		rc += "\t\t\tIs tuner being used for PPV content:         ";
		if ((tunerUse & 0x4) == 0)
			rc += "NO\n";
		else
			rc += "YES\n";
		rc += "\t\t\tThe display endpoint for the tuner is:       ";
		switch ((tunerUse >> 3) & 0x7) {
		case 0:
			rc += "NONE";
			break;
		case 1:
			rc += "MAIN TV";
			break;
		case 2:
			rc += "PIP";
			break;
		}
		rc += "\n";

		return rc;
	}

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 *
	 * @return size of this message
	 */
	public int size() {
		// return super.size() + SDVQueryConfirmMsgLength;
		return 28;
	}

	/**
	 * @return the m_timeLastUserActivity
	 */
	public int getLastUserActivity() {
		return m_timeLastUserActivity;
	}

	/**
	 * @param lastUserActivity
	 *            the m_timeLastUserActivity to set
	 */
	public void setLastUserActivity(int lastUserActivity) {
		m_timeLastUserActivity = lastUserActivity;
	}

}

package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVQueryRequest message. <br>
 * <br>
 * This message is sent from the SDV Server to the Client to query the current
 * sourceId. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBQueryRequest extends SDBMessage {

	final short SDVQueryRequestMsgLength = 12;

	private short m_reserved;

	private int[] messageFieldSegments = new int[] { 2 };

	/**
	 * constructor
	 */
	public SDBQueryRequest() {
		super(SDVConstants.SDVMessageID_SDVQueryRequest);
		if (Log.INFO_ON) {
			Log.printInfo("SDVQueryRequest");
		}

	} // SDVQueryRequest()

	// test Pellos
	public void setReserved(short reserved){
		m_reserved = reserved;
	}

	/**
	 * Returns the reserved short in this message. This should always be 0xFFFF.
	 */
	public short getReserved() {
		return m_reserved;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Parses an incoming SDVQueryRequest message.
	 *
	 * @param dis
	 *            a DataInputStream object containing the SDVQueryRequest
	 *            message
	 */
	public short parse(DataInputStream dis) {
		short ret = super.parse(dis);

		if (ret == SDVConstants.FORMAT_OK) {
			try {
				m_reserved = dis.readShort();

				if (m_reserved != ((short)0xFFFF)) {
					Log.printWarning("SDVQueryRequest.parse(): ERROR - m_reserved = 0x"
									+ Integer.toHexString(m_reserved));
					return SDVConstants.FORMAT_ERROR;
				}
			} catch (IOException e) {
				Log.printWarning("SDVQueryRequest.parse(): " + e.getMessage());
                Log.print(e);
			}
		} // if(ret == SDVConstants.FORMAT_OK)

		return ret;

	} // parse(DataInputStream in)

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVQueryRequest\n";

		rc += super.toString();

		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reserved & 0xFFFF);

		return rc;

	} // toString()

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 */
	public int size() {
		return super.size() + SDVQueryRequestMsgLength;
	}


	// pellos Test
	public byte[] toByteArray() {
		byte[] rc = new byte[0];
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_reserved);
			rc = bo.toByteArray();

			out.close();
			out = null;
		} catch (IOException e) {
			Log.printWarning("SDVProgramSelectRequest.toByteArray(): "
					+ e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	}

}

package com.alticast.illico.epg.sdv.ccp;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.sdv.SDVConstants;

/**
 * This class encapsulates the SDVProgramSelectConfirm message. <br>
 * <br>
 * The SDV SERVER sends this message to the SDV CLIENT in response to the
 * SDBProgramSelectRequest message. <br>
 * <br>
 * The SDV CLIENT uses the tuning parameters received in this message
 * (modulation, freq, MPEG Program #) to access the requested SDV channel. <br>
 * <br>
 *
 * @author dkimble
 *
 */

public class SDBProgramSelectConfirm extends SDBMessage {

	final short SDVProgramSelectConfirmMsgLength = 18;

	private short m_response;
	private int m_sourceID;
	// private data
	private short m_privateDataLength;
	private byte m_reservedByte;
	private byte m_modulationFormat;
	private int m_tuningFrequency;
	private short m_MPEGProgramNumber;
	private short m_reservedShort;

	private int[] messageFieldSegments = new int[] { 2, 4, 2, 1, 1, 4, 2, 2 };

	// response codes
	public static final short rspOK = 0x0000;
	public static final short rspFormatError = 0x0001;
	public static final short rspBcProgramOutOfService = 0x0006;
	public static final short rspRedirect = 0x0007;
	public static final short rspInvalidSG = (short) 0x8001;
	public static final short rspUnknownClient = (short) 0x8002;
	public static final short rspSeNoResource = (short) 0x8003;
	public static final short rspNetBwNotAvail = (short) 0x8004;
	public static final short rspSrvCapacityExceeded = (short) 0x8005;
	public static final short rspUnknownError = (short) 0x80FF;

	// modulation format
	private final byte mfUnknownFormat = 0x00;
	private final byte mfQAM16 = 0x06;
	private final byte mfQAM32 = 0x07;
	private final byte mfQAM64 = 0x08;
	private final byte mfQAM128 = 0x0C;
	private final byte mfQAM256 = 0x10;

	/**
	 * constructor
	 */
	public SDBProgramSelectConfirm() {
		super(SDVConstants.SDVMessageID_SDVProgramSelectConfirm);
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectConfirm");
		}

	} // SDVProgramSelectConfirm()

	// test Pellos
	public void setResponse(short res) {
		m_response = res;
	}
	// test Pellos
	public void setSourceID(int src) {
		m_sourceID = src;
	}

	// test Pellos
	public void setModulationFormat(byte modulF) {
		m_modulationFormat = modulF;
	}

	// test Pellos
	public void setPrivateDataLength(short dataLength) {
		m_privateDataLength = dataLength;
	}

	// test Pellos
	public void setMassageLength(){
		setMessageLength((short) size());
	}

/**
	 * Returns the response code from the previous SDVProgramSelectRequest message.
	 * <br><br>
	 *
	 * Values are as follows:
	 * <TABLE>
	 * <TBODY>
	 * <THEAD>
	 * <TH>Code
	 * <TH>Value
	 * <TR><TD>rspOK<TD>0x0000
	 * <TR><TD>rspFormatError<TD>0x0001
	 * <TR><TD>rspInvalidSG<TD>0x8001
	 * <TR><TD>rspUnknownClient<TD>0x8002
	 * <TR><TD>rspSeNoResource<TD>0x8003
	 * <TR><TD>rspNetBwNotAvail<TD>0x8004
	 * <TR><TD>rspSrvCapacityExceeded<TD>0x8005
	 * <TR><TD>rspUnknownError<TD>0x80FF
	 * </TBODY>
	 * </TABLE>
	 */
	public short getResponse() {
		return m_response;
	}

	/**
	 * Returns the sourceID from the previous SDVProgramSelectRequest message.
	 */
	public int getSourceID() {
		return m_sourceID;
	}

	/**
	 * Returns the length of the private data section of this message This
	 * should always be
	 */
	public int getPrivateDataLength() {
		return m_privateDataLength;
	}

	/**
	 * Returns the reserved byte in this SDVProgramSelectConfirm message. <br>
	 * <br>
	 * This should always be 0xFF.
	 */
	public byte getReservedByte() {
		return m_reservedByte;
	}

/**
	 * Returns modulation mode to use to tune to the sourceID requested by the SDV client
	 * in the previous SDVProgramSelectRequest message.
	 *
	 * <TABLE>
	 * <TBODY>
	 * <THEAD>
	 * <TH>Value
	 * <TH>Modulation
	 * <TR><TD>0x00<TD>Unknown modulation format
	 * <TR><TD>0x01-0x05<TD>Reserved
	 * <TR><TD>0x06<TD>QAM 16 modulation format
	 * <TR><TD>0x07<TD>QAM 32 modulation format
	 * <TR><TD>0x08<TD>QAM 64 modulation format
	 * <TR><TD>0x09-0x0B<TD>Reserved
	 * <TR><TD>0x0C<TD>QAM 128 modulation format
	 * <TR><TD>0x0D-0x0F<TD>Reserved
	 * <TR><TD>0x10<TD>QAM 256 modulation format
	 * <TR><TD>0x11-0xFF<TD>Reserved
	 * </TBODY>
	 * </TABLE>
	 */
	public byte getModulationFormat() {
		return m_modulationFormat;
	}

	/**
	 * Returns tuning frequency (in Hz) to use to tune to the SDV sourceID
	 * requested by the SDV client in the previous SDVProgramSelectRequest
	 * message.
	 */
	public int getTuningFrequency() {
		return m_tuningFrequency;
	}

	/**
	 * Returns MPEG program number to use to tune to the SDV sourceID requested
	 * by the SDV client in the previous SDVProgramSelectRequest message.
	 */
	public short getMPEGProgramNumber() {
		return m_MPEGProgramNumber;
	}

	/**
	 * Returns the reserved short in this SDVProgramSelectConfirm message. This
	 * should always be 0xFFFF.
	 */
	public short getReservedShort() {
		return m_reservedShort;
	}

	/**
	 * Returns an int array which describes the sizes of all fields in this
	 * message. <br>
	 * <br>
	 * This allows a hex dump display of this message to include visual cues
	 * (e.g. ".") at field boundaries.
	 */
	public int[] getMessageFieldSegments() {
		int superNumFields = super.getMessageFieldSegments().length;
		int[] combinedArray = new int[superNumFields
				+ messageFieldSegments.length];

		System.arraycopy(super.getMessageFieldSegments(), 0, combinedArray, 0,
				superNumFields);
		System.arraycopy(messageFieldSegments, 0, combinedArray,
				superNumFields, messageFieldSegments.length);

		return combinedArray;

	} // getMessageFieldSegments()

	/**
	 * Parses an incoming SDVProgramSelectConfirm message.
	 *
	 * @param dis
	 *            a DataInputStream object containing the
	 *            SDVProgramSelectConfirm message
	 */
	public short parse(DataInputStream dis) {
		if (Log.INFO_ON) {
			Log.printInfo("SDVProgramSelectConfirm.parse()");
		}

		short ret = super.parse(dis);
		Log.printDebug("SDV: ret =" + ret);
		if (ret == SDVConstants.FORMAT_OK) {
			try {
				m_response = dis.readShort();

				if (m_response != SDVConstants.FORMAT_OK) {
					Log.printWarning("SDVProgramSelectConfirm.parse(): ERROR - m_response = "
									+ responseToString(m_response));
				}
				m_sourceID = dis.readInt();
				m_privateDataLength = dis.readShort();
				m_reservedByte = dis.readByte();
				m_modulationFormat = dis.readByte();
				m_tuningFrequency = dis.readInt();
				m_MPEGProgramNumber = dis.readShort();
				m_reservedShort = dis.readShort();

				Log.printDebug("SDV: m_sourceID =" + m_sourceID);
				Log.printDebug("SDV: m_privateDataLength =" + (m_privateDataLength & 0xFFFF));
				Log.printDebug("SDV: m_reservedByte =" + m_reservedByte);
				Log.printDebug("SDV: m_modulationFormat =" + m_modulationFormat);
				Log.printDebug("SDV: m_tuningFrequency =" + m_tuningFrequency);
				Log.printDebug("SDV: m_MPEGProgramNumber =" + m_MPEGProgramNumber);
				Log.printDebug("SDV: m_reservedShort =" + (m_reservedShort & 0xFFFF));
			} catch (IOException e) {
				Log.printWarning("SDVProgramSelectConfirm.parse(): "
						+ e.getMessage());
                Log.print(e);
				ret = SDVConstants.FORMAT_ERROR;
			}
		} // if(ret == SDVConstants.FORMAT_OK)

		return ret;

	} // parse(DataInputStream in)

	/**
	 * Returns a string describing the contents of this message in human
	 * readable form.
	 */
	public String toString() {
		String rc;

		rc = "\n\tSDVProgramSelectConfirm\n";

		rc += super.toString();

		rc += "\t\tResponse               = 0x"
				+ Integer.toHexString(m_response & 0xFFFF) + " = "
				+ responseToString(m_response) + "\n";
		rc += "\t\tsourceID               = " + m_sourceID + "\n";
		rc += "\t\tPrivate Data Length    = " + m_privateDataLength + "\n";
		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedByte & 0xFF) + "\n";
		rc += "\t\tModulation Format      = 0x"
				+ Integer.toHexString(m_modulationFormat) + " = "
				+ modulationFormatToString(m_modulationFormat) + "\n";
		rc += "\t\tTuning Frequency       = " + m_tuningFrequency + "Hz\n";
		rc += "\t\tMPEG Program Number    = " + m_MPEGProgramNumber + "\n";
		rc += "\t\tReserved               = 0x"
				+ Integer.toHexString(m_reservedShort & 0xFFFF) + "\n";

		return rc;

	} // toString()

	/**
	 * Returns a string representation of the response code.
	 *
	 * See the description of {@link #getResponse()} for details on this field.
	 */
	public String responseToString(short response) {
		switch (response) {
		case rspOK:
			return "rspOK";
		case rspFormatError:
			return "rspFormatError";
		case rspBcProgramOutOfService:
			return "rspBcProgramOutOfService";
		case rspRedirect:
			return "rspRedirect";
		case rspInvalidSG:
			return "rspInvalidSG";
		case rspUnknownClient:
			return "rspUnknownClient";
		case rspSeNoResource:
			return "rspSeNoResource";
		case rspNetBwNotAvail:
			return "rspBwNotAvail";
		case rspSrvCapacityExceeded:
			return "rspSrvCapacityExceeded";
		case rspUnknownError:
			return "rspUnknownError";
		default:
			return "ERROR - ILLEGAL RESPONSE CODE!";
		}

	} // responseToString()

	/**
	 * Returns a string representation of the modulation format.
	 *
	 * See the description of {@link #getModulationFormat()} for details on this
	 * field.
	 */
	public String modulationFormatToString(short modulationFormat) {
		switch (modulationFormat) {
		case mfUnknownFormat:
			return "Unknown modulation format";
		case mfQAM16:
			return "QAM 16";
		case mfQAM32:
			return "QAM 32";
		case mfQAM64:
			return "QAM 64";
		case mfQAM128:
			return "QAM 128";
		case mfQAM256:
			return "QAM 256";
		default:
			return "ERROR - ILLEGAL MODULATION FORMAT!";
		}

	} // modulationFormatToString()

	/**
	 * Returns the size of this message, including DSM-CC header and the
	 * sessionID from {@link SDBMessage}
	 */
	public int size() {
		return super.size() + SDVProgramSelectConfirmMsgLength;
	}


	// pellos Test
	public byte[] toByteArray() {
		byte[] rc = new byte[0];
		byte[] s = super.toByteArray();
		ByteArrayOutputStream bo = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(bo);

		try {
			out.write(s);

			out.writeShort(m_reservedShort);
			out.writeInt(m_sourceID);

			out.writeShort(m_privateDataLength);
			out.writeByte((byte)0xFF);
			out.writeByte(m_modulationFormat);
			out.writeInt(m_tuningFrequency);
			out.writeShort(m_MPEGProgramNumber);
			out.writeShort((short)0xFFFF);

			rc = bo.toByteArray();

			out.close();
			out = null;
		} catch (IOException e) {
			Log.printWarning("SDVProgramSelectRequest.toByteArray(): "
					+ e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException ioe) {
					// do nothing
				}
			}
		}

		return rc;

	}

}

package com.alticast.illico.epg.sdv;

import java.util.Calendar;

import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import org.ocap.shared.dvr.*;
import org.ocap.net.*;
import javax.tv.locator.*;
import java.util.*;
import java.text.*;

public class Session {
    public static final String EMPTY = "-";
    public static final String[] DISPLAY_TEXT = { "", "Main", "PIP" };

    public static final short STATUS_IDLE          = 0;
    public static final short STATUS_PENDING       = 1;
    public static final short STATUS_UNAVAILABLE   = 2;
    public static final short STATUS_READY         = 3;

    public short status = STATUS_IDLE;

    byte[] sessionId;
    public long time;
    public long waitStartTime;

    public ChannelController channelController;
    // 다른 보고 있는 것을 뺏어서 간 경우, 실패하면 다시 주기로 한다.
    public ChannelController previousController;
    public Channel channel;
    public int request = 0;
    public int sourceId;

    int freq;

    String sessionIdString;

    public Session(byte[] sessionBytes) {
        sessionId = sessionBytes;
        StringBuffer sb = new StringBuffer(40);
        for (int i = 0; i < 7; i++) {
            byte b = sessionBytes[i];
            sb.append(Character.forDigit(b >> 4 & 0xf, 16));
            sb.append(Character.forDigit(b & 0xf, 16));
            sb.append('.');
        }
        sessionIdString = sb.toString().toUpperCase();
    }

    public byte[] getSessionId() {
        return sessionId;
    }

    public synchronized void setChannel(int frq, Channel ch, short newStatus) {
        this.channel = ch;
        this.freq = frq / 1000000;
        this.status = status;
        this.sourceId = ch.getSourceId();
        this.status = newStatus;
        this.time = System.currentTimeMillis();
    }

    public synchronized void resetRecording(int freq, int pn, int mod, int sid) {
        if (request <= 0) {
            return;
        }
        RecordingRequest req = RecordingStatus.getInstance().getRecordingRequest(request);
        if (req == null) {
            return;
        }
        int reqSid = AppData.getInteger(req, AppData.SOURCE_ID);
        if (reqSid != sid) {
            return;
        }
        RecordingSpec spec = req.getRecordingSpec();
        if (spec == null || !(spec instanceof LocatorRecordingSpec)) {
            return;
        }
        LocatorRecordingSpec locSpec = (LocatorRecordingSpec) spec;
        Locator[] loc = locSpec.getSource();
        OcapLocator newLocator = null;
        try {
            String form = loc[0].toExternalForm();
            newLocator = new OcapLocator(freq, pn, mod);
            String newForm = newLocator.toExternalForm();
            if (Log.DEBUG_ON) {
                Log.printDebug("Session.resetRecording: check locator = " + newForm.equals(form));
            }
            if (newForm.equals(form)) {
                AppData.set(req, AppData.SDV_STATUS, new Integer(SDVSessionManager.SDV_RECORDING_OK));
                return;
            }
        } catch (Exception ex) {
            return;
        }
        if (newLocator == null) {
            return;
        }
        Log.printError("Session: recording should be rescheduled = " + newLocator);
        AppData.set(req, AppData.SDV_STATUS, new Integer(SDVSessionManager.SDV_RECORDING_PENDING));
        RecordingStatus.getInstance().replace(req, locSpec, newLocator, SDVSessionManager.SDV_RECORDING_OK);
        // TODO - delete and schedule - 그런데 session manager에서 event를 받을 듯 한데....

    }

//    public synchronized void stopChannel() {
//        channelController = null;
//        channel = null;
//        if (request == null) {
//            stopAll();
//        }
//    }
//
//    public synchronized void setRecording(RecordingRequest req) {
//        request = req;
//    }
//
//    public synchronized void stopRecording() {
//        request = null;
//        if (channelController == null) {
//            stopAll();
//        }
//    }

    public synchronized boolean isExpired() {
        if (channelController == null && request < 0
                    && System.currentTimeMillis() - waitStartTime > 30 * Constants.MS_PER_SECOND) {
            Log.printError("Session is expired = " + request + " : " + new Date(waitStartTime));
            return true;
        }
        return false;
    }

    public synchronized void stopAll() {
        status = STATUS_IDLE;
        request = 0;
        channel = null;
        previousController = null;
        channelController = null;
        sourceId = 0;
        freq = 0;
    }

    public synchronized void stopAndRecover() {
        ChannelController cc = previousController;
        stopAll();
        if (cc != null) {
            cc.changeChannel(cc.getCurrent());
        }
    }

    public synchronized void update() {
        if (status == STATUS_IDLE) {
            return;
        }
        ChannelController cc = channelController;
        if (cc == null) {
            return;
        }
//        NetworkInterface ni = cc.getNetworkInterface();
//        if (ni == null) {
//            return;
//        }
    }

    public byte getTunerUse() {
        boolean recording = request != 0;
        boolean ppv = channel != null && channel.getType() == Channel.TYPE_PPV;
        int display = getDisplay();
        return SDVSessionManager.getTunerUse(recording, ppv, display, false);
    }

    public int getDisplay() {
        ChannelController cc = channelController;
        if (cc == null) {
            return 0;
        } else {
            return cc.getVideoIndex() + 1;
        }
    }

    public boolean isRecording() {
        return request != 0;
    }

    public String toString() {
        return "Session-" + sessionIdString;
    }

    //////////////////////////////////////////////////////////////////////////
    // Diag
    //////////////////////////////////////////////////////////////////////////

    public void dump() {
        Log.printDebug("#####################################################");
        Log.printDebug(toString());
        print(SdvSessionStatus.NAME);
        Log.printDebug(SdvSessionStatus.LABLES[SdvSessionStatus.SOURCE_ID] +  " = " + String.valueOf(sourceId));
        print(SdvSessionStatus.TUNER_USE);
    }

    private void print(int index) {
        Log.printDebug(SdvSessionStatus.LABLES[index] +  " = " + getStatus(index));
    }

    private String getTunerUseString() {
        int display = getDisplay();
        if (!isRecording()) {
            if (display == 0) {
                return EMPTY;
            } else {
                return DISPLAY_TEXT[display];
            }
        } else {
            if (display == 0) {
                return "Rec(" + request + ")";
            } else {
                return DISPLAY_TEXT[display] + " ,Rec(" + request + ")";
            }
        }
    }

    public static String getStatusString(int status) {
        switch (status) {
            case STATUS_IDLE:
                return "Idle";
            case STATUS_PENDING:
                return "Pending";
            case STATUS_UNAVAILABLE:
                return "Unavailable";
            case STATUS_READY:
                return "Ready";
            default:
                return "Unknown";
        }
    }

    String getStatus(int id) {
        Channel ch = channel;
        switch (id) {
            case SdvSessionStatus.NAME:
                if (ch == null) {
                    return getStatusString(status);
                } else {
                    return "#" + ch.getNumber() + " - " + getStatusString(status);
                }
            case SdvSessionStatus.SESSION_ID:
                return sessionIdString;
            case SdvSessionStatus.SAMSVCID_TYPE:
                if (ch != null) {
                    return (ch instanceof SdvChannel) ? "Switched" : "Broadcast";
                }
                // TODO
                break;
            case SdvSessionStatus.SOURCE_ID:
                if (sourceId > 0) {
                    return String.valueOf(sourceId);
                }
                break;
            case SdvSessionStatus.ACT_TIME:
                if (time > 0) {
                    return new SimpleDateFormat(SdvDiagnostics.DATE_PATTERN).format(new Date(time));
                }
                break;
            case SdvSessionStatus.RETRIES_RESENDS:
                // TODO
            case SdvSessionStatus.RETUNES:
                break;
            case SdvSessionStatus.TUNER_STATUS:
                if (status == STATUS_IDLE) {
                    return "Inactive";
                } else {
                    return "Active";
                }
            case SdvSessionStatus.TUNER_USE:
                return getTunerUseString();
            case SdvSessionStatus.TV_REC_RSRC:
                // TODO
                break;
            case SdvSessionStatus.SDV_FREQ:
                if (freq > 0) {
                    return String.valueOf(freq);
                }
                break;
            case SdvSessionStatus.LAST_CCP_ERR:
                // TODO
            case SdvSessionStatus.SESSION_ERR_TIME:
                // TODO
                break;
        }
        return EMPTY;
    }

}

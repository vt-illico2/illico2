package com.alticast.illico.epg.sdv;

import com.alticast.illico.epg.sdv.ccp.*;
import com.alticast.illico.epg.sdv.mc.*;
import com.alticast.illico.epg.data.*;
import com.videotron.tvi.illico.log.Log;
import java.util.*;
import javax.tv.util.*;

public class RetryHandler {

	long timeout;
    long retryCount;
    long retryInterval;

    /** key = TransactionID, value = SdvRetryItem. */
    Hashtable table = new Hashtable();

    private static RetryHandler instance = new RetryHandler();

    public static RetryHandler getInstance() {
        return instance;
    }

    private RetryHandler() {
    }

    public void setConfig(SdvConfigParams config) {
        timeout = config.mMessageResponseTimeout;
        retryCount = config.mMessageRequestMaxRetriesCount;
        retryInterval = config.mMessageRequestRetryInterval;
    }

    public synchronized void messageSent(SDBMessage msg) {
        switch (msg.getMessageID()) {
            case SDVConstants.SDVMessageID_SDVInitRequest:
                break;
            case SDVConstants.SDVMessageID_SDVProgramSelectRequest:
                Channel ch = ((SDBProgramSelectRequest) msg).getChannel();
                if (ch != null && !(ch instanceof SdvChannel)) {
                    // do not wait for response
                    return;
                }
                break;
            default:
                return;
        }
        TransactionID tid = msg.getTransactionID();
        SdvRetryItem item = (SdvRetryItem) table.get(tid);
        if (item == null) {
            item = new SdvRetryItem(msg);
            table.put(tid, item);
        }
        item.sent();
    }

    public synchronized void messageReceived(SDBMessage msg) {
        TransactionID tid = msg.getTransactionID();
        Log.printInfo("RetryHandler: messageReceived = " + tid);
        Log.printDebug("RetryHandler: table = " + table);
        SdvRetryItem item = (SdvRetryItem) table.remove(tid);
        if (item != null) {
            Log.printInfo("RetryHandler: found item = " + item);
            item.reveived();
        }
    }

    class SdvRetryItem implements TVTimerWentOffListener {
        int count;
        SDBMessage msg;
        TVTimerSpec responseTimer = new TVTimerSpec();
        TVTimerSpec sendTimer = new TVTimerSpec();

        public SdvRetryItem(SDBMessage m) {
            msg = m;
            responseTimer.addTVTimerWentOffListener(this);
            sendTimer.addTVTimerWentOffListener(this);
        }

        public synchronized void sent() {
            count++;
            long delay = timeout;
            if (delay > 0) {
                responseTimer.setDelayTime(delay);
                responseTimer.setRepeat(false);
                TVTimer timer = TVTimer.getTimer();
                try {
                    timer.scheduleTimerSpec(responseTimer);
                } catch (TVTimerScheduleFailedException ex) {
                    Log.print(ex);
                }
            } else {
                Log.printWarning("SdvRetryItem: invalid timeout = " + delay);
            }
        }

        public synchronized void reveived() {
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(responseTimer);
            timer.deschedule(sendTimer);
        }

        public String toString() {
            return "SdvRetryItem[" + msg.getMessageID() + ";" + msg.getTransactionID() + ";" + count + "]";
        }

        public synchronized void timerWentOff(TVTimerWentOffEvent e) {
            TVTimerSpec spec = e.getTimerSpec();
            if (spec == responseTimer) {
                Log.printWarning("SdvRetryItem: response timeout = " + this);
                if (count <= retryCount) {
                    sendTimer.setDelayTime(retryInterval);
                    sendTimer.setRepeat(false);
                    TVTimer timer = TVTimer.getTimer();
                    try {
                        timer.scheduleTimerSpec(sendTimer);
                    } catch (TVTimerScheduleFailedException ex) {
                        Log.print(ex);
                    }
                } else {
                    Log.printWarning("SdvRetryItem: can't send anymore. retryCount = " + this);
                    table.remove(msg.getTransactionID());
                    SDVController.getInstance().notifyTimeout(msg);
                }
            } else if (spec == sendTimer) {
                Log.printInfo("SdvRetryItem: re-send message = " + this);
                if (msg instanceof SDBProgramSelectRequest) {
                    if (((SDBProgramSelectRequest) msg).isExpired()) {
                        Log.printInfo("SdvRetryItem: re-send ... expired.");
                        table.remove(msg.getTransactionID());
                        return;
                    }
                }
                SDVController.getInstance().push(msg, msg.getTransactionID());
            }
        }
    }

}


package com.alticast.illico.epg;

import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.framework.DataCenter;
import javax.tv.util.*;
import java.util.*;
import com.alticast.illico.epg.data.RecordingStatus;
//import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;

public class HeapManager implements TVTimerWentOffListener {

    private static final long CHECK_DELAY = 5 * Constants.MS_PER_MINUTE;

    private static final int FREE_RATIO_FOR_GC_NOW = 2;
    private static final int FREE_RATIO_FOR_GC_SKIP = 30;

    private static int FREE_MEMORY_PERCENT_CRITICAL = 5;
    private static int FREE_MEMORY_PERCENT_LOW = 10;

    private static HeapManager instance = new HeapManager();

    public static HeapManager getInstance() {
        return instance;
    }

    TVTimerSpec firstTimer = new TVTimerSpec();
    TVTimerSpec repeatTimer = new TVTimerSpec();

    long lastGcEndTime = 0;
    long lastGcProcessTime = 0;
    long lastBefore = 0;
    long lastAfter = 0;
    int count = 0;
    String lastUpdateStatus = "";

    private HeapManager() {
        repeatTimer.setDelayTime(CHECK_DELAY);
        repeatTimer.setRepeat(true);
        repeatTimer.setRegular(true);
        repeatTimer.addTVTimerWentOffListener(this);
        firstTimer.addTVTimerWentOffListener(this);

        DataCenter dataCenter = DataCenter.getInstance();
        FREE_MEMORY_PERCENT_CRITICAL = dataCenter.getInt("FREE_MEMORY_PERCENT_CRITICAL", 5);
        FREE_MEMORY_PERCENT_LOW = dataCenter.getInt("FREE_MEMORY_PERCENT_LOW", 10);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"HeapManager.start()");
        long time = System.currentTimeMillis();
        // next minute 17 second
        time = (time / Constants.MS_PER_MINUTE + 1) * Constants.MS_PER_MINUTE
               + 17 * Constants.MS_PER_SECOND;
        firstTimer.setAbsoluteTime(time);
        firstTimer.setRepeat(false);
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(firstTimer);
        try {
            timer.scheduleTimerSpec(firstTimer);
        } catch (TVTimerScheduleFailedException ex) {
            startMonitoring();
        }
    }

    private void startMonitoring() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"HeapManager.startMonitoring()");
        firstTimer.removeTVTimerWentOffListener(this);
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(repeatTimer);
        try {
            timer.scheduleTimerSpec(repeatTimer);
        } catch (TVTimerScheduleFailedException ex) {
            startMonitoring();
        }
    }

    public void gc(boolean force) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"HeapManager.gc("+force+")");
        Runtime runtime = Runtime.getRuntime();
        long beforeFree = runtime.freeMemory();
        long total = runtime.totalMemory();
        float beforePercent = (float) beforeFree * 100 / total;

        if (Log.INFO_ON) {
            Log.printInfo("HeapManager.check: current free = " + beforePercent + "%");
        }

        if (force) {
            Log.printInfo("HeapManager.gc: by force");
        } else if (beforePercent < FREE_RATIO_FOR_GC_NOW) {
            Log.printInfo("HeapManager.gc: not enough memory");
            return;
        } else if (beforePercent < FREE_RATIO_FOR_GC_SKIP) {
            long cur = System.currentTimeMillis();
            if (cur - UserActivityChecker.lastUserActivity < CHECK_DELAY) {
                Log.printDebug("HeapManager.gc: skip by user action");
                return;
            } else if (RecordingStatus.getInstance().getRecordingCount() > 0) {
                Log.printDebug("HeapManager.gc: skip by inprogress recording");
                return;
            } else {
                if (Log.INFO_ON) {
                    Log.printInfo("HeapManager.gc: lastUserActivity = " + new Date(UserActivityChecker.lastUserActivity));
                }
            }
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("HeapManager.gc: skip by sufficient memory.");
            }
            return;
        }
        long start = System.currentTimeMillis();
        System.gc();
        lastGcEndTime = System.currentTimeMillis();
        lastGcProcessTime = lastGcEndTime - start;
        long afterFree = runtime.freeMemory();

        float afterPercent = (float) afterFree * 100 / total;
        if (Log.INFO_ON) {
            Log.printInfo("HeapManager.gc: after free = " + afterPercent + "%");
        }
        lastBefore = beforeFree;
        lastAfter = afterFree;
        count++;
        lastUpdateStatus = Log.DEBUG_DATE_FORMAT.format(new Date(lastGcEndTime))
                    + ", count = " + count + ", " + lastGcProcessTime + "ms, "
                    + beforePercent + "% -> " + afterPercent + "%";

        if (afterPercent < FREE_MEMORY_PERCENT_CRITICAL) {
            Log.printError("not enough memory (critical) = " + afterPercent + "%");
        } else if (afterPercent < FREE_MEMORY_PERCENT_LOW) {
            Log.printWarning("not enough memory (low) = " + afterPercent + "%");
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"HeapManager.timerWentOff()");
        if (e.getTimerSpec() == repeatTimer) {
            gc(false);
			//[2014.11.06]Kenneth+June : Removed for Evergy Star tuner off 
            //MiniCarouselReader.getInstance().forceTuneWithFreeTuner();
        } else {
            startMonitoring();
        }
    }

    public String getStatus() {
        return "Last Manual GC: " + lastUpdateStatus;
    }

}

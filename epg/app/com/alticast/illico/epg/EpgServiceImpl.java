package com.alticast.illico.epg;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;
import com.alticast.illico.epg.tuner.ContentionHandler;
import com.alticast.illico.epg.data.ChannelDatabaseBuilder;
import com.alticast.illico.epg.data.ChannelDatabase;
import com.alticast.illico.epg.data.EpgDataManager;
import com.alticast.illico.epg.data.Program;
import com.alticast.illico.epg.data.RecordingStatus;
import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.sdv.SDVSessionManager;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.navigator.TunerManager;
import com.alticast.illico.epg.navigator.VolumeController;
import com.videotron.tvi.illico.epg.communication.CommunicationManager;
import java.rmi.RemoteException;
import java.rmi.Remote;
import java.awt.Rectangle;
import java.util.HashSet;
import java.util.Vector;
import java.util.Enumeration;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
//->Kenneth[2015.6.25] DDC-107
import com.videotron.tvi.illico.util.*;
//<-

public final class EpgServiceImpl implements EpgService {

    private XletContext xletContext;
    private static EpgServiceImpl instance = new EpgServiceImpl();

    private ChannelDatabaseBuilder channelDatabaseBuilder = ChannelDatabaseBuilder.getInstance();

    private static final Short COMPANION_LOCK = new Short((short) 1);

    public static EpgServiceImpl getInstance() {
        return instance;
    }

    private EpgServiceImpl() {
    }

    public ChannelContext getChannelContext(int videoIndex) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getChannelContext()");
        try {
            return ChannelController.get(videoIndex);
        } catch (ArrayIndexOutOfBoundsException ex) {
        } catch (NullPointerException ex) {
        }
        return null;
    }

    public void start(XletContext xletContext) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.start()");
        this.xletContext = xletContext;
        bindToIxc();
    }

    public void dispose() {
        try {
            IxcRegistry.unbind(xletContext, IXC_NAME);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    private void bindToIxc() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.bindToIxc()");
        try {
            IxcRegistry.bind(xletContext, IXC_NAME, this);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void notifyChannelPackageUpdated() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.notifyChannelPackageUpdated()");
        CaManager.getInstance().checkAllChannels(true);
    }

    public boolean isPpvWatching() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.isPpvWatching()");
        ChannelController cc = ChannelController.get(0);
        Channel ch = cc.getCurrent();
        return ch != null && ch.getType() != Channel.TYPE_PPV
                    && cc.getState() == ChannelController.CLEAR
                    && !ChannelBackground.getInstance().isRunning();
    }

    public boolean isAdultBlocked(TvProgram p) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.isAdultBlocked("+p+")");
        if (p instanceof Program) {
            return ParentalControl.getLevel((Program) p) == ParentalControl.ADULT_CONTENT;
        } else {
            return false;
        }
    }

    public VideoController getVideoController() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getVideoController()");
        return EpgCore.getInstance().getVideoResizer();
    }

    public void changeState(int state) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeState("+state+")");
        EpgCore.getInstance().changeState(state);
    }

    public void changeChannel(int id) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannel("+id+")");
        if (EpgCore.navigableStbMode) {
            ChannelController.get(0).changeChannel(id);
        } else {
            Log.printInfo("EpgService.changeChannel: not in valid STB mode");
        }
    }

    //->Kenneth[2017.10.13] VDTRMASTER-6218 : Standby->Running 시 delay 줘야 함.
    public void changeChannel(final int id, String msg) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannel("+id+", "+msg+")");
        try {
            if (EpgCore.navigableStbMode) {
                if (msg != null && msg.equals("MONITOR:RUNNING")) {
                    new Thread(){
                        public void run() {
                            try {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannel(): Before Thread.sleep()");
                                Thread.sleep(1000);
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER +"EpgServiceImpl.changeChannel(): After Thread.sleep() : Call changeChannel()");
                                ChannelController.get(0).changeChannel(id);
                            } catch (Exception e) {
                                Log.print(e);
                            }
                        }
                    }.start();
                } else {
                    ChannelController.get(0).changeChannel(id);
                }
            } else {
                Log.printInfo("EpgService.changeChannel: not in valid STB mode");
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //<-

    public void changeChannel(TvChannel ch) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannel("+ch+")");
        if (EpgCore.navigableStbMode) {
            ChannelController.get(0).changeChannel(ch);
        } else {
            Log.printInfo("EpgService.changeChannel: not in valid STB mode");
        }
    }

    public void changeChannelByNumber(int number) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannelByNumber("+number+")");
        if (EpgCore.navigableStbMode) {
            ChannelController.get(0).changeChannelByNumber(number);
        } else {
            Log.printInfo("EpgService.changeChannel: not in valid STB mode");
        }
    }

    public void changeChannelByRecordingChange(int recording, HashSet selectedChannels) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.changeChannelByRecordingChange("+recording+")");
        if (EpgCore.navigableStbMode) {
            if (EpgCore.epgVideoContext) {
                RecordingStatus.getInstance().changeChannelByRecordingChange(recording, selectedChannels);
            } else {
                TunerManager.getInstance().allocateTunerToRecording(recording);
            }
        } else {
            Log.printInfo("EpgService.changeChannel: not in valid STB mode");
        }
    }

    public void stopChannel() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.stopChannel()");
        ChannelController.get(0).stopChannel();
    }


    public TvChannel getDataChannel() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getDataChannel()");
        return ChannelDatabase.current.getDataChannel();
    }

    public TvChannel getCurrentChannel() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getCurrentChannel()");
        return ChannelController.get(0).getCurrentChannel();
    }

    public TvChannel getChannel(int id) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getChannel("+id+")");
        return ChannelDatabase.current.getChannelById(id);
    }

    public TvChannel getChannel(String callLetter) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getChannel("+callLetter+")");
        return ChannelDatabase.current.getChannelByName(callLetter);
    }

    public TvChannel getChannelByNumber(int number) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getChannelByNumber("+number+")");
        return ChannelDatabase.current.getChannelByNumber(number);
    }

    public TvProgram getProgram(int id, long time) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getProgram("+id+")");
        return EpgDataManager.getInstance().getProgram(
            ChannelDatabase.current.getChannelById(id), time);
    }

    public TvProgram getProgram(String callLetter, long time) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getProgram("+callLetter+")");
        return EpgDataManager.getInstance().getProgram(
            channelDatabaseBuilder.getDatabase().getChannelByName(callLetter), time);
    }

    public TvProgram getProgram(TvChannel ch, long time) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getProgram("+ch+")");
        return getProgram(ch.getId(), time);
    }

    public TvProgram getProgramByChannelNumber(int number, long time) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getProgramByChannelNumber("+number+")");
        return EpgDataManager.getInstance().getProgram(
            channelDatabaseBuilder.getDatabase().getChannelByNumber(number), time);
    }

    public TvProgram requestProgram(String callLetter, long time) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestProgram("+callLetter+")");
        return EpgDataManager.getInstance().requestProgram(callLetter, time);
    }

    public TvProgram getCurrentProgram(int id) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getCurrentProgram("+id+")");
        return EpgDataManager.getInstance().getCurrentProgram(
            channelDatabaseBuilder.getDatabase().getChannelById(id));
    }

    public void addChannelEventListener(ChannelEventListener l) throws RemoteException {
        ChannelController.get(0).addChannelEventListener(l);
    }

    public void removeChannelEventListener(ChannelEventListener l) throws RemoteException {
        ChannelController.get(0).removeChannelEventListener(l);
    }

    public void addEpgDataListener(EpgDataListener l) throws RemoteException {
        EpgDataManager.getInstance().addListener(l);
    }

    public void removeEpgDataListener(EpgDataListener l) throws RemoteException {
        EpgDataManager.getInstance().removeListener(l);
    }

    public void addCaUpdateListener(CaUpdateListener l) throws RemoteException {
        CaManager.getInstance().addListener(l);
    }

    public void removeCaUpdateListener(CaUpdateListener l) throws RemoteException {
        CaManager.getInstance().removeListener(l);
    }

    public void selectSdvHomeChannel() throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.selectSdvHomeChannel()");
        MiniCarouselReader.getInstance().selectHomeChannel();
    }

    public synchronized String requestSdvRecording(int sourceId) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestSdvRecording("+sourceId+")");
        String loc = SDVSessionManager.getInstance().requestSdvRecording(sourceId);
        Log.printInfo("EpgService.requestSdvRecording: returns " + loc);
        return loc;
    }

    public synchronized void notifyRequestedSdvRecordingFailed(int sourceId) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.notifyRequestedSdvRecordingFailed("+sourceId+")");
        SDVSessionManager.getInstance().notifyRequestedSdvRecordingFailed(sourceId);
    }

    public TvProgram getHighestRatingProgram(String callLetter, long from, long to) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.getHighestRatingProgram("+callLetter+")");
        Program p = EpgDataManager.getInstance().getProgram(channelDatabaseBuilder.getDatabase().getChannelByName(callLetter), from);
        if (p == null) {
            return null;
        }
        Program high = p;
        while ((p = p.next) != null && p.startTime < to) {
            if (p.getRatingIndex() > high.getRatingIndex()) {
                high = p;
            }
        }
        return high;
    }

    public RemoteIterator requestProgramsByTitle(String callLetter, String title, long from) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestProgramsByTitle("+title+")");
        Vector vector = EpgDataManager.getInstance().requestProgramsByTitle(callLetter, title, from);
        if (vector == null || vector.size() == 0) {
            return findProgramsByTitle(callLetter, title, from, Long.MAX_VALUE);
        } else {
            return new RemoteIteratorImpl(vector);
        }
    }

    public RemoteIterator requestPrograms(String callLetter, String programId, String seriesId, long from) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestPrograms("+programId+")");
        Vector vector = EpgDataManager.getInstance().requestPrograms(callLetter, programId, seriesId, from);
        if (vector == null || vector.size() == 0) {
            return findPrograms(callLetter, programId, seriesId, from, Long.MAX_VALUE);
        } else {
            return new RemoteIteratorImpl(vector);
        }
    }

    public RemoteIterator findPrograms(String callLetter, String programId, String seriesId, long from, long to) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.findPrograms("+programId+")");
        Vector vector = null;
        if (programId != null) {
            vector = EpgDataManager.getInstance().findProgramsById(callLetter, programId, from, to);
        } else if (seriesId != null) {
            vector = EpgDataManager.getInstance().findSeries(callLetter, seriesId, from, to);
        }
        return new RemoteIteratorImpl(vector);
    }

    public RemoteIterator findProgramsByTitle(String callLetter, String title, long from, long to) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.findProgramsByTitle("+title+")");
        Vector vector = EpgDataManager.getInstance().findProgramsByTitle(callLetter, title, from, to);
        return new RemoteIteratorImpl(vector);
    }

    public RemoteIterator findEpisodes(String callLetter, String seriesId, String programId) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.findEpisode("+seriesId+")");
        return new RemoteIteratorImpl(EpgDataManager.getInstance().findEpisodes(callLetter, seriesId, programId));
    }

/*
    public boolean requestTuner() {
        Log.printInfo("EpgService.requestTuner");
        boolean check = ContentionHandler.getInstance().checkFreeTuner();
        Log.printInfo("EpgService.checkFreeTuner = " + check);
        if (check) {
            return true;
        }
        // TODO
//        TunerRequestor requestor = new TunerRequestor();
//        PopupController.getInstance().showTunerRequest(requestor);
//        try {
//            synchronized (requestor) {
//                requestor.wait();
//            }
//        } catch (Exception ex) {
//            Log.print(ex);
//        }
//        return requestor.result;
        return false;
    }
*/

    public int showProgramDetails(final String[] params) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.showProgramDetails("+params+")");
        synchronized (COMPANION_LOCK) {
            final EpgCore core = EpgCore.getInstance();
            if (!EpgDataManager.getInstance().isValid(Long.parseLong(params[2]))) {
                return -110;
            }
            if (core.getEpgState() == STATE_GUIDE) {
                core.gridEpg.searchActionAdapter.actionRequested("", params);
            } else {
                if ("VOD".equals(EpgCore.getInstance().getVideoContextName())) {
                    try {
                        CommunicationManager.getMonitorService().exitToChannel();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                    Thread t = new Thread() {
                        public void run() {
                            try {
                                sleep(500);
                            } catch (Exception ex) {
                            }
                            core.startUnboundApp("EPG", params);
                        }
                    };
                    t.start();
                } else {
                    core.startUnboundApp("EPG", params);
                }
            }
            return 0;
        }
    }

    public void toggleMute() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.toggleMute()");
        VolumeController.getInstance().toggleMute();
    }

    //->Kenneth[2015.6.25] DDC-107
    public int requestTune(int channelNumber) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune("+channelNumber+")");
        synchronized (COMPANION_LOCK) {
            Channel ch = ChannelDatabase.current.getChannelByNumber(channelNumber);
            if (ch == null) {
                throw new IllegalArgumentException("channel not found : no = " + channelNumber);
            }

            boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune() : canPlayUhd = "+canPlayUhd);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune() : defintion = "+ch.getDefinition());
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune() : SUPPORT_UHD = "+Environment.SUPPORT_UHD);
            if (ch.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD && !canPlayUhd) {
                return -134;
            }

            //->Kenneth[2015.7.29] HDCP2.2 체크도 추가
            if (ch.getDefinition() == Channel.DEFINITION_4K && Environment.SUPPORT_UHD && VideoOutputUtil.getInstance().existHdcpApi()) {
                boolean hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune() : hdcp22Supported = "+hdcp22Supported);
                if (!hdcp22Supported) {
                    Log.printDebug("EpgServceImpl.requestTune: returns -134 -- HDCP2.2");
                    return -134;
                }
            }
            //<-

            ChannelController cc = ChannelController.get(0);
            boolean ableToTune = TunerManager.getInstance().checkTune(cc, ch);
            if (ableToTune) {
                if (EpgCore.getInstance().getMonitorState() != MonitorService.FULL_SCREEN_APP_STATE) {
                    cc.changeChannel(ch, true, true, false);
                } else {
                    try {
                        CommunicationManager.getMonitorService().exitToChannel(ch.getId());
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            } else {
                throw new IllegalStateException("no tuners avaliable");
            }
            boolean isSubscribed = ch.isSubscribed();
            if (isSubscribed) return 0;
            return -107;
        }
    }
    /*
    public boolean requestTune(int channelNumber) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.requestTune("+channelNumber+")");
        synchronized (COMPANION_LOCK) {
            Channel ch = ChannelDatabase.current.getChannelByNumber(channelNumber);
            if (ch == null) {
                throw new IllegalArgumentException("channel not found : no = " + channelNumber);
            }
            ChannelController cc = ChannelController.get(0);
            boolean ableToTune = TunerManager.getInstance().checkTune(cc, ch);
            if (ableToTune) {
                if (EpgCore.getInstance().getMonitorState() != MonitorService.FULL_SCREEN_APP_STATE) {
                    cc.changeChannel(ch, true, true, false);
                } else {
                    try {
                        CommunicationManager.getMonitorService().exitToChannel(ch.getId());
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            } else {
                throw new IllegalStateException("no tuners avaliable");
            }
            return ch.isSubscribed();
        }
    }
    */
    //<-

    public void setLogStateListener(LogStateListener l) {
        ChannelController.get(0).setLogStateListener(l);
    }

    class RemoteIteratorImpl implements RemoteIterator {
        Vector vector;
        Enumeration enumeration;

        public RemoteIteratorImpl(Vector list) {
            if (list == null) {
                this.vector = new Vector();
            } else {
                this.vector = list;
            }
            enumeration = vector.elements();
        }

        public void reset() {
            enumeration = vector.elements();
        }

        public int size() {
            return vector == null ? 0 : vector.size();
        }

        public boolean hasNext() throws RemoteException {
            return enumeration.hasMoreElements();
        }

        public Remote next() throws RemoteException {
            if (enumeration.hasMoreElements()) {
                return (Remote) enumeration.nextElement();
            } else {
                return null;
            }
        }

    }


    //->Kenneth[2017.9.27] R7.4 : SDV Barker 를 Error Message 가 띄운후 팝업 닫을 때 호출되는 함수
    public void sdvErrorPopupClosed(String errorCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgServiceImpl.sdvErrorPopupClosed("+errorCode+")");
        ChannelBackground.getInstance().sdvErrorPopupClosed(errorCode);
    }
}

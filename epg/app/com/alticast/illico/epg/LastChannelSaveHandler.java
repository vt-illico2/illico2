package com.alticast.illico.epg;

import com.alticast.illico.epg.data.Channel;
import com.alticast.illico.epg.data.VirtualChannel;
import com.alticast.illico.epg.navigator.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.*;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextListener;
import javax.tv.util.*;
import org.dvb.user.*;

public class LastChannelSaveHandler implements DataUpdateListener, ServiceContextListener, TVTimerWentOffListener {

    private static final String UP_NAME_LAST_CHANNEL = "Last Channel";

    private static LastChannelSaveHandler instance = new LastChannelSaveHandler();

    public static LastChannelSaveHandler getInstance() {
        return instance;
    }

    boolean enabled = false;
    TVTimerSpec saveTimer;

    private LastChannelSaveHandler() {
        DataCenter.getInstance().addDataUpdateListener(PreferenceNames.POWER_ON_CHANNEL, this);
        saveTimer = new TVTimerSpec();
        saveTimer.setDelayTime(DataCenter.getInstance().getLong("LAST_CHANNEL_SAVE_DELAY", 5000L));
        saveTimer.addTVTimerWentOffListener(this);
    }

    public void dataUpdated(String key, Object old, Object value) {
        setEnabled(Definitions.LAST_CHANNEL.equals(value));
    }

    public void dataRemoved(String key) {
    }

    private synchronized void setEnabled(boolean en) {
        Log.printDebug(App.LOG_HEADER+"LastChannelSaveHandler.setEnabled: " + enabled + " -> " + en);
        if (enabled == en) {
            return;
        }
        this.enabled = en;
        if (enabled) {
            Environment.getServiceContext(0).addListener(this);
            receiveServiceContextEvent(null);
        } else {
            Environment.getServiceContext(0).removeListener(this);
        }
    }

    public String load() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"LastChannelSaveHandler.load()");
        return GeneralPreferenceUtil.read(UP_NAME_LAST_CHANNEL);
    }

    public void receiveServiceContextEvent(ServiceContextEvent e) {
        // TODO - blocked channel은 event가 안온다.
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(saveTimer);
        try {
            timer.scheduleTimerSpec(saveTimer);
        } catch (Exception ex) {
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"LastChannelSaveHandler.timerWentOff()");
        Channel ch = ChannelController.get(0).getCurrent();
        if (ch == null) {
            return;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"LastChannelSaveHandler.timerWentOff() : Call GeneralPreference.write()");
        GeneralPreferenceUtil.write(UP_NAME_LAST_CHANNEL, ch.getName());
    }

}

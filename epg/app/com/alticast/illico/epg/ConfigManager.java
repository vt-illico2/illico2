package com.alticast.illico.epg;

import java.io.File;
import java.util.HashSet;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.alticast.illico.epg.config.*;
import com.alticast.illico.epg.data.*;

/**
 * OOB, IB 등으로 오는 configuration 들을 관리.
 */
public class ConfigManager implements InbandDataListener, DataUpdateListener {

    /** File에서 만든 java object instance를 저장하기 위한 key suffix. */
    private static final String INSTANCE_SUFFIX = "_instance";

    // OOB, IB file이 저장되는 key
    public static final String CHANNEL_LOGO  = "CHANNEL_LOGO";

    //->Kenneth[2015.4.16] R5 
    public static final String TUTORIAL_IMAGE = "TUTORIAL_IMAGE";

    public static final String EPG_CONFIG = "EPG_CONFIG";
    public static final String PPV_CONFIG = "PPV_CONFIG";   // not using
    public static final String CHANNEL_INFO  = "CHANNEL_INFO";
    public static final String CHANNEL_TYPE  = "CHANNEL_TYPE";
    public static final String VIRTUAL_CHANNEL_MAP  = "VIRTUAL_CHANNEL_MAP";
    public static final String VIRTUAL_CHANNEL_DATA = "VIRTUAL_CHANNEL_DATA";
    public static final String SDV_SAM  = "SDV_SAM";
    //->Kenneth[2017.9.26] R7.4 에서는 SDV_Barker_Messages.txt 더 이상 안 씀
    //public static final String SDV_BARKER  = "SDV_BARKER";
    //<-
    public static final String SDV_POPUP = "SDV_POPUP";
    public static final String FREE_PREVIEWS = "FREE_PREVIEWS";
    public static final String PREVIEW_CHANNELS = "PREVIEW_CHANNELS";

    // Instance를 저장하기 위한 key
    public static final String EPG_CONFIG_INSTANCE           = EPG_CONFIG + INSTANCE_SUFFIX;
    public static final String PPV_CONFIG_INSTANCE           = PPV_CONFIG + INSTANCE_SUFFIX;
    public static final String CHANNEL_INFO_INSTANCE         = CHANNEL_INFO + INSTANCE_SUFFIX;
    public static final String CHANNEL_TYPE_INSTANCE         = CHANNEL_TYPE + INSTANCE_SUFFIX;
    public static final String VIRTUAL_CHANNEL_MAP_INSTANCE  = VIRTUAL_CHANNEL_MAP + INSTANCE_SUFFIX;
    public static final String VIRTUAL_CHANNEL_DATA_INSTANCE  = VIRTUAL_CHANNEL_DATA + INSTANCE_SUFFIX;
    public static final String SDV_SAM_INSTANCE              = SDV_SAM + INSTANCE_SUFFIX;
    //->Kenneth[2017.9.26] R7.4 에서는 SDV_Barker_Messages.txt 더 이상 안 씀
    //public static final String SDV_BARKER_INSTANCE           = SDV_BARKER + INSTANCE_SUFFIX;
    //<-
    public static final String SDV_POPUP_INSTANCE            = SDV_POPUP + INSTANCE_SUFFIX;

    private DataCenter dataCenter = DataCenter.getInstance();

    // Instance
    public PpvConfig ppvConfig;
    public EpgConfig epgConfig;
    public ChannelLogos channelLogos;
    public ChannelInfoData channelInfo;
    public ChannelType channelType;
    public VirtualChannelMap virtualChannelMap;
    public VirtualChannelData virtualChannelData;
    public SdvSam sdvSam;
    public HashSet freePreviews = new HashSet();
    public HashSet isaChannels = new HashSet();
    //->Kenneth[2016.8.17] CYO Bundling/A la carte
    public HashSet isaExcludedChannels = new HashSet();
    public int[] isaExcludedTypes = null;

    // type 이 ISA excluded types 에 포함되는지 돌려준다. 
    public boolean isISAExcludedType(int type) {
        if (isaExcludedTypes == null) return false;
        for (int i = 0 ; i < isaExcludedTypes.length ; i++) {
            if (isaExcludedTypes[i] == type) return true;
        }
        return false;
    }
    //<-

    private static final ConfigManager instance = new ConfigManager();

    public static ConfigManager getInstance() {
        return instance;
    }

    private ConfigManager() {
        // OOB, IB files
        dataCenter.addDataUpdateListener(EPG_CONFIG, this);
        dataCenter.addDataUpdateListener(PPV_CONFIG, this);  // not using
        dataCenter.addDataUpdateListener(CHANNEL_INFO, this);   // channel 부가정보
        dataCenter.addDataUpdateListener(CHANNEL_TYPE, this);   // not using
        dataCenter.addDataUpdateListener(CHANNEL_LOGO, this);
        dataCenter.addDataUpdateListener(VIRTUAL_CHANNEL_MAP, this);    // virtual channel SI
        dataCenter.addDataUpdateListener(VIRTUAL_CHANNEL_DATA, this);   // virtual channel UI
        dataCenter.addDataUpdateListener(SDV_SAM, this);    // SDV channel SI
        //->Kenneth[2017.9.26] R7.4 에서는 SDV_Barker_Messages.txt 더 이상 안 씀
        //dataCenter.addDataUpdateListener(SDV_BARKER, this); // SDV tuning info, SDV popup message
        //<-
        dataCenter.addDataUpdateListener(SDV_POPUP, this);  // SDV popup title, button
        dataCenter.addDataUpdateListener(FREE_PREVIEWS, this);  // free preview channels
        dataCenter.addDataUpdateListener(PREVIEW_CHANNELS, this);   // free preview channel - EIDs mapping
        //->Kenneth[2015.4.16] R5 
        dataCenter.addDataUpdateListener(TUTORIAL_IMAGE, this);
    }

    /** Called by Monitor. */
    public void receiveInbandData(String locator) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ConfigManager.receiveInbandData = " + locator);
        }
        EpgDataManager.getInstance().startDownloading();
//        DataAdapterManager.getInstance().getInbandAdapter().synchronousLoad(locator);
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator, false);
        // 모든 IB data가 load될 때 까지 위 API는 block 된다.
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            monitor.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
        } catch (Exception e) {
            Log.print(e);
        }
        EpgDataManager.getInstance().readPrograms();
        CaManager.getInstance().readyToCheck();
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ConfigManager.dataUpdated : " + key);
        }
        File file = (File) value;
        if (EPG_CONFIG.equals(key)) {
            EpgConfig config = EpgConfig.create(file);
            if (config == null || config.equals(epgConfig)) {
                // skip
                return;
            }
            //->Kenneth[2015.4.18] R5 : old time 을 업데이트시에 전달. currentDisplay 를 reset 하기 위함. 
            long oldStartTime = 0L;
            long oldEndTime = 0L;
            if (this.epgConfig != null) {
                oldStartTime = epgConfig.tutorialStartDate;
                oldEndTime = epgConfig.tutorialEndDate;
            }
            this.epgConfig = config;
            DataCenter.getInstance().put(EPG_CONFIG_INSTANCE, config);
            //->Kenneth[2015.4.16] R5 
            Tutorial.getInstance().tutorialDataUpdated(oldStartTime, oldEndTime);
        } else if (PPV_CONFIG.equals(key)) {
            PpvConfig config = PpvConfig.create(file);
            if (config == null || config.equals(ppvConfig)) {
                // skip
                return;
            }
            this.ppvConfig = config;
            if (Log.DEBUG_ON) {
                Log.printDebug(ppvConfig);
            }
            DataCenter.getInstance().put(PPV_CONFIG_INSTANCE, config);
        } else if (CHANNEL_INFO.equals(key)) {
            ChannelInfoData config = ChannelInfoData.create(file);
            if (Log.DEBUG_ON) {
                Log.printDebug("old info = " + channelInfo);
                Log.printDebug("new info = " + config);
//                config.dump();
            }
            if (config == null || config.equals(channelInfo)) {
                // skip
                return;
            }
            this.channelInfo = config;
            DataCenter.getInstance().put(CHANNEL_INFO_INSTANCE, config);
            for (int i = 0; i < config.languageType.length; i++) {
                DataCenter.getInstance().put("epg.no_data_title_" + config.languageType[i], config.defaultText[i]);
            }
        } else if (CHANNEL_TYPE.equals(key)) {
            ChannelType config = ChannelType.create(file);
            if (Log.EXTRA_ON) {
                config.dump();
            }
            if (config == null || config.equals(channelType)) {
                // skip
                return;
            }
            this.channelType = config;
            DataCenter.getInstance().put(CHANNEL_TYPE_INSTANCE, config);
        } else if (CHANNEL_LOGO.equals(key)) {
            this.channelLogos = ChannelLogos.create(file);
            Thread t = new Thread(channelLogos);
            t.start();
        } else if (VIRTUAL_CHANNEL_MAP.equals(key)) {
            VirtualChannelMap config = VirtualChannelMap.create(file);
            if (config == null || config.equals(virtualChannelMap)) {
                // skip
                return;
            }
            this.virtualChannelMap = config;
            DataCenter.getInstance().put(VIRTUAL_CHANNEL_MAP_INSTANCE, config);
        } else if (VIRTUAL_CHANNEL_DATA.equals(key)) {
            VirtualChannelData config = VirtualChannelData.create(file);
            if (config == null || config.equals(virtualChannelData)) {
                // skip
                return;
            }
            this.virtualChannelData = config;
            DataCenter.getInstance().put(VIRTUAL_CHANNEL_DATA_INSTANCE, config);
        } else if (SDV_SAM.equals(key)) {
            SdvSam config = SdvSam.create(file);
            if (config == null || config.equals(sdvSam)) {
                // skip
                return;
            }
//            config.dump();
            this.sdvSam = config;
            DataCenter.getInstance().put(SDV_SAM_INSTANCE, config);
        //->Kenneth[2017.9.26] R7.4 에서는 SDV_Barker_Messages.txt 더 이상 안 씀
        /*
        } else if (SDV_BARKER.equals(key)) {
            SdvBarker config = SdvBarker.create(file);
            if (config == null) {
                // skip
                return;
            }
            DataCenter.getInstance().put(SDV_BARKER_INSTANCE, config);
        */
            //<-
        } else if (SDV_POPUP.equals(key)) {
            SdvPopup config = SdvPopup.create(file);
            if (config == null) {
                // skip
                return;
            }
            DataCenter.getInstance().put(SDV_POPUP_INSTANCE, config);
        } else if (FREE_PREVIEWS.equals(key)) {
            FreePreviews config = FreePreviews.create(file);
            if (config == null) {
                return;
            }
            Log.printDebug(config);
            this.freePreviews = config.getChannelNames();
        } else if (PREVIEW_CHANNELS.equals(key)) {
            PreviewChannels config = PreviewChannels.create(file);
            if (config == null) {
                return;
            }
            PreviewChannels.setCurrent(config);
        //->Kenneth[2015.4.16] R5 
        } else if (TUTORIAL_IMAGE.equals(key)) {
            Tutorial.getInstance().resetImage(value);
        }
    }

    public void dataRemoved(String key)  {
    }

    public static int oneByteToInt(byte[] data, int offset) {
        return ((int) data[offset]) & 0xFF;
    }

    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return ((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF);
    }

    public static int threeBytesToInt(byte[] data, int offset) {
        return ((0xFF & data[offset]) << 16)
             | ((0xFF & data[offset + 1]) << 8)
             | ((0xFF & data[offset + 2]));
    }

    public static int fourBytesToInt(byte[] data, int offset) {
        return ((0xFF & data[offset]) << 24)
             | ((0xFF & data[offset + 1]) << 16)
             | ((0xFF & data[offset + 2]) << 8)
             | ((0xFF & data[offset + 3]));
    }

    //->Kenneth[2015.4.9] R5 : Tutorial 
    public static long eightBytesToLong(byte[] data, int offset) {
        return ((long)(0xFF & data[offset]) << 56)
             | ((long)(0xFF & data[offset + 1]) << 48)
             | ((long)(0xFF & data[offset + 2]) << 40)
             | ((long)(0xFF & data[offset + 3]) << 32)
             | ((long)(0xFF & data[offset + 4]) << 24)
             | ((long)(0xFF & data[offset + 5]) << 16)
             | ((long)(0xFF & data[offset + 6]) << 8)
             | ((long)(0xFF & data[offset + 7]));
    }

    public static String byteArrayToString(byte[] data, int offset, int length) {
        return new String(data, offset, length);
    }

}

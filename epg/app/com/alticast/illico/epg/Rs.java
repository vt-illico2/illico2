package com.alticast.illico.epg;

import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.upp.*;
import com.alticast.illico.epg.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.menu.*;
import com.alticast.illico.epg.ppv.*;
import com.alticast.ui.*;

/**
 * This class is Resource class of EPG.
 *
 * @author  June Park
 */
public class Rs {
    //->Kenneth[2015.2.3] 4K
    public static final MenuItem MENU_MORE_INFO = new MenuItem("menu.more_info");

    // 주의 : 각 text파일과 맞아야 함.
    public static final MenuItem MENU_CLOSE = new MenuItem("epg.close");
    public static final MenuItem MENU_VIEW_PROGRAM = new MenuItem("menu.view_this_program");
    public static final MenuItem MENU_TUNE_CHANNEL = new MenuItem("menu.tune_channel");
    public static final MenuItem MENU_TUNE_IN = new MenuItem("menu.tune_in");
    public static final MenuItem MENU_SET_REMINDER = new MenuItem("menu.set_reminder");
    public static final MenuItem MENU_REMOVE_REMINDER = new MenuItem("menu.remove_reminder");

    public static final MenuItem MENU_SET_ALERT = new MenuItem("menu.set_alert");
    public static final MenuItem MENU_REMOVE_ALERT = new MenuItem("menu.remove_alert");

    public static final MenuItem MENU_RECORD = new MenuItem("menu.record");
    public static final MenuItem MENU_RECORD_PROGRAM = new MenuItem("menu.record_this_program");
    public static final MenuItem MENU_CANCEL_RECORDING = new MenuItem("menu.cancel_the_recording");
    public static final MenuItem MENU_STOP_RECORDING = new MenuItem("menu.stop_recording");
    public static final MenuItem MENU_ADD_FAV_CHANNEL = new MenuItem("menu.add_fav_channel");
    public static final MenuItem MENU_REMOVE_FAV_CHANNEL = new MenuItem("menu.remove_fav_channel");
    public static final MenuItem MENU_BLOCK_CHANNEL = new MenuItem("menu.block_channel");
    public static final MenuItem MENU_UNBLOCK_CHANNEL = new MenuItem("menu.unblock_channel");
    public static final MenuItem MENU_GO_TO_PREFERENCE = new MenuItem("menu.go_to_preference");
    public static final MenuItem MENU_GO_TO_HELP = new MenuItem("menu.go_to_help");

    public static final MenuItem MENU_SORT_BY_NUMBER = new MenuItem("menu.sort_by_number");
    public static final MenuItem MENU_SORT_BY_NAME = new MenuItem("menu.sort_by_name");
    public static final MenuItem MENU_SORTING = new SortMenu("menu.sorting",
        new MenuItem[] {
            MENU_SORT_BY_NUMBER,
            MENU_SORT_BY_NAME,
        }
    );

    //->Kenneth[2017.3.16] R7.3 : D-option 에 search 추가됨
    public static final MenuItem MENU_SEARCH = new MenuItem("epg.search");
    //<-

    public static final MenuItem MENU_FILTER_HD = new MenuItem("filter.hd");
//    public static final MenuItem MENU_FILTER_SD = new MenuItem("filter.sd");


    public static final MenuItem MENU_FILTER_TYPE_ALL = new MenuItem("filter.type.all");
//    public static final MenuItem MENU_FILTER_TYPE_GAL_RADIO = new MenuItem("filter.type.gal_radio");
    public static final MenuItem MENU_FILTER_TYPE_PPV = new MenuItem("filter.type.ppv");
    public static final MenuItem MENU_FILTER_TYPE_SDV = new MenuItem("filter.tech");
//    public static final MenuItem MENU_FILTER_TYPE_SPORTSMAX = new MenuItem("filter.type.sportsmax");
//    public static final MenuItem MENU_FILTER_BY_TYPE = new MenuItem("menu.type",
//        new MenuItem[] {
//            MENU_FILTER_TYPE_ALL,
//            MENU_FILTER_TYPE_GAL_RADIO,
//            MENU_FILTER_TYPE_PPV,
//            MENU_FILTER_TYPE_SPORTSMAX,
//        }
//    );

    public static final MenuItem MENU_FILTER_FAVOURITES = new MenuItem("filter.favourites");
//    public static final MenuItem MENU_FILTER_CUSTOM = new MenuItem("filter.custom");
    public static final MenuItem MENU_FILTER_SUBSCRIBED = new MenuItem("filter.subscribed");
//    public static final MenuItem MENU_FILTER_UNSUBSCRIBED = new MenuItem("filter.unsubscribed");

//    public static final MenuItem MENU_FILTER_BY_PERSONALIZED = new MenuItem("menu.personalized_filter",
//        new MenuItem[] {
//            MENU_FILTER_FAVOURITES,
//            MENU_FILTER_CUSTOM,
//            MENU_FILTER_SUBSCRIBED,
//            MENU_FILTER_UNSUBSCRIBED,
//        }
//    );
//    public static final MenuItem MENU_FILTER_TECHNICIAN = new MenuItem("filter.tech");

//    public static final MenuItem MENU_FILTER_MY_LANGUAGE = new MenuItem("filter.channels_in_my_language");

    public static final MenuItem MENU_FILTER_ENGLISH = new MenuItem("filter.english");
    public static final MenuItem MENU_FILTER_FRENCH = new MenuItem("filter.french");
    public static final MenuItem MENU_FILTER_INTERNATIONAL = new MenuItem("filter.international");

    public static final MenuItem MENU_CHANGE_FILTER = new FilterMenu("menu.change_filter",
        new MenuItem[] {
            MENU_FILTER_TYPE_ALL,
            MENU_FILTER_SUBSCRIBED,
            MENU_FILTER_FAVOURITES,
//            MENU_FILTER_MY_LANGUAGE,
            MENU_FILTER_ENGLISH,
            MENU_FILTER_FRENCH,
            MENU_FILTER_INTERNATIONAL,
            // genre, sd, hd 는 아래에
        }
    );

    public static final MenuItem[] MENU_FILTER_GENRE = new MenuItem[5];
    static {
        for (int i = 0; i < MENU_FILTER_GENRE.length; i++) {
            MENU_FILTER_GENRE[i] = new MenuItem("filter.genre." + (i + 1));
            MENU_CHANGE_FILTER.add(MENU_FILTER_GENRE[i]);
        }
        MENU_CHANGE_FILTER.add(MENU_FILTER_HD);
        MENU_CHANGE_FILTER.add(MENU_FILTER_TYPE_PPV);
    }

//    public static final MenuItem MENU_FILTER_BY_GENRE = new MenuItem("menu.genre", MENU_FILTER_GENRE );

    public static final MenuItem MENU_LAUNCH_PIP = new MenuItem("menu.launch_pip");
    public static final MenuItem MENU_CLOSE_PIP = new MenuItem("menu.close_pip");
//    public static final MenuItem MENU_TURN_ON_PARENTAL_CONTROL = new MenuItem("menu.turn_on_parental_control");

    public static final MenuItem MENU_CAPTION_OFF = new UpMenuItem("menu.caption.off", PreferenceNames.CC_DISPLAY, Definitions.OPTION_VALUE_OFF);
    public static final MenuItem MENU_CAPTION_ON = new UpMenuItem("menu.caption.on", PreferenceNames.CC_DISPLAY, Definitions.OPTION_VALUE_ON);
    public static final MenuItem MENU_CAPTION_ON_WITH_MUTE = new UpMenuItem("menu.caption.on_with_mute", PreferenceNames.CC_DISPLAY, Definitions.OPTION_VALUE_ON_MUTE);
    public static final MenuItem MENU_INVOKE_CAPTION = new UpMenu("menu.invoke_caption", PreferenceNames.CC_DISPLAY,
        new MenuItem[] {
            MENU_CAPTION_OFF,
            MENU_CAPTION_ON,
            MENU_CAPTION_ON_WITH_MUTE,
        }
    );
    public static final MenuItem MENU_PICTURE_NORMAL = new UpMenuItem("menu.picture.normal", PreferenceNames.VIDEO_ZOOM_MODE, Definitions.VIDEO_ZOOM_MODE_NORMAL);
    public static final MenuItem MENU_PICTURE_WIDE = new UpMenuItem("menu.picture.wide", PreferenceNames.VIDEO_ZOOM_MODE, Definitions.VIDEO_ZOOM_MODE_WIDE);
    public static final MenuItem MENU_PICTURE_ZOOM = new UpMenuItem("menu.picture.zoom", PreferenceNames.VIDEO_ZOOM_MODE, Definitions.VIDEO_ZOOM_MODE_ZOOM);
    public static final MenuItem MENU_PICTURE = new UpMenu("menu.picture", PreferenceNames.VIDEO_ZOOM_MODE,
        new MenuItem[] {
            MENU_PICTURE_NORMAL,
            MENU_PICTURE_WIDE,
            MENU_PICTURE_ZOOM,
        }
    );
    
    //->Kenneth[2016.7.18] R7.2 Zoom key 처리 (option popup 의 header 바뀜)
    public static final MenuItem MENU_PICTURE_BY_SHARP = new UpMenu("menu.picture.by_sharp", PreferenceNames.VIDEO_ZOOM_MODE,
        new MenuItem[] {
            MENU_PICTURE_NORMAL,
            MENU_PICTURE_WIDE,
            MENU_PICTURE_ZOOM,
        }
    );
    //<-

    //->Kenneth[2017.2.22] R7.3 Highlight 를 Option Menu 에서 처리하도록 바뀜
    public static final MenuItem MENU_HIGHLIGHT_NONE = new UpMenuItem("epg.highlight_none", Highlight.PREF_KEY, Highlight.HIGHLIGHT_NONE);
    public static final MenuItem MENU_HIGHLIGHT0 = new UpMenuItem("epg.highlight0", Highlight.PREF_KEY, Highlight.HIGHLIGHT0);
    public static final MenuItem MENU_HIGHLIGHT1 = new UpMenuItem("epg.highlight1", Highlight.PREF_KEY, Highlight.HIGHLIGHT1);
    public static final MenuItem MENU_HIGHLIGHT2 = new UpMenuItem("epg.highlight2", Highlight.PREF_KEY, Highlight.HIGHLIGHT2);
    public static final MenuItem MENU_HIGHLIGHT3 = new UpMenuItem("epg.highlight3", Highlight.PREF_KEY, Highlight.HIGHLIGHT3);
    public static final MenuItem MENU_HIGHLIGHT4 = new UpMenuItem("epg.highlight4", Highlight.PREF_KEY, Highlight.HIGHLIGHT4);
    public static final MenuItem MENU_HIGHLIGHT5 = new UpMenuItem("epg.highlight5", Highlight.PREF_KEY, Highlight.HIGHLIGHT5);
    public static final MenuItem MENU_HIGHLIGHT6 = new UpMenuItem("epg.highlight6", Highlight.PREF_KEY, Highlight.HIGHLIGHT6);
    public static final MenuItem MENU_HIGHLIGHT7 = new UpMenuItem("epg.highlight7", Highlight.PREF_KEY, Highlight.HIGHLIGHT7);
    public static final MenuItem MENU_HIGHLIGHT8 = new UpMenuItem("epg.highlight8", Highlight.PREF_KEY, Highlight.HIGHLIGHT8);
    public static final MenuItem MENU_HIGHLIGHT = new UpMenu("epg.highlights", Highlight.PREF_KEY,
        new MenuItem[] {
            MENU_HIGHLIGHT_NONE,
            MENU_HIGHLIGHT0,
            MENU_HIGHLIGHT1,
            MENU_HIGHLIGHT2,
            MENU_HIGHLIGHT3,
            MENU_HIGHLIGHT4,
            MENU_HIGHLIGHT5,
            MENU_HIGHLIGHT6,
            MENU_HIGHLIGHT7,
            MENU_HIGHLIGHT8,
        }
    );
    //<-

    public static final MenuItem MENU_SAP_FRENCH = new UpMenuItem("menu.change_sap.french", PreferenceNames.SAP_LANGUAGE_AUDIO, Definitions.LANGUAGE_FRENCH);
    public static final MenuItem MENU_SAP_ENGLISH = new UpMenuItem("menu.change_sap.english", PreferenceNames.SAP_LANGUAGE_AUDIO, Definitions.LANGUAGE_ENGLISH);
    public static final MenuItem MENU_SAP_SPANISH = new UpMenuItem("menu.change_sap.spanish", PreferenceNames.SAP_LANGUAGE_AUDIO, Definitions.LANGUAGE_SPANISH);
    public static final MenuItem MENU_SAP = new UpMenu("menu.change_sap", PreferenceNames.SAP_LANGUAGE_AUDIO,
        new MenuItem[] {
            MENU_SAP_FRENCH,
            MENU_SAP_ENGLISH,
            MENU_SAP_SPANISH,
        }
    );

    public static final MenuItem MENU_AUDIO_DESC_ON = new UpMenuItem("menu.audio_desc.on", PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_ON);
    public static final MenuItem MENU_AUDIO_DESC_OFF = new UpMenuItem("menu.audio_desc.off", PreferenceNames.DESCRIBED_AUDIO_DISPLAY, Definitions.OPTION_VALUE_OFF);
    public static final MenuItem MENU_AUDIO_DESC = new UpMenu("menu.audio_desc", PreferenceNames.DESCRIBED_AUDIO_DISPLAY,
        new MenuItem[] {
            MENU_AUDIO_DESC_ON,
            MENU_AUDIO_DESC_OFF,
        }
    );

    public static MenuItem MENU_ORDER = new MenuItem("ppv.order");
    public static MenuItem MENU_UNBLOCK = new MenuItem("ppv.unblock_content");
    public static MenuItem MENU_OTHER_SHOWTIME = new MenuItem("ppv.see_other_showtimes");
    public static MenuItem MENU_CANCEL_ORDER = new MenuItem("ppv.cancel_order");
    public static MenuItem MENU_HOW_TO_ORDER = new MenuItem("ppv.how_to_order");
    public static MenuItem MENU_HOW_TO_ORDER_OPTION = new MenuItem("ppv.how_to_order_option");

    public static MenuItem MENU_CART = new MenuItem("ppv.cart_option");
    public static MenuItem MENU_TUNE_TO_TRAILER = new MenuItem("ppv.tune_trailer");
    public static MenuItem MENU_NEW_THIS_MONTH = new MenuItem("ppv.new_this_month");

    public static MenuItem PPV_OPTIONS = new MenuItem("PPV_OPTIONS",
        new MenuItem[] { MENU_CART, MENU_HOW_TO_ORDER_OPTION, MENU_TUNE_TO_TRAILER, MENU_NEW_THIS_MONTH }
    );

    public static MenuItem MENU_CHANGE_PACKAGE = new MenuItem("epg.change_tv_package");
    public static MenuItem MENU_SUBSCRIBE_CHANNEL = new SubscribeMenuItem("epg.subscribe_channel");

    //->Kenneth[2015.4.15] : R5 : Channel On demand 관련 두개 메뉴 추가 (normal, adult)
    public static MenuItem MENU_COD = new MenuItem("menu.channel_on_demand");
    public static MenuItem MENU_ADULT_COD = new MenuItem("menu.adult_channel_on_demand");

    public static final MenuItem getRecordMenu(Program p) {
        if (p == null || p instanceof NullProgram || p instanceof FakeProgram) {
            return null;
        }
        if (p.isPast()) {
            // TODO - TSB에 있는것 녹화할 수 있지 않나?
            return null;
        }
        if (!EpgCore.getInstance().supportDvr()) {
            return null;
        }
        Channel ch = p.getEpgChannel();
        if (ch == null || !ch.isRecordable()) {
            return null;
        }
        if (!RecordingStatus.getInstance().contains(p)) {
            if (p instanceof PpvProgram) {
                if (!PpvController.getInstance().isPurchasedOrReserved((PpvProgram) p)) {
                    // 구매 안한 PPV
                    return null;
                }
            } else {
                if (App.BLOCK_TO_RECORD_UNSUB_CHANNEL && !p.isAuthorized()) {
                    return null;
                }
            }
            return Rs.MENU_RECORD;
        } else if (p.isFuture()) {
            return Rs.MENU_CANCEL_RECORDING;
        } else {
            return Rs.MENU_STOP_RECORDING;
        }
    }

    public static final MenuItem getReminderMenu(Program p) {
        if (p == null || p instanceof NullProgram || p instanceof FakeProgram) {
            return null;
        }
        if (!p.isFuture()) {
            return null;
        }
        if (ReminderManager.getInstance().contains(p)) {
            return Rs.MENU_REMOVE_REMINDER;
        } else {
            if (p instanceof PpvProgram || p.isAuthorized()) {
                return Rs.MENU_SET_REMINDER;
            } else {
                return null;
            }
        }
    }

    public static final MenuItem MENU_PC_ACTIVATE = new MenuItem("menu.pc_activate");
    public static final MenuItem MENU_PC_SUSPEND  = new MenuItem("menu.pc_suspend");
    public static final MenuItem MENU_PC_ENABLE   = new MenuItem("menu.pc_enable");

    public static final MenuItem getParentalControlMenu() {
        DataCenter dc = DataCenter.getInstance();
        Object pcValue = DataCenter.getInstance().get(RightFilter.PARENTAL_CONTROL);
        if (Definitions.OPTION_VALUE_ON.equals(pcValue)) {
            return MENU_PC_SUSPEND;     // ON
        } else if (Definitions.OPTION_VALUE_OFF.equals(pcValue)) {
            return MENU_PC_ACTIVATE;    // OFF
        } else {
            return MENU_PC_ENABLE;      // Suspended
        }
    }

    public static final MenuItem getMoreOptionsMenu(Channel ch) {
        MenuItem more = new MenuItem("menu.more_options");
        more.add(Rs.MENU_CHANGE_FILTER);
        more.add(Rs.MENU_INVOKE_CAPTION);
        more.add(Rs.MENU_SAP);
        more.add(Rs.MENU_AUDIO_DESC);
//        more.add(Rs.MENU_SET_SLEEP_TIMER);
        return more;
    }


}

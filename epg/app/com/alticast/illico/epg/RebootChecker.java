package com.alticast.illico.epg;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.framework.DataCenter;
import java.awt.event.KeyEvent;
import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;

/**
  * Kenneth : This class handles reboot fuction of R4.1
  */
public class RebootChecker {
    private static RebootChecker instance;
    private int longPressDuration;
    private long timePowerPressed;

    /**
      * Returns instance of this class.
      */
    public static RebootChecker getInstance() {
        if (instance == null) {
            instance = new RebootChecker();
        }
        return instance;
    }

    private RebootChecker() {
        try {
            longPressDuration = DataCenter.getInstance().getInt("POWER_LONG_PRESS_DURATION");
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"POWER_LONG_PRESS_DURATION = "+longPressDuration);
        } catch (Exception e ) {
            e.printStackTrace();
        }
        if (longPressDuration == 0) {
            longPressDuration = 5000;
        }
    }

    // 파워키 길게 누르는 것을 체크하는 코드
    public void checkKeyEvent(UserEvent e) {
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"[RebootChecker] : ckeckKeyEvent("+e+")");
        int type = e.getType();
        int code = e.getCode();
        if (code != HRcEvent.VK_POWER) {
            timePowerPressed = -1L;
            return;
        }
        if (type == KeyEvent.KEY_PRESSED) {
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"[RebootChecker] : POWER PRESSED");
            timePowerPressed = System.currentTimeMillis();
        } else if (type == KeyEvent.KEY_RELEASED) {
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"[RebootChecker] : POWER RELEASED");
            if (timePowerPressed == -1L) return;
            long timeCurrent = System.currentTimeMillis();
            int gap = (int)(timeCurrent - timePowerPressed);
            if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"[RebootChecker] : gap = "+gap);
            if (gap >= longPressDuration) {
                // Reboot
                Log.printDebug(App.LOG_HEADER+"[RebootChecker] : Bingo!. Reboot.");
                /*
                int curMode = Host.getInstance().getPowerMode();
                if (curMode == Host.FULL_POWER) {
                    Log.printDebug("Kenneth [RebootChecker] : State before Reboot is FULL_POWER(Running).");
                } else if (curMode == Host.LOW_POWER) {
                    Log.printDebug("Kenneth [RebootChecker] : State before Reboot is LOW_POWER(Standby).");
                }
                */
                // 2014.10.6 : 마케팅 요청으로 지움
                //Host.getInstance().setPowerMode(Host.FULL_POWER);
                Host.getInstance().reboot();
            } else {
                timePowerPressed = -1L;
            }
        } 
    }
}


    /* 아래의 소스코드는 12345 같은 키코드 조합을 가지고 reboot 시키는 코드이다.
       파워버튼은 길게 누르는 것으로 요구사항이 바뀌었으므로 코드를 막기는 하나 보관할 가치가 있다고 판단해서 남겨둠
    // constructor
    private RebootChecker() {
        try {
            String[] keys = TextUtil.tokenize(DataCenter.getInstance().getString("REBOOT_KEY_CODES"), ',');
            int[] keysInt = new int[keys.length];
            for (int i = 0; i < keys.length; i++) {
                keysInt[i] = Integer.parseInt(keys[i]);
            }
            rebootCodes = keysInt;
            cachedCodes = new int[rebootCodes.length];
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void checkKeyCode(int keyCode) {
        if (rebootCodes == null || rebootCodes.length == 0) return;
        appendToCache(keyCode);
        if (hasSameValues(rebootCodes, cachedCodes)) {
            LogB.println("Bingo! : Call Host.reboot()");
            Host.getInstance().reboot();
        }
    }

    // Append a keycode in to the int[] cache.
    private void appendToCache(int keyCode) {
        if (cachedCodes == null || cachedCodes.length == 0) return;
        try {
            for (int i = 0 ; i < cachedCodes.length ; i++) {
                if (i == cachedCodes.length-1) {
                    cachedCodes[i] = keyCode;
                } else {
                    cachedCodes[i] = cachedCodes[i+1];
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    // Returns whether both int[] has same values.
    private boolean hasSameValues(int[] arr1, int[] arr2) {
        printIntArray(arr1);
        printIntArray(arr2);
        if (arr1 == null || arr2 == null || arr1.length != arr2.length) return false;
        boolean result = true;
        try {
            for (int i = 0 ; i < arr1.length ; i++) {
                if (arr1[i] != arr2[i]) {
                    result = false;
                    break;
                }
            }
        } catch (Exception e) {
            Log.print(e);
            result = false;
        }
        return result;
    }

    // Just print the int[].
    private void printIntArray(int[] arr) {
        if (arr == null || arr.length == 0) return;
        StringBuffer buf = new StringBuffer("{");
        try {
            for (int i = 0 ; i < arr.length ; i++) {
                buf.append(String.valueOf(arr[i]));
                if (i == (arr.length-1)) {
                    buf.append('}');
                } else {
                    buf.append(", ");
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        LogB.println("int[] : "+buf.toString());
    }
    */

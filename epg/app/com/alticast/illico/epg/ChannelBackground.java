package com.alticast.illico.epg;

import com.alticast.ui.*;
import com.alticast.illico.epg.navigator.*;
import com.alticast.illico.epg.ui.*;
import com.alticast.illico.epg.gui.*;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.navigator.EpgChannelEventListener;
import com.alticast.illico.epg.EpgCore;
import com.alticast.illico.epg.navigator.ChannelController;
import com.alticast.illico.epg.radio.RadioChannelPage;
import com.alticast.illico.epg.virtualchannel.VirtualChannelPage;
import com.alticast.illico.epg.ppv.PpvController;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.awt.event.KeyEvent;
import org.dvb.event.*;
import javax.tv.service.selection.*;

/**
 * Main A/V 대신 graphics를 덮기 위한 VideoImage의 sub class.
 * ChannelBackgroundPanel 이라는 UI를 layered UI 에 add 한다.
 */
public class ChannelBackground extends VideoImage implements LayeredKeyHandler {

    private static ChannelBackground instance = new ChannelBackground();

    public static ChannelBackground getInstance() {
        return instance;
    }

    LayeredWindow window;
    LayeredUI ui;

    ChannelBackgroundPanel current;
    // full screen app 상태나 resize될 경우는 pause 상태로 만들어 layered ui를 deactivate 시킨다.
    boolean paused = false;

    private ChannelBackground() {
        window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.setVisible(true);

        ui = WindowProperty.EPG_BACKGROUND.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
    }

    protected synchronized void setState(int newState) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState("+newState+")");
        if (state != PIP_UNSUB || newState != PIP_UNSUB) {
            // 깜빡임 방지
            if (state == PIP_NOT_SUPPORT_UHD && newState == PIP_NOT_SUPPORT_UHD) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Do not call stopPanel() : Same PIP_NOT_SUPPORT_UHD state!");
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call stopPanel()");
                stopPanel();
            }
        }
        super.setState(newState);
        Channel ch = channel;
        ChannelBackgroundPanel comp;
        switch (newState) {
            case PIP_CLEAR:
                return;
            case PIP_SDV:
            case PIP_SDV_KEEP_ALIVE:
            case PIP_GALAXIE:
                return;
            case PIP_PPV_IND:
            case PIP_PPV_VCC:
                comp = PpvController.getInstance();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call PpvController.start()");
                PpvController.getInstance().start(ch);
                break;
            case PIP_RADIO:
                comp = RadioChannelPage.getInstance();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call RadioChannelPage.start()");
                RadioChannelPage.getInstance().start(ch);
                break;
            case PIP_UNSUB:
            case PIP_UNSUB_ISA:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call startUnsubscribedPanel()");
                startUnsubscribedPanel(ch);
                return;
            //->Kenneth[2015.1.30] 4K
            case PIP_NOT_SUPPORT_UHD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call startIncompatibilityPanel()");
                startIncompatibilityPanel(ch);
                return;
            //->Kenneth[2015.2.28] 4K
            case PIP_DUAL_UHD_NOT_SUPPORTED:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call startDualUhdPipLimitPanel()");
                startDualUhdPipLimitPanel(ch);
                return;
            case PIP_VIRTUAL:
                VirtualChannelPage vcp = new VirtualChannelPage();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call VirtualChannelPage.start()");
                vcp.start(ch);
                comp = vcp;
                break;
            case PIP_BLOCK_PROGRAM:
                if (ch != null && ch.getType() == Channel.TYPE_PPV) {
                    // show ppv blocked page
                    PpvController ppv = PpvController.getInstance();
                    boolean started = ppv.start(ch);
                    if (started) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call showPanel("+ppv+")");
                        showPanel(ppv);
                        return;
                    }
                }
                comp = new ChannelBlockedPanel(channel, ChannelController.BLOCK_BY_RATING);
                break;
            case PIP_BLOCK_CHANNEL:
                comp = new ChannelBlockedPanel(channel, ChannelController.BLOCK_BY_CHANNEL);
                break;
            default:
                return;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.setState() : Call showPanel("+comp+")");
        showPanel(comp);
    }

    public synchronized void showBlockedPanel(Channel ch, short blockType) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.showBlockedPanel("+ch+")");
        ChannelBlockedPanel panel = new ChannelBlockedPanel(ch, blockType);
        panel.start();
        showPanel(panel);
    }

    /** from ServiceContextEvent of ChannelController.
    public synchronized void _startUnsubscribedPanel(int sid) {
        Channel ch = ChannelController.get(0).getCurrent();
        if (ch != null && ch.getSourceId() == sid) {
            startUnsubscribedPanel(ch);
        }
    }
     */

    private synchronized void startUnsubscribedPanel(Channel ch) {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.startUnsubscribedPanel: " + ch);
        if (current != null && (current instanceof UnsubscribedChannelPanel)
                && ((UnsubscribedChannelPanel) current).channel.equals(ch)) {
            // 깜빡임 방지
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startUnsubscribedPanel: check 1");
            ((UnsubscribedChannelPanel) current).setChannel(ch);
            //->Kenneth[2017.3.16] R7.3 : VBM 로그 
            Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
            EpgVbmController.getInstance().writeUnsubscriptionScreen(ch, p);
            //<-
            return;
        }
        if (ch.equals(channel) && current != null && current instanceof ChannelBlockedPanel) {
            // 이미 블럭 상태
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startUnsubscribedPanel: check 2");
            return;
        }
        stopPanel();
        this.channel = ch;
        UnsubscribedChannelPanel panel = new UnsubscribedChannelPanel();
        panel.start(ch);
        showPanel(panel);
        //->Kenneth[2017.3.16] R7.3 : VBM 로그 
        Program p = EpgDataManager.getInstance().getCurrentProgram(ch);
        EpgVbmController.getInstance().writeUnsubscriptionScreen(ch, p);
        //<-
    }

    //->Kenneth[2015.1.30] 4K
    private synchronized void startIncompatibilityPanel(Channel ch) {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.startIncompatibilityPanel: " + ch);
        if (current != null && (current instanceof IncompatibilityChannelPanel)
                && ((IncompatibilityChannelPanel) current).channel.equals(ch)) {
            // 깜빡임 방지
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startIncompatibilityPanel: check 1 : Just set channel and do nothing");
            ((IncompatibilityChannelPanel) current).setChannel(ch);
            return;
        }
        if (ch.equals(channel) && current != null && current instanceof ChannelBlockedPanel) {
            // 이미 블럭 상태
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startIncompatibilityPanel: check 2 : Do nothing");
            return;
        }
        stopPanel();
        this.channel = ch;
        IncompatibilityChannelPanel panel = new IncompatibilityChannelPanel();
        panel.start(ch);
        showPanel(panel);
    }

    //->Kenneth[2015.2.28] 4K
    private synchronized void startDualUhdPipLimitPanel(Channel ch) {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.startDualUhdPipLimitPanel: " + ch);
        if (current != null && (current instanceof DualUhdPipLimitPanel)
                && ((DualUhdPipLimitPanel) current).channel.equals(ch)) {
            // 깜빡임 방지
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startDualUhdPipLimitPanel: check 1");
            ((DualUhdPipLimitPanel) current).setChannel(ch);
            return;
        }
        if (ch.equals(channel) && current != null && current instanceof ChannelBlockedPanel) {
            // 이미 블럭 상태
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.startDualUhdPipLimitPanel: check 2");
            return;
        }
        stopPanel();
        this.channel = ch;
        DualUhdPipLimitPanel panel = new DualUhdPipLimitPanel();
        panel.start(ch);
        showPanel(panel);
    }

    public synchronized boolean startSdvError(short messageId, short responseId, int sourceId, boolean byForce) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.startSdvError()");
        boolean ret = super.startSdvError(messageId, responseId, sourceId, byForce);
        if (ret) {
            stopPanel();
            SdvBarkerPanel panel = new SdvBarkerPanel(messageId, responseId, SdvBarkerPanel.TYPE_ERROR);
            panel.start();
            showPanel(panel);
        }
        return ret;
    }

    public synchronized void startKeepAlive() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.startKeepAlive()");
        stopPanel();
        super.startKeepAlive();
        SdvBarkerPanel panel = new SdvBarkerPanel((short) 0x8005, (short) 0, SdvBarkerPanel.TYPE_KEEP_ALIVE);
        panel.start();
        showPanel(panel);
    }

    private synchronized void showPanel(ChannelBackgroundPanel panel) {
        if (Log.INFO_ON) {
            Log.printDebug(App.LOG_HEADER+"ChannelBackground.showPanel: " + panel + ", " + channel + ", paused=" + paused);
        }
        window.add(panel);
        this.current = panel;
        if (!paused) {
            ui.activate();
        }
    }

    public synchronized void stopPanel() {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.stopPanel");
        ui.deactivate();
        if (current != null) {
            current.stop();
        }
        window.removeAll();
        this.current = null;
    }

    public synchronized void pause() {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.pause");
        paused = true;
        // GUIDE 닫으면 화면만 사라지도록 한다.
        ui.deactivate();
        if (current != null) {
            current.pause();
        }
    }

    public synchronized void resume() {
        Log.printDebug(App.LOG_HEADER+"ChannelBackground.resume");
        paused = false;
        // pause되고 stop 되었으면 child가 없고, pause만 되었으면 있다.
        if (window.getComponentCount() > 0) {
            ui.activate();
        }
    }

    public boolean isRunning() {
        ChannelBackgroundPanel p = current;
        return p != null && p.isRunning();
    }

    public ChannelBackgroundPanel getCurrentPanel() {
        return current;
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.handleKeyEvent("+e+")");
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        if (current != null) {
            return current.handleKey(code);
        }
        return false;
    }

    //->Kenneth[2017.9.27] : EpgServiceImpl 로부터 호출
    public void sdvErrorPopupClosed(String errorCode) {
        if (Log.ALL_ON) Log.printDebug(App.LOG_HEADER+"ChannelBackground.sdvErrorPopupClosed() : current = "+current);
        if (current == null) return;
        if (current instanceof SdvBarkerPanel) {
            ((SdvBarkerPanel)current).sdvErrorPopupClosed(errorCode);
        }
    }
    //<-
}

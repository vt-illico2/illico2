package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.TextReader;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class SdvSam {

    // channels
    public SdvChannelInfo[] channels;

    public static SdvSam create(File file) {
        return create(TextReader.read(file));
    }

    public static SdvSam create(String[] data) {
        try {
            return new SdvSam(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private SdvSam(String[] data) {
        ArrayList list = new ArrayList(data.length);
        for (int i = 0; i < data.length; i++) {
            try {
                SdvChannelInfo s = new SdvChannelInfo(data[i]);
                list.add(s);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        int size = list.size();
        SdvChannelInfo[] array = new SdvChannelInfo[size];
        array = (SdvChannelInfo[]) list.toArray(array);
        Arrays.sort(array);
        channels = array;
    }

    public void dump() {
        Log.printDebug("SdvSam.dump==========");
        for (int i = 0; i < channels.length; i++) {
            Log.printDebug("SdvSam.channel[" + i + "] = " + channels[i]);
        }
    }

    /** check channels. */
    public boolean equals(Object o) {
        if (o != null && o instanceof SdvSam) {
            SdvSam s = (SdvSam) o;
            if (s.channels == null || channels == null) {
                return s.channels == channels;
            }
            if (s.channels.length != channels.length) {
                return false;
            }
            for (int i = 0; i < channels.length; i++) {
                if (!channels.equals(s.channels[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

}

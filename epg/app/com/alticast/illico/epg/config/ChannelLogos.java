package com.alticast.illico.epg.config;

import java.awt.*;
import java.util.*;
import java.io.*;
import org.dvb.ui.*;
import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

public class ChannelLogos implements Runnable {

    public static final String SHARED_MEMORY_KEY = "ChannelLogoTable";
    public static final String DEFAULT_IMAGE_NAME = "default";
    public static final String SCALED_PREFIX = "scaled_";
    public static final String LOGO_PREFIX = "logo.";

    private static final boolean USE_BUFFER = true;

    static ImagePool imagePool = FrameworkMain.getInstance().getImagePool();

    static {
        // default logo
        if (!imagePool.containsKey(DEFAULT_IMAGE_NAME)) {
            Image logo = imagePool.getImage("default_channel_logo.png");
            if (logo != null) {
                imagePool.put(DEFAULT_IMAGE_NAME, logo);
                SharedMemory.getInstance().put(LOGO_PREFIX + DEFAULT_IMAGE_NAME, logo);
                Image scaled = logo.getScaledInstance(80, 24, Image.SCALE_FAST);
                imagePool.put(SCALED_PREFIX + DEFAULT_IMAGE_NAME, scaled);
            }
        }
    }

    String string;

    Hashtable table;

    public static ChannelLogos create(File file) {
        try {
            return new ChannelLogos(ZipFileReader.read(file));
        } catch (Exception ex) {
            Log.print(ex);
            return new ChannelLogos(ex.toString());
        }
//        return null;
    }

    public static Image getLogo(String name) {
        if (imagePool.containsKey(name)) {
            return imagePool.getImage(name);
        } else {
            return imagePool.getImage(DEFAULT_IMAGE_NAME);
        }
    }

    public static Image getScaledLogo(String name) {
        if (imagePool.containsKey(SCALED_PREFIX + name)) {
            return imagePool.getImage(SCALED_PREFIX + name);
        } else {
            return imagePool.getImage(SCALED_PREFIX + DEFAULT_IMAGE_NAME);
        }
    }

    public ChannelLogos(String s) {
        string = s;
    }

    private ChannelLogos(Hashtable table) {
        this.table = table;
        long totalSize = 0;

        synchronized (DEFAULT_IMAGE_NAME) {
            long time1 = System.currentTimeMillis();
            Iterator it = table.entrySet().iterator();
            Map.Entry entry;
            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                String name = getEntryName((String) entry.getKey());
                byte[] imageByte = (byte[]) entry.getValue();
                totalSize += imageByte.length;
                imagePool.remove(name);
                Image im = imagePool.createImage(imageByte, name);
                SharedMemory.getInstance().put("logo." + name, im);
            }
            imagePool.waitForAll();
            long time2 = System.currentTimeMillis();
            Log.printInfo("ChannelLogos: update time for default size = " + (time2 - time1));

            Enumeration en = table.keys();
            while (en.hasMoreElements()) {
                String name = getEntryName((String) en.nextElement());
                Image im = imagePool.getImage(name);
                if (im != null) {
                    Image scaled = im.getScaledInstance(im.getWidth(null) * 80 / 100,
                                                        im.getHeight(null) * 80 / 100, Image.SCALE_FAST);
                    imagePool.remove(SCALED_PREFIX + name);
                    imagePool.put(SCALED_PREFIX + name, scaled);
                }
            }
            long time3 = System.currentTimeMillis();
            Log.printInfo("ChannelLogos: update time for fast scaled size = " + (time3 - time2));
        }
        string = "ChannelLogos: " + table.size() + " : " + totalSize;
        Log.printInfo(string);
    }

    private String getEntryName(String name) {
        if (name.endsWith("_logo")) {
            return TextUtil.urlDecode(name.substring(0, name.length() - 5));
        } else {
            return name;
        }
    }

    public void run() {
        if (table == null) {
            return;
        }
        MediaTracker mediaTracker = new MediaTracker(new Container());
        synchronized (DEFAULT_IMAGE_NAME) {
            long time2 = System.currentTimeMillis();
            Enumeration en = table.keys();

            Hashtable imageTable = new Hashtable();
            while (en.hasMoreElements()) {
                String name = getEntryName((String) en.nextElement());
                Image im = imagePool.getImage(name);
                if (im != null) {
                    Image scaled;
                    int w = im.getWidth(null) * 80 / 100;
                    int h = im.getHeight(null) * 80 / 100;
                    if (USE_BUFFER) {
                        DVBBufferedImage dm = new DVBBufferedImage(w, h);
                        Graphics g = dm.getGraphics();
                        g.drawImage(im, 0, 0, w, h, null);
                        g.dispose();
                        scaled = dm;
                    } else {
                        scaled = im.getScaledInstance(w, h, Image.SCALE_SMOOTH);
                    }
                    mediaTracker.addImage(scaled, 0);
                    imageTable.put(name, scaled);
                }
                try {
                    mediaTracker.waitForAll();
                } catch (Exception ex) {
                }
            }

            long time3 = System.currentTimeMillis();
            Log.printInfo("ChannelLogos: update time for scaled size = " + (time3 - time2));

            if (ConfigManager.getInstance().channelLogos != this) {
                Log.printInfo("ChannelLogos: changed. flush all images.");
                en = imageTable.elements();
                while (en.hasMoreElements()) {
                    Image im = (Image) en.nextElement();
                    im.flush();
                }
            } else {
                en = imageTable.keys();
                while (en.hasMoreElements()) {
                    String name = (String) en.nextElement();
                    imagePool.remove(SCALED_PREFIX + name);
                    imagePool.put(SCALED_PREFIX + name, (Image) imageTable.get(name));
                }
            }
        }
        FrameworkMain.getInstance().printSimpleLog(string);
    }

    public String toString() {
        return string;
    }


}

package com.alticast.illico.epg.config;

import com.videotron.tvi.illico.util.Constants;
import java.util.*;

public class ChannelInfo {

    public String callLetter;
    public String[] descriptions = new String[Constants.LANGUAGES.length];
    public int rating;
    public String language;
	//->Kenneth : 4K by June
    public int definition;  // fixed for 4K
    public int type;
    public int genreId;

    public String friendlyName;
    public String fullName;

    // radio 채널 전용
    public String radioFrequency;

    // virtual channel 용
    public String appName;
    public String param;

    public int screenData = 0;

    //->Kenneth[2017.3.23] R7.3 APD
    public boolean apdDisabled;
    //<-

    public String toString() {
        return "ChannelInfo: " + callLetter + ", " + rating
            //->Kenneth : 4K by June
            + ", " + language + ", " + definition + ", " + type
            + ", " + genreId + ", " + friendlyName + ", " + fullName;
    }

}


package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.util.Constants;
import java.io.File;

/** 실제로 사용하지는 않는 듯. */
public class ChannelType {
    public int version;
    public int[] code;
    public String[] name;

    public static ChannelType create(File file) {
        return create(BinaryReader.read(file));
    }

    public static ChannelType create(byte[] data) {
        try {
            return new ChannelType(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }


    private ChannelType(byte[] data) {
        int index = -1;
        this.version = ConfigManager.oneByteToInt(data, ++index);

        int channelSize = ConfigManager.oneByteToInt(data, ++index);

        code = new int[channelSize];
        name = new String[channelSize];
        for (int a = 0; a < channelSize; a++) {
            code[a] = ConfigManager.oneByteToInt(data, ++index);

            int len = ConfigManager.oneByteToInt(data, ++index);
            name[a] = ConfigManager.byteArrayToString(data, ++index, len);
            index += len - 1;
        }
    }

    public int getVersion() {
        return version;
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof ChannelType) {
            return ((ChannelType)o).getVersion() == version;
        }
        return false;
    }

    public void dump() {
        for (int i = 0; i < code.length; i++) {
            Log.printDebug("ChannelType[" + i + "] = " + code[i] + ", " + name[i]);
        }
    }

    public String toString() {
        return "ChannelType[size=" + code.length + ", ver=" + version + "]";
    }

}

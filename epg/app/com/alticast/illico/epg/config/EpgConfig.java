package com.alticast.illico.epg.config;

import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.framework.*;
import java.io.File;
import java.util.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.epg.sdv.mc.TuningInfo;

/**
 * MS 에서 보내주는 config.
 */
public class EpgConfig {

    public int version;
    public int daysBootup;  // inband adapter, 부팅할 때 load할 days
    public int daysCannotRemove;    // epg data manager
    public int purgingMethodeGrid;
    public int purgingMethodDescriptions;
    public int daysNavigation;      // epg data manager, 최대 이동가능 날짜
    public float miniEpgDisplayTime;  // mini epg
    public float miniEpgInfoDisplayTime;  // mini epg
    public String ipAddr;           // epg data manager
    public String dnsName;          // epg data manager
    public int port;                // epg data manager
    public String contextRoot;      // epg data manager
    public String[] proxyHosts;     // epg data manager
    public int[] proxyPorts;        // epg data manager
    //->Kenneth[2015.1.31] 4K 
    private String enUhdFuncText;
    private String frUhdFuncText;
    private String enUhdShortFuncText;
    private String frUhdShortFuncText;
    private String enUhdLimitText;
    private String frUhdLimitText;
    //->Kenneth[2015.4.9] R5 : Tutorial 
    private String enTutorialTitle;
    private String frTutorialTitle;
    private String enTutorialImageName;
    private String frTutorialImageName;
    private String enTutorialFirstButtonName;
    private String frTutorialFirstButtonName;
    private String enTutorialSecondButtonName;
    private String frTutorialSecondButtonName;
    private String enTutorialThirdButtonName;
    private String frTutorialThirdButtonName;
    public long tutorialStartDate;
    public long tutorialEndDate;
    public int tutorialMaxDisplay;
    //->Kenneth[2016.7.28] R7.2 : Tutorial versioning
    public String tutorialVersion;
    //<-

    //->Kenneth[2017.9.26] R7.4 : MC tuning info
    public Vector mcTuningInfos;
    //<-

    public String getTutorialTitle() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enTutorialTitle;
        }
        return frTutorialTitle;
    }

    public String getTutorialImageName() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enTutorialImageName;
        }
        return frTutorialImageName;
    }

    public String getTutorialFirstButtonName() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enTutorialFirstButtonName;
        }
        return frTutorialFirstButtonName;
    }

    public String getTutorialSecondButtonName() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enTutorialSecondButtonName;
        }
        return frTutorialSecondButtonName;
    }

    public String getTutorialThirdButtonName() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enTutorialThirdButtonName;
        }
        return frTutorialThirdButtonName;
    }

    public String getUhdFuncText() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enUhdFuncText;
        }
        return frUhdFuncText;
    }

    public String getUhdShortFuncText() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enUhdShortFuncText;
        }
        return frUhdShortFuncText;
    }

    public String getUhdLimitText() {
        String lang = FrameworkMain.getInstance().getCurrentLanguage();
        if ("en".equalsIgnoreCase(lang)) {
            return enUhdLimitText;
        }
        return frUhdLimitText;
    }

    public static EpgConfig create(File file) {
        return create(BinaryReader.read(file));
    }

    public static EpgConfig create(byte[] data) {
        try {
            return new EpgConfig(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private EpgConfig(byte[] data) {
        int index = 0;
        this.version = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  version = " + version);

        this.daysBootup = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  daysBootup = " + daysBootup);

        this.daysCannotRemove = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  daysCannotRemove = " + daysCannotRemove);

        this.purgingMethodeGrid = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  purgingMethodeGrid = " + purgingMethodeGrid);

        this.purgingMethodDescriptions = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  purgingMethodDescriptions = " + purgingMethodDescriptions);

        this.daysNavigation = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  daysNavigation = " + daysNavigation);

        this.miniEpgDisplayTime = Float.intBitsToFloat(ConfigManager.fourBytesToInt(data, index));
        Log.printDebug(App.LOG_HEADER+"EpgConfig  miniEpgDisplayTime = " + miniEpgDisplayTime);
        index += 4;

        this.miniEpgInfoDisplayTime = Float.intBitsToFloat(ConfigManager.fourBytesToInt(data, index));
        Log.printDebug(App.LOG_HEADER+"EpgConfig  miniEpgInfoDisplayTime = " + miniEpgInfoDisplayTime);
        index += 4;

        int dnsNameLength = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  dnsNameLength = " + dnsNameLength);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  data.length = " + data.length + ", index = " + index);

        String dnsName = new String(data, index, dnsNameLength);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  dnsName = " + dnsName);
        index += dnsNameLength;
        this.dnsName = dnsName;

        this.port = ConfigManager.twoBytesToInt(data, index++, false);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  port = " + port);
        index++;

        int contextRootLength = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  contextRootLength = " + contextRootLength);
        String contextRoot = new String(data, index, contextRootLength);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  contextRoot = " + contextRoot);
        index += contextRootLength;
        this.contextRoot = contextRoot;

        int proxySize = ConfigManager.oneByteToInt(data, index++);
        Log.printDebug(App.LOG_HEADER+"EpgConfig  proxy size = " + proxySize);
        this.proxyHosts = new String[proxySize];
        this.proxyPorts = new int[proxySize];
        for (int i = 0; i < proxySize; i++) {
            int ipLen = ConfigManager.oneByteToInt(data, index++);
            proxyHosts[i] = new String(data, index, ipLen);
            Log.printDebug(App.LOG_HEADER+"EpgConfig  proxyHosts[" + i + "] = " + proxyHosts[i]);
            index += ipLen;
            proxyPorts[i] = ConfigManager.twoBytesToInt(data, index++, false);
            Log.printDebug(App.LOG_HEADER+"EpgConfig  proxyPorts[" + i + "] = " + proxyPorts[i]);
            index++;
        }
        //->Kenneth[2015.1.31] 4K 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : data.length = "+data.length+", index = "+index);
        if (index >= data.length) return;
        int langSize = 0;
        try {
            langSize = ConfigManager.oneByteToInt(data, index++);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : langSize "+langSize);
            for (int i = 0 ; i < langSize ; i++) {
                String lang = new String(data, index, 2);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : lang "+lang);
                index += 2;
                if ("en".equalsIgnoreCase(lang)) {
                    //English
                    int funcTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    enUhdFuncText = new String(data, index, funcTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enUhdFuncText = "+enUhdFuncText);
                    index += funcTextSize;
                    int shortFuncTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    enUhdShortFuncText = new String(data, index, shortFuncTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enUhdShortFuncText = "+enUhdShortFuncText);
                    index += shortFuncTextSize; 
                } else {
                    // French
                    int funcTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    frUhdFuncText = new String(data, index, funcTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frUhdFuncText = "+frUhdFuncText);
                    index += funcTextSize;
                    int shortFuncTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    frUhdShortFuncText = new String(data, index, shortFuncTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frUhdShortFuncText = "+frUhdShortFuncText);
                    index += shortFuncTextSize; 
                }
            }
            for (int i = 0 ; i < langSize ; i++) {
                String lang = new String(data, index, 2);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : lang "+lang);
                index += 2;
                if ("en".equalsIgnoreCase(lang)) {
                    //English
                    int limitTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    enUhdLimitText = new String(data, index, limitTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enUhdLimitText = "+enUhdLimitText);
                    index += limitTextSize;
                } else {
                    // French
                    int limitTextSize = ConfigManager.twoBytesToInt(data, index++, false);
                    index ++;
                    frUhdLimitText = new String(data, index, limitTextSize);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frUhdLimitText = "+frUhdLimitText);
                    index += limitTextSize;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }

        //->Kenneth[2015.4.9] R5 : Tutorial 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig (Tutorial): data.length = "+data.length+", index = "+index);
        if (index >= data.length) return;
        try {
            for (int i = 0 ; i < langSize ; i++) {
                String lang = new String(data, index, 2);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : lang "+lang);
                index += 2;
                if ("en".equalsIgnoreCase(lang)) {
                    //English
                    int tutorialTitleLength = ConfigManager.oneByteToInt(data, index++);
                    enTutorialTitle = new String(data, index, tutorialTitleLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enTutorialTitle = "+enTutorialTitle);
                    index += tutorialTitleLength;

                    int tutorialImageNameLength = ConfigManager.oneByteToInt(data, index++);
                    enTutorialImageName = new String(data, index, tutorialImageNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enTutorialImageName = "+enTutorialImageName);
                    index += tutorialImageNameLength;

                    int tutorialFirstButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    enTutorialFirstButtonName = new String(data, index, tutorialFirstButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enTutorialFirstButtonName = "+enTutorialFirstButtonName);
                    index += tutorialFirstButtonNameLength;

                    int tutorialSecondButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    enTutorialSecondButtonName = new String(data, index, tutorialSecondButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enTutorialSecondButtonName = "+enTutorialSecondButtonName);
                    index += tutorialSecondButtonNameLength;

                    int tutorialThirdButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    enTutorialThirdButtonName = new String(data, index, tutorialThirdButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : enTutorialThirdButtonName = "+enTutorialThirdButtonName);
                    index += tutorialThirdButtonNameLength;
                } else {
                    // French
                    int tutorialTitleLength = ConfigManager.oneByteToInt(data, index++);
                    frTutorialTitle = new String(data, index, tutorialTitleLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frTutorialTitle = "+frTutorialTitle);
                    index += tutorialTitleLength;

                    int tutorialImageNameLength = ConfigManager.oneByteToInt(data, index++);
                    frTutorialImageName = new String(data, index, tutorialImageNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frTutorialImageName = "+frTutorialImageName);
                    index += tutorialImageNameLength;

                    int tutorialFirstButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    frTutorialFirstButtonName = new String(data, index, tutorialFirstButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frTutorialFirstButtonName = "+frTutorialFirstButtonName);
                    index += tutorialFirstButtonNameLength;

                    int tutorialSecondButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    frTutorialSecondButtonName = new String(data, index, tutorialSecondButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frTutorialSecondButtonName = "+frTutorialSecondButtonName);
                    index += tutorialSecondButtonNameLength;

                    int tutorialThirdButtonNameLength= ConfigManager.oneByteToInt(data, index++);
                    frTutorialThirdButtonName = new String(data, index, tutorialThirdButtonNameLength);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frTutorialThirdButtonName = "+frTutorialThirdButtonName);
                    index += tutorialThirdButtonNameLength;
                }
            }
            tutorialStartDate = ConfigManager.eightBytesToLong(data, index);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialStartDate = "+tutorialStartDate);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialStartDate = "+new Date(tutorialStartDate));
            index += 8;
            tutorialEndDate = ConfigManager.eightBytesToLong(data, index);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialEndDate = "+tutorialEndDate);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialEndDate = "+new Date(tutorialEndDate));
            index += 8;
            tutorialMaxDisplay = ConfigManager.fourBytesToInt(data, index);
            index += 4;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialMaxDisplay = "+tutorialMaxDisplay);
        } catch (Exception e) {
            Log.print(e);
        }

        //->Kenneth[2016.8.4] R7.2 : tutorial version
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig (Tutorial version): data.length = "+data.length+", index = "+index);
        if (index >= data.length) return;
        try {
            int tutorialVersionLength = ConfigManager.oneByteToInt(data, index++);
            tutorialVersion = new String(data, index, tutorialVersionLength);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tutorialVersion = "+tutorialVersion);
            index += tutorialVersionLength;
        } catch (Exception e) {
            Log.print(e);
        }
        //<-

        //->Kenneth[2017.9.26] R7.4 : SDV tuning info
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig (SDV tuning info): data.length = "+data.length+", index = "+index);
        if (index >= data.length) return;
        try {
            int tuningInfoLength = ConfigManager.oneByteToInt(data, index++);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : tuningInfoLength = "+tuningInfoLength);
            mcTuningInfos = new Vector();
            for (int i = 0 ; i < tuningInfoLength ; i++) {
                // DNCS
                int dncsLength = ConfigManager.oneByteToInt(data, index++);
                String dncs = new String(data, index, dncsLength);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : DNCS = "+dncs);
                index += dncsLength;
                // Frequency (: 를 구분자로 여러개 존재 가능)
                int freqLength = ConfigManager.oneByteToInt(data, index++);
                String frequencies = new String(data, index, freqLength);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : frequencies = "+frequencies);
                index += freqLength;
                // Modulation
                int modLength = ConfigManager.oneByteToInt(data, index++);
                String modulation = new String(data, index, modLength);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : modulation = "+modulation);
                index += modLength;
                // 가져온 정보를 가지고 TuningInfo 를 만들어서 vector 에 저장한다
                String[] freqArr = TextUtil.tokenize(frequencies, ":");
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : freqArr = "+freqArr);
                if (freqArr == null) continue;
                for (int j = 0 ; j < freqArr.length ; j++) {
                    //->Kenneth[2017.10.12] R7.4 : VDTRMASTER-6219 : freq 에 1000000 을 해야 함.
                    TuningInfo t = new TuningInfo(dncs, Integer.parseInt(freqArr[j])*1000000, getModulation(modulation));
                    //TuningInfo t = new TuningInfo(dncs, Integer.parseInt(freqArr[j]), getModulation(modulation));
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"EpgConfig : "+t.toString());
                    mcTuningInfos.addElement(t);
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-
    }

    //->Kenneth[2017.9.26] R7.4 : SdvBarker 클래스 는 더이상 사용 안함으로 인해 그 쪽 필요한 함수를 옮김
    private int getModulation(String mod) {
        int m = 0;
        try {
            if ("256".equals(mod)) {
                m = 0x10;
            } else if ("16".equals(mod)) {
                m = 0x06;
            } else if ("32".equals(mod)) {
                m = 0x07;
            } else if ("64".equals(mod)) {
                m = 0x08;
            } else if ("128".equals(mod)) {
                m = 0x0C;
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return m;
    }
    //<-

    public int getVersion() {
        return version;
    }

    public String toString() {
        return "EpgConfig [contextRoot=" + contextRoot + ", daysBootup=" + daysBootup
                + ", daysCannotRemove=" + daysCannotRemove + ", daysNavigation=" + daysNavigation
                + ", dnsName=" + dnsName + ", miniEpgDisplayTime=" + miniEpgDisplayTime + ", miniEpgInfoDisplayTime=" + miniEpgInfoDisplayTime
                + ", port=" + port + ", purgingMethodDescriptions=" + purgingMethodDescriptions
                + ", purgingMethodeGrid=" + purgingMethodeGrid
                + ", version=" + version + "]";
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof EpgConfig) {
            return ((EpgConfig)o).getVersion() == version;
        }
        return false;
    }

}

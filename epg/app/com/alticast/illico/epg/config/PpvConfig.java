package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import java.io.File;

/**
 * not using
 */
public class PpvConfig {

    public int version;
    public int daysIB;  // inband adapter
    public int daysOOB;    // epg data manager

    public String[] languageType;
    public String[] defaultText;

    public static PpvConfig create(File file) {
        return create(BinaryReader.read(file));
    }

    public static PpvConfig create(byte[] data) {
        try {
            return new PpvConfig(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private PpvConfig(byte[] data) {
        int index = -1;
        this.version = ConfigManager.oneByteToInt(data, ++index);
        Log.printDebug("ConfigManager  PpvConfig  version = " + version);

        this.daysIB = ConfigManager.oneByteToInt(data, ++index);
        Log.printDebug("ConfigManager  PpvConfig  daysIB = " + daysIB);

        this.daysOOB = ConfigManager.oneByteToInt(data, ++index);
        Log.printDebug("ConfigManager  PpvConfig  daysOOB = " + daysOOB);

        int languageTypeSize = ConfigManager.oneByteToInt(data, ++index);
        languageType = new String[languageTypeSize];
        defaultText = new String[languageTypeSize];

        for (int a = 0; a < languageTypeSize; a++) {
            languageType[a] = ConfigManager.byteArrayToString(data, ++index, 2);
            ++index;

            int defaultTextLength = ConfigManager.oneByteToInt(data, ++index);

            defaultText[a] = ConfigManager.byteArrayToString(data, ++index, defaultTextLength);
            index += defaultTextLength - 1;
        }
    }

    public int getVersion() {
        return version;
    }

    public String toString() {
        return "PpvConfig [daysIB=" + daysIB + ", daysOOB=" + daysOOB
                + ", version=" + version + "]";
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof PpvConfig) {
            return ((PpvConfig)o).getVersion() == version;
        }
        return false;
    }

}

package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.data.Channel;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.util.Constants;
import java.io.File;
import java.util.Hashtable;
import java.util.Vector;
import java.awt.Color;

public class VirtualChannelData {
    public int version;

    // language
    public String[] languageTypes;
    public Category[] categories;

    // call_letter, Category[]
    public Hashtable table;

    public static VirtualChannelData create(File file) {
        return create(BinaryReader.read(file));
    }

    public static VirtualChannelData create(byte[] data) {
        try {
            return new VirtualChannelData(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }


    private VirtualChannelData(byte[] data) {
        int index = 0;
        this.version = ConfigManager.oneByteToInt(data, index++);
        int languageSize = ConfigManager.oneByteToInt(data, index++);
        this.languageTypes = new String[languageSize];

        int categorySize = ConfigManager.oneByteToInt(data, index++);
        this.categories = new Category[categorySize];

        Hashtable categoryTable = new Hashtable();

        for (int i = 0; i < categorySize; i++) {
            Category c = new Category();
            c.id = ConfigManager.twoBytesToInt(data, index, false);
            index += 2;
            c.title = new String[languageSize];
            for (int j = 0; j < languageSize; j++) {
                if (i == 0) {
                    languageTypes[j] = ConfigManager.byteArrayToString(data, index, 2);
                }
                index += 2;
                int titleLen = ConfigManager.oneByteToInt(data, index++);
                c.title[j] = ConfigManager.byteArrayToString(data, index, titleLen);
                index += titleLen;
            }
            int color = ConfigManager.threeBytesToInt(data, index);
            index += 3;
            c.setColor(color);
            int pageSize = ConfigManager.oneByteToInt(data, index++);
            Page[] pages = new Page[pageSize];
            for (int j = 0; j < pageSize; j++) {
                Page p = new Page();
                p.name = new String[languageSize];
                p.imagePath = new String[languageSize];
                for (int k = 0; k < languageSize; k++) {
                    index += 2;
                    int nameLen = ConfigManager.oneByteToInt(data, index++);
                    p.name[k] = ConfigManager.byteArrayToString(data, index, nameLen);
                    index += nameLen;
                    int pathLen = ConfigManager.oneByteToInt(data, index++);
                    p.imagePath[k] = ConfigManager.byteArrayToString(data, index, pathLen);
                    index += pathLen;
                }
                pages[j] = p;
            }
            c.pages = pages;
            categories[i] = c;
            categoryTable.put(new Integer(c.id), c);
        }

        this.table = new Hashtable();
        int chLen = ConfigManager.oneByteToInt(data, index++);
        for (int i = 0; i < chLen; i++) {
            ChannelData cd = new ChannelData();

            int nameLen = ConfigManager.oneByteToInt(data, index++);
            cd.channelName = ConfigManager.byteArrayToString(data, index, nameLen);
            index += nameLen;

            int brandLen = ConfigManager.oneByteToInt(data, index++);
            cd.brand = ConfigManager.byteArrayToString(data, index, brandLen);
            index += brandLen;

            Vector list = new Vector();
            int listSize = ConfigManager.oneByteToInt(data, index++);
            for (int j = 0; j < listSize; j++) {
                int id = ConfigManager.twoBytesToInt(data, index++, false);
                index++;
                list.addElement(categoryTable.get(new Integer(id)));
            }
            if (list.size() > 0) {
                Category[] array = new Category[list.size()];
                list.copyInto(array);
                cd.categories = array;
                table.put(cd.channelName, cd);
            }
        }
    }

    public int getVersion() {
        return version;
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof VirtualChannelData) {
            return ((VirtualChannelData)o).getVersion() == version;
        }
        return false;
    }

    public String toString() {
        return "VirtualChannelData[size=" + categories.length + ", ver=" + version + "]";
    }

    public ChannelData getData(Channel ch) {
        return (ChannelData) table.get(ch.getName());
    }

    public class ChannelData {
        public Category[] categories;
        public String channelName;
        public String brand;
    }

    public class Category {
        public int id;
        public String[] title;
        public Color color = Color.white;
        public Page[] pages;

        public void setColor(int value) {
            color = new Color(value);
            if (Log.DEBUG_ON) {
                Log.printDebug("Category.setColor: " + value + ", " + color);
            }
        }
    }

    public class Page {
        public String[] name;
        public String[] imagePath;
    }

}

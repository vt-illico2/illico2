package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.util.Util;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.util.Constants;
import java.io.File;
import java.util.Hashtable;
import com.videotron.tvi.illico.ixc.epg.TvChannel;

public class ChannelInfoData {
    public int version;

//    public int daysIbOcMinimal;
//    public int daysIbOcFull;
    public int daysOobOc;

    // language
    public String[] languageType;
    public String[] defaultText;

    // channels
    // <key = call_letter, value = ChannelInfo >
    public Hashtable channelTable;

    // sister channel mapping
    public String[] hdName;
    public String[] sdName;

    public static ChannelInfoData create(File file) {
        return create(BinaryReader.read(file));
    }

    public static ChannelInfoData create(byte[] data) {
        try {
            return new ChannelInfoData(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }


    private ChannelInfoData(byte[] data) {
        int index = -1;
        this.version = ConfigManager.oneByteToInt(data, ++index);

        int languageTypeSize = ConfigManager.oneByteToInt(data, ++index);
        languageType = new String[languageTypeSize];
        defaultText = new String[languageTypeSize];

//        this.daysIbOcMinimal = ConfigManager.oneByteToInt(data, ++index);
//        this.daysIbOcFull = ConfigManager.oneByteToInt(data, ++index);

        this.daysOobOc = ConfigManager.oneByteToInt(data, ++index);

        for (int a = 0; a < languageTypeSize; a++) {
            languageType[a] = ConfigManager.byteArrayToString(data, ++index, 2);
            ++index;

            int defaultTextLength = ConfigManager.oneByteToInt(data, ++index);

            defaultText[a] = ConfigManager.byteArrayToString(data, ++index, defaultTextLength);
            index += defaultTextLength - 1;
        }

        int channelSize = ConfigManager.twoBytesToInt(data, ++index, false);
        ++index;

//        ChannelInfo[] channels = new ChannelInfo[channelSize];
        channelTable = new Hashtable();
        for (int a = 0; a < channelSize; a++) {
            ChannelInfo c = new ChannelInfo();

            int len = ConfigManager.oneByteToInt(data, ++index);
            c.callLetter = ConfigManager.byteArrayToString(data, ++index, len);
            index += len - 1;
//String str = c.callLetter;
//int screenData = 0;
//    if ("ILLIC".equals(str) || "C_VIE".equals(str) || "FAIRC".equals(str)) {
//        screenData = 7;
//    } else if ("CHOM".equals(str) || "ATN".equals(str)) {
//        screenData = 6;
//    } else if ("CBFT".equals(str) || "OTN1".equals(str)) {
//        screenData = 5;
//    } else if ("CIVM".equals(str) || "ARTA".equals(str)) {
//        screenData = 4;
//    } else if ("CFTM".equals(str) || "FPTV".equals(str)) {
//        screenData = 3;
//    } else if ("CFJP".equals(str) || "ERT".equals(str)) {
//        screenData = 2;
//    } else if ("CJOH".equals(str) || "DWTV".equals(str)) {
//        screenData = 1;
//    }
//c.screenData = screenData;

            for (int i = 0; i < languageTypeSize; i++) {
                String lang = ConfigManager.byteArrayToString(data, ++index, 2);
                ++index;
                len = ConfigManager.oneByteToInt(data, ++index);

                String desc = ConfigManager.byteArrayToString(data, ++index, len);
                index += len - 1;

                c.descriptions[Util.getLanguageIndex(lang)] = desc;
            }

            len = ConfigManager.oneByteToInt(data, ++index);
            c.friendlyName = ConfigManager.byteArrayToString(data, ++index, len);
            index += len - 1;

            c.type = ConfigManager.oneByteToInt(data, ++index);

            len = ConfigManager.oneByteToInt(data, ++index);
            c.fullName = ConfigManager.byteArrayToString(data, ++index, len);
            index += len - 1;

            len = ConfigManager.oneByteToInt(data, ++index);
            c.radioFrequency = ConfigManager.byteArrayToString(data, ++index, len);
            index += len - 1;

            c.rating = ConfigManager.oneByteToInt(data, ++index);

            c.language = ConfigManager.byteArrayToString(data, ++index, 2);
            ++index;

			//->Kenneth : 4K by June
            String resolution = ConfigManager.byteArrayToString(data, ++index, 2).toUpperCase();
            if ("HD".equals(resolution)) {
                c.definition = TvChannel.DEFINITION_HD;
            } else if ("UD".equals(resolution)) {
                c.definition = TvChannel.DEFINITION_4K;
            } else {
                c.definition = TvChannel.DEFINITION_SD;
            }
            ++index;

            c.genreId = ConfigManager.oneByteToInt(data, ++index);

            len = ConfigManager.oneByteToInt(data, ++index);
            if (len > 0) {
                c.appName = ConfigManager.byteArrayToString(data, ++index, len);
                index += len - 1;

                len = ConfigManager.oneByteToInt(data, ++index);
                c.param = ConfigManager.byteArrayToString(data, ++index, len);
                index += len - 1;
            }
            channelTable.put(c.callLetter, c);

            //=>Kenneth[2017.3.23] R7.3 중에 로그 찍는 거 막음.
            //TEST
            //Log.printDebug(c.toString());
            //<-
        }

        int hdChannelSize = ConfigManager.oneByteToInt(data, ++index);
        hdName = new String[hdChannelSize];
        sdName = new String[hdChannelSize];
        for (int a = 0; a < hdChannelSize; a++) {
            int hdNameLength = ConfigManager.oneByteToInt(data, ++index);

            hdName[a] = ConfigManager.byteArrayToString(data, ++index, hdNameLength);
            index += hdNameLength - 1;

            int sdNameLength = ConfigManager.oneByteToInt(data, ++index);

            sdName[a] = ConfigManager.byteArrayToString(data, ++index, sdNameLength);
            index += sdNameLength - 1;
        }
        //->Kenneth[2017.3.23] R7.3 APD disabled
        // 아래 APD disabled 에서 사용하기 위한 cache 
        // 이를 통해서 중복되는 call letter 로 인해 channel_info.txt 파일 사이즈가 커지는 것을 막는다
        String[] chNames = new String[channelSize];
        //<-
        try {
            for (int i = 0; i < channelSize; i++) {
                int len = ConfigManager.oneByteToInt(data, ++index);
                String str = ConfigManager.byteArrayToString(data, ++index, len);
                //->Kenneth[2017.3.23] R7.3 APD disabled
                chNames[i] = str;
                //<-
                index += len - 1;
                ChannelInfo info = (ChannelInfo) channelTable.get(str);
                int screenData = ConfigManager.oneByteToInt(data, ++index);
                if (info != null) {
                    info.screenData = screenData;
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        //->Kenneth[2017.3.23] R7.3 APD disabled
        if ((index+1) >= data.length) return;
        try {
            for (int i = 0; i < channelSize; i++) {
                ChannelInfo info = (ChannelInfo) channelTable.get(chNames[i]);
                String valueStr = ConfigManager.byteArrayToString(data, ++index, 1);
                if ("N".equalsIgnoreCase(valueStr)) {
                    info.apdDisabled = false;
                } else {
                    info.apdDisabled = true;
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        //<-
    }

    public int getVersion() {
        return version;
    }

    public String[] getLanguageType() {
        return languageType;
    }

    private String arrayToString(Object array, int len) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        for (int i = 0; i < len; i++) {
            if (i > 0)
                buffer.append(", ");
            if (array instanceof int[])
                buffer.append(((int[]) array)[i]);
            if (array instanceof Object[])
                buffer.append(((Object[]) array)[i]);
        }
        buffer.append("]");
        return buffer.toString();
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof ChannelInfoData) {
            return ((ChannelInfoData)o).getVersion() == version;
        }
        return false;
    }

    public String toString() {
        return "ChannelInfoData[size=" + channelTable.size() + ", ver=" + version + "]";
    }

}

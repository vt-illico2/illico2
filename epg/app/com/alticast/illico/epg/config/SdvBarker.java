package com.alticast.illico.epg.config;

import com.alticast.illico.epg.*;
import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.util.*;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.sdv.mc.TuningInfo;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import java.io.File;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.Vector;

//->Kenneth[2017.9.26] R7.4 이후로는 더이상 이 클래스를 사용하지 않는다
public class SdvBarker {

    private Hashtable table = new Hashtable();
    public TuningInfo[] tuningInfos;

    public static SdvBarker create(File file) {
        try {
            return create(TextReader.read(new FileReader(file)));
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private static SdvBarker create(String[] data) {
        try {
            return new SdvBarker(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private SdvBarker(String[] data) {
        // 첫 line은 mini carousel tuning info를 담고 있다.
        String s = data[0];
        String[] sp = TextUtil.tokenize(s, "|");
        Vector vector = new Vector();
        for (int i = 0; i < sp.length - 2; i++) {
            TuningInfo ti = parseInfo(sp[i + 1]);
            if (ti != null) {
                vector.addElement(ti);
            }
        }
        // 다음 line 부터는 message id, response id 별 message
        for (int i = 1; i < data.length; i++) {
            if (data[i] == null || data[i].trim().length() == 0) {
                continue;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"SdvBarker[" + i + "] = " + data[i]);
            }
            sp = TextUtil.tokenize(data[i], "|");
            int mid = Util.parseHexString(sp[0]);
            int rid = Util.parseHexString(sp[1]);
            table.put(new Integer((mid << 16) | rid), new String[] { sp[2], sp[3] });
        }

        if (vector.size() > 0) {
            TuningInfo[] infos = new TuningInfo[vector.size()];
            vector.copyInto(infos);
            this.tuningInfos = infos;
        }
    }

    public String getMessage(short messageId, short responseId) {
        Object o = table.get(new Integer((((int) messageId & 0xFFFF) << 16) | ((int) responseId & 0xFFFF)));
        if (o == null) {
            return null;
        }
        String[] data = (String[]) o;
        boolean eng = Constants.LANGUAGE_ENGLISH.equals(FrameworkMain.getInstance().getCurrentLanguage());
        return eng ? data[0] : data[1];
    }

    private TuningInfo parseInfo(String s) {
        try {
            int p = s.indexOf(':');
            int freq = Integer.parseInt(s.substring(0, p)) * 1000000;
            String mod = s.substring(p + 1);

            int m;
            if ("256".equals(mod)) {
                m = 0x10;
            } else if ("16".equals(mod)) {
                m = 0x06;
            } else if ("32".equals(mod)) {
                m = 0x07;
            } else if ("64".equals(mod)) {
                m = 0x08;
            } else if ("128".equals(mod)) {
                m = 0x0C;
            } else {
                m = 0;
            }
            return new TuningInfo(freq, m);
        } catch (Exception ex) {
            return null;
        }
    }

}

package com.alticast.illico.epg.config;

public class VirtualChannelInfo {

    public String callLetter;
    public int sourceId;
    public int number;

    public String toString() {
        return "VirtualChannelInfo: " + callLetter + ", " + sourceId + ", " + number;
    }

}


package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.util.Util;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import java.io.File;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.Vector;

public class SdvErrorMessage {
    public String errorCode;
    public short messageId;
    public short responseId;
    public String titleKey;
    public String buttonKey;

    public static SdvErrorMessage create(String line) {
        try {
            SdvErrorMessage m = new SdvErrorMessage();
            String[] sp = TextUtil.tokenize(line, "|");
            m.errorCode = sp[0];
            m.messageId = (short) Util.parseHexString(sp[1]);
            m.responseId = (short) Util.parseHexString(sp[2]);
            m.titleKey = sp[3];
            m.buttonKey = sp[4];
            return m;
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }
}


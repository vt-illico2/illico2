package com.alticast.illico.epg.config;

import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import java.awt.*;
import java.io.*;
import java.util.*;
//->Kenneth[2015.4.16] : R5 : 추가
public class Tutorial {
    public static final String PREF_KEY = "TUTORIAL_CURRENT_COUNT";
    public static final int DO_NOT_DISPLAY_AGAIN = -999;
    // 이건 do not display 를 무력화 시키는 시그널이다.
    // 즉 ms 에서 이 값을 설정하면 do not display 를 엎어쓴다. 요구사항이 없던건데 만약을 위해서 넣어둠.
    public static final int DISPLAY_AGAIN = 126;
    private static Tutorial instance;
    private PreferenceService ps;
    protected Tutorial () {
        init();
    }

    private void init() {
        try {
            ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Tutorial getInstance() {
        if (instance == null) {
            instance = new Tutorial();
        }
        return instance;
    }
    
    public String getTitle() {
        return ConfigManager.getInstance().epgConfig.getTutorialTitle();
    }

    public String getImageName() {
        return ConfigManager.getInstance().epgConfig.getTutorialImageName();
    }

    public String getFirstButtonName() {
        return ConfigManager.getInstance().epgConfig.getTutorialFirstButtonName();
    }

    public String getSecondButtonName() {
        return ConfigManager.getInstance().epgConfig.getTutorialSecondButtonName();
    }

    public String getThirdButtonName() {
        return ConfigManager.getInstance().epgConfig.getTutorialThirdButtonName();
    }

    public long getStartDate() {
        return ConfigManager.getInstance().epgConfig.tutorialStartDate;
    }

    public long getEndDate() {
        return ConfigManager.getInstance().epgConfig.tutorialEndDate;
    }

    public int getMaxDisplay() {
        return ConfigManager.getInstance().epgConfig.tutorialMaxDisplay;
    }

    public void increaseViewCount() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.increaseViewCount()");
        int curCount = getCurrentViewCount();
        int maxCount = getMaxDisplay();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : curCount = "+curCount);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : maxCount = "+maxCount);
        if (curCount == DO_NOT_DISPLAY_AGAIN) return;
        if (curCount >= maxCount) return;
        setCurrentViewCount(++curCount);
    }

    public void doNotDisplayAgain() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.doNotDisplayAgain()");
        setCurrentViewCount(DO_NOT_DISPLAY_AGAIN);
    }

    // totorial popup 을 띄워야 할지 알려준다.
    public boolean needToDisplayTutorial() {
        try {
            //->Kenneth[2016.7.28] R7.2 : Tutorial versioning
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : tutorialVersion = ["
                    +ConfigManager.getInstance().epgConfig.tutorialVersion+"]");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : App.VERSION = ["+App.VERSION+"]");
            if (!App.VERSION.equals(ConfigManager.getInstance().epgConfig.tutorialVersion)) {
                return false;
            }
            //<-
            int curCount = getCurrentViewCount();
            int maxCount = getMaxDisplay();
            long startTime = getStartDate();
            long endTime = getEndDate();
            long curTime = System.currentTimeMillis();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : curCount = "+curCount);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : maxCount = "+maxCount);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : startTime = "+startTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : endTime = "+endTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() : curTime = "+curTime);
            if (curTime < startTime) {
                // 1. start 시간이 아직 도래하지 않음.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() returns false ----- case 1");
                return false;
            }
            if (curTime > endTime) {
                // 2. end 시간이 이미 지나버림.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() returns false ----- case 2");
                return false;
            }
            if (maxCount == 0) {
                // 3. max 가 0 이면 보이지 않음.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() returns false ----- case 3");
                return false;
            }
            if (curCount >= maxCount) {
                // 4. 저장된 count 가 max 와 같거나 큰 경우
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() returns false ----- case 4");
                return false;
            }
            if (curCount == DO_NOT_DISPLAY_AGAIN) {
                // 5. 사용자가 Don't display again 을 세팅 한 경우
                if (maxCount != DISPLAY_AGAIN) {
                    // max count 가 display_again 으로 오면 팝업을 보이게 됨.
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.needToDisplayTutorial() returns false ----- case 5");
                    return false;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return true;
    }

    private int getCurrentViewCount() {
        int currentViewCount = 0;
        try {
            if (ps == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.getCurrentViewCount() ps is null, so call init() again");
                init();
            }
            String countStr = ps.getPreferenceValue(PREF_KEY);
            if (countStr != null && countStr.length() > 0) {
                currentViewCount = Integer.parseInt(countStr);
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.getCurrentViewCount() returns "+currentViewCount);
        return currentViewCount;
    }

    private void setCurrentViewCount(int count) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.setCurrentViewCount("+count+")");
        try {
            if (ps == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.setCurrentViewCount() ps is null, so call init() again");
                init();
            }
            ps.setPreferenceValue(PREF_KEY, count+"");
        } catch (Exception e) {
            Log.print(e);
        }
    }

    // EpgConfig 에 OOB tutorial 데이터 파싱 후에 ConfigManager 로부터 불리운다.
    public void tutorialDataUpdated(long oldStartTime, long oldEndTime) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated()");
        try {
            int curCount = getCurrentViewCount();
            int maxCount = getMaxDisplay();
            long startTime = getStartDate();
            long endTime = getEndDate();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : curCount = "+curCount);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : maxCount = "+maxCount);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : startTime = "+startTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : endTime = "+endTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : oldStartTime = "+oldStartTime);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : oldEndTime = "+oldEndTime);
            if (curCount == DO_NOT_DISPLAY_AGAIN && maxCount == DISPLAY_AGAIN) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : BINGO! Call setCurrentViewCount(0)");
                setCurrentViewCount(0);
            } else if (oldStartTime > 0 && oldEndTime > 0) {
                if ((startTime != oldStartTime) || (endTime != oldEndTime)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.tutorialDataUpdated() : Date updated. So Call setCurrentViewCount(0)");
                    setCurrentViewCount(0);
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public Image getTutorialImage() {
        String name = getImageName();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.getTutorialImage("+name+")");
        if (name == null) return null;
        Image img = FrameworkMain.getInstance().getImagePool().getImage(name);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.getTutorialImage returns "+img);
        return img;
    }

    // ConfigManager 로부터 IB 이미지 업데이트 되었을 경우 불리운다.
    // 생각해 보니 새로 이미지 추가하는 것만 있고 올드 이미지 삭제하는 건 없네..(2015.4.16 -_-)
    public void resetImage(Object data) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.resetImage("+data+")");
        try {
            Hashtable table = ZipFileReader.read((File) data);
            if (table != null) {
                int size = table.size();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Tutorial.resetImage() : size = "+size);
                if (size == 0) return;
                Enumeration keys = table.keys();
                while (keys.hasMoreElements()) {
                    try {
                        String imageName = (String)keys.nextElement();
                        byte[] imageByte = (byte[]) table.get(imageName);
                        Log.printDebug(App.LOG_HEADER+"Tutorial.imageName = "+imageName);
                        Log.printDebug(App.LOG_HEADER+"Tutorial.imageByte = "+imageByte.length);
                        FrameworkMain.getInstance().getImagePool().createImage(imageByte, imageName);
                        FrameworkMain.getInstance().getImagePool().waitForAll();
                        Log.printDebug(App.LOG_HEADER+FrameworkMain.getInstance().getImagePool().getImage(imageName));
                    } catch (Exception e) {
                        Log.print(e);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.alticast.illico.epg.config;

public class EpgLogos {
    private int version;
    private String favouriteIcoName;
    private int programIdentifierIconSize;
    private int[] programId;
    private String[] programIdentifierIconName;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getFavouriteIcoName() {
        return favouriteIcoName;
    }

    public void setFavouriteIcoName(String favouriteIcoName) {
        this.favouriteIcoName = favouriteIcoName;
    }

    public int getProgramIdentifierIconSize() {
        return programIdentifierIconSize;
    }

    public void setProgramIdentifierIconSize(int programIdentifierIconSize) {
        this.programIdentifierIconSize = programIdentifierIconSize;
        programId = new int[programIdentifierIconSize];
        programIdentifierIconName = new String[programIdentifierIconSize];
    }

    public int[] getProgramId() {
        return programId;
    }

    public void setProgramId(int num, int programId) {
        this.programId[num] = programId;
    }

    public String[] getProgram_identifier_icon_name() {
        return programIdentifierIconName;
    }

    public void setProgramIdentifierIconName(int num, String programIdentifierIconName) {
        this.programIdentifierIconName[num] = programIdentifierIconName;
    }

    public String toString() {
        return "EpgLogos [favouriteIcoName="
                + favouriteIcoName
                + ", programId="
                + (programId != null ? arrayToString(programId, programId.length) : null)
                + ", programIdentifierIconName="
                + (programIdentifierIconName != null ? arrayToString(programIdentifierIconName,
                        programIdentifierIconName.length) : null) + ", programIdentifierIconSize="
                + programIdentifierIconSize + ", version=" + version + "]";
    }

    private String arrayToString(Object array, int len) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        for (int i = 0; i < len; i++) {
            if (i > 0)
                buffer.append(", ");
            if (array instanceof int[])
                buffer.append(((int[]) array)[i]);
            if (array instanceof Object[])
                buffer.append(((Object[]) array)[i]);
        }
        buffer.append("]");
        return buffer.toString();
    }

}

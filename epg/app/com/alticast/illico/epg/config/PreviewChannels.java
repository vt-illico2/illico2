package com.alticast.illico.epg.config;

import com.opencable.handler.cahandler.*;
import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class PreviewChannels {

    // String call_letter, long[] eids
    Hashtable table = new Hashtable();
    // Long EID
    ArrayList idList = new ArrayList();
    // Long EID, String call_letter
    Hashtable idChannelMap = new Hashtable();

    // Long EID, Boolean Auth
    static Hashtable authMap = new Hashtable();

    private static final Object LOCK = new Byte((byte) 0xff);

    private static PreviewChannels current = null;

    private static CAListener listener = new CAListener();

    public static void setCurrent(PreviewChannels next) {
        current = next;
        recheckAuth();
    }

    public static void recheckAuth() {
        synchronized (LOCK) {
            PreviewChannels pc = current;
            if (pc == null) {
                return;
            }
            int size = pc.idList.size();
            CaManager cm = CaManager.getInstance();
            for (int i = 0; i < size; i++) {
                Long id = (Long) pc.idList.get(i);
                if (!authMap.containsKey(id)) {
                    Log.printDebug(App.LOG_HEADER+"PreviewChannels: new EID = " + id);
                    long eid = id.longValue();
                    try {
                        CAAuthorization auth = cm.getResourceAuthorization(eid, listener);
                        if (auth != null) {
                            boolean b = auth.isAuthorized();
                            authMap.put(id, Boolean.valueOf(b));
                            Log.printDebug(App.LOG_HEADER+"PreviewChannels: authorized = " + b);
                        } else {
                            Log.printDebug(App.LOG_HEADER+"PreviewChannels: CAAuthorization is null");
                        }
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                } else {
                    Log.printDebug(App.LOG_HEADER+"PreviewChannels: EID is already exists = " + id);
                }
            }
        }
    }

    public static PreviewChannels create(File file) {
        try {
            return create(TextReader.read(new FileReader(file)));
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private static PreviewChannels create(String[] data) {
        try {
            if (data != null && data.length > 0) {
                return new PreviewChannels(data);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private PreviewChannels(String[] data) {
        for (int i = 0; i < data.length; i++) {
            String s = data[i];
            int pos = s.indexOf('=');
            if (pos > 0) {
                String key = s.substring(0, pos);
                String[] ids = TextUtil.tokenize(s.substring(pos + 1), ',');
                long[] eids = new long[ids.length];
                for (int j = 0; j < ids.length; j++) {
                    eids[j] = Long.parseLong(ids[j]);
                    Long eid = new Long(eids[j]);
                    idList.add(eid);
                    idChannelMap.put(eid, key);
                }
                table.put(key, eids);
            }
        }
    }

    public static boolean hasPackage(Channel ch) {
        return hasPackage(ch.getName());
    }

    //->Kenneth[2016.8.29] : CYO Bundling 수행하면서 정준과 나눈 대화를 history 로 기록해 둔다.
    // 1. preview 관련해서 EPG 가 OOB 로 받는 파일은 2 개임. channel_free_previews.xml / previewChannels.txt
    // 2. PreviewChannels.java 는 previewChannels.txt 의 내용을 처리하는 곳으로 preview 채널의 EID 를 가지고 패지키 여부를 체크한다.
    // 3. previewChannels.txt 에는 다음과 같은 라인들이 포함되어 있다. "CBC = 123, 456, 789"
    // 4. 라인의 의미는 예를 들면 "CBC = 고급형채널패키지|HD패키지|여름특선패키지" 라는 식이다.
    // 5. 어떤 가입자가 HD 패키지 가입해 있으면 CBC 는 가입된 채널이라고 true 를 돌려줄 것임.
    // 6. CA 의 경우 CBC source id = 6006 이라 치면 source id 로 체크해도 true 로 나올 거고 hasPackage 도 true 를 줄 것임.
    // 7. 만약 다른 채널이 "CTV=고급형패키지|여름특선패키지" 로 되어 있다면 그 가입자가 HD 패키지가 있으면 hasPackage 는 false를 돌려줄 것임.
    // 8. 즉 얘는 CTV 는 가입안했다 라고 이야기 할 수 있음. (주의 : "가입/미가입" 이지 "AV 볼수있음/없음" 이 아님.)
    // 9. "AV볼수있음/없음" 은 isAuthorized 나 isSubscribed 로 판단한다. PreviewChannels.hasPackage() 랑관계없음."
    // 10. 다만 중요한 것은 source id 로 검사해 보면 보일수도 있고 (isAuthorized 가 true 리턴) 안 보일수도 (false 리턴) 있음.
    // 11. 보인다면 (true) free preview 니까 선물표시, 안 보인다면 (false) 걍 안보이는 거니까 미가입채널 아이콘 그림.

    // 12. 실제 테스트해보니 lab 이나 qualif 에 previewChannels.txt 에 아무 내용 없었음.(MS 에서만 확인한 거라 실제 송출되는 내용은 확인필요)
    // 13. 아래 보면 pc == null 이면 true 리턴함. 즉 previewChannels 를 못 받거나 내용이 없으면 hasPackage 는 항상 true 임.
    // 14. 이에 대한 준의 답변은 "그냥 체크하지 못하는 상황이기 때문에 미가입으로 단정할 근거가 부족하다" 라는 것임.
    // 15. 이 경우 source id 로 체크하는 것에 의존하게 될 것이기 때문에 별 문제 없을 것이다 라고 함.
    public static boolean hasPackage(String callLetter) {
        PreviewChannels pc = current;
        if (pc == null) {
            return true;
        }
        long[] ids = (long[]) pc.table.get(callLetter);
        boolean noPackage = false;
        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                Long id = new Long(ids[i]);
                Boolean auth = (Boolean) authMap.get(id);
                if (auth != null) {
                    if (auth.booleanValue()) {
                        Log.printDebug(App.LOG_HEADER+"PreviewChannels: " + callLetter + " has package : " + id);
                        return true;
                    } else {
                        Log.printDebug(App.LOG_HEADER+"PreviewChannels: " + callLetter + " doesn't have package : " + id);
                        noPackage = true;
                    }
                }
            }
        }
        return !noPackage;
    }


    static class CAListener implements CAAuthorizationListener {
        public void deliverEvent(CAAuthorizationEvent event) {
            Log.printInfo(App.LOG_HEADER+"PreviewChannels.deliverEvent: event = " + event);
            if (event == null || event.getTargetType() != CAAuthorization.RESOURCE_TARGET) {
                return;
            }
            long id = event.getTargetId();
            boolean auth = event.isAuthorized();
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"PreviewChannels.deliverEvent: targetId   = " + id);
                Log.printDebug(App.LOG_HEADER+"PreviewChannels.deliverEvent: authorized = " + auth);
            }
            if (id >= CaManager.EID_PREFIX) {
                id -= CaManager.EID_PREFIX;
            }
            Long eid = new Long(id);
            Boolean old = (Boolean) authMap.put(eid, Boolean.valueOf(auth));
            if (old != null && old.booleanValue() != auth) {
                Log.printDebug(App.LOG_HEADER+"PreviewChannels.deliverEvent: changed !");
                PreviewChannels pc = PreviewChannels.current;
                if (pc != null) {
                    String call = (String) pc.idChannelMap.get(eid);
                    if (call != null) {
                        ChannelDatabase db = ChannelDatabase.current;
                        Channel ch = db.getChannelByName(call);
                        if (ch != null) {
                            CaManager.getInstance().notifyCaUpdated(ch.getSourceId(), hasPackage(call));
                        }
                    }
                }
            }
        }
    }


}


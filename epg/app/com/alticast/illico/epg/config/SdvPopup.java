package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.alticast.illico.epg.sdv.mc.MiniCarouselReader;
import com.alticast.illico.epg.util.Util;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import java.io.File;
import java.io.FileReader;
import java.util.Hashtable;
import java.util.Vector;

public class SdvPopup {

    private Hashtable table = new Hashtable();

    public static SdvPopup create(File file) {
        try {
            return create(TextReader.read(new FileReader(file)));
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private static SdvPopup create(String[] data) {
        try {
            return new SdvPopup(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private SdvPopup(String[] data) {
        for (int i = 0; i < data.length; i++) {
            SdvErrorMessage m = SdvErrorMessage.create(data[i]);
            if (m != null) {
                table.put(new Integer((((int) m.messageId & 0xFFFF) << 16) | ((int) m.responseId & 0xFFFF)), m);
            }
        }
    }

    public SdvErrorMessage getMessage(short messageId, short responseId) {
        Object o = table.get(new Integer((((int) messageId & 0xFFFF) << 16) | ((int) responseId & 0xFFFF)));
        if (o == null) {
            return null;
        }
        return (SdvErrorMessage) o;
    }
}




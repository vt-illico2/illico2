package com.alticast.illico.epg.config;

import com.alticast.illico.epg.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.util.Constants;
import java.io.File;

public class VirtualChannelMap {
    public int version;

    // language
    public String[] languageType;
    public String[] defaultText;

    // channels
    public VirtualChannelInfo[] channels;


    public static VirtualChannelMap create(File file) {
        return create(BinaryReader.read(file));
    }

    public static VirtualChannelMap create(byte[] data) {
        try {
            return new VirtualChannelMap(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }


    private VirtualChannelMap(byte[] data) {
        int index = 0;
        this.version = ConfigManager.oneByteToInt(data, index++);
        int channelSize = ConfigManager.twoBytesToInt(data, index, false);
        index += 2;

        channels = new VirtualChannelInfo[channelSize];
        for (int a = 0; a < channelSize; a++) {
            VirtualChannelInfo c = new VirtualChannelInfo();
            channels[a] = c;

            int callLettersLength = ConfigManager.oneByteToInt(data, index++);
            c.callLetter = ConfigManager.byteArrayToString(data, index, callLettersLength);
            index += callLettersLength;

            c.sourceId = ConfigManager.twoBytesToInt(data, index, false);
            index += 2;

            c.number = ConfigManager.twoBytesToInt(data, index, false);
            index += 2;
        }
    }

    public int getVersion() {
        return version;
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof VirtualChannelMap) {
            return ((VirtualChannelMap)o).getVersion() == version;
        }
        return false;
    }

    public void dump() {
        Log.printDebug("VirtualChannelMap.dump==========");
        Log.printDebug("VirtualChannelMap.version = " + version);
        for (int i = 0; i < channels.length; i++) {
            Log.printDebug("VirtualChannelMap.channel[" + i + "] = " + channels[i]);
        }
    }

    public String toString() {
        return "VirtualChannelMap[size=" + channels.length + ", ver=" + version + "]";
    }

}

package com.alticast.illico.epg.config;

import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class SdvChannelInfo implements Comparable {

    public String callLetter;
    public int number;
    public int sourceId;
    public int hubId;

    public SdvChannelInfo(String data) {
        String[] s = TextUtil.tokenize(data, '|');
        callLetter = s[0];
        number = Integer.parseInt(s[1]);
        sourceId = Integer.parseInt(s[2]);
        hubId = Integer.parseInt(s[3]);
        //->Kenneth
        this.toString();
    }

    public String toString() {
        return "SdvChannelInfo: " + callLetter + ", " + number + ", " + sourceId + ", " + hubId;
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof SdvChannelInfo) {
            SdvChannelInfo s = (SdvChannelInfo) o;
            return hubId == s.hubId
                && sourceId == s.sourceId
                && number == s.number
                && callLetter.equals(s.callLetter);
        }
        return false;
    }

    /** Comparable. */
    public int compareTo(Object o) {
        if (o instanceof SdvChannelInfo) {
            SdvChannelInfo s = (SdvChannelInfo) o;
            int diff = number - s.number;
            if (diff != 0) {
                return diff;
            }
            diff = sourceId - s.sourceId;
            if (diff != 0) {
                return diff;
            }
            diff = hubId - s.hubId;
            if (diff != 0) {
                return diff;
            }
            return callLetter.compareTo(s.callLetter);
        } else {
            return -1;
        }
    }

}



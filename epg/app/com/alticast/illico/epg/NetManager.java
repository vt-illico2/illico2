package com.alticast.illico.epg;

import java.util.*;
import com.opencable.handler.nethandler.*;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.epg.data.*;
import com.alticast.illico.epg.sdv.*;
import com.videotron.tvi.illico.util.Environment;
import org.ocap.hardware.pod.POD;

public class NetManager implements NetHandlerStateListener, Runnable {

    public static final String HUB_ID_KEY = "HUB_ID";

    NetHandler handler;
    int state = -1;
    int hubId = -1;

    private static NetManager instance = new NetManager();

    public static NetManager getInstance() {
        return instance;
    }

    public NetManager() {
    }

    public void init(NetHandler handler) {
        this.handler = handler;
        try {
            handler.addStateListener(this);
            synchronized (this) {
                setState(handler.getState());
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NetManager.init() : Call Thread.start()");
            new Thread(this, "NetManager.getHubId").start();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public synchronized void deliverEvent(NetHandlerStateEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NetManager.deliverEvent("+event+")");
        setState(event.getNewState());
    }

    private void setState(int newState) {
        this.state = newState;
        if (state == NetHandler.NH_READY) {
            try {
                String hh = handler.getAttribute("HubId");
            } catch (Exception ex) {
            }
        }
    }

    /** Returns the hub id. */
    public int getHubId() {
        return hubId;
    }

    private int readyHubId() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NetManager.readyHubId()");
        if (App.EMULATOR) {
            return 10;
        }
        byte[] b = POD.getInstance().getHostParam(0x0D);
        if (b == null || b.length <= 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("Util: no hubId.");
            }
            return 0;
        }
        int id = 0;
        for (int i = 0; i < b.length; i++) {
            id = id << 8;
            id = id | b[i];
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetManager: hubId = " + id);
        }
        return id;
    }

    public void run() {
        while (true) {
            hubId = readyHubId();
            if (hubId > 0) {
                SdvDiagnostics.set(SdvDiagnostics.HUB_ID, hubId);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NetManager.run() : hudId = "+hubId);
                DataCenter.getInstance().put(HUB_ID_KEY, new Integer(hubId));
                return;
            }
            try {
                Thread.sleep(2000L);
            } catch (Exception ex) {
            }
        }
    }
}

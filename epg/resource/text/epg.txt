options	Options	Options
search	Recherche	Search
details	Détails	Details
change_date	Changer date	Change date
scroll_description	Défiler la description	Scroll description
change_page	Changer page	Change Page
tv_guide	Guide horaire	Program Guide
filters	Filtres	Filters
extended_view	Plein écran	Full Screen
normal_view	Vue normale	Normal View
exit	Quitter	Exit
back	Retour	Back
close	Fermer	Close
Yes	Oui	Yes
No	Non	No
OK	OK	OK
Cancel	Annuler	Cancel
frequency	Fréquence :	Frequency :
no_data_title	Information non disponible	Information unavailable
no_filtered_popup_title	Filtre vide	Filter is empty 
no_filtered_popup_msg	Ce filtre est présentement vide.	This filter is currently empty.
fav_empty_popup_title	Aucune chaîne favorite	No favourite channels selected
fav_empty_popup_msg	"Votre liste de chaînes favorites est vide.||Pour ajouter une chaîne à vos favoris,|sélectionnez‐la, puis appuyez sur|la touche <button name=""btn_pro""/> au bas de votre|télécommande."	"Your Favourite Channels list is empty.||To add a channel to your favourites,|select it and press <button name=""btn_pro""/>|at the bottom of your remote."
filtered_by	Filtre : 	Filter : 
actors	Acteurs	Actors
director	Réalisateur	Director
past_program	Passé	Previously
now_playing	Présentement diffusé	Now playing:
future_program	À venir	Upcoming
pip_swap	Inverser PIP	PIP Swap
pip_size	Affichage PIP	PIP Display
pip_position	Position PIP	PIP Position
today	Aujourd’hui	Today
full_guide	Guide complet	Full Guide
no_more_data_popup_title	Information non disponible	Information unavailable
no_more_data_popup_msg	Aucune donnée disponible pour le moment.	Data unavailable at this time.
blocked_title	Contenu bloqué	Blocked Content
blocked_channel	Chaîne bloquée	Blocked Channel
blocked_message_1	Cette émission est bloquée.	This program is blocked.
blocked_message_2	Cette chaîne est bloquée.	This channel is blocked.
station	Station	Station
unblock_popup_title	Chaîne débloquée	Channel Unblocked
unblock_popup_msg_0	La chaîne sera débloquée jusqu’à ce que vous syntonisiez une autre chaîne.	Channel will remain unblocked until you tune in to another channel.
unblock_popup_msg_4	La chaîne sera débloquée pendant 4 heures.	Channel will remain unblocked for 4 hours.
unblock_popup_msg_8	La chaîne sera débloquée pendant 8 heures.	Channel will remain unblocked for 8 hours.
reminder_change_popup_title	Modifier votre rappel?	Modify your reminder?
reminder_change_popup_msg_1	Vous avez déjà programmé un rappel pour %% 	You have already set a reminder for %%
reminder_change_popup_msg_2	Voulez-vous le remplacer par:	Do you want to replace it with:
press_b_unblock_1	"Appuyez sur <button name=""btn_b_big""/> pour débloquer cette émission."	"Press <button name=""btn_b_big""/> to unblock this program."
press_b_unblock_2	"Appuyez sur <button name=""btn_b_big""/> pour débloquer cette chaîne."	"Press <button name=""btn_b_big""/> to unblock this channel."
suspend_desc	"Pour suspendre les contrôles et limites temporairement, appuyez sur <button name=""btn_d""/> et|sélectionnez « Suspendre les contrôles et limites »"	"To temporarily suspend controls and limits, press <button name=""btn_d""/> and|select ""Suspend controls and limits""."
modify_pc_desc	"Pour plus d'options, allez à Menu ≥ Paramètres ≥ Contrôles et limites."	"For more options, go to Menu ≥ Settings ≥ Controls and Limits."
modify_pc_desc2	"Ce contenu est bloqué. Pour modifier les Contrôles et limites,|allez à Menu › Paramètres › Contrôles et limites."	"This content is blocked. To modify your Controls and Limits,|go to Menu › Settings › Controls and Limits."
change_category	Changer de catégorie	Change Category
virtual_default	Contenu non disponible	Content unavailable
desc_go_pref	Veuillez entrer le NIP admin|pour accéder aux paramètres.	Please enter your Admin PIN|to access your preferences.
unblock_pin	"Entrez le NIP admin pour débloquer|l’accès au contenu SD, HD et Sur demande|de FULL_NAME."	"Enter your Admin PIN.|All SD, HD and On Demand content for|FULL_NAME|will be unblocked."
block_pin	"Entrez le NIP admin pour bloquer|l’accès au contenu SD, HD et Sur demande|de FULL_NAME."	"Enter your Admin PIN.|All SD, HD and On Demand content for|FULL_NAME|will be blocked."
suspend_pin	Veuillez entrer le NIP admin|pour suspendre les Contrôles et limites.	Please enter your Admin PIN|to suspend Controls and Limits.
restore_pin	Veuillez entrer le NIP admin|pour rétablir les Contrôles et limites.	Please enter your Admin PIN|to restore Controls and Limits.
rec_unsub_title	Chaîne non disponible	Channel not available
rec_unsub_msg	"Chaîne non-incluse dans votre forfait.|Pour vous abonner, visitez le %1 ou composez le %2."	"This channel is not included in your package.|To subscribe, go to %1 or %2."
subscribe_now	M’abonner maintenant	Subscribe now
change_tv_package	Modifier mon forfait télé	Change my TV package
change_package	Modifier mon forfait	Change my package
subscribe_channel	M’abonner maintenant	Subscribe to this channel
subscribe_now_to	Abonnez-vous dès maintenant à %%	Subscribe now to %%
add_to_package	Ajoutez %% à votre forfait	Add %% to your package
subscribe_isa_text_1	Ajoutez rapidement cette chaîne à votre	Easily add this channel to your package
subscribe_isa_text_2	forfait grâce à l’abonnement instantané!	through the instant subscription! 
subscribe_cyo_text_1	Cette chaîne peut être ajoutée à votre forfait	This channel can be added to your package
subscribe_cyo_text_2	via l’application Changer vos chaines	through the Change Your Channels application
about	À propos de cette chaîne	About this channel
unsub_title	Cette chaîne n’est pas incluse dans votre forfait actuel.	This channel is not included in your current package.
unsub_line1	Pour vous abonner à cette chaîne :	To subscribe to this channel:
go_to	visitez le	Go to
or	ou	or
call_to	composez le	call
URL	www.videotron.com/espaceclient	www.videotron.com/customercentre
PHONE_NUMBER	1 877 512-0911	1-877-512-0911
admin_pin1	Seul l'administrateur peut accéder à cette section.	Only the Administrator can access this section.
admin_pin2	Veuillez entrer le NIP admin.	Please enter your Admin PIN.
discover_ch	Découvrez cette chaîne sur :	Discover this channel on :
hdmi_title	Contrôle du volume non disponible	Volume control unavailable
hdmi_msg	Utilisez la télécommande de votre téléviseur ou programmez votre télécommande illico.|Info : %%	Use your TV’s remote control or program your illico remote control.|Info %%
dont_show_again	Ne plus afficher ce message	Don’t show this message again
hdmi_url	www.videotron.com/telecommande	www.videotron.com/remotecontrol
hdmi_simple_title	Programmez votre télécommande illico :	Program your illico remote control :
uhd_title	Contenu ultra-haute Définition (UHD)	Ultra HD Content
highlight0	Films	Movies
highlight1	Séries	Series
highlight2	Sports	Sports
highlight3	Documentaires	Documentaries
highlight4	Enfants	Children's
highlight5	Variétés	Variety shows
highlight6	Téléréalités	Reality shows
highlight7	Nouvelles	News
highlight8	Musique	Music
highlight_none	Aucun	None
highlights	Types de contenus	Content types
clear_highlights	Effacer surbrillance	Clear Highlights
highlight_title	Types de contenus	Content types
highlight_selection_title	Surbrillance de contenu	Content types
highlight_desc1	Mettez votre type de contenu préféré	Highlight your favourite content type
highlight_desc2	en surbrillance et trouvez-le rapidement.	so you can find it more easily.
add_fav	Ajouter ch. favorites	Add favorite Ch.
remove_fav	Retirer ch. favorites	Remove favorite Ch.
back_to_grid	Retour à la grille	Back to grid
incomp_reason_no_uhd_package	Votre terminal n’est pas compatible UHD/4K	Your terminal is not UHD/4K compatible
incomp_reason_epg850	Code 850 – Télé ou câble HDMI non compatible UHD/4K	Code  850 – TV or HDMI cable not UHD/4K compatible
incomp_reason_epg851	Code 851 – Télé ou port HDMI non compatible HDCP 2.2	Code  851 – TV or HDMI port not HDCP 2.2 compatible
my_filters	Mes filtres	My Filters
channel_filters	Filtres de chaînes	Channel filters

text_IND	Télé à la carte	Pay-per-View
text_VCC	Télé à la carte	Pay-per-View
text_galaxie	Stingray Musique	Stingray Music
text_radio	Chaîne radio	Radio Channel
text_blocked_program	Émission bloquée	Blocked Program
text_blocked_channel	Chaîne bloquée	Blocked Channel
text_unsub	Abonnement requis	Subscription required
text_sdv	Chaîne non disponible	Channel unavailable
text_isa	Cette chaîne est disponible en abonnement instantané.	This channel is available for instant subscription.

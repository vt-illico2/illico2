﻿This is EPG App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 1.2.6.1_PO2
    - Release App file Name
      : EPG_PO2.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2014.01.29

***
version 1.2.6.1_PO2 - 2014.01.29
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5022 [CQ][PO2][ISA][R4] For free preview channels, In EPG application/current program: if user presses on SELECT button then EPG is not tuning in directly to the channel

***
version 1.2.5.0_PO2 - 2014.01.06
    - New Feature
        + Displays ISA or unsubscribed icon when the channel is accessible by free preview.
    - Resolved Issues
        N/A

***
version 1.2.4.1_PO2 - 2013.12.19
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4981 [CQ][PO2][ISA][R4][Preview] - Can't Subscribe to a channel from Free preview channels menu

***
version 1.2.3.0_PO2 - 2013.11.21
    - New Feature
        + Launch ISA popup when user press enter from the current program of EPG.
        + The toggle of ISA icon will be synchronized with program's "subscribe now" icon.
        + Use ISA app to launch CYO application
    - Resolved Issues
        + N/A

***
version 1.2.2.0_PO2 - 2013.11.05
    - New Feature
        + ISA icon and Unsubribed icon will be toggled in EPG guide.
    - Resolved Issues
        + N/A

***
version 1.2.1.0_PO2 - 2013.10.30
    - New Feature
        + Shows PIN enabler before launch CYO.
        + New IXC API to be notified update of channel package.
    - Resolved Issues
        + N/A

***
version 1.2.0.0_PO2 - 2013.10.28
    - New Feature
        + First release for ISA
    - Resolved Issues
        + N/A

/*
 *  @(#)MusicListRenderer.java 1.0 2011.06.08
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.AlphaComposite;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;

import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBGraphics;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.ContentListUI;
import com.videotron.tvi.illico.pcs.ui.MusicListUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>MusicListRenderer</code> The class to paint a GUI for list.
 *
 * @since   2011.06.08
 * @version $Revision: 1.24 $ $Date: 2012/09/28 05:55:04 $
 * @author  tklee
 */
public class MusicListRenderer extends BasicRenderer {
    /** The clip rectangle for wall UI. */
    private final Rectangle clipRec = new Rectangle(0, 0, 960, 480);
    /** The path name. */
    private static final String[] NAME_PATH =
        {"music", "albums", "music", "music", "artists", "artists", "artists", "artists",
            "music", "genres", "music", "playlists", "music"};

    /** The wall play icon img. */
    private Image wallPlayIconImg = null;
    /** The list play icon img. */
    private Image listPlayIconImg = null;
    /** The list play icon dim img. */
    private Image listPlayIconDimImg = null;
    /** The wall view all icon img. */
    private Image wallViewAllIconImg = null;
    /** The wall photo high img. */
    private Image wallPhotoHighImg = null;
    /** The wall default img. */
    private Image wallDefaultImg = null;
    /** The wall photo shadow img. */
    private Image wallPhotoShadowImg = null;
    /** The wall focus img. */
    private Image wallFocusImg = null;
    /** The wall shadow top img. */
    private Image wallShadowTopImg = null;
    /** The wall shadow bottom img. */
    private Image wallShadowBottomImg = null;
    /** The arr left img. */
    private Image arrLImg = null;
    /** The arr right img. */
    private Image arrRImg = null;
    /** The arr top img. */
    private Image arrTImg = null;
    /** The arr bottom img. */
    private Image arrBImg = null;
    /** The title glow img. */
    private Image titleGlowImg = null;
    /** The list bg img. */
    private Image listBgImg = null;
    /** The list line img. */
    private Image listLineImg = null;
    /** The list shadow top img. */
    private Image listShadowTopImg = null;
    /** The list shadow bottom img. */
    private Image listShadowBottomImg = null;
    /** The list focus img. */
    private Image listFocusImg = null;
    /** The list album bg img. */
    private Image listAlbumBgImg = null;
    /** The list floder glow img. */
    private Image listFloderGlowImg = null;
    /** The list random icon img. */
    private Image listRandomIconImg = null;
    /** The list random icon focus img. */
    private Image listRandomIconFocusImg = null;

    /** The content txt. */
    public String[] contentTxt = new String[13];
    /** The sort txt. */
    private String[] sortTxt = new String[4];
    /** The viewall txt. */
    private String[] viewallTxt = new String[2];
    /** The random txt. */
    private String randomTxt = null;
    /** The music txt. */
    private String musicTxt = null;
    /** The albums txt. */
    private String albumsTxt = null;
    /** The artists txt. */
    private String artistsTxt = null;
    /** The artist txt. */
    private String artistTxt = null;
    /** The genres txt. */
    private String genresTxt = null;
    /** The genre txt. */
    private String genreTxt = null;
    /** The playlists txt. */
    private String playlistsTxt = null;
    /** The view all txt. */
    private String viewAllTxt = null;
    /** The songs txt. */
    private String songsTxt = null;
    /** The all songs txt. */
    public String allSongsTxt = "";
    /** The no playlist title txt. */
    private String noPlaylistTitleTxt = null;
    /** The no playlist sub title txt. */
    private String noPlaylistSubTitleTxt = null;
    /** The no playlist desc. */
    private String noPlaylistDesc = null;
    /** The albums small txt. */
    private String albumsSmallTxt = null;
    /** The songs small txt. */
    private String songsSmallTxt = null;
    /** The album txt. */
    private String albumTxt = null;
    /** The album txt2. */
    private String albumTxt2 = null;
    /** The song txt. */
    private String songTxt = null;
    /** The playlist txt. */
    private String playlistTxt = null;
    /** The path txt. */
    private String[] pathTxt = new String[NAME_PATH.length];

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        wallPhotoHighImg = getImage("09_high_133.png");
        wallPhotoShadowImg = getImage("09_sh_133.png");
        wallDefaultImg = getImage("09_mu_default.png");
        wallFocusImg = getImage("09_focus_133.png");
        arrTImg = getImage("02_ars_t.png");
        arrBImg = getImage("02_ars_b.png");
        arrLImg = getImage("02_ars_l.png");
        arrRImg = getImage("02_ars_r.png");
        titleGlowImg = getImage("07_wizard_glow.png");
        wallPlayIconImg = getImage("09_play.png");
        wallViewAllIconImg = getImage("09_mu_viewall.png");
        wallShadowTopImg = getImage("09_ph_sha_t.png");
        wallShadowBottomImg = getImage("09_ph_sha_b.png");
        listBgImg = getImage("09_list_bg.png");
        listLineImg = getImage("06_listline.png");
        listShadowTopImg = getImage("06_list_sha_t.png");
        listShadowBottomImg = getImage("06_list_sha.png");
        listFocusImg = getImage("06_list_foc.png");
        listAlbumBgImg = getImage("09_album_bg.png");
        listFloderGlowImg = getImage("09_al_folder_glow.png");
        listPlayIconImg = getImage("play_btn.png");
        listPlayIconDimImg = getImage("play_btn_dim.png");
        listRandomIconImg = getImage("09_icon_random.png");
        listRandomIconFocusImg = getImage("09_icon_random_f.png");
        super.prepare(c);
        musicTxt = getString("music");
        albumsTxt = getString("albums");
        artistsTxt = getString("artists");
        artistTxt = getString("Artist");
        genresTxt = getString("genres");
        genreTxt = getString("Genre");
        playlistsTxt = getString("playlists");
        playlistTxt = getString("Playlist");
        viewAllTxt = getString("viewAll");
        songsTxt = getString("songs");
        contentTxt[0] = musicTxt;
        contentTxt[1] = albumsTxt;
        contentTxt[2] = musicTxt;
        contentTxt[3] = musicTxt;
        contentTxt[4] = artistTxt;
        contentTxt[5] = artistsTxt;
        contentTxt[8] = musicTxt;
        contentTxt[9] = genresTxt;
        contentTxt[10] = musicTxt;
        contentTxt[11] = playlistsTxt;
        contentTxt[12] = musicTxt;
        albumsSmallTxt = getString("albumsSmall");
        songsSmallTxt = getString("songsSmall");
        albumTxt = getString("album");
        albumTxt2 = getString("Album");
        songTxt = getString("song");
        sortTxt[0] = getString("sortByAlpaA");
        sortTxt[1] = getString("sortByAlpaZ");
        sortTxt[2] = getString("sortByNewest");
        sortTxt[3] = getString("sortByOldest");
        viewallTxt[0] = viewAllTxt;
        viewallTxt[1] = songsSmallTxt;
        randomTxt = getString("playRandom");
        allSongsTxt = getString("allSongs");
        noPlaylistTitleTxt = getString("noPlaylistTitle");
        noPlaylistSubTitleTxt = getString("noPlaylistSubTitle");
        noPlaylistDesc = getString("noPlaylistDesc");
        for (int i = 0; i < NAME_PATH.length; i++) {
            pathTxt[i] = getString(NAME_PATH[i]);
        }
        locTitles = new String[] {locTitleTxt, pathTxt[0]};
    }

    /**
     * Reset location title.
     *
     * @param title the title
     */
    public void resetLocationTitle(String title) {
        locTitles = new String[] {locTitleTxt, title};
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        MusicListUI ui = (MusicListUI) c;
        String path = pathTxt[ui.uiType];
        if (ui.currentRoot != null
                && (ui.uiType == MusicListUI.UI_ARTIST_ALBUM_SONGS || ui.uiType == MusicListUI.UI_ARTIST_ALL_SONGS)) {
            Vector list = ui.currentRoot.getChildren();
            Content tempContent = (Content) list.elementAt(ui.photoFocus);
            if (tempContent == null) { tempContent = (Content) list.elementAt(ui.photoFocus + 1); }
            if (tempContent != null) { path = tempContent.getArtist(); }
        }
        locTitles[1] = path;
        super.paint(g, c);
        if (ui.currentRoot == null) { return; }
        Vector list = ui.currentRoot.getChildren();
        int totalCount = 0;
        if (list != null) { totalCount = list.size(); }
        String str = "";
        if (ui.uiType == MusicListUI.UI_ALBUM || ui.uiType == MusicListUI.UI_ARTIST_ALBUM) {
            Rectangle srcRec = g.getClipBounds();
            if (srcRec != null) {
                g.setClip(srcRec.intersection(clipRec));
            } else { g.setClip(clipRec); }
            int[] num = BasicUI.getInitLastNumberForWall(ui.photoFocus, ui.focusPosition, totalCount,
                    MusicListUI.IMAGES_PER_PAGE);
            g.setFont(Rs.F16);
            int addX = 0;
            int addY = 0;
            for (int i = num[0]; i <= num[1]; i++) {
                Content content = (Content) list.elementAt(i);
                //if (content == null && i > 0) { continue; }
                addX = (i % MusicListUI.IMAGES_PER_LINE) * 164;
                addY = ((i - num[0]) / ContentListUI.IMAGES_PER_LINE) * 177;
                g.translate(addX, addY);
                g.setColor(Rs.DVB90C22);
                g.fillRect(86, 111, 133, 133);
                g.drawImage(wallPhotoShadowImg, 86, 244, c);
                if (i == 0) {
                    g.drawImage(wallViewAllIconImg, 140, 140, c);
                    g.setFont(Rs.F17);
                    g.setColor(Rs.C255);
                    GraphicUtil.drawStringCenter(g, viewallTxt[0], 150, 198);
                    GraphicUtil.drawStringCenter(g, viewallTxt[1], 150, 198 + 14);
                } else {
                    Image img = null;
                    if (content != null) { img = content.getIconImage(); }
                    if (img == null) {
                        g.drawImage(wallDefaultImg, 86 + 52, 153, c);
                    } else {
                        int imgWidth = img.getWidth(null);
                        int imgHeight = img.getHeight(null);
                        int posX = 86 + ((133 - imgWidth) / 2);
                        int posY = 111 + ((133 - imgHeight) / 2);
                        g.drawImage(img, posX, posY, c);
                    }
                }
                //g.drawImage(wallPhotoHighImg, 86, 111, 133, 133, c);
                g.drawImage(wallPhotoHighImg, 86, 111, c);
                if (i != ui.photoFocus && i != 0 && content != null) {
                    String tempStr = content.getArtist();
                    if (ui.uiType == MusicListUI.UI_ARTIST_ALBUM) {
                        tempStr = content.getName() + getDate(content);
                    }
                    str = TextUtil.shorten(tempStr, g.getFontMetrics(), 215 - 90);
                    g.setColor(Rs.C200);
                    g.setFont(Rs.F16);
                    g.drawString(str, 90, 258);
                    if (ui.uiType == MusicListUI.UI_ALBUM) {
                        g.setFont(Rs.F15);
                        g.setColor(Rs.C160);
                        str = content.getName() + getDate(content);
                        str = TextUtil.shorten(str, g.getFontMetrics(), 215 - 90);
                        g.drawString(str, 90, 270);
                    }
                }
                g.translate(-addX, -addY);
            }
            g.drawImage(wallShadowTopImg, 0, 88, c);
            g.drawImage(wallShadowBottomImg, 0, 445, c);
            if (srcRec != null) { g.setClip(srcRec); }
            //focus
            addX = (ui.photoFocus % MusicListUI.IMAGES_PER_LINE) * 164;
            addY = ((ui.photoFocus - num[0]) / MusicListUI.IMAGES_PER_LINE) * 177;
            g.translate(addX, addY);
            g.drawImage(wallFocusImg, 78, 108, c);
            if (ui.photoFocus % MusicListUI.IMAGES_PER_LINE != 0) { g.drawImage(arrLImg, 67, 167, c); }
            if ((ui.photoFocus + 1) % MusicListUI.IMAGES_PER_LINE != 0 && ui.photoFocus != totalCount - 1) {
                g.drawImage(arrRImg, 221, 167, c);
            }
            if (ui.photoFocus - MusicListUI.IMAGES_PER_LINE >= 0) { g.drawImage(arrTImg, 141, 93, c); }
            //if (ui.photoFocus + MusicListUI.IMAGES_PER_LINE < totalCount) {
            if (!ui.isLastLineForGrid(totalCount, ui.photoFocus, MusicListUI.IMAGES_PER_LINE)) {
                int posY = 245;
                if (ui.photoFocus != 0) {
                    posY = 271;
                    if (ui.uiType == MusicListUI.UI_ARTIST_ALBUM) { posY = 261; }
                }
                g.drawImage(arrBImg, 141, posY, c);
            }
            g.setFont(Rs.F16);
            g.setColor(Rs.C255203000);
            /*Content focusContent = (Content) list.elementAt(ui.photoFocus);
            if (focusContent != null) {
                //String tempStr = focusContent.getArtist();
                //if (ui.uiType == MusicListUI.UI_ARTIST_ALBUM) { tempStr = focusContent.getName(); }
                //str = TextUtil.shorten(tempStr, g.getFontMetrics(), 215 - 90);
                //GraphicUtil.drawStringCenter(g, str, 154, 260);
                if (ui.uiType == MusicListUI.UI_ALBUM) {
                    str = TextUtil.shorten(focusContent.getArtist(), g.getFontMetrics(), 215 - 90);
                    GraphicUtil.drawStringCenter(g, str, 154, 260);
                    //str = TextUtil.shorten(focusContent.getName(), g.getFontMetrics(), 215 - 90);
                    //GraphicUtil.drawStringCenter(g, str, 154, 272);
                }
            }*/
            if (ui.photoFocus > 0) {
                DVBGraphics dvbGraphics = (DVBGraphics) g;
                DVBAlphaComposite baseComposite = DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
                try {
                    dvbGraphics.setDVBComposite(DVBAlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.85f));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                g.drawImage(wallPlayIconImg, 130, 206, c);
                try { dvbGraphics.setDVBComposite(baseComposite); } catch (Exception e) { e.printStackTrace(); }
            }
            g.translate(-addX, -addY);
            //title
            g.drawImage(titleGlowImg, 20, 70, c);
            /*if (ui.uiType == MusicListUI.UI_ALBUM) {
                str = contentTxt[1] + " (" + (list.size() - 1) + ")";
            } else { str = contentTxt[4] + " : " + ui.currentRoot.getName() + " (" + (list.size() - 1) + ")"; }*/
            g.setFont(Rs.F22);
            str = contentTxt[4] + " : " + ui.currentRoot.getName();
            String numTxt = " (" + (list.size() - 1) + ")";
            if (ui.uiType == MusicListUI.UI_ALBUM) { str = contentTxt[1]; }
            str = getTitle(str, numTxt, g, 712 - 54);
            g.setColor(Rs.C227227228);
            g.drawString(str, 54, 95);

            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringRight(g, sortTxt[ui.sortType], 909, 95);
            g.drawImage(hotkeybgImg, 236, 466, c);
        } else {
            paintList(g, c);
        }
    }

    /**
     * Graphics paint for list.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    private void paintList(Graphics g, UIComponent c) {
        MusicListUI ui = (MusicListUI) c;
        Vector list = ui.currentRoot.getChildren();
        int totalCount = 0;
        if (list != null) { totalCount = list.size(); }
        String str = "";
        g.drawImage(hotkeybgImg, 236, 466, c);
        g.drawImage(listBgImg, 10, 137, c);
        if (ui.uiType == MusicListUI.UI_NOPLAYLIST) {
            str = playlistTxt + " (0)";
            g.setFont(Rs.F20);
            g.setColor(Rs.C046046045);
            g.drawString(str, 71, 162);
            g.setColor(Rs.C214182061);
            g.drawString(str, 70, 161);

            g.setFont(Rs.F22);
            g.setColor(Rs.C230);
            GraphicUtil.drawStringCenter(g, noPlaylistTitleTxt, 310, 261);
            /*g.setFont(Rs.F19);
            String[] desc = TextUtil.split(noPlaylistSubTitleTxt, g.getFontMetrics(), 540 - 94);
            for (int i = 0; i < desc.length; i++) {
                GraphicUtil.drawStringCenter(g, desc[i], 310, 289 + (i * 18));
            }*/
            g.setFont(Rs.F17);
            g.setColor(Rs.C150);
            String[] desc = TextUtil.split(noPlaylistDesc, g.getFontMetrics(), 540 - 94);
            for (int i = 0; i < desc.length; i++) {
                g.drawString(desc[i], 94, 336 + (i * 18));
            }
            return;
        }
        //list
        int[] num = BasicUI.getInitLastNumber(ui.photoFocus, ui.focusPosition, totalCount,
                MusicListUI.ROWS_PER_PAGE);
        int lineGap = 32;
        int addY = 0;
        for (int i = num[0], j = 0; i <= num[1]; i++, j++) {
            addY = (j * lineGap);
            g.translate(0, addY);
            if (j < MusicListUI.ROWS_PER_PAGE - 1) { g.drawImage(listLineImg, 70, 208, c); }
            if (i != ui.photoFocus) { paintListItem(g, c, false, i); }
            g.translate(-0, -addY);
        }
        if (num[0] > 0) {
            g.drawImage(listShadowTopImg, 54, 173, c);
            g.drawImage(arrTImg, 308, 166, c);
        }
        if (num[1] < totalCount - 1) {
            g.drawImage(listShadowBottomImg, 54, 406, c);
            g.drawImage(arrBImg, 308, 457, c);
        }
        //focus
        addY = (ui.focusPosition * lineGap);
        g.translate(0, addY);
        g.drawImage(listFocusImg, 53, 174, c);
        paintListItem(g, c, true, ui.photoFocus);
        g.translate(-0, -addY);
        //text
        if (ui.enableSort()) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C227227228);
            GraphicUtil.drawStringRight(g, sortTxt[ui.sortType], 584, 131);
        }
        Content focusContent = (Content) list.elementAt(ui.photoFocus);
        //Content lastContent = (Content) list.elementAt(list.size() - 1);
        Content tempContent = focusContent;
        if (tempContent == null) { tempContent = (Content) list.elementAt(ui.photoFocus + 1); }
        Content parent = tempContent.getParent();
        String artistName = tempContent.getArtist();
        int totalNum = list.size() - 1;
        if (ui.uiType == MusicListUI.UI_ALBUM_SONGS) {
            str = albumTxt2 + " : " + tempContent.getAlbumName();
        } else if (ui.uiType == MusicListUI.UI_ALL_SONGS) {
            str = songsTxt;
        } else if (ui.uiType == MusicListUI.UI_ARTIST) {
            str = artistsTxt;
            totalNum = list.size();
        } else if (ui.uiType == MusicListUI.UI_ARTIST_SONGS) {
            str = artistTxt + " : " + artistName;
        } else if (ui.uiType == MusicListUI.UI_ARTIST_ALBUM_SONGS) {
            str = artistName + " : " + tempContent.getAlbumName();
        } else if (ui.uiType == MusicListUI.UI_ARTIST_ALL_SONGS) {
            str = artistName + " : " + allSongsTxt;
        } else if (ui.uiType == MusicListUI.UI_GENRE) {
            str = genresTxt;
            totalNum = list.size();
        } else if (ui.uiType == MusicListUI.UI_GENRE_SONGS) {
            //str = genreTxt + " : " + tempContent.getGenre();
            str = genreTxt + " : " + parent.getName();
        } else if (ui.uiType == MusicListUI.UI_PLAYLIST) {
            str = playlistTxt;
            if (list.size() > 1) { str = playlistsTxt; }
            totalNum = list.size();
        } else if (ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
            str = playlistTxt + " : " + parent.getName();
        }
        g.setFont(Rs.F20);
        str = getShorten(str, totalNum, g, 566 - 70);
        g.setColor(Rs.C046046045);
        g.drawString(str, 71, 162);
        g.setColor(Rs.C214182061);
        g.drawString(str, 70, 161);
        //album
        g.drawImage(listAlbumBgImg, 606, 105, c);
        Image img = null;
        if (focusContent != null) { img = focusContent.getIconImage(); }
        if (img == null) {
            g.drawImage(wallDefaultImg, 726, 199, 59, 84, c);
        } else {
            int posX = 653 + ((203 - img.getWidth(null)) / 2);
            int posY = 140 + ((203 - img.getHeight(null)) / 2);
            g.drawImage(img, posX, posY, c);
        }
        g.drawImage(listFloderGlowImg, 653, 140, c);
        g.drawImage(listPlayIconDimImg, 725, 395, c);

        g.setFont(Rs.F20);
        g.setColor(Rs.C255203000);
        str = tempContent.getArtist();
        if (ui.uiType == MusicListUI.UI_GENRE) {
            str = focusContent.getGenre();
        } else if (ui.uiType == MusicListUI.UI_PLAYLIST) {
            str = focusContent.getName();
        } else if (ui.uiType == MusicListUI.UI_ALL_SONGS || ui.uiType == MusicListUI.UI_GENRE_SONGS
                || ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
            str = "";
            if (focusContent != null) { str = focusContent.getArtist(); }
        } else if (ui.uiType == MusicListUI.UI_ARTIST) {
            str = focusContent.getName();
        }
        GraphicUtil.drawStringCenter(g, getShorten(str, g, 310), 755, 368);

        str = tempContent.getAlbumName() + getDate(tempContent);
        if (ui.uiType == MusicListUI.UI_ARTIST) {
            int count = focusContent.getTotalCount();
            String txt = albumTxt;
            if (count > 1) { txt = albumsSmallTxt; }
            str = count + " " + txt;
            count = focusContent.getSongsCount();
            if (count > 0) {
                txt = songTxt;
                if (count > 1) { txt = songsSmallTxt; }
                str += ", " + count + " " + txt;
            }
        } else if (ui.uiType == MusicListUI.UI_GENRE || ui.uiType == MusicListUI.UI_PLAYLIST) {
            int count = focusContent.getTotalCount();
            String txt = songTxt;
            if (count > 1) { txt = songsSmallTxt; }
            str = count + " " + txt;
        } else if (ui.uiType == MusicListUI.UI_ARTIST_ALL_SONGS || ui.uiType == MusicListUI.UI_ALL_SONGS
                || ui.uiType == MusicListUI.UI_GENRE_SONGS || ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
            str = "";
            if (focusContent != null) { str = focusContent.getAlbumName() + getDate(focusContent); }
        }
        GraphicUtil.drawStringCenter(g, getShorten(str, g, 310), 756, 388);
    }
    
    /**
     * Paint list item.
     *
     * @param g the Graphics
     * @param c the UIComponent
     * @param isFocus the is focus
     * @param index the index
     */
    private void paintListItem(Graphics g, UIComponent c, boolean isFocus, int index) {
        Image randomIcon = listRandomIconImg;
        if (isFocus) {
            g.setFont(Rs.F19);
            g.setColor(Rs.C0);
            randomIcon = listRandomIconFocusImg;
        } else {
            g.setFont(Rs.F18);
            g.setColor(Rs.C230);
        }
        MusicListUI ui = (MusicListUI) c;
        Content content = (Content) ui.currentRoot.getChildren().elementAt(index);
        if (content == null) {
            if (index == 0 && ui.isTempNode(ui.uiType)) {
                g.drawImage(randomIcon, 65, 182, c);
                g.drawString(randomTxt, 99, 197);
            } else { return; }            
        } else {
            if (!isFocus) {
                String str = content.getName();
                int width = 510 - 69;
                if (ui.uiType == MusicListUI.UI_ALBUM_SONGS || ui.uiType == MusicListUI.UI_ALL_SONGS
                        || ui.uiType == MusicListUI.UI_ARTIST_SONGS || ui.uiType == MusicListUI.UI_ARTIST_ALBUM_SONGS
                        || ui.uiType == MusicListUI.UI_ARTIST_ALL_SONGS || ui.uiType == MusicListUI.UI_GENRE_SONGS
                        || ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
                    str = "";
                    if (index < 10) { str = "0"; }
                    str += (index) + "." + "  " + content.getName();
                    //str = content.getTrackNum() + "." + "  " + content.getName();
                    if (ui.uiType == MusicListUI.UI_ALL_SONGS || ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
                        width = 320 - 69;
                    } else if (ui.uiType == MusicListUI.UI_GENRE_SONGS) { width = 421 - 69 - 5; }
                } else if (ui.uiType == MusicListUI.UI_PLAYLIST) {
                    str = getName(content, g, width);
                }
                g.drawString(getShorten(str, g, width), 69, 197);
                g.setFont(Rs.F17);
                g.setColor(Rs.C189);
            }
            if (ui.uiType == MusicListUI.UI_ALL_SONGS || ui.uiType == MusicListUI.UI_PLAYLIST_SONGS) {
                g.drawString(getShorten(content.getArtist(), g, 436 - 329), 329, 198);
                g.drawString(getShorten(content.getGenre(), g, 510 - 442), 442, 198);
            } else if (ui.uiType == MusicListUI.UI_GENRE_SONGS) {
                g.drawString(getShorten(content.getArtist(), g, 510 - 423), 423, 198);
            }
            if (ui.uiType == MusicListUI.UI_ARTIST || ui.uiType == MusicListUI.UI_GENRE) {
                if (ui.uiType == MusicListUI.UI_ARTIST) {
                    int songsCount = content.getSongsCount();
                    if (songsCount > 0) { GraphicUtil.drawStringCenter(g, "(" + songsCount + ")", 545, 197); }
                } else { GraphicUtil.drawStringCenter(g, "(" + content.getTotalCount() + ")", 545, 197); }
            } else {
                if (ui.uiType == MusicListUI.UI_PLAYLIST && content.getDuration() <= 0) { return; }
                GraphicUtil.drawStringRight(g, getDurationTxt(content.getDuration()), 566, 198);
            }
        }
    }
    
    /**
     * Gets the name.
     *
     * @param content the Content
     * @param g the Graphics
     * @param width the width for limit
     * @return the name
     */
    private String getName(Content content, Graphics g, int width) {
        if (content == null) { return ""; }
        int numWidth = 0;
        String numTxt = "";
        //if (content.getChildren() != null) {
        if (content.getTotalCount() > 0) {
            numTxt = " (" + content.getTotalCount() + ")";
            numWidth = g.getFontMetrics().stringWidth(numTxt);
        }
        return TextUtil.shorten(content.getName(), g.getFontMetrics(), width - numWidth) + numTxt;
    }

    /**
     * Gets the shorten.
     *
     * @param str the str
     * @param totalCount the total count
     * @param g the g
     * @param width the width
     * @return the shorten String
     */
    private String getShorten(String str, int totalCount, Graphics g, int width) {
        int numWidth = 0;
        String numTxt = " (" + totalCount + ")";
        numWidth = g.getFontMetrics().stringWidth(numTxt);
        return TextUtil.shorten(str, g.getFontMetrics(), width - numWidth) + numTxt;
    }

    /**
     * Gets the shorten.
     *
     * @param str the str
     * @param g the g
     * @param width the width
     * @return the shorten String
     */
    private String getShorten(String str, Graphics g, int width) {
        return TextUtil.shorten(str, g.getFontMetrics(), width);
    }

    /**
     * Gets the duration txt.
     *
     * @param seconds the seconds
     * @return the duration txt
     */
    private String getDurationTxt(long seconds) {
        long hour = seconds / 3600;
        long remain = seconds % 3600;
        long min = remain / 60;
        long sec = remain % 60;
        String str = "";
        if (hour > 0) {
            str += hour + ":";
            if (min < 10) { str += "0"; }
        }
        str += min + ":";
        if (sec < 10) { str += "0"; }
        str += sec;
        return str;
    }
    
    /**
     * Gets the album name.
     *
     * @param content the content
     * @return the album name
     */
    public String getDate(Content content) {
        if (content == null) { return ""; }
        String date = content.getDate();
        if (date != null && date.length() > 0) {
            if (date.length() > 4) { date = date.substring(0, 4); }
            return " (" + date + ")";
        }
        return "";
    }
}

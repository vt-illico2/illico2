/*
 *  @(#)ContentListRenderer.java 1.0 2011.06.08
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.ContentListUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ContentListRenderer</code> The class to paint a GUI for list.
 *
 * @since   2011.06.08
 * @version $Revision: 1.12 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class ContentListRenderer extends BasicRenderer {
    /** The clip rectangle for wall UI. */
    private final Rectangle clipRec = new Rectangle(0, 0, 960, 480);

    /** The wall photo high img. */
    private Image wallPhotoHighImg = null;
    /** The wall folder img. */
    private Image wallFolderImg = null;
    /** The wall default img. */
    private Image wallDefaultImg = null;
    /** The wall photo shadow img. */
    private Image wallPhotoShadowImg = null;
    /** The wall folder stroke img. */
    private Image wallFolderStrokeImg = null;
    /** The wall shadow top img. */
    private Image wallShadowTopImg = null;
    /** The wall shadow bottom img. */
    private Image wallShadowBottomImg = null;
    /** The wall focus img. */
    private Image wallFocusImg = null;
    /** The arr left img. */
    private Image arrLImg = null;
    /** The arr right img. */
    private Image arrRImg = null;
    /** The arr top img. */
    private Image arrTImg = null;
    /** The arr bottom img. */
    private Image arrBImg = null;
    /** The title glow img. */
    private Image titleGlowImg = null;

    /** The list bg img. */
    private Image listBgImg = null;
    /** The list line img. */
    private Image listLineImg = null;
    /** The list shadow top img. */
    private Image listShadowTopImg = null;
    /** The list shadow bottom img. */
    private Image listShadowBottomImg = null;
    /** The list focus img. */
    private Image listFocusImg = null;
    /** The list album bg img. */
    private Image listAlbumBgImg = null;
    /** The list default img. */
    private Image listDefaultImg = null;
    /** The list folder img. */
    private Image listFolderImg = null;
    /** The list folder stroke img. */
    private Image listFolderStrokeImg = null;
    /** The list floder glow img. */
    private Image listFloderGlowImg = null;

    /** The wall folder film img. */
    private Image wallFolderFilmImg = null;
    /** The wall film img. */
    private Image wallFilmImg = null;
    /** The wall default film img. */
    private Image wallDefaultFilmImg = null;
    /** The wall default video img. */
    private Image wallDefaultVideoImg = null;
    /** The list default video img. */
    private Image listDefaultVideoImg = null;
    /** The list folder film img. */
    private Image listFolderFilmImg = null;
    /** The list default film img. */
    private Image listDefaultFilmImg = null;
    /** The list film img. */
    private Image listFilmImg = null;

    /** The content txt. */
    private String[] contentTxt = new String[3];
    /** The sort txt. */
    private String[] sortTxt = new String[4];

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        wallPhotoHighImg = getImage("09_high_133.png");
        wallFolderImg = getImage("09_al_fol.png");
        wallDefaultImg = getImage("09_ph_default.png");
        wallPhotoShadowImg = getImage("09_sh_133.png");
        wallFolderStrokeImg = getImage("09_al_stroke.png");
        wallShadowTopImg = getImage("09_ph_sha_t.png");
        wallShadowBottomImg = getImage("09_ph_sha_b.png");
        wallFocusImg = getImage("09_focus_133.png");
        titleGlowImg = getImage("07_wizard_glow.png");
        arrTImg = getImage("02_ars_t.png");
        arrBImg = getImage("02_ars_b.png");
        arrLImg = getImage("02_ars_l.png");
        arrRImg = getImage("02_ars_r.png");
        listBgImg = getImage("09_list_bg.png");
        listLineImg = getImage("06_listline.png");
        listShadowTopImg = getImage("06_list_sha_t.png");
        listShadowBottomImg = getImage("06_list_sha.png");
        listFocusImg = getImage("06_list_foc.png");
        listAlbumBgImg = getImage("09_album_bg.png");
        listDefaultImg = getImage("09_ph_default_b.png");
        listFolderImg = getImage("09_al_fol_b.png");
        listFolderStrokeImg = getImage("09_al_stroke_b.png");
        listFloderGlowImg = getImage("09_al_folder_glow.png");

        wallFolderFilmImg = getImage("09_vid_film_f.png");
        wallFilmImg = getImage("09_vid_film.png");
        wallDefaultFilmImg = getImage("09_vid_default.png");
        wallDefaultVideoImg = getImage("09_vid_default_f.png");
        listDefaultVideoImg = getImage("09_vid_default_f_b.png");
        listFolderFilmImg = getImage("09_vid_film_s.png");
        listDefaultFilmImg = getImage("09_vid_default_b.png");
        listFilmImg = getImage("09_vid_film_b.png");
        super.prepare(c);
        contentTxt[0] = getString("photos");
        contentTxt[1] = getString("music");
        contentTxt[2] = getString("videos");
        sortTxt[0] = getString("sortByAlpaA");
        sortTxt[1] = getString("sortByAlpaZ");
        sortTxt[2] = getString("sortByNewest");
        sortTxt[3] = getString("sortByOldest");
    }

    /**
     * Reset location title.
     *
     * @param menuFocus the menu focus
     */
    public void resetLocationTitle(int menuFocus) {
        locTitles = new String[] {locTitleTxt, contentTxt[menuFocus]};
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        ContentListUI ui = (ContentListUI) c;
        if (ui.currentNode == null) { return; }
        Vector list = ui.currentNode.getChildren();
        int totalCount = list.size();
        String str = "";
        if (ui.uiType == ContentListUI.UI_WALL) {
            Rectangle srcRec = g.getClipBounds();
            if (srcRec != null) {
                g.setClip(srcRec.intersection(clipRec));
            } else { g.setClip(clipRec); }
            //Rectangle oldRec = g.getClipBounds();
            //g.setClip(clipRec);
            int[] num = BasicUI.getInitLastNumberForWall(ui.photoFocus, ui.focusPosition, totalCount,
                    ContentListUI.IMAGES_PER_PAGE);
            g.setFont(Rs.F16);
            int addX = 0;
            int addY = 0;
            for (int i = num[0]; i <= num[1]; i++) {
                Content content = (Content) list.elementAt(i);
                //if (content == null) { continue; }
                //Logger.debug(this, "paint()-name : " + content.getName());
                Image img = null; //content.getIconImage();
                if (content != null) { img = content.getIconImage(); }
                addX = (i % ContentListUI.IMAGES_PER_LINE) * 164;
                addY = ((i - num[0]) / ContentListUI.IMAGES_PER_LINE) * 177;
                g.translate(addX, addY);
                g.setColor(Rs.DVB90C22);
                g.fillRect(86, 111, 133, 133);
                g.drawImage(wallPhotoShadowImg, 86, 244, c);
                if (content != null && content.isFolder()) {
                    g.drawImage(wallFolderImg, 96, 132, c);
                    if (img == null) {
                        if (ui.menuFocus == BasicUI.MENU_PHOTO) {
                            g.drawImage(wallDefaultImg, 86 + 37, 167, c);
                        } else if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                            g.drawImage(wallDefaultVideoImg, 86 + 44, 167, c);
                        }
                    } else {
                        g.setColor(Rs.C35);
                        g.fillRect(110, 159, 86, 51);
                        int posX = 110 + ((86 - img.getWidth(null)) / 2);
                        int posY = 159 + ((51 - img.getHeight(null)) / 2);
                        g.drawImage(img, posX, posY, c);
                        if (ui.menuFocus == BasicUI.MENU_VIDEO) { g.drawImage(wallFolderFilmImg, 115, 159, c); }
                        g.drawImage(wallFolderStrokeImg, 110, 159, c);
                    }
                } else {
                    if (img == null) {
                        if (ui.menuFocus == BasicUI.MENU_PHOTO) {
                            g.drawImage(wallDefaultImg, 86 + 38, 159, c);
                        } else if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                            g.drawImage(wallDefaultFilmImg, 86 + 7, 112, c);
                        }
                    } else {
                        int imgWidth = img.getWidth(null);
                        int imgHeight = img.getHeight(null);
                        int posX = 86 + ((133 - imgWidth) / 2);
                        int posY = 111 + ((133 - imgHeight) / 2);
                        g.drawImage(img, posX, posY, c);
                        if (ui.menuFocus == BasicUI.MENU_VIDEO) { g.drawImage(wallFilmImg, 95, 113, c); }
                    }
                }
                g.drawImage(wallPhotoHighImg, 86, 111, c);
                if (content != null) {
                    if (content.isFolder()) {
                        str = getName(content, g, 215 - 90);
                    } else { str = TextUtil.shorten(content.getName(), g.getFontMetrics(), 215 - 90); }
                    if (i != ui.photoFocus) {
                        g.setColor(Rs.C200);
                        g.drawString(str, 90, 259);
                        if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                            String date = content.getDate();
                            if (date != null) { g.drawString(content.getDate(), 90, 259 + 14); }
                        }
                    }
                }
                g.translate(-addX, -addY);
            }
            g.drawImage(wallShadowTopImg, 0, 88, c);
            g.drawImage(wallShadowBottomImg, 0, 445, c);
            g.setClip(srcRec);
            //focus
            addX = (ui.photoFocus % ContentListUI.IMAGES_PER_LINE) * 164;
            addY = ((ui.photoFocus - num[0]) / ContentListUI.IMAGES_PER_LINE) * 177;
            g.translate(addX, addY);
            g.drawImage(wallFocusImg, 78, 108, c);
            if (ui.photoFocus % ContentListUI.IMAGES_PER_LINE != 0) { g.drawImage(arrLImg, 67, 167, c); }
            if ((ui.photoFocus + 1) % ContentListUI.IMAGES_PER_LINE != 0 && ui.photoFocus != totalCount - 1) {
                g.drawImage(arrRImg, 221, 167, c);
            }
            if (ui.photoFocus - ContentListUI.IMAGES_PER_LINE >= 0) { g.drawImage(arrTImg, 141, 93, c); }
            //if (ui.photoFocus + ContentListUI.IMAGES_PER_LINE < totalCount) {
            if (!ui.isLastLineForGrid(totalCount, ui.photoFocus, ContentListUI.IMAGES_PER_LINE)) {
                int posY = 263;
                if (ui.menuFocus == BasicUI.MENU_VIDEO) { posY = 273; }
                g.drawImage(arrBImg, 141, posY, c);
            }
            g.setColor(Rs.C255203000);
            Content focusContent = (Content) list.elementAt(ui.photoFocus);
            if (focusContent != null) {
                if (focusContent.isFolder()) {
                    str = getName(focusContent, g, 215 - 90);
                } else { str = TextUtil.shorten(focusContent.getName(), g.getFontMetrics(), 215 - 90); }
                //GraphicUtil.drawStringCenter(g, str, 154, 261);
                if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                    GraphicUtil.drawStringCenter(g, focusContent.getDate(), 154, 261 + 14);
                }
            }
            g.translate(-addX, -addY);
            //title
            g.drawImage(titleGlowImg, 20, 70, c);
            /*if (ui.currentNode == BasicUI.contentRoot[ui.menuFocus]) {
                str = contentTxt[ui.menuFocus] + " (" + BasicUI.contentRoot[ui.menuFocus].getTotalCount() + ")";
            } else { str = ui.currentNode.getName() + " (" + totalCount + ")"; }*/
            g.setFont(Rs.F22);
            str = ui.currentNode.getName();
            String numTxt = " (" + totalCount + ")";
            if (ui.currentNode.getId() == BasicUI.contentRoot[ui.menuFocus].getId()) {
                str = contentTxt[ui.menuFocus];
                numTxt = " (" + BasicUI.contentRoot[ui.menuFocus].getTotalCount() + ")";
            }
            str = getTitle(str, numTxt, g, 712 - 54);
            g.setColor(Rs.C227227228);
            g.drawString(str, 54, 95);

            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringRight(g, sortTxt[ui.sortType], 909, 95);
            g.drawImage(hotkeybgImg, 236, 466, c);
        } else {
            paintList(g, c);
        }
    }

    /**
     * Graphics paint for list.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    private void paintList(Graphics g, UIComponent c) {
        ContentListUI ui = (ContentListUI) c;
        Vector list = ui.currentNode.getChildren();
        int totalCount = list.size();
        String str = "";
        g.drawImage(hotkeybgImg, 236, 466, c);
        g.drawImage(listBgImg, 10, 137, c);
        //list
        int[] num = BasicUI.getInitLastNumber(ui.photoFocus, ui.focusPosition, totalCount,
                ContentListUI.ROWS_PER_PAGE);
        g.setFont(Rs.F18);
        g.setColor(Rs.C230);
        int lineGap = 32;
        int addY = 0;
        for (int i = num[0], j = 0; i <= num[1]; i++, j++) {
            addY = (j * lineGap);
            g.translate(0, addY);
            if (j < ContentListUI.ROWS_PER_PAGE - 1) { g.drawImage(listLineImg, 70, 208, c); }
            if (i != ui.photoFocus) {
                Content content = (Content) list.elementAt(i);
                if (content == null) {
                    g.translate(-0, -addY);
                    continue;
                }
                g.drawString(getName(content, g, 474 - 69), 69, 197);
                String date = content.getDate();
                if (date != null) { GraphicUtil.drawStringRight(g, date, 567, 197); }
            }
            g.translate(-0, -addY);
        }
        if (num[0] > 0) {
            g.drawImage(listShadowTopImg, 54, 173, c);
            g.drawImage(arrTImg, 308, 166, c);
        }
        if (num[1] < totalCount - 1) {
            g.drawImage(listShadowBottomImg, 54, 406, c);
            g.drawImage(arrBImg, 308, 457, c);
        }
        //focus
        addY = (ui.focusPosition * lineGap);
        g.translate(0, addY);
        Content focusContent = (Content) list.elementAt(ui.photoFocus);
        g.drawImage(listFocusImg, 53, 174, c);
        g.setFont(Rs.F19);
        g.setColor(Rs.C0);
        //g.drawString(getName(focusContent, g, 474 - 69), 69, 198);
        GraphicUtil.drawStringRight(g, focusContent.getDate(), 569, 198);
        g.translate(-0, -addY);
        //text
        g.setFont(Rs.F17);
        g.setColor(Rs.C227227228);
        GraphicUtil.drawStringRight(g, sortTxt[ui.sortType], 584, 131);
        //title
        /*if (ui.currentNode == BasicUI.contentRoot[ui.menuFocus]) {
            str = contentTxt[ui.menuFocus] + " (" + BasicUI.contentRoot[ui.menuFocus].getTotalCount() + ")";
        } else { str = ui.currentNode.getName() + " (" + totalCount + ")"; }*/
        g.setFont(Rs.F20);
        str = ui.currentNode.getName();
        String numTxt = " (" + totalCount + ")";
        if (ui.currentNode.getId() == BasicUI.contentRoot[ui.menuFocus].getId()) {
            str = contentTxt[ui.menuFocus];
            numTxt = " (" + BasicUI.contentRoot[ui.menuFocus].getTotalCount() + ")";
        }
        str = getTitle(str, numTxt, g, 569 - 70); 
        g.setColor(Rs.C046046045);
        g.drawString(str, 71, 162);
        g.setColor(Rs.C214182061);
        g.drawString(str, 70, 161);
        //album
        g.drawImage(listAlbumBgImg, 606, 105, c);
        //Image img = focusContent.getImage();
        Image img = focusContent.getIconImage();
        if (focusContent.isFolder()) {
            g.drawImage(listFolderImg, 680, 181, c);
            if (img == null) {
                if (ui.menuFocus == BasicUI.MENU_PHOTO) {
                    g.drawImage(listDefaultImg, 718, 230, c);
                } else if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                    g.drawImage(listDefaultVideoImg, 725, 230, c);
                }
            } else {
                g.setColor(Rs.C45);
                g.fillRect(697, 217, 116, 69);
                int posX = 697 + ((116 - img.getWidth(null)) / 2);
                int posY = 217 + ((69 - img.getHeight(null)) / 2);
                g.drawImage(img, posX, posY, c);
                if (ui.menuFocus == BasicUI.MENU_VIDEO) { g.drawImage(listFolderFilmImg, 705, 218, c); }
                g.drawImage(listFolderStrokeImg, 697, 217, c);
            }
            g.drawImage(listFloderGlowImg, 653, 140, c);
        } else {
            if (img == null) {
                if (ui.menuFocus == BasicUI.MENU_PHOTO) {
                    g.drawImage(listDefaultImg, 718, 223, c);
                } else if (ui.menuFocus == BasicUI.MENU_VIDEO) {
                    g.drawImage(listDefaultFilmImg, 663, 141, c);
                }
            } else {
                int posX = 653 + ((203 - img.getWidth(null)) / 2);
                int posY = 140 + ((203 - img.getHeight(null)) / 2);
                g.drawImage(img, posX, posY, c);
                if (ui.menuFocus == BasicUI.MENU_VIDEO) { g.drawImage(listFilmImg, 668, 141, c); }
            }
        }
        g.setFont(Rs.F20);
        g.setColor(Rs.C255203000);
        GraphicUtil.drawStringCenter(g, getName(focusContent, g, 310), 755, 368);
        GraphicUtil.drawStringCenter(g, focusContent.getDate(), 756, 389);
    }
    
    /**
     * Gets the name.
     *
     * @param content the Content
     * @param g the Graphics
     * @param width the width for limit
     * @return the name
     */
    private String getName(Content content, Graphics g, int width) {
        int numWidth = 0;
        String numTxt = "";
        if (content.getTotalCount() > 0) {
            numTxt = " (" + content.getTotalCount() + ")";
            numWidth = g.getFontMetrics().stringWidth(numTxt);
        }
        return TextUtil.shorten(content.getName(), g.getFontMetrics(), width - numWidth) + numTxt;
    }
}

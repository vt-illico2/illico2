/*
 *  @(#)VideoPlayRenderer.java 1.0 2011.07.22
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.ui.VideoPlayUI;

/**
 * <code>VideoPlayRenderer</code> The class to paint a GUI for VideoPlayUI.
 *
 * @since   2011.07.22
 * @version $Revision: 1.6 $ $Date: 2011/10/27 08:21:36 $
 * @author  tklee
 */
public class VideoPlayRenderer extends BasicRenderer {
    /** The feed img name. */
    private final String[] feedImgName = {"04_feed_rew_16.png", "04_feed_rew_8.png", "04_feed_rew_4.png",
            "04_feed_rew_2.png", "04_flip_st_04_foc_b.png", "04_flip_st_slow.png", "04_flip_st_05_foc_b.png",
            "04_feed_ff_2.png", "04_feed_ff_4.png", "04_feed_ff_8.png", "04_feed_ff_16.png",
            "04_flip_st_03_foc_b.png", "04_flip_st_01_foc_b.png", "04_flip_st_07_foc_b.png"};
    /** The feed back img. */
    private Image[] feedBackImg = new Image[feedImgName.length];
    /** The bg img. */
    private Image bgImg = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgImg = getImage("04_flip_st_foc_b01.png");
        for (int i = 0; i < feedImgName.length; i++) {
            feedBackImg[i] = getImage(feedImgName[i]);
        }
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        VideoPlayUI ui = (VideoPlayUI) c;
        if (ui.feedShowCount == -1) { return; }
        int feedFocus = ui.feedImgFocus;
        Logger.debug(this, "ui.feedImgFocus : " + feedFocus);
        if (feedFocus >= 0) {
            g.drawImage(bgImg, 429, 197, c);
            g.drawImage(feedBackImg[feedFocus], 451, 223, c);
        }
    }
}

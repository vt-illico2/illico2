/*
 *  @(#)InitRenderer.java 1.0 2011.05.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.ui.InitUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>InitRenderer</code> The class to paint a GUI for initialization.
 *
 * @since   2011.05.18
 * @version $Revision: 1.7 $ $Date: 2012/09/28 05:55:04 $
 * @author  tklee
 */
public class InitRenderer extends BasicRenderer {
    /** The no subs teaser img. */
    private Image noSubsTeaserImg = null;
    /** The focus btn img. */
    private Image focusBtnImg = null;
    /** The dim btn img. */
    private Image dimBtnImg = null;
    /** The tv bg img. */
    private Image tvBgImg = null;
    /** The icon question img. */
    private Image iconQuestionImg = null;
    /** The icon check img. */
    private Image iconCheckImg = null;
    /** The icon x img. */
    private Image iconXImg = null;
    /** The connected img. */
    private Image connectedImg = null;
    /** The search ani images. */
    public Image[] searchAniImage = new Image[6];

    /** The list title txt. */
    private String backBtnTxt = null;
    /** The search devices txt. */
    private String searchDevTxt = null;
    /** The connect device txt. */
    private String connectDevTxt = null;
    /** The connected device txt. */
    private String connectedDevTxt = null;
    /** The no device txt. */
    private String noDevTxt = null;
    /** The reason txt. */
    private String reasonTxt = null;
    /** The no dev desc txt. */
    private String[] noDevDescTxt = new String[4];
    /** The no dev btn txt. */
    public String[] noDevBtnTxt = new String[2];
    /** The no connect btn txt. */
    public String[] noConnectBtnTxt = new String[3];
    /** The btn cancel txt. */
    private String btnCancelTxt = null;
    /** The connect device descscription txt. */
    private String[] noAppNoteTxt = new String[2];
    /** The no connect txt. */
    private String noConnectTxt = null;
    /** The no connect desc txt. */
    private String[] noConnectDescTxt = new String[2];

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        //noSubsTeaserImg = getImage("teaser_img_ms.png");
        if (Config.language.equals(Definitions.LANGUAGE_FRENCH)) {
            noSubsTeaserImg = getImage("teaser_img_fr.png");
        } else { noSubsTeaserImg = getImage("teaser_img_en.png"); }
        focusBtnImg = getImage("05_focus.png");
        dimBtnImg = getImage("05_focus_dim.png");
        tvBgImg = getImage("09_pctotv_image.png");
        iconQuestionImg = getImage("09_icon_b_01.png");
        iconXImg = getImage("09_icon_b_02.png");
        iconCheckImg = getImage("09_icon_b_03.png");
        connectedImg = getImage("09_pro_finish.png");
        for (int i = 0; i < searchAniImage.length; i++) {
            searchAniImage[i] = getImage("09_pro_0" + (i + 1) + ".png");
        }
        super.prepare(c);

        backBtnTxt = getString("back");
        searchDevTxt = getString("searchDevices");
        connectDevTxt = getString("connectTo");
        connectedDevTxt = getString("connectedTo");
        noDevTxt = getString("noDevice");
        reasonTxt = getString("possibleReason");
        noDevDescTxt = TextUtil.tokenize(getString("noDeviceDesc"), "^");

        noDevBtnTxt[0] = getString("btnRetry");
        btnCancelTxt = getString("btnCancel");
        noDevBtnTxt[1] = btnCancelTxt;
        noConnectBtnTxt[0] = getString("btnRetry");
        noConnectBtnTxt[1] = btnCancelTxt;
        noConnectBtnTxt[2] = getString("btnChangeDev");
        noAppNoteTxt = TextUtil.tokenize(getString("noAppNote"), "^");
        noConnectTxt = getString("noConnection");
        String desc = getString("noConnectDesc");
        int index = desc.indexOf("^");
        if (index != -1) {
            noConnectDescTxt[0] = desc.substring(0, index);
            noConnectDescTxt[1] = desc.substring(index + 1);
        } else {
            noConnectDescTxt[0] = desc;
            noConnectDescTxt[1] = "";
        }
        locTitles = new String[] {locTitleTxt};
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        InitUI ui = (InitUI) c;
        int uiType = ui.uiType;
        Logger.debug(this, "uiType : " + uiType);
        if (uiType == InitUI.UI_INIT) { return; }

        if (uiType == InitUI.UI_NO_SUBSCRIBED) {
            Image image = ui.teaserImage;
            if (image == null) { image = noSubsTeaserImg; }
            g.drawImage(image, 47, 129, c);
            g.drawImage(focusBtnImg, 483, 388, c);
            g.setFont(Rs.F18);
            g.setColor(Rs.C3);
            GraphicUtil.drawStringCenter(g, backBtnTxt, 562, 410);
        } else if (uiType == InitUI.UI_DEVICE_SEARCH) {
            g.drawImage(tvBgImg, 17, 56, c);
            g.drawImage(searchAniImage[ui.searchImgNum], 423, 136, c);
            g.setFont(Rs.F35);
            g.setColor(Rs.C045044044);
            GraphicUtil.drawStringCenter(g, searchDevTxt, 481, 304);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, searchDevTxt, 480, 303);
        } else if (uiType == InitUI.UI_NO_SEARCH) {
            g.drawImage(tvBgImg, 17, 56, c);
            g.drawImage(searchAniImage[0], 423, 136, c);
            g.setFont(Rs.F35);
            g.setColor(Rs.C045044044);
            GraphicUtil.drawStringCenter(g, noDevTxt, 508, 304);
            g.setColor(Rs.C255);
            int xPos = GraphicUtil.drawStringCenter(g, noDevTxt, 507, 303);
            g.drawImage(iconQuestionImg, xPos - (389 - 341), 270, c);
            paintNoDevTxt(g, ui);
            paintButtons(g, ui, noDevBtnTxt);
        } else if (uiType == InitUI.UI_DEVICE_CONNECTING) {
            g.drawImage(tvBgImg, 17, 56, c);
            g.drawImage(searchAniImage[ui.searchImgNum], 423, 136, c);
            g.setFont(Rs.F35);
            String str = connectDevTxt;
            g.setColor(Rs.C045044044);
            GraphicUtil.drawStringCenter(g, str, 481, 304);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, str, 480, 303);

            str = "'" + TextUtil.shorten(ui.connectDevice.getDeviceName(), g.getFontMetrics(), 862 - 98) + "'";
            g.setColor(Rs.C045044044);
            GraphicUtil.drawStringCenter(g, str, 481, 346);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, str, 480, 345);
            //paintNoAppTxt(g, ui);

            g.setFont(Rs.F18);
            g.setColor(Rs.C3);
            g.drawImage(focusBtnImg, 400, 441, c);
            GraphicUtil.drawStringCenter(g, btnCancelTxt, 479, 463);
        } else if (uiType == InitUI.UI_DEVICE_CONNECTED) {
            g.drawImage(tvBgImg, 17, 56, c);
            g.drawImage(connectedImg, 423, 136, c);
            String str = connectedDevTxt;
            g.setFont(Rs.F35);
            g.setColor(Rs.C045044044);
            //GraphicUtil.drawStringCenter(g, str, 506, 304);
            g.drawString(str, 404, 304);
            g.setColor(Rs.C255);
            //int xPos = GraphicUtil.drawStringCenter(g, str, 505, 303);
            //g.drawImage(iconCheckImg, xPos - (303 - 251), 270, c);
            g.drawString(str, 403, 303);
            g.drawImage(iconCheckImg, 352, 270, c);

            str = "'" + TextUtil.shorten(ui.connectDevice.getDeviceName(), g.getFontMetrics(), 862 - 98) + "'";
            g.setColor(Rs.C045044044);
            GraphicUtil.drawStringCenter(g, str, 481, 346);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, str, 480, 345);

            paintNoAppTxt(g, ui);
        } else if (uiType == InitUI.UI_NO_CONNECT) {
            g.drawImage(tvBgImg, 17, 56, c);
            g.drawImage(searchAniImage[0], 423, 136, c);
            g.setFont(Rs.F35);
            g.setColor(Rs.C045044044);
            String str = noConnectTxt + " '" + ui.connectDevice.getDeviceName() + "'";
            str = TextUtil.shorten(str, g.getFontMetrics(), 900 - 100);
            GraphicUtil.drawStringCenter(g, str, 508, 304);
            g.setColor(Rs.C255);
            int xPos = GraphicUtil.drawStringCenter(g, str, 507, 303);
            g.drawImage(iconXImg, xPos - (216 - 162), 274, c);
            paintNoConnectDevTxt(g, ui);
            paintButtons(g, ui, noConnectBtnTxt);
        }
    }

    /**
     * Graphics paint text for no application on server.
     *
     * @param g the Graphics
     * @param ui the InitUI
     */
    private void paintNoAppTxt(Graphics g, InitUI ui) {
        if (!ui.connectDevice.isAppInstalled()) {
            g.setFont(Rs.F19);
            for (int i = 0; i < noAppNoteTxt.length; i++) {
                int addPos = i * 19;
                GraphicUtil.drawStringCenter(g, noAppNoteTxt[i], 480, 342 + addPos);
            }
        }
    }

    /**
     * Graphics paint text for no device.
     *
     * @param g the Graphics
     * @param ui the InitUI
     */
    private void paintNoDevTxt(Graphics g, InitUI ui) {
        g.setFont(Rs.F19);
        g.setColor(Rs.C255);
        g.drawString(reasonTxt, 299, 343);
        for (int i = 0; noDevDescTxt != null && i < noDevDescTxt.length; i++) {
            int addPos = i * 21;
            g.setColor(Rs.C255);
            g.drawString(noDevDescTxt[i], 322, 368 + addPos);
            g.setColor(Rs.C255213000);
            g.drawString(String.valueOf(i + 1), 299, 368 + addPos);
        }
    }

    /**
     * Graphics paint text for no device.
     *
     * @param g the Graphics
     * @param ui the InitUI
     */
    private void paintNoConnectDevTxt(Graphics g, InitUI ui) {
        g.setFont(Rs.F19);
        g.setColor(Rs.C255);
        g.drawString(reasonTxt, 228, 339);
        for (int i = 0; i < noConnectDescTxt.length; i++) {
            int basePos = i * 47;
            g.setFont(Rs.F19);
            g.setColor(Rs.C255);
            String[] desc = TextUtil.split(noConnectDescTxt[i], g.getFontMetrics(), 814 - 258);
            for (int j = 0; j < desc.length; j++) {
                int addPos = basePos + (j * 19);
                g.drawString(desc[j], 258, 363 + addPos);
            }

            g.setFont(Rs.F40);
            g.setColor(Rs.C255213000);
            g.drawString(String.valueOf(i + 1), 227, 377 + basePos);
        }
    }

    /**
     * Graphics paint buttons.
     *
     * @param g the Graphics
     * @param ui the InitUI
     * @param btnTxt the button txts
     */
    private void paintButtons(Graphics g, InitUI ui, String[] btnTxt) {
        g.setFont(Rs.F18);
        g.setColor(Rs.C3);
        for (int i = 0; i < btnTxt.length; i++) {
            int addPos = i * 164;
            if (btnTxt.length == 3) { addPos -= (320 - 235); }
            Image btnImg = dimBtnImg;
            if (i == ui.btnFocus) { btnImg = focusBtnImg; }
            g.drawImage(btnImg, 320 + addPos, 451, ui);
            GraphicUtil.drawStringCenter(g, btnTxt[i], 399 + addPos, 472);
            //g.drawString(btnTxt[i], 399 + addPos, 462);
        }
    }
}

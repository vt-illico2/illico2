/*
 *  @(#)BasicRenderer.java 1.0 2011.05.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.ContentListUI;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>BasicRenderer</code> This class the basic renderer class for each renderer.
 * Each renderder has to inherit this class as super class.
 * @since   2011.03.19
 * @version $Revision: 1.5 $ $Date: 2012/09/28 05:55:04 $
 * @author  tklee
 */
public class BasicRenderer extends Renderer {
    /** The bg img. */
    private static Image bgImg = null;
    /** The clock img. */
    private static Image clockImg = null;
    /** The logo img. */
    private static Image logoImg = null;
    /** The logo part img. */
    private static Image logoPartImg = null;
    /** The logo part dim img. */
    private static Image logoPartDimImg = null;
    /** The btn back img. */
    public static Image btnBackImg = null;
    /** The hotkeybg. */
    public static Image hotkeybgImg = null;
    /** The btn a img. */
    public static Image btnAImg = null;
    /** The btn b img. */
    public static Image btnBImg = null;
    /** The btn c img. */
    public static Image btnCImg = null;
    /** The btn d img. */
    public static Image btnDImg = null;
    /** The btn exit img. */
    public static Image btnExitImg = null;
    /** The btn search img. */
    public static Image btnSearchImg = null;
    /** The btn page img. */
    public static Image btnPageImg = null;
    /** The progress point img. */
    public static Image progressPointImg = null;
    /** The browse ani image. */
    //public static Image[] browseAniImage = new Image[6];

    /** The location title txt. */
    protected static String locTitleTxt = null;
    /** The btn back txt. */
    public static String btnBackTxt = null;
    /** The btn music player txt. */
    public static String btnMPlayerTxt = null;
    /** The btn options txt. */
    public static String btnOptTxt = null;
    /** The btn search txt. */
    public static String btnSearchTxt = null;
    /** The btn page txt. */
    public static String btnPageTxt = null;
    /** The btn view change txt. */
    public static String btnViewChangeTxt = null;
    /** The btn browse txt. */
    public static String btnBrowseTxt = null;
    /** The btn close txt. */
    public static String btnCloseTxt = null;
    /** The btn erase txt. */
    public static String btnEraseTxt = null;
    /** The of txt. */
    protected static String ofTxt = null;

    /** The location titles. */
    protected String[] locTitles = null;

    /**
     * Instantiates a new basic renderer.
     */
    public BasicRenderer() { }

    /**
     * Gets the string value of UI from DataCenter. If the value is null, return a empty string as "".
     *
     * @param key the key
     * @return the string
     */
    public static String getString(String key) {
        if (key == null) { return ""; }
        String value = DataCenter.getInstance().getString("renderer." + key);
        if (value != null) { return value; }
        return "";
    }

    /**
     * Gets the image from DataCenter.
     *
     * @param imageName the image name
     * @return the image
     */
    public static Image getImage(String imageName) {
        return DataCenter.getInstance().getImage(imageName);
    }

    /**
     * remove the image from DataCenter.
     *
     * @param imageName the image name
     */
    public void removeImage(String imageName) {
        DataCenter.getInstance().removeImage(imageName);
    }

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the UIComponent
     */
    public void prepare(UIComponent c) {
        hotkeybgImg = getImage("01_hotkeybg.png");
        //clockBgImg = getImage("clock_bg.png");
        bgImg = getImage("bg.jpg");
        clockImg = getImage("clock.png");
        logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        if (logoImg == null) { logoImg = getImage("logo_en.png"); }
        logoPartImg = getImage("his_over.png");
        logoPartDimImg = getImage("his_dim.png");
        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        if (imgTable != null && !imgTable.isEmpty()) {
            btnBackImg = (Image) imgTable.get(PreferenceService.BTN_BACK);
            btnAImg = (Image) imgTable.get(PreferenceService.BTN_A);
            btnBImg = (Image) imgTable.get(PreferenceService.BTN_B);
            btnCImg = (Image) imgTable.get(PreferenceService.BTN_C);
            btnDImg = (Image) imgTable.get(PreferenceService.BTN_D);
            btnExitImg = (Image) imgTable.get(PreferenceService.BTN_EXIT);
            btnPageImg = (Image) imgTable.get(PreferenceService.BTN_PAGE);
            btnSearchImg = (Image) imgTable.get(PreferenceService.BTN_SEARCH);
        } else {
            btnBackImg = getImage("b_btn_back.png");
            btnAImg = getImage("b_btn_a.png");
            btnBImg = getImage("b_btn_b.png");
            btnCImg = getImage("btn_c.png");
            btnDImg = getImage("b_btn_d.png");
            btnExitImg = getImage("b_btn_exit.png");
            btnPageImg = getImage("b_btn_page.png");
            btnSearchImg = getImage("b_btn_search.png");
        }
        //btnSlashImg = getImage("btn_slash.png");
        progressPointImg = getImage("09_smp_point.png");
        /*for (int i = 0; i < browseAniImage.length; i++) {
            browseAniImage[i] = getImage("09_pro_s_0" + (i + 1) + ".png");
        }*/
        FrameworkMain.getInstance().getImagePool().waitForAll();

        locTitleTxt = getString("locTitle");
        btnBackTxt = getString("back");
        btnMPlayerTxt = getString("musicPlayer");
        btnOptTxt = getString("options");
        btnSearchTxt = getString("search");
        btnPageTxt = getString("changePg");
        btnViewChangeTxt = getString("changeView");
        btnBrowseTxt = getString("browseBy");
        btnCloseTxt = getString("close");
        btnEraseTxt = getString("erase");
        ofTxt = getString("ofTxt");
    }

    /**
     * Get the renderer bound of UI .
     *
     * @param c the UIComponent
     * @return the preferred bounds
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        g.drawImage(logoImg, 53, 21, c);
        int pos = 53 + logoImg.getWidth(c) + 15;

        g.setFont(Rs.F18);
        for (int i = 0; locTitles != null && i < locTitles.length; i++) {
            Image img = logoPartDimImg;
            g.setColor(Rs.C124);
            if (i == locTitles.length - 1) {
                img = logoPartImg;
                g.setColor(Rs.C239);
            }
            g.drawImage(img, pos, 45, c);
            pos += img.getWidth(c) + 11;
            g.drawString(locTitles[i], pos, 57);
            pos += g.getFontMetrics().stringWidth(locTitles[i]) + 11;
        }

        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(Rs.F17);
                g.setColor(Rs.C255);
                longDateWth = g.getFontMetrics().stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
            }
        }
        if (clockImg != null) {
            int imgClockWth = clockImg.getWidth(c);
            g.drawImage(clockImg, 910 - longDateWth - 4 - imgClockWth, 44, c);
        }
    }

    /**
     * Paint selected item by SelectPopup.
     *
     * @param g the g
     * @param c the UIComponent
     */
    public void paintSelectedItem(Graphics g, UIComponent c) { }

    /**
     * Gets the title.
     *
     * @param title the title
     * @param numTxt the num txt
     * @param g the g
     * @param width the width
     * @return the title
     */
    protected String getTitle(String title, String numTxt, Graphics g, int width) {
        int numWidth = g.getFontMetrics().stringWidth(numTxt);
        return TextUtil.shorten(title, g.getFontMetrics(), width - numWidth) + numTxt;
    }

    /**
     * Gets the title number.
     *
     * @param ui the ui
     * @param content the content
     * @return the title number
     */
    /*protected String getTitleNumber(BasicUI ui, Content content) {
        if (ui.isDownloadCompleted(content)) {
            return " (" + content.getChildCount() + ")";
        } else {
            int listCount = 0;
            Vector children = content.getChildren();
            if (children != null) {
                listCount = children.size();
                if (children.elementAt(0) == null) { listCount--; }
            }
            return " (" + listCount + " " + ofTxt + " " + content.getChildCount() + ")";
        }
    }*/

    /**
     * Gets the title color.
     *
     * @param ui the ui
     * @param content the content
     * @param isWallUI the is wall ui
     * @return the title color
     */
    /*protected Color getTitleColor(BasicUI ui, Content content, boolean isWallUI) {
        if (ui.isDownloadCompleted(content)) {
            if (isWallUI) {
                return Rs.C227227228;
            } else { return Rs.C214182061; }
        } else if (ui.browseCount > 1) {
            return Rs.C255075079;
        }
        return Rs.C200;
    }*/
}

/*
 *  @(#)HomeRenderer.java 1.0 2011.05.24
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.HomeUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>HomeRenderer</code> The class to paint a GUI for initialization.
 *
 * @since   2011.05.24
 * @version $Revision: 1.5 $ $Date: 2012/05/03 03:35:38 $
 * @author  tklee
 */
public class HomeRenderer extends BasicRenderer {
    /** The device label bg img. */
    private Image devLabelBgImg = null;
    /** The content focus bg img. */
    private Image contFocusBgImg = null;
    /** The content bg img img. */
    private Image contBgImgImg = null;
    /** The content icon focus img. */
    private Image[] contIconFocusImg = new Image[3];
    /** The content icon img. */
    private Image[] contIconImg = new Image[3];
    /** The arrow top img. */
    private Image arrTImg = null;
    /** The arrow bottom img. */
    private Image arrBImg = null;
    /** The arrow left img. */
    private Image arrLImg = null;
    /** The arrow right img. */
    private Image arrRImg = null;
    /** The device label txt. */
    private String devLabelTxt = null;
    /** The change device txt. */
    private String changeDevTxt = null;
    /** The preference txt. */
    private String prefTxt = null;
    /** The exit txt. */
    private String exitTxt = null;
    /** The content txt. */
    private String[] contentTxt = new String[3];
    /** The exit btn pos. */
    public int exitBtnPosX = 0;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        devLabelBgImg = getImage("08_admin_bg.png");
        contFocusBgImg = getImage("09_dev_foc.png");
        contBgImgImg = getImage("09_dev_bg.png");
        contIconFocusImg[0] = getImage("09_dev_icon_p_foc.png");
        contIconFocusImg[1] = getImage("09_dev_icon_m_foc.png");
        contIconFocusImg[2] = getImage("09_dev_icon_v_foc.png");
        contIconImg[0] = getImage("09_dev_icon_p.png");
        contIconImg[1] = getImage("09_dev_icon_m.png");
        contIconImg[2] = getImage("09_dev_icon_v.png");
        arrTImg = getImage("02_ars_t.png");
        arrBImg = getImage("02_ars_b.png");
        arrLImg = getImage("02_ars_l.png");
        arrRImg = getImage("02_ars_r.png");
        super.prepare(c);
        devLabelTxt = getString("lDevice");
        changeDevTxt = getString("changeDev");
        prefTxt = getString("pref");
        exitTxt = getString("exitPress");
        contentTxt[0] = getString("photos");
        contentTxt[1] = getString("music");
        contentTxt[2] = getString("videos");
        locTitles = new String[] {locTitleTxt};
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        g.drawImage(hotkeybgImg, 236, 466, c);
        g.setFont(Rs.F18);
        g.setColor(Rs.C236211143);
        g.drawString(exitTxt, 66, 503);
        exitBtnPosX = 66 + g.getFontMetrics().stringWidth(exitTxt) + 7;
        g.drawImage(btnExitImg, exitBtnPosX, 488, c);
        HomeUI ui = (HomeUI) c;
        if (BasicUI.contentRoot == null || ui.connectDevice == null) { return; }

        g.drawImage(devLabelBgImg, 716, 69, c);
        g.setFont(Rs.F17);
        g.setColor(Rs.C255);
        String str = TextUtil.shorten(devLabelTxt + " "
                + ui.connectDevice.getDeviceName(), g.getFontMetrics(), 909 - 740);
        GraphicUtil.drawStringRight(g, str, 909, 95);

        g.drawImage(contBgImgImg, 136, 178, c);
        g.drawImage(contBgImgImg, 636, 178, c);
        g.drawImage(contFocusBgImg, 369, 160, c);
        int index = ui.menuFocus;
        g.drawImage(contIconFocusImg[ui.menuFocus], 389, 187, c);
        g.setFont(Rs.F24);
        g.setColor(Rs.C253203000);
        str = contentTxt[index] + " (" + BasicUI.contentRoot[index].getTotalCount() + ")";
        GraphicUtil.drawStringCenter(g, str, 481, 401);
        index = ui.menuFocus - 1;
        if (index < 0) { index = HomeUI.MENU_VIDEO; }
        g.drawImage(contIconImg[index], 161, 202, c);
        g.setFont(Rs.F20);
        g.setColor(Rs.C255);
        str = contentTxt[index] + " (" + BasicUI.contentRoot[index].getTotalCount() + ")";
        GraphicUtil.drawStringCenter(g, str, 230, 381);
        index = ui.menuFocus + 1;
        if (index > HomeUI.MENU_VIDEO) { index = HomeUI.MENU_PHOTO; }
        g.drawImage(contIconImg[index], 661, 202, c);
        str = contentTxt[index] + " (" + BasicUI.contentRoot[index].getTotalCount() + ")";
        GraphicUtil.drawStringCenter(g, str, 734, 381);

        g.drawImage(arrTImg, 468, 80, c);
        g.drawImage(arrBImg, 468, 456, c);
        g.drawImage(arrLImg, 349, 256, c);
        g.drawImage(arrRImg, 596, 256, c);
        g.setFont(Rs.F19);
        g.setColor(Rs.C216);
        GraphicUtil.drawStringCenter(g, changeDevTxt, 481, 111);
        GraphicUtil.drawStringCenter(g, prefTxt, 481, 451);
    }
}

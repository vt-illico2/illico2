/*
 *  @(#PreferenceRenderer.java 1.0 2011.07.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.ui.PreferenceUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>PreferenceRenderer</code> The class to paint a GUI for Preference.
 *
 * @since   2011.07.04
 * @version $Revision: 1.4 $ $Date: 2011/10/05 23:35:56 $
 * @author  tklee
 */
public class PreferenceRenderer extends BasicRenderer {
    /** The Constant DELIM_TXT. */
    private static final String DELIM_TXT = "^";
    /** The desc shadow img. */
    private Image descShadowImg = null;
    /** The top shadow img. */
    private Image topShadowImg = null;
    /** The input img. */
    private Image inputImg = null;
    /** The input focus img. */
    private Image inputFocusImg = null;
    /** The input focus dim img. */
    private Image inputFocusDimImg = null;
    /** The panel dim img. */
    private Image panelDimImg = null;
    /** The panel focus img. */
    private Image panelFocusImg = null;

    /** The title txt. */
    private String titleTxt = null;
    /** The menu txt. */
    public String[] menuTxt = null;
    /** The enable txt. */
    public String[] enableTxt = null;
    /** The effect txt. */
    public String[] effectTxt = null;
    /** The interval txt. */
    public String[] intervalTxt = null;
    /** The screensaver txt. */
    public String[] screensaverTxt = null;
    /** The yesno txt. */
    public String[] yesnoTxt = null;
    /** The video scaling txt. */
    public String[] videoScalingTxt = null;
    /** The desc txt. */
    private String[] descTxt = null;
    /** The preview txt. */
    private String previewTxt = null;
    /** The wait txt. */
    public String[] waitTxt = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        descShadowImg = getImage("08_descr_sh.png");
        topShadowImg = getImage("08_top_sh.png");
        inputImg = getImage("input_210_sc.png");
        inputFocusImg = getImage("input_210_foc2.png");
        inputFocusDimImg = getImage("input_210_high.png");
        panelDimImg = getImage("08_dimmed_panel.png");
        panelFocusImg = getImage("08_dimmed_panel_foc.png");
        super.prepare(c);
        locTitles = new String[] {locTitleTxt};
        titleTxt = getString("prefTitle");
        menuTxt = TextUtil.tokenize(getString("prefMenus"), DELIM_TXT);
        enableTxt = TextUtil.tokenize(getString("enalbleTxt"), DELIM_TXT);
        effectTxt = TextUtil.tokenize(getString("prefEffects"), DELIM_TXT);
        intervalTxt = TextUtil.tokenize(getString("prefIntervals"), DELIM_TXT);
        screensaverTxt = TextUtil.tokenize(getString("prefScreensaver"), DELIM_TXT);
        yesnoTxt = TextUtil.tokenize(getString("yesnoTxt"), DELIM_TXT);
        videoScalingTxt = TextUtil.tokenize(getString("prefVideoScaling"), DELIM_TXT);
        descTxt = TextUtil.tokenize(getString("prefDesc"), DELIM_TXT);
        previewTxt = getString("Preview");
        waitTxt = TextUtil.tokenize(getString("prefWaits"), DELIM_TXT);
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        PreferenceUI ui = (PreferenceUI) c;
        g.drawImage(hotkeybgImg, 367, 466, c);
        g.drawImage(descShadowImg, 0, 411, c);
        g.drawImage(topShadowImg, 0, 77, c);
        g.setFont(Rs.F26);
        g.setColor(Rs.C255);
        g.drawString(titleTxt, 55, 113);

        g.setFont(Rs.F15_D);
        g.setColor(Rs.C218205162);
        String[] desc = TextUtil.split(descTxt[ui.menuFocus], g.getFontMetrics(), 905 - 55, 2);
        for (int i = 0; i < desc.length; i++) {
            g.drawString(desc[i], 55, 443 + (i * 20));
        }

        /*String[] values = {enableTxt[ui.autoConnectFocus], effectTxt[ui.effectFocus], intervalTxt[ui.intervalFocus],
                screensaverTxt[ui.screensaverFocus], yesnoTxt[ui.musicCrossFadeFocus],
                videoScalingTxt[ui.videoScalingFocus]};*/
        String[] values = {enableTxt[ui.autoConnectFocus], effectTxt[ui.effectFocus], intervalTxt[ui.intervalFocus],
                screensaverTxt[ui.screensaverFocus], videoScalingTxt[ui.videoScalingFocus]};
        for (int i = 0; i < menuTxt.length; i++) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C210);
            int addY = i * 37;
            if (i == PreferenceUI.MENU_FAVOR_DEVICE) {
                String[] menu = TextUtil.split(menuTxt[i], g.getFontMetrics(), 234 - 56, 2);
                for (int j = 0; j < menu.length; j++) {
                    g.drawString(menu[j], 56, 155 + (j * 14));
                }
            } else {
                g.drawString(menuTxt[i], 56, 219 + ((i - 1) * 37));
                addY += 21;
            }

            g.setFont(Rs.F18);
            g.setColor(Rs.C180);
            Image img = inputImg;
            if (i == ui.menuFocus && ui.isLeftStatus) {
                img = inputFocusImg;
                g.setColor(Rs.C240);
            }
            g.drawImage(img, 339, 141 + addY, c);
            g.drawString(values[i], 353, 162 + addY);
        }

        g.setFont(Rs.F18);
        if (ui.autoConnectFocus == PreferenceUI.AUTOCONNECT_ENABLED) {
            Image img = inputImg;
            g.setColor(Rs.C180);
            if (ui.menuFocus == PreferenceUI.MENU_FAVOR_DEVICE && !ui.isLeftStatus) {
                img = inputFocusImg;
                g.setColor(Rs.C240);
            }
            g.drawImage(img, 575, 141, c);
            String str =
                TextUtil.shorten(PreferenceProxy.getInstance().favorDeviceName, g.getFontMetrics(), 769 - 589);
            g.drawString(str, 589, 162);
        }
        if (ui.effectFocus != PreferenceUI.EFFECT_NO) {
            Image img = panelDimImg;
            g.setColor(Rs.C200);
            if (ui.menuFocus == PreferenceUI.MENU_SLIDESHOW_EFFECT && !ui.isLeftStatus) {
                img = panelFocusImg;
                g.setColor(Rs.C3);
            }
            g.drawImage(img, 575, 200, c);
            GraphicUtil.drawStringCenter(g, previewTxt, 680, 220);
        }
        if (ui.screensaverFocus != PreferenceUI.SCREENSAVER_NO) {
            Image img = inputImg;
            g.setColor(Rs.C180);
            if (ui.menuFocus == PreferenceUI.MENU_MUSIC_SCREENSAVER && !ui.isLeftStatus) {
                img = inputFocusImg;
                g.setColor(Rs.C240);
            }
            g.drawImage(img, 575, 273, c);
            g.drawString(waitTxt[ui.waitFocus], 589, 294);
        }
    }

    /**
     * Paint selected item by SelectPopup.
     *
     * @param g the g
     * @param c the UIComponent
     */
    public void paintSelectedItem(Graphics g, UIComponent c) {
        PreferenceUI ui = (PreferenceUI) c.getParent();
        g.setFont(Rs.F17);
        g.setColor(Rs.C210);
        int addY = ui.menuFocus * 37;
        if (ui.menuFocus == PreferenceUI.MENU_FAVOR_DEVICE) {
            String[] menu = TextUtil.split(menuTxt[ui.menuFocus], g.getFontMetrics(), 234 - 56, 2);
            for (int j = 0; j < menu.length; j++) {
                g.drawString(menu[j], 56, 155 + (j * 14));
            }
        } else {
            g.drawString(menuTxt[ui.menuFocus], 56, 219 + ((ui.menuFocus - 1) * 37));
            addY += 21;
        }
        if (ui.isLeftStatus) {
            /*String[] values = {enableTxt[ui.autoConnectFocus], effectTxt[ui.effectFocus], intervalTxt[ui.intervalFocus],
                    screensaverTxt[ui.screensaverFocus], yesnoTxt[ui.musicCrossFadeFocus],
                    videoScalingTxt[ui.videoScalingFocus]};*/
            String[] values = {enableTxt[ui.autoConnectFocus], effectTxt[ui.effectFocus], intervalTxt[ui.intervalFocus],
                    screensaverTxt[ui.screensaverFocus], videoScalingTxt[ui.videoScalingFocus]};
            g.setFont(Rs.F18);
            g.setColor(Rs.C240);
            g.drawImage(inputFocusDimImg, 339, 141 + addY, c);
            g.drawString(values[ui.menuFocus], 353, 162 + addY);
        } else {
            g.setFont(Rs.F18);
            if (ui.menuFocus == PreferenceUI.MENU_FAVOR_DEVICE) {
                g.setColor(Rs.C240);
                g.drawImage(inputFocusDimImg, 575, 141, c);
                g.drawString(PreferenceProxy.getInstance().favorDeviceName, 589, 162);
            } else {
                g.setColor(Rs.C240);
                g.drawImage(inputFocusDimImg, 575, 273, c);
                g.drawString(waitTxt[ui.waitFocus], 589, 294);
            }
        }
    }
}

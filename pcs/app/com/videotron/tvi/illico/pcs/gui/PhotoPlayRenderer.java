/*
 *  @(#)PhotoPlayRenderer.java 1.0 2011.07.28
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.ui.PhotoPlayUI;

/**
 * <code>PhotoPlayRenderer</code> The class to paint a GUI for PhotoPlayUI.
 *
 * @since   2011.07.28
 * @version $Revision: 1.8 $ $Date: 2012/10/08 23:45:21 $
 * @author  tklee
 */
public class PhotoPlayRenderer extends BasicRenderer {
    /** The size for zoom image. */
    private static final int[] SIZE_ZOOM = {199, 112};

    /** The zoom outline img. */
    private Image zoomOutlineImg = null;
    /** The zoom focus img. */
    private Image zoomFocusImg = null;
    /** The exit txt. */
    private String exitTxt = null;
    /** The exit btn pos. */
    public int exitBtnPosX = 0;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        zoomOutlineImg = getImage("09_zoom_outline.png");
        zoomFocusImg = getImage("09_zoom_s_focus.png");
        exitTxt = getString("toLeaveZoom");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        PhotoPlayUI ui = (PhotoPlayUI) c;
        if (ui.photoContent == null) { return; }
        Image img = ui.photoContent.getImage();
        if (ui.flipBar == ui.viewerBar) {
            /*if (img != null) {
                int posX = ((PhotoPlayUI.SIZE_FULL[0] - img.getWidth(null)) / 2);
                int posY = ((PhotoPlayUI.SIZE_FULL[1] - img.getHeight(null)) / 2);
                g.drawImage(img, posX, posY, c);
            }*/
        } else {
            if (img != null) {
                int focus = ui.flipBar.getFocus();
                int zoom = PhotoPlayUI.ZOOM_VALUE[focus];
                int zoomPart = PhotoPlayUI.ZOOM_PART[focus];
                int width = PhotoPlayUI.SIZE_FULL[0] / zoom;
                int height = PhotoPlayUI.SIZE_FULL[1] / zoom;
                //int posX = ui.zoomMovingPoint[0] * width;
                //int posY = ui.zoomMovingPoint[1] * height;
                int posX = 0;
                int posY = 0;
                if (zoom > 1) {
                    posX = width / 2 * ui.zoomMovingPoint[0];
                    posY = PhotoPlayUI.SIZE_FULL[1] - height;
                    if (ui.zoomMovingPoint[1] < zoomPart - 1) {
                        posY = height / 2 * ui.zoomMovingPoint[1];
                        int rest = PhotoPlayUI.SIZE_FULL[1] % zoom;
                        if (rest > 0) { posY += rest * (ui.zoomMovingPoint[1] + 1) / 10; }
                        rest = height % 2;
                        if (rest > 0) { posY += rest * (ui.zoomMovingPoint[1] + 1) / 10; }
                    }
                }
                int posX2 = width + posX;
                int posY2 = height + posY;
                int bufImgWidth = ui.zoomBufImg.getWidth();
                int bufImgHeight = ui.zoomBufImg.getHeight();
                int uiX = (960 - bufImgWidth) / 2;
                int uiY = (540 - bufImgHeight) / 2;
                g.drawImage(ui.zoomBufImg, uiX, uiY, uiX + bufImgWidth, uiY + bufImgHeight,
                        posX, posY, posX2, posY2, c);
                if (!ui.flipBar.isAlpha()) {
                    int[] focusSize = new int[]{176, 99}; //160, 120
                    g.drawImage(zoomOutlineImg, 735, 57, focusSize[0] + 6, focusSize[1] + 6, c);
                    g.drawImage(ui.zoomBufImg, 738, 60, focusSize[0], focusSize[1], c);
                    int focusWidth = focusSize[0] / zoom;
                    int focusHeight = focusSize[1] / zoom;
                    //
                    int focusX = 0;
                    int focusY = 0;
                    if (zoom > 1) {
                        focusX = focusWidth / 2 * ui.zoomMovingPoint[0];
                        focusY = focusSize[1] - focusHeight;
                        if (ui.zoomMovingPoint[1] < zoomPart - 1) {
                            focusY = focusHeight / 2 * ui.zoomMovingPoint[1];
                            int rest = focusSize[1] % zoom;
                            if (rest > 0) { focusY += rest * (ui.zoomMovingPoint[1] + 1) / 10; }
                            rest = focusHeight % 2;
                            if (rest > 0) { focusY += rest * (ui.zoomMovingPoint[1] + 1) / 10; }
                        }
                    }
                    g.setColor(Rs.C255203000);
                    for (int i = 0; i < 3; i++) {
                        //g.drawRect(735 + focusX + i, 57 + focusY + i, focusWidth - (i * 2 + 1), focusHeight - (i * 2 + 1));
                        g.drawRect(735 + focusX + i, 57 + focusY + i, focusWidth - (i * 2 + 1) + 6, focusHeight - (i * 2 + 1) + 6);
                    }
                    g.setFont(Rs.F18);
                    g.setColor(Rs.C0);
                    g.drawString(exitTxt, 65, 504);
                    g.setColor(Rs.C236211143);
                    g.drawString(exitTxt, 64, 503);
                    exitBtnPosX = 65 + g.getFontMetrics().stringWidth(exitTxt + " ");
                    g.drawImage(btnExitImg, exitBtnPosX, 488, c);
                }
            }
        }
        //g.drawImage(img, x+w, y+h, w, h, 0, 0, x, y, this);
    }
}

/*
 *  @(#)DataConverter.java 1.0 2011.03.11
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.pcs.util;


/**
 * <code>DataConverter</code> this class is util to convert data type.
 *
 * @since   2011.03.11
 * @version $Revision: 1.1 $ $Date: 2011/07/19 17:50:50 $
 * @author  tklee
 */
public final class DataConverter {
    /**
     * Constructor.
     */
    private DataConverter() { }

    /**
     * convert Two bytes to int.
     *
     * @param data the data
     * @param offset the offset
     * @return the int
     */
    public static int convertTwoBytesToInt(byte[] data, int offset) {
        return convertTwoBytesToInt(data, offset, false);
    }

    /**
     * convert two bytes to int.
     *
     * @param data the data
     * @param offset the offset
     * @param byNegative the by negative
     * @return the int
     */
    public static int convertTwoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF));
    }

    /**
     * convert One byte to int.
     *
     * @param data the data
     * @param offset the offset
     * @return the int
     */
    public static int convertOneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Convert byte array to string.
     *
     * @param data the data
     * @param offset the offset
     * @param length the length
     * @return the string
     */
    public static String convertToString(byte[] data, int offset, int length) {
        String result = null;
        try {
            //result = new String(data, offset, length);
            result = new String(data, offset, length, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

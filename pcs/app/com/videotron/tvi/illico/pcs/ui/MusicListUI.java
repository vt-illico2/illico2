/*
 *  @(#)MusicListUI.java 1.0 2011.06.14
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.gui.MusicListRenderer;
import com.videotron.tvi.illico.pcs.ui.comp.AutoScroller;
import com.videotron.tvi.illico.pcs.ui.comp.LogDisplayer;
import com.videotron.tvi.illico.pcs.ui.popup.MusicBrowsingPopup;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.pcs.ui.popup.NotiPopup;
import com.videotron.tvi.illico.pcs.ui.popup.Popup;
import com.videotron.tvi.illico.pcs.ui.popup.SearchPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>MusicListUI</code> This class show the list page for music.
 *
 * @since   2011.06.14
 * @version $Revision: 1.43 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class MusicListUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant TH_CONTENT_LIST. */
    protected static final String TH_CONTENT_LIST = "contentList";
    /** The Constant TH_MUSIC_PLAY. */
    protected static final String TH_MUSIC_PLAY = "musicPlay";

    /** The Constant UI_ALBUM. */
    public static final int UI_ALBUM = 0;
    /** The Constant UI_ALBUM_SONGS. */
    public static final int UI_ALBUM_SONGS = 1;
    /** The Constant UI_ALL_SONGS. */
    public static final int UI_ALL_SONGS = 2;
    /** The Constant UI_ARTIST. */
    public static final int UI_ARTIST = 3;
    /** The Constant UI_ARTIST_SONGS. */
    public static final int UI_ARTIST_SONGS = 4;
    /** The Constant UI_ARTIST_ALBUM. */
    public static final int UI_ARTIST_ALBUM = 5;
    /** The Constant UI_ARTIST_ALBUM_SONGS. */
    public static final int UI_ARTIST_ALBUM_SONGS = 6;
    /** The Constant UI_ARTIST_ALL_SONGS. */
    public static final int UI_ARTIST_ALL_SONGS = 7;
    /** The Constant UI_GENRE. */
    public static final int UI_GENRE = 8;
    /** The Constant UI_GENRE_SONGS. */
    public static final int UI_GENRE_SONGS = 9;
    /** The Constant UI_PLAYLIST. */
    public static final int UI_PLAYLIST = 10;
    /** The Constant UI_PLAYLIST_SONGS. */
    public static final int UI_PLAYLIST_SONGS = 11;
    /** The Constant UI_NOPLAYLIST. */
    public static final int UI_NOPLAYLIST = 12;

    /** The Constant HISTORY_NONE. */
    protected static final int HISTORY_NONE = -1;
    /** The Constant HISTORY_ADD. */
    protected static final int HISTORY_ADD = 0;
    /** The Constant HISTORY_RESET. */
    protected static final int HISTORY_RESET = 1;

    /** The Constant GC_COUNT_PLAYLIST. */
    private static final int GC_COUNT_PLAYLIST = 500;

    /** The MusicListRenderer instance. */
    protected MusicListRenderer renderer = null;
    /** The view change popup. */
    protected MusicBrowsingPopup musicBrowsePopup = null;

    /** The Constant OPT_GOTO. */
    protected static final MenuItem OPT_GOTO =
            new MenuItem("renderer.goTo", new MenuItem[]{OPT_PHOTO, OPT_MUSIC, OPT_VIDEO});
    /** The Constant OPT_GOTO2. */
    protected static final MenuItem OPT_GOTO2 =
            new MenuItem("renderer.goTo", new MenuItem[]{OPT_PHOTO, OPT_MUSIC, OPT_VIDEO});
    /** The Constant OPT_SORT. */
    protected static final MenuItem OPT_SORT = new MenuItem("renderer.sorting",
            //new MenuItem[]{OPT_SORT_ALPHA_A, OPT_SORT_ALPHA_Z, OPT_SORT_NEWEST, OPT_SORT_OLDEST});
            new MenuItem[]{OPT_SORT_ALPHA_A, OPT_SORT_OLDEST});
    /** The menu item without sort. */
    protected final MenuItem menuItem = new MenuItem("D-Option",
            new MenuItem[] {OPT_GOTO, OPT_PREFERENCE, OPT_CHANGE_DEV, OPT_HELP});
    /** The full menu item. */
    protected final MenuItem fullMenuItem = new MenuItem("D-Option",
            new MenuItem[] {OPT_SORT, OPT_GOTO2, OPT_PREFERENCE, OPT_CHANGE_DEV, OPT_HELP});

    /** The Constant SLEEP_TIME. */
    protected static final long SLEEP_TIME = 1000L;

    public Content currentRoot = null;
    /** The temp root. */
    protected Content tempRoot = null;
    /** The history stack. */
    protected Stack historyStack = null;
    /** The list ticker. */
    public AutoScroller listTicker = null;
    /** The folder thread. */
    private Thread folderThread = null;
    /** The photo focus. */
    public int photoFocus = 0;
    /** The focus position. */
    public int focusPosition = 0;
    /** The temp photo focus. */
    protected int tempPhotoFocus = 0;
    /** The temp focus position. */
    protected int tempFocusPosition = 0;
    /** The temp ui type. */
    protected int tempUIType = 0;
    /** The temp sort type. */
    protected int tempSortType = 0;
    /** The is sort. */
    protected boolean isSort = true;
    /** The ui type. */
    public int uiType = 0;
    /** whether must request to browse on not. */
    protected boolean isNewBrowse = true;
    /** The history set type. */
    protected int historySetType = HISTORY_NONE;
    /** The timer spec. */
    //protected TVTimerSpec timerSpec = null;
    /** The is timer. */
    //protected boolean isTimer = false;
    /** The last change ui type. */
    protected int lastChangeUiType = -1;
    /** The view change id. */
    protected int viewChangeId = -1;
    /** The playlist duration count. */
    public int playlistDurationCount = 0;

    /**
     * Initialize the Scene.
     */
    public MusicListUI() {
        renderer = new MusicListRenderer();
        setRenderer(renderer);
        rootMenuItem = fullMenuItem;
        historyStack = new Stack();
        musicBrowsePopup = new MusicBrowsingPopup();
        listTicker = new AutoScroller();
        listTicker.setSleepTime(Config.autoscrollSleepSec * 1000L);
        listTicker.setLineGap(12);
        clickEffect = new ClickingEffect(this, 5);
        sortType = Content.SORT_ALPHA_A;
        /*timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME * Config.folderRotationSec);
        timerSpec.setRepeat(true);*/
        pagingDataThread = new PagingThread();
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();
        super.start(isReset, sendData);
        isNewBrowse = false;
        if (isReset) {
            photoFocus = 0;
            focusPosition = 0;
            uiType = 0;
            sortType = Content.SORT_ALPHA_A;
            tempRoot = musicRoot[SEQ_ALBUM];
            for (int i = 0; i < musicRoot.length; i++) {
                if (musicRoot[i] != null) { musicRoot[i].setChildren(null); }
            }
            isNewBrowse = true;
            historyStack.clear();
        }
        changeNode(photoFocus, focusPosition, tempRoot, uiType, sortType, isReset, HISTORY_NONE);
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        footer.addButtonWithLabel(BasicRenderer.btnSearchImg, BasicRenderer.btnSearchTxt);
        footer.addButtonWithLabel(BasicRenderer.btnPageImg, BasicRenderer.btnPageTxt);
        footer.addButtonWithLabel(BasicRenderer.btnAImg, BasicRenderer.btnBrowseTxt);
        footer.addButtonWithLabel(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
        footer.addButtonWithLabel(BasicRenderer.btnDImg, BasicRenderer.btnOptTxt);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        stopTimer();
        super.stop();
        renderer.stop();
        listTicker.stop();
        tempRoot = currentRoot;
        flushPhotoImgs(currentRoot);
        currentRoot = null;
        if (tempRoot != null) { tempRoot.setChildren(null); }
        conImgPool.clear();
        playlistDurationCount = 0;
    }

    /**
     * Clear temp data.
     */
    public void clearTempData() {
        tempRoot = null;
        searchContent = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
        historyStack.clear();
        tempRoot = null;
        currentRoot = null;
        musicRoot = null;
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        final int focus = photoFocus;
        final Content reqNode = currentRoot;
        folderThread = new Thread("ContentListUI|startTimer()") {
            public void run() {
                Thread startThread = folderThread;
                while (folderThread != null && startThread == folderThread
                        && focus == photoFocus && reqNode == currentRoot) {
                    try {
                        Thread.sleep(SLEEP_TIME * Config.folderRotationSec);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (folderThread == null || startThread != folderThread) { break; }
                    if (focus != photoFocus || reqNode != currentRoot) { break; }
                    timerWentOff(reqNode, focus);
                }
            }
        };
        folderThread.start();
    }

    /**
     * Start timer for folder.
     */
    private void startTimerForFolder() {
        stopTimer();
        if (uiType == UI_GENRE || uiType == UI_ARTIST || uiType == UI_PLAYLIST) {
            Content focusNode = null;
            Vector list = currentRoot.getChildren();
            if (list != null) { focusNode = (Content) list.elementAt(photoFocus); }
            Logger.debug(this, "startTimerForFolder()-focusNode : " + focusNode);
            if (focusNode != null && focusNode.isFolder()) { startTimer(); }
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        folderThread = null;
    }

    /**
     * execute the processing for folder.
     *
     * @param reqNode the req node
     * @param focus the focus
     */
    private void timerWentOff(Content reqNode, int focus) {
        Content focusNode = (Content) reqNode.getChildren().elementAt(photoFocus);
        if (focusNode != null && focusNode.isFolder()
                && (uiType == UI_GENRE || uiType == UI_ARTIST || uiType == UI_PLAYLIST)) {
            int urlFocus = focusNode.getPhotoIndexInFolder();
            int oldUrlFocus = 0;
            Vector urlList = focusNode.getIconUrlsInFolder();
            if (urlList == null) {
                focusNode.setPhotoIndexInFolder(0);
            } else {
                oldUrlFocus = urlFocus;
            }
            if (urlList == null || (urlList.size() > 0 && urlList.size() - 1 == urlFocus % Config.loadDataNumber
                    && (urlList.size() == Config.loadDataNumber || urlFocus >= Config.loadDataNumber))) {
                int startIndex = 0;
                if (urlList != null && urlList.size() == Config.loadDataNumber) { startIndex = urlFocus + 1; }
                Vector list = HNHandler.getInstance().requestSearchPhotoUrlList(focusNode.getId(),
                        Content.SORT_ALPHA_A, MENU_MUSIC, startIndex);
                if (focus != photoFocus || reqNode != currentRoot) { return; }
                if (list != null && list.size() > 0) {
                    focusNode.setIconUrlsInFolder(list);
                    urlFocus = startIndex;
                    if (urlList == null && list.size() > 1) {
                        urlFocus = 1;
                    }
                } else if (urlList != null) {
                    if (urlFocus < Config.loadDataNumber) {
                        urlFocus = 0;
                    } else {
                        startIndex = 0;
                        list = HNHandler.getInstance().requestSearchPhotoUrlList(focusNode.getId(),
                                Content.SORT_ALPHA_A, MENU_MUSIC, startIndex);
                        if (list != null && list.size() > 0) {
                            focusNode.setIconUrlsInFolder(list);
                            urlFocus = startIndex;
                        } else {
                            urlFocus = urlFocus - urlList.size() + 1;
                        }
                    }
                }
            } else if (urlList.size() > 1) {
                if (++urlFocus == urlList.size()) { urlFocus = 0; }
            }
            focusNode.setPhotoIndexInFolder(urlFocus);
            urlList = focusNode.getIconUrlsInFolder();
            if (urlList != null && urlList.size() > 0 && urlFocus != oldUrlFocus) {
                int[] size = SIZE_LIST_FILE;
                String oldIconUrl = focusNode.getIconUrl();
                focusNode.setIconUrl((String) urlList.elementAt(urlFocus));
                String key = focusNode.getId() + focusNode.getIconUrl();
                Image img = conImgPool.getImage(key);
                if (img != null) {
                    focusNode.setIconImage(img);
                } else {
                    if (focus != photoFocus || reqNode != currentRoot) { return; }
                    img = MusicListUI.this.getContentImage(key, focusNode.getIconUrl(), null, size);
                    conImgPool.waitForAll();
                    focusNode.setIconImage(img);
                }
                if (!checkVisible()) { return; }
                if (focus == photoFocus || reqNode != currentRoot) { repaint(); }
                if (oldIconUrl != null) {
                    conImgPool.remove(focusNode.getId() + oldIconUrl);
                }
            }
        }
    }

    /**
     * Move focus.
     *
     * @param reqNode the req node
     * @param focus the focus
     * @param position the position
     * @param setPosterImg the set poster img
     * @param reqUiType the req ui type
     */
    private void moveFocus(Content reqNode, int focus, int position, boolean setPosterImg, int reqUiType) {
        Vector children = reqNode.getChildren();
        if (children == null) { return; }
        stopTimer();
        int reqDataFocus = focus;
        int curDataFocus = photoFocus;
        if (isTempNode(reqUiType)) {
            reqDataFocus--;
            curDataFocus--;
        }
        int reqPage = getPageNumber(reqDataFocus);
        int curPage = getPageNumber(curDataFocus);
        photoFocus = focus;
        focusPosition = position;
        if (Config.enableDebugUI) { LogDisplayer.getInstance().setFocusPage(reqPage + 1); }
        reqNode.setLastFocus(photoFocus);
        Content lastNode = (Content) children.elementAt(photoFocus);
        reqNode.setLastFocusId(lastNode != null ? lastNode.getId() : null);
        setTicker();
        repaint();
        if (setPosterImg) { setPosterImages(); }
        startTimerForFolder();
        int totalCount = reqNode.getChildCount();
        int lastPageNum = getLastPageNumber(totalCount);
        int cachePageNumber = Config.cachePageNumber;
        if (Config.cachePageNumber % 2 == 0) { cachePageNumber++; }
        if (curPage != reqPage && lastPageNum + 1 > cachePageNumber) {
            String actionType = PagingThread.TYPE_LOAD;
            int actionPage = reqPage + Config.cachePageNumber / 2;
            if (curPage > reqPage) {
                actionPage++;
                actionType = PagingThread.TYPE_REMOVE;
            }
            if (actionPage > lastPageNum) { actionPage = actionPage - lastPageNum - 1; }
            loadPagingData(reqNode, MENU_MUSIC, actionPage, actionType);

            actionType = PagingThread.TYPE_REMOVE;
            actionPage = reqPage - Config.cachePageNumber / 2 - 1;
            if (curPage > reqPage) {
                actionPage++;
                actionType = PagingThread.TYPE_LOAD;
            }
            if (actionPage < 0) { actionPage = lastPageNum + actionPage + 1; }
            loadPagingData(reqNode, MENU_MUSIC, actionPage, actionType);
        }
    }

    /**
     * Check focus data.
     *
     * @param focus the focus
     * @param position the position
     * @param setPosterImg the set poster img
     */
    private void checkFocusData(final int focus, final int position, final boolean setPosterImg) {
        final Content reqNode = currentRoot;
        final int reqUiType = uiType;
        if (reqNode == null || reqNode.getChildren() == null) { return; }
        if (reqNode.getChildCount() <= Config.loadDataNumber) {
            moveFocus(reqNode, focus, position, setPosterImg, reqUiType);
            return;
        }

        int reqDataFocus = focus;
        int curDataFocus = photoFocus;
        if (isTempNode(reqUiType)) {
            reqDataFocus--;
            curDataFocus--;
        }
        final int reqPage = getPageNumber(reqDataFocus);
        //int curPage = getPageNumber(curDataFocus);
        Hashtable browsePages = reqNode.getBrowsePageNums();
        if (browsePages == null) { return; }

        Hashtable errorPages = reqNode.getErrorPageNums();
        if (errorPages != null && errorPages.containsKey(String.valueOf(reqPage))) {
            showConnectErrorPopup();
        } else if (browsePages.containsKey(String.valueOf(reqPage))) {
            moveFocus(reqNode, focus, position, setPosterImg, reqUiType);
        } else {
            CommunicationManager.getInstance().showNotDelayLoadingAnimation();
            new Thread("ContentListUI|checkFocusData()") {
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(300L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!checkVisible() || currentRoot != reqNode || currentRoot == null || uiType != reqUiType
                                || !CommunicationManager.getInstance().isLoadingAnimation()) {
                            break;
                        }
                        Hashtable browsePages = reqNode.getBrowsePageNums();
                        if (browsePages == null) { break; }
                        Logger.debug(this, "checkFocusData()-reqPage : " + reqPage);
                        Hashtable errorPages = reqNode.getErrorPageNums();
                        if (browsePages.containsKey(String.valueOf(reqPage))) {
                            EventQueue.invokeLater(new Runnable() {
                                public void run() {
                                    CommunicationManager.getInstance().hideLoadingAnimation();
                                    moveFocus(reqNode, focus, position, setPosterImg, reqUiType);
                                }
                            });
                            break;
                        } else if (errorPages != null && errorPages.containsKey(String.valueOf(reqPage))) {
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            showConnectErrorPopup();
                            break;
                        }
                    }
                }
            } .start();
        }
    }

    /**
     * Checks if is temp node.
     *
     * @param reqUiType the ui type
     * @return true, if is temp node
     */
    public boolean isTempNode(int reqUiType) {
        if ((reqUiType == UI_ALBUM || reqUiType == UI_ALBUM_SONGS || reqUiType == UI_ALL_SONGS
                || reqUiType == UI_ARTIST_SONGS || reqUiType == UI_ARTIST_ALBUM
                || reqUiType == UI_ARTIST_ALBUM_SONGS || reqUiType == UI_ARTIST_ALL_SONGS
                || reqUiType == UI_GENRE_SONGS  || reqUiType == UI_PLAYLIST_SONGS)) {
            return true;
        }
        return false;
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
                if (photoFocus % IMAGES_PER_LINE != 0) {
                    checkFocusData(photoFocus - 1, focusPosition - 1, false);
                }
            }
            break;
        case Rs.KEY_RIGHT:
            if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
                int totalCount = currentRoot.getChildren().size();
                if ((photoFocus + 1) % IMAGES_PER_LINE != 0 && photoFocus < totalCount - 1) {
                    checkFocusData(photoFocus + 1, focusPosition + 1, false);
                }
            }
            break;
        case Rs.KEY_UP:
            if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
                if (photoFocus / IMAGES_PER_LINE > 0) {
                    int reqFocus =  photoFocus - IMAGES_PER_LINE;
                    int reqPosition = focusPosition;
                    boolean setPosterImg = true;
                    if (focusPosition >= IMAGES_PER_LINE) {
                        reqPosition = focusPosition - IMAGES_PER_LINE;
                        setPosterImg = false;
                    }
                    checkFocusData(reqFocus, reqPosition, setPosterImg);
                }
            } else {
                if (photoFocus > 0) {
                    int reqPosition = focusPosition;
                    if (focusPosition != MIDDLE_POS || photoFocus == focusPosition) { reqPosition--; }
                    checkFocusData(photoFocus - 1, reqPosition, true);
                }
            }
            break;
        case Rs.KEY_DOWN:
            int totalCount = currentRoot.getChildren().size();
            if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
                if (!isLastLineForGrid(totalCount, photoFocus, IMAGES_PER_LINE)) {
                    int reqFocus = totalCount - 1;
                    if (photoFocus + IMAGES_PER_LINE < totalCount) { reqFocus = photoFocus + IMAGES_PER_LINE; }
                    boolean setPosterImg = focusPosition >= IMAGES_PER_LINE;
                    int reqPosition = IMAGES_PER_LINE + (reqFocus % IMAGES_PER_LINE);
                    checkFocusData(reqFocus, reqPosition, setPosterImg);
                }
            } else {
                if (photoFocus < totalCount - 1) {
                    int reqPosition = focusPosition;
                    if ((focusPosition < ROWS_PER_PAGE - 1)
                            && (focusPosition != MIDDLE_POS || focusPosition + photoFocus >= totalCount - 1)) {
                        reqPosition++;
                    }
                    checkFocusData(photoFocus + 1, reqPosition, true);
                }
            }
            break;
        case Rs.KEY_ENTER:
            if (uiType == MusicListUI.UI_ALBUM || uiType == MusicListUI.UI_ARTIST_ALBUM) {
                int addX = (photoFocus % IMAGES_PER_LINE) * 164;
                int addY = (focusPosition / ContentListUI.IMAGES_PER_LINE) * 177;
                clickEffect.start(82 + addX, 108 + addY, 386 - 246, 247 - 108);
            } else {
                clickEffect.start(55, 174 + (focusPosition * 32), 584 - 55, 338 - 302);
            }
            isNewBrowse = true;
            Content focusNode = (Content) currentRoot.getChildren().elementAt(photoFocus);
            if (uiType == UI_ALBUM) {
                if (photoFocus == 0) {
                    changeNode(0, 0, musicRoot[SEQ_SONGS], UI_ALL_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
                } else { changeNode(0, 0, focusNode, UI_ALBUM_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD); }
            } else if (uiType == UI_ARTIST) {
                if (focusNode.getTotalCount() == 1) {
                    changeNode(0, 0, focusNode, UI_ARTIST_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
                } else {
                    changeNode(0, 0, focusNode, UI_ARTIST_ALBUM, Content.SORT_ALPHA_A, true, HISTORY_ADD);
                }
            } else if (uiType == UI_ARTIST_ALBUM) {
                if (photoFocus == 0) {
                    tempRoot = new Content();
                    tempRoot.setName(currentRoot.getName());
                    tempRoot.setFolder(true);
                    tempRoot.setId(currentRoot.getId());
                    tempRoot.setArtistAllsongsDir(true);
                    changeNode(0, 0, tempRoot, UI_ARTIST_ALL_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
                } else {
                    changeNode(0, 0, focusNode, UI_ARTIST_ALBUM_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
                }
            } else if (uiType == UI_GENRE) {
                changeNode(0, 0, focusNode, UI_GENRE_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
            } else if (uiType == UI_PLAYLIST) {
                changeNode(0, 0, focusNode, UI_PLAYLIST_SONGS, Content.SORT_ALPHA_A, true, HISTORY_ADD);
            } else { playMusic(); }
            break;
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            if (!historyStack.isEmpty()) {
                showPrevList();
            } else {
                historyStack.clear();
                SceneManager.getInstance().goToScene(SceneManager.SC_HOME, false, null);
            }
            break;
        case KeyCodes.SEARCH:
            footer.clickAnimation(BasicRenderer.btnSearchTxt);
            addSearchPopup(true);
            break;
        case Rs.KEY_PAGE_UP:
        case Rs.KEY_PAGE_DOWN:
            footer.clickAnimation(BasicRenderer.btnPageTxt);
            changePage(keyCode);
            break;
        case KeyCodes.COLOR_A:
            footer.clickAnimation(BasicRenderer.btnBrowseTxt);
            lastChangeUiType = -1;
            viewChangeId = -1;
            //isViewChangeLoading = false;
            musicBrowsePopup.resetFocus();
            musicBrowsePopup.setListFocus(getMusicSeq());
            addPopup(musicBrowsePopup);
            break;
        case KeyCodes.COLOR_B:
            footer.clickAnimation(BasicRenderer.btnBImg);
            musicPlayerPopup.resetFocus();
            addPopup(musicPlayerPopup);
            break;
        case KeyCodes.PLAY:
            playMusic();
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Show prev list.
     */
    protected void showPrevList() {
        Object[] history = (Object[]) historyStack.pop();
        isNewBrowse = false;
        Content prevContent = (Content) history[0];
        int[] historyNum = (int[]) history[1];
        changeNode(historyNum[0], historyNum[1], prevContent, historyNum[2], historyNum[3], false,
                HISTORY_NONE);
    }

    /**
     * Adds the search popup.
     *
     * @param isResetStateFocus the is reset state focus
     */
    private void addSearchPopup(boolean isResetStateFocus) {
        Content genreData = null;
        int musicSeq = BasicUI.SEQ_ALBUM;
        if (uiType < UI_ALL_SONGS) {
            musicSeq = BasicUI.SEQ_ALBUM;
        } else if (uiType < UI_ARTIST) {
            musicSeq = BasicUI.SEQ_SONGS;
        } else if (uiType < UI_GENRE) {
            musicSeq = BasicUI.SEQ_ARTIST;
        } else if (uiType < UI_PLAYLIST) {
            musicSeq = BasicUI.SEQ_GENRES;
            if (uiType == UI_GENRE) {
                genreData = (Content) currentRoot.getChildren().elementAt(photoFocus);
            } else { genreData = currentRoot; }
        } else if (uiType <= UI_NOPLAYLIST) {
            musicSeq = BasicUI.SEQ_PLAYLISTS;
        }
        searchPopup.setListOrder(MENU_MUSIC, musicSeq, genreData);
        if (isResetStateFocus) { searchPopup.resetStateFocus(); }
        addPopup(searchPopup);
    }

    /**
     * Play music.
     */
    private void playMusic() {
        if (uiType == UI_ALBUM_SONGS || uiType == UI_ALL_SONGS || uiType == UI_ARTIST_ALL_SONGS
                || uiType == UI_GENRE_SONGS || uiType == UI_PLAYLIST_SONGS || uiType == UI_ARTIST_SONGS
                || uiType == UI_ARTIST_ALBUM_SONGS) {
            Hashtable errorPages = currentRoot.getErrorPageNums();
            if (errorPages != null && !errorPages.isEmpty()) {
                showConnectErrorPopup();
                return;
            }
        }
        loadData(new Object[]{TH_MUSIC_PLAY}, true);
    }

    /**
     * Gets the music seq.
     *
     * @return the music seq
     */
    private int getMusicSeq() {
        int musicSeq = SEQ_ALBUM;
        if (uiType < UI_ALL_SONGS) {
            musicSeq = SEQ_ALBUM;
        } else if (uiType < UI_ARTIST) {
            musicSeq = SEQ_SONGS;
        } else if (uiType < UI_GENRE) {
            musicSeq = SEQ_ARTIST;
        } else if (uiType < UI_PLAYLIST) {
            musicSeq = SEQ_GENRES;
        } else if (uiType <= UI_NOPLAYLIST) {
            musicSeq = SEQ_PLAYLISTS;
        }
        return musicSeq;
    }

    /**
     * Gets the play title.
     *
     * @param musicSeq the music seq
     * @param parent the parent
     * @return the play title
     */
    private String getPlayTitle(int musicSeq, Content parent) {
        String title = "";
        if (musicSeq == SEQ_SONGS) {
            title = renderer.allSongsTxt;
        } else {
            if (musicSeq == SEQ_ARTIST) {
                /*if (uiType == UI_ARTIST) {
                    title = parent.getName();
                } else {
                    Vector children = parent.getChildren();
                    title = ((Content) children.elementAt(children.size() - 1)).getArtist();
                }*/
                if (uiType == UI_ARTIST_ALBUM || uiType == UI_ARTIST_ALBUM_SONGS) {
                    title += " (" + parent.getName() + ")";
                } else {
                    title += " (" + renderer.allSongsTxt.toLowerCase() + ")";
                }
            } else if (musicSeq == SEQ_GENRES) {
                title = parent.getName() + " (" + renderer.allSongsTxt.toLowerCase() + ")";
            } else {
                title = parent.getName();
            }
        }
        return title;
    }

    /**
     * Change page.
     *
     * @param keyCode the key code
     */
    private void changePage(int keyCode) {
        int totalCount = currentRoot.getChildren().size();
        if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
            if (totalCount > IMAGES_PER_PAGE) {
                int[] num = getInitLastNumberForWall(photoFocus, focusPosition, totalCount, IMAGES_PER_PAGE);
                if (keyCode == Rs.KEY_PAGE_UP && num[0] > 0) {
                    int reqFocus = photoFocus - IMAGES_PER_PAGE;
                    if (num[0] - IMAGES_PER_PAGE < 0) { reqFocus = photoFocus - IMAGES_PER_LINE; }
                    if (reqFocus != photoFocus) { checkFocusData(reqFocus, focusPosition, true); }
                } else if (keyCode == Rs.KEY_PAGE_DOWN && num[1] - num[0] >= IMAGES_PER_PAGE) {
                    int reqFocus = photoFocus + IMAGES_PER_LINE;
                    if ((totalCount - 1) - num[1] > 0) { reqFocus =  photoFocus + IMAGES_PER_PAGE; }
                    if (reqFocus > totalCount - 1) { reqFocus = totalCount - 1; }
                    if (reqFocus != photoFocus) { checkFocusData(reqFocus, focusPosition, true); }
                }
            }
        } else {
            if (totalCount > ROWS_PER_PAGE) {
                if (keyCode == Rs.KEY_PAGE_UP && focusPosition >= MIDDLE_POS) {
                    int initNum = photoFocus - focusPosition;
                    int reqFocus = MIDDLE_POS;
                    if (initNum - ROWS_PER_PAGE >= 0) { reqFocus =  initNum - ROWS_PER_PAGE + MIDDLE_POS; }
                    if (reqFocus != photoFocus) { checkFocusData(reqFocus, MIDDLE_POS, true); }
                } else if (keyCode == Rs.KEY_PAGE_DOWN && focusPosition <= MIDDLE_POS) {
                    int lastNum = 0;
                    lastNum = photoFocus + ROWS_PER_PAGE - 1 - focusPosition;
                    if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                    int reqFocus = totalCount - 1 - MIDDLE_POS;
                    if (lastNum + ROWS_PER_PAGE < totalCount) { reqFocus =  lastNum + ROWS_PER_PAGE - MIDDLE_POS; }
                    if (reqFocus != photoFocus) { checkFocusData(reqFocus, MIDDLE_POS, true); }
                }
            }
        }
    }

    /**
     * D-Option Selected.
     *
     * @param item the item
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }
        if (item.equals(OPT_PHOTO) || item.equals(OPT_MUSIC) || item.equals(OPT_VIDEO)) {
            int newIndex = MENU_PHOTO;
            if (item.equals(OPT_MUSIC)) {
                newIndex = MENU_MUSIC;
            } else if (item.equals(OPT_VIDEO)) { newIndex = MENU_VIDEO; }
            if (newIndex != MENU_MUSIC) {
                goToContentUI(newIndex);
            } else if (uiType != UI_ALBUM) {
                isNewBrowse = true;
                isSceneReset = true;
                changeNode(0, 0, musicRoot[SEQ_ALBUM], UI_ALBUM, Content.SORT_ALPHA_A, true, HISTORY_RESET);
            }
        } else if (item.equals(OPT_SORT_ALPHA_A) || item.equals(OPT_SORT_ALPHA_Z) || item.equals(OPT_SORT_NEWEST)
                || item.equals(OPT_SORT_OLDEST)) {
            int type = 0;
            if (item.equals(OPT_SORT_ALPHA_Z)) {
                type = Content.SORT_ALPHA_Z;
            } else if (item.equals(OPT_SORT_NEWEST)) {
                type = Content.SORT_NEWEST;
            } else if (item.equals(OPT_SORT_OLDEST)) { type = Content.SORT_OLDEST; }
            if (type != sortType) {
                isNewBrowse = true;
                Content reqNode = (Content) currentRoot.clone();
                changeNode(0, 0, reqNode, uiType, type, true, HISTORY_NONE);
            }
        } else { super.selected(item); }
    }

    /**
     * Change node.
     *
     * @param focus the focus
     * @param position the position
     * @param newRoot the new Root node
     * @param newType the music type
     * @param newSortType the new sort type
     * @param isSorting the is sort
     * @param historySettingType the history setting type
     */
    private void changeNode(int focus, int position, Content newRoot, int newType, int newSortType, boolean isSorting,
            int historySettingType) {
        stopTimer();
        setValueForChangeNode(focus, position, newRoot, newType, newSortType, isSorting, historySettingType);
        loadData(new Object[]{TH_CONTENT_LIST}, true);
    }

    /**
     * Sets the value for change node.
     *
     * @param focus the focus
     * @param position the position
     * @param newRoot the new root
     * @param newType the new type
     * @param newSortType the new sort type
     * @param isSorting the is sorting
     * @param historySettingType the history setting type
     */
    private void setValueForChangeNode(int focus, int position, Content newRoot, int newType, int newSortType,
            boolean isSorting, int historySettingType) {
        stopPagingThread();
        tempPhotoFocus = focus;
        tempFocusPosition = position;
        //tempList = newList;
        tempRoot = newRoot;
        tempUIType = newType;
        tempSortType = newSortType;
        isSort = isSorting;
        historySetType = historySettingType;
    }

    /**
     * Change view.
     *
     * @param reqViewType the req view type
     * @param reqUiType the req ui type
     * @param changeId the change id
     */
    private synchronized void changeView(int reqViewType, int reqUiType, int changeId) {
        Logger.debug(this, "called changeView()-reqUiType : " + reqUiType);
        if (SceneManager.getInstance().curSceneId != SceneManager.SC_MUSIC_LIST
                || viewChangeId != changeId || uiType == reqUiType) {
            CommunicationManager.getInstance().hideLoadingAnimation();
            return;
        }
        //isViewChangeLoading = true;
        isNewBrowse = true;
        setValueForChangeNode(0, 0, musicRoot[reqViewType], reqUiType, Content.SORT_ALPHA_A, true, HISTORY_RESET);
        notifyFromDataLoadingThread(new Object[]{TH_CONTENT_LIST, new Integer(changeId)}, false);
        //isViewChangeLoading = false;
    }

    /**
     * Enable sort.
     *
     * @return true, if successful
     */
    public boolean enableSort() {
        return uiType != UI_ALBUM_SONGS && uiType != UI_ARTIST_SONGS && uiType != UI_ARTIST_ALBUM_SONGS;
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(TH_CONTENT_LIST)) {
            processContentList(obj, isFromThread);
            if (searchContent != null) {
                searchContent = null;
                if (!isSceneReset) {
                    addSearchPopup(false);
                }
            }
            if (isSceneReset) { SceneManager.getInstance().scenes[previousSceneID].clearTempData(); }
            isSceneReset = false;
        } else if (procId.equals(TH_MUSIC_PLAY)) {
            notifyFromThreadForPlay();
        }
    }

    /**
     * Process content list.
     *
     * @param obj the obj
     * @param isFromThread the is from thread
     */
    protected void processContentList(Object[] obj, boolean isFromThread) {
        if (isSceneReset) { resetRootChildren(); }
        stopTimer();
        Vector result = null;
        int reqDataFocus = tempPhotoFocus;
        if (isTempNode(tempUIType)) { reqDataFocus--; }
        int pageNum = getPageNumber(reqDataFocus);
        int startIndex = pageNum * Config.loadDataNumber;
        if (tempRoot != null && (isNewBrowse || tempRoot.getChildren() == null)) {
            Logger.debug(this, "tempRoot : " + tempRoot);
            boolean prepare = HNHandler.getInstance().prepareChildNode(tempRoot.getId(),
                    tempRoot, MENU_MUSIC, tempSortType);
            if (prepare) {
                if (!isNewBrowse && tempPhotoFocus > 0 && reqDataFocus >= tempRoot.getChildCount()) {
                    tempPhotoFocus = 0;
                    tempFocusPosition = 0;
                    pageNum = 0;
                    startIndex = 0;
                }
                result = HNHandler.getInstance().setChildNode(tempRoot, MENU_MUSIC, tempSortType, startIndex,
                        Config.loadDataNumber);
                if (result != null && !isNewBrowse && tempPhotoFocus > 0) {
                    int index = reqDataFocus - startIndex;
                    if (index >= result.size()
                            || !((Content) result.elementAt(index)).getId().equals(tempRoot.getLastFocusId())) {
                        tempPhotoFocus = 0;
                        tempFocusPosition = 0;
                        pageNum = 0;
                        startIndex = 0;
                        result = HNHandler.getInstance().setChildNode(tempRoot, MENU_MUSIC, tempSortType,
                                startIndex, Config.loadDataNumber);
                    }
                }
            }
            Logger.debug(this, "notifyFromDataLoadingThread()-result : " + result);
            Content.sortType = tempSortType;
            tempRoot.setLastFocus(tempPhotoFocus);
            tempRoot.setErrorPageNums(null);
            tempRoot.setBrowsePageNums(null);
            if (!checkVisible()) { return; }
            int[] numbers = getNumberForPage(tempRoot.getChildCount(), pageNum);
            if (result == null || result.size() != numbers[1]) { result = null; }
            if (isNewBrowse && tempUIType == UI_ARTIST_SONGS && result != null && !result.isEmpty()) {
                tempRoot = (Content) result.elementAt(0);
                startIndex = 0;
                prepare = HNHandler.getInstance().prepareChildNode(tempRoot.getId(),
                        tempRoot, MENU_MUSIC, tempSortType);
                result = null;
                if (prepare) {
                    result = HNHandler.getInstance().setChildNode(tempRoot, MENU_MUSIC, tempSortType, startIndex,
                            Config.loadDataNumber);
                }
                Logger.debug(this, "notifyFromDataLoadingThread()-result : " + result);
                if (!checkVisible()) { return; }
            }
            isNewBrowse = true;
            if (result != null) {
                Vector children = new Vector();
                children.setSize(tempRoot.getChildCount());
                for (int i = 0; i < result.size(); i++) {
                    children.setElementAt(result.elementAt(i), startIndex + i);
                }
                if (isNewBrowse && isTempNode(tempUIType)) {
                    children.add(0, null);
                }
                tempRoot.setChildren(children);
                Hashtable browsePages = new Hashtable();
                browsePages.put(String.valueOf(pageNum), "");
                tempRoot.setBrowsePageNums(browsePages);
                tempRoot.setLastLoadedPage(pageNum + 1);
                Content lastNode = (Content) children.elementAt(tempPhotoFocus);
                tempRoot.setLastFocusId(lastNode != null ? lastNode.getId() : null);
                //
                int reqUiType = UI_LIST;
                if (tempUIType == MusicListUI.UI_ALBUM || tempUIType == MusicListUI.UI_ARTIST_ALBUM) {
                    reqUiType = UI_WALL;
                }
                if (!checkListPage(tempPhotoFocus, tempFocusPosition, tempRoot, MENU_MUSIC, tempSortType, reqUiType)) {
                    result = null;
                }
            }
        }
        //for view change
        if (!isFromThread && obj.length > 1) {
            Integer changeValue = (Integer) obj[1];
            if (changeValue != null && changeValue.intValue() != viewChangeId) {
                startTimerForFolder();
                if (tempRoot != null) { tempRoot.setChildren(null); }
                return;
            }
        }
        if (tempRoot == null || (!isNewBrowse && tempRoot.getChildren() == null)
                || (isNewBrowse && result == null)) {
            if (tempUIType != UI_PLAYLIST) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                showConnectErrorPopup();
                lastChangeUiType = -1;
                viewChangeId = -1;
                isSceneReset = false;
                if (tempRoot != null) {
                    Hashtable errorPages = tempRoot.getErrorPageNums();
                    if (errorPages == null) { errorPages = new Hashtable(); }
                    errorPages.put(String.valueOf(pageNum), "");
                    tempRoot.setErrorPageNums(errorPages);
                }
                if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                    setDebugData(tempRoot, pageNum + 1);
                }
                tempRoot = null;
                return;
            } else { tempUIType = UI_NOPLAYLIST; }
        }
        //for all focus
        if (historySetType == HISTORY_ADD) {
            historyStack.push(new Object[]{currentRoot,
                    new int[]{photoFocus, focusPosition, uiType, sortType}});
        } else if (historySetType == HISTORY_RESET) {
            //in the case of moving by popup, if back key is pressed UI show home(album).
            if (!historyStack.isEmpty()) {
                Object albumHistory = historyStack.firstElement();
                historyStack.clear();
                if (tempUIType != UI_ALBUM) { historyStack.push(albumHistory); }
            } else { //in the case of Album
                if (tempUIType != UI_ALBUM) {
                    historyStack.push(new Object[]{currentRoot,
                            new int[]{photoFocus, focusPosition, uiType, sortType}});
                }
            }
        }
        CommunicationManager.getInstance().hideLoadingAnimation();
        if (!checkVisible()) { return; }
        listTicker.stop();
        remove(listTicker);
        uiType = tempUIType;
        sortType = tempSortType;
        tempRoot.setChildSort(tempSortType);
        Content oldRoot = currentRoot;
        //
        currentRoot = tempRoot;
        photoFocus = tempPhotoFocus;
        focusPosition = tempFocusPosition;
        if (uiType != tempUIType) {
            String title = renderer.contentTxt[tempUIType];
            if (tempUIType == UI_ARTIST_ALBUM_SONGS || tempUIType == UI_ARTIST_ALL_SONGS) {
                title = ((Content) result.elementAt(0)).getArtist();
            }
            renderer.resetLocationTitle(title);
        }
        if (enableSort()) {
            rootMenuItem = fullMenuItem;
        } else { rootMenuItem = menuItem; }

        Content flushNode = oldRoot;
        if (oldRoot == currentRoot || (isSceneReset && oldRoot != null)) { flushNode = null; }
        if (oldRoot != null && oldRoot.getId().equals(currentRoot.getId())) { flushNode = null; }
        if (oldRoot != null && (oldRoot != currentRoot || oldRoot.getParent() == currentRoot)) {
            oldRoot.setChildren(null);
            flushNode = null;
        }
        if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
            setDebugData(tempRoot, pageNum + 1);
        }
        flushPhotoImgs(flushNode);
        if (!isFromThread && obj.length > 1 && oldRoot != currentRoot) {
            Content parent = oldRoot;
            while (parent.getParent() != null) {
                parent = parent.getParent();
            }
            parent.setChildren(null);
        }
        setTicker();
        loadInitPagingData(currentRoot, reqDataFocus, MENU_MUSIC);
        repaint();
        Logger.debug(this, "notifyFromDataLoadingThread()-tempRoot : " + tempRoot);
        Logger.debug(this, "notifyFromDataLoadingThread()-currentRoot : " + currentRoot);
        if (tempUIType != UI_NOPLAYLIST) { setPosterImages(); }
        startTimerForFolder();
        CommunicationManager.getInstance().hideLoadingAnimation();
    }

    /**
     * Notify from thread for play.
     */
    private void notifyFromThreadForPlay() {
        Content musicContent = null;
        int musicFocus = 0;
        boolean isRandom = false;
        if (uiType == UI_ALBUM_SONGS || uiType == UI_ALL_SONGS || uiType == UI_ARTIST_ALL_SONGS
                || uiType == UI_GENRE_SONGS || uiType == UI_PLAYLIST_SONGS || uiType == UI_ARTIST_SONGS
                || uiType == UI_ARTIST_ALBUM_SONGS) {
            if (currentRoot.getChildren().size() > 1) {
                musicFocus = photoFocus - 1;
                if (photoFocus == 0) {
                    isRandom = true;
                    musicFocus = 0;
                }
                musicContent = (Content) currentRoot.clone();
            }
        } else if (((uiType == UI_ARTIST_ALBUM || uiType == UI_ALBUM) && photoFocus > 0)
                || uiType == UI_ARTIST || uiType == UI_GENRE || uiType == UI_PLAYLIST) {
            Content focusContent = (Content) currentRoot.getChildren().elementAt(photoFocus);
            musicContent = (Content) focusContent.clone();
        }
        if (musicContent == null) {
            Logger.debug(this, "notifyFromThreadForPlay()-musicContent is null. so return.");
            return;
        }
        musicContent.setChildren(null);
        int musicSeq = getMusicSeq();
        if (uiType == UI_ARTIST) {
            musicContent.setArtistAllsongsDir(true);
            int childCount = HNHandler.getInstance().getChildCountForArtist(musicContent.getId(),
                    Content.SORT_ALPHA_A);
            if (childCount == -1) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                showConnectErrorPopup();
                return;
            } else { musicContent.setChildCount(childCount); }
        }
        String playTitle = getPlayTitle(musicSeq, musicContent);
        boolean isPlay = musicPlayerPopup.playMusic(musicContent, musicFocus, isRandom, musicSeq, playTitle,
                musicSeq == BasicUI.SEQ_ARTIST);
        CommunicationManager.getInstance().hideLoadingAnimation();
        if (isPlay) {
            if (!checkVisible()) { return; }
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    musicPlayerPopup.resetFocus();
                    addPopup(musicPlayerPopup);
                }
            });
        } else { showConnectErrorPopup(); }
    }

    /**
     * Sets the ticker.
     */
    private void setTicker() {
        if (uiType == UI_ALBUM || uiType == UI_ARTIST_ALBUM) {
            listTicker.setFont(Rs.F16);
            listTicker.setTextColor(Rs.C255203000);
            listTicker.setTextPosY(261 - 250);
            int addX = (photoFocus % IMAGES_PER_LINE) * 164;
            int addY = (focusPosition / IMAGES_PER_LINE) * 177;
            int addHeight = 0;
            if (uiType == UI_ALBUM) { addHeight += 12; }
            listTicker.setBounds(90 + addX, 250 + addY, 215 - 90, 263 - 250 + addHeight);
            listTicker.setAlign(AutoScroller.ALIGN_CENTER);
        } else {
            listTicker.setFont(Rs.F19);
            listTicker.setTextColor(Rs.C0);
            listTicker.setTextPosY(326 - 302);
            listTicker.setAlign(AutoScroller.ALIGN_LEFT);
            int width = 510 - 69;
            if (uiType == UI_ALL_SONGS || uiType == UI_PLAYLIST_SONGS) {
                width = 320 - 69;
            } else if (uiType == UI_GENRE_SONGS) { width = 421 - 69 - 5; }
            listTicker.setBounds(69, 174 + (focusPosition * 32), width, 36);
        }
        String text = null;
        String addedText = null;
        Vector list = currentRoot.getChildren();
        if (list != null && !list.isEmpty()) {
            Content content = (Content) list.elementAt(photoFocus);
            if (content != null) {
                text = content.getName();
                if (uiType == UI_ALBUM) {
                    addedText = text;
                    text = content.getArtist();
                    addedText += renderer.getDate(content);
                } else if (uiType == UI_ARTIST_ALBUM) {
                    text += renderer.getDate(content);
                } else if (uiType == UI_ALBUM_SONGS || uiType == UI_ALL_SONGS || uiType == UI_ARTIST_SONGS
                        || uiType == UI_ARTIST_ALBUM_SONGS || uiType == UI_ARTIST_ALL_SONGS || uiType == UI_GENRE_SONGS
                        || uiType == UI_PLAYLIST_SONGS) {
                    text = photoFocus + "." + "  " + text;
                    if (photoFocus < 10) { text = "0" + text; }
                } else if (uiType == UI_PLAYLIST) { text += " (" + content.getTotalCount() + ")"; }
            }
        }
        listTicker.setText(text, addedText);
        add(listTicker);
        //repaint();
    }

    /**
     * Flush photo imgs.
     *
     * @param oldRoot the old root
     */
    private void flushPhotoImgs(Content oldRoot) {
        Logger.debug(this, "called flushPhotoImgs()");
        conImgPool.clear();
        if (oldRoot == null) { return; }
        Vector oldList = oldRoot.getChildren();
        for (int i = 0; oldList != null && i < oldList.size(); i++) {
            Content content = (Content) oldList.elementAt(i);
            if (content != null) { content.setIconImage(null); }
        }
    }

    /**
     * Sets the list data.
     *
     * @param focus the focus
     * @param position the position
     * @param reqRoot the req root
     * @param reqUiType the req ui type
     * @param reqSortType the req sort type
     */
    private synchronized void setListData(int focus, int position, Content reqRoot, int reqUiType, int reqSortType) {
        if (reqRoot == null) { return; }
        if (!checkFocus(focus, reqUiType, reqRoot)) { return; }
        if (reqUiType != UI_PLAYLIST && reqUiType != UI_ARTIST) { return; }
        int[] numbers =
            BasicUI.getInitLastNumber(focus, position, reqRoot.getChildren().size(), ROWS_PER_PAGE);
        for (int i = numbers[0]; i <= numbers[1]; i++) {
            if (!checkFocus(i, reqUiType, reqRoot, true)) { break; }
            Content node = (Content) reqRoot.getChildren().elementAt(i);
            if (node != null) {
                if (reqUiType == UI_PLAYLIST && node.getDuration() == 0) {
                    long totalDuration = 0;
                    int currentCount = 0;
                    int totalCount = node.getChildCount();
                    while (true) {
                        if (!checkFocus(i, reqUiType, reqRoot, true)) { break; }
                        long durations = HNHandler.getInstance().getSumDuration(node, currentCount);
                        playlistDurationCount += Config.loadDataNumber;
                        if (playlistDurationCount > GC_COUNT_PLAYLIST) {
                            SceneManager.getInstance().executeGC();
                            playlistDurationCount = 0;
                        }
                        totalDuration += durations;
                        currentCount += Config.loadDataNumber;
                        if (currentCount >= totalCount) {
                            node.setDuration(totalDuration);
                            break;
                        }
                        try {
                            Thread.sleep(50L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!checkFocus(i, reqUiType, reqRoot, true)) { break; }
                    repaint();
                } else if (reqUiType == UI_ARTIST && node.getSongsCount() == 0) {
                    int totalSongCount = 0;
                    int currentCount = 0;
                    int totalCount = node.getChildCount();
                    while (true) {
                        if (!checkFocus(i, reqUiType, reqRoot, true)) { break; }
                        int count =
                            HNHandler.getInstance().setSongsCountForAtrist(node, reqSortType, currentCount);
                        totalSongCount += count;
                        currentCount += Config.loadDataNumber;
                        if (currentCount >= totalCount) {
                            node.setSongsCount(totalSongCount);
                            break;
                        }
                        try {
                            Thread.sleep(50L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!checkFocus(i, reqUiType, reqRoot, true)) { break; }
                    repaint();
                }
            }
        }
    }

    /**
     * Sets the poster images.
     */
    private void setPosterImages() {
        final int focus = photoFocus;
        final int position = focusPosition;
        final Content reqRoot = currentRoot;
        final int reqUiType = uiType;
        final int reqSortType = sortType;
        if (reqUiType == UI_PLAYLIST || reqUiType == UI_ARTIST) {
            new Thread("MusicListUI|setListData()") {
                public void run() {
                    setListData(focus, position, reqRoot, reqUiType, reqSortType);
                }
            } .start();
        }

        new Thread("MusicListUI|setPosterImage()") {
            public void run() {
                if (reqRoot == null) { return; }
                if (!checkFocus(focus, reqUiType, reqRoot)) { return; }
                if (reqUiType == UI_ALBUM || reqUiType == UI_ARTIST_ALBUM) {
                    int[] numbers =
                        getInitLastNumberForWall(focus, position, reqRoot.getChildren().size(), IMAGES_PER_PAGE);
                    try {
                        Thread.sleep(500L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!checkFocus(focus, reqUiType, reqRoot)) { return; }
                    for (int i = numbers[0]; i <= numbers[1]; i++) {
                        if (!checkFocus(i, reqUiType, reqRoot)) { break; }
                        Content node = (Content) reqRoot.getChildren().elementAt(i);
                        if (node != null) {
                            String artist = node.getArtist();
                            if (artist == null || artist.length() == 0) {
                                HNHandler.getInstance().getMusicInfoForAlbum(node);
                            }
                        }
                    }
                    for (int i = numbers[0]; i <= numbers[1]; i++) {
                        if (!checkFocus(i, reqUiType, reqRoot)) { break; }
                        Content node = (Content) reqRoot.getChildren().elementAt(i);
                        if (node == null) { continue; }
                        if (node != null && node.getIconImage() == null) {
                            int[] size = SIZE_WALL_FILE;
                            String key = node.getId() + node.getIconUrl();
                            Image img = MusicListUI.this.getContentImage(key, node.getIconUrl(), null, size);
                            conImgPool.waitForAll();
                            node.setIconImage(img);
                            if (!checkVisible()) { return; }
                            if (checkFocus(i, reqUiType, reqRoot)) { repaint(); }
                        }
                        try {
                            if (i < numbers[1]) { Thread.sleep(100L); }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Vector list = reqRoot.getChildren();
                    if (list == null) { return; }
                    Content node = (Content) list.elementAt(focus);
                    if (node != null && node.getIconImage() == null) {
                        String iconUrl = node.getIconUrl();
                        int[] size = SIZE_LIST_FILE;
                        String key = node.getId() + iconUrl;
                        Image img = conImgPool.getImage(key);
                        if (img != null) {
                            node.setIconImage(img);
                        } else {
                            try {
                                Thread.sleep(500L);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (!checkFocus(focus, reqUiType, reqRoot)) { return; }
                            if (iconUrl == null && (reqUiType == UI_GENRE || reqUiType == UI_ARTIST
                                    || reqUiType == UI_PLAYLIST)) {
                                iconUrl = HNHandler.getInstance().requestSearchPhotoUrl(node.getId(),
                                        reqSortType, MENU_MUSIC, false);
                                node.setIconUrl(iconUrl);
                            }
                            if (!checkFocus(focus, reqUiType, reqRoot)) { return; }
                            key = node.getId() + iconUrl;
                            img = MusicListUI.this.getContentImage(key, iconUrl, null, size);
                            conImgPool.waitForAll();
                            node.setIconImage(img);
                        }
                        if (!checkVisible()) { return; }
                        if (checkFocus(focus, reqUiType, reqRoot)) { repaint(); }
                    }
                }
                if (SceneManager.getInstance().curSceneId == SceneManager.SC_INVALID) {
                    conImgPool.clear();
                    return;
                }
            }
        } .start();
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @param reqUiType the ui type
     * @param reqRoot the requested Root node
     * @return true, if successful
     */
    private boolean checkFocus(int reqIndex, int reqUiType, Content reqRoot) {
        boolean isAll = false;
        if (reqUiType == UI_ALBUM || reqUiType == UI_ARTIST_ALBUM) { isAll = true; }
        return checkFocus(reqIndex, reqUiType, reqRoot, isAll);
    }

    /**
     * Check focus for all list.
     *
     * @param reqIndex the request index
     * @param reqUiType the req ui type
     * @param reqRoot the requested Root node
     * @param isAll the is all
     * @return true, if successful
     */
    private boolean checkFocus(int reqIndex, int reqUiType, Content reqRoot, boolean isAll) {
        if (SceneManager.getInstance().curSceneId != SceneManager.SC_MUSIC_LIST
                || uiType != reqUiType || currentRoot != reqRoot || currentRoot == null) {
            return false;
        }
        if (isAll) {
            int totalCount = reqRoot.getChildren().size();
            int[] numbers = getInitLastNumberForWall(photoFocus, focusPosition, totalCount, IMAGES_PER_PAGE);
            return reqIndex >= numbers[0] && reqIndex <= numbers[1];
        } else { return reqIndex == photoFocus; }
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof MusicBrowsingPopup) {
            final int viewType = ((Integer) para).intValue();
            final int reqUiType = getUIType(viewType);
            final int chaingId = ++viewChangeId;
            if (reqUiType == lastChangeUiType) { return; }
            lastChangeUiType = reqUiType;
            new Thread("MusicBrowsing") {
                public void run() {
                    try {
                        Thread.sleep(300L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    changeView(viewType, reqUiType, chaingId);
                }
            } .start();
        } else if (popup instanceof NotiPopup) {
            removePopup();
            if (currentRoot == null) {
                SceneManager.getInstance().goToScene(previousSceneID, false, null);
            } else {
                loadInitPagingData(currentRoot, photoFocus, MENU_MUSIC);
                startTimerForFolder();
                setTicker();
                repaint();
            }
        } else if (popup instanceof MusicPlayerPopup) {
            int[] value = (int[]) para;
            removePopup();
            if (value != null && value[0] == MusicPlayerPopup.UI_SELECT_MUSIC && value[1] == 0) {
                if (uiType != UI_ALBUM) {
                    isNewBrowse = true;
                    changeNode(0, 0, musicRoot[SEQ_ALBUM], UI_ALBUM, Content.SORT_ALPHA_A, true, HISTORY_RESET);
                }
            }
        } else if (popup instanceof SearchPopup) {
            removePopup();
            if (para != null) {
                goToSearchContent((Content) para);
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * close the popup by normal popup.
     */
    public void requestPopupClose() {
        Popup curPopup = popup;
        removePopup();
        if (curPopup instanceof MusicBrowsingPopup) {
            if (lastChangeUiType != -1 && lastChangeUiType != uiType
                    && (lastChangeUiType != UI_PLAYLIST || uiType != UI_NOPLAYLIST)) {
                CommunicationManager.getInstance().showLoadingAnimation();
            }
            loadInitPagingData(currentRoot, photoFocus, MENU_MUSIC);
            //if (isViewChangeLoading) { CommunicationManager.getInstance().showLoadingAnimation(); }
        }
    }

    /**
     * Gets the uI type.
     *
     * @param musicSeq the music seq
     * @return the uI type
     */
    private int getUIType(int musicSeq) {
        int newUIType = UI_ALBUM;
        if (musicSeq == MusicBrowsingPopup.SEQ_ARTIST) {
            newUIType = UI_ARTIST;
        } else if (musicSeq == MusicBrowsingPopup.SEQ_SONGS) {
            newUIType = UI_ALL_SONGS;
        } else if (musicSeq == MusicBrowsingPopup.SEQ_GENRES) {
            newUIType = UI_GENRE;
        } else if (musicSeq == MusicBrowsingPopup.SEQ_PLAYLISTS) {
            newUIType = UI_PLAYLIST;
        }
        return newUIType;
    }

    /**
     * Sets the debug data.
     *
     * @param reqNode the new debug data
     * @param focusPage the focus page
     */
    public void setDebugData(Content reqNode, int focusPage) {
        LogDisplayer.getInstance().resetData();
        Content debugNode = reqNode;
        if (debugNode == null) { debugNode = currentRoot; }
        if (debugNode == null) { return; }
        if (focusPage < 0) { focusPage = getPageNumber(photoFocus) + 1; }

        LogDisplayer.getInstance().setInitData(debugNode.getId(), debugNode.getName(),
                getLastPageNumber(debugNode.getChildCount()) + 1);
        LogDisplayer.getInstance().setFocusPage(focusPage);
        LogDisplayer.getInstance().setLastLoadedPage(debugNode.getLastLoadedPage());
        LogDisplayer.getInstance().setLastRemovedPage(debugNode.getLastRemovedPage());
        LogDisplayer.getInstance().setLoadedPages(debugNode.getBrowsePageNums());
        LogDisplayer.getInstance().setErrorPages(debugNode.getErrorPageNums());
    }
}

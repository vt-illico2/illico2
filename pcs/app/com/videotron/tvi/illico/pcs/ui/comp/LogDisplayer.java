package com.videotron.tvi.illico.pcs.ui.comp;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.pcs.common.Rs;

/**
 * This class is the LogDisplayer class to display logs.
 *
 * @since 2011.03.25
 * @version $Revision: 1.1 $ $Date: 2012/09/18 02:43:37 $
 * @author tklee
 */

public final class LogDisplayer  extends Component {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** Instance of CacheManager. */
    private static LogDisplayer instance = null;

    /** The KE y_ set. */
    public static final String KEY_SET = "00000";

    /** The is log show. */
    public boolean isLogShow = false;
    /** The key. */
    public String key = "";

    /** The entry id. */
    private String entryId = "";
    /** The entry name. */
    private String entryName = "";
    /** The total page. */
    private int totalPage = 0;
    /** The focus page. */
    private int focusPage = 0;
    /** The last loaded page. */
    private int lastLoadedPage = 0;
    /** The last removed page. */
    private int lastRemovedPage = 0;
    /** The loaded pages. */
    private Hashtable loadedPages = null;
    /** The error pages. */
    private Hashtable errorPages = null;

    /**
     * Gets the singleton instance of MainManager.
     * @return CacheManager
     */
    public static synchronized LogDisplayer getInstance() {
        if (instance == null) { instance = new LogDisplayer(); }
        return instance;
    }

    /**
     * Instantiates a new log displayer.
     */
    private LogDisplayer() {
        setBounds(Rs.SCENE_BOUND);
    }

    /**
     * Show.
     *
     * @param rootContainer the root container
     */
    public void show(Container rootContainer) {
        LogDisplayer.getInstance().isLogShow = true;
        LogDisplayer.getInstance().key = "";
        rootContainer.add(this, 0);
        rootContainer.repaint();
    }

    /**
     * Hide.
     *
     * @param rootContainer the root container
     */
    private void hide(Container rootContainer) {
        LogDisplayer.getInstance().isLogShow = false;
        LogDisplayer.getInstance().key = "";
        rootContainer.remove(this);
        rootContainer.repaint();
    }

    /**
     * Stop.
     *
     * @param rootContainer the root container
     */
    public void stop(Container rootContainer) {
        hide(rootContainer);
        resetData();
    }

    /**
     * Reset data.
     */
    public void resetData() {
        entryId = "";
        entryName = "";
        totalPage = 0;
        focusPage = 0;
        lastLoadedPage = 0;
        lastRemovedPage = 0;
        loadedPages = null;
        errorPages = null;
    }

    /**
     * Sets the init data.
     *
     * @param id the id
     * @param name the name
     * @param totalPageNum the total page num
     */
    public void setInitData(String id, String name, int totalPageNum) {
        entryId = id;
        entryName = name;
        totalPage = totalPageNum;
    }

    /**
     * Sets the focus page.
     *
     * @param pageNum the new focus page
     */
    public void setFocusPage(int pageNum) {
        focusPage = pageNum;
    }

    /**
     * Sets the last loaded page.
     *
     * @param pageNum the loadingPage to set
     */
    public void setLastLoadedPage(int pageNum) {
        lastLoadedPage = pageNum;
        //loadedPages.addElement(String.valueOf(pageNum));
    }

    /**
     * Sets the last removed page.
     *
     * @param pageNum the lastRemovedPage to set
     */
    public void setLastRemovedPage(int pageNum) {
        lastRemovedPage = pageNum;
        /*for (int i = 0; i < loadedPages.size(); i++) {
            int num = Integer.parseInt((String) loadedPages.elementAt(i));
            if (num == pageNum) {
                loadedPages.removeElementAt(i);
                break;
            }
        }*/
    }

    /**
     * Sets the error page.
     *
     * @param pages the new error pages
     */
    public void setErrorPages(Hashtable pages) {
        errorPages = pages;
        /*if (errorPages.length() > 0) {
            errorPages += ", ";
        } else { errorPages = String.valueOf(pageNum); }*/
    }

    /**
     * Sets the loaded pages.
     *
     * @param pages the new loaded pages
     */
    public void setLoadedPages(Hashtable pages) {
        loadedPages = pages;
        /*if (errorPages.length() > 0) {
            errorPages += ", ";
        } else { errorPages = String.valueOf(pageNum); }*/
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     */
    public void paint(Graphics g) {
        g.setColor(Rs.DVB60C255);
        g.fillRect(0, 0, 960, 540);
        g.setFont(Rs.F24);
        //g.setColor(Rs.C253203000);
        g.setColor(Rs.C55);
        g.drawString("PCS Debug UI - You can close this UI using Exit Key", 100, 70);
        g.drawString("Container Id : " + entryId, 100, 100);
        g.drawString("Container Name : " + entryName, 100, 130);
        g.drawString("Total Page : " + totalPage, 100, 160);
        g.drawString("Focused Page : " + focusPage, 100, 190);
        g.drawString("Last Loaded Page : " + lastLoadedPage, 100, 220);
        g.drawString("Last Removed Page : " + lastRemovedPage, 100, 250);
        /*for (int i = 0; loadedPages != null && i < loadedPages.size(); i++) {
            String page = (String) loadedPages.elementAt(i);
            if (i > 0) { str += ", "; }
            str += page;
        }*/
        g.drawString("Loaded Pages : " + getPageTxt(loadedPages), 100, 280);
        g.drawString("Error Pages : " + getPageTxt(errorPages), 100, 310);
    }

    /**
     * Gets the page txt.
     *
     * @param table the table
     * @return the page txt
     */
    private String getPageTxt(Hashtable table) {
        String str = "";
        if (table != null) {
            Enumeration keys = table.keys();
            for (int i = 0; keys.hasMoreElements(); i++) {
                int page = Integer.parseInt((String) keys.nextElement()) + 1;
                if (i > 0) { str += ", "; }
                str += page;
            }
        }
        return str;
    }
}

package com.videotron.tvi.illico.pcs.ui.comp;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.MainManager;

/**
 * An image pool. This class caches images by name for content.
 *
 *
 * @version $Revision: 1.16 $ $Date: 2012/09/18 02:43:37 $
 * @author tklee
 */
public class ContentImagePool extends Hashtable {
    /** serialVersionUID. */
    private static final long serialVersionUID = -727135816389965869L;

    /** Default media tracker id for image file. */
    //tklee
    //private static final int TRACKER_ID_FILE = 1001;

    /** Toolkit to decode image. */
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    /** MediaTracker. */
    private MediaTracker tracker = new MediaTracker(new Container());
    /** The new buffered img for rotation. */
    private DVBBufferedImage newBufferedImg = null;

    /**
     * Creates Image with bytes source and store Image with specified key.
     *
     * @param data source bytes of image file.
     * @param key hashtable key for the this Image instance.
     * @param size the size
     * @return Image instance.
     */
    /*public Image getImage(String key, byte[] data, int[] size) {
        return getImage(key, data, size, key);
    }*/

    /**
     * Creates Image with bytes source and store Image with specified key.
     *
     * @param key hashtable key for the this Image instance.
     * @param data source bytes of image file.
     * @param size the size
     * @param imgUrl the img url
     * @return Image instance.
     */
    public Image getImage(String key, byte[] data, int[] size, String imgUrl) {
        if (key == null) { return null; }
        Object value = super.get(key);
        if (value != null) { return (Image) value; }
        //if (data == null) { return null; }

        Image image = null;
        if (data != null) {
            image = toolkit.createImage(data);
        } else {
            try {
                image = toolkit.getImage(new URL(imgUrl));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (image == null) {
            Logger.debug(this, "ImagePool fail to create image : " + key);
            return null;
        }
        try {
            int imgWidth = image.getWidth(null);
            int imgHeight = image.getHeight(null);
            Logger.debug(this, "getImage()-imgWidth : " + imgWidth);
            Logger.debug(this, "getImage()-imgHeight : " + imgHeight);
            if (imgWidth <= 0 || imgHeight <= 0) {
                MainManager.getInstance().printError(MainManager.ERR_NO_PLAY_RETRIEVE,
                        "Unalble to show image : " + imgUrl);
                return null;
            } else {
                int[] imgScaledSize = getScaledSize(imgWidth, imgHeight, size);
                Logger.debug(this, "getImage()-imgScaledSize[0] : " + imgScaledSize[0]);
                Logger.debug(this, "getImage()-imgScaledSize[1] : " + imgScaledSize[1]);
                image = image.getScaledInstance(imgScaledSize[0], imgScaledSize[1], Image.SCALE_DEFAULT);
            }
            //put(key + size[0] + "X" + size[1], image);
            put(key, image);
            //tklee
            //tracker.addImage(image, TRACKER_ID_FILE);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return image;
    }

    /**
     * Gets the scaled size.
     *
     * @param imgWidth the img width
     * @param imgHeight the img height
     * @param size the size
     * @return the scaled size
     */
    private int[] getScaledSize(int imgWidth, int imgHeight, int[] size) {
        double wRatio = (double) imgWidth / (double) (size[0]); //1.9958333333333333333333333
        double hRatio = (double) imgHeight / (double) (size[1]); //2
        double divideValue = hRatio; // 2
        if (wRatio >= hRatio) { divideValue = wRatio; }
        return new int[] {(int) (imgWidth / divideValue), (int) (imgHeight / divideValue)}; //958, 540
    }

    /**
     * Gets the rotate image.
     *
     * @param image the image
     * @return the rotate image
     */
    public Image getRotateImage(Image image) {
        if (image == null) { return null; }
        int imgW = image.getWidth(null);
        int imgH = image.getHeight(null);
        int size = imgW * imgH;
        int[] pixels1 = new int[size];
        PixelGrabber pg = new PixelGrabber(image, 0, 0, imgW, imgH, pixels1, 0, imgW);
        try {
            pg.grabPixels();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Image rotateImage = null;
        try {
            //remove("rotate");
            int[] pixels2 = new int[size];
            int idx = 0;
            for (int x = 0; x < imgW; x++) {
                for (int y = imgH - 1; y >= 0; y--) {
                    pixels2[idx++] = pixels1[y * imgW + x];
                }
            }
            pixels1 = null;
            rotateImage = toolkit.createImage(new MemoryImageSource(imgH, imgW, pixels2, 0, imgH));
            //rotateImage = rotateImage.getScaledInstance(imgH, imgW, Image.SCALE_DEFAULT);
            pixels2 = null;
            //tklee
            tracker.addImage(rotateImage, ImagePool.DEFAULT_ID);
            tracker.waitForID(ImagePool.DEFAULT_ID);
            tracker.removeImage(rotateImage);
            remove("rotate");
            put("rotate", rotateImage);
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error err) {
            err.printStackTrace();
        }
        Logger.debug(this, "getRotateImage()-img : " + image);
        return rotateImage;
    }

    /**
     * Gets the rotate image.
     *
     * @param oldImg the old img
     * @param c the Container
     * @return the rotate image
     */
    public Image getRotateImage(Image oldImg, Container c) {
        try {
            int width = oldImg.getWidth(null);
            int height = oldImg.getHeight(null);
            Logger.debug(this, "getRotateImage()-width : " + width + ", height : " + height);
            DVBBufferedImage oldBufferedImg = null;
            if (oldImg instanceof DVBBufferedImage) {
                oldBufferedImg = (DVBBufferedImage) oldImg;
            } else {
                oldBufferedImg = new DVBBufferedImage(width, height);
                Graphics g = oldBufferedImg.getGraphics();
                g.drawImage(oldImg, 0, 0, c);
            }
            Logger.debug(this, "getRotateImage()-oldBufferedImg : " + oldBufferedImg);
            newBufferedImg = new DVBBufferedImage(height, width);
            int[] srcPixels = new int[width * height];
            int[] dstPixels = new int[width];
            oldBufferedImg.getRGB(0, 0, width, height, srcPixels, 0, width);
            for (int h = 0; h < height; ++h) {
                System.arraycopy(srcPixels, h * width, dstPixels, 0, width);
                newBufferedImg.setRGB(height - 1 - h, 0, 1, width, dstPixels, 0, 1);
            }
            Logger.debug(this, "getRotateImage()-newBufferedImg : " + newBufferedImg);
            Logger.debug(this, "getRotateImage()-success!!!!!!!!!!");
            //tklee
            tracker.addImage(newBufferedImg, ImagePool.DEFAULT_ID);
            tracker.waitForID(ImagePool.DEFAULT_ID);
            Logger.debug(this, "getRotateImage()-completed waitForID from tracker");
            tracker.removeImage(newBufferedImg);
            Logger.debug(this, "getRotateImage()-completed to remove the image from tracker");
            remove("rotate");
            Logger.debug(this, "getRotateImage()-completed to remove the key of rotate");
            put("rotate", newBufferedImg);
            Logger.debug(this, "getRotateImage()-completed to put the key of rotate");
            return newBufferedImg;
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Error err) {
            err.printStackTrace();
        }
        return null;
    }


    /**
     * Gets the image.
     *
     * @param key the key
     * @param size the size
     * @return the image
     */
    /*public Image getImage(String key, int[] size) {
        if (key == null) { return null; }
        Object value = super.get(key + size[0] + "X" + size[1]);
        return (Image) value;
    }*/
    public Image getImage(String key) {
        if (key == null) { return null; }
        Object value = super.get(key);
        return (Image) value;
    }

    /**
     * Starts loading all images tracked by this ImagePool. This method waits
     * until all the images being tracked have finished loading.
     *
     * @see MediaTracker#waitForAll
     */
    public void waitForAll() {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Starts loading all images tracked by this ImagePool with specified id.
     * This method waits until all the images with the specified identifier have
     * finished loading.
     *
     * @param id the identifier of the images to check
     * @see MediaTracker#waitForID(int)
     */
    public void waitForID(int id) {
        FrameworkMain.getInstance().getImagePool().waitForID(id);
    }

    /**
     * Flushes all image and clear this ImagePool.
     */
    public void clear() {
        // flush all images
        Enumeration keys = keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            remove(key);
        }
        //tklee
        remove("rotate");
        flushImage(newBufferedImg);
        newBufferedImg = null;
        // actually this is not meaningful.
        super.clear();
    }

    /**
     * Put image.
     *
     * @param key the key
     * @param image the image
     */
    public void put(String key, Image image) {
        if (key == null) { return; }
        super.put(key, image);
        FrameworkMain.getInstance().getImagePool().put(key, image);
    }

    /**
     * Removes and flush the image from ImagePool.
     *
     * @param key image file name.
     * @return Image instance.
     */
    public Object remove(Object key) {
        if (key == null) { return null; }
        super.remove(key);
        return FrameworkMain.getInstance().getImagePool().remove(key);
    }

    /**
     * Flush image.
     *
     * @param image the image
     */
    public void flushImage(Image image) {
        if (image != null) {
            tracker.removeImage(image);
            image.flush();
        }
    }

    /**
     * Removes and flush the image from ImagePool.
     *
     * @param key image file name.
     * @param size the size
     * @return Image instance.
     */
    /*public Object remove(String key, int[] size) {
        return remove(key + size[0] + "X" + size[1]);
    }*/
}


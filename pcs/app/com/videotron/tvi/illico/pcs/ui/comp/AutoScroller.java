/*
 *  @(#)AutoScroller.java 1.0 2011.10.05
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.comp;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>AutoScroller</code> the component to scroll automatically.
 *
 * @since   2011.10.05
 * @version $Revision: 1.4 $ $Date: 2011/10/14 20:11:38 $
 * @author  tklee
 */
public class AutoScroller extends UIComponent {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;
    /** The Constant TIC_TIME. */
    private static final long TIC_TIME = 300L;
    /** The Constant SLEEP_TIME. */
    //private static final long SLEEP_TIME = 2000L;
    /** The Constant MOVE_SIZE. */
    private static final int MOVE_SIZE = 10;
    /** The Constant ALIGN_LEFT. */
    public static final int ALIGN_LEFT      = 0;
    /** The Constant ALIGN_CENTER. */
    public static final int ALIGN_CENTER    = 1;

    /** The text. */
    private String text = null;
    /** The text font. */
    private Font textFont = null;
    /** The font metrics. */
    private FontMetrics fontMetrics = null;
    /** The text color. */
    private Color textColor = null;
    /** The timer spec. */
    private TVTimerSpec timerSpec = null;
    /** The left listener. */
    private RightLeftTimerListener leftListener = null;
    /** The sleep listener. */
    private SleepTimerListener sleepListener = null;
    /** The start x. */
    private int startX = 0;
    /** The text pos y. */
    private int textPosY = 0;
    /** The text align. */
    private int textAlign = 0;
    /** The is started. */
    private boolean isStarted = false;
    /** The is sleep. */
    private boolean isSleep = true;
    /** The sleep time. */
    private long sleepTime = 2000L;
    /** The line gap. */
    private int lineGap = 0;
    /** The added text. */
    private String addedText = null;

    /**
     * Instantiates a new auto scroller.
     */
    public AutoScroller() {
        renderer = new AutoScrollerRenderer();
        setRenderer(renderer);
        leftListener = new RightLeftTimerListener();
        sleepListener = new SleepTimerListener();
        timerSpec = new TVTimerSpec();
    }

    /**
     * Sets the text.
     *
     * @param newText the new text
     * @param newAddedText the new added text
     */
    public void setText(String newText, String newAddedText) {
        text = newText;
        addedText = newAddedText;
        isSleep = true;
        stop();
        if (text == null && addedText == null) return;
        startX = 0;
        int width = getWidth();
        if (textAlign == ALIGN_CENTER) {
            String str = TextUtil.shorten(text, fontMetrics, width);
            startX = (width / 2) - (fontMetrics.stringWidth(str) / 2);
            if ((text != null && width < fontMetrics.stringWidth(text))
                    || (addedText != null && width < fontMetrics.stringWidth(addedText))) { start(); }
        } else {
            if (width < fontMetrics.stringWidth(text)) { start(); }
        }
    }

    /**
     * @return the lineGap
     */
    public int getLineGap() {
        return lineGap;
    }

    /**
     * @param gap the lineGap to set
     */
    public void setLineGap(int gap) {
        this.lineGap = gap;
    }

    /**
     * set the text font.
     *
     * @param newFont the new font
     * @see java.awt.Container#setFont(java.awt.Font)
     */
    public void setFont(Font newFont) {
        textFont = newFont;
        fontMetrics = FontResource.getFontMetrics(newFont);
    }

    /**
     * Sets the text color.
     *
     * @param color the new text color
     */
    public void setTextColor(Color color) {
        textColor = color;
    }

    /**
     * Sets the text pos y.
     *
     * @param posY the new text pos y
     */
    public void setTextPosY(int posY) {
        textPosY = posY;
    }

    /**
     * Sets the align.
     *
     * @param align the new align
     */
    public void setAlign(int align) {
        this.textAlign = align;
    }

    /**
     * set sleepTime.
     * @param time the sleepTime to set
     */
    public void setSleepTime(long time) {
        this.sleepTime = time;
    }

    /**
     * start scroll.
     * @see com.videotron.tvi.illico.framework.UIComponent#start()
     */
    public void start() {
        if (isStarted) return;
        sleep();
        isStarted = true;
    }

    /**
     * stop scroll.
     * @see com.videotron.tvi.illico.framework.UIComponent#stop()
     */
    public void stop() {
        releaseAll();
        isStarted = false;
    }

    /**
     * Release all.
     */
    private void releaseAll() {
        timerSpec.removeTVTimerWentOffListener(leftListener);
        timerSpec.removeTVTimerWentOffListener(sleepListener);
        TVTimer.getTimer().deschedule(timerSpec);
    }

    /**
     * Sleep.
     */
    private void sleep() {
        releaseAll();
        timerSpec.addTVTimerWentOffListener(sleepListener);
        timerSpec.setAbsolute(true);
        timerSpec.setDelayTime(sleepTime);
        timerSpec.setRepeat(false);
        try {
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The listener interface for receiving rightLeftTimer events.
     * The class that is interested in processing a rightLeftTimer
     * event implements this interface, and the object created
     * with that class is registered with a component using the
     * component's <code>addRightLeftTimerListener</code> method. When
     * the rightLeftTimer event occurs, that object's appropriate
     * method is invoked.
     *
     * @see RightLeftTimerEvent
     */
    class RightLeftTimerListener implements TVTimerWentOffListener {
        /**
         * implements the abstract method of TVTimerWentOffListener.
         *
         * @param e the TVTimerWentOffEvent
         * @see javax.tv.util.TVTimerWentOffListener#timerWentOff(javax.tv.util.TVTimerWentOffEvent)
         */
        public synchronized void timerWentOff(TVTimerWentOffEvent e) {
            String str = "";
            if (text != null) { str += text; }
            if (addedText != null) {
                if (text != null) { str += " - "; }
                str += addedText;
            }
            int textWidth = fontMetrics.stringWidth(str);
            startX = startX - MOVE_SIZE;
            if (startX + textWidth < 0) { startX = getWidth(); }
            repaint();
        }
    }

    /**
     * The listener interface for receiving sleepTimer events.
     * The class that is interested in processing a sleepTimer
     * event implements this interface, and the object created
     * with that class is registered with a component using the
     * component's <code>addSleepTimerListener</code> method. When
     * the sleepTimer event occurs, that object's appropriate
     * method is invoked.
     *
     * @see SleepTimerEvent
     */
    class SleepTimerListener implements TVTimerWentOffListener {
        /**
         * implements the abstract method of TVTimerWentOffListener.
         *
         * @param e the TVTimerWentOffEvent
         * @see javax.tv.util.TVTimerWentOffListener#timerWentOff(javax.tv.util.TVTimerWentOffEvent)
         */
        public synchronized void timerWentOff(TVTimerWentOffEvent e) {
            releaseAll();
            if (!isStarted) return;
            timerSpec.addTVTimerWentOffListener(leftListener);
            timerSpec.setAbsolute(true);
            timerSpec.setDelayTime(TIC_TIME);
            timerSpec.setRepeat(true);
            try {
                TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            isSleep = false;
        }
    }

    /**
     * <code>ScreensaverRenderer</code> The class to paint a GUI.
     *
     * @since   2011.09.19
     * @version $Revision: 1.4 $ $Date: 2011/10/14 20:11:38 $
     * @author  tklee
     */
    public class AutoScrollerRenderer extends Renderer {
        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            Logger.debug(this, "c.getBounds() : " + c.getBounds());
            return c.getBounds();
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) { }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            //if (text != null) {
            String str = "";
            g.setFont(textFont);
            g.setColor(textColor);
            if (isSleep) {
                int width = c.getWidth();
                if (text != null) {
                    str = TextUtil.shorten(text, g.getFontMetrics(), width);
                    g.drawString(str, startX, textPosY);
                }
                if (addedText != null) {
                    str = TextUtil.shorten(addedText, fontMetrics, width);
                    int posX = (width / 2) - (fontMetrics.stringWidth(str) / 2);
                    g.drawString(str, posX, textPosY + lineGap);
                }
            } else {
                if (text != null) { str += text; }
                if (addedText != null) {
                    if (text != null) { str += " - "; }
                    str += addedText;
                }
                g.drawString(str, startX, textPosY);
            }
            //}
        }
    }
}

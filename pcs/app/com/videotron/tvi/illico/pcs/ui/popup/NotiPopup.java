/*
 *  @(#)NotiPopup.java 1.0 2011.06.29
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>NotiPopup</code> the popup for notification.
 *
 * @since   2011.06.29
 * @version $Revision: 1.6 $ $Date: 2012/09/18 02:44:00 $
 * @author  tklee
 */
public class NotiPopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_NO_DETECTED_PHOTO. */
    public static final int MODE_NO_DETECTED_PHOTO = 0;
    /** The Constant MODE_NO_DETECTED_MUSIC. */
    public static final int MODE_NO_DETECTED_MUSIC = 1;
    /** The Constant MODE_NO_DETECTED_VIDEO. */
    public static final int MODE_NO_DETECTED_VIDEO = 2;
    /** The Constant MODE_DOWNLOAD_ERROR. */
    public static final int MODE_LOADING_ERROR = 3;
    /** The Constant MODE_NO_SORTING. */
    public static final int MODE_NO_SORTING = 4;

    /**
     * Initialize the Component.
     */
    public NotiPopup() {
        setBounds(276, 113, 410, 330);
        setRenderer(new PopupRenderer());
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() { }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_ENTER:
            clickEffect.start(403 - 276, 318 - 113, 558 - 403, 32);
            scene.requestPopupOk(null);
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            scene.requestPopupOk(null);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.6 $ $Date: 2012/09/18 02:44:00 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The noti icon img. */
        private Image notiIconImg = null;

        /** The title txt. */
        private String[] titleTxt = new String[5];
        /** The desc txt. */
        private String[] descTxt = new String[5];
        /** The btn txt. */
        private String[] btnTxt = new String[5];

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("pop_sha.png");
            bgMImg = getImage("05_pop_glow_m.png");
            bgTImg = getImage("05_pop_glow_t.png");
            bgBImg = getImage("05_pop_glow_b.png");
            gapImg = getImage("pop_gap_379.png");
            highImg = getImage("pop_high_350.png");
            btnFocusImg = getImage("05_focus.png");
            notiIconImg = getImage("icon_noti_or.png");
            super.prepare(c);

            titleTxt[0] = getString("noFileTitle");
            titleTxt[1] = getString("noFileTitle");
            titleTxt[2] = getString("noFileTitle");
            titleTxt[3] = getString("loadingErrorTitle");
            titleTxt[4] = getString("noSortingTitle");
            descTxt[0] = getString("noPhotosDesc");
            descTxt[1] = getString("noMusicDesc");
            descTxt[2] = getString("noVideosDesc");
            descTxt[3] = getString("loadingErrorDesc");
            descTxt[4] = getString("noSortingDesc");
            btnTxt[0] = getString("btnOK");
            btnTxt[1] = getString("btnOK");
            btnTxt[2] = getString("btnOK");
            btnTxt[3] = getString("btnRetry");
            btnTxt[4] = getString("btnOK");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -113);
            //bg
            g.drawImage(shadowImg, 306, 364, 350, 79, c);
            g.drawImage(bgMImg, 276, 193, 410, 124, c);
            g.drawImage(bgTImg, 276, 113, c);
            g.drawImage(bgBImg, 276, 317, c);
            //
            g.setColor(Rs.C35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(gapImg, 285, 181, c);
            g.drawImage(highImg, 306, 143, c);
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            int titleWidth = g.getFontMetrics().stringWidth(titleTxt[mode]) + 31;
            int titlePosX = 482 - titleWidth / 2;
            //GraphicUtil.drawStringCenter(g, titleTxt[mode], 496, 169);
            g.drawImage(notiIconImg, titlePosX, 150, c);
            g.drawString(titleTxt[mode], titlePosX + 31, 169);
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            String[] desc = TextUtil.split(descTxt[mode], g.getFontMetrics(), 614 - 346);
            int baseY = 259 - ((desc.length - 1) * 10);
            for (int i = 0; i < desc.length; i++) {
                int addPos = i * 20;
                GraphicUtil.drawStringCenter(g, desc[i], 483, baseY + addPos);
            }
            //btn
            g.setColor(Rs.C3);
            g.drawImage(btnFocusImg, 403, 318, c);
            GraphicUtil.drawStringCenter(g, btnTxt[mode], 482, 339);
            g.translate(276, 113);
        }
    }
}

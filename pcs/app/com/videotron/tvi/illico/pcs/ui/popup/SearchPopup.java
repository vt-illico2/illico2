/*
 *  @(#)SearchPopup.java 1.0 2011.08.29
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.keyboard.AlphanumericKeyboard;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SelectPopup</code> Search Popup by title.
 *
 * @since   2011.08.29
 * @version $Revision: 1.15 $ $Date: 2012/09/18 02:44:00 $
 * @author  tklee
 */
public class SearchPopup extends Popup implements KeyboardListener {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2941875915202666451L;
    /** The popup loading point. */
    private final Point popupLoadingPoint = new Point(746, 264);

    /** The Constant STATE_KEYBOARD. */
    private static final int STATE_KEYBOARD = 0;
    /** The Constant STATE_TAB. */
    private static final int STATE_TAB = 1;
    /** The Constant STATE_LIST. */
    private static final int STATE_LIST = 2;

    /** The Constant ROWS_PER_PAGE. */
    private static final int ROWS_PER_PAGE = 14;
    /** The Constant SEARCH_WAIT_TIME. */
    public static final long SEARCH_WAIT_COUNT = 2;
    /** The middle position. */
    private static final int LIST_MIDDLE_POS = 7;
    /** The Constant MINMUM_CHAR. */
    private static final int MINMUM_CHAR = 2;

    /** The data loading thread. */
    private DataLoadingThread dataLoadingThread = null;
    /** The AlphanumericKeyboard object. */
    private AlphanumericKeyboard keyboard = null;
    /** The search data as result. */
    private Vector searchData = null;
    /** The Footer instance. */
    private Footer footer = null;
    /** The renderer. */
    private PopupRenderer renderer = null;

    /** The state focus. */
    private int stateFocus= 0;
    /** The current letters. */
    private String currentLetters = "";
    /** The search letters. */
    private String searchLetters = "";
    /** The focus index. */
    private int focusIndex = 0;
    /** The focus position. */
    private int focusPosition = 0;
    /** The search id. */
    private int searchId = -1;
    /** The search wait count. */
    private int searchWaitCount = 0;
    /** The order index about result. */
    private int[][] orderIndex = null;
    /** The tab focus. */
    private int[] tabFocus = new int[2];
    /** The tab focus pos. */
    private int[] tabFocusPos = new int[2];
    /** The search index. */
    private Vector searchIndex = null;
    /** The genre root id. */
    private String genreRootId = null;
    /** The menu type. */
    private int menuType = -1;
    /** The genre content. */
    //private Content genreContent = null;
    /** The search genre content. */
    //private Content searchGenreContent = null;

    /**
     * Instantiates a new select popup.
     */
    public SearchPopup() {
        setBounds(220, 37, 740, 503);
        renderer = new PopupRenderer();
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
        try {
            keyboard = new AlphanumericKeyboard();
            keyboard.setMode(KeyboardOption.MODE_EMBEDDED);
            keyboard.setText("");
            keyboard.setMaxChars(20);
            keyboard.setTempViewText(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        keyboard.setLocation(343 - 220, 6);
        add(keyboard);
        dataLoadingThread = new DataLoadingThread();
        footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
        footer.setBounds(0, 489 - 37, 902 - 220, 24);
        //setBounds(10, 488, 900, 24);
        add(footer);
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        prepare();
        try {
            keyboard.registerKeyboardListener(this);
            //keyboard.setText("");
            keyboard.setTempViewText(true);
            //Keyboard.setLanguage("English");
            //keyboard.setAlphanumericType(KeyboardOption.ALPHANUMERIC_ALL);
            keyboard.prepare();
            if (stateFocus == 0) {
                keyboard.setFocus(AlphanumericKeyboard.INDEX_INITIAL);
            } else { keyboard.setFocus(AlphanumericKeyboard.INDEX_FOCUS_LOST); }
            keyboard.start();
            keyboard.setText(currentLetters);
            keyboard.moveCursor(currentLetters.length());
        } catch (Exception e) {
            e.printStackTrace();
        }

        //stateFocus = 0;
        //currentLetters = "";
        //searchLetters = "";
        //focusIndex = 0;
        //focusPosition = 0;
        searchId = -1;
        //searchData = null;
        searchWaitCount = 0;
        setFooterBtn();
        if (orderIndex == null) { setListOrder(-1, -1, null); }
    }

    /**
     * Reset state focus.
     */
    public void resetStateFocus() {
        stateFocus = 0;
    }

    /**
     * Sets the list order.
     *
     * @param reqMenuType the req menu type
     * @param musicSeq the music seq
     * @param genreData the genre data
     */
    public void setListOrder(int reqMenuType, int musicSeq, Content genreData) {
        //genreContent = genreData;
        menuType = reqMenuType;
        genreRootId = null;
        if (genreData != null) { genreRootId = genreData.getId(); }
        orderIndex = new int[3][];
        if (menuType == BasicUI.MENU_VIDEO) {
            orderIndex[0] = new int[]{BasicUI.MENU_VIDEO};
            orderIndex[1] = new int[]{BasicUI.MENU_PHOTO};
            orderIndex[2] = new int[]{BasicUI.SEQ_SONGS, BasicUI.SEQ_ALBUM, BasicUI.SEQ_ARTIST, BasicUI.SEQ_PLAYLISTS};
        } else if (menuType == BasicUI.MENU_MUSIC) {
            if (musicSeq == BasicUI.SEQ_SONGS) {
                orderIndex[0] =
                    new int[]{BasicUI.SEQ_SONGS, BasicUI.SEQ_ARTIST, BasicUI.SEQ_ALBUM, BasicUI.SEQ_PLAYLISTS};
            } else if (musicSeq == BasicUI.SEQ_ARTIST) {
                orderIndex[0] =
                    new int[]{BasicUI.SEQ_ARTIST, BasicUI.SEQ_SONGS, BasicUI.SEQ_ALBUM, BasicUI.SEQ_PLAYLISTS};
            } else if (musicSeq == BasicUI.SEQ_ALBUM) {
                orderIndex[0] =
                    new int[]{BasicUI.SEQ_ALBUM, BasicUI.SEQ_SONGS, BasicUI.SEQ_ARTIST, BasicUI.SEQ_PLAYLISTS};
            } else if (musicSeq == BasicUI.SEQ_PLAYLISTS) {
                orderIndex[0] =
                    new int[]{BasicUI.SEQ_PLAYLISTS, BasicUI.SEQ_SONGS, BasicUI.SEQ_ALBUM, BasicUI.SEQ_ARTIST};
            } else if (musicSeq == BasicUI.SEQ_GENRES) {
                orderIndex[0] =
                    new int[]{BasicUI.SEQ_GENRES, BasicUI.SEQ_ARTIST, BasicUI.SEQ_ALBUM, BasicUI.SEQ_PLAYLISTS};
            }
            orderIndex[1] = new int[]{BasicUI.MENU_PHOTO};
            orderIndex[2] = new int[]{BasicUI.MENU_VIDEO};
        } else {
            orderIndex[0] = new int[]{BasicUI.MENU_PHOTO};
            orderIndex[1] = new int[]{BasicUI.MENU_VIDEO};
            orderIndex[2] = new int[]{BasicUI.SEQ_SONGS, BasicUI.SEQ_ALBUM, BasicUI.SEQ_ARTIST, BasicUI.SEQ_PLAYLISTS};
        }
    }

    /**
     * set the footer button.
     */
    private void setFooterBtn() {
        footer.reset();
        if (stateFocus == STATE_KEYBOARD) {
            footer.addButtonWithLabel(BasicRenderer.btnCImg, BasicRenderer.btnEraseTxt);
        }
        footer.addButtonWithLabel(BasicRenderer.btnExitImg, BasicRenderer.btnCloseTxt);
    }

    /**
     * when popup is finished, this is called.
     */
    public void stop() {
        CommunicationManager.getInstance().hideLoadingAnimation();
        keyboard.removeKeyboardListener(this);
        try {
            keyboard.stop();
        } catch (Throwable t) {
            Logger.debug(this, "[Error]keyboard stop : " + t.getMessage());
        }
        dataLoadingThread.stop();
        searchId = -1;
        /*searchData = null;
        orderIndex = null;
        searchIndex = null;
        genreContent = null;*/
    }

    /**
     * when App is finished, this is called.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#dispose()
     */
    public void dispose() {
        searchData = null;
        orderIndex = null;
        searchIndex = null;
        genreRootId = null;
        //genreContent = null;
        //searchGenreContent = null;
        currentLetters = "";
        searchLetters = "";
        focusIndex = 0;
        focusPosition = 0;
    }

    /**
     * Gets the genre data.
     *
     * @return the genre data
     */
    /*public Content getGenreData() {
        return searchGenreContent;
    }*/

    /**
     * Check string size by right.
     *
     * @param fm the FontMetrics
     * @param str the str for check
     * @param checkWidth the check width
     * @return the string
     */
    private String checkStringSizeByRight(FontMetrics fm, String str, int checkWidth) {
        if (fm.stringWidth(str) > checkWidth) {
            for (int i = str.length() - 1; i >= 0; i--) {
                if (fm.stringWidth(str.substring(i)) > checkWidth) {
                    return str.substring(i + 1);
                }
            }
        }
        return str;
    }

     /**
      * It does a key action.
      *
      * @param keyCode the key code
      * @return true, if successful
      */
     public boolean keyAction(int keyCode) {
         if (keyCode == Rs.KEY_EXIT || keyCode == KeyCodes.LIVE || keyCode == KeyCodes.SEARCH) {
             searchId = -1;
             if (keyCode == Rs.KEY_EXIT) { footer.clickAnimation(BasicRenderer.btnCloseTxt); }
             //tklee - 20120827
             //scene.requestPopupClose();
             scene.requestPopupOk(null);
             return true;
         }
         if (stateFocus == STATE_KEYBOARD) {
             if (keyCode == KeyCodes.COLOR_C) { footer.clickAnimation(BasicRenderer.btnEraseTxt); }
             if (keyboard.handleKey(keyCode)) {
                 searchWaitCount = 0;
                 repaint();
                 return true;
             } else { return false; }
         } else {
             switch (keyCode) {
             case Rs.KEY_LEFT:
                 if (stateFocus == STATE_TAB) {
                     stateFocus = STATE_KEYBOARD;
                     keyboard.setFocus(AlphanumericKeyboard.INDEX_INITIAL);
                     setFooterBtn();
                 } else if (stateFocus == STATE_LIST) {
                     stateFocus = STATE_TAB;
                 }
                 repaint();
                 break;
             case Rs.KEY_RIGHT:
                 if (stateFocus == STATE_TAB) {
                     stateFocus = STATE_LIST;
                     repaint();
                 }
                 break;
             case Rs.KEY_UP:
                 if (stateFocus == STATE_TAB) {
                     int tabIndex = 0;
                     int[] tabData = null;
                     for (int i = 0; i < searchIndex.size(); i++) {
                         tabData = (int[]) searchIndex.elementAt(i);
                         if (tabData[0] == tabFocus[0] && tabData[1] == tabFocus[1]) {
                             tabIndex = i;
                             break;
                         }
                     }
                     Logger.debug(this, "tabIndex : " + tabIndex);
                     if (tabIndex > 0) {
                         int[] nextTabData = (int[]) searchIndex.elementAt(tabIndex - 1);
                         tabFocus[0] = nextTabData[0];
                         tabFocus[1] = nextTabData[1];
                         focusPosition -= focusIndex - nextTabData[2];
                         focusIndex = nextTabData[2];
                         if (focusPosition < 0) { focusPosition = 0; }
                         // for VDTRMASTER-3891
                         Content item = (Content) searchData.elementAt(focusIndex - 1);
                         if (focusPosition == 0 && item != null && item.getTotalCount() != 0) {
                             focusPosition = 1;
                         }
                         //
                         repaint();
                     }
                 } else if (stateFocus == STATE_LIST) {
                     if (focusIndex > 1) {
                         while (focusIndex > 1) {
                             int initNum = focusIndex - focusPosition;
                             if (focusPosition > LIST_MIDDLE_POS || initNum == 0) { focusPosition--; }
                             focusIndex--;
                             Content item = (Content) searchData.elementAt(focusIndex);
                             if (item != null && item.getTotalCount() == 0) {
                                 tabFocus[0] = item.getMenuType();
                                 tabFocus[1] = item.getMusicSeq();
                                 break;
                             }
                         }
                         repaint();
                     }
                 }
                 repaint();
                 break;
             case Rs.KEY_DOWN:
                 if (stateFocus == STATE_TAB) {
                     int tabIndex = 0;
                     int[] tabData = null;
                     for (int i = 0; i < searchIndex.size(); i++) {
                         tabData = (int[]) searchIndex.elementAt(i);
                         if (tabData[0] == tabFocus[0] && tabData[1] == tabFocus[1]) {
                             tabIndex = i;
                             break;
                         }
                     }
                     if (tabIndex < searchIndex.size() - 1) {
                         int[] nextTabData = (int[]) searchIndex.elementAt(tabIndex + 1);
                         tabFocus[0] = nextTabData[0];
                         tabFocus[1] = nextTabData[1];
                         focusPosition += nextTabData[2] - focusIndex;
                         focusIndex = nextTabData[2];
                         Content item = (Content) searchData.elementAt(focusIndex);
                         Content parent = item.getParent();
                         int size = 3;
                         if (parent.isOpen()) { size = parent.getTotalCount(); }
                         int diff = focusPosition + size - ROWS_PER_PAGE;
                         if (focusPosition > LIST_MIDDLE_POS && diff > 0) {
                             focusPosition -= diff;
                             if (focusPosition < 0) { focusPosition = 0; }
                         }
                         repaint();
                     }
                 } else if (stateFocus == STATE_LIST) {
                     int totalCount = searchData.size();
                     int tempIndex = focusIndex;
                     int tempPosition = focusPosition;
                     if (tempIndex < totalCount - 1) {
                         while (tempIndex < totalCount - 1) {
                             int lastNum = 0;
                             if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                             lastNum = focusIndex + ROWS_PER_PAGE - 1 - focusPosition;
                             if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                             if (tempPosition < LIST_MIDDLE_POS || lastNum == totalCount - 1) { tempPosition++; }
                             tempIndex++;
                             Content item = (Content) searchData.elementAt(tempIndex);
                             if (item != null && item.getTotalCount() == 0) {
                                 tabFocus[0] = item.getMenuType();
                                 tabFocus[1] = item.getMusicSeq();
                                 focusIndex = tempIndex;
                                 focusPosition = tempPosition;
                                 break;
                             }
                         }
                         repaint();
                     }
                 }
                 break;
             case Rs.KEY_ENTER:
                 processEntryKey();
                 break;
             default:
                 return false;
             }
             searchWaitCount = 0;
             return true;
         }
     }

     /**
      * Process entry key.
      */
     private void processEntryKey() {
         searchId++;
         if (stateFocus == STATE_TAB) {
             Content item = (Content) searchData.elementAt(focusIndex);
             Content parent = item.getParent();
             if (parent.getTotalCount() > 3) {
                 clickEffect.start(602 - 220, tabFocusPos[0] - 37, 57, tabFocusPos[1] - tabFocusPos[0]);

                 int tabIndex = 0;
                 int[] tabData = null;
                 for (int i = 0; i < searchIndex.size(); i++) {
                     tabData = (int[]) searchIndex.elementAt(i);
                     if (tabData[0] == tabFocus[0] && tabData[1] == tabFocus[1]) {
                         tabIndex = i;
                         break;
                     }
                 }
                 boolean isOpen = parent.isOpen();
                 if (isOpen) {
                     searchData.removeAll(parent.getChildren());
                     for (int i = 0; i < 3; i++) {
                         searchData.add(tabData[2] + i, parent.getChildren().elementAt(i));
                     }
                 } else {
                     for (int i = 0; i < 3; i++) {
                         searchData.removeElementAt(tabData[2]);
                     }
                     searchData.addAll(tabData[2], parent.getChildren());
                 }
                 int diff = focusIndex - tabData[2];
                 focusIndex -= diff;
                 focusPosition -= diff;
                 if (focusPosition < 0) { focusPosition = 0; }

                 int addPos = parent.getTotalCount() - 3;
                 if (isOpen) { addPos = - addPos; }
                 for (int i = tabIndex + 1; i < searchIndex.size(); i++) {
                     tabData = (int[]) searchIndex.elementAt(i);
                     tabData[2] += addPos;
                 }
                 parent.setOpen(!isOpen);
                 repaint();
             }
         } else if (stateFocus == STATE_LIST) {
             clickEffect.start(659 - 220, 105 - 28 + (focusPosition * 28) - 37, 238, 29);
             Content item = (Content) searchData.elementAt(focusIndex);
             scene.requestPopupOk(item);
         }
     }

    /**
     * Implements the KeyboardListener method.
     *
     * @param position the position
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#cursorMoved(int)
     */
    public void cursorMoved(int position) throws RemoteException {
        Logger.debug(this, "called cursorMoved()-position : " + position);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param direction the direction
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#focusOut(int)
     */
    public void focusOut(int direction) throws RemoteException {
        Logger.debug(this, "called focusOut()-direction : " + direction);
        searchWaitCount = 0;
        switch (direction) {
        case Rs.KEY_UP:
        case Rs.KEY_DOWN:
        case Rs.KEY_LEFT:
            keyboard.setPreviousFocus();
            repaint();
            break;
        case Rs.KEY_RIGHT:
            if (searchData != null && !searchData.isEmpty()) {
                searchId++;
                stateFocus = STATE_LIST;
                setFooterBtn();
            } else { keyboard.setPreviousFocus(); }
            repaint();
            break;
        default:
            break;
        }
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputCanceled(java.lang.String)
     */
    public void inputCanceled(String text) throws RemoteException {
        Logger.debug(this, "called inputCanceled()-direction : " + text);
        searchId = -1;
        handleKey(Rs.KEY_EXIT);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputEnded(java.lang.String)
     */
    public void inputEnded(String text) throws RemoteException {
        Logger.debug(this, "called inputEnded()-text : " + text);
        Logger.debug(this, "inputEnded()-searchData : " + searchData);
        if (searchData != null && !searchData.isEmpty()) {
            Logger.debug(this, "inputEnded()-searchData is ready");
            searchId++;
            keyboard.setFocus(AlphanumericKeyboard.INDEX_FOCUS_LOST);
            stateFocus = STATE_LIST;
            repaint();
        } else { keyboard.setPreviousFocus(); }
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputCleared()
     */
    public void inputCleared() throws RemoteException {
        Logger.debug(this, "called inputCleared()");
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param mode the mode
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#modeChanged(int)
     */
    public void modeChanged(int mode) throws RemoteException {
        Logger.debug(this, "called modeChanged()-mode : " + mode);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#textChanged(java.lang.String)
     */
    public void textChanged(String text) throws RemoteException {
        Logger.debug(this, "called textChanged()-text : " + text);
        searchId++;
        if (text != null && text.length() > 0) {
            currentLetters = text.substring(0, 1).toUpperCase();
            if (text.length() > 1) {
                currentLetters += text.substring(1);
                String letters = currentLetters;
                if (letters.length() >= MINMUM_CHAR && !searchLetters.equalsIgnoreCase(letters)) {
                    dataLoadingThread.setObj(new String[]{String.valueOf(searchId), letters});
                }
            }
        } else {
            currentLetters = "";
            searchLetters = "";
            searchData = null;
        }
        repaint();
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the obj
     */
    private void notifyFromDataLoadingThread(Object obj) {
        String[] reqData = (String[]) obj;
        int myId = Integer.parseInt(reqData[0]);
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (myId != searchId) { return; }
            if (++searchWaitCount >= SEARCH_WAIT_COUNT) { break; }
        }

        if (myId == searchId) {
            //CommunicationManager.getInstance().showNotDelayLoadingAnimation(popupLoadingPoint);
            CommunicationManager.getInstance().showLoadingAnimation(popupLoadingPoint);
            Vector searchList = new Vector();
            Vector vIndex = new Vector();
            String searchCriteria =
                "dc:title contains \"" + reqData[1] + "\" and upnp:class derivedfrom \"object.item\"";
            for (int i = 0; i < orderIndex.length; i++) {
                if (orderIndex[i].length == 1) {
                    String reqId = BasicUI.contentRoot[orderIndex[i][0]].getId();
                    Content result = getSearchData(reqId, searchCriteria, orderIndex[i][0], -1);
                    if (result != null) {
                        searchList.addElement(result);
                        Vector chlidren = result.getChildren();
                        int size = chlidren.size();
                        result.setTotalCount(size);
                        int[] searchPos = {orderIndex[i][0], -1, searchList.size()};
                        vIndex.addElement(searchPos);
                        for (int j = 0; j < 3; j++) {
                            if (j < size) {
                                searchList.addElement(chlidren.elementAt(j));
                            } else { searchList.addElement(null); }
                        }
                    }
                } else { //music
                    Vector musicResult = new Vector();
                    Content musicContent = new Content();
                    int totalCount = 0;
                    for (int j = 0; j < orderIndex[i].length; j++) {
                        String musicSearchCriteria = "dc:title contains \"" + reqData[1] + "\"";
                        //Content root = BasicUI.musicRoot[orderIndex[i][j]];
                        String reqId = BasicUI.musicRoot[orderIndex[i][j]].getId();
                        if (orderIndex[i][j] == BasicUI.SEQ_GENRES) {
                            //root = genreContent;
                            reqId = genreRootId;
                        }
                        if (reqId != null) { musicSearchCriteria += " and @parentID = \"" + reqId + "\""; }
                        Content result =
                            getSearchData(reqId, musicSearchCriteria, BasicUI.MENU_MUSIC, orderIndex[i][j]);
                        if (result != null) {
                            //musicResult.addElement(result);
                            result.setParent(musicContent);
                            Vector chlidren = result.getChildren();
                            int size = chlidren.size();
                            result.setTotalCount(size);
                            int[] searchPos =
                                {BasicUI.MENU_MUSIC, orderIndex[i][j], searchList.size() + 1 + musicResult.size()};
                            vIndex.addElement(searchPos);
                            totalCount += size;
                            for (int k = 0; k < 3; k++) {
                                if (k < size) {
                                    musicResult.addElement(chlidren.elementAt(k));
                                } else { musicResult.addElement(null); }
                            }
                        }
                        if (myId != searchId) {
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            return;
                        }
                    }
                    Logger.debug(this, "totalCount : " + totalCount);
                    if (!musicResult.isEmpty()) {
                        musicContent.setTotalCount(totalCount);
                        musicContent.setMenuType(BasicUI.MENU_MUSIC);
                        searchList.addElement(musicContent);
                        searchList.addAll(musicResult);
                    }
                }
                if (myId != searchId) {
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    return;
                }
            }
            Logger.debug(this, "before hideLoading");
            CommunicationManager.getInstance().hideLoadingAnimation();
            if (myId == searchId) {
                //searchGenreContent = genreContent;
                this.searchIndex = vIndex;
                searchLetters = reqData[1];
                searchData = searchList;
                focusIndex = 1;
                focusPosition = 1;
                if (searchList != null && !searchList.isEmpty()) {
                    Content firstContent = (Content) searchList.elementAt(focusIndex);
                    tabFocus[0] = firstContent.getMenuType();
                    tabFocus[1] = firstContent.getMusicSeq();
                }
                repaint();
            }
        }
    }

    /**
     * return the search data.
     *
     * @param reqId the req id
     * @param searchCriteria the search criteria
     * @param reqMenuType the req menu type
     * @param reqMusicSeq the req music seq
     * @return the Vector as search result
     */
    private Content getSearchData(String reqId, String searchCriteria, int reqMenuType, int reqMusicSeq) {
        Logger.debug(this, "called getSearchData()-reqMenuType : " + reqMenuType);
        if (reqId != null) {
            Content resultContent = new Content();
            int reqCount = Config.searchMaxNumber / 3;
            if (menuType != -1) {
                reqCount = Config.searchMaxNumber * Config.searchLocationPriority / 100;
                if (reqMenuType != menuType) {
                    int rest = Config.searchMaxNumber - reqCount;
                    reqCount = rest / 2;
                }
            }
            if (reqMenuType == BasicUI.MENU_MUSIC) { reqCount /= 4; }

            HNHandler.getInstance().requestSearchEntries(resultContent, reqId, searchCriteria,
                    reqMenuType, reqMusicSeq, reqCount);
            Vector result = resultContent.getChildren();
            if (result != null && !result.isEmpty()) {
                resultContent.setMenuType(reqMenuType);
                resultContent.setMusicSeq(reqMusicSeq);
                return resultContent;
            }
        }
        return null;
    }

    /**
     * <code>DataLoadingThread</code> This class process datas loaded in queue.
     *
     * @since   2011.02.25
     * @version 1.0, 2010.02.25
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class DataLoadingThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = null;

        /**
         * This is constructor to define variables.
         */
        public DataLoadingThread() {
            queue = new ArrayList();
        }

        /**
         * set Object for processing into queue.
         * @param obj   Object for processing.
         */
        public void setObj(Object obj) {
            Logger.debug(this, "called setObj()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "TitleSearch-DataLoadingThread");
                thread.start();
            }

            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    queue.notify();
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                //if (queue.isEmpty()) {
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //}
                if (thread == null) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object obj = queue.remove(0);
                    Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                    if (queue.isEmpty()) { notifyFromDataLoadingThread(obj); }
                }
            }
            queue.clear();
        }
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.15 $ $Date: 2012/09/18 02:44:00 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The bg shadow bottom img. */
        private Image bgShadowBottomImg = null;
        /** The bg shadowleft img. */
        private Image bgShadowleftImg = null;
        /** The bg shadow right img. */
        private Image bgShadowRightImg = null;
        /** The bg left img. */
        private Image bgLeftImg = null;
        /** The bg right img. */
        private Image bgRightImg = null;
        /** The title img. */
        private Image titleImg = null;
        /** The bg high img. */
        private Image bgHighImg = null;
        /** The exit img. */
        //private Image exitImg = null;
        /** The list bg gap img. */
        private Image listBgGapImg = null;
        /** The list line img. */
        private Image listLineImg = null;
        /** The list bg high img. */
        private Image listBgHighImg = null;
        /** The list shadow top img. */
        private Image listShadowTopImg = null;
        /** The list shadow bottom img. */
        private Image listShadowBottomImg = null;
        /** The arr top img. */
        private Image arrTopImg = null;
        /** The arr bottom img. */
        private Image arrBottomImg = null;
        /** The list focus img. */
        private Image listFocusImg = null;
        /** The txt box img. */
        private Image txtBoxImg = null;
        /** The cursor img. */
        private Image cursorImg = null;
        /** The list focus dim img. */
        private Image listFocusDimImg = null;
        //
        /** The album icon img. */
        private Image albumIconImg = null;
        /** The album icon focus img. */
        private Image albumIconFocusImg = null;
        /** The song icon img. */
        private Image songIconImg = null;
        /** The song icon focus img. */
        private Image songIconFocusImg = null;
        /** The video icon img. */
        private Image videoIconImg = null;
        /** The video icon focus img. */
        private Image videoIconFocusImg = null;
        /** The photo icon img. */
        private Image photoIconImg = null;
        /** The photo icon focus img. */
        private Image photoIconFocusImg = null;
        /** The playlist icon img. */
        private Image playlistIconImg = null;
        /** The playlist icon focus img. */
        private Image playlistIconFocusImg = null;
        /** The artist icon img. */
        private Image artistIconImg = null;
        /** The artist icon focus img. */
        private Image artistIconFocusImg = null;
        /** The open icon img. */
        private Image openIconImg = null;
        /** The open icon focus img. */
        private Image openIconFocusImg = null;
        /** The close icon img. */
        private Image closeIconImg = null;
        /** The close icon focus img. */
        private Image closeIconFocusImg = null;
        /** The tab high img. */
        private Image tabHighImg = null;
        /** The tab shadow img. */
        private Image tabShadowImg = null;
        /** The tab focus high img. */
        private Image tabFocusHighImg = null;
        /** The content image. */
        private Image[][] contentImage = new Image[3][];
        /** The content focus image. */
        private Image[][] contentFocusImage =  new Image[3][];

        /** The title txt. */
        private String titleTxt = null;
        /** The tip txt. */
        private String tipTxt = null;
        /** The tip msg txt. */
        private String tipMsgTxt = null;
        /** The no search txt. */
        private String noSearchTxt = null;
        /** The empty txt. */
        private String emptyTxt = null;
        /** The menu txt. */
        private String[] menuTxt = new String[3];
        /** The music seq txt. */
        private String[] musicSeqTxt = new String[5];

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            bgShadowBottomImg = getImage("03_bg_sha.png");
            bgRightImg = getImage("03_bg_02.png");
            bgLeftImg = getImage("03_bg_01.png");
            bgShadowleftImg = getImage("03_bg_l.png");
            titleImg = getImage("03_title.png");
            listBgGapImg = getImage("03_bg01_gap.png");
            bgHighImg = getImage("03_bg01_high.png");
            listShadowTopImg = getImage("03_result_sha_top.png");
            listShadowBottomImg = getImage("03_result_sha_d.png");
            arrTopImg = getImage("02_ars_t.png");
            arrBottomImg = getImage("02_ars_b.png");
            listFocusImg = getImage("09_focus02.png");
            listFocusDimImg = getImage("09_focus02_dim.png");
            txtBoxImg = getImage("04_key_search.png");
            cursorImg = getImage("04_key_cursor.png");
            bgShadowRightImg = getImage("03_shadow_r.png");
            listBgHighImg = getImage("03_bg01_high.png");

            listLineImg = getImage("09_line.png");
            albumIconImg = getImage("09_icon_album.png");
            albumIconFocusImg = getImage("09_icon_album_foc.png");
            tabHighImg = getImage("09_result_high.png");
            tabShadowImg = getImage("09_result_sh.png");
            songIconImg = getImage("09_icon_song.png");
            songIconFocusImg = getImage("09_icon_song_foc.png");
            openIconImg = getImage("09_icon_open.png");
            openIconFocusImg = getImage("09_icon_open_foc.png");
            closeIconImg = getImage("09_icon_close.png");
            closeIconFocusImg = getImage("09_icon_close_foc.png");
            videoIconImg = getImage("09_icon_video.png");
            videoIconFocusImg = getImage("09_icon_video_foc.png");
            photoIconImg = getImage("09_icon_photo.png");
            photoIconFocusImg = getImage("09_icon_photo_foc.png");
            playlistIconImg = getImage("09_icon_playlist.png");
            playlistIconFocusImg = getImage("09_icon_playlist_foc.png");
            artistIconImg = getImage("09_icon_artist.png");
            artistIconFocusImg = getImage("09_icon_artist_foc.png");
            tabFocusHighImg = getImage("09_focus_01_high.png");
            contentImage[0] = new Image[]{photoIconImg};
            contentImage[1] = new Image[]{albumIconImg, artistIconImg, songIconImg, songIconImg, playlistIconImg};
            contentImage[2] = new Image[]{videoIconImg};
            contentFocusImage[0] = new Image[]{photoIconFocusImg};
            contentFocusImage[1] = new Image[]{albumIconFocusImg, artistIconFocusImg, songIconFocusImg,
                    songIconFocusImg, playlistIconFocusImg};
            contentFocusImage[2] = new Image[]{videoIconFocusImg};

            titleTxt = getString("searchTitle");
            tipTxt = getString("Tip");
            tipMsgTxt = getString("searchTip");
            noSearchTxt = getString("noSearch");
            emptyTxt = getString("searchEmpty");
            menuTxt[0] = getString("photos");
            menuTxt[1] = getString("music");
            menuTxt[2] = getString("videos");
            musicSeqTxt[0] = getString("albums");
            musicSeqTxt[1] = getString("artists");
            musicSeqTxt[2] = getString("songs");
            musicSeqTxt[3] = getString("songs");
            musicSeqTxt[4] = getString("playListsForSearch");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
             g.translate(-220, -37);
             g.drawImage(bgLeftImg, 343, 77, c);
             g.drawImage(bgRightImg, 592, 77, 368, 402, c);
             g.drawImage(titleImg, 343, 38, c);
             g.drawImage(bgHighImg, 602, 77, 296, 470 - 77, c);
             g.drawImage(bgShadowBottomImg, 343, 479, 617, 61, c);
             g.drawImage(bgShadowleftImg, 220, 168, c);
             g.drawImage(bgShadowRightImg, 820, 0, 140, 540, c);
             g.drawImage(txtBoxImg, 352, 76, c);

             g.setFont(Rs.F20);
             g.setColor(Rs.C046046045);
             g.drawString(titleTxt, 379, 65);
             g.setColor(Rs.C202174097);
             g.drawString(titleTxt, 378, 64);
             g.setFont(Rs.F18);
             g.setColor(Rs.C255);

             if (currentLetters.length() == 0) {
                 g.setFont(Rs.F17);
                 g.setColor(Rs.DVB40C255);
                 g.drawString(emptyTxt, 366, 100);
                 g.drawImage(cursorImg, 366, 104, c);
             } else {
                 boolean isDimText = keyboard.getTempViewText() && currentLetters.length() > 0;
                 g.setFont(Rs.F20);
                 int checkWidth = 570 - 366;
                 FontMetrics fm = g.getFontMetrics();
                 int stingWidth = fm.stringWidth(currentLetters);
                 if (stingWidth > checkWidth) {
                     String str = checkStringSizeByRight(fm, currentLetters, checkWidth);
                     stingWidth = fm.stringWidth(str);
                     if (isDimText) {
                         g.setColor(Rs.DVB200C170);
                         g.fillRect(570 - stingWidth, 86, stingWidth, 17);
                         g.setColor(Rs.C255);
                     }
                     g.drawString(str, 570 - stingWidth, 100);
                     g.drawImage(cursorImg, 570, 104, c);
                 } else {
                     if (isDimText) {
                         g.setColor(Rs.DVB200C170);
                         g.fillRect(366, 86, stingWidth, 17);
                         g.setColor(Rs.C255);
                     }
                     g.drawString(currentLetters, 366, 100);
                     g.drawImage(cursorImg, 366 + stingWidth, 104, c);
                 }
             }

             paintRight(g, c);
             g.translate(220, 37);
         }

         /**
          * Paint right section.
          *
          * @param g the Graphics
          * @param c the UIComponent
          */
         private void paintRight(Graphics g, UIComponent c) {
             if (CommunicationManager.getInstance().isLoadingAnimation()) { return; }
             if (searchData == null) {
                 g.setFont(Rs.F24);
                 g.setColor(Rs.C255);
                 g.drawString(tipTxt, 635, 194);

                 g.setFont(Rs.F18);
                 g.setColor(Rs.C184);
                 String[] tokens = TextUtil.tokenize(tipMsgTxt, "^");
                 int addPos = -18;
                 for (int i = 0; i < tokens.length; i++) {
                     String[] arrStr = TextUtil.split(tokens[i], g.getFontMetrics(), 864 - 634);
                     for (int j = 0; j < arrStr.length; j++) {
                         addPos += 18;
                         g.drawString(arrStr[j], 634, 220 + addPos);
                     }
                     addPos += 18;
                 }
             } else  if (searchData != null && !searchData.isEmpty()) {
                 paintList(g, c);
             } else {
                 g.setFont(Rs.F18);
                 g.setColor(Rs.C184);
                 String[] arrStr = TextUtil.split(noSearchTxt, g.getFontMetrics(), 864 - 634);
                 for (int i = 0; i < arrStr.length; i++) {
                     int addPos = i * 18;
                     g.drawString(arrStr[i], 634, 220 + addPos);
                 }
             }
         }

         /**
          * Paint list.
          *
          * @param g the Graphics
          * @param c the UIComponent
          */
         private void paintList(Graphics g, UIComponent c) {
             g.setColor(Rs.C55);
             g.fillRect(602, 77, 296, 392);
             int lineGap = 28;
             int addPos = 0;
             int totalCount = searchData.size();
             Logger.debug(this, "paint()-totalCount : " + totalCount);
             g.setFont(Rs.F17);
             int initNum = focusIndex - focusPosition;
             int lastNum = 0;
             if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
             lastNum = focusIndex + ROWS_PER_PAGE - 1 - focusPosition;
             if (lastNum >= totalCount) { lastNum = totalCount - 1; }
             int tabStartPos = -1;
             int tabEndPos = -1;
             //Content startItem = null;
             Content parent = null;
             for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                 //Logger.debug(this, "i : " + i);
                 addPos = (j * lineGap);
                 Content item = (Content) searchData.elementAt(i);
                 if (i != focusIndex || stateFocus != STATE_LIST) { paintItem(g, c, false, item, addPos); }
                 if (item == null) {
                     tabEndPos = 77 + addPos + lineGap;
                 } else if (item.getTotalCount() == 0) {
                     if (tabStartPos == -1) {
                         tabStartPos = 77 + addPos;
                         tabEndPos = tabStartPos + lineGap;
                         parent = item.getParent();
                     } else if (parent.getMenuType() != item.getMenuType()
                             || parent.getMusicSeq() != item.getMusicSeq()) {
                         paintTab(g, c, parent, tabStartPos, tabEndPos);
                         tabStartPos = 77 + addPos;
                         tabEndPos = tabStartPos + lineGap;
                         parent = item.getParent();
                     } else { tabEndPos = 77 + addPos + lineGap; }
                 } else {
                     if (tabStartPos != -1) {
                         paintTab(g, c, parent, tabStartPos, tabEndPos);
                         tabStartPos = -1;
                         tabEndPos = -1;
                         parent = null;
                     }
                 }
             }
             if (tabStartPos != -1) { paintTab(g, c, parent, tabStartPos, tabEndPos); }
             g.drawImage(listBgHighImg, 602, 77, c);
             if (stateFocus == STATE_LIST) {
                 paintItem(g, c, true, searchData.elementAt(focusIndex), (focusPosition * lineGap));
             }
             if (initNum > 0) {
                 g.drawImage(listShadowTopImg, 602, 104 - 28, c);
                 g.drawImage(arrTopImg, 739, 62, c);
             }
             if (lastNum < totalCount - 1) {
                 g.drawImage(listShadowBottomImg, 602, 439, c);
                 g.drawImage(arrBottomImg, 739, 467, c);
             }
         }

         /**
          * Paint item.
          *
          * @param g the Graphics
          * @param c the UIComponent
          * @param isFocus the is focus
          * @param item the item
          * @param addPos the add position
          */
         private void paintItem(Graphics g, UIComponent c, boolean isFocus, Object item, int addPos) {
             g.setFont(Rs.F17);
             if (item == null) {
                 g.drawImage(listLineImg, 669, (132 - 28) + addPos, c);
                 return;
             }
             Content data = (Content) item;
             if (data.getTotalCount() != 0) {
                 g.setColor(Rs.DVB179C102);
                 g.fillRect(602, 77 + addPos, 296, 28);
                 g.drawImage(listBgGapImg, 603, 100 + addPos, c);
                 String str = menuTxt[data.getMenuType()] + " (" + data.getTotalCount() + ")";
                 g.setColor(Rs.C230);
                 g.drawString(str, 613, 96 + addPos);
             } else {
                 String name = TextUtil.shorten(data.getName(), g.getFontMetrics(), 884 - 675);
                 if (isFocus) {
                     g.setColor(Rs.C0);
                     g.drawImage(listFocusImg, 659, 105 - 28 + addPos, c);
                     g.drawString(name, 675, 124 - 28 + addPos);
                 } else {
                     g.drawImage(listLineImg, 669, (132 - 28) + addPos, c);
                     String letters = searchLetters.trim().toLowerCase();
                     String remainStr = name.toLowerCase();
                     String realStr = name;
                     String str = null;
                     int posX = 675;
                     int posY = 123 - 28 + addPos;
                     for (int i = 0; i != -1 || remainStr.length() > 0;) {
                         i = remainStr.indexOf(letters);
                         if(i == -1) {
                             g.setColor(Rs.C255);
                             g.drawString(realStr, posX, posY);
                             break;
                         } else if (i != 0) {
                             g.setColor(Rs.C255);
                             str = realStr.substring(0, i);
                             g.drawString(str, posX, posY);
                             posX += g.getFontMetrics().stringWidth(str);
                             remainStr = remainStr.substring(i);
                             realStr = realStr.substring(i);
                         } else {
                             g.setColor(Rs.C254196014);
                             str = realStr.substring(0, letters.length());
                             g.drawString(str, posX, posY);
                             posX += g.getFontMetrics().stringWidth(str);
                             remainStr = remainStr.substring(letters.length());
                             realStr = realStr.substring(letters.length());
                         }
                     }
                 }
             }
         }
         
         /**
          * Paint item.
          *
          * @param g the Graphics
          * @param c the UIComponent
          * @param item the item
          * @param addPos the add position
          */
         private void paintTab(Graphics g, UIComponent c, Content item, int startPos, int endPos) {
             Logger.debug(this, "paintTab()-startPos : " + startPos);
             Logger.debug(this, "paintTab()-endPos : " + endPos);
             int lineGap = 28;
             int diff = endPos - startPos;
             int totalCount = item.getTotalCount();
             boolean isFocus =
                 stateFocus == STATE_TAB && tabFocus[0] == item.getMenuType() && tabFocus[1] == item.getMusicSeq();
             Image conImg = getContentImage(item.getMenuType(), item.getMusicSeq(), isFocus);
             if (isFocus) {
                 g.setColor(Rs.C252202004);
             } else { g.setColor(Rs.C41); }
             g.fillRect(602, startPos, 57, diff);
             if (!isFocus) { g.drawImage(tabShadowImg, 602, endPos - 27, c); }
             if (isFocus) {
                 g.setColor(Rs.C0);
                 tabFocusPos[0] = startPos;
                 tabFocusPos[1] = endPos;
             } else { g.setColor(Rs.C208206206); }
             if (item.getMenuType() == BasicUI.MENU_MUSIC) {
                 g.setFont(Rs.F15);
                 GraphicUtil.drawStringCenter(g, musicSeqTxt[item.getMusicSeq()], 631, startPos + 16);
                 if (diff > lineGap) {
                     g.drawImage(conImg, 620, startPos + 19, c);
                     g.setFont(Rs.F17);
                     GraphicUtil.drawStringCenter(g, "(" + totalCount + ")", 631, startPos + 55);
                 }
                 if (endPos < 456) {
                     g.setColor(Rs.DVB179C102);
                     g.fillRect(602, endPos, 296, 2);
                 }
             } else {
                 g.setFont(Rs.F17);
                 g.drawImage(conImg, 620, startPos + 4, c);
                 if (diff > lineGap) {
                     GraphicUtil.drawStringCenter(g, "(" + totalCount + ")", 631, startPos + 40);
                 }
             }
             if (diff > lineGap * 2 && totalCount > 3) {
                 Image iconImg = openIconImg;
                 if (isFocus) { iconImg = openIconFocusImg; }
                 if (item.isOpen()) {
                     iconImg = closeIconImg;
                     if (isFocus) { iconImg = closeIconFocusImg; }
                 }
                 g.drawImage(iconImg, 622, endPos - 20, c);
             }
             Image highImg = tabHighImg;
             if (isFocus) { highImg = tabFocusHighImg; }
             if (diff > lineGap) { g.drawImage(highImg, 602, startPos, c); }
         }
         
         /**
          * Gets the content image.
          *
          * @param menuType the menu type
          * @param musicSeq the music seq
          * @param isFocus the is focus
          * @return the content image
          */
         private Image getContentImage(int menuType, int musicSeq, boolean isFocus) {
             Image[][] imgs = null;
             if (isFocus) {
                 imgs = contentFocusImage;
             } else { imgs = contentImage; }
             if (menuType == BasicUI.MENU_MUSIC) {
                 return imgs[menuType][musicSeq];
             } else {
                 return imgs[menuType][0];
             }
         }
    }
}

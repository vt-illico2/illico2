/*
 *  @(#)PreferenceUI.java 1.0 2011.07.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.Point;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.gui.PreferenceRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.DeviceSelectPopup;
import com.videotron.tvi.illico.pcs.ui.popup.PreviewPopup;
import com.videotron.tvi.illico.pcs.ui.popup.SelectPopup;

/**
 * <code>PreferenceUI</code> This class show the setting page for preference.
 *
 * @since   2011.07.04
 * @version $Revision: 1.10 $ $Date: 2012/09/25 05:03:40 $
 * @author  tklee
 */
public class PreferenceUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MENU_FAVOR_DEVICE. */
    public static final int MENU_FAVOR_DEVICE = 0;
    /** The Constant MENU_SLIDESHOW_EFFECT. */
    public static final int MENU_SLIDESHOW_EFFECT = 1;
    /** The Constant MENU_SLIDESHOW_INTERVAL. */
    public static final int MENU_SLIDESHOW_INTERVAL = 2;
    /** The Constant MENU_MUSIC_SCREENSAVER. */
    public static final int MENU_MUSIC_SCREENSAVER = 3;
    /** The Constant MENU_MUSIC_CROSS_FADE. */
    //public static final int MENU_MUSIC_CROSS_FADE = 4;
    /** The Constant MENU_VIDEO_SCALING. */
    public static final int MENU_VIDEO_SCALING = 4;

    /** The Constant EFFECT_NO. */
    public static final int EFFECT_NO = 0;
    /** The Constant EFFECT_RANDOM. */
    public static final int EFFECT_RANDOM = 1;
    /** The Constant EFFECT_FADE. */
    public static final int EFFECT_FADE = 2;
    /** The Constant EFFECT_DISSOLVE. */
    public static final int EFFECT_DISSOLVE = 3;
    /** The Constant EFFECT_PAGE_FLIP. */
    public static final int EFFECT_PAGE_FLIP = 4;

    /** The Constant SCREENSAVER_ALBUM. */
    public static final int SCREENSAVER_ALBUM = 0;
    /** The Constant SCREENSAVER_PHOTO. */
    public static final int SCREENSAVER_PHOTO = 1;
    /** The Constant SCREENSAVER_NO. */
    public static final int SCREENSAVER_NO = 2;

    /** The Constant AUTOCONNECT_DISABLED. */
    public static final int AUTOCONNECT_DISABLED = 0;
    /** The Constant AUTOCONNECT_ENABLED. */
    public static final int AUTOCONNECT_ENABLED = 1;

    /** The Constant MUSICCROSS_NO. */
    //public static final int MUSICCROSS_NO = 0;
    /** The Constant MUSICCROSS_YES. */
    //public static final int MUSICCROSS_YES = 1;

    /** The Constant VIDEOSCALING_ACTUAL. */
    public static final int VIDEOSCALING_ACTUAL = 0;
    /** The Constant VIDEOSCALING_FULL. */
    public static final int VIDEOSCALING_FULL = 1;

    /** The Constant INTERVAL_3. */
    public static final int INTERVAL_3 = 0;
    /** The Constant INTERVAL_5. */
    public static final int INTERVAL_5 = 1;
    /** The Constant INTERVAL_10. */
    public static final int INTERVAL_10 = 2;

    /** The Constant WAIT_1. */
    public static final int WAIT_1 = 0;
    /** The Constant WAIT_5. */
    public static final int WAIT_5 = 1;
    /** The Constant WAIT_10. */
    public static final int WAIT_10 = 2;

    /** The MusicListRenderer instance. */
    private PreferenceRenderer renderer = null;
    /** The select popup. */
    private SelectPopup selectPopup = null;
    /** The preview popup. */
    private PreviewPopup previewPopup = null;
    /** The menu focus. */
    public int menuFocus = MENU_FAVOR_DEVICE;
    /** The is left status. */
    public boolean isLeftStatus = true;
    /** The auto connect focus. */
    public int autoConnectFocus = AUTOCONNECT_DISABLED;
    /** The effect focus. */
    public int effectFocus = EFFECT_DISSOLVE;
    /** The interval focus. */
    public int intervalFocus = INTERVAL_5;
    /** The screensaver focus. */
    public int screensaverFocus = SCREENSAVER_ALBUM;
    /** The wait focus. */
    public int waitFocus = WAIT_5;
    /** The music cross fade focus. */
    //public int musicCrossFadeFocus = MUSICCROSS_NO;
    /** The video scaling focus. */
    public int videoScalingFocus = VIDEOSCALING_ACTUAL;

    /**
     * Initialize the Scene.
     */
    public PreferenceUI() {
        renderer = new PreferenceRenderer();
        setRenderer(renderer);
        selectPopup = new SelectPopup();
        previewPopup = new PreviewPopup();
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();
        super.start(isReset, sendData);
        if (isReset) {
            menuFocus = MENU_FAVOR_DEVICE;
            isLeftStatus = true;
            String str = PreferenceProxy.getInstance().favorUDN;
            autoConnectFocus = AUTOCONNECT_DISABLED;
            if (str != null && !str.equals("")) { autoConnectFocus = AUTOCONNECT_ENABLED; }
            PreferenceProxy prefProxy = PreferenceProxy.getInstance();
            effectFocus = getFocus(prefProxy.slideshowEffect, PreferenceProxy.EFFECTS);
            intervalFocus = getFocus(prefProxy.slideshowInterval, PreferenceProxy.INTERVALS);
            screensaverFocus = getFocus(prefProxy.musicScreensaver, PreferenceProxy.SCREENSAVERS);
            //musicCrossFadeFocus = getFocus(prefProxy.musicCrossFade, PreferenceProxy.MUSIC_CROSS_FADES);
            videoScalingFocus = getFocus(prefProxy.videoScaling, PreferenceProxy.VIDEO_SIZES);
            waitFocus = getFocus(prefProxy.screensaverWait, PreferenceProxy.WAITS);
        }
    }

    /**
     * Gets the focus.
     *
     * @param checkStr the check str
     * @param list the list
     * @return the focus
     */
    private int getFocus(String checkStr, String[] list) {
        if (checkStr == null) { return 0; }
        for (int i = 0; i < list.length; i++) {
            if (checkStr.equals(list[i])) { return i; }
        }
        return 0;
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        renderer.stop();
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (!isLeftStatus) {
                isLeftStatus = true;
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (isLeftStatus && ((menuFocus == MENU_FAVOR_DEVICE && autoConnectFocus == AUTOCONNECT_ENABLED) ||
                    (menuFocus == MENU_SLIDESHOW_EFFECT && effectFocus != EFFECT_NO) ||
                    (menuFocus == MENU_MUSIC_SCREENSAVER && screensaverFocus != SCREENSAVER_NO))) {
                isLeftStatus = false;
                repaint();
            }
            break;
        case Rs.KEY_UP:
            if (menuFocus > 0) {
                menuFocus--;
                isLeftStatus = true;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (menuFocus < MENU_VIDEO_SCALING) {
                menuFocus++;
                isLeftStatus = true;
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            if (menuFocus == MENU_SLIDESHOW_EFFECT && !isLeftStatus) {
                clickEffect.start(575, 200, 210, 32);
                addPopup(previewPopup);
            } else {
                int posX = 339;
                int posY = 141 + (menuFocus * 37);
                if (menuFocus != PreferenceUI.MENU_FAVOR_DEVICE) { posY += 21; }
                if (!isLeftStatus) { posX = 575; }
                clickEffect.start(posX, posY, 210, 32);
                int focus = 0;
                String[] data = null;
                if (menuFocus == MENU_FAVOR_DEVICE) {
                    if (isLeftStatus) {
                        data = renderer.enableTxt;
                        focus = autoConnectFocus;
                    } else if (deviceInfos != null && deviceInfos.size() > 0) {
                        /*data = new String[deviceInfos.size()];
                        for (int i = 0; i < data.length; i++) {
                            DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(i);
                            data[i] = devInfo.getDeviceName();
                            if (PreferenceProxy.getInstance().favorUDN.equals(devInfo.getUDN())) { focus = i; }
                        }*/
                        String favorUDN = PreferenceProxy.getInstance().favorUDN;
                        if (favorUDN != null && favorUDN.length() > 0) {
                            for (int i = 0; i < deviceInfos.size(); i++) {
                                DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(i);
                                if (favorUDN.equals(devInfo.getUDN())) {
                                    focus = i;
                                    break;
                                }
                            }
                        }
                        devSelectPop.setMode(DeviceSelectPopup.MODE_FAVORITE);
                        devSelectPop.setData(deviceInfos);
                        devSelectPop.resetFocus();
                        devSelectPop.setListFocus(focus);
                        addPopup(devSelectPop);
                        return true;
                    }
                } else if (menuFocus == MENU_SLIDESHOW_EFFECT) {
                    data = renderer.effectTxt;
                    focus = effectFocus;
                } else if (menuFocus == MENU_SLIDESHOW_INTERVAL) {
                    data = renderer.intervalTxt;
                    focus = intervalFocus;
                } else if (menuFocus == MENU_MUSIC_SCREENSAVER) {
                    if (isLeftStatus) {
                        data = renderer.screensaverTxt;
                        focus = screensaverFocus;
                    } else {
                        data = renderer.waitTxt;
                        focus = waitFocus;
                    }
                /*} else if (menuFocus == MENU_MUSIC_CROSS_FADE) {
                    data = renderer.yesnoTxt;
                    focus = musicCrossFadeFocus;*/
                } else if (menuFocus == MENU_VIDEO_SCALING) {
                    data = renderer.videoScalingTxt;
                    focus = VIDEOSCALING_ACTUAL;
                }
                if (data != null) {
                    posX = 519;
                    if (!isLeftStatus) { posX = 669; }
                    posY = 83 + (menuFocus * 37);
                    if (menuFocus != PreferenceUI.MENU_FAVOR_DEVICE) { posY += 21; }
                    selectPopup.setData(renderer, data, focus, renderer.menuTxt[menuFocus], 3, new Point(posX, posY));
                    addPopup(selectPopup);
                }
            }
            break;
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof SelectPopup) {
            int focus = ((Integer) para).intValue();
            PreferenceProxy prefProxy = PreferenceProxy.getInstance();
            String name = null;
            String value = null;
            if (menuFocus == MENU_FAVOR_DEVICE) {
                name = PreferenceProxy.NAME_FAVOR_DEVICE;
                String udn = "";
                if (isLeftStatus) {
                    value = "";
                    if (focus != autoConnectFocus) {
                        autoConnectFocus = focus;
                        DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(0);
                        if (focus == AUTOCONNECT_ENABLED) {
                            value = devInfo.getDeviceName();
                            udn = devInfo.getUDN();
                        }
                        prefProxy.favorDeviceName = value;
                        prefProxy.favorUDN = udn;
                        prefProxy.setPreferenceValue(name, value);
                        prefProxy.setPreferenceValue(PreferenceProxy.NAME_FAVOR_UDN, udn);
                    }
                } else {
                    DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(focus);
                    value = devInfo.getDeviceName();
                    udn = devInfo.getUDN();
                    if (!udn.equals(prefProxy.favorUDN)) {
                        prefProxy.favorDeviceName = value;
                        prefProxy.favorUDN = udn;
                        prefProxy.setPreferenceValue(name, value);
                        prefProxy.setPreferenceValue(PreferenceProxy.NAME_FAVOR_UDN, udn);
                    }
                }
            } else if (menuFocus == MENU_SLIDESHOW_EFFECT) {
                if (focus != effectFocus) {
                    effectFocus = focus;
                    name = PreferenceProxy.NAME_SLIDESHOW_EFFECT;
                    value =  PreferenceProxy.EFFECTS[focus];
                    prefProxy.slideshowEffect = value;
                    prefProxy.setPreferenceValue(name, value);
                }
            } else if (menuFocus == MENU_SLIDESHOW_INTERVAL) {
                if (focus != intervalFocus) {
                    intervalFocus = focus;
                    name = PreferenceProxy.NAME_SLIDESHOW_INTERVAL;
                    value =  PreferenceProxy.INTERVALS[focus];
                    prefProxy.slideshowInterval = value;
                    prefProxy.setPreferenceValue(name, value);
                }
            } else if (menuFocus == MENU_MUSIC_SCREENSAVER) {
                if (isLeftStatus) {
                    if (focus != screensaverFocus) {
                        name = PreferenceProxy.NAME_MUSIC_SCREENSAVER;
                        value = PreferenceProxy.SCREENSAVERS[focus];
                        prefProxy.musicScreensaver = value;
                        prefProxy.setPreferenceValue(name, value);

                        name = PreferenceProxy.NAME_SCREENSAVER_WAIT;
                        if (focus == SCREENSAVER_NO || screensaverFocus == SCREENSAVER_NO) {
                            value = "";
                            if (screensaverFocus == SCREENSAVER_NO) { value = PreferenceProxy.WAITS[WAIT_5]; }
                            prefProxy.screensaverWait = value;
                            prefProxy.setPreferenceValue(name, value);
                            waitFocus = WAIT_5;
                        }
                        screensaverFocus = focus;
                    }
                } else {
                    if (focus != waitFocus) {
                        waitFocus = focus;
                        name = PreferenceProxy.NAME_SCREENSAVER_WAIT;
                        value =  PreferenceProxy.WAITS[focus];
                        prefProxy.screensaverWait = value;
                        prefProxy.setPreferenceValue(name, value);
                    }
                }
            /*} else if (menuFocus == MENU_MUSIC_CROSS_FADE) {
                if (focus != musicCrossFadeFocus) {
                    musicCrossFadeFocus = focus;
                    name = PreferenceProxy.NAME_MUSIC_CROSS_FADE;
                    value =  PreferenceProxy.MUSIC_CROSS_FADES[focus];
                    prefProxy.musicCrossFade = value;
                    prefProxy.setPreferenceValue(name, value);
                }*/
            } else if (menuFocus == MENU_VIDEO_SCALING) {
                if (focus != videoScalingFocus) {
                    videoScalingFocus = focus;
                    name = PreferenceProxy.NAME_VIDEO_SCALING;
                    value =  PreferenceProxy.VIDEO_SIZES[focus];
                    prefProxy.videoScaling = value;
                    prefProxy.setPreferenceValue(name, value);
                }
            }
            removePopup();
        } else if (popup instanceof DeviceSelectPopup) {
            DeviceInfo devInfo = (DeviceInfo) para;
            if (devInfo != null) {
                PreferenceProxy prefProxy = PreferenceProxy.getInstance();
                String value = devInfo.getDeviceName();
                String udn = devInfo.getUDN();
                if (!udn.equals(prefProxy.favorUDN)) {
                    prefProxy.favorDeviceName = value;
                    prefProxy.favorUDN = udn;
                    prefProxy.setPreferenceValue(PreferenceProxy.NAME_FAVOR_DEVICE, value);
                    prefProxy.setPreferenceValue(PreferenceProxy.NAME_FAVOR_UDN, udn);
                }
            }
            removePopup();
        } else { super.requestPopupOk(para); }
    }

    /**
     * Checks if is slide show.
     *
     * @return true, if is slide show
     */
    public boolean isSlideShow() {
        return popup != null && popup == previewPopup;
    }
}

/*
 *  @(#)MusicListUI.java 1.0 2011.06.14
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.MusicBrowsingPopup;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.pcs.ui.popup.SearchPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>MusicListUI</code> This class show the list page for music.
 *
 * @since   2011.06.14
 * @version $Revision: 1.2 $ $Date: 2012/09/19 07:42:40 $
 * @author  tklee
 */
public class MusicSearchListUI extends MusicListUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /**
     * Initialize the Scene.
     */
    public MusicSearchListUI() {
        super();
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        tempRoot = null;
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        footer.addButtonWithLabel(BasicRenderer.btnPageImg, BasicRenderer.btnPageTxt);
        footer.addButtonWithLabel(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
        footer.addButtonWithLabel(BasicRenderer.btnDImg, BasicRenderer.btnOptTxt);
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            if (!historyStack.isEmpty()) {
                showPrevList();
            } else {
                historyStack.clear();
                SceneManager.getInstance().goToScene(previousSceneID, false, null);
            }
            break;
        case KeyCodes.SEARCH:
        case KeyCodes.COLOR_A:
            break;
        default:
            return super.keyAction(keyCode);
        }
        return true;
    }

    /**
     * D-Option Selected.
     *
     * @param item the item
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }
        if (item.equals(OPT_PHOTO) || item.equals(OPT_MUSIC) || item.equals(OPT_VIDEO)) {
            int newIndex = MENU_PHOTO;
            if (item.equals(OPT_MUSIC)) {
                newIndex = MENU_MUSIC;
            } else if (item.equals(OPT_VIDEO)) { newIndex = MENU_VIDEO; }
            goToContentUI(newIndex);
        } else { super.selected(item); }
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(TH_CONTENT_LIST)) {
            if (currentRoot == null) {
                int musicSeq = searchContent.getMusicSeq();
                Logger.debug(this, "notifyFromDataLoadingThread()-musicSeq : " + musicSeq);
                tempUIType = UI_ALBUM_SONGS;
                if (musicSeq == SEQ_ARTIST) {
                    if (searchContent.getChildCount() == 1) {
                        tempUIType = UI_ARTIST_SONGS;
                    } else { tempUIType = UI_ARTIST_ALBUM; }
                } else if (musicSeq == SEQ_PLAYLISTS) { tempUIType = UI_PLAYLIST_SONGS; }
                tempRoot = searchContent;
            }
            processContentList(obj, isFromThread);
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof MusicBrowsingPopup) {
            return;
        } else if (popup instanceof MusicPlayerPopup) {
            //int[] value = (int[]) para;
            removePopup();
            /*if (value != null && value[0] == MusicPlayerPopup.UI_SELECT_MUSIC && value[1] == 0) {
                if (uiType != UI_ALBUM) {
                    isNewBrowse = true;
                    changeNode(0, 0, musicRoot[SEQ_ALBUM], UI_ALBUM, Content.SORT_ALPHA_A, true, HISTORY_ADD, 0);
                }
            }*/
        } else if (popup instanceof SearchPopup) {
            return;
        } else { super.requestPopupOk(para); }
    }
}

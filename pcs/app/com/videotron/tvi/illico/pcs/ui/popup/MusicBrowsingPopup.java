/*
 *  @(#)MusicBrowsingPopup.java 1.0 2011.06.28
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>ViewChangePopup</code> the popup to browse music.
 *
 * @since   2011.06.28
 * @version $Revision: 1.8 $ $Date: 2012/04/14 01:37:24 $
 * @author  tklee
 */
public class MusicBrowsingPopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant STATE_LIST. */
    private static final int STATE_LIST = 0;
    /** The Constant STATE_BTN. */
    private static final int STATE_BTN = 1;

    /** The Constant SEQ_ALBUM. */
    public static final int SEQ_ALBUM = 0;
    /** The Constant SEQ_ARTIST. */
    public static final int SEQ_ARTIST = 1;
    /** The Constant SEQ_SONGS. */
    public static final int SEQ_SONGS = 2;
    /** The Constant SEQ_GENRES. */
    public static final int SEQ_GENRES = 3;
    /** The Constant SEQ_PLAYLISTS. */
    public static final int SEQ_PLAYLISTS = 4;

    /** The list focus. */
    private int listFocus = 0;
    /** The state focus. */
    private int stateFocus = 0;
    /** The prev focus. */
    private int prevFocus = 0;

    /**
     * Initialize the Component.
     */
    public MusicBrowsingPopup() {
        setBounds(186, 92, 592, 335);
        setRenderer(new PopupRenderer());
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() { }

    /**
     * reset focus index.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        listFocus = 0;
        stateFocus = 0;
        prevFocus = 0;
    }

    /**
     * Sets the list focus.
     *
     * @param focus the new list focus
     */
    public void setListFocus(int focus) {
        listFocus = focus;
        prevFocus = focus;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (stateFocus == STATE_BTN) {
                stateFocus = STATE_LIST;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (stateFocus == STATE_LIST) {
                stateFocus = STATE_BTN;
                repaint();
            }
            break;
        case Rs.KEY_LEFT:
            if (stateFocus == STATE_LIST && listFocus > 0) {
                listFocus--;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case Rs.KEY_RIGHT:
            if (stateFocus == STATE_LIST && listFocus < SEQ_PLAYLISTS) {
                listFocus++;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case KeyCodes.COLOR_A:
            if (stateFocus == STATE_LIST) {
                listFocus = (listFocus + 1) % 5;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case Rs.KEY_ENTER:
            if (stateFocus == STATE_LIST) {
                clickEffect.start(254 + (listFocus * 100) - 186, 225 - 92, 56, 57);
                //scene.requestPopupOk(new Integer(listFocus));
                scene.requestPopupClose();
            } else {
                clickEffect.start(403 - 186, 343 - 92, 558 - 403, 32);
                scene.requestPopupOk(new Integer(prevFocus));
                scene.requestPopupClose();
            }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.8 $ $Date: 2012/04/14 01:37:24 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The bg img. */
        private Image bgImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The btn dim img. */
        private Image btnDimImg = null;
        /** The view icon img. */
        private Image[] viewIconImg = new Image[5];
        /** The arrow l img. */
        private Image arrowLImg = null;
        /** The arrow r img. */
        private Image arrowRImg = null;
        /** The view icon focus img. */
        private Image viewIconFocusImg = null;

        /** The title txt. */
        private String titleTxt = null;
        /** The desc txt. */
        private String descTxt = null;
        /** The note txt. */
        private String[] viewTxt = new String[5];
        /** The btn cancel txt. */
        private String btnCancelTxt = null;

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            bgImg = getImage("09_popbg_529.png");
            btnFocusImg = getImage("05_focus.png");
            btnDimImg = getImage("05_focus_dim.png");
            viewIconImg[0] = getImage("09_viewicon03.png");
            viewIconImg[1] = getImage("09_viewicon04.png");
            viewIconImg[2] = getImage("09_viewicon06.png");
            viewIconImg[3] = getImage("09_viewicon05.png");
            viewIconImg[4] = getImage("09_viewicon02.png");
            viewIconFocusImg = getImage("09_view_foc.png");
            arrowLImg = getImage("02_ars_l.png");
            arrowRImg = getImage("02_ars_r.png");
            super.prepare(c);
            titleTxt = getString("browseMusicTitle");
            descTxt = getString("browseMusicDesc");
            btnCancelTxt = getString("btnCanecl");
            viewTxt[0] = BasicRenderer.getString("albums");
            viewTxt[1] = BasicRenderer.getString("artists");
            viewTxt[2] = BasicRenderer.getString("songs");
            viewTxt[3] = BasicRenderer.getString("genres");
            viewTxt[4] = BasicRenderer.getString("playlists");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-186, -92);
            //bg
            g.drawImage(bgImg, 186, 92, c);
            //btn
            Image btnImg = btnDimImg;
            if (stateFocus == STATE_BTN) { btnImg = btnFocusImg; }
            if (btnImg != null) { g.drawImage(btnImg, 403, 343, c); }
            g.setFont(Rs.F18);
            g.setColor(Rs.C3);
            GraphicUtil.drawStringCenter(g, btnCancelTxt, 482, 364);
            //title
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            GraphicUtil.drawStringCenter(g, titleTxt, 482, 149);
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, descTxt, 482, 199);
            //icon
            for (int i = 0; i < viewIconImg.length; i++) {
                int addX = i * 100;
                g.drawImage(viewIconImg[i], 254 + addX, 225, c);
                g.setFont(Rs.F18);
                g.setColor(Rs.C182);
                if (stateFocus == STATE_LIST) {
                    if (i == listFocus) {
                        g.drawImage(viewIconFocusImg, 254 + addX, 225, c);
                        if (i > 0) { g.drawImage(arrowLImg, 238 + addX, 241, c); }
                        if (i < viewIconImg.length - 1) { g.drawImage(arrowRImg, 309 + addX, 241, c); }
                        g.setFont(Rs.F20);
                        g.setColor(Rs.C252202004);
                    }
                }
                //playlists for French
                if (i == viewTxt.length - 1 && Config.language.equals(Definitions.LANGUAGE_FRENCH)) {
                    int posY = 304;
                    String txt = viewTxt[i];
                    int index = txt.indexOf(" ");
                    if (index != -1) {
                        GraphicUtil.drawStringCenter(g, txt.substring(0, index), 282 + addX, posY);
                        txt = txt.substring(index + 1);
                        posY += 18;
                    }
                    GraphicUtil.drawStringCenter(g, txt, 282 + addX, posY);
                } else { GraphicUtil.drawStringCenter(g, viewTxt[i], 282 + addX, 304); }
            }
            g.translate(186, 92);
        }
    }
}

/*
 *  @(#)PhotoPlayUI.java 1.0 2011.07.28
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.flipbar.FlipBar;
import com.videotron.tvi.illico.flipbar.FlipBarAction;
import com.videotron.tvi.illico.flipbar.FlipBarContent;
import com.videotron.tvi.illico.flipbar.FlipBarFactory;
import com.videotron.tvi.illico.flipbar.FlipBarListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.gui.PhotoPlayRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.pcs.ui.popup.NotiPopup;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>PhotoPlayUI</code> This class is for Playing of Photo.
 *
 * @since   2011.07.28
 * @version $Revision: 1.38 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class PhotoPlayUI extends BasicUI implements FlipBarListener, TVTimerWentOffListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;
    /** The Constant TH_CONTENT_LIST. */
    private static final String TH_SHOW_PHOTO = "showPhoto";
    /** The Constant TH_LOTATE_PHOTO. */
    private static final String TH_LOTATE_PHOTO = "lotatePhoto";

    /** The Constant SIZE_FULL. */
    public static int[] SIZE_FULL = {940, 520};

    /** The Constant BTN_SKIP_BACK. */
    private static final int BTN_SKIP_BACK = 0;
    /** The Constant BTN_STOP. */
    private static final int BTN_STOP = 1;
    /** The Constant BTN_PLAY. */
    private static final int BTN_PLAY = 2;
    /** The Constant BTN_SKIP_FORWARD. */
    private static final int BTN_SKIP_FORWARD = 3;
    /** The Constant BTN_RANDOM. */
    private static final int BTN_RANDOM = 4;
    /** The Constant BTN_REPEAT. */
    private static final int BTN_REPEAT = 5;
    /** The Constant BTN_ZOOM. */
    private static final int BTN_ZOOM = 6;
    /** The Constant BTN_ROTATE. */
    private static final int BTN_ROTATE = 7;

    /** The Constant ZOOM_MOVE_SIZE. */
    //public static final int ZOOM_MOVE_SIZE = 20;

    /** The Constant FLIPBAR_SHOW_SEC. */
    private static final int FLIPBAR_SHOW_SEC5 = 5;
    /** The Constant SLEEP_TIME. */
    private static final long SLEEP_TIME = 500L;
    /** The Constant FEED_SHOW_SEC. */
    private static final int FEED_SHOW_SEC = 3;
    /** The Constant FEED_PLAY. */
    private static final int FEED_PLAY = 0;
    /** The Constant FEED_PAUSE. */
    private static final int FEED_PAUSE = 1;
    /** The Constant ZOOM_VALUE. */
    public static final int[] ZOOM_VALUE = {1, 2, 4, 8};
    /** The Constant ZOOM_PART. */
    public static final int[] ZOOM_PART = {1, 3, 7, 15};

    /** The renderer. */
    private PhotoPlayRenderer renderer = null;
    /** The viewer bar. */
    public final FlipBar viewerBar = FlipBarFactory.getInstance().createPcsPhotoFlipBar();
    /** The zoom bar. */
    public final FlipBar zoomBar = FlipBarFactory.getInstance().createPcsPhotoZoomBar();
    /** The FlipBar instance. */
    public FlipBar flipBar = null;
    /** The timer spec. */
    private TVTimerSpec timerSpec = null;
    /** The photo list. */
    //public Vector photoList = null;
    /** The photo content. */
    public Content photoContent = null;
    /** The random buffer. */
    private Vector randomBuffer = null;
    /** The zoom buf img. */
    public DVBBufferedImage zoomBufImg = null;
    /** The imgae label. */
    private ImageLabel imageLabel = null;
    /** The feedback comp. */
    private FeedbackComp feedbackComp = null;

    /** The is timer. */
    private boolean isTimer = false;
    /** The button focus. */
    //private int buttonFocus = 2;
    /** The flipbar show count. */
    private int flipbarShowCount = 0;
    /** The flipbarlimit count. */
    private int flipbarlimitCount = FLIPBAR_SHOW_SEC5;
    /** The zoom moving point. */
    public int[] zoomMovingPoint = {0, 0};
    /** The photo focus. */
    public int photoFocus = 0;
    /** The is slide show. */
    private boolean isSlideShow = false;
    /** The is random. */
    private boolean isRandom = false;
    /** The is repeat. */
    private boolean isRepeat = false;
    /** The slideshow interval. */
    private int slideshowInterval = -1;
    /** The slideshow count. */
    private int slideshowCount = 0;
    /** The flip option. */
    private int flipOption = ImageLabel.NONE_EFFECT;
    /** The random focus. */
    private int randomFocus = 0;
    /** The feed show count. */
    public int feedShowCount = -1;
    /** The feed img focus. */
    public int feedImgFocus = -1;
    /** The parent content. */
    public Content parentContent = null;
    /** The folder count. */
    private int folderCount = 0;
    /** The thread id. */
    private int requestId = -1;
    /** The entry id. */
    private String entryId = null;

    /**
     * Initialize the Scene.
     */
    public PhotoPlayUI() {
        SIZE_FULL[0] = Config.photoMaxWidth;
        SIZE_FULL[1] = Config.photoMaxHeight;
        renderer = new PhotoPlayRenderer();
        setRenderer(renderer);
        //flipBar = FlipBarFactory.getInstance().createPcsPhotoFlipBar();
        flipBar = viewerBar;
        zoomBar.setBounds(36, 335, 175, 175);
        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME);
        timerSpec.setRepeat(true);
        clickEffect = new ClickingEffect(this, 5);
        feedbackComp = new FeedbackComp();
        zoomBufImg = new DVBBufferedImage(SIZE_FULL[0], SIZE_FULL[1]);
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        remove(footer);
        prepare();
        requestId = -1;
        feedbackComp.prepare();
        flipbarShowCount = 0;
        flipbarlimitCount = FLIPBAR_SHOW_SEC5;
        flipBar = viewerBar;
        footer.setShowBackgroundText(true);
        super.start(isReset, sendData);
        //buttonFocus = 2;
        isSlideShow = false;
        isRandom = false;
        isRepeat = false;
        //isRotate = false;
        slideshowCount = 0;
        feedShowCount = -1;
        flipOption = ImageLabel.NONE_EFFECT;
        entryId = null;
        //uiType = 0;
        String inervalStr = PreferenceProxy.getInstance().slideshowInterval;
        if (PreferenceProxy.INTERVAL_3.equals(inervalStr)) {
            slideshowInterval = 3;
        } else if (PreferenceProxy.INTERVAL_5.equals(inervalStr)) {
            slideshowInterval = 5;
        } else if (PreferenceProxy.INTERVAL_10.equals(inervalStr)) {
            slideshowInterval = 10;
        }
        loadData(new Object[]{TH_SHOW_PHOTO}, true);
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        if (flipBar == viewerBar) {
            footer.addButtonWithLabel(BasicRenderer.btnExitImg, BasicRenderer.btnCloseTxt);
        }
        footer.addButtonWithLabel(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
        processMusicFooterComp();
    }

    /**
     * Checks if is zoom mode for photo.
     *
     * @return true, if is zoom mode for photo
     * @see com.videotron.tvi.illico.pcs.ui.BasicUI#isZoomModeForPhoto()
     */
    protected boolean isZoomModeForPhoto() {
        return flipBar == zoomBar;
    }

    /**
     * Adds the footer.
     */
    private void addFooter() {
        processMusicFooterComp();
        add(musicFooterComp, 1);
        add(footer, 1);
    }

    /**
     * Removes the footer.
     */
    private void removeFooter() {
        remove(footer);
        remove(musicFooterComp);
        musicFooterComp.stop();
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        footer.setShowBackgroundText(false);
        stopTimer();
        renderer.stop();
        flipBar.removeListener(this);
        flipBar.stop();
        //photoList = null;
        if (photoContent != null) { photoContent.setImage(null); }
        photoContent = null;
        randomBuffer = null;
        conImgPool.clear();
        if (imageLabel != null) { remove(imageLabel); }
        imageLabel = null;
        isSlideShow = false;
        parentContent = null;
        folderCount = 0;
        requestId = -1;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
    }

    /**
     * Checks if is slide show.
     *
     * @return true, if is slide show
     */
    public boolean isSlideShow() {
        return isSlideShow && slideshowInterval > 0
            && parentContent != null && parentContent.getChildCount() > folderCount;
    }

    /**
     * Stop slide show.
     */
    public void stopSlideShow() {
        if (isSlideShow) {
            isSlideShow = false;
            FlipBarAction action = FlipBarAction.PLAY;
            flipBar.setAction(action, BTN_PLAY);
            flipBar.setFocus(BTN_PLAY);
        }
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        if (isTimer) { return; }
        try {
            timerSpec.addTVTimerWentOffListener(this);
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            isTimer = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        if (!isTimer) { return; }
        if (timerSpec != null) {
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
            isTimer = false;
        }
    }

    /**
     * implements the method of TVTimerWentOffListener.
     *
     * @param e the e
     */
    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (!isTimer) { return; }
        if (feedShowCount != -1) {
            feedShowCount++;
            if (feedShowCount * SLEEP_TIME >= FEED_SHOW_SEC * 1000L) {
                feedShowCount = -1;
                feedImgFocus = -1;
                repaint();
            }
        }
        if (flipbarShowCount != -1) {
            flipbarShowCount++;
            if (flipbarShowCount * SLEEP_TIME >= flipbarlimitCount * 1000L) {
                flipbarShowCount = -1;
                removeFooter();
                if (flipBar == viewerBar) {
                    flipBar.stop();
                } else {
                    flipBar.setAlpha(true);
                }
                repaint();
            }
        }
        if (isSlideShow() && slideshowCount > -1) {
            slideshowCount++;
            Logger.debug(this, "timerWentOff()-slideshowCount : " + slideshowCount);
            if (slideshowCount * SLEEP_TIME >= slideshowInterval * 1000L) {
                int random = randomFocus;
                int photo = photoFocus;
                int curPhotoFocus = photoFocus;
                boolean isChange = false;
                int totalCount = parentContent.getChildCount();
                if (isRandom && totalCount - folderCount > 2) {
                    if (random >= randomBuffer.size() - 1) {
                        random = randomBuffer.size() - 1;
                        if (isRepeat) {
                            random = 0;
                            photo = Integer.parseInt((String) randomBuffer.elementAt(random));
                            isChange = true;
                        }
                    } else {
                        random++;
                        photo = Integer.parseInt((String) randomBuffer.elementAt(random));
                        isChange = true;
                    }
                } else {
                    if (photo >= totalCount - 1) {
                        photo = totalCount - 1;
                        if (isRepeat) {
                            photo = folderCount;
                            isChange = true;
                        }
                    } else {
                        photo++;
                        isChange = true;
                    }
                }
                if (slideshowCount > -1) { slideshowCount = 0; }
                if (isChange) {
                    final Content content = HNHandler.getInstance().getPlayContent(entryId, MENU_PHOTO,
                            parentContent.getChildSort(), photo, false, null);
                    if (content == null) {
                        showDataErrorPopup();
                        return;
                    }
                    Image img = conImgPool.getImage(content.getImgUrl());
                    if (!checkFocus(curPhotoFocus)) {
                        if (slideshowCount > -1) { slideshowCount = 0; }
                        return;
                    }
                    if (img != null) {
                        content.setImage(img);
                    } else {
                        String key = content.getId() + content.getImgUrl();
                        content.setImage(PhotoPlayUI.this.getContentImage(key,
                                content.getImgUrl(), null, SIZE_FULL));
                        conImgPool.waitForAll();
                    }
                    if (!checkFocus(curPhotoFocus)) {
                        if (slideshowCount > -1) { slideshowCount = 0; }
                        return;
                    }
                    final int finalPhoto = photo;
                    final int finalRandom = random;
                    if (!isSlideShow) { return; }
                    slideshowCount = -1;
                    EventQueue.invokeLater(new Runnable() {
                        public void run() {
                            if (isSlideShow) {
                                feedShowCount = -1;
                                feedImgFocus = -1;
                                hideFlipbar();
                                repaint();
                                slideshowCount = 0;
                                photoFocus = finalPhoto;
                                setFlipBarContent(content, finalPhoto);
                                parentContent.setLastFocus(photoFocus);
                                parentContent.setLastFocusId(content.getId());
                                randomFocus = finalRandom;
                                setImage(content, finalPhoto, true);
                            }
                            slideshowCount = 0;
                        }
                    });
                }
            }
        }
    }

    /**
     * Reset flipbar count.
     */
    private void resetFlipbarCount() {
        if (flipbarShowCount != -1) { flipbarShowCount = 0; }
        if (flipBar == zoomBar) {
            flipbarShowCount = 0;
            if (flipBar.isAlpha()) {
                addFooter();
                flipBar.setAlpha(false);
                repaint();
            }
        }
    }

    /**
     * show flipbar and stop animation for viewer. called by OK/UP/Down keys.
     */
    private void showFlipbar() {
        slideshowCount = 0;
        flipBar.setAction(FlipBarAction.PLAY, BTN_PLAY);
        int itemCount = 1;
        if (parentContent != null) { itemCount = parentContent.getChildCount() - folderCount; }
        if (itemCount > 1) {
            flipBar.setFocus(BTN_PLAY);
        } else { flipBar.setFocus(BTN_STOP); }
        isSlideShow = false;
        flipbarShowCount = 0;
        addFooter();
        feedShowCount = -1;
        feedImgFocus = -1;
        flipBar.start();
        repaint();
    }

    /**
     * Show feed image.
     */
    private void showFeedImg() {
        feedImgFocus = FEED_PLAY;
        if (!isSlideShow) { feedImgFocus = FEED_PAUSE; }
        feedShowCount = 0;
    }

    /**
     * Hide flipbar.
     */
    private void hideFlipbar() {
        flipbarShowCount = -1;
        removeFooter();
        if (flipBar == zoomBar) {
            flipBar.setAlpha(true);
        } else { flipBar.stop(); }
    }

    /**
     * implement handleKey method.
     * added to block all input keys while lotation is excuting.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    /*public boolean handleKey(int code) {
        if (slideshowCount == -1) { return true; }
        return super.handleKey(code);
    }*/

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        boolean isFlipbarAlpha = flipBar.isAlpha();
        switch (keyCode) {
        case Rs.KEY_LEFT:
            resetFlipbarCount();
            if (flipBar == zoomBar) {
                if (!isFlipbarAlpha && zoomMovingPoint[0] > 0) {
                    zoomMovingPoint[0]--;
                    repaint();
                }
            } else {
                if (flipBar.isVisible()) {
                    flipBar.handleKey(keyCode);
                } else { processPlayKey(keyCode); }
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            resetFlipbarCount();
            if (flipBar == zoomBar) {
                int zoomPart = ZOOM_PART[flipBar.getFocus()];
                if (!isFlipbarAlpha && zoomMovingPoint[0] < zoomPart - 1) {
                    zoomMovingPoint[0]++;
                    repaint();
                }
            } else {
                if (flipBar.isVisible()) {
                    flipBar.handleKey(keyCode);
                } else { processPlayKey(keyCode); }
                repaint();
            }
            break;
        case Rs.KEY_UP:
            resetFlipbarCount();
            if (flipBar == zoomBar) {
                if (!isFlipbarAlpha && zoomMovingPoint[1] > 0) {
                    zoomMovingPoint[1]--;
                    repaint();
                }
            } else if (!flipBar.isVisible()) { showFlipbar(); }
            break;
        case Rs.KEY_DOWN:
            resetFlipbarCount();
            if (flipBar == zoomBar) {
                int zoomPart = ZOOM_PART[flipBar.getFocus()];
                if (!isFlipbarAlpha && zoomMovingPoint[1] < zoomPart - 1) {
                    zoomMovingPoint[1]++;
                    repaint();
                }
            } else if (!flipBar.isVisible()) { showFlipbar(); }
            break;
        case Rs.KEY_ENTER:
            resetFlipbarCount();
            if (flipBar == zoomBar) {
                if (isFlipbarAlpha) { break; }
                int focus = flipBar.getFocus();
                if (++focus == 4) { focus = 0; }
                if (focus == 0) {
                    zoomMovingPoint[0] = 0;
                    zoomMovingPoint[1] = 0;
                } else {
                    zoomMovingPoint[0] += zoomMovingPoint[0] + 1;
                    zoomMovingPoint[1] += zoomMovingPoint[1] + 1;
                }
                flipBar.setFocus(focus);
                repaint();
            } else {
                if (!flipBar.isVisible()) {
                    showFlipbar();
                } else {
                    flipBar.handleKey(keyCode);
                    processPlayKey(keyCode);
                }
            }
            break;
        case KeyCodes.PLAY:
        case KeyCodes.STOP:
        case KeyCodes.PAUSE:
        case KeyCodes.BACK:
        case KeyCodes.FORWARD:
            if (flipBar != zoomBar) {
                resetFlipbarCount();
                processPlayKey(keyCode);
            } else if (keyCode == KeyCodes.BACK || keyCode == KeyCodes.FORWARD) {
                processExitKey();
                resetFlipbarCount();
                processPlayKey(keyCode);
            } else if (keyCode == KeyCodes.STOP) { processExitKey(); }
            break;
        case Rs.KEY_BACK:
            if (flipBar != zoomBar) {
                resetFlipbarCount();
            }
            processPlayKey(KeyCodes.STOP);
            break;
        case Rs.KEY_EXIT:
            if (flipBar == zoomBar && !flipBar.isAlpha()) {
                clickEffect.start(renderer.exitBtnPosX, 488, 31, 22);
            } else if (flipBar == viewerBar && flipBar.isVisible()) {
                footer.clickAnimation(BasicRenderer.btnExitImg);
            }
            processExitKey();
            break;
        case KeyCodes.LIVE:
            processExitKey();
            break;
        case KeyCodes.COLOR_B:
            if (flipBar.isVisible() && !flipBar.isAlpha()) { footer.clickAnimation(BasicRenderer.btnMPlayerTxt); }
            isSlideShow = false;
            hideFlipbar();
            musicPlayerPopup.resetFocus();
            addPopup(musicPlayerPopup);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Process exit key.
     */
    private void processExitKey() {
        if (flipBar == zoomBar) {
            flipbarShowCount = 0;
            flipBar.stop();
            remove(flipBar);
            flipBar = viewerBar;
            flipBar.start();
            add(flipBar, 0);
            addFooterBtn();
            add(footer, 1);
            add(feedbackComp);
            add(imageLabel);
            repaint();
        } else if (flipBar.isVisible()) {
            hideFlipbar();
            repaint();
        } else { processPlayKey(KeyCodes.STOP); }
    }

    /**
     * Process play key.
     *
     * @param keyCode the key code
     */
    private void processPlayKey(int keyCode) {
        Logger.debug(this, "called processPlayKey()-keyCode : " + keyCode);
        int btnFocus = -1;
        switch (keyCode) {
        case Rs.KEY_ENTER:
            btnFocus = flipBar.getFocus();
            break;
        case KeyCodes.BACK:
        case Rs.KEY_LEFT:
            btnFocus = BTN_SKIP_BACK;
            break;
        case KeyCodes.STOP:
            btnFocus = BTN_STOP;
            break;
        case KeyCodes.PLAY:
            btnFocus = BTN_PLAY;
            break;
        case KeyCodes.FORWARD:
        case Rs.KEY_RIGHT:
            btnFocus = BTN_SKIP_FORWARD;
            break;
        case KeyCodes.PAUSE:
            btnFocus = BTN_PLAY;
            break;
        default:
            break;
        }
        Logger.debug(this, "processPlayKey()-btnFocus : " + btnFocus);
        int totalCount = 1;
        int itemCount = 1;
        if (parentContent != null) {
            totalCount = parentContent.getChildCount();
            itemCount = totalCount - folderCount;
        }
        Logger.debug(this, "processPlayKey()-totalCount : " + totalCount);
        Logger.debug(this, "processPlayKey()-itemCount : " + itemCount);
        if (itemCount <= 1 && (btnFocus == BTN_SKIP_BACK || btnFocus == BTN_PLAY
                || btnFocus == BTN_SKIP_FORWARD || btnFocus == BTN_REPEAT || btnFocus == BTN_RANDOM)) {
            return;
        }

        if (btnFocus == BTN_SKIP_BACK) {
            slideshowCount = 0;
            if (itemCount > 1) {
                if (isRandom && itemCount > 2) {
                    if (randomFocus > 0) {
                        randomFocus--;
                        photoFocus = Integer.parseInt((String) randomBuffer.elementAt(randomFocus));
                        showNextImage(false);
                    }
                } else {
                    int checkNum = 0;
                    if (parentContent.isFirstFolder()) { checkNum = folderCount; }
                    if (photoFocus > checkNum) {
                        photoFocus--;
                        showNextImage(false);
                    }
                }
            }
            slideshowCount = 0;
        } else if (btnFocus == BTN_STOP) {
            isSlideShow = false;
            slideshowCount = 0;
            Content content = (Content) receivedData;
            Content sendContent = null;
            if (content.isFolder()) { sendContent = content; }
            SceneManager.getInstance().goToScene(previousSceneID, false, sendContent);
            return;
        } else if (btnFocus == BTN_PLAY) {
            slideshowCount = 0;
            if (keyCode == Rs.KEY_ENTER) {
                isSlideShow = !isSlideShow;
                if (isSlideShow) { hideFlipbar(); }
            } else if (keyCode == KeyCodes.PLAY) {
                if (!isSlideShow) { flipBar.setAction(FlipBarAction.PAUSE, BTN_PLAY); }
                isSlideShow = true;
                hideFlipbar();
            } else if (keyCode == KeyCodes.PAUSE) {
                isSlideShow = !isSlideShow;
                FlipBarAction action = FlipBarAction.PAUSE;
                if (!isSlideShow) { action = FlipBarAction.PLAY; }
                flipBar.setAction(action, BTN_PLAY);
                flipBar.setFocus(BTN_PLAY);
                if (!isSlideShow) {
                    flipbarShowCount = 0;
                    addFooter();
                    flipBar.start();
                } else { hideFlipbar(); }
            }
            showFeedImg();
        } else if (btnFocus == BTN_SKIP_FORWARD) {
            slideshowCount = 0;
            if (itemCount > 1) {
                if (isRandom && itemCount > 2) {
                    if (randomFocus >= randomBuffer.size() - 1) {
                        randomFocus = 0;
                    } else { randomFocus++; }
                    photoFocus = Integer.parseInt((String) randomBuffer.elementAt(randomFocus));
                } else {
                    int checkNum = itemCount;
                    if (parentContent.isFirstFolder()) { checkNum = totalCount; }
                    if (photoFocus >= checkNum - 1) {
                        photoFocus = 0;
                        if (parentContent.isFirstFolder()) { photoFocus = folderCount; }
                    } else { photoFocus++; }
                }
                showNextImage(false);
            }
            slideshowCount = 0;
        } else if (btnFocus == BTN_RANDOM) {
            slideshowCount = 0;
            isRandom = !isRandom;
            if (isRandom && itemCount > 2) {
                Vector temp = new Vector();
                int initNum = 0;
                int checkNum = itemCount;
                if (parentContent.isFirstFolder()) {
                    initNum = folderCount;
                    checkNum = totalCount;
                }
                for (int i = initNum; i < checkNum; i++) {
                    temp.addElement(i + "");
                }
                randomBuffer = new Vector();
                randomFocus = 0;
                randomBuffer.addElement(temp.remove(photoFocus - initNum));
                while (!temp.isEmpty()) {
                    int random = (int) (Math.random() * temp.size());
                    randomBuffer.addElement(temp.remove(random));
                }
            } else {
                randomBuffer = null;
            }
        } else if (btnFocus == BTN_REPEAT) {
            isRepeat = !isRepeat;
        } else if (btnFocus == BTN_ZOOM) {
            slideshowCount = 0;
            flipbarShowCount = 0;
            if (isSlideShow) { flipBar.setAction(FlipBarAction.PLAY, BTN_PLAY); }
            isSlideShow = false;
            flipBar.stop();
            remove(flipBar);
            remove(imageLabel);
            remove(feedbackComp);
            flipBar = zoomBar;
            flipBar.setFocus(1);
            flipBar.setAlpha(false);
            flipBar.start();
            add(flipBar, 0);
            addFooterBtn();
            add(footer, 1);
            zoomMovingPoint[0] = 1;
            zoomMovingPoint[1] = 1;
            feedShowCount = -1;
            feedImgFocus = -1;
        } else if (btnFocus == BTN_ROTATE) {
            //setRotateImage();
            if (photoContent != null && photoContent.getImage() != null) {
                slideshowCount = -1;
                loadData(new Object[]{TH_LOTATE_PHOTO}, true);
            }
        }
        resetFlipbarCount();
        repaint();
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(TH_SHOW_PHOTO)) {
            Content content = (Content) receivedData;
            entryId = content.getId();
            String searchId = null;
            photoFocus = -1;
            int sortType = -1;
            Content reqContent = null;
            if (searchContent == null) {
                if (content.isFolder()) {
                    photoFocus = 0;
                    parentContent = content;
                    parentContent.setChildSort(Content.SORT_ALPHA_A);
                    entryId = parentContent.getId();
                } else {
                    parentContent = content.getParent();
                    photoFocus = parentContent.getLastFocus();
                    entryId = parentContent.getBrowseId();
                }
                sortType = parentContent.getChildSort();
                folderCount =
                    HNHandler.getInstance().requestSearchFolderCount(entryId, sortType);
                Logger.debug(this, "notifyFromDataLoadingThread()-parentContent.getChildCount() : "
                        + parentContent.getChildCount());
                if (folderCount == -1 || parentContent.getChildCount() < folderCount) {
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    showDataErrorPopup();
                    return;
                } else if (content.isFolder() && parentContent.getChildCount() == folderCount) {
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    notiPopup.setMode(MENU_PHOTO);
                    addPopup(notiPopup);
                    return;
                }
                if (content.isFolder()) {
                    photoFocus = folderCount;
                    parentContent.setLastFocus(photoFocus);
                }
                reqContent = HNHandler.getInstance().getPlayContent(entryId, MENU_PHOTO, sortType,
                        photoFocus, false, searchId);
            } else {
                entryId = content.getParentId();
                searchId = content.getId();
                reqContent = content;
            }
            //Content reqContent = HNHandler.getInstance().getPlayContent(entryId, MENU_PHOTO, sortType,
            //        photoFocus, false);
            if (reqContent == null) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                //notiPopup.setMode(MENU_PHOTO);
                //addPopup(notiPopup);
                showDataErrorPopup();
                return;
            }
            parentContent.setLastFocusId(reqContent.getId());
            FlipBarContent flipBarContent = new FlipBarContent(reqContent.getName(), 0, 0);
            flipBar = viewerBar;
            flipBar.setContent(flipBarContent);
            setFlipBarContent(reqContent, photoFocus);
            flipBar.addListener(this);
            flipBar.setFocus(BTN_PLAY);
            //flipBar.setDisplayActions(flipbarActions);
            flipBar.setAction(FlipBarAction.PLAY, BTN_PLAY);
            flipBar.setAction(FlipBarAction.RANDOM_OFF, BTN_RANDOM);
            flipBar.setAction(FlipBarAction.REPEAT_OFF, BTN_REPEAT);
            boolean enableBtn = true;
            int itemCount = 1;
            if (parentContent != null) { itemCount = parentContent.getChildCount() - folderCount; }
            if (itemCount < 2) {
                enableBtn = false;
                flipBar.setFocus(BTN_STOP);
            }
            for (int i = 0; i <= 5; i++) {
                if (i == 1) { continue; }
                flipBar.setEnabled(i, enableBtn);
            }
            flipBar.start();
            setImage(reqContent, photoFocus, false);
            CommunicationManager.getInstance().hideLoadingAnimation();
            add(flipBar);
            add(footer);
            add(feedbackComp);
            if (imageLabel != null) { add(imageLabel); }
            photoContent = reqContent;
            startTimer();
            repaint();
        } else if (procId.equals(TH_LOTATE_PHOTO)) {
            setRotateImage();
            CommunicationManager.getInstance().hideLoadingAnimation();
        }
    }

    /**
     * Show next image using Thread.
     * @param calledBySlideShow the called by slide show
     */
    private void showNextImage(final boolean calledBySlideShow) {
        Logger.debug(this, "showNextImage()-");
        final int focus = photoFocus;
        final Content reqNode = HNHandler.getInstance().getPlayContent(entryId, MENU_PHOTO,
                parentContent.getChildSort(), focus, false, null);
        if (reqNode == null) {
            showDataErrorPopup();
            return;
        }
        setFlipBarContent(reqNode, focus);
        parentContent.setLastFocus(focus);
        parentContent.setLastFocusId(reqNode.getId());
        final int reqId = ++requestId;
        new Thread("PhotoPlayUI|showNextImage()") {
            public void run() {
                processNextImage(reqNode, calledBySlideShow, reqId, focus);
            }
        } .start();
    }

    /**
     * Sets the flip bar content.
     *
     * @param reqNode the req node
     * @param focus the focus
     */
    private void setFlipBarContent(Content reqNode, int focus) {
        FlipBarContent flipBarContent = flipBar.getContents()[0];
        flipBarContent.setName(reqNode.getName());
        String data = "(1/1)";
        if (parentContent != null) {
            int initNum = 0;
            if (parentContent.isFirstFolder()) { initNum = folderCount; }
            data = "(" + (focus - initNum + 1) + "/"
                + (parentContent.getChildCount() - folderCount) + ")";
        }
        flipBarContent.setData(data);
        //flipBar.setContent(flipBarContent);
    }

    /**
     * Process next image.
     *
     * @param reqNode the req node
     * @param calledBySlideShow the called by slide show
     * @param reqId the req id
     * @param reqFocus the req focus
     */
    private synchronized void processNextImage(final Content reqNode, final boolean calledBySlideShow, final int reqId,
            final int reqFocus) {
        Logger.debug(this, "called processNextImage()-reqId : " + reqId);
        if (!checkFocus(reqFocus) || reqId != requestId) { return; }
        try {
            Thread.sleep(300L);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!checkFocus(reqFocus) || reqId != requestId
                || (photoContent != null && photoContent.getId() == reqNode.getId())) {
            return;
        }
        Logger.debug(this, "processNextImage()-call setImage()");
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                setImage(reqNode, reqFocus, calledBySlideShow);
            }
        });
    }

    /**
     * Sets the image from PC.
     *
     * @param content the content
     * @param focus the focus
     * @param calledBySlideShow the called by slide show
     */
    private void setImage(Content content, int focus, boolean calledBySlideShow) {
        Logger.debug(this, "setImage()-");
        if (content != null) {
            if (!calledBySlideShow) {
                if (!checkFocus(focus)) { return; }
                String key = content.getId() + content.getImgUrl();
                content.setImage(PhotoPlayUI.this.getContentImage(key, content.getImgUrl(), null,
                        SIZE_FULL));
                conImgPool.waitForAll();
            }
            if (!checkVisible()) { return; }
            if (checkFocus(focus)) {
                final Content oldContent = photoContent;
                photoContent = content;
                Graphics g = zoomBufImg.getGraphics();
                int bufImgWidth = zoomBufImg.getWidth();
                int bufImgHeight = zoomBufImg.getHeight();
                g.clearRect(0, 0, bufImgWidth, bufImgHeight);
                final Image bufImg = content.getImage();
                if (bufImg != null) {
                    int posX = ((bufImgWidth - bufImg.getWidth(null)) / 2);
                    int posY = ((bufImgHeight - bufImg.getHeight(null)) / 2);
                    g.drawImage(bufImg, posX, posY, this);

                    if (imageLabel == null) {
                        imageLabel = new ImageLabel(bufImg);
                        imageLabel.setSize(960, 540);
                        imageLabel.setAlign(true);
                    } else {
                        int effectType = getEffectType(calledBySlideShow);
                        if (effectType == ImageLabel.NONE_EFFECT) {
                            imageLabel.replaceImage(bufImg, effectType);
                            repaint();
                        } else {
                            final int replaceType = effectType;
                            imageLabel.replaceImage(bufImg, replaceType);
                            repaint();
                            flushImage(oldContent);
                            Logger.debug(this, "setImage()-end");
                            return;
                        }
                    }
                } else {
                    Logger.debug(this, "setImage()-Cannot get the image. So show error message.");
                    showDataErrorPopup();
                }
                flushImage(oldContent);
            } else {
                Logger.debug(this, "setImage()-return false.-slideshowCount : " + slideshowCount);
                flushImage(content);
            }
        }
    }

    /**
     * Flush image.
     *
     * @param content the Content
     */
    private void flushImage(Content content) {
        if (content == null) { return; }
        content.setImage(null);
        conImgPool.remove("rotate");
        String key = content.getId() + content.getImgUrl();
        conImgPool.remove(key);
    }

    /**
     * Gets the effect type.
     *
     * @param calledBySlideShow the called by slide show
     * @return the effect type
     */
    private int getEffectType(boolean calledBySlideShow) {
        int effectType = ImageLabel.NONE_EFFECT;
        String slideshowEffect = PreferenceProxy.getInstance().slideshowEffect;
        if (calledBySlideShow && slideshowEffect != null && !slideshowEffect.equals(PreferenceProxy.EFFECT_NO)) {
            if (slideshowEffect.equals(PreferenceProxy.EFFECT_RANDOM)) {
                effectType = (int) (Math.random() * 6) + 1;
            } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_FADE)) {
                effectType = ImageLabel.FADE_EFFECT;
            } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_DISSOLVE)) {
                effectType = ImageLabel.DISSOLVE_EFFECT;
            } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_PAGEFLIP)) {
                if (flipOption >= ImageLabel.FLIP_FROM_TOP && flipOption < ImageLabel.FLIP_FROM_RIGHT) {
                    flipOption++;
                } else { flipOption = ImageLabel.FLIP_FROM_TOP; }
                effectType = flipOption;
            }
        }
        return effectType;
    }

    /**
     * Sets the image rotated.
     */
    private void setRotateImage() {
        Logger.debug(this, "called setRotateImage()");
        Content reqContent = photoContent;
        Image prevImage = reqContent.getImage();
        Image img = conImgPool.getRotateImage(prevImage, this);
        Logger.debug(this, "setRotateImage()-rotate image : " + img);
        if (!checkVisible() || img == null) {
            slideshowCount = 0;
            CommunicationManager.getInstance().hideLoadingAnimation();
            return;
        }
        reqContent.setImage(img);
        String key = reqContent.getId() + reqContent.getImgUrl();
        conImgPool.remove(key);
        conImgPool.flushImage(prevImage);
        Logger.debug(this, "setRotateImage()-completed to flush prevImage");
        Graphics g = zoomBufImg.getGraphics();
        int bufImgWidth = zoomBufImg.getWidth();
        int bufImgHeight = zoomBufImg.getHeight();
        g.clearRect(0, 0, bufImgWidth, bufImgHeight);
        Logger.debug(this, "setRotateImage()-completed to clearRect zoomBufImg");
        Image bufImg = reqContent.getImage();
        if (bufImg != null) {
            int[] imgSize = {bufImg.getWidth(null), bufImg.getHeight(null)};
            int[] scaledSize = getScaledSizeForRotate(bufImg.getWidth(null), bufImg.getHeight(null));
            int posX = ((bufImgWidth - scaledSize[0]) / 2);
            int posY = ((bufImgHeight - scaledSize[1]) / 2);
            g.drawImage(bufImg, posX, posY, scaledSize[0] + posX, scaledSize[1] + posY,
                    0, 0, imgSize[0], imgSize[1], this);
            Logger.debug(this, "setRotateImage()-completed to draw into zoomBufImg");
        }
        CommunicationManager.getInstance().hideLoadingAnimation();
        imageLabel.replaceImage(zoomBufImg, ImageLabel.NONE_EFFECT);
        imageLabel.repaint();
        slideshowCount = 0;
        Logger.debug(this, "setRotateImage()-end");
    }

    /**
     * Gets the scaled size.
     *
     * @param imgWidth the img width
     * @param imgHeight the img height
     * @return the scaled size
     */
    private int[] getScaledSizeForRotate(int imgWidth, int imgHeight) {
        double wRatio = (double) imgWidth / (double) (SIZE_FULL[0]);
        double hRatio = (double) imgHeight / (double) (SIZE_FULL[1]);
        double divideValue = hRatio;
        if (wRatio >= hRatio) { divideValue = wRatio; }
        return new int[] {(int) (imgWidth / divideValue), (int) (imgHeight / divideValue)};
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @return true, if successful
     */
    private boolean checkFocus(int reqIndex) {
        if (SceneManager.getInstance().curSceneId != SceneManager.SC_PHOTO_PLAY) {
            return false;
        }
        return reqIndex == photoFocus && slideshowCount > -1;
    }

    /**
     * Show data error popup.
     */
    private void showDataErrorPopup() {
        stopSlideShow();
        hideFlipbar();
        showErrorMessage(ERRMSG_INIT,
                MainManager.getInstance().getErrorCode(MainManager.ERR_LOST_CONNECTION));
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            removePopup();
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
        } else if (popup instanceof MusicPlayerPopup) {
            if (flipBar == zoomBar && flipBar.isAlpha()) { removeFooter(); }
            removePopup();
            if (flipBar == viewerBar) { showFlipbar(); }
        } else { super.requestPopupOk(para); }
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param action the FlipBarAction
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener
     *      #actionPerformed(com.videotron.tvi.illico.flipbar.FlipBarAction)
     */
    public void actionPerformed(FlipBarAction action) {
        Logger.debug(this, "called actionPerformed()-action name : " + action.getName());
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param time the time
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener#notifyShown(long)
     */
    public void notifyShown(long time) {
        Logger.debug(this, "called notifyShown()-time : " + time);
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param selection the selection
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener#notifyHidden(boolean)
     */
    public void notifyHidden(boolean selection) {
        Logger.debug(this, "called notifyHidden()-selection : " + selection);
    }

    /**
     * <code>FeedbackComp</code> the component for feedback.
     *
     * @since   2011.10.27
     * @author  tklee
     */
    public class FeedbackComp extends UIComponent {
        /** Is the serialVersionID. */
        private static final long serialVersionUID = 1L;

        /**
         * Initialize the Component.
         */
        public FeedbackComp() {
            setBounds(0, 0, 960, 540);
            setRenderer(new FeedbackRenderer());
        }

        /**
         * <code>PopupRenderer</code> The class to paint a GUI.
         *
         * @since   2011.05.20
         * @version $Revision: 1.38 $ $Date: 2012/10/18 11:29:56 $
         * @author  tklee
         */
        public class FeedbackRenderer extends Renderer {
            /** The feed img name. */
            private final String[] feedImgName = {"04_flip_st_05_foc_b.png", "04_flip_st_04_foc_b.png"};

            /** The feed back img. */
            private Image[] feedBackImg = new Image[feedImgName.length];
            /** The bg img. */
            private Image bgImg = null;

            /**
             * Get the renderer bound of UI .
             *
             * @param c the UIComponent
             * @return the preferred bounds
             */
            public Rectangle getPreferredBounds(UIComponent c) {
                Logger.debug(this, "c.getBounds() : " + c.getBounds());
                return c.getBounds();
            }

            /**
             * This will be called before the related UIComponent is shown.
             *
             * @param c the c
             */
            public void prepare(UIComponent c) {
                bgImg = BasicRenderer.getImage("04_flip_st_foc_b01.png");
                for (int i = 0; i < feedImgName.length; i++) {
                    feedBackImg[i] = BasicRenderer.getImage(feedImgName[i]);
                }
                FrameworkMain.getInstance().getImagePool().waitForAll();
            }

            /**
             * Graphics paint.
             *
             * @param g the Graphics
             * @param c the UIComponent
             */
            protected void paint(Graphics g, UIComponent c) {
                if (feedShowCount == -1) { return; }
                int feedFocus = feedImgFocus;
                Logger.debug(this, "feedImgFocus : " + feedFocus);
                if (feedFocus >= 0) {
                    g.drawImage(bgImg, 429, 197, c);
                    g.drawImage(feedBackImg[feedFocus], 451, 223, c);
                }
            }
        }
    }
}

/*
 *  @(#)SelectPopup.java 1.0 2011.05.20
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.ui.comp.AutoScroller;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SelectPopup</code> the popup for selection.
 *
 * @since   2011.05.20
 * @version $Revision: 1.10 $ $Date: 2012/08/03 06:16:24 $
 * @author  tklee
 */
public class DeviceSelectPopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_CONNECTION. */
    public static final int MODE_CONNECTION = 0;
    /** The Constant MODE_FAVORITE. */
    public static final int MODE_FAVORITE = 1;
    /** The Constant MODE_CONNECTION_CHANGE. */
    public static final int MODE_CONNECTION_CHANGE = 2;

    /** The Constant ROWS_PER_PAGE. */
    private static final int ROWS_PER_PAGE = 5;
    /** The Constant MIDDLE_POS. */
    private static final int MIDDLE_POS = 2;

    /** The Constant STATE_LIST. */
    private static final int STATE_LIST = 0;
    /** The Constant STATE_BTN. */
    private static final int STATE_BTN = 1;

    /** The list ticker. */
    private AutoScroller listTicker = null;
    /** The device infos. */
    private Vector deviceInfos = null;
    /** The list focus. */
    private int listFocus = 0;
    /** The focus position. */
    private int focusPosition = 0;
    /** The state focus. */
    private int stateFocus = 0;

    /**
     * Initialize the Component.
     */
    public DeviceSelectPopup() {
        setBounds(276, 57, 410, 474);
        //renderer = new PopupRenderer();
        setRenderer(new PopupRenderer());
        clickEffect = new ClickingEffect(this, 5);
        listTicker = new AutoScroller();
        listTicker.setSleepTime(Config.autoscrollSleepSec * 1000L);
        listTicker.setFont(Rs.F18);
        listTicker.setTextColor(Rs.C1);
        listTicker.setTextPosY(21);
        listTicker.setAlign(AutoScroller.ALIGN_LEFT);
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        prepare();
        setTicker();
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() {
        deviceInfos = null;
        listTicker.stop();
    }

    /**
     * reset focus index.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        listFocus = 0;
        stateFocus = 0;
        focusPosition = 0;
    }

    /**
     * Sets the list focus.
     *
     * @param focus the new list focus
     */
    public void setListFocus(int focus) {
        listFocus = focus;

        int totalCount = deviceInfos.size();
        focusPosition = 0;
        if (focus < MIDDLE_POS || totalCount <= ROWS_PER_PAGE) {
            focusPosition = focus;
        } else {
            focusPosition = ROWS_PER_PAGE - (totalCount - focus);
            if (focusPosition < MIDDLE_POS) { focusPosition = MIDDLE_POS; }
        }
    }

    /**
     * Sets the data.
     *
     * @param devInfos DeviceInfo list by Vector
     */
    public void setData(Vector devInfos) {
        deviceInfos = devInfos;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (stateFocus == STATE_LIST) {
                if (listFocus > 0) {
                    if (focusPosition != MIDDLE_POS || listFocus == focusPosition) { focusPosition--; }
                    listFocus--;
                    setTicker();
                    repaint();
                }
            } else {
                stateFocus = STATE_LIST;
                setTicker();
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (stateFocus == STATE_LIST) {
                int totalCount = deviceInfos.size();
                if (listFocus < totalCount - 1) {
                    if (focusPosition != MIDDLE_POS || focusPosition + listFocus >= totalCount - 1) {
                        focusPosition++;
                    }
                    listFocus++;
                } else { stateFocus = STATE_BTN; }
                setTicker();
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            if (stateFocus == STATE_LIST) {
                clickEffect.start(343 - 276, 170 + (listFocus * 31) - 57, 615 - 343, 32);
                scene.requestPopupOk(deviceInfos.elementAt(listFocus));
            } else {
                clickEffect.start(406 - 276, 402 - 57, 561 - 406, 32);
                //SceneManager.getInstance().requestStartMenuApp();
                scene.requestPopupOk(null);
            }
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            if (mode == MODE_CONNECTION) {
                scene.addExitConfirmPopup(keyCode);
            } else if (mode == MODE_CONNECTION_CHANGE) {
                scene.requestPopupOk(null);
            } else { scene.requestPopupClose(); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Sets the ticker.
     */
    private void setTicker() {
        if (stateFocus == STATE_LIST) {
            listTicker.setBounds(383 - 276, 170 + (focusPosition * 31) - 57, 200, 36);
            DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(listFocus);
            listTicker.setText(devInfo.getDeviceName(), null);
            add(listTicker);
        } else {
            listTicker.setText(null, null);
            remove(listTicker);
        }
        //repaint();
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.10 $ $Date: 2012/08/03 06:16:24 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The title text tag. */
        private final String[] titleTextTag = {"devSelectTitle", "favoriteSelectTitle", "devSelectTitle"};
        /** The desc text tag. */
        private final String[] descTextTag = {"devSelectDesc", "favoriteSelectDesc", "devSelectDesc"};

        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The list bg img. */
        private Image listBgImg = null;
        /** The list focus img. */
        private Image listFocusImg = null;
        /** The icon question focus img. */
        private Image iconQFocusImg = null;
        /** The icon question img. */
        private Image iconQImg = null;
        /** The icon check focus img. */
        private Image iconCheckFocusImg = null;
        /** The icon check img. */
        private Image iconCheckImg = null;
        /** The iconfavor focus img. */
        private Image iconfavorFocusImg = null;
        /** The iconfavor img. */
        private Image iconfavorImg = null;
        /** The list shadow t img. */
        private Image listShadowTImg = null;
        /** The list shadow b img. */
        private Image listShadowBImg = null;
        /** The arrow t img. */
        private Image arrowTImg = null;
        /** The arrow b img. */
        private Image arrowBImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The btn dim img. */
        private Image btnDimImg = null;

        /** The title txt. */
        private String titleTxt = null;
        /** The desc txt. */
        private String descTxt = null;
        /** The note txt. */
        private String[] noteTxt = new String[2];
        /** The btn cancel txt. */
        private String btnCancelTxt = null;

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("pop_sha.png");
            bgMImg = getImage("05_pop_glow_m.png");
            bgTImg = getImage("05_pop_glow_t.png");
            bgBImg = getImage("05_pop_glow_b.png");
            gapImg = getImage("pop_gap_379.png");
            highImg = getImage("pop_high_350.png");

            listBgImg = getImage("pop_list272x156.png");
            listFocusImg = getImage("pop_list274_f.png");

            iconQFocusImg = getImage("icon_q_foc.png");
            iconQImg = getImage("icon_q.png");
            iconCheckFocusImg = getImage("check_foc.png");
            iconCheckImg = getImage("check.png");
            iconfavorFocusImg = getImage("icon_fav_foc.png");
            iconfavorImg = getImage("icon_fav_g.png");

            listShadowTImg = getImage("09_270_t.png");
            listShadowBImg = getImage("09_270_b.png");
            arrowTImg = getImage("02_ars_t.png");
            arrowBImg = getImage("02_ars_b.png");

            btnFocusImg = getImage("05_focus.png");
            btnDimImg = getImage("05_focus_dim.png");
            super.prepare(c);

            titleTxt = getString(titleTextTag[mode]);
            descTxt = getString(descTextTag[mode]);
            btnCancelTxt = getString("btnCanecl");
            noteTxt = TextUtil.tokenize(getString("devSelectNote"), "^");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -57);
            //InitUI ui = (InitUI) c;
            //bg
            g.drawImage(shadowImg, 306, 452, 350, 79, c);
            g.drawImage(bgMImg, 276, 137, 410, 269, c);
            g.drawImage(bgTImg, 276, 57, c);
            g.drawImage(bgBImg, 276, 406, c);
            //
            g.setColor(Rs.C35);
            g.fillRect(306, 87, 350, 368);
            g.drawImage(gapImg, 285, 125, c);
            g.drawImage(highImg, 306, 87, c);
            //list
            g.drawImage(listBgImg, 343, 170, c);

            int totalCount = deviceInfos.size();
            if (totalCount > 0) {
                int lineGap = 31;
                g.setFont(Rs.F18);
                int initNum = listFocus - focusPosition;
                int lastNum = 0;
                if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = listFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                    DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(i);
                    int addPos = (j * lineGap);
                    if (i != listFocus || stateFocus != STATE_LIST) { paintItem(g, c, false, devInfo, addPos); }
                }
                if (initNum > 0) { g.drawImage(listShadowTImg, 344, 170, c); }
                if (lastNum < totalCount - 1) { g.drawImage(listShadowBImg, 344, 292, c); }
                if (stateFocus == STATE_LIST) {
                    DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(listFocus);
                    paintItem(g, c, true, devInfo, (focusPosition * lineGap));
                    if (!devInfo.isAppInstalled()) {
                        g.setFont(Rs.F18);
                        g.setColor(Rs.C182);
                        for (int i = 0; i < noteTxt.length; i++) {
                            int addPos = i * 19;
                            GraphicUtil.drawStringCenter(g, noteTxt[i], 484, 360 + addPos);
                        }
                    }
                }
                if (initNum > 0) { g.drawImage(arrowTImg, 471, 163, c); }
                if (lastNum < totalCount - 1) { g.drawImage(arrowBImg, 470, 316, c); }
            }
            //txt
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            GraphicUtil.drawStringCenter(g, titleTxt, 478, 113);
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, descTxt, 481, 154);
            //btn
            g.setColor(Rs.C3);
            Image btnImg = btnDimImg;
            if (stateFocus == STATE_BTN) { btnImg = btnFocusImg; }
            if (btnImg != null) { g.drawImage(btnImg, 406, 402, c); }
            GraphicUtil.drawStringCenter(g, btnCancelTxt, 485, 423);
            g.translate(276, 57);
        }

        /**
         * Paint item.
         *
         * @param g the Graphics
         * @param c the UIComponent
         * @param isFocus the is focus
         * @param item the DeviceInfo
         * @param addPos the add position
         */
        private void paintItem(Graphics g, UIComponent c, boolean isFocus, DeviceInfo item, int addPos) {
            g.setColor(Rs.C215214214);
            if (isFocus) {
                g.setColor(Rs.C1);
                g.drawImage(listFocusImg, 343, 170 + addPos, c);
            }
            if (!isFocus) {
                String name = item.getDeviceName();
                g.drawString(TextUtil.shorten(name, g.getFontMetrics(), 583 - 383), 383, 191 + addPos);
            }

            Image iconImg = null;
            if (item.isAppInstalled()) {
                iconImg = iconCheckImg;
                if (isFocus) { iconImg = iconCheckFocusImg; }
            } else {
                iconImg = iconQImg;
                if (isFocus) { iconImg = iconQFocusImg; }
            }
            if (iconImg != null) { g.drawImage(iconImg, 355, 175 + addPos, c); }

            //if (name != null && PreferenceProxy.favorDevs.containsKey(name)) {
            String udn = item.getUDN();
            if (udn != null && udn.equals(PreferenceProxy.getInstance().favorUDN)) {
                iconImg = iconfavorImg;
                if (isFocus) { iconImg = iconfavorFocusImg; }
                 g.drawImage(iconImg, 585, 176 + addPos, c);
            }
        }
    }
}

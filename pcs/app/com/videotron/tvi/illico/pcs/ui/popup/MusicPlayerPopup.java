/*
 *  @(#)MusicPlayerPopup.java 1.0 2011.06.29
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import javax.media.ControllerErrorEvent;
import javax.media.PrefetchCompleteEvent;
import javax.media.RateChangeEvent;
import javax.media.StartEvent;
import javax.tv.service.selection.NormalContentEvent;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.shared.media.BeginningOfContentEvent;
import org.ocap.shared.media.EndOfContentEvent;

import com.videotron.tvi.illico.flipbar.FlipBar;
import com.videotron.tvi.illico.flipbar.FlipBarAction;
import com.videotron.tvi.illico.flipbar.FlipBarFactory;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelDataUpdateListener;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayListener;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.controller.ui.ScreensaverController;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.data.GalaxieContent;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.PhotoPlayUI;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>MusicPlayerPopup</code> the popup for music player.
 *
 * @since   2011.06.29
 * @version $Revision: 1.35 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class MusicPlayerPopup extends Popup implements MediaPlayListener,
        TVTimerWentOffListener, GalaxieChannelDataUpdateListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ROWS_PER_PAGE. */
    public static final int ROWS_PER_PAGE = 5;
    /** The Constant MIDDLE_POS. */
    public static final int MIDDLE_POS = 2;

    /** The Constant PLAY_NO. */
    public static final int PLAY_NO = -1;
    /** The Constant PLAY_MY_MUSIC. */
    public static final int PLAY_MY_MUSIC = 0;
    /** The Constant PLAY_GALAXIE. */
    public static final int PLAY_GALAXIE = 1;
    /** The Constant PAUSE_MY_MUSIC. */
    public static final int PAUSE_MY_MUSIC = 2;
    /** The Constant PAUSE_GALAXIE. */
    public static final int PAUSE_GALAXIE = 3;

    /** The Constant UI_NO_MUSIC. */
    public static final int UI_NO_MUSIC = 0;
    /** The Constant UI_SELECT_MUSIC. */
    public static final int UI_SELECT_MUSIC = 1;
    /** The Constant UI_PLAY_MUSIC. */
    public static final int UI_PLAY_MUSIC = 2;
    /** The Constant UI_SELECT_GALAXIE. */
    public static final int UI_SELECT_GALAXIE = 3;
    /** The Constant UI_PLAY_GALAXIE. */
    public static final int UI_PLAY_GALAXIE = 4;

    /** The Constant MENU_MY_MUSIC. */
    private static final int MENU_MY_MUSIC = 0;
    /** The Constant MENU_GALAXIE_CH. */
    private static final int MENU_GALAXIE_CH = 1;

    /** The Constant BTN_STOP. */
    private static final int BTN_STOP = 0;
    /** The Constant BTN_SKIP_BACK. */
    private static final int BTN_SKIP_BACK = 1;
    /** The Constant BTN_PLAY. */
    private static final int BTN_PLAY = 2;
    /** The Constant BTN_SKIP_FORWARD. */
    private static final int BTN_SKIP_FORWARD = 3;
    /** The Constant BTN_RANDOM. */
    private static final int BTN_RANDOM = 4;
    /** The Constant BTN_REPEAT. */
    private static final int BTN_REPEAT = 5;
    /** The Constant BTN_GALAXIE_PLAY. */
    private static final int BTN_GALAXIE_PLAY = 1;
    /** The Constant BTN_GALAXIE_CH. */
    private static final int BTN_GALAXIE_CH = 2;

    /** The Constant KEY_ALBUM_IMG. */
    private static final String KEY_ALBUM_IMG = "musicPlayerAlbumImage";

    /** The Constant SLEEP_TIME. */
    //private static final long SLEEP_TIME = 500L;
    //test
    private static final long SLEEP_TIME = 200L;

    /** The Constant PLAY_RATE. */
    private static final float[] PLAY_RATE = {-16, -8, -4, -2, 0, 1, 2, 4, 8, 16};
    /** The Constant RATE_RW16. */
    private static final int RATE_RW16 = 0;
    /** The Constant RATE_RW8. */
    //private static final int RATE_RW8 = 1;
    /** The Constant RATE_RW4. */
    //private static final int RATE_RW4 = 2;
    /** The Constant RATE_RW2. */
    private static final int RATE_RW2 = 3;
    /** The Constant RATE_PAUSE. */
    private static final int RATE_PAUSE = 4;
    /** The Constant RATE_PLAY. */
    private static final int RATE_PLAY = 5;
    /** The Constant RATE_FF2. */
    private static final int RATE_FF2 = 6;
    /** The Constant RATE_FF4. */
    //private static final int RATE_FF4 = 7;
    /** The Constant RATE_FF8. */
    //private static final int RATE_FF8 = 8;
    /** The Constant RATE_FF16. */
    private static final int RATE_FF16 = 9;

    /** The play music list. */
    public Vector playMusicList = null;
    /** The play music focus. */
    public int playMusicFocus = 0;
    /** The galaxie list. */
    private Vector galaxieList = null;
    /** The galaxie ch data. */
    public GalaxieChannelData galaxieChData = null;
    /** The play galaxie focus. */
    public int playGalaxieFocus = 0;
    //public static GalaxieContent playGalaxieContent = null;
    /** The play music type. */
    public int playMusicType = MusicPlayerPopup.PLAY_NO;
    /** The music bar. */
    public final FlipBar musicBar = FlipBarFactory.getInstance().createPcsMusicFlipBar();
    /** The galaxie bar. */
    public final FlipBar galaxieBar = FlipBarFactory.getInstance().createPcsGalaxieFlipBar();
    /** The music select list. */
    private Vector musicSelectList = null;
    /** The renderer. */
    private PopupRenderer renderer = null;
    /** The Footer instance. */
    private Footer footer = null;
    /** The FlipBar instance. */
    public FlipBar flipBar = null;
    /** The timer spec. */
    private TVTimerSpec timerSpec = null;
    /** The random buffer. */
    private Vector randomBuffer = null;
    /** The showing effect. */
    private Effect showingEffect = null;
    /** The hiding effect. */
    private Effect hidingEffect = null;

    /** The is timer. */
    private boolean isTimer = false;
    /** The ui type. */
    private int uiType = 0;
    /** The content focus. */
    private int contentFocus = 0;
    /** The list focus. */
    private int listFocus = 0;
    /** The focus position. */
    private int focusPosition = 0;
    /** The album img. */
    public Image albumImg = null;
    /** The album url. */
    //private String albumUrl = null;
    /** The play time. */
    public long playTime = 0;
    /** The is repeat. */
    private boolean isRepeat = false;
    /** The is random. */
    private boolean isRandom = false;
    /** The music seq. */
    private int musicSeq = 0;
    /** The third title. */
    private String thirdTitle = "";
    /** The random focus. */
    private int randomFocus = 0;
    /** The exit sleep count. */
    private int showCount = 0;
    /** The rate focus. */
    private int rateFocus = RATE_PLAY;
    /** The show hiding effect. */
    private boolean showHidingEffect = true;
    /** The music content. */
    private Content musicContent = null;
    /** The paging data thread. */
    private PagingThread pagingDataThread = null;
    /** The thread id. */
    private int requestId = -1;

    /**
     * Initialize the Component.
     */
    public MusicPlayerPopup() {
        setBounds(0, 220, 960, 320);
        renderer = new PopupRenderer();
        setRenderer(renderer);
        musicBar.setBounds(432, 410 - 220, 261, 68);
        galaxieBar.setBounds(431, 410 - 220, 254, 68);
        footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
        footer.setBounds(0, 488 - 220, 900, 24);
        add(footer);
        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME);
        timerSpec.setRepeat(true);
        timerSpec.setRegular(false);
        clickEffect = new ClickingEffect(this, 5);
        showingEffect = new MovingEffect(this, 8,
                new Point(0, 40), new Point(0, 0),
                MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        //showingEffect.updateBackgroundBeforeStart(false);
        hidingEffect = new MovingEffect(this, 8,
                new Point(0, 0), new Point(0, 40),
                MovingEffect.FADE_OUT, MovingEffect.GRAVITY);
        hidingEffect.updateBackgroundBeforeStart(false);
        pagingDataThread = new PagingThread();
    }

    /**
     * when popup is started, this is called.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#start()
     */
    public void start() {
        prepare();
        if (playMusicType == PLAY_NO) {
            uiType = UI_NO_MUSIC;
            playTime = 0;
            isRepeat = false;
            isRandom = false;
        } else {
            uiType = UI_PLAY_MUSIC;
            if (playMusicType == PLAY_GALAXIE || playMusicType == PAUSE_GALAXIE) {
                uiType = UI_PLAY_GALAXIE;
                //setGalaxieChData();
                setFlipBar(galaxieBar);
                if (albumImg == null) { setAlbumImage(null); }
            } else if (playMusicType == PLAY_MY_MUSIC || playMusicType == PAUSE_MY_MUSIC) {
                setFlipBar(musicBar);
                if (albumImg == null) { setAlbumImage(null); }
            }
        }
        showHidingEffect = true;
        resetFocus();
        addFooterBtn();
        showCount = 0;
    }

    /**
     * Start effect.
     */
    public void startEffect() {
        setVisible(false);
        showingEffect.start();
        startTimer();
        setVisible(true);
    }

    /**
     * Adds the footer button.
     */
    private void addFooterBtn() {
        footer.reset();
        if (uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE) {
            footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        }
        footer.addButtonWithLabel(BasicRenderer.btnBImg, renderer.hideMPTxt);
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() {
        if (playMusicType != PLAY_GALAXIE && playMusicType != PAUSE_GALAXIE) {
            CommunicationManager.getInstance().removeGalaxieChannelDataUpdatedListener();
        }
        if (playMusicType != PLAY_MY_MUSIC) {
            stopTimer();
            stopPagingThread();
        }
        scene = null;
        musicSelectList = null;
        //albumImg = null;
        stopFlipBar();
        //flushAlbumImage();
        CommunicationManager.getInstance().hideLoadingAnimation();
        if (uiType != UI_PLAY_MUSIC) { randomBuffer = null; }
        showCount = 0;
        if (!ScreensaverController.getInstance().isActive() && showHidingEffect) { hidingEffect.start(); }
    }

    /**
     * when App is finished, this is called.
     */
    public void dispose() {
        MediaPlayHandler.getInstance().stop(this);
        CommunicationManager.getInstance().removeGalaxieChannelDataUpdatedListener();
        stopTimer();
        stopPagingThread();
        galaxieList = null;
        galaxieChData = null;
        playMusicList = null;
        playMusicFocus = 0;
        playMusicType = PLAY_NO;
        playTime = 0;
        flipBar = null;
        isRepeat = false;
        isRandom = false;
        flushAlbumImage();
        albumImg = null;
        randomBuffer = null;
        musicContent = null;
        requestId = -1;
        //playGalaxieContent = null;
    }

    /**
     * reset focus index.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        contentFocus = 0;
        listFocus = 0;
        focusPosition = 0;
    }

    /**
     * Sets the flip bar.
     *
     * @param bar the new flip bar
     */
    private void setFlipBar(FlipBar bar) {
        stopFlipBar();
        flipBar = bar;
        if (flipBar == musicBar) {
            flipBar.setFocus(2);
            FlipBarAction action = FlipBarAction.RANDOM_OFF;
            if (isRandom) { action = FlipBarAction.RANDOM_ON; }
            flipBar.setAction(action, BTN_RANDOM);
            action = FlipBarAction.REPEAT_OFF;
            if (isRepeat) { action = FlipBarAction.REPEAT_ON; }
            flipBar.setAction(action, BTN_REPEAT);
            action = FlipBarAction.PAUSE;
            if (playMusicType == PAUSE_MY_MUSIC) { action = FlipBarAction.PLAY; }
            flipBar.setAction(action, BTN_PLAY);
            boolean enableBtn = true;
            if (playMusicList != null && playMusicList.size() < 2) {
                enableBtn = false;
            }
            for (int i = 0; i <= 5; i++) {
                if (i == 0 || i == 2) { continue; }
                flipBar.setEnabled(i, enableBtn);
            }
        } else {
            flipBar.setFocus(1);
            FlipBarAction action = FlipBarAction.PAUSE;
            if (playMusicType == PAUSE_GALAXIE) { action = FlipBarAction.PLAY; }
            flipBar.setAction(action, BTN_GALAXIE_PLAY);
        }
        //flipBar.addListener(this);
        flipBar.start();
        add(flipBar);
    }

    /**
     * Stop flip bar.
     */
    private void stopFlipBar() {
        if (flipBar != null) {
            flipBar.stop();
            //flipBar.removeListener(this);
            remove(flipBar);
        }
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        if (isTimer) { return; }
        try {
            timerSpec.addTVTimerWentOffListener(this);
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            isTimer = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        if (!isTimer) { return; }
        if (timerSpec != null) {
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
            isTimer = false;
        }
    }

    /**
     * implements the method of TVTimerWentOffListener.
     *
     * @param e the e
     */
    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (!isTimer) { return; }
        if (playMusicType == PLAY_MY_MUSIC || playMusicType == PAUSE_MY_MUSIC) {
            playTime = MediaPlayHandler.getInstance().getPlayTimeLong();
            Logger.info(this, "==========timerWentOff : " + playTime);
            if (scene != null) { repaint(); }
        } else if (uiType == UI_PLAY_GALAXIE) {
            //repaint();
        }
        if (scene != null) {
            if (++showCount * SLEEP_TIME >= Config.musicplayerShowSec * 1000L) {
                showCount = 0;
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        if (checkMusicSearch()) {
                            BasicUI.searchContent = null;
                        }
                        scene.requestPopupClose();
                        showCount = 0;
                    }
                });
            }
        } else { showCount = 0; }
    }

    /**
     * Check music search.
     *
     * @return true, if this playing is for music search.
     */
    private boolean checkMusicSearch() {
        if (SceneManager.getInstance().curSceneId != SceneManager.SC_MUSIC_SEARCH_LIST
                && BasicUI.searchContent != null
                && BasicUI.searchContent.getMenuType() == BasicUI.MENU_MUSIC) {
            return true;
        }
        return false;
    }

    /**
     * Sets the galaxie ch data by Thread.
     */
    private void setGalaxieChData() {
        if (galaxieList == null || galaxieList.size() == 0) { return; }
        galaxieChData = null;
        CommunicationManager.getInstance().showLoadingAnimation();
        new Thread("MusicPlayerPopup|getGalaxieInfo") {
            public void run() {
                GalaxieContent content = (GalaxieContent) galaxieList.elementAt(playGalaxieFocus);
                Object[] sharedData = content.getChInfos();
                Integer sourceId = null;
                if (!Environment.EMULATOR) {
                    Integer chNum = (Integer) sharedData[GalaxieContent.INDEX_CHANNEL_NUMBER];
                    galaxieChData = CommunicationManager.getInstance().getGalaxieData(chNum.intValue());
                    sourceId = (Integer) sharedData[GalaxieContent.INDEX_CHANNEL_SOURCE_ID];
                    CommunicationManager.getInstance().addGalaxieChannelDataUpdatedListener(MusicPlayerPopup.this);
                }
                CommunicationManager.getInstance().hideLoadingAnimation();
                if (scene == null) {
                    galaxieChData = null;
                    return;
                }
                if (uiType != UI_PLAY_GALAXIE) {
                    MediaPlayHandler.getInstance().start(sourceId, MusicPlayerPopup.this);
                    uiType = UI_PLAY_GALAXIE;
                    playMusicType = PLAY_GALAXIE;
                }
                processMusicFooter();
                addFooterBtn();
                setFlipBar(galaxieBar);
                repaint();
                setAlbumImage(null);
            }
        } .start();
    }

    /**
     * Flush album image.
     */
    private void flushAlbumImage() {
        //if (albumUrl == null) { return; }
        BasicUI.conImgPool.flushImage(albumImg);
        //albumUrl = null;
        albumImg = null;
        BasicUI.conImgPool.remove(KEY_ALBUM_IMG);
    }

    /**
     * Sets the album image.
     *
     * @param galaxieImgData the new album image
     */
    private void setAlbumImage(final byte[] galaxieImgData) {
        int focus = playMusicFocus;
        final int reqUiType = uiType;
        final int[] size = BasicUI.SIZE_WALL_FILE;
        Vector list = playMusicList;
        if (reqUiType == UI_PLAY_GALAXIE) {
            list = galaxieList;
            if (galaxieChData == null) { return; }
            focus = playGalaxieFocus;
        }
        if (list == null) { return; }
        Object node = list.elementAt(focus);
        String tempUrl = null;
        if (reqUiType == UI_PLAY_MUSIC) {
            if (node == null) { return; }
            tempUrl = ((Content) node).getIconUrl();
        } else if (galaxieImgData == null) {
            try {
                tempUrl = galaxieChData.getCoverUrl();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        flushAlbumImage();
        albumImg = null;
        final int reqFocus = focus;
        if (reqUiType == UI_PLAY_GALAXIE && galaxieImgData != null) { tempUrl = "galaxieImage"; }
        final String reqAlbumUrl = tempUrl;
        Logger.debug(this, "reqAlbumUrl : " + reqAlbumUrl);
        final GalaxieChannelData galaxieData = galaxieChData;
        new Thread("AllMovie|setAlbumImage()") {
            public void run() {
                if (!checkFocus(reqFocus, reqUiType)) { return; }
                if (reqAlbumUrl != null) {
                    try {
                        Thread.sleep(300L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!checkFocus(reqFocus, reqUiType)) { return; }
                    Image img = null;
                    if (reqUiType == UI_PLAY_MUSIC) {
                        //img = scene.getContentImage(reqAlbumUrl, null, size);
                        img = BasicUI.conImgPool.getImage(KEY_ALBUM_IMG, null, size, reqAlbumUrl);
                    } else if (reqUiType == UI_PLAY_GALAXIE) {
                        try {
                            byte[] imgByte = galaxieImgData;
                            if (imgByte == null) {
                                Logger.debug(this, "reqAlbumUrl : " + reqAlbumUrl);
                                Logger.debug(this, "galaxieData.getProxyIPs() : " + galaxieData.getProxyIPs());
                                Logger.debug(this, "galaxieData.getProxyPorts() : " + galaxieData.getProxyPorts());
                                imgByte = URLRequestor.getBytes(galaxieData.getProxyIPs(),
                                        galaxieData.getProxyPorts(), reqAlbumUrl, null);
                            }
                            if (imgByte != null) {
                                img = BasicUI.conImgPool.getImage(KEY_ALBUM_IMG, imgByte, size, null);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (img != null) {
                        Logger.debug(this, "img : " + img);
                        BasicUI.conImgPool.waitForAll();
                        //albumUrl = reqAlbumUrl;
                        if (!scene.checkVisible()) { return; }
                        if (checkFocus(reqFocus, reqUiType)) {
                            albumImg = img;
                            repaint();
                        } else { flushAlbumImage(); }
                    }
                }
                Logger.debug(this, "albumImg : " + albumImg);
                if (SceneManager.getInstance().curSceneId == SceneManager.SC_INVALID) {
                    flushAlbumImage();
                    BasicUI.conImgPool.clear();
                    return;
                }
            }
        } .start();
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @param reqUiType the ui type
     * @return true, if successful
     */
    private boolean checkFocus(int reqIndex, int reqUiType) {
        Logger.debug(this, "checkFocus()-reqIndex : " + reqIndex);
        if (scene == null) {
            return false;
        } else if (reqUiType == UI_PLAY_MUSIC) {
            return  reqIndex == playMusicFocus;
        } else if (reqUiType == UI_PLAY_GALAXIE) {
            return  reqIndex == playGalaxieFocus;
        }
        return false;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        showCount = 0;
        switch (keyCode) {
        case Rs.KEY_UP:
            if ((uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE) && listFocus > 0) {
                if (focusPosition != MIDDLE_POS || listFocus == focusPosition) { focusPosition--; }
                listFocus--;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if ((uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE)) {
                Vector list = musicSelectList;
                if (uiType == UI_SELECT_GALAXIE) { list = galaxieList; }
                int totalCount = list.size();
                if (listFocus < totalCount - 1) {
                    if (focusPosition != MIDDLE_POS || focusPosition + listFocus >= totalCount - 1) {
                        focusPosition++;
                    }
                    listFocus++;
                    repaint();
                }
            }
            break;
        case Rs.KEY_LEFT:
            if (uiType == UI_NO_MUSIC && contentFocus > 0) {
                contentFocus--;
                repaint();
            } else if (uiType == UI_PLAY_MUSIC || uiType == UI_PLAY_GALAXIE) {
                if (flipBar.isVisible()) { flipBar.handleKey(keyCode); }
                //btnFocus = flipBar.getFocus();
                //repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (uiType == UI_NO_MUSIC && contentFocus == 0) {
                contentFocus++;
                repaint();
            } else if (uiType == UI_PLAY_MUSIC || uiType == UI_PLAY_GALAXIE) {
                if (flipBar.isVisible()) { flipBar.handleKey(keyCode); }
                //btnFocus = flipBar.getFocus();
                //repaint();
            }
            break;
        case Rs.KEY_ENTER:
            keyEnter();
            break;
        case KeyCodes.BACK:
        case KeyCodes.FORWARD:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
            if (uiType == UI_PLAY_MUSIC) { processPlayKey(keyCode); }
            break;
        case KeyCodes.PLAY:
        case KeyCodes.STOP:
        case KeyCodes.PAUSE:
            if (uiType == UI_PLAY_MUSIC) {
                processPlayKey(keyCode);
            } else if (uiType == UI_PLAY_GALAXIE) { processGalaxieKey(keyCode); }
            break;
        case KeyCodes.COLOR_B:
            footer.clickAnimation(renderer.hideMPTxt);
            if (checkMusicSearch()) {
                BasicUI.searchContent = null;
            }
            if (scene instanceof PhotoPlayUI) {
                scene.requestPopupOk(null);
            } else { scene.requestPopupClose(); }
            break;
        case KeyCodes.LIVE:
        case Rs.KEY_EXIT:
            if (checkMusicSearch()) {
                BasicUI.searchContent = null;
            }
            scene.requestPopupClose();
            break;
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            if (uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE) {
                uiType = UI_NO_MUSIC;
                addFooterBtn();
                repaint();
                stopPagingThread();
            }
            break;
        case Rs.KEY_PAGE_UP:
        case Rs.KEY_PAGE_DOWN:
            changePage(keyCode);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Key enter.
     */
    private void keyEnter() {
        if (uiType == UI_NO_MUSIC) {
            clickEffect.start(285 + (contentFocus * 248) - 0, 312 - 220, 425 - 285, 451 - 312);
            keyEnterOfNoMusic();
        } else if (uiType == UI_SELECT_MUSIC) {
            clickEffect.start(276 - 0, 313 + (focusPosition * 31) - 220, 682 - 276, 32);
            if (listFocus == 0) {
                //if (scene instanceof MusicListUI) {
                if (SceneManager.getInstance().curSceneId == SceneManager.SC_MUSIC_LIST) {
                    scene.requestPopupOk(new int[]{uiType, listFocus});
                } else { scene.goToContentUI(BasicUI.MENU_MUSIC); }
            } else if (playMusicList != null && listFocus == 1) {
                if (!playMusic(musicContent, playMusicFocus, false, musicSeq, thirdTitle, false)) {
                    BasicUI.showConnectErrorPopup();
                    return;
                }
                uiType = UI_PLAY_MUSIC;
                addFooterBtn();
                setFlipBar(musicBar);
                setAlbumImage(null);
                repaint();
            } else {
                CommunicationManager.getInstance().showLoadingAnimation();
                new Thread("MusicPlayerPopup|getPlayListItem") {
                    public void run() {
                        Content reqContent = (Content) musicSelectList.elementAt(listFocus);
                        if (!playMusic(reqContent, 0, false, BasicUI.SEQ_PLAYLISTS, reqContent.getName(), false)) {
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            BasicUI.showConnectErrorPopup();
                            return;
                        }
                        if (musicSelectList == null || scene == null) {
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            return;
                        }
                        CommunicationManager.getInstance().hideLoadingAnimation();
                        uiType = UI_PLAY_MUSIC;
                        addFooterBtn();
                        setFlipBar(musicBar);
                        setAlbumImage(null);
                        repaint();
                    }
                } .start();
            }
        } else if (uiType == UI_SELECT_GALAXIE) {
            clickEffect.start(276 - 0, 313 + (focusPosition * 31) - 220, 682 - 276, 32);
            playGalaxieFocus = listFocus;
            setGalaxieChData();
        } else if (uiType == UI_PLAY_MUSIC) {
            flipBar.handleKey(Rs.KEY_ENTER);
            processPlayKey(Rs.KEY_ENTER);
        } else if (uiType == UI_PLAY_GALAXIE) {
            flipBar.handleKey(Rs.KEY_ENTER);
            processGalaxieKey(Rs.KEY_ENTER);
        }
    }

    /**
     * Key enter of no music.
     */
    private void keyEnterOfNoMusic() {
        listFocus = 0;
        focusPosition = 0;
        if (contentFocus == MENU_MY_MUSIC) {
            musicSelectList = new Vector();
            musicSelectList.addElement(renderer.goMyMusicTxt);
            if (playMusicList != null) {
                Content content = (Content) playMusicList.elementAt(playMusicFocus);
                String str = renderer.playLastTxt + " : " + content.getName();
                musicSelectList.addElement(str);
                listFocus = 1;
                focusPosition = 1;
            }
            if (BasicUI.musicRoot != null) {
                final Content playlistRoot = BasicUI.musicRoot[BasicUI.SEQ_PLAYLISTS];
                if (playlistRoot != null) {
                    CommunicationManager.getInstance().showLoadingAnimation();
                    new Thread("MusicPlayerPopup|getPlayList") {
                        public void run() {
                            Vector list = null;
                            Content tempRoot = new Content();
                            tempRoot.setId(playlistRoot.getId());
                            Vector result = null;
                            boolean prepare = HNHandler.getInstance().prepareChildNode(tempRoot.getId(),
                                    tempRoot, BasicUI.MENU_MUSIC, Content.SORT_ALPHA_A);
                            if (prepare) {
                                result = HNHandler.getInstance().setChildNode(tempRoot, BasicUI.MENU_MUSIC,
                                        Content.SORT_ALPHA_A, 0, Config.loadDataNumber);
                            }
                            if (result != null && result.size() > 0) {
                                int totalCount = tempRoot.getChildCount();
                                int currentCount = 0;
                                list = new Vector();
                                //list.setSize(totalCount);
                                tempRoot.setChildren(list);
                                while (true) {
                                    for (int i = 0; i < result.size() && currentCount + i < totalCount; i++) {
                                        list.addElement(result.elementAt(i));
                                        //list.setElementAt(result.elementAt(i), currentCount + i);
                                    }
                                    currentCount += result.size();
                                    if (currentCount >= totalCount || result.size() < Config.loadDataNumber) { break; }
                                    result = HNHandler.getInstance().setChildNodeForPaging(tempRoot, BasicUI.MENU_MUSIC,
                                            Content.SORT_ALPHA_A, currentCount, Config.loadDataNumber);
                                }
                                if (musicSelectList == null || scene == null) {
                                    CommunicationManager.getInstance().hideLoadingAnimation();
                                    return;
                                }
                            }
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            if (list != null && !list.isEmpty()) {
                                musicSelectList.addAll(list);
                            }
                            uiType = UI_SELECT_MUSIC;
                            addFooterBtn();
                            repaint();
                        }
                    } .start();
                    return;
                }
            }
            uiType = UI_SELECT_MUSIC;
            addFooterBtn();
            repaint();
        } else {
            if (galaxieList == null || galaxieList.size() == 0) {
                CommunicationManager.getInstance().showLoadingAnimation();
                new Thread("MusicPlayerPopup|getGalaxieList") {
                    public void run() {
                        Object[][] channelData =
                            (Object[][]) SharedMemory.getInstance().get(EpgService.DATA_KEY_CHANNELS);
                        Hashtable caTable =
                            (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);
                        galaxieList = new Vector();
                       // Logger.debug(this, "channelData.length : " + channelData.length);
                        for (int i = 0; i < channelData.length; i++) {
                            //Logger.debug(this, "channelData.length[" + i + "] : " + channelData[i].length);
                            boolean isSubscribed = true;
                            int chType = TvChannel.TYPE_GALAXIE;
                            if (!Environment.EMULATOR) {
                                Integer sourceId =
                                    (Integer) channelData[i][GalaxieContent.INDEX_CHANNEL_SOURCE_ID];
                                if (caTable != null) {
                                    Boolean subscribed = (Boolean) caTable.get(sourceId);
                                    if (subscribed != null) { isSubscribed = subscribed.booleanValue(); }
                                }
                                chType =
                                    ((Integer) channelData[i][GalaxieContent.INDEX_CHANNEL_TYPE]).intValue();
                            }
                            if (chType == TvChannel.TYPE_GALAXIE && isSubscribed) {
                                GalaxieContent content = new GalaxieContent();
                                content.setChInfos(channelData[i]);
                                String chName = (String) channelData[i][GalaxieContent.INDEX_CHANNEL_NAME];
                                content.setTvChannel(CommunicationManager.getInstance().getChannel(chName));
                                galaxieList.add(content);
                            }
                        }
                        CommunicationManager.getInstance().hideLoadingAnimation();
                        playGalaxieFocus = 0;
                        uiType = UI_SELECT_GALAXIE;
                        addFooterBtn();
                        repaint();
                    }
                } .start();
            } else {
                playGalaxieFocus = 0;
                uiType = UI_SELECT_GALAXIE;
                addFooterBtn();
                repaint();
            }
        }
    }

    /**
     * Process play key.
     *
     * @param keyCode the key code
     */
    public void processPlayKey(int keyCode) {
        Logger.debug(this, "called processPlayKey()-keyCode : " + keyCode);
        int btnFocus = 0;
        switch (keyCode) {
        case KeyCodes.BACK:
            btnFocus = BTN_SKIP_BACK;
            break;
        case KeyCodes.STOP:
            btnFocus = BTN_STOP;
            break;
        case KeyCodes.PLAY:
            btnFocus = BTN_PLAY;
            break;
        case KeyCodes.FORWARD:
            btnFocus = BTN_SKIP_FORWARD;
            break;
        case KeyCodes.PAUSE:
            btnFocus = BTN_PLAY;
            break;
        case Rs.KEY_ENTER:
            btnFocus = flipBar.getFocus();
            break;
        case KeyCodes.REWIND:
            if (rateFocus == RATE_RW16 || rateFocus >= RATE_PAUSE) {
                rateFocus = RATE_RW2;
            } else { rateFocus--; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
            return;
        case KeyCodes.FAST_FWD:
            if (rateFocus == RATE_FF16 || rateFocus <= RATE_PLAY) {
                rateFocus = RATE_FF2;
            } else { rateFocus++; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
            return;
        default:
            break;
        }
        if (playMusicList != null && playMusicList.size() < 2) {
            if (btnFocus == BTN_SKIP_BACK || btnFocus == BTN_SKIP_FORWARD
                    || btnFocus == BTN_REPEAT || btnFocus == BTN_RANDOM) {
                return;
            }
        }
        if (keyCode != Rs.KEY_ENTER) { flipBar.setFocus(btnFocus); }

        int totalCount = playMusicList.size();
        if (btnFocus == BTN_SKIP_BACK) {
            if (totalCount > 1) {
                if (isRandom && totalCount > 2) {
                    int prevFocus = randomFocus - 1;
                    if (prevFocus < 0 && isRepeat) { prevFocus = totalCount - 1; }
                    if (prevFocus >= 0) {
                        checkFocusData(prevFocus, true, false);
                    }
                } else {
                    int prevFocus = playMusicFocus - 1;
                    if (prevFocus < 0 && isRepeat) { prevFocus = totalCount - 1; }
                    if (prevFocus >= 0) {
                        checkFocusData(prevFocus, false, false);
                    }
                }
            }
        } else if (btnFocus == BTN_STOP) {
            requestId = -1;
            playMusicType = PLAY_NO;
            MediaPlayHandler.getInstance().stop(this);
            stopPagingThread();
            processMusicFooter();
            uiType = UI_NO_MUSIC;
            playTime = 0;
            isRepeat = false;
            isRandom = false;
            stopFlipBar();
            flushAlbumImage();
            //stopTimer();
            addFooterBtn();
            if (checkMusicSearch()) {
                BasicUI.searchContent = null;
                scene.addPopup(BasicUI.searchPopup);
            }
        } else if (btnFocus == BTN_PLAY) {
            boolean exePlayer = true;
            if (keyCode == Rs.KEY_ENTER) {
                if (playMusicType == PLAY_MY_MUSIC) {
                    playMusicType = PAUSE_MY_MUSIC;
                } else { playMusicType = PLAY_MY_MUSIC; }
            } else if (keyCode == KeyCodes.PLAY) {
                if (playMusicType == PAUSE_MY_MUSIC) {
                    playMusicType = PLAY_MY_MUSIC;
                } else {
                    if (rateFocus == RATE_PLAY) { exePlayer = false; }
                }
            } else if (keyCode == KeyCodes.PAUSE) {
                if (playMusicType == PLAY_MY_MUSIC) {
                    playMusicType = PAUSE_MY_MUSIC;
                } else { playMusicType = PLAY_MY_MUSIC; }
            }
            if (exePlayer) {
                FlipBarAction action = FlipBarAction.PAUSE;
                if (playMusicType == PAUSE_MY_MUSIC) { action = FlipBarAction.PLAY; }
                flipBar.setAction(action, BTN_PLAY);
                repaint();
                if (MediaPlayHandler.getInstance().equalPlayer(this)
                        && MediaPlayHandler.getInstance().isInitPlayer()) {
                    rateFocus = RATE_PAUSE;
                    if (playMusicType == PLAY_MY_MUSIC) { rateFocus = RATE_PLAY; }
                    MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
                } else if (playMusicType == PLAY_MY_MUSIC) {
                    final int reqId = ++requestId;
                    new Thread("MusicPlayerPopup|processPlayKey()") {
                        public void run() {
                            startMusic(reqId, playMusicFocus);
                        }
                    } .start();
                }
                processMusicFooter();
            }
        } else if (btnFocus == BTN_SKIP_FORWARD) {
            if (totalCount > 1) {
                if (isRandom && totalCount > 2) {
                    int nextFocus = randomFocus + 1;
                    if (nextFocus == totalCount && isRepeat) { nextFocus = 0; }
                    if (nextFocus < totalCount) {
                        checkFocusData(nextFocus, true, true);
                    }
                } else {
                    int nextFocus = playMusicFocus + 1;
                    if (nextFocus == totalCount && isRepeat) { nextFocus = 0; }
                    if (nextFocus < totalCount) {
                        checkFocusData(nextFocus, false, true);
                    }
                }
            }
        } else if (btnFocus == BTN_RANDOM) {
            stopPagingThread();
            isRandom = !isRandom;
            if (isRandom && playMusicList.size() > 2) {
                Vector temp = new Vector();
                for (int i = 0; i < playMusicList.size(); i++) {
                    temp.addElement(i + "");
                }
                randomBuffer = new Vector();
                randomFocus = 0;
                randomBuffer.addElement(temp.remove(playMusicFocus));
                while (!temp.isEmpty()) {
                    int random = (int) (Math.random() * temp.size());
                    randomBuffer.addElement(temp.remove(random));
                }
                loadInitPagingData(musicContent, randomFocus, true);
            } else {
                randomBuffer = null;
                loadInitPagingData(musicContent, playMusicFocus, false);
            }
        } else if (btnFocus == BTN_REPEAT) {
            isRepeat = !isRepeat;
        }
        repaint();
    }

    /**
     * Move focus.
     *
     * @param reqNode the req node
     * @param focus the focus
     * @param isRandomPlay the is random play
     * @param isNextFocus the is next focus
     */
    private void moveFocus(Content reqNode, int focus, boolean isRandomPlay, boolean isNextFocus) {
        //boolean isNext = false;
        if (isRandomPlay) {
            //if (focus > randomFocus) { isNext = true; }
            randomFocus = focus;
            playMusicFocus = Integer.parseInt((String) randomBuffer.elementAt(randomFocus));
        } else {
            //if (focus > playMusicFocus) { isNext = true; }
            playMusicFocus = focus;
        }
        setAlbumImage(null);
        repaint();
        Vector list = reqNode.getChildren();
        int totalCount = list.size();
        int lastContentNum = totalCount - 1;
        int cacheContentNum = Config.cachePageNumber * Config.loadDataNumber;
        if (cacheContentNum % 2 == 0) { cacheContentNum++; }
        if (list == playMusicList && lastContentNum + 1 > cacheContentNum) {
            String actionType = PagingThread.TYPE_LOAD;
            int actionPage = playMusicFocus + Config.cachePageNumber * Config.loadDataNumber / 2;
            if (!isNextFocus) {
                actionPage++;
                actionType = PagingThread.TYPE_REMOVE;
            }
            if (actionPage > lastContentNum) { actionPage = actionPage - lastContentNum - 1; }
            loadPagingData(reqNode, actionPage, actionType);

            actionType = PagingThread.TYPE_REMOVE;
            actionPage = playMusicFocus - Config.cachePageNumber * Config.loadDataNumber / 2 - 1;
            if (!isNextFocus) {
                actionPage++;
                actionType = PagingThread.TYPE_LOAD;
            }
            if (actionPage < 0) { actionPage = lastContentNum + actionPage + 1; }
            loadPagingData(reqNode, actionPage, actionType);
        }

        final int reqId = ++requestId;
        new Thread("MusicPlayerPopup|moveFocus()") {
            public void run() {
                startMusic(reqId, playMusicFocus);
            }
        } .start();
    }

    /**
     * Check focus data.
     *
     * @param focus the focus(playMusic focus or random focus)
     * @param isRandomPlay the is random play
     * @param isNextFocus the is next focus
     */
    private void checkFocusData(final int focus, final boolean isRandomPlay, final boolean isNextFocus) {
        MediaPlayHandler.getInstance().stop(this);
        final Content reqNode = musicContent;
        final Vector reqList = playMusicList;
        if (reqList == null || reqList == null) { return; }

        int musicFocus = focus;
        if (isRandomPlay) { musicFocus = Integer.parseInt((String) randomBuffer.elementAt(focus)); }
        final int reqMusicFocus = musicFocus;
        Hashtable errorPages = reqNode.getErrorPageNums();
        playMusicType = PLAY_MY_MUSIC;
        if (errorPages != null && errorPages.containsKey(String.valueOf(musicFocus))) {
            BasicUI.showConnectErrorPopup();
        } else if (reqList.elementAt(musicFocus) != null) {
            moveFocus(reqNode, focus, isRandomPlay, isNextFocus);
        } else {
            if (scene != null) { CommunicationManager.getInstance().showLoadingAnimation(); }
            new Thread("MusicPlayerPopup|checkFocusData()") {
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(300L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (musicContent != reqNode || playMusicList == null || playMusicType != PLAY_MY_MUSIC
                                || (scene != null && !CommunicationManager.getInstance().isLoadingAnimation())) {
                            break;
                        }
                        Hashtable errorPages = reqNode.getErrorPageNums();
                        if (reqList.elementAt(reqMusicFocus) != null) {
                            if (scene != null) { CommunicationManager.getInstance().hideLoadingAnimation(); }
                            moveFocus(reqNode, focus, isRandomPlay, isNextFocus);
                            break;
                        } else if (errorPages != null && errorPages.containsKey(String.valueOf(reqMusicFocus))) {
                            if (scene != null) { CommunicationManager.getInstance().hideLoadingAnimation(); }
                            BasicUI.showConnectErrorPopup();
                            break;
                        }
                    }
                }
            } .start();
        }
    }

    /**
     * Process galaxie play key.
     *
     * @param keyCode the key code
     */
    public void processGalaxieKey(int keyCode) {
        Logger.debug(this, "called processGalaxieKey()-keyCode : " + keyCode);
        int btnFocus = 0;
        switch (keyCode) {
        case KeyCodes.STOP:
            btnFocus = BTN_STOP;
            break;
        case KeyCodes.PLAY:
            btnFocus = BTN_GALAXIE_PLAY;
            break;
        case KeyCodes.PAUSE:
            btnFocus = BTN_GALAXIE_PLAY;
            break;
        case Rs.KEY_ENTER:
            btnFocus = flipBar.getFocus();
            break;
        default:
            break;
        }
        if (keyCode != Rs.KEY_ENTER) { flipBar.setFocus(btnFocus); }
        if (btnFocus == BTN_STOP) {
            MediaPlayHandler.getInstance().stop(this);
            playMusicType = PLAY_NO;
            CommunicationManager.getInstance().removeGalaxieChannelDataUpdatedListener();
            processMusicFooter();
            uiType = UI_NO_MUSIC;
            playTime = 0;
            stopFlipBar();
            flushAlbumImage();
            //stopTimer();
            addFooterBtn();
        } else if (btnFocus == BTN_GALAXIE_PLAY) {
            boolean exePlayer = true;
            if (keyCode == Rs.KEY_ENTER) {
                if (playMusicType == PLAY_GALAXIE) {
                    playMusicType = PAUSE_GALAXIE;
                } else { playMusicType = PLAY_GALAXIE; }
            } else if (keyCode == KeyCodes.PLAY) {
                if (playMusicType == PAUSE_GALAXIE) {
                    playMusicType = PLAY_GALAXIE;
                } else { exePlayer = false; }
            } else if (keyCode == KeyCodes.PAUSE) {
                if (playMusicType == PLAY_GALAXIE) {
                    playMusicType = PAUSE_GALAXIE;
                } else { exePlayer = false; }
                //} else { playMusicType = PLAY_GALAXIE; }
            }
            if (exePlayer) {
                FlipBarAction action = FlipBarAction.PAUSE;
                if (playMusicType == PAUSE_GALAXIE) {
                    action = FlipBarAction.PLAY;
                    MediaPlayHandler.getInstance().stop(this);
                } else {
                    Integer sourceId = null;
                    if (!Environment.EMULATOR) {
                        GalaxieContent content = (GalaxieContent) galaxieList.elementAt(playGalaxieFocus);
                        Object[] sharedData = content.getChInfos();
                        sourceId = (Integer) sharedData[GalaxieContent.INDEX_CHANNEL_SOURCE_ID];
                    }
                    MediaPlayHandler.getInstance().start(sourceId, this);
                }
                processMusicFooter();
                flipBar.setAction(action, BTN_GALAXIE_PLAY);
                repaint();
            }
        } else if (btnFocus == BTN_GALAXIE_CH) {
            uiType = UI_SELECT_GALAXIE;
            CommunicationManager.getInstance().removeGalaxieChannelDataUpdatedListener();
            addFooterBtn();
            stopFlipBar();
            flushAlbumImage();
            //stopTimer();
        }
        repaint();
    }

    /**
     * Change page.
     *
     * @param keyCode the key code
     */
    private void changePage(int keyCode) {
        if (uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE) {
            Vector list = musicSelectList;
            if (uiType == UI_SELECT_GALAXIE) { list = galaxieList; }
            int totalCount = list.size();
            if (keyCode == Rs.KEY_PAGE_UP && focusPosition >= MIDDLE_POS) {
                int initNum = listFocus - focusPosition;
                focusPosition = MIDDLE_POS;
                if (initNum - ROWS_PER_PAGE >= 0) {
                    listFocus = initNum - ROWS_PER_PAGE + MIDDLE_POS;
                } else { listFocus = MIDDLE_POS; }
                repaint();
            } else if (keyCode == Rs.KEY_PAGE_DOWN && focusPosition <= MIDDLE_POS) {
                int lastNum = 0;
                lastNum = listFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                focusPosition = MIDDLE_POS;
                if (lastNum + ROWS_PER_PAGE < totalCount) {
                    listFocus = lastNum + ROWS_PER_PAGE - MIDDLE_POS;
                } else { listFocus = totalCount - 1 - MIDDLE_POS; }
                repaint();
            }
        }
    }

    /**
     * Play music.
     *
     * @param content the content
     * @param focus the focus for playing
     * @param isRandomPlay whether play by random or normal
     * @param seq the seq
     * @param name the name
     * @param needArtistForTitle the need artist for title
     * @return true, if successful
     */
    public boolean playMusic(Content content, int focus, boolean isRandomPlay, int seq, String name,
            boolean needArtistForTitle) {
        if (content == null) { return true; }
        stopPagingThread();
        MediaPlayHandler.getInstance().stop(this);
        isRandom = isRandomPlay;
        musicSeq = seq;
        thirdTitle = name;
        Vector list = null;
        musicContent = content;
        int totalCount = content.getChildCount();
        if (content.isFolder()) {
            list = content.getChildren();
            if (list == null) {
                list = new Vector();
                list.setSize(totalCount);
            }
            musicContent.setChildren(list);
            if (isRandom && totalCount > 2) {
                Vector temp = new Vector();
                for (int i = 0; i < list.size(); i++) {
                    temp.addElement(i + "");
                }
                randomBuffer = new Vector();
                randomFocus = 0;
                while (!temp.isEmpty()) {
                    int random = (int) (Math.random() * temp.size());
                    randomBuffer.addElement(temp.remove(random));
                }
                focus = Integer.parseInt((String) randomBuffer.elementAt(randomFocus));
            } else { randomBuffer = null; }
        } else {
            list = new Vector();
            //list.setSize(1);
            list.addElement(content);
            randomBuffer = null;
        }
        Content reqContent = (Content) list.elementAt(focus);
        if (reqContent == null) {
            int startIndex = -1;
            String entryId = content.getParentId();
            String searchId = content.getId();
            if (content.isFolder()) {
                startIndex = focus;
                entryId = content.getId();
                searchId = null;
            }
            reqContent = HNHandler.getInstance().getPlayContent(entryId,
                    BasicUI.MENU_MUSIC, content.getChildSort(), startIndex, content.isArtistAllsongsDir(), searchId);
            if (reqContent == null) { return false; }
            list.setElementAt(reqContent, focus);
        }
        if (content.isFolder()) {
            if (needArtistForTitle && reqContent != null && musicSeq == BasicUI.SEQ_ARTIST) {
                thirdTitle = reqContent.getArtist() + thirdTitle;
            }
            int reqFocus = focus;
            if (isRandom && totalCount > 2) { reqFocus = randomFocus; }
            loadInitPagingData(musicContent, reqFocus, randomBuffer != null);
        }
        playMusicType = PLAY_MY_MUSIC;
        playMusicFocus = focus;
        playMusicList = list;
        playTime = 0;
        isRepeat = false;
        processMusicFooter();
        flushAlbumImage();
        final int reqId = ++requestId;
        new Thread("MusicPlayerPopup|playMusic()") {
            public void run() {
                startMusic(reqId, playMusicFocus);
            }
        } .start();
        return true;
    }

    /**
     * start music.
     *
     * @param reqId the req id
     * @param reqFocus the req focus
     */
    private synchronized void startMusic(final int reqId, final int reqFocus) {
        Logger.debug(this, "called startMusic()-reqId : " + reqId);
        if (reqId != requestId || playMusicList == null || playMusicType != PLAY_MY_MUSIC) {
            return;
        }
        try {
            Thread.sleep(300L);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (reqId != requestId || playMusicList == null
                || playMusicType != PLAY_MY_MUSIC || reqFocus != playMusicFocus) {
            return;
        }
        Logger.debug(this, "startMusic()-call start()");
        playTime = 0;
        //playMusicType = PLAY_MY_MUSIC;
        musicBar.setAction(FlipBarAction.PAUSE, BTN_PLAY);
        repaint();
        Content content = (Content) playMusicList.elementAt(playMusicFocus);
        rateFocus = RATE_PLAY;
        MediaPlayHandler.getInstance().start(content.getService(), this);
        stopTimer();
        startTimer();
    }

    /**
     * Notify event from MediaPlayHandler with ServiceContextEvent or controllEvent.
     *
     * @param event the event receive from MediaPlayHandler
     */
    public void notifyEvent(Object event) {
        if (event instanceof ServiceContextEvent) {
            if (event instanceof NormalContentEvent) {
                playMusicType = PLAY_GALAXIE;
                galaxieBar.setAction(FlipBarAction.PAUSE, BTN_GALAXIE_PLAY);
            } else {
                playMusicType = PAUSE_GALAXIE;
                galaxieBar.setAction(FlipBarAction.PLAY, BTN_GALAXIE_PLAY);
            }
            processMusicFooter();
            repaint();
        } else {
            if (playMusicType != PLAY_MY_MUSIC && playMusicType != PAUSE_MY_MUSIC) { return; }
            if (event instanceof BeginningOfContentEvent) {
                Logger.debug(this, "controllerUpdate() : BeginningOfContentEvent");
                processPlayKey(KeyCodes.PLAY);
            } else if (event instanceof EndOfContentEvent) {
                Logger.debug(this, "controllerUpdate() : EndOfContentEvent");
                MediaPlayHandler.getInstance().stop(this);
                rateFocus = RATE_PLAY;
                if (playMusicList != null) {
                    int totalCount = playMusicList.size();
                    playMusicType = PAUSE_MY_MUSIC;
                    boolean isNextPlay = false;
                    int nextFocus = -1;
                    if (isRandom && totalCount > 2) {
                        if (isRepeat || randomFocus + 1 < totalCount) {
                            nextFocus = randomFocus + 1;
                            if (nextFocus == totalCount && isRepeat) { nextFocus = 0; }
                            if (nextFocus < totalCount) {
                                //randomFocus = nextFocus;
                                //playMusicFocus = Integer.parseInt((String) randomBuffer.elementAt(randomFocus));
                                isNextPlay = true;
                            }
                        }
                    } else {
                        if (isRepeat || playMusicFocus + 1 < totalCount) {
                            nextFocus = playMusicFocus + 1;
                            if (nextFocus == totalCount && isRepeat) { nextFocus = 0; }
                            if (nextFocus < totalCount) {
                                //playMusicFocus = nextFocus;
                                isNextPlay = true;
                            }
                        }
                    }
                    if (isNextPlay) {
                        playTime = 0;
                        checkFocusData(nextFocus, isRandom && totalCount > 2, true);
                        /*Content content = (Content) playMusicList.elementAt(playMusicFocus);
                        ContentItem contentItem = (ContentItem) content.getContentEntry();
                        if (MediaPlayHandler.getInstance().start(contentItem.getItemService(), this)) {
                            playMusicType = PLAY_MY_MUSIC;
                        }
                        setAlbumImage(null);
                        stopTimer();
                        startTimer();*/
                    } else {
                        FlipBarAction action = FlipBarAction.PAUSE;
                        if (playMusicType == PAUSE_MY_MUSIC) { action = FlipBarAction.PLAY; }
                        flipBar.setAction(action, BTN_PLAY);
                        repaint();
                        if (scene == null) { stopTimer(); }
                    }
                } else {
                    requestId = -1;
                    playMusicType = MusicPlayerPopup.PLAY_NO;
                    if (scene == null) { stopTimer(); }
                }
                processMusicFooter();
                repaint();
            } else if (event instanceof RateChangeEvent) {
                float rate = ((RateChangeEvent) event).getRate();
                Logger.debug(this, "controllerUpdate() : RateChangeEvent-rate : " + rate);
                //playTime = MediaPlayHandler.getInstance().getPlayTimeLong();
                repaint();
            } else if (event instanceof ControllerErrorEvent) {
                playTime = 0;
                playMusicType = PAUSE_MY_MUSIC;
                musicBar.setAction(FlipBarAction.PLAY, BTN_PLAY);
                processMusicFooter();
                repaint();
                if (scene != null) {
                    showHidingEffect = false;
                    scene.requestPopupClose();
                } else { stopTimer(); }
                if (ScreensaverController.getInstance().isActive()) {
                    ScreensaverController.getInstance().keyAction();
                }
                stopPagingThread();
                BasicUI.showConnectErrorPopup();
            } else if (event instanceof PrefetchCompleteEvent) {
                //
            } else if (event instanceof StartEvent) {
                Logger.debug(this, "controllerUpdate() : StartEvent-playTime : " + playTime);
                playMusicType = PLAY_MY_MUSIC;
                //Video play에 의해 pause 된 경우 playTime을 처리함.
                if (playTime > 0) { MediaPlayHandler.getInstance().setMediaTime((int) (playTime / 1000)); }
                musicBar.setAction(FlipBarAction.PAUSE, BTN_PLAY);
                processMusicFooter();
                repaint();
            }
        }
    }

    /**
     * Notify stop from MediaPlayHandler.
     */
    public void notifyStop() {
        if (playMusicType == PLAY_MY_MUSIC || playMusicType == PAUSE_MY_MUSIC) {
            playMusicType = PAUSE_MY_MUSIC;
            playTime = MediaPlayHandler.getInstance().getPlayTimeLong();
        } else if (playMusicType == PLAY_GALAXIE || playMusicType == PAUSE_GALAXIE) {
            playMusicType = PAUSE_GALAXIE;
        }
        if (scene == null) { stopTimer(); }
        stopPagingThread();
        processMusicFooter();
    }

    /**
     * Process music footer.
     */
    private void processMusicFooter() {
        Logger.debug(this, "called processMusicFooter()");
        BasicUI parent = scene;
        SceneManager sceneManager = SceneManager.getInstance();
        int curSceneId = sceneManager.curSceneId;
        if (parent == null && curSceneId != SceneManager.SC_INVALID) {
            parent = sceneManager.scenes[curSceneId];
        }
        parent.processMusicFooterComp();
    }

    /**
     * Notify a updated data for Galaxie channel.
     * @param data To include a information
     */
    public void notifyGalaxieChannelDataUpdated(GalaxieChannelData data) {
        Logger.debug(this, "called notifyGalaxieChannelDataUpdated()-data : " + data);
        flushAlbumImage();
        galaxieChData = data;
        repaint();
        processMusicFooter();
    }

    /**
     * Notify a updated cover data for Galaxie channel.
     * @param img byte array for image.
     */
    public void notifyCoverImageUpdated(byte[] img) {
        Logger.debug(this, "called notifyCoverImageUpdated()-img : " + img);
        setAlbumImage(img);
    }

    /**
     * Load paging data.
     *
     * @param curNode the cur node
     * @param reqNum the req num
     * @param actionType the action type
     */
    private void loadPagingData(Content curNode, int reqNum, String actionType) {
        if (pagingDataThread == null) { return; }
        pagingDataThread.loadData(new Object[]{curNode, new Integer(reqNum), actionType});
    }

    /**
     * Stop paging thread.
     */
    private void stopPagingThread() {
        if (pagingDataThread == null) { return; }
        pagingDataThread.stop();
    }

    /**
     * Load init paging data.
     *
     * @param reqNode the req node
     * @param focus the focus
     * @param isRandomPlay the is random play
     */
    private void loadInitPagingData(Content reqNode, int focus, boolean isRandomPlay) {
        Vector list = reqNode.getChildren();
        if (list == null) { return; }
        int totalCount = list.size();
        if (totalCount > 1) {
            int lastContentNum = totalCount - 1;
            int cacheContentNum = Config.cachePageNumber * Config.loadDataNumber;
            if (cacheContentNum % 2 == 0) { cacheContentNum++; }
            int loopCount = Config.cachePageNumber * Config.loadDataNumber / 2;
            if (lastContentNum + 1 < cacheContentNum) { loopCount = (lastContentNum + 1) / 2; }
            for (int i = 1; i <= loopCount; i++) {
                int addFocus = focus + i;
                if (addFocus > lastContentNum) { addFocus = addFocus - lastContentNum - 1; }
                if (isRandomPlay) { addFocus = Integer.parseInt((String) randomBuffer.elementAt(addFocus)); }
                if (list.elementAt(addFocus) == null) { loadPagingData(reqNode, addFocus, PagingThread.TYPE_LOAD); }

                addFocus = focus - i;
                if (addFocus < 0) { addFocus = lastContentNum + addFocus + 1; }
                if (isRandomPlay) { addFocus = Integer.parseInt((String) randomBuffer.elementAt(addFocus)); }
                if (list.elementAt(addFocus) == null) { loadPagingData(reqNode, addFocus, PagingThread.TYPE_LOAD); }
            }
        }
    }

    /**
     * <code>PagingThread</code> This class process to bring rest datas by paging logic.
     *
     * @since   2012.08.24
     * @version 1.0, 2010.08.24
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class PagingThread implements Runnable {
        /** The Constant TYPE_LOAD. */
        public static final String TYPE_LOAD = "load";
        /** The Constant TYPE_REMOVE. */
        public static final String TYPE_REMOVE = "remove";
        /** The thread. */
        private Thread thread = null;
        /** The queue. */
        private ArrayList queue = new ArrayList();

        /**
         * Load data.
         *
         * @param obj the obj
         */
        public void loadData(Object[] obj) {
            Logger.debug(this, "called loadData()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "MusicPlayer-PagingThread");
                thread.start();
            }
            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    //Logger.debug(this, "loadData() - queue.add");
                    queue.notify();
                    //Logger.debug(this, "loadData() - request queue notify");
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            Logger.debug(this, "called stop()");
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            Thread startThread = thread;
            while (thread != null && startThread == thread) {
                try {
                    Thread.sleep(100L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null || startThread != thread) { break; }
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //Logger.debug(this, "run() - thread : " + thread);
                if (thread == null || startThread != thread) { break; }
                //Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object[] obj = (Object[]) queue.remove(0);
                    if (obj != null) {
                        Content curNode = (Content) obj[0];
                        int reqNum = ((Integer) obj[1]).intValue();
                        String actionType = (String) obj[2];
                        //Logger.debug(this, "req Num : " + reqNum);
                        //Logger.debug(this, "actionType : " + actionType);
                        Vector list = curNode.getChildren();
                        Hashtable errorPages = curNode.getErrorPageNums();
                        if (TYPE_LOAD.equals(actionType)) {
                            if (list != null && list.elementAt(reqNum) != null) { continue; }
                            if (errorPages != null && errorPages.containsKey(String.valueOf(reqNum))) { continue; }

                            Content reqContent = HNHandler.getInstance().getPlayContent(curNode.getId(),
                                    BasicUI.MENU_MUSIC, curNode.getChildSort(), reqNum, curNode.isArtistAllsongsDir(),
                                    null);
                            //
                            if (thread == null || startThread != thread) { break; }
                            if (reqContent == null) {
                                if (errorPages == null) { errorPages = new Hashtable(); }
                                errorPages.put(String.valueOf(reqNum), "");
                                curNode.setErrorPageNums(errorPages);
                                Logger.debug(this, "error.put : " + reqNum);
                            } else {
                                list.setElementAt(reqContent, reqNum);
                                //Logger.debug(this, "browse.put : " + reqNum);
                                repaint();
                            }
                        } else {
                            list.setElementAt(null, reqNum);
                            //Logger.debug(this, "browse.remove : " + reqNum);
                        }
                    }
                }
            }
        }
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.35 $ $Date: 2012/10/18 11:29:56 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The bg img. */
        private Image bgImg = null;
        //private Image btnBImg = null;
        /** The menu img. */
        private Image[] menuImg = new Image[2];
        /** The menu glow img. */
        private Image menuGlowImg = null;
        /** The menu focus img. */
        private Image menuFocusImg = null;
        /** The arrow l img. */
        private Image arrowLImg = null;
        /** The arrow r img. */
        private Image arrowRImg = null;
        //musics select list
        /** The list bg img. */
        private Image listBgImg = null;
        /** The list focus img. */
        private Image listFocusImg = null;
        /** The list shadow t img. */
        private Image listShadowTImg = null;
        /** The list shadow b img. */
        private Image listShadowBImg = null;
        /** The arrow t img. */
        private Image arrowTImg = null;
        /** The arrow b img. */
        private Image arrowBImg = null;
        //music play
        /** The album bg img. */
        private Image albumBgImg = null;
        /** The album glow img. */
        private Image albumGlowImg = null;
        /** The prev icon img. */
        private Image prevIconImg = null;
        /** The next icon img. */
        private Image nextIconImg = null;
        /** The part img. */
        private Image partImg = null;
        /** The galaxie logo img. */
        private Image galaxieLogoImg = null;
        /** The galaxie album bg img. */
        private Image galaxieAlbumBgImg = null;

        /** The title name txt. */
        private String[] titleNameTxt = new String[5];
        /** The menu txt. */
        private String[] menuTxt = new String[2];
        /** The hide mp txt. */
        private String hideMPTxt = null;
        /** The go my music txt. */
        public String goMyMusicTxt = null;
        /** The play last txt. */
        public String playLastTxt = null;
        /** The prev song txt. */
        public String prevSongTxt = null;
        /** The next song txt. */
        public String nextSongTxt = null;
        /** The music items. */
        public String[] musicItems = new String[3];
        /** The second title txt. */
        private String[] secondTitleTxt = new String[5];

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            bgImg = getImage("09_mp_bg.png");
            arrowLImg = getImage("02_ars_l.png");
            arrowRImg = getImage("02_ars_r.png");

            menuImg[0] = getImage("09_m_d.png");
            menuImg[1] = getImage("09_g_d.png");
            menuGlowImg = getImage("09_mp_glow.png");
            menuFocusImg = getImage("09_focus_133.png");

            listBgImg = getImage("pop_bg406.png");
            listFocusImg = getImage("09_focus412.png");
            listShadowTImg = getImage("07_issues_top.png");
            listShadowBImg = getImage("07_issues_bottom.png");
            arrowTImg = getImage("02_ars_t.png");
            arrowBImg = getImage("02_ars_b.png");

            albumBgImg = getImage("09_m_d.png");
            albumGlowImg = getImage("09_mp_glow.png");
            prevIconImg = getImage("09_icon_pre.png");
            nextIconImg = getImage("09_icon_next.png");
            partImg = getImage("09_mp_sep.png");
            galaxieLogoImg = getImage("09_mp_glogo.png");
            galaxieAlbumBgImg = getImage("09_g_d.png");
            super.prepare(c);
            String mpSelectMusicTxt = getString("mpSelectMusic");
            titleNameTxt[0] = mpSelectMusicTxt;
            titleNameTxt[1] = mpSelectMusicTxt;
            titleNameTxt[2] = getString("mpPlayMusic");
            titleNameTxt[3] = getString("mpSelectGalaxie");
            titleNameTxt[4] = getString("mpPlayGalaxie");
            menuTxt[0] = getString("mpMyMusic");
            menuTxt[1] = getString("mpGalaxieChs");
            hideMPTxt = getString("hideMP");
            goMyMusicTxt = getString("goMyMusic");
            playLastTxt = getString("playLast");
            prevSongTxt = getString("prevSong");
            nextSongTxt = getString("nextSong");
            musicItems[0] = getString("title");
            musicItems[1] = getString("artist");
            musicItems[2] = getString("albumTitle");
            secondTitleTxt[0] = BasicRenderer.getString("Album") + " :";
            secondTitleTxt[1] = getString("artist") + " :";
            secondTitleTxt[2] = BasicRenderer.getString("songs") + " :";
            secondTitleTxt[3] = BasicRenderer.getString("Genre") + " :";
            secondTitleTxt[4] = BasicRenderer.getString("Playlist") + " :";
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-0, -220);
            //bg
            g.drawImage(bgImg, 0, 220, c);
            g.setFont(Rs.F18);
            g.setColor(Rs.C200);
            String title = titleNameTxt[uiType];
            if (uiType == UI_PLAY_MUSIC && musicContent != null && musicContent.isFolder()) {
                title += " - " + secondTitleTxt[musicSeq] + " " + thirdTitle;
            }
            g.drawString(title, 57, 298);

            if (uiType == UI_NO_MUSIC) {
                for (int i = 0; i < menuImg.length; i++) {
                    int addX = i * 248;
                    g.drawImage(menuImg[i], 267 + addX, 315, c);
                    g.drawImage(menuGlowImg, 288 + addX, 315, c);
                    g.setFont(Rs.F17);
                    g.setColor(Rs.C190);
                    if (i == contentFocus) {
                        g.drawImage(menuFocusImg, 281 + addX, 312, c);
                        if (i > 0) {
                            g.drawImage(arrowLImg, 512, 369, c);
                        } else { g.drawImage(arrowRImg, 429, 369, c); }
                        g.setFont(Rs.F19);
                        g.setColor(Rs.C255203000);
                        GraphicUtil.drawStringCenter(g, menuTxt[i], 355 + addX, 468);
                    } else { GraphicUtil.drawStringCenter(g, menuTxt[i], 354 + addX, 467); }
                }
            } else if (uiType == UI_SELECT_MUSIC || uiType == UI_SELECT_GALAXIE) {
                g.drawImage(listBgImg, 276, 313, c);
                Vector list = musicSelectList;
                if (uiType == UI_SELECT_GALAXIE) {
                    list = galaxieList;
                    g.drawImage(galaxieLogoImg, 75, 376, c);
                }
                int totalCount = list.size();
                if (totalCount > 0) {
                    int lineGap = 31;
                    g.setFont(Rs.F18);
                    g.setColor(Rs.C215214214);
                    int initNum = listFocus - focusPosition;
                    int lastNum = 0;
                    if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                    lastNum = listFocus + ROWS_PER_PAGE - 1 - focusPosition;
                    if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                    for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                        Object content = list.elementAt(i);
                        int addPos = (j * lineGap);
                        if (i != listFocus) { paintItem(g, c, false, content, addPos); }
                    }
                    if (initNum > 0) { g.drawImage(listShadowTImg, 277, 314, c); }
                    if (lastNum < totalCount - 1) { g.drawImage(listShadowBImg, 277, 436, c); }
                    //focus
                    Object focusedObj = list.elementAt(listFocus);
                    paintItem(g, c, true, focusedObj, (focusPosition * lineGap));
                    if (initNum > 0) { g.drawImage(arrowTImg, 470, 307, c); }
                    if (lastNum < totalCount - 1) { g.drawImage(arrowBImg, 469, 460, c); }
                    if (uiType == UI_SELECT_GALAXIE) {
                        g.setFont(Rs.F20);
                        g.setColor(Rs.C255);
                        GalaxieContent content = (GalaxieContent) focusedObj;
                        TvChannel channel = content.getTvChannel();
                        if (channel != null) {
                            try {
                                String str = TextUtil.shorten(channel.getFullName(), g.getFontMetrics(), 894 - 704);
                                g.drawString(str, 704, 378);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                String[] str =
                                    TextUtil.split(channel.getDescription(), g.getFontMetrics(), 894 - 704, 2);
                                for (int i = 0; i < str.length; i++) {
                                    g.drawString(str[i], 704, 402 + (i * 17));
                                }
                            } catch (Throwable t) {
                                t.printStackTrace();
                            }
                        }
                    }
                }
            } else if (uiType == UI_PLAY_MUSIC) {
                g.drawImage(albumBgImg, 236, 323, c);
                if (albumImg != null) {
                    try {
                        int imgWidth = albumImg.getWidth(null);
                        int imgHeight = albumImg.getHeight(null);
                        int posX = 257 + ((133 - imgWidth) / 2);
                        int posY = 323 + ((133 - imgHeight) / 2);
                        g.drawImage(albumImg, posX, posY, c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                g.drawImage(albumGlowImg, 257, 323, c);
                if (musicContent != null && musicContent.isFolder()) { g.drawImage(prevIconImg, 95, 357, c); }
                g.drawImage(partImg, 233, 321, c);
                g.drawImage(partImg, 732, 321, c);

                Content focusContent = (Content) playMusicList.elementAt(playMusicFocus);
                String[] infos = {focusContent.getName(), focusContent.getArtist(), focusContent.getAlbumName()};
                for (int i = 0; i < musicItems.length; i++) {
                    int addPos = i * 20;
                    g.setFont(Rs.F16);
                    g.setColor(Rs.C130);
                    g.drawString(musicItems[i], 419, 333 + addPos);
                    if (infos[i] != null) {
                        g.setFont(Rs.F18);
                        g.setColor(Rs.C255);
                        g.drawString(TextUtil.shorten(infos[i], g.getFontMetrics(), 707 - 513), 513, 333 + addPos);
                    }
                }
                // progress bar
                g.setFont(Rs.F16);
                g.setColor(Rs.C195);
                Formatter formatter = Formatter.getCurrent();
                String str = formatter.getDuration(playTime);
                g.drawString(str, 418, 398);
                long duration = focusContent.getDurationLong();
                if (duration <= 0) { duration = MediaPlayHandler.getInstance().getDurationLong(); }
                str = formatter.getDuration(duration);
                g.drawString(str, 673, 398);
                int barRight = 461 + 204;
                int barLeft = 461;
                int barWidth = barRight - barLeft;
                int curWidth = 0;
                if (duration > 0) { curWidth = getWidth(playTime, 0, duration, barWidth); }
                g.setColor(Rs.C120);
                g.fillRect(barLeft, 390, barWidth, 6);
                g.setColor(Rs.C255213000);
                g.fillRect(barLeft, 390, curWidth, 6);

                //previous song & next song
                g.setFont(Rs.F17);
                g.setColor(Rs.C126125125);
                if (musicContent != null && musicContent.isFolder()) {
                    g.drawString(prevSongTxt, 121, 369);
                    g.drawString(nextSongTxt, 754, 369);
                    
                    int width = g.getFontMetrics().stringWidth(nextSongTxt);
                    g.drawImage(nextIconImg, 754 + width + 6, 357, c);
                }
                int totalCount = playMusicList.size();
                if (totalCount > 1) {
                    g.setColor(Rs.C224);
                    int prevFocus = playMusicFocus - 1;
                    if (isRandom && totalCount > 2) { prevFocus = randomFocus - 1; }
                    if (prevFocus < 0 && isRepeat) { prevFocus = totalCount - 1; }
                    if (prevFocus >= 0) {
                        if (isRandom && totalCount > 2) {
                            prevFocus = Integer.parseInt((String) randomBuffer.elementAt(prevFocus));
                        }
                        Content content = (Content) playMusicList.elementAt(prevFocus);
                        if (content != null) {
                            g.setFont(Rs.F18);
                            str = TextUtil.shorten(content.getName(), g.getFontMetrics(), 212 - 57);
                            GraphicUtil.drawStringRight(g, str, 212, 393);
                            g.setFont(Rs.F17);
                            str = TextUtil.shorten(content.getArtist(), g.getFontMetrics(), 212 - 57);
                            GraphicUtil.drawStringRight(g, str, 212, 411);
                        }
                    }
                    int nextFocus = playMusicFocus + 1;
                    if (isRandom && totalCount > 2) { nextFocus = randomFocus + 1; }
                    if (nextFocus == totalCount && isRepeat) { nextFocus = 0; }
                    if (nextFocus < totalCount) {
                        if (isRandom && totalCount > 2) {
                            nextFocus = Integer.parseInt((String) randomBuffer.elementAt(nextFocus));
                        }
                        Content content = (Content) playMusicList.elementAt(nextFocus);
                        if (content != null) {
                            g.setFont(Rs.F18);
                            str = TextUtil.shorten(content.getName(), g.getFontMetrics(), 212 - 57);
                            g.drawString(str, 754, 393);
                            g.setFont(Rs.F17);
                            str = TextUtil.shorten(content.getArtist(), g.getFontMetrics(), 212 - 57);
                            g.drawString(str, 754, 411);
                        }
                    }
                }
            } else if (uiType == UI_PLAY_GALAXIE) {
                g.drawImage(galaxieAlbumBgImg, 236, 323, c);
                if (albumImg != null) {
                    try {
                        int imgWidth = albumImg.getWidth(null);
                        int imgHeight = albumImg.getHeight(null);
                        int posX = 257 + ((133 - imgWidth) / 2);
                        int posY = 323 + ((133 - imgHeight) / 2);
                        g.drawImage(albumImg, posX, posY, c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                g.drawImage(albumGlowImg, 257, 323, c);
                g.drawImage(partImg, 233, 321, c);
                g.drawImage(partImg, 732, 321, c);

                String[] infos = new String[3];
                if (galaxieChData != null) {
                    try {
                        infos[0] = galaxieChData.getCurrentTitle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        infos[1] = galaxieChData.getCurrentArtist();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        infos[2] = galaxieChData.getCurrentAlbum();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                for (int i = 0; i < musicItems.length; i++) {
                    int addPos = i * 20;
                    g.setFont(Rs.F16);
                    g.setColor(Rs.C130);
                    g.drawString(musicItems[i], 419, 333 + addPos);
                    if (infos[i] != null) {
                        g.setFont(Rs.F18);
                        g.setColor(Rs.C255);
                        g.drawString(TextUtil.shorten(infos[i], g.getFontMetrics(), 707 - 513), 513, 333 + addPos);
                    }
                }

                //next song
                g.setFont(Rs.F17);
                g.setColor(Rs.C126125125);
                g.drawString(nextSongTxt, 754, 369);
                if (galaxieChData != null) {
                    g.setFont(Rs.F18);
                    String str = null;
                    try {
                        str = TextUtil.shorten(galaxieChData.getNextTitle(), g.getFontMetrics(), 212 - 57);
                        g.drawString(str, 754, 393);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    g.setFont(Rs.F17);
                    try {
                        str = TextUtil.shorten(galaxieChData.getNextArtist(), g.getFontMetrics(), 212 - 57);
                        g.drawString(str, 754, 411);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                //channel
                g.drawImage(galaxieLogoImg, 58, 359, c);
                if (galaxieList != null && galaxieList.size() > playGalaxieFocus) {
                    g.setFont(Rs.F18);
                    g.setColor(Rs.C224);
                    GalaxieContent content = (GalaxieContent) galaxieList.elementAt(playGalaxieFocus);
                    Object[] sharedData = content.getChInfos();
                    String str = (String) sharedData[GalaxieContent.INDEX_CHANNEL_NAME];
                    g.drawString(str, 58, 415);
                    TvChannel channel = content.getTvChannel();
                    if (channel != null) {
                        try {
                            str = TextUtil.shorten(channel.getFullName(), g.getFontMetrics(), 220 - 120);
                            g.drawString(str, 120, 415);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            g.translate(0, 220);
        }

        /**
         * Paint item.
         *
         * @param g the Graphics
         * @param c the UIComponent
         * @param isFocus the is focus
         * @param item the DeviceInfo
         * @param addPos the add position
         */
        private void paintItem(Graphics g, UIComponent c, boolean isFocus, Object item, int addPos) {
            g.setColor(Rs.C215214214);
            if (isFocus) {
                g.setColor(Rs.C0);
                g.drawImage(listFocusImg, 274, 313 + addPos, c);
            }
            if (uiType == UI_SELECT_MUSIC) {
                String name = "";
                if (item instanceof String) {
                    name = TextUtil.shorten((String) item, g.getFontMetrics(), 666 - 292);
                } else {
                    name = getName(((Content) item), g, 666 - 292);
                }
                g.drawString(name, 292, 335 + addPos);
            } else {
                GalaxieContent content = (GalaxieContent) item;
                Object[] sharedData = content.getChInfos();
                TvChannel channel = content.getTvChannel();
                String str = ((Integer) sharedData[GalaxieContent.INDEX_CHANNEL_NUMBER]).toString();
                g.drawString(str, 292, 335 + addPos);
                str = (String) sharedData[GalaxieContent.INDEX_CHANNEL_NAME];
                g.drawString(str, 336, 335 + addPos);
                if (channel != null) {
                    try {
                        str = TextUtil.shorten(channel.getFullName(), g.getFontMetrics(), 666 - 399);
                        g.drawString(str, 399, 335 + addPos);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        /**
         * Gets the name.
         *
         * @param content the Content
         * @param g the Graphics
         * @param width the width for limit
         * @return the name
         */
        private String getName(Content content, Graphics g, int width) {
            int numWidth = 0;
            String numTxt = "";
            if (content.getTotalCount() > 0) {
                numTxt = " (" + content.getTotalCount() + ")";
                numWidth = g.getFontMetrics().stringWidth(numTxt);
            }
            return TextUtil.shorten(content.getName(), g.getFontMetrics(), width - numWidth) + numTxt;
        }

        /**
         * Gets the width.
         *
         * @param time the time
         * @param from the from
         * @param to the to
         * @param totalWidth the total width
         * @return the width
         */
        private int getWidth(long time, long from, long to, int totalWidth) {
            long dur = to - from;
            long cur = time - from;
            return Math.min(totalWidth, Math.max(0, (int) (cur * totalWidth / dur)));
        }
    }
}

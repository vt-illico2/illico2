/*
 *  @(#)HomeUI.java 1.0 2011.05.24
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.gui.HomeRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.SearchPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>HomeUI</code> This class show the home page.
 *
 * @since   2011.05.24
 * @version $Revision: 1.17 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class HomeUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant TH_CONTENT_COUNT. */
    private static final String TH_CONTENT_SEARCH = "searchContent";
    /** The Constant OPT_GOTO. */
    protected static final MenuItem OPT_GOTO =
            new MenuItem("renderer.goTo", new MenuItem[]{OPT_PHOTO, OPT_MUSIC, OPT_VIDEO});

    /** The HomeRenderer instance. */
    private HomeRenderer renderer = null;
    /** The connect device. */
    public DeviceInfo connectDevice = null;
    /** The temp device. */
    public static DeviceInfo tempDevice = null;

    /** The menu focus. */
    public int menuFocus = MENU_MUSIC;

    /**
     * Initialize the Scene.
     */
    public HomeUI() {
        renderer = new HomeRenderer();
        setRenderer(renderer);
        rootMenuItem = new MenuItem("D-Option", new MenuItem[] {OPT_GOTO, OPT_PREFERENCE, OPT_CHANGE_DEV, OPT_HELP});
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();
        super.start(isReset, sendData);
        if (isReset) {
            menuFocus = MENU_MUSIC;
            tempDevice = (DeviceInfo) receivedData;
        } /* else  if (focusHistory != null && focusHistory.length > 0) {
            menuFocus = Integer.parseInt((String) focusHistory[0]);
        }*/
        resetRootChildren();
        loadData(new Object[]{TH_CONTENT_SEARCH}, true);
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        //footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        footer.addButtonWithLabel(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
        footer.addButtonWithLabel(BasicRenderer.btnDImg, BasicRenderer.btnOptTxt);
    }

    /**
     * set the focus history.
     * called by super.stop()
     */
    /*protected void setFocusHistory() {
        Object[] objs = new Object[1];
        objs[0] = String.valueOf(menuFocus);
        setFocusHistory(objs);
    }*/

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        renderer.stop();
        connectDevice = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
        contentRoot = null;
        connectDevice = null;
        tempDevice = null;
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (--menuFocus < 0) { menuFocus = MENU_VIDEO; }
            repaint();
            break;
        case Rs.KEY_RIGHT:
            if (++menuFocus > MENU_VIDEO) { menuFocus = 0; }
            repaint();
            break;
        case Rs.KEY_ENTER:
            clickEffect.start(369, 160, 223, 372 - 160);
            goToContentUI(menuFocus);
            break;
        case Rs.KEY_BACK:
            break;
        case Rs.KEY_UP:
            //if (keyCode == Rs.KEY_BACK) { footer.clickAnimation(BasicRenderer.btnBackTxt); }
            //musicPlayerPopup.dispose();
            SceneManager.getInstance().goToScene(SceneManager.SC_INIT, false, null);
            break;
        case Rs.KEY_DOWN:
            goToPreferenceUI();
            break;
        case KeyCodes.COLOR_B:
            footer.clickAnimation(BasicRenderer.btnMPlayerTxt);
            musicPlayerPopup.resetFocus();
            addPopup(musicPlayerPopup);
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            if (keyCode == Rs.KEY_EXIT) { clickEffect.start(renderer.exitBtnPosX, 488, 31, 22); }
            return false;
        case KeyCodes.SEARCH:
            footer.clickAnimation(BasicRenderer.btnSearchTxt);
            searchPopup.setListOrder(-1, -1, null);
            searchPopup.resetStateFocus();
            addPopup(searchPopup);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        Logger.debug(this, "called notifyFromDataLoadingThread()-procId : " + procId);
        if (procId.equals(TH_CONTENT_SEARCH)) {
            Logger.debug(this, "notifyFromDataLoadingThread()-contentRoot : " + contentRoot);
            for (int i = 0; contentRoot != null && i < contentRoot.length; i++) {
                HNHandler.getInstance().setTotalCount(contentRoot[i]);
            }
            CommunicationManager.getInstance().hideLoadingAnimation();
            connectDevice = tempDevice;
            Logger.debug(this, "notifyFromDataLoadingThread()-connectDevice : " + connectDevice);
            repaint();
            if (searchContent != null) {
                searchContent = null;
                if (!isSceneReset) {
                    searchPopup.setListOrder(-1, -1, null);
                    addPopup(searchPopup);
                }
            }
            clearAllTempData();
        }
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof SearchPopup) {
            removePopup();
            if (para != null) { goToSearchContent((Content) para); }
        } else { super.requestPopupOk(para); }
    }
}

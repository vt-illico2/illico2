/*
 *  @(#)Popup.java 1.0 2011.05.20
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>Popup</code> the super class of each popup.
 *
 * @since   2011.05.20
 * @version $Revision: 1.9 $ $Date: 2012/10/16 01:05:13 $
 * @author  tklee
 * @see     java.awt.Component
 */
public class Popup extends UIComponent {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The data center. */
    protected DataCenter dataCenter = DataCenter.getInstance();

    /** The scene for listener. */
    protected BasicUI scene = null;
    /** The ClickingEffect instance. */
    protected ClickingEffect clickEffect = null;

    /** The mode to identify. */
    protected int mode = 0;

    /**
     * return popup mode.
     *
     * @return popup mode
     */
    public int getMode() {
        return mode;
    }

    /**
     * set popup mode.
     *
     * @param popupMode  popup mode
     */
    public void setMode(int popupMode) {
        this.mode = popupMode;
    }

    /**
     * Reset focus.
     */
    public void resetFocus() {
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        prepare();
    }

    /**
     * Start effect.
     */
    public void startEffect() { }

    /**
     * when popup is finished, this is called.
     */
    public void stop() {
    }

    /**
     * when App is finished, this is called.
     */
    public void dispose() {
    }

    /**
     * Sets the listener.
     *
     * @param listener the listener for callback as BasicUI
     */
    public void setListener(BasicUI listener) {
        scene = listener;
    }

    /**
     * implement handleKey method.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    public boolean handleKey(int code) {
        //if (code == KeyCodes.LAST) { return true; }
        if (CommunicationManager.getInstance().isLoadingAnimation()) {
            if (code == Rs.KEY_EXIT || code == KeyCodes.LIVE) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                scene.requestPopupClose();
            }
            return true;
        }
        if (keyAction(code)) { return true; }
        if (code == Rs.KEY_EXIT || code == KeyCodes.LIVE) {
            scene.requestPopupClose();
            return true;
        }
        return BasicUI.verifyKeyConsistency(code);
    }

    /**
     * Key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        return true;
    }

    /**
     * <code>BasicPopupRenderer</code> This class the basic renderer class for each popup renderer.
     * Each popup renderder has to inherit this class as super class.
     * @since   2011.05.20
     * @version $Revision: 1.9 $ $Date: 2012/10/16 01:05:13 $
     * @author  tklee
     */
    public class BasicPopupRenderer extends Renderer {
        /**
         * Gets the string value of UI from DataCenter. If the value is null, return a empty string as "".
         *
         * @param key the key
         * @return the string
         */
        public String getString(String key) {
            if (key == null) { return ""; }
            String value = DataCenter.getInstance().getString("popup." + key);
            if (value != null) { return value; }
            return "";
        }

        /**
         * Gets the image from DataCenter.
         *
         * @param imageName the image name
         * @return the image
         */
        public Image getImage(String imageName) {
            return DataCenter.getInstance().getImage(imageName);
        }

        /**
         * remove the image from DataCenter.
         *
         * @param imageName the image name
         */
        public void removeImage(String imageName) {
            DataCenter.getInstance().removeImage(imageName);
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the UIComponent
         */
        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            Logger.debug(this, "c.getBounds() : " + c.getBounds());
            return c.getBounds();
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) { }
    }
}

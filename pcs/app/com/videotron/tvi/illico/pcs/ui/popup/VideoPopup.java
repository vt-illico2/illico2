/*
 *  @(#)VideoPopup.java 1.0 2011.07.26
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>VideoPopup</code> the popup for Video stopped and finished.
 *
 * @since   2011.07.26
 * @version $Revision: 1.9 $ $Date: 2012/09/18 02:44:00 $
 * @author  tklee
 */
public class VideoPopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_STOPPED. */
    public static final int MODE_STOPPED = 0;
    /** The Constant MODE_FINISHED. */
    public static final int MODE_FINISHED = 1;
    /** The Constant MODE_STOPPED_SEARCH. */
    public static final int MODE_STOPPED_SEARCH = 2;
    /** The Constant MODE_FINISHED_SEARCH. */
    public static final int MODE_FINISHED_SEARCH = 3;
    /** The Constant EXE_CONTINUE. */
    public static final int EXE_CONTINUE = 0;
    /** The Constant EXE_BEGINNING. */
    public static final int EXE_BEGINNING = 1;
    /** The Constant EXE_LIST. */
    public static final int EXE_LIST = 2;
    /** The Constant EXE_LIST. */
    public static final int EXE_LIST_SEARCH = 3;
    /** The btn exe. */
    private final int[][] btnExe = {
            {EXE_CONTINUE, EXE_BEGINNING},
            {EXE_BEGINNING, EXE_LIST},
            {EXE_CONTINUE, EXE_BEGINNING},
            {EXE_BEGINNING, EXE_LIST_SEARCH}};
    /** The Footer instance. */
    private Footer footer = null;
    /** The renderer. */
    private PopupRenderer renderer = null;

    /** The title. */
    private String title = "";
    /** The btn focus. */
    private int btnFocus = 0;

    /**
     * Initialize the Component.
     */
    public VideoPopup() {
        setBounds(276, 113, 410, 330);
        renderer = new PopupRenderer();
        setRenderer(renderer);
        footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
        footer.setBounds(0, 377 - 113, 649 - 276, 24);
        add(footer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        prepare();
        footer.reset();
        if (mode == MODE_STOPPED || mode == MODE_FINISHED) {
            footer.addButtonWithLabel(BasicRenderer.btnExitImg, renderer.returnListTxt);
        } else { footer.addButtonWithLabel(BasicRenderer.btnExitImg, renderer.exitSearchList); }
        btnFocus = 0;
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() { }

    /**
     * Sets the title.
     *
     * @param contentTitle the new title
     */
    public void setTitle(String contentTitle) {
        title = contentTitle;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (btnFocus > 0) {
                btnFocus--;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (btnFocus < btnExe[mode].length - 1) {
                btnFocus++;
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            clickEffect.start(333 - 276, 242 + (btnFocus * 37) - 113, 293, 32);
            scene.requestPopupOk(new Integer(btnExe[mode][btnFocus]));
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            if (keyCode == Rs.KEY_EXIT) { footer.clickAnimation(BasicRenderer.btnExitImg); }
            if (mode == MODE_STOPPED || mode == MODE_FINISHED) {
                scene.requestPopupOk(new Integer(EXE_LIST));
            } else { scene.requestPopupOk(new Integer(EXE_LIST_SEARCH)); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.9 $ $Date: 2012/09/18 02:44:00 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The btn dim img. */
        private Image btnDimImg = null;

        /** The title txt. */
        private String[] titleTxt = new String[4];
        /** The btn txt. */
        private String[][] btnTxt = new String[4][2];
        /** The return list txt. */
        public String returnListTxt = null;
        /** The return search list txt. */
        private String returnSearchListTxt = null;
        /** The exit search list. */
        public String exitSearchList = null;

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("05_popup_sha.png");
            bgMImg = getImage("05_bgglow_m.png");
            bgTImg = getImage("05_bgglow_t.png");
            bgBImg = getImage("05_bgglow_b.png");
            gapImg = getImage("05_sep.png");
            highImg = getImage("05_high.png");
            btnFocusImg = getImage("btn_293_foc.png");
            btnDimImg = getImage("btn_293.png");
            super.prepare(c);
            titleTxt[0] = getString("videoStopTitle");
            titleTxt[1] = getString("videoFinishTitle");
            titleTxt[2] = titleTxt[0];
            titleTxt[3] = titleTxt[1];
            String continueTxt = getString("btnResume");
            String begginingTxt = getString("btnBiggining");
            returnListTxt = getString("returnVideoList");
            returnSearchListTxt = getString("returnSearchList");
            exitSearchList = getString("exitSearchList");
            btnTxt[0][0] = continueTxt;
            btnTxt[0][1] = begginingTxt;
            btnTxt[1][0] = begginingTxt;
            btnTxt[1][1] = returnListTxt;
            btnTxt[2][0] = continueTxt;
            btnTxt[2][1] = begginingTxt;
            btnTxt[3][0] = begginingTxt;
            btnTxt[3][1] = returnSearchListTxt;
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -113);
            //bg
            g.drawImage(bgMImg, 276, 168, 410, 172, c);
            g.drawImage(bgTImg, 276, 113, c);
            g.drawImage(bgBImg, 276, 340, c);
            g.drawImage(shadowImg, 306, 364, c);
            g.setColor(Rs.C35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(gapImg, 285, 181, c);
            g.drawImage(highImg, 306, 143, c);

            //btn
            g.setFont(Rs.F18);
            g.setColor(Rs.C0);
            for (int i = 0; i < btnTxt[mode].length; i++) {
                Image btnImg = btnDimImg;
                if (i == btnFocus) { btnImg = btnFocusImg; }
                int addPos = i * 37;
                g.drawImage(btnImg, 333, 242 + addPos, c);
                GraphicUtil.drawStringCenter(g, btnTxt[mode][i], 480, 263 + addPos);
            }
            //title
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            String descTxt = " " + titleTxt[mode];
            int descWidth = g.getFontMetrics().stringWidth(descTxt);
            String str = TextUtil.shorten(title, g.getFontMetrics(), (627 - 333) - descWidth) + descTxt;
            GraphicUtil.drawStringCenter(g, str, 484, 170);
            g.translate(276, 113);
        }
    }
}

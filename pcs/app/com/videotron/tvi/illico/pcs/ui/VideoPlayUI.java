/*
 *  @(#)VideoPlayUI.java 1.0 2011.07.22
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import javax.media.ControllerErrorEvent;
import javax.media.MediaTimeSetEvent;
import javax.media.PrefetchCompleteEvent;
import javax.media.RateChangeEvent;
import javax.media.StartEvent;
import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hn.content.ContentItem;
import org.ocap.shared.media.BeginningOfContentEvent;
import org.ocap.shared.media.EndOfContentEvent;

import com.videotron.tvi.illico.flipbar.FlipBar;
import com.videotron.tvi.illico.flipbar.FlipBarAction;
import com.videotron.tvi.illico.flipbar.FlipBarContent;
import com.videotron.tvi.illico.flipbar.FlipBarFactory;
import com.videotron.tvi.illico.flipbar.FlipBarListener;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayListener;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.VideoPlayRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.NotiPopup;
import com.videotron.tvi.illico.pcs.ui.popup.VideoPopup;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>VideoPlayUI</code> This class is for Playing of Video.
 *
 * @since   2011.07.22
 * @version $Revision: 1.24 $ $Date: 2012/09/21 06:37:03 $
 * @author  tklee
 */
public class VideoPlayUI extends BasicUI implements FlipBarListener, TVTimerWentOffListener, MediaPlayListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;
    /** The Constant TH_CONTENT_LIST. */
    private static final String TH_START_PLAY = "startPlay";

    /** The Constant PLAY_RATE. */
    private static final float[] PLAY_RATE = {-16, -8, -4, -2, 0, 0.25f, 1, 2, 4, 8, 16};
    /** The Constant RATE_RW16. */
    private static final int RATE_RW16 = 0;
    /** The Constant RATE_RW8. */
    //private static final int RATE_RW8 = 1;
    /** The Constant RATE_RW4. */
    //private static final int RATE_RW4 = 2;
    /** The Constant RATE_RW2. */
    private static final int RATE_RW2 = 3;
    /** The Constant RATE_PAUSE. */
    private static final int RATE_PAUSE = 4;
    /** The Constant RATE_SLOW. */
    private static final int RATE_SLOW = 5;
    /** The Constant RATE_PLAY. */
    private static final int RATE_PLAY = 6;
    /** The Constant RATE_FF2. */
    private static final int RATE_FF2 = 7;
    /** The Constant RATE_FF4. */
    //private static final int RATE_FF4 = 8;
    /** The Constant RATE_FF8. */
    //private static final int RATE_FF8 = 9;
    /** The Constant RATE_FF16. */
    private static final int RATE_FF16 = 10;
    /** The Constant FOCUS_STOP. */
    private static final int FOCUS_STOP = PLAY_RATE.length;
    /** The Constant FOCUS_SKIP_BACK. */
    private static final int FOCUS_SKIP_BACK = FOCUS_STOP + 1;
    /** The Constant FOCUS_SKIP_FORWARD. */
    private static final int FOCUS_SKIP_FORWARD = FOCUS_STOP + 2;

    /** The Constant BTN_SKIP_BACK. */
    private static final int BTN_SKIP_BACK = 0;
    /** The Constant BTN_RW. */
    private static final int BTN_RW = 1;
    /** The Constant BTN_STOP. */
    private static final int BTN_STOP = 2;
    /** The Constant BTN_PAUSE. */
    private static final int BTN_PAUSE = 3;
    /** The Constant BTN_PLAY. */
    private static final int BTN_PLAY = 4;
    /** The Constant BTN_FF. */
    private static final int BTN_FF = 5;
    /** The Constant BTN_SKIP_FORWARD. */
    private static final int BTN_SKIP_FORWARD = 6;
    /** The Constant BTN_SCREEN_SCALE. */
    private static final int BTN_SCREEN_SCALE = 7;

    /** The Constant SKIP_SEC. */
    private static final int SKIP_SEC = 30;
    /** The Constant BACK_SKIP_SEC. */
    private static final int BACK_SKIP_SEC = 8;
    /** The Constant FEED_SHOW_SEC. */
    private static final int FEED_SHOW_SEC = 3;
    /** The Constant FLIPBAR_SHOW_SEC. */
    private static final int FLIPBAR_SHOW_SEC5 = 5;
    /** The Constant FLIPBAR_SHOW_SEC60. */
    private static final int FLIPBAR_SHOW_SEC60 = 60;
    /** The Constant SLEEP_TIME. */
    private static final long SLEEP_TIME = 500L;

    /** The renderer. */
    private VideoPlayRenderer renderer = null;
    /** The video popup. */
    private VideoPopup videoPopup = null;
    /** The FlipBar instance. */
    private FlipBar flipBar = null;
    /** The timer spec. */
    private TVTimerSpec timerSpec = null;

    /** The is timer. */
    private boolean isTimer = false;
    /** The rate focus. */
    private int rateFocus = RATE_PLAY;
    /** The btn focus. */
    private int buttonFocus = 3;
    /** The is full screen. */
    private boolean isFullScreen = false;
    /** The feed img focus. */
    public int feedImgFocus = -1;
    /** The feed show count. */
    public int feedShowCount = -1;
    /** The flipbar show count. */
    private int flipbarShowCount = 0;
    /** The flipbarlimit count. */
    private int flipbarlimitCount = FLIPBAR_SHOW_SEC5;
    /** The play content. */
    private Content playContent = null;

    /**
     * Initialize the Scene.
     */
    public VideoPlayUI() {
        renderer = new VideoPlayRenderer();
        setRenderer(renderer);
        flipBar = FlipBarFactory.getInstance().createPcsVodFlipBar();
        videoPopup = new VideoPopup();
        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME);
        timerSpec.setRepeat(true);
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();
        remove(footer);
        feedImgFocus = -1;
        feedShowCount = -1;
        flipbarShowCount = 0;
        flipbarlimitCount = FLIPBAR_SHOW_SEC5;
        super.start(isReset, sendData);
        rateFocus = RATE_PLAY;
        buttonFocus = 3;
        //if (isReset) { }
        loadData(new Object[]{TH_START_PLAY}, true);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        stopTimer();
        MediaPlayHandler.getInstance().resizeVideo(0, 0, 960, 540);
        MediaPlayHandler.getInstance().dispose();
        renderer.stop();
        flipBar.removeListener(this);
        flipBar.stop();
        playContent = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        if (isTimer) { return; }
        try {
            timerSpec.addTVTimerWentOffListener(this);
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            isTimer = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        if (!isTimer) { return; }
        if (timerSpec != null) {
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
            isTimer = false;
        }
    }

    /**
     * implements the method of TVTimerWentOffListener.
     *
     * @param e the e
     */
    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (!isTimer) { return; }
        if (feedShowCount != -1) {
            feedShowCount++;
            if (feedShowCount * SLEEP_TIME >= FEED_SHOW_SEC * 1000L) {
                feedShowCount = -1;
                feedImgFocus = -1;
                repaint();
            }
        }
        if (flipbarShowCount != -1) {
            flipbarShowCount++;
            Logger.debug(this, "timerWentOff()-flipbarShowCount : " + flipbarShowCount);
            Logger.debug(this, "timerWentOff()-flipbarlimitCount : " + flipbarlimitCount);
            if (flipbarShowCount * SLEEP_TIME >= flipbarlimitCount * 1000L) {
                flipbarShowCount = -1;
                flipBar.stop();
                Logger.debug(this, "timerWentOff()-flipBar.stop()");
            }
        }
        long playTime = MediaPlayHandler.getInstance().getPlayTimeLong();
        Logger.debug(this, "timerWentOff()-playTime : " + playTime);
        flipBar.setMediaTime(playTime);
    }

    /**
     * Reset flipbar count.
     */
    private void resetFlipbarCount() {
        if (flipbarShowCount != -1) { flipbarShowCount = 0; }
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        resetFlipbarCount();
        switch (keyCode) {
        case Rs.KEY_LEFT:
        case Rs.KEY_RIGHT:
            if (flipBar.isVisible()) {
                flipbarShowCount = 0;
                flipBar.handleKey(keyCode);
                buttonFocus = flipBar.getFocus();
            }
            break;
        case Rs.KEY_UP:
        case Rs.KEY_DOWN:
            break;
        case Rs.KEY_ENTER:
            if (!flipBar.isVisible()) {
                flipbarShowCount = 0;
                flipBar.start();
            } else {
                flipBar.handleKey(keyCode);
                buttonFocus = flipBar.getFocus();
                processPlayKey(keyCode);
            }
            break;
        case KeyCodes.BACK:
        case KeyCodes.PAUSE:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
        case KeyCodes.PLAY:
        case KeyCodes.STOP:
        case KeyCodes.FORWARD:
            processPlayKey(keyCode);
            break;
        case Rs.KEY_BACK:
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            if (flipBar.isVisible()) {
                flipbarShowCount = -1;
                flipBar.stop();
                repaint();
            } else { SceneManager.getInstance().goToScene(previousSceneID, false, null); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Process play key.
     *
     * @param keyCode the key code
     */
    private void processPlayKey(int keyCode) {
        int btnFocus = buttonFocus;
        switch (keyCode) {
        case KeyCodes.BACK:
            btnFocus = BTN_SKIP_BACK;
            break;
        case KeyCodes.REWIND:
            btnFocus = BTN_RW;
            break;
        case KeyCodes.STOP:
            btnFocus = BTN_STOP;
            break;
        case KeyCodes.PAUSE:
            btnFocus = BTN_PAUSE;
            break;
        case KeyCodes.PLAY:
            btnFocus = BTN_PLAY;
            break;
        case KeyCodes.FAST_FWD:
            btnFocus = BTN_FF;
            break;
        case KeyCodes.FORWARD:
            btnFocus = BTN_SKIP_FORWARD;
            break;
        default:
            break;
        }
        if (keyCode != Rs.KEY_ENTER) {
            flipBar.setFocus(btnFocus);
            if (!flipBar.isVisible()) { flipBar.start(); }
        }

        if (btnFocus == BTN_SKIP_BACK) {
            int setTime = (int) MediaPlayHandler.getInstance().getPlayTime() - BACK_SKIP_SEC;
            if (setTime < 0) { setTime = 0; }
            MediaPlayHandler.getInstance().setMediaTime(setTime);
            feedImgFocus = FOCUS_SKIP_BACK;
            feedShowCount = 0;
            repaint();
            flipBar.setMediaTime(MediaPlayHandler.getInstance().getPlayTimeLong());
        } else if (btnFocus == BTN_RW) {
            if (rateFocus == RATE_RW16 || rateFocus >= RATE_PAUSE) {
                rateFocus = RATE_RW2;
            } else { rateFocus--; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
        } else if (btnFocus == BTN_STOP) {
            rateFocus = RATE_PAUSE;
            feedImgFocus = FOCUS_STOP;
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
            feedShowCount = 0;
            repaint();
            videoPopup.setMode(VideoPopup.MODE_STOPPED);
            if (searchContent != null) { videoPopup.setMode(VideoPopup.MODE_STOPPED_SEARCH); }
            videoPopup.setTitle(playContent.getName());
            addPopup(videoPopup);
        } else if (btnFocus == BTN_PAUSE) {
            if (rateFocus == RATE_PAUSE) {
                rateFocus = RATE_PLAY;
            } else { rateFocus = RATE_PAUSE; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
        } else if (btnFocus == BTN_PLAY) {
            if (rateFocus == RATE_PLAY) {
                rateFocus = RATE_SLOW;
            } else { rateFocus = RATE_PLAY; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
        } else if (btnFocus == BTN_FF) {
            if (rateFocus == RATE_FF16 || rateFocus <= RATE_PLAY) {
                rateFocus = RATE_FF2;
            } else { rateFocus++; }
            MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
        } else if (btnFocus == BTN_SKIP_FORWARD) {
            long duration = playContent.getDuration();
            if (duration <= 0) { duration = (int) MediaPlayHandler.getInstance().getDuration(); }
            if (duration > 15) {
                long setTime = (int) MediaPlayHandler.getInstance().getPlayTime() + SKIP_SEC;
                if (setTime > duration) { setTime = duration - 15; }
                MediaPlayHandler.getInstance().setMediaTime(setTime);
                feedImgFocus = FOCUS_SKIP_FORWARD;
                feedShowCount = 0;
                repaint();
                flipBar.setMediaTime(MediaPlayHandler.getInstance().getPlayTimeLong());
            }
        } else if (btnFocus == BTN_SCREEN_SCALE) {
            isFullScreen = !isFullScreen;
            changeResolution();
            feedImgFocus = -1;
            feedShowCount = -1;
        }
        if (rateFocus <= RATE_RW2 || rateFocus >= RATE_FF2) {
            flipbarShowCount = -1;
        } else { flipbarShowCount = 0; }
        if (rateFocus == RATE_PAUSE) {
            flipbarlimitCount = FLIPBAR_SHOW_SEC60;
        } else { flipbarlimitCount = FLIPBAR_SHOW_SEC5; }
        Logger.debug(this, "processPlayKey()-feedImgFocus : " + feedImgFocus);
        repaint();
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(TH_START_PLAY)) {
            Content content = (Content) receivedData;
            Service service = null;
            if (content != null) {
                /*if (searchContent == null) {
                    playContent = content;
                } else {
                    playContent = HNHandler.getInstance().getPlayContent(content.getParentId(),
                            BasicUI.MENU_VIDEO, -1, -1, false, content.getId());
                }*/
                playContent = content;
                service = playContent.getService();
            }
            if (service == null || flipBar == null) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                //showNotiPopup();
                showErrorMessage(ERRMSG_INIT, MainManager.getInstance().getErrorCode(MainManager.ERR_LOST_CONNECTION));
                return;
            }
            if (!checkVisible()) { return; }
            MediaPlayHandler.getInstance().start(service, this);
            if (!checkVisible()) {
                MediaPlayHandler.getInstance().stop(this);
                return;
            }
        }
    }

    /**
     * Show NotiPopup.
     */
    private void showNotiPopup() {
        notiPopup.setMode(BasicUI.MENU_VIDEO);
        addPopup(notiPopup);
    }

    /**
     * Change resolution.
     */
    private void changeResolution() {
        flipBar.setAction(FlipBarAction.SCREEN_SMALL, 7);
        int[] resolution = playContent.getResolution();
        Logger.debug(this, "changeResolution()-resolution : " + resolution);
        if (resolution != null) {
            Logger.debug(this, "changeResolution()-resolution[0] : " + resolution[0]);
            Logger.debug(this, "changeResolution()-resolution[1] : " + resolution[1]);
        }
        if (resolution != null && resolution[0] < 960 && resolution[1] < 540) {
            if (isFullScreen) {
                MediaPlayHandler.getInstance().resizeVideo(0, 0, 960, 540);
            } else {
                int posX = ((960 - resolution[0]) / 2);
                int posY = ((540 - resolution[1]) / 2);
                flipBar.setAction(FlipBarAction.SCREEN_FULL, 7);
                MediaPlayHandler.getInstance().resizeVideo(posX, posY, resolution[0], resolution[1]);
            }
        }
    }

    /**
     * Notify event from MediaPlayHandler with ServiceContextEvent or controllEvent.
     *
     * @param event the event receive from MediaPlayHandler
     */
    public void notifyEvent(Object event) {
        Logger.debug(this, "notifyEvent()-event : " + event);
        if (event instanceof ServiceContextEvent) {
            return;
        } else {
            if (event instanceof BeginningOfContentEvent) {
                Logger.debug(this, "controllerUpdate() : BeginningOfContentEvent");
                processPlayKey(KeyCodes.PLAY);
            } else if (event instanceof EndOfContentEvent) {
                Logger.debug(this, "controllerUpdate() : EndOfContentEvent");
                rateFocus = RATE_PAUSE;
                MediaPlayHandler.getInstance().setPlayRate(PLAY_RATE[rateFocus]);
                feedImgFocus = rateFocus;
                feedShowCount = 0;
                repaint();
                videoPopup.setMode(VideoPopup.MODE_FINISHED);
                if (searchContent != null) { videoPopup.setMode(VideoPopup.MODE_FINISHED_SEARCH); }
                videoPopup.setTitle(playContent.getName());
                addPopup(videoPopup);
            } else if (event instanceof PrefetchCompleteEvent) {
                return;
            } else if (event instanceof StartEvent) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                isFullScreen = true;
                if (PreferenceProxy.VIDEO_SIZE_ACTUAL.equals(PreferenceProxy.getInstance().videoScaling)) {
                    isFullScreen = false;
                }
                changeResolution();
                long duration = playContent.getDurationLong();
                if (duration <= 0) { duration = MediaPlayHandler.getInstance().getDurationLong(); }
                flipBar.setContent(new FlipBarContent(playContent.getName(), 0, duration));
                flipBar.setMediaTime(0);
                flipBar.addListener(this);
                flipBar.setFocus(buttonFocus);
                flipBar.start();
                add(flipBar);
                startTimer();
                repaint();
            } else if (event instanceof ControllerErrorEvent) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                //showNotiPopup();
                showErrorMessage(ERRMSG_INIT, MainManager.getInstance().getErrorCode(MainManager.ERR_LOST_CONNECTION));
            } else if (event instanceof RateChangeEvent) {
                float rate = ((RateChangeEvent) event).getRate();
                if (rate != 0.0f || feedImgFocus != FOCUS_STOP) {
                    for (int i = 0; i < PLAY_RATE.length; i++) {
                        if (rate == PLAY_RATE[i]) {
                            feedImgFocus = i;
                            feedShowCount = 0;
                            repaint();
                            break;
                        }
                    }
                }
            } else if (event instanceof MediaTimeSetEvent) {
                long playTime = MediaPlayHandler.getInstance().getPlayTimeLong();
                Logger.debug(this, "notifyFromMediaPlayHandler()-playTime : " + playTime);
                flipBar.setMediaTime(playTime);
                repaint();
            }
        }
    }

    /**
     * Notify stop from MediaPlayHandler.
     */
    public void notifyStop() { }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            removePopup();
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
        } else if (popup instanceof VideoPopup) {
            int exeMode = ((Integer) para).intValue();
            feedImgFocus = -1;
            removePopup();
            if (exeMode == VideoPopup.EXE_CONTINUE) {
                processPlayKey(KeyCodes.PLAY);
            } else if (exeMode == VideoPopup.EXE_BEGINNING) {
                MediaPlayHandler.getInstance().setMediaTime(0);
                flipBar.setMediaTime(MediaPlayHandler.getInstance().getPlayTimeLong());
                processPlayKey(KeyCodes.PLAY);
            } else if (exeMode == VideoPopup.EXE_LIST || exeMode == VideoPopup.EXE_LIST_SEARCH) {
                SceneManager.getInstance().goToScene(previousSceneID, false, null);
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param action the FlipBarAction
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener
     *      #actionPerformed(com.videotron.tvi.illico.flipbar.FlipBarAction)
     */
    public void actionPerformed(FlipBarAction action) {
        Logger.debug(this, "called actionPerformed()-action name : " + action.getName());
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param time the time
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener#notifyShown(long)
     */
    public void notifyShown(long time) {
        Logger.debug(this, "called notifyShown()-time : " + time);
    }

    /**
     * implements method of FlipBarListener.
     *
     * @param selection the selection
     * @see com.videotron.tvi.illico.flipbar.FlipBarListener#notifyHidden(boolean)
     */
    public void notifyHidden(boolean selection) {
        Logger.debug(this, "called notifyHidden()-selection : " + selection);
    }
}

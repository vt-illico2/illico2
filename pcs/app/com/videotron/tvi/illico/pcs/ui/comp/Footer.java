package com.videotron.tvi.illico.pcs.ui.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;
import java.util.Hashtable;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ui.Scrollable;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.SharedMemory;

public class Footer extends Component {

    public static final short ALIGN_LEFT = 1;
    public static final short ALIGN_RIGHT = 2;

    public static final int DYNAMIC_KEY = 0;
    public static final int STATIC_KEY  = 1;
    public static final int TEXT        = 2;

    private static Color cButton = new Color(241, 241, 241);
    private static Color backColor = new Color(0, 0, 0);
    private static Font fButton = FontResource.BLENDER.getFont(17);

    private static DataCenter dataCenter = DataCenter.getInstance();
    private static final int DIV_WIDTH = 2;
    private static final int GAP = 5;

    private short align;
    private boolean showDivider;
    private int textType;
    private boolean showBackgroundText;

    private Vector images = new Vector();
    private Vector texts = new Vector();

    private Image scrollIcon;
    private Scrollable scroll;

    private int[] p;
    private ClickingEffect clickEffect;

    public Footer() {
        this(ALIGN_RIGHT, true, DYNAMIC_KEY);
    }

    public Footer(short align, boolean showDivider) {
        this(align, showDivider, DYNAMIC_KEY);
    }

    public Footer(short align, boolean showDivider, int textType) {
        this.align = align;
        this.showDivider = showDivider;
        this.textType = textType;
        setForeground(cButton);
        setFont(fButton);
        setVisible(true);
        setBounds(10, 488, 900, 24);

        clickEffect = new ClickingEffect(this, 5);
    }
    
    public void setUpdateBackgroundBeforeStart(boolean updateBgBefStart) {
        if (clickEffect != null) {
            clickEffect.updateBackgroundBeforeStart(updateBgBefStart);
        }
    }

    public void reset() {
    	texts.removeAllElements();
    	images.removeAllElements();
    }

    public Image addButton(String buttonKey, String textKey) {
        Hashtable btnTable = (Hashtable) SharedMemory.getInstance().get("Footer Img");
        if (btnTable != null) {
            Image icon = (Image) btnTable.get(buttonKey);
            addButton(icon, textKey);
            return icon;
        }
        return null;
    }

    public void addButton(Image image, String textKey) {
        if (image == null) {
            return;
        }
        images.addElement(image);
        if (textType == STATIC_KEY) {
            texts.addElement(dataCenter.getString(textKey));
        } else {
            texts.addElement(textKey);
        }
    }

    public void addButtonWithLabel(Image image, String textLbl) {
        images.add(image);
        texts.add(textLbl);
    }

    public void removeButton(String textLbl) {
        if (texts.contains(textLbl)) {
            int idx = texts.indexOf(textLbl);
            texts.remove(idx);
            images.remove(idx);
        }
    }

    public void changeText(Image image, String textKey) {
        int index = images.indexOf(image);
        if (index >= 0) {
            if (textType == STATIC_KEY) {
                texts.setElementAt(dataCenter.getString(textKey), index);
            } else {
                texts.setElementAt(textKey, index);
            }
        }
    }

    public void linkWithScrollTexts(Image icon, Scrollable scrollTexts) {
        scrollIcon = icon;
        scroll = scrollTexts;
    }

    private String getText(int i) {
        switch (textType) {
            case TEXT:
            case STATIC_KEY:
                return (String) texts.elementAt(i);
            case DYNAMIC_KEY:
                return dataCenter.getString((String) texts.elementAt(i));
            default:
                return "";
        }
    }

    public void clickAnimation(String key) {
    	for (int i = 0; i < texts.size(); i++) {
    		String t = (String) texts.get(i);
    		if (t.equals(key) || t.equals(dataCenter.getString(key))) {
    			clickAnimation(i);
    			return;
    		}
    	}
    }

    public void clickAnimation(Image icon) {
        int index = images.indexOf(icon);
        if (index >= 0) {
            clickAnimation(index);
        }
    }

    public void clickAnimation(int idx) {
    	if (p == null) {
        	return;
        }
    	if (idx < 0) {
    		idx += images.size();
    	}
    	int h = ((Image) images.get(idx)).getHeight(null);
    	idx *= 3;
        int x = 0;
    	if (align == ALIGN_RIGHT) {
            x = getWidth() - p[p.length - 1];
        }

    	clickEffect.start(x + p[idx], 0, p[idx+1] - p[idx] - GAP, h);
    }

    public Rectangle getBoundsOfLabel(int idx) {
    	if (idx < 0) {
    		idx += images.size();
    	}
    	int h = ((Image) images.get(idx)).getHeight(null);
    	idx *= 3;
        int x = 0;
    	if (align == ALIGN_RIGHT) {
            x = getWidth() - p[p.length - 1];
        }
    	idx++;
    	return new Rectangle(x + p[idx], 0, p[idx+1] - p[idx] - GAP, h);
    }

    /**
     * @return the showBackgroundText
     */
    public boolean isShowBackgroundText() {
        return showBackgroundText;
    }

    /**
     * @param showBackgroundText the showBackgroundText to set
     */
    public void setShowBackgroundText(boolean showBackgroundText) {
        this.showBackgroundText = showBackgroundText;
    }

    public void paint(Graphics g) {
        int size = texts.size();
        if (size == 0) {
            return;
        }
        g.setColor(getForeground());
        g.setFont(getFont());
        FontMetrics fm = g.getFontMetrics();

        int divWidth = 0;
        if (size > 1) {
            divWidth = DIV_WIDTH;
        }

        boolean skipScroll = (scroll != null && !scroll.hasMultiplePages());

        p = new int[size * 3];
        int k = 0;
        int x = 0;
        String s;
        Image im;
        for (int i = 0; i < size; i++) {
            s = getText(i);
            im = (Image) images.elementAt(i);
            if (skipScroll && im == scrollIcon) {
            	 p[k++] = x;
            	 p[k++] = x;
            	 p[k++] = x;
            	continue;
            }
            p[k++] = x;
            x = x + im.getWidth(this) + GAP;
            p[k++] = x;
            x = x + fm.stringWidth(s) + GAP;
            p[k++] = x;
            x = x + DIV_WIDTH + GAP;
        }
        if (align == ALIGN_RIGHT) {
            x = getWidth() - x + divWidth + GAP;
        } else {
            x = 0;
        }

        k = 0;
        Color cDiv1 = new Color(200, 200, 200, 150);
        Color cDiv2 = new Color(150, 150, 150, 120);
        for (int i = 0; i < size; i++) {
            im = (Image) images.elementAt(i);
            if (skipScroll && im == scrollIcon) {
            	k += 3;
            	continue;
            }
            g.drawImage(im, x + p[k++], 0, this);
            int posText = x + p[k++];
            if (showBackgroundText) {
                g.setColor(backColor);
                g.drawString(getText(i), posText + 1, 15 + 1);
            }
            g.setColor(getForeground());
            g.drawString(getText(i), posText, 15);
            if (showDivider && i < size - 1) {
            	g.setColor(cDiv1);
            	int div = x + p[k++];
            	g.drawLine(div, 4, div, 16);
            	div++;
            	g.setColor(cDiv2);
            	g.drawLine(div, 4, div, 16);
            } else {
                k++;
            }
        }
    }
}

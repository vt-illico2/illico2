/*
 *  @(#)ScreenSaver.java 1.0 2011.09.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.comp;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.PhotoPlayUI;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.pcs.ui.popup.Popup;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ScreenSaver</code> the ui for screensaver.
 *
 * @since   2011.09.19
 * @version $Revision: 1.8 $ $Date: 2012/09/18 02:43:37 $
 * @author  tklee
 */
public class ScreenSaver extends Popup implements TVTimerWentOffListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_MUSIC. */
    private static final int MODE_MUSIC = 0;
    /** The Constant MODE_GALAXIE. */
    private static final int MODE_GALAXIE = 1;
    /** The Constant MODE_PHOTO. */
    private static final int MODE_PHOTO = 2;
    /** The Constant SLEEP_TIME. */
    private static final long SLEEP_TIME = 1000L;
    /** The Constant MUSIC_CHANGE_COUNT. */
    private static final int MUSIC_CHANGE_COUNT = 5;

    /** The timer spec. */
    private TVTimerSpec timerSpec = null;
    /** The photo list. */
    //private Vector photoList = null;
    private Content photoRoot = null;
    /** The imgae label. */
    private ImageLabel imgaeLabel = null;
    /** The mode to identify. */
    private int mode = 0;
    /** The is timer. */
    private boolean isTimer = false;
    /** The timer count. */
    private int timerCount = 0;
    /** The trans position. */
    private int[] transPosition = new int[2];
    /** The photo URL. */
    private String photoUrl = null;
    /** The photo count. */
    private int photoCount = 0;
    /** The flip option. */
    private int flipOption = ImageLabel.NONE_EFFECT;

    /**
     * Initialize the Component.
     */
    public ScreenSaver() {
        setBounds(0, 0, 960, 540);
        setRenderer(new ScreensaverRenderer());
        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME);
        timerSpec.setRepeat(true);
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        timerCount = 0;
        flipOption = ImageLabel.NONE_EFFECT;
        String screensaver = PreferenceProxy.getInstance().musicScreensaver;
        if (screensaver.equals(PreferenceProxy.SCREENSAVER_ALBUM)) {
            int musicType = BasicUI.musicPlayerPopup.playMusicType;
            mode = MODE_MUSIC;
            if (musicType == MusicPlayerPopup.PLAY_GALAXIE) { mode = MODE_GALAXIE; }
        } else {
            mode = MODE_PHOTO;
            photoRoot = BasicUI.contentRoot[BasicUI.MENU_PHOTO];
        }
        setTranslationPosition();
        prepare();
        if (mode == MODE_PHOTO) { showNextImage(0); }
        startTimer();
    }

    /**
     * called when popup is finished.
     */
    public void stop() {
        stopTimer();
        //photoList = null;
        photoRoot = null;
        flushImage(photoUrl);
        photoUrl = null;
        //photoContent = null;
        if (imgaeLabel != null) { remove(imgaeLabel); }
        imgaeLabel = null;
        transPosition[0] = 0;
        transPosition[1] = 0;
    }

    /**
     * Sets the translation position.
     */
    private void setTranslationPosition() {
        if (mode == MODE_MUSIC) {
            transPosition[0] = (int) (Math.random() * 383);
            transPosition[1] = (int) (Math.random() * 296);
        } else if (mode == MODE_GALAXIE) {
            transPosition[0] = (int) (Math.random() * 544) - (291 - 50);
            transPosition[1] = (int) (Math.random() * 366) - (156 - 53);
        }
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        if (isTimer) { return; }
        try {
            timerSpec.addTVTimerWentOffListener(this);
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            isTimer = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        if (!isTimer) { return; }
        if (timerSpec != null) {
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
            isTimer = false;
        }
    }

    /**
     * implements the method of TVTimerWentOffListener.
     *
     * @param e the TVTimerWentOffEvent
     */
    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (!isTimer) { return; }
        timerCount++;
        if (timerCount >= MUSIC_CHANGE_COUNT) {
            timerCount = 0;
            if (mode == MODE_MUSIC || mode == MODE_GALAXIE) {
                setTranslationPosition();
                repaint();
            } else {
                if (photoRoot != null) {
                    showNextImage((int) (Math.random() * photoRoot.getTotalCount()));
                }
            }
        }
    }

    /**
     * Show next image using Thread.
     *
     * @param focus the focus
     */
    private void showNextImage(final int focus) {
        Logger.debug(this, "showNextImage-focus : " + focus);
        if (photoRoot == null) { return; }
        photoCount = photoRoot.getTotalCount();
        if (photoCount < 1) {
            flushImage(photoUrl);
            photoUrl = null;
            if (imgaeLabel != null) { remove(imgaeLabel); }
            imgaeLabel = null;
            repaint();
            return;
        }
        new Thread("ScreenSaver|showNextImage()") {
            public void run() {
                if (photoRoot == null) { return; }
                String url = HNHandler.getInstance().
                    getPhotoUrlForScreensaver(photoRoot.getAllEntryId(), focus);
                Logger.debug(this, "showNextImage-url : " + url);
                timerCount = 0;
                if (photoRoot == null) { return; }
                if (url != null) {
                    final Image img =
                        BasicUI.conImgPool.getImage("ScreenSaver" + url, null, PhotoPlayUI.SIZE_FULL, url);
                    BasicUI.conImgPool.waitForAll();
                    timerCount = 0;
                    if (photoRoot == null) {
                        flushImage(url);
                        return;
                    }
                    final String oldUrl = photoUrl;
                    photoUrl = url;
                    if (imgaeLabel == null) {
                        imgaeLabel = new ImageLabel(img);
                        imgaeLabel.setSize(960, 540);
                        imgaeLabel.setAlign(true);
                        add(imgaeLabel);
                        repaint();
                    } else {
                        if (img != null) {
                            int effectType = ImageLabel.NONE_EFFECT;
                            String slideshowEffect = PreferenceProxy.getInstance().slideshowEffect;
                            if (slideshowEffect != null && !slideshowEffect.equals(PreferenceProxy.EFFECT_NO)) {
                                if (slideshowEffect.equals(PreferenceProxy.EFFECT_RANDOM)) {
                                    effectType = (int) (Math.random() * 6) + 1;
                                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_FADE)) {
                                    effectType = ImageLabel.FADE_EFFECT;
                                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_DISSOLVE)) {
                                    effectType = ImageLabel.DISSOLVE_EFFECT;
                                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_PAGEFLIP)) {
                                    if (flipOption >= ImageLabel.FLIP_FROM_TOP
                                            && flipOption < ImageLabel.FLIP_FROM_RIGHT) {
                                        flipOption++;
                                    } else { flipOption = ImageLabel.FLIP_FROM_TOP; }
                                    effectType = flipOption;
                                }
                            }
                            final int replaceType = effectType;
                            EventQueue.invokeLater(new Runnable() {
                                public void run() {
                                    timerCount = 0;
                                    imgaeLabel.replaceImage(img, replaceType);
                                    repaint();
                                    if (!oldUrl.equals(photoUrl)) { flushImage(oldUrl); }
                                    timerCount = 0;
                                }
                            });
                            return;
                        } else { repaint(); }
                    }
                    if (!oldUrl.equals(photoUrl)) { flushImage(oldUrl); }
                    timerCount = 0;
                    if (photoRoot == null) {
                        flushImage(oldUrl);
                        flushImage(photoUrl);
                        flushImage(url);
                        return;
                    }
                }
            }
        } .start();
    }

    /**
     * Flush image.
     *
     * @param url the url for photo
     */
    private void flushImage(String url) {
        if (url == null) { return; }
        BasicUI.conImgPool.remove("ScreenSaver" + url);
    }

    /**
     * <code>ScreensaverRenderer</code> The class to paint a GUI.
     *
     * @since   2011.09.19
     * @version $Revision: 1.8 $ $Date: 2012/09/18 02:43:37 $
     * @author  tklee
     */
    public class ScreensaverRenderer extends BasicPopupRenderer {
        /** The music default img. */
        private Image musicDefaultImg = null;
        /** The music glow img. */
        private Image musicGlowImg = null;
        /** The galaxie logo img. */
        private Image galaxieLogoImg = null;
        /** The photo default img. */
        private Image photoDefaultImg = null;

        /** The music items. */
        public String[] musicItems = new String[3];
        /** The galaxie items. */
        public String[] galaxieItems = new String[2];

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            musicDefaultImg = getImage("09_m_d.png");
            musicGlowImg = getImage("09_mp_glow.png");
            galaxieLogoImg = getImage("g_logo.png");
            photoDefaultImg = getImage("04_default_screensaver.JPG");
            super.prepare(c);

            musicItems[0] = getString("title");
            musicItems[1] = getString("artist");
            musicItems[2] = getString("albumTitle");
            galaxieItems[0] = getString("title") + ":";
            galaxieItems[1] = getString("next") + ":";
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            if (mode == MODE_MUSIC) {
                g.setColor(Rs.C0);
                g.fillRect(0, 0, 960, 540);
                final int[] transPos = transPosition;
                g.translate(transPos[0], transPos[1]);
                g.drawImage(musicDefaultImg, 50, 53, c);
                Image albumImg = BasicUI.musicPlayerPopup.albumImg;
                if (albumImg != null) {
                    try {
                        int imgWidth = albumImg.getWidth(null);
                        int imgHeight = albumImg.getHeight(null);
                        int posX = 71 + ((133 - imgWidth) / 2);
                        int posY = 53 + ((133 - imgHeight) / 2);
                        g.drawImage(albumImg, posX, posY, c);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                g.drawImage(musicGlowImg, 71, 53, c);
                Vector playMusicList = BasicUI.musicPlayerPopup.playMusicList;
                if (playMusicList != null) {
                    int playMusicFocus = BasicUI.musicPlayerPopup.playMusicFocus;
                    Content focusContent = (Content) playMusicList.elementAt(playMusicFocus);
                    String[] infos = {focusContent.getName(), focusContent.getArtist(), focusContent.getAlbumName()};
                    for (int i = 0; i < musicItems.length; i++) {
                        int addPos = i * 20;
                        g.setFont(Rs.F16);
                        g.setColor(Rs.C130);
                        g.drawString(musicItems[i], 235, 101 + addPos);
                        if (infos[i] != null) {
                            g.setFont(Rs.F18);
                            g.setColor(Rs.C255);
                            g.drawString(TextUtil.shorten(infos[i], g.getFontMetrics(), 885 - 712), 329, 101 + addPos);
                        }
                    }
                }
                g.translate(-transPos[0], -transPos[1]);
            } else if (mode == MODE_GALAXIE) {
                g.setColor(Rs.C0211011013);
                g.fillRect(0, 0, 960, 540);
                final int[] transPos = transPosition;
                g.translate(transPos[0], transPos[1]);
                g.drawImage(galaxieLogoImg, 291, 166, c);
                GalaxieChannelData galaxieData = BasicUI.musicPlayerPopup.galaxieChData;
                if (galaxieData != null) {
                    String[] infos = new String[2];
                    try {
                        infos[0] = galaxieData.getCurrentTitle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        infos[1] = galaxieData.getNextTitle();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    g.setFont(Rs.F16);
                    int startTitlePos = 0;
                    for (int i = 0; i < galaxieItems.length; i++) {
                        int width = g.getFontMetrics().stringWidth(galaxieItems[i]);
                        if (width > startTitlePos) { startTitlePos = width; }
                    }
                    startTitlePos += (337 + 10);
                    for (int i = 0; i < galaxieItems.length; i++) {
                        int addPos = i * 39;
                        g.setFont(Rs.F16);
                        g.setColor(Rs.C228026034);
                        g.drawString(galaxieItems[i], 337, 172 + addPos);
                        if (infos[i] != null) {
                            g.setFont(Rs.F24);
                            if (i == 0) {
                                g.setColor(Rs.C255);
                            } else { g.setColor(Rs.C167); }
                            g.drawString(TextUtil.shorten(infos[i], g.getFontMetrics(), 592 - startTitlePos),
                                    startTitlePos, 174 + addPos);
                        }
                    }
                }
                g.translate(-transPos[0], -transPos[1]);
            } else {
                if (photoCount < 1) {
                    //Logger.debug(this, "photoDefaultImg : " + photoDefaultImg);
                    g.drawImage(photoDefaultImg, 0, 0, c);
                } else {
                    g.setColor(Rs.C0);
                    g.fillRect(0, 0, 960, 540);
                }
            }
        }
    }
}

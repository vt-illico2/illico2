/*
 *  @(#)InitUI.java 1.0 2011.05.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.InbandManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.communication.VbmController;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.gui.InitRenderer;
import com.videotron.tvi.illico.pcs.ui.popup.DeviceSelectPopup;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>InitUI</code> This class show the information of initialization.
 *
 * @since   2011.05.18
 * @version $Revision: 1.19 $ $Date: 2012/09/18 02:44:09 $
 * @author  tklee
 */
public class InitUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant TH_DEVICE_SEARCH. */
    private static final String TH_DEVICE_SEARCH = "searchDevices";
    /** The Constant TH_DEVICE_CONNECT. */
    private static final String TH_DEVICE_CONNECT = "connectDevices";

    /** The Constant TYPE_INIT. */
    public static final int UI_INIT = -1;
    /** The Constant TYPE_NO_SUBSCRIBED. */
    public static final int UI_NO_SUBSCRIBED = 0;
    /** The Constant UI_DEVICE_SEARCH. */
    public static final int UI_DEVICE_SEARCH = 1;
    /** The Constant UI_NO_SEARCH. */
    public static final int UI_NO_SEARCH = 2;
    /** The Constant UI_DEVICE_CONNECTING. */
    public static final int UI_DEVICE_CONNECTING = 3;
    /** The Constant UI_DEVICE_CONNECTED. */
    public static final int UI_DEVICE_CONNECTED = 4;
    /** The Constant UI_NO_CONNECT. */
    public static final int UI_NO_CONNECT = 5;

    /** The Constant BTN_RETRY. */
    private static final int BTN_RETRY = 0;
    /** The Constant BTN_CANCEL. */
    private static final int BTN_CANCEL = 1;

    /** The InitRenderer instance. */
    private InitRenderer renderer = null;
    /** The connect device. */
    public DeviceInfo connectDevice = null;
    /** The search thread. */
    private SearchThread searchThread = null;
    /** The teaser image. */
    public Image teaserImage = null;

    /** The ui type. */
    public int uiType = UI_INIT;
    /** The search img num. */
    public int searchImgNum = 0;
    /** The btn focus. */
    public int btnFocus = 0;
    /** The is search thread. */
    //private boolean isSearchThread = false;
    /** The whether search is completed. */
    private boolean isSearchCompleted = false;

    /**
     * Initialize the Scene.
     */
    public InitUI() {
        renderer = new InitRenderer();
        setRenderer(renderer);
        searchThread = new SearchThread();
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();

        //isSearchThread = false;
        searchImgNum = 0;
        btnFocus = 0;
        super.start(isReset, sendData);
        devSelectPop.setMode(DeviceSelectPopup.MODE_CONNECTION);
        if (isReset) {
            contentRoot = null;
            uiType = UI_INIT;
            isSearchCompleted = false;
            processInit();
        } else {
            previousSceneID = ((Integer) sendData[0]).intValue();
            if (receivedData == null) { //change device
                devSelectPop.setMode(DeviceSelectPopup.MODE_CONNECTION_CHANGE);
                uiType = UI_DEVICE_SEARCH;
                startDeviceSearching();
            } else { //disconnected device
                initDataForConnectedDevice();
                connectDevice = (DeviceInfo) receivedData;
                uiType = UI_NO_CONNECT;
            }
        }
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        renderer.stop();
        //isSearchThread = false;
        searchThread.destory();
        connectDevice = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        teaserImage = null;
        renderer.stop();
        connectDevice = null;
        deviceInfos = null;
        searchThread.destory();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (uiType == UI_NO_SEARCH || uiType == UI_NO_CONNECT) {
                if (btnFocus > 0) {
                    btnFocus--;
                    repaint();
                }
            }
            break;
        case Rs.KEY_RIGHT:
            if (uiType == UI_NO_SEARCH || uiType == UI_NO_CONNECT) {
                int size = renderer.noDevBtnTxt.length;
                if (uiType == UI_NO_CONNECT) { size = renderer.noConnectBtnTxt.length; }
                if (btnFocus < size - 1) {
                    btnFocus++;
                    repaint();
                }
            }
            break;
        case Rs.KEY_ENTER:
            if (uiType == UI_NO_SUBSCRIBED) {
                clickEffect.start(483, 388, 638 - 483, 32);
                SceneManager.getInstance().requestStartMenuApp();
            } else if (uiType == UI_NO_SEARCH) {
                clickEffect.start(320 + (btnFocus * 164), 451, 638 - 483, 32);
                if (btnFocus == BTN_RETRY) {
                    startDeviceSearching();
                } else { SceneManager.getInstance().requestStartMenuApp(); }
            } else if (uiType == UI_DEVICE_CONNECTING) {
                clickEffect.start(400, 441, 638 - 483, 32);
                //isSearchThread = false;
                searchThread.stop();
                if (deviceInfos.size() < 2
                        || connectDevice.getDeviceName().equals(PreferenceProxy.getInstance().favorUDN))
                {
                    SceneManager.getInstance().requestStartMenuApp();
                } else {
                    uiType = UI_DEVICE_SEARCH;
                    searchImgNum = 0;
                    //isSearchThread = true;
                    searchImgNum = 0;
                    devSelectPop.setData(deviceInfos);
                    devSelectPop.resetFocus();
                    addPopup(devSelectPop);
                }
            } else if (uiType == UI_NO_CONNECT) {
                clickEffect.start(235 + (btnFocus * 164), 451, 638 - 483, 32);
                if (btnFocus == BTN_RETRY) {
                    startDeviceConnecting();
                } else if (btnFocus == BTN_CANCEL) {
                    SceneManager.getInstance().requestStartMenuApp();
                } else { startDeviceSearching(); }
            }
            break;
        case Rs.KEY_BACK:
        case KeyCodes.FORWARD:
        case KeyCodes.BACK:
        case KeyCodes.PAUSE:
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Process initialzation.
     */
    private void processInit() {
        Logger.info(this, "called processInit()");
        CommunicationManager.getInstance().hideLoadingAnimation();
        boolean isSubscribed = CommunicationManager.getInstance().checkAppAuthorization();
        if (Environment.EMULATOR) { isSubscribed = true; }
        Logger.debug(this, "processInit()-isSubscribed : " + isSubscribed);
        if (!isSubscribed) {
            Logger.debug(this, "processInit()-isSubscribed : " + isSubscribed);
            String teaserUrl = null;
            if (Config.language.equals(Definitions.LANGUAGE_FRENCH)) {
                teaserUrl = InbandManager.teaserUrlFr;
            } else { teaserUrl = InbandManager.teaserUrlEn; }
            Logger.debug(this, "processInit()-teaserUrl : " + teaserUrl);
            final String imageUrl = teaserUrl;
            if (imageUrl != null) {
                CommunicationManager.getInstance().showLoadingAnimation();
                new Thread("InitUI|getTeaserImage()") {
                    public void run() {
                        try {
                            byte[] teaserData = URLRequestor.getBytes(imageUrl, null);
                            if (teaserData != null) {
                                teaserImage =
                                    FrameworkMain.getInstance().getImagePool().createImage(teaserData, imageUrl);
                                FrameworkMain.getInstance().getImagePool().waitForAll();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        CommunicationManager.getInstance().hideLoadingAnimation();
                        uiType = UI_NO_SUBSCRIBED;
                        repaint();
                    }
                } .start();
                return;
            }
            uiType = UI_NO_SUBSCRIBED;
            repaint();
        } else { startDeviceSearching(); }
    }

    /**
     * Start searching devices.
     */
    private void startDeviceSearching() {
        uiType = UI_DEVICE_SEARCH;
        searchImgNum = 0;
        repaint();
        //isSearchThread = true;
        //startThread();
        searchThread.start();
        loadData(new Object[]{TH_DEVICE_SEARCH}, false);
    }

    /**
     * Start device connecting.
     */
    private void startDeviceConnecting() {
        uiType = UI_DEVICE_CONNECTING;
        searchImgNum = 0;
        repaint();
        //isSearchThread = true;
        //startThread();
        searchThread.start();
        loadData(new Object[]{TH_DEVICE_CONNECT}, false);
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(TH_DEVICE_SEARCH)) {
            connectDevice = null;
            try {
                Thread.sleep(50L);
            } catch (Exception e) {
                e.printStackTrace();
            }
            long startTime = System.currentTimeMillis();
            deviceInfos = HNHandler.getInstance().getDeviceInfos();
            long endTime = System.currentTimeMillis();
            if (!checkVisible()) { return; }
            //if (Environment.EMULATOR) {
            long sleepTime = 1000L - (endTime - startTime);
            if (sleepTime > 0) {
                try {
                    Thread.sleep(sleepTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //isSearchThread = false;
            searchThread.stop();
            if (deviceInfos != null) { Logger.debug(this, "deviceInfos : " + deviceInfos.size()); }
            if (deviceInfos == null || deviceInfos.size() == 0) {
                uiType = UI_NO_SEARCH;
                btnFocus = 0;
                repaint();
            } else {
                String favorUDN = PreferenceProxy.getInstance().favorUDN;
                Logger.debug(this, "favorUDN : " + favorUDN);
                if (!isSearchCompleted && deviceInfos.size() == 1) {
                    connectDevice = (DeviceInfo) deviceInfos.elementAt(0);
                    startDeviceConnecting();
                } else {
                    if (!isSearchCompleted && favorUDN != null && favorUDN.length() > 0) {
                        for (int i = 0; i < deviceInfos.size(); i++) {
                            DeviceInfo devInfo = (DeviceInfo) deviceInfos.elementAt(i);
                            if (favorUDN.equals(devInfo.getUDN())) {
                                connectDevice = devInfo;
                                startDeviceConnecting();
                                isSearchCompleted = true;
                                return;
                            }
                        }
                    }
                    Logger.debug(this, "deviceInfos : " + deviceInfos.size());
                    searchImgNum = 0;
                    //devSelectPop.setMode(DeviceSelectPopup.MODE_CONNECTION);
                    devSelectPop.setData(deviceInfos);
                    devSelectPop.resetFocus();
                    addPopup(devSelectPop);
                }
                isSearchCompleted = true;
            }
        } else if (procId.equals(TH_DEVICE_CONNECT)) {
            contentRoot = HNHandler.getInstance().getContentRoot(connectDevice.getDevice());
            if (Environment.EMULATOR) {
                try {
                    Thread.sleep(3000L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //isSearchThread = false;
            searchThread.stop();
            if (!checkVisible()) { return; }
            if (uiType != UI_DEVICE_CONNECTING) { return; }
            if (contentRoot != null) {
                uiType = UI_DEVICE_CONNECTED;
                repaint();
                //startThread();
                searchThread.start();
                int numberOfFiles = 0;
                for (int i = 0; i < contentRoot.length; i++) {
                    numberOfFiles += contentRoot[i].getTotalCount();
                }
                VbmController.getInstance().writeDeviceLog(connectDevice.getDeviceName(),
                        connectDevice.getDeviceVersion(), numberOfFiles);
            } else {
                uiType = UI_NO_CONNECT;
                btnFocus = 0;
                repaint();
            }
        }
    }

    /**
     * Inits the data for connected device.
     */
    private void initDataForConnectedDevice() {
        HomeUI.tempDevice = null;
        if (musicFooterComp != null) { musicFooterComp.stop(); }
        conImgPool.clear();
        contentRoot = null;
        musicRoot = null;
        //artistSongs.clear();
        musicPlayerPopup.dispose();
        searchPopup.dispose();
        clearAllTempData();
        previousSceneID = SceneManager.SC_INVALID;
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof DeviceSelectPopup) {
            connectDevice = (DeviceInfo) para;
            removePopup();
            if (connectDevice != null) {
                if (previousSceneID != SceneManager.SC_INVALID) { initDataForConnectedDevice(); }
                startDeviceConnecting();
            } else {
                if (previousSceneID == SceneManager.SC_INVALID) {
                    SceneManager.getInstance().requestStartMenuApp();
                } else { SceneManager.getInstance().goToScene(previousSceneID, false, null); }
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * <code>SearchThread</code> This class process time for Searching.
     *
     * @since   2011.07.15
     * @version 1.0, 2010.07.15
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class SearchThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = new ArrayList();
        /** The is start. */
        private boolean isStart = false;

        /**
         * Start thread.
         */
        public void start() {
            Logger.debug(this, "called start()");
            if (thread == null) {
                thread = new Thread(this, "InitUI-SearchThread");
                thread.start();
            }

            isStart = true;
            synchronized (queue) {
                queue.notify();
                Logger.debug(this, "start() - request notify");
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            Logger.debug(this, "called stop()");
            isStart = false;
        }

        /**
         * destory Thread.
         */
        public void destory() {
            thread = null;
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                try {
                    synchronized (queue) {
                        if (!isStart) {
                            Logger.debug(this, "run() - wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null) { break; }
                Logger.debug(this, "run() - isStart : " + isStart);
                int count = 0;
                while (isStart && thread != null) {
                    try {
                        Thread.sleep(300);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Logger.debug(this, "run() - isStart : " + isStart);
                    if (thread == null || !isStart) { break; }
                    if (uiType == UI_DEVICE_CONNECTED) {
                        if (++count == 3) {
                            //isSearchThread = false;
                            isStart = false;
                            //Object[] values = {getCurrentSceneId(), connectDevice, null};
                            SceneManager.getInstance().goToScene(SceneManager.SC_HOME, true, connectDevice);
                            break;
                        }
                    } else {
                        searchImgNum++;
                        if (searchImgNum > renderer.searchAniImage.length - 1) { searchImgNum = 0; }
                        repaint();
                    }
                }
            }
            isStart = false;
        }
    }
}

/*
 *  @(#)ContentListUI.java 1.0 2011.05.30
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.gui.ContentListRenderer;
import com.videotron.tvi.illico.pcs.ui.comp.AutoScroller;
import com.videotron.tvi.illico.pcs.ui.comp.LogDisplayer;
import com.videotron.tvi.illico.pcs.ui.popup.NotiPopup;
import com.videotron.tvi.illico.pcs.ui.popup.SearchPopup;
import com.videotron.tvi.illico.pcs.ui.popup.ViewChangePopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>ContentListUI</code> This class show the list page for content.
 *
 * @since   2011.05.30
 * @version $Revision: 1.41 $ $Date: 2012/10/18 11:29:56 $
 * @author  tklee
 */
public class ContentListUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant TH_CONTENT_LIST. */
    private static final String TH_CONTENT_LIST = "contentList";

    /** The Constant OPT_GOTO. */
    protected static final MenuItem OPT_GOTO =
            new MenuItem("renderer.goTo", new MenuItem[]{OPT_PHOTO, OPT_MUSIC, OPT_VIDEO});
    /** The Constant OPT_SORT. */
    protected static final MenuItem OPT_SORT = new MenuItem("renderer.sorting",
            //new MenuItem[]{OPT_SORT_ALPHA_A, OPT_SORT_ALPHA_Z, OPT_SORT_NEWEST, OPT_SORT_OLDEST});
            new MenuItem[]{OPT_SORT_ALPHA_A, OPT_SORT_OLDEST});
    /** The Constant SLEEP_TIME. */
    private static final long SLEEP_TIME = 1000L;

    /** The ContentListRenderer instance. */
    private ContentListRenderer renderer = null;
    /** The view change popup. */
    private ViewChangePopup viewChangePopup = null;
    /** The list ticker. */
    private AutoScroller listTicker = null;
    /** The folder thread. */
    private Thread folderThread = null;

    /** The current node. */
    public Content currentNode = null;
    /** The temp node. */
    private Content tempNode = null;
    /** The history stack. */
    private Stack historyStack = null;
    /** The photo focus. */
    public static int photoFocus = 0;
    /** The focus position. */
    public int focusPosition = 0;
    /** The temp photo focus. */
    private int tempPhotoFocus = 0;
    /** The temp focus position. */
    private int tempFocusPosition = 0;
    /** The temp menu focus. */
    private int tempMenuFocus = 0;
    /** The temp sort type. */
    private int tempSortType = 0;
    /** The temp ui type. */
    private int tempUIType = 0;
    /** The temp entry id. */
    private String tempEntryId = null;
    /** The ui type. */
    public int uiType = 0;
    /** The menu focus. */
    public int menuFocus = 0;
    /** whether must request to browse on not. */
    private boolean isNewBrowse = true;
    /** The is enter key for list. */
    private boolean isEnterKeyForList = false;

    /**
     * Initialize the Scene.
     */
    public ContentListUI() {
        renderer = new ContentListRenderer();
        setRenderer(renderer);
        rootMenuItem = new MenuItem("D-Option",
                new MenuItem[] {OPT_SORT, OPT_GOTO, OPT_PREFERENCE, OPT_CHANGE_DEV, OPT_HELP});
        historyStack = new Stack();
        viewChangePopup = new ViewChangePopup();
        listTicker = new AutoScroller();
        listTicker.setSleepTime(Config.autoscrollSleepSec * 1000L);
        clickEffect = new ClickingEffect(this, 5);
        sortType = Content.SORT_NEWEST;
        pagingDataThread = new PagingThread();
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        Logger.debug(this, "called start()");
        prepare();
        super.start(isReset, sendData);
        isNewBrowse = false;
        isEnterKeyForList = false;
        if (isReset) {
            photoFocus = 0;
            focusPosition = 0;
            menuFocus = ((Integer) receivedData).intValue();
            if (menuFocus == MENU_PHOTO) {
                uiType = UI_WALL;
                uiType = PreferenceProxy.getInstance().photoUiType;
            } else {
                uiType = UI_LIST;
                uiType = PreferenceProxy.getInstance().videoUiType;
            }
            resetRootChildren();
            sortType = getSortType(menuFocus);
            tempNode = contentRoot[menuFocus];
            renderer.resetLocationTitle(menuFocus);
            historyStack.clear();
            isNewBrowse = true;
        } else {
            Content reqNode = (Content) receivedData;
            if (reqNode != null) {
                historyStack.push(new int[]{photoFocus, focusPosition, sortType});
                tempNode = reqNode;
                sortType = reqNode.getChildSort();
            }
            if (tempNode != null) {
                photoFocus = tempNode.getLastFocus();
                focusPosition = resetPosition(tempNode, uiType, photoFocus);
            }
        }
        changeNode(photoFocus, focusPosition, tempNode, menuFocus, sortType, uiType);
    }

    /**
     * Sets the sort type.
     *
     * @param menuIndex the focus of current menu
     * @return the sort type
     */
    private int getSortType(int menuIndex) {
        return Content.SORT_ALPHA_A;
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        footer.addButtonWithLabel(BasicRenderer.btnSearchImg, BasicRenderer.btnSearchTxt);
        footer.addButtonWithLabel(BasicRenderer.btnPageImg, BasicRenderer.btnPageTxt);
        footer.addButtonWithLabel(BasicRenderer.btnAImg, BasicRenderer.btnViewChangeTxt);
        footer.addButtonWithLabel(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
        footer.addButtonWithLabel(BasicRenderer.btnDImg, BasicRenderer.btnOptTxt);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        stopTimer();
        renderer.stop();
        listTicker.stop();
        remove(listTicker);
        tempNode = currentNode;
        flushPhotoImgs(currentNode);
        currentNode = null;
        if (tempNode != null) { tempNode.setChildren(null); }
        conImgPool.clear();
        isEnterKeyForList = false;
    }

    /**
     * Clear temp data.
     */
    public void clearTempData() {
        tempNode = null;
        searchContent = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
        historyStack.clear();
        tempNode = null;
        currentNode = null;
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        final int focus = photoFocus;
        final Content reqNode = currentNode;
        folderThread = new Thread("ContentListUI|startTimer()") {
            public void run() {
                Thread startThread = folderThread;
                while (folderThread != null && startThread == folderThread
                        && focus == photoFocus && reqNode == currentNode) {
                    try {
                        Thread.sleep(SLEEP_TIME * Config.folderRotationSec);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (folderThread == null || startThread != folderThread) { break; }
                    if (focus != photoFocus || reqNode != currentNode) { break; }
                    timerWentOff(reqNode, focus);
                }
            }
        };
        folderThread.start();
    }

    /**
     * Start timer for folder.
     */
    private void startTimerForFolder() {
        stopTimer();
        Content focusNode = null;
        Vector list = currentNode.getChildren();
        if (list != null) { focusNode = (Content) list.elementAt(photoFocus); }
        Logger.debug(this, "startTimerForFolder()-focusNode : " + focusNode);
        if (focusNode != null && focusNode.isFolder()) { startTimer(); }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        folderThread = null;
    }

    /**
     * execute the processing for folder.
     *
     * @param reqNode the req node
     * @param focus the focus
     */
    private void timerWentOff(Content reqNode, int focus) {
        Vector children = reqNode.getChildren();
        if (children == null) { return; }
        Content focusNode = (Content) children.elementAt(focus);
        if (focusNode != null && focusNode.isFolder()) {
            int urlFocus = focusNode.getPhotoIndexInFolder();
            int oldUrlFocus = 0;
            Vector urlList = focusNode.getIconUrlsInFolder();
            if (urlList == null) {
                focusNode.setPhotoIndexInFolder(0);
            } else {
                oldUrlFocus = urlFocus;
            }
            if (urlList == null || (urlList.size() > 0 && urlList.size() - 1 == urlFocus % Config.loadDataNumber
                    && (urlList.size() == Config.loadDataNumber || urlFocus >= Config.loadDataNumber))) {
                int startIndex = 0;
                if (urlList != null && urlList.size() == Config.loadDataNumber) { startIndex = urlFocus + 1; }
                Vector list = HNHandler.getInstance().requestSearchPhotoUrlList(focusNode.getId(),
                        getSortType(menuFocus), menuFocus, startIndex);
                if (focus != photoFocus || reqNode != currentNode) { return; }
                if (list != null && list.size() > 0) {
                    focusNode.setIconUrlsInFolder(list);
                    urlFocus = startIndex;
                    if (urlList == null && list.size() > 1) {
                        urlFocus = 1;
                    }
                } else if (urlList != null) {
                    if (urlFocus < Config.loadDataNumber) {
                        urlFocus = 0;
                    } else {
                        startIndex = 0;
                        list = HNHandler.getInstance().requestSearchPhotoUrlList(focusNode.getId(),
                                getSortType(menuFocus), menuFocus, startIndex);
                        if (list != null && list.size() > 0) {
                            focusNode.setIconUrlsInFolder(list);
                            urlFocus = startIndex;
                        } else {
                            urlFocus = urlFocus - urlList.size() + 1;
                        }
                    }
                }
            } else if (urlList.size() > 1) {
                if (++urlFocus == urlList.size()) { urlFocus = 0; }
            }
            focusNode.setPhotoIndexInFolder(urlFocus);
            urlList = focusNode.getIconUrlsInFolder();
            if (urlList != null && urlList.size() > 0 && urlFocus != oldUrlFocus) {
                Logger.debug(this, "timerWentOff()-urlList.size() : " + urlList.size());
                int[] size = SIZE_LIST_FOLDER;
                if (uiType == UI_WALL) {
                    size = SIZE_WALL_FOLDER;
                }
                String oldIconUrl = focusNode.getIconUrl();
                focusNode.setIconUrl((String) urlList.elementAt(urlFocus % Config.loadDataNumber));
                String key = focusNode.getId() + focusNode.getIconUrl();
                Image img = conImgPool.getImage(key);
                if (img != null) {
                    focusNode.setIconImage(img);
                } else {
                    if (focus != photoFocus || reqNode != currentNode) { return; }
                    img = ContentListUI.this.getContentImage(key, focusNode.getIconUrl(), null, size);
                    conImgPool.waitForAll();
                    focusNode.setIconImage(img);
                }
                if (!checkVisible()) { return; }
                if (focus == photoFocus || reqNode != currentNode) { repaint(); }
                if (oldIconUrl != null) {
                    conImgPool.remove(focusNode.getId() + oldIconUrl);
                }
            }
        }
    }

    /**
     * Move focus.
     *
     * @param reqNode the req node
     * @param focus the focus
     * @param position the position
     * @param setPosterImg the set poster img
     */
    private void moveFocus(Content reqNode, int focus, int position, boolean setPosterImg) {
        Vector children = reqNode.getChildren();
        if (children == null) { return; }
        stopTimer();
        int reqPage = getPageNumber(focus);
        int curPage = getPageNumber(photoFocus);
        photoFocus = focus;
        focusPosition = position;
        if (Config.enableDebugUI) { LogDisplayer.getInstance().setFocusPage(reqPage + 1); }
        reqNode.setLastFocus(photoFocus);
        reqNode.setLastFocusId(((Content) children.elementAt(photoFocus)).getId());
        setTicker();
        repaint();
        if (setPosterImg) { setPosterImages(); }
        startTimerForFolder();
        int totalCount = reqNode.getChildCount();
        int lastPageNum = getLastPageNumber(totalCount);
        int cachePageNumber = Config.cachePageNumber;
        if (Config.cachePageNumber % 2 == 0) { cachePageNumber++; }
        if (curPage != reqPage && lastPageNum + 1 > cachePageNumber) {
            String actionType = PagingThread.TYPE_LOAD;
            int actionPage = reqPage + Config.cachePageNumber / 2;
            if (curPage > reqPage) {
                actionPage++;
                actionType = PagingThread.TYPE_REMOVE;
            }
            if (actionPage > lastPageNum) { actionPage = actionPage - lastPageNum - 1; }
            loadPagingData(reqNode, menuFocus, actionPage, actionType);

            actionType = PagingThread.TYPE_REMOVE;
            actionPage = reqPage - Config.cachePageNumber / 2 - 1;
            if (curPage > reqPage) {
                actionPage++;
                actionType = PagingThread.TYPE_LOAD;
            }
            if (actionPage < 0) { actionPage = lastPageNum + actionPage + 1; }
            loadPagingData(reqNode, menuFocus, actionPage, actionType);
        }
    }

    /**
     * Check focus data.
     *
     * @param focus the focus
     * @param position the position
     * @param setPosterImg the set poster img
     */
    private void checkFocusData(final int focus, final int position, final boolean setPosterImg) {
        final Content reqNode = currentNode;
        if (reqNode == null || reqNode.getChildren() == null) { return; }
        if (reqNode.getChildCount() <= Config.loadDataNumber) {
            moveFocus(reqNode, focus, position, setPosterImg);
            return;
        }

        final int reqPage = getPageNumber(focus);
        Hashtable browsePages = reqNode.getBrowsePageNums();
        if (browsePages == null) { return; }

        Hashtable errorPages = reqNode.getErrorPageNums();
        if (errorPages != null && errorPages.containsKey(String.valueOf(reqPage))) {
            showConnectErrorPopup();
        } else if (browsePages.containsKey(String.valueOf(reqPage))) {
            moveFocus(reqNode, focus, position, setPosterImg);
        } else {
            CommunicationManager.getInstance().showNotDelayLoadingAnimation();
            new Thread("ContentListUI|checkFocusData()") {
                public void run() {
                    while (true) {
                        try {
                            Thread.sleep(300L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (!checkVisible() || SceneManager.getInstance().curSceneId != SceneManager.SC_CONTENT_LIST
                                || currentNode != reqNode || currentNode == null || reqNode.getChildren() == null
                                || !CommunicationManager.getInstance().isLoadingAnimation()) {
                            break;
                        }
                        Hashtable browsePages = reqNode.getBrowsePageNums();
                        if (browsePages == null) { break; }
                        Logger.debug(this, "checkFocusData()-reqPage : " + reqPage);
                        Hashtable errorPages = reqNode.getErrorPageNums();
                        if (browsePages.containsKey(String.valueOf(reqPage))) {
                            EventQueue.invokeLater(new Runnable() {
                                public void run() {
                                    CommunicationManager.getInstance().hideLoadingAnimation();
                                    moveFocus(reqNode, focus, position, setPosterImg);
                                }
                            });
                            break;
                        } else if (errorPages != null && errorPages.containsKey(String.valueOf(reqPage))) {
                            CommunicationManager.getInstance().hideLoadingAnimation();
                            showConnectErrorPopup();
                            break;
                        }
                    }
                }
            } .start();
        }
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (uiType == UI_WALL) {
                if (photoFocus % IMAGES_PER_LINE != 0) {
                    checkFocusData(photoFocus - 1, focusPosition - 1, false);
                }
            }
            break;
        case Rs.KEY_RIGHT:
            if (uiType == UI_WALL) {
                int totalCount = currentNode.getChildren().size();
                if ((photoFocus + 1) % IMAGES_PER_LINE != 0 && photoFocus < totalCount - 1) {
                    checkFocusData(photoFocus + 1, focusPosition + 1, false);
                }
            }
            break;
        case Rs.KEY_UP:
            if (uiType == UI_WALL) {
                if (photoFocus / IMAGES_PER_LINE > 0) {
                    int reqFocus =  photoFocus - IMAGES_PER_LINE;
                    int reqPosition = focusPosition;
                    boolean setPosterImg = true;
                    if (focusPosition >= IMAGES_PER_LINE) {
                        reqPosition = focusPosition - IMAGES_PER_LINE;
                        setPosterImg = false;
                    }
                    checkFocusData(reqFocus, reqPosition, setPosterImg);
                }
            } else {
                if (photoFocus > 0) {
                    int reqPosition = focusPosition;
                    if (focusPosition != MIDDLE_POS || photoFocus == focusPosition) { reqPosition--; }
                    checkFocusData(photoFocus - 1, reqPosition, true);
                }
            }
            break;
        case Rs.KEY_DOWN:
            int totalCount = currentNode.getChildren().size();
            if (uiType == UI_WALL) {
                if (!isLastLineForGrid(totalCount, photoFocus, IMAGES_PER_LINE)) {
                    int reqFocus = totalCount - 1;
                    if (photoFocus + IMAGES_PER_LINE < totalCount) { reqFocus = photoFocus + IMAGES_PER_LINE; }
                    boolean setPosterImg = focusPosition >= IMAGES_PER_LINE;
                    int reqPosition = IMAGES_PER_LINE + (reqFocus % IMAGES_PER_LINE);
                    checkFocusData(reqFocus, reqPosition, setPosterImg);
                }
            } else {
                if (photoFocus < totalCount - 1) {
                    int reqPosition = focusPosition;
                    if ((focusPosition < ROWS_PER_PAGE - 1)
                            && (focusPosition != MIDDLE_POS || focusPosition + photoFocus >= totalCount - 1)) {
                        reqPosition++;
                    }
                    checkFocusData(photoFocus + 1, reqPosition, true);
                }
            }
            break;
        case Rs.KEY_ENTER:
            processKeyEnter();
            break;
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            isEnterKeyForList = false;
            if (currentNode.getParent() != null) {
                int[] historyNum = (int[]) historyStack.pop();
                isNewBrowse = false;
                changeNode(historyNum[0], historyNum[1], currentNode.getParent(), menuFocus,
                        historyNum[2], uiType);
            } else {
                Logger.debug(this, "contentRoot[menuFocus].getId()-" + contentRoot[menuFocus].getId());
                Logger.debug(this, "currentNode.getId()-" + currentNode.getId());
                historyStack.clear();
                SceneManager.getInstance().goToScene(SceneManager.SC_HOME, false, null);
            }
            break;
        case KeyCodes.SEARCH:
            footer.clickAnimation(BasicRenderer.btnSearchTxt);
            searchPopup.setListOrder(menuFocus, -1, null);
            searchPopup.resetStateFocus();
            addPopup(searchPopup);
            break;
        case Rs.KEY_PAGE_UP:
        case Rs.KEY_PAGE_DOWN:
            footer.clickAnimation(BasicRenderer.btnPageTxt);
            changePage(keyCode);
            break;
        case KeyCodes.COLOR_A:
            footer.clickAnimation(BasicRenderer.btnViewChangeTxt);
            viewChangePopup.resetFocus();
            viewChangePopup.setListFocus(uiType);
            addPopup(viewChangePopup);
            break;
        case KeyCodes.COLOR_B:
            footer.clickAnimation(BasicRenderer.btnMPlayerTxt);
            musicPlayerPopup.resetFocus();
            addPopup(musicPlayerPopup);
            break;
        case KeyCodes.PLAY:
            if (menuFocus == MENU_PHOTO) {
                Content focusNode = (Content) currentNode.getChildren().elementAt(photoFocus);
                Hashtable errorPages = currentNode.getErrorPageNums();
                if (!focusNode.isFolder() && errorPages != null && !errorPages.isEmpty()) {
                    showConnectErrorPopup();
                    return true;
                }
                SceneManager.getInstance().goToScene(SceneManager.SC_PHOTO_PLAY, true, focusNode);
            } else if (menuFocus == MENU_VIDEO) { processKeyEnter(); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Process key enter.
     */
    private void processKeyEnter() {
        if (uiType == UI_WALL) {
            int addX = (photoFocus % IMAGES_PER_LINE) * 164;
            int addY = (focusPosition / ContentListUI.IMAGES_PER_LINE) * 177;
            clickEffect.start(82 + addX, 108 + addY, 386 - 246, 247 - 108);
        } else {
            clickEffect.start(55, 174 + (focusPosition * 32), 584 - 55, 338 - 302);
        }
        Content focusNode = (Content) currentNode.getChildren().elementAt(photoFocus);
        if (focusNode.isFolder()) {
            isNewBrowse = true;
            isEnterKeyForList = true;
            historyStack.push(new int[]{photoFocus, focusPosition, sortType});
            changeNode(0, 0, focusNode, menuFocus, getSortType(menuFocus), uiType);
        } else {
            if (menuFocus == MENU_VIDEO) {
                SceneManager.getInstance().goToScene(SceneManager.SC_VIDEO_PLAY, true, focusNode);
            } else {
                Hashtable errorPages = currentNode.getErrorPageNums();
                if (errorPages != null && !errorPages.isEmpty()) {
                    showConnectErrorPopup();
                    return;
                }
                SceneManager.getInstance().goToScene(SceneManager.SC_PHOTO_PLAY, true, focusNode);
            }
        }
    }

    /**
     * Change page.
     *
     * @param keyCode the key code
     */
    private void changePage(int keyCode) {
        int totalCount = currentNode.getChildren().size();
        if (uiType == UI_WALL && totalCount > IMAGES_PER_PAGE) {
            int[] num = getInitLastNumberForWall(photoFocus, focusPosition, totalCount, IMAGES_PER_PAGE);
            if (keyCode == Rs.KEY_PAGE_UP && num[0] > 0) {
                int reqFocus = photoFocus - IMAGES_PER_PAGE;
                if (num[0] - IMAGES_PER_PAGE < 0) { reqFocus = photoFocus - IMAGES_PER_LINE; }
                if (reqFocus != photoFocus) { checkFocusData(reqFocus, focusPosition, true); }
            } else if (keyCode == Rs.KEY_PAGE_DOWN && num[1] - num[0] >= IMAGES_PER_PAGE) {
                int reqFocus = photoFocus + IMAGES_PER_LINE;
                if ((totalCount - 1) - num[1] > 0) { reqFocus =  photoFocus + IMAGES_PER_PAGE; }
                if (reqFocus > totalCount - 1) { reqFocus = totalCount - 1; }
                if (reqFocus != photoFocus) { checkFocusData(reqFocus, focusPosition, true); }
            }
        } else if (uiType == UI_LIST && totalCount > ROWS_PER_PAGE) {
            if (keyCode == Rs.KEY_PAGE_UP && focusPosition >= MIDDLE_POS) {
                int initNum = photoFocus - focusPosition;
                int reqFocus = MIDDLE_POS;
                if (initNum - ROWS_PER_PAGE >= 0) { reqFocus =  initNum - ROWS_PER_PAGE + MIDDLE_POS; }
                if (reqFocus != photoFocus) { checkFocusData(reqFocus, MIDDLE_POS, true); }
            } else if (keyCode == Rs.KEY_PAGE_DOWN && focusPosition <= MIDDLE_POS) {
                int lastNum = 0;
                lastNum = photoFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                int reqFocus = totalCount - 1 - MIDDLE_POS;
                if (lastNum + ROWS_PER_PAGE < totalCount) { reqFocus =  lastNum + ROWS_PER_PAGE - MIDDLE_POS; }
                if (reqFocus != photoFocus) { checkFocusData(reqFocus, MIDDLE_POS, true); }
            }
        }
    }

    /**
     * D-Option Selected.
     *
     * @param item the item
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }
        Logger.debug(this, "selected()-item.getKey() : " + item.getKey());
        if (item.equals(OPT_PHOTO) || item.equals(OPT_MUSIC) || item.equals(OPT_VIDEO)) {
            int newIndex = MENU_PHOTO;
            if (item.equals(OPT_MUSIC)) {
                newIndex = MENU_MUSIC;
            } else if (item.equals(OPT_VIDEO)) { newIndex = MENU_VIDEO; }
            //if (newIndex != menuFocus) {
            if (newIndex == MENU_MUSIC) {
                goToContentUI(newIndex);
            } else {
                if (contentRoot[newIndex].getTotalCount() > 0) {
                    //historyStack.clear();
                    //currentNode = null;
                    isNewBrowse = true;
                    if (newIndex == MENU_PHOTO) {
                        tempUIType = PreferenceProxy.getInstance().photoUiType;
                    } else { tempUIType = PreferenceProxy.getInstance().videoUiType; }
                    //listTicker.stop();
                    //remove(listTicker);
                    isSceneReset = true;
                    Content reqNode = contentRoot[newIndex];
                    if (reqNode == currentNode) { reqNode = (Content) currentNode.clone(); }
                    changeNode(0, 0, reqNode, newIndex, getSortType(newIndex), tempUIType);
                } else { goToContentUI(newIndex); }
            }
        } else if (item.equals(OPT_SORT_ALPHA_A) || item.equals(OPT_SORT_ALPHA_Z) || item.equals(OPT_SORT_NEWEST)
                || item.equals(OPT_SORT_OLDEST)) {
            int type = 0;
            if (item.equals(OPT_SORT_ALPHA_Z)) {
                type = Content.SORT_ALPHA_Z;
            } else if (item.equals(OPT_SORT_NEWEST)) {
                type = Content.SORT_NEWEST;
            } else if (item.equals(OPT_SORT_OLDEST)) { type = Content.SORT_OLDEST; }
            if (type != sortType) {
                isNewBrowse = true;
                Content reqNode = (Content) currentNode.clone();
                changeNode(0, 0, reqNode, menuFocus, type, uiType);
            }
        } else { super.selected(item); }
    }

    /**
     * Change node.
     *
     * @param focus the focus
     * @param position the position
     * @param newNode the new node
     * @param menuIndex the menu focus
     * @param newSortType the new sort type
     * @param newUIType the new ui type
     */
    private void changeNode(int focus, int position, Content newNode, int menuIndex, int newSortType,
            int newUIType) {
        stopTimer();
        stopPagingThread();
        tempPhotoFocus = focus;
        tempFocusPosition = position;
        tempNode = newNode;
        tempMenuFocus = menuIndex;
        tempSortType = newSortType;
        tempUIType = newUIType;
        loadData(new Object[]{TH_CONTENT_LIST}, true);
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        stopTimer();
        String procId = (String) obj[0];
        if (procId.equals(TH_CONTENT_LIST)) {
            if (isSceneReset) {
                resetRootChildren();
                historyStack.clear();
            }
            Vector result = null;
            int pageNum = getPageNumber(tempPhotoFocus);
            int startIndex = pageNum * Config.loadDataNumber;
            if (tempNode != null && (isNewBrowse || tempNode.getChildren() == null)) {
                boolean prepare = HNHandler.getInstance().prepareChildNode(tempNode.getId(),
                        tempNode, tempMenuFocus, tempSortType);
                if (prepare) {
                    if (!isNewBrowse && tempPhotoFocus > 0 && tempPhotoFocus >= tempNode.getChildCount()) {
                        tempPhotoFocus = 0;
                        tempFocusPosition = 0;
                        pageNum = 0;
                        startIndex = 0;
                    }
                    result = HNHandler.getInstance().setChildNode(tempNode, tempMenuFocus, tempSortType, startIndex,
                            Config.loadDataNumber);
                    if (result != null && !isNewBrowse && tempPhotoFocus > 0) {
                        int index = tempPhotoFocus - startIndex;
                        if (index >= result.size()
                                || !((Content) result.elementAt(index)).getId().equals(tempNode.getLastFocusId())) {
                            tempPhotoFocus = 0;
                            tempFocusPosition = 0;
                            pageNum = 0;
                            startIndex = 0;
                            result = HNHandler.getInstance().setChildNode(tempNode, tempMenuFocus, tempSortType,
                                    startIndex, Config.loadDataNumber);
                        }
                    }
                }
                Logger.debug(this, "notifyFromDataLoadingThread()-result : " + result);
                Content.sortType = tempSortType;
                tempNode.setLastFocus(tempPhotoFocus);
                tempNode.setErrorPageNums(null);
                tempNode.setBrowsePageNums(null);
                if (!checkVisible()) { return; }
                int[] numbers = getNumberForPage(tempNode.getChildCount(), pageNum);
                if (result == null || result.size() != numbers[1]) { result = null; }
                //
                isNewBrowse = true;
                if (result != null) {
                    Vector children = new Vector();
                    children.setSize(tempNode.getChildCount());
                    for (int i = 0; i < result.size(); i++) {
                        children.setElementAt(result.elementAt(i), startIndex + i);
                    }
                    tempNode.setChildren(children);
                    Hashtable browsePages = new Hashtable();
                    browsePages.put(String.valueOf(pageNum), "");
                    tempNode.setBrowsePageNums(browsePages);
                    tempNode.setLastLoadedPage(pageNum + 1);
                    if (startIndex == 0) {
                        Content firstChild = (Content) result.elementAt(0);
                        tempNode.setFirstFolder(firstChild.isFolder());
                    }
                    tempNode.setLastFocusId(((Content) children.elementAt(tempPhotoFocus)).getId());
                    //
                    if (!checkListPage(tempPhotoFocus, tempFocusPosition, tempNode, tempMenuFocus, tempSortType,
                            tempUIType)) {
                        result = null;
                    }
                }
            }
            if (tempNode == null || (!isNewBrowse && tempNode.getChildren() == null)
                    || (isNewBrowse && result == null)) {
                CommunicationManager.getInstance().hideLoadingAnimation();
                //notiPopup.setMode(menuFocus);
                //addPopup(notiPopup);
                showConnectErrorPopup();
                if (isEnterKeyForList) { historyStack.pop(); }
                isSceneReset = false;
                if (tempNode != null) {
                    Hashtable errorPages = tempNode.getErrorPageNums();
                    if (errorPages == null) { errorPages = new Hashtable(); }
                    errorPages.put(String.valueOf(pageNum), "");
                    tempNode.setErrorPageNums(errorPages);
                }
                if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                    setDebugData(tempNode, pageNum + 1);
                }
                tempNode = null;
                return;
            }
            CommunicationManager.getInstance().hideLoadingAnimation();
            if (!checkVisible()) { return; }
            //stopTimer();
            listTicker.stop();
            remove(listTicker);
            uiType = tempUIType;
            sortType = tempSortType;
            tempNode.setChildSort(tempSortType);
            Content oldNode = currentNode;
            //
            //
            currentNode = tempNode;
            photoFocus = tempPhotoFocus;
            focusPosition = tempFocusPosition;
            if (menuFocus != tempMenuFocus) { renderer.resetLocationTitle(tempMenuFocus); }
            menuFocus = tempMenuFocus;
            Content flushNode = oldNode;
            if (oldNode == currentNode || (isSceneReset && oldNode != null)) { flushNode = null; }
            if (oldNode != null && oldNode.getId().equals(currentNode.getId())) { flushNode = null; }
            if (oldNode != null && (oldNode != currentNode || oldNode.getParent() == currentNode)) {
                oldNode.setChildren(null);
                flushNode = null;
            }
            if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                setDebugData(tempNode, pageNum + 1);
            }
            flushPhotoImgs(flushNode);
            setTicker();
            loadInitPagingData(currentNode, photoFocus, menuFocus);
            repaint();
            setPosterImages();
            startTimerForFolder();
            if (searchContent != null) {
                searchContent = null;
                if (!isSceneReset) {
                    searchPopup.setListOrder(menuFocus, -1, null);
                    addPopup(searchPopup);
                }
            }
            if (isSceneReset) { SceneManager.getInstance().scenes[previousSceneID].clearTempData(); }
            isSceneReset = false;
        }
    }

    /**
     * Flush photo imgs.
     *
     * @param oldNode the old node
     */
    private void flushPhotoImgs(Content oldNode) {
        conImgPool.clear();
        if (oldNode == null) { return; }
        Vector list = oldNode.getChildren();
        for (int i = 0; list != null && i < list.size(); i++) {
            Content content = (Content) list.elementAt(i);
            if (content != null) { content.setIconImage(null); }
        }
    }

    /**
     * Sets the ticker.
     */
    private void setTicker() {
        if (uiType == UI_LIST) {
            listTicker.setFont(Rs.F19);
            listTicker.setTextColor(Rs.C0);
            listTicker.setTextPosY(326 - 302);
            listTicker.setBounds(69, 174 + (focusPosition * 32), 474 - 69, 36);
            listTicker.setAlign(AutoScroller.ALIGN_LEFT);
        } else {
            listTicker.setFont(Rs.F16);
            listTicker.setTextColor(Rs.C255203000);
            listTicker.setTextPosY(261 - 250);
            int addX = (photoFocus % IMAGES_PER_LINE) * 164;
            int addY = (focusPosition / IMAGES_PER_LINE) * 177;
            listTicker.setBounds(90 + addX, 250 + addY, 215 - 90, 263 - 250);
            listTicker.setAlign(AutoScroller.ALIGN_CENTER);
        }
        String text = null;
        Vector list = currentNode.getChildren();
        if (list != null && !list.isEmpty()) {
            Content content = (Content) list.elementAt(photoFocus);
            if (content != null) {
                text = content.getName();
                if (content.getTotalCount() > 0) { text += " (" + content.getTotalCount() + ")"; }
            }
        }
        listTicker.setText(text, null);
        add(listTicker);
        //repaint();
    }

    /**
     * Sets the poster images.
     */
    private void setPosterImages() {
        Logger.debug(this, "called setPosterImages()");
        final int focus = photoFocus;
        final int position = focusPosition;
        final Content reqNode = currentNode;
        final int reqUiType = this.uiType;
        new Thread("AllMovie|setPosterImage()") {
            public void run() {
                Vector list = reqNode.getChildren();
                if (list == null) { return; }
                if (!checkFocus(focus, reqUiType, reqNode)) { return; }
                if (reqUiType == UI_LIST) {
                    Content node = (Content) list.elementAt(focus);
                    if (node == null) { return; }
                    if (node.getIconImage() == null) {
                        int[] size = SIZE_LIST_FILE;
                        if (node.isFolder()) { size = SIZE_LIST_FOLDER; }
                        String key = node.getId() + node.getIconUrl();
                        Image img = conImgPool.getImage(key);
                        if (img != null) {
                            node.setIconImage(img);
                        } else {
                            try {
                                Thread.sleep(500L);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (!checkFocus(focus, reqUiType, reqNode)) { return; }
                            img = ContentListUI.this.getContentImage(key, node.getIconUrl(), null, size);
                            conImgPool.waitForAll();
                            node.setIconImage(img);
                        }
                        if (!checkVisible()) { return; }
                        if (checkFocus(focus, reqUiType, reqNode)) { repaint(); }
                    }
                } else {
                    int[] numbers =
                        getInitLastNumberForWall(focus, position, list.size(), IMAGES_PER_PAGE);
                    try {
                        Thread.sleep(500L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!checkFocus(focus, reqUiType, reqNode)) { return; }
                    for (int i = numbers[0]; i <= numbers[1]; i++) {
                        if (!checkFocus(i, reqUiType, reqNode)) { break; }
                        Content node = (Content) list.elementAt(i);
                        if (node == null) { continue; }
                        if (node.getIconImage() == null) {
                            int[] size = SIZE_WALL_FILE;
                            if (node.isFolder()) { size = SIZE_WALL_FOLDER; }
                            String key = node.getId() + node.getIconUrl();
                            Image img = ContentListUI.this.getContentImage(key, node.getIconUrl(), null, size);
                            conImgPool.waitForAll();
                            node.setIconImage(img);
                            if (!checkVisible()) { return; }
                            if (checkFocus(i, reqUiType, reqNode)) { repaint(); }
                        }
                        try {
                            if (i < numbers[1]) { Thread.sleep(100L); }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                if (SceneManager.getInstance().curSceneId == SceneManager.SC_INVALID) {
                    conImgPool.clear();
                    return;
                }
            }
        } .start();
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @param reqUiType the ui type
     * @param reqNode the requested Content
     * @return true, if successful
     */
    private boolean checkFocus(int reqIndex, int reqUiType, Content reqNode) {
        if (SceneManager.getInstance().curSceneId != SceneManager.SC_CONTENT_LIST
                || uiType != reqUiType || currentNode != reqNode || currentNode == null) {
            return false;
        }
        if (reqUiType == UI_LIST) {
            return reqIndex == photoFocus;
        } else {
            int totalCount = reqNode.getChildren().size();
            int[] numbers = getInitLastNumberForWall(photoFocus, focusPosition, totalCount, IMAGES_PER_PAGE);
            return reqIndex >= numbers[0] && reqIndex <= numbers[1];
        }
    }

    /**
     * Reset position.
     *
     * @param node the node
     * @param newUIType the new ui type
     * @param focus the focus
     * @return the int for new position
     */
    private int resetPosition(Content node, int newUIType, int focus) {
        int totalCount = node.getChildCount();
        int position = 0;
        Logger.debug(this, "resetPosition()-totalCount : " + totalCount);
        Logger.debug(this, "resetPosition()-newUIType : " + newUIType);
        Logger.debug(this, "resetPosition()-focus : " + focus);
        if (newUIType == UI_WALL) {
            position = focus % IMAGES_PER_LINE;
            int firstNum = focus - position;
            if (firstNum + IMAGES_PER_LINE > totalCount - 1 && firstNum > 0) {
                position += IMAGES_PER_LINE;
            }
        } else {
            if (focus < MIDDLE_POS || totalCount <= ROWS_PER_PAGE) {
                position = focus;
            } else {
                position = ROWS_PER_PAGE - (totalCount - focus);
                if (position < MIDDLE_POS) { position = MIDDLE_POS; }
            }
        }
        Logger.debug(this, "resetPosition()-position : " + position);
        return position;
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof ViewChangePopup) {
            int viewType = ((Integer) para).intValue();
            //removePopup();
            if (viewType != uiType) {
                stopTimer();
                uiType = viewType;
                if (menuFocus == MENU_PHOTO) {
                    PreferenceProxy.getInstance().photoUiType = uiType;
                    PreferenceProxy.getInstance().setPreferenceValue(PreferenceProxy.NAME_PHOTO_UI,
                            String.valueOf(uiType));
                } else {
                    PreferenceProxy.getInstance().videoUiType = uiType;
                    PreferenceProxy.getInstance().setPreferenceValue(PreferenceProxy.NAME_VIDEO_UI,
                            String.valueOf(uiType));
                }
                focusPosition = resetPosition(currentNode, uiType, photoFocus);
                flushPhotoImgs(currentNode);
                setTicker();
                repaint();
                setPosterImages();
                startTimerForFolder();
            }
        } else if (popup instanceof NotiPopup) {
            removePopup();
            if (currentNode == null) {
                SceneManager.getInstance().goToScene(previousSceneID, false, null);
            } else {
                loadInitPagingData(currentNode, photoFocus, menuFocus);
                startTimerForFolder();
                setTicker();
                repaint();
            }
        } else if (popup instanceof SearchPopup) {
            removePopup();
            if (para != null) {
                goToSearchContent((Content) para);
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * Sets the debug data.
     *
     * @param reqNode the new debug data
     * @param focusPage the focus page
     */
    public void setDebugData(Content reqNode, int focusPage) {
        LogDisplayer.getInstance().resetData();
        Content debugNode = reqNode;
        if (debugNode == null) { debugNode = currentNode; }
        if (debugNode == null) { return; }
        if (focusPage < 0) { focusPage = getPageNumber(photoFocus) + 1; }

        LogDisplayer.getInstance().setInitData(debugNode.getId(), debugNode.getName(),
                getLastPageNumber(debugNode.getChildCount()) + 1);
        LogDisplayer.getInstance().setFocusPage(focusPage);
        LogDisplayer.getInstance().setLastLoadedPage(debugNode.getLastLoadedPage());
        LogDisplayer.getInstance().setLastRemovedPage(debugNode.getLastRemovedPage());
        LogDisplayer.getInstance().setLoadedPages(debugNode.getBrowsePageNums());
        LogDisplayer.getInstance().setErrorPages(debugNode.getErrorPageNums());
    }
}

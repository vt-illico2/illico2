/*
 *  @(#)ViewChangePopup.java 1.0 2011.06.13
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>ViewChangePopup</code> the popup to select view type(wall or list) for list.
 *
 * @since   2011.06.13
 * @version $Revision: 1.8 $ $Date: 2011/10/28 06:24:57 $
 * @author  tklee
 */
public class ViewChangePopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant STATE_LIST. */
    private static final int STATE_LIST = 0;
    /** The Constant STATE_BTN. */
    private static final int STATE_BTN = 1;

    /** The list focus. */
    private int listFocus = 0;
    /** The state focus. */
    private int stateFocus = 0;
    /** The prev focus. */
    private int prevFocus = 0;

    /**
     * Initialize the Component.
     */
    public ViewChangePopup() {
        setBounds(276, 113, 410, 364);
        setRenderer(new PopupRenderer());
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() { }

    /**
     * reset focus index.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        listFocus = 0;
        stateFocus = 0;
        prevFocus = 0;
    }

    /**
     * Sets the list focus.
     *
     * @param focus the new list focus
     */
    public void setListFocus(int focus) {
        listFocus = focus;
        prevFocus = focus;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (stateFocus == STATE_BTN) {
                stateFocus = STATE_LIST;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (stateFocus == STATE_LIST) {
                stateFocus = STATE_BTN;
                repaint();
            }
            break;
        case Rs.KEY_LEFT:
            if (stateFocus == STATE_LIST && listFocus > 0) {
                listFocus--;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case KeyCodes.COLOR_A:
            if (stateFocus == STATE_LIST) {
                listFocus = (listFocus + 1) % 2;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case Rs.KEY_RIGHT:
            if (stateFocus == STATE_LIST && listFocus == 0) {
                listFocus++;
                repaint();
                scene.requestPopupOk(new Integer(listFocus));
            }
            break;
        case Rs.KEY_ENTER:
            if (stateFocus == STATE_LIST) {
                clickEffect.start(401 + (listFocus * 102) - 276, 245 - 113, 56, 57);
                //scene.requestPopupOk(new Integer(listFocus));
                scene.requestPopupClose();
            } else {
                clickEffect.start(403 - 276, 352 - 113, 558 - 403, 32);
                scene.requestPopupOk(new Integer(prevFocus));
                scene.requestPopupClose();
            }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.8 $ $Date: 2011/10/28 06:24:57 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The btn dim img. */
        private Image btnDimImg = null;
        /** The view icon img. */
        private Image[] viewIconImg = new Image[2];
        /** The view icon focus img. */
        private Image viewIconFocusImg = null;
        /** The arrow l img. */
        private Image arrowLImg = null;
        /** The arrow r img. */
        private Image arrowRImg = null;

        /** The title txt. */
        private String titleTxt = null;
        /** The desc txt. */
        private String descTxt = null;
        /** The note txt. */
        private String[] viewTxt = new String[2];
        /** The btn cancel txt. */
        private String btnCancelTxt = null;

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("pop_sha.png");
            bgMImg = getImage("05_pop_glow_m.png");
            bgTImg = getImage("05_pop_glow_t.png");
            bgBImg = getImage("05_pop_glow_b.png");
            gapImg = getImage("pop_gap_379.png");
            highImg = getImage("pop_high_350.png");
            btnFocusImg = getImage("05_focus.png");
            btnDimImg = getImage("05_focus_dim.png");
            viewIconImg[0] = getImage("09_viewicon01.png");
            viewIconImg[1] = getImage("09_viewicon02.png");
            viewIconFocusImg = getImage("09_view_foc.png");
            arrowLImg = getImage("02_ars_l.png");
            arrowRImg = getImage("02_ars_r.png");
            super.prepare(c);
            titleTxt = getString("viewSelectTitle");
            descTxt = getString("viewSelectDesc");
            btnCancelTxt = getString("btnCanecl");
            viewTxt[0] = getString("viewWall");
            viewTxt[1] = getString("viewList");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -113);
            //bg
            g.drawImage(shadowImg, 306, 398, 350, 79, c);
            g.drawImage(bgMImg, 276, 193, 410, 159, c);
            g.drawImage(bgTImg, 276, 113, c);
            g.drawImage(bgBImg, 276, 352, c);
            g.setColor(Rs.C35);
            g.fillRect(306, 143, 350, 259);
            g.drawImage(gapImg, 285, 181, c);
            g.drawImage(highImg, 306, 143, c);
            //btn
            Image btnImg = btnDimImg;
            if (stateFocus == STATE_BTN) { btnImg = btnFocusImg; }
            if (btnImg != null) { g.drawImage(btnImg, 403, 352, c); }
            g.setFont(Rs.F18);
            g.setColor(Rs.C3);
            GraphicUtil.drawStringCenter(g, btnCancelTxt, 482, 373);
            //title
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            GraphicUtil.drawStringCenter(g, titleTxt, 481, 169);
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            GraphicUtil.drawStringCenter(g, descTxt, 482, 219);
            //icon
            for (int i = 0; i <= 1; i++) {
                int addX = i * 102;
                g.drawImage(viewIconImg[i], 401 + addX, 245, c);
                g.setFont(Rs.F18);
                g.setColor(Rs.C182);
                if (stateFocus == STATE_LIST) {
                    if (i == listFocus) {
                        g.drawImage(viewIconFocusImg, 401 + addX, 245, c);
                        Image img = arrowRImg;
                        if (i > 0) { img = arrowLImg; }
                        g.drawImage(img, 457 + (i * 29), 260, c);
                        g.setFont(Rs.F20);
                        g.setColor(Rs.C252202004);
                    }
                }
                GraphicUtil.drawStringCenter(g, viewTxt[i], 430 + addX, 324);
            }
            g.translate(276, 113);
        }
    }
}

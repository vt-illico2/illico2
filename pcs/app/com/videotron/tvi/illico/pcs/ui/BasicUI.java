/*
 *  @(#)BasicUI.java 1.0 2011.05.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.player.MediaPlayHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.gui.BasicRenderer;
import com.videotron.tvi.illico.pcs.ui.comp.ContentImagePool;
import com.videotron.tvi.illico.pcs.ui.comp.Footer;
import com.videotron.tvi.illico.pcs.ui.comp.LogDisplayer;
import com.videotron.tvi.illico.pcs.ui.popup.ConfirmPopup;
import com.videotron.tvi.illico.pcs.ui.popup.DeviceSelectPopup;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.pcs.ui.popup.NotiPopup;
import com.videotron.tvi.illico.pcs.ui.popup.Popup;
import com.videotron.tvi.illico.pcs.ui.popup.SearchPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This Class is the BasicUI class. All Scene is drawn on BasicUI.
 *
 * @since 2011.05.18
 * @version $Revision: 1.44 $ $Date: 2012/10/18 11:29:56 $
 * @author tklee
 */
public abstract class BasicUI extends UIComponent implements ClockListener , MenuListener {
    /** serialVersionUID for serializable type.*/
    private static final long serialVersionUID = 1L;

    /** The Constant ERRMSG_NONE. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_NONE = 0;
    /** The Constant ERRMSG_PREV. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_PREV = 1;
    /** The Constant ERRMSG_EXIT. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_EXIT = 2;
    /** The Constant ERRMSG_INIT. */
    public static final int ERRMSG_INIT = 3;

    /** The Constant MENU_VIDEO. */
    public static final int MENU_PHOTO = 0;
    /** The Constant MENU_MUSIC. */
    public static final int MENU_MUSIC = 1;
    /** The Constant MENU_PHOTO. */
    public static final int MENU_VIDEO = 2;

    /** The Constant IMAGES_PER_PAGE. */
    public static final int IMAGES_PER_PAGE = 10;
    /** The Constant IMAGES_PER_LINE. */
    public static final int IMAGES_PER_LINE = 5;
    /** The Constant ROWS_PER_PAGE. */
    public static final int ROWS_PER_PAGE = 9;
    /** The Constant MIDDLE_POS. */
    public static final int MIDDLE_POS = 4;

    /** The Constant SEQ_ALBUM. */
    public static final int SEQ_ALBUM = 0;
    /** The Constant SEQ_ARTIST. */
    public static final int SEQ_ARTIST = 1;
    /** The Constant SEQ_SONGS. */
    public static final int SEQ_SONGS = 2;
    /** The Constant SEQ_GENRES. */
    public static final int SEQ_GENRES = 3;
    /** The Constant SEQ_PLAYLISTS. */
    public static final int SEQ_PLAYLISTS = 4;

    /** The Constant UI_WALL. */
    public static final int UI_WALL = 0;
    /** The Constant UI_LIST. */
    public static final int UI_LIST = 1;

    /** The Constant SIZE_WALL_FILE. */
    public static final int[]  SIZE_WALL_FILE = {133, 133};
    /** The Constant SIZE_WALL_FOLDER. */
    public static final int[]  SIZE_WALL_FOLDER = {86, 51};
    /** The Constant SIZE_LIST_FILE. */
    public static final int[]  SIZE_LIST_FILE = {203, 203};
    /** The Constant SIZE_LIST_FOLDER. */
    public static final int[]  SIZE_LIST_FOLDER = {116, 69};

    /** The Constant OPT_PHOTO. */
    protected static final MenuItem OPT_PHOTO = new MenuItem("renderer.photos");
    /** The Constant OPT_MUSIC. */
    protected static final MenuItem OPT_MUSIC = new MenuItem("renderer.music");
    /** The Constant OPT_VIDEO. */
    protected static final MenuItem OPT_VIDEO = new MenuItem("renderer.videos");
    /** The Constant OPTION_PREFERENCE. */
    protected static final MenuItem OPT_PREFERENCE = new MenuItem("renderer.pref");
    /** The Constant OPT_CHANGE_DEV. */
    protected static final MenuItem OPT_CHANGE_DEV = new MenuItem("renderer.changeDev");
    /** The Constant OPT_HELP. */
    protected static final MenuItem OPT_HELP = new MenuItem("renderer.help");
    /** The Constant OPT_SORT_ALPHA_A. */
    protected static final MenuItem OPT_SORT_ALPHA_A = new MenuItem("renderer.sortAlpaA");
    /** The Constant OPT_SORT_ALPHA_Z. */
    protected static final MenuItem OPT_SORT_ALPHA_Z = new MenuItem("renderer.sortAlpaZ");
    /** The Constant OPT_SORT_NEWEST. */
    protected static final MenuItem OPT_SORT_NEWEST = new MenuItem("renderer.sortNewest");
    /** The Constant OPT_SORT_OLDEST. */
    protected static final MenuItem OPT_SORT_OLDEST = new MenuItem("renderer.sortOldest");
    /** The Constant MUSIC_FOOTER_B. */
    protected static final String MUSIC_FOOTER_B = "                                               ";
    /** The Constant MUSIC_FOOTER_B_PHOTO. */
    protected static final String MUSIC_FOOTER_B_PHOTO = "                                           ";
    /** The Constant MUSIC_FOOTER_B_PHOTO_VIEWER. */
    protected static final String MUSIC_FOOTER_B_PHOTO_VIEWER = "                            ";
    /** The Footer instance. */
    protected static Footer footer = null;
    /** The OptionScreen. */
    protected static OptionScreen optScr = null;
    /** The data loading thread. */
    private DataLoadingThread dataLoadingThread = null;
    /** The popup. */
    public Popup popup = null;
    /** The noti popup. */
    protected static NotiPopup notiPopup = null;
    /** The music player popup. */
    public static MusicPlayerPopup musicPlayerPopup = null;
    /** The search popup. */
    public static SearchPopup searchPopup = null;
    /** The confirm popup. */
    protected static ConfirmPopup confirmPopup = null;
    /** The DeviceSelectPopup instance. */
    protected static DeviceSelectPopup devSelectPop = null;
    /** The popup bg comp. */
    private static PopupBgComp popupBgComp = null;
    /** The exit popup bg comp. */
    private static PopupBgComp exitPopupBgComp = null;
    /** The music footer comp. */
    protected static MusicFooterComp musicFooterComp = null;
    /** The root menu item for D-Option. */
    protected MenuItem rootMenuItem = null;
    /** The image pool for content. */
    public static ContentImagePool conImgPool = null;
    /** The ClickingEffect instance. */
    protected ClickingEffect clickEffect = null;

    /** The DeviceInfo array. */
    public static Vector deviceInfos = null;
    /** The objects for content root. */
    public static Content[] contentRoot = null;
    /** The objects for music root. */
    public static Content[] musicRoot = null;
    /** The artist songs. */
    //public static Hashtable artistSongs = new Hashtable();

    /** Is stored the previous Scene ID. */
    protected int previousSceneID = -1;
    /** Is stored the value. */
    protected Object receivedData = null;
    /** The focus history. */
    //protected Object[] focusHistory = null;
    /** The err msg action. */
    public int errMsgAction = -1;
    /** The confirm popup for exit by hotkey. */
    public Popup exitConfirmPopup = null;
    /** The sort type. */
    public int sortType = -1;
    /** The paging thread. */
    protected PagingThread pagingDataThread = null;
    /** The paging ani thread. */
    //public PagingThread pagingAniThread = null;
    /** The browse img num. */
    //public int browseImgNum = -1;
    /** The browse count. */
    //public int browseCount = 0;
    /** The temp browse count. */
    //protected int tempBrowseCount = 0;
    /** The search content. */
    public static Content searchContent = null;
    /** The is scene reset. */
    protected boolean isSceneReset = true;

    /**
     * Instantiates a new BasicUI.
     */
    public BasicUI() {
        dataLoadingThread = new DataLoadingThread();
        if (popupBgComp == null) {
            popupBgComp = new PopupBgComp();
            notiPopup = new NotiPopup();
            devSelectPop = new DeviceSelectPopup();
            musicPlayerPopup = new MusicPlayerPopup();
            musicFooterComp = new MusicFooterComp();
            searchPopup = new SearchPopup();
            confirmPopup = new ConfirmPopup();
            exitPopupBgComp = new PopupBgComp();
        }
        if (footer == null) {
            footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.TEXT);
            conImgPool = new ContentImagePool();
        }
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     * @param sendData the send data
     */
    public void start(boolean isReset, Object[] sendData) {
        isSceneReset = isReset;
        if (optScr == null) { optScr = new OptionScreen(); }
        addFooterBtn();
        //changeFooterBtn();
        add(footer);
        processMusicFooterComp();

        if (isReset) { previousSceneID = ((Integer) sendData[0]).intValue(); }
        receivedData = sendData[1];
        Clock.getInstance().addClockListener(this);
    }

    /**
     * update a clock.
     *
     * @param date the date
     */
    public void clockUpdated(Date date) {
        repaint();
    }

    /**
     * Gets the current scene id.
     *
     * @return the current scene id
     */
    protected String getCurrentSceneId() {
        return String.valueOf(SceneManager.getInstance().curSceneId);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        Clock.getInstance().removeClockListener(this);
        CommunicationManager.getInstance().hideLoadingAnimation();
        removeExitConfirmPopup();
        removePopup();
        removeAll();
        receivedData = null;
        if (optScr != null) { optScr.stop(); }
        errMsgAction = -1;
        stopPagingThread();
    }

    /**
     * Is disposed the scene.
     */
    public void dispose() {
        stop();
        removeAll();
        if (dataLoadingThread != null) { dataLoadingThread.stop(); }
        if (musicFooterComp != null) { musicFooterComp.stop(); }
        optScr = null;
        conImgPool.clear();
        deviceInfos = null;
        contentRoot = null;
        musicRoot = null;
        //artistSongs.clear();
        musicPlayerPopup.dispose();
        searchPopup.dispose();
        MediaPlayHandler.getInstance().dispose();
        HNHandler.getInstance().dispose();
        clearAllTempData();
    }

    /**
     * implement handleKey method.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    public boolean handleKey(int code) {
        Logger.debug(this, "handleKey()-code : " + code);
        if (code == KeyCodes.MENU || code == Rs.KEY_GUIDE || code == KeyCodes.VOD || code == KeyCodes.SETTINGS) {
            CommunicationManager.getInstance().hideLoadingAnimation();
            stopSlideShow();
            addExitConfirmPopup(code);
            return true;
        }
        if (exitConfirmPopup != null) { return exitConfirmPopup.handleKey(code); }
        if (popup != null) { return popup.handleKey(code); }
        if (CommunicationManager.getInstance().isLoadingAnimation()) {
            if (verifyKeyConsistency(code)) { return true; }
        }

        if (keyAction(code)) { return true; }
        if (code == KeyCodes.COLOR_D) {
            if (rootMenuItem != null) {
                footer.clickAnimation(BasicRenderer.btnOptTxt);
                if (optScr != null) {
                    if (sortType > -1) {
                        OPT_SORT_ALPHA_A.setChecked(false);
                        OPT_SORT_ALPHA_Z.setChecked(false);
                        OPT_SORT_NEWEST.setChecked(false);
                        OPT_SORT_OLDEST.setChecked(false);
                        if (sortType == Content.SORT_ALPHA_A) {
                            OPT_SORT_ALPHA_A.setChecked(true);
                        } else if (sortType == Content.SORT_ALPHA_Z) {
                            OPT_SORT_ALPHA_Z.setChecked(true);
                        } else if (sortType == Content.SORT_NEWEST) {
                            OPT_SORT_NEWEST.setChecked(true);
                        } else if (sortType == Content.SORT_OLDEST) {
                            OPT_SORT_OLDEST.setChecked(true);
                        }
                    }
                    optScr.start(rootMenuItem, this);
                }
            }
            return true;
        }
        if (code == Rs.KEY_EXIT || code == KeyCodes.LIVE) {
            CommunicationManager.getInstance().hideLoadingAnimation();
            addExitConfirmPopup(code);
            return true;
        }

        switch (code) {
        case KeyCodes.FORWARD:
        case KeyCodes.BACK:
            int playType = musicPlayerPopup.playMusicType;
            if (playType == MusicPlayerPopup.PLAY_MY_MUSIC || playType == MusicPlayerPopup.PAUSE_MY_MUSIC) {
                musicPlayerPopup.processPlayKey(code);
                return true;
            }
            break;
        case KeyCodes.PAUSE:
        case KeyCodes.STOP:
            playType = musicPlayerPopup.playMusicType;
            if (playType != MusicPlayerPopup.PLAY_NO) {
                musicPlayerPopup.resetFocus();
                addPopup(musicPlayerPopup);
                if (code == KeyCodes.STOP) {
                    if (playType == MusicPlayerPopup.PLAY_MY_MUSIC || playType == MusicPlayerPopup.PAUSE_MY_MUSIC) {
                        musicPlayerPopup.processPlayKey(code);
                    } else { musicPlayerPopup.processGalaxieKey(code); }
                } else {
                    if (playType == MusicPlayerPopup.PLAY_MY_MUSIC) {
                        musicPlayerPopup.processPlayKey(code);
                    } else if (playType == MusicPlayerPopup.PLAY_GALAXIE) { musicPlayerPopup.processGalaxieKey(code); }
                }
                return true;
            }
            break;
        default:
            break;
        }
        return verifyKeyConsistency(code);
    }

    /**
     * Verify key consistency.
     *
     * @param code the code
     * @return true, if verify
     */
    public static boolean verifyKeyConsistency(int code) {
        switch (code) {
        case KeyCodes.FORWARD:
        case KeyCodes.BACK:
        case KeyCodes.PAUSE:
        case KeyCodes.STOP:
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_D:
        case KeyCodes.SEARCH:
        case KeyCodes.RECORD:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
        case KeyCodes.PLAY:
        case KeyCodes.PIP:
        case KeyCodes.LIST:
        case Rs.KEY_CH_UP:
        case Rs.KEY_CH_DOWN:
        case Rs.KEY_PAGE_UP:
        case Rs.KEY_PAGE_DOWN:
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
        case KeyCodes.FAV:
        case Rs.KEY_LEFT:
        case Rs.KEY_RIGHT:
        case Rs.KEY_DOWN:
        case Rs.KEY_UP:
        case Rs.KEY_ENTER:
        case Rs.KEY_BACK:
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * add Popup component to UI.
     *
     * @param popupComp the popup comp
     */
    public void addPopup(Popup popupComp) {
        if (popup == popupComp) {
            repaint();
            return;
        }
        removePopup();
        popup = popupComp;
        if (popup == null) {
            repaint();
            return;
        }
        popup.setListener(this);
        popup.start();
        add(popupBgComp, 0);
        add(popup, 0);
        //popup.start();
        popup.startEffect();
        repaint();
    }

    /**
     * add Popup component to UI.
     *
     * @param keyCode the key code
     */
    public void addExitConfirmPopup(int keyCode) {
        if (exitConfirmPopup != null) { return; }
        confirmPopup.setMode(ConfirmPopup.MODE_APP_EXIT);
        confirmPopup.resetFocus();
        /*int goingValue = ConfirmPopup.GO_MENU;
        if (keyCode == Rs.KEY_GUIDE) {
            goingValue = ConfirmPopup.GO_GUIDE;
        } else if (keyCode == KeyCodes.VOD) { goingValue = ConfirmPopup.GO_VOD; }*/
        confirmPopup.setGoingValue(keyCode);
        exitConfirmPopup = confirmPopup;
        exitConfirmPopup.setListener(this);
        exitConfirmPopup.start();
        add(exitPopupBgComp, 0);
        add(exitConfirmPopup, 0);
        repaint();
    }

    /**
     * remove Popup on UI.
     */
    protected void removePopup() {
        if (popup == null) { return; }
        popup.stop();
        remove(popup);
        remove(popupBgComp);
        popup = null;
        repaint();
    }

    /**
     * remove Popup on UI.
     */
    protected void removeExitConfirmPopup() {
        if (exitConfirmPopup == null) { return; }
        exitConfirmPopup.stop();
        remove(exitConfirmPopup);
        remove(exitPopupBgComp);
        exitConfirmPopup = null;
        repaint();
    }

    /**
     * close the popup by normal popup.
     */
    public void requestPopupClose() {
        removePopup();
    }

    /**
     * close the popup by specific popup.
     */
    public void requestExitConfirmPopupClose() {
        removeExitConfirmPopup();
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup || popup instanceof SearchPopup) {
            removePopup();
        }
    }

    /**
     * Notify from ErrorMessage after action is performed.
     * perform next state by errMsgAction type.
     *
     * @param btnType the btn type
     */
    public void notifyErrorMessage(int btnType) {
        if (errMsgAction == ERRMSG_PREV) {
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
        } else if (errMsgAction == ERRMSG_EXIT) {
            CommunicationManager.getInstance().exitToChannel();
        } else if (errMsgAction == ERRMSG_INIT) {
            SceneManager.getInstance().goToScene(SceneManager.SC_INIT, false, HomeUI.tempDevice);
        }
        errMsgAction = -1;
    }

    /**
     * Show error message.
     *
     * @param errAction the error action
     * @param errCode the error code
     */
    public void showErrorMessage(int errAction, String errCode) {
        if (errMsgAction != -1) { return; }
        errMsgAction = errAction;
        CommunicationManager.getInstance().showErrorMessage(errCode);
    }

    /**
     * Show connect error popup.
     */
    public static void showConnectErrorPopup() {
        SceneManager sceneManager = SceneManager.getInstance();
        int curSceneId = sceneManager.curSceneId;
        if (curSceneId != SceneManager.SC_INVALID) {
            BasicUI currentScene = sceneManager.scenes[curSceneId];
            currentScene.stopSlideShow();
            currentScene.showErrorMessage(BasicUI.ERRMSG_INIT,
                    MainManager.getInstance().getErrorCode(MainManager.ERR_LOST_CONNECTION));
        }
    }

    /**
     * Check error message.
     *
     * @return true, whether ErrorMessage will be showed or not.
     */
    protected boolean checkErrorMessageShow() {
        //if (OobManager.getInstance().errorType != OobManager.NO_ERR) { return true; }
        return false;
    }

    /**
     * Load data by using Thread.
     *
     * @param obj request data
     * @param isLoadingBar the is loading bar. if true, loading bar will be showed.
     */
    protected void loadData(Object[] obj, boolean isLoadingBar) {
        if (isLoadingBar) { CommunicationManager.getInstance().showLoadingAnimation(); }
        if (dataLoadingThread != null) { dataLoadingThread.setObj(obj); }
    }

    /**
     * return the result of action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected abstract boolean keyAction(int keyCode);

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) { }

    /**
     * Checks if is slide show.
     *
     * @return true, if is slide show
     */
    public boolean isSlideShow() {
        return false;
    }

    /**
     * Stop slide show.
     */
    public void stopSlideShow() { }

    /**
     * Clear temp data.
     */
    public void clearTempData() { }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
    }

    /**
     * Sets the debug data.
     *
     * @param reqNode the new debug data
     * @param focusPage the focus page
     */
    public void setDebugData(Content reqNode, int focusPage) { }

    /**
     * Checks if is zoom mode for photo.
     *
     * @return true, if is zoom mode for photo
     */
    protected boolean isZoomModeForPhoto() {
        return false;
    }

    /**
     * Process music footer comp.
     */
    public void processMusicFooterComp() {
        int musicType = musicPlayerPopup.playMusicType;
        Logger.debug(this, "called processMusicFooterComp()-playMusicType : " + musicType);
        SceneManager sceneManager = SceneManager.getInstance();
        int curSceneId = sceneManager.curSceneId;
        if (musicType != MusicPlayerPopup.PLAY_NO //&& curSceneId == SceneManager.SC_MUSIC_LIST
                && curSceneId != SceneManager.SC_INVALID
                && curSceneId != SceneManager.SC_INIT && curSceneId != SceneManager.SC_PREFERENCE
                && curSceneId != SceneManager.SC_VIDEO_PLAY) {
            String name = getPlayingMusicName();
            if (name == null || name.length() == 0) { return; }
            if (curSceneId != SceneManager.SC_PHOTO_PLAY) {
                musicFooterComp.setBounds(628, 492, 808 - 628, 503 - 492 + 20);
                footer.changeText(BasicRenderer.btnBImg, MUSIC_FOOTER_B);
            } else {
                if (isZoomModeForPhoto()) { //zoom mode
                    musicFooterComp.setBounds(628 + 102, 492, 808 - 628, 503 - 492 + 20);
                    footer.changeText(BasicRenderer.btnBImg, MUSIC_FOOTER_B_PHOTO);
                } else {
                    musicFooterComp.setBounds(628 + 102 + 62, 492, 808 - 628 - 62, 503 - 492 + 20);
                    footer.changeText(BasicRenderer.btnBImg, MUSIC_FOOTER_B_PHOTO_VIEWER);
                }
            }
            add(musicFooterComp);
            musicFooterComp.start();
            repaint();
        } else {
            remove(musicFooterComp);
            musicFooterComp.stop();
            footer.changeText(BasicRenderer.btnBImg, BasicRenderer.btnMPlayerTxt);
            repaint();
        }
    }

    /**
     * D-Option canceled.
     */
    public void canceled() {
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * D-Option Selected.
     *
     * @param item the item
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }
        if (item.equals(OPT_PHOTO) || item.equals(OPT_MUSIC) || item.equals(OPT_VIDEO)) {
            int menuIndex = MENU_VIDEO;
            if (item.equals(OPT_PHOTO)) {
                menuIndex = MENU_PHOTO;
            } else if (item.equals(OPT_MUSIC)) { menuIndex = MENU_MUSIC; }
            goToContentUI(menuIndex);
        } else if (item.equals(OPT_PREFERENCE)) {
            goToPreferenceUI();
        } else if (item.equals(OPT_CHANGE_DEV)) {
            //musicPlayerPopup.dispose();
            SceneManager.getInstance().goToScene(SceneManager.SC_INIT, false, null);
        } else if (item.equals(OPT_HELP)) {
            SceneManager.getInstance().requestStartHelpApp();
        }
    }

    /**
     * Go to content ui.
     *
     * @param menuFocus the menu focus
     */
    public void goToContentUI(int menuFocus) {
        int totalCount = contentRoot[menuFocus].getTotalCount();
        if (totalCount > 0) {
            //Object[] values = {getCurrentSceneId(), String.valueOf(menuFocus), null};
            int sceneId = SceneManager.SC_CONTENT_LIST;
            if (menuFocus == MENU_MUSIC) { sceneId = SceneManager.SC_MUSIC_LIST; }
            SceneManager.getInstance().goToScene(sceneId, true, new Integer(menuFocus));
        } else {
            notiPopup.setMode(menuFocus);
            addPopup(notiPopup);
        }
    }

    /**
     * Go to preference ui.
     */
    protected void goToPreferenceUI() {
        SceneManager.getInstance().goToScene(SceneManager.SC_PREFERENCE, true, null);
    }

    /**
     * Gets the init and the last number for wall.
     *
     * @param focus the focus
     * @param focusPosition the focus position
     * @param totalCount the total count
     * @param itemsPerPage the items per page
     * @return the init and the last number
     */
    public static int[] getInitLastNumberForWall(int focus, int focusPosition, int totalCount, int itemsPerPage) {
        int initNum = focus - focusPosition;
        int lastNum = initNum + itemsPerPage + (itemsPerPage / 2) - 1;
        if (lastNum > totalCount - 1) { lastNum = totalCount - 1; }
        return new int[] {initNum, lastNum};
    }

    /**
     * Gets the init and the last number for list.
     *
     * @param focus the focus
     * @param focusPosition the focus position
     * @param totalCount the total count
     * @param itemsPerPage the items per page
     * @return the init and the last number
     */
    public static int[] getInitLastNumber(int focus, int focusPosition, int totalCount, int itemsPerPage) {
        int initNum = focus - focusPosition;
        int lastNum = 0;
        lastNum = focus + itemsPerPage - 1 - focusPosition;
        if (lastNum >= totalCount) { lastNum = totalCount - 1; }
        return new int[] {initNum, lastNum};
    }

    /**
     * Gets the image.
     *
     * @param key the key
     * @param imgUrl the image url
     * @param data the data
     * @param size the size for image
     * @return the Image object
     */
    public Image getContentImage(String key, String imgUrl, byte[] data, int[] size) {
        Logger.debug(this, "getContentImage()-imgUrl : " + imgUrl);
        if (imgUrl == null || imgUrl.length() == 0) {
            Logger.error(this, "getContentImage()-image url is empty.");
            return null;
        }
        //Image img = conImgPool.getImage(imgUrl, size);
        Image img = conImgPool.getImage(key);
        if (img == null) {
            try {
                img = conImgPool.getImage(key, data, size, imgUrl);
                if (!checkVisible()) {
                    conImgPool.clear();
                    return null;
                }
                /*byte[] imgByte = URLRequestor.getBytes(imgUrl, null);
                if (!checkVisible()) { return null; }
                if (imgByte != null) {
                    img = conImgPool.getImage(imgUrl, imgByte, size);
                    if (!checkVisible()) {
                        conImgPool.clear();
                        return null;
                    }
                } else { Logger.error(this, "getContentImage()-" + imgUrl + " : cannot get image from server."); }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return img;
    }

    /**
     * Check visibility.
     *
     * @return true, if successful
     */
    public boolean checkVisible() {
        boolean isVisible = isVisible() && SceneManager.getInstance().curSceneId != SceneManager.SC_INVALID;
        if (!isVisible) {
            HNHandler.getInstance().dispose();
            stop();
            dispose();
        }
        return isVisible;
    }

    /**
     * Show NotiPopup.
     *
     * @param menuType the menu type
     */
    private void showNotiPopup(int menuType) {
        notiPopup.setMode(menuType);
        addPopup(notiPopup);
    }

    /**
     * Gets the playing music name.
     *
     * @return the playing music name
     */
    private String getPlayingMusicName() {
        String name = "";
        int musicType = musicPlayerPopup.playMusicType;
        if (musicType == MusicPlayerPopup.PAUSE_MY_MUSIC
                || musicType == MusicPlayerPopup.PLAY_MY_MUSIC) {
            Vector list = musicPlayerPopup.playMusicList;
            if (list == null || list.isEmpty()) { return name; }
            Content content = (Content) list.elementAt(musicPlayerPopup.playMusicFocus);
            name = musicFooterComp.getName(content.getArtist(), content.getName());
        } else if (musicPlayerPopup.galaxieChData != null) {
            try {
                name = musicFooterComp.getName(musicPlayerPopup.galaxieChData.getCurrentArtist(),
                        musicPlayerPopup.galaxieChData.getCurrentTitle());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return name;
    }

    /**
     * Checks if is last line for grid.
     *
     * @param totalCount the total count
     * @param focus the focus
     * @param imagesPerLine the images per line
     * @return true, if is last line for grid
     */
    public boolean isLastLineForGrid(int totalCount, int focus, int imagesPerLine) {
        return focus / imagesPerLine == (totalCount - 1) / imagesPerLine;
    }

    /**
     * Go to search content.
     *
     * @param selectedSearchContent the selected search content
     */
    public void goToSearchContent(Content selectedSearchContent) {
        Logger.debug(this, "called goToSearchContent()-selectedSearchContent : " + selectedSearchContent);
        if (selectedSearchContent == null) { return; }
        searchContent = selectedSearchContent;
        int menuType = searchContent.getMenuType();
        if (menuType == MENU_VIDEO) {
            SceneManager.getInstance().goToScene(SceneManager.SC_VIDEO_PLAY, true, searchContent);
        } else if (menuType == MENU_PHOTO) {
            SceneManager.getInstance().goToScene(SceneManager.SC_PHOTO_PLAY, true, searchContent);
        } else {
            int musicSeq = searchContent.getMusicSeq();
            if (musicSeq == SEQ_ALBUM || musicSeq == SEQ_ARTIST || musicSeq == SEQ_PLAYLISTS) {
                SceneManager.getInstance().goToScene(SceneManager.SC_MUSIC_SEARCH_LIST, true, new Integer(MENU_MUSIC));
            } else {
                CommunicationManager.getInstance().showLoadingAnimation();
                new Thread("BasicUI|goToSearchContent") {
                    public void run() {
                        boolean isPlay = musicPlayerPopup.playMusic(searchContent, 0, false,
                                searchContent.getMusicSeq(), "", false);
                        CommunicationManager.getInstance().hideLoadingAnimation();
                        if (isPlay) {
                            if (!checkVisible()) { return; }
                            EventQueue.invokeLater(new Runnable() {
                                public void run() {
                                    musicPlayerPopup.resetFocus();
                                    addPopup(musicPlayerPopup);
                                }
                            });
                        } else { showConnectErrorPopup(); }
                    }
                } .start();
            }
        }
    }

    /**
     * Reset root children.
     */
    protected void resetRootChildren() {
        for (int i = 0; contentRoot != null && i < contentRoot.length; i++) {
            if (contentRoot[i] != null) { contentRoot[i].setChildren(null); }
        }
        for (int i = 0; musicRoot != null && i < musicRoot.length; i++) {
            if (musicRoot[i] != null) { musicRoot[i].setChildren(null); }
        }
    }

    /**
     * Clear all temp data.
     */
    protected void clearAllTempData() {
        int[] listId = {SceneManager.SC_CONTENT_LIST, SceneManager.SC_MUSIC_LIST, SceneManager.SC_MUSIC_SEARCH_LIST};
        for (int i = 0; i < listId.length; i++) {
            BasicUI scene = SceneManager.getInstance().scenes[listId[i]];
            if (scene != null) { scene.clearTempData(); }
        }
    }

    /**
     * The Class PopupBgComp is for background when popup is showed.
     */
    protected class PopupBgComp extends Component {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /**
         * constructor.
         */
        public PopupBgComp() {
            setBounds(0, 0, 960, 540);
        }

        /**
         * paint.
         *
         * @param g the g
         */
        public void paint(Graphics g) {
            g.setColor(Rs.DVB80C12);
            g.fillRect(0, 0, 960, 540);
        }
    }

    /**
     * The Class MusicFooterComp is for footer when music is played.
     */
    protected class MusicFooterComp extends Component implements Runnable {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;
        /** The thread. */
        private Thread thread = null;
        /** The FontMetrics. */
        private FontMetrics fm = FontResource.getFontMetrics(Rs.F17);
        /** The start pos x. */
        private int startPosX = 0;
        /** The current name. */
        private String currentName = null;

        /**
         * constructor.
         */
        public MusicFooterComp() {
            setBounds(628, 492, 808 - 628, 503 - 492 + 20);
        }

        /**
         * start thread.
         */
        public void start() {
            Logger.debug(this, "called start()");
            startPosX = 0;
            if (thread == null) {
                thread = new Thread(this, "BasicUI-DataLoadingThread");
                thread.start();
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            startPosX = 0;
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                try {
                    Thread.sleep(500L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null) { break; }
                int musicType = musicPlayerPopup.playMusicType;
                SceneManager sceneManager = SceneManager.getInstance();
                int curSceneId = sceneManager.curSceneId;
                //if (musicType != MusicPlayerPopup.PLAY_NO && curSceneId == SceneManager.SC_MUSIC_LIST) {
                if (musicType != MusicPlayerPopup.PLAY_NO
                        && curSceneId != SceneManager.SC_INVALID
                        && curSceneId != SceneManager.SC_INIT && curSceneId != SceneManager.SC_PREFERENCE
                        && curSceneId != SceneManager.SC_VIDEO_PLAY) {
                    String name = getPlayingMusicName();
                    if (name != null && name.length() > 0) {
                        if (!name.equals(currentName)) {
                            currentName = name;
                            startPosX = 0;
                        } else {
                            int limitWidth = getWidth();
                            int textWidth = fm.stringWidth(name);
                            //startPosX -= 10;
                            //if (startPosX + textWidth < 0) { startPosX = 808 - 628; }
                            if (textWidth > limitWidth) {
                                startPosX -= 10;
                                if (startPosX + textWidth < 0) { startPosX = limitWidth; }
                            } else { startPosX = 0; }
                        }
                    } else {
                        currentName = name;
                        startPosX = 0;
                    }
                    repaint();
                } else {
                    startPosX = 0;
                    currentName = null;
                }
            }
        }

        /**
         * Gets the name.
         *
         * @param artist the artist
         * @param title the title
         * @return the name
         */
        public String getName(String artist, String title) {
            String name = "";
            if (artist != null) { name += artist; }
            if (title != null) {
                if (artist != null) { name += " - "; }
                name += title;
            }
            return name;
        }

        /**
         * paint.
         *
         * @param g the g
         */
        public void paint(Graphics g) {
            if (currentName != null && thread != null) {
                g.setFont(Rs.F17);
                if (SceneManager.getInstance().curSceneId == SceneManager.SC_PHOTO_PLAY) {
                    g.setColor(Rs.C0);
                    g.drawString(currentName, startPosX + 1, 503 - 492 + 1);
                }
                g.setColor(Rs.C241);
                g.drawString(currentName, startPosX, 503 - 492);
                // progress bar
                int musicType = musicPlayerPopup.playMusicType;
                if (musicType == MusicPlayerPopup.PAUSE_MY_MUSIC || musicType == MusicPlayerPopup.PLAY_MY_MUSIC) {
                    Content focusContent =
                        (Content) musicPlayerPopup.playMusicList.elementAt(musicPlayerPopup.playMusicFocus);
                    if (focusContent != null) {
                        int barWidth = getWidth();
                        long duration = focusContent.getDurationLong();
                        if (duration <= 0) { duration = MediaPlayHandler.getInstance().getDurationLong(); }
                        int curWidth = 0;
                        if (duration > 0) { curWidth = getWidth(musicPlayerPopup.playTime, 0, duration, barWidth); }
                        g.setColor(Rs.C85);
                        int baseY = 503 - 492 + 5;
                        g.fillRect(0, baseY, barWidth, 3);
                        g.setColor(Rs.C255213000);
                        g.fillRect(0, baseY, curWidth, 3);
                        int pointPosX = curWidth - 4;
                        if (curWidth == 0) {
                            pointPosX += 1;
                        } else if (barWidth - curWidth < 5) {
                            pointPosX -= (5 + (curWidth - barWidth));
                        }
                        g.drawImage(BasicRenderer.progressPointImg, pointPosX, baseY - 3, this);
                    }
                }
            }
        }

        /**
         * Gets the width.
         *
         * @param time the time
         * @param from the from
         * @param to the to
         * @param totalWidth the total width
         * @return the width
         */
        private int getWidth(long time, long from, long to, int totalWidth) {
            long dur = to - from;
            long cur = time - from;
            return Math.min(totalWidth, Math.max(0, (int) (cur * totalWidth / dur)));
        }
    }

    /**
     * <code>DataLoadingThread</code> This class process datas loaded in queue.
     *
     * @since   2011.02.25
     * @version 1.0, 2010.02.25
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class DataLoadingThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = null;

        /**
         * This is constructor to define variables.
         */
        public DataLoadingThread() {
            queue = new ArrayList();
        }

        /**
         * set Object for processing into queue.
         * @param obj   Object for processing.
         */
        public void setObj(Object[] obj) {
            Logger.debug(this, "called setObj()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "BasicUI-DataLoadingThread");
                thread.start();
            }

            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    Logger.debug(this, "run() - queue.add");
                    queue.notify();
                    Logger.debug(this, "run() - request queue notify");
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.debug(this, "run() - thread : " + thread);
                if (thread == null) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object[] obj = (Object[]) queue.remove(0);
                    String procId = (String) obj[0];
                    Logger.debug(this, "run() - procId : " + procId);
                    Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                    if (queue.isEmpty()) { notifyFromDataLoadingThread(obj, true); }
                }
            }
            queue.clear();
        }
    }

    /**
     * Load paging data.
     *
     * @param curNode the cur node
     * @param menuFocus the menu focus
     * @param pageNum the page num
     * @param actionType the action type
     */
    public void loadPagingData(Content curNode, int menuFocus, int pageNum, String actionType) {
        if (pagingDataThread == null) { return; }
        pagingDataThread.loadData(new Object[]{curNode, new Integer(menuFocus), new Integer(pageNum),
                actionType});
    }

    /**
     * Stop paging thread.
     */
    protected void stopPagingThread() {
        if (pagingDataThread == null) { return; }
        pagingDataThread.stop();
    }

    /**
     * Gets the number for page.
     *
     * @param totalCount the total count
     * @param pageNum the page num
     * @return the number for page
     */
    protected int[] getNumberForPage(int totalCount, int pageNum) {
        int startIndex = pageNum * Config.loadDataNumber;
        int requestCount = Config.loadDataNumber;
        if (startIndex + Config.loadDataNumber > totalCount) { requestCount = totalCount - startIndex; }
        return new int[]{startIndex, requestCount};
    }

    /**
     * Gets the page number.
     *
     * @param focus the focus
     * @return the page number
     */
    protected int getPageNumber(int focus) {
        if (focus < 0) { return 0; }
        return focus / Config.loadDataNumber;
    }

    /**
     * Gets the total page number.
     *
     * @param totalCount the total count
     * @return the total page number
     */
    protected int getLastPageNumber(int totalCount) {
        if (totalCount < 0) { return 0; }
        return (totalCount - 1) / Config.loadDataNumber;
    }

    /**
     * Load init paging data.
     *
     * @param reqNode the req node
     * @param focus the focus
     * @param menuFocus the menu focus
     */
    protected void loadInitPagingData(Content reqNode, int focus, int menuFocus) {
        int pageNum = getPageNumber(focus);
        int totalCount = reqNode.getChildCount();
        if (totalCount > Config.loadDataNumber) {
            int lastPageNum = getLastPageNumber(totalCount); //309
            Logger.debug(this, "loadInitPagingData()-cachePageNumber : " + Config.cachePageNumber);
            int cachePageNumber = Config.cachePageNumber; //21
            if (Config.cachePageNumber % 2 == 0) { cachePageNumber++; }
            int loopCount = Config.cachePageNumber / 2; // 10
            if (lastPageNum + 1 < cachePageNumber) { loopCount = (lastPageNum + 1) / 2; }
            for (int i = 1; i <= loopCount; i++) { // 10 loop
                int addPage = pageNum + i;
                if (addPage > lastPageNum) { addPage = addPage - lastPageNum - 1; }
                loadPagingData(reqNode, menuFocus, addPage, PagingThread.TYPE_LOAD);

                addPage = pageNum - i;
                if (addPage < 0) { addPage = lastPageNum + addPage + 1; }
                loadPagingData(reqNode, menuFocus, addPage, PagingThread.TYPE_LOAD);
            }
        }
    }

    /**
     * Check list page.
     *
     * @param reqFocus the req focus
     * @param reqPosition the req position
     * @param reqNode the req node
     * @param reqMenuFocus the req menu focus
     * @param reqSortType the req sort type
     * @param reqUIType the req ui type
     * @return true, if successful
     */
    protected boolean checkListPage(int reqFocus, int reqPosition, Content reqNode, int reqMenuFocus, int reqSortType,
            int reqUIType) {
        if (reqFocus > 0) {
            int addIndex = 0;
            if (reqMenuFocus == MENU_MUSIC && reqNode.getChildCount() + 1 == reqNode.getChildren().size()) {
                addIndex = 1;
            }
            int pageNum = getPageNumber(reqFocus - addIndex);
            int[] numbers = getNumberForPage(reqNode.getChildCount(), pageNum);
            int[] uiNum = BasicUI.getInitLastNumberForWall(reqFocus, reqPosition,
                    reqNode.getChildCount(), IMAGES_PER_PAGE);
            if (reqUIType == UI_LIST) {
                uiNum = BasicUI.getInitLastNumber(reqFocus, reqPosition, reqNode.getChildCount(), ROWS_PER_PAGE);
            }
            if (uiNum[0] > 0) { uiNum[0] -= addIndex; }
            if (uiNum[1] > 0) { uiNum[1] -= addIndex; }
            Logger.debug(this, "notifyFromDataLoadingThread()-uiNum[0] : " + uiNum[0]);
            Logger.debug(this, "notifyFromDataLoadingThread()-uiNum[1] : " + uiNum[1]);
            Logger.debug(this, "notifyFromDataLoadingThread()-numbers[0] : " + numbers[0]);
            Logger.debug(this, "notifyFromDataLoadingThread()-numbers[1] : " + numbers[1]);
            if (uiNum[0] < numbers[0]) {
                if (!setTempPageData(pageNum - 1, reqNode, reqMenuFocus, reqSortType)) { return false; }
            }
            if (uiNum[1] > numbers[0] + numbers[1] - 1) {
                if (!setTempPageData(pageNum + 1, reqNode, reqMenuFocus, reqSortType)) { return false; }
            }
        }
        return true;
    }

    /**
     * Sets the temp data for photo.
     *
     * @param pageNum the page num
     * @param reqNode the req node
     * @param reqMenuFocus the req menu focus
     * @param reqSortType the req sort type
     * @return true, if successful
     */
    private boolean setTempPageData(int pageNum, Content reqNode, int reqMenuFocus, int reqSortType) {
        int[] numbers = getNumberForPage(reqNode.getChildCount(), pageNum);
        Vector list = HNHandler.getInstance().setChildNodeForPaging(reqNode, reqMenuFocus,
                reqSortType, numbers[0], numbers[1]);
        return setPagingData(reqNode, list, reqMenuFocus, pageNum, numbers);
    }

    /**
     * Sets the paging data.
     *
     * @param curNode the cur node
     * @param list the list
     * @param menuFocus the menu focus
     * @param pageNum the page num
     * @param numbers the numbers
     * @return true, if successful
     */
    protected boolean setPagingData(Content curNode, Vector list, int menuFocus, int pageNum, int[] numbers) {
        Hashtable browsePages = curNode.getBrowsePageNums();
        Hashtable errorPages = curNode.getErrorPageNums();
        if (list == null || list.size() != numbers[1]) {
            if (errorPages == null) { errorPages = new Hashtable(); }
            errorPages.put(String.valueOf(pageNum), "");
            curNode.setErrorPageNums(errorPages);
            Logger.debug(this, "errorPages.put : " + pageNum);
            if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                LogDisplayer.getInstance().setErrorPages(errorPages);
                repaint();
            }
        } else {
            Vector children = curNode.getChildren();
            if (children != null) {
                int addIndex = 0;
                if (menuFocus == MENU_MUSIC && curNode.getChildCount() + 1 == children.size()) {
                    addIndex = 1;
                }
                for (int i = 0; i < list.size(); i++) {
                    children.setElementAt(list.elementAt(i), numbers[0] + i + addIndex);
                }
                if (browsePages == null) { browsePages = new Hashtable(); }
                browsePages.put(String.valueOf(pageNum), "");
                curNode.setBrowsePageNums(browsePages);
                Logger.debug(this, "browsePages.put : " + pageNum);
                if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                    curNode.setLastLoadedPage(pageNum + 1);
                    LogDisplayer.getInstance().setLastLoadedPage(pageNum + 1);
                    repaint();
                }
                return true;
            }
        }
        return false;
    }

    /**
     * <code>PagingThread</code> This class process to bring rest datas by paging logic.
     *
     * @since   2012.08.24
     * @version 1.0, 2010.08.24
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class PagingThread implements Runnable {
        /** The Constant TYPE_LOAD. */
        public static final String TYPE_LOAD = "load";
        /** The Constant TYPE_REMOVE. */
        public static final String TYPE_REMOVE = "remove";
        /** The thread. */
        private Thread thread = null;
        /** The queue. */
        private ArrayList queue = new ArrayList();

        /**
         * Load data.
         *
         * @param obj the obj
         */
        public void loadData(Object[] obj) {
            Logger.debug(this, "called loadData()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "BasicUI-PagingThread");
                thread.start();
            }
            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    Logger.debug(this, "loadData() - queue.add");
                    queue.notify();
                    Logger.debug(this, "loadData() - request queue notify");
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            Logger.debug(this, "called stop()");
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            Thread startThread = thread;
            while (thread != null && startThread == thread) {
                try {
                    Thread.sleep(100L);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null || startThread != thread) { break; }
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Logger.debug(this, "run() - thread : " + thread);
                if (thread == null || startThread != thread) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object[] obj = (Object[]) queue.remove(0);
                    if (obj != null) {
                        Content curNode = (Content) obj[0];
                        int menuFocus = ((Integer) obj[1]).intValue();
                        int pageNum = ((Integer) obj[2]).intValue();
                        String actionType = (String) obj[3];
                        Logger.debug(this, "pageNum : " + pageNum);
                        Logger.debug(this, "actionType : " + actionType);
                        int[] numbers = getNumberForPage(curNode.getChildCount(), pageNum);
                        Hashtable browsePages = curNode.getBrowsePageNums();
                        Hashtable errorPages = curNode.getErrorPageNums();
                        if (TYPE_LOAD.equals(actionType)) {
                            if (browsePages != null && browsePages.containsKey(String.valueOf(pageNum))) { continue; }
                            if (errorPages != null && errorPages.containsKey(String.valueOf(pageNum))) { continue; }

                            Vector list = HNHandler.getInstance().setChildNodeForPaging(curNode, menuFocus,
                                    curNode.getChildSort(), numbers[0], numbers[1]);
                            if (thread == null || startThread != thread) { break; }
                            setPagingData(curNode, list, menuFocus, pageNum, numbers);
                        } else {
                            if (browsePages == null || !browsePages.containsKey(String.valueOf(pageNum))) { continue; }
                            Vector children = curNode.getChildren();
                            if (children != null) {
                                int addIndex = 0;
                                if (menuFocus == MENU_MUSIC && curNode.getChildCount() + 1 == children.size()) {
                                    addIndex = 1;
                                }
                                for (int i = numbers[0]; i < numbers[0] + numbers[1]; i++) {
                                    Content content = (Content) children.elementAt(i + addIndex);
                                    if (content != null) {
                                        conImgPool.remove(content.getId() + content.getIconUrl());
                                        Logger.debug(this, "removed image : " + (i + addIndex));
                                    }
                                    children.setElementAt(null, (i + addIndex));
                                    Logger.debug(this, "removed element in buffer : " +  (i + addIndex));
                                }
                                if (thread == null || startThread != thread) { break; }
                                if (browsePages != null) {
                                    browsePages.remove(String.valueOf(pageNum));
                                    Logger.debug(this, "browsePages.remove : " + pageNum);
                                    if (Config.enableDebugUI && LogDisplayer.getInstance().isLogShow) {
                                        curNode.setLastRemovedPage(pageNum + 1);
                                        LogDisplayer.getInstance().setLastRemovedPage(pageNum + 1);
                                        repaint();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

/*
 *  @(#)ConfirmPopup.java 1.0 2011.10.03
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ConfirmPopup</code> the popup for confrimation.
 *
 * @since   2011.10.03
 * @version $Revision: 1.6 $ $Date: 2011/10/19 01:55:46 $
 * @author  tklee
 */
public class ConfirmPopup extends Popup {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_APP_EXIT. */
    public static final int MODE_APP_EXIT = 0;

    /** The Constant BTN_OK. */
    private static final int BTN_OK = 0;
    /** The Constant BTN_CANCEL. */
    private static final int BTN_CANCEL = 1;

    /** The Constant GO_MENU. */
    public static final int GO_MENU = 0;
    /** The Constant GO_GUIDE. */
    public static final int GO_GUIDE = 1;
    /** The Constant GO_900. */
    public static final int GO_VOD = 2;

    /** The Constant NAME_GOING_APP. */
    //private static final String[] NAME_GOING_APP = {"Menu", "EPG", "VOD"};
    /** The value of going. */
    private int valueOfGoing = 0;
    /** The btn focus. */
    private int btnFocus = 0;

    /**
     * Initialize the Component.
     */
    public ConfirmPopup() {
        setBounds(276, 113, 410, 330);
        setRenderer(new PopupRenderer());
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() {
        valueOfGoing = 0;
    }

    /**
     * Sets the going value.
     *
     * @param value the new going value
     */
    public void setGoingValue(int value) {
        valueOfGoing = value;
    }

    /**
     * Reset focus.
     *
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        btnFocus = 0;
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (btnFocus > 0) {
                btnFocus--;
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (btnFocus == 0) {
                btnFocus++;
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            clickEffect.start(325 + (btnFocus * 157) - 276, 318 - 113, 558 - 403, 32);
            if (btnFocus == BTN_OK) {
                //SceneManager.getInstance().requestStartApp(NAME_GOING_APP[valueOfGoing]);
                if (valueOfGoing == Rs.KEY_EXIT || valueOfGoing == KeyCodes.LIVE) {
                    CommunicationManager.getInstance().exitToChannel();
                } else { CommunicationManager.getInstance().startUnboundAppWithHotKey(valueOfGoing); }
            } else { scene.requestExitConfirmPopupClose(); }
            break;
        case Rs.KEY_EXIT:
        case KeyCodes.LIVE:
            scene.requestExitConfirmPopupClose();
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.6 $ $Date: 2011/10/19 01:55:46 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The btn focus img. */
        private Image btnFocusImg = null;
        /** The noti icon img. */
        private Image notiIconImg = null;
        /** The focus dim img. */
        private Image focusDimImg = null;

        /** The title txt. */
        private String[] titleTxt = new String[1];
        /** The desc txt. */
        private String[] descTxt = new String[1];
        /** The btn cancel txt. */
        private String[] btnTxt = new String[2];

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("pop_sha.png");
            bgMImg = getImage("05_pop_glow_m.png");
            bgTImg = getImage("05_pop_glow_t.png");
            bgBImg = getImage("05_pop_glow_b.png");
            gapImg = getImage("pop_gap_379.png");
            highImg = getImage("pop_high_350.png");
            btnFocusImg = getImage("05_focus.png");
            notiIconImg = getImage("icon_noti_or.png");
            focusDimImg = dataCenter.getImage("05_focus_dim.png");
            super.prepare(c);
            titleTxt[0] = getString("confirm");
            descTxt[0] = getString("leavingDesc");
            btnTxt[0] = getString("btnYes");
            btnTxt[1] = getString("btnNo");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -113);
            //bg
            g.drawImage(shadowImg, 306, 364, 350, 79, c);
            g.drawImage(bgMImg, 276, 193, 410, 124, c);
            g.drawImage(bgTImg, 276, 113, c);
            g.drawImage(bgBImg, 276, 317, c);
            //
            g.setColor(Rs.C35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(gapImg, 285, 181, c);
            g.drawImage(highImg, 306, 143, c);
            g.setFont(Rs.F24);
            g.setColor(Rs.C252202004);
            GraphicUtil.drawStringCenter(g, titleTxt[mode], 482, 169);
            //
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            String[] desc = TextUtil.split(descTxt[mode], g.getFontMetrics(), 614 - 346);
            int baseY = 259 - ((desc.length - 1) * 10);
            for (int i = 0; i < desc.length; i++) {
                int addPos = i * 20;
                GraphicUtil.drawStringCenter(g, desc[i], 483, baseY + addPos);
            }
            //btn
            g.setColor(Rs.C3);
            for (int i = 0; i < btnTxt.length; i++) {
                if (i == btnFocus) {
                    g.drawImage(btnFocusImg, 325 + (i * 157), 318, c);
                } else { g.drawImage(focusDimImg, 325 + (i * 157), 318, c); }
                GraphicUtil.drawStringCenter(g, btnTxt[i], 402 + (i * 157), 339);
            }
            g.translate(276, 113);
        }
    }
}

/*
 *  @(#)PreviewPopup.java 1.0 2011.09.23
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.ui.popup;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * <code>PreviewPopup</code> the popup for preview.
 *
 * @since   2011.09.23
 * @version $Revision: 1.5 $ $Date: 2012/09/25 05:03:40 $
 * @author  tklee
 */
public class PreviewPopup extends Popup implements TVTimerWentOffListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;
    /** The Constant SLEEP_TIME. */
    private static final long SLEEP_TIME = 1000L;

    /** The renderer. */
    private PopupRenderer renderer = null;
    /** The timer spec. */
    private TVTimerSpec timerSpec = null;
    /** The imgae label. */
    private ImageLabel imgaeLabel = null;

    /** The is timer. */
    private boolean isTimer = false;
    /** The timer count. */
    private int timerCount = 0;
    /** The photo focus. */
    private int photoFocus = 0;
    /** The slideshow interval. */
    //private int slideshowInterval = 5;
    /** The flip option. */
    private int flipOption = ImageLabel.FLIP_FROM_TOP;

    /**
     * Initialize the Component.
     */
    public PreviewPopup() {
        setBounds(276, 113, 410, 330);
        renderer = new PopupRenderer();
        setRenderer(renderer);
        timerSpec = new TVTimerSpec();
        timerSpec.setDelayTime(SLEEP_TIME);
        timerSpec.setRepeat(true);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        timerCount = 0;
        photoFocus = 0;
        flipOption = ImageLabel.FLIP_FROM_TOP;
        /*slideshowInterval = 5;
        String inervalStr = PreferenceProxy.getInstance().slideshowInterval;
        if (PreferenceProxy.INTERVAL_3.equals(inervalStr)) {
            slideshowInterval = 3;
        } else if (PreferenceProxy.INTERVAL_5.equals(inervalStr)) {
            slideshowInterval = 5;
        } else if (PreferenceProxy.INTERVAL_10.equals(inervalStr)) {
            slideshowInterval = 10;
        }*/
        prepare();
        showNextImage();
        startTimer();
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.pcs.ui.popup.Popup#stop()
     */
    public void stop() {
        stopTimer();
        if (imgaeLabel != null) { remove(imgaeLabel); }
        imgaeLabel = null;
    }

    /**
     * startTimer.
     */
    private void startTimer() {
        if (isTimer) { return; }
        try {
            timerSpec.addTVTimerWentOffListener(this);
            TVTimer.getTimer().scheduleTimerSpec(timerSpec);
            isTimer = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop Timer.
     */
    private void stopTimer() {
        if (!isTimer) { return; }
        if (timerSpec != null) {
            timerSpec.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(timerSpec);
            isTimer = false;
        }
    }

    /**
     * implements the method of TVTimerWentOffListener.
     *
     * @param e the TVTimerWentOffEvent
     */
    public synchronized void timerWentOff(TVTimerWentOffEvent e) {
        if (!isTimer || timerCount < 0) { return; }
        timerCount++;
        //if (timerCount >= slideshowInterval) {
        if (timerCount >= Config.previewSlideshowIntervalSec) {
            timerCount = -1;
            if (isTimer) {
                photoFocus = (photoFocus + 1) % 2;
                showNextImage();
            }
        }
    }

    /**
     * Show next image using Thread.
     */
    private void showNextImage() {
        Logger.debug(this, "showNextImage-photoFocus : " + photoFocus);
        if (imgaeLabel == null) {
            imgaeLabel = new ImageLabel(renderer.previewImg[photoFocus]);
            imgaeLabel.setBounds(391 - 276, 199 - 113, 180, 101);
            imgaeLabel.setAlign(true);
            add(imgaeLabel);
            repaint();
            timerCount = 0;
        } else {
            int effectType = ImageLabel.NONE_EFFECT;
            String slideshowEffect = PreferenceProxy.getInstance().slideshowEffect;
            if (slideshowEffect != null && !slideshowEffect.equals(PreferenceProxy.EFFECT_NO)) {
                if (slideshowEffect.equals(PreferenceProxy.EFFECT_RANDOM)) {
                    effectType = (int) (Math.random() * 6) + 1;
                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_FADE)) {
                    effectType = ImageLabel.FADE_EFFECT;
                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_DISSOLVE)) {
                    effectType = ImageLabel.DISSOLVE_EFFECT;
                } else if (slideshowEffect.equals(PreferenceProxy.EFFECT_PAGEFLIP)) {
                    if (flipOption >= ImageLabel.FLIP_FROM_TOP && flipOption < ImageLabel.FLIP_FROM_RIGHT) {
                        flipOption++;
                    } else { flipOption = ImageLabel.FLIP_FROM_TOP; }
                    effectType = flipOption;
                }
            }
            if (effectType == ImageLabel.NONE_EFFECT) {
                imgaeLabel.replaceImage(renderer.previewImg[photoFocus], effectType);
                repaint();
                timerCount = 0;
            } else {
                final int replaceType = effectType;
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        imgaeLabel.replaceImage(renderer.previewImg[photoFocus], replaceType);
                        timerCount = 0;
                        //imgaeLabel.repaint();
                    }
                });
                return;
            }
        }
        /*new Thread("PreviewPopup|showNextImage()") {
            public void run() {
                if (!isTimer) { return; }
            }
        } .start();*/
    }

    /**
     * process key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_ENTER:
            clickEffect.start(403 - 276, 319 - 113, 558 - 403, 32);
            scene.requestPopupClose();
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * <code>PopupRenderer</code> The class to paint a GUI.
     *
     * @since   2011.05.20
     * @version $Revision: 1.5 $ $Date: 2012/09/25 05:03:40 $
     * @author  tklee
     */
    public class PopupRenderer extends BasicPopupRenderer {
        /** The shadow img. */
        private Image shadowImg = null;
        /** The bg m img. */
        private Image bgMImg = null;
        /** The bg t img. */
        private Image bgTImg = null;
        /** The bg b img. */
        private Image bgBImg = null;
        /** The gap img. */
        private Image gapImg = null;
        /** The high img. */
        private Image highImg = null;
        /** The preview bg img. */
        private Image previewBgImg = null;
        /** The preview img. */
        public Image[] previewImg = new Image[2];
        /** The btn focus img. */
        private Image btnFocusImg = null;

        /** The title txt. */
        private String titleTxt = null;
        /** The btn cancel txt. */
        private String btnOKTxt = null;

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) {
            shadowImg = getImage("05_popup_sha.png");
            bgMImg = getImage("05_bgglow_m.png");
            bgTImg = getImage("05_bgglow_t.png");
            bgBImg = getImage("05_bgglow_b.png");
            gapImg = getImage("05_sep.png");
            highImg = getImage("05_high.png");
            previewBgImg = getImage("09_pre_bg.png");
            previewImg[0] = getImage("09_pre_a.png");
            previewImg[1] = getImage("09_pre_b.png");
            btnFocusImg = getImage("05_focus.png");
            super.prepare(c);

            titleTxt = getString("preview");
            btnOKTxt = getString("btnOK");
        }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            g.translate(-276, -113);
            //bg
            g.drawImage(bgMImg, 276, 168, 410, 172, c);
            g.drawImage(bgTImg, 276, 113, c);
            g.drawImage(bgBImg, 276, 340, c);
            g.drawImage(shadowImg, 304, 364, c);
            g.setColor(Rs.C35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(gapImg, 285, 181, c);
            g.drawImage(highImg, 306, 143, c);
            g.drawImage(previewBgImg, 385, 193, c);

            //title
            g.setFont(Rs.F24);
            g.setColor(Rs.C255203000);
            GraphicUtil.drawStringCenter(g, titleTxt, 481, 169);
            g.setFont(Rs.F18);
            g.setColor(Rs.C255);
            //btn
            g.setColor(Rs.C3);
            g.drawImage(btnFocusImg, 403, 319, c);
            GraphicUtil.drawStringCenter(g, btnOKTxt, 482, 340);
            g.translate(276, 113);
        }
    }
}

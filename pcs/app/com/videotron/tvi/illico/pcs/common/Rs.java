/*
 *  @(#)Rs.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.common;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class is the Resource class.
 * @version $Revision: 1.9 $ $Date: 2012/09/18 02:42:18 $
 * @author tklee
 */
public final class Rs {
    /** The Constant F15. */
    public static final Font F15 = FontResource.BLENDER.getFont(15);
    /** The Constant F16. */
    public static final Font F16 = FontResource.BLENDER.getFont(16);
    /** The Constant F17. */
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    /** The Constant F18. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    /** The Constant F19. */
    public static final Font F19 = FontResource.BLENDER.getFont(19);
    /** The Constant F24. */
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    /** The Constant F26. */
    public static final Font F26 = FontResource.BLENDER.getFont(26);
    /** The Constant F35. */
    public static final Font F35 = FontResource.BLENDER.getFont(35);
    /** The Constant F40. */
    public static final Font F40 = FontResource.BLENDER.getFont(40);
    /** The Constant F20. */
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    /** The Constant F22. */
    public static final Font F22 = FontResource.BLENDER.getFont(22);
    /** The Constant F15_D. */
    public static final Font F15_D = FontResource.DINMED.getFont(15);

    /** The Constant C0. */
    public static final Color C0 = new Color(0, 0, 0);
    /** The Constant C1. */
    public static final Color C1 = new Color(1, 1, 1);
    /** The Constant C3. */
    public static final Color C3 = new Color(3, 3, 3);
    /** The Constant C35. */
    public static final Color C35 = new Color(35, 35, 35);
    /** The Constant C45. */
    public static final Color C45 = new Color(45, 45, 45);
    /** The Constant C85. */
    public static final Color C85 = new Color(85, 85, 85);
    /** The Constant C78. */
    public static final Color C78 = new Color(78, 78, 78);
    /** The Constant C255. */
    public static final Color C255 = new Color(255, 255, 255);
    /** The Constant C216. */
    public static final Color C216 = new Color(216, 216, 216);
    /** The Constant C182. */
    public static final Color C182 = new Color(182, 182, 182);
    /** The Constant C239. */
    public static final Color C239 = new Color(239, 239, 239);
    /** The Constant C124. */
    public static final Color C124 = new Color(124, 124, 124);
    /** The Constant C133. */
    public static final Color C133 = new Color(133, 133, 133);
    /** The Constant C200. */
    public static final Color C200 = new Color(200, 200, 200);
    /** The Constant C230. */
    public static final Color C230 = new Color(230, 230, 230);
    /** The Constant C160. */
    public static final Color C160 = new Color(160, 160, 160);
    /** The Constant C167. */
    public static final Color C167 = new Color(167, 167, 167);
    /** The Constant C130. */
    public static final Color C130 = new Color(130, 130, 130);
    /** The Constant C189. */
    public static final Color C189 = new Color(189, 189, 189);
    /** The Constant C150. */
    public static final Color C150 = new Color(150, 150, 150);
    /** The Constant C180. */
    public static final Color C180 = new Color(180, 180, 180);
    /** The Constant C190. */
    public static final Color C190 = new Color(190, 190, 190);
    /** The Constant C210. */
    public static final Color C210 = new Color(210, 210, 210);
    /** The Constant C240. */
    public static final Color C240 = new Color(240, 240, 240);
    /** The Constant C241. */
    public static final Color C241 = new Color(241, 241, 241);
    /** The Constant C224. */
    public static final Color C224 = new Color(224, 224, 224);
    /** The Constant C045044044. */
    public static final Color C045044044 = new Color(45, 44, 44);
    /** The Constant C046046045. */
    public static final Color C046046045 = new Color(46, 46, 45);
    /** The Constant C215214214. */
    public static final Color C215214214 = new Color(215, 214, 214);
    /** The Constant C252202004. */
    public static final Color C252202004 = new Color(252, 202, 4);
    /** The Constant C255213000. */
    public static final Color C255213000 = new Color(255, 213, 0);
    /** The Constant C236211143. */
    public static final Color C236211143 = new Color(236, 211, 143);
    /** The Constant C253203000. */
    public static final Color C253203000 = new Color(253, 203, 0);
    /** The Constant C255203000. */
    public static final Color C255203000 = new Color(255, 203, 0);
    /** The Constant C227227228. */
    public static final Color C227227228 = new Color(227, 227, 228);
    /** The Constant C214182061. */
    public static final Color C214182061 = new Color(214, 182, 61);
    /** The Constant C218205162. */
    public static final Color C218205162 = new Color(218, 205, 162);
    /** The Constant C027024012. */
    public static final Color C027024012 = new Color(27, 24, 12);
    /** The Constant C214182055. */
    public static final Color C214182055 = new Color(214, 182, 55);
    /** The Constant C126125125. */
    public static final Color C126125125 = new Color(126, 125, 125);
    /** The Constant C254196014. */
    public static final Color C254196014 = new Color(254, 196, 14);
    /** The Constant C228026034. */
    public static final Color C228026034 = new Color(228, 26, 34);
    /** The Constant C195. */
    public static final Color C195 = new Color(195, 195, 195);
    /** The Constant C120. */
    public static final Color C120 = new Color(120, 120, 120);
    /** The Constant C202174097. */
    public static final Color C202174097 = new Color(202, 174, 97);
    /** The Constant C208206206. */
    public static final Color C208206206 = new Color(208, 206, 206);
    /** The Constant C55. */
    public static final Color C55 = new Color(55, 55, 55);
    /** The Constant C184. */
    public static final Color C184 = new Color(184, 184, 184);
    /** The Constant C41. */
    public static final Color C41 = new Color(41, 41, 41);
    /** The Constant C0211011013. */
    public static final Color C0211011013 = new Color(21, 11, 13);
    /** The Constant C255075079. */
    public static final Color C255075079 = new Color(255, 75, 79);
    /** The Constant DVB80C12. */
    public static final Color DVB80C12 = new DVBColor(12, 12, 12, 255 * 80 / 100);
    /** The Constant DVB90C22. */
    public static final Color DVB90C22 = new DVBColor(22, 22, 22, 255 * 90 / 100);
    /** The Constant DVB40C255. */
    public static final Color DVB40C255 = new DVBColor(255, 255, 255, 255 * 40 / 100);
    /** The Constant DVB179C102. */
    public static final Color DVB179C102 = new DVBColor(102, 102, 102, 179);
    /** The Constant DVB200C170. */
    public static final Color DVB200C170 = new DVBColor(170, 170, 170, 200);
    /** The Constant DVB90C255. */
    public static final Color DVB60C255 = new DVBColor(255, 255, 255, 255 * 60 / 100);

    /** Screen Size. */
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, 960, 540);
    /** The Constant NONE_BOUND. */
    public static final Rectangle NONE_BOUND = new Rectangle(0, 0, 0, 0);

    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = KeyEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = KeyEvent.VK_DOWN;
    /** The Constant KEY_ENTER. */
    public static final int KEY_ENTER = HRcEvent.VK_ENTER;
    /** The Constant KEY_PAGE_UP. */
    public static final int KEY_PAGE_UP = HRcEvent.VK_PAGE_UP;
        /** The Constant KEY_PAGE_DOWN. */
    public static final int KEY_PAGE_DOWN = HRcEvent.VK_PAGE_DOWN;
    /** The Constant KEY_BACK. */
    public static final int KEY_BACK = KeyCodes.LAST;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    /** The Constant KEY_CH_UP. */
    public static final int KEY_CH_UP = OCRcEvent.VK_CHANNEL_UP;
    /** The Constant KEY_CH_DOWN. */
    public static final int KEY_CH_DOWN = OCRcEvent.VK_CHANNEL_DOWN;
    /** The Constant KEY_GUIDE. */
    public static final int KEY_GUIDE = HRcEvent.VK_GUIDE;

    /**
     * Instantiates a contructor.
     */
    private Rs() { }
}

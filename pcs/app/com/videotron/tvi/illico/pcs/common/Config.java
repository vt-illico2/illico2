/*
 *  @(#)Config.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.pcs.common;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;

/**
 * <code>Config</code> the class for application configuration.
 *
 * @version $Revision: 1.94 $ $Date: 2012/10/18 11:29:56 $
 * @author tklee
 */
public final class Config {
    /** Application Version. */
    public static final String APP_VERSION = "1.1.15.1";
    /** Application Name. */
    public static final String APP_NAME = "PCS";

    /** The language. */
    public static String language = Definitions.LANGUAGE_FRENCH;

    /** The musicplayer show sec. */
    public static int musicplayerShowSec = 10;
    /** The autoscroll sleep sec. */
    public static int autoscrollSleepSec = 2;
    /** The default screensaver sleep sec. */
    public static int defaultScreensaverSleepSec = 600;
    /** The folder rotation sec. */
    public static int folderRotationSec = 2;
    /** The dlna request timeout sec. */
    public static int dlnaRequestTimeoutSec = 20;
    /** The preview slideshow interval sec. */
    public static int previewSlideshowIntervalSec = 2;
    /** The set moca pw. */
    public static boolean setMocaPw = false;
    /** The load data number. */
    public static int loadDataNumber = 15;
    /** The cach page number. */
    public static int cachePageNumber = 13;
    /** The enable debug ui. */
    public static boolean enableDebugUI = false;
    /** The search max number. */
    public static int searchMaxNumber = 300;
    /** The search location priority. */
    public static int searchLocationPriority = 50;
    /** The photo max width. */
    public static int photoMaxWidth = 940;
    /** The photo max height. */
    public static int photoMaxHeight = 520;
    /** The valid model. */
    //public static Hashtable validModel = null;

    /**
     * set configuration values.
     */
    public static void setConfigValues() {
        Logger.debug(Config.class, "called setConfigValues()");
        try {
            musicplayerShowSec = Integer.parseInt((String) DataCenter.getInstance().get("musicplayerShowSec"));
            autoscrollSleepSec = Integer.parseInt((String) DataCenter.getInstance().get("autoscrollSleepSec"));
            defaultScreensaverSleepSec =
                Integer.parseInt((String) DataCenter.getInstance().get("defaultScreensaverSleepSec"));
            folderRotationSec = Integer.parseInt((String) DataCenter.getInstance().get("folderRotationSec"));
            dlnaRequestTimeoutSec = Integer.parseInt((String) DataCenter.getInstance().get("dlnaRequestTimeoutSec"));
            previewSlideshowIntervalSec =
                Integer.parseInt((String) DataCenter.getInstance().get("previewSlideshowIntervalSec"));
            setMocaPw = DataCenter.getInstance().getBoolean("setMocaPw");
            loadDataNumber = Integer.parseInt((String) DataCenter.getInstance().get("loadDataNumber"));
            cachePageNumber = Integer.parseInt((String) DataCenter.getInstance().get("cachePageNumber"));
            enableDebugUI = DataCenter.getInstance().getBoolean("enableDebugUI");
            searchMaxNumber = Integer.parseInt((String) DataCenter.getInstance().get("searchMaxNumber"));
            searchLocationPriority = Integer.parseInt((String) DataCenter.getInstance().get("searchLocationPriority"));
            photoMaxWidth = Integer.parseInt((String) DataCenter.getInstance().get("photoMaxWidth"));
            photoMaxHeight = Integer.parseInt((String) DataCenter.getInstance().get("photoMaxHeight"));
            /*String validModelName = (String) DataCenter.getInstance().get("validModelName");
            if (validModelName != null && validModelName.length() != 0 && !validModelName.equalsIgnoreCase("all")) {
                String[] names = TextUtil.tokenize(validModelName, "^");
                validModel = new Hashtable();
                for (int i = 0; i < names.length; i++) {
                    Logger.debug(Config.class, "validModel : " + names[i]);
                    validModel.put(names[i].toLowerCase(), "");
                }
            }*/
            Logger.debug(Config.class, "musicplayerShowSec : " + musicplayerShowSec);
            Logger.debug(Config.class, "autoscrollSleepSec : " + autoscrollSleepSec);
            Logger.debug(Config.class, "defaultScreensaverSleepSec : " + defaultScreensaverSleepSec);
            Logger.debug(Config.class, "folderRotationSec : " + folderRotationSec);
            Logger.debug(Config.class, "dlnaRequestTimeoutSec : " + dlnaRequestTimeoutSec);
            Logger.debug(Config.class, "previewSlideshowIntervalSec : " + previewSlideshowIntervalSec);
            Logger.debug(Config.class, "setMocaPw : " + setMocaPw);
            Logger.debug(Config.class, "loadDataNumber : " + loadDataNumber);
            Logger.debug(Config.class, "cachePageNumber : " + cachePageNumber);
            Logger.debug(Config.class, "enableDebugUI : " + enableDebugUI);
            Logger.debug(Config.class, "searchMaxNumber : " + searchMaxNumber);
            Logger.debug(Config.class, "searchLocationPriority : " + searchLocationPriority);
            Logger.debug(Config.class, "photoMaxWidth : " + photoMaxWidth);
            Logger.debug(Config.class, "photoMaxHeight : " + photoMaxHeight);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * constructor.
     */
    private Config() { }
}

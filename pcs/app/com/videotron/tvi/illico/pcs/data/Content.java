/*
 *  @(#)Content.java 1.0 2011.05.24
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.data;

import java.awt.Image;
import java.text.Collator;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import javax.tv.service.Service;

/**
 * This class is device information.
 *
 * @since 2011.05.24
 * @version $Revision: 1.15 $ $Date: 2012/10/18 11:29:56 $
 * @author tklee
 */
public class Content implements Comparable, Cloneable {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** The Constant FR_COLLATOR. */
    private static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);
    /** The Constant SORT_ALPHA_A. */
    public static final int SORT_ALPHA_A = 0;
    /** The Constant SORT_ALPHA_Z. */
    public static final int SORT_ALPHA_Z = 1;
    /** The Constant SORT_NEWEST. */
    public static final int SORT_NEWEST = 2;
    /** The Constant SORT_OLDEST. */
    public static final int SORT_OLDEST = 3;

    /** The sort type. */
    public static int sortType = SORT_NEWEST;
    /** The parent. */
    private Content parent = null;
    /** The children. */
    private Vector children = null;
    /** The name. */
    private String name = null;
    /** The total count. */
    private int totalCount = 0;
    /** The img url. */
    private String imgUrl = null;
    /** The image. */
    private Image image = null;
    /** The date. */
    private String date = "";
    /** The artist. */
    private String artist = "";
    /** The track num. */
    private int trackNum = 0;
    /** The duration. */
    private long duration = 0;
    /** The duration long. */
    private long durationLong = 0L;
    /** The album name. */
    private String albumName = null;
    /** The genre. */
    private String genre = null;
    /** The resources url. */
    private String resourceUrl = null;
    /** The id. */
    private String id = null;
    /** The is folder. */
    private boolean isFolder = false;
    /** The content all entry. */
    private String allEntryId = null;
    /** The browse id. */
    private String browseId = null;
    /** The service. */
    private Service service = null;
    /** The icon url. */
    private String iconUrl = null;
    /** The icon image. */
    private Image iconImage = null;
    /** The resolution. */
    private int[] resolution = null;
    /** The menu type for search data. */
    private int menuType = 0;
    /** The music seq for search data. */
    private int musicSeq = 0;
    /** The is open for search. */
    private boolean isOpen = false;
    /** The photo index in folder. */
    private int photoIndexInFolder = 0;
    /** The icon urls in folder. */
    private Vector iconUrlsInFolder = null;
    /** The songs count. */
    private int songsCount = 0;
    /** The sort date. */
    private String sortDate = "";
    /** The child count. */
    private int childCount = 0;
    /** The is artist allsongs dir. */
    private boolean isArtistAllsongsDir = false;
    /** The child sort. */
    private int childSort = SORT_ALPHA_A;
    /** The browse page nums. */
    private Hashtable browsePageNums = null;
    /** The error page nums. */
    private Hashtable errorPageNums = null;
    /** The last loaded page. */
    private int lastLoadedPage = 0;
    /** The last removed page. */
    private int lastRemovedPage = 0;
    /** The last focus. */
    private int lastFocus = 0;
    /** The last focus id. */
    private String lastFocusId = null;
    /** The first folder. */
    private boolean firstFolder = false;
    /** The parent id. */
    private String parentId = null;

    /**
     * @return the photoIndexInFolder
     */
    public int getPhotoIndexInFolder() {
        return photoIndexInFolder;
    }

    /**
     * @return the iconUrlsInFolder
     */
    public Vector getIconUrlsInFolder() {
        return iconUrlsInFolder;
    }

    /**
     * @param photoIndex the photoIndexInFolder to set
     */
    public void setPhotoIndexInFolder(int photoIndex) {
        this.photoIndexInFolder = photoIndex;
    }

    /**
     * @param iconUrls the iconUrlsInFolder to set
     */
    public void setIconUrlsInFolder(Vector iconUrls) {
        this.iconUrlsInFolder = iconUrls;
    }

    /**
     * @return the isOpen
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * @param isOpened the isOpen to set
     */
    public void setOpen(boolean isOpened) {
        this.isOpen = isOpened;
    }

    /**
     * @return the menuType
     */
    public int getMenuType() {
        return menuType;
    }

    /**
     * @param type the menuType to set
     */
    public void setMenuType(int type) {
        this.menuType = type;
    }

    /**
     * @return the musicSeq
     */
    public int getMusicSeq() {
        return musicSeq;
    }

    /**
     * @param seq the musicSeq to set
     */
    public void setMusicSeq(int seq) {
        this.musicSeq = seq;
    }

    /**
     * @return the resolution
     */
    public int[] getResolution() {
        return resolution;
    }

    /**
     * @param resolutionValue the resolution to set
     */
    public void setResolution(int[] resolutionValue) {
        this.resolution = resolutionValue;
    }

    /**
     * @return the iconUrl
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * @param iconImgUrl the iconUrl to set
     */
    public void setIconUrl(String iconImgUrl) {
        this.iconUrl = iconImgUrl;
    }

    /**
     * @return the iconImage
     */
    public Image getIconImage() {
        return iconImage;
    }

    /**
     * @param iconImg the iconImage to set
     */
    public void setIconImage(Image iconImg) {
        this.iconImage = iconImg;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param iD the id to set
     */
    public void setId(String iD) {
        id = iD;
    }

    /**
     * @return the imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * @param url the imgUrl to set
     */
    public void setImgUrl(String url) {
        this.imgUrl = url;
    }

    /**
     * @return the resourceUrl
     */
    public String getResourceUrl() {
        return resourceUrl;
    }

    /**
     * @param resUrl the resourceUrl to set
     */
    public void setResourceUrl(String resUrl) {
        resourceUrl = resUrl;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * @param genreStr the genre to set
     */
    public void setGenre(String genreStr) {
        genre = genreStr;
    }

    /**
     * @return the albumName
     */
    public String getAlbumName() {
        return albumName;
    }

    /**
     * @param aName the albumName to set
     */
    public void setAlbumName(String aName) {
        albumName = aName;
    }

    /**
     * @return the duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * @param durationSec the duration to set
     */
    public void setDuration(long durationSec) {
        duration = durationSec;
    }

    /**
     * @return the durationLong
     */
    public long getDurationLong() {
        return durationLong;
    }

    /**
     * @param longValue the durationLong to set
     */
    public void setDurationLong(long longValue) {
        this.durationLong = longValue;
    }

    /**
     * @return the trackNum
     */
    public int getTrackNum() {
        return trackNum;
    }

    /**
     * @param number the trackNum to set
     */
    public void setTrackNum(int number) {
        trackNum = number;
    }

    /**
     * @return the image
     */
    public Image getImage() {
        return image;
    }

    /**
     * @param img the image to set
     */
    public void setImage(Image img) {
        this.image = img;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }

    /**
     * @param releaseDate the date to set
     */
    public void setDate(String releaseDate) {
        this.date = releaseDate;
    }

    /**
     * @return the parent
     */
    public Content getParent() {
        return parent;
    }

    /**
     * @param parentVideo the parent to set
     */
    public void setParent(Content parentVideo) {
        this.parent = parentVideo;
    }

    /**
     * @return the children
     */
    public Vector getChildren() {
        return children;
    }

    /**
     * @param vChildren the children to set
     */
    public void setChildren(Vector vChildren) {
        this.children = vChildren;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param fileName the name to set
     */
    public void setName(String fileName) {
        this.name = fileName;
    }

    /**
     * @return the totalCount
     */
    public int getTotalCount() {
        return totalCount;
    }

    /**
     * @param count the totalCount to set
     */
    public void setTotalCount(int count) {
        this.totalCount = count;
    }

    /**
     * @return the artist
     */
    public String getArtist() {
        return artist;
    }

    /**
     * @return the songsCount
     */
    public int getSongsCount() {
        return songsCount;
    }

    /**
     * @param count the songsCount to set
     */
    public void setSongsCount(int count) {
        this.songsCount = count;
    }

    /**
     * @param artistName the artist to set
     */
    public void setArtist(String artistName) {
        artist = artistName;
    }

    /**
     * @return the sortDate
     */
    public String getSortDate() {
        return sortDate;
    }

    /**
     * @param dateForSort the sortDate to set
     */
    public void setSortDate(String dateForSort) {
        this.sortDate = dateForSort;
    }

    /**
     * @return the childCount
     */
    public int getChildCount() {
        return childCount;
    }

    /**
     * @param childCount the childCount to set
     */
    public void setChildCount(int childCount) {
        this.childCount = childCount;
    }

    /**
     * @return the isArtistAllsongsDir
     */
    public boolean isArtistAllsongsDir() {
        return isArtistAllsongsDir;
    }

    /**
     * @param isArtistAllsongsDir the isArtistAllsongsDir to set
     */
    public void setArtistAllsongsDir(boolean isArtistAllsongsDir) {
        this.isArtistAllsongsDir = isArtistAllsongsDir;
    }

    /**
     * @return the childSort
     */
    public int getChildSort() {
        return childSort;
    }

    /**
     * @return the browsePageNums
     */
    public Hashtable getBrowsePageNums() {
        return browsePageNums;
    }

    /**
     * @param childSort the childSort to set
     */
    public void setChildSort(int childSort) {
        this.childSort = childSort;
    }

    /**
     * @param browsePageNums the browsePageNums to set
     */
    public void setBrowsePageNums(Hashtable browsePageNums) {
        this.browsePageNums = browsePageNums;
    }

    /**
     * @return the errorPageNums
     */
    public Hashtable getErrorPageNums() {
        return errorPageNums;
    }

    /**
     * @param errorPageNums the errorPageNums to set
     */
    public void setErrorPageNums(Hashtable errorPageNums) {
        this.errorPageNums = errorPageNums;
    }

    /**
     * @return the lastLoadedPage
     */
    public int getLastLoadedPage() {
        return lastLoadedPage;
    }

    /**
     * @return the lastRemovedPage
     */
    public int getLastRemovedPage() {
        return lastRemovedPage;
    }

    /**
     * @param lastLoadedPage the lastLoadedPage to set
     */
    public void setLastLoadedPage(int lastLoadedPage) {
        this.lastLoadedPage = lastLoadedPage;
    }

    /**
     * @param lastRemovedPage the lastRemovedPage to set
     */
    public void setLastRemovedPage(int lastRemovedPage) {
        this.lastRemovedPage = lastRemovedPage;
    }

    /**
     * @return the lastFocus
     */
    public int getLastFocus() {
        return lastFocus;
    }

    /**
     * @return the last focus id
     */
    public String getLastFocusId() {
        return lastFocusId;
    }

    /**
     * Sets the last focus.
     *
     * @param lastFocus the lastFocus to set
     */
    public void setLastFocus(int lastFocus) {
        this.lastFocus = lastFocus;
    }

    /**
     * Sets the last focus id.
     *
     * @param focusId the new last focus id
     */
    public void setLastFocusId(String focusId) {
        this.lastFocusId = focusId;
    }

    /**
     * @return the browseId
     */
    public String getBrowseId() {
        return browseId;
    }

    /**
     * @param browseId the browseId to set
     */
    public void setBrowseId(String browseId) {
        this.browseId = browseId;
    }

    /**
     * @return the allEntryId
     */
    public String getAllEntryId() {
        return allEntryId;
    }

    /**
     * @param allEntryId the allEntryId to set
     */
    public void setAllEntryId(String allEntryId) {
        this.allEntryId = allEntryId;
    }

    /**
     * @return the isFolder
     */
    public boolean isFolder() {
        return isFolder;
    }

    /**
     * @param isFolder the isFolder to set
     */
    public void setFolder(boolean isFolder) {
        this.isFolder = isFolder;
    }

    /**
     * @return the service
     */
    public Service getService() {
        return service;
    }

    /**
     * @param service the service to set
     */
    public void setService(Service service) {
        this.service = service;
    }

    /**
     * @return the firstFolder
     */
    public boolean isFirstFolder() {
        return firstFolder;
    }

    /**
     * @param firstFolder the firstFolder to set
     */
    public void setFirstFolder(boolean firstFolder) {
        this.firstFolder = firstFolder;
    }

    /**
     * @return the parentId
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * @param parentId the parentId to set
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * implements abstract method of Comparable Class.
     * <p>
     *
     * @param obj the object to compare
     * @return compared value.
     */
    public int compareTo(Object obj) {
        Content paraData = ((Content) obj);
        String ownTxt = getName();
        String paraTxt = paraData.getName();
        if (sortType >= SORT_NEWEST) {
            ownTxt = getSortDate();
            paraTxt = paraData.getSortDate();
        }
        if (ownTxt == null && paraTxt == null) {
            return 0;
        } else if (ownTxt == null) {
            //if (sortType == SORT_ALPHA_Z || sortType == SORT_NEWEST) { return -1; }
            return 1;
        } else if (paraTxt == null) {
            //if (sortType == SORT_ALPHA_Z || sortType == SORT_NEWEST) { return 1; }
            return -1;
        }
        //int returnValue = ownTxt.compareToIgnoreCase(paraTxt);
        int returnValue = FR_COLLATOR.compare(ownTxt, paraTxt);
        if (sortType == SORT_ALPHA_Z || sortType == SORT_NEWEST) { returnValue = -returnValue; }
        return returnValue;
    }

    /**
     * return clone Object.
     *
     * @return the object
     * @see java.lang.Object#clone()
     */
    public Object clone() {
        try {
            return super.clone();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

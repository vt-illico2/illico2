/*
 *  @(#)DeviceInfo.java 1.0 2011.05.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.data;

import org.ocap.hn.Device;

/**
 * This class is device information.
 *
 * @since 2011.05.19
 * @version $Revision: 1.3 $ $Date: 2012/03/26 00:59:13 $
 * @author tklee
 */
public class DeviceInfo {
    /** The Device instance. */
    private Device device = null;
    /** whether VT app is installed on PC or not. */
    private boolean isAppInstalled = true;
    /** The device version. */
    private String deviceVersion = null;

    /**
     * @return the device
     */
    public Device getDevice() {
        return device;
    }

    /**
     * @param dev the device to set
     */
    public void setDevice(Device dev) {
        this.device = dev;
    }

    /**
     * @return the isAppInstalled
     */
    public boolean isAppInstalled() {
        return isAppInstalled;
    }

    /**
     * @param isInstalled the isAppInstalled to set
     */
    public void setAppInstalled(boolean isInstalled) {
        this.isAppInstalled = isInstalled;
    }

    /**
     * Gets the UDN.
     *
     * @return the UDN
     */
    public String getUDN() {
        return device.getProperty(Device.PROP_UDN);
    }

    /**
     * Gets the Device Name.
     *
     * @return the Device Name
     */
    public String getDeviceName() {
        return device.getName();
    }

    /**
     * @return the deviceVersion
     */
    public String getDeviceVersion() {
        return device.getProperty(Device.PROP_MODEL_NUMBER);
    }

    /**
     * @param devVersion the deviceVersion to set
     */
    /*public void setDeviceVersion(String devVersion) {
        this.deviceVersion = devVersion;
    }*/
}

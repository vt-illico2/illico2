/*
 *  @(#)GalaxieContent.java 1.0 2011.06.29
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.data;

import com.videotron.tvi.illico.ixc.epg.TvChannel;

/**
 * This class is device information.
 *
 * @since 2011.06.29
 * @version $Revision: 1.2 $ $Date: 2011/08/23 07:17:32 $
 * @author tklee
 */
public class GalaxieContent {
    /** A index of channel number in channel data. */
    public static final int INDEX_CHANNEL_NUMBER = 0;
    /** A index of channel name in channel data. */
    public static final int INDEX_CHANNEL_NAME = 1;
    /** A index of channel type in channel data. */
    public static final int INDEX_CHANNEL_TYPE = 4;
    /** A index of channel source id in channel data. */
    public static final int INDEX_CHANNEL_SOURCE_ID = 5;

    /** The ch infos. */
    private Object[] chInfos = null;
    /** The tv channel. */
    private TvChannel tvChannel = null;
    /**
     * @return the chInfos
     */
    public Object[] getChInfos() {
        return chInfos;
    }
    /**
     * @param infos the chInfos to set
     */
    public void setChInfos(Object[] infos) {
        this.chInfos = infos;
    }
    /**
     * @return the tvChannel
     */
    public TvChannel getTvChannel() {
        return tvChannel;
    }
    /**
     * @param channel the tvChannel to set
     */
    public void setTvChannel(TvChannel channel) {
        this.tvChannel = channel;
    }
}

/*
 *  @(#)CommunicationManager.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.communication;

import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelData;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieChannelDataUpdateListener;
import com.videotron.tvi.illico.ixc.galaxie.GalaxieService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.App;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class for connection the IXC.
 *
 * @since 2011.05.16
 * @author tklee
 * @version $Revision: 1.15 $ $Date: 2012/10/08 23:45:21 $
 */
public class CommunicationManager {
    /** The lookup sleep time. */
    private final long lookupSleepTime = 2000;
    /** The point of loading animation to show on screen. */
    private final Point loadingAniPoint = new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2);

    /** Class instance. */
    private static CommunicationManager instance = null;

    /** The MonitorService. */
    private MonitorService monitorService = null;
    /** The ErrorMessageService. */
    private ErrorMessageService errMsgService = null;
    /** The StcService. */
    private StcService stcService = null;
    /** The LoadingAnimationService. */
    private LoadingAnimationService loadingService = null;
    /** The EpgService. */
    private EpgService epgService = null;
    /** The galaxie service. */
    private GalaxieService galaxieService = null;
    /** The screensaver service. */
    private ScreenSaverService screensaverService = null;

    /** The ScreenSaverProxy instance. */
    private ScreenSaverProxy screensaverProxy = null;
    /** The ErrorMessageProxy instance. */
    private ErrorMessageProxy errMsgProxy = null;

    /** The is loading animation. */
    private boolean isLoadingAnimation = false;

    /** The current tv channel. */
    private TvChannel currentTvChannel = null;

    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static synchronized CommunicationManager getInstance() {
        if (instance == null) { instance = new CommunicationManager(); }
        return instance;
    }

    /**
     * lookup Services via IXC.
     */
    public void start() {
        lookUpService("/1/2030/", PreferenceService.IXC_NAME);
        lookUpService("/1/1/", MonitorService.IXC_NAME);
        lookUpService("/1/2026/", EpgService.IXC_NAME);
        lookUpService("/1/2075/", LoadingAnimationService.IXC_NAME);
        lookUpService("/1/2100/", StcService.IXC_NAME);
        lookUpService("/1/2025/", ErrorMessageService.IXC_NAME);
        lookUpService("/1/2070/", GalaxieService.IXC_NAME);
        lookUpService("/1/1010/", ScreenSaverService.IXC_NAME);
        lookUpService("/1/2085/", VbmService.IXC_NAME);
    }

    /**
     * Look up service.
     * @param path path string
     * @param name IXC name
     */
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("IXC_LookUp:" + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(App.xletContext, ixcName);
                        if (remote != null) {
                            Logger.debug(this, "lookUpService()-success : " + name);
                            if (name.equals(MonitorService.IXC_NAME)) {
                                monitorService = (MonitorService) remote;
                                InbandManager.getInstance().addInbandDataListener(monitorService);
                                screensaverProxy = new ScreenSaverProxy();
                            } else if (name.equals(LoadingAnimationService.IXC_NAME)) {
                                loadingService = (LoadingAnimationService) remote;
                            } else if (name.equals(StcService.IXC_NAME)) {
                                stcService = (StcService) remote;
                                new STCServiceProxy().startService();
                            } else if (name.equals(ErrorMessageService.IXC_NAME)) {
                                errMsgService = (ErrorMessageService) remote;
                                errMsgProxy = new ErrorMessageProxy();
                            } else if (name.equals(EpgService.IXC_NAME)) {
                                epgService = (EpgService) remote;
                            } else if (name.equals(PreferenceService.IXC_NAME)) {
                                PreferenceProxy.getInstance().addPreferenceListener((PreferenceService) remote);
                            } else if (name.equals(GalaxieService.IXC_NAME)) {
                                galaxieService = (GalaxieService) remote;
                            } else if (name.equals(ScreenSaverService.IXC_NAME)) {
                                screensaverService = (ScreenSaverService) remote;
                            } else if (name.equals(VbmService.IXC_NAME)) {
                                VbmController.getInstance().init((VbmService) remote);
                            }
                            break;
                        }
                    } catch (Exception e) {
                        Logger.debug(this, "lookUpService()-not bound : " + name);
                    }
                    try {
                        Thread.sleep(lookupSleepTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    /**
     * Gets the parameter from Monitor.
     *
     * @return the start parameter
     */
    public String[] getParameter() {
        if (monitorService != null) {
            try {
                String[] parm = monitorService.getParameter(Config.APP_NAME);
                Logger.debug(this, "getParameter()-startParm : " + parm);
                return parm;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Start Unbound Application.
     *
     * @param reqAppName application Name.
     * @param reqParams null.
     * @return true, if successful
     */
    public boolean requestStartUnboundApplication(String reqAppName, String[] reqParams) {
        try {
            if (monitorService != null) {
                if (currentTvChannel != null) { changeChannel(currentTvChannel); }
                return monitorService.startUnboundApplication(reqAppName, reqParams);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Request start unbound application.
     *
     * @param keyCode the key code
     */
    public void startUnboundAppWithHotKey(int keyCode) {
        try {
            if (monitorService != null) {
                if (currentTvChannel != null) { changeChannel(currentTvChannel); }
                monitorService.startUnboundAppWithHotKey(keyCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Exit to channel.
     */
    public void exitToChannel() {
        try {
            if (monitorService != null) {
                if (currentTvChannel != null) {
                    //stopChannel();
                    //monitorService.exitToChannel(currentTvChannel.getSourceId());
                    changeChannel(currentTvChannel);
                    monitorService.exitToChannel();
                } else { monitorService.exitToChannel(); }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Exit to channel.
     *
     * @param sourceId the source id
     */
    private void exitToChannel(int sourceId) {
        try {
            if (monitorService != null) { monitorService.exitToChannel(sourceId); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the user has the authorization to execute the application.
     *
     * @return {@link MonitorService#NOT_AUTHORIZED},
     *         {@link MonitorService#AUTHORIZED},
     *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID},
     *         {@link MonitorService#CHECK_ERROR}.
     *         false, if NOT_AUTHORIZED
     */
    public boolean checkAppAuthorization() {
        try {
            if (monitorService != null) {
                return monitorService.checkAppAuthorization(Config.APP_NAME) != MonitorService.NOT_AUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * This method start loadingAnimation.
     *
     * @param point the point for x, y position
     */
    public void showLoadingAnimation(Point point) {
        try {
            if (loadingService != null) {
                loadingService.showLoadingAnimation(point);
                isLoadingAnimation = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method does a starting loadingAnimation without delay.
     *
     * @param point the point for x, y position
     */
    public void showNotDelayLoadingAnimation(Point point) {
        if (loadingService == null) { return; }
        try {
            loadingService.showNotDelayLoadingAnimation(point);
            isLoadingAnimation = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method does a starting loadingAnimation.
     */
    public void showLoadingAnimation() {
        showLoadingAnimation(loadingAniPoint);
    }

    /**
     * This method does a starting loadingAnimation without delay.
     */
    public void showNotDelayLoadingAnimation() {
        showNotDelayLoadingAnimation(loadingAniPoint);
    }

    /**
     * @return the isLoadingAnimation
     */
    public boolean isLoadingAnimation() {
        return isLoadingAnimation;
    }

    /**
     * This method stop loadingAnimation.
     */
    public void hideLoadingAnimation() {
        try {
            if (loadingService != null) {
                loadingService.hideLoadingAnimation();
                isLoadingAnimation = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add ScreenSaverListener.
     */
    public void addScreenSaverListener() {
        if (screensaverProxy != null) { screensaverProxy.addScreenSaverListener(); }
    }

    /**
     * Removes removeScreenSaverListener from MonitorService.
     */
    public void removeScreenSaverListener() {
        if (screensaverProxy != null) { screensaverProxy.removeScreenSaverListener(); }
    }

    /**
     * Show screen saver.
     */
    public void showScreenSaver() {
        if (screensaverService == null) { return; }
        try {
            screensaverService.showScreenSaver(Config.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide screen saver.
     */
    public void hideScreenSaver() {
        if (screensaverService == null) { return; }
        try {
            screensaverService.hideScreenSaver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Reset screen saver timer.
     */
    /*public void resetScreenSaverTimer() {
        try {
            monitorService.resetScreenSaverTimer();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Start screen saver.
     */
    /*public void startScreenSaver() {
        try {
            monitorService.startScreenSaver();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Sets the screen saver confirm.
     *
     * @param isConfirm the screen saver confirm
     */
    /*public void setScreenSaverConfirm(boolean isConfirm) {
        if (screensaverProxy != null) { screensaverProxy.isConfirm = isConfirm; }
    }*/

    /**
     * Show error message.
     * @param code the code or error
     */
    public void showErrorMessage(String code) {
        if (errMsgProxy != null) { errMsgProxy.showErrorMessage(code); }
    }

    /**
     * Hide error message.
     */
    public void hideErrorMessage() {
        if (errMsgProxy != null) { errMsgProxy.hideErrorMessage(); }
    }

    /**
     * Gets the TvChannel.
     *
     * @param callLetter the ch call letter(name)
     * @return the channel
     */
    public TvChannel getChannel(String callLetter) {
        if (epgService == null) { return null; }
        try {
            return epgService.getChannel(callLetter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Change channel.
     *
     * @param tvCh the TvChannel
     */
    public void changeChannel(TvChannel tvCh) {
        if (epgService == null) { return; }
        try {
            epgService.changeChannel(tvCh);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Change channel.
     *
     * @param id the id
     */
    public void changeChannel(int id) {
        if (epgService == null) { return; }
        try {
            epgService.changeChannel(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * stop channel.
     */
    public void stopChannel() {
        if (epgService == null) { return; }
        try {
            epgService.stopChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set the current channel.
     */
    public void setCurrentChannel() {
        Logger.debug(this, "called setCurrentChannel()");
        currentTvChannel = null;
        if (epgService == null) { return; }
        try {
            currentTvChannel = epgService.getCurrentChannel();
            Logger.debug(this, "setCurrentChannel()-currentTvChannel : " + currentTvChannel);
            if (currentTvChannel != null) {
                Logger.debug(this, "setCurrentChannel()-currentTvChannel : " + currentTvChannel.getSourceId());
                Logger.debug(this, "setCurrentChannel()-currentTvChannel : " + currentTvChannel.getCallLetter());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the galaxie data.
     *
     * @param sourceId the source id
     * @return the galaxie data
     */
    public GalaxieChannelData getGalaxieData(int sourceId) {
        if (galaxieService == null) { return null; }
        try {
            return galaxieService.getGalaxieData(sourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Add a listener to update a data of Galaxie channel.
     * @param l listener to receive a updated data.
     */
    public void addGalaxieChannelDataUpdatedListener(GalaxieChannelDataUpdateListener l) {
        if (galaxieService == null) { return; }
        try {
            galaxieService.addGalaxieChannelDataUpdatedListener(l);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove a listener.
     */
    public void removeGalaxieChannelDataUpdatedListener() {
        if (galaxieService == null) { return; }
        try {
            galaxieService.removeGalaxieChannelDataUpdatedListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * request resize video panel with reqeusted bound.
     * @param x x position
     * @param y y position
     * @param w width
     * @param h height
     */
    public void resizeVideo(int x, int y, int w, int h) {
        if (epgService == null) { return; }
        try {
            epgService.getVideoController().resize(x, y, w, h, VideoController.DO_NOT_SHOW);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The Class ScreenSaverProxy implement ScreenSaverConfirmationListener.
     */
    class ScreenSaverProxy implements ScreenSaverConfirmationListener {
        /** The is confirm. */
        public boolean isConfirm = false;
        /**
         * Add ScreenSaverListener.
         */
        public void addScreenSaverListener() {
            try {
                //isConfirm = true;
                monitorService.addScreenSaverConfirmListener(this, Config.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Removes removeScreenSaverListener from MonitorService.
         */
        public void removeScreenSaverListener() {
            try {
                monitorService.removeScreenSaverConfirmListener(this, Config.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * ScreenSaver is Confirmed to listeners.
         *
         * @return true
         * @throws RemoteException the remote exception
         */
        public boolean confirmScreenSaver() throws RemoteException {
            return isConfirm;
        }
    }

    /**
     * The Class ErrorMessageProxy implement ErrorMessageListener.
     */
    class ErrorMessageProxy implements ErrorMessageListener {
        /**
         * Show error message.
         *
         * @param code the code
         */
        public void showErrorMessage(String code) {
            try {
                errMsgService.showErrorMessage(code, this);
            } catch (Exception e) {
                Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
            }
        }

        /**
         * Hide error message.
         */
        public void hideErrorMessage() {
            try {
                errMsgService.hideErrorMessage();
            } catch (Exception e) {
                Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
            }
        }

        /**
         * called when ErrorMessageService completed the action requested.
         *
         * @param buttonType the button type
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener#actionPerformed(int)
         */
        public void actionPerformed(int buttonType) throws RemoteException {
            //if (buttonType == ErrorMessageListener.BUTTON_TYPE_CLOSE) { }
            SceneManager sceneManager = SceneManager.getInstance();
            sceneManager.scenes[sceneManager.curSceneId].notifyErrorMessage(buttonType);
        }
    }

    /**
     * The Class STCServiceProxy implement LogLevelChangeListener.
     */
    class STCServiceProxy implements LogLevelChangeListener {
        /**
         * Start service.
         */
        public void startService() {
            try {
                int currentLevel = stcService.registerApp(Config.APP_NAME);
                Logger.debug(this, "stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
            Object logCacheObj = SharedMemory.getInstance().get("stc-" + Config.APP_NAME);
            if (logCacheObj != null) { DataCenter.getInstance().put("LogCache", logCacheObj); }
            try {
                stcService.addLogLevelChangeListener(Config.APP_NAME, this);
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
        }

        /**
         * implements LogLevelChangeListener method.
         * when log level is changed, this method is called.
         *
         * @param logLevel the log level
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener#logLevelChanged(int)
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }
}

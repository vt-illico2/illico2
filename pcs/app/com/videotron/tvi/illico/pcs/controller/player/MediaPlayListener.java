/*
 *  @(#)MediaPlayListener.java 1.0 2011.08.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.player;

/**
 * <code>MediaPlayListener</code> This class is the listener for playing of media.
 *
 * @since   2011.08.04
 * @version $Revision: 1.1 $ $Date: 2011/08/10 17:55:48 $
 * @author  tklee
 */
public interface MediaPlayListener {
    /**
     * Notify event from MediaPlayHandler with ServiceContextEvent or controllEvent.
     *
     * @param event the event receive from MediaPlayHandler
     */
    void notifyEvent(Object event);

    /**
     * Notify stop.
     */
    void notifyStop();
}

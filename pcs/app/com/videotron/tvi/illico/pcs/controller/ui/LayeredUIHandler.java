/*
 *  @(#)LayeredUIHandler.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.ui;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * The Class LayeredUIHandler.<p>
 * This create the LayeredUI and handle the key event of LayeredKey.
 *
 * @since 2011.05.16
 * @version $Revision: 1.2 $ $Date: 2011/10/07 22:29:22 $
 * @author tklee
 */
public class LayeredUIHandler {
    /** The layered UI. */
    private LayeredUI layeredUI = null;
    /** The layered container. */
    private LayeredContainer layeredContainer = null;

    /**
     * Instantiates a new LayeredUIHandler.
     *
     * @param keyHandler the LayeredKeyHandler
     */
    public LayeredUIHandler(final LayeredKeyHandler keyHandler) {
        layeredContainer = new LayeredContainer();
        layeredContainer.setBounds(Rs.SCENE_BOUND);
        layeredContainer.setVisible(true);
        layeredUI = WindowProperty.PCS.createLayeredDialog(layeredContainer, Rs.SCENE_BOUND, keyHandler);
        layeredUI.deactivate();
    }

    /**
     * destroy resources.
     */
    public void destroy() {
        if (layeredContainer != null) {
            layeredContainer.setVisible(false);
            layeredContainer.removeAll();
            layeredContainer = null;
        }
        if (layeredUI != null) {
            layeredUI.deactivate();
            WindowProperty.dispose(layeredUI);
            layeredUI = null;
        }
    }

    /**
     * activate layeredUI.
     */
    public void activate() {
        if (layeredUI != null) { layeredUI.activate(); }
    }

    /**
     * deactivate layeredUI.
     */
    public void deactivate() {
        if (layeredUI != null) { layeredUI.deactivate(); }
    }

    /**
     * return LayeredContainer.
     * <p>
     * @return          LayeredContainer            LayeredContainer(Root Container)
     */
    public LayeredContainer getLayeredContainer() {
        return layeredContainer;
    }

    /**
     * <code>LayeredContainer</code> LayeredWindow Class.
     */
    private class LayeredContainer extends LayeredWindow {
        /** serialVersionUID for serializable type.*/
        private static final long serialVersionUID = 1L;

        /** called when other UI is disappeared on this UI.
         * @see com.alticast.ui.LayeredWindow#notifyClean()
         */
        public void notifyClean() {
            Log.printDebug("notifyClean() called.");
        }

        /** called when other UI is covered on this UI.
         * @see com.alticast.ui.LayeredWindow#notifyShadowed()
         */
        public void notifyShadowed() {
            ScreensaverController.getInstance().notifyShadowed();
        }
    }
}

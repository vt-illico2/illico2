/*
 *  @(#)NetAuthorizationHandlerImpl.java 1.0 2011.07.11
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.hn;

import java.net.InetAddress;
import java.net.URL;

import org.ocap.hn.security.NetAuthorizationHandler;

import com.videotron.tvi.illico.pcs.common.Logger;

/**
 * implements NetAuthorizationHandler.
 * @since 2011.07.11
 * @author tklee
 * @version $Revision: 1.2 $ $Date: 2012/07/20 01:49:06 $
 */
public class NetAuthorizationHandlerImpl implements NetAuthorizationHandler {
    /**
     * Notifies the authorization handler application that an action it registered interest in has been received.
     *
     * @param actionName Name of the action received. Will match the name passed to
     * the NetSecurityManager.setAuthorizationApplication method.
     * @param inetAddress IP address the transaction was sent from.
     * @param macAddress MAC address the transaction was sent from if present at any layer of the received
     * communications protocol. Can be empty String if not present. The format is EUI-48 with 6 colon separated 2 digit
     * bytes in hexadecimal notation with no leading "0x", e.g. "00:11:22:AA:BB:CC".
     * @param activityId The unique identifier of the activity if known. If no activityId is association with the
     * transaction the implementation SHALL pass a value of -1;
     * @return True if the activity is accepted, otherwise returns false.
     * @see org.ocap.hn.security.NetAuthorizationHandler#notifyAction(java.lang.String, java.net.InetAddress,
     * java.lang.String, int)
     */
    public boolean notifyAction(String actionName, InetAddress inetAddress, String macAddress, int activityId) {
        Logger.debug(this, "called notifyAction()-actionName : " + actionName + ", activityId : " + activityId);
        return false;
    }

    /**
     * Notifies the registered authorization handler application that an activity has ended.
     *
     * @param activityId Unique identifier assigned to the activity and passed to the notifyActivityStart method.
     * @see org.ocap.hn.security.NetAuthorizationHandler#notifyActivityEnd(int)
     */
    public void notifyActivityEnd(int activityId) {
        Logger.debug(this, "called notifyActivityEnd()-activityId : " + activityId);
    }

    /**
     * Notifies the registered authorization handler application that an activity to access
     * cable services has been started. The handler application will accept or deny the ability
     * for the activity to continue.
     *
     * @param inetAddress IP address the transaction was sent from.
     * @param macAddress MAC address the transaction was sent from if present at any layer of the received
     * communications protocol. Can be empty String if not present. The format is EUI-48 with 6 colon separated
     * 2 digit bytes in hexadecimal notation with no leading "0x", e.g. "00:11:22:AA:BB:CC".
     * @param url The URL requested by the transaction.
     * @param activityId The unique identifier of the activity, e.g. recorded content playback.
     * @return True if the activity is accepted, otherwise returns false.
     * @see org.ocap.hn.security.NetAuthorizationHandler#notifyAction(java.lang.String,
     * java.net.InetAddress, java.lang.String, int)
     */
    public boolean notifyActivityStart(InetAddress inetAddress, String macAddress, URL url, int activityId) {
        Logger.debug(this, "called notifyActivityStart()-activityId : " + activityId);
        return true;
    }
}

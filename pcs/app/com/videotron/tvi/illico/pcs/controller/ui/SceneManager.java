/*
 *  @(#)SceneManager.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.ui;

import java.awt.Container;
import java.awt.event.KeyEvent;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.VbmController;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.ContentListUI;
import com.videotron.tvi.illico.pcs.ui.HomeUI;
import com.videotron.tvi.illico.pcs.ui.InitUI;
import com.videotron.tvi.illico.pcs.ui.MusicListUI;
import com.videotron.tvi.illico.pcs.ui.MusicSearchListUI;
import com.videotron.tvi.illico.pcs.ui.PhotoPlayUI;
import com.videotron.tvi.illico.pcs.ui.PreferenceUI;
import com.videotron.tvi.illico.pcs.ui.VideoPlayUI;
import com.videotron.tvi.illico.pcs.ui.comp.LogDisplayer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class is the SceneManager class.
 *
 * @version $Revision: 1.14 $ $Date: 2012/09/25 05:03:40 $
 * @author tklee
 */
public final class SceneManager implements LayeredKeyHandler {
    /** The Constant NUM_SCENE. */
    private static final int NUM_SCENE = 8;
    /** The Constant SC_INVALID. */
    public static final int SC_INVALID = -1;
    /** The Constant SC_ONE_CH. */
    public static final int SC_INIT = 0;
    /** The Constant SC_HOME. */
    public static final int SC_HOME = 1;
    /** The Constant SC_PHOTO_LIST. */
    public static final int SC_CONTENT_LIST = 2;
    /** The Constant SC_MUSIC_LIST. */
    public static final int SC_MUSIC_LIST = 3;
    /** The Constant SC_PREFERENCE. */
    public static final int SC_PREFERENCE = 4;
    /** The Constant SC_VIDEO_PLAY. */
    public static final int SC_VIDEO_PLAY = 5;
    /** The Constant SC_PHOTO_PLAY. */
    public static final int SC_PHOTO_PLAY = 6;
    /** The Constant SC_MUSIC_SEARCH_LIST. */
    public static final int SC_MUSIC_SEARCH_LIST = 7;

    /** Instance of SceneManager. */
    private static SceneManager instance = null;
    /** The LayeredUIHandler. */
    private LayeredUIHandler layeredHandler = null;
    /** The root container. */
    private Container rootContainer = null;

    /** scene list. */
    public BasicUI[] scenes = null;
    /** is a current Scene ID. */
    public int curSceneId = SC_INVALID;

    /**
     * Save scene historys.
     * included previous scene id, datas.
     */
    //private Vector history = new Vector();
    /** The start param. */
    private String[] startParam = null;

    /**
     * Gets the singleton instance of SceneManager.
     * @return SceneManager
     */
    public static synchronized SceneManager getInstance() {
        if (instance == null) { instance = new SceneManager(); }
        return instance;
    }

    /**
     * Constructor.
     */
    private SceneManager() {
    }

    /**
     * Initialize a SceneManager.
     */
    public void init() {
        layeredHandler = new LayeredUIHandler(this);
        rootContainer = layeredHandler.getLayeredContainer();
        scenes = new BasicUI[NUM_SCENE];
        curSceneId = SC_INVALID;
    }

    /**
     * start the SceneManage.
     */
    public void start() {
        Logger.info(this, "called start()");
        layeredHandler.activate();
        startParam = CommunicationManager.getInstance().getParameter();
        VbmController.getInstance().writeStartLog(startParam);
        if (Environment.EMULATOR) {
            new Thread("start()") {
                public void run() {
                    try { Thread.sleep(10000L); } catch (Exception e) { e.printStackTrace(); }
                    goToScene(SC_INIT, true, null);
                    ScreensaverController.getInstance().start();
                }
            } .start();
        } else {
            goToScene(SC_INIT, true, null);
            ScreensaverController.getInstance().start();
        }
        //layeredHandler.activate();
    }

    /**
     * pause the SceneManage.
     */
    public void pause() {
        Logger.info(this, "called pause()");
        layeredHandler.deactivate();
        //history.clear();
        if (rootContainer != null && Config.enableDebugUI) { LogDisplayer.getInstance().stop(rootContainer); }
        if (curSceneId != SC_INVALID) {
            scenes[curSceneId].stop();
            rootContainer.removeAll();
        }
        for (int i = 0; scenes != null && i < scenes.length; i++) {
            if (scenes[i] != null) {
                scenes[i].dispose();
                scenes[i] = null;
            }
        }
        curSceneId = SC_INVALID;
        FrameworkMain.getInstance().getImagePool().clear();
        ScreensaverController.getInstance().dispose();
        VbmController.getInstance().writeEndLog();
    }

    /**
     * dispose the SceneManage.
     */
    public void dispose() {
        pause();
        layeredHandler.destroy();
        rootContainer = null;
        //history = null;
        scenes = null;
    }

    /**
     * go to NextScene.
     *
     * @param reqSceneId Scene id.
     * @param isReset whether reset or not
     * @param data  the data of scene(sceneId, needed data, focus index);
     */
    //public void goToScene(int reqSceneId, boolean isRest, Object[] data) {
    public void goToScene(int reqSceneId, boolean isReset, Object data) {
        Logger.info(this, "goToScene()-reqSceneId = " + reqSceneId + " curSceneId = " + curSceneId);
        if (reqSceneId == SC_INVALID) {
            requestStartMenuApp();
            return;
        }
        if (curSceneId == reqSceneId) { return; }

        if (curSceneId != SC_INVALID) {
            scenes[curSceneId].stop();
            rootContainer.remove(scenes[curSceneId]);
            if (curSceneId == SC_CONTENT_LIST || curSceneId == SC_MUSIC_LIST) {
                if (reqSceneId < SC_PREFERENCE && reqSceneId != SC_INIT) { scenes[curSceneId].clearTempData(); }
            }
        }

        /*if (data != null) {
            history.add(data);
        } else if (data == null && history.size() > 0) { history.remove(history.size() - 1); }*/

        if (scenes[reqSceneId] == null) { scenes[reqSceneId] = createScene(reqSceneId); }
        Object[] sendData = {new Integer(curSceneId), data};
        curSceneId = reqSceneId;
        scenes[reqSceneId].start(isReset, sendData);
        rootContainer.add(scenes[reqSceneId]);
        rootContainer.repaint();
    }

    /**
     * Creates and Initialize the Scene.
     *
     * @param reqSceneId    scene id for request
     * @return scene    requested scene
     */
    private BasicUI createScene(int reqSceneId) {
        BasicUI scene = null;
        switch (reqSceneId) {
            case SC_INIT:
                scene = new InitUI();
                break;
            case SC_HOME:
                scene = new HomeUI();
                break;
            case SC_PREFERENCE:
                scene = new PreferenceUI();
                break;
            case SC_CONTENT_LIST:
                scene = new ContentListUI();
                break;
            case SC_MUSIC_LIST:
                scene = new MusicListUI();
                break;
            case SC_VIDEO_PLAY:
                scene = new VideoPlayUI();
                break;
            case SC_PHOTO_PLAY:
                scene = new PhotoPlayUI();
                break;
            case SC_MUSIC_SEARCH_LIST:
                scene = new MusicSearchListUI();
                break;
            default:
                break;
        }
        //scene.setVisible(false);
        return scene;
    }

    /**
     * Gets the previous data on the next Scene.
     * @return data is a previous scene data.
     */
    /*public Object[] getSceneHistory() {
        if (history.size() > 0) {
            Object[] data = (Object[]) history.elementAt(history.size() - 1);
            return data;
        }
        return null;
    }*/

    /**
     * implement LayeredKeyHandler method. handle KeyEvent.
     *
     * @param event the UserEvent
     * @return true, if handle key
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (event == null) {
            Logger.debug(this, "handleKeyEvent()-User Event is null.");
            return false;
        }
        if (event.getType() != KeyEvent.KEY_PRESSED || curSceneId == SC_INVALID) { return false; }
        long currentTime = System.currentTimeMillis();
        long keyWhen = event.getWhen();
        Logger.debug(this, "handleKeyEvent()-currentTime : " + currentTime);
        Logger.debug(this, "handleKeyEvent()-keyWhen : " + keyWhen);
        //test
        //executeGC();
        //
        int keyCode = event.getCode();
        Logger.debug(this, "handleKeyEvent()-keyCode : " + keyCode);
        Logger.debug(this, "handleKeyEvent()-Config.enableDebugUI : " + Config.enableDebugUI);
        if (Config.enableDebugUI) {
            boolean isLogShow = LogDisplayer.getInstance().isLogShow;
            if (isLogShow && keyCode == Rs.KEY_EXIT) {
                LogDisplayer.getInstance().stop(rootContainer);
                return true;
            } else if (!isLogShow) {
                if (keyCode == OCRcEvent.VK_0) {
                    LogDisplayer.getInstance().key += "0";
                    Logger.debug(this, "handleKeyEvent()-keyset : " + LogDisplayer.getInstance().key);
                    if (LogDisplayer.getInstance().key.equals(LogDisplayer.KEY_SET)) {
                        scenes[curSceneId].setDebugData(null, -1);
                        LogDisplayer.getInstance().show(rootContainer);
                        return true;
                    }
                } else { LogDisplayer.getInstance().key = ""; }
            }
        }
        if (ScreensaverController.getInstance().keyAction()) {
            Logger.debug(this, "handleKeyEvent()-ScreensaverController keyAction is true.");
            return true;
        }
        BasicUI currentScene = scenes[curSceneId];
        if (System.currentTimeMillis() - event.getWhen() > 500 && !currentScene.isSlideShow()) { return true; }
        return currentScene.handleKey(keyCode);
    }
    //test
    /**
     * Execute gc.
     */
    public void executeGC() {
        Runtime rt = Runtime.getRuntime();
        Logger.debug(this, "executeGC()-Memory : " + rt.freeMemory() + "/" + rt.totalMemory());
        System.gc();
        Logger.debug(this, "executeGC()-Memory after GC : " + rt.freeMemory() + "/" + rt.totalMemory());
    }
    /**
     * Prints the memory.
     */
    public void printMemory() {
        Runtime rt = Runtime.getRuntime();
        Logger.debug(this, "executeGC2()-Memory : " + rt.freeMemory() + "/" + rt.totalMemory());
    }

    /**
     * request to start menu application.
     */
    public void requestStartMenuApp() {
        if (startParam == null) { startParam = CommunicationManager.getInstance().getParameter(); }
        Logger.debug(this, "startParam : " + startParam);
        boolean isRequestStart = false;
        if (startParam != null && startParam.length > 0 && startParam[0] != null) {
            Logger.debug(this, "startParams[0] : " + startParam[0]);
        }
        isRequestStart =  CommunicationManager.getInstance().requestStartUnboundApplication("Menu",
                new String[]{MonitorService.REQUEST_APPLICATION_LAST_KEY});
        if (!isRequestStart) { CommunicationManager.getInstance().exitToChannel(); }
    }

    /**
     * request to start application.
     *
     * @param appName the app name
     */
    private void requestStartApp(String appName) {
        boolean isRequestStart =  CommunicationManager.getInstance().requestStartUnboundApplication(appName,
                new String[]{MonitorService.REQUEST_APPLICATION_HOT_KEY});
        if (!isRequestStart) { CommunicationManager.getInstance().exitToChannel(); }
    }

    /**
     * request to start help application.
     */
    public void requestStartHelpApp() {
        CommunicationManager.getInstance().requestStartUnboundApplication("Help",
                new String[]{Config.APP_NAME, Config.APP_NAME});
    }
}

/*
 *  @(#)MediaPlayHandler.java 1.0 2011.07.22
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.player;

import java.awt.Rectangle;
import java.util.Vector;

import javax.media.ConnectionErrorEvent;
import javax.media.ControllerErrorEvent;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.EndOfMediaEvent;
import javax.media.InternalErrorEvent;
import javax.media.Manager;
import javax.media.MediaLocator;
import javax.media.Player;
import javax.media.PrefetchCompleteEvent;
import javax.media.RealizeCompleteEvent;
import javax.media.StartEvent;
import javax.media.Time;
import javax.media.protocol.DataSource;
import javax.tv.media.AWTVideoSize;
import javax.tv.media.AWTVideoSizeControl;
import javax.tv.service.Service;
import javax.tv.service.selection.NormalContentEvent;
import javax.tv.service.selection.SelectionFailedEvent;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextListener;

import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.dvb.service.selection.DvbServiceContext;
import org.ocap.diagnostics.MIBDefinition;
import org.ocap.diagnostics.MIBManager;
import org.ocap.diagnostics.MIBObject;
import org.ocap.hn.content.ContentContainer;
import org.ocap.shared.media.BeginningOfContentEvent;
import org.ocap.shared.media.EndOfContentEvent;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.util.Environment;

/**
 * This Class is the Player class for video and audio.
 *
 * @since 2011.07.22
 * @version $Revision: 1.14 $ $Date: 2012/09/18 02:43:04 $
 * @author tklee
 */
public class MediaPlayHandler implements ControllerListener, ServiceContextListener {
    /** Class instance. */
    private static MediaPlayHandler instance = null;
    /** The player. */
    private Player player = null;
    /** The service context. */
    private ServiceContext serviceContext = null;
    /** The video size control. */
    private AWTVideoSizeControl videoSizeControl = null;
    /** The listener. */
    private MediaPlayListener listener = null;
    /** The is init player. */
    private boolean isInitPlayer = false;
    /** The start obj. */
    private Object startObj = null;

    /**
     * return MediaPlayHandler instance.
     * @return MediaPlayHandler
     */
    public static synchronized MediaPlayHandler getInstance() {
        if (instance == null) { instance = new MediaPlayHandler(); }
        return instance;
    }

    /**
     * Constructor.
     */
    public MediaPlayHandler() {
        setServiceContxnt();
    }

    /**
     * Sets the service contxnt.
     */
    private void setServiceContxnt() {
        if (serviceContext == null) {
            serviceContext = Environment.getServiceContext(0);
        }
        Logger.debug(this, "Environment sc : " + serviceContext);
        /*if (serviceContext != null && (player == null || videoSizeControl == null)) {
            ServiceContentHandler[] handlers = serviceContext.getServiceContentHandlers();
            if (handlers == null) { return; }
            for (int i = 0; i < handlers.length; i++) {
                if (handlers[i] != null && handlers[i] instanceof Player) {
                    player = (Player) handlers[i];
                    videoSizeControl = (AWTVideoSizeControl) player.getControl("javax.tv.media.AWTVideoSizeControl");
                    break;
                }
            }
        }*/
    }

    /**
     * dispose player.
     * <p>
     */
    public void dispose() {
        Logger.info(this, "called dispose()");
        isInitPlayer = false;
        stop(listener);
        removeServiceContextListener();
        listener = null;
        startObj = null;
    }

    /**
     * Start.
     *
     * @param service the service
     * @param playListener the play listener
     * @return true, if successful
     */
    public boolean start(Object service, MediaPlayListener playListener) {
        Logger.info(this, "called start()-service : " + service);
        if (service == null || Environment.EMULATOR) { return false; }
        setServiceContxnt();
        try {
            stop(playListener);
            listener = playListener;
            addServiceContextListener();
            startObj = service;
            if (service instanceof Service) {
                //test
                MediaLocator mediaLocator = null;
                //DataSource dataSource = null;
                try {
                    String path = ((Service) service).getLocator().toExternalForm();
                    Logger.debug(this, "start()-path : " + path);
                    mediaLocator = new MediaLocator(path);
                    //dataSource = Manager.createDataSource(mediaLocator);
                    //player = Manager.createPlayer(dataSource);
                    player = Manager.createPlayer(mediaLocator);
                    player.addControllerListener(this);
                    player.start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //serviceContext.select((Service) service);
            } else { CommunicationManager.getInstance().changeChannel(((Integer) service).intValue()); }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * stop.
     * <p>
     *
     * @param playListener the play listener
     */
    public void stop(MediaPlayListener playListener) {
        Logger.info(this, "called stop()");
        isInitPlayer = false;
        removeServiceContextListener();
        if (listener != null && listener != playListener) {
            listener.notifyStop();
            listener = null;
        }
        if (startObj != null && startObj instanceof Integer) {
            CommunicationManager.getInstance().stopChannel();
        }
        Logger.info(this, "stop()-player : " + player);
        if (player != null) {
            player.removeControllerListener(this);
            try {
                player.stop();
                Logger.info(this, "stop()-player.stop() is completed");
                //player.deallocate();
                player.close();
                Logger.info(this, "stop()-player.close() is completed");
            } catch (Throwable t) {
                t.printStackTrace();
            }
            player = null;
        }
        //if (serviceContext != null) { serviceContext.stop(); }
    }

    /**
     * Equal player.
     *
     * @param playListener the play listener
     * @return true, if successful
     */
    public boolean equalPlayer(MediaPlayListener playListener) {
        return listener != null && listener == playListener;
    }

    /**
     * Adds the listener.
     *
     * @param playListener the play listener
     */
    /*public void addListener(MediaPlayListener playListener) {
        listener = playListener;
    }*/

    /**
     * Removes the listener.
     */
    /*public void removeListener() {
        listener = null;
    }*/

    /**
     * play.
     * <p>
     */
    public void play() {
        try {
            if (player != null) {
                player.start();
                setPlayRate(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * pause.
     * <p>
     */
    public void pause() {
        setPlayRate(0);
    }

    /**
     * set media time for skip.
     * @param setTime the add time
     */
    public void setMediaTime(long setTime) {
        Logger.info(this, "called setMediaTime()-setTime : " + setTime);
        if (player != null && isInitPlayer) {
            Time mediaTime = new Time((double) setTime);
            player.setMediaTime(mediaTime);
        }
    }

    /**
     * add media time for skip.
     * @param addTime the add time
     */
    public void addMediaTime(int addTime) {
        Logger.info(this, "called addMediaTime()-addTime : " + addTime);
        if (player != null && isInitPlayer) {
            Time mediaTime = new Time(getPlayTime() + addTime);
            player.setMediaTime(mediaTime);
        }
    }

    /**
     * RW / FF
     * <p>.
     *
     * @param rate the new play rate
     */
    public void setPlayRate(float rate) {
        Logger.info(this, "called setPlayRate()-rate : " + rate);
        if (player != null && isInitPlayer && player.getState() != Player.Unrealized
                && player.getState() != Player.Realizing) {
            Logger.debug(this, "setPlayRate() - state : " + player.getState());
            player.setRate(rate);
        }
    }

    /**
     * return rate.
     * <p>
     *
     * @return the play rate
     */
    public float getPlayRate() {
        Logger.info(this, "called getPlayRate()");
        if (player != null && isInitPlayer && player.getState() != Player.Unrealized
                && player.getState() != Player.Realizing) {
            return player.getRate();
        }
        return 0;
    }

    /**
     * return play time.
     * <p>
     *
     * @return the play time
     */
    public double getPlayTime() {
        Logger.info(this, "called getPlayTime()");
        double playTime = 0;
        if (player != null && isInitPlayer) { playTime = player.getMediaTime().getSeconds(); }
        Logger.info(this, "getPlayTime()-playTime : " + playTime);
        return playTime;
    }

    /**
     * return play time.
     * <p>
     *
     * @return the play time long
     */
    public long getPlayTimeLong() {
        Logger.info(this, "called getPlayTimeLong()");
        long playTime = 0;
        if (player != null && isInitPlayer) { playTime = player.getMediaTime().getNanoseconds() / 1000000; }
        Logger.info(this, "getPlayTimeLong()-playTime : " + playTime);
        return playTime;
    }

    /**
     * return duration.
     * <p>
     *
     * @return the duration
     */
    public double getDuration() {
        Logger.info(this, "called getDuration()");
        if (player == null || !isInitPlayer) { return 0; }
        return player.getDuration().getSeconds();
    }

    /**
     * return duration by long value.
     * <p>
     *
     * @return the duration
     */
    public long getDurationLong() {
        Logger.info(this, "called getDurationLong()");
        long duration = 0;
        if (player != null && isInitPlayer) { duration = player.getDuration().getNanoseconds() / 1000000; }
        Logger.info(this, "getDurationLong()-duration : " + duration);
        return duration;
    }

    /**
     * return init boolean.
     * <p>
     *
     * @return true, if is inits the player
     */
    public boolean isInitPlayer() {
        return isInitPlayer;
    }

    /**
     * Resize video pane as specified.
     *
     * @param x x point
     * @param y y point
     * @param w width
     * @param h height
     * @return the Rectangle as result
     */
    public Rectangle resizeVideo(int x, int y, int w, int h) {
        return resizeVideo(new Rectangle(x, y, w, h));
    }

    /**
     * Resize video pane as specified.
     * @param dst destination
     * @return the Rectangle as result
     */
    public Rectangle resizeVideo(Rectangle dst) {
        if (videoSizeControl != null) {
            Rectangle src = new Rectangle(videoSizeControl.getSourceVideoSize());
            AWTVideoSize awtVideoSize = new AWTVideoSize(src, dst);
            if (dst.width != 0 && dst.height != 0) {
                AWTVideoSize checkedSize = videoSizeControl.checkSize(awtVideoSize);
                videoSizeControl.setSize(checkedSize);
                return checkedSize.getDestination();
            } else {
                videoSizeControl.setSize(awtVideoSize);
                return awtVideoSize.getDestination();
            }
        }
        return null;
    }

    /**
     * implements a abstract method of ServiceContextListener.
     *
     * @param e the e
     */
    public void receiveServiceContextEvent(final ServiceContextEvent e) {
        Logger.info(this, "called receiveServiceContextEvent() : " + e);
        Service service = e.getServiceContext().getService();
        Logger.debug(this, "receiveServiceContextEvent()-Service : " + service);
        if (e instanceof NormalContentEvent) {
            isInitPlayer = true;
            /*if (serviceContext != null) {
                ServiceContentHandler[] handlers = serviceContext.getServiceContentHandlers();
                if (handlers == null) { return; }
                for (int i = 0; i < handlers.length; i++) {
                    if (handlers[i] != null && handlers[i] instanceof Player) {
                        player = (Player) handlers[i];
                        videoSizeControl =
                            (AWTVideoSizeControl) player.getControl("javax.tv.media.AWTVideoSizeControl");
                        break;
                    }
                }
                if (player != null) { player.addControllerListener(this); }
                Logger.debug(this, "receiveServiceContextEvent()-getDuration() : " + getDuration());
                notifyToListener(e);
//                new Thread("MediaPlayHandler|verifyAV()") {
//                    public void run() {
//                        boolean isAV = false;
//                        for (int i = 0; i < 20; i++) {
//                            isAV = verifyAV();
//                            if (isAV) { break; }
//                            try {
//                                Thread.sleep(500L);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        Logger.debug(this, "receiveServiceContextEvent()-isAV : " + isAV);
//                        notifyToListener(e);
//                    }
//                } .start();
            } else { notifyToListener(e); }*/
            notifyToListener(e);
        } else {
            isInitPlayer = false;
            if (e instanceof SelectionFailedEvent) {
                Logger.debug(this, "receiveServiceContextEvent()-e : SelectionFailedEvent");
                int reason = ((SelectionFailedEvent) e).getReason();
                Logger.debug(this, "receiveServiceContextEvent()-reason : " + reason);
            }
            notifyToListener(e);
        }
    }

    /**
     * implements abstract method of ControllerListener.
     * <p>
     *
     * @param controllerEvent the controller event
     */
    public void controllerUpdate(ControllerEvent controllerEvent) {
        Logger.info(this, "called controllerUpdate() : " + controllerEvent);
        Logger.debug(this, "controllerUpdate() - player.getState() : " + player.getState());
        if (controllerEvent instanceof BeginningOfContentEvent) {
            Logger.debug(this, "controllerUpdate() : BeginningOfContentEvent");
        }
        if (controllerEvent instanceof EndOfContentEvent) {
            Logger.debug(this, "controllerUpdate() : EndOfContentEvent");
        }
        if (controllerEvent instanceof RealizeCompleteEvent) {
            Logger.debug(this, "controllerUpdate() : RealizeCompleteEvent");
        } else if (controllerEvent instanceof PrefetchCompleteEvent) {
            Logger.debug(this, "controllerUpdate() : PrefetchCompleteEvent");
            //isInitPlayer = true;
            //videoSizeControl = (AWTVideoSizeControl) player.getControl("javax.tv.media.AWTVideoSizeControl");
            //Logger.debug(this, "controllerUpdate()-getDuration() : " + getDuration());
        } else if (controllerEvent instanceof StartEvent) {
            Logger.debug(this, "controllerUpdate() : StartEvent");
            isInitPlayer = true;
            videoSizeControl = (AWTVideoSizeControl) player.getControl("javax.tv.media.AWTVideoSizeControl");
            Logger.debug(this, "controllerUpdate()-getDuration() : " + getDuration());
        } else if (controllerEvent instanceof ConnectionErrorEvent) {
            Logger.debug(this, "controllerUpdate() : ConnectionErrorEvent");
            stop(listener);
        } else if (controllerEvent instanceof InternalErrorEvent) {
            Logger.debug(this, "controllerUpdate() : InternalErrorEvent");
            stop(listener);
        } else if (controllerEvent instanceof ControllerErrorEvent) {
            Logger.debug(this, "controllerUpdate() : ControllerErrorEvent");
            stop(listener);
        } else if (controllerEvent instanceof EndOfMediaEvent) {

        }
        if (controllerEvent instanceof ControllerErrorEvent) {
            MainManager.getInstance().printError(MainManager.ERR_LOST_CONNECTION, "Unalble to play");
        }
        notifyToListener(controllerEvent);
    }

    /**
     * Verify av. not use because HN doesn't use tuner.
     *
     * @return true, if successful
     */
    public boolean verifyAV() {
        NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
        for (int i = 0; i < nis.length; i++) {
            Logger.debug(this, "verifyLock()-nis[" + i + "] = " + nis[i]);
        }
        String oid = null;
        if (serviceContext != null && serviceContext instanceof DvbServiceContext) {
            NetworkInterface ni = ((DvbServiceContext) serviceContext).getNetworkInterface();
            if (ni == null) {
                Logger.debug(this, "verifyLock()-NetworkInterface is null.");
                return false;
            }
            oid = "1.3.6.1.4.1.4491.2.3.1.1.1.2.7.3.1.14.2";
            if (ni.equals(nis[0])) { oid = "1.3.6.1.4.1.4491.2.3.1.1.1.2.7.3.1.14.1"; }
            Logger.debug(this, "verifyLock()-ni = " + ni + ", oid = " + oid);
        }
        MIBDefinition[] defs = MIBManager.getInstance().queryMibs(oid);
        if (defs == null) {
            Logger.debug(this, "verifyLock()-MIBDefinition[] is null.");
            return false;
        }
        boolean ret;
        for (int i = 0; i < defs.length; i++) {
            MIBDefinition def = defs[i];
            if (def == null) {
                Logger.debug(this, "verifyLock()-MIBDefinition[" + i + "] is null.");
                continue;
            }
            MIBObject obj = def.getMIBObject();
            if (obj == null) {
                Logger.debug(this, "verifyLock()-MIBObject is null.");
                continue;
            }
            byte[] mibObjectData = obj.getData();
            if (mibObjectData == null) {
                Logger.debug(this, "verifyLock()-mibObjectData[" + i + "] is null.");
                continue;
            }
            ret = mibObjectData[0] == 2;
            Logger.debug(this, "verifyLock()-mibObjectData[0] = " + mibObjectData[0]);
            return ret;
        }
        return false;
    }

    /**
     * Notify event to scene.
     *
     * @param event the event
     */
    private void notifyToListener(Object event) {
        if (listener != null) {
            listener.notifyEvent(event);
        }
    }

    /**
     * add ServiceContextListener in ServiceContext.
     *
     */
    public void addServiceContextListener() {
        if (serviceContext != null) { serviceContext.addListener(this); }
    }

    /**
     * remove ServiceContextListener in ServiceContext.
     *
     */
    public void removeServiceContextListener() {
        if (serviceContext != null) { serviceContext.removeListener(this); }
    }
}

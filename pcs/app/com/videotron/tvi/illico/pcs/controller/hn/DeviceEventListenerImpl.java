/*
 *  @(#)DeviceEventListenerImpl 1.0 2011.07.11
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.hn;

import org.ocap.hn.Device;
import org.ocap.hn.DeviceEvent;
import org.ocap.hn.DeviceEventListener;

import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.HomeUI;

/**
 * implements DeviceEventListener.
 * @since 2011.07.11
 * @author tklee
 * @version $Revision: 1.7 $ $Date: 2012/09/18 02:42:56 $
 */
public class DeviceEventListenerImpl implements DeviceEventListener {
    /**
     * Callback function for Device events.
     *
     * @param event Device event
     * @see org.ocap.hn.DeviceEventListener#notify(org.ocap.hn.DeviceEvent)
     */
    public void notify(DeviceEvent event) {
        Logger.info(this, "notify()-event : " + event);
        Logger.info(this, "notify()-event.getType() : " + event.getType());
        Device device = (Device) event.getSource();
        SceneManager sceneManager = SceneManager.getInstance();
        if (HNHandler.getInstance().verifyPC(device) && event.getType() == DeviceEvent.DEVICE_REMOVED
                && sceneManager.curSceneId != SceneManager.SC_INVALID) {
            DeviceInfo connectedDevice = HomeUI.tempDevice;
            Logger.info(this, "notify()-connectedDevice : " + connectedDevice);
            if (connectedDevice != null) {
                Logger.info(this, "notify()-connectedDevice : " + connectedDevice.getUDN());
            }
            Logger.info(this, "notify()-device udn : " + device.getProperty(Device.PROP_UDN));
            if (connectedDevice != null && connectedDevice.getUDN().equals(device.getProperty(Device.PROP_UDN))) {
                MainManager.getInstance().printError(MainManager.ERR_LOST_CONNECTION, "UDN : "
                        + connectedDevice.getUDN());
                int errMsgAction = sceneManager.scenes[sceneManager.curSceneId].errMsgAction;
                Logger.info(this, "notify()-errMsgAction : " + errMsgAction);
                if (errMsgAction != BasicUI.ERRMSG_INIT) {
                    SceneManager.getInstance().goToScene(SceneManager.SC_INIT, false, connectedDevice);
                }
            }
        }
    }
}

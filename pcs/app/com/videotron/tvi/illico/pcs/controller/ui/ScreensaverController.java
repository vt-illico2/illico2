/*
 *  @(#)ScreensaverController.java 1.0 2011.09.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.ui;

import java.awt.EventQueue;

import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.common.Rs;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.pcs.ui.comp.ScreenSaver;
import com.videotron.tvi.illico.pcs.ui.popup.MusicPlayerPopup;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class conroll the screensaver.
 *
 * @since 2011.09.16
 * @version $Revision: 1.11 $ $Date: 2012/10/10 07:48:52 $
 * @author tklee
 */
public final class ScreensaverController {
    /** Instance of ScreensaverController. */
    private static ScreensaverController instance = null;

    /** The controller thread. */
    private Thread controllerThread = null;
    /** The current scene. */
    private BasicUI curScene = null;
    /** The screen saver comp. */
    private ScreenSaver  screenSaverComp = null;
    /** The count. */
    private int count = 0;

    /**
     * Gets the singleton instance of ScreensaverController.
     * @return ScreensaverController
     */
    public static synchronized ScreensaverController getInstance() {
        if (instance == null) { instance = new ScreensaverController(); }
        return instance;
    }

    /**
     * Instantiates a new screensaver controller.
     */
    private ScreensaverController() {
        screenSaverComp = new ScreenSaver();
    }

    /**
     * This method starts a SceneManager.
     */
    public void start() {
        Logger.info(this, "called start()");
        count = 0;
        CommunicationManager.getInstance().addScreenSaverListener();
        controllerThread = new Thread("ScreensaverController|controllerThread") {
            public void run() {
                while (controllerThread != null) {
                    //Logger.debug(this, "curScene : " + curScene);
                    try {
                        Thread.sleep(1000L);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (CommunicationManager.getInstance().isLoadingAnimation()) { continue; }
                    String screensaver = PreferenceProxy.getInstance().musicScreensaver;
                    if (curScene != null) {
                        /*if (PreferenceProxy.SCREENSAVER_PHOTO.equals(screensaver) && ++count % 10 == 0) {
                            Content photoRoot = BasicUI.contentRoot[BasicUI.MENU_PHOTO];
                            HNHandler.getInstance().setTotalCount(photoRoot);
                            count = 0;
                        }*/
                        continue;
                    }
                    SceneManager sceneManager = SceneManager.getInstance();
                    int curSceneId = sceneManager.curSceneId;
                    //Logger.debug(this, "curSceneId : " + curSceneId);
                    if (controllerThread == null || curSceneId == SceneManager.SC_INVALID) { break; }

                    BasicUI scene = sceneManager.scenes[curSceneId];
                    int musicType = BasicUI.musicPlayerPopup.playMusicType;
                    //Logger.debug(this, "musicType : " + musicType);
                    if (curSceneId == SceneManager.SC_VIDEO_PLAY || (scene != null && scene.isSlideShow())) {
                        count = 0;
                        //CommunicationManager.getInstance().setScreenSaverConfirm(false);
                    } else {
                        boolean isDefault = true;
                        if (musicType == MusicPlayerPopup.PLAY_MY_MUSIC || musicType == MusicPlayerPopup.PLAY_GALAXIE) {
                            Logger.debug(this, "screensaver : " + screensaver);
                            if (!screensaver.equals(PreferenceProxy.SCREENSAVER_NO)) {
                                isDefault = false;
                                /*if (screensaver.equals(PreferenceProxy.SCREENSAVER_PHOTO)) {
                                    Content photoRoot = BasicUI.contentRoot[BasicUI.MENU_PHOTO];
                                    if (count % 10 == 0) { HNHandler.getInstance().setTotalCount(photoRoot); }
                                    //if ((photoRoot == null || photoRoot.getTotalCount() < 1)) {
                                    //    isDefault = true;
                                    //}
                                }*/
                            }
                        }
                        long time = getWaitTime();
                        if (isDefault) { time = Config.defaultScreensaverSleepSec; }
                        final long waitTime = time;
                        final boolean isDefaultUI = isDefault;
                        //Logger.debug(this, "waitTime : " + waitTime);
                        //Logger.debug(this, "count : " + count);
                        if (count < waitTime) {
                            count++;
                            if (count == waitTime) {
                                if (activeOtherUI()) {
                                    count = 0;
                                } else {
                                    EventQueue.invokeLater(new Runnable() {
                                        public void run() {
                                            SceneManager sceneManager = SceneManager.getInstance();
                                            int curSceneId = sceneManager.curSceneId;
                                            if (controllerThread != null && curSceneId != SceneManager.SC_INVALID
                                                    && count == waitTime) {
                                                if (isDefaultUI) {
                                                    curScene = sceneManager.scenes[curSceneId];
                                                    CommunicationManager.getInstance().showScreenSaver();
                                                } else { addScreensaver(); }
                                                count = 0;
                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                controllerThread = null;
            }
        };
        controllerThread.start();
    }

    /**
     * Gets the wait time.
     *
     * @return the wait time
     */
    private long getWaitTime() {
        String waitStr = PreferenceProxy.getInstance().screensaverWait;
        //Logger.debug(this, "waitStr : " + waitStr);
        if (PreferenceProxy.WAIT_1.equals(waitStr)) {
            return 60;
        } else if (PreferenceProxy.WAIT_10.equals(waitStr)) {
            return 10 * 60;
        }
        return 5 * 60;
    }

    /**
     * This method dispose a SceneManager.
     */
    public void dispose() {
        count = 0;
        controllerThread = null;
        removeScreensaver();
        curScene = null;
        CommunicationManager.getInstance().removeScreenSaverListener();
    }

    /**
     * Reset count.
     *
     * @return true, if this key code is used.
     */
    public boolean keyAction() {
        count = 0;
        if (curScene != null) {
            removeScreensaver();
            return true;
        }
        return false;
    }

    /**
     * Checks if is active.
     *
     * @return true, if is active
     */
    public boolean isActive() {
        return curScene != null;
    }

    /**
     * add ScreenSaver component to UI.
     */
    private void addScreensaver() {
        Logger.debug(this, "called addScreensaver");
        if (curScene != null || controllerThread == null) { return; }
        try {
            SceneManager sceneManager = SceneManager.getInstance();
            int curSceneId = sceneManager.curSceneId;
            if (controllerThread == null || curSceneId == SceneManager.SC_INVALID) { return; }
            curScene = sceneManager.scenes[curSceneId];
            if (controllerThread == null || curSceneId == SceneManager.SC_INVALID || curScene == null) { return; }
            screenSaverComp.start();
            curScene.add(screenSaverComp, 0);
            curScene.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * remove ScreenSaver on UI.
     */
    private void removeScreensaver() {
        Logger.debug(this, "called removeScreensaver()");
        count = 0;
        screenSaverComp.stop();
        if (curScene == null) { return; }
        CommunicationManager.getInstance().hideScreenSaver();
        curScene.remove(screenSaverComp);
        curScene.repaint();
        curScene = null;
    }

    /**
     * called when other UI is covered on this UI.
     * @see com.alticast.ui.LayeredWindow#notifyShadowed()
     */
    public void notifyShadowed() {
        Logger.info(this, "notifyShadowed() called.");
        if (curScene != null && activeOtherUI()) { removeScreensaver(); }
    }

    /**
     * Active other ui.
     *
     * @return true, if successful
     */
    private boolean activeOtherUI() {
        LayeredUIInfo[] infos = LayeredUIManager.getInstance().getAllLayeredUIInfos();
        Logger.debug(this, "infos :" + infos);
        if (infos == null) { return false; }
        // screensaver를 제외한 것이 있으면 true.
        int priority = WindowProperty.PCS.getPriority();
        String name = WindowProperty.PCS.getName();
        for (int i = 0; i < infos.length; i++) {
            //Logger.debug(this, "infos[i] :" + infos[i]);
            if (infos[i] == null) { continue; }
            //Logger.debug(this, "infos[i] :" + infos[i].getPriority());
            //Logger.debug(this, "infos[i] :" + infos[i].getName());
            //Logger.debug(this, "infos[i] :" + infos[i].isActive());
            if (infos[i].getPriority() >= priority && !name.equals(infos[i].getName()) && infos[i].isActive()
                    && !infos[i].getBounds().equals(Rs.NONE_BOUND)
                    && !WindowProperty.SCREEN_SAVER.getName().equals(infos[i].getName())
                    && !WindowProperty.LOADING.getName().equals(infos[i].getName())) {
                return true;
            }
        }
        //UNBOUND_APP_UPDTAE_POPUP 일때만 true.
        /*String name = WindowProperty.UNBOUND_APP_UPDTAE_POPUP.getName();
        for (int i = 0; i < infos.length; i++) {
            if (infos[i] == null) { continue; }
            if (name.equals(infos[i].getName()) && infos[i].isActive()
                    && !infos[i].getBounds().equals(Rs.NONE_BOUND)) {
                return true;
            }
        }*/
        return false;
    }
}

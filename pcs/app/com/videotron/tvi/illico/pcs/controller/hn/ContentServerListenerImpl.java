/*
 *  @(#)ContentServerListenerImpl 1.0 2012.08.2
 *  Copyright (c) 2012 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.hn;

import org.ocap.hn.ContentServerEvent;
import org.ocap.hn.ContentServerListener;

import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.ui.BasicUI;

/**
 * implements ContentServerListener.
 * @since 2012.08.02
 * @author tklee
 * @version $Revision: 1.1 $ $Date: 2012/08/02 03:57:00 $
 */
public class ContentServerListenerImpl implements ContentServerListener {
    /**
     * Called when a ContentEntry has been added, changed or removed from the ContentServerNetModule.
     *
     * @param event ContentServerEvent
     * @see org.ocap.hn.ContentServerListener#contentUpdated(org.ocap.hn.ContentServerEvent)
     */
    public void contentUpdated(ContentServerEvent event) {
        Logger.info(this, "contentUpdated-event : " + event);
        Logger.info(this, "contentUpdated-event.getEventID() : " + event.getEventID());
        if (verifyStatus()) {
            Content[] contentRoot = BasicUI.contentRoot;
            for (int i = 0; verifyStatus() && contentRoot != null && i < contentRoot.length; i++) {
                HNHandler.getInstance().setTotalCount(contentRoot[i]);
            }
            if (verifyStatus() && contentRoot != null) {
                SceneManager sceneManager = SceneManager.getInstance();
                sceneManager.scenes[sceneManager.curSceneId].repaint();
            }
        }
    }

    /**
     * Verify status.
     *
     * @return true, if verify
     */
    private boolean verifyStatus() {
        return SceneManager.getInstance().curSceneId != SceneManager.SC_INVALID;
    }
}

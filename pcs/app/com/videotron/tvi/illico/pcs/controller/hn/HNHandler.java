/*
 *  @(#)HNHandler.java 1.0 2011.05.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.hn;

import java.util.Vector;

import org.ocap.hn.ContentServerNetModule;
import org.ocap.hn.Device;
import org.ocap.hn.NetActionRequest;
import org.ocap.hn.NetList;
import org.ocap.hn.NetManager;
import org.ocap.hn.NetModule;
import org.ocap.hn.NetworkInterface;
import org.ocap.hn.content.ContentContainer;
import org.ocap.hn.content.ContentEntry;
import org.ocap.hn.content.ContentItem;
import org.ocap.hn.content.navigation.ContentList;
import org.ocap.hn.profiles.upnp.UPnPConstants;
import org.ocap.hn.recording.RecordingNetModule;
import org.ocap.hn.security.NetSecurityManager;

import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.MainManager;
import com.videotron.tvi.illico.pcs.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.pcs.controller.communication.VbmController;
import com.videotron.tvi.illico.pcs.data.Content;
import com.videotron.tvi.illico.pcs.data.DeviceInfo;
import com.videotron.tvi.illico.pcs.ui.BasicUI;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class process the interface with homenetworking.
 * @since 2011.05.19
 * @author tklee
 * @version $Revision: 1.48 $ $Date: 2012/10/18 11:29:56 $
 */
public final class HNHandler {
    /** The Constant DEV_MODEL_NAME. */
    public static final String DEV_MODEL_NAME = "My PC on illico TV";
    /** The Constant NAME_PHOTO. */
    private static final String NAME_PHOTO = "Photos";
    /** The Constant NAME_MUSIC. */
    private static final String NAME_MUSIC = "Music";
    /** The Constant NAME_VIDEO. */
    private static final String NAME_VIDEO = "Video";
    /** The Constant NAME_FOLDER. */
    private static final String NAME_FOLDER = "Folders";
    /** The Constant NAME_ALL. */
    private static final String NAME_ALL = "All";
    /** The Constant NAME_M_AUDIO_INDEXES. */
    private static final String NAME_M_AUDIO_INDEXES = "Music Indexes";
    /** The Constant NAME_M_PLAYLISTS. */
    private static final String NAME_M_PLAYLISTS = "Playlists";
    /** The Constant NAME_M_GENRES. */
    private static final String NAME_M_GENRES = "Genres";
    /** The Constant NAME_M_ATRISTS. */
    //private static final String NAME_M_ATRISTS = "Artists";
    /** The Constant NAME_M_ALBUMS. */
    private static final String NAME_M_ALBUMS = "Albums";
    /** The Constant NAME_M_ARTISTS_ALLBUMS. */
    private static final String NAME_M_ARTISTS_ALLBUMS = "Artists / Albums";
    /** The Constant RES_DURATION. */
    private static final String RES_DURATION = "res@duration";
    /** The Constant RES. */
    private static final String RES = "res";
    /** The Constant RES_RESOLUTION. */
    private static final String RES_RESOLUTION = "res@resolution";
    /** The Constant CHILD_COUNT. */
    public static final String CHILD_COUNT = "@childCount";

    /** Class instance. */
    private static HNHandler instance = null;

    /** The NetManager instance. */
    private NetManager netManager = null;
    /** The DeviceEventListenerImpl instance. */
    private DeviceEventListenerImpl devEventListener = null;
    /** The content server listener. */
    private ContentServerListenerImpl contentServerListener = null;
    /** The cs net module. */
    private ContentServerNetModule csNetModule = null;
    /** The file extensions. */
    private StringBuffer fileExts = null;
    /** The request buffer. */
    private final Vector requestBuffer = new Vector();
    /** true if this instance is valid state. */
    private boolean isValid = false;
    /** true if The moca pw is set. */
    private boolean isMocaSet = false;
    /** The timeout. */
    private long timeout = 20000L;
    /** The is new browse. */
    private boolean isNewBrowse = false;

    /**
     * Get HNHandler instance.
     * @return HNHandler
     */
    public static synchronized HNHandler getInstance() {
        if (instance == null) { instance = new HNHandler(); }
        return instance;
    }

    /**
     * Instantiates a new HNHandler.
     */
    private HNHandler() {
        devEventListener = new DeviceEventListenerImpl();
        contentServerListener = new ContentServerListenerImpl();
        timeout = Config.dlnaRequestTimeoutSec * 1000L;
        //createdItems = new Hashtable();
        //requestThread = new RequestThread();
        //init();
    }

    /**
     * Instantiates HN module if netManager is null.
     *
     * @param isSleeping the is sleeping
     */
    public void init(boolean isSleeping) {
        requestBuffer.clear();
        //Logger.debug(this, "init()-netManager : " + netManager);
        //if (netManager != null) { return; }
        Logger.debug(this, "setMocaPw : " + Config.setMocaPw);
        netManager = NetManager.getInstance();
        Logger.debug(this, "init()-netManager : " + netManager);
        if (netManager != null && !isMocaSet) {
            NetworkInterface [] netInterfaces = NetworkInterface.getNetworkInterfaces();
            Logger.debug(this, "init()-netInterfaces : " + netInterfaces);
            if (netInterfaces != null) {
                Logger.debug(this, "init()-netInterfaces.length : " + netInterfaces.length);
                for (int i = 0; i < netInterfaces.length; i++) {
                    Logger.debug(this, "init()-netInterfaces[i].getType() : " + netInterfaces[i].getType());
                    try {
                        if (netInterfaces[i].getType() == NetworkInterface.MOCA) {
                            NetSecurityManager  securityManager = NetSecurityManager.getInstance();
                            if (securityManager != null) {
                                securityManager.setAuthorizationHandler(new NetAuthorizationHandlerImpl());
                            }
                            securityManager.enableMocaPrivacy(netInterfaces[i]);
                            Logger.debug(this, "setMocaPw : " + Config.setMocaPw);
                            if (Config.setMocaPw) {
                                securityManager.setNetworkPassword(netInterfaces[i], "71029509025718750");
                            }
                            isMocaSet = true;
                            if (isSleeping) {
                                try {
                                    Thread.sleep(1000);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Dispose.
     */
    public void dispose() {
        Logger.debug(this, "called dispose()");
        if (netManager != null) { netManager.removeDeviceEventListener(devEventListener); }
        if (csNetModule != null) { csNetModule.removeContentServerListener(contentServerListener); }
        csNetModule = null;
        isValid = false;
        fileExts = null;
        Vector cloneReqBuffer = (Vector) requestBuffer.clone();
        Logger.debug(this, "dispose()-requestBuffer.size() : " + cloneReqBuffer.size());
        for (int i = 0; i < cloneReqBuffer.size(); i++) {
            try {
                NetActionRequest request = (NetActionRequest) cloneReqBuffer.elementAt(i);
                request.cancel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        cloneReqBuffer = null;
        requestBuffer.clear();
        //createdItems.clear();
    }

    /**
     * Verify pc.
     *
     * @param device the device
     * @return true, if successful
     */
    public boolean verifyPC(Device device) {
        Logger.debug(this, "verifyPC()-device : " + device);
        if (device == null) { return false; }
        Logger.debug(this, "verifyPC()-device.getType() : " + device.getType());
        Logger.debug(this, "verifyPC()-device.isLocal() : " + device.isLocal());
        String friendlyName = device.getProperty(Device.PROP_FRIENDLY_NAME);
        Logger.debug(this, "verifyPC()-friendlyName : " + friendlyName);

        if (device == null || device.isLocal() || !Device.TYPE_MEDIA_SERVER.equals(device.getType())) { return false; }
        //this is added logic. - VDTRMASTER-3723 Do not display non-tversity servers in the list of found servers
        String modelName = device.getProperty(Device.PROP_MODEL_NAME);
        Logger.debug(this, "verifyPC()-modelName : " + modelName);
        /*Logger.debug(this, "verifyPC()-version : " + device.getVersion());
        Logger.debug(this, "verifyPC()-device version : " + device.getProperty(Device.PROP_DEVICE_VERSION));
        Logger.debug(this, "verifyPC()-model number : " + device.getProperty(Device.PROP_MODEL_NUMBER));
        Logger.debug(this, "verifyPC()-serial number : " + device.getProperty(Device.PROP_SERIAL_NUMBER));*/
        if (!DEV_MODEL_NAME.equals(modelName)) { return false; }
        /*if (Config.validModel != null
                && (modelName == null || !Config.validModel.containsKey(modelName.toLowerCase()))) {
            return false;
        }*/
        //
        ContentServerNetModule csModule = (ContentServerNetModule) device.getNetModule(NetModule.CONTENT_SERVER);
        Logger.debug(this, "verifyPC()-csModule : " + csModule);
        RecordingNetModule recModule = (RecordingNetModule) device.getNetModule(NetModule.CONTENT_RECORDER);
        Logger.debug(this, "verifyPC()-recModule : " + recModule);
        return csModule != null && recModule == null;
        //|| !DEV_MODEL_NAME.equalsIgnoreCase(device.getProperty(Device.PROP_MODEL_NAME))
    }

    /**
     * Gets the device list.
     *
     * @return the device list
     */
    public Vector getDeviceInfos() {
        Logger.debug(this, "called getDeviceInfos()");
        isValid = true;
        init(true);
        /*if (Environment.EMULATOR) {
            Vector testData = new Vector();
            testData.addElement(new DeviceInfo());
            testData.addElement(new DeviceInfo());
            return testData;
        }*/
        if (netManager == null) { return null; }
        try {
            netManager.updateDeviceList();
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            NetList netList = netManager.getDeviceList(null);
            netManager.addDeviceEventListener(devEventListener);
            Logger.debug(this, "getDeviceInfos()-netList : " + netList);
            if (netList == null || netList.size() == 0) { return null; }
            Logger.debug(this, "getDeviceInfos()-netList.size() : " + netList.size());
            Vector list = new Vector();
            Vector secondList = new Vector();
            for (int i = 0; i < netList.size(); i++) {
                Device device = (Device) netList.getElement(i);
                /*Logger.debug(this, "========================================================");
                Logger.debug(this, "device name : " + device.getName());
                Enumeration e = device.getKeys();
                while (e.hasMoreElements()) {
                    String key = (String) e.nextElement();
                    Logger.debug(this, key + " : " + device.getProperty(key));
                }
                Logger.debug(this, "InetAddress : " + device.getInetAddress());
                Logger.debug(this, "========================================================");*/
                if (!HNHandler.getInstance().verifyPC(device)) { continue; }
                if (!isValid) { break; }
                DeviceInfo info = new DeviceInfo();
                info.setDevice(device);
                info.setAppInstalled(DEV_MODEL_NAME.equals(device.getProperty(Device.PROP_MODEL_NAME)));
                //String modelName = device.getProperty(Device.PROP_MODEL_NAME);
                //info.setAppInstalled(Config.validModel.containsKey(modelName.toLowerCase()));
                String favorUDN = PreferenceProxy.getInstance().favorUDN;
                if (favorUDN != null && favorUDN.length() > 0 && favorUDN.equals(info.getUDN())) {
                    list.add(0, info);
                } else if (info.isAppInstalled()) {
                    list.addElement(info);
                } else { secondList.addElement(info); }
            }
            list.addAll(secondList);
            if (list.size() > 0) {
                return list;
                /*DeviceInfo[] infos = new DeviceInfo[list.size()];
                list.copyInto(infos);
                return infos;*/
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.error(this, e.toString());
        }
        return null;
    }

    /**
     * Gets the content root with content list.
     *
     * @param device the device
     * @return the content root for music, photo and videos
     */
    public Content[] getContentRoot(Device device) {
        Logger.info(this, "called getContentRoot()-device : " + device);
        if (device == null) { return null; }
        if (csNetModule != null) { csNetModule.removeContentServerListener(contentServerListener); }
        csNetModule = (ContentServerNetModule) device.getNetModule(NetModule.CONTENT_SERVER);
        csNetModule.addContentServerListener(contentServerListener);
        Logger.info(this, "getContentRoot()-completed to call addContentServerListener()");
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        NetActionRequest request = csNetModule.requestRootContainer(handler);
        Logger.info(this, "getContentRoot()-request : " + request);
        try {
            Logger.info(this, "getContentRoot()-request.getActionStatus() : " + request.getActionStatus());
            if (handler.response == null) {
                synchronized (request) {
                    request.wait(timeout);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!isValid) { return null; }
        //requestThread.start();
        ContentContainer rootContainer = (ContentContainer) handler.response;
        Content[] rootContent = null;
        if (rootContainer != null) {
            ContentList rootList = requestBrowseEntries("0", "", true);
            Logger.debug(this, "rootList : " + rootList);
            if (rootList != null) { Logger.debug(this, "rootList.size() : " + rootList.size()); }
            Logger.debug(this, "getContentRoot() - isValid : " + isValid);
            if (!isValid) { return null; }
            if (rootList != null && rootList.size() > 0) {
                rootContent = new Content[3];
                while (rootList.hasMoreElements()) {
                    ContentEntry rootEntry = (ContentEntry) rootList.nextElement();
                    Logger.debug(this, "=================================");
                    String protocolInfo = getMetaData(rootEntry, "res@protocolInfo");
                    Logger.debug(this, "getContentRoot() - protocolInfo : " + protocolInfo);
                    String classs = getMetaData(rootEntry, "upnp:class");
                    Logger.debug(this, "getContentRoot() - classs : " + classs);
                    Logger.debug(this, "=================================");
                    String rootName = getMetaData(rootEntry, UPnPConstants.TITLE);
                    Logger.debug(this, "rootName : " + rootName);
                    Logger.debug(this, "rootEntry : " + rootEntry);
                    if (rootName == null || !(rootEntry instanceof ContentContainer)) { continue; }
                    ContentContainer container = (ContentContainer) rootEntry;
                    //if (container.isEmpty()) { continue; }
                    ContentList cList = requestBrowseEntries(container.getID(), "", true);
                    Logger.debug(this, "cList : " + cList);
                    if (cList != null) { Logger.debug(this, "cList.size() : " + cList.size()); }
                    if (!isValid) { return null; }
                    if (cList != null && cList.size() > 0) {
                        Content content = new Content();
                        while (cList.hasMoreElements()) {
                            ContentEntry entry = (ContentEntry) cList.nextElement();
                            if (!(entry instanceof ContentContainer)) { continue; }
                            if (!isValid) { return null; }
                            ContentContainer subContainer = (ContentContainer) entry;
                            String entryName = getMetaData(entry, UPnPConstants.TITLE);
                            Logger.debug(this, "getContentRoot()-entryName : " + entryName);
                            if (entryName.equalsIgnoreCase(NAME_ALL)) {
                                String strChildCount = getMetaData(entry, CHILD_COUNT);
                                Logger.debug(this, "getContentRoot() - childCount : " + strChildCount);
                                if (strChildCount != null) { content.setTotalCount(Integer.parseInt(strChildCount)); }
                                //content.setContentAllEntry(entry);
                                content.setAllEntryId(entry.getID());
                            }
                            if (rootName.startsWith(NAME_PHOTO) || rootName.startsWith(NAME_VIDEO)) {
                                if (entryName.equalsIgnoreCase(NAME_FOLDER)) {
                                    content.setId(entry.getID());
                                    content.setParentId(entry.getParentID());
                                    //content.setContentEntry(entry);
                                }
                            } else if (rootName.startsWith(NAME_MUSIC)
                                    && entryName.equalsIgnoreCase(NAME_M_AUDIO_INDEXES)) {
                                content.setId(entry.getID());
                                content.setParentId(entry.getParentID());
                                //content.setContentEntry(entry);
                                setMusicRoot(subContainer);
                            }
                            content.setFolder(entry instanceof ContentContainer);
                            if (!isValid) { return null; }
                        }
                        if (rootName.startsWith(NAME_PHOTO)) {
                            rootContent[BasicUI.MENU_PHOTO] = content;
                        } else if (rootName.startsWith(NAME_VIDEO)) {
                            rootContent[BasicUI.MENU_VIDEO] = content;
                        } else { rootContent[BasicUI.MENU_MUSIC] = content; }
                    }
                    if (!isValid) { return null; }
                }
                // test
                /*for (int i = 0; i < rootContent.length; i++) {
                    rootContent[i] = new Content();
                    rootContent[i].setId(rootContainer.getID());
                    rootContent[i].setTotalCount(10);
                    rootContent[i].setContentEntry(rootContainer);
                }*/
                //
            }
            String[] name = {NAME_PHOTO, NAME_MUSIC, NAME_VIDEO};
            for (int i = 0; i < name.length; i++) {
                if (rootContent == null || rootContent[i] == null) {
                    MainManager.getInstance().printError(MainManager.ERR_NO_DISCOVERY,
                            device.getName() + " : " + name[i]);
                }
            }
        } else {
            MainManager.getInstance().printError(MainManager.ERR_NO_DISCOVERY, device.getName() + " : RootContainer");
        }
        boolean isConnected = rootContent != null && BasicUI.musicRoot != null;
        for (int i = 0; isConnected && rootContent != null && i < rootContent.length; i++) {
            if (rootContent[i] == null) {
                //rootContent[i] = new Content();
                isConnected = false;
            }
        }
        if (!isConnected) { return null; }
        return rootContent;
    }

    /**
     * Sets the music root.
     *
     * @param musicContainer the ContentContainer for music
     */
    private void setMusicRoot(ContentContainer musicContainer) {
        ContentList cList = requestBrowseEntries(musicContainer.getID(), "", true);
        if (!isValid) { return; }
        Logger.debug(this, "setMusicRoot()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "setMusicRoot()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            BasicUI.musicRoot = new Content[5];
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!(entry instanceof ContentContainer)) { continue; }
                if (!isValid) { return; }
                ContentContainer subContainer = (ContentContainer) entry;
                String entryName = getMetaData(entry, UPnPConstants.TITLE);
                Logger.debug(this, "setMusicRoot()-entryName : " + entryName);
                Logger.debug(this, "=================================");
                String protocolInfo = getMetaData(entry, "res@protocolInfo");
                Logger.debug(this, "setMusicRoot() - protocolInfo : " + protocolInfo);
                String classs = getMetaData(entry, "upnp:class");
                Logger.debug(this, "setMusicRoot() - classs : " + classs);
                Logger.debug(this, "=================================");
                Content content = new Content();
                int index = 0;
                if (entryName.equalsIgnoreCase(NAME_ALL)) {
                    index = BasicUI.SEQ_SONGS;
                } else if (entryName.equalsIgnoreCase(NAME_M_ALBUMS)) {
                    index = BasicUI.SEQ_ALBUM;
                } else if (entryName.equalsIgnoreCase(NAME_M_GENRES)) {
                    index = BasicUI.SEQ_GENRES;
                } else if (entryName.equalsIgnoreCase(NAME_M_PLAYLISTS)) {
                    index = BasicUI.SEQ_PLAYLISTS;
                } else if (entryName.equalsIgnoreCase(NAME_M_ARTISTS_ALLBUMS)) {
                    index = BasicUI.SEQ_ARTIST;
                } else { continue; }
                content.setId(entry.getID());
                content.setParentId(entry.getParentID());
                //content.setContentEntry(entry);
                content.setFolder(entry instanceof ContentContainer);
                BasicUI.musicRoot[index] = content;
                if (!isValid) { return; }
            }
        }
    }

    /**
     * Gets the sort criteria.
     *
     * @param sortType the sort type
     * @return the sort criteria
     */
    private String getSortCriteria(int sortType) {
        String sortCriteria = "";
        if (sortType == Content.SORT_ALPHA_A) {
            sortCriteria = "+" + UPnPConstants.TITLE;
        } else  if (sortType == Content.SORT_ALPHA_Z) {
            sortCriteria = "-" + UPnPConstants.TITLE;
        } else if (sortType == Content.SORT_NEWEST) {
            sortCriteria = "-" + UPnPConstants.CREATION_DATE;
        } else if (sortType == Content.SORT_OLDEST) {
            sortCriteria = "+" + UPnPConstants.CREATION_DATE;
        }
        return sortCriteria;
    }

    /**
     * Gets the browse entry.
     *
     * @param parentId the parent id
     * @param contentType the content type
     * @return the browse entry
     */
    private ContentEntry getBrowseEntry(String parentId, int contentType) {
        ContentList cList = requestBrowseEntries(parentId, "*", "", false, 0, 1);
        if (!isValid) { return null; }
        Logger.debug(this, "getBrowseEntry()-cList : " + cList);
        if (cList == null || cList.size() == 0) { return null; }
        int childCount = -1;
        ContentEntry entry = (ContentEntry) cList.nextElement();
        while (true) {
            childCount = getChildCount(entry);
            if (childCount == 1 && (contentType == BasicUI.MENU_PHOTO || contentType == BasicUI.MENU_VIDEO)) {
                cList = requestBrowseEntries(entry.getID(), "*", "", true, 0, 1);
                if (!isValid) { return null; }
                Logger.debug(this, "getBrowseEntry()-cList : " + cList);
                if (cList == null || cList.size() == 0) { return null; }
                ContentEntry tempEntry = (ContentEntry) cList.nextElement();
                if (tempEntry instanceof ContentContainer) {
                    entry = tempEntry;
                    continue;
                }
            }
            break;
        }
        return entry;
    }

    /**
     * Gets the child count.
     *
     * @param parentId the parent id
     * @param sortType the sort type
     * @return the child count
     */
    public int getChildCountForArtist(String parentId, int sortType) {
        int totalCount = 0;
        int startIndex = 0;
        while (true) {
            ContentList cList =
                requestBrowseEntries(parentId, CHILD_COUNT, getSortCriteria(sortType), true,
                        startIndex, Config.loadDataNumber);
            if (!isValid) { return -1; }
            Logger.debug(this, "getChildCountForArtist()-cList : " + cList);
            if (cList == null) { return -1; }
            int size = cList.size();
            Logger.debug(this, "getChildCountForArtist()-cList.size() : " + size);
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!isValid) { return -1; }
                int childCount = getChildCount(entry);
                if (childCount == -1) { return -1; }
                totalCount += childCount;
            }
            if (size < Config.loadDataNumber) { return totalCount; }
            startIndex += Config.loadDataNumber;
        }
    }

    /**
     * Gets the child count.
     *
     * @param entry the entry
     * @return the child count
     */
    private int getChildCount(ContentEntry entry) {
        int childCount = -1;
        String strChildCount = getMetaData(entry, CHILD_COUNT);
        Logger.debug(this, "getChildCount() - strChildCount : " + strChildCount);
        if (strChildCount != null) { childCount = Integer.parseInt(strChildCount); }
        return childCount;
    }

    /**
     * Prepare child node.
     *
     * @param parentId the parent id
     * @param parent the parent
     * @param contentType the content type
     * @param sortType the sort type
     * @return true, if successful
     */
    public boolean prepareChildNode(String parentId, Content parent, int contentType, int sortType) {
        parent.setChildren(null);
        parent.setBrowseId(null);
        parent.setChildCount(0);
        int childCount = -1;
        if (parent.isArtistAllsongsDir()) {
            parent.setBrowseId(parent.getId());
            childCount = getChildCountForArtist(parent.getId(), sortType);
        } else {
            ContentEntry browseEntry = getBrowseEntry(parentId, contentType);
            if (browseEntry != null) {
                parent.setBrowseId(browseEntry.getID());
                childCount = getChildCount(browseEntry);
            }
        }
        if (childCount > 0) {
            parent.setChildCount(childCount);
            return true;
        }
        return false;
    }

    /**
     * Sets the child node.
     *
     * @param parent the parent
     * @param contentType the content type
     * @param sortType the sort type
     * @param startingIndex the starting index
     * @param requestedCount the requested count
     * @return the vector
     */
    public Vector setChildNode(Content parent, int contentType, int sortType, int startingIndex,
            int requestedCount) {
        /*parent.setChildren(null);
        parent.setBrowseId(null);
        parent.setChildCount(0);
        Vector result = null;
        int childCount = -1;
        if (parent.isArtistAllsongsDir()) {
            parent.setBrowseId(parent.getId());
            childCount = getChildCountForArtist(parent.getId(), sortType);
        } else {
            ContentEntry browseEntry = getBrowseEntry(parentId, contentType);
            if (browseEntry != null) {
                parent.setBrowseId(browseEntry.getID());
                childCount = getChildCount(browseEntry);
            }
        }
        if (childCount > 0) {
            parent.setChildCount(childCount);
            result = setChildNodeForPaging(parent, contentType, sortType, startingIndex, requestedCount);
        }
        return result;*/
        return setChildNodeForPaging(parent, contentType, sortType, startingIndex, requestedCount);
    }

    /**
     * Sets the child node.
     *
     * @param parent the parent
     * @param contentType the content type
     * @param sortType the sort type
     * @param startingIndex the starting index
     * @param requestedCount the requested count
     * @return the vector
     */
    public Vector setChildNodeForPaging(Content parent, int contentType, int sortType, int startingIndex,
            int requestedCount) {
        isNewBrowse = true;
        synchronized (this) {
            if (!isValid) { return null; }
            isNewBrowse = false;
            fileExts = new StringBuffer();
            //Vector result = browseChildNode(parent.getBrowseEntry().getID(), parent, contentType, sortType,
            Vector result = browseChildNode(parent.getBrowseId(), parent, contentType, sortType,
                    startingIndex, requestedCount);
            if (!isValid) { return null; }
            if (fileExts != null && fileExts.length() > 0) {
                VbmController.getInstance().writeFileExtLog(fileExts.toString());
            }
            fileExts = null;
            return result;
        }
    }

    /**
     * Sets the child node.
     *
     * @param parentId the parent id
     * @param parent the parent
     * @param contentType the content type
     * @param sortType the sort type
     * @param startingIndex the starting index
     * @param requestedCount the requested count
     * @return true, if successful
     */
    private Vector browseChildNode(String parentId, Content parent, int contentType, int sortType, int startingIndex,
            int requestedCount) {
        if (!isValid || isNewBrowse) { return null; }
        Logger.debug(this, "startingIndex : " + startingIndex);
        Logger.debug(this, "requestedCount : " + requestedCount);
        ContentList cList = null;
        if (parent.isArtistAllsongsDir()) {
            String searchCriteria = "upnp:class derivedfrom \"object.item\"";
            cList = requestSearchEntries(parent.getId(), searchCriteria, "*", getSortCriteria(sortType), startingIndex,
                    requestedCount);
        } else {
            cList = requestBrowseEntries(parentId, "*", getSortCriteria(sortType), true, startingIndex, requestedCount);
        }
        if (!isValid || isNewBrowse) { return null; }
        Logger.debug(this, "cList : " + cList);
        if (cList != null) { Logger.debug(this, "cList.size() : " + cList.size()); }
        if (cList == null || cList.size() == 0) { return null; }

        Vector contentList = new Vector();
        while (cList.hasMoreElements()) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            if (!isValid || isNewBrowse) { return null; }
            if (entry instanceof ContentContainer) {
                int childCount = getChildCount(entry);
                if (childCount == 1 && (contentType == BasicUI.MENU_PHOTO || contentType == BasicUI.MENU_VIDEO)) {
                    ContentEntry browseEntry = getBrowseEntry(entry.getID(), contentType);
                    if (browseEntry != null) { entry = browseEntry; }
                }
            }
            Content content = new Content();
            //content.setContentEntry(entry);
            content.setFolder(entry instanceof ContentContainer);
            String entryName = getMetaData(entry, UPnPConstants.TITLE);
            Logger.debug(this, "setChildNode() - entryName : " + entryName);
            setDate(content, entry);
            content.setName(entryName);
            content.setParent(parent);
            content.setId(entry.getID());
            content.setParentId(entry.getParentID());
            String iconUrl = getMetaData(entry, UPnPConstants.ICON_REF);
            Logger.debug(this, "setChildNode() - iconUrl : " + iconUrl);
            Logger.debug(this, "=================================");
            String protocolInfo = getMetaData(entry, "res@protocolInfo");
            Logger.debug(this, "getContentRoot() - protocolInfo : " + protocolInfo);
            String classs = getMetaData(entry, "upnp:class");
            Logger.debug(this, "getContentRoot() - classs : " + classs);
            Logger.debug(this, "=================================");
            if (entry instanceof ContentContainer) {
                int childCount = getChildCount(entry);
                if (childCount > -1) {
                    content.setTotalCount(childCount);
                    content.setChildCount(childCount);
                }
                if (contentType == BasicUI.MENU_PHOTO || contentType == BasicUI.MENU_VIDEO) {
                    String searchUrl = requestSearchPhotoUrl(content.getId(), sortType, contentType, true);
                    //if (searchUrl != null) { iconUrl = searchUrl; }
                    content.setIconUrl(searchUrl);
                }
                //if (musicRoot == null || parent != musicRoot[BasicUI.SEQ_GENRES]) { content.setIconUrl(iconUrl); }
            } else if (entry instanceof ContentItem) {
                String resUrl = getMetaData(entry, RES);
                Logger.debug(this, "setChildNode() - resUrl : " + resUrl);
                //for vbm log
                if (resUrl != null) {
                    int index = resUrl.lastIndexOf(".");
                    if (index != -1) {
                        String ext = resUrl.substring(index + 1);
                        //Logger.debug(this, "setChildNode() - extension : " + ext);
                        if (fileExts != null) {
                            if (fileExts.length() > 0) { fileExts.append(VbmController.VALUE_SEPARATOR); }
                            fileExts.append(ext);
                        }
                    }
                }
                String imgUrl = null;
                if (contentType == BasicUI.MENU_PHOTO) {
                    imgUrl = resUrl;
                    content.setImgUrl(imgUrl);
                } else {
                    content.setResourceUrl(resUrl);
                    imgUrl = getMetaData(entry, UPnPConstants.ALBUM_ART);
                    Logger.debug(this, "setChildNode() - imgUrl : " + imgUrl);
                    content.setImgUrl(imgUrl);
                }
                if (contentType == BasicUI.MENU_PHOTO || contentType == BasicUI.MENU_VIDEO) {
                    content.setIconUrl(iconUrl);
                }
                setResolution(content, entry);
                content.setService(((ContentItem) entry).getItemService());
            }
            setDuraton(content, entry);

            Logger.debug(this, "setChildNode() - TRACK_NUMBER : " + getMetaData(entry, UPnPConstants.TRACK_NUMBER));
            String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
            Logger.debug(this, "setChildNode() - albumArt : " + albumArt);
            /*if (contentType != BasicUI.MENU_MUSIC || !(entry instanceof ContentContainer)
                    || musicRoot == null || parent != musicRoot[BasicUI.SEQ_GENRES]) {
                if (albumArt != null || contentType == BasicUI.MENU_MUSIC) { content.setIconUrl(albumArt); }
            }*/
            if (contentType == BasicUI.MENU_MUSIC) { content.setIconUrl(albumArt); }
            String album = getMetaData(entry, UPnPConstants.ALBUM);
            Logger.debug(this, "setChildNode() - album : " + album);
            content.setAlbumName(album);
            String artist = getMetaData(entry, UPnPConstants.ARTIST);
            Logger.debug(this, "setChildNode() - artist : " + artist);
            content.setArtist(artist);
            String genre = getMetaData(entry, UPnPConstants.GENRE);
            Logger.debug(this, "setChildNode() - genre : " + genre);
            content.setGenre(genre);
            if (!isValid || isNewBrowse) { return null; }
            contentList.addElement(content);
        }
        return contentList;
    }

    /**
     * Gets the play content.
     *
     * @param entryId the entry id
     * @param contentType the content type
     * @param sortType the sort type
     * @param startIndex the start index
     * @param isArtistAllsongsDir the is artist allsongs dir
     * @param searchId the search id
     * @return the play content
     */
    public Content getPlayContent(String entryId, int contentType, int sortType, int startIndex,
            boolean isArtistAllsongsDir, String searchId) {
        ContentList cList = null;
        String propertyFilter = "";
        String searchCriteria = "upnp:class derivedfrom \"object.item\" and @parentID = \"" + entryId
            + "\" and @id = \"" + searchId + "\"";
        Logger.debug(this, "getPlayContent()-searchCriteria : " + searchCriteria);
        if (contentType == BasicUI.MENU_VIDEO) {
            //cList = requestBrowseEntries(entryId,
            //        UPnPConstants.TITLE + "," + RES_DURATION + "," + RES_RESOLUTION, "", false, 0, 1);
            propertyFilter = UPnPConstants.TITLE + "," + RES_DURATION + "," + RES_RESOLUTION;
            cList = requestSearchEntries(entryId, searchCriteria, propertyFilter, getSortCriteria(sortType), 0, 1);
        } else if (contentType == BasicUI.MENU_PHOTO) {
            propertyFilter = UPnPConstants.TITLE + "," + RES + "," + RES_RESOLUTION;
            if (startIndex != -1) {
                cList = requestBrowseEntries(entryId, propertyFilter, getSortCriteria(sortType), true, startIndex, 1);
            } else {
                //cList = requestBrowseEntries(entryId, propertyFilter, getSortCriteria(sortType), false, 0, 1);
                cList = requestSearchEntries(entryId, searchCriteria, propertyFilter, getSortCriteria(sortType),
                        0, 1);
            }
        } else if (contentType == BasicUI.MENU_MUSIC) {
            propertyFilter = UPnPConstants.TITLE + "," + RES_DURATION + "," + RES_RESOLUTION
                + "," + UPnPConstants.ARTIST + "," + UPnPConstants.ALBUM + "," + UPnPConstants.ALBUM_ART
                + "," + UPnPConstants.GENRE;
            if (startIndex != -1) {
                if (isArtistAllsongsDir) {
                    searchCriteria = "upnp:class derivedfrom \"object.item\"";
                    cList = requestSearchEntries(entryId, searchCriteria, propertyFilter, getSortCriteria(sortType),
                            startIndex, 1);
                } else {
                    cList = requestBrowseEntries(entryId, propertyFilter, getSortCriteria(sortType), true,
                            startIndex, 1);
                }
            } else {
                //cList = requestBrowseEntries(entryId, propertyFilter, getSortCriteria(sortType), false, 0, 1);
                cList = requestSearchEntries(entryId, searchCriteria, propertyFilter, getSortCriteria(sortType),
                        0, 1);
            }
        }
        if (!isValid) { return null; }
        Logger.debug(this, "getPlayContent()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "getPlayContent()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            Content content = new Content();
            //content.setContentEntry(entry);
            content.setFolder(entry instanceof ContentContainer);
            String entryName = getMetaData(entry, UPnPConstants.TITLE);
            setDate(content, entry);
            content.setName(entryName);
            content.setId(entry.getID());
            content.setParentId(entry.getParentID());
            String iconUrl = getMetaData(entry, UPnPConstants.ICON_REF);
            if (entry instanceof ContentContainer) {
                int childCount = getChildCount(entry);
                if (childCount > -1) { content.setTotalCount(childCount); }
            } else if (entry instanceof ContentItem) {
                content.setService(((ContentItem) entry).getItemService());
                String resUrl = getMetaData(entry, RES);
                if (contentType == BasicUI.MENU_PHOTO) {
                    content.setImgUrl(resUrl);
                } else { content.setResourceUrl(resUrl); }
                content.setIconUrl(iconUrl);
                setResolution(content, entry);
            }
            setDuraton(content, entry);

            String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
            if (contentType == BasicUI.MENU_MUSIC) { content.setIconUrl(albumArt); }
            String album = getMetaData(entry, UPnPConstants.ALBUM);
            content.setAlbumName(album);
            String artist = getMetaData(entry, UPnPConstants.ARTIST);
            content.setArtist(artist);
            String genre = getMetaData(entry, UPnPConstants.GENRE);
            content.setGenre(genre);
            return content;
        }
        return null;
    }

    /**
     * Sets the date.
     *
     * @param content the new date
     * @param entry the entry
     */
    private void setDate(Content content, ContentEntry entry) {
        String date = getMetaData(entry, UPnPConstants.CREATION_DATE);
        Logger.debug(this, "setDate() - date : " + date);
        if (date == null) { return; }
        int index = date.indexOf("T");
        if (index != -1) { date = date.substring(0, index); }
        content.setDate(date);
        String sortDate = date;
        if (sortDate != null) {
            String[] yymmdd = TextUtil.tokenize(sortDate, "-");
            int length = yymmdd.length;
            if (length == 3) {
                sortDate = yymmdd[0];
                for (int i = 1; i < length; i++) {
                    if (yymmdd[i].length() == 1) {
                        sortDate += "-0" + yymmdd[i];
                    } else { sortDate += "-" + yymmdd[i]; }
                }
            }
        }
        Logger.debug(this, "setDate() - sortDate : " + sortDate);
    }

    /**
     * Sets the resolution.
     *
     * @param content the new resolution
     * @param entry the entry
     */
    private void setResolution(Content content, ContentEntry entry) {
        String resolution = getMetaData(entry, RES_RESOLUTION);
        Logger.debug(this, "setResolution() - resolution : " + resolution);
        if (resolution != null) {
            int index = resolution.indexOf("x");
            if (index != -1) {
                int[] value = new int[2];
                value[0] = Integer.parseInt(resolution.substring(0, index));
                value[1] = Integer.parseInt(resolution.substring(index + 1));
                content.setResolution(value);
            }
        }
    }

    /**
     * Sets the duraton.
     *
     * @param content the new duraton
     * @param entry the entry
     */
    private void setDuraton(Content content, ContentEntry entry) {
        String durationStr = getMetaData(entry, RES_DURATION);
        Logger.debug(this, "setDuraton() - durationStr : " + durationStr);
        if (durationStr != null && durationStr.length() > 0) {
            int sec = 0;
            String str = durationStr;
            int pointIdx = str.indexOf(".");
            if (pointIdx != -1) { str = str.substring(0, pointIdx); }
            int idx = str.indexOf(":");
            for (int i = 0; idx != -1; i++) {
                String temp = str.substring(0, idx);
                str = str.substring(idx + 1);
                if (i == 0) {
                    sec += Integer.parseInt(temp) * 60 * 60;
                } else if (i == 1) {
                    sec += Integer.parseInt(temp) * 60;
                    sec += Integer.parseInt(str);
                    break;
                }
                idx = str.indexOf(":");
            }
            content.setDuration(sec);
            content.setDurationLong(sec * 1000L);
            if (pointIdx != -1) {
                str = durationStr.substring(pointIdx + 1);
                if (str != null && str.length() > 0) {
                    long durationLong = sec * 1000L + Integer.parseInt(str);
                    content.setDurationLong(durationLong);
                }
            }
        }
    }

    /**
     * Gets the all photo for screensaver.
     *
     * @param parentId the parent id
     * @param startingIndex the starting index
     * @return the photo url for screensaver
     */
    public String getPhotoUrlForScreensaver(String parentId, int startingIndex) {
        ContentList cList = requestBrowseEntries(parentId, RES, "", true, startingIndex, 1);
        if (!isValid) { return null; }
        Logger.debug(this, "getAllPhotoForScreensaver()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "getAllPhotoForScreensaver()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            if (!isValid) { return null; }
            String resUrl = getMetaData(entry, RES);
            return resUrl;
        }
        return null;
    }

    /**
     * Sets the total count.
     *
     * @param parent the new total count
     */
    public void setTotalCount(Content parent) {
        Logger.debug(this, "called setTotalCount()");
        if (parent == null) { return; }
        //ContentEntry allEntry = parent.getContentAllEntry();
        String allEntryId = parent.getAllEntryId();
        if (allEntryId == null) {
            Logger.debug(this, "setTotalCount()-allEntry is null");
            String parentId = parent.getId();
            int index = parentId.lastIndexOf("/");
            if (index != -1) { parentId = parentId.substring(0, index); }
            ContentEntry allEntry = requestSearchAllEntry(parentId, NAME_ALL);
            //parent.setContentAllEntry(allEntry);
            if (allEntry == null) {
                parent.setTotalCount(0);
            } else {
                parent.setAllEntryId(allEntry.getID());
                String strChildCount = getMetaData(allEntry, CHILD_COUNT);
                Logger.debug(this, "setTotalCount() - childCount : " + strChildCount);
                if (strChildCount != null) { parent.setTotalCount(Integer.parseInt(strChildCount)); }
            }
            return;
        }
        ContentList cList = requestBrowseEntries(allEntryId, "", false);
        if (!isValid) { return; }
        Logger.debug(this, "setTotalCount()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "setTotalCount()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() == 0) {
            parent.setAllEntryId(null);
            parent.setTotalCount(0);
            return;
        }
        if (cList != null && cList.size() > 0) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            if (!isValid) { return; }
            String strChildCount = getMetaData(entry, CHILD_COUNT);
            Logger.debug(this, "setTotalCount() - childCount : " + strChildCount);
            if (strChildCount != null) { parent.setTotalCount(Integer.parseInt(strChildCount)); }
        }
    }

    /**
     * Request search entries for all container.
     *
     * @param parentId the parent id
     * @param title the title
     * @return the content entry
     */
    private ContentEntry requestSearchAllEntry(String parentId, String title) {
        Logger.info(this, "called requestSearchAllEntry()-title : " + title);
        ContentList cList = requestBrowseEntries(parentId, "", true);
        if (cList != null) { Logger.debug(this, "requestSearchAllEntry()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!(entry instanceof ContentContainer)) { continue; }
                if (!isValid) { return null; }
                String entryName = getMetaData(entry, UPnPConstants.TITLE);
                Logger.debug(this, "requestSearchAllEntry()-entryName : " + entryName);
                if (entryName.equalsIgnoreCase(title)) { return entry; }
                //return entry;
            }
        }
        return null;
    }

    /**
     * Gets the all photo for screensaver.
     *
     * @param content the content
     */
    public void getMusicInfoForAlbum(Content content) {
        ContentList cList = requestBrowseEntries(content.getId(),
                UPnPConstants.ARTIST + "," + UPnPConstants.CREATION_DATE, "", true, 0, 1);
        if (!isValid) { return; }
        Logger.debug(this, "getMusicInfoForAlbum()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "getMusicInfoForAlbum()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            if (!isValid) { return; }
            String artist = getMetaData(entry, UPnPConstants.ARTIST);
            Logger.debug(this, "getMusicInfoForAlbum() - artist : " + artist);
            String date = getMetaData(entry, UPnPConstants.CREATION_DATE);
            Logger.debug(this, "getMusicInfoForAlbum() - date : " + date);
            if (date != null) {
                int index = date.indexOf("T");
                if (index != -1) { date = date.substring(0, index); }
            }
            content.setArtist(artist);
            content.setDate(date);
        }
    }

    /**
     * Sets the total duration.
     *
     * @param content the new total duration
     * @param startIndex the start index
     * @return the sum duration
     */
    public long getSumDuration(Content content, int startIndex) {
        ContentList cList = requestBrowseEntries(content.getId(), RES_DURATION, "", true,
                startIndex, Config.loadDataNumber);
        if (!isValid) { return 0; }
        Logger.debug(this, "getSumDuration()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "setTotalDuration()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            long totalDuration = 0;
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!isValid) { return 0; }
                String durationStr = getMetaData(entry, RES_DURATION);
                Logger.debug(this, "getSumDuration() - durationStr : " + durationStr);
                if (durationStr != null && durationStr.length() > 0) {
                    int sec = 0;
                    String str = durationStr;
                    int pointIdx = str.indexOf(".");
                    if (pointIdx != -1) { str = str.substring(0, pointIdx); }
                    int idx = str.indexOf(":");
                    for (int i = 0; idx != -1; i++) {
                        String temp = str.substring(0, idx);
                        str = str.substring(idx + 1);
                        if (i == 0) {
                            sec += Integer.parseInt(temp) * 60 * 60;
                        } else if (i == 1) {
                            sec += Integer.parseInt(temp) * 60;
                            sec += Integer.parseInt(str);
                            break;
                        }
                        idx = str.indexOf(":");
                    }
                    totalDuration += sec;
                }
            }
            //content.setDuration(totalDuration);
            return totalDuration;
        }
        return 0;
    }

    /**
     * Sets the songs count for atrist.
     *
     * @param content the new songs count for atrist
     * @param sortType the sort type
     * @param startIndex the start index
     * @return the int
     */
    public int setSongsCountForAtrist(Content content, int sortType, int startIndex) {
        ContentList cList = requestBrowseEntries(content.getId(), CHILD_COUNT + "," + UPnPConstants.ALBUM_ART,
                getSortCriteria(sortType), true, startIndex, Config.loadDataNumber);
        if (!isValid) { return 0; }
        Logger.debug(this, "setSongsCountForAtrist()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "setSongsCountForAtrist()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            int totalCount = 0;
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!isValid) { return 0; }
                String strChildCount = getMetaData(entry, CHILD_COUNT);
                String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
                Logger.debug(this, "setSongsCountForAtrist() - strChildCount : " + strChildCount);
                Logger.debug(this, "setSongsCountForAtrist() - albumArt : " + albumArt);
                if (strChildCount != null) { totalCount += Integer.parseInt(strChildCount); }
                if (content.getIconUrl() == null && albumArt != null) { content.setIconUrl(albumArt); }
            }
            return totalCount;
        }
        return 0;
        //content.setSongsCount(totalCount);
        /*if (size < Config.loadDataNumber) { break; }
        startIndex += Config.loadDataNumber;
        content.setSongsCount(totalCount);*/
    }

    /**
     * Requests a browse of this ContentServer which results in the creation of a ContentList.
     *
     * @param entryId the ID of the ContentEntry on the server to start the browse from.
     * A value of "0" SHALL indicate the root container on this server.
     * @param sortCriteria the sort criteria
     * @param isChildBrowse the is child browse
     * @return the ContentList
     */
    private ContentList requestBrowseEntries(String entryId, String sortCriteria, boolean isChildBrowse) {
        return requestBrowseEntries(entryId, "*", sortCriteria, isChildBrowse, 0, 0);
    }

    /**
     * Requests a browse of this ContentServer which results in the creation of a ContentList.
     *
     * @param entryId the ID of the ContentEntry on the server to start the browse from.
     * A value of "0" SHALL indicate the root container on this server.
     * @param propertyFilter the property filter
     * @param sortCriteria the sort criteria
     * @param isChildBrowse the is child browse
     * @param startingIndex the starting index
     * @param requestedCount the requested count
     * @return the ContentList
     */
    private ContentList requestBrowseEntries(String entryId, String propertyFilter, String sortCriteria,
            boolean isChildBrowse, int startingIndex, int requestedCount) {
        Logger.info(this, "called requestBrowseEntries()-entryId : " + entryId);
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        NetActionRequest request =
            csNetModule.requestBrowseEntries(entryId, propertyFilter, isChildBrowse,
                    startingIndex, requestedCount, sortCriteria, handler);
        Logger.info(this, "called requestBrowseEntries()-request : " + request);
        requestBuffer.addElement(request);
        try {
            if (handler.response == null) {
                synchronized (request) {
                    request.wait(timeout);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!isValid) { return null; }
        requestBuffer.removeElement(request);
        ContentList contentList = (ContentList) handler.response;
        if (contentList == null) {
            MainManager.getInstance().printError(MainManager.ERR_NO_PLAY_RETRIEVE, "entryId : " + entryId);
        }
        return contentList;
    }

    /**
     * Requests a browse of this ContentServer which results in the creation of a ContentList.
     *
     * @param entryId the ID of the ContentEntry on the server to start the browse from.
     * A value of "0" SHALL indicate the root container on this server.
     * @param searchCriteria the search criteria
     * @param propertyFilter the property filter
     * @param sortCriteria the sort criteria
     * @param startingIndex the starting index
     * @param requestedCount the requested count
     * @return the ContentList
     */
    private ContentList requestSearchEntries(String entryId, String searchCriteria, String propertyFilter,
            String sortCriteria, int startingIndex, int requestedCount) {
        Logger.info(this, "called requestSearchEntries()-entryId : " + entryId);
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        NetActionRequest request = csNetModule.requestSearchEntries(entryId,
                propertyFilter, startingIndex, requestedCount, searchCriteria, sortCriteria, handler);
        Logger.info(this, "called requestSearchEntries()-request : " + request);
        requestBuffer.addElement(request);
        try {
            if (handler.response == null) {
                synchronized (request) {
                    request.wait(timeout);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!isValid) { return null; }
        requestBuffer.removeElement(request);
        ContentList cList = (ContentList) handler.response;
        Logger.info(this, "requestSearchEntries()-cList : " + cList);
        if (cList == null) {
            MainManager.getInstance().printError(MainManager.ERR_NO_PLAY_RETRIEVE, "entryId : " + entryId);
        } else { Logger.info(this, "requestSearchEntries()-cList.size() : " + cList.size()); }
        return cList;
    }

    /**
     * Request search entries.
     *
     * @param searchContent the search content
     * @param parentId the parent id
     * @param searchCriteria the search format
     * @param menuType the menu type
     * @param musicSeq the music seq
     * @param reqCount the req count
     */
    public void requestSearchEntries(Content searchContent, String parentId, String searchCriteria,
            int menuType, int musicSeq, int reqCount) {
        Logger.info(this, "called requestSearchEntries()-menuType : " + menuType
                + ", musicSeq : " + musicSeq + ", reqCount : " + reqCount);
        String propertyFilter = "@id,@parentID,upnp:class,dc:title," + CHILD_COUNT;
        if (menuType == BasicUI.MENU_VIDEO) {
            propertyFilter += "," + RES_DURATION + "," + RES_RESOLUTION;
        } else if (menuType == BasicUI.MENU_PHOTO) {
            propertyFilter += "," + RES + "," + RES_RESOLUTION;
        } else {
            propertyFilter += "," + RES_DURATION + "," + RES_RESOLUTION
            + "," + UPnPConstants.ARTIST + "," + UPnPConstants.ALBUM + "," + UPnPConstants.ALBUM_ART
            + "," + UPnPConstants.GENRE;
        }

        int loopCount = (reqCount - 1) / Config.loadDataNumber + 1;
        Vector returnValue = new Vector();
        int startIndex = 0;
        int receiveCount = Config.loadDataNumber;
        if (reqCount < receiveCount) { receiveCount = reqCount; }
        for (int i = 0; i < loopCount; i++) {
            ContentList cList =
                requestSearchEntries(parentId, searchCriteria, propertyFilter, "", startIndex, receiveCount);
            if (cList == null || !cList.hasMoreElements()) { return; }
            int size = cList.size();
            Logger.info(this, "requestSearchEntries()-size() : " + size);
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                Content content = new Content();
                Logger.debug(this, "requestSearchEntries() - entry.getID() : " + entry.getID());
                Logger.debug(this, "requestSearchEntries() - @id : " + getMetaData(entry, "@id"));
                Logger.debug(this, "requestSearchEntries() - entry.getParentID() : " + entry.getParentID());
                Logger.debug(this, "requestSearchEntries() - parentID : " + getMetaData(entry, "@parentID"));
                Logger.debug(this, "requestSearchEntries() - upnp:class : " + getMetaData(entry, "upnp:class"));
                int childCount = getChildCount(entry);
                Logger.debug(this, "requestSearchEntries() - childCount : " + childCount);
                String entryName = getMetaData(entry, UPnPConstants.TITLE);
                Logger.debug(this, "requestSearchEntries() - entryName : " + entryName);
                if (childCount > -1) { content.setChildCount(childCount); }
                content.setName(entryName);
                content.setId(entry.getID());
                content.setParentId(entry.getParentID());
                //content.setContentEntry(entry);
                content.setFolder(entry instanceof ContentContainer);
                if (entry instanceof ContentItem) { content.setService(((ContentItem) entry).getItemService()); }
                content.setParent(searchContent);
                content.setMenuType(menuType);
                content.setMusicSeq(musicSeq);
                returnValue.addElement(content);

                setDate(content, entry);
                String iconUrl = getMetaData(entry, UPnPConstants.ICON_REF);
                if (entry instanceof ContentItem) {
                    String resUrl = getMetaData(entry, RES);
                    if (menuType == BasicUI.MENU_PHOTO) {
                        content.setImgUrl(resUrl);
                    } else { content.setResourceUrl(resUrl); }
                    content.setIconUrl(iconUrl);
                    setResolution(content, entry);
                }
                setDuraton(content, entry);
                if (menuType == BasicUI.MENU_MUSIC) {
                    String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
                    content.setIconUrl(albumArt);
                    String album = getMetaData(entry, UPnPConstants.ALBUM);
                    content.setAlbumName(album);
                    String artist = getMetaData(entry, UPnPConstants.ARTIST);
                    content.setArtist(artist);
                    String genre = getMetaData(entry, UPnPConstants.GENRE);
                    content.setGenre(genre);
                }
            }
            searchContent.setChildren(returnValue);
            if (size < Config.loadDataNumber) { break; }
            startIndex += receiveCount;
            if (startIndex + Config.loadDataNumber > reqCount) {
                receiveCount = reqCount - startIndex;
            } else { receiveCount = Config.loadDataNumber; }
        }
        //return returnValue;
    }

    /**
     * Request search entries.
     *
     * @param parentId the parent id
     * @param sortType the sort type
     * @param menuType the menu type
     * @param isOnlyItems the is only items
     * @return the photo url
     */
    public String requestSearchPhotoUrl(String parentId, int sortType, int menuType, boolean isOnlyItems) {
        Logger.info(this, "called requestSearchPhotoUrl()");
        String searchCriteria = "(@parentID = \"" + parentId + "\")";
        if (isOnlyItems) { searchCriteria = "(upnp:class derivedfrom \"object.item\") and " + searchCriteria; }
        /*if (menuType == BasicUI.MENU_MUSIC) {
            searchCriteria = "(" +  UPnPConstants.ALBUM_ART + " != \"null\") and " + searchCriteria;
        } else {
            //searchCriteria = "(" +  UPnPConstants.ICON_REF + " exists true) and " + searchCriteria;
            searchCriteria = "(" + UPnPConstants.ICON_REF + " exists true and " +  UPnPConstants.ICON_REF
            + " contains \"http\") and " + searchCriteria;
        }*/
        Logger.debug(this, "requestSearchPhotoUrl() - searchCriteria : " + searchCriteria);
        ContentList cList = requestSearchEntries(parentId, searchCriteria,
                RES + ",upnp:class,@parentID," + UPnPConstants.ICON_REF + "," + UPnPConstants.ALBUM_ART,
                getSortCriteria(sortType), 0, 1);
        if (cList != null && cList.size() > 0) {
            ContentEntry entry = (ContentEntry) cList.nextElement();
            if (!isValid) { return null; }
            String iconUrl = getMetaData(entry, UPnPConstants.ICON_REF);
            String resUrl = getMetaData(entry, RES);
            String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
            Logger.debug(this, "requestSearchPhotoUrl() - iconUrl : " + iconUrl);
            Logger.debug(this, "requestSearchPhotoUrl() - albumArt : " + albumArt);
            if (menuType == BasicUI.MENU_PHOTO) {
                if (iconUrl == null) { iconUrl = resUrl; }
            } else if (menuType == BasicUI.MENU_MUSIC) {
                iconUrl = albumArt;
            }
            return iconUrl;
        }
        return null;
    }

    /**
     * Request search entries.
     *
     * @param parentId the parent id
     * @param sortType the sort type
     * @param menuType the menu type
     * @param startIndex the start index
     * @return the photo url
     */
    public Vector requestSearchPhotoUrlList(String parentId, int sortType, int menuType, int startIndex) {
        Logger.info(this, "called requestSearchPhotoUrlList()");
        String searchCriteria = "@parentID = \"" + parentId + "\"";
        if (menuType == BasicUI.MENU_PHOTO) {
            searchCriteria = "upnp:class derivedfrom \"object.item\" and " + searchCriteria;
        }
        Vector returnValue = new Vector();
        ContentList cList = requestSearchEntries(parentId, searchCriteria,
                "upnp:class,@parentID," + UPnPConstants.ICON_REF + "," + UPnPConstants.ALBUM_ART,
                getSortCriteria(sortType), startIndex, Config.loadDataNumber);
        if (!isValid) { return null; }
        Logger.debug(this, "requestSearchPhotoUrlList()-cList : " + cList);
        if (cList != null) { Logger.debug(this, "requestSearchPhotoUrlList()-cList.size() : " + cList.size()); }
        if (cList != null && cList.size() > 0) {
            while (cList.hasMoreElements()) {
                ContentEntry entry = (ContentEntry) cList.nextElement();
                if (!isValid) { return null; }
                String iconUrl = getMetaData(entry, UPnPConstants.ICON_REF);
                String albumArt = getMetaData(entry, UPnPConstants.ALBUM_ART);
                //Logger.debug(this, "requestSearchPhotoUrlList() - albumArt : " + albumArt);
                if (menuType == BasicUI.MENU_MUSIC) { iconUrl = albumArt; }
                if (iconUrl != null) { returnValue.addElement(iconUrl); }
            }
        }
        return returnValue;
    }

    /**
     * Request search folder count.
     *
     * @param parentId the parent id
     * @param sortType the sort type
     * @return the vector
     */
    public int requestSearchFolderCount(String parentId, int sortType) {
        Logger.info(this, "called requestSearchFolderCount()");
        String searchCriteria = "upnp:class derivedfrom \"object.container\" and @parentID = \"" + parentId + "\"";
        int totalCount = 0;
        int startIndex = 0;
        while (true) {
            ContentList cList = requestSearchEntries(parentId, searchCriteria,
                    "upnp:class,@parentID", getSortCriteria(sortType), startIndex, Config.loadDataNumber);
            if (cList == null) { return -1; }
            int size = cList.size();
            totalCount += size;
            if (size < Config.loadDataNumber) { return totalCount; }
            startIndex += Config.loadDataNumber;
        }
    }

    /**
     * Gets the meta data.
     *
     * @param entry the entry
     * @param name the name
     * @return the meta data for name
     */
    private String getMetaData(ContentEntry entry, String name) {
        //Logger.debug(this, "called getMetaData()-name : " + name);
        Object metaData = entry.getRootMetadataNode().getMetadata(name);
        //Logger.debug(this, "called getMetaData()-metaData : " + metaData);
        if (metaData == null) { return null; }
        if (metaData instanceof String) {
            //Logger.debug(this, "called getMetaData()-metaData : " + metaData);
            return (String) metaData;
        } else if (metaData instanceof String[]) {
            //Logger.debug(this, "called getMetaData()-metaData : String[]");
            String[] arrMetaData = (String[]) metaData;
            if (arrMetaData != null && arrMetaData.length > 0) { return arrMetaData[0]; }
        } else {
            Logger.debug(this, "called getMetaData()-metaData : other type!!!!");
        }
        return null;
    }

    /**
     * Gets the meta data for resource.
     *
     * @param entry the entry
     * @param name the name
     * @return the meta data for name
     */
    /*private String getResourceData(ContentEntry entry, String name) {
        String[] resData = (String[]) entry.getRootMetadataNode().getMetadata(name);
        if (resData != null && resData.length > 0) { return resData[0]; }
        return null;
    }*/
}

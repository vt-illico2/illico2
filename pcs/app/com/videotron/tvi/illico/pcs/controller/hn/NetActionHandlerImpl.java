/*
 *  @(#)NetActionHandlerImpl.java 1.0 2011.07.11
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.hn;

import org.ocap.hn.NetActionEvent;
import org.ocap.hn.NetActionHandler;
import org.ocap.hn.NetActionRequest;

import com.videotron.tvi.illico.pcs.common.Logger;

/**
 * implements NetActionHandlerImpl.
 * @since 2011.07.11
 * @author tklee
 * @version $Revision: 1.1 $ $Date: 2011/07/19 17:50:50 $
 */
public class NetActionHandlerImpl implements NetActionHandler {
    /** The request. */
    public NetActionRequest request = null;
    /** The response. */
    public Object response = null;
    /** The status. */
    public int status = 0;
    /** The error. */
    public int error = 0;

    /**
     * Notifies the application of an action event.
     *
     * @param event the NetActionEvent
     * @see org.ocap.hn.NetActionHandler#notify(org.ocap.hn.NetActionEvent)
     */
    public void notify(NetActionEvent event) {
        request = event.getActionRequest();
        response = event.getResponse();
        status = event.getActionStatus();
        error = event.getError();
        Logger.debug(this, "notify()-status : " + status + ", error : " + error);
        Logger.debug(this, "notify()-request : " + request);
        if (request != null) {
            synchronized (request) {
                request.notify();
            }
        }
    }
}

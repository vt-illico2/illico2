/*
 *  @(#)MainManager.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.pcs.controller.hn.HNHandler;
import com.videotron.tvi.illico.pcs.controller.ui.SceneManager;
import com.videotron.tvi.illico.util.Environment;

/**
 * This class is the main class.
 *
 * @since 2011.05.16
 * @version $Revision: 1.6 $ $Date: 2012/09/26 07:52:22 $
 * @author tklee
 */
public final class MainManager {
    /** Instance of MainManager. */
    private static MainManager instance = new MainManager();

    /** The Constant NO_ERR. */
    public static final int NO_ERR = -1;
    /** The Constant ERR_NO_DISCOVER. */
    public static final int ERR_NO_DISCOVERY = 0;
    /** The Constant ERR_NO_PLAY_RETRIEVE. */
    public static final int ERR_NO_PLAY_RETRIEVE = 1;
    /** The Constant ERR_LOST_CONNECTION. */
    public static final int ERR_LOST_CONNECTION = 2;
    /** The Constant ERR_NO_SUPPORTED_TYPE. */
    public static final int ERR_NO_SUPPORTED_TYPE = 3;
    /** The Constant ERR_SIZE_EXCEED. */
    public static final int ERR_SIZE_EXCEED = 4;
    /** The error code. */
    private final String[] errCode = {"PCS500", "PCS501", "PCS502", "PCS503", "PCS504"};
    /** The errpor message. */
    private final String[] errMsg = {"Unable to discover content stored on DLNA servers.",
            "Unable to play / retrieve a specific media element stored on DLNA server.",
            "Lost connection to a DLNA server.",
            "Unable to play / display a file of a given media type.",
            "Unable to play /display a file because its size exceeds the maximum capacity for the STB."};

    /**
     * Gets the singleton instance of MainManager.
     * @return MainManager
     */
    public static synchronized MainManager getInstance() {
        return instance;
    }

    /**
     * initial method after called Constructor.
     */
    public void init() {
        Logger.info(this, "called init()");
        Config.setConfigValues();
        SceneManager.getInstance().init();
        //OobManager.getInstance().start();
        CommunicationManager.getInstance().start();
        //HNHandler.getInstance().init();
    }

    /**
     * This method starts a SceneManager.
     */
    public void start() {
        Logger.info(this, "called start()-vendor name : " + Environment.VENDOR_NAME);
        HNHandler.getInstance().init(false);
        CommunicationManager.getInstance().setCurrentChannel();
        SceneManager.getInstance().start();
    }

    /**
     * This method pause processes.
     */
    public void pause() {
        SceneManager.getInstance().pause();
        HNHandler.getInstance().dispose();
        CommunicationManager.getInstance().hideLoadingAnimation();
    }

    /**
     * This method dispose a SceneManager.
     */
    public void destroy() {
        SceneManager.getInstance().dispose();
    }

    /**
     * Prints the error.
     *
     * @param errType the err type
     */
    public void printError(int errType) {
        printError(errType, null);
    }

    /**
     * Prints the error.
     *
     * @param errType the error type. refer to upper constant.
     * @param addTxt the add text
     */
    public void printError(int errType, String addTxt) {
        if (errType == NO_ERR) { return; }
        String errTxt = "[" + errCode[errType] + "]" + errMsg[errType];
        if (addTxt != null) { errTxt += "-" + addTxt; }
        if (errType == ERR_SIZE_EXCEED) {
            Log.printWarning(errTxt);
        } else { Log.printError(errTxt); }
    }

    /**
     * Gets the error code.
     *
     * @param errorType the error type
     * @return the error code
     */
    public String getErrorCode(int errorType) {
        return errCode[errorType];
    }
}
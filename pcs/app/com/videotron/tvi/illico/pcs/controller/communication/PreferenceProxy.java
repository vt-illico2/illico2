/*
 *  @(#)PreferenceProxy.java 1.0 2011.05.16
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.communication;

import java.rmi.RemoteException;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @since 2011.03.19
 * @version $Revision: 1.5 $ $Date: 2012/04/29 01:53:04 $
 * @author tklee
 */
public final class PreferenceProxy implements PreferenceListener {
    /** Is a instance of PreferenceProxy. */
    private static PreferenceProxy instance  = null;

    /** The Constant NAME_FAVOR_DEVICE. */
    public static final String NAME_FAVOR_DEVICE = "PCS_FAVOR_DEVICE";
    /** The Constant NAME_FAVOR_UDN. */
    public static final String NAME_FAVOR_UDN = "PCS_FAVOR_UDN";
    /** The Constant NAME_SLIDESHOW_EFFECT. */
    public static final String NAME_SLIDESHOW_EFFECT = "PCS_SLIDESHOW_EFFECT";
    /** The Constant NAME_SLIDESHOW_INTERVAL. */
    public static final String NAME_SLIDESHOW_INTERVAL = "PCS_SLIDESHOW_INTERVAL";
    /** The Constant NAME_MUSIC_SCREENSAVER. */
    public static final String NAME_MUSIC_SCREENSAVER = "PCS_MUSIC_SCREENSAVER";
    /** The Constant NAME_MUSIC_CROSS_FADE. */
    public static final String NAME_MUSIC_CROSS_FADE = "PCS_MUSIC_CROSS_FADE";
    /** The Constant NAME_VIDEO_SCALING. */
    public static final String NAME_VIDEO_SCALING = "PCS_VIDEO_SCALING";
    /** The Constant NAME_SCREENSAVER_WAIT. */
    public static final String NAME_SCREENSAVER_WAIT = "PCS_SCREENSAVER_WAIT";
    /** The Constant NAME_PHOTO_UI. */
    public static final String NAME_PHOTO_UI = "PCS_PHOTO_UI";
    /** The Constant NAME_VIDEO_UI. */
    public static final String NAME_VIDEO_UI = "PCS_VIDEO_UI";

    /** The Constant EFFECT_NO. */
    public static final String EFFECT_NO = "No transition";
    /** The Constant EFFECT_RANDOM. */
    public static final String EFFECT_RANDOM = "Random";
    /** The Constant EFFECT_FADE. */
    public static final String EFFECT_FADE = "Fade";
    /** The Constant EFFECT_DISSOLVE. */
    public static final String EFFECT_DISSOLVE = "Dissolve";
    /** The Constant EFFECT_PAGEFLIP. */
    public static final String EFFECT_PAGEFLIP = "Page Flip";
    /** The Constant EFFECTS. */
    public static final String[] EFFECTS = {EFFECT_NO, EFFECT_RANDOM, EFFECT_FADE, EFFECT_DISSOLVE, EFFECT_PAGEFLIP};

    /** The Constant INTERVAL_3. */
    public static final String INTERVAL_3 = "3 seconds";
    /** The Constant INTERVAL_5. */
    public static final String INTERVAL_5 = "5 seconds";
    /** The Constant INTERVAL_10. */
    public static final String INTERVAL_10 = "10 seconds";
    /** The Constant INTERVALS. */
    public static final String[] INTERVALS = {INTERVAL_3, INTERVAL_5, INTERVAL_10};

    /** The Constant SCEENSAVER_ALBUM. */
    public static final String SCREENSAVER_ALBUM = "Album Covers";
    /** The Constant SCEENSAVER_PHOTO. */
    public static final String SCREENSAVER_PHOTO = "All My Photos";
    /** The Constant SCEENSAVER_NO. */
    public static final String SCREENSAVER_NO = "No screensaver";
    /** The Constant SCREENSAVIERS. */
    public static final String[] SCREENSAVERS = {SCREENSAVER_ALBUM, SCREENSAVER_PHOTO, SCREENSAVER_NO};

    /** The Constant WAIT_1. */
    public static final String WAIT_1 = "1 minute";
    /** The Constant WAIT_5. */
    public static final String WAIT_5 = "5 minutes";
    /** The Constant WAIT_10. */
    public static final String WAIT_10 = "10 minutes";
    /** The Constant WAITS. */
    public static final String[] WAITS = {WAIT_1, WAIT_5, WAIT_10};

    /** The Constant VIDEO_SIZE_ACTUAL. */
    public static final String VIDEO_SIZE_ACTUAL = "Actual Size";
    /** The Constant VIDEO_SIZE_FULL. */
    public static final String VIDEO_SIZE_FULL = "Full Size";
    /** The Constant VIDEO_SIZES. */
    public static final String[] VIDEO_SIZES = {VIDEO_SIZE_ACTUAL, VIDEO_SIZE_FULL};
    /** The Constant MUSIC_CROSS_FADES. */
    public static final String[] MUSIC_CROSS_FADES = {Definitions.OPTION_VALUE_NO, Definitions.OPTION_VALUE_YES};

    /** The favor device name. */
    public String favorDeviceName = null;
    /** The favor udn. */
    public String favorUDN = null;
    /** The slideshow effect. */
    public String slideshowEffect = null;
    /** The slideshow interval. */
    public String slideshowInterval = null;
    /** The music screensaver. */
    public String musicScreensaver = null;
    /** The music cross fade. */
    public String musicCrossFade = null;
    /** The video scaling. */
    public String videoScaling = null;
    /** The screensaver wait. */
    public String screensaverWait = null;
    /** The photo ui type. */
    public int photoUiType = 0;
    /** The video ui type. */
    public int videoUiType = 1;

    /** a instance of PreferenceService. */
    private PreferenceService prefSvc = null;

    /** The favorites devices. */
    //public static Hashtable favorDevs = null;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) { instance = new PreferenceProxy(); }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() { }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (prefSvc != null) {
            try {
                prefSvc.removePreferenceListener(Config.APP_NAME, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            prefSvc = null;
        }
        instance = null;
    }

    /**
     * Look up User Profile and Preference.
     *
     * @param prefService PreferenceService gained from IXC
     */
    public void addPreferenceListener(PreferenceService prefService) {
        Logger.info(this, "called addPreferenceListener()");
        try {
            prefSvc = prefService;
            Logger.debug(this, "addPreferenceListener()-prefSvc : " + prefSvc);
            if (prefSvc != null) {
                String[] prefNames = {PreferenceNames.LANGUAGE, NAME_FAVOR_DEVICE, NAME_SLIDESHOW_EFFECT,
                        NAME_SLIDESHOW_INTERVAL, NAME_MUSIC_SCREENSAVER, NAME_MUSIC_CROSS_FADE, NAME_VIDEO_SCALING,
                        NAME_SCREENSAVER_WAIT, NAME_FAVOR_UDN, NAME_PHOTO_UI, NAME_VIDEO_UI};
                String[] prefValues = {Definitions.LANGUAGE_FRENCH, "", EFFECT_DISSOLVE,
                        INTERVAL_5, SCREENSAVER_ALBUM, Definitions.OPTION_VALUE_NO, VIDEO_SIZE_FULL, WAIT_5, "",
                        String.valueOf(0), String.valueOf(1)};

                String[] uppData = prefSvc.addPreferenceListener(this, Config.APP_NAME, prefNames, prefValues);
                Logger.debug(this, "uppData.length : " + uppData.length);
                if (uppData != null && uppData.length == prefNames.length) {
                    DataCenter.getInstance().put(PreferenceNames.LANGUAGE, uppData[0]);
                    Config.language = uppData[0];
                    favorDeviceName = uppData[1];
                    slideshowEffect = uppData[2];
                    slideshowInterval = uppData[3];
                    musicScreensaver = uppData[4];
                    musicCrossFade = uppData[5];
                    videoScaling = uppData[6];
                    screensaverWait = uppData[7];
                    favorUDN = uppData[8];
                    try {
                        photoUiType = Integer.parseInt(uppData[9]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        videoUiType = Integer.parseInt(uppData[10]);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Save a preference value into UP.
     *
     * @param name the name
     * @param value the value
     */
    public void setPreferenceValue(String name, String value) {
        Logger.info(this, "called setPreferenceValue()-prefSvc : " + prefSvc);
        if (prefSvc != null) {
            try {
                Log.printInfo("name = " + name + " value = " + value);
                prefSvc.setPreferenceValue(name, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        Log.printDebug("called receiveUpdatedPreference()");
        if (name == null || value == null) { return; }
        Log.printInfo("name = " + name + ", value =" + value);
        if (PreferenceNames.LANGUAGE.equals(name)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
            Config.language = value;
        }
    }
}

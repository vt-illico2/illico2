/*
 *  @(#)InbandManager.java 1.0 2011.10.15
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.pcs.controller.communication;

import java.io.File;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.pcs.common.Config;
import com.videotron.tvi.illico.pcs.common.Logger;
import com.videotron.tvi.illico.pcs.util.DataConverter;
import com.videotron.tvi.illico.util.Environment;

/**
 * This class process inband data.
 * @author tklee
 * @version $Revision: 1.4 $ $Date: 2011/10/14 20:11:38 $
 */
public class InbandManager implements DataUpdateListener, InbandDataListener {
    /** The Constant IB_CONFIG. */
    private static final String IB_CONFIG = "teaserConfig";
    /** The Constant IB_FILE_NAME. */
    private static final String IB_FILE_NAME = "pcs_teaser";

    /** The Constant LANG_FR. */
    private static final String LANG_FR = "fr";
    /** The Constant LANG_EN. */
    private static final String LANG_EN = "en";

    /** Class instance. */
    private static InbandManager instance = null;
    /** The monitor service. */
    private MonitorService monitorService = null;

    /** The teaser url fr. */
    public static String teaserUrlFr = null;
    /** The teaser url en. */
    public static String teaserUrlEn = null;

    /** The inband version. */
    private int inbandVersion = -1;

    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static synchronized InbandManager getInstance() {
        if (instance == null) { instance = new InbandManager(); }
        return instance;
    }

    /**
     * Adds the inband data listener.
     *
     * @param service the service
     */
    public void addInbandDataListener(MonitorService service) {
        monitorService = service;
        DataCenter.getInstance().addDataUpdateListener(IB_CONFIG, this);
        if (Environment.EMULATOR) {
            DataCenter.getInstance().put(IB_CONFIG, new File("data/pcs_teaser.txt"));
        }
        try {
            monitorService.addInbandDataListener(this, Config.APP_NAME);
        } catch (Exception e) {
            Logger.error(this, "dataUpdated()-" + e.getMessage());
        }
    }

    /**
     * Update a data via in-band when Monitor tuned a In-band channel for Data.
     *
     * @param locator The locator for in-band data channel.
     * @throws RemoteException the remote exception
     */
    public void receiveInbandData(String locator) throws RemoteException {
        Logger.info(this, "receiveInbandData()-locator : " + locator);
        if (monitorService == null) { return; }
        int version = monitorService.getInbandDataVersion(IB_FILE_NAME);
        Logger.debug(this, "receiveInbandData()-version : " + version);
        if (inbandVersion != version || version == MonitorService.VERSION_NOT_FOUND) {
            inbandVersion = version;
            DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        }
        monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        Logger.info(this, " called dataUpdated()-key : " + key);
        if (key.equals(IB_CONFIG) && value != null) {
            File file = (File) value;
            if (file == null) { return; }
            byte[] data = BinaryReader.read(file);
            if (data != null) {
                try {
                    int index = 0;
                    int version = DataConverter.convertOneByteToInt(data, index);
                    Logger.debug(this, "dataUpdated()-version : " + version);
                    int urlLength = DataConverter.convertOneByteToInt(data, ++index);
                    Logger.debug(this, "dataUpdated()-urlLength : " + urlLength);
                    String urlName = DataConverter.convertToString(data, ++index, urlLength);
                    Logger.debug(this, "dataUpdated()-urlName : " + urlName);
                    index += urlLength - 1;
                    int langCount = DataConverter.convertOneByteToInt(data, ++index);
                    Logger.debug(this, "dataUpdated()-langCount : " + langCount);
                    for (int i = 0; i < langCount; i++) {
                        String langType = DataConverter.convertToString(data, ++index, 2);
                        Logger.debug(this, "dataUpdated()-langType : " + langType);
                        index += 2 - 1;
                        int imgNameLength = DataConverter.convertOneByteToInt(data, ++index);
                        Logger.debug(this, "dataUpdated()-imgNameLength : " + imgNameLength);
                        String imgName = DataConverter.convertToString(data, ++index, imgNameLength);
                        Logger.debug(this, "dataUpdated()-imgName : " + imgName);
                        if (langType.equals(LANG_FR)) {
                            teaserUrlFr = urlName + "/" + imgName;
                            Logger.debug(this, "dataUpdated()-teaserUrlFr : " + teaserUrlFr);
                        } else {
                            teaserUrlEn = urlName + "/" + imgName;
                            Logger.debug(this, "dataUpdated()-teaserUrlEn : " + teaserUrlEn);
                        }
                        index += imgNameLength - 1;
                    }
                } catch (Exception e) {
                    Logger.error(this, "Inband data error during parse-" + e.getMessage());
                }
            }
        }
    }

    /**
     * Called when data has been removed.
     *
     * @param key the key
     */
    public void dataRemoved(String key) { }
}

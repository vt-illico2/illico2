﻿This is Weather App Release Notes.
GENERAL RELEASE INFORMATION
 - Release Details
    - Release version
      : version R7.1.1
    - Release file name
      : Weather.zip
    - Release type (official, debug, test,...)
      : official       
    - Release date
      : 2016.xx.xx

***
version R7.1.X - 2016.xx.xx
    - New Feature   
        N/A
    - Resolved Issues
       + VDTRMASTER-5846 [prod][R7.1] Weather service sends multiple request simultaneously
***
version 4K.4.0 - 2015.07.31
    - New Feature   
        + Applying Flat design modification.
    - Resolved Issues
        N/A
***
version 4K.3.0 - 2015.06.12
    - New Feature   
        + update a 02_d_shadow.png
    - Resolved Issues
        N/A
***
version 4K.2.0 - 2015.5.13
    - New Feature   
        + update a 00_mini_line.png
        + update a 07_bg_main.jpg
        + update a bg.jpg
    - Resolved Issues
        N/A
***
version 4K.1.0 - 2015.4.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
    - Resolved Issues
        N/A
    
*** 
version 1.0.28.0
    - New Feature 
        + modified log about server module. (Add more logs to analyze easily in the Splunk system)
    - Resolved Issues 
        N/A
*** 
version 1.0.27.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-4585 [R3][SEARCH][REGR-R3]:"D Options" menu stays on top of Search screen instead of closing before displaying the Search screen
        + VDTRMASTER-4586 R3][UI/GUI] [Weather] ]:Changing Unit of measure Preference C or F : sentence does not fit in the frame     
*** 
version 1.0.26.0
    - New Feature 
        + Applied changed ApplicationConfig module.
    - Resolved Issues 
        N/A          
*** 
version 1.0.25.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-4357 [R3] [Weather] [INTGR] - Tune in to Weather Network button is missing          
*** 
version 1.0.24.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRSUPPORT-155 The weather widget does not follow the user unit of measure preference. Ex: Preference is Farenheit and the widget continue to display Celcius          
*** 
version 1.0.23.0
    - New Feature
        + changed VBM parameter. ("Unknown" to "") 
    - Resolved Issues 
        N/A     
*** 
version 1.0.22.0
    - New Feature
        + added VBM-group id module.
        + wrote version info to application.prop        
    - Resolved Issues 
        N/A
*** 
version 1.0.21.0
    - New Feature
        + modified VBM module.(modified MID-1011000003)
    - Resolved Issues 
        N/A
*** 
version 1.0.20.0
    - New Feature 
        + fixed the issue that does not dispose of LayeredUI. 
    - Resolved Issues 
        N/A        
*** 
version 1.0.19.0
    - New Feature 
        + MET501 is added newly 
        + MET502 is added newly
        + MET503 is added newly
    - Resolved Issues 
        N/A  
*** 
version 1.0.18.0
    - New Feature
        + Merged from R2 source.
        + integrated with VBM log.
    - Resolved Issues
        N/A   
*** 
version 1.0.17.0
    - New Feature 
        + Modified to have delay time when loading animation is called.
    - Resolved Issues 
        N/A    
*** 
version 1.0.16.0
    - New Feature 
        + Applied changed KeyboardOption interface.
    - Resolved Issues 
        N/A
*** 
version 1.0.15.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.
*** 
version 1.0.14.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.          
*** 
version 1.0.13.0
    - New Feature 
        + Changed the logo image. (French and English)
    - Resolved Issues 
        N/A          
*** 
version 1.0.12.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2660 Weather App : change channel - modify behaviour          
*** 
version 1.0.11.0
    - New Feature 
        + Modified the channel event logic of weather application.
    - Resolved Issues 
        N/A          
*** 
version 1.0.10.0
    - New Feature 
        + Fixed the search popup module.
    - Resolved Issues 
        N/A      
*** 
version 1.0.9.0
    - New Feature 
        + Fixed the search popup module of weather main.
        + Modified the search popup logic when input text cleared. 
    - Resolved Issues 
        N/A     
*** 
version 1.0.8.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 1.0.7.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2503 [UI/GUI] [weather] Adjust TOC for consistency with other ITV apps.
*** 
version 1.0.6.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2503 [UI/GUI] [weather] Adjust TOC for consistency with other ITV apps.
*** 
version 1.0.5.3
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2379 [UI/GUI] [Weather] - Wrong error message is displayed.
        + VDTRMASTER-2466 DDC 6451 - Weather Short term page displayed weather for previous hometown.
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.
*** 
version 1.0.4.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2459 [UI/GUI] [Weather] - Long and short term: TOC correction
        + VDTRMASTER-2455 WIDGET Last arrow to the right doesn't disapear
*** 
version 1.0.3.1
    - New Feature 
        + Added sort module of search popup
    - Resolved Issues 
        + VDTRMASTER-2407 DDC6451 - WEATHER Country name is not displayed
*** 
version 1.0.2.1
    - New Feature 
        + Fixed a issue that did not adapt home town info when user replace home town info at the weather preference.
    - Resolved Issues 
        + VDTRMASTER-2376 DDC 6451 - WEATHER Humidy percentage is not displayed
*** 
version 1.0.1.0
    - New Feature 
        + added background image of D-Option
    - Resolved Issues 
        N/A
*** 
version 0.1.71.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2462 DDC 6451 - Weather Feels likes temp not correctly displayed.       
*** 
version 0.1.70.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2462 DDC 6451 - Weather Feels likes temp not correctly displayed.       
*** 
version 0.1.69.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3548 [WEATHER/iTV] [REGRESSION] Feels like Temp is missing from Bottom section (This afternoon, this evening, tonight and XX morning)       
*** 
version 0.1.68.0
    - New Feature 
        + Modified to have delay time when loading animation is called. (Related to VDTRMASTER-3313)
    - Resolved Issues 
        N/A        
*** 
version 0.1.67.0
    - New Feature 
        + Applied changed KeyboardOption interface.
    - Resolved Issues 
        N/A          
*** 
version 0.1.66.0
    - New Feature 
        + Modified request parameter to move help app. 
    - Resolved Issues 
        N/A        
*** 
version 0.1.65.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.
*** 
version 0.1.64.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.          
*** 
version 0.1.63.0
    - New Feature 
        + Changed the logo image. (French and English)
    - Resolved Issues 
        N/A          
*** 
version 0.1.62.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2660 Weather App : change channel - modify behaviour          
*** 
version 0.1.61.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2660 Weather App : change channel - modify behaviour          
*** 
version 0.1.60.0
    - New Feature 
        + Modified the channel event logic of weather application.
    - Resolved Issues 
        N/A          
*** 
version 0.1.59.0
    - New Feature 
        + Fixed the search popup module.
    - Resolved Issues 
        N/A      
*** 
version 0.1.58.0
    - New Feature 
        + Fixed the search popup module of weather main.
        + Modified the search popup logic when input text cleared. 
    - Resolved Issues 
        N/A      
*** 
version 0.1.57.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 0.1.56.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2503 [UI/GUI] [weather] Adjust TOC for consistency with other ITV apps.
*** 
version 0.1.55.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2503 [UI/GUI] [weather] Adjust TOC for consistency with other ITV apps.
*** 
version 0.1.54.3
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2379 [UI/GUI] [Weather] - Wrong error message is displayed.
        + VDTRMASTER-2466 DDC 6451 - Weather Short term page displayed weather for previous hometown.
        + VDTRMASTER-2467 DDC 6451 - Weather Search function not efficient.
*** 
version 0.1.53.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2459 [UI/GUI] [Weather] - Long and short term: TOC correction
        + VDTRMASTER-2455 WIDGET Last arrow to the right doesn't disapear
*** 
version 0.1.52.1
    - New Feature 
        + Added sort module of search popup
    - Resolved Issues 
        + VDTRMASTER-2407 DDC6451 - WEATHER Country name is not displayed
*** 
version 0.1.51.1
    - New Feature 
        + Fixed a issue that did not adapt home town info when user replace home town info at the weather preference.
    - Resolved Issues 
        + VDTRMASTER-2376 DDC 6451 - WEATHER Humidy percentage is not displayed
*** 
version 0.1.50.0
    - New Feature 
        + added background image of D-Option
    - Resolved Issues 
        N/A  
*** 
version 0.1.49.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2168  [UI/GUI] [ITV] - Missing a "exit" button to get out of the application  
*** 
version 0.1.48.0
    - New Feature 
        + Fixed animation module of the search popup. (Did not animate to Color-C key)
    - Resolved Issues 
        N/A
*** 
version 0.1.47.0
    - New Feature 
        + Fixed animation module of the search popup .
        + Changed weather logo image.
    - Resolved Issues 
        N/A
*** 
version 0.1.46.0
    - New Feature 
        + Fixed keyboard module.
    - Resolved Issues 
        N/A
*** 
version 0.1.45.0
    - New Feature 
        + Added Color-C button action at Search popup.
        + Fixed D-Option (Go to help)
    - Resolved Issues 
        N/A
*** 
version 0.1.44.1
    - New Feature 
        + Clicking animation
    - Resolved Issues 
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
*** 
version 0.1.43.0
    - New Feature 
        + Applied modified Framework(OC unload)
    - Resolved Issues 
        N/A
*** 
version 0.1.42.0
    - New Feature 
        + Fixed the animation module of weather main scene.
    - Resolved Issues 
        N/A
*** 
version 0.1.41.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1990  [UI/GUI] Missing TOC for Wizard and Weather
        + VDTRMASTER-1992  [UI/GUI] [Weather] - Not enough content to test footer
*** 
version 0.1.40.1
    - New Feature 
        + Removed DCA keys at scene without scaled video.
    - Resolved Issues 
        + VDTRMASTER-1992  [UI/GUI] [Meteo] - Not enough content to test footer
*** 
version 0.1.39.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1955  [UI/GUI] [Weather] Meteo homepage should be updated
*** 
version 0.1.38.2
    - New Feature 
        + Added page up/down at search popup.
    - Resolved Issues 
        + VDTRMASTER-1948 [UI/GUI] [Weather] Truncated weather description in french
        + VDTRMASTER-1950 [UI/GUI] [Weather] Do not allow Hometown removal
*** 
version 0.1.37.0
    - New Feature 
        + fixed build issue of weather application.
    - Resolved Issues 
        N/A   
*** 
version 0.1.36.0
    - New Feature 
        + fixed build issue of weather application.
    - Resolved Issues 
        N/A       
*** 
version 0.1.35.0
    - New Feature 
        + fixed build issue of weather application.
    - Resolved Issues 
        N/A
*** 
version 0.1.34.0
    - New Feature 
        + fixed build issue of weather application.
    - Resolved Issues 
        N/A
*** 
version 0.1.33.0
    - New Feature 
        + compile with latest keyboard lib.
    - Resolved Issues 
        N/A
*** 
version 0.1.32.0
    - New Feature 
        + Added weather application information to the "ocap.host.application" file .
    - Resolved Issues 
        N/A
*** 
version 0.1.31.0
    - New Feature 
        + Added the footer animation.
    - Resolved Issues 
        N/A
*** 
version 0.1.30.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1725  [UI/GUI][TOC] Weather & Lottery
*** 
version 0.1.29.0
    - New Feature 
        + Fixed the animation of main scene (scaled video - related).
        + Blocked key event of PVR-related  at the scene without scaled video.
    - Resolved Issues 
        N/A
*** 
version 0.1.28
    - New Feature 
        + applied the latest TOC data.
    - Resolved Issues 
        N/A
*** 
version 0.1.27
    - New Feature 
        + chaged IB module. (synchronousLoad -> asynchronousLoad)
    - Resolved Issues 
        N/A
*** 
version 0.1.26
    - New Feature 
        + Fixed animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.25
    - New Feature 
        + Fixed animation module of the main screen.
        + Fixed renderer module for preventing memory leak.
    - Resolved Issues 
        N/A
*** 
version 0.1.24
    - New Feature 
        + Fixed animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.23
    - New Feature 
        + Applied animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.22
    - New Feature 
        + Fixed popup module. (No action for key code "Back")
        + Changed call letter about french weather channel and english weather channel. (En : TWN, Fr : METEO)
    - Resolved Issues 
        N/A      
*** 
version 0.1.21
    - New Feature 
        + Changed some GUI issues.
    - Resolved Issues 
        N/A
*** 
version 0.1.20
    - New Feature 
        + Fixed proxy server module.
    - Resolved Issues 
        N/A
*** 
version 0.1.19
    - New Feature 
        + Fixed server module.
    - Resolved Issues 
        N/A
*** 
version 0.1.18
    - New Feature 
        + Fixed SAX parser handler module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.17
    - New Feature 
        + Fixed server module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.16
    - New Feature 
        + Changed some GUI issues.
        + Fixed Stc service module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.15
    - New Feature 
        + Added weather widget background image.
        + Fixed scaled video module (show info bar and do not hide).
    - Resolved Issues 
        N/A 
*** 
version 0.1.14
    - New Feature 
        + Added error code. (MET502)
    - Resolved Issues 
        N/A 
*** 
version 0.1.13
    - New Feature 
        + Fixed IB data parsing error.
        + Added click effect.
    - Resolved Issues 
        N/A 
*** 
version 0.1.12
    - New Feature 
        + Fixed preference module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.11
    - New Feature 
        + Fixed preference module. (If the searched city duplicates registered favorite city or hometown, then open duplicated warning popup.)
        + Changed provider logo image for french.
    - Resolved Issues 
        N/A 
*** 
version 0.1.10
    - New Feature 
        + Fixed instruction module of weather main screen.
        + Fixed city display naming rule.
        + Fixed provider logo logic.
        + Fixed preferece bug. (did not add hometown)
        + Fixed weather main dashboard module.
        + Fixed weather data display rule. (Display “N/A” when no data available.)
    - Resolved Issues 
        N/A  
*** 
version 0.1.9
    - New Feature 
        + Fixed meue tree bugs.
        + Fixed menu tree description bug.
    - Resolved Issues 
        N/A  
*** 
version 0.1.8
    - New Feature 
        + Fixed search popup bug.
        + Fixed Screen saver module.
    - Resolved Issues 
        N/A  
*** 
version 0.1.7
    - New Feature 
        + Fixed search popup bug.(Temp view text - related)
        + Fixed Weather channel check module.
    - Resolved Issues 
        N/A  
*** 
version 0.1.6
    - New Feature 
        + Added message of the search popup. (No match. Your search did not match anything currently available.)
        + Fixed keyboard module of the search popup. (disappearing focus.)
        + Fixed key action module of the search popup. (about Exit key)
        + Changed letter count of allowing searching.(to 2 letters)
        + Modified Keyboard listener implementing class for changing keyboard listener.
    - Resolved Issues 
        N/A  
*** 




This is Weather App Release Notes.

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed IB parsing module.
- Release name : version 0.1.5
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed search module error.
- Release name : version 0.1.4
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed scaled video module (Do not show info bar).
- Release name : version 0.1.3
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fix : Fixed search module.
  + Fix : Fixed home town postal code module.  
  + Fix : added Error message service.
  + Fix : added Screen saver listener.
- Release name : version 0.1.2
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fix : Fixed D-Option GUI bug.
  + Fix : Fixed preference module(hometown and favorite city replace, remove etc.)
- Release name : version 0.1.1
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.28
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : Initial release.
- Release name : version 0.1.0
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.28
-----------------------------------------------------------------------------------

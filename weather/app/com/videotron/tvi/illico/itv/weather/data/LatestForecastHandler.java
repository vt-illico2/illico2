package com.videotron.tvi.illico.itv.weather.data;

import java.awt.Image;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.itv.weather.comp.SAXHandlerAdapter;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.log.Log;

public class LatestForecastHandler extends SAXHandlerAdapter {
    private LatestForecast latestForecast;
    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("DATAFILE")) {
            latestForecast = new LatestForecast();
        } else if (valueXMLTag.equalsIgnoreCase("SITE")) {
            latestForecast.setCityInfo(getCityInfo(attr));
        } else if (valueXMLTag.equalsIgnoreCase("OBS")) {
            latestForecast.setCurrentCondition(getCurrentCondition(attr));
        } else if (valueXMLTag.equalsIgnoreCase("PERIOD")) {
            latestForecast.addShortTermForecast(getShortTermForecast(attr));
        } else if (valueXMLTag.equalsIgnoreCase("WEEKDAY")) {
            latestForecast.addLongTermForecast(getLongTermForecast(attr));
        }
    }
    public void endElement(String valueXMLTag) {
        if (valueXMLTag != null && valueXMLTag.equalsIgnoreCase("DATAFILE")) {
            setResultObject(latestForecast);
        }
    }
    public void parseCDData(String valueXMLTag, String valueCDData) {
        if (valueXMLTag.equalsIgnoreCase("NextUpdate")) {
            latestForecast.setNextUpdateDate(valueCDData);
        }
    }
    private CityInfo getCityInfo(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CityInfo ci = new CityInfo();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("NAME")) {
                ci.setCityName(lValue);
            } else if (lName.equalsIgnoreCase("PROV")) {
                ci.setProvinceName(lValue);
            } else if (lName.equalsIgnoreCase("COUNTRY")) {
                ci.setCountryName(lValue);
                if (lValue != null) {
                    if (lValue.equalsIgnoreCase("Canada")) {
                        ci.setCityType(CityInfo.CITY_TYPE_CANADA);
                    } else {
                        ci.setCityType(CityInfo.CITY_TYPE_OTHER_CITY);
                    }
                }
            } else if (lName.equalsIgnoreCase("LOCATIONID")) {
                ci.setLocationId(lValue);
            }
        }
        return ci;
    }
    private CurrentCondition getCurrentCondition(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CurrentCondition cc = new CurrentCondition();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("DATE")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] DATE : ["+lValue+"]");
                }
                cc.setWeatherDate(lValue);
            } else if (lName.equalsIgnoreCase("STATION")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] STATION : ["+lValue+"]");
                }
                cc.setWeatherStation(lValue);
            } else if (lName.equalsIgnoreCase("ICON")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] ICON : ["+lValue+"]");
                }
                cc.setIconCode(lValue);
                if (lValue != null) {
                    Image imgIconCode = ImageManager.getInstance().getImage("icons/"+lValue.trim()+".png", ImageManager.TYPE_TEMPORARY);
                    cc.setIconCodeImage(imgIconCode);
                }
            } else if (lName.equalsIgnoreCase("FORECAST_TEXT")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] FORECAST_TEXT : ["+lValue+"]");
                }
                cc.setForecastText(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] TEMPERATURE_C : ["+lValue+"]");
                }
                cc.setTemperatureC(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] TEMPERATURE_F : ["+lValue+"]");
                }
                cc.setTemperatureF(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] FEELS_LIKE_C : ["+lValue+"]");
                }
                cc.setFeelsLikeC(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] FEELS_LIKE_F : ["+lValue+"]");
                }
                cc.setFeelsLikeF(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_KM")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] WIND_SPEED_KM : ["+lValue+"]");
                }
                cc.setWindSpeedKm(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_MI")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] WIND_SPEED_MI : ["+lValue+"]");
                }
                cc.setWindSpeedMi(lValue);
            } else if (lName.equalsIgnoreCase("WIND_DIR")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] WIND_DIR : ["+lValue+"]");
                }
                cc.setWindDirection(lValue);
            } else if (lName.equalsIgnoreCase("HUMIDITY")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getCurrentCondition] HUMIDITY : ["+lValue+"]");
                }
                cc.setHumidity(lValue);
            }
        }
        return cc;
    }
    private ShortTermForecast getShortTermForecast(Attributes attr) {
        if (attr == null) {
            return null;
        }
        ShortTermForecast stf = new ShortTermForecast();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("ID")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] ID : ["+lValue+"]");
                }
                stf.setId(lValue);
            } else if (lName.equalsIgnoreCase("DATE")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] DATE : ["+lValue+"]");
                }
                stf.setDate(lValue);
            } else if (lName.equalsIgnoreCase("NAME")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] NAME : ["+lValue+"]");
                }
                stf.setName(lValue);
            } else if (lName.equalsIgnoreCase("ICON")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] ICON : ["+lValue+"]");
                }
                stf.setIconCode(lValue);
                if (lValue != null) {
                    Image imgIconCode = ImageManager.getInstance().getImage("icons/"+lValue.trim()+".png", ImageManager.TYPE_TEMPORARY);
                    stf.setIconCodeImage(imgIconCode);
                }
            } else if (lName.equalsIgnoreCase("FORECAST_TEXT")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] FORECAST_TEXT : ["+lValue+"]");
                }
                stf.setForecastText(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] TEMPERATURE_C : ["+lValue+"]");
                }
                stf.setTemperatureC(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] TEMPERATURE_F : ["+lValue+"]");
                }
                stf.setTemperatureF(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] FEELS_LIKE_C : ["+lValue+"]");
                }
                stf.setFeelsLikeC(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] FEELS_LIKE_F : ["+lValue+"]");
                }
                stf.setFeelsLikeF(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_KM")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] WIND_SPEED_KM : ["+lValue+"]");
                }
                stf.setWindSpeedKm(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_MI")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] WIND_SPEED_MI : ["+lValue+"]");
                }
                stf.setWindSpeedMi(lValue);
            } else if (lName.equalsIgnoreCase("WIND_DIR")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] WIND_DIR : ["+lValue+"]");
                }
                stf.setWindDirection(lValue);
            } else if (lName.equalsIgnoreCase("POP")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getShortTermForecast] POP : ["+lValue+"]");
                }
                stf.setProbabilityOfPrecipitation(lValue);
            }
        }
        return stf;
    }
    private LongTermForecast getLongTermForecast(Attributes attr) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LatestForecastHandler.getLongTermForecast]attr:" + attr);
        }
        if (attr == null) {
            return null;
        }
        LongTermForecast ltf = new LongTermForecast();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("ID")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] ID : ["+lValue+"]");
                }
                ltf.setId(lValue);
            } else if (lName.equalsIgnoreCase("DATE")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] DATE : ["+lValue+"]");
                }
                ltf.setDate(lValue);
            } else if (lName.equalsIgnoreCase("NAME")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] NAME : ["+lValue+"]");
                }
                ltf.setName(lValue);
            } else if (lName.equalsIgnoreCase("ICON")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] ICON : ["+lValue+"]");
                }
                ltf.setIconCode(lValue);
                if (lValue != null) {
                    Image imgIconCode = ImageManager.getInstance().getImage("icons/"+lValue.trim()+".png", ImageManager.TYPE_TEMPORARY);
                    ltf.setIconCodeImage(imgIconCode);
                }
            } else if (lName.equalsIgnoreCase("FORECAST_TEXT")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] FORECAST_TEXT : ["+lValue+"]");
                }
                ltf.setForecastText(lValue);
            } else if (lName.equalsIgnoreCase("HIGH_TEMP_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] HIGH_TEMP_C : ["+lValue+"]");
                }
                ltf.setHighTemperatureC(lValue);
            } else if (lName.equalsIgnoreCase("LOW_TEMP_C")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] LOW_TEMP_C : ["+lValue+"]");
                }
                ltf.setLowTemperatureC(lValue);
            } else if (lName.equalsIgnoreCase("HIGH_TEMP_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] HIGH_TEMP_F : ["+lValue+"]");
                }
                ltf.setHighTemperatureF(lValue);
            } else if (lName.equalsIgnoreCase("LOW_TEMP_F")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] LOW_TEMP_F : ["+lValue+"]");
                }
                ltf.setLowTemperatureF(lValue);
            } else if (lName.equalsIgnoreCase("POP")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] POP : ["+lValue+"]");
                }
                ltf.setProbabilityOfPrecipitation(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_KM")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] WIND_SPEED_KM : ["+lValue+"]");
                }
                ltf.setWindSpeedKm(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_MI")) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LatestForecastHandler.getLongTermForecast] WIND_SPEED_MI : ["+lValue+"]");
                }
                ltf.setWindSpeedMi(lValue);
            }
        }
        return ltf;
    }
}

package com.videotron.tvi.illico.itv.weather.comp;

public interface FavoriteCityDashboardListener {
    void lostDashboardFocus();
    void selectedFavoriteCityWidget(FavoriteCityWidget banner);
}

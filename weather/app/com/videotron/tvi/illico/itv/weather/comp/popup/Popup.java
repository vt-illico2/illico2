package com.videotron.tvi.illico.itv.weather.comp.popup;

import java.awt.Container;

import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;

abstract public class Popup extends Container implements AnimationRequestor {
    private static final long serialVersionUID = 1L;
    /*****************************************************************
     * variables - PopupListener
     *****************************************************************/
    protected PopupListener pListener;

    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void dispose() {
        stopPopup();
        disposePopup();
        pListener = null;
    }

    public void start() {
        startPopup();
    }

    public void stop() {
        stopPopup();
    }

    /*****************************************************************
     * methods - PopupListener
     *****************************************************************/
    public void setPopupListener(PopupListener pListener) {
        this.pListener = pListener;
    }

    public void removePopupListener() {
        this.pListener = null;
    }

    public PopupListener getPopupListener() {
        return pListener;
    }

    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void disposePopup();

    abstract protected void startPopup();

    abstract protected void stopPopup();

    abstract public boolean keyAction(int keyCode);
    /*****************************************************************
     * AnimationRequestor - related
     *****************************************************************/
    public boolean skipAnimation(Effect effect) {
        return false;
    }
    public void animationStarted(Effect effect) {
    }
    public void animationEnded(Effect effect) {
    }
}

package com.videotron.tvi.illico.itv.weather.data;

public class ServerInfo {
    private String host; //IP or DNS
    private int port; //Port
    private String contextRoot; //Context Root
    private ProxyInfo[] proxyInfos; //Proxy Info
    
    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
    }
    public String getContextRoot() {
        return contextRoot;
    }
    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
    }
    public ProxyInfo[] getProxyInfos() {
        return proxyInfos;
    }
    public void setProxyInfos(ProxyInfo[] proxyInfos) {
        this.proxyInfos = proxyInfos;
    }
}
package com.videotron.tvi.illico.itv.weather;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * 
 * @version $Revision: 1.43 $ $Date: 2017/01/12 19:49:02 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "R7.1.1";//"1.0.28.0";
    /** Application Name. */
    public static final String APP_NAME = "Weather";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    /** This constant is flag whether emulator test or not. */
    public static final boolean FORCED_LOCAL_SERVER = IS_EMULATOR && true;
    public static final boolean FORCED_LOCAL_IB = IS_EMULATOR && true;
    public static final boolean LOCAL_DATA_AS_MS_DATA = true; //MS_DATA or USER_PROP_DATA
    
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    public static final Font F15 = FontResource.BLENDER.getFont(15, true);
    public static final Font F16 = FontResource.BLENDER.getFont(16, true);
    public static final Font F17 = FontResource.BLENDER.getFont(17, true);
    public static final Font F18 = FontResource.BLENDER.getFont(18, true);
    public static final Font F19 = FontResource.BLENDER.getFont(19, true);
    public static final Font F20 = FontResource.BLENDER.getFont(20, true);
    public static final Font F21 = FontResource.BLENDER.getFont(21, true);
    public static final Font F22 = FontResource.BLENDER.getFont(22, true);
    public static final Font F24 = FontResource.BLENDER.getFont(24, true);
    public static final Font F25 = FontResource.BLENDER.getFont(25, true);
    public static final Font F26 = FontResource.BLENDER.getFont(26, true);
    public static final Font F27 = FontResource.BLENDER.getFont(27, true);
    public static final Font F28 = FontResource.BLENDER.getFont(28, true);
    public static final Font F30 = FontResource.BLENDER.getFont(30, true);
    public static final Font F32 = FontResource.BLENDER.getFont(32, true);
    public static final Font F33 = FontResource.BLENDER.getFont(33, true);
    public static final Font F34 = FontResource.BLENDER.getFont(34, true);
    public static final Font F48 = FontResource.BLENDER.getFont(48, true);
    public static final Font F50 = FontResource.BLENDER.getFont(50, true);
    public static final Font F53 = FontResource.BLENDER.getFont(53, true);
    
    public static final FontMetrics FM15 = FontResource.getFontMetrics(F15);
    public static final FontMetrics FM16 = FontResource.getFontMetrics(F16);
    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
    public static final FontMetrics FM24 = FontResource.getFontMetrics(F24);
    public static final FontMetrics FM25 = FontResource.getFontMetrics(F25);
    public static final FontMetrics FM26 = FontResource.getFontMetrics(F26);
    public static final FontMetrics FM27 = FontResource.getFontMetrics(F27);
    public static final FontMetrics FM28 = FontResource.getFontMetrics(F28);
    public static final FontMetrics FM30 = FontResource.getFontMetrics(F30);
    public static final FontMetrics FM32 = FontResource.getFontMetrics(F32);
    public static final FontMetrics FM33 = FontResource.getFontMetrics(F33);
    public static final FontMetrics FM34 = FontResource.getFontMetrics(F34);
    public static final FontMetrics FM48 = FontResource.getFontMetrics(F48);
    public static final FontMetrics FM50 = FontResource.getFontMetrics(F50);
    public static final FontMetrics FM53 = FontResource.getFontMetrics(F53);
    
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C000000000 = new Color(0, 0, 0);
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C046046045 = new Color(46, 46, 45);
    public static final Color C047047047 = new Color(47, 47, 47);
    public static final Color C050050050 = new Color(50, 50, 50);
    public static final Color C078078078 = new Color(78, 78, 78);
    public static final Color C086086086 = new Color(86, 86, 86);
    public static final Color C110110110 = new Color(110, 110, 110);
    public static final Color C133133133 = new Color(133, 133, 133);
    public static final Color C142142142 = new Color(142, 142, 142);
    public static final Color C164164164 = new Color(164, 164, 164);
    public static final Color C171171171 = new Color(171, 171, 171);
    public static final Color C180180180 = new Color(180, 180, 180);
    public static final Color C182182182 = new Color(182, 182, 182);
    public static final Color C185185185 = new Color(185, 185, 185);
    public static final Color C187189192 = new Color(187, 189, 192);
    public static final Color C193191191 = new Color(193, 191, 191);
    public static final Color C198198198 = new Color(198, 198, 198);
    public static final Color C202174097 = new Color(202, 174, 97);
    public static final Color C210210210 = new Color(210, 210, 210);
    public static final Color C214182055 = new Color(214, 182, 55);
    public static final Color C219219219 = new Color(219, 219, 219);
    public static final Color C236211143 = new Color(236, 211, 143);
    public static final Color C236236237 = new Color(236, 236, 237);
    public static final Color C245212016 = new Color(245, 212, 16);
    public static final Color C249197000 = new Color(249, 197, 0);
    public static final Color C255204000 = new Color(255, 204, 0);
    public static final Color C255255255 = new Color(255, 255, 255);
    public static final  Color DVB000000000153 = new DVBColor(0, 0, 0, 153);
    public static final  Color DVB203203203020 = new DVBColor(203, 203, 203, 20);
    public static final  Color DVB203203203026 = new DVBColor(203, 203, 203, 26);
    public static final  Color DVB219219219051 = new DVBColor(219, 219, 219, 51);
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;
    
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);
    
    /*********************************************************************************
     * DataCenter key-related
     *********************************************************************************/
    public static final String DCKEY_WEATHER_CHANNEL_EN = "WEATHER_CHANNEL_EN";
    public static final String DCKEY_WEATHER_CHANNEL_FR = "WEATHER_CHANNEL_FR";
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
    public static final String ERROR_CODE_SERVER_ACCESS_PROBLEM = "MET500";
    public static final String ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM = "MET501";
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM = "MET502";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "MET503";
}

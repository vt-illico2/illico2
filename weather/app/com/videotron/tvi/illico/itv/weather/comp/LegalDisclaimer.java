package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;

public class LegalDisclaimer extends Component {
    private static final long serialVersionUID = -271138887086349168L;
    private Image imgProvLogoEn;
    private Image imgProvLogoFr;
    
    public void init() {
        if (imgProvLogoEn == null) {
            imgProvLogoEn = ImageManager.getInstance().getImage("logo_en.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgProvLogoFr == null) {
            imgProvLogoFr = ImageManager.getInstance().getImage("logo_fr.png", ImageManager.TYPE_TEMPORARY);
        }
    }
    public void dispose() {
        imgProvLogoEn = null;
        imgProvLogoFr = null;
    }
    public void start() {
    }
    public void stop() {
    }
    public void paint(Graphics g) {
        Image imgProvLogo = null;
        String langTag = DataManager.getInstance().getCurrentLanguageTag();
        if (langTag != null && langTag.equals(DataManager.LANGUAGE_TAG_FR)) {
            imgProvLogo = imgProvLogoFr;
        } else {
            imgProvLogo = imgProvLogoEn;
        }
        if (imgProvLogo != null) {
            g.drawImage(imgProvLogo, 0, 0, this);
        }
    }
}
package com.videotron.tvi.illico.itv.weather.ui;

import java.awt.Image;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.comp.PopupSearch;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.controller.RPManagerException;
import com.videotron.tvi.illico.itv.weather.controller.SceneManager;
import com.videotron.tvi.illico.itv.weather.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.LongTermForecast;
import com.videotron.tvi.illico.itv.weather.gui.WeatherLongTermForecastRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class WeatherLongTermForecastUI extends BaseUI {
    private static final long serialVersionUID = 7441667616008799529L;
//    private DataCenter dCenter = DataCenter.getInstance();
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Footer footer;
    /*********************************************************************************
     * Popup-related
     *********************************************************************************/
    private PopupSearch popSearch;
    private PopupAdapter popAdaptSearch;
    /*********************************************************************************
     * LatestForecast-related
     *********************************************************************************/
    private String msg;
    public static final int PERIOD_COUNT = 5;
    private String[] locIds;
    private LatestForecast[] latestForecasts;
    private int curIdx;

    protected void initScene() {
        renderer = new WeatherLongTermForecastRenderer();
        setRenderer(renderer);
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
    }
    protected void disposeScene() {
        msg = null;
        locIds = null;
        latestForecasts = null;
        if (popSearch != null) {
            popSearch.dispose();
            popSearch = null;
        }
        popAdaptSearch = null;
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        renderer = null;
    }
    protected void startScene(boolean resetScene, String[] params) {
        CommunicationManager.getInstance().requestResizeScreen(587, 72, 322, 181);
        prepare();
        if (resetScene) {
            locIds = params;
            if (locIds == null || locIds.length == 0) {
                msg = DataCenter.getInstance().getString("TxtWeather.No_City_Info");
                return;
            }
            if (footer != null) {
                footer.reset();
                if (locIds.length == 1) {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtWeather.Back");
                    footer.addButton(PreferenceService.BTN_SEARCH, "TxtWeather.City_Search");
                } else {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtWeather.Back");
                    footer.addButton(PreferenceService.BTN_SEARCH, "TxtWeather.City_Search");
                    footer.addButton(PreferenceService.BTN_ARROW, "TxtWeather.Change_City");
                }
            }
            initLatestForecasts();
        }
    }
    protected void stopScene() {
        isRequested = true;
        CommunicationManager.getInstance().requestHideLoadingAnimation();
        if (popSearch != null) {
            popSearch.removePopupListener();
            PopupController.getInstance().closePopup(popSearch);
        }
        closeAllPopup();
    }
    protected boolean keyAction(int keyCode) {
        int tempValue = -1;
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_LEFT:
                tempValue = (latestForecasts.length + curIdx - 1) % latestForecasts.length;
                if (tempValue != curIdx) {
                    if (footer != null) {
                        footer.clickAnimation("TxtWeather.Change_City");
                    }
                    curIdx = tempValue;
                    resetCurrentLatestForecast();
                    repaint();
                }
                return true;
            case Rs.KEY_RIGHT:
                tempValue = (curIdx + 1) % latestForecasts.length;
                if (tempValue != curIdx) {
                    if (footer != null) {
                        footer.clickAnimation("TxtWeather.Change_City");
                    }
                    curIdx = tempValue;
                    resetCurrentLatestForecast();
                    repaint();
                }
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.Back");
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_WEATHER_MAIN, false, null);
                return true;
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherLongTermForecastUI.keyAction]KeyCodes.COLOR_A");
                }
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.City_Search");
                }
                if (popSearch == null) {
                    popSearch = new PopupSearch();
                    popSearch.setBounds(0, 0, 960, 540);
                }
                popSearch.setPopupListener(getPopupAdapterSearch());
                PopupController.getInstance().openPopup(popSearch);
                return true;
        }
        return false;
    }
    /*********************************************************************************
     * LatestForecast-related
     *********************************************************************************/
    private void initLatestForecasts() {
        CommunicationManager.getInstance().requestShowLoadingAnimation(480, 270);
        isRequested = false;
        new Thread(Rs.APP_NAME + ".LongTermForecast") {
            public void run() {
                boolean existErrorData = false;
                String exceptionMsg = null;
                int locIdsLth = locIds.length;
                latestForecasts = new LatestForecast[locIdsLth];
                for (int i=0; i<locIdsLth; i++) {
                    try{
                        latestForecasts[i] = DataManager.getInstance().getLatestForecast(locIds[i]);
                    }catch(RPManagerException e) {
                        e.printStackTrace();
                        exceptionMsg = e.getMessage();
                        existErrorData = true;
                    }
                }
                curIdx = 0;
                loadCurrentLatestForecastIconCodeImage();
                isRequested = true;
                CommunicationManager.getInstance().requestHideLoadingAnimation();
                repaint();
                if (existErrorData) {
                    CommunicationManager.getInstance().requestShowErrorMessage(exceptionMsg, null);
                    return;
                }
            }
        }.start();
    }
    private void resetCurrentLatestForecast() {
        if (latestForecasts == null) {
            return;
        }
        CommunicationManager.getInstance().requestShowLoadingAnimation(480, 270);
        isRequested = false;
        loadCurrentLatestForecast();
        loadCurrentLatestForecastIconCodeImage();
        isRequested = true;
        CommunicationManager.getInstance().requestHideLoadingAnimation();
    }
    private void loadCurrentLatestForecast() {
        try{
            latestForecasts[curIdx] = DataManager.getInstance().getLatestForecast(locIds[curIdx]);
        }catch(RPManagerException e) {
            e.printStackTrace();
        }
    }
    private void loadCurrentLatestForecastIconCodeImage() {
        LongTermForecast[] ltfs = null;
        if (latestForecasts != null && curIdx < latestForecasts.length && latestForecasts[curIdx] != null) {
            ltfs = latestForecasts[curIdx].getLongTermForecasts();
            if (ltfs != null) {
                for (int i=0; i<ltfs.length; i++) {
                    if (ltfs[i] == null) {
                        continue;
                    }
                    String iconCode = ltfs[i].getIconCode();
                    if (iconCode == null) {
                        continue;
                    }
                    Image imgIconCode = ImageManager.getInstance().getImage(iconCode, ImageManager.TYPE_TEMPORARY);
                    if (imgIconCode == null) {
                        dMgr.loadIconCodeImage(iconCode);
                    }
                }
            }
        }
    }
    /*********************************************************************************
     * PopupAdapter-related
     *********************************************************************************/
    private PopupAdapter getPopupAdapterSearch() {
        if (popAdaptSearch == null) {
            popAdaptSearch = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupSearch pop = (PopupSearch)popupEvent.getPopup();
                    String selectedLocId = null;
                    CityInfo selectedCityInfo = pop.getSelectedCityInfo();
                    if (selectedCityInfo != null) {
                        selectedLocId = selectedCityInfo.getLocationId();
                    }
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                    if (selectedLocId == null) {
                        return;
                    }
                    int tempValue = getLocationIdIndex(locIds, selectedLocId);
                    if (tempValue >= 0) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[WeatherLongTermForecastUI.popupOK]"+selectedLocId+" is already included.");
                        }
                        if (tempValue != curIdx) {
                            curIdx = tempValue;
                            resetCurrentLatestForecast();
                            repaint();
                        }
                        return;
                    }
                    String[] tempLocIds = (String[]) locIds.clone();
                    locIds = new String[tempLocIds.length + 1];
                    locIds[0] = selectedLocId;
                    for (int i=1; i<locIds.length; i++) {
                        locIds[i] = tempLocIds[i-1];
                    }
                    WeatherLongTermForecastUI.this.locIds = locIds;
                    initLatestForecasts();
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupSearch pop = (PopupSearch)popupEvent.getPopup();
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
                private int getLocationIdIndex(String[] srcLocIds, String targetLocId) {
                    if (srcLocIds == null || targetLocId == null) {
                        return -1;
                    }
                    for (int i=0; i<srcLocIds.length; i++) {
                        if (srcLocIds[i] == null) {
                            continue;
                        }
                        if (srcLocIds[i].equals(targetLocId)) {
                            return i;
                        }
                    }
                    return -1;
                }
            };
        }
        return popAdaptSearch;
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public String getMessge() {
        return msg;
    }
    public int getCurrentIndex() {
        return curIdx;
    }
    public LatestForecast[] getLatestForecasts() {
        return latestForecasts;
    }
}

package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Component;
import java.awt.Graphics;

import com.videotron.tvi.illico.itv.weather.Rs;

abstract public class PreferencePopupAddon extends Component{
    private static final long serialVersionUID = 1L;
    public void paint(Graphics g) {
      //Draws background.
      g.setColor(Rs.DVB000000000153);
      g.fillRect(0, 0, 960, 540);
      renderer(g);
    }
    abstract public void renderer(Graphics g);
}

package com.videotron.tvi.illico.itv.weather.controller;

import java.awt.Image;
import java.io.File;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.Util;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CurrentCondition;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.ProxyInfo;
import com.videotron.tvi.illico.itv.weather.data.ServerInfo;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

public class DataManager implements DataUpdateListener {
    private static DataManager instance;
    private static PreferenceProxy pProxy = PreferenceProxy.getInstance();
    private RPManager rpMgr;

    private final StringBuffer sb = new StringBuffer();

    public static final int TEMPERATURE_UNIT_ID_C = 0;
    public static final int TEMPERATURE_UNIT_ID_F = 1;
    public static final String TEMPERATURE_UNIT_C = "C";
    public static final String TEMPERATURE_UNIT_F = "F";
    private int temperatureUnit = -1;

    public static final String LANGUAGE_TAG_EN = "en";
    public static final String LANGUAGE_TAG_FR = "fr";
    private String curLanguageTag;

    private String weatherChannelNameEn;
    private String weatherChannelNameFr;

//    private Hashtable latestForecastHash;
    private Hashtable cityInfoHashByPostalCode;
    private static final String HASH_KEY_HEADER_CITY_LIST = "CITY_LIST_";
    private Hashtable cityListHash;
    // private Hashtable iconCodeImageHash;
    
    private Comparator COMP_CITY_NAME=new Comparator(){
        Collator usCollator;
        public int compare(Object o1, Object o2) {
            if (usCollator==null) {
                usCollator = Collator.getInstance(Locale.CANADA_FRENCH);
            }
            return usCollator.compare(((CityInfo)o1).getDisplayName(), ((CityInfo)o2).getDisplayName());
        }
    }; 

    /*********************************************************************************
     * Favorite city-related
     *********************************************************************************/
    public static final String SEPARATOR = "|";
    public static final int FAVORITE_CITY_MAX_COUNT = 4;
    private String[] favCityLocIds;
    private String[] validFavCityLocIds;
    private String[] regCityLocIds;
    /*********************************************************************************
     * IB-related
     *********************************************************************************/
    public static final int ITV_SERVER_TYPE = 3;
    private static final String ITV_SERVER_DATA_IB = "ITV_SERVER_DATA_IB";
    private ServerInfo serverInfo;
    private int latestVer;

    /*********************************************************************************
     * Life cycle-related
     *********************************************************************************/
    private DataManager() {
    }
    public synchronized static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }
    public void init() {
        latestVer = -1;
        if (Rs.FORCED_LOCAL_IB) {
            if (Rs.LOCAL_DATA_AS_MS_DATA) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Emulator - Properties.");
                }
                byte[] bytesITvServer = DataConverter.getBytesITvServer();
                parseITvServerData(bytesITvServer);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Test by ms data.");
                }
                File fileITvServer = new File("resource/test_data/ms_data/itv_server.txt");
                byte[] bytesITvServer = Util.getByteArrayFromFile(fileITvServer);
                parseITvServerData(bytesITvServer);
            }
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Use real data.(Inband, OOB)");
            }
            File fileLoadedITvServer = (File)DataCenter.getInstance().get(ITV_SERVER_DATA_IB);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded ITV Server file : "+fileLoadedITvServer);
            }
            if (fileLoadedITvServer != null) {
                dataUpdated(ITV_SERVER_DATA_IB, null, fileLoadedITvServer);
            }
            DataCenter.getInstance().addDataUpdateListener(ITV_SERVER_DATA_IB, this);
        }
        // iconCodeImageHash = new Hashtable();
    }
    public void dispose() {
        stop();
        DataCenter.getInstance().removeDataUpdateListener(ITV_SERVER_DATA_IB, this);
    }
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.start]start.");
        }
//        latestForecastHash = new Hashtable();
        cityInfoHashByPostalCode = new Hashtable();
        cityListHash = new Hashtable();
        String language = pProxy.getLanguage();
        if (language != null && language.equals(Definitions.LANGUAGE_FRENCH)) {
            curLanguageTag = LANGUAGE_TAG_FR;
        } else {
            curLanguageTag = LANGUAGE_TAG_EN;
        }
        initFavoriteCityLocationId();
    }
    public void stop() {
        favCityLocIds = null;
        validFavCityLocIds = null;
        regCityLocIds = null;
        curLanguageTag = null;
        weatherChannelNameEn = null;
        weatherChannelNameFr = null;
//        if (latestForecastHash != null) {
//            latestForecastHash.clear();
//            latestForecastHash = null;
//        }
        if (cityInfoHashByPostalCode != null) {
            cityInfoHashByPostalCode.clear();
            cityInfoHashByPostalCode = null;
        }
        if (cityListHash != null) {
            cityListHash.clear();
            cityListHash = null;
        }
    }
    /*********************************************************************************
     * IB data-related
     *********************************************************************************/
    private boolean parseITvServerData(byte[] src) {
        if (src == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }       
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
            return false;
        }
        try {
            int index = 0;
            //Version
            int version = src[index++];
            if (version == latestVer) {
                return true;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseITVServerData]version : " + version);
            }
            //Server list size
            int serverListSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseITVServerData]Server list size : " + serverListSize);
            }
            ServerInfo tempServerInfo = null;
            for (int i=0; i<serverListSize; i++) {
                //Server type
                int serverType = src[index++];
                //IpDns
                int ipDnsLth = src[index++];
                String ipDns = new String(src, index, ipDnsLth, "ISO8859_1");
                index += ipDnsLth;
                //Port
                int port = Util.twoBytesToInt(src, index, true);
                index += 2;
                //Context root
                int contextRootLth = src[index++];
                String contextRoot = new String(src, index, contextRootLth, "ISO8859_1");
                index += contextRootLth;
                //Proxy list size
                int proxyListSize = src[index++];
                ProxyInfo[] tempProxyInfoList = new ProxyInfo[proxyListSize];
                for (int j=0; j<proxyListSize; j++) {
                    tempProxyInfoList[j] = new ProxyInfo();
                    //Proxy ip
                    int proxyIpLth = src[index++];
                    String proxyIp = new String(src, index, proxyIpLth, "ISO8859_1");
                    index += proxyIpLth;
                    tempProxyInfoList[j].setProxyHost(proxyIp);
                    //Proxy port
                    int proxyPort = Util.twoBytesToInt(src, index, true);
                    index += 2;
                    tempProxyInfoList[j].setProxyPort(proxyPort);
                }
                if (serverType == ITV_SERVER_TYPE) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseITvServerData]Host : " + ipDns);
                        Log.printDebug("[DataMgr.parseITvServerData]Port : " + port);
                        Log.printDebug("[DataMgr.parseITvServerData]Context root : " + contextRoot);
                        if (tempProxyInfoList != null) {
                            for (int j=0; j<tempProxyInfoList.length; j++) {
                                Log.printDebug("[DataMgr.parseITvServerData]Proxy info["+j+"] - Host : " + tempProxyInfoList[j].getProxyHost());
                                Log.printDebug("[DataMgr.parseITvServerData]Proxy info["+j+"] - Port : " + tempProxyInfoList[j].getProxyPort());
                            }
                        }
                    }
                    tempServerInfo = new ServerInfo();
                    tempServerInfo.setHost(ipDns);
                    tempServerInfo.setPort(port);
                    tempServerInfo.setContextRoot(contextRoot);
                    tempServerInfo.setProxyInfos(tempProxyInfoList);
                    break;
                }
            }
            latestVer = version;
            serverInfo = tempServerInfo;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }            
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
            return false;
        }
        return true;
    }
    /*********************************************************************************
     * Favorite city-related
     *********************************************************************************/
    private RPManager getRPManager() {
        if (rpMgr == null) {
            if (Rs.FORCED_LOCAL_SERVER) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.getRPManager]Create RP manager using local data.");
                }
                rpMgr = new RPManagerLocal();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.getRPManager]Create RP manager using server data.");
                }
                rpMgr = new RPManagerServer();
            }
            rpMgr.init();
        }
        return rpMgr;
    }
    public boolean setTemperatureUnit(int reqTemperatureUnit) {
        String value = null;
        switch(reqTemperatureUnit) {
            case TEMPERATURE_UNIT_ID_C:
                value = TEMPERATURE_UNIT_C;
                break;
            case TEMPERATURE_UNIT_ID_F:
                value = TEMPERATURE_UNIT_F;
                break;
        }
        boolean result = pProxy.setTemperatureUnit(value);
        if (result) {
            temperatureUnit = reqTemperatureUnit;
        }
        return result;
    }
    public int getTemperatureUnit() {
        String uppTemperatureUnit = pProxy.getTemperatureUnit();
        if (uppTemperatureUnit != null && uppTemperatureUnit.equals(TEMPERATURE_UNIT_F)) {
            temperatureUnit = TEMPERATURE_UNIT_ID_F;
        } else {
            temperatureUnit = TEMPERATURE_UNIT_ID_C;
        }
        return temperatureUnit;
    }
    public String getWeatherChannelCallLetter() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getWeatherChannelNameByCurrentLanguage]start.");
        }
        String curLangTag = getCurrentLanguageTag();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getWeatherChannelNameByCurrentLanguage]Currnt language tag : " + curLangTag);
        }
        if (curLangTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getWeatherChannelNameByCurrentLanguage]Currnt language tag is null. return null.");
            }
            return null;
        }
        String wChName = null;
        if (curLangTag.equals(LANGUAGE_TAG_EN)) {
            if (weatherChannelNameEn == null) {
                weatherChannelNameEn = DataCenter.getInstance().getString("WEATHER_CHANNEL_NAME_EN");
            }
            wChName = weatherChannelNameEn;
        } else if (curLangTag.equals(LANGUAGE_TAG_FR)) {
            if (weatherChannelNameFr == null) {
                weatherChannelNameFr = DataCenter.getInstance().getString("WEATHER_CHANNEL_NAME_FR");
            }
            wChName = weatherChannelNameFr;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getWeatherChannelNameByCurrentLanguage]Weather channel name : "+wChName);
        }
        return wChName;
    }
    public CurrentCondition getCurrentCondition(String reqLocationId) {
        return null;
    }
    public String getLocationId(String reqPostalCode) {
        return null;
    }
    public ServerInfo getServerInfo() {
        return serverInfo;
    }
    /*********************************************************************************
     * Favorite city-related
     *********************************************************************************/
    synchronized public boolean setHomeTownLocationId(String reqLocId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setHomeTownLocationId]start.");
            Log.printDebug("[DataMgr.setHomeTownLocationId]Param - home town location id : " + reqLocId);
        }
        if (reqLocId == null) {
            reqLocId = "";
        }
        boolean result = pProxy.setHomeTownLocationId(reqLocId);
        if (result) {
            regCityLocIds = null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setHomeTownLocationId]setHomeTownLocationId to UPP. result : "+result);
        }
        return result;
    }
    synchronized public String getHomeTownLocationId() throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getHomeTownLocationId]start.");
        }
        String homeTownLocId = pProxy.getHomeTownLocationId();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getHomeTownLocationId]UPP home town location id : " + homeTownLocId);
        }
        if (homeTownLocId == null || homeTownLocId.length() == 0) {
            String adminPostalCode = pProxy.getPostalCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getHomeTownLocationId]UPP admin postal code : "
                        + adminPostalCode);
            }
            String lTag = getCurrentLanguageTag();
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getHomeTownLocationId]Language tag : " + lTag);
            }
            CityInfo cInfo = getRPManager().getCityInfoByPostalCode(lTag, adminPostalCode);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getHomeTownLocationId]Response city info : " + cInfo);
            }
            if (cInfo != null) {
                cityInfoHashByPostalCode.put(adminPostalCode, cInfo);
                homeTownLocId = cInfo.getLocationId();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.getHomeTownLocationId]Response home town location id : "
                            + homeTownLocId);
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getHomeTownLocationId]end : " + homeTownLocId);
        }
        return homeTownLocId;
    }
    synchronized public void initFavoriteCityLocationId() {
        favCityLocIds = new String[FAVORITE_CITY_MAX_COUNT];
        String[] tokenedFavCityLocIds = null;
        String uppFavCityLocIdsStr = pProxy.getFavoriteCityLocationIdString();
        if (uppFavCityLocIdsStr != null) {
            tokenedFavCityLocIds = TextUtil.tokenize(uppFavCityLocIdsStr, SEPARATOR);
        }
        int tokenedFavCityLocIdsLth = 0;
        if (tokenedFavCityLocIds != null) {
            tokenedFavCityLocIdsLth = tokenedFavCityLocIds.length;
        }
        int loopCount = FAVORITE_CITY_MAX_COUNT;
        if (tokenedFavCityLocIdsLth < FAVORITE_CITY_MAX_COUNT) {
            loopCount = tokenedFavCityLocIdsLth;
        }
        for (int i = 0; i < loopCount; i++) {
            favCityLocIds[i] = tokenedFavCityLocIds[i];
        }
    }
    synchronized public String[] getValidFavoriteCityLocationIds() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getValidFavoriteCityLocationIds]start.");
            Log.printDebug("[DataMgr.getValidFavoriteCityLocationIds]Cached valid favorite city location ids : "
                            + validFavCityLocIds);
        }
        if (validFavCityLocIds == null) {
            if (favCityLocIds != null) {
                Vector tempValidFacCityLocIdVec = new Vector();
                for (int i = 0; i < favCityLocIds.length; i++) {
                    if (favCityLocIds[i] != null && favCityLocIds[i].length() > 0) {
                        tempValidFacCityLocIdVec.addElement(favCityLocIds[i]);
                    }
                }
                int tempValidFacCityLocIdVecSize = tempValidFacCityLocIdVec.size();
                validFavCityLocIds = new String[tempValidFacCityLocIdVecSize];
                validFavCityLocIds = (String[]) tempValidFacCityLocIdVec.toArray(validFavCityLocIds);
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getValidFavoriteCityLocationIds]end : " + validFavCityLocIds);
        }
        return validFavCityLocIds;
    }
    synchronized public boolean setFavoriteCityLocationId(int reqIndex, String reqLocId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteCityLocationId]start.");
            Log.printDebug("[DataMgr.setFavoriteCityLocationId]Param - Request location id[index] : " + reqLocId+"["+reqIndex+"]");
        }
        boolean result = false;
        if (reqIndex < FAVORITE_CITY_MAX_COUNT && favCityLocIds != null) {
            String[] tempFavCityLocIds = (String[]) favCityLocIds.clone();
            tempFavCityLocIds[reqIndex] = reqLocId;
            result = putFavoriteCityLocationIdToUPP(tempFavCityLocIds);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.setFavoriteCityLocationId]Upp process result : " + result);
            }
            if (result) {
                favCityLocIds = tempFavCityLocIds;
                validFavCityLocIds = null;
                regCityLocIds = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteCityLocationId]end : " + result);
        }
        return result;
    }
    synchronized public boolean setFavoriteCityLocationIdAtVacant(String reqLocId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteCityLocationIdAtVacant]start.");
            Log.printDebug("[DataMgr.setFavoriteCityLocationIdAtVacant]Param - Request location id : " + reqLocId);
        }
        int vacantIdx = -1;
        if (favCityLocIds != null) {
            for (int i=0; i<favCityLocIds.length; i++) {
                if (favCityLocIds[i] == null || favCityLocIds[i].trim().length() == 0) {
                    vacantIdx = i;
                    break;
                }
            }
        }
        if (vacantIdx == -1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.setFavoriteCityLocationIdAtVacant]No vacant index. return false.");
            }
            return false;
        }
        boolean result = setFavoriteCityLocationId(vacantIdx, reqLocId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteCityLocationIdAtVacant]end : " + result);
        }
        return result;
    }
    synchronized public String[] getAllFavoriteCityLocationIds() {
        if (favCityLocIds == null) {
            favCityLocIds = new String[FAVORITE_CITY_MAX_COUNT];
        }
        return favCityLocIds;
    }
    public String getCurrentLanguageTag() {
        return curLanguageTag;
    }
    synchronized private boolean putFavoriteCityLocationIdToUPP(String[] reqFavCityLocIds) {
        boolean result = false;
        if (reqFavCityLocIds != null) {
            sb.setLength(0);
            int reqFavCityLocIdsLth = reqFavCityLocIds.length;
            for (int i = 0; i < reqFavCityLocIdsLth; i++) {
                String favCityLocId = reqFavCityLocIds[i];
                if (favCityLocId == null) {
                    favCityLocId = "";
                }
                sb.append(favCityLocId);
                if (i != reqFavCityLocIdsLth - 1) {
                    sb.append(SEPARATOR);
                }
            }
            String favCityLocIdStr = sb.toString();
            result = pProxy.setFavoriteCityLocationIdString(favCityLocIdStr);
        }
        return result;
    }
    synchronized public String[] getRegisteredCityLocationIds() throws RPManagerException {
        if (regCityLocIds == null) {
            Vector tempRegCityLocIdVec = new Vector();
            String htLocId = getHomeTownLocationId();
            if (htLocId != null) {
                tempRegCityLocIdVec.addElement(htLocId);
            }
            String[] vfavCityLocIds = getValidFavoriteCityLocationIds();
            if (vfavCityLocIds != null) {
                for (int i = 0; i < vfavCityLocIds.length; i++) {
                    if (vfavCityLocIds[i] != null) {
                        tempRegCityLocIdVec.addElement(vfavCityLocIds[i]);
                    }
                }
            }
            int tempRegCityLocIdVecSz = tempRegCityLocIdVec.size();
            regCityLocIds = new String[tempRegCityLocIdVecSz];
            regCityLocIds = (String[]) tempRegCityLocIdVec.toArray(regCityLocIds);
        }
        return regCityLocIds;
    }
    public boolean isDuplicatedLocId(String reqLocId) {
        if (reqLocId == null) {
            return false;
        }
        String tempHometownLocId = null;
        try{
            tempHometownLocId = getHomeTownLocationId();
        }catch(Exception ignore) {
            ignore.printStackTrace();
        }
        if (tempHometownLocId != null && reqLocId.equals(tempHometownLocId)) {
            return true;
        }
        String[] tempRegedCityLocIdList = null;
        try{
            tempRegedCityLocIdList = getRegisteredCityLocationIds();
        }catch(Exception ignore) {
            ignore.printStackTrace();
        }
        if (tempRegedCityLocIdList != null) {
            for (int i=0; i<tempRegedCityLocIdList.length; i++) {
                if (tempRegedCityLocIdList[i] != null && reqLocId.equals(tempRegedCityLocIdList[i])) {
                    return true;
                }
            }
        }
        return false;
    }

    public void updatedPreferenceLanguage() {
        String language = pProxy.getLanguage();
        if (language != null && language.equals(Definitions.LANGUAGE_FRENCH)) {
            curLanguageTag = LANGUAGE_TAG_FR;
        } else {
            curLanguageTag = LANGUAGE_TAG_EN;
        }
    }
    /*********************************************************************************
     * RP-related
     *********************************************************************************/
    synchronized public CityInfo getCityInfoByLocationId(String regLocId) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getCityInfoByLocationId]start.");
            Log.printDebug("[DataMgr.getCityInfoByLocationId]Param  - Request location id : [" + regLocId + "]");
        }
        if (regLocId == null || regLocId.trim().length() == 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getCityInfoByLocationId]Request location id is null. return null.");
            }
            return null;
        }
        String lType = getCurrentLanguageTag();
        if (lType == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getCityInfoByLocationId]Language type is null. return null.");
            }
            return null;
        }
        LatestForecast latestForecast = getLatestForecast(regLocId);
        if (latestForecast == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getCityInfoByLocationId]Latest forecast is null. return null.");
            }
            return null;
        }
        CityInfo cityInfo = latestForecast.getCityInfo();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getCityInfoByLocationId]City info : [" + cityInfo + "]");
        }
        return cityInfo;
    }
    synchronized public LatestForecast getLatestForecast(String regLocId) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getLatestForecast]start.");
            Log.printDebug("[DataMgr.getLatestForecast]Param  - Request location id : [" + regLocId + "]");
        }
        if (regLocId == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getLatestForecast]Request location id is null. return null.");
            }
            return null;
        }
        String lType = getCurrentLanguageTag();
        if (lType == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getLatestForecast]Language type is null. return null.");
            }
            return null;
        }
//        String latestForecastHashKey = regLocId + SEPARATOR + lType;
//        LatestForecast lForecast = (LatestForecast) latestForecastHash.get(latestForecastHashKey);
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getLatestForecast]Cached latest forecast hash key : "+latestForecastHashKey);
//            Log.printDebug("[DataMgr.getLatestForecast]Cached latest forecast : "+lForecast);
//        }
//        if (lForecast != null) {
//            Date nextUpdateDate = lForecast.getNextUpdateDate();
//            Date curDate = new Date();
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getLatestForecast]Next update date : "+nextUpdateDate);
//                Log.printDebug("[DataMgr.getLatestForecast]Current date : "+curDate);
//            }
//            if (nextUpdateDate != null && curDate.before(nextUpdateDate)) {
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DataMgr.getLatestForecast]Latest forecast is valid.");
//                }
//                return lForecast;
//            }
//            latestForecastHash.remove(latestForecastHashKey);
//            lForecast = null;
//        }
        // Invalid latest forecast. so gets latest forecast from server.
//        lForecast = getRPManager().getLatestForecast(lType, regLocId);
        LatestForecast lForecast = getRPManager().getLatestForecast(lType, regLocId);
//        if (lForecast != null) {
//            latestForecastHash.put(latestForecastHashKey, lForecast);
//        }
        return lForecast;
    }
    synchronized public void loadIconCodeImage(String reqIconCode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.loadIconCodeImage]start.");
            Log.printDebug("[DataMgr.loadIconCodeImage]Param  - Request icon code : [" + reqIconCode + "]");
        }
        if (reqIconCode == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.loadIconCodeImage]Request icon code is null. return null.");
            }
            return;
        }
        Image imgIconCode = ImageManager.getInstance().getImage(reqIconCode, ImageManager.TYPE_TEMPORARY);
        if (imgIconCode != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.loadIconCodeImage]Cached icon code image is valid. end");
            }
            return;
        }
    }
    synchronized public CityInfo[] searchCityInfoByInputText(String inputTxt) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.searchCityInfoByInputText]Start");
            Log.printDebug("[DataMgr.searchCityInfoByInputText]Param - inputTxt : ["+inputTxt+"]");
        }
        if (inputTxt == null) {
            return null;
        }
        String curLangTag = getCurrentLanguageTag();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.searchCityInfoByInputText]Current language tag : ["+curLangTag+"]");
        }
        // check city info in hashtable
        CityInfo cityInfoPostalCode = (CityInfo) cityInfoHashByPostalCode.get(inputTxt);
        if (cityInfoPostalCode == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.searchCityInfoByInputText]Cached city info by postal code is null.");
            }
            String reqPostalCode = inputTxt.toUpperCase();
            try{
                cityInfoPostalCode = getRPManager().getCityInfoByPostalCode(curLangTag, reqPostalCode);
            }catch(RPManagerException ignore) {
            }
            if (cityInfoPostalCode != null) {
                cityInfoHashByPostalCode.put(inputTxt, cityInfoPostalCode);
            }
        }
        // Check city info by city list
        String cityListKeyCanadaCities = HASH_KEY_HEADER_CITY_LIST + curLangTag + RPManager.CITY_LIST_TAG_CANADA;
        CityInfo[] cityInfoCanadaCities = (CityInfo[]) cityListHash.get(cityListKeyCanadaCities);
        if (cityInfoCanadaCities == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.searchCityInfoByInputText]Cached city list(Canada) is null.");
            }
            cityInfoCanadaCities = getRPManager().getCityList(curLangTag, RPManager.CITY_LIST_TAG_CANADA);
            if (cityInfoCanadaCities != null) {
                cityListHash.put(cityListKeyCanadaCities, cityInfoCanadaCities);
            }
        }
        String cityListKeyOtherCities = HASH_KEY_HEADER_CITY_LIST + curLangTag + RPManager.CITY_LIST_TAG_OTHER_CITY;
        CityInfo[] cityInfoOtherCities = (CityInfo[]) cityListHash.get(cityListKeyOtherCities);
        if (cityInfoOtherCities == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.searchCityInfoByInputText]Cached city list(Other city) is null.");
            }
            cityInfoOtherCities = getRPManager().getCityList(curLangTag, RPManager.CITY_LIST_TAG_OTHER_CITY);
            if (cityInfoOtherCities != null) {
                cityListHash.put(cityListKeyOtherCities, cityInfoOtherCities);
            }
        }
        // Search cities
        ArrayList searchedCityInfo = new ArrayList();
        if (cityInfoPostalCode != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.searchCityInfoByInputText]Searched the city info by postal code.");
            }
            searchedCityInfo.add(cityInfoPostalCode);
        }
        String upperCaseInputTxt = inputTxt.toUpperCase();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.searchCityInfoByInputText]Upper case input text : ["+upperCaseInputTxt+"]");
        }
        ArrayList vStartsWth = new ArrayList();
        ArrayList vIndexOf = new ArrayList();
        if (cityInfoCanadaCities != null) {
            for (int i = 0; i < cityInfoCanadaCities.length; i++) {
                if (cityInfoCanadaCities[i] == null) {
                    continue;
                }
                String dName = cityInfoCanadaCities[i].getCityName();
                if (dName == null) {
                    continue;
                }
                dName=Util.replaceAccentString(dName);
                dName = dName.toUpperCase();
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DataMgr.searchCityInfoByInputText]Compare canada city name - "+i+" : ["+dName+"]");
//                }
                int index = dName.indexOf(upperCaseInputTxt);
                if (index == 0) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.searchCityInfoByInputText]Searched the city info by canada city - "+dName);
//                    }
                    vStartsWth.add(cityInfoCanadaCities[i]);
                } else if (index > 0) {
                    vIndexOf.add(cityInfoCanadaCities[i]);
                }
            }
        }
        if (cityInfoOtherCities != null) {
            for (int i = 0; i < cityInfoOtherCities.length; i++) {
                if (cityInfoOtherCities[i] == null) {
                    continue;
                }
                String dName = cityInfoOtherCities[i].getCityName();
                if (dName == null) {
                    continue;
                }
                dName = dName.toUpperCase();
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DataMgr.searchCityInfoByInputText]Compare other city name - "+i+" : ["+dName+"]");
//                }
                int index = dName.indexOf(upperCaseInputTxt);
                if (index == 0) {
                    vStartsWth.add(cityInfoOtherCities[i]);
                }else if (index > 0) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.searchCityInfoByInputText]Searched the city info by other city - "+dName);
//                    }
//                    searchedCityInfo.addElement(cityInfoOtherCities[i]);
                    vIndexOf.add(cityInfoOtherCities[i]);
                }
            }
        }
        Collections.sort(vStartsWth, COMP_CITY_NAME);
        Collections.sort(vIndexOf, COMP_CITY_NAME);
        searchedCityInfo.addAll(vStartsWth);
        searchedCityInfo.addAll(vIndexOf);
//        Collections.sort(searchedCityInfo);
        int searchedCityInfoSz = searchedCityInfo.size();
        CityInfo[] searchedCInfos = new CityInfo[searchedCityInfoSz];
        searchedCInfos = (CityInfo[]) searchedCityInfo.toArray(searchedCInfos);
        return searchedCInfos;
    }
    /*********************************************************************************
     * DataUpdateListener-implemented
     *********************************************************************************/
    /**
     * Called when data has been removed.
     * @param key key of data.
     */
    public void dataRemoved(String key){
    }
    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value){
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataUpdated]key : " + key);
            Log.printDebug("[DataMgr.dataUpdated]old : " + old);
            Log.printDebug("[DataMgr.dataUpdated]value : " + value); 
        }
        if (key == null) {
            return;
        }
        if (key.equals(ITV_SERVER_DATA_IB)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated iTv server data.");
            }
            File fileITvServerData = (File) value;
            if (fileITvServerData != null) {
                byte[] bytesITvServerData = Util.getByteArrayFromFile(fileITvServerData);
                parseITvServerData(bytesITvServerData);
            }
        }
    }
}

package com.videotron.tvi.illico.itv.weather.data;

import java.awt.Image;

public class CurrentCondition {
    //Current Condition
    private String weatherDate; //Date and time in 24-hour format and in that location’s time zone. The format is the XML dateTime format: e.g. 2010-05-26T21:00:00Z
    private String weatherStation; //Weather station
    private String iconCode; //ICON Code for the icon to display
    private Image imgIconCode; //Icon code image for icon code.
    private String forecastText; //Forecast text to display
    private String temperatureC; //Current temperature in Celsius
    private String temperatureF; //Current temperature in Fahrenheit
    private String feelsLikeC; //“Feels Like” temperature (Celcius)
    private String feelsLikeF; //“Feels Like” temperature(Fahrenheit)
    private String windSpeedKm; //Wind speed in km/h
    private String windSpeedMi; //Wind speed in miles/h
    private String windDirection; //Wind direction
    private String humidity; //Humidity percentage
    
    public String getWeatherDate() {
        return weatherDate;
    }
    public void setWeatherDate(String weatherDate) {
        this.weatherDate = weatherDate;
    }
    public String getWeatherStation() {
        return weatherStation;
    }
    public void setWeatherStation(String weatherStation) {
        this.weatherStation = weatherStation;
    }
    
    public String getIconCode() {
        return iconCode;
    }
    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }
    
    public Image getIconCodeImage() {
        return imgIconCode;
    }
    public void setIconCodeImage(Image imgIconCode) {
        this.imgIconCode = imgIconCode;
    }
    
    public String getForecastText() {
        return forecastText;
    }
    public void setForecastText(String forecastText) {
        this.forecastText = forecastText;
    }
    
    public String getTemperatureC() {
        return temperatureC;
    }
    public void setTemperatureC(String temperatureC) {
        this.temperatureC = temperatureC;
    }
    
    public String getTemperatureF() {
        return temperatureF;
    }
    public void setTemperatureF(String temperatureF) {
        this.temperatureF = temperatureF;
    }
    
    public String getFeelsLikeC() {
        return feelsLikeC;
    }
    public void setFeelsLikeC(String feelsLikeC) {
        this.feelsLikeC = feelsLikeC;
    }
    
    public String getFeelsLikeF() {
        return feelsLikeF;
    }
    public void setFeelsLikeF(String feelsLikeF) {
        this.feelsLikeF = feelsLikeF;
    }
    
    public String getWindSpeedKm() {
        return windSpeedKm;
    }
    public void setWindSpeedKm(String windSpeedKm) {
        this.windSpeedKm = windSpeedKm;
    }
    
    public String getWindSpeedMi() {
        return windSpeedMi;
    }
    public void setWindSpeedMi(String windSpeedMi) {
        this.windSpeedMi = windSpeedMi;
    }
    
    public String getWindDirection() {
        return windDirection;
    }
    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }
    
    public String getHumidity() {
        return humidity;
    }
    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }
}

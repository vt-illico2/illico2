package com.videotron.tvi.illico.itv.weather.controller;

public class RPManagerException extends Exception {
    private static final long serialVersionUID = -8484665764851228691L;
    public RPManagerException(String code) {
        super(code);
    }
}

package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.weather.comp.popup.Popup;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class PopupNotification extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
//    private DataCenter dataCenter = DataCenter.getInstance();
    
    private Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    private Color C003003003 = new Color(3, 3, 3);
    private Color C035035035 = new Color(35, 35, 35);
    private Color C229229229 = new Color(229, 229, 229);
    private Color C255204000 = new Color(255, 204, 0);
//    private Color C255234160 = new Color(255, 234, 160);
    private Font F18 = FontResource.BLENDER.getFont(18, true);
//    private Font F19 = FontResource.BLENDER.getFont(19, true);
    private Font F24 = FontResource.BLENDER.getFont(24, true);
    private FontMetrics FM18 = FontResource.getFontMetrics(F18);
//    private FontMetrics FM19 = FontResource.getFontMetrics(F19);
    private FontMetrics FM24 = FontResource.getFontMetrics(F24);
    
    private Image imgPopRedHaloBig;
    private Image imgPopRedHaloSmall;
    private Image imgPopBGBig;
//    private Image imgPopBGSmall;
    private Image imgPopGap;
//    private Image imgPopShadow;
    private Image imgPopIcon;
    private Image imgButtonB;
    private Image imgButtonF;
    
    public static final int POPUP_SIZE_BIG = 0;
    public static final int POPUP_SIZE_SMALL = 1;
    private static final int[] POPUP_SIZE_VALID_W = {370, 318};
    private static final int[] POPUP_SIZE_MAX_LINE = {6, 4};
    private static final int[][] POPUP_SIZE_Y_POS = {{288, 280, 272, 264, 256, 248}, {266, 258, 250, 242}};
    private static final int CONTENT_GAP = 19;
    private int curPopSize = POPUP_SIZE_BIG;
    
    public static final int POPUP_TYPE_QUESTION = 0;
    public static final int POPUP_TYPE_INFORMATION = 1;
    private int curPopType = POPUP_TYPE_INFORMATION;
    
    private final int buttonGap = 157;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_OK = 0;
    public static final int BUTTON_CANCEL = 1;
    private int curButton;
    
    private String title;
//    private String subTitle;
    private String content;
    
    public PopupNotification() {
        imgPopRedHaloBig = ImageManager.getInstance().getImage("pop_red_538.png", ImageManager.TYPE_TEMPORARY);
        imgPopRedHaloSmall = ImageManager.getInstance().getImage("pop_red_478.png", ImageManager.TYPE_TEMPORARY);
        imgPopBGBig = ImageManager.getInstance().getImage("pop_high_402.png", ImageManager.TYPE_TEMPORARY);
//        imgPopBGSmall = ImageManager.getInstance().getImage("pop_high_350.png", ImageManager.TYPE_TEMPORARY);
        imgPopGap = ImageManager.getInstance().getImage("pop_gap_379.png", ImageManager.TYPE_TEMPORARY);
//        imgPopShadow = ImageManager.getInstance().getImage("pop_sha.png", ImageManager.TYPE_TEMPORARY);
        imgPopIcon = ImageManager.getInstance().getImage("icon_noti_red.png", ImageManager.TYPE_TEMPORARY);
        imgButtonB = ImageManager.getInstance().getImage("05_focus_dim.png", ImageManager.TYPE_TEMPORARY);
        imgButtonF = ImageManager.getInstance().getImage("05_focus.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    
    protected void disposePopup() {
        imgPopRedHaloBig = null;
        imgPopRedHaloSmall = null;
        imgPopBGBig = null;
//        imgPopBGSmall = null;
        imgPopGap = null;
//        imgPopShadow = null;
        imgPopIcon = null;
        imgButtonB = null;
        imgButtonF = null;
    }

    protected void startPopup() {
        curButton = BUTTON_OK;        
    }

    protected void stopPopup() {
        curPopSize = POPUP_SIZE_BIG;
        curPopType = POPUP_TYPE_INFORMATION;
    }
    
    public void paint(Graphics g) {
        // Backgound
        g.setColor(DVB012012012204);
        g.fillRect(0, 0, 960, 540);
        switch(curPopSize) {
            case POPUP_SIZE_BIG:
                if (imgPopRedHaloBig != null) {
                    g.drawImage(imgPopRedHaloBig, 216, 64, this);
                }
//                if (imgPopShadow != null) {
//                    g.drawImage(imgPopShadow, 280, 415, 404, 79, this);
//                }
                g.setColor(C035035035);
                g.fillRect(280, 143, 402, 276);
                if (imgPopBGBig != null) {
                    g.drawImage(imgPopBGBig, 280, 143, this);
                }
                if (imgPopGap != null) {
                    g.drawImage(imgPopGap, 292, 181, this);
                }
                break;
            case POPUP_SIZE_SMALL:
                if (imgPopRedHaloSmall != null) {
                    g.drawImage(imgPopRedHaloSmall, 242, 79, this);
                }
//                if (imgPopShadow != null) {
//                    g.drawImage(imgPopShadow, 306, 364, 350, 79, this);
//                }
                g.setColor(C035035035);
                g.fillRect(306, 143, 350, 224);
//                if (imgPopBGSmall != null) {
//                    g.drawImage(imgPopBGSmall, 306, 143, this);
//                }
                if (imgPopGap != null) {
                    g.drawImage(imgPopGap, 285, 181, this);
                }
                break;
        }
        //Icon
        final int titleGap = 6;
        int titleAreaWth = 0;
        int imgPopIconWth = 0;
        if (imgPopIcon !=null) {
            imgPopIconWth = imgPopIcon.getWidth(this);
            titleAreaWth += imgPopIconWth;
            titleAreaWth += titleGap;
        }
        //Title
        int titleWth = 0;
        if (title != null) {
            titleWth = FM24.stringWidth(title);
            titleAreaWth += titleWth;
        }
        int xPos = 480 - (titleAreaWth/2);
        if (imgPopIcon !=null) {
            g.drawImage(imgPopIcon, xPos, 153, this) ;
            xPos += imgPopIconWth;
            xPos += titleGap;
        }
        if (titleWth > 0) {
            g.setFont(F24);
            g.setColor(C255204000);
            g.drawString(title, xPos, 169);
        }
        
        //Button
        g.setFont(F18);
        g.setColor(C003003003);
        int yPos = 318;
        if (curPopSize == POPUP_SIZE_BIG) {
            yPos = 368;
        }
        switch(curPopType) {
            case POPUP_TYPE_QUESTION:
                for (int i=0; i<BUTTON_COUNT; i++) {
                    Image imgButton = null;
                    if (i == curButton) {
                        imgButton = imgButtonF;
                    } else {
                        imgButton = imgButtonB;
                    }
                    if (imgButton != null) {
                        g.drawImage(imgButton, 325+(i*buttonGap), yPos, this);
                    }
                    String txtButton = null;
                    if (i == BUTTON_OK) {
                        txtButton = DataCenter.getInstance().getString("TxtWeather.Confirm");
                    } else if (i == BUTTON_CANCEL){
                        txtButton = DataCenter.getInstance().getString("TxtWeather.Back");
                    }
                    if (txtButton != null) {
                        int txtButtonWth = FM18.stringWidth(txtButton);
                        g.drawString(txtButton, 403+(i*buttonGap)-(txtButtonWth/2), yPos +22);
                    }
                }
                break;
            case POPUP_TYPE_INFORMATION:
                g.drawImage(imgButtonF, 403, yPos, this);
                String txtOK = "OK";
                int txtOkWth = FM18.stringWidth(txtOK);
                g.drawString(txtOK, 480 -(txtOkWth/2), yPos+22);
                break;
        }
        
        //SubTitle
//        if (subTitle != null) {
//            int subTitleWth = FM19.stringWidth(subTitle);
//            if (subTitleWth > POPUP_SIZE_VALID_W[curPopSize]) {
//                subTitle = TextUtil.shorten(subTitle, FM19, POPUP_SIZE_VALID_W[curPopSize]);
//                subTitleWth = POPUP_SIZE_VALID_W[curPopSize];
//            }
//            g.setFont(F19);
//            g.setColor(C255234160);
//            g.drawString(subTitle, 480 - (subTitleWth/2), 214);
//        }
        
        //Content
        if (content != null) {
            g.setFont(F18);
            g.setColor(C229229229);
            String[] conts = TextUtil.split(content, FM18, POPUP_SIZE_VALID_W[curPopSize], POPUP_SIZE_MAX_LINE[curPopSize]);
            int contsCnt = conts.length;
            int yCont = POPUP_SIZE_Y_POS[curPopSize][contsCnt-1] - 16;
//            if (subTitle == null) {
//                yCont -= 16;
//            }
            for (int i=0; i<contsCnt; i++){
                String cont = conts[i];
                int contWth = FM18.stringWidth(cont);
                g.drawString(cont, 480 - (contWth/2), yCont+(i*CONTENT_GAP));
            }
        }
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case HRcEvent.VK_LEFT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_OK) {
                    return true;
                }
                curButton = BUTTON_OK;
                repaint();
                return true;
            case HRcEvent.VK_RIGHT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_CANCEL) {
                    return true;
                }
                curButton = BUTTON_CANCEL;
                repaint();
                return true;
            case HRcEvent.VK_ENTER:
                if (curButton == BUTTON_OK) {
                    pListener.popupOK(new PopupEvent(this));
                } else {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case OCRcEvent.VK_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    
    public int getCurrentButton() {
        return curButton;
    }
    
    public void setPopupSize(int popupSize) {
        curPopSize = popupSize;
    }
    
    public void setPopupType (int popupType) {
        curPopType = popupType;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
}

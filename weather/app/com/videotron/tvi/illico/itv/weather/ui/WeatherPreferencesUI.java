package com.videotron.tvi.illico.itv.weather.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.comp.OptionItem;
import com.videotron.tvi.illico.itv.weather.comp.PopupOption;
import com.videotron.tvi.illico.itv.weather.comp.PopupSearch;
import com.videotron.tvi.illico.itv.weather.comp.PreferencePopupAddon;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.RPManagerException;
import com.videotron.tvi.illico.itv.weather.controller.SceneManager;
import com.videotron.tvi.illico.itv.weather.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.gui.WeatherPreferencesRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class WeatherPreferencesUI extends BaseUI {
    private static final long serialVersionUID = 7441667616008799529L;
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Footer footer;
    private PreferencePopupAddon prefPopAddon;
    private boolean isActivatePreferencePopupAddon;
    /*********************************************************************************
     * Popup-related
     *********************************************************************************/
    private PopupSearch popSearch;
    private PopupAdapter popAdaptSearch;
    
    private final String OPTION_ITEM_KEY_CELSIUS = "OPTION_ITEM_KEY_CELSIUS";
    private final String OPTION_ITEM_KEY_FAHRENHEIT = "OPTION_ITEM_KEY_FAHRENHEIT";
    private final String OPTION_ITEM_KEY_REPLACE = "OPTION_ITEM_KEY_REPLACE";
    private final String OPTION_ITEM_KEY_REMOVE = "OPTION_ITEM_KEY_REMOVE";
    private final String OPTION_ITEM_KEY_SET_AS_HOMETOWN = "OPTION_ITEM_KEY_SET_AS_HOMETOWN";
    private final String OPTION_ITEM_KEY_CANCEL = "OPTION_ITEM_KEY_CANCEL";
    
    private OptionItem[] optionItemsUnitPreference;
    private OptionItem optionItemCelsius;
    private OptionItem optionItemFahrenheit;
    private OptionItem optionItemReplaceHometown;
    private OptionItem optionItemReplaceFavCity;
    private OptionItem optionItemRemoveFavCity;
    private OptionItem optionItemSetAsHometown;
    private OptionItem optionItemCancel;
    private PopupOption popOption;
    private PopupAdapter popAdaptUnitPreference;
    
    public static final int FAVORITE_CITY_GAP = 37;
    public static final int AREA_TEMPERATURE_UNIT = 0;
    public static final int AREA_HOMETOWN = 1;
    public static final int AREA_FAVORITE_CITY = 2;
    private int curArea;
    
    private int curTemperatureUnit;
    private CityInfo hometownCityInfo;
    private CityInfo[] favCityInfos;
    private int curFavCityInfoIdx;
    
    private Image imgInput = DataCenter.getInstance().getImage("input_210_high.png");
    
    protected void initScene() {
        renderer = new WeatherPreferencesRenderer();
        setRenderer(renderer);
        if (prefPopAddon == null) {
            prefPopAddon = new PreferencePopupAddon() {
                private static final long serialVersionUID = 3238177139457992982L;
                public void renderer(Graphics g) {
                    int labelX = 0;
                    int labelY = 0;
                    int valueY = 0;
                    String label = null;
                    String value = null;
                    switch(curArea) {
                        case AREA_TEMPERATURE_UNIT:
                            label = DataCenter.getInstance().getString("TxtWeather.Units_Preference");
                            labelX = 54;
                            labelY = 162;
                            if (curTemperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                                value = "°C";
                            } else if(curTemperatureUnit == DataManager.TEMPERATURE_UNIT_ID_F) {
                                value = "°F";
                            }
                            valueY = 162;
                            break;
                        case AREA_HOMETOWN:
                            label = DataCenter.getInstance().getString("TxtWeather.Hometown");
                            labelX = 81;
                            labelY = 238;
                            value = hometownCityInfo.getDisplayName();
                            valueY = 238;
                            break;
                        case AREA_FAVORITE_CITY:
                            label = DataCenter.getInstance().getString("TxtWeather.Favorite_City");
                            label += " " + (curFavCityInfoIdx + 1);
                            labelX = 81;
                            labelY = 274 + (curFavCityInfoIdx * 37);
                            value = favCityInfos[curFavCityInfoIdx].getDisplayName();
                            valueY = 275 + (curFavCityInfoIdx * FAVORITE_CITY_GAP);
                            break;
                    }
                    g.setFont(Rs.F17);
                    g.setColor(Rs.C255255255);
                    g.drawString(label, labelX, labelY);
                    g.drawImage(imgInput, 339, valueY - 21, this);
                    g.setFont(Rs.F18);
                    g.drawString(value, 352, valueY);
                }
            };
            prefPopAddon.setBounds(0, 0, 960, 540);
        }
        
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (popOption == null) {
            popOption = new PopupOption();
            popOption.setBounds(0, 0, 960, 540);
        }
        if (optionItemCelsius == null) {
            optionItemCelsius = new OptionItem();
            optionItemCelsius.setPrimaryKey(OPTION_ITEM_KEY_CELSIUS);
            optionItemCelsius.setDisplayText("ºC");
        }
        if (optionItemFahrenheit == null) {
            optionItemFahrenheit = new OptionItem();
            optionItemFahrenheit.setPrimaryKey(OPTION_ITEM_KEY_FAHRENHEIT);
            optionItemFahrenheit.setDisplayText("ºF");
        }
        if (optionItemsUnitPreference == null) {
            optionItemsUnitPreference = new OptionItem[]{optionItemCelsius, optionItemFahrenheit};
        }
        optionItemReplaceHometown = new OptionItem();
        optionItemReplaceHometown.setPrimaryKey(OPTION_ITEM_KEY_REPLACE);
//        optionItemRemoveHometown = new OptionItem();
//        optionItemRemoveHometown.setPrimaryKey(OPTION_ITEM_KEY_REMOVE);
        optionItemReplaceFavCity = new OptionItem();
        optionItemReplaceFavCity.setPrimaryKey(OPTION_ITEM_KEY_REPLACE);
        optionItemRemoveFavCity = new OptionItem();
        optionItemRemoveFavCity.setPrimaryKey(OPTION_ITEM_KEY_REMOVE);
        optionItemSetAsHometown = new OptionItem();
        optionItemSetAsHometown.setPrimaryKey(OPTION_ITEM_KEY_SET_AS_HOMETOWN);
        optionItemCancel = new OptionItem();
        optionItemCancel.setPrimaryKey(OPTION_ITEM_KEY_CANCEL);
    }
    protected void disposeScene() {
        if (popSearch != null) {
            popSearch.dispose();
            popSearch = null;
        }
        popAdaptSearch = null;
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (popOption != null) {
            popOption.dispose();
            popOption = null;
        }
        renderer = null; 
    }
    protected void startScene(boolean resetScene, String[] params) {
        CommunicationManager.getInstance().requestResizeScreen(0, 0, 0, 0);
        prepare();
        if (footer != null) {
            footer.reset();
            footer.addButton(PreferenceService.BTN_BACK, "TxtWeather.Back");
        }
        if (resetScene) {
            //Temperature unit
            curTemperatureUnit = dMgr.getTemperatureUnit();
            CommunicationManager.getInstance().requestShowLoadingAnimation(480, 270);
            isRequested = false;
            new Thread(Rs.APP_NAME + ".Preference") {
                public void run() {
                    //Hometown
                    try{
                        String hometownLocationId = dMgr.getHomeTownLocationId();
                        hometownCityInfo = dMgr.getCityInfoByLocationId(hometownLocationId);
                    }catch(RPManagerException e) {
                        CommunicationManager.getInstance().requestHideLoadingAnimation();
                        CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                        return;
                    }
                    //Favorite cities
                    String[] favCityLocIds = dMgr.getAllFavoriteCityLocationIds();
                    int favCityLocIdsLth = favCityLocIds.length;
                    favCityInfos = new CityInfo[favCityLocIdsLth];
                    boolean existErrorData = false;
                    String exceptionMsg = null;
                    for (int i=0; i<favCityLocIdsLth; i++) {
                        if (favCityLocIds[i] != null) {
                            try{
                                favCityInfos[i] = dMgr.getCityInfoByLocationId(favCityLocIds[i]);
                            }catch(RPManagerException e) {
                                e.printStackTrace();
                                favCityInfos[i] = new CityInfo();
                                existErrorData = true;
                                exceptionMsg = e.getMessage();
                            }
                        }
                    }
                    //Index, Area
                    curFavCityInfoIdx = 0;
                    curArea = AREA_TEMPERATURE_UNIT;
                    isRequested = true;
                    CommunicationManager.getInstance().requestHideLoadingAnimation();
                    repaint();
                    if (existErrorData) {
                        CommunicationManager.getInstance().requestShowErrorMessage(exceptionMsg, null);
                        return;
                    }
                }
            }.start();
        }
    }
    protected void stopScene() {
        deactivatePreferencePopupAddon();
        if (popOption != null) {
            popOption.removePopupListener();
            popOption.stop();
            PopupController.getInstance().closePopup(popOption);
        }
        if (popSearch != null) {
            popSearch.removePopupListener();
            PopupController.getInstance().closePopup(popSearch);
        }
        closeAllPopup();
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_UP:
                switch(curArea) {
                    case AREA_TEMPERATURE_UNIT:
                        return true;
                    case AREA_HOMETOWN:
                        curArea = AREA_TEMPERATURE_UNIT;
                        break;
                    case AREA_FAVORITE_CITY:
                        if (curFavCityInfoIdx == 0) {
                            curArea = AREA_HOMETOWN;
                        } else {
                            curFavCityInfoIdx --;
                        }
                        break;
                }
                repaint();
                return true;
            case Rs.KEY_DOWN:
                switch(curArea) {
                    case AREA_TEMPERATURE_UNIT:
                        curArea = AREA_HOMETOWN;
                        break;
                    case AREA_HOMETOWN:
                        curArea = AREA_FAVORITE_CITY;
                        curFavCityInfoIdx = 0;
                        break;
                    case AREA_FAVORITE_CITY:
                        if (curFavCityInfoIdx == DataManager.FAVORITE_CITY_MAX_COUNT - 1) {
                            return true;
                        }
                        curFavCityInfoIdx ++;                            
                        break;
                }
                repaint();
                return true;
            case Rs.KEY_OK:
                keyActionOK();
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.Back");
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_WEATHER_MAIN, false, null);
                return true;
        }
        return false;
    }
    private void keyActionOK() {
        switch(curArea) {
            case AREA_TEMPERATURE_UNIT:
                new ClickingEffect(this, 5).start(339, 141, 210, 32);
                if (popOption != null) {
                    activatePreferencePopupAddon();
                    popOption.setPopupListener(getPopupAdapterPopupOption());
                    popOption.setFocusLocation(482, 141);
                    int popOptionIdx = -1;
                    if (curTemperatureUnit == DataManager.TEMPERATURE_UNIT_ID_F) {
                        popOptionIdx = 1;
                    } else {
                        popOptionIdx = 0;
                    }
                    String txtUnitsPreference = DataCenter.getInstance().getString("TxtWeather.Units_Preference_Option");
                    popOption.setPopupTitle(txtUnitsPreference);
                    popOption.setOptionItems(optionItemsUnitPreference, popOptionIdx);
                    PopupController.getInstance().openPopup(popOption);
                }
                return;
            case AREA_HOMETOWN:
                new ClickingEffect(this, 5).start(339, 217, 210, 32);
                if (hometownCityInfo == null) {
                    if (popSearch == null) {
                        popSearch = new PopupSearch();
                        popSearch.setBounds(0, 0, 960, 540);
                    }
                    popSearch.setPopupListener(getPopupAdapterSearch());
                    PopupController.getInstance().openPopup(popSearch);
                } else {
                    if (popOption == null) {
                        return;
                    }
                    activatePreferencePopupAddon();
                    popOption.setPopupListener(getPopupAdapterPopupOption());
                    popOption.setFocusLocation(482, 217);
                    resetOptionItemText();
                    String txtHometown = DataCenter.getInstance().getString("TxtWeather.Hometown");
                    popOption.setPopupTitle(txtHometown);
                    OptionItem[] optionItemsHometown = new OptionItem[]{
                            optionItemReplaceHometown, optionItemCancel
                    };
//                    OptionItem[] optionItemsHometown = new OptionItem[]{
//                            optionItemReplaceHometown, optionItemRemoveHometown, optionItemCancel
//                    };
                    popOption.setOptionItems(optionItemsHometown, 0);
                    PopupController.getInstance().openPopup(popOption);
                }
                return;
            case AREA_FAVORITE_CITY:
                new ClickingEffect(this, 5).start(339, 254 + (curFavCityInfoIdx * FAVORITE_CITY_GAP), 210, 32);
                if (favCityInfos == null) {
                    return ;
                }
                if (curFavCityInfoIdx >= favCityInfos.length) {
                    return;
                }
                if (favCityInfos[curFavCityInfoIdx] == null) {
                    if (popSearch == null) {
                        popSearch = new PopupSearch();
                        popSearch.setBounds(0, 0, 960, 540);
                    }
                    popSearch.setPopupListener(getPopupAdapterSearch());
                    PopupController.getInstance().openPopup(popSearch);
                } else {
                    if (popOption == null) {
                        return;
                    }
                    activatePreferencePopupAddon();
                    popOption.setPopupListener(getPopupAdapterPopupOption());
                    popOption.setFocusLocation(482, 254 + (curFavCityInfoIdx * FAVORITE_CITY_GAP));
                    resetOptionItemText();
                    String txtFavoriteCity = DataCenter.getInstance().getString("TxtWeather.Favorite_City");
                    popOption.setPopupTitle(txtFavoriteCity);
                    OptionItem[] optionItemsFavoriteCity = new OptionItem[]{
                            optionItemReplaceFavCity, optionItemRemoveFavCity, optionItemSetAsHometown, optionItemCancel
                    };
                    popOption.setOptionItems(optionItemsFavoriteCity, 0);
                    PopupController.getInstance().openPopup(popOption);
                }
        }
    }
    private void resetOptionItemText() {
        switch(curArea) {
            case AREA_HOMETOWN:
                if (optionItemReplaceHometown != null) {
                    String txtReplace = DataCenter.getInstance().getString("TxtWeather.Replace");
                    if (txtReplace != null) {
                        optionItemReplaceHometown.setDisplayText(txtReplace);
                    }
                }
//                if (optionItemRemoveHometown != null) {
//                    String txtRemove = DataCenter.getInstance().getString("TxtWeather.Remove");
//                    if (txtRemove != null) {
//                        optionItemRemoveHometown.setDisplayText(txtRemove);
//                    }
//                }
                break;
            case AREA_FAVORITE_CITY:
                if (optionItemReplaceFavCity != null) {
                    String txtReplace = DataCenter.getInstance().getString("TxtWeather.Replace");
                    if (txtReplace != null) {
                        optionItemReplaceFavCity.setDisplayText(txtReplace);
                    }
                }
                if (optionItemRemoveFavCity != null) {
                    String txtRemove = DataCenter.getInstance().getString("TxtWeather.Remove");
                    if (txtRemove != null) {
                        optionItemRemoveFavCity.setDisplayText(txtRemove);
                    }
                }
                if (optionItemSetAsHometown != null) {
                    String txtSetAsHometown = DataCenter.getInstance().getString("TxtWeather.Set_As_Hometown");
                    if (txtSetAsHometown != null) {
                        optionItemSetAsHometown.setDisplayText(txtSetAsHometown);
                    }
                }
                break;
        }
        if (optionItemCancel != null) {
            String txtCancel = DataCenter.getInstance().getString("TxtWeather.Cancel");
            if (txtCancel != null) {
                optionItemCancel.setDisplayText(txtCancel);
            }
        }
    }
    /*********************************************************************************
     * PopupAdapter-related
     *********************************************************************************/
    private PopupAdapter getPopupAdapterSearch() {
        if (popAdaptSearch == null) {
            popAdaptSearch = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupSearch pop = (PopupSearch)popupEvent.getPopup();
                    CityInfo selectedCityInfo = pop.getSelectedCityInfo();
                    String selectedLocId = null;
                    if (selectedCityInfo != null) {
                        selectedLocId = selectedCityInfo.getLocationId();
                    }
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                    if (selectedLocId == null) {
                        return;
                    }
                    boolean isDuplicatedLocId = dMgr.isDuplicatedLocId(selectedLocId);
                    if (isDuplicatedLocId) {
                        String txtDuplicatedCityTitle = DataCenter.getInstance().getString("TxtWeather.Duplicated_City_Title");
                        String txtDuplicatedCityCont = DataCenter.getInstance().getString("TxtWeather.Duplicated_City_Content");
                        openPopupInfo(txtDuplicatedCityTitle, txtDuplicatedCityCont);
                        return;
                    }
                    switch(curArea) {
                        case AREA_HOMETOWN:
                            boolean resSetHomeTownLocationId = dMgr.setHomeTownLocationId(selectedLocId);
                            if (resSetHomeTownLocationId) {
                                hometownCityInfo = selectedCityInfo;
                            }
                            break;
                        case AREA_FAVORITE_CITY:
                            boolean resSetFavoriteCityLocationId = dMgr.setFavoriteCityLocationId(curFavCityInfoIdx, selectedLocId);
                            if (resSetFavoriteCityLocationId) {
                                favCityInfos[curFavCityInfoIdx] = selectedCityInfo;
                            }
                            break;
                    }
                    repaint();
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupSearch pop = (PopupSearch)popupEvent.getPopup();
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
            };
        }
        return popAdaptSearch;
    }
    public PopupAdapter getPopupAdapterPopupOption() {
        if (popAdaptUnitPreference == null) {
            popAdaptUnitPreference = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupOption pop = (PopupOption)popupEvent.getPopup();
                    OptionItem selectedOptionItem = null;
                    if (pop != null) {
                        deactivatePreferencePopupAddon();
                        selectedOptionItem = pop.getSelectedOptionItem();
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop);
                    }
                    if (selectedOptionItem != null) {
                        if (selectedOptionItem.equals(optionItemCelsius)) {
                            changeTemperatureUnit(DataManager.TEMPERATURE_UNIT_ID_C);
                        } else if (selectedOptionItem.equals(optionItemFahrenheit)) {
                            changeTemperatureUnit(DataManager.TEMPERATURE_UNIT_ID_F);
                        } else if (selectedOptionItem.equals(optionItemReplaceHometown)) {
                            replaceHometown();
                        }
//                        else if (selectedOptionItem.equals(optionItemRemoveHometown)) {
//                            removeHometown();
//                        }
                        else if (selectedOptionItem.equals(optionItemReplaceFavCity)) {
                            replaceFavoriteCity();
                        } else if (selectedOptionItem.equals(optionItemRemoveFavCity)) {
                            removeFavoriteCity();
                        } else if (selectedOptionItem.equals(optionItemSetAsHometown)) {
                            setAsHometown();
                        } else if (selectedOptionItem.equals(optionItemCancel)) {
                            cancel();
                        } 
                    }
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupOption pop = (PopupOption)popupEvent.getPopup();
                    if (pop != null) {
                        deactivatePreferencePopupAddon();
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop);
                    }
                }
                /*********************************************************************************
                 * Option item-related
                 *********************************************************************************/
                private void changeTemperatureUnit(int reqTemperatureUnit) {
                    boolean result = dMgr.setTemperatureUnit(reqTemperatureUnit);
                    if (result) {
                        curTemperatureUnit = reqTemperatureUnit;
                        repaint();
                    }
                }
                private void replaceHometown() {
                    if (popSearch == null) {
                        popSearch = new PopupSearch();
                        popSearch.setBounds(0, 0, 960, 540);
                    }
                    popSearch.setPopupListener(getPopupAdapterSearch());
                    PopupController.getInstance().openPopup(popSearch);
                }
//                private void removeHometown() {
//                    if (hometownCityInfo != null) {
//                        boolean result = dMgr.setHomeTownLocationId("");
//                        if (result) {
//                            hometownCityInfo = null;
//                        }
//                        repaint();
//                    }
//                }
                private void replaceFavoriteCity() {
                    if (popSearch == null) {
                        popSearch = new PopupSearch();
                        popSearch.setBounds(0, 0, 960, 540);
                    }
                    popSearch.setPopupListener(getPopupAdapterSearch());
                    PopupController.getInstance().openPopup(popSearch);
                }
                private void removeFavoriteCity() {
                    if (favCityInfos != null && curFavCityInfoIdx < favCityInfos.length && favCityInfos[curFavCityInfoIdx] != null) {
                        boolean result = dMgr.setFavoriteCityLocationId(curFavCityInfoIdx, null);
                        if (result) {
                            favCityInfos[curFavCityInfoIdx] = null;
                        }
                        repaint();
                    }
                }
                private void setAsHometown() {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[WeatherPreferencesUI.setAsHometown] Called");
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[WeatherPreferencesUI.setAsHometown] favCityInfos : "+favCityInfos);
                        Log.printDebug("[WeatherPreferencesUI.setAsHometown] curFavCityInfoIdx : "+curFavCityInfoIdx);
                        if (favCityInfos != null) {
                            Log.printDebug("[WeatherPreferencesUI.setAsHometown] favCityInfos.length : "+favCityInfos.length);
                            Log.printDebug("[WeatherPreferencesUI.setAsHometown] favCityInfos[curFavCityInfoIdx] : "+favCityInfos[curFavCityInfoIdx]);
                        }
                        
                    }
                    if (favCityInfos != null && curFavCityInfoIdx < favCityInfos.length && favCityInfos[curFavCityInfoIdx] != null) {
                        String homeTownLocId = favCityInfos[curFavCityInfoIdx].getLocationId();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[WeatherPreferencesUI.setAsHometown] homeTownLocId : "+homeTownLocId);
                        }
                        boolean result = dMgr.setHomeTownLocationId(homeTownLocId);
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[WeatherPreferencesUI.setAsHometown] result : "+result);
                        }
                        if (result) {
                            hometownCityInfo = favCityInfos[curFavCityInfoIdx];
                        }
                        result = dMgr.setFavoriteCityLocationId(curFavCityInfoIdx, null);
                        if (result) {
                            favCityInfos[curFavCityInfoIdx] = null;
                        }
                        repaint();
                    }
                }
                private void cancel() {
                    repaint();
                }
            };
        }
        return popAdaptUnitPreference;
    }
    public void activatePreferencePopupAddon() {
        if (prefPopAddon != null) {
            add(prefPopAddon, 0);
            isActivatePreferencePopupAddon = true;
        }
    }
    public void deactivatePreferencePopupAddon() {
        if (prefPopAddon != null) {
            isActivatePreferencePopupAddon = false;
            remove(prefPopAddon);
        }
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public CityInfo[] getFavoriteCityInfos() {
        return favCityInfos;
    }
    public CityInfo getHometownCityInfo() {
        return hometownCityInfo;
    }
    public int getTemperatureUnit() {
        return curTemperatureUnit;
    }
    public int getCurrentArea() {
        return curArea;
    }
    public int getFavoriteCityInfoIndex() {
        return curFavCityInfoIdx;
    }
    public boolean isActivatePreferencePopupAddon() {
        return isActivatePreferencePopupAddon;
    }
}

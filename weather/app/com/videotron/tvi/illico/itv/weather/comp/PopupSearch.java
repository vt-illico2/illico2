package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.Util;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.comp.popup.Popup;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.controller.PreferenceProxy;
import com.videotron.tvi.illico.itv.weather.controller.RPManagerException;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.keyboard.AlphanumericKeyboard;
import com.videotron.tvi.illico.keyboard.Keyboard;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class PopupSearch extends Popup implements KeyboardListener {
    private static final long serialVersionUID = 5342026025879871164L;
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    public static final int STATUS_READY = 0;
    public static final int STATUS_REQUESTING = 1;
    public static final int STATUS_REQUESTED_VALID = 2;
    public static final int STATUS_REQUESTED_INVALID = 3;
    private int curStatus;

    private static final int SEARCHING_TYPE_AUTO = 0;
    private static final int SEARCHING_TYPE_OK = 1;

    private PopupNotification popNotification;
    private PopupAdapter popAdaptDefault;
    private AlphanumericKeyboard keyboard;

//    private Image imgBgShadow;
    private Image imgBg;
    private Image imgBgR1;
    private Image imgBgR2;
    private Image imgBgL1;
//    private Image imgBgL2;
    private Image imgTitleBg;
    private Image imgSearchBar;
    private Image imgCursor;
//    private Image imgListBg;
    private Image imgListFocus;
    private Image imgListLine;
//    private Image imgListShadowT;
//    private Image imgListShadowB;
    private Image imgListArrowT;
    private Image imgListArrowB;

    private static final Font F17 = FontResource.BLENDER.getFont(17, true);
    private static final Font F18 = FontResource.BLENDER.getFont(18, true);
    private static final Font F20 = FontResource.BLENDER.getFont(20, true);
    private static final Font F24 = FontResource.BLENDER.getFont(24, true);
    private static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    private static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    private static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    private static final FontMetrics FM24 = FontResource.getFontMetrics(F24);

    private static final Color C000000000 = new Color(0, 0, 0);
    private static final Color C046046045 = new Color(46, 46, 45);
    private static final Color C055055055 = new Color(55, 55, 55);
    private static final Color C184184184 = new Color(184, 184, 184);
    private static final Color C202174097 = new Color(202, 174, 97);
    private static final Color C230230230 = new Color(230, 230, 230);
    private static final Color C255203001 = new Color(255, 203, 1);
    private static final Color C255255255 = new Color(255, 255, 255);
    private static final Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    private static final Color DVB102102102179 = new DVBColor(102, 102, 102, 179);
    private static final Color DVB255255255102 = new DVBColor(255, 255, 255, 102);

    private SearchTimerAction timerAction;

    private static final int AREA_KEYBOARD = 0;
    private static final int AREA_LIST = 1;
    private int curArea;
    /*********************************************************************************
     * Keyboard-related
     *********************************************************************************/
    private static final int MAX_TXT_WIDTH = 218;
    private static final int MIN_TXT_COUNT = 1;
    private int curCursorIdx;
    private String tempText;
    private String curText;
    private String searchedText;
    /*********************************************************************************
     * List-related
     *********************************************************************************/
    private static final int LIST_MAX_WTH = 270;
    private static final int LIST_LINE_CONT = 13;
    private CityInfo[] searchedCInfos;
    private int totalListCount;
    private int listIdx;
    private int listStartIdx;
    private int listDispCnt;
    private static int curSearchingType;
    /*********************************************************************************
     * Footer-related
     *********************************************************************************/
    private Footer footer;

    public PopupSearch() {
//        imgBgShadow = ImageManager.getInstance().getImage("03_bg_sha.png", ImageManager.TYPE_TEMPORARY);
        imgBg = ImageManager.getInstance().getImage("03_bg_02.png", ImageManager.TYPE_TEMPORARY);
        imgBgR1 = ImageManager.getInstance().getImage("03_bg_03.png", ImageManager.TYPE_TEMPORARY);
        imgBgR2 = ImageManager.getInstance().getImage("03_bg_02.png", ImageManager.TYPE_TEMPORARY);
        imgBgL1 = ImageManager.getInstance().getImage("03_bg_01.png", ImageManager.TYPE_TEMPORARY);
//        imgBgL2 = ImageManager.getInstance().getImage("03_bg_l.png", ImageManager.TYPE_TEMPORARY);
        imgTitleBg = ImageManager.getInstance().getImage("03_title.png", ImageManager.TYPE_TEMPORARY);
        imgSearchBar = ImageManager.getInstance().getImage("04_key_search.png", ImageManager.TYPE_TEMPORARY);
        imgCursor = ImageManager.getInstance().getImage("04_key_cursor.png", ImageManager.TYPE_TEMPORARY);
//        imgListBg = ImageManager.getInstance().getImage("03_bg01_high.png", ImageManager.TYPE_TEMPORARY);
        imgListFocus = ImageManager.getInstance().getImage("03_focus02.png", ImageManager.TYPE_TEMPORARY);
        imgListLine = ImageManager.getInstance().getImage("03_line_03.png", ImageManager.TYPE_TEMPORARY);
//        imgListShadowT = ImageManager.getInstance().getImage("03_result_sha_top.png", ImageManager.TYPE_TEMPORARY);
//        imgListShadowB = ImageManager.getInstance().getImage("03_result_sha_d.png", ImageManager.TYPE_TEMPORARY);
        imgListArrowT = ImageManager.getInstance().getImage("02_ars_t.png", ImageManager.TYPE_TEMPORARY);
        imgListArrowB = ImageManager.getInstance().getImage("02_ars_b.png", ImageManager.TYPE_TEMPORARY);
        if (footer == null) {
            footer = new Footer();
            footer.setBounds(440, 489, 460, 24);
            add(footer);
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (keyboard == null) {
            KeyboardImpl  keyboardImpl = new KeyboardImpl();
            keyboard = (AlphanumericKeyboard) Keyboard.create(keyboardImpl);
            keyboard.setLocation(344, 48);
            keyboard.setMaxChars(20);
            keyboard.setFocus(0);
            add(keyboard);
        }
        if (timerAction == null) {
            timerAction = new SearchTimerAction();
            timerAction.init();
        }
        if (popAdaptDefault == null) {
            popAdaptDefault = new PopupAdapter() {
                public void popupOK(PopupEvent pe) {
                    PopupController.getInstance().closePopup(popNotification);
                }
            };
        }
        if (popNotification == null) {
            popNotification = new PopupNotification();
            popNotification.setBounds(0, 0, 960, 540);
        }
    }
    protected void disposePopup() {
        if (popNotification != null) {
            popNotification.dispose();
            popNotification = null;
        }
        popAdaptDefault=null;
        if (keyboard != null) {
            keyboard.removeKeyboardListener(this);
            keyboard = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }
    }
    protected void startPopup() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.startPopup]keyboard : "+keyboard);
        }
        curArea = AREA_KEYBOARD;
        setFooter(curArea);
        resetPopupSearch(true);
    }
    protected void stopPopup() {
        CommunicationManager.getInstance().requestHideLoadingAnimation();
        if (keyboard != null) {
            keyboard.removeKeyboardListener(this);
        }
        if (timerAction != null) {
            timerAction.stop();
        }
        curText = null;
        searchedText = null;
        searchedCInfos = null;
    }
    private void resetPopupSearch(boolean isInitKeyboard) {
        if (keyboard != null) {
            keyboard.registerKeyboardListener(this);
            tempText = DataCenter.getInstance().getString("TxtWeather.Temp_View_Text");
//            if (isInitKeyboard) {
//                curText = DataCenter.getInstance().getString("TxtWeather.Temp_View_Text");
//            } else {
//                curText = "";
//            }
            curText = "";
            if (curText != null) {
                keyboard.setTempViewText(true);
                keyboard.setText(curText);
            }
            String curLang = PreferenceProxy.getInstance().getLanguage();
            Keyboard.setLanguage(curLang);
            keyboard.prepare();
        }
        curArea = AREA_KEYBOARD;
        setFooter(AREA_KEYBOARD);
        curCursorIdx = 0;
        totalListCount= 0 ;
        listIdx = 0 ;
        listStartIdx = 0 ;
        listDispCnt = 0 ;
        curStatus = STATUS_READY;
    }

    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyCodes.SEARCH:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case OCRcEvent.VK_EXIT:
                if (footer != null) {
                    footer.setUpdateBackgroundBeforeStart(false);
                    footer.clickAnimation("TxtWeather.Close");
                }
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        switch(curArea) {
            case AREA_KEYBOARD:
                if (keyboard != null) {
                    if (keyCode == KeyCodes.COLOR_C) {
                        if (footer != null) {
                            footer.setUpdateBackgroundBeforeStart(false);
                            footer.clickAnimation("TxtWeather.Erase");
                        }
                    }
                    return keyboard.handleKey(keyCode);
                }
                return true;
            case AREA_LIST:
                return keyActionList(keyCode);
        }
        return false;
    }
    private boolean keyActionList(int keyCode) {
        switch(keyCode) {
              case KeyEvent.VK_0:
              case KeyEvent.VK_1:
              case KeyEvent.VK_2:
              case KeyEvent.VK_3:
              case KeyEvent.VK_4:
              case KeyEvent.VK_5:
              case KeyEvent.VK_6:
              case KeyEvent.VK_7:
              case KeyEvent.VK_8:
              case KeyEvent.VK_9:
              case OCRcEvent.VK_CHANNEL_UP:
              case OCRcEvent.VK_CHANNEL_DOWN:
                  return true;
            case KeyEvent.VK_UP:
                if (totalListCount <= 0) {
                    return true;
                }
                if (listIdx == 0) {
                    return true;
                }
                listIdx  --;
                setDisplayCityInfo();
                repaint();
                return true;
            case KeyEvent.VK_DOWN:
                if (totalListCount <= 0) {
                    return true;
                }
                if (listIdx == totalListCount-1) {
                    return true;
                }
                listIdx  ++;
                setDisplayCityInfo();
                repaint();
                return true;
            case KeyEvent.VK_LEFT:
                if (keyboard != null) {
                    keyboard.setPreviousFocus();
                    curArea = AREA_KEYBOARD;
                    setFooter(AREA_KEYBOARD);
                    repaint();
                }
                return true;
            case KeyEvent.VK_RIGHT:
                return true;
            case KeyEvent.VK_ENTER:
                if (totalListCount <= 0) {
                    return true;
                }
                if (pListener != null) {
                    pListener.popupOK(new PopupEvent(this));
                }
                return true;
            case KeyEvent.VK_PAGE_UP:
                if (footer != null) {
                    footer.setUpdateBackgroundBeforeStart(false);
                    footer.clickAnimation("TxtWeather.Page_Up_Down");
                }
                if (totalListCount <= 0 || totalListCount <= LIST_LINE_CONT) {
                    return true;
                }
                if (listIdx - LIST_LINE_CONT  < 0) {
                    listIdx = 0;
                } else {
                    listIdx -= LIST_LINE_CONT;
                }
                setDisplayCityInfo();
                repaint();
                return true;
            case KeyEvent.VK_PAGE_DOWN:
                if (footer != null) {
                    footer.setUpdateBackgroundBeforeStart(false);
                    footer.clickAnimation("TxtWeather.Page_Up_Down");
                }
                if (totalListCount <= 0 || totalListCount <= LIST_LINE_CONT) {
                    return true;
                }
                if (listIdx + LIST_LINE_CONT >= totalListCount) {
                    listIdx = totalListCount-1;
                } else {
                    listIdx += LIST_LINE_CONT;
                }
                setDisplayCityInfo();
                repaint();
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
        //Background
        g.setColor(DVB012012012204);
        g.fillRect(0, 0, 960, 540);
//        if (imgBgShadow != null) {
//            g.drawImage(imgBgShadow, 343, 479, 661, 61, this);
//        }
        if (imgBg != null) {
            g.drawImage(imgBg, 592, 77, 368, 402, this);
        }
        if (imgBgR1 != null) {
            g.drawImage(imgBgR1, 898, 77, this);
        }
        if (imgBgR2 != null) {
            g.drawImage(imgBgR2, 910, 77, 94, 402, this);
        }
        if (imgBgL1 != null) {
            g.drawImage(imgBgL1, 343, 77, this);
        }
//        if (imgBgL2 != null) {
//            g.drawImage(imgBgL2, 220, 168, this);
//        }
        if (imgTitleBg != null) {
            g.drawImage(imgTitleBg, 343, 37, this);
        }
        if (imgSearchBar != null) {
            g.drawImage(imgSearchBar, 351, 77, this);
        }
        //Txt
        String txtCitySearch = DataCenter.getInstance().getString("TxtWeather.City_Search");
        if (txtCitySearch != null) {
            g.setFont(F20);
            g.setColor(C046046045);
            g.drawString(txtCitySearch, 379, 65);
            g.setColor(C202174097);
            g.drawString(txtCitySearch, 378, 64);
        }
        //Keyboard
        int cursorPos = 0;
        String txtInput = null;
        if (keyboard.getTempViewText()) {
//            if (curText != null) {
//                g.setFont(F17);
//                g.setColor(DVB255255255102);
//                txtInput = TextUtil.shorten(curText, FM17, MAX_TXT_WIDTH);
//            }
            if (tempText != null) {
                g.setFont(F17);
                g.setColor(DVB255255255102);
                txtInput = TextUtil.shorten(tempText, FM17, MAX_TXT_WIDTH);
            }
        } else {
            g.setFont(F20);
            g.setColor(C255255255);
            txtInput = curText;
            String tempTxtInput = txtInput.substring(0, curCursorIdx);
            cursorPos = FM20.stringWidth(tempTxtInput);
        }
        if (txtInput != null) {
            g.drawString(txtInput, 363, 101);
        }
        if (imgCursor != null) {
            g.drawImage(imgCursor, 362 + cursorPos, 104, this);
        }
        //List
        g.setColor(C055055055);
        g.fillRect(602, 77, 296, 392);
//        if (imgListBg != null) {
//            g.drawImage(imgListBg, 602, 77, this);
//        }
//        //Footer
//        int txtCloseWth = 0;
//        String txtClose = DataCenter.getInstance().getString("TxtWeather.Close");
//        if (txtClose != null) {
//            txtCloseWth = FM17.stringWidth(txtClose);
//            g.setFont(F17);
//            g.setColor(C255255255);
//            g.drawString(txtClose, 898-txtCloseWth, 503);
//        }
//        if (imgIconExit != null) {
//            int imgIconExitWth = imgIconExit.getWidth(this);
//            g.drawImage(imgIconExit, 898-txtCloseWth - 4- imgIconExitWth, 489, this);
//        }
        //Status
        if (curStatus == STATUS_REQUESTED_VALID) {
            g.setColor(DVB102102102179);
            g.fillRect(602, 77, 296, 27);
            String txtCityFound = DataCenter.getInstance().getString("TxtWeather.City_Found");
            if (txtCityFound != null) {
                g.setFont(F17);
                g.setColor(C230230230);
                g.drawString(txtCityFound+" ("+totalListCount+")", 613, 95);
            }
            if (totalListCount <= 0) {
                String txtNoResult = DataCenter.getInstance().getString("TxtWeather.No_Result");
                if (txtNoResult != null) {
                    g.setFont(F18);
                    g.setColor(C184184184);
                    String[] txtNoResults = TextUtil.split(txtNoResult, FM18, 230, '|');
                    for(int i=0; i<txtNoResults.length; i++) {
                        if (txtNoResults[i] != null) {
                            GraphicUtil.drawStringCenter(g, txtNoResults[i], 750, 275 + (i*18), FM18);
                        }
                    }
                }
            } else {
                if (imgListLine != null) {
                    for (int i = 0; i < LIST_LINE_CONT; i++) {
                        if (i != LIST_LINE_CONT - 1) {
                            g.drawImage(imgListLine, 612, 132 + (i * 28), this);
                        }
                    }
                }
                int searchedTextLth = 0;
                if (searchedText != null) {
                    searchedTextLth = searchedText.length();
                }
                for (int i = 0; i < listDispCnt; i++) {
                    int idx = listStartIdx + i;
                    CityInfo cInfo = searchedCInfos[idx];
                    if (cInfo == null) {
                        continue;
                    }
                    String dName = cInfo.getDisplayName();
                    if (dName == null) {
                        continue;
                    }
                    dName = TextUtil.shorten(dName, FM17, LIST_MAX_WTH);
                    if (curArea == AREA_LIST && idx == listIdx) {
                        g.drawImage(imgListFocus, 602, 105 + (i*28), this);
                        g.setColor(C000000000);
                        g.drawString(dName, 614, 123 + (i*28));
                    } else {
                        String upperSearchedTxt = searchedText.toUpperCase();
                        String temp=Util.replaceAccentString(dName);
                        temp=temp.toUpperCase();
                        int tempIdx = temp.indexOf(upperSearchedTxt);
                        if (tempIdx < 0) {
                            g.setColor(C255255255);
                            g.drawString(dName, 614, 123 + (i*28));
                        } else {
                            int drawX = 614;
                            g.setColor(C255255255);
                            String frontDName = dName.substring(0, tempIdx);
                            g.drawString(frontDName, drawX, 123 + (i*28));
                            drawX += FM17.stringWidth(frontDName);
                            g.setColor(C255203001);
                            String matchedName = dName.substring(tempIdx, tempIdx+searchedTextLth);
                            g.drawString(matchedName, drawX, 123 + (i*28));
                            drawX += FM17.stringWidth(matchedName);
                            g.setColor(C255255255);
                            String rearName = dName.substring(tempIdx+searchedTextLth);
                            g.drawString(rearName, drawX, 123 + (i*28));
                        }
                    }
                }
                // Arrow
                if (totalListCount > LIST_LINE_CONT && curArea == AREA_LIST) {
                    if (listStartIdx != 0) {
//                        if (imgListShadowT != null) {
//                            g.drawImage(imgListShadowT, 602, 104, this);
//                        }
                        if (imgListArrowT != null) {
                            g.drawImage(imgListArrowT, 739, 62, this);
                        }
                    }
                    if (listStartIdx + listDispCnt < totalListCount) {
//                        if (imgListShadowB != null) {
//                            g.drawImage(imgListShadowB, 602, 439, this);
//                        }
                        if (imgListArrowB != null) {
                            g.drawImage(imgListArrowB, 739, 467, this);
                        }
                    }
                }
            }
        }
        switch(curStatus) {
            case STATUS_READY:
                String txtTip = DataCenter.getInstance().getString("TxtWeather.Tip");
                if (txtTip != null) {
                    g.setFont(F24);
                    g.setColor(C255255255);
                    GraphicUtil.drawStringCenter(g, txtTip, 750, 244, FM24);
                }
                String txtNotification = DataCenter.getInstance().getString("TxtWeather.Tip_Desc");
                if (txtNotification != null) {
                    g.setFont(F18);
                    g.setColor(C184184184);
                    String[] txtNotifications = TextUtil.split(txtNotification, FM18, 230, '|');
                    for(int i=0; i<txtNotifications.length; i++) {
                        if (txtNotifications[i] != null) {
                            GraphicUtil.drawStringCenter(g, txtNotifications[i], 750, 275 + (i*18), FM18);
                        }
                    }
                }
                break;
            case STATUS_REQUESTING:
                break;
            case STATUS_REQUESTED_INVALID:
                String txtError = DataCenter.getInstance().getString("TxtWeather.Error");
                if (txtError != null) {
                    g.setFont(F18);
                    g.setColor(C184184184);
                    String[] txtErrors = TextUtil.split(txtError, FM18, 230, 2);
                    for(int i=0; i<txtErrors.length; i++) {
                        if (txtErrors[i] != null) {
                            GraphicUtil.drawStringCenter(g, txtErrors[i], 750, 275 + (i*18), FM18);
                        }
                    }
                }
                break;
        }
        super.paint(g);
    }
    private void setDisplayCityInfo() {
        int tempListStartIdx = -1;
        int tempListDispCnt = -1;
        int searchedCInfosLth = -1;
        if (searchedCInfos != null) {
            searchedCInfosLth = searchedCInfos.length;
        }
        if (searchedCInfosLth <= LIST_LINE_CONT) {
            tempListStartIdx = 0;
            tempListDispCnt = searchedCInfosLth;
        } else {
            int halfDispCnt = LIST_LINE_CONT / 2;
            if (listIdx < halfDispCnt) {
                tempListStartIdx = 0;
            } else if (listIdx >= searchedCInfosLth - halfDispCnt) {
                tempListStartIdx = searchedCInfosLth - LIST_LINE_CONT;
            } else {
                tempListStartIdx = listIdx - halfDispCnt;
            }
            tempListDispCnt = LIST_LINE_CONT;
        }
        if (listStartIdx != tempListStartIdx || listDispCnt != tempListDispCnt) {
            listStartIdx = tempListStartIdx;
            listDispCnt = tempListDispCnt;
        }
    }
    private void searchCityInfo(String inputTxt, int searchingType) {
        curStatus = STATUS_REQUESTING;
        curSearchingType = searchingType;
        CommunicationManager.getInstance().requestShowLoadingAnimation(749, 271);
        new Thread(Rs.APP_NAME + ".popupSearch") {
            public void run() {
                try{
                    searchedCInfos = DataManager.getInstance().searchCityInfoByInputText(curText);
//                    if (!CommunicationManager.getInstance().isLoadingAnimationService()) {
//                        return;
//                    }
                    if (searchedCInfos != null) {
                        totalListCount = searchedCInfos.length;
                    } else {
                        totalListCount = 0;
                    }
                    if (totalListCount == 0) {
                        listIdx = 0;
                        switch(curSearchingType) {
                            case SEARCHING_TYPE_AUTO:
                                break;
                            case SEARCHING_TYPE_OK:
                                if (keyboard != null) {
                                    keyboard.setFocus(AlphanumericKeyboard.INDEX_DONE);
                                }
                                break;
                        }
                    } else {
                        listIdx = 0;
                        setDisplayCityInfo();
                        switch(curSearchingType) {
                            case SEARCHING_TYPE_AUTO:
                                break;
                            case SEARCHING_TYPE_OK:
                                curArea = AREA_LIST;
                                setFooter(AREA_LIST);
                                break;
                        }
                    }
                    searchedText = curText;
                    curStatus = STATUS_REQUESTED_VALID;
                }catch(RPManagerException e) {
                    e.printStackTrace();
                    searchedCInfos = null;
                    curStatus = STATUS_REQUESTED_INVALID;
                    CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                } finally {
                    CommunicationManager.getInstance().requestHideLoadingAnimation();
                    repaint();
                }
            }
        }.start();
    }
    /*********************************************************************************
     * KeyboardListener-implemented
     *********************************************************************************/
    public CityInfo getSelectedCityInfo() {
        if (searchedCInfos == null || listIdx >=totalListCount) {
            return null;
        }
        return searchedCInfos[listIdx];
    }
    /*********************************************************************************
     * KeyboardListener-implemented
     *********************************************************************************/
    public void cursorMoved(int position) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.cursorMoved]position : "+position);
        }
        if (position >= 0) {
            curCursorIdx = position;
        }
        resetTimerAction();
        repaint();
    }
    public void focusOut(int direction) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.focusOut]direction : "+direction);
        }
        if (timerAction != null) {
            timerAction.stop();
        }
        switch(direction) {
            case KeyEvent.VK_RIGHT:
                if (totalListCount > 0) {
                    curArea = AREA_LIST;
                    setFooter(AREA_LIST);
                    repaint();
                    return;
                }
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT:
                if (keyboard != null) {
                    keyboard.setPreviousFocus();
                    resetTimerAction();
                }
                break;
        }
    }
    public void inputCanceled(String text) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.inputCanceled]text : "+text);
        }
        if (timerAction != null) {
            timerAction.stop();
        }
        if (pListener != null) {
            pListener.popupCancel(new PopupEvent(this));
        }
    }
    public void inputEnded(String text) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.inputEnded]text : " + text);
        }
        if (timerAction != null) {
            timerAction.stop();
        }
        if (keyboard != null && keyboard.getTempViewText()) {
            keyboard.setFocus(AlphanumericKeyboard.INDEX_DONE);
            return;
        }
        if (text == null || text.length() < MIN_TXT_COUNT) {
            if (keyboard != null) {
                keyboard.setFocus(AlphanumericKeyboard.INDEX_DONE);
            }
            return;
        }
        if (text.equalsIgnoreCase(searchedText)) {
            if (totalListCount > 0) {
                listIdx = 0;
                curArea = AREA_LIST;
                setFooter(AREA_LIST);
                if (keyboard != null) {
                    keyboard.setFocus(AlphanumericKeyboard.INDEX_FOCUS_LOST);
                }
            } else {
                if (keyboard != null) {
                    keyboard.setFocus(AlphanumericKeyboard.INDEX_DONE);
                }
            }
            repaint();
            return;
        }
        searchCityInfo(text, SEARCHING_TYPE_OK);
    }
    public void modeChanged(int mode) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.modeChanged]mode : "+mode);
        }
    }
    public void textChanged(String ext) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.textChanged]ext : "+ext);
        }
        curText = ext;
        if (curText != null) {
            int curTextLth = curText.length();
            if (Log.DEBUG_ON) {
                Log.printDebug("[PopupSearch.textChanged]curTextLth : "+curTextLth);
            }
            if (curTextLth <= 0) {
                resetPopupSearch(true);
            } else {
                String firstTxt = curText.substring(0,1);
                String otherTxt = curText.substring(1);
                curText = firstTxt.toUpperCase() + otherTxt;
            }
        }
        resetTimerAction();
        repaint();
    }
    public void inputCleared() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.inputCleared]Start.");
        }
        resetPopupSearch(false);
    }
    private void resetTimerAction() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.resetTimerAction]start.");
            Log.printDebug("[PopupSearch.resetTimerAction]Current text : "+curText);
        }
        if (curText == null || keyboard.getTempViewText() || (searchedText != null && curText.equals(searchedText))) {
            if (timerAction != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[PopupSearch.resetTimerAction]Timer action stoped.");
                }
                timerAction.stop();
            }
            return;
        }
        int curTextLth = curText.length();
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopupSearch.textChanged]curTextLth : "+curTextLth);
        }
        if (timerAction != null) {
            if (curTextLth > MIN_TXT_COUNT) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[PopupSearch.resetTimerAction]Timer action started.");
                }
                timerAction.start();
            }else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[PopupSearch.resetTimerAction]Timer action stoped.");
                }
                timerAction.stop();
            }
        }
    }
    private void setFooter(int reqArea) {
        switch(reqArea) {
            case AREA_KEYBOARD:
                if (footer != null) {
                    footer.reset();
                    footer.addButton(PreferenceService.BTN_C, "TxtWeather.Erase");
                    footer.addButton(PreferenceService.BTN_EXIT, "TxtWeather.Close");
                }
                break;
            case AREA_LIST:
                if (footer != null) {
                    footer.reset();
                    if (totalListCount > LIST_LINE_CONT) {
                        footer.addButton(PreferenceService.BTN_PAGE, "TxtWeather.Page_Up_Down");
                    }
                    footer.addButton(PreferenceService.BTN_EXIT, "TxtWeather.Close");
                }
                break;
        }
    }
//    private void openPopupInfo(String cont) {
//        if (popNotification == null) {
//            return;
//        }
//        popNotification.setPopupListener(popAdaptDefault);
//        popNotification.setPopupSize(PopupNotification.POPUP_SIZE_SMALL);
//        popNotification.setPopupType(PopupNotification.POPUP_TYPE_INFORMATION);
//        popNotification.setContent(cont);
//        PopupController.getInstance().openPopup(popNotification);
//    }
//    private void closePopup() {
//        if (popNotification != null) {
//            popNotification.removePopupListener();
//            PopupController.getInstance().closePopup(popNotification);
//        }
//    }
    /*****************************************************************
     * methods - TVTimer-related
     *****************************************************************/
    class SearchTimerAction implements TVTimerWentOffListener{
        private TVTimerSpec searchTimer;
        public void init() {
            if (searchTimer == null) {
                searchTimer = new TVTimerSpec();
                searchTimer.setDelayTime(2000L);
                searchTimer.setRepeat(false);
                searchTimer.addTVTimerWentOffListener(this);
            }
        }
        public void dispose() {
            stop();
            if (searchTimer != null) {
                searchTimer.removeTVTimerWentOffListener(this);
                searchTimer = null;
            }
        }
        public void start() {
            stop();
            if (searchTimer != null) {
                try{
                    TVTimer.getTimer().scheduleTimerSpec(searchTimer);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        public void stop() {
            if (searchTimer != null) {
                try{
                    TVTimer.getTimer().deschedule(searchTimer);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        public void timerWentOff(TVTimerWentOffEvent event) {
            searchCityInfo(curText, SEARCHING_TYPE_AUTO);
        }
    }
    class KeyboardImpl implements KeyboardOption {
        public String getDefaultText() throws RemoteException {
            return "";
        }
        public char getEchoChars() throws RemoteException {
            return 0;
        }
        public String getInputPattern() throws RemoteException {
            return null;
        }
        public int getMaxChars() throws RemoteException {
            return MAX_TXT_WIDTH;
        }
        public short getMode() throws RemoteException {
            return KeyboardOption.MODE_EMBEDDED;
        }
        public Point getPosition() throws RemoteException {
            return null;
        }
        public boolean getTempDefaultText() throws RemoteException {
            return false;
        }
        public String getTitle() throws RemoteException {
            return null;
        }
        public int getType() throws RemoteException {
            return TYPE_ALPHANUMERIC;
        }
        public boolean isClosedWithMaxInput() throws RemoteException {
            return false;
        }
        public int getAlphanumericType() throws RemoteException {
            return -1;
        }
        public boolean isKeyboardChangedDeactivate() throws RemoteException {
            return true;
        }
    }
}

package com.videotron.tvi.illico.itv.weather.data;

import java.util.Vector;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.itv.weather.comp.SAXHandlerAdapter;

public class CityInfoSearchHandler extends SAXHandlerAdapter {
    private Vector cInfoVector;
    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("CITIES")) {
            cInfoVector = new Vector();
        } else if (valueXMLTag.equalsIgnoreCase("CITY")) {
            CityInfo cInfo = getCityInfo(attr);
            if (cInfo != null) {
                cInfoVector.addElement(cInfo);
            }
        }
    }
    public void endElement(String valueXMLTag) {
        if (valueXMLTag != null && valueXMLTag.equalsIgnoreCase("CITIES")) {
            if (cInfoVector != null) {
                int cInfoVectorSz = cInfoVector.size();
                CityInfo[] cInfos = new CityInfo[cInfoVectorSz];
                cInfos = (CityInfo[])cInfoVector.toArray(cInfos);
                setResultObject(cInfos);
            }
        }
    }
    private CityInfo getCityInfo(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CityInfo cInfo = new CityInfo();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("NAME")) {
                cInfo.setCityName(lValue);
            } else if (lName.equalsIgnoreCase("PROV")) {
                cInfo.setProvinceName(lValue);
            } else if (lName.equalsIgnoreCase("COUNTRY")) {
                cInfo.setCountryName(lValue);
                if (lValue != null) {
                    if (lValue.equalsIgnoreCase("Canada")) {
                        cInfo.setCityType(CityInfo.CITY_TYPE_CANADA);
                    } else {
                        cInfo.setCityType(CityInfo.CITY_TYPE_OTHER_CITY);
                    }
                }
            } else if (lName.equalsIgnoreCase("LOCATIONID")) {
                cInfo.setLocationId(lValue);
            }
        }
        return cInfo;
    }
}

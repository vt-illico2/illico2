package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CurrentCondition;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class FavoriteCityWidget extends Component{
    private static final long serialVersionUID = -1420328045902368321L;
    
    private Color c000019022 = new Color(1, 19, 22);
    private Color c094094094 = new Color(94, 94, 94);
    private Color c255255255 = new Color(255, 255, 255);
    private Color dc181180180204 = new DVBColor(181, 180, 180, 204);
    private Font f17 = FontResource.BLENDER.getFont(17, true);
    private Font f18 = FontResource.BLENDER.getFont(18, true);
    private Font f19 = FontResource.BLENDER.getFont(19, true);
    private Font f30 = FontResource.BLENDER.getFont(30, true);
    private FontMetrics fm17 = FontResource.getFontMetrics(f17);
    private FontMetrics fm19 = FontResource.getFontMetrics(f19);
    private FontMetrics fm30 = FontResource.getFontMetrics(f30);
    
    private Image imgFocus;
    private Image imgLeft;
    private Image imgRight;
    private Image imgTempC;
    private Image imgTempF;
    
    private static final int IMAGE_WIDTH = 176;
    private static final int IMAGE_HEIGHT = 105;
    public static final int BANNER_TYPE_NORMAL = 0;
    public static final int BANNER_TYPE_VACANT = 1;
    public static final int BANNER_TYPE_OPTION = 2;
    
    private LatestForecast lf;
    private Image imgBG;
    private boolean isFirstIdx;
    private boolean isLastIdx;
    private boolean isSelectable;
    private boolean isSelected;
    private int bannerType;
    
    private int tempUnit;
    private String displayName;
    private String curTemperature;
    private Image imgIconCode;
    public FavoriteCityWidget() {
        tempUnit = DataManager.getInstance().getTemperatureUnit();
        imgFocus = ImageManager.getInstance().getImage("00_banner_focus.png", ImageManager.TYPE_TEMPORARY);
        imgLeft = ImageManager.getInstance().getImage("02_ars_l.png", ImageManager.TYPE_TEMPORARY);
        imgRight = ImageManager.getInstance().getImage("02_ars_r.png", ImageManager.TYPE_TEMPORARY);
        imgTempC = ImageManager.getInstance().getImage("11_c.png", ImageManager.TYPE_TEMPORARY);
        imgTempF = ImageManager.getInstance().getImage("11_f.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public FavoriteCityWidget(LatestForecast lf) {
        this();
        this.lf = lf;
        if (lf != null) {
            CityInfo cInfo = lf.getCityInfo();
            if (cInfo != null) {
                displayName = cInfo.getDisplayName();
            }
            CurrentCondition cc = lf.getCurrentCondition();
            if (cc != null) {
                imgIconCode = cc.getIconCodeImage();
                if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                    curTemperature = cc.getTemperatureC();
                } else if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_F){
                    curTemperature = cc.getTemperatureF();
                }
            }
        }
    }
    public void dispose() {
        imgBG = null;
        lf = null;
        imgTempF = null;
        imgTempC = null;
        imgRight = null;
        imgLeft = null;
        imgFocus = null;
        
        fm17 = null;
        fm19 = null;
        fm30 = null;
        
        f30 = null;
        f19 = null;
        f17 = null;
        
        dc181180180204 = null;
        c255255255 = null;
        c094094094 = null;
        c000019022 = null;
    }
    public void start() {
    }
    public void stop() {
    }
    public void paint(Graphics g) {
        if (bannerType == BANNER_TYPE_VACANT) {
            if (imgBG != null) {
                g.drawImage(imgBG, 374 - 353, 363 - 351, this);
            }
            String txtSetWidget = DataCenter.getInstance().getString("TxtWeather.Add_Favorite_City");
            if (txtSetWidget != null) {
                g.setFont(f17);
                g.setColor(c094094094);
                g.fillRect(387 - 353, 409 - 351, 152, 1);
                g.setColor(dc181180180204);
                g.drawString(txtSetWidget, 460 - 353 -(fm17.stringWidth(txtSetWidget)/2), 407 - 351);
            }
            return;
        }
        if (isSelected && imgFocus != null) {
            g.drawImage(imgFocus, 366 - 353, 357 - 351, this);
        }
        if (imgBG != null) {
            g.drawImage(imgBG, 374 - 353, 351 - 351, IMAGE_WIDTH, IMAGE_HEIGHT, this);
        }
        if (isSelected) {
            if (!isFirstIdx && imgLeft != null) {
                g.drawImage(imgLeft, 353 - 353, 392 - 351, this);
            }
            if (!isLastIdx && imgRight != null) {
                g.drawImage(imgRight, 554 - 353, 392 - 351, this);
            }
        }
        if (bannerType == BANNER_TYPE_OPTION) {
            String txtAddFavCity = DataCenter.getInstance().getString("TxtWeather.Add_Favorite_City");
            if (txtAddFavCity != null) {
                g.setFont(f18);
                g.setColor(c255255255);
                g.drawString(txtAddFavCity, 385 - 353, 433 - 351);
            }
            return;
        }
        g.setFont(f19);
        g.setColor(c000019022);
        if (displayName != null) {
            String shortenedDisplayName = TextUtil.shorten(displayName, fm19, 160);
            g.drawString(shortenedDisplayName, 385 - 353, 384 - 351);  
        } else {
            g.drawString("N/A", 385 - 353, 384 - 351);
        }
        if (imgIconCode != null) {
            g.drawImage(imgIconCode, 385 - 353, 387 - 351, 90, 54, this);
        }
        g.setFont(f30);
        g.setColor(c255255255);
        if (curTemperature != null) {
            g.drawString(curTemperature, 477 - 353, 424 - 351);
            Image imgTempUnit =  null;
            if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                imgTempUnit = imgTempC;
            } else if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_F){
                imgTempUnit = imgTempF;
            }
            if (imgTempUnit != null) {
                int curTemperatureWth = fm30.stringWidth(curTemperature);
                g.drawImage(imgTempUnit, 477 - 353 + curTemperatureWth + 6, 409 - 351, this);
            }
        } else {
            g.drawString("N/A", 477 - 353, 424 - 351);
        }
    }
    
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
    public void setBackgroundImage(Image imgBG) {
        this.imgBG = imgBG;
    }
    public void setBannerType(int bannerType) {
        this.bannerType = bannerType;
    }
    public void setFirstIndex(boolean isFirstIdx) {
        this.isFirstIdx = isFirstIdx;
    }
    public void setLastIndex(boolean isLastIdx) {
        this.isLastIdx = isLastIdx;
    }
    public void setSelectable(boolean isSelectable) {
        this.isSelectable = isSelectable;
    }
    public boolean isSelected() {
        return isSelected;
    }
    public boolean isFirstIndex() {
        return isFirstIdx;
    }
    public boolean isLastIndex() {
        return isLastIdx;
    }
    public boolean isSelectable() {
        return isSelectable;
    }
    public int getBannerType() {
        return bannerType;
    }
    public LatestForecast getLatestForecast() {
        return lf;
    } 
}

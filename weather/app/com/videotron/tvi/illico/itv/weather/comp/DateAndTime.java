package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Date;

import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;

public class DateAndTime extends Container implements ClockListener {
    private static final long serialVersionUID = 1L;
    
    private final Color C255255255 = new Color(255, 255, 255);
    private final Font F17 = FontResource.BLENDER.getFont(17, true);
    private final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    private Image imgClock;

    private String longDate;
    private int longDateWth;
    private int ttWth;

    public void start() {
        imgClock = ImageManager.getInstance().getImage("clock.png", ImageManager.TYPE_TEMPORARY);
        ttWth = getWidth();
        clockUpdated(null);
        Clock.getInstance().addClockListener(this);
    }
    public void stop() {
        Clock.getInstance().removeClockListener(this);
        ttWth = 0;
        longDate = null;
        imgClock = null;
    }
    public void paint(Graphics g) {
        if (longDate != null) {
            g.setFont(F17);
            g.setColor(C255255255);
            g.drawString(longDate, ttWth - longDateWth, 14);
        }
        if (imgClock != null) {
            int imgClockWth = imgClock.getWidth(this);
            g.drawImage(imgClock, ttWth - longDateWth - 4 - imgClockWth, 0, this);
        }
    }
    public void clockUpdated(Date date) {
        Formatter formatter = Formatter.getCurrent();
        if (formatter != null) {
            longDate = formatter.getLongDate();
            if (longDate != null) {
                longDateWth = FM17.stringWidth(longDate);
            }
        }
        repaint();
    }
}

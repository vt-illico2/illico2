package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.weather.comp.popup.Popup;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class PopupQuestion extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    
    private Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    private Color C003003003 = new Color(3, 3, 3);
    private Color C035035035 = new Color(35, 35, 35);
    private Color C252202004 = new Color(252, 202, 4);
    private Color C255255255 = new Color(255, 255, 255);
    private Font F18 = FontResource.BLENDER.getFont(18, true);
    private Font F24 = FontResource.BLENDER.getFont(24, true);
    private FontMetrics FM18 = FontResource.getFontMetrics(F18);
    private FontMetrics FM24 = FontResource.getFontMetrics(F24);
    
//    private Image imgPopBgMid;
//    private Image imgPopBgBot;
//    private Image imgPopBgTop;
//    private Image imgPopBgShadow;
//    private Image imgPopBgHigh;
    private Image imgPopBgGap;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    
    private static final int CONTENT_GAP = 20;    
    private static final int TITLE_VALID_WIDTH = 330;
    private static final int CONTENT_VALID_WIDTH = 316;
    private static final char TEXT_SEPARATOR = '|';
    private static final int BUTTON_GAP = 164;
    
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_YES = 0;
    public static final int BUTTON_NO = 1;
    private int curButton;
    
    private String title;
    private String[] contents;
    private int contentsH;
    
    public PopupQuestion() {
//        if (imgPopBgMid == null) {
//            imgPopBgMid = ImageManager.getInstance().getImage("05_pop_glow_m.png", ImageManager.TYPE_TEMPORARY);
//        }
//        if (imgPopBgBot == null) {
//            imgPopBgBot = ImageManager.getInstance().getImage("05_pop_glow_b.png", ImageManager.TYPE_TEMPORARY);
//        }
//        if (imgPopBgTop == null) {
//            imgPopBgTop = ImageManager.getInstance().getImage("05_pop_glow_t.png", ImageManager.TYPE_TEMPORARY);
//        }
//        if (imgPopBgShadow == null) {
//            imgPopBgShadow = ImageManager.getInstance().getImage("pop_sha.png", ImageManager.TYPE_TEMPORARY);
//        }
//        if (imgPopBgHigh == null) {
//            imgPopBgHigh = ImageManager.getInstance().getImage("pop_high_350.png", ImageManager.TYPE_TEMPORARY);
//        }
        if (imgPopBgGap == null) {
            imgPopBgGap = ImageManager.getInstance().getImage("pop_gap_379.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgButtonBasic == null) {
            imgButtonBasic = ImageManager.getInstance().getImage("05_focus_dim.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgButtonFocus == null) {
            imgButtonFocus = ImageManager.getInstance().getImage("05_focus.png", ImageManager.TYPE_TEMPORARY);
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void disposePopup() {
//        imgPopBgMid = null;
//        imgPopBgBot = null;
//        imgPopBgTop = null;
//        imgPopBgShadow = null;
//        imgPopBgHigh = null;
        imgPopBgGap = null;
        imgButtonBasic = null;
        imgButtonFocus = null;
    }
    protected void startPopup() {
        curButton = BUTTON_YES;        
    }
    protected void stopPopup() {
    }
    public void paint(Graphics g) {
        // Backgound
        g.setColor(DVB012012012204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopBgMid != null) {
//            g.drawImage(imgPopBgMid, 276, 193, 410, 124, this);
//        }
//        if (imgPopBgBot != null) {
//            g.drawImage(imgPopBgBot, 276, 317, this);
//        }
//        if (imgPopBgTop != null) {
//            g.drawImage(imgPopBgTop, 276, 113, this);
//        }
//        if (imgPopBgShadow != null) {
//            g.drawImage(imgPopBgShadow, 306, 364, 350, 79, this);
//        }
        g.setColor(C035035035);
        g.fillRect(306, 143, 350, 224);
//        if (imgPopBgHigh != null) {
//            g.drawImage(imgPopBgHigh, 306, 143, this);
//        }
        if (imgPopBgGap != null) {
            g.drawImage(imgPopBgGap, 285, 181, this);
        }
        //Title
        if (title != null) {
            g.setFont(F24);
            g.setColor(C252202004);
            g.drawString(title, 482 - (FM24.stringWidth(title)/2), 169);
        }
        //Content
        if (contents != null) {
            g.setFont(F18);
            g.setColor(C255255255);
            int startY = 264 - (contentsH/2);
            for (int i=0; i<contents.length ; i++) {
                String content = contents[i];
                if (content == null || content.length() == 0) {
                    continue;
                }
                int contentWth = FM18.stringWidth(content);
                g.drawString(content, 483 - (contentWth/2), startY + (i*CONTENT_GAP));
            }
        }
        
        //Button
        for (int i=0; i<BUTTON_COUNT; i++) {
            Image imgButton = null;
            if (i==curButton) {
                imgButton = imgButtonFocus;
            } else {
                imgButton = imgButtonBasic;
            }
            if (imgButton != null) {
                g.drawImage(imgButton, 321 + (i*BUTTON_GAP), 318, this);
            }
        }
        g.setFont(F18);
        g.setColor(C003003003);
        String txtYes = DataCenter.getInstance().getString("TxtWeather.Yes");
        if (txtYes != null) {
            g.drawString(txtYes, 399 - (FM18.stringWidth(txtYes)/2), 339);
        }
        String txtNo = DataCenter.getInstance().getString("TxtWeather.No");
        if (txtNo != null) {
            g.drawString(txtNo, 564 - (FM18.stringWidth(txtNo)/2), 339);
        }
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case HRcEvent.VK_LEFT:
                if (curButton == BUTTON_YES) {
                    return true;
                }
                curButton = BUTTON_YES;
                repaint();
                return true;
            case HRcEvent.VK_RIGHT:
                if (curButton == BUTTON_NO) {
                    return true;
                }
                curButton = BUTTON_NO;
                repaint();
                return true;
            case HRcEvent.VK_ENTER:
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                effect.start(322 + (curButton * BUTTON_GAP), 318, 155, 32);
                if (curButton == BUTTON_YES) {
                    pListener.popupOK(new PopupEvent(this));
                } else {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case OCRcEvent.VK_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    public int getCurrentButton() {
        return curButton;
    }
    public void setTitle(String title) {
        this.title = null;
        if (title != null) {
            this.title = TextUtil.shorten(title, FM24, TITLE_VALID_WIDTH);
        }
    }
    public void setContent(String content) {
        contents = null;
        contentsH = 0;
        if (content != null) {
            contents = TextUtil.split(content, FM18, CONTENT_VALID_WIDTH, TEXT_SEPARATOR);
            if (contents != null) {
                contentsH = contents.length * CONTENT_GAP;
            }
        }
    }
}


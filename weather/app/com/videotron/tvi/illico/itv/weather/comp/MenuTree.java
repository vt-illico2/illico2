package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class MenuTree extends Component {
    private static final long serialVersionUID = 4315440403676430625L;
    private Color C003003003 = new Color(3, 3, 3);
    private Color C046046045 = new Color(46, 46, 45);
    private Color C202174097 = new Color(202, 174, 97);
    private Color C229229229 = new Color(229, 229, 229);
    private Font F18 = FontResource.BLENDER.getFont(18, true);
    private Font F20 = FontResource.BLENDER.getFont(20, true);
    private Font F21 = FontResource.BLENDER.getFont(21, true);
    private FontMetrics FM18 = FontResource.getFontMetrics(F18);
    private FontMetrics FM21 = FontResource.getFontMetrics(F21);
    private Image imgMenuTreeFocus;
    private Image imgMenuTreeLine;
    private Image imgMenuTreeBullet;

    private static final int MENU_GAP = 34;
    /*********************************************************************************
     * MenuTree-related
     *********************************************************************************/
    private Image imgMenuTreeIcon;
    private String txtKeyMenuTreeTitle;
    /*********************************************************************************
     * Index-related
     *********************************************************************************/
    private int idx;
    private boolean isActiveFocus;
    private boolean isFocusComponent;
    /*********************************************************************************
     * MenuTreeItem-related
     *********************************************************************************/
    private MenuTreeItem[] mtItems;
    private int mtItemCount;
    /*********************************************************************************
     * MenuTreeListener-related
     *********************************************************************************/
    private MenuTreeListener lstner;

    public void init() {
        imgMenuTreeFocus = ImageManager.getInstance().getImage("00_menufocus.png", ImageManager.TYPE_TEMPORARY);
        imgMenuTreeLine = ImageManager.getInstance().getImage("00_mini_line.png", ImageManager.TYPE_TEMPORARY);
        imgMenuTreeBullet = ImageManager.getInstance().getImage("01_bullet01.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
        C003003003 = new Color(3, 3, 3);
        C046046045 = new Color(46, 46, 45);
        C202174097 = new Color(202, 174, 97);
        C229229229 = new Color(229, 229, 229);
    }
    public void dispose() {
        imgMenuTreeFocus = null;
        imgMenuTreeLine = null;
        imgMenuTreeBullet = null;
        C003003003 = null;
        C046046045 = null;
        C202174097 = null;
        C229229229 = null;
        F18 = null;
        F20 = null;
        F21 = null;
        lstner = null;
        mtItems = null;
        txtKeyMenuTreeTitle = null;
    }
    public void start(boolean reset) {
        if (reset) {
            idx = -1;
            indexChanged(0);
            isActiveFocus = false;
            AlphaEffect fadeIn = new AlphaEffect(this, 15, AlphaEffect.FADE_IN) {
                public void animationEnded(Effect effect) {
                    isActiveFocus = true;
                    repaint();
                }
            };
            fadeIn.start();
        }
    }
    public void stop() {
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case HRcEvent.VK_UP:
                if (mtItems == null || idx == 0) {
                    return true;
                }
                indexChanged(idx - 1);
                repaint();
                return true;
            case HRcEvent.VK_DOWN:
                if (mtItems == null || idx == mtItemCount - 1) {
                    return true;
                }
                indexChanged(idx + 1);
                repaint();
                return true;
            case HRcEvent.VK_LEFT:
                return true;
            case HRcEvent.VK_RIGHT:
                if (lstner != null) {
                    lstner.lostMenuTreeFocus();
                }
                return true;
            case HRcEvent.VK_ENTER:
                if (lstner != null && mtItems != null && idx < mtItemCount && mtItems[idx] != null) {
//                    ClickingEffect clickEffect = new ClickingEffect(this, 5);
//                    clickEffect.setClipBounds(0, 0, 412, 248);
//                    clickEffect.start(52, 101 + (idx * 34), 298, 42);
//                    mt.setBounds(0, 71, 412, 469);
                    lstner.selectedMenuTreeItem(mtItems[idx], new Rectangle(52, 101 + (idx * 34), 298, 42));
                }
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
//        final int y=71;
        final int y=0;
        //Menu tree icon image
        int startX = 58;
        if (imgMenuTreeIcon != null) {
            g.drawImage(imgMenuTreeIcon, startX, 76-y, this);
            startX += imgMenuTreeIcon.getWidth(this);
        }
        //Menu tree title
        if (txtKeyMenuTreeTitle!= null) {
            String mtTitle = DataCenter.getInstance().getString(txtKeyMenuTreeTitle);
            if (mtTitle != null) {
                g.setFont(F20);
                g.setColor(C046046045);
                g.drawString(mtTitle, startX+1, 95-y);
                g.setColor(C202174097);
                g.drawString(mtTitle, startX, 94-y);
            }
        }
        //Menu tree text
        if (mtItems != null) {
            //Normal
            g.setFont(F18);
            g.setColor(C229229229);
            for (int i=0; i<mtItemCount; i++) {
                if (imgMenuTreeLine != null) {
                    g.drawImage(imgMenuTreeLine, 66, 139-y + (i*MENU_GAP), this);
                }
                if (isFocusComponent && i == idx && isActiveFocus) {
                    continue;
                }
                if (mtItems[i] == null) {
                    continue;
                }
                String mtTitleTxtKey = mtItems[i].getMenuTreeTitleTextKey();
                if (mtTitleTxtKey == null) {
                    continue;
                }
                String txtMenu = DataCenter.getInstance().getString(mtTitleTxtKey);
                if (txtMenu == null) {
                    continue;
                }
                txtMenu = TextUtil.shorten(txtMenu, FM18, 236);
                if (imgMenuTreeBullet != null) {
                    g.drawImage(imgMenuTreeBullet, 72, 119-y + (i*MENU_GAP), this);
                }
                g.drawString(txtMenu, 95, 128-y+(i*MENU_GAP));
            }
            //Focused
            if (isFocusComponent && isActiveFocus) {
                if (imgMenuTreeFocus != null) {
                    g.drawImage(imgMenuTreeFocus, 48, 101 - y + (idx * MENU_GAP), this);
                }
                if (mtItems != null && idx < mtItemCount && mtItems[idx] != null) {
                    String txtMenu = null;
                    String mtTitleTxtKey = mtItems[idx].getMenuTreeTitleTextKey();
                    if (mtTitleTxtKey != null) {
                        txtMenu = DataCenter.getInstance().getString(mtTitleTxtKey);
                    }
                    if (txtMenu != null) {
                        txtMenu = TextUtil.shorten(txtMenu, FM21, 236);
                        g.setFont(F21);
                        g.setColor(C003003003);
                        g.drawString(txtMenu, 95, 128 - y + (idx * MENU_GAP));
                    }
                }
            }
        }
    }
    private void indexChanged(int reqIdx) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[MenuTree.indexChanged]Start.");
            Log.printDebug("[MenuTree.indexChanged]Param - Request index : "+reqIdx);
        }
        if (reqIdx == idx) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MenuTree.indexChanged]Request index is same.");
            }
            return;
        }
        idx = reqIdx;
        if (lstner != null && mtItems != null && idx < mtItemCount && mtItems[idx] != null) {
            lstner.changedMenuTreeItem(mtItems[idx]);
        }
    }
    public void setMenuTreeItems(MenuTreeItem[] mtItems) {
        this.mtItems = mtItems;
        if (mtItems != null) {
            mtItemCount = mtItems.length;
            indexChanged(0);
        } else {
            mtItemCount = 0;
        }
    }
    public MenuTreeItem getCurrentMenuTreeItem() {
        MenuTreeItem mtItem = null;
        if (mtItems != null && idx < mtItemCount) {
            mtItem = mtItems[idx];
        }
        return mtItem;
    }
    public void setMenuTreeListener(MenuTreeListener lstner) {
        this.lstner = lstner;
    }
    public void removeMenuTreeListener() {
        this.lstner = null;
    }
    public void setFocusComponent(boolean isFocusComponent) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[MenuTree.setFocusComponent]isFocusComponent : "+isFocusComponent);
        }
        this.isFocusComponent = isFocusComponent;
    }
    public boolean isFocusComponent() {
        return isFocusComponent;
    }
    public void setMenuTreeTitleKey(String mtTitleKey) {
        this.txtKeyMenuTreeTitle = mtTitleKey;
    }
    public int getSelectedMenuIndex() {
        return idx;
    }
    public void setMenuTreeItemFocus(MenuTreeItem target) {
        if (target == null || mtItems == null) {
            return;
        }
        for (int i=0; i<mtItems.length; i++) {
            if (mtItems[i] == null) {
                continue;
            }
            if (target.equals(mtItems[i])) {
                idx = i;
                if (lstner != null && mtItems != null && idx < mtItemCount && mtItems[idx] != null) {
                    lstner.changedMenuTreeItem(mtItems[idx]);
                }
                break;
            }
        }
    }
    public void setMenuTreeIconImage(Image imgMenuTreeIcon) {
        this.imgMenuTreeIcon = imgMenuTreeIcon;
    }
}

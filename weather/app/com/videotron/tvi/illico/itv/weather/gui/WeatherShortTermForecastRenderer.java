package com.videotron.tvi.illico.itv.weather.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CurrentCondition;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.ShortTermForecast;
import com.videotron.tvi.illico.itv.weather.ui.WeatherShortTermForecastUI;
import com.videotron.tvi.illico.util.TextUtil;

public class WeatherShortTermForecastRenderer extends BaseRenderer {
    /*********************************************************************************
     * Image-related
     *********************************************************************************/
//    private Image imgShadow;
    private Image imgWHigh;
    private Image imgCityBG;
    private Image imgArrowR;
    private Image imgArrowL;
    private Image imgBigTemperatureUnitC;
    private Image imgBigTemperatureUnitF;
    private Image imgMidTemperatureUnitC;
    private Image imgMidTemperatureUnitF;
    private Image imgSmallTemperatureUnitC;
    private Image imgSmallTemperatureUnitF;
    
    private static final int PERIOD_GAP = 214;
    
    protected void prepareChild() {
//        imgShadow = ImageManager.getInstance().getImage("11_weather_sha.png", ImageManager.TYPE_TEMPORARY);
        imgWHigh = ImageManager.getInstance().getImage("11_w_high.png", ImageManager.TYPE_TEMPORARY);
        imgCityBG = ImageManager.getInstance().getImage("11_city_bg.png", ImageManager.TYPE_TEMPORARY);
        imgArrowR = ImageManager.getInstance().getImage("02_ars_r.png", ImageManager.TYPE_TEMPORARY);
        imgArrowL = ImageManager.getInstance().getImage("02_ars_l.png", ImageManager.TYPE_TEMPORARY);
        imgBigTemperatureUnitC = ImageManager.getInstance().getImage("11_c_b.png", ImageManager.TYPE_TEMPORARY);
        imgBigTemperatureUnitF = ImageManager.getInstance().getImage("11_f_b.png", ImageManager.TYPE_TEMPORARY);
        imgMidTemperatureUnitC = ImageManager.getInstance().getImage("11_c_s.png", ImageManager.TYPE_TEMPORARY);
        imgMidTemperatureUnitF = ImageManager.getInstance().getImage("11_f_s.png", ImageManager.TYPE_TEMPORARY);
        imgSmallTemperatureUnitC = ImageManager.getInstance().getImage("11_c_xs_white.png", ImageManager.TYPE_TEMPORARY);
        imgSmallTemperatureUnitF = ImageManager.getInstance().getImage("11_f_xs_white.png", ImageManager.TYPE_TEMPORARY);
    }
    
    protected void paintRenderer(Graphics g, UIComponent c) {
        /*********************************************************************************
         * Draw background
         *********************************************************************************/
        WeatherShortTermForecastUI scene = (WeatherShortTermForecastUI)c; 
        boolean isRequested = scene.isRequested();
        if (!isRequested) {
            return;
        }
        //Message
        String msg = scene.getMessge();
        if (msg != null) {
            g.setFont(Rs.F27);
            g.setColor(Rs.C245212016);
            int reqMsgWth = Rs.FM27.stringWidth(msg);
            g.drawString(msg, 480 - (reqMsgWth / 2), 394);
            return;
        }
        //Title
        String txtShortTermForecast = DataCenter.getInstance().getString("TxtWeather.Short_Term_Forecast");
        if (txtShortTermForecast != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtShortTermForecast, 63, 97);
        }
        /*********************************************************************************
         * Draw data
         *********************************************************************************/
        LatestForecast[] latestForecasts = scene.getLatestForecasts();
        int curIdx = scene.getCurrentIndex();
        if (latestForecasts == null || latestForecasts.length == 0) {
            return;
        }
        /*********************************************************************************
         * Cities title
         *********************************************************************************/
        if (imgCityBG != null) {
            g.drawImage(imgCityBG, 0, 87, c);
        }
        int latestForecastsLth = latestForecasts.length;
        if (latestForecastsLth > 1) {
            if (imgArrowR != null) {
                g.drawImage(imgArrowR, 467, 124, c);
            }
            if (imgArrowL != null) {
                g.drawImage(imgArrowL, 134, 124, c);
            }
            g.setFont(Rs.F17);
            g.setColor(Rs.C180180180);
            int prevIdx = (latestForecastsLth+curIdx-1) % latestForecastsLth;
            String prevCityName = null;
            if (latestForecasts[prevIdx] != null) {
                CityInfo cInfo = latestForecasts[prevIdx].getCityInfo();
                if (cInfo != null) {
                    prevCityName = cInfo.getDisplayName();
                }
            }
            if (prevCityName == null) {
                prevCityName = "N/A";
            } else {
                prevCityName = TextUtil.shorten(prevCityName, Rs.FM17, 68);
            }
            g.drawString(prevCityName, 65, 142);
            
            int nextIdx = (curIdx+1) % latestForecastsLth;
            String nextCityName = null;
            if (latestForecasts[nextIdx] != null) {
                CityInfo cInfo = latestForecasts[nextIdx].getCityInfo();
                if (cInfo != null) {
                    nextCityName = cInfo.getDisplayName();
                }
            }
            if (nextCityName == null) {
                nextCityName = "N/A";
            } else {
                nextCityName = TextUtil.shorten(nextCityName, Rs.FM17, 68);
            }
            g.drawString(nextCityName, 490, 142);
        }
        String curCityName = null;
        if (latestForecasts[curIdx] != null) {
            CityInfo cInfo = latestForecasts[curIdx].getCityInfo();
            if (cInfo != null) {
                curCityName = cInfo.getDisplayName();
            }
        }
        if (curCityName == null) {
            curCityName = "N/A";
        } else {
            curCityName = TextUtil.shorten(curCityName, Rs.FM24, 300);
        }
        g.setFont(Rs.F24);
        g.setColor(Rs.C249197000);
        g.drawString(curCityName, 310 - (Rs.FM24.stringWidth(curCityName)/2), 143);
        /*********************************************************************************
         * Current weather
         *********************************************************************************/
        int temperatureUnit = DataManager.getInstance().getTemperatureUnit();
        String txtCurrentWeather = DataCenter.getInstance().getString("TxtWeather.Current_Codition");
        if (txtCurrentWeather != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtCurrentWeather, 63, 185);
        }
        String txtHumidity = DataCenter.getInstance().getString("TxtWeather.Humidity");
        String txtWind = DataCenter.getInstance().getString("TxtWeather.Wind");        
        String txtFeelsLike = DataCenter.getInstance().getString("TxtWeather.Feels_Like");
        String txtPOP = DataCenter.getInstance().getString("TxtWeather.POP");
        
        CurrentCondition cc = null;
        if (latestForecasts[curIdx] != null) {
            cc = latestForecasts[curIdx].getCurrentCondition();
        }
        if (cc == null) {
            String txtNotAvailable = DataCenter.getInstance().getString("TxtWeather.Not_Available");
            if (txtNotAvailable != null) {
                int txtNotAvailableLth = Rs.FM30.stringWidth(txtNotAvailable);
                g.setFont(Rs.F30);
                g.setColor(Rs.C255204000);
                g.drawString(txtNotAvailable, 320 - (txtNotAvailableLth/2), 244);
            }
        } else {
            //Current weather - weather icon
            Image imgIconCode = cc.getIconCodeImage();
            if (imgIconCode != null) {
                g.drawImage(imgIconCode, 64, 196, 140, 85, c);
            }
            //Current weather - temperature
            String temperature = null;
            Image imgBigTemperatureUnit = null;
            String feelsLike = null;
            if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                temperature = cc.getTemperatureC();
                imgBigTemperatureUnit = imgBigTemperatureUnitC;
                feelsLike = cc.getFeelsLikeC();
            } else {
                temperature = cc.getTemperatureF();
                imgBigTemperatureUnit = imgBigTemperatureUnitF;
                feelsLike = cc.getFeelsLikeF();
            }
            if (temperature != null) {
                g.setFont(Rs.F53);
                g.setColor(Rs.C255204000);
                g.drawString(temperature, 216, 238);
                int temperatureWth = Rs.FM53.stringWidth(temperature);
                if (imgBigTemperatureUnit != null) {
                    g.drawImage(imgBigTemperatureUnit, 216 + 3 + temperatureWth, 219, c);
                }
            } else {
                g.setFont(Rs.F53);
                g.setColor(Rs.C255204000);
                g.drawString("N/A", 216, 238);
            }
            //Current weather - forecast Text
            String forecastText = cc.getForecastText();
            if (forecastText != null) {
                g.setFont(Rs.F18);
                g.setColor(Rs.C255255255);
                String[] forecastTexts = TextUtil.split(forecastText, Rs.FM18, 106, 2);
                for (int i = 0; i < forecastTexts.length; i++) {
                    g.drawString(forecastTexts[i], 217, 265 + (i * 18));
                }
            }
            //Current weather - Separator
            g.setColor(Rs.DVB219219219051);
            g.fillRect(329, 203, 1, 68);
            //Current weather - label
            g.setFont(Rs.F16);
            g.setColor(Rs.C187189192);
            if (txtHumidity != null) {
                g.drawString(txtHumidity+ " :", 351, 221);
            }
            if (txtWind != null) {
                g.drawString(txtWind+ " :", 351, 241);
            }
            if (txtFeelsLike != null) {
                g.drawString(txtFeelsLike+ " :", 351, 261);
            }
            //Current weather - value
            g.setFont(Rs.F17);
            g.setColor(Rs.C219219219);
            String humidity = cc.getHumidity();
            if (humidity != null) {
                g.drawString(humidity + "%", 438, 221);
            } else {
                g.drawString("N/A", 438, 221);
            }
            String windDirection = cc.getWindDirection();
            if (windDirection == null) {
                windDirection = "";
            }
            String windSpeedKm = cc.getWindSpeedKm();
            if (windSpeedKm == null) {
                windSpeedKm = "";
            }
            String wind = windDirection + " " + windSpeedKm + "Km/h";
            if (wind != null) {
                g.drawString(wind, 438, 241);
            }
            //Current weather - Feels like
            if (feelsLike != null) {
                g.drawString(feelsLike, 438, 261);
                int feelsLikeWth = Rs.FM17.stringWidth(feelsLike);
                Image imgSmallTemperatureUnit = null;
                if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                    imgSmallTemperatureUnit = imgSmallTemperatureUnitC;
                } else {
                    imgSmallTemperatureUnit = imgSmallTemperatureUnitF;
                }
                if (imgSmallTemperatureUnit != null) {
                    g.drawImage(imgSmallTemperatureUnit, 438 + 3 + feelsLikeWth, 261 - 9, c);
                }
            } else {
                g.drawString("N/A", 438, 261);
            }
        }
        /*********************************************************************************
         * Short term forecast
         *********************************************************************************/
        ShortTermForecast[] stfs = null;
        if (latestForecasts[curIdx] != null) {
            stfs = latestForecasts[curIdx].getShortTermForecasts();
        }
        g.setColor(Rs.C000000000);
        g.fillRect(63, 299, 846, 174);
        g.setColor(Rs.DVB203203203020);
        g.fillRect(275, 335, 210, 137);
        g.setColor(Rs.DVB203203203026);
        g.fillRect(697, 335, 212, 137);
        if (imgWHigh != null) {
            g.drawImage(imgWHigh, 63, 298, c);
        }
//        if (imgShadow != null) {
//            g.drawImage(imgShadow, 16, 463, c);
//        }
        if (stfs != null && stfs.length > 0) {
            int stfsLth = stfs.length;
            int loopCnt = 0;
            if (stfsLth > WeatherShortTermForecastUI.PERIOD_COUNT) {
                loopCnt = WeatherShortTermForecastUI.PERIOD_COUNT;
            } else {
                loopCnt = stfsLth;
            }
            Image imgSmallTemperatureUnit = null;
            Image imgMidTemperatureUnit = null;
            if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                imgSmallTemperatureUnit = imgSmallTemperatureUnitC;
                imgMidTemperatureUnit = imgMidTemperatureUnitC;
            } else {
                imgSmallTemperatureUnit = imgSmallTemperatureUnitF;
                imgMidTemperatureUnit = imgMidTemperatureUnitF;
            }
            for (int i=0; i<loopCnt; i++) {
                String nameSTF = null;
                Image imgIconCodeSTF = null;
                String temperatureSTF = null;
                String feelsLikeSTF = null;
                String popSTF = null;
                if (stfs[i] != null) {
                    //Name
                    nameSTF = stfs[i].getName();
                    if (nameSTF != null) {
                        nameSTF = TextUtil.shorten(nameSTF, Rs.FM19, 190);    
                    } else {
                        nameSTF = "N/A";
                    }
                    //Icon code image
                    imgIconCodeSTF = stfs[i].getIconCodeImage(); 
                    //Temperature
                    if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                        temperatureSTF = stfs[i].getTemperatureC();
                    } else {
                        temperatureSTF = stfs[i].getTemperatureF();
                    }
                    //POP
                    popSTF = stfs[i].getProbabilityOfPrecipitation();
                    if (popSTF != null && (popSTF.trim()).length() > 0) {
                        popSTF = popSTF + "%";
                    } else {
                        popSTF = "N/A";
                    }
                    // Feels Like
                    if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                        feelsLikeSTF = stfs[i].getFeelsLikeC();
                    } else {
                        feelsLikeSTF = stfs[i].getFeelsLikeF();
                    }
                    //Draw - Name
                    g.setFont(Rs.F19);
                    g.setColor(Rs.C255255255);
                    g.drawString(nameSTF, 167 - (Rs.FM19.stringWidth(nameSTF)/2) + (i * PERIOD_GAP), 323);
                    //Draw - Icon code image
                    if (imgIconCodeSTF != null) {
                        g.drawImage(imgIconCodeSTF, 121 + (i * PERIOD_GAP), 342, 98, 60, c);
                    }
                    //Draw - Temperature
                    if (temperatureSTF != null && (temperatureSTF.trim()).length() > 0) {
                        g.setFont(Rs.F33);
                        g.setColor(Rs.C255255255);
                        g.drawString(temperatureSTF, 176 - Rs.FM33.stringWidth(temperatureSTF) + (i * PERIOD_GAP), 422);
                        if (imgMidTemperatureUnit != null) {
                            g.drawImage(imgMidTemperatureUnit, 179 + (i * PERIOD_GAP), 410, c);
                        }
                    }
                    //Draw - POP
                    if (txtPOP != null) {
                        g.setFont(Rs.F16);
                        g.setColor(Rs.C185185185);
                        g.drawString(txtPOP+" :", 120 + (i * PERIOD_GAP), 441);
                    }
                    g.setFont(Rs.F17);
                    g.setColor(Rs.C219219219);
                    g.drawString(popSTF, 237 - Rs.FM17.stringWidth(popSTF) + (i * PERIOD_GAP), 441);
                    // Draw - Feels Like
                    if (txtFeelsLike != null) {
                        g.setFont(Rs.F16);
                        g.setColor(Rs.C185185185);
                        g.drawString(txtFeelsLike+" :", 120 + (i * PERIOD_GAP), 458);
                    }
                    if (feelsLikeSTF != null && (feelsLikeSTF.trim()).length() > 0) {
                        g.setFont(Rs.F17);
                        g.setColor(Rs.C255255255);
                        g.drawString(feelsLikeSTF, 223-(Rs.FM17.stringWidth(feelsLikeSTF)) + (i * PERIOD_GAP), 458);
                        if (imgSmallTemperatureUnit != null) {
                            g.drawImage(imgSmallTemperatureUnit, 223 + (i * PERIOD_GAP), 449, c);
                        }
                    }
                }else {
                    g.setFont(Rs.F33);
                    g.setColor(Rs.C255255255);
                    g.drawString("N/A", 167 - (Rs.FM33.stringWidth("N/A")/2) + (i * PERIOD_GAP), 414);
                }
            }
        } else {
            g.setFont(Rs.F33);
            g.setColor(Rs.C255255255);
            for (int i=0; i<WeatherShortTermForecastUI.PERIOD_COUNT; i++) {
                g.drawString("N/A", 167 - (Rs.FM33.stringWidth("N/A")/2) + (i * PERIOD_GAP), 414);
            }
        }
    }
}

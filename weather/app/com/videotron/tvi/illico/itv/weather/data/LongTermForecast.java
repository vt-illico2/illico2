package com.videotron.tvi.illico.itv.weather.data;

import java.awt.Image;


public class LongTermForecast {
    //Short-term Forecasts 
    public static final String LONG_TERM_ID_TODAY = "1";
    
    /*
     *     ID Similar to PERIOD, the numbers in the ID correspond to the NAME attribute, where:
            1 = day 1 (today)
            2 = day 2 (tomorrow)
            3 = day 3 (day after tomorrow)
            4 = day 4 (etc.)
            5 = day 5
            But unlike the PERIOD ID, these numbers are fixed and do not change throughout the day.
     */
    private String id; //ID
    private String date; //Date for forecast day in XML dateTime format
    private String name; //Name of forecast day
    private String iconCode; //Icon to display
    private Image imgIconCode; //Icon code image for icon code.
    private String forecastText; //Forecast text
    private String highTemperatureC; //Expected high (Celsius)
    private String lowTemperatureC; //Expected low (Celsius)
    private String highTemperatureF; //Expected high (Fahrenheit)
    private String lowTemperatureF; //Expected low (Fahrenheit)
    private String probabilityOfPrecipitation; //Probability of precipitation (in %)
    private String windSpeedKm; //Wind speed in km/h
    private String windSpeedMi; //Wind speed in miles/h
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getIconCode() {
        return iconCode;
    }
    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }
    
    public Image getIconCodeImage() {
        return imgIconCode;
    }
    public void setIconCodeImage(Image imgIconCode) {
        this.imgIconCode = imgIconCode;
    }
    
    public String getForecastText() {
        return forecastText;
    }
    public void setForecastText(String forecastText) {
        this.forecastText = forecastText;
    }
    
    public String getHighTemperatureC() {
        return highTemperatureC;
    }
    public void setHighTemperatureC(String highTemperatureC) {
        this.highTemperatureC = highTemperatureC;
    }
    
    public String getLowTemperatureC() {
        return lowTemperatureC;
    }
    public void setLowTemperatureC(String lowTemperatureC) {
        this.lowTemperatureC = lowTemperatureC;
    }
    
    public String getHighTemperatureF() {
        return highTemperatureF;
    }
    public void setHighTemperatureF(String highTemperatureF) {
        this.highTemperatureF = highTemperatureF;
    }
    
    public String getLowTemperatureF() {
        return lowTemperatureF;
    }
    public void setLowTemperatureF(String lowTemperatureF) {
        this.lowTemperatureF = lowTemperatureF;
    }
    
    public String getProbabilityOfPrecipitation() {
        return probabilityOfPrecipitation;
    }
    public void setProbabilityOfPrecipitation(String probabilityOfPrecipitation) {
        this.probabilityOfPrecipitation = probabilityOfPrecipitation;
    }
    
    public String getWindSpeedKm() {
        return windSpeedKm;
    }
    public void setWindSpeedKm(String windSpeedKm) {
        this.windSpeedKm = windSpeedKm;
    }
    
    public String getWindSpeedMi() {
        return windSpeedMi;
    }
    public void setWindSpeedMi(String windSpeedMi) {
        this.windSpeedMi = windSpeedMi;
    }
}

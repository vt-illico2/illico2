package com.videotron.tvi.illico.itv.weather.controller;

import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.ui.BaseUI;
import com.videotron.tvi.illico.itv.weather.ui.WeatherLongTermForecastUI;
import com.videotron.tvi.illico.itv.weather.ui.WeatherMainUI;
import com.videotron.tvi.illico.itv.weather.ui.WeatherPreferencesUI;
import com.videotron.tvi.illico.itv.weather.ui.WeatherShortTermForecastUI;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

public class SceneManager {
    private static SceneManager instance;

    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lUIWeather;
    private LayeredWindow lWinWeather;
    private LayeredKeyWizard lKeyWeather;

    private Hashtable scenes;
    private BaseUI curScene;
    private int curSceneId;
    private SceneManager(){
    }
    public static synchronized SceneManager getInstance() {
        if (instance == null) {
            instance = new SceneManager();
        }
        return instance;
    }
    public void init() {
        if (lWinWeather == null) {
            lWinWeather = new LayeredWindowWizard();
            lWinWeather.setVisible(true);
        }
        if (lKeyWeather == null) {
            lKeyWeather = new LayeredKeyWizard();
        }
        lUIWeather = WindowProperty.WEATHER.createLayeredDialog(lWinWeather, lWinWeather.getBounds(), lKeyWeather);
        lUIWeather.deactivate();
        if (scenes == null) {
            scenes = new Hashtable();
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
    }
    public void dispose() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                BaseUI scene = (BaseUI)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.dispose();
                scene = null;
            }
            scenes = null;
        }
        curScene = null;
        if (lUIWeather != null) {
            lUIWeather.deactivate();
            WindowProperty.dispose(lUIWeather);
            lUIWeather=null;
        }
        ImagePool imgPool = FrameworkMain.getInstance().getImagePool();
        if (imgPool != null) {
            imgPool.clear();
        }
    }
    public void start() {
        if (lUIWeather != null) {
            lUIWeather.activate();
        }
        WeatherVbmController.getInstance().writeWeatherAppStart();
        goToNextScene(SceneTemplate.SCENE_ID_WEATHER_MAIN, true, null);
    }
    public void stop() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                BaseUI scene = (BaseUI)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.stop();
            }
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinWeather.remove(curScene);
            curScene = null;
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
        if (lUIWeather != null) {
            lUIWeather.deactivate();
        }
        WeatherVbmController.getInstance().writeWeatherAppExit();
    }

    public void goToNextScene(int reqSceneId, boolean isReset, String[] params) {
        if (curSceneId == reqSceneId) {
            return;
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinWeather.remove(curScene);
        }

        //Start current scene
        BaseUI scene = getScene(reqSceneId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.handleKeyEvent]scene : "+scene);
        }
        if (scene != null) {
            scene.start(isReset, params);
            lWinWeather.add(scene);
            curSceneId = reqSceneId;
            curScene = scene;
        }
        //Repaint();
        lWinWeather.repaint();
    }
    private BaseUI getScene(int reqSceneId) {
        BaseUI res = (BaseUI)scenes.get(new Integer(reqSceneId));
        //Check Scene
        if (res == null) {
            res = createScene(reqSceneId);
            if (res != null) {
                scenes.put(new Integer(reqSceneId), res);
            }
        }
        return res;
    }

    private BaseUI createScene(int reqSceneId) {
        BaseUI scene = null;
        switch(reqSceneId) {
            case SceneTemplate.SCENE_ID_WEATHER_MAIN:
                scene = new WeatherMainUI();
                break;
            case SceneTemplate.SCENE_ID_SHORT_TERM_FORECAST:
                scene = new WeatherShortTermForecastUI();
                break;
            case SceneTemplate.SCENE_ID_LONG_TERM_FORECAST:
                scene = new WeatherLongTermForecastUI();
                break;
            case SceneTemplate.SCENE_ID_WEATHER_PREFERENCES:
                scene = new WeatherPreferencesUI();
                break;
        }
        if (scene != null) {
            scene.init();
        }
        return scene;
    }
    public int getCurrentSceneId() {
        return curSceneId;
    }
    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowWizard extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowWizard() {
            setBounds(Rs.SCENE_BOUND);
        }
        public void notifyShadowed() {
        }
    }
    class LayeredKeyWizard implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (curScene == null) {
                return false;
            }
            //Block PVR-related keys at the scene without sacled video.
            int keyCode = userEvent.getCode();
            switch(curSceneId) {
                case SceneTemplate.SCENE_ID_WEATHER_PREFERENCES:
                    switch(keyCode) {
                        case KeyCodes.REWIND:
                        case KeyCodes.FAST_FWD:
                        case KeyCodes.PLAY:
                        case KeyCodes.PAUSE:
                        case KeyCodes.STOP:
                        case KeyCodes.RECORD:
                        case KeyCodes.LIVE:
                            return true;
                    }
                    break;
            }
            int keyType = userEvent.getType();
            if (keyType != KeyEvent.KEY_PRESSED) {
                return false;
            }
            return curScene.handleKey(userEvent.getCode());
        }
    }
}

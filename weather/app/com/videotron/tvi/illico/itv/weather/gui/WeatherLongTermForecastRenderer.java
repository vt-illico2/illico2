package com.videotron.tvi.illico.itv.weather.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CurrentCondition;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.LongTermForecast;
import com.videotron.tvi.illico.itv.weather.ui.WeatherLongTermForecastUI;
import com.videotron.tvi.illico.util.TextUtil;

public class WeatherLongTermForecastRenderer extends BaseRenderer {
    /*********************************************************************************
     * Image-related
     *********************************************************************************/
//    private Image imgShadow;
    private Image imgWHigh;
    private Image imgCityBG;
    private Image imgArrowR;
    private Image imgArrowL;
    private Image imgBigTemperatureUnitC;
    private Image imgBigTemperatureUnitF;
    private Image imgMidTemperatureUnitC;
    private Image imgMidTemperatureUnitF;
    private Image imgSmaTemperatureUnitC;
    private Image imgSmaTemperatureUnitF;
    private Image imgSmaTemperatureUnitGrayC;
    private Image imgSmaTemperatureUnitGrayF;
  
    private static final int PERIOD_GAP = 170;
    
    protected void prepareChild() {
//        imgShadow = ImageManager.getInstance().getImage("11_weather_sha.png", ImageManager.TYPE_TEMPORARY);
        imgWHigh = ImageManager.getInstance().getImage("11_w_high.png", ImageManager.TYPE_TEMPORARY);
        imgCityBG = ImageManager.getInstance().getImage("11_city_bg.png", ImageManager.TYPE_TEMPORARY);
        imgArrowR = ImageManager.getInstance().getImage("02_ars_r.png", ImageManager.TYPE_TEMPORARY);
        imgArrowL = ImageManager.getInstance().getImage("02_ars_l.png", ImageManager.TYPE_TEMPORARY);
        imgBigTemperatureUnitC = ImageManager.getInstance().getImage("11_c_b.png", ImageManager.TYPE_TEMPORARY);
        imgBigTemperatureUnitF = ImageManager.getInstance().getImage("11_f_b.png", ImageManager.TYPE_TEMPORARY);
        imgMidTemperatureUnitC = ImageManager.getInstance().getImage("11_c_s.png", ImageManager.TYPE_TEMPORARY);
        imgMidTemperatureUnitF = ImageManager.getInstance().getImage("11_f_s.png", ImageManager.TYPE_TEMPORARY);
        imgSmaTemperatureUnitC = ImageManager.getInstance().getImage("11_c_xs_white.png", ImageManager.TYPE_TEMPORARY);
        imgSmaTemperatureUnitF = ImageManager.getInstance().getImage("11_f_xs_white.png", ImageManager.TYPE_TEMPORARY);
        imgSmaTemperatureUnitGrayC = ImageManager.getInstance().getImage("11_c_xs.png", ImageManager.TYPE_TEMPORARY);
        imgSmaTemperatureUnitGrayF = ImageManager.getInstance().getImage("11_f_xs.png", ImageManager.TYPE_TEMPORARY);
    }
    
    protected void paintRenderer(Graphics g, UIComponent c) {
        /*********************************************************************************
         * Draw background
         *********************************************************************************/
        WeatherLongTermForecastUI scene = (WeatherLongTermForecastUI)c; 
        boolean isRequested = scene.isRequested();
        if (!isRequested) {
            return;
        }
        //Message
        String msg = scene.getMessge();
        if (msg != null) {
            g.setFont(Rs.F27);
            g.setColor(Rs.C245212016);
            int reqMsgWth = Rs.FM27.stringWidth(msg);
            g.drawString(msg, 480 - (reqMsgWth / 2), 394);
            return;
        }
        //Title
        String txtShortTermForecast = DataCenter.getInstance().getString("TxtWeather.Long_Term_Forecast");
        if (txtShortTermForecast != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtShortTermForecast, 63, 97);
        }
        /*********************************************************************************
         * Draw data
         *********************************************************************************/
        LatestForecast[] latestForecasts = scene.getLatestForecasts();
        int curIdx = scene.getCurrentIndex();
        if (latestForecasts == null || latestForecasts.length == 0) {
            return;
        }
        /*********************************************************************************
         * Cities title
         *********************************************************************************/
        if (imgCityBG != null) {
            g.drawImage(imgCityBG, 0, 87, c);
        }
        int latestForecastsLth = latestForecasts.length;
        if (latestForecastsLth > 1) {
            if (imgArrowR != null) {
                g.drawImage(imgArrowR, 467, 124, c);
            }
            if (imgArrowL != null) {
                g.drawImage(imgArrowL, 134, 124, c);
            }
            g.setFont(Rs.F17);
            g.setColor(Rs.C180180180);
            int prevIdx = (latestForecastsLth+curIdx-1) % latestForecastsLth;
            String prevCityName = null;
            if (latestForecasts[prevIdx] != null) {
                CityInfo cInfo = latestForecasts[prevIdx].getCityInfo();
                if (cInfo != null) {
                    prevCityName = cInfo.getDisplayName();
                }
            }
            if (prevCityName == null) {
                prevCityName = "N/A";
            } else {
                prevCityName = TextUtil.shorten(prevCityName, Rs.FM17, 68);
            }
            g.drawString(prevCityName, 65, 142);
            
            int nextIdx = (curIdx+1) % latestForecastsLth;
            String nextCityName = null;
            if (latestForecasts[nextIdx] != null) {
                CityInfo cInfo = latestForecasts[nextIdx].getCityInfo();
                if (cInfo != null) {
                    nextCityName = cInfo.getDisplayName();
                }
            }
            if (nextCityName == null) {
                nextCityName = "N/A";
            } else {
                nextCityName = TextUtil.shorten(nextCityName, Rs.FM17, 68);
            }
            g.drawString(nextCityName, 490, 142);
        }
        String curCityName = null;
        if (latestForecasts[curIdx] != null) {
            CityInfo cInfo = latestForecasts[curIdx].getCityInfo();
            if (cInfo != null) {
                curCityName = cInfo.getDisplayName();
            }
        }
        if (curCityName == null) {
            curCityName = "N/A";
        } else {
            curCityName = TextUtil.shorten(curCityName, Rs.FM24, 300);
        }
        g.setFont(Rs.F24);
        g.setColor(Rs.C249197000);
        g.drawString(curCityName, 310 - (Rs.FM24.stringWidth(curCityName)/2), 143);
        /*********************************************************************************
         * Current weather
         *********************************************************************************/
        int temperatureUnit = DataManager.getInstance().getTemperatureUnit();
        String txtCurrentWeather = DataCenter.getInstance().getString("TxtWeather.Current_Codition");
        if (txtCurrentWeather != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtCurrentWeather, 63, 185);
        }
        String txtHumidity = DataCenter.getInstance().getString("TxtWeather.Humidity");
        String txtWind = DataCenter.getInstance().getString("TxtWeather.Wind");        
        String txtFeelsLike = DataCenter.getInstance().getString("TxtWeather.Feels_Like");
        String txtPOP = DataCenter.getInstance().getString("TxtWeather.POP");
        
        CurrentCondition cc = null;
        if (latestForecasts[curIdx] != null) {
            cc = latestForecasts[curIdx].getCurrentCondition();
        }
        if (cc == null) {
            String txtNotAvailable = DataCenter.getInstance().getString("TxtWeather.Not_Available");
            if (txtNotAvailable != null) {
                int txtNotAvailableLth = Rs.FM30.stringWidth(txtNotAvailable);
                g.setFont(Rs.F30);
                g.setColor(Rs.C255204000);
                g.drawString(txtNotAvailable, 320 - (txtNotAvailableLth/2), 244);
            }
        } else {
            //Current weather - weather icon
            Image imgIconCode = cc.getIconCodeImage();
            if (imgIconCode != null) {
                g.drawImage(imgIconCode, 64, 196, 140, 85, c);
            }
            //Current weather - temperature
            String temperature = null;
            Image imgBigTemperatureUnit = null;
            String feelsLike = null;
            if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                temperature = cc.getTemperatureC();
                imgBigTemperatureUnit = imgBigTemperatureUnitC;
                feelsLike = cc.getFeelsLikeC();
            } else {
                temperature = cc.getTemperatureF();
                imgBigTemperatureUnit = imgBigTemperatureUnitF;
                feelsLike = cc.getFeelsLikeF();
            }
            if (temperature != null) {
                g.setFont(Rs.F53);
                g.setColor(Rs.C255204000);
                g.drawString(temperature, 216, 238);
                int temperatureWth = Rs.FM53.stringWidth(temperature);
                if (imgBigTemperatureUnit != null) {
                    g.drawImage(imgBigTemperatureUnit, 216 + 3 + temperatureWth, 219, c);
                }
            } else {
                g.setFont(Rs.F53);
                g.setColor(Rs.C255204000);
                g.drawString("N/A", 216, 238);
            }
            //Current weather - forecast Text
            String forecastText = cc.getForecastText();
            if (forecastText != null) {
                g.setFont(Rs.F18);
                g.setColor(Rs.C255255255);
                String[] forecastTexts = TextUtil.split(forecastText, Rs.FM18, 106, 2);
                for (int i = 0; i < forecastTexts.length; i++) {
                    g.drawString(forecastTexts[i], 217, 265 + (i * 18));
                }
            }
            //Current weather - Separator
            g.setColor(Rs.DVB219219219051);
            g.fillRect(329, 203, 1, 68);
            //Current weather - label
            g.setFont(Rs.F16);
            g.setColor(Rs.C187189192);
            if (txtHumidity != null) {
                g.drawString(txtHumidity+ " :", 351, 221);
            }
            if (txtWind != null) {
                g.drawString(txtWind+ " :", 351, 241);
            }
            if (txtFeelsLike != null) {
                g.drawString(txtFeelsLike+ " :", 351, 261);
            }
            //Current weather - value
            g.setFont(Rs.F17);
            g.setColor(Rs.C219219219);
            String humidity = cc.getHumidity();
            if (humidity != null) {
                g.drawString(humidity + "%", 438, 221);
            } else {
                g.drawString("N/A", 438, 221);
            }
            String windDirection = cc.getWindDirection();
            if (windDirection == null) {
                windDirection = "";
            }
            String windSpeedKm = cc.getWindSpeedKm();
            if (windSpeedKm == null) {
                windSpeedKm = "";
            }
            String wind = windDirection + " " + windSpeedKm + "Km/h";
            if (wind != null) {
                g.drawString(wind, 438, 241);
            }
            //Current weather - Feels like
            if (feelsLike != null) {
                g.drawString(feelsLike, 438, 261);
                int feelsLikeWth = Rs.FM17.stringWidth(feelsLike);
                Image imgSmallTemperatureUnit = null;
                if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                    imgSmallTemperatureUnit = imgSmaTemperatureUnitC;
                } else {
                    imgSmallTemperatureUnit = imgSmaTemperatureUnitF;
                }
                if (imgSmallTemperatureUnit != null) {
                    g.drawImage(imgSmallTemperatureUnit, 438 + 3 + feelsLikeWth, 261 - 9, c);
                }
            } else {
                g.drawString("N/A", 438, 261);
            }
        }
        /*********************************************************************************
         * Long term forecast
         *********************************************************************************/
        LongTermForecast[] ltfs = null;
        if (latestForecasts[curIdx] != null) {
            ltfs = latestForecasts[curIdx].getLongTermForecasts();
        }
        g.setColor(Rs.C000000000);
        g.fillRect(63, 299, 846, 174);
        g.setColor(Rs.DVB203203203020);
        g.fillRect(235, 335, 168, 137);
        g.fillRect(573, 335, 168, 137);
        if (imgWHigh != null) {
            g.drawImage(imgWHigh, 63, 298, c);
        }
//        if (imgShadow != null) {
//            g.drawImage(imgShadow, 16, 463, c);
//        }
        if (ltfs != null && ltfs.length > 0) {
            int stfsLth = ltfs.length;
            int loopCnt = 0;
            if (stfsLth > WeatherLongTermForecastUI.PERIOD_COUNT) {
                loopCnt = WeatherLongTermForecastUI.PERIOD_COUNT;
            } else {
                loopCnt = stfsLth;
            }
            Image imgMidTemperatureUnit = null;
            Image imgSmaTemperatureUnitGray = null;
            if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                imgMidTemperatureUnit = imgMidTemperatureUnitC;
                imgSmaTemperatureUnitGray = imgSmaTemperatureUnitGrayC;
            } else {
                imgMidTemperatureUnit = imgMidTemperatureUnitF;
                imgSmaTemperatureUnitGray = imgSmaTemperatureUnitGrayF;
            }
            for (int i=0; i<loopCnt; i++) {
                String nameLTF = null;
                Image imgIconCodeLTF = null;
                String highTemperatureLTF = null;
                String lowTemperatureLTF = null;
                String popLTF = null;
                if (ltfs[i] != null) {
                    //Name
                    nameLTF = ltfs[i].getName();
                    if (nameLTF != null) {
                        nameLTF = TextUtil.shorten(nameLTF, Rs.FM19, 190);    
                    } else {
                        nameLTF = "N/A";
                    }
                    //Icon code image
                    imgIconCodeLTF = ltfs[i].getIconCodeImage(); 
                    //Temperature
                    if (temperatureUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                        lowTemperatureLTF = ltfs[i].getLowTemperatureC();
                        highTemperatureLTF = ltfs[i].getHighTemperatureC();
                    } else {
                        lowTemperatureLTF = ltfs[i].getLowTemperatureF();
                        highTemperatureLTF = ltfs[i].getHighTemperatureF();
                    }
                    //POP
                    popLTF = ltfs[i].getProbabilityOfPrecipitation();
                    if (popLTF != null && (popLTF.trim()).length() > 0) {
                        popLTF = popLTF + "%";
                    } else {
                        popLTF = "N/A";
                    }
                    //Draw - Name
                    g.setFont(Rs.F19);
                    g.setColor(Rs.C255255255);
                    g.drawString(nameLTF, 147 - (Rs.FM19.stringWidth(nameLTF)/2) + (i * PERIOD_GAP), 323);
                    //Draw - Icon code image
                    if (imgIconCodeLTF != null) {
                        g.drawImage(imgIconCodeLTF, 100 + (i * PERIOD_GAP), 348, 98, 60, c);
                    }
                    //Draw - Temperature
                    if (highTemperatureLTF != null && (highTemperatureLTF.trim()).length() > 0) {
                        g.setFont(Rs.F33);
                        g.setColor(Rs.C255255255);
                        int highTemperatureLTFWth = Rs.FM33.stringWidth(highTemperatureLTF);
                        g.drawString(highTemperatureLTF, 127 - highTemperatureLTFWth + (i * PERIOD_GAP), 428);
                        if (imgMidTemperatureUnit != null) {
                            g.drawImage(imgMidTemperatureUnit, 131 + (i * PERIOD_GAP), 416, c);
                        }
                    }
                    g.setFont(Rs.F24);
                    g.setColor(Rs.C164164164);
                    g.drawString("/", 151 + (i * PERIOD_GAP), 428);
                    if (lowTemperatureLTF != null && (lowTemperatureLTF.trim()).length() > 0) {
                        int lowTemperatureLTFWth = Rs.FM24.stringWidth(lowTemperatureLTF);
                        g.drawString(lowTemperatureLTF, 168 + (i * PERIOD_GAP), 428);
                        if (imgSmaTemperatureUnitGray != null) {
                            g.drawImage(imgSmaTemperatureUnitGray, 168+2+lowTemperatureLTFWth + (i * PERIOD_GAP), 419, c);
                        }
                    }
                    //Draw - POP
                    if (txtPOP != null) {
                        g.setFont(Rs.F16);
                        g.setColor(Rs.C185185185);
                        g.drawString(txtPOP+" :", 106 + (i * PERIOD_GAP), 448);
                    }
                    g.setFont(Rs.F17);
                    g.setColor(Rs.C219219219);
                    g.drawString(popLTF, 161 + (i * PERIOD_GAP), 448);
                } else {
                    g.setFont(Rs.F33);
                    g.setColor(Rs.C255255255);
                    g.drawString("N/A", 154 - (Rs.FM33.stringWidth("N/A")/2) + (i * PERIOD_GAP), 414);
                }
            }
        } else {
            g.setFont(Rs.F33);
            g.setColor(Rs.C255255255);
            for (int i=0; i<WeatherLongTermForecastUI.PERIOD_COUNT; i++) {
                g.drawString("N/A", 154 - (Rs.FM33.stringWidth("N/A")/2) + (i * PERIOD_GAP), 414);
            }
        }
    }
}

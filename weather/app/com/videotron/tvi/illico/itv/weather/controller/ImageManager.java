package com.videotron.tvi.illico.itv.weather.controller;

import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;

public class ImageManager {
    private static ImageManager instance;
    private DataCenter dCenter = DataCenter.getInstance();
    public static final int TYPE_PERMANENT = 0; //Dispose -> Remove
    public static final int TYPE_TEMPORARY = 1; //Stop, Dispose -> Remove
    private Vector perImgNameVec;
    private Vector tempImgNameVec;
    private ImageManager() {
        init();
    }
    synchronized public static ImageManager getInstance() {
        if (instance == null) {
            instance = new ImageManager();
        }
        return instance;
    }
    public void init() {
        dispose();
        perImgNameVec = new Vector();
        tempImgNameVec = new Vector();
    }
    public void dispose() {
        stop();
        if (perImgNameVec != null) {
            int perImgNameVecSz = perImgNameVec.size();
            for (int i=0; i<perImgNameVecSz; i++) {
                String perImgName = (String)perImgNameVec.elementAt(i);
                if (perImgName != null) {
                    dCenter.removeImage(perImgName);
                }
            }
            perImgNameVec.clear();
            perImgNameVec = null;
        }
    }
    public void stop() {
        if (tempImgNameVec != null) {
            int tempImgNameVecSz = tempImgNameVec.size();
            for (int i=0; i<tempImgNameVecSz; i++) {
                String tempImgName = (String)tempImgNameVec.elementAt(i);
                if (tempImgName != null) {
                    dCenter.removeImage(tempImgName);
                }
            }
            tempImgNameVec.clear();
            tempImgNameVec = null;
        }
    }
    public void start() {
        stop();
        tempImgNameVec = new Vector();
    }
    public Image getImage(String key, int type) {
        if (key == null) {
            return null;
        }
        Vector checkVec = null;
        switch(type) {
            case TYPE_PERMANENT:
                checkVec = perImgNameVec;
                break;
            case TYPE_TEMPORARY:
                checkVec = tempImgNameVec;
                break;
        }
        if (checkVec == null) {
            return null;
        }
        synchronized(checkVec) {
            if (!checkVec.contains(key)) {
                checkVec.addElement(key);
            }
        }
        return dCenter.getImage(key);
    }
    public Image createImage(byte[] src, String key, int type) {
        if (src==null || key == null) {
            return null;
        }
        Vector checkVec = null;
        switch(type) {
            case TYPE_PERMANENT:
                checkVec = perImgNameVec;
                break;
            case TYPE_TEMPORARY:
                checkVec = tempImgNameVec;
                break;
        }
        if (checkVec == null) {
            return null;
        }
        synchronized(checkVec) {
            if (!checkVec.contains(key)) {
                checkVec.addElement(key);
            }
        }
        return FrameworkMain.getInstance().getImagePool().createImage(src, key);
    }
    synchronized public void removeImage(String key) {
        if (key == null) {
            return;
        }
        if (perImgNameVec != null && perImgNameVec.contains(key)) {
            perImgNameVec.removeElement(key);
        }
        if (tempImgNameVec != null && tempImgNameVec.contains(key)) {
            tempImgNameVec.removeElement(key);
        }
        dCenter.removeImage(key);
    }
}

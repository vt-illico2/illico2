package com.videotron.tvi.illico.itv.weather.data;

import java.awt.Image;


public class ShortTermForecast {
    //Short-term Forecasts 
    public static final String SHORT_TERM_ID_THIS_MORNING = "1";
    public static final String SHORT_TERM_ID_THIS_AFTERNOON = "2";
    public static final String SHORT_TERM_ID_THIS_EVENING = "3";
    public static final String SHORT_TERM_ID_TONIGHT = "4";
    public static final String SHORT_TERM_ID_TOMORROW_MORNING = "5";
    public static final String SHORT_TERM_ID_TOMORROW_AFTERNOON = "6";
    public static final String SHORT_TERM_ID_TOMORROW_EVENING = "7";
    /*
     *  1 = This morning
        2 = This afternoon
        3 = This evening
        4 = Tonight
        5 = Tomorrow morning
        6 = Tomorrow afternoon
        7 = Tomorrow evening
        NOTE: The numbers CHANGE throughout the day.
     */
    private String id; //Identifies a period of the day
    private String date; //Date for the forecast period in XML dateTime format
    private String name; //Name of the period
    private String iconCode; //Icon to use
    private Image imgIconCode; //Icon code image for icon code.
    private String forecastText; //Forecast text
    private String temperatureC;  //Forecast temperature (Celsius)
    private String temperatureF; //Forecast temperature (Fahrenheit)
    private String probabilityOfPrecipitation; //Probability of precipitation (in %)
    
    private String feelsLikeC;
    private String feelsLikeF;
    private String windSpeedKm;
    private String windSpeedMi;
    private String windDir;
    
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    public String getIconCode() {
        return iconCode;
    }
    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }
    
    public Image getIconCodeImage() {
        return imgIconCode;
    }
    public void setIconCodeImage(Image imgIconCode) {
        this.imgIconCode = imgIconCode;
    }
    
    public String getForecastText() {
        return forecastText;
    }
    public void setForecastText(String forecastText) {
        this.forecastText = forecastText;
    }
    
    public String getTemperatureC() {
        return temperatureC;
    }
    public void setTemperatureC(String temperatureC) {
        this.temperatureC = temperatureC;
    }
    
    public String getTemperatureF() {
        return temperatureF;
    }
    public void setTemperatureF(String temperatureF) {
        this.temperatureF = temperatureF;
    }
    
    public String getProbabilityOfPrecipitation() {
        return probabilityOfPrecipitation;
    }
    public void setProbabilityOfPrecipitation(String probabilityOfPrecipitation) {
        this.probabilityOfPrecipitation = probabilityOfPrecipitation;
    }
    
    public String getFeelsLikeC() {
        return feelsLikeC;
    }
    public void setFeelsLikeC(String feelsLikeC) {
        this.feelsLikeC = feelsLikeC;
    }
    
    public String getFeelsLikeF() {
        return feelsLikeF;
    }
    public void setFeelsLikeF(String feelsLikeF) {
        this.feelsLikeF = feelsLikeF;
    }
    
    public String getWindSpeedKm() {
        return windSpeedKm;
    }
    public void setWindSpeedKm(String windSpeedKm) {
        this.windSpeedKm = windSpeedKm;
    }
    
    public String getWindSpeedMi() {
        return windSpeedMi;
    }
    public void setWindSpeedMi(String windSpeedMi) {
        this.windSpeedMi = windSpeedMi;
    }
    
    public String getWindDirection() {
        return windDir;
    }
    public void setWindDirection(String windDir) {
        this.windDir = windDir;
    }
}

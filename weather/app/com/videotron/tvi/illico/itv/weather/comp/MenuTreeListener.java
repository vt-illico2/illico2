package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Rectangle;

public interface MenuTreeListener {
    void lostMenuTreeFocus();
    void selectedMenuTreeItem(MenuTreeItem selectedItem, Rectangle selectedItemArea);
    void changedMenuTreeItem(MenuTreeItem changedItem);
}

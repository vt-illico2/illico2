package com.videotron.tvi.illico.itv.weather.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.log.Log;

public class InbandDataListenerImpl implements InbandDataListener{
    public void receiveInbandData(String locator) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]start.");
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]Param - Locator : " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        CommunicationManager.getInstance().requestCompleteReceivingData();
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]end.");
        }
    }
}
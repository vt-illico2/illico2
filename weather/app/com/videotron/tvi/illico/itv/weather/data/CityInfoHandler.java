package com.videotron.tvi.illico.itv.weather.data;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.itv.weather.comp.SAXHandlerAdapter;

public class CityInfoHandler extends SAXHandlerAdapter {
    private CityInfo cInfo;
    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("DATAFILE")) {
            cInfo = new CityInfo();
        } else if (valueXMLTag.equalsIgnoreCase("SITE")) {
            cInfo = getCityInfo(attr);
        }
    }
    public void endElement(String valueXMLTag) {
        if (valueXMLTag != null && valueXMLTag.equalsIgnoreCase("DATAFILE")) {
            setResultObject(cInfo);
        }
    }
    private CityInfo getCityInfo(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CityInfo cInfo = new CityInfo();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("NAME")) {
                cInfo.setCityName(lValue);
            } else if (lName.equalsIgnoreCase("PROV")) {
                cInfo.setProvinceName(lValue);
            } else if (lName.equalsIgnoreCase("COUNTRY")) {
                cInfo.setCountryName(lValue);
                if (lValue != null) {
                    if (lValue.equalsIgnoreCase("Canada")) {
                        cInfo.setCityType(CityInfo.CITY_TYPE_CANADA);
                    } else {
                        cInfo.setCityType(CityInfo.CITY_TYPE_OTHER_CITY);
                    }
                }
            } else if (lName.equalsIgnoreCase("LOCATIONID")) {
                cInfo.setLocationId(lValue);
            }
        }
        return cInfo;
    }
}

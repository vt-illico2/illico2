package com.videotron.tvi.illico.itv.weather.data;

import java.util.Date;
import java.util.Vector;

import com.videotron.tvi.illico.itv.weather.DateUtil;
import com.videotron.tvi.illico.log.Log;

public class LatestForecast {
    private CityInfo ci;
    private CurrentCondition cc;
    private Vector stfVec = new Vector();
    private Vector ltfVec = new Vector();
    private ShortTermForecast[] stfs;
    private LongTermForecast[] ltfs;
    private Date nextUpdateDate;

    public CityInfo getCityInfo() {
        return ci;
    }

    public void setCityInfo(CityInfo ci) {
        this.ci = ci;
    }

    public CurrentCondition getCurrentCondition() {
        return cc;
    }

    public void setCurrentCondition(CurrentCondition cc) {
        this.cc = cc;
    }

    public void addShortTermForecast(ShortTermForecast stf) {
        stfVec.addElement(stf);
    }

    public ShortTermForecast[] getShortTermForecasts() {
        if (stfs == null) {
            stfs = new ShortTermForecast[stfVec.size()];
            stfs = (ShortTermForecast[]) stfVec.toArray(stfs);
        }
        return stfs;
    }

    public void addLongTermForecast(LongTermForecast ltf) {
        ltfVec.addElement(ltf);
    }

    public LongTermForecast[] getLongTermForecasts() {
        if (ltfs == null) {
            ltfs = new LongTermForecast[ltfVec.size()];
            ltfs = (LongTermForecast[]) ltfVec.toArray(ltfs);
        }
        return ltfs;
    }

    public void setNextUpdateDate(String nextUpdateDateStr) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LatestForecast.setNextUpdateDate]Param - next update date string : " + nextUpdateDateStr);
        }
        if (nextUpdateDateStr == null) {
            return;
        }
        nextUpdateDate = DateUtil.parse(nextUpdateDateStr);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LatestForecast.setNextUpdateDate] next update date : " + nextUpdateDate);
        }
    }

//    public void setNextUpdateDate(String nextUpdateDateStr) {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[LatestForecast.setNextUpdateDate]Param - next update date string : " + nextUpdateDateStr);
//        }
//        // Date nextUpdateDate2=parse(nextUpdateDateStr);
//
//        if (nextUpdateDateStr == null) {
//            return;
//        }
//        int index = nextUpdateDateStr.indexOf('T');
//        if (index < 0) {
//            return;
//        }
//        try {
//            String dateSrc = nextUpdateDateStr.substring(0, index);
//            int year = Integer.parseInt(dateSrc.substring(0, 4));
//            int month = Integer.parseInt(dateSrc.substring(5, 7));
//            int date = Integer.parseInt(dateSrc.substring(8, 10));
//            String timeSrc = nextUpdateDateStr.substring(index + 1);
//            int hour = Integer.parseInt(timeSrc.substring(0, 2));
//            int minute = Integer.parseInt(timeSrc.substring(3, 5));
//            int second = Integer.parseInt(timeSrc.substring(6, 8));
//            Calendar curCal = Calendar.getInstance();
//            curCal.set(year, month - 1, date, hour, minute, second);
//            nextUpdateDate = curCal.getTime();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public Date getNextUpdateDate() {
        return nextUpdateDate;
    }
}

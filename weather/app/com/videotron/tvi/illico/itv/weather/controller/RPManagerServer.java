package com.videotron.tvi.illico.itv.weather.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.comp.SAXHandler;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CityInfoHandler;
import com.videotron.tvi.illico.itv.weather.data.CityInfoSearchHandler;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.LatestForecastHandler;
import com.videotron.tvi.illico.itv.weather.data.ProxyInfo;
import com.videotron.tvi.illico.itv.weather.data.ServerInfo;
import com.videotron.tvi.illico.log.Log;

public class RPManagerServer implements RPManager {
    private static final String SERVER_PROTOCOL = "HTTP";
    private SAXParser parser;
    private DefaultHandler hdlrLatestForecast;
    private DefaultHandler hdlrCityInfo;

    /*********************************************************************************
     * Life cycle-related
     *********************************************************************************/
    public void init() {
        try {
            parser = SAXParserFactory.newInstance().newSAXParser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void dispose() {
        parser = null;
    }
    public void start() {
    }
    public void stop() {
    }
    /*********************************************************************************
     * Service-related
     *********************************************************************************/
    public CityInfo getCityInfoByPostalCode(String reqLangTag, String reqPostalCode) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode]start");
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode]Param - request postal code : ["+reqPostalCode+"]");
        }
        if (reqLangTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfoByPostalCode]Request language type is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (reqPostalCode == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfoByPostalCode]Request postal code is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (reqPostalCode.trim().length() < 3) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfoByPostalCode]Request postal code length is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String postalCode = reqPostalCode.trim().toUpperCase().substring(0, 3);
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode][CityInfoByPostalCode]Request postal code : ["+postalCode+"]");
        }
        DefaultHandler handler = new CityInfoHandler();
        String filePath = "postal/" + reqLangTag + "/" + postalCode + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode][CityInfoByPostalCode]Request URL info : ["+filePath+"]");
        }
        HttpURLConnection con = null;
        InputStream in = null;
        URL url = null;
        try {
            url = getUrl(filePath);
            try {
                if (url != null) {
                    con = (HttpURLConnection) url.openConnection();
                }
                if (con != null) {
                    int code = con.getResponseCode();
                    
                    if (Log.DEBUG_ON) {
                    	Log.printDebug("[RPMgrServer.getCityInfoByPostalCode] responseCode=" + code);
                    }
                    in = con.getInputStream();
                }
            } catch (Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][CityInfoByPostalCode]Cannot contact Weather HTTP server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
            }
            try {
                parser.parse(in, handler);
            } catch (Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfoByPostalCode]Cannot parse XML files received from Weather Server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
            }
        } catch (RPManagerException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        CityInfo cityInfo = null;
        if (saxHandler != null) {
            cityInfo = (CityInfo) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityInfoByPostalCode][CityInfoByPostalCode]Response city info : "+cityInfo);
        }
        return cityInfo;
    }
    public LatestForecast getLatestForecast(String reqLangTag, String reqLocId) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLatestForecast]start");
            Log.printDebug("[RPMgrServer.getLatestForecast]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrServer.getLatestForecast]Param - request location id : ["+reqLocId+"]");
        }
        if (reqLangTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Request language type is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (reqLocId == null || reqLocId.trim().length() == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Request location id is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (reqLocId.trim().length() == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Request location id length is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        DefaultHandler handler = getDefaultHandlerLatestForecast();
        String filePath = "forecast/" + reqLangTag + "/" + reqLocId + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLatestForecast]Request URL info : ["+filePath+"]");
        }
        HttpURLConnection con = null;
        InputStream in = null;
        URL url = null;
        try {
            url = getUrl(filePath);
            try{
                if (url != null) {
                    con = (HttpURLConnection) url.openConnection();
                }
                if (con != null) {
                	int code = con.getResponseCode();
                    
                    if (Log.DEBUG_ON) {
                    	Log.printDebug("[RPMgrServer.getLatestForecast] responseCode=" + code);
                    }
                    in = con.getInputStream();
                }
            }catch(Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][LatestForecast]Cannot contact Weather HTTP server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
            }
            try{
                parser.parse(in, handler);
            }catch(Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Cannot parse XML files received from Weather Server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
            }
        } catch (RPManagerException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        LatestForecast latestForecast = null;
        if (saxHandler != null) {
            latestForecast = (LatestForecast) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLatestForecast]Response latest forecast : "+latestForecast);
        }
        return latestForecast;
    }
    public CityInfo[] getCityList(String reqLangTag, String reqCityListTag) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityList]start");
            Log.printDebug("[RPMgrServer.getCityList]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrServer.getCityList]Param - request city list tag : ["+reqCityListTag+"]");
        }
        if (reqLangTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityList]Request language type is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (reqCityListTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityList]Request city list tag is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        DefaultHandler handler = getDefaultHandlerCityInfoSearch();
        String filePath = "forecast/cities/" + reqLangTag + "/cities-" + reqCityListTag + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getCityList]Request URL info : ["+filePath+"]");
        }
        HttpURLConnection con = null;
        InputStream in = null;
        URL url = null;
        try {
            url = getUrl(filePath);
            try{
                if (url != null) {
                    con = (HttpURLConnection) url.openConnection();
                }
                if (con != null) {
                	int code = con.getResponseCode();
                    
                    if (Log.DEBUG_ON) {
                    	Log.printDebug("[RPMgrServer.getCityList] responseCode=" + code);
                    }
                    in = con.getInputStream();
                }
            }catch(Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][CityList]Cannot contact Weather HTTP server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
            }
            try{
                parser.parse(in, handler);
            }catch(Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityList]Cannot parse XML files received from Weather Server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
            }
        } catch (RPManagerException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        CityInfo[] cityList = null;
        if (saxHandler != null) {
            cityList = (CityInfo[]) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLatestForecast]Response city list : "+cityList);
        }
        return cityList;
    }

    private URL getUrl(String fileName) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getURL]Start.");
        }
        if (fileName == null || fileName.trim().length() == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Request file name is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        ServerInfo serverInfo = DataManager.getInstance().getServerInfo();
        if (serverInfo == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"]["+fileName+"]Server info is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (fileName.startsWith("/")) {
            fileName = fileName.substring(1);
        }
        String host = serverInfo.getHost();
        if (host == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"]["+fileName+"]Host is invalid.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String urlInfo = SERVER_PROTOCOL + "://" + host + ":" + serverInfo.getPort();
        String fileInfo = "";
        String contextRoot = serverInfo.getContextRoot();
        if (contextRoot != null && contextRoot.trim().length() > 0) {
            if (!contextRoot.startsWith("/")) {
                contextRoot = "/" + contextRoot;
            }
            if (contextRoot.endsWith("/")) {
                contextRoot = contextRoot.substring(1);
            }
            fileInfo = contextRoot;
        }
        fileInfo += "/" + fileName;
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getURL]URL info : " + urlInfo);
            Log.printDebug("[RPMgrServer.getURL]File info : " + fileInfo);
        }
        ProxyInfo[] pInfos = serverInfo.getProxyInfos();
        URL url = null;
        if (pInfos == null || pInfos.length == 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]non-Proxy type.");
            }
            try {
                url = new URL(urlInfo + fileInfo);
            } catch (IOException e) {
                e.printStackTrace();
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"]["+fileName+"]Cannot create URL object by following exception ["+e.getMessage()+"].");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]non-Proxy URL : " + url);
            }
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]sProxy type.");
            }
            for (int i = 0; i < pInfos.length; i++) {
                if (pInfos[i] == null) {
                    continue;
                }
                String proxyHost = pInfos[i].getProxyHost();
                int proxyPort = pInfos[i].getProxyPort();
                try {
                    url = new URL(SERVER_PROTOCOL, proxyHost, proxyPort, urlInfo + fileInfo);
                } catch (IOException e) {
                    e.printStackTrace();
                    if (Log.ERROR_ON) {
                        Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"]["+fileName+"]Cannot create URL object by following exception ["+e.getMessage()+"].");
                    }
                    throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
                }
                if (url != null) {
                    break;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]Proxy URL : " + url);
            }
        }
        return url;
    }
    /*********************************************************************************
     * DefaultHandler-related
     *********************************************************************************/
    private DefaultHandler getDefaultHandlerLatestForecast() {
        if (hdlrLatestForecast == null) {
            hdlrLatestForecast = new LatestForecastHandler();
        }
        return hdlrLatestForecast;
    }
    private DefaultHandler getDefaultHandlerCityInfoSearch() {
        if (hdlrCityInfo == null) {
            hdlrCityInfo = new CityInfoSearchHandler();
        }
        return hdlrCityInfo;
    }
}

package com.videotron.tvi.illico.itv.weather.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.ui.WeatherPreferencesUI;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

public class WeatherPreferencesRenderer extends BaseRenderer {
    private Image imgActionBoxBasic;
    private Image imgActionBoxFocus;
    private Image imgNormalBoxBasic;
    private Image imgNormalBoxFocus;

    protected void prepareChild() {
        imgActionBoxBasic = ImageManager.getInstance().getImage("input_210.png", ImageManager.TYPE_TEMPORARY);
        imgActionBoxFocus = ImageManager.getInstance().getImage("input_210_foc2.png", ImageManager.TYPE_TEMPORARY);
        imgNormalBoxBasic = ImageManager.getInstance().getImage("input_210_n.png", ImageManager.TYPE_TEMPORARY);
        imgNormalBoxFocus = ImageManager.getInstance().getImage("input_210_foc1.png", ImageManager.TYPE_TEMPORARY);
    }

    protected void paintRenderer(Graphics g, UIComponent c) {
        /*********************************************************************************
         * Draw background
         *********************************************************************************/
        WeatherPreferencesUI scene = (WeatherPreferencesUI)c;
        boolean isRequested = scene.isRequested();
        if (!isRequested) {
            return;
        }
        int curArea = scene.getCurrentArea();
        boolean isActivatePreferencePopupAddon = scene.isActivatePreferencePopupAddon();
        CityInfo[] cityInfoFavs = scene.getFavoriteCityInfos();
        int curFavCityInfoIdx = scene.getFavoriteCityInfoIndex();

        String txtPreferences = DataCenter.getInstance().getString("TxtWeather.Preferences");
        if (txtPreferences != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtPreferences, 55, 113);
        }
        g.setFont(Rs.F17);
        g.setColor(Rs.C210210210);
        //Label - Favourite cities
        String txtFavouriteCities = DataCenter.getInstance().getString("TxtWeather.Favorite_Cities");
        if (txtFavouriteCities != null) {
            g.drawString(txtFavouriteCities, 54, 204);
        }
        //Label - Units preference
        if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_TEMPERATURE_UNIT) {
            String txtUnitsPreference = DataCenter.getInstance().getString("TxtWeather.Units_Preference");
            if (txtUnitsPreference != null) {
                g.drawString(txtUnitsPreference, 54, 162);
            }
        }
        //Label - Hometown
        if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_HOMETOWN) {
            String txtHometown = DataCenter.getInstance().getString("TxtWeather.Hometown");
            if (txtHometown != null) {
                g.drawString(txtHometown, 81, 238);
            }
        }
        //Label - Favorite city
        String txtFavoriteCity = DataCenter.getInstance().getString("TxtWeather.Favorite_City");
        if (txtFavoriteCity != null) {
            for (int i=0; i<DataManager.FAVORITE_CITY_MAX_COUNT; i++) {
                if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_FAVORITE_CITY || i != curFavCityInfoIdx) {
                    g.drawString(txtFavoriteCity + " " + (i + 1), 81, 274 + (i * 37));
                }
            }
        }
        g.setFont(Rs.F18);
        //Value - Units preference
        if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_TEMPERATURE_UNIT) {
            int curTemperatureUnit = scene.getTemperatureUnit();
            if (imgActionBoxBasic != null) {
                g.drawImage(imgActionBoxBasic, 339, 141, c);
            }
            String curTemperatureUnitValue = null;
            switch(curTemperatureUnit) {
                case DataManager.TEMPERATURE_UNIT_ID_C:
                    curTemperatureUnitValue = "°C";
                    break;
                case DataManager.TEMPERATURE_UNIT_ID_F:
                    curTemperatureUnitValue = "°F";
                    break;
            }
            if (curArea == WeatherPreferencesUI.AREA_TEMPERATURE_UNIT) {
                if (imgActionBoxFocus != null) {
                    g.drawImage(imgActionBoxFocus, 339, 141, c);
                }
                g.setColor(Rs.C255255255);
            } else {
                g.setColor(Rs.C182182182);
            }
            if (curTemperatureUnitValue != null) {
                g.drawString(curTemperatureUnitValue, 352, 162);
            }
        }
        //Value - Home town
        String txtAdd = DataCenter.getInstance().getString("TxtWeather.Add");
        if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_HOMETOWN) {
            CityInfo cityInfoHometown = scene.getHometownCityInfo();
            Color cCityNameHometown = null;
            String cityNameHometown = null;
            if (cityInfoHometown != null) {
                cityNameHometown = cityInfoHometown.getDisplayName();
                if (cityNameHometown != null) {
                    cityNameHometown = TextUtil.shorten(cityNameHometown, Rs.FM18, 192);
                }
                if (imgActionBoxBasic != null) {
                    g.drawImage(imgActionBoxBasic, 339, 217, c);
                }
                if (curArea == WeatherPreferencesUI.AREA_HOMETOWN) {
                    if (imgActionBoxFocus != null) {
                        g.drawImage(imgActionBoxFocus, 339, 217, c);
                    }
                    cCityNameHometown = Rs.C255255255;
                } else {
                    cCityNameHometown = Rs.C182182182;
                }
            } else {
                cityNameHometown = txtAdd;
                if (imgNormalBoxBasic != null) {
                    g.drawImage(imgNormalBoxBasic, 339, 217, c);
                }
                if (curArea == WeatherPreferencesUI.AREA_HOMETOWN) {
                    if (imgNormalBoxFocus != null) {
                        g.drawImage(imgNormalBoxFocus, 339, 217, c);
                    }
                    cCityNameHometown = Rs.C255255255;
                } else {
                    cCityNameHometown = Rs.C182182182;
                }
            }
            if (cityNameHometown != null) {
                if (cCityNameHometown != null) {
                    g.setColor(cCityNameHometown);
                }
                g.drawString(cityNameHometown, 352, 238);
            }
        }
        //Value - Favorite city
        if (cityInfoFavs != null) {
            g.setColor(Rs.C182182182);
            for (int i=0; i<cityInfoFavs.length; i++) {
                if (curArea == WeatherPreferencesUI.AREA_FAVORITE_CITY && curFavCityInfoIdx == i) {
                    continue;
                }
                Image imgInputFavCity = null;
                String favCityName = null;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherPreferencesRenderer.paintRenderer]cityInfoFavs["+i+"] : "+cityInfoFavs[i]);
                }
                if (cityInfoFavs[i] != null) {
                    favCityName = cityInfoFavs[i].getDisplayName();
                    if (favCityName != null) {
                        favCityName = TextUtil.shorten(favCityName, Rs.FM18, 192);
                    } else {
                        favCityName = "N/A";
                    }
                    imgInputFavCity = imgActionBoxBasic;
                } else {
                    favCityName = txtAdd;
                    imgInputFavCity = imgNormalBoxBasic;
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherPreferencesRenderer.paintRenderer]favCityName["+i+"] : "+favCityName);
                }
                if (imgInputFavCity != null) {
                    g.drawImage(imgInputFavCity, 339, 254 + (i * WeatherPreferencesUI.FAVORITE_CITY_GAP), c);
                }
                if (favCityName != null) {
                    g.drawString(favCityName, 352, 275 + (i * WeatherPreferencesUI.FAVORITE_CITY_GAP));
                }
            }
            if (!isActivatePreferencePopupAddon || curArea != WeatherPreferencesUI.AREA_FAVORITE_CITY) {
                if (curArea == WeatherPreferencesUI.AREA_FAVORITE_CITY && curFavCityInfoIdx < cityInfoFavs.length) {
                    g.setColor(Rs.C255255255);
                    String favCityName = null;
                    if (cityInfoFavs[curFavCityInfoIdx] != null) {
                        favCityName = cityInfoFavs[curFavCityInfoIdx].getDisplayName();
                        if (favCityName != null) {
                            favCityName = TextUtil.shorten(favCityName, Rs.FM18, 192);
                        }
                        if (imgActionBoxBasic != null) {
                            g.drawImage(imgActionBoxBasic, 339, 254 + (curFavCityInfoIdx * WeatherPreferencesUI.FAVORITE_CITY_GAP), c);
                        }
                        if (imgActionBoxFocus != null) {
                            g.drawImage(imgActionBoxFocus, 339, 254 + (curFavCityInfoIdx * WeatherPreferencesUI.FAVORITE_CITY_GAP), c);
                        }
                    } else {
                        favCityName = txtAdd;
                        if (imgNormalBoxBasic != null) {
                            g.drawImage(imgNormalBoxBasic, 339, 254 + (curFavCityInfoIdx * WeatherPreferencesUI.FAVORITE_CITY_GAP), c);
                        }
                        if (imgNormalBoxFocus != null) {
                            g.drawImage(imgNormalBoxFocus, 339, 254 + (curFavCityInfoIdx * WeatherPreferencesUI.FAVORITE_CITY_GAP), c);
                        }
                    }
                    if (favCityName != null) {
                        g.drawString(favCityName, 352, 275 + (curFavCityInfoIdx * WeatherPreferencesUI.FAVORITE_CITY_GAP));
                    }
                }
            }
        }
    }
}

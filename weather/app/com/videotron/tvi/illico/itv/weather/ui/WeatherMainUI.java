package com.videotron.tvi.illico.itv.weather.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Rectangle;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.comp.CurrentWeatherInHomeTown;
import com.videotron.tvi.illico.itv.weather.comp.FavoriteCityDashboard;
import com.videotron.tvi.illico.itv.weather.comp.FavoriteCityDashboardListener;
import com.videotron.tvi.illico.itv.weather.comp.FavoriteCityWidget;
import com.videotron.tvi.illico.itv.weather.comp.MenuTree;
import com.videotron.tvi.illico.itv.weather.comp.MenuTreeDescriptor;
import com.videotron.tvi.illico.itv.weather.comp.MenuTreeItem;
import com.videotron.tvi.illico.itv.weather.comp.MenuTreeListener;
import com.videotron.tvi.illico.itv.weather.comp.PopupQuestion;
import com.videotron.tvi.illico.itv.weather.comp.PopupSearch;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.controller.RPManagerException;
import com.videotron.tvi.illico.itv.weather.controller.SceneManager;
import com.videotron.tvi.illico.itv.weather.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.gui.WeatherMainRenderer;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;

public class WeatherMainUI extends BaseUI implements MenuTreeListener, FavoriteCityDashboardListener, MenuListener,
        ChannelEventListener {
    private static final long serialVersionUID = -4152969367611819304L;
    private DataManager dMgr = DataManager.getInstance();
    private CommunicationManager commMgr = CommunicationManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private MenuTree mt;
    private MenuTreeItem mtItemShortTermForecast;
    private MenuTreeItem mtItemLongTermForecast;
    private MenuTreeItem mtItemTuneWeatherCh;
    private MenuTreeItem mtItemPreferences;
    private MenuTreeItem[] mtItemFull;
    private MenuTreeItem[] mtItemTuned;
    private MenuTreeDescriptor mtDesc;
    private Footer footer;
    private PopupSearch popSearch;
    private CurrentWeatherInHomeTown cWeather;
    private FavoriteCityDashboard favCityDash;
    /*********************************************************************************
     * Option-related
     *********************************************************************************/
    private OptionScreen optScr;
    private static final MenuItem OPTION_PREFERENCES = new MenuItem("TxtWeather.Option_Preference");
    private static final MenuItem OPTION_HELP = new MenuItem("TxtWeather.Option_Help");
    /*********************************************************************************
     * TxtKey-related
     *********************************************************************************/
    private final String txtKeyMenuTreeTitle = "TxtWeather.Weather";
    private Image imgMenuTreeIcon;
    private static final int MENU_ITEM_ID_SHORT_TERM_FORECAST = 0;
    private static final int MENU_ITEM_ID_LONG_TERM_FORECAST = 1;
    private static final int MENU_ITEM_ID_TURN_TO_THE_WEATHER_NETWORK = 2;
    private static final int MENU_ITEM_ID_PREFERENCES = 3;
    private static final String[] TXT_KEY_MENU_TITLES = { "TxtWeather.Short_Term_Forecast",
            "TxtWeather.Long_Term_Forecast", "TxtWeather.Turn_To_The_Weather_Network", "TxtWeather.Preferences" };
    private static final String[] TXT_KEY_MENU_DESCS = { "TxtWeather.Short_Term_Forecast_Desc",
            "TxtWeather.Long_Term_Forecast_Desc", "TxtWeather.Turn_To_The_Weather_Network_Desc",
            "TxtWeather.Preferences_Desc" };
    private int curMenuIdx;
    private boolean isWeatherChannel;

    private static final int AREA_MENU_TREE = 0;
    private static final int AREA_DASHBOARD = 1;

    private ClickingEffect clickEffect;
    
    protected void initScene() {
        renderer = new WeatherMainRenderer();
        setRenderer(renderer);
        if (mtItemShortTermForecast == null) {
            mtItemShortTermForecast = new MenuTreeItem();
            mtItemShortTermForecast.setMenuTreePrimaryKey(MENU_ITEM_ID_SHORT_TERM_FORECAST);
            mtItemShortTermForecast.setMenuTreeTitleTextKey(TXT_KEY_MENU_TITLES[MENU_ITEM_ID_SHORT_TERM_FORECAST]);
        }
        if (mtItemLongTermForecast == null) {
            mtItemLongTermForecast = new MenuTreeItem();
            mtItemLongTermForecast.setMenuTreePrimaryKey(MENU_ITEM_ID_LONG_TERM_FORECAST);
            mtItemLongTermForecast.setMenuTreeTitleTextKey(TXT_KEY_MENU_TITLES[MENU_ITEM_ID_LONG_TERM_FORECAST]);
        }
        if (mtItemTuneWeatherCh == null) {
            mtItemTuneWeatherCh = new MenuTreeItem();
            mtItemTuneWeatherCh.setMenuTreePrimaryKey(MENU_ITEM_ID_TURN_TO_THE_WEATHER_NETWORK);
            mtItemTuneWeatherCh.setMenuTreeTitleTextKey(TXT_KEY_MENU_TITLES[MENU_ITEM_ID_TURN_TO_THE_WEATHER_NETWORK]);
        }
        if (mtItemPreferences == null) {
            mtItemPreferences = new MenuTreeItem();
            mtItemPreferences.setMenuTreePrimaryKey(MENU_ITEM_ID_PREFERENCES);
            mtItemPreferences.setMenuTreeTitleTextKey(TXT_KEY_MENU_TITLES[MENU_ITEM_ID_PREFERENCES]);
        }
        if (mtItemFull == null) {
            mtItemFull = new MenuTreeItem[] { mtItemShortTermForecast, mtItemLongTermForecast, mtItemTuneWeatherCh,
                    mtItemPreferences };
        }
        if (mtItemTuned == null) {
            mtItemTuned = new MenuTreeItem[] { mtItemShortTermForecast, mtItemLongTermForecast, mtItemPreferences };
        }
        if (imgMenuTreeIcon == null) {
            imgMenuTreeIcon = ImageManager.getInstance().getImage("weather.png", ImageManager.TYPE_TEMPORARY);
        }
        if (mt == null) {
            mt = new MenuTree();
            mt.init();
            mt.setBounds(0, 0, 960, 540);
            mt.setMenuTreeTitleKey(txtKeyMenuTreeTitle);
            mt.setMenuTreeIconImage(imgMenuTreeIcon);
            mt.setMenuTreeItems(mtItemFull);
            mt.setMenuTreeListener(this);
        }
        if (mtDesc == null) {
            mtDesc = new MenuTreeDescriptor();
            mtDesc.init();
            mtDesc.setBounds(373, 79, 204, 166);
            add(mtDesc);
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (cWeather == null) {
            cWeather = new CurrentWeatherInHomeTown();
            cWeather.init();
            cWeather.setBounds(375, 232, 540, 151);
            add(cWeather);
        }
        if (favCityDash == null) {
            favCityDash = new FavoriteCityDashboard();
            favCityDash.init();
            favCityDash.setBounds(353, 346, 596, 119);
            favCityDash.setListener(this);
            add(favCityDash);
        }
        if (optScr == null) {
            optScr = new OptionScreen();
        }
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
            clickEffect.setClipBounds(0, 0, 360, 540);
        }
    }

    protected void disposeScene() {
        clickEffect = null;
        optScr = null;
        if (popSearch != null) {
            popSearch.dispose();
            popSearch = null;
        }
        if (favCityDash != null) {
            remove(favCityDash);
            favCityDash.removeListener();
            favCityDash.dispose();
            favCityDash = null;
        }
        if (cWeather != null) {
            remove(cWeather);
            cWeather.dispose();
            cWeather = null;
        }
        if (mtDesc != null) {
            remove(mtDesc);
            mtDesc.dispose();
            mtDesc = null;
        }
        if (mt != null) {
            remove(mt);
            mt.removeMenuTreeListener();
            mt.dispose();
            mt = null;
        }
        imgMenuTreeIcon = null;
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        renderer = null;
    }

    protected void startScene(boolean resetScene, String[] params) {
        prepare();
        resetMenuTreeItem();
        if (resetScene) {
            curMenuIdx = 0;
            setComponentArea(AREA_MENU_TREE);
            AlphaEffect fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
            fadeIn.start();
            if (mt != null) {
                add(mt);
                mt.start(true);
            }
        } else {
            if (mt != null) {
                add(mt);
            }
        }
        if (cWeather != null) {
            cWeather.start();
        }
        if (favCityDash != null) {
            favCityDash.start();
        }
        setMenuTreeDescription();
        if (footer != null) {
            footer.reset();
            footer.addButton(PreferenceService.BTN_EXIT, "TxtWeather.Exit");
            footer.addButton(PreferenceService.BTN_SEARCH, "TxtWeather.City_Search");
            footer.addButton(PreferenceService.BTN_D, "TxtWeather.Options");
        }
        if (resetScene) {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    commMgr.requestChangeCurrentChannel();
                    commMgr.requestResizeScreen(587, 72, 322, 181);
                }
            });
        } else {
            commMgr.requestResizeScreen(587, 72, 322, 181);
        }
        commMgr.requestAddChannelEventListener(this);
    }

    protected void stopScene() {
        if (mt != null) {
            remove(mt);
        }
        commMgr.requestRemoveChannelEventListener(this);
        if (optScr != null) {
            optScr.stop();
        }
        if (popSearch != null) {
            popSearch.removePopupListener();
            PopupController.getInstance().closePopup(popSearch);
        }
        closeAllPopup();
        if (favCityDash != null) {
            favCityDash.stop();
        }
    }

    protected boolean keyAction(int keyCode) {
        if (keyCode == KeyCodes.LAST) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherMainUI.keyAction]keyCode is KeyCodes.LAST.");
            }
            boolean isCalledFromMenu = commMgr.isCalledFromMenu();
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherMainUI.keyAction]isCalledFromMenu : " + isCalledFromMenu);
            }
            if (isCalledFromMenu) {
                commMgr.requestStartUnboundApplication("Menu", null);
                return true;
            }
            return true;
        }
        boolean isMenuTreeFocused = false;
        boolean isOtherCitiesFocused = false;
        if (mt != null) {
            isMenuTreeFocused = mt.isFocusComponent();
        }
        if (favCityDash != null) {
            isOtherCitiesFocused = favCityDash.isFocusComponent();
        }
        switch (keyCode) {
            case Rs.KEY_EXIT:
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.Exit");
                }
                CommunicationManager.getInstance().requestExitToChannel();
                return true;
            case KeyCodes.SEARCH:
            	if (optScr != null) {
            		optScr.stop();
            	}
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.City_Search");
                }
                if (popSearch == null) {
                    popSearch = new PopupSearch();
                    popSearch.setBounds(0, 0, 960, 540);
                }
                popSearch.setPopupListener(getPopupAdapterSearch());
                PopupController.getInstance().openPopup(popSearch);
                return true;
            case KeyCodes.COLOR_D:
                if (footer != null) {
                    footer.clickAnimation("TxtWeather.Options");
                }
                MenuItem root = new MenuItem("WeatherMain");
                root.add(OPTION_PREFERENCES);
                root.add(OPTION_HELP);
                optScr.start(root, this);
                return true;
        }
        if (isMenuTreeFocused) {
            return mt.keyAction(keyCode);
        }
        if (isOtherCitiesFocused) {
            return favCityDash.keyAction(keyCode);
        }
        return false;
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public void setMenuTreeDescription() {
        if (mtDesc != null) {
            mtDesc.setTitle(DataCenter.getInstance().getString(TXT_KEY_MENU_TITLES[curMenuIdx]));
            mtDesc.setDescription(DataCenter.getInstance().getString(TXT_KEY_MENU_DESCS[curMenuIdx]));
        }
    }

    /*********************************************************************************
     * MenuTreeListener-implemented
     *********************************************************************************/
    public void lostMenuTreeFocus() {
        boolean isReadyDashboard = false;
        if (favCityDash != null) {
            isReadyDashboard = favCityDash.isReadyComponent();
        }
        if (isReadyDashboard) {
            setComponentArea(AREA_DASHBOARD);
        } else {
            setComponentArea(AREA_MENU_TREE);
        }
        setMenuTreeDescription();
    }

    public void selectedMenuTreeItem(MenuTreeItem reqMenuTreeItem, Rectangle selItemArea) {
        if (reqMenuTreeItem == null) {
            return;
        }
        if (selItemArea != null) {
            if (clickEffect != null) {
                clickEffect.start(selItemArea.x, selItemArea.y, selItemArea.width, selItemArea.height);
            }
        }
        int pKey = reqMenuTreeItem.getMenuTreePrimaryKey();
        switch (pKey) {
        case MENU_ITEM_ID_SHORT_TERM_FORECAST:
            String[] shortTermLocIds = null;
            try {
                shortTermLocIds = dMgr.getRegisteredCityLocationIds();
            } catch (RPManagerException e) {
                e.printStackTrace();
                CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                return;
            }
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_SHORT_TERM_FORECAST, true, shortTermLocIds);
            break;
        case MENU_ITEM_ID_LONG_TERM_FORECAST:
            String[] longTermLocIds = null;
            try {
                longTermLocIds = dMgr.getRegisteredCityLocationIds();
            } catch (RPManagerException e) {
                e.printStackTrace();
                CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                return;
            }
            if (isWeatherChannel()) {
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LONG_TERM_FORECAST, true,
                        longTermLocIds);
            } else {
                String txtTuneToTheWeatherNetwork = DataCenter.getInstance().getString(
                        "TxtWeather.TUNE_TO_THE_WEATHER_NETWORK");
                String txtDesc = DataCenter.getInstance().getString("TxtWeather.TUNE_TO_THE_WEATHER_NETWORK_LONG_TERM");
                openPopupQuestion(getPopupAdapterLongTermForecast(), txtTuneToTheWeatherNetwork, txtDesc);
            }
            break;
        case MENU_ITEM_ID_TURN_TO_THE_WEATHER_NETWORK:
            tuneToWeatherChannel();
            break;
        case MENU_ITEM_ID_PREFERENCES:
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_WEATHER_PREFERENCES, true, null);
            break;
        }
    }

    public void changedMenuTreeItem(MenuTreeItem changedItem) {
        if (changedItem == null) {
            return;
        }
        curMenuIdx = changedItem.getMenuTreePrimaryKey();
        setMenuTreeDescription();
        repaint();
    }

    /*********************************************************************************
     * FavoriteCitiesListener-implemented
     *********************************************************************************/
    public void lostDashboardFocus() {
        setComponentArea(AREA_MENU_TREE);
        setMenuTreeDescription();
    }

    public void selectedFavoriteCityWidget(FavoriteCityWidget reqWidget) {
        if (reqWidget == null) {
            return;
        }
        int bennerType = reqWidget.getBannerType();
        if (bennerType == FavoriteCityWidget.BANNER_TYPE_OPTION) {
            if (popSearch == null) {
                popSearch = new PopupSearch();
                popSearch.setBounds(0, 0, 960, 540);
            }
            popSearch.setPopupListener(getPopupAdapterSearchAddFavoriteCity());
            PopupController.getInstance().openPopup(popSearch);
            return;
        } else if (bennerType == FavoriteCityWidget.BANNER_TYPE_NORMAL) {
            String[] regCityLocIds = null;
            try {
                regCityLocIds = dMgr.getRegisteredCityLocationIds();
            } catch (RPManagerException e) {
                e.printStackTrace();
                CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                return;
            }
            String selectedLocId = null;
            LatestForecast selectedLatestForecast = reqWidget.getLatestForecast();
            if (selectedLatestForecast != null) {
                CityInfo selectedCityInfo = selectedLatestForecast.getCityInfo();
                if (selectedCityInfo != null) {
                    selectedLocId = selectedCityInfo.getLocationId();
                }
            }
            int selectedLocIdIdx = 0;
            if (selectedLocId != null) {
                for (int i = 0; i < regCityLocIds.length; i++) {
                    if (regCityLocIds[i] == null) {
                        continue;
                    }
                    if (regCityLocIds[i].equalsIgnoreCase(selectedLocId)) {
                        selectedLocIdIdx = i;
                        break;
                    }
                }
            }
            String[] shortTermLocIds = null;
            if (selectedLocIdIdx != 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherMainUI.selectedBanner]selectedLocIdIdx : [" + selectedLocIdIdx
                            + "]");
                }
                int regCityLocIdsLth = regCityLocIds.length;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherMainUI.selectedBanner]regCityLocIdsLth : [" + regCityLocIdsLth
                            + "]");
                }
                shortTermLocIds = new String[regCityLocIdsLth];
                for (int i = 0; i < regCityLocIdsLth; i++) {
                    int regCityLocIdsIdx = (selectedLocIdIdx + i) % regCityLocIdsLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[WeatherMainUI.selectedBanner]change index i - regCityLocIdsIdx : ["
                                + i + " - " + regCityLocIdsIdx + "]");
                    }
                    shortTermLocIds[i] = regCityLocIds[regCityLocIdsIdx];
                }
            } else {
                shortTermLocIds = regCityLocIds;
            }
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_SHORT_TERM_FORECAST, true, shortTermLocIds);
        }
    }

    public void setComponentArea(int reqArea) {
        switch (reqArea) {
        case AREA_MENU_TREE:
            if (mt != null) {
                mt.setFocusComponent(true);
            }
            if (favCityDash != null) {
                favCityDash.setFocusComponent(false);
            }
            break;
        case AREA_DASHBOARD:
            if (mt != null) {
                mt.setFocusComponent(false);
            }
            if (favCityDash != null) {
                favCityDash.setFocusComponent(true);
            }
            break;
        }
        repaint();
    }

    /*********************************************************************************
     * MenuListener-implemented
     *********************************************************************************/
    public void canceled() {
        if (optScr != null) {
            optScr.stop();
        }
    }

    public void selected(MenuItem item) {
        if (optScr != null) {
            optScr.stop();
        }
        if (item == null) {
            return;
        }
        if (item.equals(OPTION_PREFERENCES)) {
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_WEATHER_PREFERENCES, true, null);
        } else if (item.equals(OPTION_HELP)) {
            String appNameHelp = DataCenter.getInstance().getString("APP_NAME_HELP");
            CommunicationManager.getInstance().requestStartUnboundApplication(appNameHelp, new String[]{null, Rs.APP_NAME});
        }
    }

    private void resetMenuTreeItem() {
        if (mt != null) {
            boolean isWeatherChannelByCurrentLanguage = isWeatherChannel();
            if (isWeatherChannel == isWeatherChannelByCurrentLanguage) {
                return;
            }
            isWeatherChannel = isWeatherChannelByCurrentLanguage;
            if (isWeatherChannelByCurrentLanguage) {
                mt.setMenuTreeItems(mtItemTuned);
            } else {
                mt.setMenuTreeItems(mtItemFull);
            }
        }
    }

    private void tuneToWeatherChannel() {
        String callLetter = dMgr.getWeatherChannelCallLetter();
        if (callLetter != null) {
            commMgr.requestChangeChannel(callLetter);
        }
    }

    private boolean isWeatherChannel() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherMainUI.isWeatherChannel]start.");
        }
        String weatherChlCallLetter = dMgr.getWeatherChannelCallLetter();
        if (weatherChlCallLetter == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherMainUI.isWeatherChannel]Weather channel call letter is null. return false.");
            }
            return false;
        }
        String curCallLetter = commMgr.requestCurrentCallLetter();
        if (curCallLetter == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherMainUI.isWeatherChannel]Current channel call letter is null. return false.");
            }
            return false;
        }
        boolean result = false;
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherMainUI.isWeatherChannel]Weather channel call letter : ["
                    + weatherChlCallLetter + "]");
            Log.printDebug("[WeatherMainUI.isWeatherChannel]Current channel call letter : [" + curCallLetter
                    + "]");
        }
        if (weatherChlCallLetter.equals(curCallLetter)) {
            result = true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherMainUI.isWeatherChannel]is weather channel : " + result);
        }
        return result;
    }

    /*********************************************************************************
     * ChannelEventListener-implemented
     *********************************************************************************/
    public void selectionRequested(int id, short type) throws RemoteException {
        resetMenuTreeItem();
    }

    public void selectionBlocked(int id, short type) throws RemoteException {
    }

    /*********************************************************************************
     * PopupAdapter-related
     *********************************************************************************/
    private PopupAdapter getPopupAdapterSearch() {
        PopupAdapter popAdaptSearch = new PopupAdapter() {
            public void popupOK(PopupEvent popupEvent) {
                PopupSearch pop = (PopupSearch) popupEvent.getPopup();
                String selectedLocId = null;
                CityInfo selectedCityInfo = pop.getSelectedCityInfo();
                if (selectedCityInfo != null) {
                    selectedLocId = selectedCityInfo.getLocationId();
                }
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
                if (selectedLocId == null) {
                    return;
                }
                String[] regCityLocIds = null;
                try {
                    regCityLocIds = dMgr.getRegisteredCityLocationIds();
                } catch (RPManagerException e) {
                    e.printStackTrace();
                    CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                    return;
                }
                int tempValue = getLocationIdIndex(regCityLocIds, selectedLocId);
                String[] locIds = null;
                if (tempValue < 0) {
                    String[] tempLocIds = (String[]) regCityLocIds.clone();
                    locIds = new String[tempLocIds.length + 1];
                    locIds[0] = selectedLocId;
                    for (int i = 1; i < locIds.length; i++) {
                        locIds[i] = tempLocIds[i - 1];
                    }
                } else {
                    int regCityLocIdsLth = regCityLocIds.length;
                    locIds = new String[regCityLocIdsLth];
                    for (int i = 0; i < regCityLocIdsLth; i++) {
                        int regCityLocIdsIdx = (tempValue + i) % regCityLocIdsLth;
                        locIds[i] = regCityLocIds[regCityLocIdsIdx];
                    }
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_SHORT_TERM_FORECAST, true, locIds);
            }

            public void popupCancel(PopupEvent popupEvent) {
                PopupSearch pop = (PopupSearch) popupEvent.getPopup();
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
            }

            private int getLocationIdIndex(String[] srcLocIds, String targetLocId) {
                if (srcLocIds == null || targetLocId == null) {
                    return -1;
                }
                for (int i = 0; i < srcLocIds.length; i++) {
                    if (srcLocIds[i] == null) {
                        continue;
                    }
                    if (srcLocIds[i].equals(targetLocId)) {
                        return i;
                    }
                }
                return -1;
            }
        };
        return popAdaptSearch;
    }

    private PopupAdapter getPopupAdapterSearchAddFavoriteCity() {
        PopupAdapter popAdaptSearch = new PopupAdapter() {
            public void popupOK(PopupEvent popupEvent) {
                PopupSearch pop = (PopupSearch) popupEvent.getPopup();
                CityInfo selectedCityInfo = pop.getSelectedCityInfo();
                String selectedLocId = null;
                if (selectedCityInfo != null) {
                    selectedLocId = selectedCityInfo.getLocationId();
                }
                pop.removePopupListener();
                PopupController.getInstance().closePopup(pop);
                if (selectedLocId == null) {
                    return;
                }
                boolean isDuplicatedLocId = dMgr.isDuplicatedLocId(selectedLocId);
                if (isDuplicatedLocId) {
                    String txtDuplicatedCityTitle = DataCenter.getInstance().getString(
                            "TxtWeather.Duplicated_City_Title");
                    String txtDuplicatedCityCont = DataCenter.getInstance().getString(
                            "TxtWeather.Duplicated_City_Content");
                    openPopupInfo(txtDuplicatedCityTitle, txtDuplicatedCityCont);
                    return;
                }
                boolean resSetFavoriteCityLocationId = dMgr.setFavoriteCityLocationIdAtVacant(selectedLocId);
                if (resSetFavoriteCityLocationId) {
                    if (favCityDash != null) {
                        favCityDash.resetFavorite();
                    }
                }
            }

            public void popupCancel(PopupEvent popupEvent) {
                PopupSearch pop = (PopupSearch) popupEvent.getPopup();
                pop.removePopupListener();
                PopupController.getInstance().closePopup(pop);
            }
        };
        return popAdaptSearch;
    }

    private PopupAdapter getPopupAdapterLongTermForecast() {
        PopupAdapter popAdaptLongTermForecast = new PopupAdapter() {
            public void popupOK(PopupEvent popupEvent) {
                PopupQuestion pop = (PopupQuestion) popupEvent.getPopup();
                int curButton = pop.getCurrentButton();
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
                if (curButton == PopupQuestion.BUTTON_YES) {
                    tuneToWeatherChannel();
                    String[] regCityLocIds = null;
                    try {
                        regCityLocIds = dMgr.getRegisteredCityLocationIds();
                    } catch (RPManagerException e) {
                        e.printStackTrace();
                        CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                        return;
                    }
                    SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LONG_TERM_FORECAST, true,
                            regCityLocIds);
                }
            }

            public void popupCancel(PopupEvent popupEvent) {
                PopupQuestion pop = (PopupQuestion) popupEvent.getPopup();
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
            }
        };
        return popAdaptLongTermForecast;
    }
}

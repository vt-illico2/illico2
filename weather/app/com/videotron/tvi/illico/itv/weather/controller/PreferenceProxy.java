package com.videotron.tvi.illico.itv.weather.controller;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    private static PreferenceProxy instance;
    private PreferenceService prefSvc;

    public static final String UPP_KEY_HOME_TOWN_LOCATION_ID = "WEATHER_HOME_TOWN_LOCATION_ID";
    public static final String UPP_KEY_FAVORITE_CITY_LOCATION_IDS = "WEATHER_FAVORITE_CITY_LOCATION_IDS";
    public static final String UPP_KEY_TEMPERATURE_UNIT = "WEATHER_TEMPERATURE_UNIT";

    private static final int UPP_INDEX_COUNT = 4;
    private static final int UPP_INDEX_LANGUAGE = 0;
//    private static final int UPP_INDEX_POSTAL_CODE = 1;
    private static final int UPP_INDEX_HOME_TOWN_LOCATION_ID = 1;
    private static final int UPP_INDEX_FAVORITE_CITY_LOCATION_IDS = 2;
    private static final int UPP_INDEX_TEMPERATURE_UNIT = 3;
    private String[] prefNames;
    private String[] prefValues;

    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }
    private PreferenceProxy() {
    }
    public void init() {
        String defaultPostalCode = DataCenter.getInstance().getString("DEFAULT_POSTAL_CODE");
        if (defaultPostalCode == null) {
            defaultPostalCode = "";
        }
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
//        prefNames[UPP_INDEX_POSTAL_CODE] = PreferenceNames.POSTAL_CODE;
        prefNames[UPP_INDEX_HOME_TOWN_LOCATION_ID] = UPP_KEY_HOME_TOWN_LOCATION_ID;
        prefNames[UPP_INDEX_FAVORITE_CITY_LOCATION_IDS] = UPP_KEY_FAVORITE_CITY_LOCATION_IDS;
        prefNames[UPP_INDEX_TEMPERATURE_UNIT] = UPP_KEY_TEMPERATURE_UNIT;

        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_FRENCH;
//        prefValues[UPP_INDEX_POSTAL_CODE] = defaultPostalCode;
        prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID] = "";
        prefValues[UPP_INDEX_FAVORITE_CITY_LOCATION_IDS] = "";
        prefValues[UPP_INDEX_TEMPERATURE_UNIT] = DataManager.TEMPERATURE_UNIT_C;
        getPreferenceService();
    }
    public void dispose() {
        if (prefSvc != null) {
            try{
                prefSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception ignore) {
            }
            prefSvc = null;
        }
        prefNames = null;
        prefValues = null;
        instance = null;
    }
    public void start() {
    }
    public void stop() {
    }
    private void checkPreferenceService() {
        if (prefSvc == null) {
            setPreferenceService();
        }
    }
    private void setPreferenceService() {
        try {
            String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
            prefSvc = (PreferenceService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
        } catch (Exception e) {
            if (Log.INFO_ON) {
                Log.printInfo("[PrefProxy.setPreferenceService]not bound - " + PreferenceService.IXC_NAME);
            }
        }
        if (prefSvc != null) {
            String[] uppData = null;
            try {
                uppData = prefSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues) ;
                receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
//                receiveUpdatedPreference(PreferenceNames.POSTAL_CODE, uppData[UPP_INDEX_POSTAL_CODE]);
                receiveUpdatedPreference(UPP_KEY_HOME_TOWN_LOCATION_ID, uppData[UPP_INDEX_HOME_TOWN_LOCATION_ID]);
                receiveUpdatedPreference(UPP_KEY_FAVORITE_CITY_LOCATION_IDS, uppData[UPP_INDEX_FAVORITE_CITY_LOCATION_IDS]);
                receiveUpdatedPreference(UPP_KEY_TEMPERATURE_UNIT, uppData[UPP_INDEX_TEMPERATURE_UNIT]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private PreferenceService getPreferenceService() {
        checkPreferenceService();
        return prefSvc;
    }
    /**
     * invoked when the value of the preference has changed.
     * @param name
     *            Updated Preference Name
     * @param value
     *            Updated value of Preference
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            prefValues[UPP_INDEX_LANGUAGE] = value;
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
            DataManager.getInstance().updatedPreferenceLanguage();
        } else if (name.equals(PreferenceNames.POSTAL_CODE)) {
//            prefValues[UPP_INDEX_POSTAL_CODE] = value;
        } else if (name.equals(UPP_KEY_HOME_TOWN_LOCATION_ID)) {
            prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID] = value;
        } else if (name.equals(UPP_KEY_FAVORITE_CITY_LOCATION_IDS)) {
            prefValues[UPP_INDEX_FAVORITE_CITY_LOCATION_IDS] = value;
        } else if (name.equals(UPP_KEY_TEMPERATURE_UNIT)) {
            prefValues[UPP_INDEX_TEMPERATURE_UNIT] = value;
        }
    }
    /*********************************************************************************
     * Return Data-related
     *********************************************************************************/
    public String getLanguage() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_LANGUAGE];
    }
    public String getPostalCode() {
        PreferenceService pSvc = getPreferenceService();
        String postalCode = null;
        if (pSvc != null) {
            try{
                postalCode = pSvc.getPostalCode();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return postalCode;
    }
//    public boolean existPostalCodeValue() {
//        if (prefValues[UPP_INDEX_POSTAL_CODE] == null) {
//            return false;
//        }
//        if ((prefValues[UPP_INDEX_POSTAL_CODE].trim()).length() <= 0) {
//            return false;
//        }
//        return true;
//    }
    public String getHomeTownLocationId() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID];
    }
    public boolean setHomeTownLocationId(String reqValue) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setHomeTownLocationId] reqValue : "+reqValue);
        }
        if (reqValue == null) {
            return false;
        }
        PreferenceService pSvc = getPreferenceService();
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setHomeTownLocationId] pSvc : "+pSvc);
        }
        if (pSvc != null) {
            try{
                pSvc.setPreferenceValue(UPP_KEY_HOME_TOWN_LOCATION_ID, reqValue);
            } catch(Exception e) {
               e.printStackTrace();
               return false;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setHomeTownLocationId] completed setHomeTownLocationId.");
        }
        return true;
    }
    public String getFavoriteCityLocationIdString() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_FAVORITE_CITY_LOCATION_IDS];
    }
    public boolean setFavoriteCityLocationIdString(String reqValue) {
        if (reqValue == null) {
            return false;
        }
        PreferenceService pSvc = getPreferenceService();
        boolean result = false;
        if (pSvc != null) {
            try{
                result = pSvc.setPreferenceValue(UPP_KEY_FAVORITE_CITY_LOCATION_IDS, reqValue);
            } catch(Exception e) {
               e.printStackTrace();
            }
        }
        return result;
    }
    public String getTemperatureUnit() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_TEMPERATURE_UNIT];
    }
    public boolean setTemperatureUnit(String reqValue) {
        if (reqValue == null) {
            return false;
        }
        PreferenceService pSvc = getPreferenceService();
        boolean result = false;
        if (pSvc != null) {
            try{
                result = pSvc.setPreferenceValue(UPP_KEY_TEMPERATURE_UNIT, reqValue);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}

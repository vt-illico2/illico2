package com.videotron.tvi.illico.itv.weather.comp;

public class MenuTreeItem {
    private int menuTreePrimaryKey;
    private String menuTreeTitleTxtKey;
    
    public int getMenuTreePrimaryKey() {
        return menuTreePrimaryKey;
    }
    public void setMenuTreePrimaryKey(int menuTreePrimaryKey) {
        this.menuTreePrimaryKey = menuTreePrimaryKey;
    }
    public String getMenuTreeTitleTextKey() {
        return menuTreeTitleTxtKey;
    }
    public void setMenuTreeTitleTextKey(String menuTreeTitleTxtKey) {
        this.menuTreeTitleTxtKey = menuTreeTitleTxtKey;
    }
}

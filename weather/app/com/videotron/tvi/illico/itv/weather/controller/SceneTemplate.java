package com.videotron.tvi.illico.itv.weather.controller;

public class SceneTemplate {
    public static final int SCENE_ID_INVALID = -1;
    public static final int SCENE_ID_WEATHER_MAIN = 0;
    public static final int SCENE_ID_SHORT_TERM_FORECAST = 1;
    public static final int SCENE_ID_LONG_TERM_FORECAST = 2;
    public static final int SCENE_ID_WEATHER_PREFERENCES = 3;
}

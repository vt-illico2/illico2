package com.videotron.tvi.illico.itv.weather.data;

import java.text.Collator;
import java.util.Locale;

public class CityInfo{
    public static final int CITY_TYPE_CANADA = 0;
    public static final int CITY_TYPE_OTHER_CITY = 1;
    private byte[] locationId;
    private byte[] countryName;
    private byte[] provinceName;
    private byte[] cityName;
    private byte[] displayName;
    private int cityType;
    
    public String getLocationId() {
        if (locationId == null) {
            return null;
        }
        return new String(locationId);
    }
    public void setLocationId(String locationId) {
        if (locationId != null) {
            this.locationId = locationId.getBytes();
        }
    }
    public String getCountryName() {
        if (countryName == null) {
            return null;
        }
        return new String(countryName);
    }
    public void setCountryName(String countryName) {
        if (countryName != null) {
            this.countryName = countryName.getBytes();
        }
    }
    
    public String getProvinceName() {
        if (provinceName == null) {
            return null;
        }
        return new String(provinceName);
    }
    public void setProvinceName(String provinceName) {
        if (provinceName != null) {
            this.provinceName = provinceName.getBytes();
        }
    }
    
    public String getCityName() {
        if (cityName == null) {
            return null;
        }
        return new String(cityName);
    }
    public void setCityName(String cityName) {
        if (cityName != null) {
            this.cityName = cityName.getBytes();
        }
    }
    
    public String getDisplayName() {
        if (displayName == null) {
            String cName = null;
            String pName = null;
            switch(cityType) {
                case CITY_TYPE_CANADA:
                    if (provinceName != null) {
                        pName = new String(provinceName);
                    }
                    break;
                case CITY_TYPE_OTHER_CITY:
                    if (countryName != null) {
                        pName = new String(countryName);
                    }
                    break;
            }
            if (cName == null && pName == null) {
                displayName = "N/A".getBytes();
            } else {
                if (cityName == null) {
                    cName = "";
                } else {
                    cName = new String(cityName);
                }

                String dName = cName;
                if (pName != null) {
                    dName +=", " + pName;
                }
                displayName = dName.getBytes();
            }
        }
        if (displayName == null) {
            return null;
        }
        return new String(displayName);
    }
    public int getCityType() {
        return cityType;
    }
    public void setCityType(int cityType) {
        this.cityType = cityType;
    }
}

package com.videotron.tvi.illico.itv.weather.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.comp.BreadCrumbs;
import com.videotron.tvi.illico.itv.weather.comp.DateAndTime;
import com.videotron.tvi.illico.itv.weather.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.weather.comp.PopupNotification;
import com.videotron.tvi.illico.itv.weather.comp.PopupQuestion;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.weather.comp.popup.PopupListener;
import com.videotron.tvi.illico.itv.weather.controller.SceneManager;
import com.videotron.tvi.illico.itv.weather.controller.SceneTemplate;
import com.videotron.tvi.illico.log.Log;

abstract public class BaseUI extends UIComponent {
    private static final long serialVersionUID = 1L;
    protected static StringBuffer dateBuffer;
    /*****************************************************************
     * variables - Component-related
     *****************************************************************/
    private DateAndTime dateAndTime;
    private BreadCrumbs bc;
    private LegalDisclaimer legalDisclaimer;
    /*****************************************************************
     * variables - Popup-related
     *****************************************************************/
    private PopupQuestion popQuestion;
    private PopupNotification popNotification;
    private PopupAdapter popAdaptDefault;
    protected boolean isRequested;
    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void init() {
        if (dateBuffer == null) {
            dateBuffer = new StringBuffer();
        }
        if (dateAndTime == null) {
            dateAndTime = new DateAndTime();
            dateAndTime.setBounds(700, 43, 210, 17);
            add(dateAndTime);
        }
        if (bc == null) {
            bc = new BreadCrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 480, 150, 30);
            add(legalDisclaimer, 0);
        }
        if (popAdaptDefault == null) {
            popAdaptDefault = new PopupAdapter() {
                public void popupOK(PopupEvent pe) {
                    PopupController.getInstance().closePopup(popNotification);
                }
            };
        }
        initScene();
    }
    public void dispose() {
        stopScene();
        disposeScene();
        if (popNotification != null) {
            popNotification.dispose();
            popNotification = null;
        }
        if (popQuestion != null) {
            popQuestion.dispose();
            popQuestion = null;
        }
        if (dateAndTime != null) {
            remove(dateAndTime);
            dateAndTime = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        popAdaptDefault=null;
        if (dateBuffer != null) {
            dateBuffer.setLength(0);
            dateBuffer = null;
        }
    }
    public void start(boolean reset, String[] params) {
        prepare();
        if (dateAndTime != null) {
            dateAndTime.start();
        }
        if (bc != null) {
            bc.start(new String[]{DataCenter.getInstance().getString("TxtWeather.Weather")});
        }
        startScene(reset, params);
    }
    public void stop() {
        stopScene();
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification);
        }
        if (popQuestion != null) {
            popQuestion.removePopupListener();
            PopupController.getInstance().closePopup(popQuestion);
        }
        if (dateAndTime != null) {
            dateAndTime.stop();
        }
    }
    /*****************************************************************
     * implement KeyListener
     *****************************************************************/
    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Scene.handleKey]code : "+code);
        }
        int curSceneId = SceneManager.getInstance().getCurrentSceneId();
        if (curSceneId != SceneTemplate.SCENE_ID_WEATHER_MAIN) {
            switch(code) {
                case Rs.KEY_EXIT:
                    CommunicationManager.getInstance().requestExitToChannel();
                    return true;
            }
        }
        if (CommunicationManager.getInstance().isLoadingAnimationService()){
            return true;
        }
        if (PopupController.getInstance().isActivePopup()){
            return true;
        }
        return keyAction(code);
    }
    /*****************************************************************
     * methods - Popup-related
     *****************************************************************/
    private void createPopupInfo(PopupListener l) {
        if (popNotification == null) {
            popNotification = new PopupNotification();
            popNotification.setBounds(0, 0, 960, 540);
        }
        popNotification.setPopupListener(l);
    }
    public void openPopupInfo(String cont) {
        openPopupInfo(popAdaptDefault, null, cont, PopupNotification.POPUP_SIZE_SMALL);
    }
    public void openPopupInfo(String sTitle, String cont) {
        openPopupInfo(popAdaptDefault, sTitle, cont, PopupNotification.POPUP_SIZE_SMALL);
    }
    protected void openPopupInfo(PopupListener l, String cont) {
        openPopupInfo(l, null, cont, PopupNotification.POPUP_SIZE_SMALL);
    }
    protected void openPopupInfo(PopupListener l,String sTitle, String cont, int size) {
        openPopup(l, sTitle, cont, size, PopupNotification.POPUP_TYPE_INFORMATION);
    }
    protected void openPopupQuestion(PopupListener l, String sTitle, String cont) {
        if (popQuestion == null) {
            popQuestion = new PopupQuestion();
            popQuestion.setBounds(0, 0, 960, 540);
        }
        popQuestion.setPopupListener(l);
        popQuestion.setTitle(sTitle);
        popQuestion.setContent(cont);
        PopupController.getInstance().openPopup(popQuestion);
    }
    protected void openPopupQuestion(PopupListener l, String cont) {
        openPopupQuestion(l, null, cont);
    }
    private void openPopup(PopupListener l, String sTitle, String cont, int size, int type) {
        createPopupInfo(l);
        popNotification.setPopupSize(size);
        popNotification.setPopupType(type);
        popNotification.setTitle(sTitle);
        popNotification.setContent(cont);
        PopupController.getInstance().openPopup(popNotification);
    }
    protected void closeAllPopup() {
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification);
        }
        if (popQuestion != null) {
            popQuestion.removePopupListener();
            PopupController.getInstance().closePopup(popQuestion);
        }
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public boolean isRequested() {
        return isRequested;
    }
    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void initScene();
    abstract protected void disposeScene();
    abstract protected void startScene(boolean resetScene, String[] params);
    abstract protected void stopScene();
    abstract protected boolean keyAction(int keyCode);
}

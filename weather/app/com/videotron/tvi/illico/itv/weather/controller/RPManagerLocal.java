package com.videotron.tvi.illico.itv.weather.controller;

import java.io.File;
import java.io.FileInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.comp.SAXHandler;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CityInfoHandler;
import com.videotron.tvi.illico.itv.weather.data.CityInfoSearchHandler;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.itv.weather.data.LatestForecastHandler;
import com.videotron.tvi.illico.log.Log;

public class RPManagerLocal implements RPManager {
    private SAXParser parser;

    private DefaultHandler hdlrLatestForecast;
    private DefaultHandler hdlrCityInfo;

    public void init() {
        try {
            parser = SAXParserFactory.newInstance().newSAXParser();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void dispose() {
        parser = null;
    }
    public void start() {
    }
    public void stop() {
    }
    public CityInfo getCityInfoByPostalCode(String reqLangTag, String reqPostalCode) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]start");
            Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Param - request postal code : ["+reqPostalCode+"]");
        }
        if (reqLangTag == null || reqPostalCode == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Request parameter is invalid. return null.");
            }
            return null;
        }
        DefaultHandler handler = new CityInfoHandler();
        String filePath = "resource/test_data/server_data/postal_" + reqLangTag + "_" + reqPostalCode + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Request file path : ["+filePath+"]");
        }
        threadSleep(2000);
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Source file is null. return null.");
            }
            return null;
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            parser.parse(fis, handler);
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Meteo HTTP server.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        } finally {
            if (fis != null) {
                try{
                    fis.close();
                }catch(Exception e) {
                    e.printStackTrace();
                }
                fis = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        CityInfo cityInfo = null;
        if (saxHandler != null) {
            cityInfo = (CityInfo) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityInfoByPostalCode]Response city info : "+cityInfo);
        }
        return cityInfo;
    }
    public LatestForecast getLatestForecast(String reqLangTag, String reqLocId) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLatestForecast]start");
            Log.printDebug("[RPMgrLocal.getLatestForecast]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrLocal.getLatestForecast]Param - request location id : ["+reqLocId+"]");
        }
        if (reqLangTag == null || reqLocId == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getLatestForecast]Request parameter is invalid. return null.");
            }
            return null;
        }
        DefaultHandler handler = getDefaultHandlerLatestForecast();
        if (handler == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getLatestForecast]XML Handler is null. return null.");
            }
            return null;
        }
        String filePath = "resource/test_data/server_data/forecast_" + reqLangTag + "_" + reqLocId + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLatestForecast]Request file path : ["+filePath+"]");
        }
        threadSleep(2000);
        File file = new File(filePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            parser.parse(fis, handler);
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Meteo HTTP server.");
            }            
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                fis = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        LatestForecast latestForecast = null;
        if (saxHandler != null) {
            latestForecast = (LatestForecast) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLatestForecast]Response latest forecast : "+latestForecast);
        }
        return latestForecast;
    }
    public CityInfo[] getCityList(String reqLangTag, String reqCityListTag) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityList]start");
            Log.printDebug("[RPMgrLocal.getCityList]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrLocal.getCityList]Param - request city list tag : ["+reqCityListTag+"]");
        }
        if (reqLangTag == null || reqCityListTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getCityList]Request parameter is invalid. return null.");
            }
            return null;
        }
        DefaultHandler handler = getDefaultHandlerCityInfoSearch();
        if (handler == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getCityList]XML Handler is null. return null.");
            }
            return null;
        }
        String filePath = "resource/test_data/server_data/city_list_" + reqLangTag + "_" + reqCityListTag + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityList]Request file path : ["+filePath+"]");
        }
        threadSleep(2000);
        File file = new File(filePath);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            parser.parse(fis, handler);
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Meteo HTTP server.");
            }            
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        } finally {
            if (fis != null) {
                try{
                    fis.close();
                }catch(Exception e) {
                    e.printStackTrace();
                }
                fis = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        CityInfo[] cityList = null;
        if (saxHandler != null) {
            cityList = (CityInfo[]) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getCityList]Response city list : "+cityList);
        }
        return cityList;
    }
    private DefaultHandler getDefaultHandlerLatestForecast() {
        if (hdlrLatestForecast == null) {
            hdlrLatestForecast = new LatestForecastHandler();
        }
        return hdlrLatestForecast;
    }
    private DefaultHandler getDefaultHandlerCityInfoSearch() {
        if (hdlrCityInfo == null) {
            hdlrCityInfo = new CityInfoSearchHandler();
        }
        return hdlrCityInfo;
    }

    private void threadSleep(long longSleep) {
        try{
            Thread.sleep(longSleep);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}

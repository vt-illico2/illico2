package com.videotron.tvi.illico.itv.weather.controller;

import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;

public interface RPManager {
    public static final String CITY_LIST_TAG_CANADA = "can";
    public static final String CITY_LIST_TAG_OTHER_CITY = "intl";
    public static final int CITY_INFO_TYPE_CANADA = 0;
    public static final int CITY_INFO_TYPE_INTERNATIONAL = 1;
    /*********************************************************************************
     * Life cycle-related
     *********************************************************************************/
    void init();
    void dispose();
    void start();
    void stop();
    /*********************************************************************************
     * Service-related
     *********************************************************************************/
    CityInfo getCityInfoByPostalCode(String reqLangTag, String reqPostalCode) throws RPManagerException;
    LatestForecast getLatestForecast(String reqLangTag, String reqLocId) throws RPManagerException;
    CityInfo[] getCityList(String reqLangTag, String reqCityListTag) throws RPManagerException;
}

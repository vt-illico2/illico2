package com.videotron.tvi.illico.itv.weather.controller;

import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.log.Log;

public class WeatherVbmController extends VbmController {
    public static final long WEATHER_APP_START = 1011000001L;
    public static final long WEATHER_APP_EXIT = 1011000002L;
    public static final long WEATHER_PARENT_APP = 1011000003L;
    
    private String sessionId;
    
    protected static WeatherVbmController instance = new WeatherVbmController();

    public static WeatherVbmController getInstance() {
        return instance;
    }
    
    protected WeatherVbmController() {
    }
    
    public void writeWeatherAppStart() {
        Log.printInfo("[WeatherVbmController.writeWeatherAppStart] start.");
        sessionId=Long.toString(System.currentTimeMillis());
        Log.printInfo("[WeatherVbmController.writeWeatherAppStart] Session ID : "+sessionId);
        String[] params = CommunicationManager.getInstance().requestGetParameter();
        Log.printInfo("[WeatherVbmController.writeWeatherAppStart] Parameters from monitor service : "+params);
        String parentAppName = "";
        if (params != null && params.length > 0) { parentAppName = params[0]; }
        Log.printInfo("[WeatherVbmController.writeWeatherAppStart] Parent app name : "+parentAppName);
        write(WEATHER_PARENT_APP, DEF_MEASUREMENT_GROUP, parentAppName);
        write(WEATHER_APP_START, DEF_MEASUREMENT_GROUP, sessionId);
        Log.printInfo("[WeatherVbmController.writeWeatherAppStart] end.");
    }
    
    public void writeWeatherAppExit() {
        Log.printInfo("[WeatherVbmController.writeWeatherAppExit] start.");
        Log.printInfo("[WeatherVbmController.writeWeatherAppExit] Session ID : "+sessionId);
        write(WEATHER_APP_EXIT, DEF_MEASUREMENT_GROUP, sessionId);
        Log.printInfo("[WeatherVbmController.writeWeatherAppExit] end.");
    }
}

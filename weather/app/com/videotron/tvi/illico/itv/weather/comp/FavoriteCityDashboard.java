package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.log.Log;

public class FavoriteCityDashboard extends Container implements AnimationRequestor {
    private static final long serialVersionUID = 7421287121989421402L;
    private Image[] imgBGs = {
            ImageManager.getInstance().getImage("11_banner_01.png", ImageManager.TYPE_TEMPORARY),
            ImageManager.getInstance().getImage("11_banner_02.png", ImageManager.TYPE_TEMPORARY),
            ImageManager.getInstance().getImage("11_banner_03.png", ImageManager.TYPE_TEMPORARY),
            ImageManager.getInstance().getImage("11_banner_04.png", ImageManager.TYPE_TEMPORARY)
    };
    private Image imgBGOption;
    private Image imgBGVacant;

    private String reqMsg;
    private FavoriteCityDashboardListener lstner;
    private boolean isFocusComponent;
    private boolean isReadyComponent;

    private LatestForecast[] lfs;
    private FavoriteCityWidget[] favCityWidgets;

    private static final int BANNER_WIDTH = 218;
    private static final int BANNER_HEIGHT = 105;
    private final int bannerGap = 180;
    private final int maxVisibleCount = 3;
    private int startIdx;
    private int displayCount;
    private int curIdx;

    public void init() {
        imgBGOption = ImageManager.getInstance().getImage("11_banner_fav.png", ImageManager.TYPE_TEMPORARY);
        imgBGVacant = ImageManager.getInstance().getImage("11_banner_blank.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        removeFavoriteCityWidgets();
        lfs = null;
        lstner = null;
        reqMsg = null;

        imgBGVacant = null;
        imgBGOption = null;
        imgBGs = null;
    }
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[FavoriteCityDashboard.start]Start.");
        }
        resetFavorite();
    }
    public void stop() {
        removeFavoriteCityWidgets();
    }
    public boolean keyAction(int keyCode) {
        int tempValue = -1;
        switch(keyCode) {
            case HRcEvent.VK_LEFT:
                if (favCityWidgets == null || favCityWidgets[curIdx] == null) {
                    return true;
                }
                if (favCityWidgets[curIdx].isFirstIndex()) {
                    favCityWidgets[curIdx].setSelected(false);
                    if (lstner != null) {
                        lstner.lostDashboardFocus();
                    }
                    return true;
                }
                tempValue = curIdx - 1;
                if (curIdx == tempValue) {
                    return true;
                }
                if (favCityWidgets[tempValue] == null || !favCityWidgets[tempValue].isSelectable()) {
                    return true;
                }
                favCityWidgets[curIdx].setSelected(false);
                curIdx = tempValue;
                if (isFocusComponent) {
                    favCityWidgets[curIdx].setSelected(true);
                } else {
                    favCityWidgets[curIdx].setSelected(false);
                }
                setFavoriteCityWidget();
                repaint();
                return true;
            case HRcEvent.VK_RIGHT:
                if (favCityWidgets == null || favCityWidgets[curIdx] == null) {
                    return true;
                }
                if (favCityWidgets[curIdx].isLastIndex()) {
                    return true;
                }
                tempValue = curIdx + 1;
                if (curIdx == tempValue) {
                    return true;
                }
                if (favCityWidgets[tempValue] == null || !favCityWidgets[tempValue].isSelectable()) {
                    return true;
                }
                favCityWidgets[curIdx].setSelected(false);
                curIdx = tempValue;
                if (isFocusComponent) {
                    favCityWidgets[curIdx].setSelected(true);
                } else {
                    favCityWidgets[curIdx].setSelected(false);
                }
                setFavoriteCityWidget();
                repaint();
                return true;
            case HRcEvent.VK_ENTER:
                if (favCityWidgets != null) {
                    if (favCityWidgets[curIdx]!= null) {
                        Rectangle r = favCityWidgets[curIdx].getBounds();
                        ClickingEffect effect = new ClickingEffect(this, 5);
                        effect.updateBackgroundBeforeStart(false);
                        effect.start(r.x + 13, r.y + 6, r.width - 26, r.height - 12);
                    }
                    if (lstner != null) {
                        lstner.selectedFavoriteCityWidget(favCityWidgets[curIdx]);
                    }
                }
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
        String txtFavCities = DataCenter.getInstance().getString("TxtWeather.Favorite_Cities");
        if (txtFavCities != null) {
            g.setFont(Rs.F20);
            g.setColor(Rs.C255255255);
            g.drawString(txtFavCities, 22, 18);
        }
        if (reqMsg != null) {
            paintBlankPanel(g, reqMsg, Rs.F30, Rs.FM30);
            return;
        }
        super.paint(g);
    }
    private void paintBlankPanel(Graphics g, String content, Font f, FontMetrics fm) {
        if (content != null) {
            int width = getWidth();
            g.setColor(Rs.C086086086);
            g.fillRect(22, 26, 540, 82);
            g.setColor(Rs.C050050050);
            g.drawRect(22, 26, 540, 82);
            int txtMessageWth = fm.stringWidth(content);
            int txtMessageX = (width-txtMessageWth)/2;
            g.setFont(f);
            g.setColor(Rs.C110110110);
            g.drawString(content, txtMessageX, 76);
            g.setColor(Rs.C050050050);
            g.drawString(content, txtMessageX - 1, 75);
        }
    }
    public void resetFavorite(){
        reqMsg = DataCenter.getInstance().getString("TxtWeather.Loading_Data");
        Thread test = new Thread(Rs.APP_NAME + ".favoriteCityDashboard") {
            public void run() {
                removeFavoriteCityWidgets();
                isReadyComponent = false;
                String[] locIds = DataManager.getInstance().getValidFavoriteCityLocationIds();
                if (locIds == null || locIds.length == 0) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[FavoriteCityDashboard.resetFavorite]Favorite city location id is zero. make default favorite city banner.");
                    }
                    favCityWidgets = new FavoriteCityWidget[3];
                    favCityWidgets[0] = new FavoriteCityWidget();
                    favCityWidgets[0].setBannerType(FavoriteCityWidget.BANNER_TYPE_OPTION);
                    favCityWidgets[0].setFirstIndex(true);
                    favCityWidgets[0].setLastIndex(true);
                    favCityWidgets[0].setSelectable(true);
                    favCityWidgets[0].setBackgroundImage(imgBGOption);

                    favCityWidgets[1] = new FavoriteCityWidget();
                    favCityWidgets[1].setBannerType(FavoriteCityWidget.BANNER_TYPE_VACANT);
                    favCityWidgets[1].setSelectable(false);
                    favCityWidgets[1].setBackgroundImage(imgBGVacant);

                    favCityWidgets[2] = new FavoriteCityWidget();
                    favCityWidgets[2].setBannerType(FavoriteCityWidget.BANNER_TYPE_VACANT);
                    favCityWidgets[2].setSelectable(false);
                    favCityWidgets[2].setBackgroundImage(imgBGVacant);
                    if (curIdx >= favCityWidgets.length) {
                        curIdx = 0;
                    }
                    setFavoriteCityWidget();
                    reqMsg = null;
                    isReadyComponent = true;
                    if (isFocusComponent) {
                        setFocusComponent(true);
                    }
                    repaint();
                    return;
                }
                
                int locIdsLth = locIds.length;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[FavoriteCityDashboard.start]locIdsLth : "+locIdsLth);
                }
                lfs = new LatestForecast[locIdsLth];
                for (int i = 0; i < locIdsLth; i++) {
                    try{
                        lfs[i] = DataManager.getInstance().getLatestForecast(locIds[i]);
                    }catch(Exception e) {
                    }
                }
                if (locIdsLth > 1) {
                    if (locIdsLth < DataManager.FAVORITE_CITY_MAX_COUNT) {
                        favCityWidgets = new FavoriteCityWidget[locIdsLth+1];
                    } else {
                        favCityWidgets = new FavoriteCityWidget[locIdsLth];
                    }
                    for (int i=0; i<locIdsLth; i++) {
                        favCityWidgets[i] = new FavoriteCityWidget(lfs[i]);
                        favCityWidgets[i].setBannerType(FavoriteCityWidget.BANNER_TYPE_NORMAL);
                        if (i == 0) {
                            favCityWidgets[i].setFirstIndex(true);
                        }
                        if (i == favCityWidgets.length - 1) {
                            favCityWidgets[i].setLastIndex(true);
                        }
                        favCityWidgets[i].setSelectable(true);
                        favCityWidgets[i].setBackgroundImage(imgBGs[i % imgBGs.length]);
                    }
                    if (locIdsLth < DataManager.FAVORITE_CITY_MAX_COUNT) {
                        favCityWidgets[favCityWidgets.length-1] = new FavoriteCityWidget();
                        favCityWidgets[favCityWidgets.length-1].setBannerType(FavoriteCityWidget.BANNER_TYPE_OPTION);
                        favCityWidgets[favCityWidgets.length-1].setLastIndex(true);
                        favCityWidgets[favCityWidgets.length-1].setSelectable(true);
                        favCityWidgets[favCityWidgets.length-1].setBackgroundImage(imgBGOption);
                    }
                } else {
                    favCityWidgets = new FavoriteCityWidget[3];
                    favCityWidgets[0] = new FavoriteCityWidget(lfs[0]);
                    favCityWidgets[0].setBannerType(FavoriteCityWidget.BANNER_TYPE_NORMAL);
                    favCityWidgets[0].setFirstIndex(true);
                    favCityWidgets[0].setSelectable(true);
                    favCityWidgets[0].setBackgroundImage(imgBGs[0]);
                    favCityWidgets[1] = new FavoriteCityWidget();
                    favCityWidgets[1].setBannerType(FavoriteCityWidget.BANNER_TYPE_OPTION);
                    favCityWidgets[1].setLastIndex(true);
                    favCityWidgets[1].setSelectable(true);
                    favCityWidgets[1].setBackgroundImage(imgBGOption);
                    favCityWidgets[2] = new FavoriteCityWidget();
                    favCityWidgets[2].setBannerType(FavoriteCityWidget.BANNER_TYPE_VACANT);
                    favCityWidgets[2].setSelectable(false);
                    favCityWidgets[2].setBackgroundImage(imgBGVacant);
                }
                if (curIdx >= favCityWidgets.length) {
                    curIdx = 0;
                }
                setFavoriteCityWidget();
                reqMsg = null;
                isReadyComponent = true;
                if (isFocusComponent) {
                    setFocusComponent(true);
                }
                repaint();
            }
        };
        test.start();
    }
    private void removeFavoriteCityWidgets() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[FavoriteCityDashboard.removeMyOtherCitiesBanners]cWeatherBanners : "+favCityWidgets);
        }
        if (favCityWidgets != null) {
            for (int i=0; i<favCityWidgets.length; i++) {
                if (favCityWidgets[i]!= null) {
                    favCityWidgets[i].dispose();
                    favCityWidgets[i] = null;
                }
            }
            favCityWidgets = null;
        }
        removeAll();
    }
    public void setFavoriteCityWidget() {
        int tempStartIdx = 0;
        int tempDisplayCount = 0;
        int tempTotalCont = 0;
        if (favCityWidgets != null) {
            tempTotalCont = favCityWidgets.length;
        }
        if (tempTotalCont <= maxVisibleCount) {
            tempStartIdx = 0;
            tempDisplayCount = tempTotalCont;
        } else {
            if (displayCount == 0) {
                startIdx = 0;
            } else if (startIdx > curIdx) {
                tempStartIdx = startIdx - 1;
            } else if (startIdx + maxVisibleCount <= curIdx) {
                tempStartIdx = startIdx + 1;
            } else {
                tempStartIdx = startIdx;
            }
            tempDisplayCount = maxVisibleCount;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[FavoriteCityDashboard.setBanner]tempStartIdx : " + tempStartIdx);
            Log.printDebug("[FavoriteCityDashboard.setBanner]tempDisplayCount : " + tempDisplayCount);
            Log.printDebug("[FavoriteCityDashboard.setBanner]tempTotalCont : " + tempTotalCont);
            Log.printDebug("[FavoriteCityDashboard.setBanner]startIdx : " + startIdx);
            Log.printDebug("[FavoriteCityDashboard.setBanner]displayCount : " + displayCount);
        }
        // remove Before widget
        for (int i = startIdx; i < startIdx + displayCount; i++) {
            if (favCityWidgets[i] != null) {
                favCityWidgets[i].stop();
                remove(favCityWidgets[i]);
            }
        }
        startIdx = tempStartIdx;
        displayCount = tempDisplayCount;
        // add Current widget
        for (int i = startIdx; i < startIdx + displayCount; i++) {
            int bannerX = bannerGap * (i - startIdx);
            if (favCityWidgets[i] == null) {
                continue;
            }
            favCityWidgets[i].setBounds(bannerX, 14, BANNER_WIDTH, BANNER_HEIGHT);
            if (i != curIdx) {
                add(favCityWidgets[i]);
            }
        }
        if (favCityWidgets[curIdx] != null) {
            add(favCityWidgets[curIdx], 0);
        }
    }
    public void setFocusComponent(boolean isFocusComponent) {
        this.isFocusComponent = isFocusComponent;
        if (favCityWidgets == null || favCityWidgets[curIdx] == null) {
            return;
        }
        if (isFocusComponent) {
            favCityWidgets[curIdx].setSelected(true);
        } else {
            favCityWidgets[curIdx].setSelected(false);
        }
    }
    public boolean isFocusComponent() {
        return isFocusComponent;
    }
    public void setListener(FavoriteCityDashboardListener lstner) {
        this.lstner = lstner;
    }
    public void removeListener() {
        this.lstner = null;
    }
    public boolean isReadyComponent() {
        return isReadyComponent;
    }

    public FavoriteCityWidget getSelectedBanner() {
        return favCityWidgets[curIdx];
    }
    /*********************************************************************************
     * AnimationRequestor-related
     *********************************************************************************/
    public boolean skipAnimation(Effect effect) {
        return false;
    }
    public void animationStarted(Effect effect) {
    }
    public void animationEnded(Effect effect) {
    }
}

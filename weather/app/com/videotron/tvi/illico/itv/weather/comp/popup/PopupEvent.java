package com.videotron.tvi.illico.itv.weather.comp.popup;

public class PopupEvent {
    private Popup popup;
    private String popupParam;

    public PopupEvent(Popup popup) {
        this.popup = popup;
    }

    public void setPopup(Popup popup) {
        this.popup = popup;
    }

    public Popup getPopup() {
        return popup;
    }

    public void setPopupParam(String popupParam) {
        this.popupParam = popupParam;
    }

    public String getPopupParam() {
        return popupParam;
    }
}

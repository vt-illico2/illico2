package com.videotron.tvi.illico.itv.weather.comp;

import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.weather.Rs;
import com.videotron.tvi.illico.itv.weather.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.weather.controller.DataManager;
import com.videotron.tvi.illico.itv.weather.controller.ImageManager;
import com.videotron.tvi.illico.itv.weather.controller.RPManagerException;
import com.videotron.tvi.illico.itv.weather.data.CityInfo;
import com.videotron.tvi.illico.itv.weather.data.CurrentCondition;
import com.videotron.tvi.illico.itv.weather.data.LatestForecast;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

public class CurrentWeatherInHomeTown extends Component {
    private static final long serialVersionUID = -6046946890310258978L;
    private DataManager dMgr = DataManager.getInstance();
    private Image imgBigTempUnitC;
    private Image imgBigTempUnitF;
    private Image imgSmallTempUnitC;
    private Image imgSmallTempUnitF;

    private String locId;
    private LatestForecast lf;
    private String reqMsg;

    private int tempUnit;
    private String displayName;
    private Image imgIconCode;
    private String temperature;
    private String forecastText;
    private String humidity;
    private String wind;
    private String feelsLike;

    public void init() {
        if (imgBigTempUnitC  == null) {
            imgBigTempUnitC = ImageManager.getInstance().getImage("11_c_b.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgBigTempUnitF  == null) {
            imgBigTempUnitF = ImageManager.getInstance().getImage("11_f_b.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgSmallTempUnitC  == null) {
            imgSmallTempUnitC = ImageManager.getInstance().getImage("11_c_xs_white.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgSmallTempUnitF  == null) {
            imgSmallTempUnitF = ImageManager.getInstance().getImage("11_f_xs_white.png", ImageManager.TYPE_TEMPORARY);
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        imgBigTempUnitC = null;
        imgBigTempUnitF = null;
        imgSmallTempUnitC = null;
        imgSmallTempUnitF = null;
        locId = null;
        lf = null;
        reqMsg =  null;
    }
    public void start() {
        reqMsg = DataCenter.getInstance().getString("TxtWeather.Loading_Data");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.init]reqMsg : "+reqMsg);
        }
        new Thread(Rs.APP_NAME + ".curretnWeatherInHometown") {
            public void run() {
                try{
                    locId = dMgr.getHomeTownLocationId();
                    if (locId != null && locId.trim().length() > 0){
                        lf = dMgr.getLatestForecast(locId);
                    } else {
                        lf = null;
                    }
                }catch(RPManagerException e) {
                    e.printStackTrace();
                    CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage(), null);
                }
                if (lf == null) {
                    reqMsg = DataCenter.getInstance().getString("TxtWeather.Not_Available");
                } else {
                    reqMsg = null;
                    CityInfo ci = lf.getCityInfo();
                    if (ci != null) {
                        displayName = ci.getDisplayName();
                    }
                    CurrentCondition cc = lf.getCurrentCondition();
                    if (cc != null) {
                        imgIconCode = cc.getIconCodeImage();
                        tempUnit = dMgr.getTemperatureUnit();
                        if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                            temperature = cc.getTemperatureC();
                        } else {
                            temperature = cc.getTemperatureF();
                        }
                        forecastText = cc.getForecastText();
                        String windDirection = cc.getWindDirection();
                        String windSpeedKm = cc.getWindSpeedKm();
                        if (windDirection == null) {
                            windDirection = "";
                        }
                        if (windSpeedKm == null) {
                            windSpeedKm = "";
                        }
                        if (windDirection.length() + windSpeedKm.length() > 0) {
                            wind = windDirection + " " + windSpeedKm + "Km/h";
                        }
                        //Feels like
                        if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                            feelsLike = cc.getFeelsLikeC();
                        } else {
                            feelsLike = cc.getFeelsLikeF();
                        }
                        //Humidity
                        humidity = cc.getHumidity();
                        if (humidity == null) {
                            humidity = "N/A";
                        } else {
                            humidity += "%";
                        }
                    }
                }
                repaint();
            }
        }.start();
    }
    public void stop() {
        lf = null;
        imgIconCode = null;
        temperature = null;
        forecastText = null;
        humidity = null;
        wind = null;
        feelsLike = null;
        displayName = null;
    }
    public void paint(Graphics g) {
        String txtCurWeather = DataCenter.getInstance().getString("TxtWeather.Current_Weather");
        if (txtCurWeather != null) {
            g.setFont(Rs.F20);
            g.setColor(Rs.C255255255);
            g.drawString(txtCurWeather, 0, 18);
        }
        final int x = 375;
        final int y = 232;
        if (reqMsg != null) {
            paintBlankPanel(g, reqMsg, Rs.F30, Rs.FM30);
//            g.setFont(Rs.F30);
//            g.setColor(Rs.C245212016);
//            int reqMsgWth = Rs.FM30.stringWidth(reqMsg);
//            g.drawString(reqMsg, (getWidth() / 2) - (reqMsgWth / 2), 50);
            return;
        }
        if (lf == null) {
            return;
        }
        // Weather Icon
        if (imgIconCode != null) {
            g.drawImage(imgIconCode, 478 - x, 279 - y, 118, 71, this);
        }
        // City
        g.setFont(Rs.F26);
        g.setColor(Rs.C255204000);
        if (displayName != null) {
            String shortenedDisplayName = TextUtil.shorten(displayName, Rs.FM26, 328);
            g.drawString(shortenedDisplayName, 375 - x, 276 - y);
        } else {
            g.drawString("N/A", 375 - x, 276 - y);
        }
        // Temperature
        g.setFont(Rs.F50);
        g.setColor(Rs.C255204000);
        if (temperature != null) {
            g.drawString(temperature, 594 - x, 318 - y);
            int temperatureWth = Rs.FM50.stringWidth(temperature);
            Image imgBigTempUnit = null;
            if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                imgBigTempUnit = imgBigTempUnitC;
            } else {
                imgBigTempUnit = imgBigTempUnitF;
            }
            if (imgBigTempUnit != null) {
                g.drawImage(imgBigTempUnit, 594 + 3 + temperatureWth - x, 300 - y, this);
            }
        } else {
            g.drawString("N/A", 594 - x, 318 - y);
        }
        // Forecast Text
        if (forecastText != null) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C255255255);
            String shortenedForecastText = TextUtil.shorten(forecastText, Rs.FM17, 114);
            g.drawString(shortenedForecastText, 598 - x, 338 - y);
        }
        //Separator
        g.setColor(Rs.DVB219219219051);
        g.fillRect(718 - x, 280 - y, 1, 64);

        g.setFont(Rs.F16);
        g.setColor(Rs.C187189192);
        //Label - Humidity
        String txtHumidity = DataCenter.getInstance().getString("TxtWeather.Humidity");
        if (txtHumidity != null) {
            g.drawString(txtHumidity, 735 - x, 300 - y);
        }
        //Label - Wind
        String txtWind = DataCenter.getInstance().getString("TxtWeather.Wind");
        if (txtWind != null) {
            g.drawString(txtWind, 735 - x, 318 - y);
        }
        //Label - Feels like
        String txtFeelsLike = DataCenter.getInstance().getString("TxtWeather.Feels_Like");
        if (txtFeelsLike != null) {
            g.drawString(txtFeelsLike, 735 - x, 336 - y);
        }
        g.setFont(Rs.F17);
        g.setColor(Rs.C219219219);
        //Value - Humidity
        if (humidity != null) {
            g.drawString(humidity, 817 - x, 300 - y);
        }
        //Value - Wind
        if (wind != null) {
            g.drawString(wind, 817 - x, 318 - y);
        }
        //Value - Feels like
        if (feelsLike != null) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C219219219);
            g.drawString(feelsLike, 817 - x, 336 - y);
            int feelsLikeWth = Rs.FM17.stringWidth(feelsLike);
            Image imgSmallTempUnit = null;
            if (tempUnit == DataManager.TEMPERATURE_UNIT_ID_C) {
                imgSmallTempUnit = imgSmallTempUnitC;
            } else {
                imgSmallTempUnit = imgSmallTempUnitF;
            }
            if (imgSmallTempUnit != null) {
                g.drawImage(imgSmallTempUnit, 817 + 3 + feelsLikeWth - x, 336 - 9 - y, this);
            }
        }
    }
    private void paintBlankPanel(Graphics g, String content, Font f, FontMetrics fm) {
        if (content != null) {
            int width = getWidth();
            g.setColor(Rs.C086086086);
            g.fillRect(0, 26, width, 64);
            g.setColor(Rs.C050050050);
            g.drawRect(0, 26, width, 64);
            int txtMessageWth = fm.stringWidth(content);
            int txtMessageX = (width-txtMessageWth)/2;
            g.setFont(f);
            g.setColor(Rs.C110110110);
            g.drawString(content, txtMessageX, 66);
            g.setColor(Rs.C050050050);
            g.drawString(content, txtMessageX - 1, 65);
        }
    }
}

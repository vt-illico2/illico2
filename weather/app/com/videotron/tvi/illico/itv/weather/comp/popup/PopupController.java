package com.videotron.tvi.illico.itv.weather.comp.popup;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.WindowProperty;

public class PopupController {
    private static PopupController instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI windowMgr;
    private LayeredWindow lwPopup;
    private LayeredKeyHandler lkhPopup;
    /*****************************************************************
     * variables - General
     *****************************************************************/
    private ArrayList popupList;
    private int popupListSize;
    private Popup curPopup;

    /*****************************************************************
     * Constructor and Disposer
     *****************************************************************/
    public static synchronized PopupController getInstance() {
        if (instance == null) {
            instance = new PopupController();
        }
        return instance;
    }
    private PopupController() {
        if (popupList == null) {
            popupList = new ArrayList();
        }
    }

    public void init() {
        if (lwPopup == null) {
            lwPopup = new LayeredWindowPopup();
            lwPopup.setVisible(true);
        }
        if (lkhPopup == null) {
            lkhPopup = new LayeredKeyHandlerPopup();
        }
        windowMgr = WindowProperty.ITV_POPUP.createLayeredDialog(lwPopup, lwPopup.getBounds(), lkhPopup);
        windowMgr.deactivate();
    }

    public void dispose() {
        if (windowMgr != null) {
            windowMgr.deactivate();
            WindowProperty.dispose(windowMgr);
            windowMgr = null;
        }
        if (lwPopup != null) {
            lwPopup.removeAll();
            lwPopup.setVisible(false);
            lwPopup = null;
        }
        lkhPopup = null;
        removeAllPopup();
    }

    /*****************************************************************
     * methods - Functional
     *****************************************************************/
    /** open pop-up **/
    public void openPopup(Popup openPopup) {
        if (popupList == null || openPopup == null) {
            return;
        }
        curPopup = openPopup;
        openPopup.start();
        popupList.add(openPopup);
        popupListSize = popupList.size();
        lwPopup.add(openPopup, 0);
        windowMgr.activate();
    }
    /** close the last of pop-up **/
    public void closePopup(Popup closePopup) {
        if (popupList == null || closePopup == null) {
            return;
        }
        closePopup.stop();
        int popupIndex = popupList.indexOf(closePopup);
        if (popupIndex != -1) {
            popupList.remove(popupIndex);
            popupListSize = popupList.size();
        }
        int compCount = 0;
        if (lwPopup != null && closePopup != null) {
            lwPopup.remove(closePopup);
            compCount = lwPopup.getComponentCount();
            curPopup = null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PopCtrl.closePopup]compCount : "+compCount);
        }
        if (windowMgr != null) {
            windowMgr.deactivate();
        }
        if (compCount > 0) {
            if (windowMgr != null) {
                windowMgr.activate();
                curPopup = (Popup)lwPopup.getComponent(0);
            }
        }
    }
    public void closeAllPopup() {
        if (popupList == null || popupListSize <= 0) {
            return;
        }
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup closePopup = (Popup) iterator.next();
            if (closePopup == null) {
                continue;
            }
            lwPopup.remove(closePopup);
            closePopup.stop();
        }
        popupList.clear();
        popupListSize = popupList.size();
        windowMgr.deactivate();
        curPopup = null;
    }

    /** get the length of a pop-up **/
    public int getPopupLength() {
        return popupListSize;
    }

    public Popup getPopup(int popupIdx) {
        if (popupList == null)
            return null;
        return (Popup) popupList.get(popupIdx);
    }

    public Popup getLastPopup() {
        int popupListSize = -1;
        if (popupList == null || (popupListSize = popupList.size()) <= 0)
            return null;
        return (Popup) popupList.get(popupListSize - 1);
    }

    public boolean isActivePopup() {
        if (popupListSize > 0)
            return true;
        return false;
    }

    public void removeAllPopup() {
        if (popupList == null || popupListSize <= 0)
            return;
        Iterator iterator = popupList.iterator();
        while (iterator.hasNext()) {
            Popup popup = (Popup) iterator.next();
            if (popup == null)
                continue;
            popup.dispose();
        }
        popupList.clear();
        popupList = null;
    }

    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowPopup extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowPopup() {
            setBounds(0, 0, 960, 540);
        }
        public void notifyShadowed() {
        }
    }
    class LayeredKeyHandlerPopup implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PopupController.handleKeyEvent]userEvent : "+userEvent);
            }
            if (userEvent == null) {
                return false;
            }
            if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }
            boolean result = false;
            if (curPopup != null) {
                result = curPopup.keyAction(userEvent.getCode());
            }
            return result;
        }
    }
}

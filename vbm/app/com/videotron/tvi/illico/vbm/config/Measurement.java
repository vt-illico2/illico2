package com.videotron.tvi.illico.vbm.config;


/***
 * data store for measurement
 * 
 * @author swkim
 *
 */
public class Measurement {

	/** application id **/
	private String appId;
	/** campaign id containing measurement  **/
	private long campaignId;
	/** campaign version containing measurement  **/
	private int campaignVersion;
	/** target version containing measurement **/
	private int targetVersion;
	

	/** usage point id **/
	private long usagePointId;
	
	/** measurement id **/
	private long measurementId;
	/** report frequency of campaign **/
	private int reportFrequency;
	/** send frequency of campaign **/
	private int sendFrequency;
	/** special frequency of campaign **/
	private int specialFrequency;
	
	/** general configuration version to check trigger measurement **/
	private int generalSettingVersion;
	
	/***
	 * getter of applicaiton id 
	 * @return application id
	 */
	public String getAppId() {
		return appId;
	}
	
	/***
	 * setter of application id
	 * @param appId - application id 
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
	
	/** getter of measurement id 
	 * 
	 * @return measurement id 
	 */
	public long getUsagePointId() {
		return usagePointId;
	}
	
	/**
	 * setter of measurement id 
	 * @param measurementId - measurement id 
	 */
	public void setUsagePointId(long usagePointId) {
		this.usagePointId = usagePointId;
	}
	

	/** getter of measurement id 
	 * 
	 * @return measurement id 
	 */
	public long getMeasurementId() {
		return measurementId;
	}
	
	/**
	 * setter of measurement id 
	 * @param measurementId - measurement id 
	 */
	public void setMeasurementId(long measurementId) {
		this.measurementId = measurementId;
	}
	
	
	
	/***
	 * getter of report frequency
	 * @return report frequency
	 */
	public int getReportFrequency() {
		return reportFrequency;
	}
	
	
	/***
	 * setter of report frequency
	 * @param reportFrequency - report frequency
	 */
	public void setReportFrequency(int reportFrequency) {
		this.reportFrequency = reportFrequency;
	}
	
	/***
	 * getter of special frequency
	 * @return special frequency
	 */
	public int getSpecialFrequency() {
		return specialFrequency;
	}
	
	/***
	 * setter of special frequency
	 * @param specialFrequency - special frequency
	 */
	public void setSpecialFrequency(int specialFrequency) {
		this.specialFrequency = specialFrequency;
	}
	
	
	/***
	 * getter of campaign id 
	 * @return campaign id 
	 */
	public long getCampaignId() {
		return campaignId;
	}
	
	/***
	 * setter of campaign id 
	 * @param campaignId - campaign id
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	
	
	/***
	 * getter of campaign version
	 * @return campaign version
	 */
	public int getCampaignVersion() {
		return campaignVersion;
	}
	
	/***
	 * setter of a campaign version
	 * @param campaignVersion - campaign version
	 */
	public void setCampaignVersion(int campaignVersion) {
		this.campaignVersion = campaignVersion;
	}
	
	
	/***
	 * getter of send frequency
	 * @return send frequency
	 */
	public int getSendFrequency() {
		return sendFrequency;
	}
	
	/***
	 * setter of send frequency
	 * @param sendFrequency - send frequency
	 */
	public void setSendFrequency(int sendFrequency) {
		this.sendFrequency = sendFrequency;
	}
	
	/***
	 * getter of target version
	 * @return  target version
	 */
	public int getTargetVersion() {
		return targetVersion;
	}
	
	
	/***
	 * setter of target version
	 * @param targetVersion - target version
	 */
	public void setTargetVersion(int targetVersion) {
		this.targetVersion = targetVersion;
	}

	public int getGeneralSettingVersion() {
		return generalSettingVersion;
	}

	public void setGeneralSettingVersion(int generalSettingVersion) {
		this.generalSettingVersion = generalSettingVersion;
	}
	
	
}

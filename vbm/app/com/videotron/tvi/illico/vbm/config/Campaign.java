package com.videotron.tvi.illico.vbm.config;

import java.util.Vector;

public class Campaign {

	/** campaign id **/
	private long campaignId; // XXXXXXXXXX, 10 digit
	
	/** campaign version **/
	private int campaignVersion; // 0~255
	
	/** target version **/
	private int targetVersion; // 0~255

	/** general configuration version **/
	private int generalConfigVersion;
	
	/** begin date of a campaign **/
	private long beginDate; //timestamp
	
	/** end date of a campaign **/
	private long endDate;  // timestamp
	
	/** measurement list in a campaign **/
	private Vector measurementList;
	
	/** index of global attribute **/
	private int global; // 0 for global, 1 for normal
	
	/** send frequency of a campaign **/
	private int sendFrequency; // 1(daily), 2(weekly), 3(monthly)
	
	/** report frequency of a campaign **/
	private int reportFrequency; // 0(hourly), 1(daily), 2(weekly), 3(monthly)
	
	/** special frequency of a campaign **/
	private int specialFrequency; 
	
	/** index whether this campaign is in target or not**/
	private boolean inTarget = false;
	
	
	/**
	 * constructor of campaign
	 */
	public Campaign(){
		measurementList = new Vector();
		
	}
	
	
	/**
	 * getter of campaign id
	 * @return campaignId
	 */
	public long getCampaignId() {
		return campaignId;
	}
	
	
	/**
	 * setter of campaign id
	 * 
	 * @param campaignId
	 */
	public void setCampaignId(long campaignId) {
		this.campaignId = campaignId;
	}
	
	
	/**
	 * getter of campaign version
	 * @return campaign version
	 */
	public int getCampaignVersion() {
		return campaignVersion;
	}
	
	
	/**
	 * setter of campaign version
	 * @param campaignVersion campaign version
	 */
	public void setCampaignVersion(int campaignVersion) {
		this.campaignVersion = campaignVersion;
	}
	
	
	/**
	 * getter of campaign id
	 * @return campaignId
	 */
	public int getGeneralConfigVersion() {
		return generalConfigVersion;
	}
	
	
	/**
	 * setter of campaign id
	 * 
	 * @param campaignId
	 */
	public void setGeneralConfigVersion(int generalConfigVersion) {
		this.generalConfigVersion = generalConfigVersion;
	}
	
	
	
	/**
	 * getter of campaign target version
	 * @return campaign target version
	 */
	public int getTargetVersion() {
		
		return targetVersion;
	}
	
	
	/**
	 * setter of campaign target version
	 * 
	 * @param targetVersion campaign target version
	 */
	public void setTargetVersion(int targetVersion) {
		this.targetVersion = targetVersion;
	}
	
	
	/**
	 * getter begin date of campaign
	 * 
	 * @return begin date of campaign
	 */
	public long getBeginDate() {
		return beginDate;
	}
	
	
	/**
	 * setter of begin date
	 * 
	 * @param beginDate begin date of a campaign
	 */
	public void setBeginDate(long beginDate) {
		this.beginDate = beginDate;
	}
	
	
	/**
	 * getter of end date
	 * 
	 * @return endDate end date of a campaign
	 */
	public long getEndDate() {
		return endDate;
	}
	
	/**
	 * setter of end date of a campaign
	 * 
	 * @param endDate end date of a campaign
	 */
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	
	
	/**
	 * getter of measurement list in a campaign
	 * @return list of measurements
	 */
	public Vector getMeasurementList() {
		return measurementList;
	}
	
	
	/**
	 * setter of measurement list of a campaign
	 * @param measurementList list of measurements of a campaign
	 */
	public void setApplications(Vector measurementList) {
		this.measurementList = measurementList;
	}
	
	
	/**
	 * getter of index of global of a  campaign 
	 * @return 0 for global, 1 for normal
	 */
	public int isGlobal() {
		return global;
	}
	
	/**
	 * setter of index of global of a campaign
	 * 
	 * @param global index of global, 0 for global, 1 for normal
	 */
	public void setGlobal(int global) {
		this.global = global;
	}
	
	
	/**
	 * set send frequency 0 for daily, 1 for weekly, 2 for monthly
	 * @return send frequency
	 */
	public int getSendFrequency() {
		return sendFrequency;
	}
	
	/**
	 * setter of send frequency
	 * @param sendFrequency
	 */
	public void setSendFrequency(int sendFrequency) {
		this.sendFrequency = sendFrequency;
	}
	
	/**
	 * getter of report frequency, 0 for hourly, 1 for daily, 2 for weekly, 3 for monthly
	 * @return report frequency
	 */
	public int getReportFrequency() {
		return reportFrequency;
	}
	
	/**
	 * setter of report frequency
	 * 
	 * @param reportFrequency report frequency of a campaign
	 */
	public void setReportFrequency(int reportFrequency) {
		this.reportFrequency = reportFrequency;
	}
	
	
	/**
	 * special frequency of a campaign
	 * 2011.11. it is applied only to weekly frequency
	 * sun -1, mon - 2, tue - 4, wed - 8, thur - 16, ...
	 * 
	 * @return sum of target date, for example sun and thur = 1 + 16 = 17 
	 */
	public int getSpecialFrequency() {
		return specialFrequency;
	}
	
	/**
	 * setter of special frequency
	 * @param specialFrequency special frequency
	 */
	public void setSpecialFrequency(int specialFrequency) {
		this.specialFrequency = specialFrequency;
	}
	
	/*
	public void setInTarget(boolean inTarget){
		this.inTarget = inTarget;
	}
	
	public boolean getInTarget(){
		return inTarget;
	}
	public ArrayList getAppList(){
		ArrayList apps = new ArrayList();
		
		boolean duplicated = false;
		String measurement = null;
		for(int i=0; i<measurementList.size(); i++){
			measurement = (String) measurementList.get(i);  
			if(measurement == null) continue;
			duplicated = false;
			for(int j=0; j<apps.size(); j++){
				// in the above, measurement is checked null.
				if(apps.get(j).equals(measurement)){
					duplicated = true;
				}				
			}
			if(!duplicated) apps.add(measurement);			
		}
				
		return apps;
	}
	
	*/
	
}

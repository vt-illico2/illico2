package com.videotron.tvi.illico.vbm.config;



import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.Timer;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;



import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.schedule.ApplicationTrigger;
import com.videotron.tvi.illico.vbm.schedule.CampaignTrigger;
import com.videotron.tvi.illico.vbm.schedule.LogFileWriter;
import com.videotron.tvi.illico.vbm.schedule.LogScheduler;
import com.videotron.tvi.illico.vbm.schedule.TriggerScheduler;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.TimerWrapper;
import com.videotron.tvi.illico.vbm.util.VbmCode;



/***
 * Class to manage configuration. All classes wanting to manage configuration should access this ConfiguratinManager
 * 
 * @author swkim
 *
 */
public class ConfigurationManager {

	/**
	 * LogConfiguration to store application's configuration.
	 */
	private static ConfigurationManager instance = null;
	
	/** data store for old configuration **/
	private static Configuration oldConfig = null;
	/** data store for old target **/
	private static Target oldTarget = null;

	/** data store for new configuration **/
	private static Configuration newConfig = null;
	/** data store for new target **/
	private static Target newTarget = null;
	
	/** data store for temporary configuration **/
	private static Configuration tmpConfig = null;
	/** data store for temporary target list **/
	private static Target tmpTarget = null;
	
	/** temporary storage of trigger status. when a new trigger executed or closed, it is managed in triggerStatusList **/
	private static HashMap triggerStatusList = null;
	
	/** data store for temporary target list **/
	private static boolean initialized = false;
	
	
	/** indicator to set target updated **/
	private static boolean targetUpdated = false;
	
	/** indicator to set configuration update **/
	private static boolean configUpdated = false;
	
	private static boolean duplicated = false;
	
	private final String SEP = "|";
	
	private int configStatus = VbmCode.INIT_VALUE;
	
	private static int targetVersion = VbmCode.INIT_VALUE;
	/***
	 * method to get configuration manager instance.
	 * 
	 * @return static instance of configuration manager
	 */
	public static ConfigurationManager getInstance() {
		if (instance == null) {
			instance = new ConfigurationManager();
		}
		return instance;
	}
	
	/***
	 * private constructor of configuration manager
	 * 
	 */
	private ConfigurationManager() {
		oldConfig = new Configuration();
		newConfig = new Configuration();
		tmpConfig = new Configuration();
		oldTarget = new Target();
		newTarget = new Target();
		tmpTarget = new Target();
	
		triggerStatusList = new HashMap();	
		
		TriggerMeasurementList.getInstance();
	}

	/****
	 * method to get newest configuration
	 * 
	 * @return new configuration broadcasted
	 */
	public synchronized Configuration getConfiguration(){
		return newConfig;
	}
	
	/***
	 * method to get newest target list 
	 * 
	 * @return new target list broadcasted
	 */
	public synchronized Target getTarget(){
		return newTarget;
	}
		

	/***
	 * method to return status of indicator for compression
	 * @return status of compression in new configuration
	 */
	public int isCompressed(){
		return newConfig.getCompression();
	}
	
	
	private void processInit(){

		if (Log.DEBUG_ON) {
			Log.printDebug(" ConfigurationManager : BC : it is first configuration. so set initial config and return.");
		}
		// Initially new config is tmp config and new target is old target
    	newConfig = tmpConfig;
		newTarget = tmpTarget;
    	
		
		SharedMemory sm = SharedMemory.getInstance();		
		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		if(logCache.size() == newConfig.getBufferSize()) {
			if (Log.DEBUG_ON) {
    			Log.printDebug(" ConfigurationManager : BC : size of buffer in VbmLog is same. So do not recreate buffer." + newConfig.getBufferSize());
			}
		} else {
			logCache.setSize(newConfig.getBufferSize());
			if (Log.DEBUG_ON) {
    			Log.printDebug(" ConfigurationManager : BC : size of buffer in VbmLog changed. Change size into " + newConfig.getBufferSize());
			}
		}


		configUpdated = true;
		targetUpdated = true;
		
		// Initially old target and old cofig is null. So execute setNewConfigAndTarget
		setNewConfigurationAndTarget();
		
	}
	
	/**
	 * if there is a change in target broadcasting, start to build new buffer list
	 * 
	 * @param is input stream of configuration broadcasted
	 */
	public synchronized void buildTarget(InputStream is){
				
		if (Log.DEBUG_ON) {
			Log.printDebug(" ConfigurationManager : BT : new target configuration is arrived. start to build new target.");
		}
		
		tmpTarget = new Target();
		tmpTarget.buildCampaignList(Misc.getMacAddress(), is);
		
		if(configStatus == VbmCode.INIT_VALUE){
			// init status
			if(targetVersion == VbmCode.INIT_VALUE) {
				// init target
				targetUpdated = true;
				configStatus = VbmCode.MEMBER_STATUS;
				targetVersion = tmpTarget.getTargetVersion();
			} else if(targetVersion == tmpTarget.getTargetVersion()) {
				// same target
				targetUpdated = true;
				configStatus = VbmCode.MEMBER_STATUS;
			} else {
				// diff target
				targetUpdated = true;
				configStatus = VbmCode.MEMBER_STATUS;
				targetVersion = tmpTarget.getTargetVersion();
			}
			
		} else if(configStatus == VbmCode.CONFIG_STATUS) {
			// there is already configuration. so execute and reset
			
			if(!initialized){
								
				processInit();
				
				targetUpdated = false;
				configUpdated = false;
				duplicated = false;
				configStatus = VbmCode.INIT_VALUE;
				
			} else if(duplicated) {
				// same config came, then ignore it
				
				targetUpdated = false;
				configUpdated = false;
				duplicated = false;
				configStatus = VbmCode.INIT_VALUE;
			} else {
				// not init, diff config came,
				

    			configUpdated = true;
    			targetUpdated = true;
    			
				setNewConfigurationAndTarget();
				
				targetUpdated = false;
				configUpdated = false;
				duplicated = false;
				configStatus = VbmCode.INIT_VALUE;
				
			}
			
		} else {
			// config status = member, continue to be member state.
			if (Log.DEBUG_ON) {
				Log.printDebug(" ConfigurationManager : BT : status : MEMBER - MEMBER. update. no this case. ignore it");
			}
			
			targetVersion = tmpTarget.getTargetVersion();
			targetUpdated = true;
			//configUpdated = false;
			//duplicated = false;
			configStatus = VbmCode.MEMBER_STATUS;
			
		}
		
		
		// swkim changed above . 2013.11.26
		/*
		if(!configUpdated) {
			if (Log.DEBUG_ON) {
    			Log.printDebug("ConfigurationManager : BT : configuration is not updated. So ignore target. " + tmpTarget.getTargetVersion());
    		}
		}
		// when it needs to trigger, configuration is set
		if(configUpdated) {

			
			if (Log.DEBUG_ON) {
				Log.printDebug(" ConfigurationManager : BT : target - updated. configuration - updated. execute setNewConfigAndTarget");
			}
			
			// target also updated, so set new configuration and target
			targetUpdated = true;
			
			setNewConfigurationAndTarget();
			
			
			configUpdated = false;
			targetUpdated = false;
			
		} else {

			if (Log.DEBUG_ON) {
				Log.printDebug(" ConfigurationManager : BT : target - updated. configuration - not updated. wait for configuration.");
			}
			targetUpdated = true;
		}
		
		*/
		
	}
		
		
	/**
	 * 
	 * when there is a new configuration file broadcasted, then build new configuration and applies changes in application
	 * @param is file input stream of configuration file
	 */
	public synchronized void buildConfiguration(InputStream is){
		/**
		 * build configuration from broadcasting signals
		 * 
		 */
		SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = null;


        if (Log.DEBUG_ON) {
			Log.printDebug(" ConfigurationManager : BC : start to build configuration.");
        }
		
        try{
        	
        	/*
        	DataInputStream in = new DataInputStream(is);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null) {
				System.out.println(strLine);
			}
			*/
        	parser = factory.newSAXParser();

        	tmpConfig = new Configuration();
            parser.parse(is ,tmpConfig);

        	if (Log.DEBUG_ON) {
    			Log.printDebug(" ConfigurationManager : BC : read and parsed tmp config");
        	}
        	
        	
        	if(configStatus == VbmCode.INIT_VALUE){
        		// init status, no member came
        		
        		if((newConfig == null) || (newConfig.getGeneralSettingVersion() == VbmCode.INIT_VALUE)) {
        			// initial application
        			
        			configUpdated = true;
        			duplicated = false;
        			configStatus = VbmCode.CONFIG_STATUS;
        			
        		} else if(tmpConfig.getGeneralSettingVersion() == newConfig.getGeneralSettingVersion()) {
        			// same configuration
        			
        			configUpdated = true;
        			duplicated = true;
        			configStatus = VbmCode.CONFIG_STATUS;
        			
        		} else {
        			// diffent config
        			
        			configUpdated = true;
        			duplicated = false;
        			configStatus = VbmCode.CONFIG_STATUS;
        			
        		}
        		
        	} else if (configStatus == VbmCode.MEMBER_STATUS) {
        		// already member came
        		
        		if((newConfig == null) || (newConfig.getGeneralSettingVersion() == VbmCode.INIT_VALUE)) {
        			// initial application
        		

        			processInit();
        			
        			configUpdated = false;
        			targetUpdated = false;
        			duplicated = false;
        			configStatus = VbmCode.INIT_VALUE;
        			
        		} else if(tmpConfig.getGeneralSettingVersion() == newConfig.getGeneralSettingVersion()) {
        			// same configuration
        			
        			// ignore it

        			configUpdated = false;
        			targetUpdated = false;
        			duplicated = false;
        			configStatus = VbmCode.INIT_VALUE;
        			
        		} else {
        			// diffent config
        			
        			configUpdated = true;
        			targetUpdated = true;
        			
        			setNewConfigurationAndTarget();

        			configUpdated = false;
        			targetUpdated = false;
        			duplicated = false;
        			configStatus = VbmCode.INIT_VALUE;
        				
        		}
        		
        		
        		
        	} else {
        		// already config came, continue to be config state
        		
        		configUpdated = true;
        		if((newConfig == null) || (tmpConfig.getGeneralSettingVersion() != newConfig.getGeneralSettingVersion())) {
        			duplicated = false;
        		} else {
        			duplicated = true;
        		}
        		configStatus = VbmCode.CONFIG_STATUS;
        	}
        	
        	/*
            if(newConfig.getGeneralSettingVersion() == VbmCode.INIT_VALUE){

        		if (Log.DEBUG_ON) {
        			Log.printDebug(" ConfigurationManager : BC : it is first configuration. so set initial config and return.");
        		}
        		// Initially new config is tmp config and new target is old target
            	newConfig = tmpConfig;
        		newTarget = tmpTarget;
            	
        		
        		SharedMemory sm = SharedMemory.getInstance();		
        		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
        		if(logCache.size() == newConfig.getBufferSize()) {
        			if (Log.DEBUG_ON) {
            			Log.printDebug(" ConfigurationManager : BC : size of buffer in VbmLog is same. So do not recreate buffer." + newConfig.getBufferSize());
        			}
        		} else {
        			logCache.setSize(newConfig.getBufferSize());
        			if (Log.DEBUG_ON) {
            			Log.printDebug(" ConfigurationManager : BC : size of buffer in VbmLog changed. Change size into " + newConfig.getBufferSize());
        			}
        		}

        		configUpdated = true;
        		// Initially old target and old cofig is null. So execute setNewConfigAndTarget
        		setNewConfigurationAndTarget();
        		
        		
            } else {
            	
            	//if((newConfig.getGeneralSettingVersion() != tmpConfig.getGeneralSettingVersion() ) 
            	//		|| (newConfig.getTargetSettingVersion() != tmpConfig.getTargetSettingVersion())){
	            //
            	//	if (Log.DEBUG_ON) {
            	//		Log.printDebug(" buildConfiguration : new config is arrived. set oldconfig into new");
                //	}
                //	
            	//	oldConfig = newConfig;
	            //	newConfig = tmpConfig;
	            //	
	        		
            	//} else {
            	//	if (Log.DEBUG_ON) {
            	//		Log.printDebug(" buildConfiguration : new config is arrived but same version");
                //	}
                //	
            	//}
            	
            	//if(newConfig.getGeneralSettingVersion() == tmpConfig.getGeneralSettingVersion()) {
            	//	if (Log.DEBUG_ON) {
            	//		Log.printDebug(" buildConfiguration : general config duplicated. So ignore it");
                //	}
                //	return;
            	//}
            	
            	// move config and move target is done in setNewConfigurationAndTarget
            	// swkim changed in 2013.11.26
            	//moveConfig();
            	
            	//if (Log.DEBUG_ON) {
        		//	Log.printDebug("ConfigurationManager : BC : value of old configuration.");
        		//}
            	//oldConfig.printValue();
            	
            	//if (Log.DEBUG_ON) {
        		//	Log.printDebug("ConfigurationManager : BC : value of new configuration.");
        		//}
            	//newConfig.printValue();
            
            	//setNewConfigurationAndTarget();
            	
            	//configUpdated = true;
            	//if (Log.DEBUG_ON) {
        		//	Log.printDebug("ConfigurationManager : BC : configuration is processed. Waiting for target updated.");
        		//}
            	
            	//if((newConfig.getGeneralSettingVersion() != tmpConfig.getGeneralSettingVersion()) &&
	            //		(newConfig.getTargetSettingVersion() != tmpConfig.getTargetSettingVersion())) {
	            //	 
	        	//	if (Log.DEBUG_ON) {
	        	//		Log.printDebug("ConfigurationManager : BC : new configuration and new target is arrived. execute setNewConfigAndTarget");
	        	//	}
	            //	setNewConfigurationAndTarget();
	            //	
	            	
	            //} else if(newConfig.getGeneralSettingVersion() != tmpConfig.getGeneralSettingVersion()){
	
	        	//	if (Log.DEBUG_ON) {
	        	//		Log.printDebug("ConfigurationManager : BC : only configuration is updated.execute updateConfigOnly");
	        	//	}
	            	
	            //	updateConfigurationOnly();
	            	
	            //} else if(newConfig.getTargetSettingVersion() != tmpConfig.getTargetSettingVersion()) {
	
	        	//	if (Log.DEBUG_ON) {
	        	//		Log.printDebug("ConfigurationManager : BC : only target is updated.");
	        	//	}
	            	
	            //	
	            //	updateTargetOnly();
	            //} else {
	
	            //	if (Log.DEBUG_ON) {
	        	//		Log.printDebug("ConfigurationManager : BC : configuration and target are same as before. so discard them."
	        	//				+ newConfig.getGeneralSettingVersion() + " " + newConfig.getTargetSettingVersion());
	        	//	}
	            //	
	            //}
	            
	            
	            // if old configuration and new configuration is the same, then do nothing.            
            } // if for initial value    
            */
        } catch(Exception e){
        	e.printStackTrace();
        }
        /*
        // start new trigger to store log files into HDD
        // when to change this check LogFileWriter
        if(newConfig.getStoreInterval() != oldConfig.getStoreInterval()){
        	Timer timer = new Timer();
        	timer.schedule(new LogFileWriter(newConfig.getStoreInterval()), newConfig.getStoreInterval() * VbmCode.MILISECOND);
        }
        
    	SharedMemory sm = SharedMemory.getInstance();		
		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		

        if(newConfig.getBufferSize() != oldConfig.getBufferSize()){
        	if (Log.DEBUG_ON) {
    			Log.printDebug("ConfigurationManager : BC : size of log buffer in new config is different from buffer size in old config. so recreate buffer");
        	}
        	logCache.setSize(newConfig.getBufferSize());    		
        }


        if(newConfig.getBufferSize() != logCache.size()){
        	if (Log.DEBUG_ON) {
    			Log.printDebug("ConfigurationManager : BC : size of log buffer is different from buffer size in config. so recreate buffer");
        	}
    		logCache.setSize(newConfig.getBufferSize());
        }
        
        initialized = true;

	*/
	}
	
	/***
	 * add global campaign id into target list
	 * 
	 */
	private void addGlobalCampaignIntoTarget(){
		Vector campaigns = newConfig.getCampaignList();
		Campaign campaign = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : AGCT : add global campaign into target list");
		}
		
		for(int i=0; i<campaigns.size(); i++){
			campaign = (Campaign)campaigns.get(i); 
			if(campaign.isGlobal() == VbmCode.IS_GLOBAL){
				if (Log.DEBUG_ON) {
        			Log.printDebug("ConfigurationManager : adGlobalCampaignIntoTarget : " + campaign.getCampaignId() + " is global. so add it in new target list");
				}
				newTarget.addCampaign(campaign.getCampaignId());
			}
		}
		
	}

	
	private void moveConfig(){

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MC : moved config new and old");
		}
		oldConfig = newConfig;
		newConfig = tmpConfig;
		

    	if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MC : old configuration in moveConfig.");
		}
    	oldConfig.printValue();
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MC : new configuration in moveConfig.");
		}
    	newConfig.printValue();
	}
	
	/***
	 * move temporary targets into new target and new targets to old target
	 */
	private void moveTarget(){
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MT : moved target new and old");
		}
	
		oldTarget = newTarget;
		newTarget = tmpTarget;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MT : move target of old config. ");
		}
		checkConfigurationValidity(oldConfig, oldTarget);
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : MT : move target of new config. ");
		}
		checkConfigurationValidity(newConfig, newTarget);
	}
	
	
	private void checkConfigurationValidity(Configuration config, Target target){
		Vector joinedCampaigns = new Vector();
		boolean existed = false;
				
		if(config == null) return;
		if(target == null) return;
		if(config.getCampaignList() == null){
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager : CCV : campaign in config is null. config =" + config.getGeneralSettingVersion());
			}
			return;
		}
		if(target.getJoinedCampaigns() == null){
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager : CCV : target in config is null. target =" + config.getTargetSettingVersion());
			}
			return;
		}
				
		for(int i=0; i<config.getCampaignList().size(); i++){
			existed = false;
			for(int j=0; j<target.getJoinedCampaigns().size(); j++){
				if ( ((Campaign)config.getCampaignList().get(i)).getCampaignId() == ((Long)target.getJoinedCampaigns().get(j)).longValue() ) {
					existed = true;
				}
			}
			
			if(existed){
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager : CCV : added campaign = " + ((Campaign)config.getCampaignList().get(i)).getCampaignId());
				}
				
				joinedCampaigns.add(config.getCampaignList().get(i));			
				
			}
		}
		
		config.setCampaignList(joinedCampaigns);
	}
	/**
	 * 
	 * if there are changes in configuration and target together, then it should check conditions.
	 * when target is already applied, then it triggers change and set flags false.
	 * if target change is not applied, then it set configuration flag true and wait for changes of target.
	 */
	private void setNewConfigurationAndTarget(){
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :   setNewConfigurationandTarget. setUnsetTriggerTask");	
		}
		// it means that configuration and target are all changed.
		// thus it should trigger changes when configuration and target are all changed.
		if(targetUpdated) {

			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  SNCT : assume target should follow configuration!!!");
			}
			
			if(initialized ){
				moveConfig();
				moveTarget();
				
			} else {
				
				// newTarget should be set in procssInit(). 
				// so check configuration validity in this module
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager : MT : move target of new config. ");
				}
				checkConfigurationValidity(newConfig, newTarget);
			}
			
	        addGlobalCampaignIntoTarget();


	        if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager : already target updated, so setNewConfigurationandTarget. unsetCampaigns : " + oldConfig.getCampaignList().size());
	        }
			//makeUnsetTriggerTask(newConfig, oldConfig, oldTarget);
	        unsetCampaigns();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  setNewConfigurationandTarget. setCampaigns : " + newConfig.getCampaignList().size());
			}
			//makeSetTriggerTask(newConfig, oldConfig, newTarget );
			setCampaigns();
			manageBuffer();
			
			//it means, when target is updated, target should trigger changes and set updated flags false;
			targetUpdated = false;
			configUpdated = false;
			
			if (newTarget.getJoinedCampaigns() == null || newTarget.getJoinedCampaigns().size() == 0) {
				if (Log.DEBUG_ON) {
					Log.printDebug("Configuration: newTarget.getJoinedCampaigns() is null or size is zero.");
					
					LogScheduler.getInstance().createSchedulerForAllLog();
				}
			}
		} else {
			// do nothing, set target to change to trigger
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  target is not updated. so wait for target updating.");
			}
			configUpdated = true;
		}
		
		
	}
	
	
	private void manageBuffer(){
        
        // start new trigger to store log files into HDD
        // when to change this check LogFileWriter
        if(newConfig.getStoreInterval() != oldConfig.getStoreInterval()){
        	Timer timer = new Timer();
        	timer.schedule(new LogFileWriter(newConfig.getStoreInterval()), newConfig.getStoreInterval() * VbmCode.MILISECOND);
        }
        
        
    	SharedMemory sm = SharedMemory.getInstance();		
		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		

        if(newConfig.getBufferSize() != oldConfig.getBufferSize()){
        	if (Log.DEBUG_ON) {
    			Log.printDebug("ConfigurationManager : BC : size of log buffer in new config is different from buffer size in old config. so recreate buffer");
        	}
        	logCache.setSize(newConfig.getBufferSize());    		
        }


        if(newConfig.getBufferSize() != logCache.size()){
        	if (Log.DEBUG_ON) {
    			Log.printDebug("ConfigurationManager : BC : size of log buffer is different from buffer size in config. so recreate buffer");
        	}
    		logCache.setSize(newConfig.getBufferSize());
        }
        
        initialized = true;

	}
	
	/**
	 * 
	 * if there is a change in configuration only, then executes trigger
	 * target is not effective in this case.
	 */
	
	/*
	private void updateConfigurationOnly(){
		
        // first change target list and add global campaign into new target

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  configuration only updated and no target changed. so execute updating now");
		}
		moveConfig();
		moveTarget();
		addGlobalCampaignIntoTarget();
	       
		// trigger changes and set flags false;		
		//makeUnsetTriggerTask(newConfig, oldConfig, oldTarget);
		//makeSetTriggerTask(newConfig, oldConfig, newTarget );
		
		unsetCampaigns();
		setCampaigns();
				
		configUpdated = false;
		targetUpdated = false;
			
	}
	
	
	*/
	/**
	 * if there is a change in target only, executes trigger and set flags
	 */
	
	/*
	private void updateTargetOnly(){
		// do not trigger
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  only target changed. updateTargetOnly");
		}
		
		if(targetUpdated) {

			moveTarget();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  copy new to old, tmp to new" + oldTarget.getJoinedCampaigns().size() + " " + newTarget.getJoinedCampaigns().size());
			}
	        addGlobalCampaignIntoTarget();

			//makeUnsetTriggerTask(newConfig, oldConfig, oldTarget);
			//makeSetTriggerTask(newConfig, oldConfig, newTarget );
    
        
	        unsetCampaigns();
	        setCampaigns();
			
			configUpdated = false;
			targetUpdated = false;
		}
		else {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  confiugration is not update. so just set flag true and wait for target updating");
			}
			// it means, when target is updated, target should trigger changes.
			targetUpdated = true;
		}
	}
	*/
	
	/***
	 * check method to confirm what is in target list
	 * 
	 * @param targetList
	 */
	private void checkTargetList(ArrayList targetList){

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  check target list : there is " + targetList.size() + " campaigns. list of target = ");
			for(int i=0; i<targetList.size(); i++){
				Log.printDebug(" [" + (Long)targetList.get(i) + " ] ");
			}
		}
		
		
		
	}
	
	/***
	 * unset event trigger in a campaign which was in old campaign and not in new campaign
	 * 
	 * @param newConfig - new configuration
	 * @param oldConfig - old configuration
	 * @param target - list of target in new configuration
	 */
	private void makeUnsetTriggerTask(Configuration newConfig, Configuration oldConfig, Target target){
	
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  makeUnsetTriggerTask");
		}
		
		if(oldConfig == null){			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager : old configuration is null. so there is nothing to unset");
			}
			return;
		}
		
		if(newConfig == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager : new configuration cannot be null. exit");
			}
			return;
		}
		
		
		Vector newCampaigns = newConfig.getCampaignList();
		Vector oldCampaigns = oldConfig.getCampaignList();

		boolean isExisting = true;
		boolean inTarget = false;
		Campaign oldCampaign = null;
		Campaign newCampaign = null;

		ArrayList targetList = null;
		if ((target == null) || (target.getJoinedCampaigns() == null)){
			targetList = new ArrayList();
		}else {
			targetList = target.getJoinedCampaigns();
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :   in makeUnsetTriggerTask, checking old target list ");
		}
		checkTargetList(targetList);
		
		for(int i=0; i<oldCampaigns.size(); i++){
			inTarget = false;
			
			oldCampaign = (Campaign) oldCampaigns.get(i);

			for(int j=0; j<targetList.size(); j++){
				if(oldCampaign.getCampaignId() == ((Long)targetList.get(j)).longValue()){
					inTarget = true;
					break;
				}
			}
			// if a new campaign is not in target list, then do not compare with old configuration.			
			if(!inTarget){
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + oldCampaign.getCampaignId() + " is not in new target. so discard it " );
				}
				continue;
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + oldCampaign.getCampaignId() + " is in old target. so unset trigger");
				}
			}
			
			isExisting = false;
			for(int j=0; j<newCampaigns.size(); j++){										
				newCampaign = (Campaign) newCampaigns.get(j);						
				if(newCampaign.getCampaignId() == oldCampaign.getCampaignId()) {
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager :  old campaign " + oldCampaign.getCampaignId() + " is in new campaign. so discard it");
					}
					isExisting = true;
					break;
				}
			}
			 
			if(!isExisting) {
				// if campaign is not existing, then stop campaign				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + oldCampaign.getCampaignId() + " is in old target and not in new campaign. so unset it");
				}
				stopCampaign(oldCampaign);
			}
		}
		
		
	}
	
	
	/**
	 * get campaign from configuration with campaign id
	 * 
	 * @param campaignId campaign id to get
	 * @param configuration configuration which campaign is in
	 * @return campaign whose campaign id is campaignId from configuration
	 */
	private Campaign getCampaign(long campaignId, Configuration configuration){
		Campaign campaign = null;
		
		if(campaignId < 0) return campaign;
		if(configuration == null) return campaign;
		
		Vector campaigns = configuration.getCampaignList();
		if(campaigns == null) return campaign;
		
		for(int i=0; i<campaigns.size(); i++){
			campaign = (Campaign) campaigns.get(i);
			if(campaignId == campaign.getCampaignId()) {
				return campaign;
			}
		}
		
		return null;
	}
	
	
	
	/**
	 * compare old campaign and new campaign which campaign id, campaign version and campaign target version.
	 * 
	 * @param oldCampaignId old campaign id 
	 * @param newCampaignId new campaign id
	 * @return true if all values are the same, otherwise false
	 */
	
	/*
	 * 
	private boolean compareCampaigns(long oldCampaignId, long newCampaignId){
		boolean isSame = false;
		
		Campaign oldCampaign = (Campaign) oldConfig.getCampaign(oldCampaignId);
		Campaign newCampaign = (Campaign) newConfig.getCampaign(newCampaignId);
		
		if(oldCampaign == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : old campaign is null");
			}
			return false;
		}
		
		if(newCampaign == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : new campaign is null");
			}
			return false;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  campare campaigns ");
			Log.printDebug("old : cid " + oldCampaign.getCampaignId() + " , cver " +  oldCampaign.getCampaignVersion()+ " , tver " +  oldCampaign.getTargetVersion() );
			Log.printDebug("new : cid " + newCampaign.getCampaignId() + " , cver " +  newCampaign.getCampaignVersion()+ " , tver " +  newCampaign.getTargetVersion() );
			
		}
		
		if( (oldCampaign.getCampaignId() == newCampaign.getCampaignId()) &&
				(oldCampaign.getCampaignVersion() == newCampaign.getCampaignVersion()) &&
				oldCampaign.getTargetVersion() == newCampaign.getTargetVersion()) {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : match campaign old " + oldCampaign.getCampaignId() + " , new " +  newCampaign.getCampaignId() );
				
			}
			
			isSame = true;
		}
			
		
		return isSame;
	}

	*/
	
	

	private boolean compareCampaigns(Campaign oldCampaign, Campaign newCampaign){
		boolean isSame = false;
		
		
		if(oldCampaign == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : old campaign is null");
			}
			return false;
		}
		
		if(newCampaign == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : new campaign is null");
			}
			return false;
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  campare campaigns ");
			Log.printDebug("old : cid " + oldCampaign.getCampaignId() + " , cver " +  oldCampaign.getCampaignVersion()+ " , tver " +  oldCampaign.getTargetVersion() );
			Log.printDebug("new : cid " + newCampaign.getCampaignId() + " , cver " +  newCampaign.getCampaignVersion()+ " , tver " +  newCampaign.getTargetVersion() );
			
		}
		
		if( (oldCampaign.getCampaignId() == newCampaign.getCampaignId()) &&
				(oldCampaign.getCampaignVersion() == newCampaign.getCampaignVersion()) &&
				oldCampaign.getTargetVersion() == newCampaign.getTargetVersion()) {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campare campaign : match campaign old " + oldCampaign.getCampaignId() + " , new " +  newCampaign.getCampaignId() );
				
			}
			
			isSame = true;
		}
			
		
		return isSame;
	}

	
	/**
	 * unset campaigns in old configuration
	 */
	
	
	

	private void unsetCampaigns(){
		
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  unsetCampaigns ");
		}
		
		Vector oldCampaigns = (Vector) oldConfig.getCampaignList();
		
		for(int i=0; i< oldCampaigns.size(); i++){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  unsetCampaigns : stop campaign " + ((Campaign)oldCampaigns.get(i)).getCampaignId() );
			}
			stopCampaign((Campaign)oldCampaigns.get(i));
		}
/*

		Vector oldCampaigns = (Vector) oldConfig.getCampaignList();
		Vector newCampaigns = newConfig.getCampaignList();
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  unsetCampaigns : old campaigns count =  " + oldCampaigns.size()
					+ ", new campaigns size = " + newCampaigns.size() );
		}
		
		boolean isExisted = false;
		for(int i=0; i<oldCampaigns.size(); i++) {
			isExisted = false;
			for(int j=0; j<newCampaigns.size(); j++) {

				//if(campaignId.equals((Long)newJoinedCampaigns.get(j))) {
				if(compareCampaigns((Campaign) oldCampaigns.get(i) ,(Campaign)newCampaigns.get(j))) {
					isExisted = true;
				}
			}
			
			if(!isExisted) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  unsetCampaigns : " + ((Campaign)oldCampaigns.get(i)).getCampaignId() + " is not reused. so stop campaign" );
				}
				// swkim changed 2013.10.21
				// parameter is campaign, so call stop campaign directly
				//unsetCampaign(oldCampaigns.get(i));
				stopCampaign((Campaign)oldCampaigns.get(i));
			}
		}
		
		*/
	}
	
	
	/*
	private void unsetCampaigns(){
		
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  unsetCampaigns ");
		}
		
		
		// already checked arraylist is null;
		ArrayList oldJoinedCampaigns = oldTarget.getJoinedCampaigns();
		ArrayList newJoinedCampaigns = newTarget.getJoinedCampaigns();
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  unsetCampaigns : old campaigns count =  " + oldJoinedCampaigns.size()
					+ ", new campaigns size = " + newJoinedCampaigns.size() );
		}
		
		boolean isExisted = false;
		Long campaignId = null;
		for(int i=0; i<oldJoinedCampaigns.size(); i++) {
			isExisted = false;
			campaignId = (Long)oldJoinedCampaigns.get(i); 
			for(int j=0; j<newJoinedCampaigns.size(); j++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  unsetCampaigns : checking " + campaignId + " with " + (Long) newJoinedCampaigns.get(j) );
				}
				//if(campaignId.equals((Long)newJoinedCampaigns.get(j))) {
				if(compareCampaigns(campaignId.longValue() ,((Long)newJoinedCampaigns.get(j)).longValue())) {
					isExisted = true;
				}
			}
			
			if(!isExisted) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  unsetCampaigns : " + campaignId + " is not reused. so stop campaign" );
				}
				
				unsetCampaign(campaignId.longValue());
			}
		}
	}
	
	*/
	
	/**
	 * unsent a campaign whose campaign id is campaignId
	 * @param campaignId campaign id to be unset
	 */
	
	private void unsetCampaign(long campaignId) {
		
		
		Vector oldCampaigns = oldConfig.getCampaignList();
		Campaign campaign = null;
		for(int i=0; i<oldCampaigns.size(); i++) {
			campaign = (Campaign) oldCampaigns.get(i);
			if (campaign.getCampaignId() == campaignId) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  unsetCampaign : found that campaign. stop it " + campaignId );
				}
				stopCampaign(campaign);
			}
		}
	}
	
	/**
	 * set campaigns in new configuration
	 */

	
	private void setCampaigns(){
		

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  setCampaigns ");
		}

		Vector newCampaigns = newConfig.getCampaignList();
		
		for(int i=0; i<newCampaigns.size(); i++){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  setCampaigns : " + ((Campaign)newCampaigns.get(i)).getCampaignId() + " is not reused. so start campaign" );
			}
			startCampaign((Campaign)newCampaigns.get(i));
			
			// swkim added to set campaign ends 2013.11.14
			setCampaignEnd((Campaign)newCampaigns.get(i));
		}


		/*
		Vector oldCampaigns = oldConfig.getCampaignList();
		Vector newCampaigns = newConfig.getCampaignList();
		
		boolean isExisted = false;

		for(int i=0; i<newCampaigns.size(); i++) {
			isExisted = false;
			for(int j=0; j<oldCampaigns.size(); j++) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  setCampaigns : checking " + ((Campaign)oldCampaigns.get(i)).getCampaignId() 
							+ " with " + ((Campaign)oldCampaigns.get(j)).getCampaignId() );
				}
				
				//if(campaignId.equals((Long)oldJoinedCampaigns.get(j))) {
				if(compareCampaigns((Campaign)newCampaigns.get(i), (Campaign)oldCampaigns.get(j))) {
					isExisted = true;
				}
			}
			
			if(!isExisted) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  setCampaigns : " + ((Campaign)newCampaigns.get(i)).getCampaignId() + " is not reused. so start campaign" );
				}
				// swkim changed 2013.10.21
				startCampaign((Campaign)newCampaigns.get(i));
			}
		}
		
		*/
	}
	
	
	/*
	private void setCampaigns(){
		

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  setCampaigns ");
		}
		
		// already checked arraylist is null;
		ArrayList oldJoinedCampaigns = oldTarget.getJoinedCampaigns();
		ArrayList newJoinedCampaigns = newTarget.getJoinedCampaigns();
		
		
		boolean isExisted = false;
		Long campaignId = null;
		for(int i=0; i<newJoinedCampaigns.size(); i++) {
			isExisted = false;
			campaignId = (Long)newJoinedCampaigns.get(i); 
			for(int j=0; j<oldJoinedCampaigns.size(); j++) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  setCampaigns : checking " + campaignId + " with " + (Long) newJoinedCampaigns.get(j) );
				}
				
				//if(campaignId.equals((Long)oldJoinedCampaigns.get(j))) {
				if(compareCampaigns(campaignId.longValue() ,((Long)oldJoinedCampaigns.get(j)).longValue())) {
					isExisted = true;
				}
			}
			
			if(!isExisted) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  setCampaigns : " + campaignId + " is not reused. so start campaign" );
				}
				
				setCampaign(campaignId.longValue());
			}
		}
	}
	
	*/
	
	/**
	 * set a campaign whose campaign id is campaignId
	 * @param campaignId campaign id
	 */
	private void setCampaign(long campaignId) {
		Vector newCampaigns = newConfig.getCampaignList();
		Campaign campaign = null;
		for(int i=0; i<newCampaigns.size(); i++) {
			campaign = (Campaign) newCampaigns.get(i);
			if (campaign.getCampaignId() == campaignId) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  setCampaign : found that campaign. start it " + campaignId );
				}
				
				startCampaign(campaign);
			}
		}
	}
	
	
	/**
	 * 
	 * get trigger list which is changed in new configuration with old configuration
	 * 
	 * @param newConfig new configuration
	 * @param oldConfig old configuration
	 * @return trigger list
	 * 
	 */
	private void makeSetTriggerTask(Configuration newConfig, Configuration oldConfig, Target target){
	
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  makeSetTriggerTask");
		}
		
		if(oldConfig == null){
			oldConfig = new Configuration();
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  old configuration is null. so create new one to make set trigger task.");
			}
		}
		
		if(newConfig == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  new configuration cannot be null. exit");
			}
			return;
		}
		
		Vector newCampaigns = newConfig.getCampaignList();
		Vector oldCampaigns = oldConfig.getCampaignList();

		boolean isNew = true;
		boolean inTarget = false;
		Campaign oldCampaign = null;
		Campaign newCampaign = null;

		ArrayList targetList = null;
		
		if(target == null){
			targetList = new ArrayList();
		}else {
			targetList = target.getJoinedCampaigns();
		}
		

		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :   in makeSetTriggerTask, checking new target list ");
		}
		checkTargetList(targetList);
		
		if(newCampaigns == null){
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  campaign in new config is null. so return.");
			}
			return;
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  in new campaign, there is " + newCampaigns.size() + " campaigns and " + targetList.size() + " targets");
			}
		}
		
		long now = Calendar.getInstance().getTimeInMillis();
		
		for(int i=0; i<newCampaigns.size(); i++){
			inTarget = false;

			newCampaign = (Campaign) newCampaigns.get(i);
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  now = " + now + " , begin = " + newCampaign.getBeginDate() + " , end = "  + newCampaign.getEndDate());
			}
			// if campaign ended, we do not care this campaign
			if ( newCampaign.getEndDate() < now) {
				if (Log.DEBUG_ON) {
					Log.printDebug(" ConfigurationManager :makeSetTriggerTask : end date of campaign is already passed. so discard this campaign" + newCampaign.getCampaignId());
				}
				continue;
			}
			
			for(int j=0; j<targetList.size(); j++){
				//System.out.println(" VBM : campaign id [" + newCampaign.getCampaignId() + "] and target id [" + ((Long)targetList.get(j)).longValue() + "]");
				if(newCampaign.getCampaignId() == ((Long)targetList.get(j)).longValue()){
					inTarget = true;
					break;
				}
			}
			// if a new campaign is not in target list, then do not compare with old configuration.			
			if(!inTarget) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  VBM : " + newCampaign.getCampaignId() + " is not in target list. so discard it.");
				}
				continue;
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  VBM : " + newCampaign.getCampaignId() + " is  in target list. so continue with it.");
				}
			}
			
			isNew = true;
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  size of old campaign" + oldCampaigns.size() );
			}
			
			for(int k=0; k<oldCampaigns.size(); k++){
										
				oldCampaign = (Campaign) oldCampaigns.get(k);
						
				if(newCampaign.getCampaignId() == oldCampaign.getCampaignId()) {
					
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager : " + newCampaign.getCampaignId() + " is in old campaign list");
					}
					if(newCampaign.getCampaignVersion() == oldCampaign.getCampaignVersion()){
						// this campaign is not chagned,so do nothign.
						if (Log.DEBUG_ON) {
							Log.printDebug("ConfigurationManager :  old campaign version and new campaign version is the same. so discard it");
						}
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("ConfigurationManager :  version changed. so stop campaing and restart it");
						}
						
						// same campaign but version changed
						stopCampaign(oldCampaign);
						startCampaign(newCampaign);
					}
					
					isNew = false;
					break;
				}
			}
			
			if(isNew) {
				// campaign is a brand new, so set schedule to trigger
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + newCampaign.getCampaignId() + " is a new campaign. so start this campaign");
				}
				startCampaign(newCampaign);
			}else {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + newCampaign.getCampaignId() + " is not a new campaign. so continue to work");
				}
			}
		}
		
		
	}
	
	
	

	
	/***
	 * stop campaign in an old configuration  in case new configuration is come
	 * 
	 * @param campaign
	 *
	private void unsetMeasurement(Measurement measurement){
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  unsetMeasurement called with " + measurement.getMeasurementId());
		}
		
		if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
		// if there is only one reference to measurement, then stop measuring for that measurement
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  it is trigger measurement. " + measurement.getMeasurementId());
			}
					
			int count = checkMeasurementInTriggerList(measurement.getMeasurementId());
			if(count <= VbmCode.LAST_ONE ){
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  count in triggerList is 1. so set value to 1 to stop trigger. count = " + count);
				}
				// createStopTriggerTask(measurement, VbmCode.INIT_VALUE);
				setMeasurementInTriggerList(measurement.getMeasurementId(), VbmCode.FIRST_ONE);
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  count in triggerList is not 1. so reduce value. count = " + count);
				}
				setMeasurementInTriggerList(measurement.getMeasurementId(), --count);
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  removing trigger measurement list of campaing " + 
					measurement.getCampaignId() + " and measurement " + measurement.getMeasurementId());
			}
			// TriggerMeasurementList will be removed in VbmLog when a log is pushed into VBMC.
			
		} else {
			createStopEventMeasurement(measurement, VbmCode.INIT_VALUE);
		}
		
		//triggerStatusList.put(Long.toString(measurement.getMeasurementId()), Integer.toString(--count));
	}
	
	
	/***
	 * start a campaign when new configuration is come and there is a new campaign
	 * 
	 * @param campaign - campaign in a new configuration
	 *
	private void setMeasurement (Measurement measurement, long beginDate){
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  startCampaign with campaign =" +  measurement.getMeasurementId());
		}
		
		int count = checkMeasurementInTriggerList(measurement.getMeasurementId());
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  measurementId =" + measurement.getMeasurementId() + ", value in triggerStatusList = " + count);
		}
		// if there is no trigger for measurement, then start trigger task. if there is already trigger started, then do not create trigger task			
		// stop trigger checks count of trigger list. so it should be invoked everytime.
		
		if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
			// case of trigger measurement
			if(TriggerMeasurementList.getInstance().count(measurement.getCampaignId(), measurement.getMeasurementId()) == VbmCode.FIRST_ONE ) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  create start trigger measurement trigger task : mid : " + measurement.getMeasurementId() + ", appid" + measurement.getAppId());
				}
				createStartTriggerMeasurement(measurement, beginDate);
			}				
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  ConfigurationManager : addming trigger measurement list of campaing " + 
					measurement.getCampaignId() + " and measurement " + measurement.getMeasurementId());
			}
			setMeasurementInTriggerList(measurement.getMeasurementId(), ++count);  // is this needed?
			// TriggerMeasurementList and TriggerMeasurementCounter is added in ApplicationTrigger.
			
		} else {
			// case of event measurement 
			if(count == VbmCode.FIRST_ONE ) {
				if (Log.DEBUG_ON) {
					Log.printDebug(" ConfigurationManager :create start event measurement trigger task : mid : " + measurement.getMeasurementId() + ", appid" + measurement.getAppId());
				}
				createStartEventMeasurement(measurement, beginDate);				
			}
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  create stop event measurement trigger task : mid : " + measurement.getMeasurementId() + ", appid" + measurement.getAppId());
			}
			createStopEventMeasurement(measurement, beginDate);

			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  measurementId =" + measurement.getMeasurementId() + ", value in triggerStatusList = " + count);
			}
			setMeasurementInTriggerList(measurement.getMeasurementId(), ++count);
		}
	}
	
	*/
	
	
	/***
	 * stop campaign in an old configuration  in case new configuration is come
	 * 
	 * @param campaign
	 */
	private void stopCampaign(Campaign campaign){

		Vector measurements = campaign.getMeasurementList();
		//Object value = null;
		int count = 0;
		Measurement measurement = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  stopCampaign called with " + campaign.getCampaignId());
		}
		
		for(int i=0; i<measurements.size(); i++){
			measurement = (Measurement) measurements.get(i);
			measurement.setCampaignId(campaign.getCampaignId());
			measurement.setCampaignVersion(campaign.getCampaignVersion());
			measurement.setTargetVersion(campaign.getTargetVersion());
			measurement.setReportFrequency(campaign.getReportFrequency());
			measurement.setSendFrequency(campaign.getSendFrequency());
			measurement.setSpecialFrequency(campaign.getSpecialFrequency());
			measurement.setGeneralSettingVersion(oldConfig.getGeneralSettingVersion());
			// swkim added config version to check trigger measurement
			
			if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
			// if there is only one reference to measurement, then stop measuring for that measurement
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager : stopcampaign:  it is trigger measurement. " + measurement.getMeasurementId());
				}
						
				count = checkMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId());
				count = count - 1;
				// 2012.08.21. swkim comment out this if because it only checked when trigger measurement executed.
				if(count == VbmCode.LAST_ONE ){
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager :  count in triggerList is less than 0. so created 'stop trigger'. count = " + count);
						Log.printDebug("ConfigurationManager :  stop trigger measurement ( " + oldConfig.getGeneralSettingVersion() + "," + 
								measurement.getCampaignId() + ", " + measurement.getMeasurementId() + ")");
					}
					// createStopTriggerTask(measurement, VbmCode.INIT_VALUE);
					
					// TriggerMeasurementList will be removed in VbmLog when a log is pushed into VBMC.
					
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager :  count in triggerList is not 1. so reduce value. count = " + count);
					}
					if(count < VbmCode.LAST_ONE) count = VbmCode.LAST_ONE;
					
				}
				// set count of measurementintrigerlist to count
				// count will be checked in applicationtrigger and if it is VbmCode.LAST_ONE, then trigger would be stopped
				//
				setMeasurementInTriggerList(campaign.getGeneralConfigVersion(),measurement.getMeasurementId(), count);
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager : stopcampaing :  it is event measurement. " + measurement.getMeasurementId());
				}
						
				count = checkMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId());
				count = count - 1;
				if(count == VbmCode.LAST_ONE ){
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager : stopcampaing :  stop event measurement ( " + 
								oldConfig.getGeneralSettingVersion() + "," + measurement.getMeasurementId() + ")");
					}
					createStopEventMeasurement( measurement, VbmCode.INIT_VALUE);
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager : stopcampaing :  reducing event measurement ( " + 
								oldConfig.getGeneralSettingVersion() + "," + measurement.getMeasurementId() + ") =  " + count);
					}
					if ( count < VbmCode.LAST_ONE) {
						count = VbmCode.LAST_ONE;					
					}
				}
				setMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId(), count);
				
			}
			
			//triggerStatusList.put(Long.toString(measurement.getMeasurementId()), Integer.toString(--count));
		}
	}
	
	private void setCampaignEnd(Campaign campaign){

		// 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		long delay = campaign.getEndDate() - System.currentTimeMillis();
		
		//Timer trigger = new Timer();
        if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager : SCE : created campaign trigger with delay, " + delay + ", cId = " + campaign.getCampaignId());
        }
       
        // false for calculated delay
        timer.schedule(new CampaignTrigger(campaign), delay );
	}
	
	/***
	 * start a campaign when new configuration is come and there is a new campaign
	 * 
	 * @param campaign - campaign in a new configuration
	 */
	private void startCampaign (Campaign campaign){


		
		int count = 0;
		
		Vector measurements = campaign.getMeasurementList();
		Measurement measurement = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  startCampaign with campaign =" + campaign.getCampaignId() + ", measurement size = " + measurements.size());
		}
		for(int i=0; i<measurements.size(); i++){
			
			measurement = (Measurement) measurements.get(i);
			measurement.setCampaignId(campaign.getCampaignId());
			measurement.setCampaignVersion(campaign.getCampaignVersion());
			measurement.setTargetVersion(campaign.getTargetVersion());
			measurement.setReportFrequency(campaign.getReportFrequency());
			measurement.setSendFrequency(campaign.getSendFrequency());
			measurement.setSpecialFrequency(campaign.getSpecialFrequency());
			measurement.setGeneralSettingVersion(newConfig.getGeneralSettingVersion());
			// swkim added config version to check trigger measurement
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  measurementId =" + measurement.getMeasurementId() + ", value in triggerStatusList = " + count);
			}
			// if there is no trigger for measurement, then start trigger task. if there is already trigger started, then do not create trigger task			
			// stop trigger checks count of trigger list. so it should be invoked everytime.
			
			if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
				// add measurement to TriggerMeasurementList
				TriggerMeasurementList.getInstance().add(measurement);
				
				count = checkMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId());				
				count = count+1;				
				setMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId(), count);
				
				// TriggerMeasurementList and TriggerMeasurementCounter is added in ApplicationTrigger.
				
				// case of trigger measurement
				if(count == VbmCode.FIRST_ONE ) {
					Log.printDebug(" ConfigurationManager :create start trigger measurement (version, mid, app) = (" + 
							newConfig.getGeneralSettingVersion() + ", " + measurement.getMeasurementId() + ", " + measurement.getAppId() + ")");
					createStartTriggerMeasurement(measurement, campaign.getBeginDate());

				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" ConfigurationManager :set count of start event measurement (version, mid, app) = (" + 
								newConfig.getGeneralSettingVersion() + ", " + measurement.getMeasurementId() + ", " + measurement.getAppId() + ") to " + count);
					
					}				
				}
				
			} else {
				// case of event measurement
				
				count = checkMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId());
				
				count = count +1 ;
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  measurementId =" + measurement.getMeasurementId() + ", value in triggerStatusList = " + count);
				}
				setMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId(), count);
				
				if(count == VbmCode.FIRST_ONE ) {
					if (Log.DEBUG_ON) {
						Log.printDebug(" ConfigurationManager :create start event measurement (version, mid, app) = (" + 
								newConfig.getGeneralSettingVersion() + ", " + measurement.getMeasurementId() + ", " + measurement.getAppId() + ")");
					}
					createStartEventMeasurement(measurement, campaign.getBeginDate());				
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" ConfigurationManager :set count of start event measurement (version, mid, app) = (" + 
								newConfig.getGeneralSettingVersion() + ", " + measurement.getMeasurementId() + ", " + measurement.getAppId() + ") to " + count);
					}
				}
				// stopEventMeasurement is created in stopCampaign
				/*
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  create stop event measurement trigger task : mid : " + measurement.getMeasurementId() + ", appid" + measurement.getAppId());
				}
				createStopEventMeasurement(measurement, campaign.getEndDate());
	*/
			}
		}
	}

	

	/***
	 * create start trigger task which is in new campaign in new configuration
	 * 
	 * @param measurement - measurement data to be triggered to start
	 * @param beginDate - start date of triggering
	 */
	private void createStartTriggerMeasurement(Measurement measurement, long beginDate){
		long delay = beginDate - Calendar.getInstance().getTimeInMillis();
		
		if(delay < 0) {			

			delay = 0;
			
			if(measurement.getReportFrequency() == VbmCode.REPORT_FREQ_HOURLY) {
				delay = Misc.getHourlyUploadTime(false);
			} else if(measurement.getReportFrequency() == VbmCode.REPORT_FREQ_DAILY) {
				delay = Misc.getDailyUploadTime(false);
			} else if(measurement.getReportFrequency() == VbmCode.REPORT_FREQ_WEEKLY) {
				delay = Misc.getWeeklyUploadTime(false);
			} else if(measurement.getReportFrequency() == VbmCode.REPORT_FREQ_MONTHLY) {
				delay = Misc.getMonthlyUploadTime(false);
			}  
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  delay is negative. so create trigger measurement at " + delay + " later : mId = " + measurement.getMeasurementId());
			}
			
		}
		TriggerScheduler.getInstance().createNewTrigger(delay, measurement, VbmCode.START_TRIGGER, newConfig.getGeneralSettingVersion(), true);
	}
	
	
	
	/***
	 * create start trigger task which is in new campaign in new configuration
	 * 
	 * @param measurement - measurement data to be triggered to start
	 * @param beginDate - start date of triggering
	 */
	private void createStartEventMeasurement(Measurement measurement, long beginDate){
		long delay = beginDate - Calendar.getInstance().getTimeInMillis();
		
		if(delay < 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  delay is negative. so trigger it now " + measurement.getMeasurementId());
			}
			delay = 0;
		}
		TriggerScheduler.getInstance().createNewTrigger(delay, measurement, VbmCode.START_TRIGGER, newConfig.getGeneralSettingVersion(), false);
	}
	
	
	
	/***
	 * create stop trigger task which is in new campaign in new configuration
	 * 
	 * @param measurement - measurement data to be triggered to stop
	 * @param beginDate - end date of triggering
	 */
	public void createStopEventMeasurement(Measurement measurement, long beginDate){
		//remove trigger task immedidately. in this case there is already a task for that measurement.
		// so in that already built trigger, it should check condition and not be fired nor remove count in triggerStatus.
		//long delay = beginDate - Calendar.getInstance().getTimeInMillis();
		// changed method to use System.getTimeInMillis for performance reason
		long delay = beginDate - System.currentTimeMillis();
		delay = (delay < 0) ? 0 : delay;
		TriggerScheduler.getInstance().createNewTrigger(delay, measurement, VbmCode.STOP_TRIGGER, oldConfig.getGeneralSettingVersion(), false);
	}
	
	/***
	 * check whether measurement is in trigger list
	 * when count of a measurement is 0, then created trigger task, an when count is 1, then stop trigger
	 * 
	 * @param measurementId
	 * 
	 * @return count of measurement executed
	 */
	public synchronized int checkMeasurementInTriggerList(int generalConfigVersion, long measurementId){
		
		Object value = null;
		int count = 0;
		
		value = triggerStatusList.get(Long.toString(generalConfigVersion) + SEP + Long.toString(measurementId));
		if(value != null) {
			try{ 
				count = Integer.parseInt((String) value);
			}catch (NumberFormatException nfe){
				if (Log.DEBUG_ON) {
		    		Log.printDebug("Error parsing count in map. " + (String) value);
		    	}
			}				
		}
		return count;
	}
	
	
	/***
	 * set measurement id in trigger list
	 * 
	 * @param measurementId - measurement id to be set
	 * @param count - count for measurement id to be set
	 */
	public synchronized void setMeasurementInTriggerList(int generalConfigVersion, long measurementId, int count){
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  setMeasurement : mid = " + measurementId + ", count = " +  count);
		}
		triggerStatusList.put(Integer.toString(generalConfigVersion) + SEP + Long.toString(measurementId), Integer.toString(count));
	}

	
	/***
	 * get list of measurement id for a target application
	 * it returns only event measurement (not included trigger measurement)
	 * 
	 * @param appName - target application name
	 * @return list of measurement ids in configuration for target application
	 * 
	 * @throws RemoteException
	 */
	public long [] getMeasurementIdsInConfigWithApp(String appName) throws RemoteException{
		long [] mIds = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  getMeasurementIdsInConfigWithApp " + appName);
		}
		ArrayList triggers = getMeasurementsInConfigWithApp(appName);
		
		if(triggers.size() ==0) return mIds;

		mIds = new long[triggers.size()];
		for(int i=0; i<mIds.length; i++){
			mIds[i] = ((Measurement)triggers.get(i)).getMeasurementId();
			if (Log.DEBUG_ON) {
				Log.printDebug("ConfigurationManager :  " + appName + " has a measurement " + mIds[i]);
			}
		}
		
		return mIds;
	}
	
	
	/**
	 * get list of measurement ids in configuration for target application
	 * 
	 * @param appName - target application name
	 * @return lsit of measurement ids
	 * 
	 * @throws RemoteException
	 */
	public ArrayList getMeasurementsInConfigWithApp(String appName) throws RemoteException{
		ArrayList triggerList = new ArrayList();
		
		boolean isDuplicated = false;
		Measurement measurement = null;
		Measurement newMeasurement = null;
		Campaign campaign = null;
		Vector campaigns = newConfig.getCampaignList();
		Vector measurementList = null;
		ArrayList joinedCampaigns = newTarget.getJoinedCampaigns();
		long now = Calendar.getInstance().getTimeInMillis();
		long campId = -1;
		//System.out.println(" VBM : campaings in new configuration = " + campaigns.size());
		for(int i=0; i<campaigns.size(); i++){
			campaign = (Campaign) campaigns.get(i);
			campId = campaign.getCampaignId();
			boolean existInConfig = false;
			
			for(int k=0; k<joinedCampaigns.size(); k++){
				if(campId == ((Long)joinedCampaigns.get(k)).longValue()){
					existInConfig = true;
				}
			}
			
			if(!existInConfig) {
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + appName + " is not in " +campaign.getCampaignId() );
				}
				
				continue;
			}
			// campaign is not started or alredy ended, then do not consider this campaign.
			if(campaign.getEndDate() < now) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  " + campaign.getCampaignId() + " is passed. discard it");
				}
				continue;
			}
			//System.out.println(" VBM : checking campaign id " + campaign.getCampaignId());
		
			measurementList = campaign.getMeasurementList();
			for(int j=0; j<measurementList.size(); j++){
				measurement = (Measurement) measurementList.get(j);
				
				//System.out.println(" VBM : checking measurement id " + measurement.getMeasurementId());
				// if measurement is a trigger measurement, then it should be managed by VBMC.
				// trigger measurement info is not given to normal application.
				// 2011.10.20
				if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR){
					if (Log.DEBUG_ON) {
						Log.printDebug(" ConfigurationManager : measurement " + measurement.getMeasurementId() + " is trigger measurement. so discard it");
					}
				}
				
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  checking " + appName + " with " + measurement.getAppId());
				}
				if(appName.equals(measurement.getAppId())){
					isDuplicated = false;
					for(int k=0; k<triggerList.size(); k++){
						//System.out.println(" VBM : checking " + measurement.getMeasurementId() + "' " + ((Long)triggerList.get(k)).longValue());
						if(measurement.getMeasurementId() == ((Measurement)triggerList.get(k)).getMeasurementId())
							isDuplicated = true;
					}
					if (!isDuplicated) {
						newMeasurement = new Measurement();
						if (Log.DEBUG_ON) {
							Log.printDebug("ConfigurationManager :  adding into measurement list for app " + appName + " : " + measurement.getMeasurementId());
						}
						newMeasurement.setMeasurementId(measurement.getMeasurementId());
						newMeasurement.setReportFrequency(campaign.getReportFrequency());
						newMeasurement.setSpecialFrequency(campaign.getSpecialFrequency());
						triggerList.add(newMeasurement);
					}
				}
			}
		}
		
		return triggerList;
	}
	

	/***
	 * get list of campaigns containing measurement id
	 * 
	 * @param measurementId - target measurement id
	 * 
	 * @return list of campaigns containing measurement id
	 */
	public ArrayList getCampaignsWithMeasurement(long measurementId){
		Set campaignList = new HashSet();
		Vector campaigns = newConfig.getCampaignList();
		Vector measurements = null;
		Campaign campaign = null;
		Measurement measurement = null;
		ArrayList joinedCampaigns = newTarget.getJoinedCampaigns();
		boolean isMember = false;
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  getCampaignsWithMeasurement " + measurementId);
		}
		for(int i=0; i<campaigns.size(); i++){
			
			campaign = (Campaign) campaigns.get(i);
			if(campaign.getEndDate() < System.currentTimeMillis()) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  getCampaignsWithMeasurement : campaign is out date" + campaign.getCampaignId());
				}
				continue;
			}
			isMember = false;
			
			for(int j=0; j<joinedCampaigns.size(); j++){
				if(campaign.getCampaignId() == ((Long)joinedCampaigns.get(j)).longValue()) {
					isMember = true;
				}
			
			}
			if(!isMember){
				if (Log.DEBUG_ON) {
					Log.printDebug("ConfigurationManager :  getCampaignsWithMeasurement " + measurementId);
				}
				continue;
			}
			measurements = campaign.getMeasurementList();
			for(int j=0; j<measurements.size(); j++){
				measurement = (Measurement) measurements.get(j);
				if(measurement.getMeasurementId() == measurementId) {
					if (Log.DEBUG_ON) {
						Log.printDebug("ConfigurationManager :  found campaign matching " + measurementId + ", with usage point id = " + measurement.getUsagePointId());
						Log.printDebug(", cId = " + measurement.getCampaignId() + ", cVer = " + measurement.getCampaignVersion() + ", tVer = " + measurement.getTargetVersion());
					}
					campaignList.add(measurement);
				}
			}
		}

		return (new ArrayList(campaignList));
	}
	
	
	/***
	 * get store interval to write logs into HDD
	 * 
	 * @return store interval
	 */
	public int getStoreInterval(){
		if(newConfig == null){
			return VbmCode.STORE_INTERVAL;
		}
		return newConfig.getStoreInterval();
	}
	
	public int getBufferSize(){
		if (newConfig == null) {
			return VbmCode.BUFFER_INIT_VALUE;
		}
		
		return newConfig.getBufferSize();
	}
	
	/***
	 * list of trigger measurement list containing measurement id
	 * 
	 * @param measurementId - target measurement id
	 * @return lsit of trigger measurement
	 */
	public ArrayList getTriggerMeasurementList(long measurementId){
		
		return TriggerMeasurementList.getInstance().remove(measurementId);
	}

	public boolean isInitialized(){
		return initialized;
	}
}

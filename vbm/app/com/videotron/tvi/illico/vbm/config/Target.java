package com.videotron.tvi.illico.vbm.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.MembershipKey;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.util.VbmCode;

/***
 * data store for target in configuration
 * it parsed configuration broadcasted and store them in this class
 * 
 * 
 * @author swkim
 *
 */
public class Target {

	/** campaign list which matches this STB **/
	private ArrayList campaigns = null;

	// when new target is created, it is already updated;
	/** indicator to set target updated **/
	private boolean isUpdated = true;
	
	private int targetVersion = -1;
	
	
	/***
	 * creator of target class
	 */
	public Target() {
		campaigns = new ArrayList();

	}
	
	/***
	 * get list of campaigned for this STB to join
	 * @return
	 */
	public  ArrayList getJoinedCampaigns(){
		if(campaigns == null) {
			campaigns = new ArrayList();
		}
		
		return campaigns;
	}
	
	/***
	 * setter of indicator to be updated
	 * @param setUpdated - updated indicator
	 */
	public void setIsUpdated(boolean setUpdated){
		isUpdated = setUpdated;
	}
	
	/***
	 * getter of indicator to be updated
	 * @return updated indicator
	 */
	public boolean getIsUpdated(){
		return isUpdated;
	}

	public int getTargetVersion(){
		return targetVersion;
	}
	
	

	/***
	 * add new campaign which includes this STB as target
	 * 
	 * @param campaignId - campaign id
	 */
	public void addCampaign(long campaignId){
		
		if(campaigns == null) {
			campaigns = new ArrayList();
		}
		boolean isNew = true;
		for(int i=0; i<campaigns.size(); i++){
			if(((Long)campaigns.get(i)).longValue() == campaignId) {
				isNew = false;
				break;
			}
		}
		if(isNew) {
			if (Log.DEBUG_ON) {
				Log.printDebug("Target : " + campaignId + " campaign is new one. So add it in target campaign list ");
			}
			System.out.println("Target : " + campaignId + " campaign is new one. So add it in target campaign list ");
			campaigns.add(new Long(campaignId));
		} else {
			//if (Log.DEBUG_ON) {
			//	Log.printDebug("Target : " + campaignId + " campaign is already in the target campaign list. so discard it");
			//}
		}
	}
	
	/**
	 * 
	 * parsing members.zip and check configurations in this configuration broadcasted
	 * 
	 * * 
	 * @param myMacAddress mac of this STB
	 * @param is - file input stream for configuration of broadcasting stream
	 */
	public void buildCampaignList(String myMacAddress,
			InputStream is) {


		ArrayList campaignList = new ArrayList();
		ZipInputStream zipInputStream = null;
		

		try {

			zipInputStream = new ZipInputStream(is);

			// Decompress a zipfile and read byte in stream
			// Loop over all of the entries in the zip file

			byte data[] = new byte[VbmCode.MAC_SIZE];
			//byte garbage[] = new byte[2]; // buffer for new line character
			String macAddress;
			ZipEntry entry;
			String tmp = null;
			String campId = null;
			
			//int idx = -1;
			// Get a file by ZipEntry

			StringTokenizer st = null;
			
			
			while ((entry = zipInputStream.getNextEntry()) != null) {
				if (!entry.isDirectory()) {
					String entryName = entry.getName();
					//System.out.println(" VBM target file name : [" + entryName + "]");
					// Read 12 bytes from file and check Mac Address
					
					//if (Log.DEBUG_ON) {
					//	Log.printDebug("file in members.zip file = " + entryName);
					//}
					
					st = new StringTokenizer(entryName, VbmCode.FILE_NAME_SEP);
					if(st.countTokens() != VbmCode.FILE_NAME_COUNT){
						//if (Log.DEBUG_ON) {
						//	Log.printError("Target : file in members.zip is not proper format." + entryName);
						//}
						continue;
					}
					campId = st.nextToken();
					st.nextToken();
					targetVersion = Integer.parseInt(st.nextToken());
					
					while ((zipInputStream.read(data)) != -1) {
						macAddress = new String(data);
						//if (Log.DEBUG_ON) {
						//	Log.printDebug("macAddress in config file = " + macAddress);
						//}
						
						if (myMacAddress.equalsIgnoreCase(macAddress)) {

							if (Log.DEBUG_ON) {
								Log.printDebug("new target is added " + entryName);
							}
							System.out.println("new target is added " + entryName);
							//campaignList.add(new Long(Long.parseLong(tmp)));
							campaignList.add(new Long(Long.parseLong(campId)));
							break;
						}						
					} 
				}
			}
		}

		catch (FileNotFoundException e) {
			System.err.println("FileNotFoundException");
			e.printStackTrace();
		}
		catch (IOException e) {
			System.err.println("IOException");
			e.printStackTrace();
		}
		finally {
			try {
				if (null != zipInputStream) {
					zipInputStream.close();
				}
				if (null != is) {
					is.close();
				}
			}
			catch (IOException e) {
				System.err.println("IOException in finally statement");
				e.printStackTrace();
			}
		}

		campaigns = campaignList;
		
	}

	

	public static void main(String[] args) {


		//String myMacAddress = "0021BE81632D";
		String myMacAddress = "0021BED5777D";

		String filePath = "C:/temp/vbm/VBM_DataOOB_하룻밤/content/members.zip";
		//String filePath = "C:/temp/content/members.zip";

		FileInputStream is = null;
		try{
			is = new FileInputStream (new File(filePath));
		}catch(Exception e){
			e.printStackTrace();
		}
		Target target = new Target();
		
		target.buildCampaignList(myMacAddress,is);		
		ArrayList myCampaignList = target.getJoinedCampaigns();

		target.addCampaign(10);
		target.addCampaign(11);
		target.addCampaign(10);
		if (null == myCampaignList || myCampaignList.size() == 0) {
			System.out.println(" VBM : I didn't belong to any campaign!!");
		}
		else {
			System.out.println(" VBM : I belong to below campaign(s)!!");
			for (int i = 0; i < myCampaignList.size(); i++) {
				//Long campaignId = (Long) myCampaignList.get(i);
				System.out.println(" VBM : [" + ((Long) target.getJoinedCampaigns().get(i)) + "]");
			}
		}

	}

}

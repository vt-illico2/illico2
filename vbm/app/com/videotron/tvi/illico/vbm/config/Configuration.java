package com.videotron.tvi.illico.vbm.config;


import java.util.Vector;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/***
 * parser of configuration and container of data in a configuration.
 * 
 * @author swkim
 *
 */
public class Configuration extends DefaultHandler{

	/** generaal setting version of a configuration **/
	private int generalSettingVersion; // 0~255
	/** target setting version of a configuration **/
	private int targetSettingVersion; // 0~255
	
	/** buffer size of a STB **/
	private int bufferSize; // 0~99999
	
	/** store interval to HDD for log writer **/
	private int storeInterval; // 0~99999
	/*
	private int isGlobal; // true for global, false for normal
	// if one of any campaign is global, this attributes is set to global
	private int sendFrequency;
	// if one of any campaign is daily, it is D.
	// if there is no daily campaign and one of any campaign is weekly, it is W.
	// else it is M
	
	private int reportFrequency;
	// if one of any campaign is daily, it is D.
	// if there is no daily campaign and one of any campaign is weekly, it is W.
	// else it is M
	
	private int specialFrequency;
	// if one of any campaign is daily, it is D.
	// if there is no daily campaign and one of any campaign is weekly, it is W.
	// else it is M
	
	*/
	
	/** list of campaigns in this configuratoin **/
	private Vector campaignList = null;
	// isGlobal and sendFrequency should be updated when new campaign is added
	// or a campaign is closed or updated.
	
	/** status of a configuration **/
	private boolean isUpdated = false;
	
	
	/** indicator to check upload connection needs compression **/
	private int isCompressed = 0;
	
	/** temporary store of a campaign **/
	private Campaign campaign = null;
	
	/** temporary store of a measurement **/
	private Vector measurementList = null;
	
	/** temporary store of a measurement **/
	private Measurement measurement = null;
	
	
	/**
	 * constructor of configuration
	 */
	public Configuration() {
		generalSettingVersion = VbmCode.INIT_VALUE;
		targetSettingVersion = VbmCode.INIT_VALUE;
		bufferSize = VbmCode.INIT_VALUE;
		storeInterval = VbmCode.INIT_VALUE;
		
		// campaingList is defined in creator
		campaignList = new Vector();

		
	}
	
	/** 
	 * getter of compression, 0 for need compression, 1 for not need compression
	 * @return status of compression
	 */
	public int getCompression(){
		return isCompressed;
	}
	
	/**
	 * setter of compression
	 * 
	 * @param isCompressed - status of compression
	 */
	public void setCompression(int isCompressed) {
		this.isCompressed = isCompressed;
	}
	
	/**
	 * getter of a general setting version of a campaign
	 * @return general setting version
	 */
	public int getGeneralSettingVersion() {
		return generalSettingVersion;
	}
	
	
	/**
	 * setter of a general setting version
	 * @param generalSettingVersion - general setting version
	 */
	public void setGeneralSettingVersion(int generalSettingVersion) {
		this.generalSettingVersion = generalSettingVersion;
	}
	
	/**
	 * getter of a target setting version for a configuration
	 * @return target setting version
	 */
	public int getTargetSettingVersion() {
		return targetSettingVersion;
	}
	
	
	/**
	 * setter of a target setting version
	 * @param targetSettingVersion - target setting version
	 */
	public void setTargetSettingVersion(int targetSettingVersion) {
		this.targetSettingVersion = targetSettingVersion;
	}
	
	
	/**
	 * getter of buffer size in a configuration. it is the buffer size of newest configuration
	 * @return buffer size in a campaign
	 */
	public int getBufferSize() {
		return bufferSize;
	}
	
	
	/** 
	 * setter of buffer size in a configuration
	 * @param bufferSize
	 */
	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}
	
	
	/**
	 * interval of log writer to store logs into HDD
	 * @return store interval
	 */
	public int getStoreInterval() {
		return storeInterval;
	}
	
	
	/**
	 * setter of a store interval for log writer
	 * @param storeInterval - store interval
	 */
	public void setStoreInterval(int storeInterval) {
		this.storeInterval = storeInterval;
	}
/*
	public int isGlobal() {
		return isGlobal;
	}
	public void setGlobal(int isGlobal) {
		this.isGlobal = isGlobal;
	}
	public int getSendFrequency() {
		return sendFrequency;
	}
	public void setSendFrequency(int sendFrequency) {
		this.sendFrequency = sendFrequency;
	}

	public int getReportFrequency() {
		return reportFrequency;
	}
	public void setReportFrequency(int reportFrequency) {
		this.reportFrequency = reportFrequency;
	}
	
	public int getSpecialFrequency() {
		return specialFrequency;
	}
	public void setSpecialFrequency(int specialFrequency) {
		this.specialFrequency = specialFrequency;
	}
	*/
	
	/***
	 * getter of a campaign list
	 * @return campaignList - list of campaigns in vector type
	 */
	public Vector getCampaignList(){
		
		if(campaignList == null) {
			campaignList = new Vector();
		}
		return campaignList;
	}
	
	
	
	/***
	 * setter of a campaigns in vector type
	 * 
	 * @param campaignList - list of a campaigns 
	 */
	public void setCampaignList(Vector campaignList){
		this.campaignList = campaignList;
	}
	
	
	/***
	 * getter of indicator of status of configuration updated
	 * @return status of a configuration, true for updated, false for not updated
	 */
	public boolean getIsUpdated(){
		return isUpdated;
	}
	
	/***
	 * setter of indicator of status of configuration updated
	 * @param isUpdated - status of configuration updated
	 */
	public void setIsUpdated(boolean isUpdated){
		this.isUpdated = isUpdated;
	}
	
	
	/** temporary storage of content parsed **/
	StringBuffer b = new StringBuffer(); 
	
	
	/***
	 * parsing rules to check start point of content parsed 
	 * 
	 */
	public void startElement(String uri, String localName, String qname,
			Attributes attributes) {

		b.setLength(0); // empty character buffer

		if (qname.equals("vt_vbm_config")) {
		} else if (qname.equals("general_setting_version")) {    	
	    } else if (qname.equals("target_setting_version")) {	    	
	    } else if (qname.equals("stb_setting")) {  	
	    } else if (qname.equals("buffer_size")) {	    	
	    } else if (qname.equals("store_interval")) {
	    } else if (qname.equals("compression")) {
	    } else if (qname.equals("campaign_setting")) {
	    	//System.out.println(" VBM : creating new Campaign object");
	    	// create new campaign and measurementList for each campaign_setting words in xml
	    	campaign = new Campaign();
	    	measurementList = new Vector(); 
	    	
	    } else if (qname.equals("camp_id")) {	    
	    } else if (qname.equals("camp_version")) {    	 
	    } else if (qname.equals("target_version")) {	    	    
	    }   else if (qname.equals("begin_date")) {	    	
	    } else if (qname.equals("end_date")) {	    	    
	    } else if (qname.equals("measure_list")) {
	    	//System.out.println(" VBM : creating new measusment");
	    	// craete new measurement for each measure_list 
	    	measurement = new Measurement();
	    	
	    } else if (qname.equals("global")) {	    	
	    } else if (qname.equals("application")) {	    	     	
	    } else if (qname.equals("usage_point")) {	    
	    } else if (qname.equals("measurement")) {	    	
	    } else if (qname.equals("send_frequency")) {	    	    
	    }else if (qname.equals("report_frequency")) {	    	
	    } else if (qname.equals("special_frequency")) {	    	
	    } else if (qname.equals("vt_vbm_config")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("createing configuration object");
			}
	    } else{
	    	if (Log.DEBUG_ON) {
				Log.printDebug("no matching attribute name" + qname);
			}    
	    }

	}
	/***
	 * parsing rules to get contents in configuration and set contents into data store
	 */
	public void endElement(String uri, String localName, String qname) {

		//System.out.println(" VBM : endElement = " + localName + ", " + qname);

		if (qname.equals("general_setting_version")) {		    
			if (Log.DEBUG_ON) {
				Log.printDebug("Configuration :  general setting version = " + b.toString());
			}
		    	setGeneralSettingVersion(Integer.parseInt(b.toString()));
		    	
	    } else if (qname.equals("target_setting_version")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : target setting version = " + b.toString());
	    	}
	    	setTargetSettingVersion(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("stb_setting")) {
	    	
	    } else if (qname.equals("buffer_size")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : VBM : buffer size = " + b.toString());
	    	}
	    	setBufferSize(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("store_interval")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : store interval size = " + b.toString());
	    	}
	    	setStoreInterval(Integer.parseInt(b.toString()));
	    } else if (qname.equals("compression")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : compression = " + b.toString());
	    	}
	    	setCompression(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("campaign_setting")) {
	    	campaign.setGeneralConfigVersion(generalSettingVersion);
	    	campaign.setApplications(measurementList);
	    	campaignList.add(campaign);
	    } else if (qname.equals("camp_id")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : camp id = " + b.toString());
	    	}
	    	campaign.setCampaignId(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("camp_version")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : campaing version e = " + b.toString());
	    	}
	    	campaign.setCampaignVersion(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("target_version")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : campign target_version = " + b.toString());
	    	}
	    	campaign.setTargetVersion(Integer.parseInt(b.toString()));
	    	
	    }   else if (qname.equals("begin_date")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : begin_date = " + b.toString());
	    	}
	    	campaign.setBeginDate(Long.parseLong(b.toString()));
	    			    	
	    } else if (qname.equals("end_date")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : end_date" + b.toString());
	    	}
	    	campaign.setEndDate(Long.parseLong(b.toString()));
	    	
	    } else if (qname.equals("measure_list")) {
	    	//if (Log.DEBUG_ON) {
			//	Log.printDebug("Configuration : measure_list = " + b.toString());
	    	//}
	    	
	    	measurement.setCampaignId(campaign.getCampaignId());
	    	measurement.setCampaignVersion(campaign.getCampaignVersion());
	    	measurement.setTargetVersion(campaign.getTargetVersion());
	    	measurement.setReportFrequency(campaign.getReportFrequency());
	    	measurement.setSendFrequency(campaign.getSendFrequency());
	    	measurement.setSpecialFrequency(campaign.getSpecialFrequency());
	    	measurement.setGeneralSettingVersion(generalSettingVersion);
	    	// swkim 2012.08.28 added generalconfigversion to check trigger measurement
	    	measurementList.add(measurement);

	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : measurement(cid,aid,upid,mid) = (" + measurement.getCampaignId() +
						", " + measurement.getAppId() +
						", " + measurement.getUsagePointId() +
						", " + measurement.getMeasurementId() + ")");
	    	}    	
	    			    	
	    } else if (qname.equals("application")) {
	    	//if (Log.DEBUG_ON) {
			//	Log.printDebug("Configuration : application = " + b.toString());
	    	//}
	    	//System.out.println("Configuration : application = " + b.toString());
	    	measurement.setAppId(b.toString());
	    
	    } else if (qname.equals("usage_point")) {
	    	// swkim 2013.10.9 added to get correct measurement
	    	//if (Log.DEBUG_ON) {
			//	Log.printDebug("Configuration : usage_point = " + b.toString());
	    	//}
	    	//System.out.println("Configuration : usage_point = " + b.toString());
	    	measurement.setUsagePointId(Long.parseLong(b.toString()));
	    	
	    } else if (qname.equals("measurement")) {
	    	//if (Log.DEBUG_ON) {
			//	Log.printDebug("Configuration : measurement = " + b.toString());
	    	//}
	    	measurement.setMeasurementId(Long.parseLong(b.toString()));
	    		    	
	    } else if (qname.equals("send_frequency")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : send_frequency" + b.toString());
	    	}
	    	campaign.setSendFrequency(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("global")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : global = " + b.toString());
	    	}
	    	campaign.setGlobal(Integer.parseInt(b.toString()));
	    	
	    }else if (qname.equals("report_frequency")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : report_frequency " + b.toString());
	    	}
	    	campaign.setReportFrequency(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("special_frequency")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : special_frequency = " + b.toString());
	    	}
	    	campaign.setSpecialFrequency(Integer.parseInt(b.toString()));
	    	
	    } else if (qname.equals("vt_vbm_config")) {
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : vt_vbm_config = " + b.toString());
				if(campaignList != null) {
					Log.printDebug("Configuration :  completed parsing campaign size = " + campaignList.size());
				} else {
					Log.printDebug("Configuration :  completed parsing campaign size = 0");
				}
				if(measurementList != null) {
					Log.printDebug("Configuration :  completed parsing measurement size = " +measurementList.size());
				} else {
					Log.printDebug("Configuration :  completed parsing measurement size = 0");
				}
	    	}
	    	
	    } else{
	    	if (Log.DEBUG_ON) {
				Log.printDebug("Configuration : no matching qname" + qname);
	    	}
	    }
	    b.setLength(0); // empty character buffer

	}

	/***
	 * put content in configuration into temporary data store
	 * 
	 */
	public void characters(char[] chars, int start, int length) {

		b.append(chars, start, length);		
	}

	
/*	

	public synchronized Vector checkAppInCampaign(String appName){
		Vector appMeasurements = new Vector();
		
		if(appName == null){
			return null;
		}
		
		Campaign campaign = null;
		Measurement measurement = null;
		
		int i, j, vtSize;
		Vector measurementList = null;
		for(i=0; i <campaignList.size(); i++){
			campaign = (Campaign) campaignList.get(i);
			if(campaign == null) continue;
			measurementList = campaign.getMeasurementList();
			vtSize = measurementList.size();

			for(j =0; j< vtSize; j++){
				
				measurement = (Measurement) measurementList.get(j);
				if(measurement == null) continue;
				if(measurement.getAppId() == null) continue;
				if(measurement.getAppId().equals(appName))
					appMeasurements.add(measurement);
			}
		}
		return appMeasurements;
	}
	*/

	
	public Campaign getCampaign(long campaignId){
		
		if(campaignList == null) {
			campaignList = new Vector();
		}
		Campaign thisCampaign = null;
		for(int i=0; i< campaignList.size(); i++){
			thisCampaign =(Campaign)campaignList.get(i); 
			if(thisCampaign.getCampaignId() == campaignId){
				break;
			}
		}
		
		return thisCampaign;
	}
	
	
	public void printValue(){
		//Vector mList = null;
		Campaign aCamp = null;
		if (Log.DEBUG_ON) {
			//Log.printDebug(" Configuration : values of VBM Configuration");
			Log.printDebug("gen_set_ver = " + generalSettingVersion + ", tar_set_ver = " + targetSettingVersion);
			for(int i=0; i<campaignList.size(); i++){
				aCamp =((Campaign)campaignList.get(i));
				Log.printDebug("campaign " + i + ", cId = " + aCamp.getCampaignId() + ", cVer = " + aCamp.getCampaignVersion() + ", tVer = " + aCamp.getTargetVersion());
			}
		}
	}
}


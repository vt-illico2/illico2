package com.videotron.tvi.illico.vbm.config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/***
 * data store for trigger measurement
 * 
 * @author swkim
 *
 */
public class TriggerMeasurementList {

	/** array list containing trigger measurement  invoked **/
	private static ArrayList list = null;
	
	/** instance of this trigger measurement list **/
	private static TriggerMeasurementList instance = null;
	
	/***
	 * private constructor of TriggerMeasurementList
	 * 
	 */
	private TriggerMeasurementList(){
		list = new ArrayList();
		
	}
	
	/***
	 * method to get instance of this TriggerMeasurementList
	 * 
	 * @return instance of this trigger measurement list 
	 */
	public static TriggerMeasurementList getInstance(){
		if(instance == null) {
			
			instance = new TriggerMeasurementList();
			
		}
		
		return instance;
	}
	
	
	/***
	 * return list of measurement and remove them in trigger measurement list
	 * 
	 * @param measurementId - measurement id 
	 * @return list of measurements
	 */
	//public synchronized ArrayList remove(long campaignId, long usagePointId, long measurementId){
	public synchronized ArrayList remove(long measurementId){	
		Measurement item = null;
		Set mList = new HashSet();
		for(int i=0; i<list.size(); i++){
			
			item = (Measurement) list.get(i);
			
			if(item.getMeasurementId() == measurementId)
			//		 &&(item.getUsagePointId() == usagePointId) &&
			//		(item.getCampaignId() == campaignId))
					{
				mList.add(item);
				list.remove(i--);
			}			
		}
		
		return (new ArrayList(mList));
	}
	

	/**
	 * add new trigger measurement in this list 
	 * @param trigger - trigger measurement 
	 */
	public synchronized void add(Measurement trigger){
		
		// assume that there is no duplicated trigger item
		// whose measurementId, campaign id and campaign version
		// so when new trigger item added, we just add it to list

		boolean existed = false;
		
		for(int i=0; i< list.size(); i++){
			if((trigger.getCampaignId() == ((Measurement)list.get(i)).getCampaignId()) &&
					(trigger.getUsagePointId() == ((Measurement)list.get(i)).getUsagePointId()) &&
					(trigger.getMeasurementId() == ((Measurement)list.get(i)).getMeasurementId()) ){
				existed = true;
			}
		}
		if(!existed) list.add(trigger);
		
	}
	
	/***
	 * count number of trigger measurement matching campaign id and measurement id
	 * @param campaignId - campaign id
	 * @param measurementId - measurement id 
	 * @return
	 */
	public synchronized int count(long campaignId, long usagePointId, long measurementId){
		
		int count = 0;
		Measurement measurement = null;
		for(int i=0; i<list.size(); i++){
			measurement = (Measurement)list.get(i);
			if((measurement.getCampaignId() == campaignId) 
				&&(measurement.getUsagePointId() == usagePointId)	
				&&(measurement.getMeasurementId() == measurementId)){
				
				count++;
			}
		}
		
		return count;
	}
	
}

package com.videotron.tvi.illico.vbm.uploader;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.Map;
import java.util.zip.DeflaterOutputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import org.json.simple.JSONValue;


/**
 * 
 * LogUploader for one buffer.
 * it uploads buffer per an application for one command.
 * it tries to upload, buff if if failed then throw away logs.
 * and then put a single failure logs into log buffer.
 * 
 * 
 * @author swkim
 *
 */
public class LogUploader extends Thread {

		/** type of log uploading **/
	int type = 0;
	/** retry count of uploading **/
	int retry = 0;
	/** request session key of this uploading **/
	String rsKey = null;
	/** log string to be uploaded **/
	String logString = null;
	/** buffers to be uploaded **/
	Map [] buffer;
	/** data store to keep list of log buffers to be uploaded **/
	Vector buffers;
	
	
	/***
	 * constructor for single log buffer 
	 * 
	 * @param retry - count of retry of this uploading
	 * @param buffer - list of logs uploaded
	 * @param rsKey - request session key
	 */
	public LogUploader(int retry, Map [] buffer, String rsKey){
		this.retry = retry;
		this.type = VbmCode.ONE_BUFFER;
		this.buffer = buffer;
		this.rsKey = rsKey;
	}
	

	/***
	 * constructor for multiple log buffer 
	 * @param retry - count of retry of this uploading
	 * @param buffers - list of logs buffers 
	 * @param rsKey - request session key
	 */
	public LogUploader(int retry, Vector buffers, String rsKey){
		this.retry = retry;
		this.type = VbmCode.MULTI_BUFFER;
		this.buffers = buffers;
		this.rsKey = rsKey;
	}
	
	/***
	 * constructor for log string
	 * @param retry - count of retry of this uploading
	 * @param logString - logs string  
	 * @param rsKey - request session key
	 */
	public LogUploader(int retry, String logString, String rsKey){
		this.retry = retry;
		this.type = VbmCode.LINE_BUFFER;
		this.logString = logString;
		this.rsKey = rsKey;
		
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogUploader : logString is set. " + this.logString);
		}
	}

	
	/***
	 * convert list of logs into json string
	 */
	private String convertMapsToString(Map [] buffers){
				
		if(buffer == null) return null;
		
		HashMap logList = new HashMap();
		try{

			logList.put(VbmCode.JSON_HOSTMAC,  Misc.getMacAddress());
			
			if(rsKey == null) {
				logList.put(VbmCode.JSON_RSKEY, new Long(VbmCode.UPLOAD_NONE));			
			} else {
				logList.put(VbmCode.JSON_RSKEY, rsKey);
			}
			
			ArrayList logs = new ArrayList();
			for(int i=0; i<buffers.length; i++){
				if(buffers[i] != null)
					logs.add(buffers[i]);
			}
			logList.put(VbmCode.JSON_LOG, logs);
						
		} catch(Exception e){
			e.printStackTrace();
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : Error converting maps to string : "  + e.getMessage());
			}
			return null;
		}

		return JSONValue.toJSONString(logList);
		
		
	}
	
	/***
	 * convert list of log buffers into single log string
	 * 
	 * @param buffers list of log buffers 
	 * 
	 * @return single log string
	 */
	private String convertVectorToString(Vector buffers){
		
		if((buffers == null) || (buffers.size() ==0)){

	    	if (Log.DEBUG_ON) {
	    		Log.printDebug("buffers is null");
	    	}
			return null;
		}
		
		Map [] buffer = null;
		HashMap logList = new HashMap();
		
		ArrayList logs = new ArrayList();
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogUploader : size of buffer vector "  + buffers.size());
		}
		try{
			for(int i=0; i<buffers.size(); i++){
				buffer = (Map [])buffers.get(i);
				if(buffer == null){
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : " + i + "th buffer in vector is null. so check next one");
					}
					continue;
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : adding " + i + "th buffers in vector");
					}
				}
				
				
				for(int j=0; j<buffer.length; j++){
					if(buffer[j] != null)
						logs.add(buffer[j]);
				}
			}
			
			logList.put(VbmCode.JSON_HOSTMAC, Misc.getMacAddress());
			
			if(rsKey == null) {
				logList.put(VbmCode.JSON_RSKEY, new Long(VbmCode.UPLOAD_NONE));			
			} else {
				logList.put(VbmCode.JSON_RSKEY, rsKey);
			}
			logList.put(VbmCode.JSON_LOG, logs);
			
		}catch(Exception e){
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : Error converting maps to string : "  + e.getMessage());
			}
			e.printStackTrace();
			return null;
		}
		
		return JSONValue.toJSONString(logList);
	}
	
	
	/***
	 * upload log string without compression
	 * 
	 * @param stringData log string
	 * @throws Exception exception when compressing log string
	 */
    private void sendWithCompression(String stringData) throws Exception { 

    	if(VbmCode.SERVER_URL == null) {
    		if (Log.DEBUG_ON) {
        		Log.printError("no URL string is defined!");
        	}
    	}
    	
    	//if (Log.DEBUG_ON) {
    	//	Log.developer.printDebug("start to upload logs into server " + VbmCode.SERVER_URL 
    	//			+ " compressed");
    	//}
    	
    	System.out.println(" VBM : json string = " + stringData);
    	
    	
        URL url = null;
        

        if(rsKey == null){
        	url = new URL(VbmCode.SERVER_URL + VbmCode.COMPRESSED_AUTO_UPLOAD); 
        	
        	if (Log.DEBUG_ON) {
    			Log.printDebug(" LogUploader : start to upload logs into server " + VbmCode.SERVER_URL + VbmCode.COMPRESSED_AUTO_UPLOAD
    				+ " compressed");
        	}
        } else {
        	url = new URL(VbmCode.SERVER_URL + VbmCode.COMPRESSED_DIRECT_UPLOAD);
        	
        	if (Log.DEBUG_ON) {
    			Log.printDebug(" LogUploader : start to upload logs into server " + VbmCode.SERVER_URL  + VbmCode.COMPRESSED_DIRECT_UPLOAD
    				+ " compressed");
        	}
        }
        
        HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
        conn.setDoOutput(true); 

        // sending data while compressing.

        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(conn.getOutputStream()); 
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(deflaterOutputStream); 
        bufferedOutputStream.write(stringData.getBytes()); 
        bufferedOutputStream.flush(); 
        bufferedOutputStream.close(); 
        deflaterOutputStream.close(); 

        // result of sending 
        int status = conn.getResponseCode();
		BufferedReader bufferedReader = null;
		InputStreamReader inputStreamReader = null;
		String returnMessage = "";

		if (status == HttpURLConnection.HTTP_OK) {
			inputStreamReader = new InputStreamReader(conn.getInputStream(), "UTF-8");
			bufferedReader = new BufferedReader(inputStreamReader);
			
			// get configuration string
			String eachLine = "";

			while ((eachLine = bufferedReader.readLine()) != null) {
				returnMessage += eachLine;
			}
			bufferedReader.close();
			inputStreamReader.close();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : connection status = " + status );
			}
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogUploader : return string = " + returnMessage);
		}

    } 

    
    
	/***
	 * upload logs without compression
	 * 
 	 * @param stringData - log string 
	 * @throws Exception - exception when converting logs into string 
	 */
    private void sendWithoutCompression(String stringData) throws Exception { 

    	if(VbmCode.SERVER_URL == null) {
    		if (Log.DEBUG_ON) {
        		Log.printError("no URL string is defined!");
        	}
    	}
    	
    	//if (Log.DEBUG_ON) {
    	//	Log.printDebug("start to upload logs into server " + VbmCode.SERVER_URL 
    	//			+ " uncompressed");
    	//}
    	
        URL url = null;
        
        if(rsKey == null){
        	if (Log.DEBUG_ON) {
    			Log.printDebug(" LogUploader : url = " + VbmCode.SERVER_URL + VbmCode.NORMAL_AUTO_UPLOAD);
        	}
        	url = new URL(VbmCode.SERVER_URL + VbmCode.NORMAL_AUTO_UPLOAD); 
        } else {
        	if (Log.DEBUG_ON) {
    			Log.printDebug(" LogUploader : url = " + VbmCode.SERVER_URL + VbmCode.NORMAL_DIRECT_UPLOAD);
        	}
        	url = new URL(VbmCode.SERVER_URL + VbmCode.NORMAL_DIRECT_UPLOAD);
        }
        
        HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
        
        conn.setDoOutput(true); 

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(conn.getOutputStream()); 
        bufferedOutputStream.write(stringData.getBytes()); 
        bufferedOutputStream.flush(); 
        bufferedOutputStream.close(); 

        if (Log.DEBUG_ON) {
			Log.printDebug(" LogUploader : sent. data = " + stringData);
        }
        
        int status = conn.getResponseCode();
		BufferedReader bufferedReader = null;
		InputStreamReader inputStreamReader = null;
		String returnMessage = "";

		if (status == HttpURLConnection.HTTP_OK) {
			inputStreamReader = new InputStreamReader(conn.getInputStream(), "UTF-8");
			bufferedReader = new BufferedReader(inputStreamReader);
			
			// get configuration string
			String eachLine = "";

			while ((eachLine = bufferedReader.readLine()) != null) {
				returnMessage += eachLine;
			}
			bufferedReader.close();
			inputStreamReader.close();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : HttpUrURLConnection is not HTTP_OK. " + status);
			}
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogUploader : response = '" + returnMessage + "'");
		}
    } 
	
    private boolean isStandAlone(){

		// 2012.02.28
		// check STB's standalone. if it is stand alone, do not upload logs
		
		MonitorService monitor = 
			(MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
		if (monitor == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : do not get MonitorService. So do not upload logs");
			}
			return true;
		} else {
			try{
				if(monitor.getSTBMode() == MonitorService.STB_MODE_STANDALONE) {
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : STB is in standalone mode. So do not upload logs");
					}
					return true;
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : STB is not in standalone mode. So keep uploading logs");
					}
				}
			}catch(RemoteException e){
				if (Log.DEBUG_ON) {
					Log.printDebug(" LogUploader : Error checking standalone mode. So do not upload logs");
					return true;
				}
			}
		}
		return false;
    }
	/**
	 * start thread to upload to syslog server.
	 * 
	 */
	public void run() {
		
		if(isStandAlone()){
			return;
		}
		
		try{
			if(type == VbmCode.ONE_BUFFER) {
				
				if((buffer == null) || ( buffer.length == 0)){
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : upload buffer is null. so do not upload");
					}
					return;
				}
				
				logString = convertMapsToString(buffer);
			}
			else if(type == VbmCode.MULTI_BUFFER){
				
				if((buffers == null) || (buffers.size() == 0)){
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : upload buffers are null. so do not upload");
					}
					return;
				}
				logString = convertVectorToString(buffers);
				
			} else if (type == VbmCode.LINE_BUFFER) {

				if (Log.DEBUG_ON) {
					Log.printDebug(" LogUploader : upload buffers with string buffer.");
				}
			}
			
			if(logString == null){
				if (Log.DEBUG_ON) {
					Log.printDebug(" LogUploader : upload string is null. so do not upload");
				}
				return;
			}
			
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogUploader : logString = " + logString + " , compression = " + ConfigurationManager.getInstance().isCompressed());
			}
			if(ConfigurationManager.getInstance().isCompressed() == VbmCode.NEED_COMPRESSION) {
				sendWithCompression(logString);
			} else{
				sendWithoutCompression(logString);
			}
		}
		catch(Exception e){
			
			if((retry+1) >= VbmCode.MAX_UPLOAD_RETRY){  // retry first and second. if not discard to upload
				if (Log.DEBUG_ON) {
					Log.printDebug(" LogUploader : exceeded maximum upload retry. remove it");		    		    		
		    	}
				//System.out.println(" VBM : exceeded maximum upload retry. remove it");
			} else{
				
				if (Log.DEBUG_ON) {
					Log.printDebug(" LogUploader : retry to upload " + (retry + 1) + " times. Sleep a little and execute next turn.");
				}
				try{
					if(retry == 0){
						Thread.sleep(VbmCode.UPLOAD_RETRY_FIRST);					
					} else {
						Thread.sleep(VbmCode.UPLOAD_RETRY_SECOND);
					}
				} catch(Exception sleep){
					sleep.printStackTrace();
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : retry to upload " + (retry + 1) + " times");
					}
				}
				
				if(type == VbmCode.ONE_BUFFER) {
					(new LogUploader((retry + 1), buffer, rsKey)).start();
				} else if(type == VbmCode.MULTI_BUFFER){
					(new LogUploader((retry + 1), buffers, rsKey)).start();
				} else if (type == VbmCode.LINE_BUFFER){
					(new LogUploader((retry + 1), logString, rsKey)).start();
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" LogUploader : error retrying upload. Type = " + type ) ;
					}
				}
			}
		}
	}
}

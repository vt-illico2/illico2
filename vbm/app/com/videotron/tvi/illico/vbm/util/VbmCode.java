package com.videotron.tvi.illico.vbm.util;

import com.videotron.tvi.illico.framework.DataCenter;




/**
 * code used in VBM.
 * all codes used in VBM Client should be defined in this class.
 * 
 * @author swkim
 *
 */
public class VbmCode {

	/**
	 * constructor for utility class. do nothing.
	 */
	protected VbmCode() {
		// do nothing.
	}

	public static final int CONFIG_STATUS = 1;

	public static final int MEMBER_STATUS = 2;
	
	/**
	 * initial buffer  size
	 * used in VbmController
	 */
	public static final int ALL_LOG_CACHE = -1;
	
	/**
	 * initial buffer  size
	 * used in VbmController
	 */
	public static final int MAX_DAY_OF_MONTH = 28;
	/**
	 * initial buffer  size
	 * used in VbmController
	 */
	public static final long MAX_FILE_SIZE = 10 * 1024 * 1024; // 10M
	
	

	/**
	 * initial buffer  size
	 * used in VbmController
	 */
	public static final int BUFFER_INIT_VALUE = 10;
	
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String DISK_EXIST = "1.0";
	
	

	/**
	 * code used in LogUploader
	 * duration between retries when failed to upload logs.
	 */
	public static final long UPLOAD_RETRY_FIRST = 30 * 1000; // 30 sec
	
	/**
	 * code used in LogUploader
	 * duration between retries when failed to upload logs.
	 */
	public static final long UPLOAD_RETRY_SECOND = 15 * 60 * 1000; // 15 min
	
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final int DAYS = 7;
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final int BINARY = 2;
	
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String LOG_FILE_PATH = "/vbm/log/";
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String LOG_FILE_NAME_DAILY = "daily.log";
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String LOG_FILE_NAME_WEEKLY = "weekly.log";
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String LOG_FILE_NAME_MONTHLY = "monthly.log";
	
	
	/**
	 * code used in VbmLog
	 * separator of input string
	 */
	public static final String LOG_SEPARATOR = "///";

	/**
	 * code used to separator trigger measurement and action measurement.
	 * if measurementId is under this separator, then it is trigger measurement.
	 * if measurementId is upper this separator, then it is an action measurement.
	 */
	public static final long MEASUREMENT_SET_SEPEARTOR = 1000000000;
	 
	

	/**
	 * code used in LogUploader.
	 * the last day of a month.
	 * if day is 29, 30, 31, then it should be 28
	 */
	public static final int LAST_DAY_IN_MONTH = 28;
	 
	/**
	 * code used in direct upload
	 * return value when failed to upload
	 */
	public static final String DAEMON_PROC = "com.videotron.tvi.illico.vbm.communication.daemon.DaemonProcessor";

	/**
	 * code used in direct upload
	 * return value when failed to upload
	 */
	public static final int DIRECT_UPLOAD_FAILED = -9;
	 

	/**
	 * code used in direct upload
	 * return value when succeeded to upload
	 */
	public static final int DIRECT_UPLOAD_TRUE = 0;
	 
	
	/**
	 * code used in cofiguration
	 * key in OOB for VBM
	 */
	public static final String VBM_DATA_OOB = "VBM_DataOOB";
	 
	 
	/**
	 * code used in cofiguration
	 * key of member file
	 */
	public static final String MEMBER_FILE_KEY = "VBM_MEMBER";
	
	
	/**
	 * code used in cofiguration
	 * key of configuration file
	 */
	public static final String CONFIG_FILE_KEY = "VBM_CONFIG";
	
	/**
	 * code used in cofiguration
	 * name of configuration file
	 */
	public static final String CONFIG_FILE = "general_configuration.xml";

	/**
	 * code used in cofiguration
	 * name of member file
	 */
	public static final String MEMBER_FILE = "members.zip";
	
	/**
	 * code used in uploader to set rsKey none
	 * hostmac is attributes for unique value of STB
	 */
	public static final long UPLOAD_NONE = -1;

	
	/**
	 * code used in uploader to set rsKey none
	 * hostmac is attributes for unique value of STB
	 */
	public static final int INIT_VALUE = -1;
	
	

	/**
	 * code used in uploader to generate payload
	 * measurement is key for measurement
	 */
	public static final String JSON_TARGET_VERSION = "tVer";
	
	/**
	 * code used in uploader to generate payload
	 * measurement is key for measurement
	 */
	public static final String JSON_MEASUREMENT = "mId";

	/**
	 * code used in uploader to generate payload
	 * measurement is key for measurement group
	 */
	public static final String JSON_MEASUREMENT_GROUP = "mGId";
	
	/**
	 * code used in uploader to generate payload
	 * hostmac is attributes for unique value of STB
	 */
	public static final String JSON_HOSTMAC = "hostmac";
	
	/**
	 * code used in uploader to generate payload
	 * rsKey is a session key 
	 */
	public static final String JSON_RSKEY = "rskey";

	
	/**
	 * code used in uploader to generate payload
	 * rsKey is a session key 
	 */
	public static final String JSON_CAMPAIGN_ID = "cId";
	
	/**
	 * code used in uploader to generate payload
	 * rsKey is a session key 
	 */
	public static final String JSON_CAMPAIGN_VERSION = "cVer";
	
	/**
	 * code used in uploader to generate payload
	 * time when generated in vbm client
	 */
	public static final String JSON_TIME = "time";
	
	
	/**
	 * code used in uploader to generate payload
	 * log code in json object
	 */
	public static final String JSON_LOG = "log";

	
	/**
	 * code for value of usage point id
	 */
	public static final String JSON_USAGE_POINT_ID = "uId";
	
	
	/**
	 * code for value of measurement id
	 */
	public static final String JSON_VALUE = "value";


	/**
	 * buffer number in LogContainer
	 * they are daily buffer, weekly buffer and monthly buffer
	 */
	public static final String NAME_IN_SHARED_MEMORY = "VBM";

	
	/**
	 * buffer number in LogContainer
	 * they are daily buffer, weekly buffer and monthly buffer
	 */
	public static final int BUFFER_NUMBER = 3;

	
	/**
	 * the last remaining
	 */
	public static final int RETURN_SUCCESS = 0;

	
	
	/**
	 * the last remaining
	 */
	public static final int RETURN_FAILURE = -9;
	
	/**
	 * flag to start trigger
	 */
	public static final boolean START_TRIGGER = true;
	
	
	

	/**
	 * flag to stop trigger
	 */
	public static final boolean STOP_TRIGGER = false;
	
	
	
	
	
	/**
	 * the last remaining
	 */
	public static final int LAST_ONE = 0;
	
	

	/**
	 * the first one
	 */
	public static final int FIRST_ONE = 1;
	
	
	/**
	 * is global
	 */
	public static final int IS_GLOBAL = 0;
	
	/**
	 * need compression when uploading
	 */
	public static final int NEED_COMPRESSION = 0;

	/**
	 * retry interval
	 */
	public static final long RETRY_INTERVAL = 1000; 

	/**
	 * maximum retry count to uploader
	 */
	public static final int MAX_UPLOAD_RETRY = 3;
	

	/**
	 * code for one buffer
	 */
	public static final int ONE_BUFFER = 1;
	

	/**
	 * code for multiple buffers
	 */
	public static final int MULTI_BUFFER = 2;
	
	/**
	 * code for multiple buffers
	 */
	public static final int LINE_BUFFER = 3;
	
	/**
	 * size of STB MAC
	 */
	public static final int MAC_SIZE = 12;
	/**
	 * SERVER ULR
	 */
	public static final String KEY_SERVER_URL = "vbm.server.url";
	
	public static final String  SERVER_URL = DataCenter.getInstance().getString(KEY_SERVER_URL);
	//public static final String SERVER_URL = "http://10.247.191.166:8080/";
	/**
	 * CATCHER URL. automatic upload with normal logs
	 */
	public static final String NORMAL_AUTO_UPLOAD = "catcher/catcher.intf";
	

	/**
	 * CATCHER URL. automatic upload with compressed logs
	 */
	public static final String COMPRESSED_AUTO_UPLOAD = "catcher/zcatcher.intf";
	

	/**
	 * CATCHER URL. direct upload with normal logs
	 */
	public static final String NORMAL_DIRECT_UPLOAD = "catcher/dcatcher.intf";

	/**
	 * CATCHER URL. direct upload with compressed logs
	 */
	public static final String COMPRESSED_DIRECT_UPLOAD = "catcher/zdcatcher.intf";
	
	/**
	 * New line character
	 */
	public static final String NEW_LINE = "\n";
	
	/**
	 * Report Frequency, Hourly
	 */
	public static final int REPORT_FREQ_HOURLY = 0;
	/**
	 * Report Frequency, Hourly
	 */
	public static final int REPORT_FREQ_DAILY = 1; 
	/**
	 * Report Frequency, Hourly
	 */
	public static final int REPORT_FREQ_WEEKLY = 2; 
	/**
	 * Report Frequency, Hourly
	 */
	public static final int REPORT_FREQ_MONTHLY = 3; 
	/**
	 * Send Frequency, Daily
	 */
	public static final int SEND_FREQ_DAILY = 1; 
	/**
	 * Send Frequency, Weekly
	 */
	public static final int SEND_FREQ_WEEKLY = 2;
	/**
	 * Send Frequency, Monthly
	 */
	public static final int SEND_FREQ_MONTHLY = 3;
	
	/**
	 * Send Frequency, Monthly
	 */
	public static final int SPECIAL_FREQ_NONE = 0;
	
	
	/**
	 * Yes for compression and global
	 */
	public static final int YES = 0;
	/**
	 * No for compression and global
	 */
	public static final int NO = 1;
	
	/**
	 * hexa decimal value for parsing byte array into int.
	 */
	public static final int SHIFT_HEXA = 0x000000FF; 
	
	/**
	 * Sleep time to get configuration .
	 */
	public static final int STORE_INTERVAL = 5 * 60 * 1000;  // 5 min
							
	
	/**
	 * Sleep time to get configuration .
	 */
	public static final int DAILY_LOG_CACHE = 0;
	
	/**
	 * Sleep time to get configuration .
	 */
	public static final int MONTHLY_LOG_CACHE = 2;
	
	/**
	 * Sleep time to get configuration .
	 */
	public static final int WEEKLY_LOG_CACHE = 1;
	
	public static final long MILISECOND = (long) 1000;
	public static final long SECOND_IN_MIN = (long) 60 ;
	public static final long SECOND_IN_HOUR = (long) 60 * 60 ;
	public static final long SECOND_IN_DAY = (long) 24 * 60 * 60 ;
	public static final long SECOND_IN_WEEK= (long) 7 * SECOND_IN_DAY;
	public static final long SECOND_IN_MONTH = (long) 28 * SECOND_IN_DAY;
	
	
	public static final String NULL_STRING = "";
	public static final String DEBUG_CODE = "VBM_DEBUG_LOGS";
	
	
	
	/**
	 * old separator for input log from client application
	 */
	public static final String OLD_SEPARATOR = "|";
	
	
	
	public static final int FILE_NAME_COUNT = 3;
	public static final String FILE_NAME_SEP = "_";
	
	
}

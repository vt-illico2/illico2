package com.videotron.tvi.illico.vbm.util;

import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author sccu
 */
public final class Misc {

    /** maximum unsigned byte value. */
    private static final int MAX_UBYTE = 255;
    
    /** maximum unsigned byte value. */
    private static final int SIZE_OCT = 256;
    
    /** byte mask value. */
    private static final int BYTE_MASK = 0xFF;

    /** MAC size. */
    private static final int LENGTH_HOST = 6;
    
    /** MAC size. */
    private static final int LOC_MAC = 6;
    
    /** MAC size. */
    private static final int SIZE_BYTE = 6;
    
    /** MAC size. */
    private static final int BASE_INT = 6;
    
    
    /** system line separator. */

    /** submission-format line separator. */
    public static final String SUBMISSION_LINE_SEPARATOR = "\r\n";
    
    /** host mac. */
    private static String mac = null;
   
    /**
     * misc utillity class.
     */
    private Misc() {
    }

    /** host mac array. */
    private static final byte[] HOSTMAC =  new byte[LENGTH_HOST];
    //
    
    /*
     * 
     *
    private static final byte[] HOSTMAC;    
    
    static {
    	HOSTMAC = new byte[LENGTH_HOST];
        try {
        		String hostMacString = Host.getInstance().getReverseChannelMAC();
                for (int x = 0; x < LENGTH_HOST; x++) {
                    HOSTMAC[x] = ubyteToByte(Integer.parseInt(hostMacString.substring(x * LOC_MAC, 
                    		x * LOC_MAC + SIZE_BYTE), BASE_INT));
                }
        } catch (Exception e) {
        	if (Log.WARNING_ON) {
				Log.developer.print(e);
			}
        }
    }
    /**
     * Get host mac string.
     * @return host mac string.
     *
    public static String getHostMacString() {
    	if (Log.DEBUG_ON) {
            Log.developer.printDebug("getHostMacString");
        }
    	if (mac == null) {
    		try {
    			if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
    					|| Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
    				mac = toHexString(getHostMac());
    			} else {
    				MonitorService monitor = 
    					(MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
    				if (monitor != null) {
    					byte[] addr = monitor.getCableCardMacAddress();
    					if (Log.DEBUG_ON) {
        		            Log.developer.printDebug("get monitor " + addr.length);
        		        }
    					mac = toHexString(addr);
    					if (Log.DEBUG_ON) {
        		            Log.developer.printDebug("MAC " +  mac);
        		        }
    				}
    			}
    		} catch (Exception e) {
    			if (Log.WARNING_ON) {
    				Log.developer.print(e);
    			}
    			return "";
    		}
    	}
    	return mac;
    }

    
*/
    

    /**
     * set MacAddress.
     * 2012.05.15 changed not to make recursive call
     */
    
    
    public static String getMacAddress() {

    	// 2012.04.16
    	// added coded to remove duplicated request to get MAC
        if (!(mac == null || mac.length() == 0)) {
            return mac.toUpperCase();            
        }
    	
    	
    	if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
            mac = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(mac, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            mac = address.toString();
        } else {
        	MonitorService monitor = null;
        	boolean gotValue = false;
            do {
            	try {
            		if (monitor == null) {
		                monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            		}
            		
	                if (monitor != null) {
	                    byte[] addr = monitor.getCableCardMacAddress();
	                    //mac = Util.getStringBYTEArray(addr);
	                    mac = toHexString(addr);
	                
		                if((mac != null) && (mac.length() > 0)) {
		                	gotValue = true;
		                } else {
		                	Thread.sleep(VbmCode.RETRY_INTERVAL);
		                }
	                }
	                
	            } catch (Exception e) {
	                if (Log.WARNING_ON) {
	                	Log.printWarning(e);
	                }
	            } finally {
	            	if(gotValue) monitor = null;
	            }
            }while(!gotValue);
        	
        }
    	
    	
        return mac.toUpperCase();            

    }
    
    
    /*
    public static String getMacAddress() {

    	if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
            mac = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(mac, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            mac = address.toString();
        } else {
            try {
                MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
                if (monitor != null) {
                    byte[] addr = monitor.getCableCardMacAddress();
                    //mac = Util.getStringBYTEArray(addr);
                    mac = toHexString(addr);
                    
                }
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                	Log.print(e);
                }
            }
        }

        if (mac == null || mac.length() == 0) {
            try {
                Thread.sleep(VbmCode.RETRY_INTERVAL);
            } catch (Exception e) {
            	Log.print(e);
            }
            return getMacAddress();
        } else {
            return mac.toUpperCase();            
        }
        
    	//return "0021BE81632D"; // 3001BE81632D
    	
    
    }
    */
    
    
    /**
     * Gets host mac array.
     * @return host mac array.
     */
    private static byte[] getHostMac() {
        return HOSTMAC;
    }

    /**
     * Converts byte array to hex String.
     * @param byteArray byte array.
     * @return host mac represented as a String object.
     */
    public static String toHexString(final byte[] byteArray) {
        if (byteArray == null) {
            return null;
        }

        StringBuffer buf = new StringBuffer(byteArray.length);

        for (int i = 0; i < byteArray.length; i++) {
            String hex = Integer.toHexString(byteArray[i] & BYTE_MASK).toUpperCase();
            if (hex.length() == 1) {
                buf.append('0').append(hex);
            } else {
                buf.append(hex);
            }
        }

        return buf.toString();
    }

    /**
     * @param array the array to be copied.
     * @return a copy of the original array.
     */
    public static int[] copyOf(int[] array) {
        int[] arr = new int[array.length];
        for (int x = 0; x < array.length; x++) {
            arr[x] = array[x];
        }
        return arr;
    }

    /**
     * Converts signed byte into unsigned byte.
     * @param b - signed byte
     * @return unsigned byte value
     */
    public static int byteToUbyte(byte b) {
        return (SIZE_OCT + b) % SIZE_OCT;
    }

    /**
     * Converts an unsigned byte into signed byte.
     * @param ubyteValue - unsigned byte value.
     * @return byte value.
     */
    public static byte ubyteToByte(int ubyteValue) {
        if (ubyteValue < 0 || MAX_UBYTE < ubyteValue) {
            throw new IllegalArgumentException("version value is out of range:" + ubyteValue);
        }
        if (ubyteValue > Byte.MAX_VALUE) {
            ubyteValue -= SIZE_OCT;
        }
        return (byte) ubyteValue;
    }
    
    
    public static long getHourlyUploadTime(boolean retry){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	long current = 0l;
    	
    	// if upload time is set
    	if(retry)
    		return  ((long)VbmCode.SECOND_IN_HOUR * VbmCode.MILISECOND);
    	else{
    		// some time in an hour
    		 return hexStringToLong(mac) % ((long)VbmCode.SECOND_IN_HOUR * VbmCode.MILISECOND);
    		
    	}
    }
    
    public static long getDailyUploadTime(boolean retry){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	
    	if(retry)
    		// a day later, when true
    		return ((long)VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND);
    	else {
    		// some time in a day, when false
    		return hexStringToLong(mac) % ((long)VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND);
    	}
    }
    
    public static long getDailyUploadTimeWithProperty(boolean retry){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	
    	long delayMinute = DataCenter.getInstance().getLong("forcibly.delay");
    	
    	if(retry)
    		// a day later, when true
    		return ((long)delayMinute * VbmCode.SECOND_IN_MIN * VbmCode.MILISECOND);
    	else {
    		// some time in a day, when false
    		return hexStringToLong(mac) % ((long)delayMinute * VbmCode.SECOND_IN_MIN * VbmCode.MILISECOND);
    	}
    }

    
    public static long getWeeklyUploadTime(boolean retry){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	
    	if(retry) {
    		// a week later
    		return ((long)VbmCode.SECOND_IN_WEEK * VbmCode.MILISECOND);
    	} else {
    		// some time in a week
    		return hexStringToLong(mac) % ((long)VbmCode.SECOND_IN_WEEK * VbmCode.MILISECOND);
    	}
    }
        
    private static long getMonthlyRandomTime(boolean retry){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	
    	if(retry) {
    		
    		// one month later
    		Calendar theMonth = Calendar.getInstance();
            // if day is over with last day in month, then set day into last day in month
    		theMonth.set(Calendar.MONTH, theMonth.get(Calendar.MONTH) + 1); // set next month
    		
    		return theMonth.getTimeInMillis();
    		
            
    	} else {
    		// some time in a month
    		return hexStringToLong(mac) % ((long)VbmCode.MAX_DAY_OF_MONTH * VbmCode.MILISECOND);
    	}
    }

    public static long getMonthlyUploadTime(boolean retry){

    	if(retry){
    		// one month later
    		// now is between 1 and 28.
    		Calendar nextMonth = Calendar.getInstance();
        	nextMonth.set(Calendar.MONTH, nextMonth.get(Calendar.MONTH) + 1);
			
        	// return duration of next trigger
            return nextMonth.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
            
    	} else {
    		// some time in a month
    		
    		long now = Calendar.getInstance().getTimeInMillis();
    		
    		// variable for next trigger
    		Calendar cal = Calendar.getInstance();
    		cal.setTimeInMillis(now + hexStringToLong(mac) % ((long)VbmCode.MAX_DAY_OF_MONTH * VbmCode.MILISECOND));
    		
    		// next scheduled date is after max day, then set next month
    		if(cal.get(Calendar.DAY_OF_MONTH) > VbmCode.MAX_DAY_OF_MONTH){
    			if(cal.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)) {
    				// if the same month, then set first day of next month
    				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH) + 1);
        			cal.set(Calendar.DAY_OF_MONTH, 1);
        			
    			} else {
    				// if it is 29,30,31th day of next month, set last day of next month
    				cal.set(Calendar.DAY_OF_MONTH, VbmCode.MAX_DAY_OF_MONTH -1);
    			}
    		}
    		
    		return cal.getTimeInMillis() - now;
    	}
    }
    
    public static long getIntervalTail(){
    	String mac = getMacAddress();
    	if(mac == null) return -1;
    	
    	return hexStringToLong(mac) % VbmCode.MILISECOND;
    }
    
    
    public static long getCurrentTime(){
    	Calendar now = Calendar.getInstance();
    	
    	int hour = now.get(Calendar.HOUR_OF_DAY);
    	int minute = now.get(Calendar.MINUTE);
    	int second = now.get(Calendar.SECOND);
    	
    	return ( hour * 60 *60 + minute * 60 + second ) * VbmCode.MILISECOND;

    }
    
    
    /**
	 * Hex String을 long으로 변환
	 * 
	 * @param hexStr
	 * @return long 값
	 */
    private static long hexStringToLong(String hexStr){
		long result = 0;
		String hex = hexStr.toUpperCase();

		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (long)(b & 0x0f) << (i*4); 
		}
		
		return result;
	}

	private static byte toByte(char c){
		switch(c){
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case 'A':
			return 10;
		case 'B':
			return 11;
		case 'C':
			return 12;
		case 'D':
			return 13;
		case 'E':
			return 14;
		case 'F':
			return 15;
		}
		return 0;
	}
	
	
	public static String [] getArrayFromVector(Vector vt) {
		String [] retArr = null;
		
		if((vt == null) || (vt.size() == 0)){
			return null;
		}
		
		int vtSize = vt.size();
		
		retArr = new String [vtSize];
		
		for(int i=0; i < vtSize; i++) {
			retArr[i] = (String) vt.get(i);			
		}
		
		return retArr;
	}
	
	
	public static String checkNull(String inStr){
		if(inStr == null) return "";
		else return inStr;
	}

}

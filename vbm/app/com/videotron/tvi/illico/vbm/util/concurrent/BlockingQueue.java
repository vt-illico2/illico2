/**
 *
 */
package com.videotron.tvi.illico.vbm.util.concurrent;

import java.util.LinkedList;

/**
 * @author clotho
 */
public class BlockingQueue {
    /** the capacity of this queue. */
    private final int capacity;

    /** the data structure of the queue. */
    private final LinkedList queue = new LinkedList();

    /** flag indicating whether this BlockingQueue instance is stopped. */
    private boolean stopped;

    /**
     * Constructs the BlockingQueue instance with the specified capacity.
     * @param capacityVal - the maximum count of the items in the queue.
     */
    public BlockingQueue(final int capacityVal) {
        this.capacity = capacityVal;
    }

    /**
     * Put the element into the end of the queue. If the queue is in blocked mode and it's size is equal to
     * <code>capacity</code>, it suspends threads until it's size becomes less than <code>capacity</code>.
     * @param o - the elements to put into the queue <code>false</code> otherwise.
     * @throws InterruptedException occurs when the queue has already shut down.
     */
    public synchronized void put(Object o) throws InterruptedException {
        if (isShutdown()) {
            // TODO : IllegalStateException
            throw new InterruptedException("Already shutdowned.");
        }

        while (queue.size() >= capacity) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace(); // ignore
            }

            if (isShutdown()) {
                throw new InterruptedException("Shut down during waiting in put().");
            }
        }

        queue.add(o);
        notify();
    }

    /**
     * Puts an object. If the internal queue is already full, it abandons to queue the object and returns false.
     * @param o - object
     * @return true if succeeds to queue the object.
     */
    public synchronized boolean putNoWait(Object o) {
        if (isShutdown()) {
            throw new IllegalStateException("Already shutdowned.");
        }

        if (queue.size() >= capacity) {
            return false;
        }

        queue.add(o);
        notify();

        return true;
    }

    /**
     * Take the first element from the queue. if queue is in blocked mode and empty, it suspends threads until a new
     * element arrives.
     * @return the first element; <code>null</code> if the queue is in non-blocked mode and empty.
     */
    public synchronized Object takeNoWait() {
        if (isShutdown()) {
            throw new IllegalStateException("Already shutdowned.");
        }

        if (queue.isEmpty()) {
            return null;
        }

        Object o = queue.removeFirst();
        notify();
        return o;
    }

    /**
     * Takes the object from the queue and returns it.
     * @return the queued object.
     * @throws InterruptedException if the queue becomes stopped during waiting to queue new data.
     */
    public synchronized Object take() throws InterruptedException {
        if (isShutdown()) {
            throw new IllegalStateException("Already shutdowned.");
        }

        while (queue.isEmpty()) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace(); // ignore
            }

            if (isShutdown()) {
                throw new InterruptedException("Shut down during waiting in take().");
            }
        }

        Object o = queue.removeFirst();
        notify();
        return o;
    }

    /**
     * Returns the current size of the queue.
     * @return - the size of the queue.
     */
    public synchronized int size() {
        return queue.size();
    }

    /**
     * Shuts down the service and clears the queue.
     */
    public synchronized void shutdown() {
        this.stopped = true;
        queue.clear();
        notifyAll();
    }

    /**
     * Tests if the queue has already shut down.
     * @return <code>true</code> if the instance has already shut down; <code>false</code> otherwise.
     */
    public synchronized boolean isShutdown() {
        return stopped;
    }

}

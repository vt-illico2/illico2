package com.videotron.tvi.illico.vbm.util;

import javax.tv.util.*;
import java.util.*;
import com.videotron.tvi.illico.log.Log;

public class TimerWrapper implements TVTimerWentOffListener {

    private Runnable task;
    private static Worker worker = new Worker("TimerWrapper.worker", true);

    private TVTimerSpec firstSpec = new TVTimerSpec();
    private TVTimerSpec repeatSpec;

    private long repeatPeriod = 0L;

    private final String name;

    public TimerWrapper() {
        this("TimerWrapper");
    }

    public TimerWrapper(String name) {
        this.name = name;
    }

    public void schedule(Runnable task, Date time) {
        schedule(task, time, 0);
    }

    public void schedule(Runnable task, Date firstTime, long period) {
        schedule(task, firstTime.getTime() - System.currentTimeMillis(), period);
    }

    public void schedule(Runnable task, long delay) {
        schedule(task, delay, 0);
    }

    public void schedule(Runnable task, long delay, long period) {
        scheduleImpl(task, delay, period, false);
    }

    public void scheduleAtFixedRate(Runnable task, Date firstTime, long period) {
        scheduleAtFixedRate(task, firstTime.getTime() - System.currentTimeMillis(), period);
    }

    public void scheduleAtFixedRate(Runnable task, long delay, long period) {
        scheduleImpl(task, delay, period, true);
    }

    private void scheduleImpl(Runnable task, long delay, long period, boolean regular) {
        this.task = task;
        delay = Math.max(delay, 0L);
        this.repeatPeriod = period;
        boolean repeat = period > 0L;

        if (Log.DEBUG_ON) {
            Log.printDebug(name + ": schedule first timer");
        }

        firstSpec.setTime(delay);
        firstSpec.setAbsolute(false);
        firstSpec.setRegular(regular);
        firstSpec.removeTVTimerWentOffListener(this);
        firstSpec.addTVTimerWentOffListener(this);
        try {
            firstSpec = TVTimer.getTimer().scheduleTimerSpec(firstSpec);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(name + ": timerWentOff");
        }
        worker.push(task);
        if (repeatPeriod > 0L && e.getTimerSpec() == firstSpec) {
            if (Log.DEBUG_ON) {
                Log.printDebug(name + ": schedule repeat timer");
            }
            repeatSpec = new TVTimerSpec();
            repeatSpec.setTime(repeatPeriod);
            repeatSpec.setAbsolute(false);
            repeatSpec.setRepeat(true);
            repeatSpec.addTVTimerWentOffListener(this);
            try {
                TVTimer.getTimer().scheduleTimerSpec(repeatSpec);
                repeatPeriod = 0L;
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

}

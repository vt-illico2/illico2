package com.videotron.tvi.illico.vbm.util;

/**
 * general code class used in VBM.
 * 
 * @author swkim
 *
 */
public class VbmInfo {

	/**
	 * constructor.
	 * do nothing.
	 */
	protected VbmInfo() {
		
	}
	/**
	 * VBM Application Name.
	 * @return	application name
	 */
	public static String getName() {
		return "VBM";
	}
	
	/**
	 * VBM application version.
	 * @return	application version
	 */
	public static String getVersion() {
		return "1.0.20.2";
		//return "0.0.1.1_R2";
	}
	
	
}

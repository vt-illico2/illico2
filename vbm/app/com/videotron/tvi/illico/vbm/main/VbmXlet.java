
/*
 *  VbmXlet.java    $Revision: 1.1 $ $Date: 2014/04/04 04:45:53 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.vbm.main;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
//import com.videotron.tvi.illico.vbm.communication.VbmConfigDataUpdateListener;
import com.videotron.tvi.illico.vbm.communication.VbmConfigDispatcher;
import com.videotron.tvi.illico.vbm.util.concurrent.Scheduler;
import com.videotron.tvi.illico.vbm.communication.CommunicationRequestDispatcher;
import com.videotron.tvi.illico.vbm.controller.VbmController;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmInfo;
import com.videotron.tvi.illico.log.Log;

/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 22.
 */
public class VbmXlet implements Xlet, ApplicationConfig {
	
	
    /** xlet context. */
    private XletContext xletContext;
    
    /** */
    private VbmController controller;
    
    /** the VbmConfigDispatcher instance. */
    private VbmConfigDispatcher vbmConfigDispatcher;
    
    /** */
    private CommunicationRequestDispatcher requestDispatcher;
    
    
    /** the Scheduler instance. */
    private Scheduler scheduler;
    
    /** ConfigListner */
    //private VbmConfigDataUpdateListener listener;

    /**
     * Initialize the xlet.
     * @param context - the XletContext instance.
     * @throws XletStateChangeException - If the Xlet cannot be initialized. 
     */
    public void initXlet(XletContext context) throws XletStateChangeException {
        
    	
    	xletContext = context;
        FrameworkMain.getInstance().init(this);
        
        if (Log.INFO_ON) {
            Log.printInfo("ISA: initXlet");
        }


    }

    /**
     * init method for the xlet.
     */
    public void init() {
        try {
        	
        	//ISAController.getInstance().init(xletContext);
        	
        	
        	buildComponets();
            if (Log.DEBUG_ON) {
                Log.printDebug("HOST MAC:" + Misc.getMacAddress());
            }
        } catch (Exception e) {
        	if (Log.ERROR_ON) {
        		Log.printError("Exception occurs on buildComponents().");
        	}
        }
        
        if (Log.DEBUG_ON) {
    		Log.printDebug("Leaving initXlet() successfully.");
    	}
    }

    
    /**
     * Start the Xlet.
     * @throws XletStateChangeException - is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
    	
        FrameworkMain.getInstance().start();	
    }

    /**
     * Pause the Xlet.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * Destroys the Xlet.
     * @param unconditional - true if the instance should be destroyed immediately without any condition.
     * @throws XletStateChangeException - is thrown if the Xlet wishes to continue to execute (Not enter
     * the Destroyed state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
    	

    	if (Log.DEBUG_ON) {
    		Log.printDebug("destroyXlet() called. VbmXlet:" + this);
    	}
    	clearComponents();
    	
    	FrameworkMain.getInstance().destroy();
    }

    /**
     * Constructs, initialize and starts components.
     * @throws Exception if methods of the components raise an exception during constructing, initializing and starting
     * components.
     */
    private void buildComponets() throws Exception {

    	scheduler = new Scheduler();    	
        scheduler.start();
        
        if (Log.DEBUG_ON) {
			Log.printDebug("started scheudler");
        }
		
		requestDispatcher = new CommunicationRequestDispatcher();

		if(Log.DEBUG_ON) {
			Log.printDebug("VBM :  starting CommunicationRequestDispatcher");
		}
						// release and bind IXC
		requestDispatcher.setXletContext(xletContext);        
		requestDispatcher.setScheduler(scheduler);        
		requestDispatcher.start();


		if(Log.DEBUG_ON) {
			Log.printDebug("VBM : started CommunicationRequestDispatcher");
		}

    	// put shared object
        controller = new VbmController();
    	controller.initApp(xletContext);
    	if (Log.DEBUG_ON) {
			Log.printDebug("initialized VbmController and initialized");
    	}
    	
		// check OOB data
		vbmConfigDispatcher = new VbmConfigDispatcher();
        vbmConfigDispatcher.setScheduler(scheduler);
        vbmConfigDispatcher.start();

        if(Log.DEBUG_ON) {
        	Log.printDebug("VBM : started VbmConfigDispatcher and checking OOB data");
        }
  	
    }
    
    
    /**
     * Clears components.
     */
    private void clearComponents() {
    	
    	controller.destroyApp();
    	
    	vbmConfigDispatcher.stop();
    	// no listner
    	//vbmConfigDispatcher.removeListener(listner);
    	requestDispatcher.destroy();
    	
    	Log.printDebug("VBM : stopped CommunicationRequestDispatcher");
    	
    	
        scheduler.stop();
        
        requestDispatcher = null;
        vbmConfigDispatcher = null;
        scheduler = null;
        controller = null;
       
    }
    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getApplicationName()
     */
    /**
     * @return application name
     */
    public String getApplicationName() {
        return VbmInfo.getName();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getVersion()
     */
    /**
     * @return application version
     */
    public String getVersion() {
        return VbmInfo.getVersion();
    }

    /*
     * (non-Javadoc)
     * @see com.videotron.tvi.illico.framework.ApplicationConfig#getXletContext()
     */
    /**
     * @return xlet context
     */
    public XletContext getXletContext() {
        return xletContext;
    }
    
}

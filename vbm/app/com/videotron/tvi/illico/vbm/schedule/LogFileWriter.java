package com.videotron.tvi.illico.vbm.schedule;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.controller.buffer.LogFileManager;
import com.videotron.tvi.illico.vbm.util.TimerWrapper;
import com.videotron.tvi.illico.vbm.util.VbmCode;

import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.ocap.hardware.Host;

/**
 * Schedule Job to trigger a command's end.
 * 
 * @author swkim
 *
 */
public class LogFileWriter extends TimerTask  {

	private int interval;
	/**
	 * constructor.
	 * creates new Log Trigger per an application  for a command
	 * @param ownerApp	owner of this LogTrigger.
	 */
	public LogFileWriter(final int interval) {
		this.interval = interval;
	}
	
	/**
	 * start action of LogTrigger.
	 * LogFileWriter is executed when new cofiguration is generated and storeInterval is changed.
	 * so when you check this code refer to configuration manager
	 */
	public void run() {
	
		if(interval != ConfigurationManager.getInstance().getStoreInterval()) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" new interval is set. So stop current LogFileWriter.");
			}
			return;
		}
		
		if(notValidHDD()){
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogFileWriter : this STB is not a valid STB.");
			}
			return;
		}

		// get vbm log object in shared memory and set store command.
		Hashtable sm = (Hashtable) SharedMemory.getInstance();
		Vector vbmLogBuffer = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		
		// store logs in buffers into disk
		
		
		if(Host.getInstance().getPowerMode() == Host.LOW_POWER) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogFileWriter : STB is in standby mode. So do not write logs into HDD.");
			}			
		} else {
			vbmLogBuffer.isEmpty();
		}
		
		// create new trigger task to store logs for next turn.
		/**
		 * check interval. unit of interval is second		 * 
		 */
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogFileWriter : wrote logs in disk. next disk write interval = " + ConfigurationManager.getInstance().getStoreInterval());
		}
		
		// 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer timer = new Timer();		
		timer.schedule(new LogFileWriter(interval), ConfigurationManager.getInstance().getStoreInterval() * VbmCode.MILISECOND); 
				//ConfigurationManager.getInstance().getStoreInterval() * VbmCode.SECOND_IN_MIN *	VbmCode.MILISECOND);
	}
	
	
	/***
	 * check whether this STB has HDD or not
	 * @return true for not valid STB, false for having STB
	 */
	private boolean notValidHDD(){
		String stbModel = LogFileManager.getInstance().checkHDD();
		
		if(stbModel == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogFileWriter : STB Model is null. so do not check");
			}
			return true;
		}
		
		if(!stbModel.equals(VbmCode.DISK_EXIST)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogFileWriter : this STB is not a valid STB." + stbModel);
			}
			return true;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("LogFileWriter : this STB is a valid STB." + stbModel);
		}
		return false;
	}
}

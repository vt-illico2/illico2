package com.videotron.tvi.illico.vbm.schedule;

import java.util.Timer;

import com.videotron.tvi.illico.vbm.config.Measurement;
import com.videotron.tvi.illico.vbm.util.TimerWrapper;
import com.videotron.tvi.illico.log.Log;


/**
 * LogScheudler is a schedule manager for many schedule tasks.
 * it is a singleton object.
 * 
 * @author swkim
 *
 */
public class TriggerScheduler {

	/**
	 * instance to be used in this application.
	 */
	private static TriggerScheduler instance = null;

	/**
	 * Constructor.
	 * it is not public but used in this class only.
	 */
	protected TriggerScheduler() {
		// do nothing
	}
	
	
	/**
	 * method to get instance of LogScheduler.
	 * 
	 * @return logScheduler instance.
	 */
	public static TriggerScheduler getInstance() {
		if (instance == null) {
			instance = new TriggerScheduler();
		}
		return instance;
	}
	
	public synchronized void createNewTrigger(long delay, Measurement measurement, boolean command, int generalConfigVersion, boolean repeat){
	    
		
		// 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
        if (Log.DEBUG_ON) {
			Log.printDebug(" TriggerScheduler : created trigger with delay, " + delay + ", mID = " + measurement.getMeasurementId() + ", and command = " + command);
        }
       
        // false for calculated delay
        timer.schedule(new ApplicationTrigger(measurement.getAppId(), measurement, command, repeat, generalConfigVersion), delay );
		
	}

}

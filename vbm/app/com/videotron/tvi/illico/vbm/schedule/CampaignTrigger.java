package com.videotron.tvi.illico.vbm.schedule;

import java.util.TimerTask;
import java.util.Vector;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.Campaign;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.config.Measurement;
import com.videotron.tvi.illico.vbm.config.TriggerMeasurementList;
import com.videotron.tvi.illico.vbm.util.VbmCode;

public class CampaignTrigger extends TimerTask {

	Campaign campaign = null;
	
	
	public CampaignTrigger(Campaign campaign){


		this.campaign = campaign;
		
	}
	
	
	public synchronized void run(){

		Vector measurements = campaign.getMeasurementList();
		//Object value = null;
		int count = 0;
		Measurement measurement = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug("ConfigurationManager :  stopCampaign called with " + campaign.getCampaignId());
		}
		
		for(int i=0; i<measurements.size(); i++){
			measurement = (Measurement) measurements.get(i);
			measurement.setCampaignId(campaign.getCampaignId());
			measurement.setCampaignVersion(campaign.getCampaignVersion());
			measurement.setTargetVersion(campaign.getTargetVersion());
			measurement.setReportFrequency(campaign.getReportFrequency());
			measurement.setSendFrequency(campaign.getSendFrequency());
			measurement.setSpecialFrequency(campaign.getSpecialFrequency());
			//measurement.setGeneralSettingVersion(campaign.getGeneralSettingVersion());
			// swkim added config version to check trigger measurement
			
			if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
			// if there is only one reference to measurement, then stop measuring for that measurement
				
				if (Log.DEBUG_ON) {
					Log.printDebug("CampaignTrigger : stopcampaign:  it is trigger measurement. " + measurement.getMeasurementId());
				}
						
				count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(campaign.getGeneralConfigVersion(), measurement.getMeasurementId());
				if (--count < 0) {
					count = 0;
				}
				// 2012.08.21. swkim comment out this if because it only checked when trigger measurement executed.
				//if(count <= VbmCode.LAST_ONE ){
					if (Log.DEBUG_ON) {
						Log.printDebug("CampaignTrigger :  count in triggerList is less than 0. so created 'stop trigger'. count = " + count);
					}
					// createStopTriggerTask(measurement, VbmCode.INIT_VALUE);
					ConfigurationManager.getInstance().setMeasurementInTriggerList(campaign.getCampaignVersion(), measurement.getMeasurementId(), count);
					
				//} else {
				//	if (Log.DEBUG_ON) {
				//		Log.printDebug("ConfigurationManager :  count in triggerList is not 1. so reduce value. count = " + count);
				//	}
				//	setMeasurementInTriggerList(measurement.getMeasurementId(), count);
				//}
				if (Log.DEBUG_ON) {
					Log.printDebug("CampaignTrigger :  removing trigger measurement list of campaing " + 
						measurement.getCampaignId() + " and measurement " + measurement.getMeasurementId());
				}
				// TriggerMeasurementList will be removed in VbmLog when a log is pushed into VBMC.
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("CampaignTrigger : stopcampaing :  it is event measurement. " + measurement.getMeasurementId());
				}
						
				count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(campaign.getCampaignVersion(), measurement.getMeasurementId());
				count = count - 1;
				ConfigurationManager.getInstance().setMeasurementInTriggerList(campaign.getCampaignVersion(), measurement.getMeasurementId(), count);
				
				// VDTRSUPPORT-293
				// count should be bigger than last one
				if(count <= VbmCode.LAST_ONE ){
					if (Log.DEBUG_ON) {
						Log.printDebug("CampaignTrigger : stopcampaing :  last event measurement. so stop event triggering. " + measurement.getMeasurementId());
					}
					
					ConfigurationManager.getInstance().createStopEventMeasurement(measurement, campaign.getEndDate());
					
				}
			}
			
			//triggerStatusList.put(Long.toString(measurement.getMeasurementId()), Integer.toString(--count));
		}
		
		if (campaign.getEndDate() <= System.currentTimeMillis()) {
			
			if (Log.DEBUG_ON) {
				Log.printDebug("CampaignTrigger : this campaign is reached in end date, so uploadBuffer.");
			}
			
			LogScheduler.getInstance().createScheduler(campaign.getSendFrequency());
			
		}
	}
}

package com.videotron.tvi.illico.vbm.schedule;

import java.util.Calendar;
import java.util.Timer;
import java.util.Vector;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.controller.buffer.LogFileManager;
import com.videotron.tvi.illico.vbm.uploader.LogUploader;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.TimerWrapper;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.videotron.tvi.illico.vbm.util.VbmInfo;

import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * LogScheudler is a schedule manager for many schedule tasks.
 * it is a singleton object.
 * 
 * @author swkim
 *
 */
public class LogScheduler {

	/** instance to be used in this application.  **/
	private static LogScheduler instance = null;
	/** indicator initialized this log scheduler **/
	private static boolean isInitialized = false;
	
	/**
	 * Constructor.
	 * it is not public but used in this class only.
	 */
	protected LogScheduler() {
		// do nothing
	}
	
	
	/**
	 * method to get instance of LogScheduler.
	 * 
	 * @return logScheduler instance.
	 */
	public static LogScheduler getInstance() {
		if (instance == null) {
			instance = new LogScheduler();
		}
		return instance;
	}
	
	/***
	 * create scheduler for all log only one time.
	 */
	public void createSchedulerForAllLog() {
		if (Log.DEBUG_ON) {
			Log.printDebug("LogScheduler: creatSchedulerForAllLog");
		}
		long delay = 0;

		if(Misc.getDailyUploadTimeWithProperty(false) > Misc.getCurrentTime()){
			delay = Misc.getDailyUploadTimeWithProperty(false) - Misc.getCurrentTime();
		} else {
			delay = VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND + Misc.getDailyUploadTimeWithProperty(false) - Misc.getCurrentTime();
		}
//        Calendar nextDay = Calendar.getInstance();
//        nextDay.add(Calendar.DAY_OF_YEAR, 1);
//
//        long period =  nextDay.getTimeInMillis() -  Calendar.getInstance().getTimeInMillis();
//        if (Log.DEBUG_ON) {
//			Log.printDebug("LogScheduler : created daily upload trigger " + delay + "/" + period);
//        }
		
		long forciblyDelayTest = DataCenter.getInstance().getLong("forcibly.delay.test");
		
		if (forciblyDelayTest != 0) {
			delay = forciblyDelayTest;
		}
		
        printTime(delay);
//        printTime(period + delay);
        
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
		timer.schedule(new LogTrigger(VbmCode.ALL_LOG_CACHE), delay, 0);
		
	}
	
	/***
	 * create scheduler for type only one time.
	 */
	public void createScheduler(int type){
		if (Log.DEBUG_ON) {
			Log.printDebug("LogScheduler: createScheduler type=" + type);
		}
		long delay = 0;

		if(Misc.getDailyUploadTimeWithProperty(false) > Misc.getCurrentTime()){
			delay = Misc.getDailyUploadTimeWithProperty(false) - Misc.getCurrentTime();
		} else {
			delay = VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND + Misc.getDailyUploadTimeWithProperty(false) - Misc.getCurrentTime();
		}
//        Calendar nextDay = Calendar.getInstance();
//        nextDay.add(Calendar.DAY_OF_YEAR, 1);
//
//        long period =  nextDay.getTimeInMillis() -  Calendar.getInstance().getTimeInMillis();
//        if (Log.DEBUG_ON) {
//			Log.printDebug("LogScheduler : created daily upload trigger " + delay + "/" + period);
//        }
		
		long forciblyDelayTest = DataCenter.getInstance().getLong("forcibly.delay.test");
		
		if (forciblyDelayTest != 0) {
			delay = forciblyDelayTest;
		}
		
        printTime(delay);
//        printTime(period + delay);
        
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
		timer.schedule(new LogTrigger(type), delay, 0);
		
	}
	
	/***
	 * create daily scheduler 
	 */
	private void createDailyScheduler(){
	
		long delay = 0;

		if(Misc.getDailyUploadTime(false) > Misc.getCurrentTime()){
			delay = Misc.getDailyUploadTime(false) - Misc.getCurrentTime();
		} else {
			delay = VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND + Misc.getDailyUploadTime(false) - Misc.getCurrentTime();
		}
        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_YEAR, 1);

        long period =  nextDay.getTimeInMillis() -  Calendar.getInstance().getTimeInMillis();
        if (Log.DEBUG_ON) {
			Log.printDebug("LogScheduler : created daily upload trigger " + delay + "/" + period);
        }
        printTime(delay);
        printTime(period + delay);
        
        // 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
		timer.schedule(new LogTrigger(VbmCode.DAILY_LOG_CACHE), delay, period);
		
	}

	/***
	 *  create weekly scheduler 
	 */
	private void createWeeklyScheduler(){

		long delay = 0;

		if(Misc.getWeeklyUploadTime(false) > Misc.getCurrentTime()){
			delay = Misc.getWeeklyUploadTime(false) - Misc.getCurrentTime();
		} else {
			delay = VbmCode.SECOND_IN_WEEK * VbmCode.MILISECOND + Misc.getWeeklyUploadTime(false) - Misc.getCurrentTime();
		}
		//delay = delay * VbmCode.MILISECOND + Misc.getIntervalTail();

		

        Calendar nextWeek = Calendar.getInstance();
        nextWeek.add(Calendar.DAY_OF_YEAR, 7);  // set 7 days for a week

        long period =  nextWeek.getTimeInMillis() -  Calendar.getInstance().getTimeInMillis();
        
        if (Log.DEBUG_ON) {
			Log.printDebug("LogScheduler : created weekly upload trigger " + delay + "/" + period);
        }
        printTime(delay);
        printTime(period + delay);
        
     // 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
		timer.schedule(new LogTrigger(VbmCode.WEEKLY_LOG_CACHE), delay, period);
	}
	
	
	/***
	 * create monthly scheduler 
	 */
	private void createMonthlyScheduler(){

   
        long delay = 0;
        
        delay = Misc.getMonthlyUploadTime(false);
       
        if (Log.DEBUG_ON) {
			Log.printDebug("LogScheduler : created monthly upload trigger " + (new Date(delay)).toString()
					+ "next = " + (new Date(delay)).toString());
        }
     // 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer trigger = new Timer();
        // in monthly scheduler, there is no period.
        // but in LogTrigger task, it invoke new monthly trigger
		timer.schedule(new LogTrigger(VbmCode.MONTHLY_LOG_CACHE), delay );
	}
	
	
	/***
	 * print next trigger time
	 * @param time next trigger time
	 */
	private void printTime(long time){
		
		 if (Log.DEBUG_ON) {
			 Date now = new Date(Calendar.getInstance().getTimeInMillis() + time);
			 Log.printDebug("LogScheduler : printTime " + now.toString());
		 }
	}

	private void setInitialized(){
		isInitialized = true; 
	}
	
	
	/***
	 * initialize Log scheduler
	 */
	public synchronized void initializeScheduler(){
        if(Log.DEBUG_ON){
        	Log.printDebug("LogScheduler : initialized and start regular daily, weekly and monthly uploader");
        }

        if(!isInitialized) {
			
			checkLogAndUpload();
			
			createDailyScheduler();
			createWeeklyScheduler();
			createMonthlyScheduler();
			
			setInitialized();
		}
		
	}
		
	/***
	 * check whether there are logs stored in HDD, then upload those logs
	 * 
	 */
	private void checkLogAndUpload(){
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogScheduler : checking logs in HDD and upload");
		}
		
		LogFileManager logFileManager = LogFileManager.getInstance();
		
		String checkDiskModel = logFileManager.getInstance().checkHDD();
		
		if(checkDiskModel == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : internal disk is not available. so do not store logs into disk.");
			}
			return;
		}
		
		if( !checkDiskModel.equals(VbmCode.DISK_EXIST)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : this model is not valid STB model. Model is " + checkDiskModel);
			}
			return;
		}
		
		String logString = logFileManager.getInstance().readLog(VbmCode.DAILY_LOG_CACHE);
		
		if(logString == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : log string of daily log buffer is null. so do not upload log");
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : there are logs in daily log, so start to upload " + logString);
			}
			(new LogUploader(0, logString, null)).start();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : after uploading logs, clearing HDD of daily log ");
			}
			logFileManager.getInstance().clearLogFile(VbmCode.DAILY_LOG_CACHE);
		}
		

		logString = logFileManager.getInstance().readLog(VbmCode.WEEKLY_LOG_CACHE);
		
		if(logString == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : log string of weekly log buffer is null. so do not upload log");
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : there are logs in weekly log, so start to upload " + logString);
			}
			(new LogUploader(0, logString, null)).start();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : after uploading logs, clearing HDD of weekly log ");
			}
			logFileManager.getInstance().clearLogFile(VbmCode.WEEKLY_LOG_CACHE);
		}
		

		logString = logFileManager.getInstance().readLog(VbmCode.MONTHLY_LOG_CACHE);
		
		if(logString == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : log string of monthly log buffer is null. so do not upload log");
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : there are logs in monthly log, so start to upload " + logString);
			}
			(new LogUploader(0, logString, null)).start();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("LogScheduler : after uploading logs, clearing HDD of monthly log ");
			}
			logFileManager.getInstance().clearLogFile(VbmCode.MONTHLY_LOG_CACHE);
		}
	}
	
	/*
	public static void main(String args[]){
		LogScheduler ls = new LogScheduler();
		
		System.out.println(Calendar.getInstance().get(Calendar.MONTH));
		System.out.println(Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
		ls.createDailyScheduler();
		ls.createWeeklyScheduler();
		ls.createMonthlyScheduler();
		Timer timer = new Timer();
		timer.schedule((new LogTrigger(VbmCode.MONTHLY_LOG_CACHE)), 0);
	}
	*/
}



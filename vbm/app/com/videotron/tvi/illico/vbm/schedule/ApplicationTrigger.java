package com.videotron.tvi.illico.vbm.schedule;

import com.videotron.tvi.illico.vbm.communication.VbmServiceImpl;
import com.videotron.tvi.illico.vbm.config.Campaign;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.config.Measurement;
import com.videotron.tvi.illico.vbm.config.TriggerMeasurementList;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;
//import com.videotron.tvi.illico.stc.communication.ApplicationProperties;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.videotron.tvi.illico.vbm.util.TimerWrapper;

//import org.dvb.io.ixc.IxcRegistry;

/**
 * TimerTask to execute set/unset trigger
 * 
 * @author swkim
 *
 */
public class ApplicationTrigger extends TimerTask  {

	/** application name for this trigger **/
	private String appName;
	/** measurement id for this trigger **/
	private Measurement measurement;
	/** command to start/stop this trigger **/
	private boolean command;
	/** retry count for this trigger task **/
	private boolean retry;
	
	private int generalConfigVersion;
	
	
	/**
	 * constructor.
	 * creates new Log Trigger per an application  for a command
	 * @param appName application name
	 * @param measurement measurement
	 * @param command true for start, false to stop
	 * @param retry is retry or not
	 */
	public ApplicationTrigger(String appName, Measurement measurement, boolean command, boolean retry, int generalConfigVersion) {
		// to use this class, check appName and schedule is not null and valid
		this.appName = appName;
		this.measurement = measurement;
		this.command = command;
		this.retry = retry;
		this.generalConfigVersion = generalConfigVersion;
	}
	
	
	/**
	 * start action of LogTrigger.
	 */
	public synchronized void run() {
		
		if(!checkMeasurementVaildity(measurement.getCampaignId())) {
			
			if (Log.DEBUG_ON) {
				Log.printDebug(" Application Trigger : Trigger Measurement : " + measurement.getMeasurementId() + " is out of date. So returned.");
			}
			return;
		}
				
		// check command. 
		// if command is start, then start.
		// if command is stop, then check all campaign about that measurement and stop if there is no measurement.

		if(measurement.getMeasurementId() < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" Application Trigger : Trigger Measurement : " + command);
			}
			//System.out.println(" Application Trigger : Trigger Measurement : " + command);
			// triggerType keeps schedule and run automatically
			if(command) startTriggerMeasurement();
			else stopTriggerMeasurement();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug(" Application Trigger : Event Measurement : " + command);
			}
			//System.out.println(" Application Trigger : Event Measurement : " + command);
			// eventType trigger is signaled to target application
			if(command) startEventMeasurement() ;
			else stopEventMeasurement();
		}

	}
	
	private boolean checkMeasurementVaildity(long cId){
		
		
		if(ConfigurationManager.getInstance().getConfiguration().getCampaign(cId) == null) return false;
		
		if(System.currentTimeMillis() > ConfigurationManager.getInstance().getConfiguration().getCampaign(cId).getEndDate()){
			if (Log.DEBUG_ON) {
				Log.printDebug(" Application Trigger : checkMeasurementVaildity : curr = " + System.currentTimeMillis() + ", end = " + ConfigurationManager.getInstance().getConfiguration().getCampaign(cId).getEndDate() );
			}
			return false;
		}
		
		
		return true;
	}
	
	
	/***
	 * start when measurement is trigger measurement
	 */
	private void startTriggerMeasurement(){
		
		int count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(generalConfigVersion, measurement.getMeasurementId());
		
		// config version can be same in this case, so change to check configuration version
		// 2012.8.28
		/*
		Vector campList = ConfigurationManager.getInstance().getConfiguration().getCampaignList();
		boolean isExisting = false;
		for(int i=0; i<campList.size(); i++){
			if(measurement.getCampaignId() == ((Campaign)campList.get(i)).getCampaignId()){
				isExisting = true;
			}
		}
		// check whether configuration is changed.
		if(!isExisting){
			if (Log.DEBUG_ON) {
				Log.printDebug("ApplicatinoTrigger : configuration changed for trigger measurement");
				Log.printDebug(" campaign for trigger measurement to trigger measurement " + measurement.getCampaignId()+
					" ," + measurement.getMeasurementId() + "," + measurement.getAppId() + ") is changed already.");
			}
			return;
		}
		
		*/
		
		if (measurement.getGeneralSettingVersion() != ConfigurationManager.getInstance().getConfiguration().getGeneralSettingVersion()){
			if (Log.DEBUG_ON) {
				Log.printDebug("ApplicatinoTrigger : configuration changed for trigger measurement");
				Log.printDebug(" campaign for trigger measurement to trigger measurement " + measurement.getCampaignId()+
					" ," + measurement.getMeasurementId() + "," + measurement.getAppId() + ") is changed already.");
			}
			return;
		}
				
		
		// stop triggering when it is not first one
		if(count == VbmCode.LAST_ONE) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ApplicatinoTrigger : trigger measurement is stopped.");
				Log.printDebug(" trigger measurement to trigger measurement to start " +
					" new ApplicationTrigger(" + measurement.getMeasurementId() + "," + measurement.getAppId() + ") is stopped already.");
			}
			return;
		}
		
		// start trigger because it is not first one
		
		TriggerMeasurementList.getInstance().add(measurement);
		
		// don't need to check count.
		// because if there are several task to execute trigger measurement, they can be different one
		long delay = countDelay();
		if (Log.DEBUG_ON) {
			Log.printDebug(" creating new trigger measurement to trigger measurement to start " +
				" new ApplicationTrigger(" + measurement.getMeasurementId() + "," + measurement.getAppId() + ") now.");
		}
		//System.out.println(" creating new trigger measurement to trigger measurement to start " +
		//				" new ApplicationTrigger(" + measurement.getMeasurementId() + "," + measurement.getAppId() + ") now.");
		// execute trigger measurement
		VbmServiceImpl vbmServiceImpl = null;
		
		try {
			vbmServiceImpl = (VbmServiceImpl) DataCenter.getInstance().get(VbmService.IXC_NAME);
			if(vbmServiceImpl != null) {
				vbmServiceImpl.triggerLogGathering(appName, measurement.getMeasurementId());
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug(" vbm service impl is null. so do not start trigger measurement." + appName + ", " + measurement.getMeasurementId() );
				}
				//delay = VbmCode.RETRY_INTERVAL;
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		// add measurement for trigger measurement.
		// this measurement will be removed when measurement log would be stored and checked in VbmLog
		if (Log.DEBUG_ON) {
			Log.printDebug(" adding TriggerMeasurementCounter and TriggerMeasurementList " + retry +
				", count = " + TriggerMeasurementList.getInstance().count(measurement.getCampaignId(), measurement.getUsagePointId(),measurement.getMeasurementId()));
		}
		//System.out.println(" adding TriggerMeasurementCounter and TriggerMeasurementList " + retry +
		//		", count = " + TriggerMeasurementList.getInstance().count(measurement.getCampaignId(),measurement.getMeasurementId()));
		//System.out.println(" trigger measurement of " + measurement.getMeasurementId() + " is triggered again in " + delay + " later");
		if (Log.DEBUG_ON) {
			Log.printDebug(" trigger measurement of " + measurement.getMeasurementId() + " is triggered again in " + delay + " later");
		}	// set next schedule
		
				
		
		// 2012.06.04. changed timer into TimerWrappter
		TimerWrapper timer = new TimerWrapper();
		//Timer timer = new Timer();
		// true for schedule delay
		timer.schedule(new ApplicationTrigger(appName, measurement, command, true, generalConfigVersion), delay);

				
	}
	
	
	/****
	 * stop trigger when measurement is trigger type
	 */
	private void stopTriggerMeasurement(){
		// checking count of trigger measurement is done in configuration manager. so this method will not be called.
		int count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(generalConfigVersion, measurement.getMeasurementId());
		//ConfigurationManager.getInstance().setMeasurementInTriggerList(measurement.getMeasurementId(), --count);

		if (Log.DEBUG_ON) {
			Log.printDebug(" failed!!! this method should not be called . "
				+ "creating new ApplicationTrigger to trigger measurement to stop" 
				+ " new ApplicationTrigger(" + measurement.getMeasurementId() + "," + measurement.getAppId() + 
				"," + count + ")");
		}
	}
	
	
	/***
	 * make delay count considering report schedule and special schedule
	 * @return delay to next trigger
	 */
	private long countDelay(){
		
		// return 1 minutes later
		//return 60 * 1000L;
		/**
		 * TODO : correct it.
		 * 
		 */
		
		if(measurement == null) return (long)VbmCode.INIT_VALUE;
		long delay = -1;
		
		// as discussion with CFC at late October, we only check special frequency in weekly report frequency.
		if(measurement.getReportFrequency() == VbmCode.REPORT_FREQ_HOURLY){
			//if(measurement.getSpecialFrequency() <= VbmCode.SPECIAL_FREQ_NONE) {
				delay = Misc.getHourlyUploadTime(retry);
			//} else {
			//	delay = Misc.getHourlyUploadTime() % measurement.getSpecialFrequency();
			//}
		} else if (measurement.getReportFrequency() == VbmCode.REPORT_FREQ_DAILY) {
			//if(measurement.getSpecialFrequency() <= VbmCode.SPECIAL_FREQ_NONE) {
				delay = Misc.getDailyUploadTime(retry);
			//	delay = Misc.getDailyUploadTime() % measurement.getSpecialFrequency();
		} else if (measurement.getReportFrequency() == VbmCode.REPORT_FREQ_WEEKLY) {
			if(measurement.getSpecialFrequency() <= VbmCode.SPECIAL_FREQ_NONE) {
				delay = Misc.getWeeklyUploadTime(retry);
			} else {
				delay = parseSpecialFrequency(measurement.getSpecialFrequency());
			}
		} else if (measurement.getReportFrequency() == VbmCode.REPORT_FREQ_MONTHLY) {
				delay = Misc.getMonthlyUploadTime(retry);
				//delay = Misc.getMonthlyUploadTime() % measurement.getSpecialFrequency();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug(" Error settting report frequency");
			}
		}
		
		return delay;

	}
	
	/***
	 * parse special frequency.
	 * it is true only to weeky report type
	 * 
	 * @param specialFreq - special frequency
	 * @return delay when report frequency is weekly and there is a special frequency related to it.
	 */
	private long parseSpecialFrequency(int specialFreq){
		long delay = -1;
		
	
        boolean [] days = new boolean [VbmCode.DAYS]; // boolean is initially false. so do not need to be initallized 
        
		String a = Integer.toBinaryString(specialFreq);
		int size = a.length();
		for(int i=size-1; i>=0; i--){
			if( a.charAt(i) == '1') days[size-i-1] = true;
		}

        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) -1;// sunday = 0

        boolean found = false;
        
        for(int i=0; i<VbmCode.DAYS; i++){
        	if((days[i]) & (i > today)) {
        		//System.out.println(" VBM : next turn in days 1 =  " + i);
        		found = true;
        		today = i - today;
        		break;
        	}
        }
        
        if(!found) {
	        for(int i=0; i<=today; i++){
	        	if(days[i]) {
	        		found = true;
	        		today = (VbmCode.DAYS - today + i);
	        		break;
	        	}
	        }
        }

        if(found){
        	if (Log.DEBUG_ON) {
				Log.printDebug(" checking next schedule : get next schedule" + today);
        	}
        } else {
        	if (Log.DEBUG_ON) {
				Log.printDebug(" checking next schedule : no next schedule");
        	}
        }
        // next execution time is daily upload time + days later
        delay = Misc.getDailyUploadTime(retry) + (today -1) * VbmCode.SECOND_IN_DAY * VbmCode.MILISECOND;
		
        return delay;
	}
	
	
	
	/***
	 * start trigger when measurement is event type
	 * 
	 */
	private void startEventMeasurement(){
		
		try{
			//int count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(measurement.getMeasurementId());
			if (Log.DEBUG_ON) {
				Log.printDebug(" ApplicationTrigger : start executed " + appName + "," + measurement.getMeasurementId() );
			}
			
			VbmServiceImpl vbmServiceImpl = (VbmServiceImpl) DataCenter.getInstance().get(VbmService.IXC_NAME);
			if(vbmServiceImpl != null) {
				vbmServiceImpl.notifyLogCommandChange(appName, measurement.getMeasurementId(), command);
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug(" vbm service impl is null. so do not start event measurement." + appName + ", " + measurement.getMeasurementId());
				}
			}
			//ConfigurationManager.getInstance().setMeasurementInTriggerList(measurementId, ++count);
			
		}catch(Exception e){
			if (Log.DEBUG_ON) {
	    		Log.printError(e.getMessage());
	    	}
		}
		
	}
	
	
	/***
	 * stop trigger when measurement is event type.
	 */
	private void stopEventMeasurement(){
		
		// in stop trigger, it will check count in trigger list and if trigger is the last one, then it will remove
		int count = ConfigurationManager.getInstance().checkMeasurementInTriggerList(generalConfigVersion, measurement.getMeasurementId());
		if (Log.DEBUG_ON) {
			Log.printDebug(" ApplicationTrigger : stop executed " + measurement.getMeasurementId()   + ", count = " + count);
		}
		// swkim changed 2013.11.01
		//if(count == VbmCode.LAST_ONE) {		
		if(count == VbmCode.FIRST_ONE) {			
			VbmServiceImpl vbmServiceImpl = (VbmServiceImpl) DataCenter.getInstance().get(VbmService.IXC_NAME);
			if(vbmServiceImpl != null){
				vbmServiceImpl.notifyLogCommandChange(appName, measurement.getMeasurementId(), false);
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug(" vbm service impl is null. so do not stop event measurement." + appName + ", " + measurement.getMeasurementId() );
				}
			}
		} else {
			if (Log.ERROR_ON) {
	     		Log.printError(count + " Invalid event measurement status list for measurement " + measurement.getMeasurementId());
	    	}
			//count = VbmCode.LAST_ONE;
		}
		
		//ConfigurationManager.getInstance().setMeasurementInTriggerList(measurement.getMeasurementId(), --count);
		
	
	}
	
	/*
	public static void main(String args[]){
		
		int specialFreq = 12;		
        boolean [] days = new boolean [VbmCode.DAYS]; // boolean is initially false. so do not need to be initallized 
        
		String a = Integer.toBinaryString(specialFreq);
		int size = a.length();
		for(int i=size-1; i>=0; i--){
			//System.out.println(i + "th = " + a.charAt(i));
			if( a.charAt(i) == '1') days[size-i-1] = true;
		}

        int today = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) -1;// sunday = 0

        boolean found = false;
        //System.out.println("today = " + today);
        for(int i=0; i<VbmCode.DAYS; i++){
        	System.out.print(days[i] + " ");
        	if((days[i]) & (i > today)) {
        		found = true;
        		today = i - today;
        		break;
        	}
        }
        
        if(!found) {
	        for(int i=0; i<=today; i++){
	        	System.out.print(days[i] + "/ ");
	        	if(days[i]) {
	        		found = true;
	        		today = (VbmCode.DAYS - today + i);
	        		break;
	        	}
	        }
        }

        if(found){
        	if (Log.DEBUG_ON) {
				Log.printDebug("get next schedule" + today);
        	}
        } else {
        	if (Log.DEBUG_ON) {
				Log.printDebug("no next schedule");
        	}
        }
	}
	
	*/
	
}

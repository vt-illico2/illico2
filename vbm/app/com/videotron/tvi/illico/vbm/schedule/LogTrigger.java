package com.videotron.tvi.illico.vbm.schedule;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.controller.buffer.LogManager;
import com.videotron.tvi.illico.vbm.util.TimerWrapper;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.videotron.tvi.illico.log.Log;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import org.ocap.hardware.Host;

/**
 * Schedule Job to trigger a command's end.
 * 
 * @author swkim
 *
 */
public class LogTrigger extends TimerTask  {

	/**
	 * task onwer's name. it is an application.
	 */
	private int type = 0;
	
	
	/**
	 * constructor.
	 * creates new Log Trigger per an application  for a command
	 * @param ownerApp	owner of this LogTrigger.
	 */
	public LogTrigger(final int type) {
		// to use this class, check appName and schedule is not null and valid
		this.type = type;
	}
	
	/**
	 * start action of LogTrigger.
	 */
	public void run() {
		if (Log.DEBUG_ON) {
			Log.printDebug("LogTrigger : for " + type + " triggered. ");
    	}
		
		if (type == VbmCode.ALL_LOG_CACHE) {
			uploadAllBuffer();
		} else {
			// swkim checkes size of target
			if((type <= VbmCode.BUFFER_NUMBER) && (ConfigurationManager.getInstance().getTarget().getJoinedCampaigns().size() >0)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("LogTrigger : for " + type + " triggered");
		    	}		
				uploadBuffer();
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("LogTrigger : for " + type + " did not trigger " + ConfigurationManager.getInstance().getTarget().getJoinedCampaigns().size());
		    	}
			}
			
	
			// for monthly trigger, it calculate the day of next month and set new trigger
			// because days in month is differ in every day
			if(type == VbmCode.MONTHLY_LOG_CACHE ){
	
		        Calendar nextMonth = Calendar.getInstance();
		        nextMonth.add(Calendar.MONTH, 1);
		        // delay is difference between next month and this month
		        long delay =  nextMonth.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
		        
		        
		        if (Log.DEBUG_ON) {
		    		Log.printDebug(" VBM : created monthly upload trigger " + nextMonth.toString());
		    	}
		        
		     // 2012.06.04. changed timer into TimerWrappter
				TimerWrapper timer = new TimerWrapper();
				//Timer trigger = new Timer();
				timer.schedule(new LogTrigger(VbmCode.MONTHLY_LOG_CACHE), delay);
				
			}
		}
	}
	
	
	/**
	 * really upload temporary buffer into uploader.
	 * 
	 */
	private void uploadBuffer(){
		
		/**
		 * TODO : recover this in real test
		 */
		SharedMemory sm = SharedMemory.getInstance();		
		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		
		if (Log.DEBUG_ON) {
			Log.printDebug("LogTrigger : upload buffer with rsKey null");
		}
		// set rskey null
		logCache.setElementAt(null, 0);
		logCache.removeElementAt(type);
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("LogTrigger : uploadBuffer : get vector from shared memory and upload.");
		}
	}
	
	private void uploadAllBuffer() {
		SharedMemory sm = SharedMemory.getInstance();		
		Vector logCache = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		
		if (Log.DEBUG_ON) {
			Log.printDebug("LogTrigger : upload buffer with rsKey null");
		}
		// set rskey null
		logCache.setElementAt(null, 0);
		logCache.removeElementAt(VbmCode.ALL_LOG_CACHE);
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("LogTrigger : uploadBuffer : get vector from shared memory and upload.");
		}
	}
	
}

package com.videotron.tvi.illico.vbm.communication;

/**
 * SysLogServerDataUpdateListener interface.
 * @author sccu
 */
public interface VbmConfigDataUpdateListener {

    /**
     * Invoked when DataUpdateEvent occurs.
     * @param event - a VbmConfigDataUpdateEvent instance.
     */
    void onDataUpdateEvent(VbmConfigDataUpdateEvent event);
}

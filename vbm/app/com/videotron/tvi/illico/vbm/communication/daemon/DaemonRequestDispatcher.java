package com.videotron.tvi.illico.vbm.communication.daemon;

import java.lang.reflect.Method;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.util.StringUtils;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.videotron.tvi.illico.vbm.util.concurrent.Scheduler;
import com.videotron.tvi.illico.vbm.util.concurrent.Task;

/**
 * STC will make this instance just one time.
 * @author thorn
 */
public class DaemonRequestDispatcher implements RemoteRequestListener {
	
	
	/** class instance. */
	private Class clazz;
	
	/** method array. */
	private Method[] methods;
	
	/** Scheduler instance. */
    private Scheduler scheduler;
    
	/**
     * Initiates an instance.
     * @param sch - scheduler instance.
     */
    public synchronized void setScheduler(Scheduler sch) {
        if (sch == null) {
            throw new IllegalArgumentException("Argument should not be null.");
        }
        scheduler = sch;
    }
    /**
     * Initiates an instance.
     * @param className - controller class name
     */
	public synchronized void init(final String className) {
		try {
			this.clazz = Class.forName(className);
			this.methods = clazz.getMethods();
			
		} catch (ClassNotFoundException e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
		}
	}
	
	/**
	 * Returns true if the DaemonRequestDispatcher has method of method name.
	 * @param method - method name
	 * @return true if the DaemonRequestDispatcher has method of method name.
	 */
	private synchronized boolean hasMethod(final String method) {
		
		if (methods == null) {
			throw new IllegalStateException();
		}
		
		for (int i = 0 , size = methods.length; i < size; i++) {
			if (method.equals(methods[i].getName())) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 
	 * @param method - the method is invoked 
	 * @param request - the argument used for the method call 
	 * @param response - the argument used for the method call
	 */
	private synchronized void invoke(String method, DaemonRequest request, DaemonResponse response) {
		try {
			for (int i = 0, size = methods.length; i < size; i++) {
				if (method.equals(methods[i].getName())) {
					Object[] args = new Object[] {request, response};
					methods[i].invoke(clazz.newInstance(), args);
					break;
				}
			}
		} catch (IllegalArgumentException e) {
			response.setResultCode(VbmCode.DIRECT_UPLOAD_FAILED);
			if (Log.ERROR_ON) {
				Log.print(e);
			}
		} catch (Exception e) {
			response.setResultCode(VbmCode.DIRECT_UPLOAD_FAILED);
			if (Log.ERROR_ON) {
				Log.print(e);
			}
		}
	}
	
	/**
     * Pass request to host application by daemon.
     * @param method - method name to invoked by host application.
     * @param queryString - parameters(parma1=value1&param2=value2..)
     * @return response of the request
     * @throws RemoteException occur during the execution of a remote method call
     */
	public byte[] request(final String method, final byte[] queryString) throws RemoteException {
		
//		DaemonRequestParser parser = new DaemonRequestParser();
//		DaemonResponseWriter writer = new DaemonResponseWriter();
		
		DaemonResponse response = new DaemonResponse();
		//1. check validation of method 
		if (!(StringUtils.hasText(method) && hasMethod(method))) {
			response.setResultCode(VbmCode.DIRECT_UPLOAD_FAILED);
			try {
				return DaemonResponseWriter.write(response);
			} catch (Exception e) {
				if (Log.ERROR_ON) {
					Log.print(e);
				}
			}
		}
		
		try {
			if (Log.DEBUG_ON) {
				Log.printDebug(new String(queryString));
			}
			//TODO
			//2. parse input
			DaemonRequest request = DaemonRequestParser.parse(new String(queryString));
			onRequest(method, request, response);
			return DaemonResponseWriter.write(response);
			
		} catch (Exception e) {
			
			if (Log.ERROR_ON) {
				Log.print(e);
			}
			
			response.setResultCode(VbmCode.DIRECT_UPLOAD_FAILED);
			try {
				return DaemonResponseWriter.write(response);
			} catch (Exception ex) {
				if (Log.ERROR_ON) {
					Log.print(ex);
				}
			}
		}
		return null;
	}
	
	/**
	 * execute the DaemonTask to invoke the method.
	 * @param method - the method is invoked 
	 * @param request - the argument used for the method call 
	 * @param response - the argument used for the method call
	 */
    private synchronized void onRequest(String method, DaemonRequest request, DaemonResponse response) {
    	if (scheduler == null) {
    		if (Log.DEBUG_ON) {
				Log.printDebug("scheduler scheduler");
			}
    	}
        scheduler.execute(new DaemonTask(method, request, response));
    }
    
    /**
     * Implements Task interface to call notifyDataUpdate().
     */
    private class DaemonTask implements Task {
    	
    	/** method string. */
        private final String method;
        /** request from daemon. */
        private final DaemonRequest request;
        /** response to daemon. */
        private final DaemonResponse response;

        /**
         * Initiates an instance.
         * @param aMethod - a method name of the method is invoked  
         * @param aRequest - the argument used for the method call 
         * @param aResponse - the argument used for the method call
         */
        public DaemonTask(final String aMethod, final DaemonRequest aRequest, final DaemonResponse aResponse) {
        	this.method = aMethod;
			this.request = aRequest;
			this.response = aResponse;
        }

        /**
         * Returns the task name.
         * @return the string "DaemonTask".
         */
        public String getName() {
            return "DaemonTask";
        }

        /**
         * Invokes method of controller.
         */
        public void run() {
           invoke(this.method, this.request, this.response);
        }
    };
}

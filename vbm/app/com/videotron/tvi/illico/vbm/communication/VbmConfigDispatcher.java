/*
 *  SysLogServerConfigDispatcher.java    $Revision: 1.1 $ $Date: 2014/04/04 04:45:53 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.vbm.communication;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.dvb.dsmcc.DSMCCObject;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.videotron.tvi.illico.vbm.util.concurrent.Scheduler;
import com.videotron.tvi.illico.vbm.util.concurrent.Task;

/**
 * Provides access to the contents of configuration files in OC.
 * It notifies modification of configuration via
 * DataUpdateListener.
 * 
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public class VbmConfigDispatcher {

	/** config file name. */
	private static final String CONFIG_FILE = VbmCode.CONFIG_FILE;

	/** member file name. */
	private static final String MEMBER_FILE = VbmCode.MEMBER_FILE; 
	
	/** Scheduler instance. */
	private Scheduler scheduler;

	/** a list of listeners. */
	private final List listeners = new ArrayList(1);

	/** OcApdater instance. */
	private final OcAdapter ocAdapter = new OcAdapter();
	

	private static boolean notInitialized = true;
	

	/**
	 * @author sccu
	 */
	private class OcAdapter implements DataUpdateListener {

		/** service string. */
		private static final String DATA_OOB = VbmCode.VBM_DATA_OOB;

		/** config file key. */
		private static final String CONFIG_FILE_KEY = VbmCode.CONFIG_FILE_KEY;
		private static final String MEMBER_FILE_KEY = VbmCode.MEMBER_FILE_KEY;

		/**
		 * Starts to listen a config file.
		 */
		public void startToListen() {
			DataCenter.getInstance().addDataUpdateListener(CONFIG_FILE_KEY, this);
			DataCenter.getInstance().addDataUpdateListener(MEMBER_FILE_KEY, this);
		}

		/**
		 * Stops to listen a config file.
		 */
		public void stopToListen() {
			DataCenter.getInstance().removeDataUpdateListener(CONFIG_FILE_KEY, this);
			DataCenter.getInstance().removeDataUpdateListener(MEMBER_FILE_KEY, this);
		}

		/**
		 * Retrieves a version file from OC.
		 * 
		 * @return a version file.
		 * @throws IOException
		 *             if fails to load from Oc.
		 */
		public File getVbmBroadcastingFile(String fileName) throws IOException {
			if (Log.DEBUG_ON) {
				Log.printDebug("Entering getVersionFile() of " + fileName);
			}

			File file = (File) DataCenter.getInstance().get(fileName);
			if (file != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("Loads " + fileName + " from data center.");
				}
				return file;
			}

			file = getOcFile(fileName);
			if (file != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("Loads " + fileName + " via DSMCC.");
				}

				return file;
			}

			throw new FileNotFoundException("Fails to load " + fileName + " from OC.");
		}

		
		
		/**
		 * Retrieves a file from OC.
		 * 
		 * @param fileName
		 *            - file name.
		 * @return the specified File instance.
		 * @throws IOException
		 *             if fails to load the specified file.
		 */
		public File getOcFile(String fileName) throws IOException {
			if (Log.DEBUG_ON) {
				Log.printDebug("Entering getOcFile() to load "
						+ fileName + ".");
			}

			DSMCCObject mountPoint = DataAdapterManager.getInstance()
					.getOobAdapter().getMountPoint();

			if (mountPoint == null) {
				throw new IOException("getMountPoint() returns null.");
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("canon path="
						+ mountPoint.getCanonicalPath());
			}

			DSMCCObject dsmcc = new DSMCCObject(mountPoint, DATA_OOB + "/"
					+ fileName);
			dsmcc.setRetrievalMode(DSMCCObject.FROM_STREAM_ONLY);
			dsmcc.synchronousLoad();

			if (Log.DEBUG_ON) {
				Log.printDebug("Loads " + fileName + " succesfully.");
			}

			return dsmcc;
		}


		/**
		 * Should not be called.
		 * 
		 * @param fileName
		 *            - file name.
		 */
		public void dataRemoved(String fileName) {
		}


		/**
		 * Invoked when a version file on OC is updated.
		 * 
		 * @param fileName
		 *            - file name
		 * @param oldFile
		 *            - old File instance.
		 * @param newFile
		 *            - new File instance.
		 */
		public void dataUpdated(String fileName, Object oldFile, Object newFile) {
			if (Log.INFO_ON) {
				Log.printDebug("Entering OcAdapter.dataUpdate().");
			}
			onOcUpdate((File) newFile);
		}
	}
	
	/**
     * Starts to listen a vbm config file.
     */
	public synchronized void start() {
		if (Log.INFO_ON) {
			Log.printInfo("started VbmConfigDispatcher thread.");
		}
		ocAdapter.startToListen();
		
		// check configurations in case configurations are delivered earlier than launching application.
		// check async update
		// 2013.02.07
		
		//if(notInitialized){
		//	try{
		//		scheduler.execute(new DataUpdateNotificationTask(ocAdapter.getVbmBroadcastingFile(MEMBER_FILE)));
		//		scheduler.execute(new DataUpdateNotificationTask(ocAdapter.getVbmBroadcastingFile(CONFIG_FILE)));
		//	}catch (IOException e){
		//		Log.print(e);
		//	}
		//	
		//	notInitialized = false;
		//}
		
	}
	
	/**
     * Stops to listen a syslogserver config file.
     */
	public void stop() {
		ocAdapter.stopToListen();
	}

	/**
	 * Initiates an instance.
	 * 
	 * @param sch
	 *            - scheduler instance.
	 */
	public synchronized void setScheduler(Scheduler sch) {
		if (sch == null) {
			throw new IllegalArgumentException("Argument should not be null.");
		}
		scheduler = sch;
	}

	/**
	 * @param configFile
	 *            - the updated syslogserver config file.
	 */
	private synchronized void onOcUpdate(File ocFile) {
		if (Log.DEBUG_ON) {
			Log.printDebug("OC Updated in vbmconfigdispatcher . " + ocFile);
		}
		scheduler.execute(new DataUpdateNotificationTask(ocFile));
		
	}
	
	

	/**
     * Implements Task interface to call notifyDataUpdate().
     */
	private class DataUpdateNotificationTask implements Task {

		/** configFile. */
		private final File configFile;

		/**
		 * @param aConfigFile
		 *            - updated syslogserver config file.
		 */
		public DataUpdateNotificationTask(final File configFile) {
			this.configFile = configFile;
		}

		/**
		 * Invokes notifyDataUpdate().
		 */
		public void run() {
			if (Log.DEBUG_ON) {
				Log.printDebug("run dataupdatenotificationtask . " + configFile.getName());
			}
			
			try {
				notifyDataUpdate(configFile);
			} catch (IOException e) {
				if (Log.ERROR_ON) {
					Log.print(e);
				}
			}
		}
		
		/**
         * Returns the task name.
         * @return the string "DataUpdateNotificationTask".
         */
		public String getName() {
			return "DataUpdateNotificationTask";
		}
	};
	
	/**
	 * Adds a listener to the list of listeners.
	 * 
	 * @param listener
	 *            - a listener to be added.
	 */
	public synchronized void addListener(VbmConfigDataUpdateListener listener) {
		listeners.add(listener);
	}

	/**
	 * Removes a listener from the list of listeners.
	 * 
	 * @param listener
	 *            - a listener to be removed.
	 */
	public synchronized void removeListener(
			VbmConfigDataUpdateListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Notifies the listeners of a data update event.
	 * 
	 * @param configFile
	 *            - sysLogServer config file.
	 * @throws IOException
	 *             if fails to read a version from OOB-OC.
	 */
	private void notifyDataUpdate(File configFile) throws IOException {
		
		if ((configFile == null) || (configFile.getName() == null)) {
			if(Log.DEBUG_ON){
				Log.printDebug("VBM : config file in notifyDataUpdate is null.");
			}
			return;
		}
		
		InputStream is = new FileInputStream(configFile);

		if (Log.DEBUG_ON) {
			Log.printDebug(configFile.getName() + " file is loading from OC. " + configFile);
		}
		
		try {
			if(configFile.getName().equals(CONFIG_FILE)) {
				ConfigurationManager.getInstance().buildConfiguration(is);
			} else if(configFile.getName().equals(MEMBER_FILE)){
				ConfigurationManager.getInstance().buildTarget(is);
			} else {
				if(Log.DEBUG_ON){
					Log.printDebug("VBM : config file in notifyDataUpdate is not config nor member.");
				}
			}
		}catch(Exception e){
			if (Log.DEBUG_ON) {
				Log.printDebug("Exception when reading configuration file in notifyDataUpdate ");
				Log.printDebug(e.getMessage());
				
			}
		} finally {
			is.close();
		}


	}

}

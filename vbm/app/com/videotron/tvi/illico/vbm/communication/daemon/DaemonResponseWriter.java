package com.videotron.tvi.illico.vbm.communication.daemon;

import java.io.IOException;
import java.util.List;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.util.VbmCode;
/**
 * @author thorn
 */
public final class DaemonResponseWriter {
	/**
	 * Constructs a instance.
	 */
	private DaemonResponseWriter() {
	}
	
	/**
	 * Write a entity of response to byte array.
	 * @param response - daemon response
	 * @return byte array
	 * @throws IOException - I/O error occurs.
	 */
	protected static byte[] writeEntity(final DaemonResponse response) throws IOException {
		StringBuffer buffer = new StringBuffer();
		buffer.append("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>");
		buffer.append("<vbm><response>");
		//<resultCode=>").append(response.getResultCode()).append("</resultCode>");
		
		List list = response.getContents();		
		for (int i = 0, size = list.size(); i < size; i++) {
			buffer.append(list.get(i).toString());
		}
		if(list.size() == 0){
			
			if(response.getResultCode() == VbmCode.RETURN_SUCCESS){
				buffer.append("<result>SUCCESS</result>");
			} else if (response.getResultCode() == VbmCode.RETURN_FAILURE) {
				buffer.append("<result>FAIL</result>");
			} else {
				buffer.append("<result>PARTILA</result>");
				buffer.append("<message>Request Session Key is not processed</message>");
			}
		}
		buffer.append("</response></vbm>");
		
		if (Log.DEBUG_ON) {
			Log.printDebug(buffer.toString());
		}
		return buffer.toString().getBytes();
	}
	/**
	 * 
	 * @param response - write response to byte array.
	 * @return byte array.
	 * @throws IOException - I/O error occurs.
	 */
	public static byte[] write(DaemonResponse response) throws IOException {
		if (Log.DEBUG_ON) {
			Log.printDebug("byte[] write(DaemonResponse response)");
		}
		return writeEntity(response);
	}
}

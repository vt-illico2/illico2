/*
 *  DataUpdateEvent.java    $Revision: 1.1 $ $Date: 2014/04/04 04:45:53 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.vbm.communication;

import com.videotron.tvi.illico.vbm.config.Configuration;

/**
 * @author sccu
 * @version v 1.0, $Revision: 1.1 $
 * @since sccu, 2010. 1. 25.
 */
public class VbmConfigDataUpdateEvent {
	/** a VBM Config instance. */
	private final Configuration config;
	
    /**
     * Constructs a instance. 
     * configuration can be one of general_configuration or target list
     * 
     * @param aConfig - a Config instance related this VBM DataUpdate event.
     */
    public VbmConfigDataUpdateEvent(final Configuration aConfig) {
    	this.config = aConfig;
    }
    
    /**
     * Returns a Config instance.
     * @return a Config instance.
     */
    public Configuration getConfiguration() {
        return config;
    }
    
}

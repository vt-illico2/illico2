package com.videotron.tvi.illico.vbm.communication.daemon;


import java.util.ArrayList;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.controller.DaemonController;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/**
 * method called by LogLevelController.
 * @author thorn
 *
 */
public class DaemonProcessor {
	

	/**
	 * @param request - the request from daemon
	 * @param response - the response to daemon
	 */
	public void measure(DaemonRequest request, DaemonResponse response) {
		
		if (Log.DEBUG_ON) {
			Log.printDebug("called uploadLog");
		}
		ArrayList aList = new ArrayList();
		String rsKey = null;
		try {
			//set
			rsKey = request.getParameter(VbmCode.JSON_RSKEY);
			if (Log.DEBUG_ON) {
				Log.printDebug("input reKey = " + rsKey);				
			}
			int ret = (new DaemonController()).processUpload(rsKey);
			if (Log.DEBUG_ON) {
				Log.printDebug("changed log level with return " + ret);				
			}
			
			aList.add("<rsKey>" +rsKey + "</rsKey>");
			
			if (ret == VbmCode.RETURN_SUCCESS) {
				aList.add("<result>SUCCESS</result>");
			} else if (ret == VbmCode.RETURN_FAILURE) {
				aList.add("<result>FAIL</result>");
			} else {
				aList.add("<result>PARTIAL</result>");
				aList.add("<message>Partial bufffer submitted.</message>");
			}			
			response.setContents(aList);
			// notify complete
			response.setResultCode(ret);
			
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log.printDebug("Exception occurred when changing log level");				
			}
			

			aList.add("<rsKey>" +rsKey + "</rsKey>");
			aList.add("<result>FAIL</result>");
			
			response.setResultCode(VbmCode.DIRECT_UPLOAD_FAILED);
		}
	}
	
	/*
	
	public static void main(String args[]){
		ArrayList aList = new ArrayList();
		
		aList.add("test");
		
		System.out.println(aList.get(0).toString());
		
	}
	*/
}

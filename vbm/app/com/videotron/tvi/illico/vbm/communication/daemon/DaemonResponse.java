package com.videotron.tvi.illico.vbm.communication.daemon;

import java.util.ArrayList;
import java.util.List;

import com.videotron.tvi.illico.log.Log;

/**
 * if resultCode is setting, then this object contents will be sended.
 * @author thorn
 *
 */
public class DaemonResponse {
	/** list instance. */
	private List list;
	
	/** ready boolean. */
	private boolean ready = false;
	
	/** Constructs DaemonResponse. */
	public DaemonResponse() {
		list = new ArrayList();
	}
	/**
	 * Add all contents.
	 * @param aList - contents list
	 */
	public void setContents(List aList) {
		this.list = aList;
	}
	
	/**
	 * Add a content to contents list.
	 * @param contents - contents object
	 */
	public void setContent(Object contents) {
		list.add(contents);
	}
	
	/**
	 * Return contents list.
	 * @return contents list
	 */
	public List getContents() {
		return this.list;
	}
	
	/**
	 * response result code.
	 */
	private int resultCode = 0;
	
	/**
	 * Return result code of response.
	 * @return result code
	 */
	public synchronized int getResultCode() {
		while (!ready) {
			try {
				wait();
			} catch (InterruptedException e) {
				if (Log.ERROR_ON) {
					Log.print(e);
				}
			}
		}
		return resultCode;
	}
	
	/**
	 * Stores a result code.
	 * @param aResultCode - result code of response.
	 */
	public synchronized void setResultCode(int aResultCode) {
		if (ready) { 
			return;
		}
		this.resultCode = aResultCode;
		ready = true;
		notifyAll();
	}
}

/*  StcServiceImpl.java    $Revision: 1.1 $ $Date: 2014/04/04 04:45:53 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vbm.communication;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;


import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.config.Measurement;
import com.videotron.tvi.illico.vbm.util.StringUtils;

/**
 * 
 * @author thorn
 * 
 */
public class VbmServiceImpl implements VbmService {
	/** a list of listeners. */
	private static Map listeners = null;

	/**
	 * Constructs a instance.
	 */
	public VbmServiceImpl() {
		listeners = new HashMap();
	}
	/**
	 * Assert that appName has actual text. If it does not an IllegalArgumentException is thrown.
	 * @param appName - application name
	 */
	protected void assertAppName(String appName) {
		if (!StringUtils.hasText(appName)) {
			throw new IllegalArgumentException("appName should not be empty.");
		}
	}

	/**
	 * Assert that measurement is valid. If it does not an IllegalArgumentException is thrown.
	 * @param appName - application name
	 */
	protected void assertMeasurement(Measurement measurement) {
		if (measurement == null) {
			throw new IllegalArgumentException("measurement should not be empty.");
		}
	}
	
	/**
	 * Asserts that a listener isn't null. If it is an IllegalArgumentException is thrown. 
	 * @param listener - a listener to check or null
	 */
	protected void assertListener(LogCommandTriggerListener listener) {
		if (listener == null) {
			throw new IllegalArgumentException("Listener should not be null.");
		}
	}

	/***
	 * add listener to get measurement list for target application
	 * 
	 * @param appName - application name 
	 * @param listner - listener of target application
	 * 
	 * @throws RemoteException 
	 */
	public synchronized void addLogCommandChangeListener(String appName,
			LogCommandTriggerListener listener) throws RemoteException {

		assertAppName(appName);
		assertListener(listener);
		listeners.put(appName, listener);
		if (Log.INFO_ON) {
			Log.printInfo("called addLogLevelChangeListener("
					+ appName + "," + listener.getClass() + ")");
		}
	}
	
	/**
     * Removes a listener from the list of listeners.
     * @param appName - app name of a listener to be removed.
     * @throws RemoteException - occur during the execution of a remote method call
     */
	public synchronized void removeLogCommandChangeListener(String appName)
			throws RemoteException {

		assertAppName(appName);
		listeners.remove(appName);
		if (Log.INFO_ON) {
			Log.printInfo("called removeLogLevelChangeListener("
					+ appName + ")");
		}
	}
	
	/**
	 * Removes all of the listeners from listeners map. 
	 */
	public synchronized void removeLogCommandChangeListener() {
		if (Log.INFO_ON) {
			Log.printInfo("called removeLogLevelChangeListener()");
		}
		listeners.clear();
	}
	
	/**
	 * 
	 * implement changes of log command in the interface
	 * 
	 * 
	 * @param appName - application name
	 * @param measurementId - target measurement id
	 * @param command - command to set or unset measurement
	 * @return true if, and only if, any exception does not occurs.
	 */
	public synchronized boolean notifyLogCommandChange(String appName,
			long measurementId, boolean command ) {

		assertAppName(appName);
		//assertMeasurement(measurement);
		
		try {

			if (Log.INFO_ON) {
				Log.printInfo("notifyLogCommandChange(" + appName + "," + measurementId + "," + command + ")");
			}
			
			LogCommandTriggerListener listener = (LogCommandTriggerListener) listeners.get(appName);
			// for debug, please uncomment out this
			this.assertListener(listener);
			//if(listener == null){
			//	System.out.println(" VBM : listner should not be null");
			//	return false;
			//}
			listener.logCommandChanged(measurementId, command);
		} catch (Exception e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
			e.printStackTrace();
			return false;
		}
		return true;
	}

	

	/**
	 * 
	 * implement trigger of log gathering for an application in the interface
	 * 
	 * 
	 * @param appName - application name
	 * @param measurement - measurement id to trigger 
	 * 
	 * @return boolean true to succeed to trigger, false to fail to trigger
	 */
	public synchronized boolean triggerLogGathering(String appName,	long measurement) {

		assertAppName(appName);
		//assertMeasurement(measurement);
		
		try {

			if (Log.INFO_ON) {
				Log.printInfo("triggerLogGathering(" + appName + "," + measurement + ")");
			}
			
			LogCommandTriggerListener listener = (LogCommandTriggerListener) listeners.get(appName);
			this.assertListener(listener);
			listener.triggerLogGathering(measurement);
		} catch (Exception e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
			//e.printStackTrace();
			return false;
		}
		return true;
	}

	
	
	/**
	 * check list of measurements for an application
	 * 
	 * @param appName application name to get list of measurements
	 * @return list of measurements applied in that application
	 */
	public synchronized long [] checkLogCommand(String appName){
		
		long [] measurementIds = null;
		
		
		if(Log.DEBUG_ON){
			Log.printDebug(" checkLogCommand(" + appName + ")");
		}
		
		try{
			measurementIds =  ConfigurationManager.getInstance().getMeasurementIdsInConfigWithApp(appName);
			
			if(measurementIds == null) {
				if(Log.DEBUG_ON){
					Log.printDebug("  VBM : checkLogCommand " + appName +  "'s measurement, but there is nothing matched.");
				}
			} else {
				if(Log.DEBUG_ON){
					Log.printDebug("  VBM : checkLogCommand(" + appName +  ")  has " + measurementIds.length + " measurements ");
					for(int i=0; i<measurementIds.length; i++){
						Log.printDebug(" mesurementId :  " + i  + " " + measurementIds[i]);
					}
				}
			}
		}catch (RemoteException e){
			if (Log.ERROR_ON) {
				Log.print(e);
			}
		}
		return measurementIds;		
		
	}
	
}

/*
 *  CommunicationRequestDispatcher.java    $Revision: 1.1 $ $Date: 2014/04/04 04:45:53 $
 *
 *  Copyright (c) 2001-2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.vbm.communication;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.IxcLookupWorker;
import com.videotron.tvi.illico.vbm.communication.daemon.DaemonRequestDispatcher;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.videotron.tvi.illico.vbm.util.concurrent.Scheduler;
/**
 * 
 * @author thorn
 * 
 */
public class CommunicationRequestDispatcher {

	/** xlet context. */
	private XletContext xletContext;

	/** Scheduler instance. */
	private Scheduler scheduler;
	/** VbmServiceImpl instance. */
	private VbmServiceImpl vbmService;
	/** DaemonService instance. */
	private DaemonService daemonService;
	/** MonitorService instance. */
	private MonitorService monitorService;
	/** DaemonRequestDispatcher instance. */
	private DaemonRequestDispatcher daemonRequestDispatcher;


	/**
	 * Sets an XletContext instance.
	 * 
	 * @param context
	 *            - an XletContext instance.
	 */
	public synchronized void setXletContext(XletContext context) {
		xletContext = context;
	}

	/**
	 * Initiates an instance.
	 * 
	 * @param sch
	 *            - scheduler instance.
	 */
	public synchronized void setScheduler(Scheduler sch) {
		if (sch == null) {
			throw new IllegalArgumentException("Argument should not be null.");
		}
		scheduler = sch;
	}

	/**
	 * start interface of VBM.
	 */
	public synchronized void start() {
		if (xletContext == null) {
			throw new IllegalStateException("XletContext should not be null.");
		}
		
		new Thread("IXC_Lookup_Thread") {

			//IxcLookupWorker lookupWorker;
			
			public void run() {
				while (true) {
					try {
						
						//lookupWorker = new IxcLookupWorker(xletContext);
						//lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME);

						if (monitorService == null) {
							monitorService = (MonitorService) IxcRegistry
									.lookup(xletContext, "/1/1/" + MonitorService.IXC_NAME);
							DataCenter.getInstance().put(MonitorService.IXC_NAME, monitorService);
							if (Log.DEBUG_ON) {
								Log.printDebug("MonitorService lookuped");
							}														
						}
					} catch (Exception e) {
						if (Log.ERROR_ON) {
							Log.printError("MonitorService faile to lookup");
							Log.print(e);
						}
					}

					try {
						if (daemonService == null) {
							daemonService = (DaemonService) IxcRegistry.lookup( xletContext, "/1/2040/"+ DaemonService.IXC_NAME);
							DataCenter.getInstance().put(DaemonService.IXC_NAME, daemonService);
							if (Log.DEBUG_ON) {
								Log.printDebug("DaemonService lookuped");
							}

							daemonRequestDispatcher = new DaemonRequestDispatcher();
							daemonRequestDispatcher.setScheduler(scheduler);
							daemonRequestDispatcher.init(VbmCode.DAEMON_PROC);
							daemonService.addListener(VbmService.IXC_NAME, daemonRequestDispatcher);
						}
					} catch (Exception e) {
						if (Log.ERROR_ON) {
							Log.printError("DaemonService faile to lookup");
							Log.print(e);
						}
					}
					

					/**
					 * ixc interface for vbm should be released after configuration is acquired.
					 */
					/*
					if(ConfigurationManager.getInstance().isInitialized()) {

						// bind
						vbmService = new VbmServiceImpl();
		
						IxcRegistry.rebind(xletContext, VbmService.IXC_NAME, vbmService);
						if(Log.DEBUG_ON) {
							Log.printDebug("VBM : CommunicationRequestDispatcher : bound to ixc");
						}
						DataCenter.getInstance().put(VbmService.IXC_NAME, vbmService);
		
						if(Log.DEBUG_ON) {
							Log.printDebug("VBM :CommunicationRequestDispatcher :  put vbm ixc into datacenter");
						}
					} else {
						
						if (Log.ERROR_ON) {
							Log.printError("CommunicationRequestDispatcher : Configuration is not initialized. After configuration is initialized, then shared ixc interface.");
						}											
					}
					
					
					if (daemonService != null  && monitorService != null && ConfigurationManager.getInstance().isInitialized()) {
						break;
					}
*/

					// bind
					vbmService = new VbmServiceImpl();
	
					IxcRegistry.rebind(xletContext, VbmService.IXC_NAME, vbmService);
					if(Log.DEBUG_ON) {
						Log.printDebug("VBM : CommunicationRequestDispatcher : bound to ixc");
					}
					DataCenter.getInstance().put(VbmService.IXC_NAME, vbmService);
	
					if(Log.DEBUG_ON) {
						Log.printDebug("VBM :CommunicationRequestDispatcher :  put vbm ixc into datacenter");
					}

					if (daemonService != null  && monitorService != null ) {
						break;
					}
					
					try {
						Thread.sleep(VbmCode.RETRY_INTERVAL);
					} catch (InterruptedException e) {
						if (Log.ERROR_ON) {
							Log.print(e);
						}
					}
				}	// while			
				
			}
		} .start(); // thread start
	}
	/**
	 * destroy.
	 */
	public void destroy() {
		if (xletContext == null) {
			throw new IllegalStateException("XletContext should not be null.");
		}
		try {
			IxcRegistry.unbind(xletContext, VbmService.IXC_NAME);

			if (daemonService != null) {
				daemonService.removeListener(VbmService.IXC_NAME);
			}
			
			
		} catch (Exception e) {
			if (Log.ERROR_ON) {
				Log.print(e);
			}
		}
	}
	
}

package com.videotron.tvi.illico.vbm.controller.buffer;


import java.util.Map;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.uploader.LogUploader;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/**
 * 
 * upload buffer to temporarily store logs.
 * it puts buffers into uploader after.
 * 
 * @author swkim
 *
 */
public class UploadBuffer {

	// change UploadBuffer from static into normal
	// because for each instance upload buffer has a different object
	// uploading of logs do not retry not keep data temporarily
	// thus it tries to upload and if it fails then throws data.
	// that means if it failed, then this object is removed from memory.
	// so when it is invoked, it creates uploader, put buffer into uploader and execute upload method in uploader
	// after that, it is removed.

	/** data store of log type **/
	private int type = 0;
	/** data store of single log buffer **/
	private Map [] buffer = null;
	/** data store of multiple log buffers **/
	private Vector buffers = null;
	/** data store of request session key **/
	String rsKey = null;
	
	
	
	/**
	 * constructor.
	 * set uploading buffer with owner application name.
	 * @param applicationName upload buffer's owner application name.
	 */
	public UploadBuffer(Map [] buffer, String rsKey) {

		type = VbmCode.ONE_BUFFER;
		
		//if (Log.DEBUG_ON) {
    	//	Log.printDebug("created new UploadBuffer to upload single buffer " + Misc.checkNull(rsKey));
    	//}
		if (Log.DEBUG_ON) {
			Log.printDebug(" UploadBuffer : created new UploadBuffer to upload single buffer " + Misc.checkNull(rsKey));
		}
		this.buffer = buffer;
		this.rsKey = rsKey;
		
	}

	/***
	 * constructor with multple buffers 
	 * 
	 * @param buffers data store of multiple buffers 
	 * @param rsKey request session key
	 */
	public UploadBuffer(Vector buffers, String rsKey) {

		
		type = VbmCode.MULTI_BUFFER;
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("UploadBuffer : created new UploadBuffer to upload multiple buffer " +  Misc.checkNull(rsKey));
    	}
		
		this.buffers = buffers;
		this.rsKey = rsKey;
		
	}
	
	
	/**
	 * upload current buffer into uploader.
	 * @return result of uploading 
	 */
	public synchronized boolean upload() {
	
		
		if (Log.DEBUG_ON) {
			Log.printDebug("UploadBuffer : start to upload bufer");
		}
		
		try {
			if(type == VbmCode.ONE_BUFFER) {
		
				if (buffer == null) {
					if (Log.DEBUG_ON) {
			    		Log.printError("UploadBuffer : buffer is null. so do not upload");
			    	}
					return true;
				}				
				
				(new LogUploader(0, buffer, rsKey)).start();
			}
			else if (type == VbmCode.MULTI_BUFFER) {
				(new LogUploader(0, buffers, rsKey)).start();
			}
			
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log.printDebug("UploadBuffer : error uploading buffer." + e.getMessage());
	    	}
			return false;
		}
		
		return true;
	}
	
	
	
}

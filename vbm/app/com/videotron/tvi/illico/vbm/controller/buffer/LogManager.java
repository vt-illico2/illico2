package com.videotron.tvi.illico.vbm.controller.buffer;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.util.VbmCode;
import com.alticast.util.SharedMemory;
import java.util.Hashtable;

/**
 * manager who manages application's buffer in STB. LogManager creates
 * buffer for each applications and manages upload buffer to send logs into
 * syslog server
 * 
 * 
 * @author swkim
 * 
 */
public class LogManager {

	
	/***
	 * initialize log manager
	 */
	public void initialize() {

		if (Log.DEBUG_ON) {
			Log.printError("LogManager : initialized LogManager.");
		}

		Hashtable sm = (Hashtable) SharedMemory.getInstance();

		int buffSize = ConfigurationManager.getInstance().getConfiguration().getBufferSize();

		if(buffSize == VbmCode.INIT_VALUE) {
			buffSize = VbmCode.BUFFER_INIT_VALUE;
		}
		
		VbmLog logCache = (VbmLog) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		if ((logCache == null) || (logCache.size() == 0)) {

			logCache = new VbmLog(buffSize);
			sm.put(VbmCode.NAME_IN_SHARED_MEMORY, logCache);
		}

	}

	// this method should be called after checking configuration and schedule.
	// in this method, it does not check configuration, schedule and
	// availability

	/***
	 * upload logs of log type
	 * 
	 * @param type log type
	 */
	public synchronized void upload(int type) {

		Hashtable sm = (Hashtable) SharedMemory.getInstance();

		VbmLog logCache = (VbmLog) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		if (logCache == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("log cache is not initialized. So do not upload.");
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("cleared and started to upload log cache of type "
								+ type);
			}

			logCache.removeElementAt(type);
		}

	}

	/***
	 * upload all logs in all log buffer with request session key
	 * 
	 * @param rsKey - request session key
	 * @return result of uploading
	 */
	public synchronized boolean uploadAll(String rsKey) {

		Hashtable sm = (Hashtable) SharedMemory.getInstance();

		VbmLog logCache = (VbmLog) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
		if (logCache == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("cleared and started to upload log cache");
			}
			return false;
		}

		logCache.setElementAt(rsKey, 0);
		logCache.removeElementAt(VbmCode.ALL_LOG_CACHE);
		
		return true;
	}
}

package com.videotron.tvi.illico.vbm.controller.buffer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;


import org.ocap.dvr.storage.MediaStorageVolume;

import org.ocap.storage.LogicalStorageVolume;
import org.ocap.storage.StorageManager;
import org.ocap.storage.StorageProxy;
import org.json.simple.JSONObject;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/***
 * LogFileManager to manage log files in HDD
 * 
 * @author swkim
 *
 */
public class LogFileManager {
	
	//private static StorageManager storageManager;
	/** static instance of log file manager **/
	private static LogFileManager instance = null;
	
	/***
	 * method to get static instance of log file manager.
	 * @return static instance of log file manager
	 */
	public static LogFileManager getInstance(){
		if(instance == null){
			instance = new LogFileManager();
		}
		
		return instance;
	}

	
	/***
	 * destroy log file manager instance
	 */
	public void destroy() {
		//storageManager = null;

	}

	
	/***
	 * check whether this STB has HDD or not
	 * it checks ocap property and return true only property is 1.0
	 * @return value of ocap property
	 */
	public String checkHDD() {
		if(Log.DEBUG_ON){
			Log.printDebug(" LogFileManager : get system property ocap.api.option.dvr = " + System.getProperty("ocap.api.option.dvr"));
		}
		return System.getProperty("ocap.api.option.dvr");
	}

	
	/***
	 * method to get logs stored in HDD
	 * @param logType type of log, 0 for daily, 1 for weekly, 2 for monthly
	 * @return data stored in HDD
	 */
	public synchronized String readLog(int logType){


		String filePath = getLogicalStorageVolume() + VbmCode.LOG_FILE_PATH;
		
		if(Log.DEBUG_ON){
			Log.printDebug(" LogFileManager : read logs from HDD in LogFileManager of logType = " + logType + ", path = " + filePath);
		}
		
	
		return readFromHDD(filePath, getFileName(logType));
	}
	
	
	/***
	 * write logs into HDD
	 * @param logType - log type
	 * @param logs - arrays of log object
	 * @return result of writing logs 
	 */
	public synchronized boolean writeLog(int logType, Map [] logs ){
		
		
		if(Log.DEBUG_ON){
			Log.printDebug("wrote logs in disk in LogFileManager. type = " + logType);
		}
		
		String filePath = getLogicalStorageVolume() + VbmCode.LOG_FILE_PATH;
		String logString = processLogs(logs);
		
		if(logString == null) {
			if(Log.DEBUG_ON){
				Log.printDebug(" do not write because log string is null.");
			}
			return true;  // return true because input logs are null, so it does not process.
		} else {
			if(Log.DEBUG_ON){
				Log.printDebug(" wrote logs in disk in VbmLog. length of log string = " + logString.length());
			}
		}
		
		if(logString.length() > VbmCode.MAX_FILE_SIZE) {
			
			if(Log.DEBUG_ON){
				Log.printDebug(" LogFileManager : size of log file exceeds. So upload logs and flush.");
			}
			Hashtable sm = (Hashtable) SharedMemory.getInstance();
			Vector vbmLogBuffer = (Vector) sm.get(VbmCode.NAME_IN_SHARED_MEMORY);
			
			// store logs in buffers into disk
			// set rsKey null
			vbmLogBuffer.setElementAt(null, VbmCode.INIT_VALUE);  // init vlaue is meaningless
			vbmLogBuffer.removeElementAt(logType);
			
			return true;
		}
		if(Log.DEBUG_ON){
			Log.printDebug("wrote logs into HDD. ");
		}
		return writeToHDD(filePath, getFileName(logType), logString);
	}


	/***
	 * return name of files with log type
	 * 
	 * @param logType - log type
	 * @return log file name
	 */
	private String getFileName(int logType) {
		String fileName = "";
		
		if (logType == VbmCode.DAILY_LOG_CACHE) {
			fileName = VbmCode.LOG_FILE_NAME_DAILY;
		} else if (logType == VbmCode.WEEKLY_LOG_CACHE) {
			fileName = VbmCode.LOG_FILE_NAME_WEEKLY;
		} else if (logType == VbmCode.MONTHLY_LOG_CACHE) {
			fileName = VbmCode.LOG_FILE_NAME_MONTHLY;
		} else {
			if(Log.DEBUG_ON){
				Log.printDebug("LogFileManager : not a valid log index " + logType);
			}
			fileName = null;
		}
		
		return fileName;
	}
	
	
	/***
	 * find logical storage volume and return path of it
	 * 
	 * @return path of LSV
	 */
	private String getLogicalStorageVolume(){

		/**
		 * TODO : recover to test in real env
		 */

		//return "d:/test";
		
		
        LogicalStorageVolume lsv = null;
        
       
		try{

	        StorageManager sm = StorageManager.getInstance();
	        if (sm == null) {
	        	if(Log.DEBUG_ON){
	    			Log.printDebug("LogFileManager :  StorageManager is null!!");
	        	}
	            return null;
	        }

	        StorageProxy[] sps = sm.getStorageProxies();
	        if (sps == null || sps.length <= 0) {
	        	if(Log.DEBUG_ON){
	    			Log.printDebug("StorageProxy array is null or its length is 0!!");
	        	}
	            return null;
	        }

	        if(Log.DEBUG_ON){
				Log.printDebug(" LogFileManager : proxies' length : " + sps.length);
	        }
	        for (int i = 0; i < sps.length; i++) {
	            LogicalStorageVolume[] lsvs = sps[i].getVolumes();
	            if (lsvs == null || lsvs.length <= 0) {
	            	if(Log.DEBUG_ON){
		    			Log.printDebug("LogFileManager : " + i + "th lsvs is null or its length is 0!!");
	            	}
	                continue;
	            }

	            System.out.println(" VBM : " + i + "th lsvs length : " + lsvs.length);
	            for (int j = 0; j < lsvs.length; j++) {
	            	if(Log.DEBUG_ON){
		    			Log.printDebug("LogFileManager : " + j + "th lsv=" + lsvs[j] + ", path=" + lsvs[j].getPath());
	            	}
	                if (lsvs[j] == null || lsvs[j] instanceof MediaStorageVolume) {
	                    continue;
	                }
	                lsv = lsvs[j];
	                break;
	            }
	        }

	        if (lsv == null) {
	        	if(Log.DEBUG_ON){
	    			Log.printDebug("LogFileManager : no matched LogicalStorageVolume!!");
	        	}
	            return null;
	        }

	        if(Log.DEBUG_ON){
    			Log.printDebug("LogFileManager : LogicalStorageVolume=" + lsv + ", path=" + lsv.getPath());
	        }

		}catch(Exception e){
			e.printStackTrace();
		}
		
		return lsv.getPath();
		
	}
	
	/***
	 * create directory of log file's path if there is no directory
	 * 
	 * @return result of this creating
	 */
	private boolean createDirectory(){
		
		String filePath = getLogicalStorageVolume();
		
		if(filePath == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" VBM : filePath is null. So do not create directory");
			}
	        return false;
		}

		String tmpString = VbmCode.LOG_FILE_PATH.substring(1, VbmCode.LOG_FILE_PATH.length());
		
		int idx = 1;
		boolean created = false;
		try{
			while((idx = tmpString.indexOf('/')) >0 ){
				filePath += "/" + tmpString.substring(0, idx);
				tmpString = tmpString.substring(idx+1, tmpString.length());
				//System.out.println(filePath);
				created =(new File(filePath)).mkdir();
				if(!created) {
					if (Log.DEBUG_ON) {
						Log.printDebug("cannot craeted " + filePath);
					}
					return false;
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug(" created " + filePath);
					}
				}
			}
		}catch(Exception e){
			
		}
		return true;
	}
	
	
	/***
	 * write log data into HDD
	 * 
	 * @param filePath - log file path
	 * @param fileName - log file name
	 * @param logString - data to be written in log file 
	 * @return
	 */
	private boolean writeToHDD(String filePath, String fileName, String logString) {

		if (Log.DEBUG_ON) {
			Log.printDebug(" wrote logs into hdd with logString " + logString + " into " + filePath + fileName);
		}
		
		try{

	        File file = new File(filePath);
	        if (!file.exists()) {
	        	if (Log.DEBUG_ON) {
	    			Log.printDebug("  VBM : " + filePath + " does not exists. so create!!");
	        	}
	            if(!createDirectory()){
	            	if (Log.DEBUG_ON) {
	        			Log.printDebug(" VBM : did not created directory. return");
	            	}
	            }
	        }
            if (Log.DEBUG_ON) {
    			Log.printDebug(" starting to write logs into HDD");
            }
			BufferedWriter bw = null;
	        try {
	            bw = new BufferedWriter( new OutputStreamWriter( new FileOutputStream( filePath + fileName)));
	            bw.write(logString, 0, logString.length());
	            
	            
	            if (Log.DEBUG_ON) {
	    			Log.printDebug(" succeeded writing logs into HDD. " + filePath + fileName);
	            }
	        } catch (Throwable t) {
	        	if (Log.DEBUG_ON) {
	    			Log.printDebug("  error writing logs into HDD. " + filePath + fileName);
	        	}
	            t.printStackTrace();
	        } finally {
	            try {
	                if (bw != null) {
	                    bw.flush();
	                    bw.close();
	                }
	            } catch (IOException ioe) {
	                ioe.printStackTrace();
	                
	                return false;
	            }
	        }
		}catch(Exception e){
			if (Log.DEBUG_ON) {
				Log.printDebug(e.getMessage());
			}
			return false;
		}
		
		return true;
	}

	/**
	 * read data stored in HDD
	 * 
	 * @param filePath - log file path
	 * @param fileName - log file name 
	 */
	private String readFromHDD(String filePath, String fileName) {
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" readFromFile " + filePath + fileName);
		}
		String fileContent = "";
		try {

			if(!(new File(filePath + fileName).exists())) {
				if (Log.DEBUG_ON) {
					Log.printDebug(" file is not exist. so do not read. " + filePath + fileName);
				}
				return null;
			}
			FileInputStream fstream = new FileInputStream(filePath + fileName);

			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;

			while ((strLine = br.readLine()) != null) {
				fileContent += strLine;
			}
			in.close();
		} catch (Exception e) {
			if (Log.DEBUG_ON) {
				Log.printDebug("  VBM : " + e.getMessage());
			}
			e.printStackTrace();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug(" data read From File " + fileContent);
		}
		return fileContent;
	}
		
	

	
	// this code is very similar to code in LogUploader.
	// so when change this code, check LogUploader, too.
	
	/***
	 * convert log object into log string
	 * @param logs - log object to be stored 
	 * 
	 * @return log string converted from logs object
	 */
	private String processLogs(Map [] logs){
				
		String mac = Misc.getMacAddress();
		// for test
		//String mac = "123456789012";
		String rsKey = null;

		// this code is used also to store logs
		// check LogFileManager, too
		if(mac == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" mac is not set. so do not process log buffers");
			}
			return null;
		}
		
		if((logs == null) || (logs.length == 0)) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" LogFileManager : logs are null. so do not process log buffers");
			}
			return null;
		}
		
		HashMap logList = new HashMap();
		
		logList.put(VbmCode.JSON_HOSTMAC, mac);
		
		// rsKey is null
		logList.put(VbmCode.JSON_RSKEY, new Long(VbmCode.UPLOAD_NONE));			
				
		ArrayList arrays = new ArrayList();
		for(int i=0; i<logs.length; i++){
			if(logs[i] != null) {
				arrays.add(logs[i]);
			}
		}
		
		//logList.put(VbmCode.JSON_LOG,  (ArrayList) Arrays.asList(logs));
		logList.put(VbmCode.JSON_LOG, arrays);
		
		return JSONObject.toJSONString(logList);
	}

	/***
	 * clear data in log file and reset log file
	 * 
	 * @param index index of log 
	 */
	public synchronized void clearLogFile(int index){
		

		String checkDiskModel = checkHDD();
		
		if(checkDiskModel == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogFileManager : internal disk is not available. so do not store logs into disk.");
			}
			return;
		}
		
		if( !checkDiskModel.equals(VbmCode.DISK_EXIST)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("LogFileManager : this model is not valid STB model. Model is " + checkDiskModel);
			}
			return;
		}
		
		String filePath = getLogicalStorageVolume() + VbmCode.LOG_FILE_PATH;
		if (Log.DEBUG_ON) {
			Log.printDebug(" LogFileManager : clearing log files of " + filePath + getFileName(index));
		}
		writeToHDD(filePath, getFileName(index), "");
	}
	
	
}

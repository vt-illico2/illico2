package com.videotron.tvi.illico.vbm.controller.buffer;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
//import com.videotron.tvi.illico.stc.uploader.LogServerConfig;
import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.config.Measurement;
import com.videotron.tvi.illico.vbm.util.VbmCode;

import java.util.LinkedList;
import java.util.Vector;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Map;

/***
 * object used in shared memory
 * 
 * @author swkim
 *
 */
public class VbmLog extends Vector {

	
	/**
	 * LogCache extends Vector so it should have a UID.
	 */
	  private static final long serialVersionUID = -2767605614048989438L;
	
	
	/**
	 * collecting buffer to store String arrays.
	 */
	private Map [][] buffers = null;
	
	/**
	 * log count.
	 */
	private int [] bufferCount;
	/**
	 * size of buffer.
	 */
	private int bufferSize = 0;
	/**
	 * target application name.
	 */
	private String appName = null;
	/**
	 * upload buffer to transfer to uploader.
	 */
	private UploadBuffer uploadBuffer = null;
	
	/**
	 * request session key to be used to synchronized with server request
	 */
	private String rsKey = null;
	
	/**
	 * actually it is not used, because default buffer size not defined.
	 * to use this creator, call set size and set appName with contains method after that. 
	 * in setSize method, memory will be allocated
	 */
	
	
	public static final String KEY_LOG_COLLECT = "vbm.log.collect";
	
	//public static final String  LOG_COLLECT = DataCenter.getInstance().getString(KEY_LOG_COLLECT);
	
	
    public VbmLog() {

    	// do not create string array.
    	// to use log cache, setSize should be called to set buffer size and create string array with that.
   
    	this.bufferSize = ConfigurationManager.getInstance().getBufferSize();
    	this.buffers = new HashMap[VbmCode.BUFFER_NUMBER][];
    	this.bufferCount = new int[VbmCode.BUFFER_NUMBER];
    	
    	if (Log.DEBUG_ON) {
    		Log.printInfo("LogCache is created with buffer size " + bufferSize);
    	}

    	for(int i=0; i<VbmCode.BUFFER_NUMBER; i++) {
    		buffers[i] = new HashMap [bufferSize];
    		bufferCount[i] = 0;
    	}
    }
    
    /**
     * actual creator of this class.
     * it is defined with initial buffer size.
     * @param initialCapacity	initial size of buffer
     */
    public VbmLog(final int initialCapacity) {

    	bufferSize = initialCapacity;
    	buffers = new HashMap[VbmCode.BUFFER_NUMBER][];
    	bufferCount = new int[VbmCode.BUFFER_NUMBER];
    	
    	for(int i=0; i<VbmCode.BUFFER_NUMBER; i++) {
    		buffers[i] = new HashMap [bufferSize];
    		bufferCount[i] = 0;
    	
    	}
    	
    	if (Log.DEBUG_ON) {
    		Log.printInfo("VbmLog is created with initial buffer size " + bufferSize);
    	}
    	//System.out.println(" VBM : LogCache is created with initial buffer size " + bufferSize);
    }
    
    
    /**
     * method to flush current buffer and recreate new buffer.
     * 
     */
    private void recreateBuffer(int type) {
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug(": create buffer of type " + type + " with buffer size = " + bufferSize);
    	}
   		buffers[type] = new HashMap [bufferSize];
   		bufferCount[type] = 0;
    	
    }
    
    
    /**
     * upload collecting buffer to new uploading buffer
     * and create new memory for collecting buffer.
     */
    private void flushBuffer(int type) {
    	
    	if (Log.DEBUG_ON) {
    		Log.printDebug("flushing buffer " + type);
    	}
    	
    	// if element count is 0, then there is no logs in buffer.
    	// So just reuse it, and do not upload
    	if (bufferCount[type] == 0) {
    		if (Log.DEBUG_ON) {
				Log.printDebug(" bufferCount of " + type + " is 0. so return.");
    		}
    		return;
    	}
    	
    	// if collecting buffer is null, then it can be a problem.
    	// but anyway in this case, we regenerate collecting buffer again
    	if (buffers[type] == null) {
    		if (Log.DEBUG_ON) {
				Log.printDebug(" buffers of " + type + " is null. so return.");
    		}
    		recreateBuffer(type);
    		return;
    	}
    	
    	// if there is no logs collected, then return
    	if (buffers[type].length == 0) {
    		//recreateBuffer(type);
    		if (Log.DEBUG_ON) {
				Log.printDebug(" buffers of " + type + " is null 2. so return.");
    		}
    		return;
    	}

    	if (Log.DEBUG_ON) {
			Log.printDebug(" there are logs in VbmLog of index " + type + ", so it moves into uploading buffer with " + buffers[type].length + " logs");
    	}

    	uploadBuffer = new UploadBuffer((Map [])buffers[type].clone(), rsKey);
		
		recreateBuffer(type);
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("uploading logs in uploading buffer");
    	}

		// to test LogCache operation only, comment out below line.
		uploadBuffer.upload();
		
		// after uploading logs, it removes logs.
		LogFileManager.getInstance().clearLogFile(type);
    }
    


    /**
     * upload collecting buffer to new uploading buffer even if buffer is null
     * and create new memory for collecting buffer.
     */
    private void flushAllBuffers() {
    	
    	Vector bufferVt = new Vector();

    	bufferVt.add(buffers[VbmCode.DAILY_LOG_CACHE]);
    	bufferVt.add(buffers[VbmCode.WEEKLY_LOG_CACHE]);
    	bufferVt.add(buffers[VbmCode.MONTHLY_LOG_CACHE]);
    	
    	// upload buffer even if it is null
    	uploadBuffer = new UploadBuffer(bufferVt, rsKey);
		
		recreateBuffer(VbmCode.DAILY_LOG_CACHE);
		recreateBuffer(VbmCode.WEEKLY_LOG_CACHE);
		recreateBuffer(VbmCode.MONTHLY_LOG_CACHE);
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" flushAllbuffers : uploading all logs in uploading buffer");
		}

		uploadBuffer.upload();
		
		// after uploading logs, it removes logs.
		LogFileManager.getInstance().clearLogFile(VbmCode.DAILY_LOG_CACHE);
		LogFileManager.getInstance().clearLogFile(VbmCode.WEEKLY_LOG_CACHE);
		LogFileManager.getInstance().clearLogFile(VbmCode.MONTHLY_LOG_CACHE);
    }
    
    
    /**
     * upload collecting buffer to new uploading buffer even if buffer is null
     * and create new memory for collecting buffer.
     */
    private void flushWithNullBuffer(int type) {
    	
    	// upload buffer even if it is null
    	uploadBuffer = new UploadBuffer((Map [])buffers[type].clone(), rsKey);
		
		recreateBuffer(type);
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" uploading logs in uploading buffer");
		}

		uploadBuffer.upload();
		
		// after uploading logs, it removes logs.
		LogFileManager.getInstance().clearLogFile(type);
    }
    

    /**
     * upload buffer of index type and create new one
     * 
     * @param index index for buffer type
     * 
     */
	public synchronized void removeElementAt(int index) {
		if (Log.DEBUG_ON) {
			Log.printDebug("  VbmLog : removeElementeAt " + index);
		}
		// to upload for direct gathering, it upload buffer even if buffer is null
		// this method should be called from direct gathering only!
		
		if(index == VbmCode.ALL_LOG_CACHE) {
			if (Log.DEBUG_ON) {
				Log.printDebug("  VbmLog : upload logs in all log buffers");
			}
			
			flushAllBuffers();
		} else {
			
			flushWithNullBuffer(index);
		}
	}		
		
	
	private String encodeString(String input){
		
		if((input == null) || (input.length() == 0)) {
			return input;
		}
		
		char [] tmp = new char[input.length()];
		String out = "";
		for (int i = 0; i < input.length(); i++) {
		    char ch = input.charAt(i);
		    if (ch <= 0xFF) {
		        tmp[i] = ch;
		    }
		}
		try{
			out = new String(tmp);
		} catch(Exception e){
			if (Log.DEBUG_ON) {
				Log.printDebug("VBMLog : encodeString : error encoding String to default encoding.");
			}
			out = input;
		}
		
		return out;
		
	}
    
    /**
     * 
     * set log of a general application into collecting buffer.
     * if collecting buffer is full, then it moves collecting buffer into a uploading buffer
     * and create new memory
     * 
     * @param arg0	log string to be added in buffer
     */
	public synchronized void addElement(Object arg0) {
	/*
		if ((LOG_COLLECT == null) ||!(LOG_COLLECT.equals("t"))){
			if (Log.DEBUG_ON) {
				Log.printDebug(" flag to collect logs is false. so do not collect logs.");
			}
			return;
		}
		
		if(arg0 == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" log element is empty.");
			}
			return;
		}
		
	*/	
		
	
		
		// swkim 2012.08.20
		String input = 	encodeString((String) arg0);
		if (Log.DEBUG_ON) {
			Log.printDebug(" after encoding into ISO8859 " + input);
		}
	// MEASUREMENT_GROUP attribute is added
		StringTokenizer st = new StringTokenizer(input, VbmCode.LOG_SEPARATOR);
		int count = st.countTokens();
		String [] tmpLog = new String[count];
		System.out.println((String ) arg0 + " " + count);
		
		if(count <= VbmCode.LAST_ONE) {
			if (Log.DEBUG_ON) {
				Log.printDebug(" log element do not have measurement id nor measurement group. So discard this log ");
			}
		}
		/*
		long measurementId = VbmCode.INIT_VALUE;
		
		for(int i=0; i<count; i++){
			tmpLog[i] = st.nextToken();
			if (Log.DEBUG_ON) {
				Log.printDebug(" tmpLog " + i + " = " + tmpLog[i]);
			}
		}
		
		try {
			measurementId = Long.parseLong(tmpLog[0]);
		} catch(Exception e){
			// Log
			if(tmpLog[0] == null){
				if (Log.DEBUG_ON) {
					Log.printDebug(" measurement id is null. so discard this log");
				}
			}else {
				if (Log.DEBUG_ON) {
					Log.printDebug(" error getting from log element" + tmpLog[0]);
				}
			}
			return;
		}
		*/
		long measurementId = VbmCode.INIT_VALUE;
		long mGroupId = VbmCode.INIT_VALUE;
		try{
			measurementId = Long.parseLong(st.nextToken().trim());
			mGroupId = Long.parseLong(st.nextToken().trim());
		}catch(Exception e){
			if (Log.DEBUG_ON) {
				Log.printDebug(" error getting from log element" + tmpLog[0]);
			}
		}
		// in other value, there is old separator '|', so parse with that separator, too.
		StringTokenizer st2 = null;
		int count2 = 0;
		ArrayList valueList = new ArrayList();
		for(int i=2; i<count; i++){
			st2 = new StringTokenizer(st.nextToken(), VbmCode.OLD_SEPARATOR);
			count2 = st2.countTokens();
			for(int j=0; j<count2; j++){
				valueList.add(st2.nextToken());
			}
		}
		
		HashMap newLog = new HashMap();
		newLog.put(VbmCode.JSON_MEASUREMENT, new Long(measurementId));
		if(mGroupId != VbmCode.INIT_VALUE) {
			newLog.put(VbmCode.JSON_MEASUREMENT_GROUP, new Long(mGroupId));
		}
		newLog.put(VbmCode.JSON_VALUE, valueList);
		
		ArrayList campaigns = getCampaignList( measurementId );
		
		
		if(campaigns.size() <= 0) {
		
			if (Log.DEBUG_ON) {
				Log.printDebug(" add log : no proper campaings : " + arg0);

			}
			
			return;
		}
		
		
		// put error log into shared object
		SharedMemory sm = SharedMemory.getInstance();
		LinkedList debugLinkedList = (LinkedList) sm.get(VbmCode.DEBUG_CODE);
		
		if(debugLinkedList != null) {
			debugLinkedList.addFirst(arg0);
			if (Log.DEBUG_ON) {
					Log.printDebug(" VBM : put logs into VBM_DEBUG_LOGS : " + arg0);
	
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug(" VBM : try to put logs into VBM_DEBUG_LOGS, but shared object is null : " + arg0);

			}
		}
		
		Measurement trigger = null;
		HashMap tmpMap = null;
		
		if (Log.DEBUG_ON) {
			Log.printDebug(" VbmLog : with measurementId = " + measurementId + " , usage point id = " + 
					mGroupId + ", there are " + campaigns.size() + " of campaigns");
		}
		
		// swkim changed to add usage point in measurement 2013.10.9
		// actually I changed getCampaingList above, so this code does not chagned
		for(int i=0; i<campaigns.size(); i++){
			trigger = (Measurement) campaigns.get(i);
			// trigger is a measurement which contains cid, upid, mid
			// type is set in program. so it does not need to be checked.
			if(bufferCount[ trigger.getSendFrequency()] >= bufferSize) {			
				flushBuffer( trigger.getSendFrequency());

				for(int k=0; k<bufferCount[trigger.getSendFrequency()]; k++){
					if(buffers[trigger.getSendFrequency()][k] == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is null");
						}
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is " + buffers[trigger.getSendFrequency()][k].toString());
						}
					}
				}
			}
			
			// in case configuration is not delivered, but logs are exceed buffer, then increase buffer.
			if(bufferCount[ trigger.getSendFrequency()] > buffers[trigger.getSendFrequency()].length) {			
				flushBuffer( trigger.getSendFrequency());
				if (Log.DEBUG_ON) {
					Log.printDebug(" flushBuffer : flushed. after then count of buffer = " + bufferCount[trigger.getSendFrequency()]);
				}
				for(int k=0; k<bufferCount[trigger.getSendFrequency()]; k++){
					if(buffers[trigger.getSendFrequency()][k] == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is null");
						}
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is " + buffers[trigger.getSendFrequency()][k].toString());
						}
					}
				}
			}

			tmpMap = (HashMap) newLog.clone();
			tmpMap.put(VbmCode.JSON_CAMPAIGN_ID, Long.toString(trigger.getCampaignId()));
			tmpMap.put(VbmCode.JSON_USAGE_POINT_ID, Long.toString(trigger.getUsagePointId()));
			tmpMap.put(VbmCode.JSON_CAMPAIGN_VERSION, Integer.toString(trigger.getCampaignVersion()));
			tmpMap.put(VbmCode.JSON_TIME, new Long(Calendar.getInstance().getTimeInMillis()));
			tmpMap.put(VbmCode.JSON_TARGET_VERSION, Integer.toString(trigger.getTargetVersion()));

			if (Log.DEBUG_ON) {
				Log.printDebug(" VbmLog : one log = " + tmpMap.toString() 
					+ " row = " + trigger.getSendFrequency() + ", col = " + bufferCount[trigger.getSendFrequency()]);
			}
			buffers[trigger.getSendFrequency()][bufferCount[trigger.getSendFrequency()]++] = (Map) tmpMap;
			//System.out.println(" VBM : cloned map " +buffers[trigger.getSendFrequency()][bufferCount[trigger.getSendFrequency()]-1].toString());
			
		}
		
		/*
		for(int i=0; i<campaigns.size(); i++){
			trigger = (Measurement) campaigns.get(i);

			// type is set in program. so it does not need to be checked.
			if(bufferCount[ trigger.getSendFrequency()] >= bufferSize) {			
				flushBuffer( trigger.getSendFrequency());

				for(int k=0; k<bufferCount[trigger.getSendFrequency()]; k++){
					if(buffers[trigger.getSendFrequency()][k] == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is null");
						}
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is " + buffers[trigger.getSendFrequency()][k].toString());
						}
					}
				}
			}
			
			// in case configuration is not delivered, but logs are exceed buffer, then increase buffer.
			if(bufferCount[ trigger.getSendFrequency()] > buffers[trigger.getSendFrequency()].length) {			
				flushBuffer( trigger.getSendFrequency());
				if (Log.DEBUG_ON) {
					Log.printDebug(" flushBuffer : flushed. after then count of buffer = " + bufferCount[trigger.getSendFrequency()]);
				}
				for(int k=0; k<bufferCount[trigger.getSendFrequency()]; k++){
					if(buffers[trigger.getSendFrequency()][k] == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is null");
						}
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug(" flushBuffer : " + trigger.getSendFrequency() + ", " + k + " is " + buffers[trigger.getSendFrequency()][k].toString());
						}
					}
				}
			}

			tmpMap = (HashMap) newLog.clone();
			tmpMap.put(VbmCode.JSON_CAMPAIGN_ID, Long.toString(trigger.getCampaignId()));
			tmpMap.put(VbmCode.JSON_CAMPAIGN_VERSION, Integer.toString(trigger.getCampaignVersion()));
			tmpMap.put(VbmCode.JSON_TIME, new Long(Calendar.getInstance().getTimeInMillis()));
			tmpMap.put(VbmCode.JSON_TARGET_VERSION, Integer.toString(trigger.getTargetVersion()));

			if (Log.DEBUG_ON) {
				Log.printDebug(" VbmLog : one log = " + tmpMap.toString() 
					+ " row = " + trigger.getSendFrequency() + ", col = " + bufferCount[trigger.getSendFrequency()]);
			}
			buffers[trigger.getSendFrequency()][bufferCount[trigger.getSendFrequency()]++] = (Map) tmpMap;
			//System.out.println(" VBM : cloned map " +buffers[trigger.getSendFrequency()][bufferCount[trigger.getSendFrequency()]-1].toString());
			
		}
		
		*/
	}
	
	/***
	 * get list of campaigns executed with measurement id considering it is trigger measurement or event measurement
	 * 2013.10.9 added usage point id to check usage point id
	 * @param usagePointId - usage point  id
	 * @param measurementId - measurement id  
	 * @return list of campaigns
	 */
	private ArrayList getCampaignList(long measurementId){		
		
		if(measurementId < VbmCode.MEASUREMENT_SET_SEPEARTOR) {
			// trigger measurement
			return ConfigurationManager.getInstance().getTriggerMeasurementList(measurementId);			
		} else {
			// event measurement
			return ConfigurationManager.getInstance().getCampaignsWithMeasurement(measurementId);			
		}		
	}
	
	/**
	 * setter of buffer's owner.
	 * set appName to start uploader, appName should be set!!!
	 * @param elem application name who owned this buffer
	 * @return result of setting name.
	 */
	public boolean contains(Object elem) {
		
		appName = (String) elem;
		
		return true;
	}
	  
	/**
	 * 
	 * set buffer's size and upload existing buffer and recreate new buffers with new buffer size.
	 * 
	 * @param newSize size of new buffer
	 * 
	 */
    public synchronized void setSize(int newSize) {
    	
    	if(bufferSize == newSize ){

        	if (Log.DEBUG_ON) {
    			Log.printDebug(" bufferSize is already size of " + newSize +". so do not resize buffer.");
        	}
        	return;
    		
    	}
    	bufferSize = newSize;
    	

    	if( newSize < 0 ){

        	if (Log.DEBUG_ON) {
    			Log.printDebug(" set size is negative. So set to default.");
        	}
        	newSize = 1;
    		
    	}
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug(" VbmLog is set with buffer size " + newSize + ", and created");
    	}
    	
    	// when there is no logs in buffer, it returns without recreating buffer.
    	// it is not directly requested. so rsKey is null.
    	if (Log.DEBUG_ON) {
			Log.printDebug(" set rsKey null");
    	}
    	this.rsKey = null;
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug("flush all buffers");
    	}
    	flushBuffer(VbmCode.DAILY_LOG_CACHE);
    	flushBuffer(VbmCode.WEEKLY_LOG_CACHE);
    	flushBuffer(VbmCode.MONTHLY_LOG_CACHE);
    	
    	// when buffer is empty, buffer size is not changed. so regenerate buffer explicitly
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug("recreated all buffers");
    	}
    	recreateBuffer(VbmCode.DAILY_LOG_CACHE);
    	recreateBuffer(VbmCode.WEEKLY_LOG_CACHE);
    	recreateBuffer(VbmCode.MONTHLY_LOG_CACHE);
    	
    }

    
    /**
     * upload current buffer, clear log file and recreate new buffer.
     * 
     */
    public synchronized void clear() {
    	
    	//if (Log.DEBUG_ON) {
    	//	Log.printDebug("LogCache is flushed");
    	//}
    	if (Log.DEBUG_ON) {
			Log.printDebug(" VBM : VbmLog is cleared and flushed. rsKey = " + rsKey);
    	}
    	flushBuffer(VbmCode.DAILY_LOG_CACHE);
    	flushBuffer(VbmCode.WEEKLY_LOG_CACHE);
    	flushBuffer(VbmCode.MONTHLY_LOG_CACHE);
    
    	
    }
    
    /**
     * returns buffer size not element count.
     * 
     * @return size of current buffer.
     * 
     */
    public synchronized int size() {
    	
    	if (Log.EXTRA_ON) {
			Log.printDebug(" VBM : Printing VBM Log Buffer");
			

			if(buffers == null) {
				Log.printDebug(" VBM : buffers is null ");
				return bufferSize;
			}
			
			if(buffers.length == 0) {
				Log.printDebug(" VBM : length of buffers is 0 ");
				return bufferSize;
			}
			
			Log.printDebug(" VBM : Printing : daily buffer ");
			
			if(buffers[VbmCode.DAILY_LOG_CACHE] == null) {
				Log.printDebug(" VBM : Printing : daily buffer is null ");
			} else {
				Log.printDebug(" VBM : Printing : daily buffer size " + bufferCount[VbmCode.DAILY_LOG_CACHE]);
				for(int i=0; i< bufferCount[VbmCode.DAILY_LOG_CACHE];  i++) {
					Log.printDebug(checkMap(buffers[VbmCode.DAILY_LOG_CACHE][i]));
				}
			}
						
			Log.printDebug(" VBM : Printing : weekly buffer ");
			if(buffers[VbmCode.WEEKLY_LOG_CACHE] == null) {
				Log.printDebug(" VBM : Printing : weekly buffer is null ");
			} else {
				Log.printDebug(" VBM : Printing : weekly buffer size " + bufferCount[VbmCode.WEEKLY_LOG_CACHE]);
				for(int i=0; i< bufferCount[VbmCode.WEEKLY_LOG_CACHE];  i++) {
					Log.printDebug(checkMap(buffers[VbmCode.WEEKLY_LOG_CACHE][i]));
				}
			}
			
			Log.printDebug(" VBM : Printing : monthly buffer ");
			if(buffers[VbmCode.MONTHLY_LOG_CACHE] == null) {
				Log.printDebug(" VBM : Printing : monthly buffer is null ");
			} else {
				Log.printDebug(" VBM : Printing : monthly buffer size " + bufferCount[VbmCode.MONTHLY_LOG_CACHE]);
				for(int i=0; i< bufferCount[VbmCode.MONTHLY_LOG_CACHE];  i++) {
					Log.printDebug(checkMap(buffers[VbmCode.MONTHLY_LOG_CACHE][i]));
				}
			}
    	}
    	
    	return bufferSize;
    }
    
    private String checkMap(Map aMap){
    	if(aMap == null) return VbmCode.NULL_STRING;
    	
    	String str = null;
    	
    	try{
    		str = aMap.toString();
    	} catch(Exception e){
    		str = VbmCode.NULL_STRING;
    	}
    	
    	return str;
    }
    
    /**
     * store logs in buffer into disk.
     * this method is called when xlet destroyed.
     *@return checks whether there are logs or not.
     */
    public synchronized boolean isEmpty() {
    	
    	if (Log.DEBUG_ON) {
			Log.printDebug(" wrote logs in disk in VbmLog isEmpty");
    	}
    	boolean ret1 = LogFileManager.getInstance().writeLog(VbmCode.DAILY_LOG_CACHE, buffers[VbmCode.DAILY_LOG_CACHE]);
    	boolean ret2 = LogFileManager.getInstance().writeLog(VbmCode.WEEKLY_LOG_CACHE, buffers[VbmCode.WEEKLY_LOG_CACHE]);
    	boolean ret3 = LogFileManager.getInstance().writeLog(VbmCode.MONTHLY_LOG_CACHE, buffers[VbmCode.MONTHLY_LOG_CACHE]);
    	
    	return (ret1 & ret2 & ret3 );
    }
    
    /***
     * set rsKey when upload
     * 
     * @param obj - rskey
     * @param index - not used
     */
    public void setElementAt(Object obj, int index){
    	this.rsKey = (String) obj;
    }

    
    /**
     * override Vector's toString to make simple job internally.
     * 
     * @return return simple overrided method.
     */
    public synchronized java.lang.String toString() {
    	return appName + " VbmLog processed";
    }
    
    
}

package com.videotron.tvi.illico.vbm.controller;

import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.vbm.controller.buffer.LogManager;
import com.videotron.tvi.illico.vbm.util.Misc;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/**
 * DaemonController is a business logic processor of requests from DAEMON.
 * Requests from DAEMON is also come from STCC and return the final results in STB.
 * 
 * @author swkim
 *
 */
public class DaemonController {

	
	/**
	 * DaemonController's creator.
	 * it creates logScheduler and stcService with static singleton object.
	 */
	public DaemonController() {
		
		//vbmService = (VbmServiceImpl) DataCenter.getInstance().get(VbmService.IXC_NAME);
	}
	
	
	/***
	 * public method shared in controller and DAEMON.
	 * when DAEMON execute processUpload, this method is called and upload logs in buffer.
	 * 
	 * @param rsKey - request session key from server
	 * @return - result of send logs 
	 */
	public int processUpload(String rsKey) {
		
		LogManager logManager = new LogManager();
		
		if (Misc.getMacAddress() == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ERROR : MAC is not set. So do not proess set log config.");
			}
		
			return VbmCode.RETURN_FAILURE; // 
		}

		if(!logManager.uploadAll(rsKey)){
			
			if (Log.DEBUG_ON) {
				Log.printDebug("ERROR : failed to upload buffers.");
			}
			
			return VbmCode.RETURN_FAILURE;
		}
			
		
		return VbmCode.RETURN_SUCCESS; // return code 0 is success;
	}

	
}

package com.videotron.tvi.illico.vbm.controller;


import java.util.Calendar;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.vbm.controller.buffer.LogFileManager;
import com.videotron.tvi.illico.vbm.controller.buffer.LogManager;

import com.videotron.tvi.illico.vbm.schedule.LogScheduler;
import com.videotron.tvi.illico.vbm.util.VbmCode;


/**
 * controller for Vbm application to use inner method.
 * it provides initXlet and destoryXlet.
 * 
 * @author swkim
 *
 */
public class VbmController {

	
	/**
	 * initialize Vbm application.
	 * @param context XletContext of this application.
	 */
	public void initApp(XletContext context) {

		if (Log.DEBUG_ON) {
			Log.printDebug("Vbm initApp callled. Current Time = " + Calendar.getInstance().toString());
    	}

		if (Log.DEBUG_ON) {
    		Log.printDebug("started VBM Service.");
    	}
		
		
		/**
		 * confirm that after initialized configuration, VBM upload logs in storage
		 * and after that flush logs in disk
		 */
		
		if (Log.DEBUG_ON) {
			Log.printDebug("Initialized shared VbmLog object");
		}
		LogManager logManager = new LogManager();
		logManager.initialize();
		
		if (Log.DEBUG_ON) {
			Log.printDebug("Initialized LogScheduler");
		}
		
		// upload logs stored in disk and  initialize scheduler for each log buffer.
		LogScheduler.getInstance().initializeScheduler();
		
	}
	
	/**
	 * 
	 * destroy Vbm application.
	 * 
	 */
	public void destroyApp() {

		// store logs in memory into disk
		LogFileManager.getInstance().clearLogFile(VbmCode.DAILY_LOG_CACHE);
		LogFileManager.getInstance().clearLogFile(VbmCode.WEEKLY_LOG_CACHE);
		LogFileManager.getInstance().clearLogFile(VbmCode.MONTHLY_LOG_CACHE);
		
	}
}

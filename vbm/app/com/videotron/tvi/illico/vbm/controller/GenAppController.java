package com.videotron.tvi.illico.vbm.controller;


import java.rmi.RemoteException;
import java.util.ArrayList;

import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.vbm.config.ConfigurationManager;
import com.videotron.tvi.illico.vbm.config.Measurement;


/**
 * controller for general applications in STB.
 * it provides methods for general applications to use STC
 * 
 * @author swkim
 *
 */
public class GenAppController {

	/**
	 * 	 *
	 * general application call stc to register. 
	 * STC check whether there is a previous configuration or not
	 * if there is a previous configuration, then it keeps previous configuration
	 * 
	 * 
	 * @param appName	targeted application name
	 * @return		returns logging level of application
	 */
	public Measurement [] checkLogCommand(String appName) {
		
		if (Log.DEBUG_ON) {
    		Log.printDebug("checkLogCommand " + appName  + " is called. ");
    		// set default log level into Debug if it is debug model
    		//logLevel = ApplicationConfig.DEBUG;
		}		 
		
		if (appName == null) {
		
			if (Log.DEBUG_ON) {
				Log.printDebug("checkLogCommand appName is null. So return no measurements.");
			}
			return null; 
		}
		
		ConfigurationManager configManager = ConfigurationManager.getInstance();
		
		ArrayList measurementList = null;
		
		 try{
			 measurementList = configManager.getMeasurementsInConfigWithApp(appName);
		 } catch (RemoteException e){
			 if (Log.DEBUG_ON) {
					Log.printDebug("Error getting measurement list.");
				}
		 }

		
		return (Measurement [])measurementList.toArray();
		
	}
	
	
}

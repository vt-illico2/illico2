\uFEFFThis is VBM App Release Notes.
GENERAL RELEASE INFORMATION
- Release Details
	- Release name
  	  : version 1.0.20.2
	- Release App file Name
  	  : VBM_1.0.20.2.zip
	- Release type (official, debug, test,...)
  	  : debug
	- Release date
	  : 2013.04.07

***
version  1.0.20.2 - 2014.04.07
    - New Feature
    	N/A
    - Resolved Issues
	+ VDTRMASTER-5071 [VBM][CQ][R4] Measurements are not transmitted when a campaign reaches its 'End Date'
	+ VDTRMASTER-5075 [VBM][CQ][R4] Measurements are not transmitted when a box is removed from a campaign
***
version  1.0.19.1 - 2014.04.01
    - New Feature
    	N/A
    - Resolved Issues
	+ VDTRMASTER-5071 [VBM][CQ][R4] Measurements are not transmitted when a campaign reaches its 'End Date'
		
***
version  1.0.18.0 - 2014.02.20
    - New Feature
    	+ fixed a incorrect logic that set a count of trigger measurement with zero in CampaignTrigger.
    - Resolved Issues
	N/A

***
version  1.0.17.2 - 2014.02.06
    - New Feature
    - Resolved Issues
	+  (VDTRSUPPORT-293) [CQ][R4]: VBM client continues collecting logs although campaigns are already over.

***
version  1.0.17.1
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-5029) [CQ][R4]:Only STB member of the campaign should send messages to the catcher log


***
version  1.0.16.1
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4855) [CQ][R4][VBM] Only the logs of target STB shall be collected.


***
version  1.0.15.5
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4937) [CQ][R4][VBM] Trigger measurements does not work correctly.
	+  (VDTRMASTER-4943) [CQ][R4][VBM] The VBM continues generating the log even the campaign has already ended.
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.ayed
	+  (VDTRMASTER-4917) [CQ][R4][VBM]:Applications cannot update the configuration of VBMded campaign.
	+  (VDTRMASTER-4918) [CQ][R4][VBM]:Unable to get logs after deactivation/Activation of campaigns

***
version  1.0.14.4
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4937) [CQ][R4][VBM] Trigger measurements does not work correctly.
	+  (VDTRMASTER-4943) [CQ][R4][VBM] The VBM continues generating the log even the campaign has already ended.
	+  (VDTRMASTER-4941) [CQ][R4][VBM]:Measurements of Menu widgets selection are not displayed
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.


***
version  1.0.13.1
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.


***
version  1.0.12.1
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4855) corrected log to add usage point considering configuration
	+  (VTBACKEND-416) [CQ][VBM][R4]Edit Application name in VBM-MS does not really count
	
***
version  1.0.12.0
    - New Feature
    - Resolved Issues
	+  (VDTRMASTER-4855) corrected log to add usage point considering configuration


***
version  1.0.10.0
    - New Feature
	+ adapted new framework libraries.

    - Resolved Issues

***
version  1.0.9.0
    - New Feature
    - Resolved Issues
	+ correct behavior of trigger measurement

***
version  1.0.8.0
    - New Feature
    - Resolved Issues
	+ (VIDEOTRONVBM-158) Not to leave the trigger log when the campaign changed after booting.
	+ (VIDEOTRONVBM-157) Can't use "Direct gathering" when Maximum Buffer Size changed from 10 to 50

***
version  1.0.7.0
    - New Feature
    - Resolved Issues
	+ (VIDEOTRONVBM-159) [VT][VBM] New feature that VBM doesn't leave the log in case that STB is in Standby mode

***
version  1.0.6.0
    - New Feature
	+  changed Timer into TimerWrapper
	+  added function to check STB mode not to write log when STB is in standby mode.
    - Resolved Issues

***
version  1.0.5.0
    - New Feature
	+  change getMacAddress method not to make a recursive call
    - Resolved Issues

***
version  1.0.4.0
    - New Feature
	+  added measurement group attribute
    - Resolved Issues


***
version  1.0.3.1
    - New Feature

    - Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.
	

***
version  1.0.2.1
    - New Feature

    - Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.

***
version  1.0.1.1
    - New Feature

    - Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.




- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-5029) [CQ][R4]:Only STB member of the campaign should send messages to the catcher log
- Release name
  : version 1.0.17.1
- Release App file Name
  : vbm.r3.v.1.0.17.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2014.2.5

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-4855) [CQ][R4][VBM] Only the logs of target STB shall be collected.
- Release name
  : version 1.0.16.1
- Release App file Name
  : vbm.r3.v.1.0.16.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.12.5

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-4937) [CQ][R4][VBM] Trigger measurements does not work correctly.
	+  (VDTRMASTER-4943) [CQ][R4][VBM] The VBM continues generating the log even the campaign has already ended.
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.ayed
	+  (VDTRMASTER-4917) [CQ][R4][VBM]:Applications cannot update the configuration of VBMded campaign.
	+  (VDTRMASTER-4918) [CQ][R4][VBM]:Unable to get logs after deactivation/Activation of campaigns

- Release name
  : version 1.0.15.5
- Release App file Name
  : vbm.r3.v.1.0.15.5.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.11.25

------------------------------------------------------------------------------




- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-4937) [CQ][R4][VBM] Trigger measurements does not work correctly.
	+  (VDTRMASTER-4943) [CQ][R4][VBM] The VBM continues generating the log even the campaign has already ended.
	+  (VDTRMASTER-4941) [CQ][R4][VBM]:Measurements of Menu widgets selection are not displayed
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.

- Release name
  : version 1.0.14.4
- Release App file Name
  : vbm.r3.v.1.0.14.4.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.11.18

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-4881) [CQ][R4][VBM] No log can be gathered after changing the target date to valid of an ended campaign.

- Release name
  : version 1.0.13.1
- Release App file Name
  : vbm.r3.v.1.0.13.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.11.14

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+  (VDTRMASTER-4855) corrected log to add usage point considering configuration
	+  (VTBACKEND-416) [CQ][VBM][R4]Edit Application name in VBM-MS does not really count
- Release name
  : version 1.0.12.0
- Release App file Name
  : vbm.r3.v.1.0.12.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.11.04

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  correct behavior of trigger measurement
- Release name
  : version 1.0.11.0
- Release App file Name
  : vbm.r3.v.1.0.11.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.09.27

------------------------------------------------------------------------------




- Release Details

- New Feature

- Resolved Issues
	+  correct behavior of trigger measurement
- Release name
  : version 1.0.10.0
- Release App file Name
  : vbm.r3.v.1.0.10.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2013.08.27

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+  correct behavior of trigger measurement
- Release name
  : version 1.0.9.0
- Release App file Name
  : vbm.r3.v.1.0.9.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.08.27

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  changed Timer into TimerWrapper
	+  added function to check STB mode not to write log when STB is in standby mode.

- Release name
  : version 1.0.8.0
- Release App file Name
  : vbm.r3.v.1.0.8.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.07.27

------------------------------------------------------------------------------


- Release Details

- New Feature

- Resolved Issues
	+ Not to leave the trigger log when the campaign changed after booting.
	+ Can't use "Direct gathering" when Maximum Buffer Size changed from 10 to 50

- Release name
  : version 1.0.7.0
- Release App file Name
  : vbm.r3.v.1.0.7.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.06.7

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  changed Timer into TimerWrapper
	+  added function to check STB mode not to write log when STB is in standby mode.

- Release name
  : version 1.0.6.0
- Release App file Name
  : vbm.r3.v.1.0.6.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.05.29

------------------------------------------------------------------------------


- Release Details

- New Feature
	+  change getMacAddress method not to make a recursive call
- Resolved Issues


- Release name
  : version 1.0.5.0
- Release App file Name
  : vbm.r3.v.1.0.5.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.05.15

------------------------------------------------------------------------------


- Release Details

- New Feature
	+  added measurement group attribute
- Resolved Issues


- Release name
  : version 1.0.4.0
- Release App file Name
  : vbm.r3.v.1.0.4.0.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.05.9

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  added measurement group attribute

- Release name
  : version 1.0.4.0
- Release App file Name
  : vbm.r3.v.1.0.3.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.04.6

------------------------------------------------------------------------------

- Release Details

- New Feature

- Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.

- Release name
  : version 1.0.3.1
- Release App file Name
  : vbm.r3.v.1.0.3.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.04.6

------------------------------------------------------------------------------

- Release Details

- New Feature

- Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.

- Release name
  : version 1.0.2.1
- Release App file Name
  : vbm.r3.v.1.0.2.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.03.27

------------------------------------------------------------------------------



- Release Details

- New Feature

- Resolved Issues
	+  (VIDEOTRONVBM-145) [Client] The bug that Campign change doesn't apply to the STB. It applied only after rebooting.

- Release name
  : version 1.0.1.1
- Release App file Name
  : vbm.r3.v.1.0.1.1.zip
- Release type (official, debug, test,...)
  : debug
- Release date
  : 2012.03.16

------------------------------------------------------------------------------


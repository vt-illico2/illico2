This is Cinema App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version R4_1.1.0
    - Release App file Name
      : R3_Cinema.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2014.10.31
***
version  R4_1.1.0 - 2014.10.31
    - New Feature
        + Rebranding : 11_default_81 changed.
        + Rebranding : 11_default_96 changed.
        + Rebranding : 11_default_144 changed.
        + Rebranding : 11_default_221 changed.
    - Resolved Issues
        + N/A
***
version  1.0.4.0 - 2012.5.15
    - New Feature
        + applied the updated VBM format(parent application name, group id).
    - Resolved Issues
        + N/A
***
version  1.0.3.0
    - New Feature
    	+ modified to put the log for parent application before the start log.
    - Resolved Issues
        + N/A
***
version  1.0.2.0
    - New Feature
        + put the blank value instead of null value if Cinema cannot get the parent name for VBM log.
    - Resolved Issues
        + N/A
***
version  1.0.1.0
    - New Feature
        + applied error codes from the latest document.
            1. CIN500 is replaced with CIN502.
            2. CIN501 is replaced with CIN500.
            3. CIN502 has new behaviour.
            4. CIN503 has new behaviour.
            5. CIN504 is removed.
    - Resolved Issues
        + N/A
***
version  1.0.0.0
    - New Feature
        + applied to integrate with VBM service.
    - Resolved Issues
        + N/A
***
version  0.2.47.0
    - New Feature
        + applied error codes from the latest document.
            1. CIN500 is replaced with CIN502.
            2. CIN501 is replaced with CIN500.
            3. CIN502 has new behaviour.
            4. CIN503 has new behaviour.
            5. CIN504 is removed.
    - Resolved Issues
        + N/A
***
version  0.2.46.0
    - New Feature
        + modified to have delay time wherever when loading animation is appeared. (Related to VDTRMASTER-3313)
    - Resolved Issues
        + N/A
***
version  0.2.45.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2541 DDC 6451 - Cinema Terminal frozen in Now Playing page.
***
version  0.2.44.0
    - New Feature
        + added '...' on focused text of search list.
    - Resolved Issues
        N/A
***
version  0.2.43.0
    - New Feature
        + modified not to load any image on native memory when stb is started.
    - Resolved Issues
        N/A
***
version  0.2.42.0
    - New Feature
        + modified to flush all images for native memory when Cinema is finished.
    - Resolved Issues
        N/A
***
version  0.2.41.0
    - New Feature
        + modified to show initial text(Tip) on right section when all input text is deleted by C key or Erase button.
    - Resolved Issues
        N/A
***
version  0.2.40.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2541 DDC 6451 - Cinema Terminal frozen in Now Playing page.
***
version  0.2.39.1
    - New Feature
        + fixed sorting logic related French accent.
    - Resolved Issues
        + VDTRMASTER-2428 DDC 6451 - Cinema Many spelling errors in search page.
***
version  0.2.38.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2392 [UI/GUI] [ITV] - Sort in D options (For Cinema)
***
version  0.2.37.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2392 [UI/GUI] [ITV] - Sort in D options (For Cinema)
***
version  0.2.36.0
    - New Feature
        + fixed to move to the screen of cinema movie when cinema is selected on main screen.
    - Resolved Issues
        N/A
***
version  0.2.35.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2326 [UI/GUI] [Cinema] In Search Movie Panel two "Exit" icons overlap
***
version  0.2.34.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2288 [UI/GUI] [General] - D-options > Close. Background should be darker.
***
version  0.2.33.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2168 [UI/GUI] [ITV] - Missing a "exit" button to get out of the application
***
version  0.2.32.0
    - New Feature
        + modified clicking effect in favorite setting.
    - Resolved Issues
        N/A
***
version  0.2.31.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2136 [UI/GUI] [ENA] 'C' Erase key is not in all keyboeard
***
version  0.2.30.0
    - New Feature
        + modified clicking effect in Preference.
    - Resolved Issues
        N/A
***
version  0.2.29.1
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
***
version  0.2.28.0
    - New Feature
        + applied modified Framework(OC unload, D-Option).
    - Resolved Issues
        + N/A
***
version  0.2.27.2
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2037 [UI/GUI] [Cinema] Sort toc
        + VDTRMASTER-2011 [UI/GUI] [Cinema] Search is not always restricted to Cinema
***
version  0.2.26.9
    - New Feature
        + removed temporary logo image.
    - Resolved Issues
        + VDTRMASTER-1976 [UI/GUI] Missing TOC for Cinema
        + VDTRMASTER-1974 [UI/GUI] [Cinema] Select cinema should display only cinemas playing selected movie
        + VDTRMASTER-2011 [UI/GUI] [Cinema] Search is not always restricted to Cinema
        + VDTRMASTER-2007 [UI/GUI] [Cinema] Wrong Showtimes format
        + VDTRMASTER-2009 [UI/GUI] [Cinema] "Showtimes in favourite cinemas" label too long
        + VDTRMASTER-2008 [UI/GUI] [Cinema] no access to Movie Details from cinema page
        + VDTRMASTER-2005 [UI/GUI] [Cinema] favourite cinema should be duplicated in their respective region AND have star icon
        + VDTRMASTER-2003 [UI/GUI] [Cinema] Detail page information should be indented
        + VDTRMASTER-2037 [UI/GUI] [Cinema] Sort toc
***
version  0.2.25.3
    - New Feature
        + modified TOC.
    - Resolved Issues
        + VDTRMASTER-1962 [UI/GUI] [Cinema] Display "No showtimes available" in Movie Showtimes
        + VDTRMASTER-1963 [UI/GUI] [Cinema] Homepage menu header should be "Cinéma en salle" in French
        + VDTRMASTER-1969 [UI/GUI] [Cinema] Text is repeated in cinema
***
version  0.2.24.0
    - New Feature
        + shows loading animation without delay on searching
    - Resolved Issues
        N/A
***
version  0.2.23.0
    - New Feature
        + removed the animation for vod movement.
    - Resolved Issues
        N/A
***
version  0.2.22.0
    - New Feature
        + applied the animation for vod movement.
    - Resolved Issues
        N/A
***
version  0.2.21.0
    - New Feature
        + modify the animation of list builder for favorite UI like a profile.
    - Resolved Issues
        N/A
***
version  0.2.20.2
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-1790 [VT] [Cinema] Cinema is necessary to apply French TOC.
        + VDTRMASTER-1804 [VT] [Cinema] The 'recording pannel' is appear on Cinema subcategory(asset page is same) when pressed 'REC' button.
***
version  0.2.19.0
    - New Feature
        + applied the key consistency
        + if App cannot receive the new release data on main page, App shows 'Content unavailable' for the area of new release.
    - Resolved Issues
        N/A
***
version  0.2.18.0
    - New Feature
        + applied the final TOC document
    - Resolved Issues
        N/A
***
version  0.2.17
    - New Feature
        + added the effect for favorite list
    - Resolved Issues
        N/A
***
version  0.2.16
    - New Feature
        + changed the order of drawing between components
    - Resolved Issues
        N/A
***
version  0.2.15
    - New Feature
        + change images for shadow
    - Resolved Issues
        N/A
***
version  0.2.14
    - New Feature
        + change the position of scroll on movie list
    - Resolved Issues
        N/A
***
version  0.2.13
    - New Feature
        + modified the process of animation
    - Resolved Issues
        N/A
***
version  0.2.12
    - New Feature
        + modified the UI about back key scenario
        + change the shadow image on scroll
        + add the effect to fade in main menu
    - Resolved Issues
        N/A
***
version  0.2.11
    - New Feature
        + modified the GUI issues
    - Resolved Issues
        N/A
***
version  0.2.10
    - New Feature
        + modify the option to SHOW on VideoController.resize()
    - Resolved Issues
        N/A
***
version  0.2.9
    - New Feature
        + add clicking animation on footer
    - Resolved Issues
        N/A
***
version  0.2.8
    - New Feature
        + modify the logic of list for performance tuning
    - Resolved Issues
        N/A
***
version  0.2.7
    - New Feature
        + add scroll top image
        + modify the logic of poster moving
        + add new API of the Keyboard
        + add clicking animation
    - Resolved Issues
        N/A
***
version  0.2.6
	- New Feature
	    + fixed the bug(logo image)
	- Resolved Issues
	    N/A
package com.videotron.tvi.illico.itv.cinema.controller;

import java.rmi.Remote;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * This class connects the IXC.
 * @author pellos
 * @version $Revision: 1.6 $ $Date: 2013/05/07 17:17:59 $
 */
public class CommunicationManager {

    /** XletContext. */
    private XletContext xletContext;

    private IxcLookupWorker ixcWorker;

    /** Class instance. */
    private static CommunicationManager instance = new CommunicationManager();

    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static synchronized CommunicationManager getInstance() {
        return instance;
    }

    /**
     * lookup Monitor, EPG, UPP, LoadingAnimation. bind CallerIDService to IXC.
     *
     * @param context XletContext
     * @param l the DataUpdateListener
     */
    public void start(XletContext context, DataUpdateListener l) {
        this.xletContext = context;
        ixcWorker = new IxcLookupWorker(context);
        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, l);
        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, l);
        ixcWorker.lookup("/1/2026/", EpgService.IXC_NAME, l);
        ixcWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME, l);
        ixcWorker.lookup("/1/2100/", StcService.IXC_NAME, l);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, l);
        ixcWorker.lookup("/1/2085/", VbmService.IXC_NAME, l);
    }

}

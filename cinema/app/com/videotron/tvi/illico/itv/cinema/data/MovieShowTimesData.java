package com.videotron.tvi.illico.itv.cinema.data;

import java.util.Vector;

/**
 * The Class is the MovieShowTimesData.
 * @version $Revision: 1.2 $ $Date: 2011/03/10 16:11:55 $
 * @author pellos
 */
public class MovieShowTimesData {

    /** The cinema id. */
    private String cinemaID;

    /** The cinema name. */
    private String cinemaName;

    /** The schedule. */
    private Vector schedule = new Vector();

    /**
     * Gets the cinema id.
     * @return the cinema id
     */
    public String getCinemaID() {
        return cinemaID;
    }

    /**
     * Sets the cinema id.
     * @param cinemaID the new cinema id
     */
    public void setCinemaID(String cinemaID) {
        this.cinemaID = cinemaID;
    }

    /**
     * Gets the cinema name.
     * @return the cinema name
     */
    public String getCinemaName() {
        return cinemaName;
    }

    /**
     * Sets the cinema name.
     * @param cinemaName the new cinema name
     */
    public void setCinemaName(String cinemaName) {
        this.cinemaName = cinemaName;
    }

    /**
     * Gets the schedule.
     * @return the schedule
     */
    public Vector getSchedule() {
        return schedule;
    }

    /**
     * Sets the schedule.
     * @param schedule the new schedule
     */
    public void setSchedule(Vector schedule) {
        this.schedule = schedule;
    }
}

/*
 *  @(#)NotiPopup.java 1.0 2011.02.24
 *  Copyright (c) 2010 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>TheatreSelectPopup</code> the popup for selection the theatre.
 *
 * @since   2011.02.24
 * @version $Revision: 1.8 $ $Date: 2011/07/05 18:26:19 $
 * @author  tklee
 */
public class TheatreSelectPopup extends Popup {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** The Constant MODE_MSG. */
    public static final int MODE_SELECT = 0;
    /** The Constant MODE_SELECT_FOCUSED. */
    public static final int MODE_SELECT_FOCUSED = 1;
    /** The rows per page. */
    private final int rowsPerPage = 11;
    /** The middle position. */
    private final int middlePosition = 5;

    /** The bg bottom img. */
    private Image bgBImg = null;
    /** The bg middle img. */
    private Image bgMImg = null;
    /** The arrow bottom img. */
    private Image arrowBImg = null;
    /** The arrow top img. */
    private Image arrowTImg = null;
    /** The list focus img. */
    private Image listFocusImg = null;
    /** The favorite icon. */
    private Image favoriteIcon = null;
    /** The foavorite focus icon. */
    private Image faavoriteFocusIcon = null;
    /** The exit icon. */
    private Image exitIcon = null;
    /** The plus icon. */
    private Image plusIcon = null;
    /** The minus icon. */
    private Image minusIcon = null;
    /** The plus icon img. */
    private Image plusIconFocusImg = null;
    /** The minus icon img. */
    private Image minusIconFocusImg = null;
    /** The top shadow img. */
    private Image topShadowImg = null;
    /** The bottom shadow img. */
    private Image bottomShadowImg = null;

    /** The title txt. */
    private String titleTxt = null;
    /** The close txt. */
    private String closeTxt = null;

    /** The list. */
    private Vector list = null;
    /** The focused list. */
    private Vector focusedList = null;
    /** The favorite size. */
    //private int favoriteSize = 0;
    /** The focus index. */
    private int focusIndex = 0;
    /** The focus position. */
    private int focusPosition = 0;
    /** The film id. */
    private String filmId = null;
    /** The lang type. */
    private String langType = null;
    /** The opened region id. */
    private Hashtable openedRegionId = new Hashtable();

    /**
    * Constructor.
    */
    public TheatreSelectPopup() {
        setBounds(0, 0, 338, 356);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Initialize.
     */
    public void init() {
        DataCenter dataCenter = DataCenter.getInstance();
        bgBImg = dataCenter.getImage("11_op2_bg_b.png");
        bgMImg = dataCenter.getImage("11_op2_bg_m.png");
        arrowBImg = dataCenter.getImage("02_ars_b.png");
        arrowTImg = dataCenter.getImage("02_ars_t.png");
        listFocusImg = dataCenter.getImage("11_op2_foc.png");
        favoriteIcon = dataCenter.getImage("02_icon_fav.png");
        faavoriteFocusIcon = dataCenter.getImage("02_icon_fav_foc.png");
        exitIcon = dataCenter.getImage("b_btn_exit.png");
        plusIcon = dataCenter.getImage("03_icon_plus.png");
        minusIcon = dataCenter.getImage("03_icon_minus.png");
        plusIconFocusImg = dataCenter.getImage("03_icon_plus_foc.png");
        minusIconFocusImg = dataCenter.getImage("03_icon_minus_foc.png");
        topShadowImg = dataCenter.getImage("11_op2_sh_t.png");
        bottomShadowImg = dataCenter.getImage("11_op2_sh_b.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
        closeTxt = dataCenter.getString("buttonNames.close");
        titleTxt = dataCenter.getString("cine.chooseTheatre");
    }

    /**
     * Reset focusIndex.
     */
    public void resetFocus() {
        focusIndex = 0;
        focusPosition = 0;
        openedRegionId.clear();
    }

    /**
     * called when popup is started.
     * @see com.videotron.tvi.illico.itv.cinema.ui.popup.Popup#start()
     */
    public void start() {
        list = new Vector();
        Vector vFavorTheatre = Rs.vfavoriteTheatre;
        for (int i = 0; i < vFavorTheatre.size(); i++) {
            //TheatreData theatreData =
            //    (TheatreData) DataManager.theatreData.get(((String[]) vFavorTheatre.elementAt(i))[0]);
            TheatreData theatreData = (TheatreData) vFavorTheatre.elementAt(i);
            if (theatreData != null) {
                if (mode == MODE_SELECT_FOCUSED
                        && !DataManager.movieTimesOfTheatreData.containsKey(theatreData.getTheatreID())) {
                    continue;
                }
                theatreData.setFavoriteTheatre(true);
                list.add(theatreData);
            }
        }
        //list.addAll(DataManager.vfavoriteTheatre);
        if (mode == MODE_SELECT_FOCUSED) {
            list.addAll(focusedList);
        } else { list.addAll(DataManager.vRegion); }
    }

    /**
     * Sets the data.
     *
     * @param focusedData the focused Data
     * @param filmID the film id
     * @param languageType the lang type
     */
    public void setData(Vector focusedData, String filmID, String languageType) {
        focusedList = focusedData;
        filmId = filmID;
        langType = languageType;
    }

    /**
     * called when popup is finished.
     * @see com.videotron.tvi.illico.itv.cinema.ui.popup.Popup#stop()
     */
    public void stop() {
        list = null;
        focusedList = null;
        filmId = null;
        langType = null;
    }

    /**
    * paint.
    *
    * @param        g           Graphics 객체.
    */
    public void paint(Graphics g) {
        g.translate(-327, -106);
        g.setColor(Rs.DVBC78);
        g.fillRect(329, 106, 302, 29);

        g.drawImage(exitIcon, 553, 436, this);
        g.setFont(Rs.F18);
        g.setColor(Rs.C255255255);
        g.drawString(closeTxt, 587, 450);
        g.setFont(Rs.F19);
        g.setColor(Rs.C027024012);
        g.drawString(titleTxt, 347, 127);
        g.setColor(Rs.C214182055);
        g.drawString(titleTxt, 346, 126);

        int lineGap = 27;
        int addPos = 0;
        for (int i = 0; i < rowsPerPage; i++) {
            addPos = (i * lineGap);
            Image bgImg = bgMImg;
            if (i == rowsPerPage - 1) { bgImg = bgBImg; }
            g.drawImage(bgImg, 327, 135 + addPos, this);
        }

        int totalCount = list.size();
        if (totalCount > 0) {
            g.setFont(Rs.F18);
            int initNum = focusIndex - focusPosition;
            int lastNum = 0;
            if (totalCount < rowsPerPage) { lastNum = totalCount - 1; }
            lastNum = focusIndex + rowsPerPage - 1 - focusPosition;
            if (lastNum >= totalCount) { lastNum = totalCount - 1; }
            for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                addPos = (j * lineGap);
                if (i != focusIndex) {
                    Object item = list.elementAt(i);
                    paintItem(g, false, item, addPos);
                }
            }
            if (initNum > 0) { g.drawImage(topShadowImg, 331, 135, this); }
            if (lastNum < totalCount - 1) { g.drawImage(bottomShadowImg, 331, 406, this); }
            paintItem(g, true, list.elementAt(focusIndex), (focusPosition * lineGap));

            if (initNum > 0) { g.drawImage(arrowTImg, 469, 125, this); }
            if (lastNum < totalCount - 1) { g.drawImage(arrowBImg, 469, 432, this); }
        }
        g.translate(327, 106);
    }

    /**
     * Paint item.
     *
     * @param g the Graphics
     * @param isFocus the is focus
     * @param item the item
     * @param addPos the add position
     */
    private void paintItem(Graphics g, boolean isFocus, Object item, int addPos) {
        if (isFocus) {
            g.setColor(Rs.C000000000);
            g.drawImage(listFocusImg, 329, 132 + addPos, this);
        }
        if (item instanceof RegionInfo) {
            RegionInfo regionInfo = (RegionInfo) item;
            String name = regionInfo.getRegionName();
            Vector theatreList = regionInfo.getTheatreList();
            int size = 0;
            if (theatreList != null) { size = theatreList.size(); }
            name = TextUtil.shorten(name, g.getFontMetrics(), 570 - 346);
            name += "(" + size + ")";
            if (!isFocus) { g.setColor(Rs.C193191191); }
            g.drawString(name, 346, 153 + addPos);
            Image openIcon = plusIcon;
            if (isFocus) { openIcon = plusIconFocusImg; }
            if (openedRegionId.containsKey(regionInfo.getRegionId())) {
                openIcon = minusIcon;
                if (isFocus) { openIcon = minusIconFocusImg; }
            }
            g.drawImage(openIcon, 596, 139 + addPos, this);
        } else {
            TheatreData theatreData = (TheatreData) item;
            //if (theatreData.isFavoriteTheatre()) {
            if (Rs.hfavoriteTheatre.containsKey(theatreData.getTheatreID())) {
                Image favIcon = favoriteIcon;
                if (isFocus) { favIcon = faavoriteFocusIcon; }
                g.drawImage(favIcon, 342, 137 + addPos, this);
            }
            if (!isFocus) { g.setColor(Rs.C224224224); }
            String name = TextUtil.shorten(theatreData.getDefaultName(), g.getFontMetrics(), 618 - 373);
            g.drawString(name, 373, 153 + addPos);
        }
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (focusIndex > 0) {
                if (focusPosition != middlePosition || focusIndex == focusPosition) { focusPosition--; }
                focusIndex--;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            int totalCount = list.size();
            if (focusIndex < totalCount - 1) {
                if (focusPosition != middlePosition || focusPosition + focusIndex >= totalCount - 1) {
                    focusPosition++;
                }
                focusIndex++;
                repaint();
            }
            break;
        case Rs.KEY_OK:
            clickEffect.start(329 - 327, 132 + (focusPosition * 27) - 106, 631 - 329, 191 - 159);
            Object item = list.elementAt(focusIndex);
            if (item instanceof RegionInfo) {
                RegionInfo regionInfo = (RegionInfo) item;
                String regionId = regionInfo.getRegionId();
                Vector theatreList = regionInfo.getTheatreList();
                if (theatreList != null && theatreList.size() > 0) {
                    if (openedRegionId.containsKey(regionId)) {
                        for (int i = 0; i < theatreList.size(); i++) {
                            list.removeElementAt(focusIndex + 1);
                        }
                        openedRegionId.remove(regionId);

                        if (focusPosition >= middlePosition) {
                            int listSize = list.size();
                            if (listSize - focusIndex < rowsPerPage - focusPosition) {
                                if (listSize - focusIndex > middlePosition) {
                                    focusPosition = middlePosition;
                                } else { focusPosition = rowsPerPage - (listSize - focusIndex); }
                            }
                        }
                    } else {
                        list.addAll(focusIndex + 1, theatreList);
                        openedRegionId.put(regionId, "");

                        if (focusPosition > middlePosition) {
                            int listSize = list.size();
                            if (listSize - focusIndex > rowsPerPage - focusPosition) {
                                if (listSize - focusIndex > middlePosition) {
                                    focusPosition = middlePosition;
                                } else { focusPosition = rowsPerPage - (listSize - focusIndex); }
                            }
                        }
                    }
                    repaint();
                }
            } else {
                TheatreData theatreData = (TheatreData) item;
                scene.requestPopupOk(new Object[]{theatreData.getTheatreID(), filmId, langType});
            }
            break;
        case Rs.KEY_EXIT:
            clickEffect.start(553 - 327, 438 - 106, 582 - 553, 454 - 438);
            scene.requestPopupClose();
            break;
        default:
            return false;
        }
        return true;
    }
}

package com.videotron.tvi.illico.itv.cinema.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;

/**
 * This class is the CacheManager class for data.
 *
 * @since 2011.03.18
 * @version $Revision: 1.4 $ $Date: 2012/02/15 04:09:58 $
 * @author tklee
 */

public final class CacheManager {
    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty("dvb.persistent.root");
    /** The Constant FOLDER_ROOT. */
    private static final String FOLDER_ROOT = "/itv/cinema/main/";
    /** The Constant FILE_DATE. */
    private static final String FILE_DATE = REPOSITORY_ROOT + FOLDER_ROOT + "date.dat";
    /** The Constant FILE_MAIN. */
    private static final String FILE_MAIN = REPOSITORY_ROOT + FOLDER_ROOT + "main.dat";
    /** The Constant FILE_IMG. */
    private static final String FILE_IMG = REPOSITORY_ROOT + FOLDER_ROOT + "mainImg.dat";

    /** Instance of CacheManager. */
    private static CacheManager instance = new CacheManager();

    /** The use cached. */
    public static boolean useCached = false;

    /** The limit date. */
    private long limitDate = -1;

    /**
     * Gets the singleton instance of MainManager.
     * @return CacheManager
     */
    public static synchronized CacheManager getInstance() {
        return instance;
    }

    /**
     * Prepare cache data.
     */
    public void prepareCacheData() {
        if (!useCached) { return; }
        Logger.debug(this, "prepareCacheData() start : ");
        LogDisplayer.getInstance().cacheStartTime = System.currentTimeMillis();
        try {
            File file = new File(REPOSITORY_ROOT + FOLDER_ROOT);
            Logger.debug(this, "prepareCacheData()-file.exists() : " + file.exists());
            if (!file.exists()) {
                file = new File(REPOSITORY_ROOT + "/itv");
                if (file.mkdir()) {
                    file = new File(REPOSITORY_ROOT + "/itv/cinema");
                    if (file.mkdir()) {
                        file = new File(REPOSITORY_ROOT + "/itv/cinema/main");
                        boolean isMake = file.mkdir();
                        if (isMake) { Logger.debug(this, "prepareCacheData()-make root dirctory."); }
                        Logger.debug(this, "prepareCacheData()-isMake : " + isMake);
                    }
                }
                Logger.debug(this, "prepareCacheData()-file : " + file);
            }
            file = new File(FILE_DATE);
            if (file.exists()) {
                String strDate = readFileToString(FILE_DATE);
                if (strDate != null) {
                    limitDate = Long.parseLong(strDate);
                    Logger.debug(this, "prepareCacheData()-limitDate : " + limitDate);
                }
            }
            //TODO
            //if (limitDate == -1 || limitDate < System.currentTimeMillis()) { return; }

            file = new File(FILE_MAIN);
            if (file.exists()) {
                DataManager.arrRecentFilmsData = (RecentFilmData[]) readObjectStream(file);
                Logger.debug(this, "prepareCacheData()-recentData : " + DataManager.arrRecentFilmsData);
            }

            Hashtable imgData = null;
            file = new File(FILE_IMG);
            if (file.exists()) { imgData = (Hashtable) readObjectStream(file); }
            if (imgData != null) { Logger.debug(this, "prepareCacheData()-imgData.lenght : " + imgData.size()); }
            DataManager.cachedImgList = imgData;
        } catch (Exception e) {
            Logger.debug(this, "prepareCacheData()-e : " + e.getMessage());
            e.printStackTrace();
        }
        LogDisplayer.getInstance().cacheEndTime = System.currentTimeMillis();
    }

    /**
     * Sets the recent data.
     *
     * @param strDate the str date
     * @param recentData the recent data
     */
    public void setRecentData(final String strDate, final RecentFilmData[] recentData) {
        if (!useCached) { return; }
        Logger.debug(this, "called setRecentData()");
        Thread thread = new Thread("setRecentData()") {
            public void run() {
                if (strDate != null) { makeFile(FILE_DATE, strDate); }
                if (recentData != null) {
                    Logger.debug(this, "setRecentData()-recentData.length : " + recentData.length);
                    writeObjectStream(recentData, new File(FILE_MAIN));
                }
            }
        };
        thread.start();
    }

    /**
     * Sets the recent img list.
     *
     * @param imgList the new recent img list
     */
    public void setRecentImgList(final Hashtable imgList) {
        if (!useCached) { return; }
        Logger.debug(this, "called setRecentImgList()");
        if (imgList == null || imgList.isEmpty()) { return; }
        Thread thread = new Thread("setRecentImgList()") {
            public void run() {
                Logger.debug(this, "setRecentImgList()-imgList.size() : " + imgList.size());
                Enumeration e = imgList.keys();
                while (e.hasMoreElements()) {
                    Object key = e.nextElement();
                    byte[] imgValue = (byte[]) imgList.get(key);
                    Logger.debug(this, "setRecentImgList()-img size-" + key + ":" + imgValue.length);
                }
                if (imgList != null) { writeObjectStream(imgList, new File(FILE_IMG)); }
            }
        };
        thread.start();
    }

    /**
     * Read object stream.
     *
     * @param file the file
     * @return the object
     */
    private Object readObjectStream(File file) {
        ObjectInputStream ois = null;
        Object objectData = null;
        try {
            ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
            objectData = ois.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objectData;
    }

    /**
     * Read file to string.
     *
     * @param fileName the file name
     * @return the string
     */
    private String readFileToString(String fileName) {
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            fis = new FileInputStream(fileName);
            baos = new ByteArrayOutputStream();
            byte temp;
            while ((temp = (byte) fis.read()) != -1) {
                baos.write(temp);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
            if (fis != null)
                fis.close();
            if (baos != null)
                baos.close();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        return baos.toString();
    }

    /**
     * Make file.
     *
     * @param fileName the file name
     * @param data the data
     */
    private void makeFile(String fileName, String data) {
        FileOutputStream fout = null;
        try {
            File tempFile = new File(fileName);
            if (tempFile.exists()) { tempFile.delete(); }
            tempFile.createNewFile();
            fout = new FileOutputStream(tempFile);
            fout.write(data.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
            if (fout != null)
                fout.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Write object stream.
     *
     * @param out the out
     * @param file the file
     */
    private void writeObjectStream(Object out, File file) {
        try {
            if (!file.exists()) { file.createNewFile(); }
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(out);
            oos.flush();
            oos.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

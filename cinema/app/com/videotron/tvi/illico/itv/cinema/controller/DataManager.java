package com.videotron.tvi.illico.itv.cinema.controller;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is the DataManager. This class connects to server. And store a data.
 * @author pellos
 * @version $Revision: 1.11 $ $Date: 2012/02/23 06:11:11 $
 */
public final class DataManager {
    /** The Constant NO_ERR. */
    public static final int NO_ERR = -1;
    /** The Constant ERR_CONNECTION. */
    private static final int ERR_CONNECTION = 0;
    /** The Constant ERR_XML_PARSING. */
    public static final int ERR_XML_PARSING = 1;
    /** The Constant ERR_CONFIG_GET. */
    private static final int ERR_CONFIG_GET = 2;
    /** The Constant ERR_CONFIG_PARSING. */
    public static final int ERR_CONFIG_PARSING = 3;
    /** The error code. */
    private final String[] errCode = {"CIN500", "CIN501", "CIN502", "CIN503"};
    /** The errpor message. */
    private final String[] errMsg = {"Cannot contact Cinema HTTP server",
            "Cannot parse XML files received from Cinema Server",
            "Cannot get the required configuration files from the Management System",
            "Cannot parse configuration files received from the Management System"};

    /** Is the TheatreData. */
    public static Hashtable theatreDetailData = new Hashtable();
    /** Is the Movie data. */
    public static Hashtable allMovie = new Hashtable();
    /** Is the Movie data recently. */
    public static Hashtable recentFilmsData = new Hashtable();
    /** Is the Movie infomation data. */
    public static Hashtable filmsInfoData = new Hashtable();
    /** Is the Movie Schedule data. */
    public static Hashtable movieTimesOfTheatreData = new Hashtable();

    /** The region. */
    public static Hashtable region = new Hashtable();
    /** The region data in Vector type. */
    public static Vector vRegion = new Vector();
    /** The theatre data. */
    public static Hashtable theatreData = new Hashtable();
    /** Is the Movie data recently in array. */
    public static RecentFilmData[] arrRecentFilmsData = null;
    /** The movie list. */
    public static Vector movieList = new Vector();
    /** The search movie. */
    public static Vector searchMovie = new Vector();

    /** Class instance. */
    private static DataManager instance = new DataManager();
    /** XMLParser instance. */
    private XMLParser xmlParser = new XMLParser();
    /** The Constant FR_COLLATOR. */
    //private static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);
    /** The is error. */
    //public boolean isError = false;
    public int errorType = NO_ERR;

    /** The cached image list on flash. */
    public static Hashtable cachedImgList = null;
    //TODO test
    //public static String recentDate = null;

    /**
     * Constructor.
     */
    private DataManager() {
    }

    /**
     * Get DataManager instance.
     * @return DataManager
     */
    public static synchronized DataManager getInstance() {
        return instance;
    }

    /**
     * Reset.
     */
    public void reset() {
        theatreDetailData.clear();
        allMovie.clear();
        recentFilmsData.clear();
        filmsInfoData.clear();
        movieTimesOfTheatreData.clear();
        //searchMovie.clear();
        region.clear();
        vRegion.clear();
        theatreData.clear();
        arrRecentFilmsData = null;
        movieList.clear();
        searchMovie.clear();
        cachedImgList = null;
    }

    /**
     * Process data.
     *
     * @param path the path
     * @param separation the separation
     * @param nameForEmul the name for emul
     */
    private void processData(String path, String separation, String nameForEmul) {
        if (!Rs.IS_EMULATOR && HttpConnection.getInstance().configFile == null) {
            errorType = ERR_CONFIG_GET;
            printError(null);
            return;
        }
        //isError = false;
        HttpURLConnection conn = null;
        InputStream inputStream = null;
        try {
            conn = HttpConnection.getInstance().getURLConnection(path);
            inputStream = conn.getInputStream();
            try {
                //xmlParser.setParserXml(separation, nameForEmul);
                xmlParser.setParserXml(inputStream, separation);
            } catch (Exception e) {
                errorType = ERR_XML_PARSING;
                printError(e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorType = ERR_CONNECTION;
            printError(e.getMessage());
            //isError = true;
        } finally {
            try {
                if (inputStream != null) { inputStream.close(); }
            } catch (Exception e) { e.printStackTrace(); }
            try {
                if (conn != null) { conn.disconnect(); }
            } catch (Exception e) { e.printStackTrace(); }
        }
    }

    /**
     * Prints the error.
     * @param addTxt the add text
     */
    public void printError(String addTxt) {
        printError(errorType, addTxt);
    }

    /**
     * Prints the error.
     *
     * @param errType the error type. refer to upper constant.
     * @param addTxt the add text
     */
    public void printError(int errType, String addTxt) {
        if (errType == NO_ERR) { return; }
        String errTxt = "[" + errCode[errType] + "]" + errMsg[errType];
        if (addTxt != null) { errTxt += "-" + addTxt; }
        Logger.error(this, errTxt);
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public String getErrorCode() {
        return errCode[errorType];
    }

    /**
     * Get the Theatre data.
     * @param thId is theatre id.
     * @return TheatreData.
     */
    public TheatreData getTheatresInfo(String thId) {
        errorType = NO_ERR;
        TheatreData thInfo = (TheatreData) theatreDetailData.get(thId);
        if (thInfo == null || thInfo.getAddress() == null) {
            processData(Rs.HORIRESCINEMA + thId, Rs.HORIRESCINEMA, thId);
            Log.printInfo("theatres =" + theatreDetailData.size());
        }
        return (TheatreData) theatreDetailData.get(thId);
    }

    /**
     * Get the Recent Movie.
     * @return RecentFilmData
     */
    public RecentFilmData[] getRecentMovie() {
        errorType = NO_ERR;
        Logger.debug(this, "arrRecentFilmsData : " + arrRecentFilmsData);
        if (arrRecentFilmsData != null) { return arrRecentFilmsData; }
        LogDisplayer.getInstance().receiveStartTime = System.currentTimeMillis();
        processData("accueil", Rs.RECENTMOVIE, "accueil");
        LogDisplayer.getInstance().receiveEndTime = System.currentTimeMillis();
        arrRecentFilmsData = new RecentFilmData[recentFilmsData.size()];

        Enumeration recentEnum = recentFilmsData.elements();
        int count = 0;
        while (recentEnum.hasMoreElements()) {
            arrRecentFilmsData[count] = (RecentFilmData) recentEnum.nextElement();
            count++;
        }
        //TODO test
        if (arrRecentFilmsData.length > 0) {
            CacheManager.getInstance().setRecentData("111111111", arrRecentFilmsData);
        }
        return arrRecentFilmsData;
    }

    /**
     * Get the Region information.
     *
     * @param regionId is region id.
     */
    public void getRegionInfo(String regionId) {
        errorType = NO_ERR;
        RegionInfo regionInfo = (RegionInfo) region.get(regionId);
        if (regionInfo == null) {
            processData(Rs.SALLESREGION + regionId, Rs.SALLESREGION, "1");
            regionInfo = (RegionInfo) region.get(regionId);
            if (regionInfo != null) {
                Vector theatreList = regionInfo.getTheatreList();
                if (theatreList != null) { Collections.sort(theatreList); }
                theatreList = regionInfo.getTheatreListForFavor();
                if (theatreList != null) { Collections.sort(theatreList); }
                //Collections.sort(vRegion);
            }
        }
    }

    /**
     * Search Movies.
     * @param name Search letter or tous.
     * @param isAll whether all or not
     * @return Vector movie list
     */
    public Vector getFilmsData(String name, boolean isAll) {
        errorType = NO_ERR;
        Logger.info(this, "getFilmsData - name : " + name);
        if (isAll) {
            if (movieList.isEmpty()) { processData(Rs.FILMSALL + name, Rs.FILMSALL, name); }
            return movieList;
        } else {
            searchMovie.clear();
            String searchStr = name.substring(0, 1);
            XMLParser.searchWord = name;
            processData(Rs.FILMSALL + searchStr.toLowerCase(), Rs.FILMSLETTRE, name);
            if (!searchMovie.isEmpty()) {
                //FilmData.sortType = FilmData.ALPHABET;
                FilmData.sortType = FilmData.SORT_ALPHABET;
                Collections.sort(searchMovie);
            }
            return searchMovie;
        }
    }

    /**
     * Get the FilmInfoData.
     * @param id is a film id.
     * @return FilmInfoData
     */
    public FilmInfoData getFilmInfoData(String id) {
        errorType = NO_ERR;
        FilmInfoData filmInfoData = (FilmInfoData) filmsInfoData.get(id);
        if (filmInfoData == null) {
            processData(Rs.FICHEFILM + id, Rs.FICHEFILM, id);
            filmInfoData = (FilmInfoData) filmsInfoData.get(id);
            Log.printInfo("filmInfoData 2= " + filmInfoData);
        }
        return filmInfoData;
    }

    /**
     * Get the MovieTimesOfTheatreData.
     * @param id is film id.
     */
    public void getMovieTimesOfTheatreData(String id) {
        movieTimesOfTheatreData.clear();
        errorType = NO_ERR;
        processData(Rs.HORAIRESALLES + id, Rs.HORAIRESALLES, id);
        /*MovieShowTimesData[] data = new MovieShowTimesData[movieTimesOfTheatreData.size()];
        Enumeration filmEnum = movieTimesOfTheatreData.elements();
        int count = 0;
        while (filmEnum.hasMoreElements()) {
            data[count] = (MovieShowTimesData) filmEnum.nextElement();
            count++;
        }
        return data;*/
    }
}

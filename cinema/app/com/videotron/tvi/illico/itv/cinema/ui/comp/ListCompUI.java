/*
 *  @(#)ListCompUI.java 1.0 2011.05.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.comp;

import java.awt.Point;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.cinema.ui.effect.FlowEffect;
import com.videotron.tvi.illico.itv.cinema.ui.effect.ListBuilderEffect;

/**
 * <code>ListCompUI</code> the component for list.
 *
 * @since   2011.05.04
 * @version $Revision: 1.3 $ $Date: 2011/06/07 02:08:25 $
 * @author  tklee
 */
public class ListCompUI extends UIComponent {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant EFFECT_LEFT. */
    public static final int EFFECT_LEFT = 0;
    /** The Constant EFFECT_RIGHT. */
    public static final int EFFECT_RIGHT = 1;

    /** The left source. */
    private final Rectangle[] leftSrc = {new Rectangle(0, 0, 380, 318), new Rectangle(683 - 200, 360, 500, 276)};
    /** The left from. */
    private final Point[] leftFrom = {new Point(0, 0), new Point(78 - 206, 132)};
    /** The left to. */
    private final Point[] leftTo = {new Point(-103, 0), new Point(78 - 206 + 103, 132)};
    /** The right source. */
    private final Rectangle[] rightSrc = {new Rectangle(683 - 200, 360, 500, 276), new Rectangle(580, 0, 380, 318)};
    /** The right from. */
    private final Point[] rightFrom = {new Point(683, 132), new Point(580, 0)};
    /** The right to. */
    private final Point[] rightTo = {new Point(683 - 103, 132), new Point(580 + 103, 0)};
    /** The focus source. */
    private final Rectangle focusSrc = new Rectangle(408, 0, 144, 266);
    /** The focus rectangle. */
    private final Rectangle focusRec = new Rectangle(408, 107, 144, 266);
    /** The left focus rectangle. */
    private final Rectangle leftFocusRec = new Rectangle(284, 132, 96, 177);
    /** The right focus rectangle. */
    private final Rectangle rightFocusRec = new Rectangle(580, 132, 96, 177);

    /** The Effect instance. */
    private Effect effect = null;

    /** whether this component is executing the animation or not. */
    public boolean isAnimation = false;

    /** The effect type. */
    public int effectType = 0;

    /**
     * Initialize the Component.
     */
    public ListCompUI() { }

    /**
     * set Renderer instance and set Effect instance.
     *
     * @param renderer the Renderer object
     * @param eff the Effect
     */
    public void setRenderer(Renderer renderer, Effect eff) {
        super.setRenderer(renderer);
        effect = eff;
    }

    /**
     * Start effect.
     *
     * @param type the effect type
     * @param leftSrcRec the left source rectangle
     * @param rightSrcRec the right source rectangle
     * @param leftFromPoint the left from Point
     * @param leftToPoint the left to Point
     * @param rightFromPoint the right from Point
     * @param rightToPoint the right to Point
     */
    public void startEffect(int type, Rectangle leftSrcRec, Rectangle rightSrcRec, Point leftFromPoint,
            Point leftToPoint, Point rightFromPoint, Point rightToPoint) {
        effectType = type;
        if (effect != null && effect instanceof ListBuilderEffect) {
            isAnimation = true;
            ((ListBuilderEffect) effect).start(type, leftSrcRec, rightSrcRec, leftFromPoint,
                    leftToPoint, rightFromPoint, rightToPoint);
            isAnimation = false;
        }
    }

    /**
     * Start effect.
     *
     * @param type the effect type
     */
    public void startEffect(int type) {
        effectType = type;
        if (effect != null && effect instanceof FlowEffect) {
            isAnimation = true;
            FlowEffect flowEffect = (FlowEffect) effect;
            if (type == EFFECT_LEFT) {
                flowEffect.setFocusRectangle(focusSrc, focusRec, leftFocusRec, rightFocusRec);
            } else { flowEffect.setFocusRectangle(focusSrc, focusRec, rightFocusRec, leftFocusRec); }
            flowEffect.start(type, leftSrc[type], leftFrom[type], leftTo[type], rightSrc[type], rightFrom[type],
                    rightTo[type]);
            isAnimation = false;
        }
    }
}

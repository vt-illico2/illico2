package com.videotron.tvi.illico.itv.cinema.ui.comp;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class draw a location.
 * @version $Revision: 1.8 $ $Date: 2011/07/08 21:32:47 $
 * @author pellos
 */
public class LocationComponent extends Container {

    /**
     * Is the serialVersionID.
     */
    private static final long serialVersionUID = 1L;

    /** Log image. */
    //private static Image appLogoImg = null;
    /** Logo image. */
    private Image logoImage;
    /** Dim image. */
    private Image hisDim;
    /** Over image. */
    private Image hisOver;

    /** Store a location. */
    private String[] location;

    /**
     * Initialize.
     */
    public void init() {
        hisDim = DataCenter.getInstance().getImage("his_dim.png");
        hisOver = DataCenter.getInstance().getImage("his_over.png");
        logoImage = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        if (logoImage == null) { logoImage = DataCenter.getInstance().getImage("logo_en.png"); }
        //appLogoImg = DataCenter.getInstance().getImage("app_logo_cinema.png");
    }

    /**
     * start a scene.
     *
     * @param sceneNumber is Scene id.
     * @param locTxt the loc txt
     */
    public void start(int sceneNumber, String locTxt) {
        String firstName = DataCenter.getInstance().getString("cine.Cinema");
        if (locTxt == null) { locTxt = ""; }
        location = null;
        Log.printInfo("sceneNumber = " + sceneNumber);
        switch (sceneNumber) {
        case SceneManager.SCENE_ID_TITLE:
            location = new String[] {firstName, DataCenter.getInstance().getString("cine.title")};
            break;
        case SceneManager.SCENE_ID_THEATRE_ASSET:
            location = new String[] {firstName, DataCenter.getInstance().getString("cine.Theatre")};
            break;
        case SceneManager.SCENE_ID_MOVIE_ASSET:
            location = new String[] {firstName, locTxt};
            break;
        case SceneManager.SCENE_ID_MOVIE_SHOWTIMES:
            location = new String[] {firstName, DataCenter.getInstance().getString("cine.TitleMovieShow")};
            break;
        default:
            location = new String[] {firstName};
            break;
        }
        Log.printInfo("location[0]" + location[0]);
    }

    /**
     * stop scene.
     */
    public void stop() {
        setVisible(false);
    }

    /**
     * paint.
     *
     * @param g the Graphics
     */
    public void paint(Graphics g) {
        int logoWidth = 0;
        if (logoImage != null) {
            //g.drawImage(logoImage, 35, 17, this);
            g.drawImage(logoImage, 53, 21, this);
            logoWidth = logoImage.getWidth(this);
        } else { logoWidth = 80; }
        int startPos = 53 + logoWidth + 15;
        g.setFont(Rs.F18);
        if (location != null) {
            if (location.length > 1) {
                g.setColor(Rs.C124124124);
                g.drawImage(hisDim, startPos, 45, this);
                startPos += hisDim.getWidth(this) + 10;
                g.drawString(location[0], startPos, 57);
                startPos += g.getFontMetrics().stringWidth(location[0]) + 11;
                g.drawImage(hisOver, startPos, 45, this);
                startPos += hisOver.getWidth(this) + 10;
                g.setColor(Rs.C236236236);
                g.drawString(location[location.length - 1], startPos, 57);
            } else if (location.length == 1) {
                g.setColor(Rs.C239);
                g.drawImage(hisOver, startPos, 45, this);
                startPos += hisOver.getWidth(this) + 10;
                g.drawString(location[0], startPos, 57);
            }
        }
        //g.drawImage(appLogoImg, 57, 482, this);
    }

    /**
     * Graphics.
     *
     * @param g the Graphics
     */
    public void update(Graphics g) {
        paint(g);
    }
}

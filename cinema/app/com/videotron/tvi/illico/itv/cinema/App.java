package com.videotron.tvi.illico.itv.cinema;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.LogDisplayer;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.log.Log;

/**
 * The initial class of Cinema.
 * @version $Revision: 1.10 $ $Date: 2013/05/07 17:17:59 $
 * @author PellOs
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    public static XletContext xletContext;
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
    }

    public void init() {
        MainManager.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        Log.printInfo("startXlet start");
        LogDisplayer.getInstance().startTime = System.currentTimeMillis();
        FrameworkMain.getInstance().start();
        MainManager.getInstance().start();
        LogDisplayer.getInstance().endTime = System.currentTimeMillis();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
        MainManager.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional
     *            If unconditional is true when this method is called, requests by the Xlet to not enter the destroyed
     *            state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
        MainManager.getInstance().destroy();
    }

    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }

    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }

    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

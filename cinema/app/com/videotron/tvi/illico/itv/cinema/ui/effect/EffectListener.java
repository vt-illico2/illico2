/*
 *  @(#)EffectListener.java 1.0 2011.05.31
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.effect;

import java.awt.Component;
import java.awt.Graphics;

/**
 * <code>EffectListener</code> This class is the listener for additional works.
 *
 * @since   2011.06.01
 * @version $Revision: 1.2 $ $Date: 2011/06/07 02:08:24 $
 * @author  tklee
 */
public interface EffectListener {
    /**
     * Request prepaint.
     *
     * @param g the Graphics
     * @param c the Component
     */
    void requestPrepaint(Graphics g, Component c);

    /**
     * Request paint.
     *
     * @param g the Graphics
     * @param c the Component
     */
    void requestPaint(Graphics g, Component c);
}

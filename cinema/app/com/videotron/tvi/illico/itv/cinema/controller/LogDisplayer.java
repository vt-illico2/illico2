package com.videotron.tvi.illico.itv.cinema.controller;

import java.awt.Component;
import java.awt.Graphics;

import com.videotron.tvi.illico.itv.cinema.common.Rs;

/**
 * This class is the LogDisplayer class to display logs.
 *
 * @since 2011.03.25
 * @version $Revision: 1.2 $ $Date: 2012/02/15 04:09:58 $
 * @author tklee
 */

public final class LogDisplayer  extends Component {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
    /** Instance of CacheManager. */
    private static LogDisplayer instance = null;
    /** The Constant USE_DISPLAY. */
    public static final boolean USE_DISPLAY = false;

    /** The start time. */
    public long startTime = 0;
    /** The end time. */
    public long endTime = 0;
    /** The receive start time. */
    public long receiveStartTime = 0;
    /** The receive end time. */
    public long receiveEndTime = 0;
    /** The img start time. */
    public long[] imgStartTime = null;
    /** The img end time. */
    public long[] imgEndTime = null;
    /** The cache start time. */
    public long cacheStartTime = 0;
    /** The cache end time. */
    public long cacheEndTime = 0;
    /** The is log show. */
    public boolean isLogShow = false;

    /**
     * Gets the singleton instance of MainManager.
     * @return CacheManager
     */
    public static synchronized LogDisplayer getInstance() {
        if (instance == null) { instance = new LogDisplayer(); }
        return instance;
    }

    /**
     * Instantiates a new log displayer.
     */
    private LogDisplayer() {
        setBounds(Rs.SCENE_BOUND);
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     */
    public void paint(Graphics g) {
        //if (!isLogShow || !USE_DISPLAY) { return; }
        g.setColor(Rs.DVB90C255);
        g.fillRect(0, 0, 960, 540);
        g.setFont(Rs.F24);
        g.setColor(Rs.C255205012);
        String logType = "Server data";
        if (CacheManager.useCached) { logType = "Cached data"; }
        g.drawString("Log Type          : " + logType + "(press page up key to change)", 100, 70);
        g.drawString("Xlet start time   : " + startTime, 100, 100);
        g.drawString("Xlet end time     : " + endTime, 100, 130);
        g.drawString("Xlet total time   : " + (endTime - startTime), 100, 160);
        g.drawString("Server start time : " + receiveStartTime, 100, 200);
        g.drawString("Server end time   : " + receiveEndTime, 100, 230);
        g.drawString("Server total time : " + (receiveEndTime - receiveStartTime), 100, 260);
        g.drawString("Cache start time  : " + cacheStartTime, 100, 300);
        g.drawString("Cache end time    : " + cacheEndTime, 100, 330);
        g.drawString("Cache total time  : " + (cacheEndTime - cacheStartTime), 100, 360);
        int yPos = 100;
        long totalTime = 0;
        for (int i = 0; imgStartTime != null && i < imgStartTime.length; i++) {
            //g.drawString("image[" + i + "] start time : " + imgStartTime[i], 400, 280);
            //g.drawString("image[" + i + "] end time : " + imgEndTime[i], 400, 310);
            g.drawString("image[" + i + "] time : " + (imgEndTime[i] - imgStartTime[i]), 500, yPos);
            totalTime += (imgEndTime[i] - imgStartTime[i]);
            yPos += 30;
        }
        g.drawString("image total time : " + totalTime, 500, yPos);
    }
}

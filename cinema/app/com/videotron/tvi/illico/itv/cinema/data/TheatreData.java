package com.videotron.tvi.illico.itv.cinema.data;

import java.text.Collator;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

/**
 * The Class TheatreData.
 * @version $Revision: 1.3 $ $Date: 2012/02/15 04:09:59 $
 * @author pellos
 */
public class TheatreData implements Comparable {
    /** The Constant FR_COLLATOR. */
    protected static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);
    /** The Constant STR_TRUE. 0 is false. */
    public static final String STR_TRUE = "1";

    /** The region id. */
    private String regionID;

    /** The region name. */
    private String regionName;

    /** The theatre id. */
    private String theatreID;

    /** The default name. */
    private String defaultName;

    /** The theatre name french. */
    private String theatreNameFrench;

    /** The schedule french. */
    private Vector scheduleFrench = new Vector();

    /** The theatre name english. */
    private String theatreNameEnglish;

    /** The schedule english. */
    private Vector scheduleEnglish = new Vector();

    /** The address. */
    private String address;

    /** The wheelchair. */
    private String wheelchair;

    /** The metro. */
    private String metro;

    /** The bus. */
    private String bus;

    /** The hearing impaired. */
    private String hearingImpaired;

    /** The films. */
    private Vector films = new Vector();
    /** The hfilms. */
    private Hashtable hfilms = new Hashtable();

    /** The favorite theatre. */
    private boolean favoriteTheatre;

    /**
     * Checks if is favorite theatre.
     * @return true, if is favorite theatre
     */
    public boolean isFavoriteTheatre() {
        return favoriteTheatre;
    }

    /**
     * Sets the favorite theatre.
     * @param favoriteTheatre the new favorite theatre
     */
    public void setFavoriteTheatre(boolean favoriteTheatre) {
        this.favoriteTheatre = favoriteTheatre;
    }

    /**
     * Gets the films.
     * @return the films
     */
    public Vector getFilms() {
        return films;
    }

    /**
     * Sets the films.
     * @param films the new films
     */
    public void setFilms(Vector films) {
        this.films = films;
    }

    /**
     * @return the hfilms
     */
    public Hashtable getHfilms() {
        return hfilms;
    }

    /**
     * set the films of Hashtable type.
     * @param films the hfilms to set
     */
    public void setHfilms(Hashtable films) {
        this.hfilms = films;
    }

    /**
     * Gets the region id.
     * @return the region id
     */
    public String getRegionID() {
        return regionID;
    }

    /**
     * Sets the region id.
     * @param regionID the new region id
     */
    public void setRegionID(String regionID) {
        this.regionID = regionID;
    }

    /**
     * Gets the region name.
     * @return the region name
     */
    public String getRegionName() {
        return regionName;
    }

    /**
     * Sets the region name.
     * @param regionName the new region name
     */
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }

    /**
     * Gets the theatre id.
     * @return the theatre id
     */
    public String getTheatreID() {
        return theatreID;
    }

    /**
     * Sets the theatre id.
     * @param iD the new theatre id
     */
    public void setTheatreID(String iD) {
        theatreID = iD;
    }

    /**
     * Gets the default name.
     * @return the default name
     */
    public String getDefaultName() {
        return defaultName;
    }

    /**
     * Sets the default name.
     * @param defaultName the new default name
     */
    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    /**
     * Gets the theatre name french.
     * @return the theatre name french
     */
    public String getTheatreNameFrench() {
        return theatreNameFrench;
    }

    /**
     * Sets the theatre name french.
     * @param theatreNameFrench the new theatre name french
     */
    public void setTheatreNameFrench(String theatreNameFrench) {
        this.theatreNameFrench = theatreNameFrench;
    }

    /**
     * Gets the schedule french.
     * @return the schedule french
     */
    public Vector getScheduleFrench() {
        return scheduleFrench;
    }

    /**
     * Sets the schedule french.
     * @param scheduleFrench the new schedule french
     */
    public void setScheduleFrench(Vector scheduleFrench) {
        this.scheduleFrench = scheduleFrench;
    }

    /**
     * Gets the theatre name english.
     * @return the theatre name english
     */
    public String getTheatreNameEnglish() {
        return theatreNameEnglish;
    }

    /**
     * Sets the theatre name english.
     * @param theatreNameEnglish the new theatre name english
     */
    public void setTheatreNameEnglish(String theatreNameEnglish) {
        this.theatreNameEnglish = theatreNameEnglish;
    }

    /**
     * Gets the schedule english.
     * @return the schedule english
     */
    public Vector getScheduleEnglish() {
        return scheduleEnglish;
    }

    /**
     * Sets the schedule english.
     * @param scheduleEnglish the new schedule english
     */
    public void setScheduleEnglish(Vector scheduleEnglish) {
        this.scheduleEnglish = scheduleEnglish;
    }

    /**
     * Gets the address.
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address.
     * @param address the new address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets the wheelchair.
     * @return the wheelchair
     */
    public String getWheelchair() {
        return wheelchair;
    }

    /**
     * Sets the wheelchair.
     * @param wheelchair the new wheelchair
     */
    public void setWheelchair(String wheelchair) {
        this.wheelchair = wheelchair;
    }

    /**
     * Gets the metro.
     * @return the metro
     */
    public String getMetro() {
        return metro;
    }

    /**
     * Sets the metro.
     * @param metro the new metro
     */
    public void setMetro(String metro) {
        this.metro = metro;
    }

    /**
     * Gets the bus.
     * @return the bus
     */
    public String getBus() {
        return bus;
    }

    /**
     * Sets the bus.
     * @param bus the new bus
     */
    public void setBus(String bus) {
        this.bus = bus;
    }

    /**
     * Gets the hearing impaired.
     * @return the hearing impaired
     */
    public String getHearingImpaired() {
        return hearingImpaired;
    }

    /**
     * Sets the hearing impaired.
     * @param hearingImpaired the new hearing impaired
     */
    public void setHearingImpaired(String hearingImpaired) {
        this.hearingImpaired = hearingImpaired;
    }

    /**
     * implements abstract method of Comparable Class.
     * <p>
     *
     * @param obj the object to compare (TheatreData)
     * @return compared value.
     */
    public int compareTo(Object obj) {
        String selfName = getDefaultName();
        String paraName = ((TheatreData) obj).getDefaultName();
        //int compareValue = selfName.compareToIgnoreCase(paraName);
        int compareValue = FR_COLLATOR.compare(selfName, paraName);
        return compareValue;
    }
}

/*
 *  @(#)NotiPopup.java 1.0 2011.02.28
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * <code>NotiPopup</code> Notification Popup.
 *
 * @since   2011.02.28
 * @version $Revision: 1.4 $ $Date: 2011/03/15 13:54:44 $
 * @author  tklee
 */
public class NotiPopup extends Popup {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_MSG. */
    public static final int MODE_MSG = 0;
    /** The Constant MODE_ERR_LOAD. */
    public static final int MODE_ERR_LOAD = 1;
    /** The Constant MODE_ERR_NO_DATA. */
    public static final int MODE_ERR_NO_DATA = 2;
    /** The Constant MSG_DELIM. */
    private static final String MSG_DELIM = "\n";

    /** The data center. */
    private final DataCenter dataCenter = DataCenter.getInstance();
    /** The txt title. */
    private String[] txtTitle = new String[3];
    /** The txt msage. */
    private String[][] txtMsg = new String[3][1];
    /** The txt ok. */
    private String txtOk = null;

    /** The bg top img. */
    private Image bgTopImg = null;
    /** The bg middle img. */
    private Image bgMiddleImg = null;
    /** The bg bottom img. */
    private Image bgBottomImg = null;
    /** The shadow img. */
    private Image shadowImg = null;
    /** The line img. */
    private Image lineImg = null;
    /** The high img. */
    private Image highImg = null;
    /** The focus img. */
    private Image focusImg = null;

    /** when Ok key is pressed this value determin if screen have to move to previous screen.*/
    private boolean isGoToPrev = false;

    /**
     * Instantiates a new noti popup.
     */
    public NotiPopup() {
        setBounds(0, 0, 410, 330);
    }

    /**
     * Instantiates.
     */
    public void init() {
        bgTopImg = dataCenter.getImage("05_pop_grow_t.png");
        bgMiddleImg = dataCenter.getImage("05_pop_grow_m.png");
        bgBottomImg = dataCenter.getImage("05_pop_grow_b.png");
        shadowImg = dataCenter.getImage("05_pop_sh_01.png");
        lineImg = dataCenter.getImage("05_pop_line_01.png");
        highImg = dataCenter.getImage("05_pop_high_01.png");
        focusImg = dataCenter.getImage("05_focus.png");

        txtTitle[0] = "";
        txtTitle[1] = dataCenter.getString("cine.notiTitle");
        txtTitle[2] = dataCenter.getString("cine.notiTitle");
        txtMsg[0][0] = "";
        txtMsg[1][0] = dataCenter.getString("cine.comError");
        txtMsg[2][0] = dataCenter.getString("cine.noData");
        txtOk = dataCenter.getString("cine.OK");
     }

    /**
     * set data.
     *
     * @param msgData message
     * @param titleTxt title text
     */
    public void setData(String msgData, String titleTxt) {
        if (msgData == null) { return; }
        StringTokenizer strTokenizer = new StringTokenizer(msgData.trim(), MSG_DELIM);
        String[] msg = new String[strTokenizer.countTokens()];
        for (int i = 0; strTokenizer.hasMoreTokens(); i++) {
            msg[i] = strTokenizer.nextToken();
        }
        txtMsg[mode] = msg;
        txtTitle[mode] = titleTxt;
    }

    /**
     * when Ok key is pressed this value determin if screen have to move to previous screen.
     *
     * @param isGoPrev if screen have to move to previous screen
     */
    public void setGoPrev(boolean isGoPrev) {
        isGoToPrev = isGoPrev;
    }

    /**
     * Checks if is go prev.
     *
     * @return true, if is go prev
     */
    public boolean isGoPrev() {
        return isGoToPrev;
    }

    /**
    * paint.
    *
    * @param        g           Graphics 객체.
    */
    public void paint(Graphics g) {
        g.translate(-276, -113);

        g.setColor(Rs.C035035035);
        g.fillRect(306, 143, 350, 224);

        g.drawImage(bgTopImg, 276, 113, this);
        g.drawImage(bgMiddleImg, 276, 193, 410, 124, this);
        g.drawImage(bgBottomImg, 276, 317, this);
        g.drawImage(shadowImg, 304, 364, this);
        g.drawImage(lineImg, 285, 181, this);
        g.drawImage(highImg, 306, 143, this);
        g.drawImage(focusImg, 410, 318, this);

        g.setFont(Rs.F24);
        g.setColor(Rs.C252202004);
        GraphicUtil.drawStringCenter(g, txtTitle[mode], 479, 169);

        g.setFont(Rs.F20);
        g.setColor(Rs.C255255255);
        //String[] str = TextUtil.split(txtMsg[mode], g.getFontMetrics(), 295);
        int checkLength = txtMsg[mode].length;
        if (checkLength > 5) { checkLength = 5; }
        int baseYPos = 250 - ((checkLength - 1) * 15);
        for (int i = 0; i < checkLength; i++) {
            GraphicUtil.drawStringCenter(g, txtMsg[mode][i], 482, baseYPos + (i * 30));
        }

        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        GraphicUtil.drawStringCenter(g, txtOk, 482, 339);

        g.translate(276, 113);
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_OK:
        case Rs.KEY_EXIT:
            if (isGoToPrev) {
                scene.requestPopupOk(null);
            } else { scene.requestPopupClose(); }
            break;
        default:
            return false;
        }
        return true;
    }
}

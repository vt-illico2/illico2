package com.videotron.tvi.illico.itv.cinema.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.MovieShowTimesData;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.log.Log;

/**
 * The Class XMLParser.
 * @version $Revision: 1.8 $ $Date: 2012/02/15 04:09:58 $
 * @author pellos
 */
public class XMLParser extends DefaultHandler {
    /** The region key. */
    private String regionKey = null;
    /** The separation. */
    private String separation = null;
    /** The film data. */
    private FilmData filmData = null;
    /** The data. */
    private FilmInfoData data = null;
    /** The th info. */
    private TheatreData thInfo = null;
    /** The movie show times data. */
    private MovieShowTimesData movieShowTimesData = null;
    /** The factory. */
    private SAXParserFactory factory = null;
    /** The parser. */
    private SAXParser parser = null;
    /** The search word. */
    public static String searchWord = "";
    /** The is fr. */
    private boolean isFr = true;

    /**
     * Instantiates a new xML parser.
     */
    public XMLParser() {
        factory = SAXParserFactory.newInstance();
    }

    /**
     * Sets the parser xml.
     *
     * @param input the input
     * @param sepa the separation
     * @throws Exception the exception
     */
    public void setParserXml(InputStream input, String sepa) throws Exception {
        separation = sepa;
        Log.printInfo("XMLParser start " + separation);
        InputSource inputSource = new InputSource(input);
        //inputSource.setEncoding("UTF-8");
        parser = factory.newSAXParser();
        parser.parse(inputSource, this);
        Log.printInfo("xml read end");
    }

    /**
     * Sets the parser xml.
     *
     * @param sepa the separation
     * @param name the name
     * @throws Exception the exception
     */
    public void setParserXml(String sepa, String name) throws Exception {
        Log.printInfo("XMLParser start " + sepa + " name = " + name);
        this.separation = sepa;
        parser = factory.newSAXParser();

        File f = null;
        if (separation.equals(Rs.HORIRESCINEMA)) {
            f = new File("data/t" + name + ".xml");
        } else if (separation.equals(Rs.FILMSLETTRE)) {
            f = new File("data/a.xml");
        } else { f = new File("data/" + name + ".xml"); }
        parser.parse(new InputSource(new FileInputStream(f)), this);
    }

    /**
     * Receive notification of the start of an element. Is received values of xml. This method makes pastille data.
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @param attributes The attributes attached to the element. If there are no attributes, it shall be an empty
     *            Attributes object.
     * @throws SAXException the sAX exception
     * @see org.xml.sax.ContentHandler#startElement
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        try {
            String key = null;
            if (separation.equals(Rs.SALLESREGION)) {
                if (localName.equals("Cinemas")) {
                    RegionInfo regionInfo = new RegionInfo();
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);

                        if (qNameSub.equals("id_region")) {
                            Logger.debug(this, "regionId : " + value);
                            regionKey = value;
                            regionInfo.setRegionId(value);
                        } else if (qNameSub.equals("region")) {
                            Logger.debug(this, "regionName : " + value);
                            regionInfo.setRegionName(value);
                        }
                    }

                    DataManager.region.put(regionKey, regionInfo);
                    DataManager.vRegion.addElement(regionInfo);
                } else if (localName.equals("salle")) {
                    RegionInfo regionInfo = (RegionInfo) DataManager.region.get(regionKey);
                    TheatreData theatreDate = new TheatreData();
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);
                        if (qNameSub.equals("id")) {
                            key = value;
                            theatreDate.setTheatreID(value);
                        } else if (qNameSub.equals("nom")) {
                            theatreDate.setDefaultName(value);
                        }
                        theatreDate.setRegionID(regionKey);
                        theatreDate.setRegionName(regionInfo.getRegionName());
                    }
                    regionInfo.addTheatreData(theatreDate);
                    if (!Rs.hfavoriteTheatre.containsKey(key)) {  regionInfo.addTheatreListForFavor(theatreDate); }

                    //DataManager.searchTheatre.put(key, regionKey);
                    if (DataManager.theatreData.get(key) == null) {
                        DataManager.theatreData.put(key, theatreDate);
                    }
                }
            } else if (separation.equals(Rs.HORIRESCINEMA)) {
                Logger.debug(this, "HORIRESCINEMA-localName : " + localName);
                if (localName.equals("root")) {
                    thInfo = new TheatreData();
                } else if (localName.equals("Salle")) {
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);
                        if (qNameSub.equals("id")) {
                            thInfo.setTheatreID(value);
                            Logger.debug(this, "theatreId : " + value);
                        } else if (qNameSub.equals("nom")) {
                            thInfo.setDefaultName(value);
                        }
                    }
                } else if (localName.equals("Adresse")) {
                    String qNameSub = attributes.getQName(0);
                    String value = attributes.getValue(0);
                    if (qNameSub.equals("numero")) {
                        thInfo.setAddress(value);
                    }
                } else if (localName.equals("Pictos")) {
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);

                        if (qNameSub.equals("chaiseroulante")) {
                            thInfo.setWheelchair(value);
                        } else if (qNameSub.equals("metro")) {
                            thInfo.setMetro(value);
                        } else if (qNameSub.equals("autobus")) {
                            thInfo.setBus(value);
                        } else if (qNameSub.equals("soustitre")) {
                            thInfo.setHearingImpaired(value);
                        }
                    }
                } else if (localName.equals("Film")) {
                    filmData = new FilmData();
                    //String title = null;
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);

                        if (qNameSub.equals("id")) {
                            filmData.setID(value);
                            Logger.debug(this, "id : " + value);
                        } else if (qNameSub.equals("nom")) {
                            filmData.setTitle(value);
                            Logger.debug(this, "nom : " + value);
                            //title = value;
                        } else if (qNameSub.equals("langue")) {
                            filmData.setLangue(value);
                            Logger.debug(this, "langue : " + value);
                            //setFilmTitle(filmData, value, title);
                        } else if (qNameSub.equals("genre")) {
                            filmData.setGenre(value);
                        } else if (qNameSub.equals("classement")) {
                            filmData.setClassement(value);
                        } else if (qNameSub.equals("duree")) {
                            filmData.setDuration(value);
                        }
                    }
                } else if (localName.equals("horaire")) {
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);
                        Logger.debug(this, "horaire-qNameSub : " + qNameSub);
                        Logger.debug(this, "horaire-value : " + value);
                        if (qNameSub.equals("heures")) {
                            filmData.getSchedule().add(value);
                        }
                    }
                }
            } else if (separation.equals(Rs.RECENTMOVIE)) {
                if (localName.equals("Films")) {
                    return;
                } else if (localName.equals("primeur")) {
                    String str = attributes.getValue("primeur");
                    if (str == null || Integer.parseInt(str) != 1) { return; }

                    RecentFilmData fData = new RecentFilmData();
                    for (int a = 0; a < attributes.getLength(); a++) {
                        String qNameSub = attributes.getQName(a);
                        String value = attributes.getValue(a);
                        if (qNameSub.equals("id")) {
                            fData.setID(value);
                        } else if (qNameSub.equals("titre")) {
                            fData.setTitle(value);
                            //if (value.length() > 0) { fData.setLangType(FilmData.LANGUAGE_FRENCH); }
                        } else if (qNameSub.equals("vfde")) {
                            fData.setVfde(value);
                            /*if (value.length() > 0) {
                                if (fData.getTitle().length() == 0
                                        || Definitions.LANGUAGE_ENGLISH.equals(Rs.language)) {
                                    fData.setLangType(FilmData.LANGUAGE_ENGLISH);
                                }
                            }*/
                        } else if (qNameSub.equals("photo")) {
                            fData.setPhotoURL(value);
                        } else if (qNameSub.equals("poster")) {
                            fData.setPosterURL(value);
                        } else if (qNameSub.equals("primeur")) {
                            fData.setPrimeur(Integer.parseInt(value));
                        }
                    }

                    if (fData.getPrimeur() == 1 && fData.getPosterURL() != null) {
                        DataManager.recentFilmsData.put(fData.getID(), fData);
                    }
                }
            } else { startElement(localName, attributes); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receive notification of the start of an element. Is received values of xml. This method makes pastille data.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param attributes The attributes attached to the element. If there are no attributes, it shall be an empty
     *            Attributes object.
     * @throws Exception the Exception
     * @see org.xml.sax.ContentHandler#startElement
     */
    public void startElement(String localName, Attributes attributes) throws Exception {
        if (separation.equals(Rs.FILMSLETTRE)) {
            if (localName.equals("Film")) {
                int searchStrLen = searchWord.length();
                String str = attributes.getValue("titre");
                if (str != null && str.length() >= searchStrLen) {
                    //Logger.debug(this, "searchWord : " + searchWord);
                    //Logger.debug(this, "value : " + value);
                    String searchLetter = str.substring(0, searchStrLen);
                    if (!searchLetter.equalsIgnoreCase(searchWord)) { return; }
                } else { return; }
                FilmData fData = new FilmData();
                //String title = null;
                for (int a = 0; a < attributes.getLength(); a++) {
                    String qNameSub = attributes.getQName(a);
                    String value = attributes.getValue(a);

                    if (qNameSub.equals("id")) {
                        fData.setID(value);
                    } else if (qNameSub.equals("titre")) {
                        fData.setTitle(value);
                        //title = value;
                    } else if (qNameSub.equals("langue")) {
                        fData.setLangue(value);
                        //setFilmTitle(filmData, value, title);
                    } else if (qNameSub.equals("genre")) {
                        fData.setGenre(value);
                    } else if (qNameSub.equals("classement")) {
                        fData.setClassement(value);
                    } else if (qNameSub.equals("duree")) {
                        fData.setDuration(value);
                    }
                }
                //DataManager.searchMovie.put(fData.getID() + fData.getLangue(), fData);
                DataManager.searchMovie.addElement(fData);
            }
        } else if (separation.equals(Rs.FILMSALL)) {
            if (localName.equals("Film")) {
                FilmData fData = new FilmData();
                //FilmData fData = null;
                //String title = null;
                for (int a = 0; a < attributes.getLength(); a++) {
                    String qNameSub = attributes.getQName(a);
                    String value = attributes.getValue(a);

                    if (qNameSub.equals("id")) {
                        //if (fData == null) {
                        //    fData = (FilmData) DataManager.moviesInTheatre.get(value);
                        //    if (fData == null) { fData = new FilmData(); }
                        //}
                        fData.setID(value);
                    } else if (qNameSub.equals("titre")) {
                        //title = value;
                        fData.setTitle(value);
                    } else if (qNameSub.equals("langue")) {
                        fData.setLangue(value);
                        //setFilmTitle(fData, value, title);
                    } else if (qNameSub.equals("genre")) {
                        fData.setGenre(value);
                    } else if (qNameSub.equals("classement")) {
                        fData.setClassement(value);
                    } else if (qNameSub.equals("duree")) {
                        fData.setDuration(value);
                    }
                }
                DataManager.allMovie.put(fData.getID() + fData.getLangue(), fData);
                DataManager.movieList.addElement(fData);
                //if (fData != null && !DataManager.moviesInTheatre.containsKey(fData.getID())) {
                //    DataManager.moviesInTheatre.put(fData.getID(), fData);
                //    DataManager.movieList.addElement(fData);
                //}
            }
        } else if (separation.equals(Rs.FICHEFILM)) {

            if (localName.equals("Fiche")) {
                data = null; //new FilmInfoData();
                for (int a = 0; a < attributes.getLength(); a++) {
                    String qNameSub = attributes.getQName(a);
                    String value = attributes.getValue(a);

                    if (qNameSub.equals("id")) {
                        if (data == null) { data = new FilmInfoData(); }
                        data.setId(value);
                    } else if (qNameSub.equals("titre")) {
                        data.setTitre(value);
                    } else if (qNameSub.equals("vfde")) {
                        data.setVfde(value);
                    } else if (qNameSub.equals("genre")) {
                        data.setGenre(value);
                    } else if (qNameSub.equals("classement")) {
                        data.setClassement(value);
                    } else if (qNameSub.equals("sortieEnSalle")) {
                        data.setReleaseDate(value);
                    } else if (qNameSub.equals("synopsis")) {
                        data.setSummary(value);
                    }
                }
            } else if (localName.equals("acteur")) {
                String qNameSub = attributes.getQName(0);
                String value = attributes.getValue(0);
                if (qNameSub.equals("nom")) {
                    data.getActors().add(value);
                }
            } else if (localName.equals("realisateur")) {
                String qNameSub = attributes.getQName(0);
                String value = attributes.getValue(0);
                if (qNameSub.equals("nom")) {
                    data.getDirectors().add(value);
                }
            } else if (localName.equals("photo")) {
                String qNameSub = attributes.getQName(0);
                String value = attributes.getValue(0);
                if (qNameSub.equals("lien")) {
                    data.getDetailPhoto().add(value);
                }
            } else if (localName.equals("poster")) {
                String qNameSub = attributes.getQName(0);
                String value = attributes.getValue(0);
                if (qNameSub.equals("lien")) {
                    data.setPosterURL(value);
                }
            }
        } else if (separation.equals(Rs.HORAIRESALLES)) {
            if (localName.equals("Cinema")) {
                movieShowTimesData = new MovieShowTimesData();
                for (int a = 0; a < attributes.getLength(); a++) {
                    String qNameSub = attributes.getQName(a);
                    String value = attributes.getValue(a);
                    Log.printInfo("qNameSub[" + a + "]=" + qNameSub);
                    Log.printInfo("value[" + a + "]=" + value);
                    if (qNameSub.equals("id")) {
                        movieShowTimesData.setCinemaID(value);
                    } else if (qNameSub.equals("nom")) {
                        movieShowTimesData.setCinemaName(value);
                    }
                }
            } else if (localName.equals("horaire")) {
                String qNameSub = attributes.getQName(0);
                String value = attributes.getValue(0);
                Logger.debug(this, "HORAIRESALLES-horaire-qNameSub : " + qNameSub);
                Logger.debug(this, "HORAIRESALLES-horaire-value : " + value);
                if (qNameSub.equals("heures")) {
                    movieShowTimesData.getSchedule().add(value);
                }
            } else if (localName.equals("Francais")) {
                isFr = true;
            } else if (localName.equals("Anglais")) {
                isFr = false;
            }
        }
    }

    /**
     * Sets the film title to FilmData.
     *
     * @param fData the f data
     * @param langType the lang type
     * @param title the title
     */
    /*private void setFilmTitle(FilmData fData, String langType, String title) {
        if (FilmData.LANGUAGE_FRENCH.equals(langType)) {
            fData.setTitleFr(title);
        } else if (FilmData.LANGUAGE_ENGLISH.equals(langType)) {
            fData.setTitleEn(title);
        } else { fData.setTitleOther(title); }
    }*/

    /**
     * Receive notification of the end of an element. Saves pastille data.
     * <p>
     * By default, do nothing. Application writers may override this method in a subclass to take specific actions at
     * the end of each element (such as finalising a tree node or writing output to a file).
     * </p>
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @throws SAXException the sAX exception
     * @see org.xml.sax.ContentHandler#endElement
     */
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (separation.equals(Rs.HORIRESCINEMA)) {
            if (localName.equals("Salle")) {
                DataManager.theatreDetailData.put(thInfo.getTheatreID(), thInfo);
            } else if (localName.equals("Film")) {
                //thInfo.getHfilms().put(filmData.getID(), filmData);
                thInfo.getHfilms().put(filmData.getID() + filmData.getLangue(), filmData);
                thInfo.getFilms().addElement(filmData);
            }
        } else if (separation.equals(Rs.FICHEFILM)) {
            if (localName.equals("Fiche") && data != null) {
                Log.printInfo("filmsInfoData put= " + data.getId());
                DataManager.filmsInfoData.put(data.getId(), data);
            }
        } else if (separation.equals(Rs.HORAIRESALLES)) {
            if (localName.equals("Cinema")) {
                String preStr = FilmData.LANGUAGE_ENGLISH;
                if (isFr) { preStr = FilmData.LANGUAGE_FRENCH; }
                DataManager.movieTimesOfTheatreData.put(preStr + movieShowTimesData.getCinemaID(), movieShowTimesData);
            }
        }
    }
}

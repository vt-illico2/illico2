/*
 *  @(#)MovieInfoRenderer.java 1.0 2011.03.02
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>MovieInfoRenderer</code> The class to paint a GUI for a movie information selected by user.
 *
 * @since   2011.03.02
 * @version $Revision: 1.8 $ $Date: 2011/06/24 23:21:24 $
 * @author  tklee
 */
public class MovieInfoRenderer extends BasicRenderer {
    /** The Constant MENU_THEATRE. */
    public static final int MENU_THEATRE = 0;
    /** The Constant MENU_INFO. */
    public static final int MENU_SHOWTIME = 1;
    /** The Constant MENU_INFO. */
    public static final int MENU_ALLMOVIE = 2;

    /** The en icon. */
    private Image enIconImg = null;
    /** The fr icon. */
    private Image frIconImg = null;
    /** The poster shadow. */
    private Image posterShadowImg = null;
    /** The poster dim. */
    private Image posterDimImg = null;
    /** The menu1 bg. */
    private Image menu1BgImg = null;
    /** The menu2 bg. */
    private Image menu2BgImg = null;
    /** The menu shadow. */
    private Image menuShadowImg = null;
    /** The menu grow. */
    private Image menuGrowImg = null;
    /** The menu line. */
    private Image menuLineImg = null;
    /** The menu arrow icon. */
    private Image menuArrIconImg = null;
    /** The menu focus. */
    private Image menuFocusImg = null;
    /** The menu middle bg img. */
    private Image menuMiddleBgImg = null;
    /** The scroll shadow. */
    public Image scrollShadowImg = null;
    /** The scroll shadow top img. */
    public Image scrollShadowTopImg = null;
    /** The detail bg shadow img. */
    public Image detailBgShadowImg = null;
    /** The default poster img. */
    private Image defaultPosterImg = null;

    /** The menu txt. */
    public String[] menuTxt = new String[3];
    /** Release String. */
    private String releaseDateTxt = null;
    /** Actors String. */
    private String actors = null;
    /** The director txt. */
    private String directorTxt = null;

    /** Film Data. */
    //public FilmData filmData = null;
    public FilmInfoData filmInfoData = null;
    /** The menu focus. */
    public int menuFocus = 0;
    /** The lang type. */
    public String langType = "";

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        enIconImg = getImage("icon_en.png");
        frIconImg = getImage("icon_fr.png");
        menu1BgImg = getImage("01_acbg_1st.png");
        menu2BgImg = getImage("01_acbg_2nd.png");
        menuShadowImg = getImage("01_ac_shadow.png");
        menuGrowImg = getImage("01_acgrow.png");
        menuLineImg = getImage("01_acline.png");
        menuArrIconImg = getImage("02_detail_ar.png");
        menuFocusImg = getImage("02_detail_bt_foc.png");
        menuMiddleBgImg = getImage("01_acbg_m.png");
        posterShadowImg = getImage("01_possh_221.png");
        posterDimImg = getImage("01_poshigh_221.png");
        scrollShadowImg = getImage("01_des_sh_dt.png");
        scrollShadowTopImg = getImage("01_des_sh_dt_t2.png");
        detailBgShadowImg = getImage("01_dtail_sh.png");
        defaultPosterImg = getImage("11_default_221.png");
        super.prepare(c);

        menuTxt[0] = getString("cine.selectThea");
        menuTxt[1] = getString("cine.seeTimesFavor");
        menuTxt[2] = getString("cine.seeAllMovies");
        releaseDateTxt = DataCenter.getInstance().getString("cine.releaseDate");
        actors = DataCenter.getInstance().getString("cine.Actors");
        directorTxt = DataCenter.getInstance().getString("cine.Director");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("icon_en.png");
        removeImage("icon_fr.png");
        removeImage("01_acbg_1st.png");
        removeImage("01_acbg_2nd.png");
        removeImage("01_ac_shadow.png");
        removeImage("01_acgrow.png");
        removeImage("01_acline.png");
        removeImage("02_detail_ar.png");
        removeImage("02_detail_bt_foc.png");
        removeImage("01_acbg_m.png");
        removeImage("01_possh_221.png");
        removeImage("01_poshigh_221.png");
        removeImage("01_hotkeybg.png");
        removeImage("01_des_sh_dt.png");
        removeImage("01_dtail_sh.png");
        for (int i = 0; i < gradeImgName.length; i++) {
            removeImage(gradeImgName[i]);
        }*/
        filmInfoData = null;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        if (filmInfoData == null) { return; }
        g.drawImage(detailBgShadowImg, 289, 202, c);
        //g.drawImage(hotkeybgImg, 236, 466, c);
      //poster
        Image posterImage = filmInfoData.getPosterImage();
        if (posterImage == null) { posterImage = defaultPosterImg; }
        if (posterImage != null) {
            g.drawImage(posterImage, 59, 120, 221, 332, c);
            g.drawImage(posterDimImg, 59, 120, c);
            g.drawImage(posterShadowImg, 31, 443, c);
        }

        //info
        g.setFont(Rs.F23);
        g.setColor(Rs.C255204000);
        String title = filmInfoData.getTitle();
        if (!langType.equals(FilmData.LANGUAGE_FRENCH)) { title = filmInfoData.getVfde(); }
        g.drawString(title, 310, 134);

        g.setColor(Rs.C200);
        g.setFont(Rs.F16);
        FontMetrics fm = g.getFontMetrics();
        String str = filmInfoData.getGenre();
        if (str == null) { str = ""; }
        String temp = filmInfoData.getDuration();
        if (temp != null && temp.length() > 0) { str += " | " + temp; }
        int pos = 311;
        String filmLang = langType;
        Image langIcon = null;
        if (filmLang.equals(FilmData.LANGUAGE_FRENCH)) {
            langIcon = frIconImg;
        } else if (filmLang.equals(FilmData.LANGUAGE_ENGLISH)) { langIcon = enIconImg; }
        Image gradeImg = getGradeImage(filmInfoData.getClassement());
        if (langIcon != null || gradeImg != null) {
            if (str.length() > 0) {
                str += " | ";
                pos += fm.stringWidth(str);
            }
            if (langIcon != null) {
                g.drawImage(langIcon, pos, 154, c);
                pos += 28;
            }
            if (gradeImg != null) { g.drawImage(gradeImg, pos, 154, c); }
        }
        g.drawString(str, 311, 165);

        str = releaseDateTxt + " : ";
        str += filmInfoData.getReleaseDate();
        g.drawString(str, 311, 183);
        g.setColor(Rs.C182182182);
        g.drawString(directorTxt + " :", 313, 228);
        g.drawString(actors + " :", 313, 247);
        g.setFont(Rs.F13_D);
        g.setColor(Rs.C255255255);
        Vector[] vList = {filmInfoData.getDirectors(), filmInfoData.getActors()};
        int xPos = 385;
        if (Rs.language.equals(Definitions.LANGUAGE_FRENCH)) { xPos = 395; }
        for (int i = 0; i < vList.length; i++) {
            str = "";
            for (int j = 0; vList[i] != null && j < vList[i].size(); j++) {
                if (j != 0) { str += ", "; }
                str += vList[i].elementAt(j);
            }
            g.drawString(TextUtil.shorten(str, g.getFontMetrics(), 910 - xPos), xPos, 229 + (i * 19));
        }

      //menu        
        g.drawImage(menu1BgImg, 626, 357, c);
        g.drawImage(menu2BgImg, 626, 400, c);
        g.drawImage(menuMiddleBgImg, 626, 436, 284, 40, c);
        g.drawImage(menuLineImg, 641, 397, c);
        g.drawImage(menuLineImg, 641, 436, c);
        g.drawImage(menuGrowImg, 625, 455, c);
        g.drawImage(menuShadowImg, 579, 467, c);
        for (int i = 0; i < menuTxt.length; i++) {
            int addedPos = i * 39;
            g.drawImage(menuArrIconImg, 649, 375 + addedPos, c);
            if (i == menuFocus) {
                g.drawImage(menuFocusImg, 624, 357 + addedPos, c);
                g.setColor(Rs.C003003003);
                g.setFont(Rs.F21);
            } else {
                g.setColor(Rs.C230230230);
                g.setFont(Rs.F18);
            }
            g.drawString(menuTxt[i], 671, 383 + addedPos);
        }
        super.paint(g, c);
    }
}

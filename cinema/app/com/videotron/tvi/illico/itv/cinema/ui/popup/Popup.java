/*
 *  @(#)Popup.java 1.0 2011.02.24
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.Container;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>Popup</code> the super class of each popup.
 *
 * @since   2011.02.24
 * @version $Revision: 1.6 $ $Date: 2011/07/05 18:26:19 $
 * @author  tklee
 * @see     java.awt.Component
 */
public class Popup extends Container {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The data center. */
    protected DataCenter dataCenter = DataCenter.getInstance();

    /** The scene for listener. */
    protected BasicUI scene = null;
    /** The ClickingEffect instance. */
    protected ClickingEffect clickEffect = null;

    /** The mode to identify. */
    protected int mode = 0;

    /**
     * return popup mode.
     *
     * @return popup mode
     */
    public int getMode() {
        return mode;
    }

    /**
     * set popup mode.
     *
     * @param popupMode  popup mode
     */
    public void setMode(int popupMode) {
        this.mode = popupMode;
    }

    /**
     * Reset focus.
     */
    public void resetFocus() {
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
    }

    /**
     * when popup is finished, this is called.
     */
    public void stop() {
    }

    /**
     * Sets the listener.
     *
     * @param listener the listener for callback as BasicUI
     */
    public void setListener(BasicUI listener) {
        scene = listener;
    }

    /**
     * implement handleKey method.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    public boolean handleKey(int code) {
        if (code == KeyCodes.LAST) { return true; }
        if (MainManager.getInstance().isLoadingAnimation()) {
            if (code == Rs.KEY_EXIT) { scene.requestPopupClose(); }
            return true;
        }
        if (keyAction(code)) { return true; }
        if (code == Rs.KEY_EXIT) {
            scene.requestPopupClose();
            return true;
        }
        return false;
    }

    /**
     * Key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        return true;
    }
}

/*
 *  @(#)ShowtimesInFavorite.java 1.0 2011.03.03
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.event.KeyEvent;
import java.util.Vector;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.gui.ShowtimesInFavoriteRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>ShowtimesInFavorite</code> The class to show showtimes of a movie in favorite theatres.
 *
 * @since   2011.03.03
 * @version $Revision: 1.14 $ $Date: 2012/02/23 06:11:11 $
 * @author  tklee
 */
public class ShowtimesInFavorite extends BasicUI implements MenuListener {

    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ID_SHOWTIMES. */
    private static final String ID_SHOWTIMES = "showtimesData";

    /** Is the ShowtimesInFavoriteRenderer instance. */
    private ShowtimesInFavoriteRenderer renderer = null;
    /** The root menu item for D-Option. */
    private final MenuItem rootMenuItem = new MenuItem("D-Option", new MenuItem[] {OPTION_PREFERENCE, OPTION_HELP});

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (renderer == null) {
            renderer = new ShowtimesInFavoriteRenderer();
            setRenderer(renderer);
            clickEffect = new ClickingEffect(this, 5);
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset the reset
     */
    protected void startScene(boolean reset) {
        //buttonIcons.setButtonIds(new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_OPTION});
        addFooterBtn(new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_OPTION});

        prepare();
        if (reset) {
            renderer.btnFocus = 0;
            renderer.btnPosition = 0;
            renderer.langType = "";
        } else if (focusHistory != null && focusHistory.length > 1) {
            renderer.btnFocus = Integer.parseInt((String) focusHistory[0]);
            renderer.btnPosition = Integer.parseInt((String) focusHistory[1]);
            renderer.langType = (String) focusHistory[1];
        }
        setVisible(true);
        loadData(new Object[]{ID_SHOWTIMES}, true);
    }

    /**
     * Gets the focus history.
     */
    protected void setFocusHistory() {
        Object[] objs = new Object[3];
        objs[0] = String.valueOf(renderer.btnFocus);
        objs[1] = String.valueOf(renderer.btnPosition);
        objs[2] = renderer.langType;
        setFocusHistory(objs);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        renderer.stop();
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * Dispose scene.
     */
    protected void disposeScene() {
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        Log.printDebug("[TheatreSelect]keyAction." + keyCode);

        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (renderer.btnFocus > 0) {
                renderer.btnFocus--;
                if (renderer.btnPosition > 0) { renderer.btnPosition--; }
                repaint();
            }
            break;
        case KeyEvent.VK_DOWN:
            int totalCount = renderer.theatreList.size();
            if (renderer.btnFocus < totalCount - 1) {
                renderer.btnFocus++;
                if (renderer.btnPosition < 1) { renderer.btnPosition++; }
                repaint();
            }
            break;
        case HRcEvent.VK_ENTER:
            clickEffect.start(309, 233 + (renderer.btnPosition * 123), 215, 97);
            Object[] paras =
                new Object[]{((TheatreData) renderer.theatreList.elementAt(renderer.btnFocus)).getTheatreID(),
                    renderer.filmInfoData.getId(), renderer.langType};
            Object[] values = {getCurrentSceneId(), paras, null};
            SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_THEATRE_ASSET, true, values);
            break;
        case KeyCodes.LAST:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
            break;
        case KeyCodes.COLOR_D:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_OPTION]);
            if (optScr != null) { optScr.start(rootMenuItem, this); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * D-Option canceled.
     */
    public void canceled() {
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * D-Option Selected.
     *
     * @param item MenuItem
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }

        if (item.equals(OPTION_PREFERENCE)) {
            Object[] values = {getCurrentSceneId(), null, null};
            SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_PREFERENCE, true, values);
        } else if (item.equals(OPTION_HELP)) {
            MainManager.getInstance().requestStartHelpApp();
        }
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        String procId = (String) obj[0];
        Logger.debug(this, "called notifyFromDataLoadingThread() - procId : " + procId);
        DataManager dataManager = DataManager.getInstance();
        if (procId.equals(ID_SHOWTIMES)) {
            String dataId = "";
            String duration = "";
            if (sendValue instanceof FilmData) {
                FilmData data = (FilmData) sendValue;
                dataId = data.getID();
                duration = data.getDuration();
                renderer.langType = data.getLangue();
            } else {
                dataId = ((RecentFilmData) sendValue).getID();
                RecentFilmData recentData = (RecentFilmData) sendValue;
                renderer.langType = FilmData.LANGUAGE_FRENCH;
                if (recentData.getTitle() == null || recentData.getTitle().length() == 0) {
                    renderer.langType = FilmData.LANGUAGE_ENGLISH;
                }

                if (DataManager.allMovie.isEmpty()) { dataManager.getFilmsData("tous", true); }
                FilmData data = (FilmData) DataManager.allMovie.get(dataId + renderer.langType);
                if (data != null) { duration = data.getDuration(); }
            }
            FilmInfoData infoData = dataManager.getFilmInfoData(dataId);
            if (infoData == null) {
                showNotiPopup();
                return;
            }
            Logger.debug(this, "called notifyFromDataLoadingThread() - after infoDAta : ");
            infoData.setDuration(duration);

            Vector favoriteList = Rs.vfavoriteTheatre;
            Vector theaList = new Vector();
            for (int i = 0; i < favoriteList.size(); i++) {
                TheatreData favorData = (TheatreData) favoriteList.elementAt(i);
                //TheatreData theatreData = dataManager.getTheatresInfo(((String[]) favoriteList.elementAt(i))[0]);
                TheatreData theatreData = dataManager.getTheatresInfo(favorData.getTheatreID());
                if (theatreData != null) { theaList.addElement(theatreData); }
            }
            Logger.debug(this, "called notifyFromDataLoadingThread() - after theatreData : ");

            if (theaList.isEmpty()) {
                showNotiPopup();
                return;
            }
            setPosterImage(infoData, -1);
            Logger.debug(this, "called notifyFromDataLoadingThread() - after setPosterImage");
            if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
                FrameworkMain.getInstance().getImagePool().clear();
                return;
            }
            MainManager.getInstance().hideLoadingAnimation();
            renderer.filmInfoData = infoData;
            renderer.theatreList = theaList;
            startComp();
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
        repaint();
    }

    /**
     * Show noti popup.
     */
    private void showNotiPopup() {
        MainManager.getInstance().hideLoadingAnimation();
        /*int popupMode = NotiPopup.MODE_ERR_NO_DATA;
        if (DataManager.getInstance().isError) { popupMode = NotiPopup.MODE_ERR_LOAD; }
        notiPopup.setMode(popupMode);
        notiPopup.setGoPrev(true);
        addPopup(notiPopup);*/
        if (!checkErrorMessageShow()) {
            DataManager.getInstance().errorType = DataManager.ERR_XML_PARSING;
            DataManager.getInstance().printError(null);
        }
        showErrorMessage(ERRMSG_PREV);
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            keyAction(KeyCodes.LAST);
        } else { super.requestPopupOk(para); }
    }
}

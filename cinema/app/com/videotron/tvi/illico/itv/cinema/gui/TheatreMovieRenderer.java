/*
 *  @(#)TheatreMovieRenderer.java 1.0 2011.03.01
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.ui.TheatreMovie;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>TheatreMovieRenderer</code> UI Renderer for TheatreMovie class.
 *
 * @since   2011.03.01
 * @version $Revision: 1.13 $ $Date: 2011/06/24 23:21:24 $
 * @author  tklee
 */
public class TheatreMovieRenderer extends BasicRenderer {
    /** The bg line. */
    private Image bgLineImg = null;
    /** The menu1 bg. */
    private Image menu1BgImg = null;
    /** The menu2 bg. */
    private Image menu2BgImg = null;
    /** The menu shadow. */
    private Image menuShadowImg = null;
    /** The menu grow. */
    private Image menuGrowImg = null;
    /** The menu line. */
    private Image menuLineImg = null;
    /** The menu arrow icon. */
    private Image menuArrIconImg = null;
    /** The menu focus. */
    private Image menuFocusImg = null;
    /** The poster focus. */
    public Image posterFocusImg = null;
    /** The poser focus dim image. */
    public Image poserFocusDimImg = null;
    /** The arrow left. */
    public Image arrLImg = null;
    /** The arrow right. */
    public Image arrRImg = null;
    /** The poster shadow. */
    public Image posterShadowImg = null;
    /** The poster dim. */
    public Image posterDimImg = null;
    /** The poster list shadow left. */
    public Image posterListShadowLImg = null;
    /** The poster list shadow right. */
    public Image posterListShadowRImg = null;
    /** The scroll shadow. */
    public Image scrollShadowImg = null;
    /** The scroll shadow top img. */
    public Image scrollShadowTopImg = null;
    /** The menu middle bg img. */
    private Image menuMiddleBgImg = null;
    /** The card bg img. */
    private Image cardBgImg = null;
    /** The access icon img. */
    private Image[] accessIconImg = new Image[4];
    /** The default poster img. */
    public Image defaultPosterImg = null;
    /** The default focus poster img. */
    public Image defaultFocusPosterImg = null;

    /** Scene Title String. */
    //private String sceneTitle = getString("cine.title");
    /** Movie title String. */
    private String title = getString("cine.theatreMovieTitle");
    /** The sort fr txt. */
    private String sortFrTxt = getString("cine.SortFR");
    /** The sort en txt. */
    private String sortEnTxt = getString("cine.SortEN");
    /** The released txt. */
    private String releaseDateTxt = getString("cine.showtimesFrom");
    /** The menu txt. */
    public String[] menuTxt = new String[3];
    /** The sort alphabetical txt. */
    private String sortAlphaTxt = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgLineImg = getImage("01_vod_bgline.png");
        menu1BgImg = getImage("01_acbg_1st.png");
        menu2BgImg = getImage("01_acbg_2nd.png");
        menuShadowImg = getImage("01_ac_shadow.png");
        menuGrowImg = getImage("01_acgrow.png");
        menuLineImg = getImage("01_acline.png");
        menuArrIconImg = getImage("02_detail_ar.png");
        menuFocusImg = getImage("02_detail_bt_foc.png");
        posterFocusImg = getImage("01_nor_foc_v.png");
        poserFocusDimImg = getImage("01_nor_foc_v_dim.png");
        arrLImg = getImage("01_arrow_l.png");
        arrRImg = getImage("01_arrow_r.png");
        posterShadowImg = getImage("01_possh_96.png");
        posterDimImg = getImage("01_poshigh_96.png");
        posterListShadowLImg = getImage("01_poster_sha_l.png");
        posterListShadowRImg = getImage("01_poster_sha_r.png");
        scrollShadowImg = getImage("01_des_sh_bun.png");
        scrollShadowTopImg = getImage("01_des_sh_bun_t.png");
        menuMiddleBgImg = getImage("01_acbg_m.png");
        cardBgImg = getImage("11_card_bg.png");
        for (int i = 0; i < accessIconImg.length; i++) {
            accessIconImg[i] = getImage("11_icon_tinfo0" + (i + 1) + ".png");
        }
        defaultPosterImg = getImage("11_default_96.png");
        defaultFocusPosterImg = getImage("11_default_144.png");
        super.prepare(c);

        title = getString("cine.theatreMovieTitle");
        sortFrTxt = getString("cine.SortFR");
        sortEnTxt = getString("cine.SortEN");
        releaseDateTxt = getString("cine.showtimesFrom");
        menuTxt[0] = getString("cine.moreInfo");
        menuTxt[1] = getString("cine.changeThea");
        menuTxt[2] = getString("cine.seeMoreShowtimes");
        //menuTxt[2] = getString("cine.seeAllMovies");
        sortAlphaTxt = DataCenter.getInstance().getString("cine.SortAlpa");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("01_vod_bgline.png");
        removeImage("01_acbg_1st.png");
        removeImage("01_acbg_2nd.png");
        removeImage("01_ac_shadow.png");
        removeImage("01_acgrow.png");
        removeImage("01_acline.png");
        removeImage("02_detail_ar.png");
        removeImage("02_detail_bt_foc.png");
        removeImage("01_nor_foc_v.png");
        removeImage("01_arrow_l.png");
        removeImage("01_arrow_r.png");
        removeImage("01_possh_96.png");
        removeImage("01_poshigh_96.png");
        removeImage("01_poster_sha_l.png");
        removeImage("01_poster_sha_r.png");
        removeImage("01_des_sh_bun.png");
        removeImage("01_hotkeybg.png");
        removeImage("11_card_bg.png");
        for (int i = 0; i < accessIconImg.length; i++) {
            removeImage("11_icon_tinfo0" + (i + 1) + ".png");
        }*/
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        TheatreMovie ui = (TheatreMovie) c;
        Vector filmList = ui.filmList;
        if (filmList == null) { return; }
        g.drawImage(bgLineImg, 0, 250, c);
        FilmData focusFilm = (FilmData) filmList.elementAt(ui.posterFocus);
        FilmInfoData filmInfo = null;
        Image posterImage = null;
        if (focusFilm != null) { filmInfo = focusFilm.getFilmInfoData(); }
        String str = "";
        int pos = 0;
      //poster
        /*int totalCount = filmList.size();
        int initNumber = 0;
        int halfPos = posterPerPage / 2;
        //int focusPos = halfPos + (posterPerPage % 2);
        if (totalCount >= posterPerPage) {
            initNumber =  posterFocus - halfPos;
            if (posterFocus < halfPos) {  initNumber = totalCount - (halfPos - posterFocus); }
        } else {
            if (posterFocus > halfPos) { initNumber = posterFocus - halfPos; }
        }
        int posterGap = 103;
        for (int i = initNumber, j = 0; j < posterPerPage; j++) {
            if (j == halfPos) {
                if (i > totalCount - 1) { i = 0; }
                i++;
                continue;
            } else if (totalCount < posterPerPage) {
                if (j < halfPos && halfPos - posterFocus > j) {
                    continue;
                } else if (i > totalCount - 1) { break; }
            }
            if (i > totalCount - 1) { i = 0; }

            FilmData film = (FilmData) filmList.elementAt(i);
            Image posterImage = null;
            if (film != null && film.getFilmInfoData() != null) {
                posterImage = film.getFilmInfoData().getPosterImage();
            }
            if (posterImage == null) { posterImage = defaultPosterImg; }
            if (posterImage != null) {
                int posterPos = (78 - posterGap) + (j * posterGap);
                if (j > halfPos) { posterPos = 580 + ((j - halfPos - 1) * posterGap); }
                g.drawImage(posterImage, posterPos, 132, 96, 144, c);
                g.drawImage(posterDimImg, posterPos, 132, 96, 141, c);
                g.drawImage(posterShadowImg, posterPos - 11, 274, c);
            }
            i++;
        }

        if (!isRightFocus) {
            g.drawImage(posterFocusImg, 405, 104, c);
            if (totalCount >= posterPerPage || posterFocus > 0) { g.drawImage(arrLImg, 385, 191, c); }
            if (totalCount >= posterPerPage || posterFocus < totalCount - 1) { g.drawImage(arrRImg, 557, 191, c); }
        } else { g.drawImage(poserFocusDimImg, 405, 104, c); }
        g.drawImage(posterListShadowLImg, 0, 6, c);
        g.drawImage(posterListShadowRImg, 856, 6, c);
        //g.drawImage(hotkeybgImg, 236, 466, c);
        FilmData focusFilm = (FilmData) filmList.elementAt(posterFocus);
        FilmInfoData filmInfo = null;
        Image posterImage = null;
        if (focusFilm != null) {
            filmInfo = focusFilm.getFilmInfoData();
            if (filmInfo != null) { posterImage = filmInfo.getPosterImage(); }
        }
        if (posterImage == null) { posterImage = defaultFocusPosterImg; }
        if (posterImage != null) { g.drawImage(posterImage, 408, 107, 144, 216, c); }
        int addWidth = 0;
        if (totalCount >= 100) { addWidth = 15; }
        g.setColor(Rs.C252202004);
        if (isRightFocus) { g.setColor(Rs.C192); }
        g.fillRect(505 - addWidth, 306, 47 + addWidth, 17);
        g.setFont(Rs.F16);
        g.setColor(Rs.C074072064);
        String str = String.valueOf(totalCount);
        if (totalCount < 10) { str = "0" + str; }
        str = "/" + str;
        GraphicUtil.drawStringRight(g, str, 549, 320);
        int pos = 549 - g.getFontMetrics().stringWidth(str);
        g.setColor(Rs.C000000000);
        str = String.valueOf(posterFocus + 1);
        if (posterFocus < 9) { str = "0" + str; }
        GraphicUtil.drawStringRight(g, str, pos, 320);
        g.setFont(Rs.F23);
        g.setColor(Rs.C255205012);
        GraphicUtil.drawStringCenter(g, focusFilm.getTitle(), 480, 348);
*/
        //title
        g.setColor(Rs.C255255255);
        g.setFont(Rs.F26);
        g.drawString(title, 61, 98);
        g.setFont(Rs.F18);
        if (ui.sortType == FilmData.SORT_LANG) {
            str = sortFrTxt;
            if (Rs.titleFirstLanguage.equals(Definitions.LANGUAGE_ENGLISH)) { str = sortEnTxt; }
        } else { str = sortAlphaTxt; }
        GraphicUtil.drawStringRight(g, str, 908, 95);

        g.setColor(Rs.C182182182);
        g.setFont(Rs.F16);
        str = releaseDateTxt;
        //if (filmInfo != null) { str += " [" + filmInfo.getReleaseDate() + "]"; }
        if (filmInfo != null) { str += " " + filmInfo.getReleaseDate(); }
        g.drawString(str, 61, 366);

        g.drawImage(cardBgImg, 339, 357, c);
        boolean[] isIcons = {checkShowIcon(ui.theatreInfo.getWheelchair()), checkShowIcon(ui.theatreInfo.getBus()),
                checkShowIcon(ui.theatreInfo.getMetro()), checkShowIcon(ui.theatreInfo.getHearingImpaired())};
        for (int i = 0, j = 0; i < isIcons.length; i++) {
            if (isIcons[i]) {
                g.drawImage(accessIconImg[i], 533 + ((j % 2) * 38), 397 + ((j / 2) * 37), c);
                j++;
            }
        }
        g.setColor(Rs.C255255255);
        g.setFont(Rs.F20);
        g.drawString(TextUtil.shorten(ui.theatreInfo.getDefaultName(), g.getFontMetrics(), 604 - 352), 352, 383);

        g.setFont(Rs.F16);
        String[] arrStr = TextUtil.split(ui.theatreInfo.getAddress(), g.getFontMetrics(), 524 - 352);
        int checkLength = arrStr.length;
        if (checkLength > 5) { checkLength = 5; }
        for (int i = 0; i < checkLength; i++) {
            g.drawString(arrStr[i], 352, 408 + (i * 14));
        }

      //menu
        g.drawImage(menu1BgImg, 626, 357, c);
        g.drawImage(menu2BgImg, 626, 400, c);
        g.drawImage(menuMiddleBgImg, 626, 436, 284, 40, c);
        g.drawImage(menuLineImg, 641, 397, c);
        g.drawImage(menuLineImg, 641, 436, c);
        g.drawImage(menuGrowImg, 625, 455, c);
        g.drawImage(menuShadowImg, 579, 467, c);
        for (int i = 0; i < menuTxt.length; i++) {
            int addedPos = i * 39;
            g.drawImage(menuArrIconImg, 649, 375 + addedPos, c);
            if (ui.isRightFocus && i == ui.menuFocus) {
                g.drawImage(menuFocusImg, 624, 357 + addedPos, c);
                g.setColor(Rs.C003003003);
                g.setFont(Rs.F21);
            } else {
                g.setColor(Rs.C230230230);
                g.setFont(Rs.F18);
            }
            g.drawString(menuTxt[i], 671, 383 + addedPos);
        }
        super.paint(g, c);
    }

    /**
     * Checks if is show icon.
     *
     * @param type the type
     * @return true, if is show icon
     */
    private boolean checkShowIcon(String type) {
        return TheatreData.STR_TRUE.equals(type);
    }
}

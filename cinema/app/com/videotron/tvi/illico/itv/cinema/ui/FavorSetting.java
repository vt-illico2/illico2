/*
 *  @(#)FavorSetting.java 1.0 2011.03.05
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Vector;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.PreferenceProxy;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.gui.FavorSettingRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ListCompUI;
import com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener;
import com.videotron.tvi.illico.itv.cinema.ui.effect.ListBuilderEffect;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>FavorSetting</code> The class to set favortie theatres.
 *
 * @since   2011.03.05
 * @version $Revision: 1.17 $ $Date: 2012/02/23 06:11:11 $
 * @author  tklee
 */
public class FavorSetting extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant STATE_THEATRE. */
    public static final int STATE_THEATRE = 0;
    /** The Constant STATE_BTN. */
    public static final int STATE_BTN = 1;
    /** The Constant STATE_FAVORITE. */
    public static final int STATE_FAVORITE = 2;

    /** The Constant BTN_NONE. */
    public static final int BTN_NONE = -1;
    /** The Constant BTN_ADD. */
    public static final int BTN_ADD = 0;
    /** The Constant BTN_REMOVE. */
    public static final int BTN_REMOVE = 1;

    /** The Constant ROWS_PER_PAGE. */
    public static final int ROWS_PER_PAGE = 9;
    /** The Constant MIDDLE_POS. */
    public static final int MIDDLE_POS = 4;
    /** The Constant MAX_FAVORITE. */
    public static final int MAX_FAVORITE = 5;

    /** Is the MovieInTheatreRenderer instance. */
    public FavorSettingRenderer renderer = null;
    /** The list component. */
    private ListCompUI listComp = null;

    /** The btn id left. */
    private final int[] btnIdLeft = {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_PAGE_UPDWON};
    /** The btn id left favor. */
    private final int[] btnIdLeftFavor = {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_PAGE_UPDWON,
            ButtonIcons.BUTTON_CLEAR_FAVOR};
    /** The btn id right. */
    private final int[] btnIdRight = {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_CLEAR_FAVOR};
    /** The btn id right order. */
    private final int[] btnIdRightOrder = {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_EDIT_ORDER,
            ButtonIcons.BUTTON_CLEAR_FAVOR};
    /** The btn id button. */
    private final int[] btnIdBtn = {ButtonIcons.BUTTON_BACK};

    /** The theatre list. */
    public Vector list = null;
    /** The favor list. */
    public Vector favorList = null;
    /** The opened region id. */
    public Hashtable openedRegionId = new Hashtable();
    /** The theatre total count. */
    public int theatreTotalCount = 0;
    /** The state focus. */
    public int stateFocus = 0;
    /** The theatre focus. */
    public int theatreFocus = 0;
    /** The focus position. */
    public int focusPosition = 0;
    /** The favor focus. */
    public int favorFocus = 0;
    /** The btn state. */
    public int btnState = FavorSetting.BTN_NONE;
    /** The is edit order. */
    public boolean isEditOrder = false;

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (renderer == null) {
            renderer = new FavorSettingRenderer();
            setRenderer(renderer);
            clickEffect = new ClickingEffect(this, 5);
            ListCompRenderer listCompRenderer = new ListCompRenderer();
            listComp = new ListCompUI();
            listComp.setBounds(0, 0, 960, 540);
            listComp.setRenderer(listCompRenderer, new ListBuilderEffect(listComp, 8, listCompRenderer));
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset the reset
     */
    protected void startScene(boolean reset) {
        prepare();

        if (reset) {
            openedRegionId.clear();
            stateFocus = 0;
            theatreFocus = 0;
            focusPosition = 0;
            favorFocus = 0;
            btnState = BTN_NONE;
            isEditOrder = false;
        }
        setVisible(true);
        loadData(new Object[]{ID_REGION_DATA}, true);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        renderer.stop();
        list = null;
        favorList = null;
        openedRegionId.clear();
    }

    /**
     * Dispose scene.
     */
    protected void disposeScene() {
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        Log.printDebug("[TheatreSelect]keyAction." + keyCode);

        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (stateFocus == STATE_THEATRE) {
                if (theatreFocus > 0) {
                    if (focusPosition != MIDDLE_POS || theatreFocus == focusPosition) { focusPosition--; }
                    theatreFocus--;
                    checkBtnState();
                    repaint();
                }
            } else if (stateFocus == STATE_FAVORITE) {
                if (favorFocus > 0) {
                    if (isEditOrder) {
                        Object obj = favorList.remove(favorFocus);
                        favorList.add(favorFocus - 1, obj);
                    }
                    favorFocus--;
                    repaint();
                }
            }
            break;
        case KeyEvent.VK_DOWN:
            if (stateFocus == STATE_THEATRE) {
                int totalCount = list.size();
                if (theatreFocus < totalCount - 1) {
                    if (focusPosition != MIDDLE_POS || focusPosition + theatreFocus >= totalCount - 1) {
                        focusPosition++;
                    }
                    theatreFocus++;
                    checkBtnState();
                    repaint();
                }
            } else if (stateFocus == STATE_FAVORITE) {
                int totalCount = favorList.size();
                if (favorFocus < totalCount - 1) {
                    if (isEditOrder) {
                        Object obj = favorList.remove(favorFocus);
                        favorList.add(favorFocus + 1, obj);
                    }
                    favorFocus++;
                    repaint();
                }
            }
            break;
        case Rs.KEY_LEFT:
            if (stateFocus > 0) {
                stateFocus--;
                if (stateFocus == STATE_THEATRE) { checkBtnState(); }
                isEditOrder = false;
                setButtonIcon();
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (stateFocus == STATE_FAVORITE) { break; }
            if (favorList.size() == 0) {
                if (stateFocus == STATE_BTN || list.elementAt(theatreFocus) instanceof RegionInfo) { break; }
            }
            stateFocus++;
            if (list.elementAt(theatreFocus) instanceof RegionInfo || !checkAdding()) {
                stateFocus = STATE_FAVORITE;
            }
            if (stateFocus == STATE_BTN) {
                btnState = BTN_ADD;
            } else { btnState = BTN_REMOVE; }
            isEditOrder = false;
            setButtonIcon();
            repaint();
            break;
        case HRcEvent.VK_ENTER:
            keyOk();
            setButtonIcon();
            break;
        case HRcEvent.VK_PAGE_UP:
            if (stateFocus != STATE_THEATRE) { break; }
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_PAGE_UPDWON]);
            if (focusPosition >= MIDDLE_POS) {
                int initNum = theatreFocus - focusPosition;
                focusPosition = MIDDLE_POS;
                if (initNum - ROWS_PER_PAGE >= 0) {
                    theatreFocus = initNum - ROWS_PER_PAGE + MIDDLE_POS;
                } else { theatreFocus = MIDDLE_POS; }
                checkBtnState();
                repaint();
            }
            break;
        case HRcEvent.VK_PAGE_DOWN:
            if (stateFocus != STATE_THEATRE) { break; }
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_PAGE_UPDWON]);
            if (focusPosition <= MIDDLE_POS) {
                int totalCount = list.size();
                int lastNum = 0;
                if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = theatreFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                focusPosition = MIDDLE_POS;
                if (lastNum + ROWS_PER_PAGE < totalCount) {
                    theatreFocus = lastNum + ROWS_PER_PAGE - MIDDLE_POS;
                } else { theatreFocus = totalCount - 1 - MIDDLE_POS; }
                checkBtnState();
                repaint();
            }
            break;
        case KeyCodes.LAST:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
            break;
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_C:
            if (!Rs.IS_EMULATOR && keyCode == KeyCodes.COLOR_C) { break; }
            if (stateFocus == STATE_FAVORITE && favorList.size() > 1) {
                footer.clickAnimation(btnName[ButtonIcons.BUTTON_EDIT_ORDER]);
                if (isEditOrder) {
                    isEditOrder = false;
                } else { isEditOrder = true; }
                repaint();
            }
            break;
        case KeyCodes.COLOR_B:
            Logger.debug(this, "keyCode : KeyCodes.COLOR_B");
            if (favorList.size() > 0) {
                footer.clickAnimation(btnName[ButtonIcons.BUTTON_CLEAR_FAVOR]);
                while (favorList.size() > 0) {
                    TheatreData removeData = (TheatreData) favorList.remove(focus);
                    addTheatre(removeData);
                    Rs.hfavoriteTheatre.remove(removeData.getTheatreID());
                }
                setPreference();
                favorFocus = 0;
                stateFocus = STATE_THEATRE;
                isEditOrder = false;
                setButtonIcon();
                checkBtnState();
                repaint();
            }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Check btn state.
     */
    private void checkBtnState() {
        if (list.elementAt(theatreFocus) instanceof RegionInfo) {
            btnState = BTN_NONE;
        } else {
            btnState = BTN_ADD;
            if (!checkAdding()) { btnState = BTN_NONE; }
        }
    }

    /**
     * add theatre from removing of favorite.
     *
     * @param addData the add data
     */
    private void addTheatre(TheatreData addData) {
        TheatreData newData = (TheatreData) DataManager.theatreData.get(addData.getTheatreID());
        if (newData != null) {
            String regionId = newData.getRegionID();
            RegionInfo regionData = (RegionInfo) DataManager.region.get(regionId);
            Vector theatreList = regionData.getTheatreListForFavor();
            int theatreSize = theatreList.size();
            theatreList.add(newData);
            Collections.sort(theatreList);
            theatreTotalCount++;

            if (openedRegionId.containsKey(regionId)) {
                for (int i = 0; i < list.size(); i++) {
                    Object obj = list.elementAt(i);
                    if (obj.equals(regionData)) {
                        for (int j = 0; j < theatreSize; j++) {
                            list.removeElementAt(i + 1);
                        }
                        list.addAll(i + 1, theatreList);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Removes the favorite.
     *
     * @param focus the focus
     */
    /*private void removeFavorite2(int focus) {
        TheatreData removeData = (TheatreData) favorList.remove(focus);
        Rs.hfavoriteTheatre.remove(removeData.getTheatreID());
        //if (renderer.favorFocus > 0) { renderer.favorFocus--; }
        if (favorFocus > 0 && favorFocus > favorList.size() - 1) { favorFocus--; }
        if (favorList.isEmpty()) {
            stateFocus = STATE_THEATRE;
            checkBtnState();
        }
    }*/

    /**
     * Key ok.
     */
    private void keyOk() {
        if (stateFocus == STATE_THEATRE) {
            Object item = list.elementAt(theatreFocus);
            if (item instanceof RegionInfo) {
                RegionInfo regionInfo = (RegionInfo) item;
                String regionId = regionInfo.getRegionId();
                Vector theatreList = regionInfo.getTheatreListForFavor();
                if (theatreList != null && theatreList.size() > 0) {
                    //clickEffect.start(54, 172 + (focusPosition * 32), 400 - 54, 272 - 236);
                    if (openedRegionId.containsKey(regionId)) {
                        for (int i = 0; i < theatreList.size(); i++) {
                            list.removeElementAt(theatreFocus + 1);
                        }
                        openedRegionId.remove(regionId);

                        if (focusPosition >= MIDDLE_POS) {
                            int listSize = list.size();
                            if (listSize - theatreFocus < ROWS_PER_PAGE - focusPosition) {
                                if (listSize - theatreFocus > MIDDLE_POS) {
                                    focusPosition = MIDDLE_POS;
                                } else { focusPosition = ROWS_PER_PAGE - (listSize - theatreFocus); }
                            }
                        }
                    } else {
                        list.addAll(theatreFocus + 1, theatreList);
                        openedRegionId.put(regionId, "");

                        if (focusPosition > MIDDLE_POS) {
                            int listSize = list.size();
                            if (listSize - theatreFocus > ROWS_PER_PAGE - focusPosition) {
                                if (listSize - theatreFocus > MIDDLE_POS) {
                                    focusPosition = MIDDLE_POS;
                                } else { focusPosition = ROWS_PER_PAGE - (listSize - theatreFocus); }
                            }
                        }
                    }
                    repaint();
                }
            } else {
                if (checkAdding()) {
                    //clickEffect.start(54, 172 + (focusPosition * 32), 400 - 54, 272 - 236);
                    processAdding();
                }
            }
        } else if (stateFocus == STATE_FAVORITE) {
            //clickEffect.start(559, 172 + (favorFocus * 32), 400 - 54, 272 - 236);
            //String[] removeData = (String[]) favorList.remove(renderer.favorFocus);
            if (isEditOrder) {
                isEditOrder = false;
                repaint();
            } else { processRemovement(); }
        } else if (stateFocus == STATE_BTN) {
            //clickEffect.start(410, 268 + (btnState * 43), 549 - 410, 300 - 268);
            if (btnState == BTN_ADD) {
                processAdding();
            } else { processRemovement(); }
        }
    }

    /**
     * Check adding.
     *
     * @return true, if successful
     */
    private boolean checkAdding() {
        Object item = list.elementAt(theatreFocus);
        TheatreData theatreData = (TheatreData) item;
        return favorList.size() < MAX_FAVORITE && !Rs.hfavoriteTheatre.containsKey(theatreData.getTheatreID());
    }

    /**
     * Process adding into favorites.
     */
    private void processAdding() {
        Object item = list.elementAt(theatreFocus);
        TheatreData theatreData = (TheatreData) item;
        if (checkAdding()) {
            Rectangle leftSrc = null;
            Rectangle rightSrc = null;
            Point leftFrom = null;
            Point leftTo = null;
            Point rightFrom = null;
            Point rightTo = null;

            if (list.size() - 1 > theatreFocus) {
                int yPos = 175 + (focusPosition * 32);
                int height = (ROWS_PER_PAGE - focusPosition) * 32;
                list.removeElementAt(theatreFocus);
                leftSrc = new Rectangle(0, yPos, 406, height);
                leftFrom = new Point(0, yPos + 32);
                leftTo = new Point(0, yPos);
            } else {
                leftSrc = new Rectangle(0, 0, 406, 540);
                focusPosition--;
                list.removeElementAt(theatreFocus);
                theatreFocus--;
            }

            int favorCount = favorList.size();
            int yPos = 175 + (favorCount * 32);
            int height = (MAX_FAVORITE - favorCount) * 32;
            rightSrc = new Rectangle(557, yPos, 353, height);
            rightFrom = new Point(557, yPos);
            rightTo = new Point(557, yPos + 32);
            listComp.startEffect(ListBuilderEffect.EFFECT_ADD, leftSrc, rightSrc, leftFrom, leftTo, rightFrom, rightTo);

            RegionInfo regionInfo = (RegionInfo) DataManager.region.get(theatreData.getRegionID());
            regionInfo.getTheatreListForFavor().remove(theatreData);
            favorList.addElement(theatreData);
            Rs.hfavoriteTheatre.put(theatreData.getTheatreID(), theatreData);
            theatreTotalCount--;
            if (favorList.size() == MAX_FAVORITE) {
                stateFocus = STATE_THEATRE;
                checkBtnState();
            }
            setPreference();
            repaint();
        }
    }

    /**
     * Process removement from favorites.
     */
    private void processRemovement() {
        TheatreData removeData = (TheatreData) favorList.remove(favorFocus);
        String regionId = removeData.getRegionID();
        RegionInfo regionData = (RegionInfo) DataManager.region.get(regionId);

        Rectangle leftSrc = null;
        Rectangle rightSrc = null;
        Point leftFrom = null;
        Point leftTo = null;
        Point rightFrom = null;
        Point rightTo = null;

        int yPos = 175 + (favorFocus * 32);
        int height = (MAX_FAVORITE - favorFocus) * 32;
        Rs.hfavoriteTheatre.remove(removeData.getTheatreID());
        if (favorFocus > 0 && favorFocus > favorList.size() - 1) { favorFocus--; }
        rightSrc = new Rectangle(557, yPos, 353, height);
        rightFrom = new Point(557, yPos + 32);
        rightTo = new Point(557, yPos);

        int newFocus = -1;
        if (openedRegionId.containsKey(regionId)) {
            for (int i = 0; i < list.size(); i++) {
                Object obj = list.elementAt(i);
                if (obj.equals(regionData)) {
                    while (true) {
                        i++;
                        Logger.debug(this, "i : " + i);
                        if (i == list.size()) { break; }
                        obj = list.elementAt(i);
                        if (obj instanceof TheatreData) {
                            Logger.debug(this, "obj equal threatreData");
                            String selfName = removeData.getDefaultName();
                            String paraName = ((TheatreData) obj).getDefaultName();
                            int compareValue = selfName.compareToIgnoreCase(paraName);
                            Logger.debug(this, "compareValue : " + compareValue);
                            if (compareValue <= 0) {
                                //newFocus = i;
                                break;
                            }
                        } else { break; }
                    }
                    if (newFocus == -1) { newFocus = i; }
                    break;
                }
            }

            int initNum = theatreFocus - focusPosition;
            int lastNum = 0;
            int totalCount = list.size();
            if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
            lastNum = theatreFocus + ROWS_PER_PAGE - 1 - focusPosition;
            if (lastNum >= totalCount) { lastNum = totalCount - 1; }
            if (newFocus >= initNum && newFocus <= lastNum) {
                yPos = 175 + ((newFocus - initNum) * 32);
                height = (lastNum - newFocus + 1) * 32;
                leftSrc = new Rectangle(0, yPos, 406, height);
                leftFrom = new Point(0, yPos);
                leftTo = new Point(0, yPos + 32);
            }
        }
        if (leftSrc == null) { leftSrc = new Rectangle(0, 0, 406, 540); }
        listComp.startEffect(ListBuilderEffect.EFFECT_REMOVE, leftSrc, rightSrc, leftFrom, leftTo, rightFrom, rightTo);
        if (favorList.isEmpty()) {
            stateFocus = STATE_THEATRE;
            checkBtnState();
        }
        addTheatre(removeData);
        setPreference();
        repaint();
    }

    /**
     * Sets the button icon.
     */
    private void setButtonIcon() {
        int[] btnId = null;
        if (stateFocus == STATE_THEATRE) {
            if (Rs.vfavoriteTheatre.size() > 0) {
                btnId = btnIdLeftFavor;
            } else { btnId = btnIdLeft; }
        } else if (stateFocus == STATE_BTN) {
            if (Rs.vfavoriteTheatre.size() > 0) {
                btnId = btnIdRight;
            } else { btnId = btnIdBtn; }
        } else {
            if (Rs.vfavoriteTheatre.size() > 1) {
                btnId = btnIdRightOrder;
            } else { btnId = btnIdRight; }
        }
        addFooterBtn(btnId);
    }

    /**
     * Sets the preference for favorite.
     */
    private void setPreference() {
        String upStr = "";
        for (int i = 0; i < Rs.vfavoriteTheatre.size(); i++) {
            TheatreData data = (TheatreData) Rs.vfavoriteTheatre.elementAt(i);
            if (i != 0) { upStr += "|"; }
            upStr += data.getTheatreID() + "^" + data.getDefaultName() + "^" + data.getRegionID();
        }
        PreferenceProxy.getInstance().setPreferenceValue(PreferenceProxy.FAVORITE_CINEMA, upStr);
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        String procId = (String) obj[0];
        Logger.debug(this, "called notifyFromDataLoadingThread() - procId : " + procId);
        DataManager dataManager = DataManager.getInstance();
        if (procId.equals(ID_REGION_DATA)) {
            if (isFromThread) {
                for (int i = 0; i < RegionInfo.RESIONID.length; i++) {
                    DataManager.getInstance().getRegionInfo(String.valueOf(RegionInfo.RESIONID[i]));
                }
            }
            if (DataManager.vRegion.isEmpty()) {
                MainManager.getInstance().hideLoadingAnimation();
                if (!checkErrorMessageShow()) {
                    dataManager.errorType = DataManager.ERR_XML_PARSING;
                    dataManager.printError(null);
                }
                showErrorMessage(ERRMSG_PREV);
            } else {
                Vector vRegion = new Vector();
                vRegion.addAll(DataManager.vRegion);
                int totalCount = 0;
                for (int i = 0; i < vRegion.size(); i++) {
                    Vector theaList = ((RegionInfo) vRegion.elementAt(i)).getTheatreListForFavor();
                    if (theaList != null) { totalCount += theaList.size(); }
                }
                MainManager.getInstance().hideLoadingAnimation();
                list = vRegion;
                theatreTotalCount = totalCount;
                favorList = Rs.vfavoriteTheatre;
                setButtonIcon();
                startComp();
                add(listComp);
            }
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
        repaint();
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            keyAction(KeyCodes.LAST);
        } else { super.requestPopupOk(para); }
    }

    /**
     * <code>ListCompRenderer</code> The class to paint a GUI of list component for animation.
     *
     * @since   2011.05.04
     * @version $Revision: 1.17 $ $Date: 2012/02/23 06:11:11 $
     * @author  tklee
     */
    public class ListCompRenderer extends Renderer implements EffectListener {
        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return listComp.getBounds();
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) { }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            //g.translate(-0, -175);
            if (list == null) { return; }
            int totalCount = list.size();
            int favorCount = favorList.size();
            int lineGap = 32;
            if (totalCount > 0) {
                g.setFont(Rs.F18);
                int initNum = theatreFocus - focusPosition;
                int lastNum = 0;
                if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = theatreFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                    int addPos = (j * lineGap);
                    if (i != theatreFocus || stateFocus == STATE_FAVORITE
                            || (stateFocus == STATE_BTN && btnState != BTN_ADD)) {
                        Object item = list.elementAt(i);
                        paintItem(g, c, false, item, addPos);
                    }
                }
                if ((stateFocus == STATE_THEATRE || (stateFocus == STATE_BTN && btnState == BTN_ADD))) {
                    paintItem(g, c, true, list.elementAt(theatreFocus), (focusPosition * lineGap));
                }
                if (!listComp.isAnimation) { requestPaint(g, c); }
            }

            g.setFont(Rs.F18);
            for (int i = 0; i < MAX_FAVORITE; i++) {
                int addPos = (i * lineGap);
                if (i != favorFocus || stateFocus == STATE_THEATRE
                        || (stateFocus == STATE_BTN && btnState != BTN_REMOVE)) {
                    String name = "";
                    if (i < favorCount) {
                        TheatreData data = (TheatreData) favorList.elementAt(i);
                        name = TextUtil.shorten(data.getDefaultName(), g.getFontMetrics(), 890 - 580);
                        g.setColor(Rs.C224224224);
                    } else {
                        name = renderer.emptyTxt;
                        g.setColor(Rs.C81);
                    }
                    g.drawString(name, 580, 197 + addPos);
                }
            }
            if (stateFocus == STATE_FAVORITE || (stateFocus == STATE_BTN && btnState == BTN_REMOVE)) {
                int addPos = favorFocus * lineGap;
                g.setColor(Rs.C000000000);
                String name = renderer.emptyTxt;
                if (favorFocus < favorList.size() && !favorList.isEmpty()) {
                    TheatreData data = (TheatreData) favorList.elementAt(favorFocus);
                    name = TextUtil.shorten(data.getDefaultName(), g.getFontMetrics(), 890 - 580);
                }
                g.drawString(name, 580, 197 + addPos);
                if (isEditOrder) {
                    g.drawImage(renderer.listArrowTopImg, 721, 160 + addPos, c);
                    g.drawImage(renderer.listArrowBottomImg, 721, 205 + addPos, c);
                }
            }
            //g.translate(0, 175);
        }

        /**
         * Paint item.
         *
         * @param g the Graphics
         * @param c the UIComponent
         * @param isFocus the is focus
         * @param item the item
         * @param addPos the add position
         */
        private void paintItem(Graphics g, UIComponent c, boolean isFocus, Object item, int addPos) {
            if (!isFocus) { g.setColor(Rs.C224224224); }
            if (isFocus) { g.setColor(Rs.C000000000); }
            if (item instanceof RegionInfo) {
                RegionInfo regionInfo = (RegionInfo) item;
                String name = TextUtil.shorten(regionInfo.getRegionName(), g.getFontMetrics(), 335 - 77);
                Vector theatreList = regionInfo.getTheatreListForFavor();
                int size = 0;
                if (theatreList != null) { size = theatreList.size(); }
                name += "(" + size + ")";
                g.drawString(name, 77, 197 + addPos);
                Image openIcon = renderer.plusIconImg;
                if (isFocus) { openIcon = renderer.plusIconFocusImg; }
                if (openedRegionId.containsKey(regionInfo.getRegionId())) {
                    openIcon = renderer.minusIconImg;
                    if (isFocus) { openIcon = renderer.minusIconFocusImg; }
                }
                g.drawImage(openIcon, 364, 181 + addPos, c);
            } else if (item instanceof TheatreData) {
                TheatreData theatreData = (TheatreData) item;
                String name = TextUtil.shorten(theatreData.getDefaultName(), g.getFontMetrics(), 385 - 97);
                g.drawString(name, 97, 197 + addPos);
            }
        }

        /**
         * implements abstract method of EffectListener.
         *
         * @param g the Graphics
         * @param c the Component
         * @see com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener#requestPaint
         * (java.awt.Graphics, java.awt.Component)
         */
        public void requestPaint(Graphics g, Component c) {
            if (list == null) { return; }
            int totalCount = list.size();
            if (totalCount > 0) {
                int initNum = theatreFocus - focusPosition;
                int lastNum = 0;
                if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = theatreFocus + ROWS_PER_PAGE - 1 - focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                if (initNum > 0) { g.drawImage(FavorSetting.this.renderer.listTopShadowImg, 55, 172, c); }
                if (lastNum < totalCount - 1) {
                    g.drawImage(FavorSetting.this.renderer.listBottomShadowImg, 54, 405, c);
                }
                if (stateFocus == STATE_THEATRE) {
                    if (initNum > 0) { g.drawImage(FavorSetting.this.renderer.listArrowTopImg, 216, 165, c); }
                    if (lastNum < totalCount - 1) {
                        g.drawImage(FavorSetting.this.renderer.listArrowBottomImg, 216, 456, c);
                    }
                }
            }
        }

        /**
         * implements abstract method of EffectListener.
         *
         * @param g the Graphics
         * @param c the Component
         * @see com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener#requestPrepaint
         * (java.awt.Graphics, java.awt.Component)
         */
        public void requestPrepaint(Graphics g, Component c) { }
    }
}

package com.videotron.tvi.illico.itv.cinema.controller;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.util.Vector;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.ui.AllMovie;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.itv.cinema.ui.CinemaMain;
import com.videotron.tvi.illico.itv.cinema.ui.FavorSetting;
import com.videotron.tvi.illico.itv.cinema.ui.MovieInfo;
import com.videotron.tvi.illico.itv.cinema.ui.PrefSetting;
import com.videotron.tvi.illico.itv.cinema.ui.ShowtimesInFavorite;
import com.videotron.tvi.illico.itv.cinema.ui.TheatreMovie;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class is the SceneManager class.
 *
 * @version $Revision: 1.16 $ $Date: 2012/02/15 04:09:58 $
 * @author Pellos
 */
public final class SceneManager {
    /** The Constant NUM_SCENE. */
    private static final int NUM_SCENE = 7;
    /** The Constant SCENE_ID_INVALID. */
    public static final int SCENE_ID_INVALID = -1;
    /** The Constant SCENE_ID_MAIN. */
    public static final int SCENE_ID_MAIN = 0;
    /** The Constant SCENE_ID_TITLE. */
    public static final int SCENE_ID_TITLE = 1;
    /** The Constant SCENE_ID_THEATRE_ASSET. */
    public static final int SCENE_ID_THEATRE_ASSET = 2;
    /** The Constant SCENE_ID_MOVIE_ASSET. */
    public static final int SCENE_ID_MOVIE_ASSET = 3;
    /** The Constant SCENE_ID_MOVIE_SHOWTIMES. */
    public static final int SCENE_ID_MOVIE_SHOWTIMES = 4;
    /** The Constant SCENE_ID_PREFERENCE. */
    public static final int SCENE_ID_PREFERENCE = 5;
    /** The Constant SCENE_ID_THEATRE. */
    public static final int SCENE_ID_FAVORITE = 6;

    /** Instance of SceneManager. */
    private static SceneManager instance = null;
    /** The LayeredUIHandler. */
    private LayeredUIHandler layeredHandler = null;
    /** The LayeredUIHandler for Popup. */
    public LayeredUIHandler popupLayeredHandler = null;
    /** The root container. */
    private Container rootContainer = null;
    /** The root container. */
    public Container popupContainer = null;

    /** Save a Scene. */
    public BasicUI[] scenes = null;
    /** is a current Scene ID. */
    public int curSceneId = SCENE_ID_INVALID;

    /**
     * Save the previous Scene ID.
     *
     * 0 : previous Scene Id.
     * 1 : send a value.
     * 2 : etc.
     */
    private Vector history = new Vector();

    /**
     * Gets the singleton instance of SceneManager.
     * @return SceneManager
     */
    public static synchronized SceneManager getInstance() {
        if (instance == null) { instance = new SceneManager(); }
        return instance;
    }

    /**
     * Constructor.
     */
    private SceneManager() {
    }

    /**
     * Initialize a SceneManager.
     */
    public void init() {
        layeredHandler = new LayeredUIHandler(WindowProperty.CINEMA);
        rootContainer = layeredHandler.getLayeredContainer();
        popupLayeredHandler = new LayeredUIHandler(WindowProperty.ITV_POPUP);
        popupContainer = popupLayeredHandler.getLayeredContainer();
        scenes = new BasicUI[NUM_SCENE];
        scenes[SCENE_ID_MAIN] = createScene(SCENE_ID_MAIN);
        curSceneId = SCENE_ID_INVALID;
    }

    /**
     * start the SceneManage.
     */
    public void start() {
        Log.printInfo("SceneManager start");
        history.clear();
        layeredHandler.activate();
        if (Rs.IS_EMULATOR) {
            new Thread("start()") {
                public void run() {
                    try { Thread.sleep(15000); } catch (Exception e) { e.printStackTrace(); }
                    goToNextScene(SceneManager.SCENE_ID_MAIN, true, null);
                }
            } .start();
        } else { goToNextScene(SceneManager.SCENE_ID_MAIN, true, null); }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainManager.getInstance().resizeScaledVideo(587, 72, 322, 181);
                MainManager.getInstance().changeChannel();
            }
        });
    }

    /**
     * pause the SceneManage.
     */
    public void pause() {
        Log.printInfo("SceneManager|pause() is called");
        popupLayeredHandler.deactivate();
        layeredHandler.deactivate();
        history.clear();
        int oldSceneId = curSceneId;
        curSceneId = SCENE_ID_INVALID;
        if (oldSceneId != SCENE_ID_INVALID) {
            scenes[oldSceneId].stop();
            rootContainer.removeAll();
            popupContainer.removeAll();
        }
        curSceneId = SCENE_ID_INVALID;
        for (int i = 0; scenes != null && i < scenes.length; i++) {
            if (scenes[i] != null) {
                scenes[i].dispose();
                scenes[i] = null;
            }
        }
        FrameworkMain.getInstance().getImagePool().clear();
    }

    /**
     * dispose the SceneManage.
     */
    public void dispose() {
        pause();
        popupLayeredHandler.destroy();
        layeredHandler.destroy();
        rootContainer = null;
        popupContainer = null;
        history = null;
        scenes = null;
    }

    /**
     * go to NextScene.
     *
     * @param reqSceneId Scene id.
     * @param reset whether resources is reset or not
     * @param data  the data of scene
     */
    public void goToNextScene(int reqSceneId, boolean reset, Object[] data) {
        Log.printInfo("reqSceneId = " + reqSceneId + " curSceneId = " + curSceneId);
        if (curSceneId == reqSceneId) { return; }

        //Stop before scene
        if (curSceneId != SCENE_ID_INVALID) {
            scenes[curSceneId].stop();
            rootContainer.remove(scenes[curSceneId]);
            if (curSceneId == SCENE_ID_MAIN) { MainManager.getInstance().resizeScaledVideo(0, 0, 0, 0); }
            if (reqSceneId == SCENE_ID_MAIN) { MainManager.getInstance().resizeScaledVideo(587, 72, 322, 181); }
        }

        if (data != null) {
            setHistoryScene(data);
        } else if (data == null) { removeHistoryScene(); }

        //Start current scene
        if (scenes[reqSceneId] == null) { scenes[reqSceneId] = createScene(reqSceneId); }
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.handleKeyEvent]scene : " + scenes[reqSceneId]);
        }
        curSceneId = reqSceneId;
        rootContainer.add(scenes[reqSceneId]);
        scenes[reqSceneId].start(reset);
        //rootContainer.repaint();
    }

    /**
     * Creates and Initialize the Scene.
     *
     * @param reqSceneId    scene id for request
     * @return scene    requested scene
     */
    private BasicUI createScene(int reqSceneId) {
        BasicUI scene = null;
        switch (reqSceneId) {
            case SceneManager.SCENE_ID_MAIN:
                scene = new CinemaMain();
                break;
            case SceneManager.SCENE_ID_TITLE:
                scene = new AllMovie();
                break;
            case SceneManager.SCENE_ID_THEATRE_ASSET:
                scene = new TheatreMovie();
                break;
            case SceneManager.SCENE_ID_MOVIE_ASSET:
                scene = new MovieInfo();
                break;
            case SceneManager.SCENE_ID_MOVIE_SHOWTIMES:
                scene = new ShowtimesInFavorite();
                break;
            case SceneManager.SCENE_ID_PREFERENCE:
                scene = new PrefSetting();
                break;
            case SceneManager.SCENE_ID_FAVORITE:
                scene = new FavorSetting();
                break;
            default:
                break;
        }
        scene.setVisible(false);
        if (scene != null) { scene.init(); }
        return scene;
    }

    /**
     * get a current Scene ID.
     * @return curSceneId
     */
    public int getCurrentSceneId() {
        return curSceneId;
    }

    /**
     * If previous scene is gone, removes a data of history.
     * @return data is a previous scene data.
     */
    public Object[] removeHistoryScene() {
        if (history.size() > 0) {
            Object[] data = (Object[]) history.remove(history.size() - 1);
            return data;
        }
        return null;
    }

    /**
     * Gets the previous data on the next Scene.
     * @return data is a previous scene data.
     */
    public Object[] getHistorySceneValue() {
        if (history.size() > 0) {
            Object[] data = (Object[]) history.elementAt(history.size() - 1);
            return data;
        }
        return null;
    }

    /**
     * save the data.
     * @param data  the data for saving into history
     */
    public void setHistoryScene(Object[] data) {
        history.add(data);
    }

    /**
     * The Class LayeredUIHandler.
     * <p>
     * This create the LayeredUI and handle the key event of LayeredKey.
     */
    public class LayeredUIHandler implements LayeredKeyHandler {
        /** The layered UI. */
        private LayeredUI layeredUI = null;
        /** The layered container. */
        private LayeredContainer layeredContainer = null;

        /**
         * Instantiates a new LayeredUIHandler.
         *
         * @param windowProperty the WindowProperty for layered UI
         */
        public LayeredUIHandler(final WindowProperty windowProperty) {
            layeredContainer = new LayeredContainer();
            layeredContainer.setBounds(Rs.SCENE_BOUND);
            layeredContainer.setVisible(true);
            layeredUI = windowProperty.createLayeredDialog(layeredContainer, Rs.SCENE_BOUND, this);
            layeredUI.deactivate();
        }

        /**
         * destroy resources.
         */
        public void destroy() {
            if (layeredContainer != null) {
                layeredContainer.setVisible(false);
                layeredContainer.removeAll();
                layeredContainer = null;
            }
            if (layeredUI != null) {
                layeredUI.deactivate();
                WindowProperty.dispose(layeredUI);
                layeredUI = null;
            }
        }

        /**
         * activate layeredUI.
         */
        public void activate() {
            if (layeredUI != null) { layeredUI.activate(); }
        }

        /**
         * deactivate layeredUI.
         */
        public void deactivate() {
            if (layeredUI != null) { layeredUI.deactivate(); }
        }

        /**
         * return LayeredContainer.
         * <p>
         * @return          LayeredContainer            LayeredContainer(Root Container)
         */
        public LayeredContainer getLayeredContainer() {
            return layeredContainer;
        }

        /**
         * implement LayeredKeyHandler method. handle KeyEvent.
         *
         * @param event the UserEvent
         * @return true, if handle key
         */
        public boolean handleKeyEvent(UserEvent event) {
            if (event == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (event.getType() != KeyEvent.KEY_PRESSED) { return false; }
            if (curSceneId == SCENE_ID_INVALID) { return false; }
            int keyCode = event.getCode();
            //TODO test
            if (LogDisplayer.USE_DISPLAY) {
                boolean isLogShow = LogDisplayer.getInstance().isLogShow;
                if (keyCode == KeyCodes.COLOR_A || (Rs.IS_EMULATOR && keyCode == KeyCodes.COLOR_D)) {
                    isLogShow = !isLogShow;
                    LogDisplayer.getInstance().isLogShow = isLogShow;
                    if (isLogShow) {
                        popupContainer.add(LogDisplayer.getInstance(), 0);
                        popupLayeredHandler.activate();
                    } else {
                        popupContainer.remove(LogDisplayer.getInstance());
                        if (BasicUI.popup == null) { popupLayeredHandler.deactivate(); }
                    }
                    //popupContainer.repaint();
                    return true;
                } else if (keyCode == Rs.KEY_PAGEUP && isLogShow) {
                    CacheManager.useCached = !CacheManager.useCached;
                    popupContainer.repaint();
                    return true;
                }
            }
            //
            boolean isHandling = scenes[curSceneId].handleKey(keyCode);
            //if (keyCode == KeyCodes.COLOR_D || keyCode == KeyCodes.FORWARD) { return true; }
            return isHandling;
        }

        /**
         * <code>LayeredContainer</code> LayeredWindow Class.
         */
        private class LayeredContainer extends LayeredWindow {
            /** serialVersionUID for serializable type.*/
            private static final long serialVersionUID = 1L;

            /** called when other UI is disappeared on this UI.
             * @see com.alticast.ui.LayeredWindow#notifyClean()
             */
            public void notifyClean() {
                Log.printDebug("notifyClean() called.");
            }

            /** called when other UI is covered on this UI.
             * @see com.alticast.ui.LayeredWindow#notifyShadowed()
             */
            public void notifyShadowed() {
                Log.printDebug("notifyShadowed() called.");
            }
        }
    }
}

/*
 *  @(#)TheatreMovie.java 1.0 2011.03.01
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Collections;
import java.util.Vector;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.gui.BasicRenderer;
import com.videotron.tvi.illico.itv.cinema.gui.TheatreMovieRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.itv.cinema.ui.AllMovie.ListCompRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ListCompUI;
import com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener;
import com.videotron.tvi.illico.itv.cinema.ui.effect.FlowEffect;
import com.videotron.tvi.illico.itv.cinema.ui.popup.ConfirmPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.TheatreSelectPopup;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>TheatreMovie</code> The class to show movie information in specified theatre. .
 *
 * @since   2011.03.01
 * @version $Revision: 1.19 $ $Date: 2012/02/23 06:11:11 $
 * @author  tklee
 */
public class TheatreMovie extends BasicUI implements MenuListener {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ID_THEATRE_INFO. */
    private static final String ID_THEATRE_INFO = "theatreInfo";
    /** The Constant ID_FILM_DATA. */
    private static final String ID_FILM_DATA = "filmData";

    /** The poster per page. */
    public final int posterPerPage = 9;
    /** The Constant MENU_MOVIE_DETAIL. */
    public static final int MENU_MOVIE_DETAIL = 0;
    /** The Constant MENU_THEATRE. */
    public static final int MENU_THEATRE = 1;
    /** The Constant MENU_INFO. */
    public static final int MENU_SHOWTIME = 2;
    /** The Constant MENU_INFO. */
    //public static final int MENU_ALLMOVIE = 2;

    /** Is the TheatreMovieRenderer instance. */
    private TheatreMovieRenderer renderer = null;
    /** The ClickingEffect instance. */
    private ClickingEffect clickEffect = null;
    /** The root menu item for D-Option. */
    //private final MenuItem rootMenuItemLang = new MenuItem("D-Option",
    //        new MenuItem[] {OPTION_PREFERENCE, OPTION_SORT_LANG, OPTION_HELP});
    /** The root menu item by alphabetically. */
    //private final MenuItem rootMenuItemAlpha = new MenuItem("D-Option",
    //        new MenuItem[] {OPTION_PREFERENCE, OPTION_SORT_ALPHA, OPTION_HELP});
    /** The root menu item for D-Option. */
    private final MenuItem rootMenuItem = new MenuItem("D-Option",
            new MenuItem[] {OPTION_PREFERENCE, OPTION_SORTING, OPTION_HELP});
    /** The list component. */
    private ListCompUI listComp = null;

    /** The poster focus. */
    private int tempPosterFocus = 0;
    /** The is temp right focus. */
    private boolean isTempRightFocus = false;

    /** Film Data. */
    public Vector filmList = null;
    /** The theatre information. */
    public TheatreData theatreInfo = null;
    /** Movie List focus or Right List focus. */
    public boolean isRightFocus = false;
    /** The menu focus. */
    public int menuFocus = 0;
    /** The poster focus. */
    public int posterFocus = 0;
    /** The sort type. */
    public int sortType = -1;
    private boolean isFocusSet = false;

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (renderer == null) {
            renderer = new TheatreMovieRenderer();
            setRenderer(renderer);
            clickEffect = new ClickingEffect(this, 5);
            ListCompRenderer listCompRenderer = new ListCompRenderer();
            listComp = new ListCompUI();
            listComp.setBounds(0, 0, 960, 540);
            listComp.setRenderer(listCompRenderer, new FlowEffect(listComp, 4, listCompRenderer));
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset the reset
     */
    protected void startScene(boolean reset) {
        add(listComp);
        prepare();
        if (scrollText != null) {
            scrollText.setBounds(61, 369, 325 - 61, 483 - 369);
            scrollText.setFont(Rs.F16);
            scrollText.setForeground(Rs.C224224224);
            scrollText.setRows(6);
            scrollText.setRowHeight(17);
            scrollText.setBottomMaskImage(renderer.scrollShadowImg);
            scrollText.setTopMaskImage(renderer.scrollShadowTopImg);
            scrollText.setInsets(376 - 369, 0, 483 - 473, 0);
        }
        scrollShadowComp.setBounds(277, 466, 683, 17);
        scrollShadowComp.setShadowImg(BasicRenderer.hotkeybgImg);

        isFocusSet = false;
        if (reset) {
            isFocusSet = true;
            isRightFocus = false;
            menuFocus = 0;
            posterFocus = 0;
            FilmData.sortType = FilmData.SORT_LANG;
        } else  if (focusHistory != null && focusHistory.length > 3) {
            isRightFocus = ((Boolean) focusHistory[0]).booleanValue();
            menuFocus = Integer.parseInt((String) focusHistory[1]);
            posterFocus = Integer.parseInt((String) focusHistory[2]);
            FilmData.sortType = Integer.parseInt((String) focusHistory[3]);
        }
        tempPosterFocus = posterFocus;
        isTempRightFocus = isRightFocus;
        setVisible(true);
        loadData(new Object[]{ID_THEATRE_INFO}, true);
        /*TheatreData theatreInfo = (TheatreData) DataManager.theatresData.get(sendValue);
        if (theatreInfo == null || theatreInfo.getAddress() == null) {
            tempPosterFocus = renderer.posterFocus;
            loadData(ID_THEATRE_INFO);
        } else { notifyFromDataLoadingThread(ID_THEATRE_INFO, false); }*/
    }

    /**
     * Gets the focus history.
     */
    protected void setFocusHistory() {
        Object[] objs = new Object[4];
        objs[0] = Boolean.valueOf(isRightFocus);
        objs[1] = String.valueOf(menuFocus);
        objs[2] = String.valueOf(posterFocus);
        objs[3] = String.valueOf(sortType);
        setFocusHistory(objs);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        renderer.stop();
        if (optScr != null) { optScr.stop(); }
        theatreInfo = null;
        filmList = null;
        listComp.isAnimation = false;
    }

    /**
     * Dispose scene.
     */
    protected void disposeScene() {
        renderer.stop();
        sortType = -1;
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        Log.printDebug("[TheatreSelect]keyAction." + keyCode);

        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (isRightFocus) {
                if (--menuFocus < 0) { isRightFocus = false; }
                repaint();
            }
            break;
        case KeyEvent.VK_DOWN:
            if (!isRightFocus) {
                isRightFocus = true;
                menuFocus = 0;
            } else {
                if (menuFocus < renderer.menuTxt.length - 1) { menuFocus++; }
            }
            repaint();
            break;
        case KeyEvent.VK_LEFT:
            if (!isRightFocus) {
                int totalCount = filmList.size();
                if (totalCount < posterPerPage) {
                    if (posterFocus > 0) {
                        //listComp.startEffect(ListCompUI.EFFECT_RIGHT);
                        posterFocus--;
                    }
                    setScrollText(posterFocus);
                    repaint();
                } else {
                    //listComp.startEffect(ListCompUI.EFFECT_RIGHT);
                    tempPosterFocus = posterFocus;
                    if (--tempPosterFocus < 0) { tempPosterFocus = totalCount - 1; }
                    moveFocus(true);
                }
            }
            break;
        case KeyEvent.VK_RIGHT:
            if (!isRightFocus) {
                int totalCount = filmList.size();
                if (totalCount < posterPerPage) {
                    if (posterFocus < totalCount - 1) {
                        //listComp.startEffect(ListCompUI.EFFECT_LEFT);
                        posterFocus++;
                    }
                    setScrollText(posterFocus);
                    repaint();
                } else {
                    //listComp.startEffect(ListCompUI.EFFECT_LEFT);
                    tempPosterFocus = posterFocus;
                    if (++tempPosterFocus > totalCount - 1) { tempPosterFocus = 0; }
                    moveFocus(false);
                }
            }
            break;
        case HRcEvent.VK_ENTER:
            Object[] values = {getCurrentSceneId(), (FilmData) filmList.elementAt(posterFocus),
                    null};
            if (isRightFocus) {
                clickEffect.start(626, 355 + (menuFocus * 39), 910 - 626, 396 - 355);
                if (menuFocus == MENU_THEATRE) {
                    /*if (DataManager.region.isEmpty()) {
                        loadData(new Object[]{ID_REGION_DATA}, true);
                    } else { notifyFromDataLoadingThread(new Object[]{ID_REGION_DATA}, false); }*/
                    loadData(new Object[]{ID_REGION_DATA_FOCUSED}, true);
                } else if (menuFocus == MENU_SHOWTIME) {
                    if (Rs.vfavoriteTheatre.size() > 0) {
                        SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_SHOWTIMES, true, values);
                    } else {
                        confirmPopup.setMode(ConfirmPopup.MODE_NO_FAVORITES);
                        confirmPopup.resetFocus();
                        addPopup(confirmPopup);
                    }
                } else {
                    //SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_TITLE, true, values);
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_ASSET, true, values);
                }
            } else {
                /*if (Rs.vfavoriteTheatre.size() > 0) {
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_SHOWTIMES, true, values);
                } else {
                    confirmPopup.setMode(ConfirmPopup.MODE_NO_FAVORITES);
                    confirmPopup.resetFocus();
                    addPopup(confirmPopup);
                }*/
                SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_ASSET, true, values);
            }
            break;
        case HRcEvent.VK_PAGE_DOWN:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SCROLL]);
            scrollText.showNextPage();
            break;
        case HRcEvent.VK_PAGE_UP:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SCROLL]);
            scrollText.showPreviousPage();
            break;
        case KeyCodes.LAST:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
            break;
        case KeyCodes.COLOR_B:
        case KeyCodes.SEARCH:
            if (!Rs.IS_EMULATOR && keyCode == KeyCodes.COLOR_B) { return false; }
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SEARCH]);
            addPopup(titleSearchPopup);
            break;
        case KeyCodes.COLOR_D:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_OPTION]);
            if (optScr != null) {
                /*if (sortType == FilmData.SORT_LANG) {
                    optScr.start(rootMenuItemAlpha, this);
                } else { optScr.start(rootMenuItemLang, this); }*/
                if (sortType == FilmData.SORT_LANG) {
                    OPTION_SORT_ALPHA.setChecked(false);
                    OPTION_SORT_LANG.setChecked(true);
                } else {
                    OPTION_SORT_ALPHA.setChecked(true);
                    OPTION_SORT_LANG.setChecked(false);
                }
                optScr.start(rootMenuItem, this);
            }
            break;
        case KeyCodes.FAST_FWD:
            if (Rs.IS_EMULATOR) {
                footer.clickAnimation(btnName[ButtonIcons.BUTTON_PAGE_CHAANGE]);
                changePage(KeyCodes.FORWARD);
                return true;
            } else { return false; }
        case KeyCodes.REWIND:
            if (Rs.IS_EMULATOR) {
                footer.clickAnimation(btnName[ButtonIcons.BUTTON_PAGE_CHAANGE]);
                changePage(KeyCodes.BACK);
                return true;
            } else { return false; }
        case KeyCodes.FORWARD:
        case KeyCodes.BACK:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_PAGE_CHAANGE]);
            changePage(keyCode);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Change page.
     *
     * @param keyCode the key code
     */
    private void changePage(int keyCode) {
        int totalCount = filmList.size();
        if (totalCount >= posterPerPage) {
            tempPosterFocus = posterFocus;
            if (keyCode == KeyCodes.FORWARD) {
                tempPosterFocus += 7;
                if (tempPosterFocus > totalCount - 1) { tempPosterFocus = tempPosterFocus - totalCount; }
            } else if (keyCode == KeyCodes.BACK) {
                tempPosterFocus -= 7;
                if (tempPosterFocus < 0) { tempPosterFocus = totalCount + tempPosterFocus; }
            }
            isFocusSet = false;
            notifyFromDataLoadingThread(new Object[]{ID_THEATRE_INFO}, true);
        }
    }

    /**
     * Move focus.
     *
     * @param isLeft whether press the left key.
     */
    private void moveFocus(boolean isLeft) {
        int[] numbers = getInitLastNumber(tempPosterFocus, filmList.size(), posterPerPage);
        int focus = numbers[0];
        if (!isLeft) { focus = numbers[1]; }
        Logger.debug(this, "moveFocus-tempPosterFocus : " + tempPosterFocus);
        Logger.debug(this, "moveFocus-focus : " + focus);
        setScrollText(tempPosterFocus);
        posterFocus = tempPosterFocus;
        repaint();
        FilmInfoData newData = ((FilmData) filmList.elementAt(focus)).getFilmInfoData();
        if (newData == null || newData.getPosterImage() == null) {
            loadData(new Object[]{ID_FILM_DATA, new Integer(focus)}, false);
        }
    }

    /**
     * D-Option canceled.
     */
    public void canceled() {
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * D-Option Selected.
     *
     * @param item MenuItem
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }

        if (item.equals(OPTION_PREFERENCE)) {
            Object[] values = {String.valueOf(SceneManager.SCENE_ID_TITLE), null, null};
            SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_PREFERENCE, true, values);
        } else if (item.equals(OPTION_SORT_LANG)) {
            Logger.debug(this, "selected()-OPTION_SORT_LANG");
            setSort(FilmData.SORT_LANG);
        } else if (item.equals(OPTION_SORT_ALPHA)) {
            Logger.debug(this, "selected()-OPTION_SORT_ALPHA");
            setSort(FilmData.SORT_ALPHABET);
        } else if (item.equals(OPTION_HELP)) {
            MainManager.getInstance().requestStartHelpApp();
        } /*else if (item.equals(BY_ALPHABETICAL)) {
            setSort(FilmData.ALPHABET);
        } else if (item.equals(BY_GENRE)) {
            setSort(FilmData.GENER);
        }*/
    }

    /**
     * Sets the sort.
     *
     * @param newSortType the new sort
     */
    private void setSort(int newSortType) {
        if (FilmData.sortType != newSortType && filmList != null) {
            tempPosterFocus = 0;
            FilmData.sortType = newSortType;
            isFocusSet = false;
            notifyFromDataLoadingThread(new Object[]{ID_THEATRE_INFO}, true);
        }
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, boolean isFromThread) {
        String procId = (String) obj[0];
        Logger.debug(this, "called notifyFromDataLoadingThread() - procId : " + procId);
        DataManager dataManager = DataManager.getInstance();
        if (procId.equals(ID_THEATRE_INFO)) {
            Object[] value = (Object[]) sendValue;
            TheatreData theatreData = dataManager.getTheatresInfo((String) value[0]);
            Vector tempList = filmList;
            if (theatreData != null) {
                tempList = theatreData.getFilms();
                //if (!tempList.isEmpty()) {
                tempList = (Vector) tempList.clone();
                Collections.sort(tempList);
                sortType = FilmData.sortType;
                //}
            }

            if (theatreData == null || theatreData.getAddress() == null || tempList == null || tempList.isEmpty()) {
                MainManager.getInstance().hideLoadingAnimation();
                if (!checkErrorMessageShow()) {
                    DataManager.getInstance().errorType = DataManager.ERR_XML_PARSING;
                    DataManager.getInstance().printError(null);
                }
                showErrorMessage(ERRMSG_PREV);
                return;
            }
            theatreInfo = theatreData;
            isRightFocus = isTempRightFocus;
            if (isFocusSet && value[1] != null && value[2] != null) {
                String filmId = (String) value[1];
                String langType = (String) value[2];
                for (int i = 0; i < tempList.size(); i++) {
                    String id = ((FilmData) tempList.elementAt(i)).getID();
                    String lang = ((FilmData) tempList.elementAt(i)).getLangue();
                    if (filmId.equals(id) && langType.equals(lang)) {
                        tempPosterFocus = i;
                        break;
                    }
                }
            }
            posterFocus = tempPosterFocus;
            MainManager.getInstance().hideLoadingAnimation();
            filmList = tempList;
            theatreData.setFilms(tempList);
            Object[] data = SceneManager.getInstance().getHistorySceneValue();
            if (data != null && data.length > 1) { data[1] = sendValue; }
            setScrollText(tempPosterFocus);
            startComp();
            add(scrollShadowComp);
            add(scrollText);
            repaint();

            final int totalCount = tempList.size();
            final int[] numbers = getInitLastNumber(tempPosterFocus, totalCount, posterPerPage);
            final Vector list = tempList;
            new Thread("AllMovie|setPosterImage()") {
                public void run() {
                    for (int i = numbers[0];; i++) {
                        if (i > totalCount - 1) { i = 0; }
                        if (!checkFocus(i)) { break; }
                        FilmData filmData = (FilmData) list.elementAt(i);
                        FilmInfoData infoData = filmData.getFilmInfoData();
                        if (infoData == null) {
                            infoData = DataManager.getInstance().getFilmInfoData(filmData.getID());
                            filmData.setFilmInfoData(infoData);
                            if (!checkFocus(i)) { break; }
                            if (SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_THEATRE_ASSET) {
                                break;
                            }
                            if (i == posterFocus) { setScrollText(posterFocus); }
                        }
                        if (SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_THEATRE_ASSET) {
                            break;
                        }
                        setPosterImage(infoData, i);
                        FrameworkMain.getInstance().getImagePool().waitForAll();
                        if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_THEATRE_ASSET) { repaint(); }
                        if (i == numbers[1]) { break; }
                    }
                    if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
                        FrameworkMain.getInstance().getImagePool().clear();
                        return;
                    }
                }
            } .start();
        } else if (procId.equals(ID_FILM_DATA)) {
            //MainManager.getInstance().hideLoadingAnimation();
            if (isFromThread) {
                new Thread("AllMovie|setPosterImage()") {
                    public void run() {
                        try {
                            Thread.sleep(50L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        int focus = ((Integer) obj[1]).intValue();
                        if (SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_THEATRE_ASSET
                                || !checkFocus(focus)) {
                            return;
                        }
                        FilmData filmData = (FilmData) filmList.elementAt(focus);
                        FilmInfoData infoData = filmData.getFilmInfoData();
                        if (infoData == null) {
                            infoData = DataManager.getInstance().getFilmInfoData(filmData.getID());
                            filmData.setFilmInfoData(infoData);
                            if (SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_THEATRE_ASSET
                                    || !checkFocus(focus)) {
                                return;
                            }
                            if (focus == posterFocus) { setScrollText(posterFocus); }
                        }
                        if (SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_THEATRE_ASSET
                                || !checkFocus(focus)) {
                            return;
                        }
                        setPosterImage(infoData, focus);
                        FrameworkMain.getInstance().getImagePool().waitForAll();
                        if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_THEATRE_ASSET) { repaint(); }
                        if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
                            FrameworkMain.getInstance().getImagePool().clear();
                            return;
                        }
                    }
                } .start();
            }
        } else if (procId.equals(ID_REGION_DATA_FOCUSED)) {
            FilmData film = (FilmData) filmList.elementAt(posterFocus);
            showCinemaPopup(film.getID(), film.getLangue());
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
        repaint();
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @return true, if successful
     */
    protected boolean checkFocus(int reqIndex) {
        int totalCount = filmList.size();
        int[] numbers = getInitLastNumber(posterFocus, totalCount, posterPerPage);
        for (int i = numbers[0];; i++) {
            if (i > totalCount - 1) { i = 0; }
            if (reqIndex == i) { return true; }
            if (i == numbers[1]) { break; }
        }
        return false;
    }

    /**
     * Adds the footer buttonns.
     */
    private void addFooterBtn() {
        int[] btns = null;
        int pageCount = scrollText.getPageCount();
        int totalCount = filmList.size();
        if (pageCount > 1 && totalCount >= posterPerPage) {
            btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SEARCH,
                    ButtonIcons.BUTTON_SCROLL, ButtonIcons.BUTTON_PAGE_CHAANGE, ButtonIcons.BUTTON_OPTION};
        } else if (pageCount > 1) {
            btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SEARCH, ButtonIcons.BUTTON_SCROLL,
                    ButtonIcons.BUTTON_OPTION};
        } else if (totalCount >= posterPerPage) {
            btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SEARCH,
                    ButtonIcons.BUTTON_PAGE_CHAANGE, ButtonIcons.BUTTON_OPTION};
        } else { btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SEARCH, ButtonIcons.BUTTON_OPTION}; }
        addFooterBtn(btns);
    }

    /**
     * Sets the scroll text.
     *
     * @param focus focus for the new scroll text
     */
    private void setScrollText(int focus) {
        if (filmList == null || filmList.isEmpty()) { return; }
        FilmData filmData = (FilmData) filmList.elementAt(focus);
        if (filmData != null && !filmData.getSchedule().isEmpty()) {
            Vector vSchedule = filmData.getSchedule();
            String schedule = "";
            for (int i = 0; i < vSchedule.size(); i++) {
                schedule += (String) vSchedule.elementAt(i) + "\n";
            }
            scrollText.setContents(schedule);
        } else { scrollText.setContents(null); }
        addFooterBtn();
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof TheatreSelectPopup) {
            removePopup();
            Object[] value = (Object[]) sendValue;
            Object[] paras = (Object[]) para;
            if (paras[0].equals(value[0])) { return; }
            sendValue = para;
            tempPosterFocus = 0;
            isTempRightFocus = false;
            FilmData.sortType = FilmData.SORT_LANG;
            isFocusSet = true;
            loadData(new Object[]{ID_THEATRE_INFO}, true);
        } else if (popup instanceof NotiPopup) {
            keyAction(KeyCodes.LAST);
        } else if (popup instanceof ConfirmPopup) {
            if (para instanceof Integer) {
                int btnFocus = ((Integer) para).intValue();
                if (btnFocus == ConfirmPopup.BTN_YES) {
                    Object[] values = {getCurrentSceneId(), null, null};
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_FAVORITE, true, values);
                } else {
                    FilmData film = (FilmData) filmList.elementAt(posterFocus);
                    Object[] values = {getCurrentSceneId(), film, null};
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_ASSET, true, values);
                }
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * <code>ListCompRenderer</code> The class to paint a GUI of list component for animation.
     *
     * @since   2011.06.02
     * @version $Revision: 1.19 $ $Date: 2012/02/23 06:11:11 $
     * @author  tklee
     */
    public class ListCompRenderer extends Renderer implements EffectListener {
        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return listComp.getBounds();
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) { }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {
            if (filmList == null) { return; }
          //poster
            int totalCount = filmList.size();
            int initNumber = 0;
            int halfPos = posterPerPage / 2;
            //int focusPos = halfPos + (posterPerPage % 2);
            if (totalCount >= posterPerPage) {
                initNumber =  posterFocus - halfPos;
                if (posterFocus < halfPos) {  initNumber = totalCount - (halfPos - posterFocus); }
            } else {
                if (posterFocus > halfPos) { initNumber = posterFocus - halfPos; }
            }

            boolean isTranslate = false;
            int posterGap = 103;
            int loopCount = posterPerPage;
            int j = 0;
            if (listComp.isAnimation) {
                loopCount++;
                if (listComp.effectType == ListCompUI.EFFECT_RIGHT) {
                    j = -1;
                    initNumber -= 1;
                    if (initNumber < 0) {
                        if (totalCount >= posterPerPage) {
                            initNumber = totalCount - 1;
                        } else { initNumber = 0; }
                    }
                    g.translate((683 - 200) - (78 - 206), 360 - 132);
                    isTranslate = true;
                }
            }
            for (int i = initNumber; j < loopCount; j++) {
                if (j == halfPos) {
                    if (i > totalCount - 1) { i = 0; }
                    i++;
                    continue;
                } else if (totalCount < posterPerPage) {
                    if (j < halfPos && halfPos - posterFocus > j) {
                        continue;
                    } else if (i > totalCount - 1) { break; }
                }
                if (i > totalCount - 1) { i = 0; }
                if (listComp.isAnimation) {
                    if (j == halfPos + 2 && listComp.effectType == ListCompUI.EFFECT_LEFT) {
                        g.translate(-200, 360 - 132);
                        isTranslate = true;
                    } else if (isTranslate && j >= halfPos - 1 && listComp.effectType == ListCompUI.EFFECT_RIGHT) {
                        g.translate(-((683 - 200) - (78 - 206)), -(360 - 132));
                        isTranslate = false;
                    }
                }

                FilmData film = (FilmData) filmList.elementAt(i);
                Image posterImage = null;
                if (film != null && film.getFilmInfoData() != null) {
                    posterImage = film.getFilmInfoData().getPosterImage();
                }
                if (posterImage == null) { posterImage = renderer.defaultPosterImg; }
                if (posterImage != null) {
                    int posterPos = (78 - posterGap) + (j * posterGap);
                    if (j > halfPos) { posterPos = 580 + ((j - halfPos - 1) * posterGap); }
                    g.drawImage(posterImage, posterPos, 132, 96, 144, c);
                    g.drawImage(renderer.posterDimImg, posterPos, 132, 96, 141, c);
                    g.drawImage(renderer.posterShadowImg, posterPos - 11, 274, c);
                }
                i++;
            }
            if (isTranslate && listComp.effectType == ListCompUI.EFFECT_LEFT) { g.translate(200, -(360 - 132)); }
            if (isTranslate && listComp.effectType == ListCompUI.EFFECT_RIGHT) {
                g.translate(-((683 - 200) - (78 - 206)), -(360 - 132));
            }

            if (!listComp.isAnimation) { requestPrepaint(g, c); }

            FilmData focusFilm = (FilmData) filmList.elementAt(posterFocus);
            FilmInfoData filmInfo = null;
            Image posterImage = null;
            if (focusFilm != null) {
                filmInfo = focusFilm.getFilmInfoData();
                if (filmInfo != null) { posterImage = filmInfo.getPosterImage(); }
            }
            if (posterImage == null) { posterImage = renderer.defaultFocusPosterImg; }
            if (posterImage != null) {
                if (listComp.isAnimation) { g.translate(0, -107); }
                g.drawImage(posterImage, 408, 107, 144, 216, c);
                if (listComp.isAnimation) { g.drawImage(renderer.posterShadowImg, 388, 320, 205, 49, c); }
                if (listComp.isAnimation) { g.translate(-0, 107); }
            }

            if (!listComp.isAnimation) { requestPaint(g, c); }
        }

        /**
         * implements abstract method of EffectListener.
         *
         * @param g the Graphics
         * @param c the Component
         * @see com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener#requestPaint
         * (java.awt.Graphics, java.awt.Component)
         */
        public void requestPaint(Graphics g, Component c) {
            if (filmList == null) { return; }
            int totalCount = filmList.size();
            if (!isRightFocus) {
                if (totalCount >= posterPerPage || posterFocus > 0) { g.drawImage(renderer.arrLImg, 385, 191, c); }
                if (totalCount >= posterPerPage || posterFocus < totalCount - 1) {
                    g.drawImage(renderer.arrRImg, 557, 191, c);
                }
            }
            g.drawImage(renderer.posterListShadowLImg, 0, 6, c);
            g.drawImage(renderer.posterListShadowRImg, 856, 6, c);

            if (!isRightFocus) {
                g.drawImage(renderer.posterFocusImg, 405, 104, c);
            } else { g.drawImage(renderer.poserFocusDimImg, 405, 104, c); }

            int addWidth = 0;
            if (totalCount >= 100) { addWidth = 15; }
            g.setColor(Rs.C252202004);
            if (isRightFocus) { g.setColor(Rs.C192); }
            g.fillRect(505 - addWidth, 306, 47 + addWidth, 17);
            g.setFont(Rs.F16);
            g.setColor(Rs.C074072064);
            String str = String.valueOf(totalCount);
            if (totalCount < 10) { str = "0" + str; }
            str = "/" + str;
            GraphicUtil.drawStringRight(g, str, 549, 320);
            int pos = 549 - g.getFontMetrics().stringWidth(str);
            g.setColor(Rs.C000000000);
            str = String.valueOf(posterFocus + 1);
            if (posterFocus < 9) { str = "0" + str; }
            GraphicUtil.drawStringRight(g, str, pos, 320);

            g.setFont(Rs.F23);
            g.setColor(Rs.C255205012);
            FilmData focusFilm = (FilmData) filmList.elementAt(posterFocus);
            GraphicUtil.drawStringCenter(g, focusFilm.getTitle(), 480, 348);
        }

        /**
         * implements abstract method of EffectListener.
         *
         * @param g the Graphics
         * @param c the Component
         * @see com.videotron.tvi.illico.itv.cinema.ui.effect.EffectListener#requestPrepaint
         * (java.awt.Graphics, java.awt.Component)
         */
        public void requestPrepaint(Graphics g, Component c) {
            if (filmList == null) { return; }
        }
    }
}

/*
 *  @(#)ConfirmPopup.java 1.0 2011.03.02
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ConfirmPopup</code> Confirm Popup(two buttons).
 *
 * @since   2011.03.02
 * @version $Revision: 1.5 $ $Date: 2011/07/05 18:26:19 $
 * @author  tklee
 */
public class ConfirmPopup extends Popup {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MODE_MSG. */
    public static final int MODE_NO_FAVORITES = 0;

    /** The Constant BTN_YES. */
    public static final int BTN_YES = 0;
    /** The Constant BTN_NO. */
    public static final int BTN_NO = 1;

    /** The data center. */
    private final DataCenter dataCenter = DataCenter.getInstance();
    /** The txt title. */
    private String[] txtTitle = new String[1];
    /** The txt msage. */
    private String[] txtMsg = new String[1];
    /** The txt ok. */
    private String[] btnTxt = new String[2];

    /** The bg top img. */
    private Image bgTopImg = null;
    /** The bg middle img. */
    private Image bgMiddleImg = null;
    /** The bg bottom img. */
    private Image bgBottomImg = null;
    /** The shadow img. */
    private Image shadowImg = null;
    /** The line img. */
    private Image lineImg = null;
    /** The high img. */
    private Image highImg = null;
    /** The focus img. */
    private Image focusImg = null;
    /** The focus dim img. */
    private Image focusDimImg = null;

    /** The btn focus. */
    private int btnFocus = 0;

    /**
     * Instantiates a new noti popup.
     */
    public ConfirmPopup() {
        setBounds(0, 0, 410, 330);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Instantiates.
     */
    public void init() {
        bgTopImg = dataCenter.getImage("05_pop_grow_t.png");
        bgMiddleImg = dataCenter.getImage("05_pop_grow_m.png");
        bgBottomImg = dataCenter.getImage("05_pop_grow_b.png");
        shadowImg = dataCenter.getImage("05_pop_sh_01.png");
        lineImg = dataCenter.getImage("05_pop_line_01.png");
        highImg = dataCenter.getImage("05_pop_high_01.png");
        focusImg = dataCenter.getImage("05_focus.png");
        focusDimImg = dataCenter.getImage("05_focus_dim.png");

        txtTitle[0] = dataCenter.getString("cine.noFavorPopupTitle");
        txtMsg[0] = dataCenter.getString("cine.noFavorPopupMsg");
        btnTxt[0] = dataCenter.getString("cine.Yes");
        btnTxt[1] = dataCenter.getString("cine.No");
    }

    /**
     * reset the button focus to 0.
     * @see com.videotron.tvi.illico.itv.cinema.ui.popup.Popup#resetFocus()
     */
    public void resetFocus() {
        btnFocus = 0;
    }

    /**
    * paint.
    *
    * @param        g           Graphics.
    */
    public void paint(Graphics g) {
        g.translate(-276, -113);

        g.setColor(Rs.C035035035);
        g.fillRect(306, 143, 350, 224);

        g.drawImage(bgTopImg, 276, 113, this);
        g.drawImage(bgMiddleImg, 276, 193, 410, 124, this);
        g.drawImage(bgBottomImg, 276, 317, this);
        g.drawImage(shadowImg, 304, 364, this);
        g.drawImage(lineImg, 285, 181, this);
        g.drawImage(highImg, 306, 143, this);

        g.setFont(Rs.F24);
        g.setColor(Rs.C252202004);
        GraphicUtil.drawStringCenter(g, txtTitle[mode], 479, 169);

        g.setFont(Rs.F20);
        g.setColor(Rs.C255255255);
        String[] str = TextUtil.split(txtMsg[mode], g.getFontMetrics(), 637 - 325);
        int checkLength = str.length;
        if (checkLength > 5) { checkLength = 5; }
        int baseYPos = 250 - ((checkLength - 1) * 15);
        for (int i = 0; i < checkLength; i++) {
            GraphicUtil.drawStringCenter(g, str[i], 482, baseYPos + (i * 30));
        }

        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i = 0; i < btnTxt.length; i++) {
            if (i == btnFocus) {
                g.drawImage(focusImg, 325 + 16 + (i * (157 - 16)), 318, this);
            } else { g.drawImage(focusDimImg, 325 + 16 + (i * (157 - 16)), 318, this); }
            GraphicUtil.drawStringCenter(g, btnTxt[i], 403 + 10 + (i * (157 - 16)), 339);
        }

        g.translate(276, 113);
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_LEFT:
            if (btnFocus > 0) {
                btnFocus--;
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (btnFocus == 0) {
                btnFocus++;
                repaint();
            }
            break;
        case Rs.KEY_OK:
            //clickEffect.start(341 + (btnFocus * 141) - 276, 318 - 113, 558 - 403, 350 - 318);
            clickEffect.start(341 + (btnFocus * 141) - 276, 318 - 113, 139, 350 - 318);
            scene.requestPopupOk(new Integer(btnFocus));
            break;
        case Rs.KEY_EXIT:
            scene.requestPopupClose();
            break;
        default:
            return false;
        }
        return true;
    }
}

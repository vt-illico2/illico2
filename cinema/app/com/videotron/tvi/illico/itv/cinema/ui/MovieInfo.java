/*
 *  @(#)MovieInfo.java 1.0 2011.03.01
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.event.KeyEvent;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.gui.BasicRenderer;
import com.videotron.tvi.illico.itv.cinema.gui.MovieInfoRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.popup.ConfirmPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.TheatreSelectPopup;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>MovieInfo</code> The class to show movie information in specified theatre.
 *
 * @since   2011.03.02
 * @version $Revision: 1.16 $ $Date: 2012/02/23 06:11:11 $
 * @author  tklee
 */
public class MovieInfo extends BasicUI implements MenuListener {

    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ID_FILM_DATA. */
    private static final String ID_FILM_DATA = "filmData";

    /** Is the MovieInTheatreRenderer instance. */
    private MovieInfoRenderer renderer = null;
    /** The ClickingEffect instance. */
    private ClickingEffect clickEffect = null;
    /** The root menu item for D-Option. */
    private final MenuItem rootMenuItem = new MenuItem("D-Option", new MenuItem[] {OPTION_PREFERENCE, OPTION_HELP});

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (renderer == null) {
            renderer = new MovieInfoRenderer();
            setRenderer(renderer);
            clickEffect = new ClickingEffect(this, 5);
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset the reset
     */
    protected void startScene(boolean reset) {
        //addFooterBtn(new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SCROLL,
        //        ButtonIcons.BUTTON_OPTION});
        prepare();
        if (scrollText != null) {
            //scrollText.setBounds(314, 272, 594 - 314, 472 - 272);
            scrollText.setBounds(292, 264, 594 - 292, 478 - 264);
            scrollText.setFont(Rs.F15_D);
            scrollText.setForeground(Rs.DVB90C255);
            scrollText.setRows(12);
            scrollText.setRowHeight(17);
            scrollText.setBottomMaskImage(renderer.scrollShadowImg);
            scrollText.setTopMaskImage(renderer.scrollShadowTopImg);
            scrollText.setInsets(272 - 264, 314 - 292, 478 - 473, 0);
        }
        scrollShadowComp.setBounds(277, 466, 683, 17);
        scrollShadowComp.setShadowImg(BasicRenderer.hotkeybgImg);

        if (reset) {
            renderer.menuFocus = 0;
            renderer.langType = "";
        } else  if (focusHistory != null && focusHistory.length > 1) {
            renderer.menuFocus = Integer.parseInt((String) focusHistory[0]);
            renderer.langType = (String) focusHistory[1];
        }
        setVisible(true);
        loadData(new Object[]{ID_FILM_DATA}, true);
        /*if (DataManager.filmsInfoData.get(sendValue) == null) {
            loadData(ID_FILM_DATA);
        } else { notifyFromDataLoadingThread(ID_FILM_DATA, false); }*/
    }

    /**
     * Gets the focus history.
     */
    protected void setFocusHistory() {
        Object[] objs = new Object[2];
        objs[0] = String.valueOf(renderer.menuFocus);
        objs[1] = renderer.langType;
        setFocusHistory(objs);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        renderer.stop();
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * Dispose scene.
     */
    protected void disposeScene() {
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        Log.printDebug("[TheatreSelect]keyAction." + keyCode);

        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (renderer.menuFocus > 0) {
                renderer.menuFocus--;
                repaint();
            }
            break;
        case KeyEvent.VK_DOWN:
            if (renderer.menuFocus < renderer.menuTxt.length - 1) {
                renderer.menuFocus++;
                repaint();
            }
            break;
        case HRcEvent.VK_ENTER:
            clickEffect.start(626, 355 + (renderer.menuFocus * 39), 910 - 626, 396 - 355);
            String filmId = renderer.filmInfoData.getId();
            Object[] values = {getCurrentSceneId(), filmId, null};
            if (renderer.menuFocus == MovieInfoRenderer.MENU_THEATRE) {
                /*if (DataManager.region.isEmpty()) {
                    loadData(new Object[]{ID_REGION_DATA}, true);
                } else { notifyFromDataLoadingThread(new Object[]{ID_REGION_DATA}, false); }*/
                loadData(new Object[]{ID_REGION_DATA_FOCUSED}, true);
            } else if (renderer.menuFocus == MovieInfoRenderer.MENU_SHOWTIME) {
                if (Rs.vfavoriteTheatre.size() > 0) {
                    values[1] = sendValue;
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_SHOWTIMES, true, values);
                } else {
                    confirmPopup.setMode(ConfirmPopup.MODE_NO_FAVORITES);
                    confirmPopup.resetFocus();
                    addPopup(confirmPopup);
                }
            } else {
                SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_TITLE, true, values);
            }
            break;
        case HRcEvent.VK_PAGE_DOWN:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SCROLL]);
            scrollText.showNextPage();
            break;
        case HRcEvent.VK_PAGE_UP:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SCROLL]);
            scrollText.showPreviousPage();
            break;
        case KeyCodes.LAST:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
            break;
        case KeyCodes.COLOR_D:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_OPTION]);
            if (optScr != null) { optScr.start(rootMenuItem, this); }
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * D-Option canceled.
     */
    public void canceled() {
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * D-Option Selected.
     *
     * @param item MenuItem
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }

        if (item.equals(OPTION_PREFERENCE)) {
            Object[] values = {getCurrentSceneId(), null, null};
            SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_PREFERENCE, true, values);
        } else if (item.equals(OPTION_HELP)) {
            MainManager.getInstance().requestStartHelpApp();
        }
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        String procId = (String) obj[0];
        Logger.debug(this, "called notifyFromDataLoadingThread() - procId : " + procId);
        if (procId.equals(ID_FILM_DATA)) {
            String dataId = "";
            String duration = "";
            if (sendValue instanceof FilmData) {
                FilmData data = (FilmData) sendValue;
                dataId = data.getID();
                duration = data.getDuration();
                renderer.langType = data.getLangue();
            } else {
                dataId = ((RecentFilmData) sendValue).getID();
                RecentFilmData recentData = (RecentFilmData) sendValue;
                renderer.langType = FilmData.LANGUAGE_FRENCH;
                if (recentData.getTitle() == null || recentData.getTitle().length() == 0) {
                    renderer.langType = FilmData.LANGUAGE_ENGLISH;
                }

                if (DataManager.allMovie.isEmpty()) { DataManager.getInstance().getFilmsData("tous", true); }
                FilmData data = (FilmData) DataManager.allMovie.get(dataId + renderer.langType);
                if (data != null) { duration = data.getDuration(); }
            }
            FilmInfoData infoData = DataManager.getInstance().getFilmInfoData(dataId);
            if (infoData == null) {
                MainManager.getInstance().hideLoadingAnimation();
                /*int popupMode = NotiPopup.MODE_ERR_NO_DATA;
                if (DataManager.getInstance().isError) { popupMode = NotiPopup.MODE_ERR_LOAD; }
                notiPopup.setMode(popupMode);
                notiPopup.setGoPrev(true);
                addPopup(notiPopup);*/
                if (!checkErrorMessageShow()) {
                    DataManager.getInstance().errorType = DataManager.ERR_XML_PARSING;
                    DataManager.getInstance().printError(null);
                }
                showErrorMessage(ERRMSG_PREV);
                return;
            }
            setPosterImage(infoData, -1);
            if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
                FrameworkMain.getInstance().getImagePool().clear();
                return;
            }
            MainManager.getInstance().hideLoadingAnimation();
            infoData.setDuration(duration);
            renderer.filmInfoData = infoData;
            scrollText.setContents(infoData.getSummary());
            addFooterBtn();
            startComp(infoData.getTitle());
            add(scrollShadowComp);
            add(scrollText);
        } else if (procId.equals(ID_REGION_DATA_FOCUSED)) {
            showCinemaPopup(renderer.filmInfoData.getId(), renderer.langType);
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
        repaint();
    }

    /**
     * Adds the footer buttonns.
     */
    private void addFooterBtn() {
        addFooterBtn(new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SCROLL, ButtonIcons.BUTTON_OPTION});
        int[] btns = null;
        if (scrollText.getPageCount() > 1) {
            btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_SCROLL, ButtonIcons.BUTTON_OPTION};
        } else { btns = new int[] {ButtonIcons.BUTTON_BACK, ButtonIcons.BUTTON_OPTION}; }
        addFooterBtn(btns);
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            keyAction(KeyCodes.LAST);
        } else if (popup instanceof ConfirmPopup) {
            if (para instanceof Integer) {
                int btnFocus = ((Integer) para).intValue();
                if (btnFocus == ConfirmPopup.BTN_YES) {
                    Object[] values = {getCurrentSceneId(), null, null};
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_FAVORITE, true, values);
                } else { removePopup(); }
            }
        } else { super.requestPopupOk(para); }
    }
}

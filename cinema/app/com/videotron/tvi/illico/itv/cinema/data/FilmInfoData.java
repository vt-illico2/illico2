package com.videotron.tvi.illico.itv.cinema.data;

import java.awt.Image;
import java.util.Vector;

/**
 * The Class FilmInfoData.
 * @version $Revision: 1.2 $ $Date: 2011/03/10 16:11:55 $
 */
public class FilmInfoData {

    /** The id. */
    private String id;

    /** The titre. */
    private String titre;

    /** The vfde. */
    private String vfde;

    /** The genre. */
    private String genre;

    /** The classement. */
    private String classement;

    /** The poster url. */
    private String posterURL;

    /** The release date. */
    private String releaseDate;

    /** The summary. */
    private String summary;

    /** The actors. */
    private Vector actors = new Vector();

    /** The directors. */
    private Vector directors = new Vector();;

    /** The detail photo. */
    private Vector detailPhoto = new Vector();

    /** The poster img. */
    private Image posterImg = null;

    /** The duration. */
    private String duration;

    /**
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(String duration) {
        this.duration = duration;
    }

    /**
     * Gets the vfde.
     * @return the vfde
     */
    public String getVfde() {
        return vfde;
    }

    /**
     * Sets the vfde.
     * @param vfde the new vfde
     */
    public void setVfde(String vfde) {
        this.vfde = vfde;
    }

    /**
     * Gets the id.
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     * @param id the new id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets the title.
     * @return the title
     */
    public String getTitle() {
        return titre;
    }

    /**
     * Sets the titre.
     * @param titre the new titre
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * Gets the genre.
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Sets the genre.
     * @param genre the new genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Gets the classement.
     * @return the classement
     */
    public String getClassement() {
        return classement;
    }

    /**
     * Sets the classement.
     * @param classement the new classement
     */
    public void setClassement(String classement) {
        this.classement = classement;
    }

    /**
     * Gets the poster url.
     * @return the poster url
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     * Sets the poster url.
     * @param posterURL the new poster url
     */
    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    /**
     * Gets the release date.
     * @return the release date
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * Sets the release date.
     * @param releaseDate the new release date
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * Gets the summary.
     * @return the summary
     */
    public String getSummary() {
        return summary;
    }

    /**
     * Sets the summary.
     * @param summary the new summary
     */
    public void setSummary(String summary) {
        this.summary = summary;
    }

    /**
     * Gets the actors.
     * @return the actors
     */
    public Vector getActors() {
        return actors;
    }

    /**
     * Sets the actors.
     * @param actors the new actors
     */
    public void setActors(Vector actors) {
        this.actors = actors;
    }

    /**
     * Gets the directors.
     * @return the directors
     */
    public Vector getDirectors() {
        return directors;
    }

    /**
     * Sets the directors.
     * @param directors the new directors
     */
    public void setDirectors(Vector directors) {
        this.directors = directors;
    }

    /**
     * Gets the detail photo.
     * @return the detail photo
     */
    public Vector getDetailPhoto() {
        return detailPhoto;
    }

    /**
     * Sets the detail photo.
     * @param detailPhoto the new detail photo
     */
    public void setDetailPhoto(Vector detailPhoto) {
        this.detailPhoto = detailPhoto;
    }
    
    /**
     * Gets the poster image.
     *
     * @return the posterImg
     */
    public Image getPosterImage() {
        return posterImg;
    }

    /**
     * Sets the poster image.
     *
     * @param posterImage the posterImg to set
     */
    public void setPosterImage(Image posterImage) {
        this.posterImg = posterImage;
    }
}

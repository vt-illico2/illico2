/*
 *  @(#)FlowEffect.java 1.0 2011.06.02
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;

/**
 * <code>FlowEffect</code> This class is effect util to flow on poster list.
 *
 * @since   2011.06.02
 * @version $Revision: 1.2 $ $Date: 2011/07/08 21:32:47 $
 * @author  tklee
 */
public class FlowEffect extends Effect {
    /** The left source rectangle. */
    private Rectangle leftSrc = null;
    /** The left from point. */
    private Point leftFrom = null;
    /** The left to point. */
    private Point leftTo = null;
    /** The right source rectangle. */
    private Rectangle rightSrc = null;
    /** The right from point. */
    private Point rightFrom = null;
    /** The right to point. */
    private Point rightTo = null;
    /** The focus source Rectangle. */
    private Rectangle focusSrc = null;
    /** The focus rectangle. */
    private Rectangle focusRec = null;
    /** The old focus rectangle. */
    private Rectangle oldFocusRec = null;
    /** The new focus rectangle. */
    private Rectangle newFocusSrc = null;

    /** The effect listener. */
    private EffectListener effectListener = null;

    /** The effect type. */
    private int effectType = 0;
    /** The left x position. */
    private int[] leftPointX = null;
    /** The right x position. */
    private int[] rightPointX = null;
    /** The old focus point x. */
    private int[] oldFocusPointX = null;
    /** The old focus point y. */
    private int[] oldFocusPointY = null;
    /** The old focus width. */
    private int[] oldFocusWidth = null;
    /** The old focus height. */
    private int[] oldFocusHeight = null;
    /** The new focus point x. */
    private int[] newFocusPointX = null;
    /** The new focus point y. */
    private int[] newFocusPointY = null;
    /** The new focus width. */
    private int[] newFocusWidth = null;
    /** The new focus height. */
    private int[] newFocusHeight = null;

    /**
     * Initialize the Component.
     *
     * @param c the Component
     * @param frameCount the frame count
     * @param listener the listener
     */
    public FlowEffect(final Component c, final int frameCount, final EffectListener listener) {
        super(c, frameCount);
        effectListener = listener;
        leftPointX = new int[frameCount];
        rightPointX = new int[frameCount];
        oldFocusPointX = new int[frameCount];
        oldFocusPointY = new int[frameCount];
        oldFocusWidth = new int[frameCount];
        oldFocusHeight = new int[frameCount];
        newFocusPointX = new int[frameCount];
        newFocusPointY = new int[frameCount];
        newFocusWidth = new int[frameCount];
        newFocusHeight = new int[frameCount];
        updateBackgroundBeforeStart(false);
    }

    /**
     * Start effect.
     *
     * @param type the effect type
     * @param leftSrcRec the left source rectangle
     * @param leftFromPoint the left from point
     * @param leftToPoint the left to point
     * @param rightSrcRec the right source rectangle
     * @param rightFromPoint the right from point
     * @param rightToPoint the right to point
     */
    public void start(int type, Rectangle leftSrcRec, Point leftFromPoint, Point leftToPoint,
            Rectangle rightSrcRec, Point rightFromPoint, Point rightToPoint) {
        effectType = type;
        leftSrc = leftSrcRec;
        leftFrom = leftFromPoint;
        leftTo = leftToPoint;
        rightSrc = rightSrcRec;
        rightFrom = rightFromPoint;
        rightTo = rightToPoint;
        if (Log.DEBUG_ON) {
            Log.printDebug("ListBuilderEffect|start() -leftFrom = " + leftFrom);
            Log.printDebug("ListBuilderEffect|start() -leftTo = " + leftTo);
        }
        int leftDiffX = 0;
        int rightDiffX = 0;
        if (leftFrom != null) { leftDiffX = leftTo.x - leftFrom.x; }
        if (rightFrom != null) { rightDiffX = rightTo.x - rightFrom.x; }
        for (int i = 0; i < frameCount; i++) {
            if (leftFrom != null) {
                leftPointX[i] = leftFrom.x + Math.round((float) (leftDiffX * i) / (frameCount - 1));
            }
            if (rightFrom != null) {
                rightPointX[i] = rightFrom.x + Math.round((float) (rightDiffX * i) / (frameCount - 1));
            }
        }
        super.start();
    }

    /**
     * set focus rectangles.
     *
     * @param focusSource the focus source
     * @param focusRectangle the focus rectangle
     * @param oldFocusRectangle the old focus rectangle
     * @param newFocusSource the new focus source
     */
    public void setFocusRectangle(Rectangle focusSource, Rectangle focusRectangle, Rectangle oldFocusRectangle,
            Rectangle newFocusSource) {
        focusSrc = focusSource;
        focusRec = focusRectangle;
        oldFocusRec = oldFocusRectangle;
        newFocusSrc = newFocusSource;
        for (int i = 0; i < frameCount; i++) {
            oldFocusPointX[i] = calcPosition(focusRec.x, oldFocusRec.x, i);
            oldFocusPointY[i] = calcPosition(focusRec.y, oldFocusRec.y, i);
            oldFocusWidth[i] = calcPosition(focusRec.width, oldFocusRec.width, i);
            oldFocusHeight[i] = calcPosition(focusRec.height, oldFocusRec.height, i);
            newFocusPointX[i] = calcPosition(newFocusSrc.x, focusRec.x, i);
            newFocusPointY[i] = calcPosition(newFocusSrc.y, focusRec.y, i);
            newFocusWidth[i] = calcPosition(newFocusSrc.width, focusRec.width, i);
            newFocusHeight[i] = calcPosition(newFocusSrc.height, focusRec.height, i);
        }
    }

    /**
     * Calculate position.
     *
     * @param fromPoint the from point
     * @param toPoint the to point
     * @param index the index
     * @return the calculated value
     */
    private int calcPosition(int fromPoint, int toPoint, int index) {
        return fromPoint + Math.round((float) ((toPoint - fromPoint) * index) / (frameCount - 1));
    }

    /**
     * Display a animation.
     * @param g Graphics.
     * @param image DVBBufferedImage to display.
     * @param frame a current frame.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        //Rectangle oldClip = g.getClipBounds();
        if (effectListener != null) { effectListener.requestPrepaint(g, component); }
        if (leftFrom != null) {
            int width = leftSrc.width;
            int height = leftSrc.height;
            g.drawImage(image, leftPointX[frame], leftTo.y, leftPointX[frame] + width, leftTo.y + height,
                    leftSrc.x, leftSrc.y, leftSrc.x + width, leftSrc.y + height, null);
        }
        if (rightFrom != null) {
            int width = rightSrc.width;
            int height = rightSrc.height;
            g.drawImage(image, rightPointX[frame], rightTo.y, rightPointX[frame] + width, rightTo.y + height,
                    rightSrc.x, rightSrc.y, rightSrc.x + width, rightSrc.y + height, null);
        }
        if (focusRec != null) {
            g.drawImage(image, oldFocusPointX[frame], oldFocusPointY[frame],
                    oldFocusPointX[frame] + oldFocusWidth[frame], oldFocusPointY[frame] + oldFocusHeight[frame],
                    focusSrc.x, focusSrc.y, focusSrc.x + focusSrc.width, focusSrc.y + focusSrc.height, null);
            g.drawImage(image, newFocusPointX[frame], newFocusPointY[frame],
                    newFocusPointX[frame] + newFocusWidth[frame], newFocusPointY[frame] + newFocusHeight[frame],
                    newFocusSrc.x, newFocusSrc.y,
                    newFocusSrc.x + newFocusSrc.width, newFocusSrc.y + newFocusSrc.height, null);
        }
        if (effectListener != null) { effectListener.requestPaint(g, component); }
    }
}

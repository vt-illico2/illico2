package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.ui.CinemaMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
/**
 * This class is the CinemaMain Renderer.
 * @version $Revision: 1.16 $ $Date: 2012/02/15 04:09:59 $
 * @author pellos
 */
public class CinemaMainRenderer extends BasicRenderer {
    /** Log image. */
    //private Image appLogo = null;
    /** Background image. */
    private Image bg = null;
    /** Menu Background image. */
    //private Image menuBg = null;
    /** Menu Shadow image. */
    //private Image menushadow = null;
    /** Button image. */
    public Image buttle01 = null;
    /** List line image. */
    public Image line = null;
    /** Menu Focus image. */
    public Image menuFocus = null;
    /** Menu Focus image. */
    public Image menuFocusDim = null;
    /** Banner Shadow image. */
    private Image bannerShadow = null;
    /** Banner Background image. */
    private Image[] bannerBgs = new Image[3];
    /** Banner Focus image. */
    private Image bannerFocus = null;
    /** Banner Focus right arrow image. */
    private Image arsR = null;
    /** Banner Focus left Arrow image. */
    private Image arsL = null;
    /** The arrow r img. */
    private Image arrowRImg = null;
    /** Banner images. */
    public Image[] bannerImage = null;
    /** Hot key background image. */
    private Image hotkeybg = null;
    /** The poster shadow of right. */
    private Image posterShadowR = null;
    /** The poster shadow of bottom. */
    private Image posterShadowB = null;
    /** The icon layer image. */
    //private Image iconLayerImg = null;
    /** The icon image. */
    private Image iconImg = null;
    /** The default poster img. */
    private Image defaultPosterImg = null;

    /** The film data of recent. */
    private RecentFilmData[] filmDatas = null;

    /** Exit String. */
    //private String toexit = null;
    /** Title String. */
    private String title = null;
    /** New release String. */
    private String newRelease = null;

    /** List String. */
    public String[] list = new String[3];
    /** The menu instructions. */
    private String[][] menuInst = new String[3][2];
    /** The widget inst. */
    private String widgetInst = null;
    /** The no content. */
    private String noContent = null;

    /** List index. */
    private int listIndex = 0;
    /** List or widget. */
    private boolean isListFocus = true;
    /** Widget page index. */
    private int widgetPos = 0;
    /** Widget index. */
    private int widgetFocusIndex = 0;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the UIComponent
     */
    public void prepare(UIComponent c) {
        bg = getImage("07_bg_main.jpg");
        //menuBg = getImage("00_menubg.png");
        //menushadow = getImage("00_menushadow.png");
        buttle01 = getImage("01_bullet01.png");
        line = getImage("00_mini_line.png");
        menuFocus = getImage("00_menufocus.png");
        menuFocusDim = getImage("00_menufocus_dim.png");
        //appLogo = getImage("app_logo_cinema.png");
        hotkeybg = getImage("01_hotkeybg_main.png");
        bannerShadow = getImage("00_banner_shadow.png");
        bannerFocus = getImage("00_banner_focus.png");
        arsR = getImage("02_ars_r.png");
        arsL = getImage("02_ars_l.png");
        posterShadowB = getImage("11_pos_sh.png");
        posterShadowR = getImage("11_pos_sh2.png");
        //iconLayerImg = getImage("Layer_7.png");
        //iconImg = getImage("00_icon01.png");
        iconImg = getImage("cinema.png");
        arrowRImg = getImage("01_arrow_r.png");
        for (int i = 0; i < bannerBgs.length; i++) {
            bannerBgs[i] = getImage("11_banner_bg0" + (i + 1) + ".png");
        }
        defaultPosterImg = getImage("11_default_81.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();

        title = getString("cine.homemenuHeader");
        newRelease = getString("cine.newLelease");
        for (int i = 0; i < menuInst.length; i++) {
            menuInst[i][0] = "";
            menuInst[i][1] = "";
        }
        String menuDesc = getString("cine.menuInst");
        Log.printDebug("CinemaMainRenderer|prepare() - menuDesc : " + menuDesc);
        String test = getString("cine.mainCon1");
        Log.printDebug("CinemaMainRenderer|prepare() - menuDesc : " + test);
        StringTokenizer st = new StringTokenizer(menuDesc, "|");
        for (int i = 0; st.hasMoreTokens(); i++) {
            String desc = st.nextToken();
            int index = desc.indexOf("^");
            if (index != -1) {
                menuInst[i][0] = desc.substring(0, index);
                menuInst[i][1] = desc.substring(index + 1);
            } else {
                menuInst[i][0] = desc;
                menuInst[i][1] = "";
            }
        }
        widgetInst = getString("cine.widgetInst");
        list[0] = getString("cine.movieTitles");
        list[1] = getString("cine.Theatres");
        list[2] = getString("cine.Preferences");
        noContent = getString("cine.noContent");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("07_bg_main.jpg");
        removeImage("00_menubg.png");
        removeImage("00_menushadow.png");
        removeImage("01_bullet01.png");
        removeImage("00_mini_line.png");
        removeImage("00_menufocus.png");
        removeImage("00_menufocus_dim.png");
        //removeImage("app_logo_cinema.png");
        removeImage("01_hotkeybg_main.png");
        removeImage("00_banner_shadow.png");
        removeImage("00_banner_focus.png");
        removeImage("02_ars_r.png");
        removeImage("02_ars_l.png");
        removeImage("11_pos_sh.png");
        removeImage("11_pos_sh2.png");
        removeImage("Layer_7.png");
        removeImage("00_icon01.png");
        removeImage("01_arrow_r.png");
        for (int i = 0; i < bannerBgs.length; i++) {
            removeImage("11_banner_bg0" + (i + 1) + ".png");
        }*/
        filmDatas = null;
        bannerImage = null;
    }

    /**
     * Sets the list Index, list or widget.
     * @param listIdx  listIndex
     * @param isList    isListFocus
     */
    public void setListIndex(int listIdx, boolean isList) {
        listIndex = listIdx;
        isListFocus = isList;
    }

    /**
     * Sets the widget index, the page index.
     * @param focus widgetFocusIndex
     * @param pos widget Position
     */
    public void setWidgetIndex(int focus, int pos) {
        widgetFocusIndex = focus;
        widgetPos = pos;
    }

    /**
     * Sets the widget Data.
     * @param data  widget data
     */
    public void setRecentFilmData(RecentFilmData[] data) {
        if (data != null) {
            bannerImage = new Image[data.length];
            /*LogDisplayer logDisplayer = LogDisplayer.getInstance();
            logDisplayer.imgStartTime = new long[data.length];
            logDisplayer.imgEndTime = new long[data.length];
            //TODO test
            Hashtable newImgList = new Hashtable();
            Hashtable cachedList = DataManager.cachedImgList;
            boolean isNew = false;
            //
            for (int i = 0; i < data.length; i++) {
                String posterUrl = data[i].getPosterURL();
                if (posterUrl == null || posterUrl.length() == 0) {
                    continue;
                }
                Image posterImg = DataCenter.getInstance().getImage(posterUrl);
                if (posterImg == null && posterUrl != null && posterUrl.length() > 0) {
                    try {
                        byte[] imgByte = null;
                        //TODO test
                        if (cachedList != null) { imgByte = (byte[]) cachedList.get(posterUrl); }
                        //
                        if (imgByte == null) {
                            logDisplayer.imgStartTime[i] = System.currentTimeMillis();
                            imgByte = HttpConnection.getInstance().getBytes(posterUrl);
                            logDisplayer.imgEndTime[i] = System.currentTimeMillis();
                            //TODO test
                            //if (imgByte != null) { newImgList.put(posterUrl, imgByte); }
                            if (imgByte != null) { isNew = true; }
                        }
                        if (imgByte != null) {
                            //TODO test
                            newImgList.put(posterUrl, imgByte);
                            posterImg = FrameworkMain.getInstance().getImagePool().createImage(imgByte, posterUrl);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                bannerImage[i] = posterImg;
            }
            FrameworkMain.getInstance().getImagePool().waitForAll();
            //TODO test
            if (newImgList.size() > 0 && isNew) {
                //if (cachedList != null) { newImgList.putAll(cachedList); }
                CacheManager.getInstance().setRecentImgList(newImgList);
            }
            //*/
        }
        filmDatas = data;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bg, 0, 0, c);
        CinemaMain ui = (CinemaMain) c;
        //tklee
        //if (filmDatas == null || ui.isMainMenuEffect) { return; }
        if (ui.isMainMenuEffect) { return; }

        /*for (int i = 0; i < list.length; i++) {
            int addedPos = i * 34;
            g.drawImage(line, 66, 139 + addedPos, c);
            g.drawImage(buttle01, 72, 119 + addedPos, c);
            if (i == listIndex) {
                g.setColor(Rs.C003003003);
                g.setFont(Rs.F21);
                g.drawImage(isListFocus ? menuFocus : menuFocusDim, 48, 101 + addedPos, c);
            } else {
                g.setColor(Rs.C224224224);
                g.setFont(Rs.F18);
            }
            g.drawString(list[i], 95, 128 + addedPos);
        }*/

        //g.drawImage(appLogo, 57, 482, c);
        g.drawImage(iconImg, 58, 76, c);

        g.setFont(Rs.F20);
        g.setColor(Rs.C046046045);
        g.drawString(title, 91, 95);
        g.setColor(Rs.C214181061);
        g.drawString(title, 90, 94);
        g.drawImage(hotkeybg, 405, 466, c);

        g.setFont(Rs.F28);
        g.setColor(Rs.C187189192);
        String tempInst = menuInst[listIndex][0];
        if (!isListFocus) { tempInst = newRelease; }
        //g.drawString(inst, 375, 97);
        String[] inst = TextUtil.split(tempInst, g.getFontMetrics(), 188);
        Logger.debug(this, "inst.length : " + inst.length);
        for (int i = 0; i < inst.length; i++) {
            g.drawString(inst[i], 375, 97 + (i * 30));
        }
        int yPos = (inst.length - 1) * 30;
        if (yPos < 0) { yPos = 0; }

        g.setFont(Rs.F14_D);
        g.setColor(Rs.C143143143);
        String[] str = null;
        if (isListFocus) {
            str = TextUtil.split(menuInst[listIndex][1], g.getFontMetrics(), 188);
        } else { str = TextUtil.split(widgetInst, g.getFontMetrics(), 188); }
        for (int i = 0; i < str.length; i++) {
            g.drawString(str[i], 375, yPos + 121 + (i * 17));
        }

        g.setFont(Rs.F19);
        g.setColor(Rs.C255255255);
        g.drawString(newRelease, 375, 325);

        int totalCount = 0;
        if (bannerImage != null) { totalCount = bannerImage.length; }
        if (isListFocus && totalCount > 3) { g.drawImage(arrowRImg, 914, 392, c); }
        g.setFont(Rs.F16);
        int initNum = widgetFocusIndex - widgetPos;
        for (int i = initNum, j = 0; j < 3; i++, j++) {
            int addedPos = j * 180;
            g.drawImage(bannerShadow, 353 + addedPos, 444, c);
            g.drawImage(bannerBgs[j], 374 + addedPos, 363, c);
            if (i != widgetFocusIndex || isListFocus) {
                Image posterImage = null;
                if (i < totalCount) { posterImage = bannerImage[i]; }
                if (posterImage == null) { posterImage = defaultPosterImg; }
                if (posterImage != null) { g.drawImage(posterImage, 380 + addedPos, 336, 81, 118, c); }
            }
            g.drawImage(posterShadowR, 461 + addedPos, 363, c);
            g.drawImage(posterShadowB, 380 + addedPos, 454, c);

            String title = noContent;
            if (i < totalCount) {
                title = filmDatas[i].getTitle();
                if (title == null || title.length() == 0) { title = filmDatas[i].getVfde(); }
            }
            str = TextUtil.split(title, g.getFontMetrics(), 544 - 467);
            int checkLength = str.length;
            if (checkLength > 5) { checkLength = 5; }
            int baseYPos = 406 - ((checkLength - 1) * 8);
            for (int k = 0; k < checkLength; k++) {
                g.drawString(str[k], 467 + addedPos, baseYPos + (k * 16));
            }
        }

        //TODO
        if (initNum + 2 < totalCount - 1) {

        }

        if (!isListFocus) {
            int addedPos = widgetPos * 180;
            g.drawImage(bannerFocus, 366 + addedPos, 357, c);
            Image posterImage = bannerImage[widgetFocusIndex];
            if (posterImage == null) { posterImage = defaultPosterImg; }
            if (posterImage != null) { g.drawImage(posterImage, 380 + addedPos, 336, 81, 118, c); }
            if (widgetFocusIndex > 0) { g.drawImage(arsL, 353 + addedPos, 392, c); }
            if (widgetFocusIndex < totalCount - 1) { g.drawImage(arsR, 554 + addedPos, 392, c); }
        }
        //super.paint(g, c);
    }
}

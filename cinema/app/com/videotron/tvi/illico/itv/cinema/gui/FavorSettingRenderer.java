/*
 *  @(#)FavorSettingRenderer.java 1.0 2011.03.05
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.ui.FavorSetting;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>FavorSettingRenderer</code> The class to paint a GUI for the favorite setting menu.
 *
 * @since   2011.03.05
 * @version $Revision: 1.8 $ $Date: 2011/06/02 02:04:21 $
 * @author  tklee
 */
public class FavorSettingRenderer extends BasicRenderer {
    /** The top shadow img. */
    private Image topShadowImg = null;
    /** The list shadow img. */
    private Image listShadowImg = null;
    /** The list middle bg img. */
    private Image listMiddleBgImg = null;
    /** The list top bg img. */
    private Image listTopBgImg = null;
    /** The list line img. */
    private Image listLineImg = null;
    /** The list title shadow img. */
    private Image listTitleShadowImg = null;
    /** The plus icon img. */
    public Image plusIconImg = null;
    /** The minus icon img. */
    public Image minusIconImg = null;
    /** The plus icon img. */
    public Image plusIconFocusImg = null;
    /** The minus icon img. */
    public Image minusIconFocusImg = null;
    /** The list top shadow img. */
    public Image listTopShadowImg = null;
    /** The list bottom shadow img. */
    public Image listBottomShadowImg = null;
    /** The list focus img. */
    public Image listFocusImg = null;
    /** The list focus dim img. */
    public Image listFocusDimImg = null;
    /** The list arrow top img. */
    public Image listArrowTopImg = null;
    /** The list arrow bottom img. */
    public Image listArrowBottomImg = null;
    /** The list glow img. */
    private Image listGlowImg = null;
    /** The list glow bottom img. */
    private Image listGlowBottomImg = null;
    /** The favor icon img. */
    private Image favorIconImg = null;
    /** The btn focus img. */
    private Image btnFocusImg = null;
    /** The btn focus dim img. */
    private Image btnFocusDimImg = null;
    /** The btn off img. */
    private Image btnOffImg = null;
    /** The btn arrow right focus img. */
    private Image btnArrowRightFocusImg = null;
    /** The btn arrow right dim img. */
    private Image btnArrowRightDimImg = null;
    /** The btn arrow left focus img. */
    private Image btnArrowLeftFocusImg = null;
    /** The btn arrow left dim img. */
    private Image btnArrowLeftDimImg = null;

    /** The title txt. */
    private String titleTxt = null;
    /** The menu txt. */
    private String[] menuTxt = new String[2];
    /** The empty txt. */
    public String emptyTxt = null;
    /** The add txt. */
    private String addTxt = null;
    /** The remove txt. */
    private String removeTxt = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        topShadowImg = getImage("08_top_sh.png");
        listShadowImg = getImage("08_listshadow.png");
        listMiddleBgImg = getImage("08_listbg_m.png");
        listTopBgImg = getImage("08_listbg_t.png");
        listLineImg = getImage("08_listline.png");
        listTitleShadowImg = getImage("08_list_titlesh.png");
        plusIconImg = getImage("03_icon_plus.png");
        minusIconImg = getImage("03_icon_minus.png");
        plusIconFocusImg = getImage("03_icon_plus_foc.png");
        minusIconFocusImg = getImage("03_icon_minus_foc.png");
        listTopShadowImg = getImage("07_list_sh_t.png");
        listBottomShadowImg = getImage("07_list_sh_b.png");
        listFocusImg = getImage("07_list_foc.png");
        listFocusDimImg = getImage("08_list_foc_dim.png");
        listArrowTopImg = getImage("02_ars_t.png");
        listArrowBottomImg = getImage("02_ars_b.png");
        listGlowImg = getImage("11_listglow_t.png");
        listGlowBottomImg = getImage("11_listglow_b.png");
        favorIconImg = getImage("08_sc_icon_fav.png");
        btnFocusImg = getImage("05_focus.png");
        btnFocusDimImg = getImage("05_focus_dim.png");
        btnOffImg = getImage("08_bt_off_dim.png");
        btnArrowRightFocusImg = getImage("08_add_ar_r.png");
        btnArrowRightDimImg = getImage("08_add_ar_r_dim.png");
        btnArrowLeftFocusImg = getImage("08_add_ar_l.png");
        btnArrowLeftDimImg = getImage("08_add_ar_l_dim.png");
        super.prepare(c);

        titleTxt = DataCenter.getInstance().getString("cine.favorSetTitle");
        menuTxt[0] = DataCenter.getInstance().getString("cine.favorMenu1");
        menuTxt[1] = DataCenter.getInstance().getString("cine.favTheatres");
        emptyTxt = DataCenter.getInstance().getString("cine.Empty");
        addTxt = DataCenter.getInstance().getString("cine.Add");
        removeTxt = DataCenter.getInstance().getString("cine.Remove");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("01_hotkeybg.png");
        removeImage("18_top_sh.png");
        removeImage("08_listshadow.png");
        removeImage("08_listbg_m.png");
        removeImage("08_listbg_t.png");
        removeImage("08_listline.png");
        removeImage("08_list_titlesh.png");
        removeImage("03_icon_plus.png");
        removeImage("03_icon_minus.png");
        removeImage("07_list_sh_t.png");
        removeImage("07_list_sh_b.png");
        removeImage("07_list_foc.png");
        removeImage("08_list_foc_dim.png");
        removeImage("02_ars_t.png");
        removeImage("02_ars_b.png");
        removeImage("11_listglow_t.png");
        removeImage("08_sc_icon_fav.png");
        removeImage("05_focus.png");
        removeImage("05_focus_dim.png");
        removeImage("08_bt_off_dim.png");
        removeImage("08_add_ar_r.png");
        removeImage("08_add_ar_r_dim.png");
        removeImage("08_add_ar_l_dim.png");
        removeImage("08_add_ar_l.png");*/
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    public void paint(Graphics g, UIComponent c) {
        FavorSetting ui = (FavorSetting) c;
        g.drawImage(bgImg, 0, 0, c);
        Vector list = ui.list;
        if (list == null) { return; }
        g.drawImage(hotkeybgImg, 236, 466, 960 - 236, 17, c);
        g.drawImage(topShadowImg, 0, 77, c);
        g.setFont(Rs.F26);
        g.setColor(Rs.C255255255);
        g.drawString(titleTxt, 55, 113);

        Vector favorList = ui.favorList;
        int favorCount = favorList.size();
        int lineGap = 32;
        int addPos = 0;
        int addPosX = 0;
        for (int j = 0; j < 2; j++) {
            addPosX = j * 505;
            g.drawImage(listTopBgImg, 54 + addPosX, 136, c);
            for (int i = 0; i < 5; i++) {
                addPos = i * lineGap;
                g.drawImage(listMiddleBgImg, 54 + addPosX, 304 + addPos, c);
            }
            g.drawImage(listShadowImg, 7 + addPosX, 455, c);
            int checkCount = FavorSetting.ROWS_PER_PAGE - 1;
            if (j == 1) {
                checkCount = 4;
                if (favorCount == 5) { checkCount = 5; }
            }
            for (int i = 0; i < checkCount; i++) {
                addPos = i * lineGap;
                if (j == 0 || (j == 1 && (i != favorCount - 1 || favorCount == 5))) {
                    g.drawImage(listLineImg, 69 + addPosX, 207 + addPos, c);
                }
            }
            if (j == 0 || favorCount > 0) { g.drawImage(listTitleShadowImg, 54 + addPosX, 169, c); }
            String menuName = null;
            if (j == 0) {
                menuName = menuTxt[j] + " (" + ui.theatreTotalCount + ")";
            } else { menuName = menuTxt[j] + " (" + favorCount + "/" + FavorSetting.MAX_FAVORITE + ")"; }
            g.setFont(Rs.F20);
            g.setColor(Rs.C000000000);
            if (j == 1) { addPosX += 27; }
            g.setColor(Rs.C214182055);
            g.drawString(menuName, 67 + addPosX, 160);
        }
        g.drawImage(favorIconImg, 565, 143, c);
        if (favorCount < FavorSetting.MAX_FAVORITE) {
            int emptyCount = FavorSetting.MAX_FAVORITE - favorCount;
            g.setColor(Rs.DVB30C7);
            int addYPos = 0;
            int addHeight = 0;
            if (favorCount > 0) {
                addYPos = 4;
            } else { addHeight = 4; }
            g.fillRect(559, 172 + (favorCount * 32) + addYPos, 347,  emptyCount * lineGap + addHeight);
            g.drawImage(listGlowBottomImg, 560, 336, c);
            if (favorCount > 0) { g.drawImage(listGlowImg, 560, 336 - (emptyCount * lineGap) - 7, c); }
        }

        int stateFocus = ui.stateFocus;
        int totalCount = list.size();
        if (totalCount > 0) {
            if ((stateFocus == FavorSetting.STATE_THEATRE || (stateFocus == FavorSetting.STATE_BTN
                    && ui.btnState == FavorSetting.BTN_ADD))) {
                Image image = listFocusImg;
                if (stateFocus == FavorSetting.STATE_BTN) { image = listFocusDimImg; }
                if (image != null) { g.drawImage(image, 52, 172 + (ui.focusPosition * lineGap), c); }
            }
            /*if (stateFocus == FavorSetting.STATE_THEATRE) {
                int initNum = ui.theatreFocus - ui.focusPosition;
                int lastNum = 0;
                if (totalCount < FavorSetting.ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = ui.theatreFocus + FavorSetting.ROWS_PER_PAGE - 1 - ui.focusPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                if (initNum > 0) { g.drawImage(listArrowTopImg, 216, 165, c); }
                if (lastNum < totalCount - 1) { g.drawImage(listArrowBottomImg, 216, 456, c); }
            }*/
        }

        if ((stateFocus == FavorSetting.STATE_FAVORITE
                || (stateFocus == FavorSetting.STATE_BTN && ui.btnState == FavorSetting.BTN_REMOVE))) {
            addPos = ui.favorFocus * lineGap;
            Image image = listFocusImg;
            if (stateFocus == FavorSetting.STATE_BTN) { image = listFocusDimImg; }
            g.drawImage(image, 557, 172 + addPos, c);
        }

        g.setFont(Rs.F18);
        Image[][] btnArrImage = {{btnArrowRightFocusImg, btnArrowRightDimImg},
                {btnArrowLeftFocusImg, btnArrowLeftDimImg}};
        String[] names = {addTxt, removeTxt};
        int[] arrPosX = {533, 419};
        for (int i = 0; i < 2; i++) {
            addPos = i * 43;
            g.setColor(Rs.C110);
            Image btnImg = btnOffImg;
            Image arrImg = btnArrImage[i][1];
            if (i == ui.btnState) {
                g.setColor(Rs.C000000000);
                btnImg = btnFocusDimImg;
                arrImg = btnArrImage[i][0];
                if (ui.stateFocus == FavorSetting.STATE_BTN) { btnImg = btnFocusImg; }
            }
            g.drawImage(btnImg, 410, 268 + addPos, c);
            g.drawImage(arrImg, arrPosX[i], 277 + addPos, c);
            GraphicUtil.drawStringCenter(g, names[i], 481, 290 + addPos);
            if (i != ui.btnState) {
                g.setColor(Rs.C055055055);
                GraphicUtil.drawStringCenter(g, names[i], 480, 289 + addPos);
            }
        }
        super.paint(g, c);
    }
}

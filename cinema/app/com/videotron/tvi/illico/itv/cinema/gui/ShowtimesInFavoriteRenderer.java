/*
 *  @(#)ShowtimesInFavoriteRenderer.java 1.0 2011.03.03
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ShowtimesInFavoriteRenderer</code> The class to paint a GUI for movie showtimes selected by user.
 *
 * @since   2011.03.03
 * @version $Revision: 1.11 $ $Date: 2011/06/24 23:21:24 $
 * @author  tklee
 */
public class ShowtimesInFavoriteRenderer extends BasicRenderer {
    /** The en icon. */
    private Image enIconImg = null;
    /** The fr icon. */
    private Image frIconImg = null;
    /** The poster shadow. */
    private Image posterShadowImg = null;
    /** The poster dim. */
    private Image posterDimImg = null;
    /** The detail bg shadow img. */
    public Image detailBgShadowImg = null;
    /** The btn focus img. */
    private Image btnFocusImg = null;
    /** The btn dim img. */
    private Image btnDimImg = null;
    /** The icon focus img. */
    private Image iconFocusImg = null;
    /** The icon dim img. */
    private Image iconDimImg = null;
    /** The bottom shadow img. */
    private Image bottomShadowImg = null;
    /** The top shadow img. */
    private Image topShadowImg = null;
    /** The top arrow img. */
    private Image topArrowImg = null;
    /** The bottom arrow img. */
    private Image bottomArrowImg = null;
    /** The default poster img. */
    private Image defaultPosterImg = null;

    /** Release String. */
    private String releaseDateTxt = null;
    /** The no showtimes txt. */
    private String noShowtimesTxt = null;

    /** Film Data. */
    public FilmInfoData filmInfoData = null;
    /** The theatre list. */
    public Vector theatreList = null;
    /** The btn focus. */
    public int btnFocus = 0;
    /** The btn position. */
    public int btnPosition = 0;
    /** The lang type. */
    public String langType = "";

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        enIconImg = getImage("icon_en.png");
        frIconImg = getImage("icon_fr.png");
        posterShadowImg = getImage("01_possh_221.png");
        posterDimImg = getImage("01_poshigh_221.png");
        hotkeybgImg = getImage("01_hotkeybg.png");
        detailBgShadowImg = getImage("01_dtail_sh.png");
        btnFocusImg = getImage("11_btn_theater_f.png");
        btnDimImg = getImage("11_btn_theater.png");
        iconFocusImg = getImage("02_icon_fav_foc.png");
        iconDimImg = getImage("02_icon_fav.png");
        bottomShadowImg = getImage("11_lotto_sh_b.png");
        topShadowImg = getImage("11_lotto_sh_t2.png");
        topArrowImg = getImage("02_ars_t.png");
        bottomArrowImg = getImage("02_ars_b.png");
        defaultPosterImg = getImage("11_default_221.png");
        super.prepare(c);

        releaseDateTxt = DataCenter.getInstance().getString("cine.releaseDate");
        noShowtimesTxt = DataCenter.getInstance().getString("cine.noShowtimes");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("icon_en.png");
        removeImage("icon_fr.png");
        removeImage("01_possh_221.png");
        removeImage("01_poshigh_221.png");
        removeImage("01_hotkeybg.png");
        removeImage("01_dtail_sh.png");
        removeImage("01_hotkeybg.png");
        removeImage("11_btn_theater_f.png");
        removeImage("11_btn_theater.png");
        removeImage("02_icon_fav_foc.png");
        removeImage("02_icon_fav.png");
        removeImage("11_lotto_sh_b.png");
        removeImage("02_ars_t.png");
        removeImage("02_ars_b.png");
        for (int i = 0; i < gradeImgName.length; i++) {
            removeImage(gradeImgName[i]);
        }*/
        filmInfoData = null;
        theatreList = null;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        if (theatreList == null || filmInfoData == null) { return; }
        g.drawImage(detailBgShadowImg, 289, 202, c);

        //info
        g.setFont(Rs.F23);
        g.setColor(Rs.C255204000);
        //String[] langData = filmData.getLangData();
        String title = filmInfoData.getTitle();
        if (!langType.equals(FilmData.LANGUAGE_FRENCH)) { title = filmInfoData.getVfde(); }
        g.drawString(title, 310, 134);

        g.setColor(Rs.C200);
        g.setFont(Rs.F16);
        FontMetrics fm = g.getFontMetrics();
        String str = filmInfoData.getGenre();
        if (str == null) { str = ""; }
        String temp = filmInfoData.getDuration();
        if (temp != null && temp.length() > 0) { str += " | " + temp; }
        int pos = 311;
        String filmLang = langType;
        Image langIcon = null;
        if (filmLang.equals(FilmData.LANGUAGE_FRENCH)) {
            langIcon = frIconImg;
        } else if (filmLang.equals(FilmData.LANGUAGE_ENGLISH)) { langIcon = enIconImg; }
        Image gradeImg = getGradeImage(filmInfoData.getClassement());
        if (langIcon != null || gradeImg != null) {
            if (str.length() > 0) {
                str += " | ";
                pos += fm.stringWidth(str);
            }
            if (langIcon != null) {
                g.drawImage(langIcon, pos, 154, c);
                pos += 28;
            }
            if (gradeImg != null) { g.drawImage(gradeImg, pos, 154, c); }
        }
        g.drawString(str, 311, 165);

        str = releaseDateTxt + " : ";
        str += filmInfoData.getReleaseDate();
        g.drawString(str, 311, 183);

        //theatres schedule
        int totalCount = theatreList.size();
        if (totalCount > 0) {
            int initNum = btnFocus - btnPosition;
            int lastNum = initNum + 1;
            if (lastNum >= totalCount) { lastNum = totalCount - 1; }
            String[] strArr = null;
            for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                int addedPos = j * 123;
                if (i == btnFocus) {
                    g.drawImage(btnFocusImg, 309, 233 + addedPos, c);
                    g.drawImage(iconFocusImg, 314, 238 + addedPos, c);
                    g.setColor(Rs.C000000000);
                } else {
                    g.drawImage(btnDimImg, 309, 233 + addedPos, c);
                    g.drawImage(iconDimImg, 314, 238 + addedPos, c);
                    g.setColor(Rs.C255255255);
                }

                TheatreData theaData = (TheatreData) theatreList.elementAt(i);
                g.setFont(Rs.F18);
                strArr = TextUtil.split(theaData.getDefaultName(), g.getFontMetrics(), 516 - 342, 2);
                paintList(g, c, strArr, 342, 254 + addedPos, 16);
                g.setFont(Rs.F15);
                strArr = TextUtil.split(theaData.getAddress(), g.getFontMetrics(), 516 - 342, 3);
                paintList(g, c, strArr, 342, 290 + addedPos, 14);

                if (i != btnFocus) {
                    g.setColor(Rs.C255255255);
                } else { g.setColor(Rs.C224224224); }
                g.setFont(Rs.F16);
                FilmData film = (FilmData) theaData.getHfilms().get(filmInfoData.getId() + langType);
                if (film != null && !film.getSchedule().isEmpty()) {
                    Vector vSchedule = film.getSchedule();
                    String schedule = "";
                    for (int k = 0; k < vSchedule.size(); k++) {
                        schedule += (String) vSchedule.elementAt(k) + "\n";
                    }
                    strArr = TextUtil.split(schedule, g.getFontMetrics(), 912 - 540, 7);
                    paintList(g, c, strArr, 540, 244 + addedPos, 17);
                } else {
                    strArr = TextUtil.split(noShowtimesTxt, g.getFontMetrics(), 912 - 540, 7);
                    paintList(g, c, strArr, 540, 244 + addedPos, 17);
                }
            }
        }
        if (btnPosition == 0) { g.drawImage(bottomShadowImg, 0, 399, c); }
        if (btnPosition == 1) { g.drawImage(topShadowImg, 0, 215, c); }
        //poster
        Image posterImage = filmInfoData.getPosterImage();
        if (posterImage == null) { posterImage = defaultPosterImg; }
        if (posterImage != null) {
            g.drawImage(posterImage, 59, 120, 221, 332, c);
            g.drawImage(posterDimImg, 59, 120, c);
            g.drawImage(posterShadowImg, 31, 443, c);
        }
        g.drawImage(hotkeybgImg, 236, 466, 683, 17, c);
        if (btnFocus > 0 && totalCount > 2) { g.drawImage(topArrowImg, 566, 208, c); }
        if (btnFocus < totalCount - 1 && totalCount > 2) { g.drawImage(bottomArrowImg, 566, 460, c); }
        super.paint(g, c);
    }

    /**
     * Graphics paint the List.
     *
     * @param g the Graphics
     * @param c the UIComponent
     * @param strArr the str arr
     * @param posX the pos x
     * @param posY the pos y
     * @param lineGap the line gap
     */
    protected void paintList(Graphics g, UIComponent c, String[] strArr, int posX, int posY, int lineGap) {
        for (int i = 0; strArr != null && i < strArr.length; i++) {
            int addPos = i * lineGap;
            g.drawString(strArr[i], posX,  posY + addPos);
        }
    }
}

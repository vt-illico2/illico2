/*
 *
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Locale;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.HttpConnection;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager.LayeredUIHandler;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.data.RegionInfo;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ClockComponent;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ContentImagePool;
import com.videotron.tvi.illico.itv.cinema.ui.comp.LocationComponent;
import com.videotron.tvi.illico.itv.cinema.ui.popup.ConfirmPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.Popup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.SelectPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.TheatreSelectPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.TitleSearchPopup;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This Class is the BasicUI class. All Scene is drawn on BasicUI.
 * @version $Revision: 1.24 $ $Date: 2012/02/23 06:11:11 $
 * @author pellos
 */
public abstract class BasicUI extends UIComponent {
    /** serialVersionUID for serializable type.*/
    private static final long serialVersionUID = 1L;
    /** The id region data. */
    protected static final String ID_REGION_DATA = "regionData";
    /** The Constant ID_REGION_DATA_FOCUSED. */
    protected static final String ID_REGION_DATA_FOCUSED = "regionDataFocused";
    /** The id region data. */
    protected static final String ID_IMAGE_DATA = "imageData";

    /** The Constant ERRMSG_NONE. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_NONE = 0;
    /** The Constant ERRMSG_PREV. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_PREV = 1;
    /** The Constant ERRMSG_EXIT. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_EXIT = 2;

    /** The ClickingEffect instance. */
    protected ClickingEffect clickEffect = null;
    /** Is viewing current location. */
    private static LocationComponent locationComp = null;
    /** Is viewing current time. */
    private static ClockComponent clockComp = null;
    /** Sets hot key. */
    protected static ButtonIcons buttonIcons = null;
    /** The Footer instance. */
    protected static Footer footer = null;
    /** The popup bg comp. */
    private static PopupBgComp popupBgComp = null;
    /** The theatre select popup. */
    protected static TheatreSelectPopup theatreSelectPopup = null;
    /** The notification popup. */
    //protected static NotiPopup notiPopup = null;
    /** The confirm popup. */
    protected static ConfirmPopup confirmPopup = null;
    /** The select popup. */
    protected static SelectPopup selectPopup = null;
    /** The title search popup. */
    protected static TitleSearchPopup titleSearchPopup = null;
    /** The OptionScreen. */
    protected static OptionScreen optScr = null;
    /** ScrollTexts Instance. */
    protected static ScrollTexts scrollText = null;
    /** The scroll shadow component. */
    protected static ScrollShadowComp scrollShadowComp = null;
    /** The popup. */
    public static Popup popup = null;
    /** The data loading thread. */
    private DataLoadingThread dataLoadingThread = null;
    /** The popup container. */
    private Container popupContainer = null;
    /** The LayeredUIHandler for Popup. */
    private LayeredUIHandler popupLayeredHandler = null;
    /** The Constant FR_COLLATOR. */
    //protected static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);
    /** The btn image. */
    public static Image[] btnImage = null;
    /** The btn name. */
    public static String[] btnName = null;

    /** D-Opton Sorting MenuItem. */
    //protected static final MenuItem BY_ALPHABETICAL = new MenuItem("TxtOption.Alphabetical");
    /** The Constant BY_RELEASED. */
    //private static final MenuItem BY_RELEASED = new MenuItem("TxtOption.Released");
    /** The Constant BY_GENRE. */
    protected static final MenuItem BY_GENRE = new MenuItem("TxtOption.Genre");
    /** D-Opton MenuItem. */
    protected static final MenuItem OPTION_PREFERENCE = new MenuItem("TxtOption.Preference");
    /** The Constant OPTION_SORT_LANG. */
    protected static final MenuItem OPTION_SORT_LANG = new MenuItem("TxtOption.sortLang");
    /** The Constant OPTION_SORT_ALPHA. */
    protected static final MenuItem OPTION_SORT_ALPHA = new MenuItem("TxtOption.sortAlpha");
        //new MenuItem("TxtOption.Sorting", new MenuItem[] {BY_ALPHABETICAL, BY_GENRE});
    /** The Constant OPTION_SORTING. */
    protected static final MenuItem OPTION_SORTING = new MenuItem("TxtOption.Sorting", 
            new MenuItem[] {OPTION_SORT_ALPHA, OPTION_SORT_LANG});
    /** The Constant OPTION_HELP. */
    protected static final MenuItem OPTION_HELP = new MenuItem("TxtOption.Help");

    /** Is stored the previous Scene ID. */
    protected static int previousSceneID = -1;
    /** Is stored the value. */
    protected static Object sendValue = null;
    /** The focus history. */
    protected Object[] focusHistory = null;
    /** The err msg action. */
    protected int errMsgAction = 0;

    /**
     * Initialize a BasicUI and a Scene.
     */
    public void init() {
        popupContainer = SceneManager.getInstance().popupContainer;
        popupLayeredHandler = SceneManager.getInstance().popupLayeredHandler;
        dataLoadingThread = new DataLoadingThread();
        if (locationComp == null) {
            //dataLoadingThread = new DataLoadingThread();
            locationComp = new LocationComponent();
            locationComp.setBounds(Rs.SCENE_BOUND);

            buttonIcons = new ButtonIcons();
            buttonIcons.setBounds(210, 484, 910 - 200, 30);
            footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);

            clockComp = new ClockComponent();
            clockComp.setBounds(Rs.SCENE_BOUND);

            popupBgComp = new PopupBgComp();
            popupBgComp.setBounds(Rs.SCENE_BOUND);

            theatreSelectPopup = new TheatreSelectPopup();
            theatreSelectPopup.setLocation(327, 106);

            //notiPopup = new NotiPopup();
            //notiPopup.setLocation(276, 113);
            scrollShadowComp = new ScrollShadowComp();
            confirmPopup = new ConfirmPopup();
            confirmPopup.setLocation(276, 113);
            selectPopup = new SelectPopup();
            titleSearchPopup = new TitleSearchPopup();
            //titleSearchPopup.setLocation(220, 37);
        }
        initScene();
    }

    /**
     * Start the Scene.
     * @param reset whether resources is reset or not
     */
    public void start(boolean reset) {
        if (optScr == null) {
            locationComp.init();
            buttonIcons.init();
            btnImage = buttonIcons.imgButtonIcons;
            btnName = buttonIcons.buttonNames;
            clockComp.init();
            theatreSelectPopup.init();
            //notiPopup.init();
            confirmPopup.init();
            selectPopup.init();
            titleSearchPopup.init();
            optScr = new OptionScreen();
            scrollText = new ScrollTexts();
        }
        footer.reset();

        Object[] data = SceneManager.getInstance().getHistorySceneValue();
        if (data != null) {
            previousSceneID = Integer.parseInt((String) data[0]);
            sendValue = data[1];
            focusHistory = null;
            if  (data.length > 2) { focusHistory = (Object[]) data[2]; }
        }
        startScene(reset);
    }

    /**
     * Sets the focus history.
     *
     * @param focusHistory the new focus history
     */
    protected void setFocusHistory(Object[] focusHistory) {
        Object[] data = SceneManager.getInstance().getHistorySceneValue();
        if (data != null && data.length > 2) { data[2] = focusHistory; }
    }

    /**
     * Sets the focus history. if need, each scene implements this method.
     */
    protected void setFocusHistory() { }

    /**
     * Gets the current scene id.
     *
     * @return the current scene id
     */
    protected String getCurrentSceneId() {
        return String.valueOf(SceneManager.getInstance().getCurrentSceneId());
    }

    /**
     * Start components.
     */
    public void startComp() {
        startComp("");
    }

    /**
     * Start component.
     *
     * @param locTxt the loc txt
     */
    public void startComp(String locTxt) {
        locationComp.start(SceneManager.getInstance().getCurrentSceneId(), locTxt);
        clockComp.start();
        add(locationComp);
        //add(buttonIcons);
        add(footer);
        add(clockComp);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        setVisible(false);
        setFocusHistory();
        removePopup();
        removeAll();
        stopScene();
    }

    /**
     * Is disposed the scene.
     */
    public void dispose() {
        stopScene();
        removeAll();
        disposeScene();
        if (dataLoadingThread != null) { dataLoadingThread.stop(); }
        optScr = null;
        scrollText = null;
        previousSceneID = -1;
    }

    /**
     * implement handleKey method.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) { Log.printDebug("[Scene.handleKey]code : " + code); }
        //if (popup != null && popup.handleKey(code)) { return true; }
        if (popup != null) {
            popup.handleKey(code);
            return true;
        }

        if (code == Rs.KEY_EXIT) {
            if (Log.DEBUG_ON) { Log.printDebug("[Scene.handleKey]Exit Key Pressed."); }
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_EXIT]);
            MainManager.getInstance().exitToChannel();
            return true;
        }
        if (MainManager.getInstance().isLoadingAnimation()) { return true; }
        if (keyAction(code)) { return true; }

        switch (code) {
        case KeyCodes.COLOR_D:
        case KeyCodes.SEARCH:
            return true;
        case KeyCodes.RECORD:
        case KeyCodes.PAUSE:
        case KeyCodes.REWIND:
        case KeyCodes.FAST_FWD:
        case KeyCodes.PLAY:
        case KeyCodes.STOP:
        case KeyCodes.FORWARD:
        case KeyCodes.BACK:
        case Rs.KEY_CH_UP:
        case Rs.KEY_CH_DOWN:
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            return SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_MAIN;
        default:
            return false;
        }
    }

    /**
     * Adds the footer button.
     *
     * @param btnId the button id
     */
    protected void addFooterBtn(int[] btnId) {
        footer.reset();
        for (int i = 0; btnId != null && i < btnId.length; i++) {
            footer.addButton(btnImage[btnId[i]], btnName[btnId[i]]);
        }
    }

    /**
     * add Popup component to UI.
     *
     * @param   popupComp   Popup Object to add.
     */
    protected void addPopup(Popup popupComp) {
        if (popup == popupComp) {
            repaint();
            return;
        }
        removePopup();
        popup = popupComp;
        if (popup == null) {
            repaint();
            return;
        }
        popup.start();
        popup.setListener(this);
        popupContainer.add(popupBgComp, 0);
        popupContainer.add(popup, 0);
        popupLayeredHandler.activate();
        SceneManager.getInstance().popupContainer.repaint();
    }

     /**
      * remove Popup on UI.
      */
     protected void removePopup() {
         if (popup == null) { return; }
         popup.stop();
         popupContainer.remove(popup);
         popupContainer.remove(popupBgComp);
         popupLayeredHandler.deactivate();
         popup = null;
         SceneManager.getInstance().popupContainer.repaint();
     }

     /**
      * close the popup.
      */
     public void requestPopupClose() {
         removePopup();
     }

     /**
      * called when pressed OK key by Popup.
      *
      * @param para the parameter
      */
     public void requestPopupOk(Object para) {
         if (popup instanceof TitleSearchPopup) {
             removePopup();
             Object[] values = {getCurrentSceneId(), para, null};
             if (Rs.vfavoriteTheatre.size() > 0) {
                 SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_SHOWTIMES, true, values);
             } else {
                 SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_ASSET, true, values);
             }
         } else if (popup instanceof TheatreSelectPopup) {
             Object[] values = {getCurrentSceneId(), para, null};
             SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_THEATRE_ASSET, true, values);
         }
     }

    /**
     * Load data by using Thread.
     *
     * @param obj datas
     * @param isLoadingBar the is loading bar. if true, loading bar will be showed.
     */
    protected void loadData(Object[] obj, boolean isLoadingBar) {
        if (isLoadingBar) { MainManager.getInstance().showLoadingAnimation(); }
        if (dataLoadingThread != null) { dataLoadingThread.setObj(obj); }
    }

    /**
     * Is the initScene.
     */
    protected abstract void initScene();

    /**
     * Is the disposeScene.
     */
    protected abstract void disposeScene();

    /**
     * IS the startScene.
     * @param reset whether resources is reset or not
     */
    protected abstract void startScene(boolean reset);

    /**
     * Is the stopScene.
     */
    protected abstract void stopScene();

    /**
     * IS the keyAction.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected abstract boolean keyAction(int keyCode);

    /**
     * Notify from data loading thread.
     *
     * @param obj datas
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        DataManager dataManager = DataManager.getInstance();
        String procId = (String) obj[0];
        if (procId.equals(ID_REGION_DATA)) {
            if (isFromThread) {
                for (int i = 0; i < RegionInfo.RESIONID.length; i++) {
                    dataManager.getRegionInfo(String.valueOf(RegionInfo.RESIONID[i]));
                    if (!checkVisible()) { return; }
                }
            }
            if (!checkVisible()) { return; }

            MainManager.getInstance().hideLoadingAnimation();
            if (DataManager.region.isEmpty()) {
                if (!checkErrorMessageShow()) {
                    dataManager.errorType = DataManager.ERR_XML_PARSING;
                    dataManager.printError(null);
                }
                showErrorMessage(ERRMSG_NONE);
            } else {
                theatreSelectPopup.setMode(TheatreSelectPopup.MODE_SELECT);
                theatreSelectPopup.resetFocus();
                addPopup(theatreSelectPopup);
            }
        }
    }

    /**
     * Show cinema popup.
     *
     * @param filmId the film ID
     * @param langType the lang type
     */
    protected void showCinemaPopup(String filmId, String langType) {
        DataManager dataManager = DataManager.getInstance();
        if (DataManager.region.isEmpty()) {
            for (int i = 0; i < RegionInfo.RESIONID.length; i++) {
                dataManager.getRegionInfo(String.valueOf(RegionInfo.RESIONID[i]));
                if (!checkVisible()) { return; }
            }
        }
        if (!checkVisible()) { return; }

        if (DataManager.region.isEmpty()) {
            showErrorMessage();
        } else {
            dataManager.getMovieTimesOfTheatreData(filmId);
            if (!checkVisible()) { return; }
            Hashtable filmCinema = DataManager.movieTimesOfTheatreData;
            if (filmCinema.isEmpty()) {
                showErrorMessage();
            } else {
                Vector allRegion = DataManager.vRegion;
                Vector newAllRegion = null;
                for (int i = 0; i < allRegion.size(); i++) {
                    RegionInfo regionInfo = (RegionInfo) allRegion.elementAt(i);
                    Vector theatreList = regionInfo.getTheatreList();
                    if (theatreList == null || theatreList.size() == 0) { continue; }
                    RegionInfo newRegionInfo = null;
                    for (int j = 0; j < theatreList.size(); j++) {
                        TheatreData cinema = (TheatreData) theatreList.elementAt(j);
                        if (filmCinema.containsKey(langType + cinema.getTheatreID())) {
                            if (newRegionInfo == null) {
                                newRegionInfo = new RegionInfo();
                                newRegionInfo.setRegionId(regionInfo.getRegionId());
                                newRegionInfo.setRegionName(regionInfo.getRegionName());
                            }
                            newRegionInfo.addTheatreData(cinema);
                        }
                    }
                    if (newRegionInfo != null) {
                        if (newAllRegion == null) { newAllRegion = new Vector(); }
                        newAllRegion.addElement(newRegionInfo);
                    }
                }
                if (!checkVisible()) { return; }
                MainManager.getInstance().hideLoadingAnimation();
                theatreSelectPopup.setMode(TheatreSelectPopup.MODE_SELECT_FOCUSED);
                theatreSelectPopup.setData(newAllRegion, filmId, langType);
                theatreSelectPopup.resetFocus();
                addPopup(theatreSelectPopup);
            }
        }
    }

    /**
     * Notify from ErrorMessage after action is performed.
     * perform next state by errMsgAction type.
     */
    public void notifyErrorMessage() {
        if (!checkVisible()) { return; }
        if (errMsgAction == ERRMSG_PREV) {
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
        } else if (errMsgAction == ERRMSG_EXIT) { MainManager.getInstance().exitToChannel(); }
    }

    /**
     * Show error message.
     *
     * @param errAction the error action
     */
    protected void showErrorMessage(int errAction) {
        if (!checkVisible()) { return; }
        errMsgAction = errAction;
        MainManager.getInstance().showErrorMessage(DataManager.getInstance().getErrorCode());
    }

    /**
     * Check error message.
     *
     * @return true, whether ErrorMessage will be showed or not.
     */
    protected boolean checkErrorMessageShow() {
        if (DataManager.getInstance().errorType != DataManager.NO_ERR) { return true; }
        return false;
    }

    /**
     * Show error message.
     */
    private void showErrorMessage() {
        MainManager.getInstance().hideLoadingAnimation();
        DataManager dataManager = DataManager.getInstance();
        if (!checkErrorMessageShow()) {
            dataManager.errorType = DataManager.ERR_XML_PARSING;
            dataManager.printError(null);
        }
        showErrorMessage(ERRMSG_NONE);
    }

    /**
     * Check visibility.
     *
     * @return true, if successful
     */
    protected boolean checkVisible() {
        boolean isVisible = isVisible() && SceneManager.getInstance().curSceneId != SceneManager.SCENE_ID_INVALID;
        if (!isVisible) {
            DataManager.getInstance().reset();
            stop();
            dispose();
        }
        return isVisible;
    }

    /**
     * Gets the init and the last number.
     *
     * @param focus the focus
     * @param totalCount the total count
     * @param itemsPerPage the items per page
     * @return the init and the last number
     */
    protected int[] getInitLastNumber(int focus, int totalCount, int itemsPerPage) {
        int halfPos = itemsPerPage / 2;
        int[] number = new int[] {0, totalCount - 1};
        if (totalCount > itemsPerPage) {
            if (focus < halfPos) {
                number[0] = totalCount - (halfPos - focus);
            } else { number[0] = focus - halfPos; }
            if (totalCount - focus <= halfPos) {
                number[1] = focus + halfPos - totalCount;
            } else { number[1] = focus + halfPos; }
        }
        return number;
    }

    /**
     * Sets the poster image.
     *
     * @param infoData FilmData for the new poster image
     */
    protected synchronized void setPosterImage(FilmInfoData infoData, int reqIndex) {
        if (infoData == null) { return; }
        if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
            FrameworkMain.getInstance().getImagePool().clear();
            return;
        } else if (!isVisible()) {
            Logger.debug(this, "setPosterImage-this scene is not visible.");
            return;
        } else if (!checkFocus(reqIndex)) {
            Logger.debug(this, "setPosterImage-focus is invalid. so return.");
            return;
        }
        String posterUrl = infoData.getPosterURL();
        if (posterUrl == null || posterUrl.length() == 0) {
            //DataManager.getInstance().printError(DataManager.ERR_POSTER_IMG, infoData.getId()
            //        + ":poster url is empty.");
            Logger.debug(this, infoData.getId() + " : poster url is empty.");
            return;
        }
        Image posterImg = DataCenter.getInstance().getImage(posterUrl);
        if (posterImg == null) {
            try {
                byte[] imgByte = HttpConnection.getInstance().getBytes(posterUrl);
                if (imgByte != null) {
                    //posterImg = FrameworkMain.getInstance().getImagePool().createImage(imgByte, posterUrl);
                    posterImg = ContentImagePool.getInstance().createImage(imgByte, posterUrl);
                    if (posterImg != null) {
                        int imgWidth = posterImg.getWidth(null);
                        int imgHeight = posterImg.getHeight(null);
                        Logger.debug(this, "setPosterImage()-imgWidth : " + imgWidth);
                        Logger.debug(this, "setPosterImage()-imgHeight : " + imgHeight);
                    }
                } else {
                    //DataManager.getInstance().printError(DataManager.ERR_POSTER_IMG, infoData.getId());
                    Logger.debug(this, infoData.getId() + " : poster data is null.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        infoData.setPosterImage(posterImg);
    }

    /**
     * Check focus.
     *
     * @param reqIndex the request index
     * @return true, if successful
     */
    protected boolean checkFocus(int reqIndex) {
        return true;
    }

    /**
     * The Class PopupBgComp is for background when the popup is showed.
     */
    class PopupBgComp extends Component {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /**
         * paint.
         *
         * @param        g           Graphics 객체.
         */
        public void paint(Graphics g) {
            g.setColor(Rs.DVB80C12);
            g.fillRect(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
        }
    }

    /**
     * The Class ScrollShadowComp is for shadow when scroll component is showed.
     */
    protected class ScrollShadowComp extends Component {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /** The shadow img. */
        private Image shadowImg = null;

        /**
         * Instantiates a new scroll shadow comp.
         */
        public ScrollShadowComp() {
            setBounds(277, 466, 683, 17);
        }

        /**
         * Sets the shadow img.
         *
         * @param shaImg the new shadow img
         */
        public void setShadowImg(Image shaImg) {
            shadowImg = shaImg;
        }

        /**
         * paint.
         *
         * @param        g           Graphics 객체.
         */
        public void paint(Graphics g) {
            Logger.info(this, "called paint()");
            g.drawImage(shadowImg, 0, 0, this);
        }
    }

    /**
     * <code>DataLoadingThread</code> This class process datas loaded in queue.
     *
     * @since   2011.02.25
     * @version 1.0, 2010.02.25
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class DataLoadingThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = null;

        /**
         * This is constructor to define variables.
         */
        public DataLoadingThread() {
            queue = new ArrayList();
        }

        /**
         * set Object for processing into queue.
         * @param obj   Object for processing.
         */
        public void setObj(Object[] obj) {
            Logger.debug(this, "called setObj()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "BasicUI-DataLoadingThread");
                thread.start();
            }

            //MainManager.getInstance().showLoadingAnimation();
            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    queue.notify();
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                Object[] obj = null;
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                        if (!queue.isEmpty()) { obj = (Object[]) queue.remove(0); }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (obj != null) {
                    String procId = (String) obj[0];
                    Logger.debug(this, "run() - procId : " + procId);
                    Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                    notifyFromDataLoadingThread(obj, true);
                }
            }
            queue.clear();
        }
    }
}

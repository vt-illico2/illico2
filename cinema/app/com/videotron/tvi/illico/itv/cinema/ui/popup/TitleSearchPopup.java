/*
 *  @(#)TitleSearchPopup.java 1.0 2011.03.07
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Vector;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.keyboard.AlphanumericKeyboard;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SelectPopup</code> Search Popup by title.
 *
 * @since   2011.03.07
 * @version $Revision: 1.10 $ $Date: 2012/02/15 04:09:59 $
 * @author  tklee
 */
public class TitleSearchPopup extends Popup implements KeyboardListener {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2941875915202666451L;
    /** The popup loading point. */
    private final Point popupLoadingPoint = new Point(746, 264);

    /** The Constant STATE_KEYBOARD. */
    private static final int STATE_KEYBOARD = 0;
    /** The Constant STATE_LIST. */
    private static final int STATE_LIST = 1;

    /** The Constant ROWS_PER_PAGE. */
    private static final int ROWS_PER_PAGE = 13;
    /** The Constant SEARCH_WAIT_TIME. */
    public static final long SEARCH_WAIT_COUNT = 2;
    /** The middle position. */
    private static final int LIST_MIDDLE_POS = 6;

    /** The btn id for keyboard. */
    private final int[] btnIdForKeyboard = {ButtonIcons.BUTTON_ERASE, ButtonIcons.BUTTON_CLOSE};
    /** The btn id for list. */
    private final int[] btnIdForList = {ButtonIcons.BUTTON_CLOSE};

    /** The bg shadow bottom img. */
    private Image bgShadowBottomImg = null;
    /** The bg shadowleft img. */
    private Image bgShadowleftImg = null;
    /** The bg shadow right img. */
    private Image bgShadowRightImg = null;
    /** The bg left img. */
    private Image bgLeftImg = null;
    /** The bg right img. */
    private Image bgRightImg = null;
    /** The title img. */
    private Image titleImg = null;
    /** The bg high img. */
    private Image bgHighImg = null;
    /** The exit img. */
    private Image exitImg = null;
    /** The list bg gap img. */
    private Image listBgGapImg = null;
    /** The list line img. */
    private Image listLineImg = null;
    /** The list bg high img. */
    private Image listBgHighImg = null;
    /** The list shadow top img. */
    private Image listShadowTopImg = null;
    /** The list shadow bottom img. */
    private Image listShadowBottomImg = null;
    /** The arr top img. */
    private Image arrTopImg = null;
    /** The arr bottom img. */
    private Image arrBottomImg = null;
    /** The list focus img. */
    private Image listFocusImg = null;
    /** The txt box img. */
    private Image txtBoxImg = null;
    /** The cursor img. */
    private Image cursorImg = null;

    /** The title txt. */
    private String titleTxt = null;
    /** The close txt. */
    private String closeTxt = null;
    /** The tip txt. */
    private String tipTxt = null;
    /** The tip msg txt. */
    private String tipMsgTxt = null;
    /** The no search txt. */
    private String noSearchTxt = null;
    /** The list title txt. */
    private String listTitleTxt = null;
    /** The empty txt. */
    private String emptyTxt = null;

    /** The data loading thread. */
    private DataLoadingThread dataLoadingThread = null;
    /** The AlphanumericKeyboard object. */
    private AlphanumericKeyboard keyboard = null;
    /** The search data as result. */
    private Vector searchData = null;
    /** The Footer instance. */
    private Footer footer = null;

    /** The state focus. */
    private int stateFocus = 0;
    /** The current letters. */
    private String currentLetters = "";
    /** The search letters. */
    private String searchLetters = "";
    /** The focus index. */
    private int focusIndex = 0;
    /** The focus position. */
    private int focusPosition = 0;
    /** The search id. */
    private int searchId = -1;
    /** The search wait count. */
    private int searchWaitCount = 0;

    /**
     * Instantiates a new select popup.
     */
    public TitleSearchPopup() {
        setBounds(220, 37, 740, 503);
        clickEffect = new ClickingEffect(this, 5);
        keyboard = new AlphanumericKeyboard();
        try {
            keyboard.setMode(KeyboardOption.MODE_EMBEDDED);
            //keyboard.setText("");
            keyboard.setMaxChars(20);
            //keyboard.setTempViewText(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        keyboard.setLocation(343 - 220, 6);
        add(keyboard);
        dataLoadingThread = new DataLoadingThread();
        footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
        footer.setBounds(0, 489 - 37, 902 - 220, 24);
        //setBounds(10, 488, 900, 24);
        add(footer);
    }

    /**
     * Instantiates.
     */
    public void init() {
        bgShadowBottomImg = dataCenter.getImage("03_bg_sha.png");
        bgShadowleftImg = dataCenter.getImage("03_bg_l.png");
        bgShadowRightImg = dataCenter.getImage("03_shadow_r.png");
        bgLeftImg = dataCenter.getImage("03_bg_01.png");
        bgRightImg = dataCenter.getImage("03_bg_02.png");
        titleImg = dataCenter.getImage("03_title.png");
        bgHighImg = dataCenter.getImage("03_bg01_high.png");
        exitImg = dataCenter.getImage("b_btn_exit.png");
        txtBoxImg = dataCenter.getImage("04_key_search.png");
        cursorImg = dataCenter.getImage("04_key_cursor.png");
        listBgGapImg = dataCenter.getImage("03_bg01_gap.png");
        listLineImg = dataCenter.getImage("11_line.png");
        listBgHighImg = dataCenter.getImage("03_bg01_high.png");
        listShadowTopImg = dataCenter.getImage("03_result_sha_top.png");
        listShadowBottomImg = dataCenter.getImage("03_result_sha_d.png");
        arrTopImg = dataCenter.getImage("02_ars_t.png");
        arrBottomImg = dataCenter.getImage("02_ars_b.png");
        listFocusImg = dataCenter.getImage("03_focus02.png");

        titleTxt = dataCenter.getString("cine.searchTitle");
        closeTxt = dataCenter.getString("buttonNames.close");
        tipTxt = dataCenter.getString("cine.Tip");
        tipMsgTxt = dataCenter.getString("cine.searchTip");
        noSearchTxt = dataCenter.getString("cine.noSearch");
        listTitleTxt = dataCenter.getString("cine.title");
        emptyTxt = dataCenter.getString("cine.searchEmpty");
    }

    /**
     * when popup is started, this is called.
     */
    public void start() {
        keyboard.registerKeyboardListener(this);
        keyboard.setText("");
        keyboard.setTempViewText(true);
        keyboard.setFocus(AlphanumericKeyboard.INDEX_INITIAL);
        //Keyboard.setLanguage("English");
        //keyboard.setAlphanumericType(KeyboardOption.ALPHANUMERIC_ALL);
        keyboard.prepare();
        keyboard.start();

        stateFocus = 0;
        currentLetters = "";
        searchLetters = "";
        focusIndex = 0;
        focusPosition = 0;
        searchId = -1;
        searchData = null;
        searchWaitCount = 0;
        setFooterBtn();
    }

    /**
     * set the footer button.
     */
    private void setFooterBtn() {
        footer.reset();
        int[] btnId = btnIdForList;
        if (stateFocus == STATE_KEYBOARD) { btnId = btnIdForKeyboard; }
        for (int i = 0; btnId != null && i < btnId.length; i++) {
            footer.addButton(BasicUI.btnImage[btnId[i]], BasicUI.btnName[btnId[i]]);
        }
    }

    /**
     * when popup is finished, this is called.
     */
    public void stop() {
        MainManager.getInstance().hideLoadingAnimation();
        keyboard.removeKeyboardListener(this);
        try {
            keyboard.stop();
        } catch (Throwable t) {
            Logger.debug(this, "[Error]keyboard stop : " + t.getMessage());
        }

        dataLoadingThread.stop();
        searchData = null;
        searchId = -1;
    }

    /**
     * Check string size by right.
     *
     * @param fm the FontMetrics
     * @param str the str for check
     * @param checkWidth the check width
     * @return the string
     */
    private String checkStringSizeByRight(FontMetrics fm, String str, int checkWidth) {
        if (fm.stringWidth(str) > checkWidth) {
            for (int i = str.length() - 1; i >= 0; i--) {
                if (fm.stringWidth(str.substring(i)) > checkWidth) {
                    return str.substring(i + 1);
                }
            }
        }
        return str;
    }

    /**
     * paint.
     *
     * @param        g           Graphics 객체.
     */
     public void paint(Graphics g) {
         g.translate(-220, -37);
         g.drawImage(bgLeftImg, 343, 77, this);
         g.drawImage(bgRightImg, 592, 77, 368, 402, this);
         g.drawImage(titleImg, 343, 38, this);
         g.drawImage(bgHighImg, 602, 77, 296, 470 - 77, this);
         g.drawImage(bgShadowBottomImg, 343, 479, 617, 61, this);
         g.drawImage(bgShadowleftImg, 220, 168, this);
         g.drawImage(bgShadowRightImg, 820, 0, 140, 540, this);
         //g.drawImage(exitImg, 826, 489, this);
         g.drawImage(txtBoxImg, 352, 76, this);

         g.setFont(Rs.F20);
         g.setColor(Rs.C046046045);
         g.drawString(titleTxt, 379, 65);
         g.setColor(Rs.C202174097);
         g.drawString(titleTxt, 378, 64);
         g.setFont(Rs.F18);
         g.setColor(Rs.C255255255);
         //g.drawString(closeTxt, 860, 503);

         if (currentLetters.length() == 0) {
             g.setFont(Rs.F17);
             g.setColor(Rs.DVB40C255);
             g.drawString(emptyTxt, 366, 100);
             g.drawImage(cursorImg, 366, 104, this);
         } else {
             g.setFont(Rs.F20);
             int checkWidth = 570 - 366;
             FontMetrics fm = g.getFontMetrics();
             int stingWidth = fm.stringWidth(currentLetters);
             if (stingWidth > checkWidth) {
                 String str = checkStringSizeByRight(fm, currentLetters, checkWidth);
                 stingWidth = fm.stringWidth(str);
                 g.drawString(str, 570 - stingWidth, 100);
                 g.drawImage(cursorImg, 570, 104, this);
             } else {
                 g.drawString(currentLetters, 366, 100);
                 g.drawImage(cursorImg, 366 + stingWidth, 104, this);
             }
         }

         paintList(g);
         g.translate(220, 37);
         super.paint(g);
     }

     /**
      * Paint list.
      *
      * @param g the Graphics
      */
     private void paintList(Graphics g) {
         if (MainManager.getInstance().isLoadingAnimation()) { return; }
         if (searchData == null) {
             g.setFont(Rs.F24);
             g.setColor(Rs.C255255255);
             g.drawString(tipTxt, 635, 194);

             g.setFont(Rs.F18);
             g.setColor(Rs.C184184184);
             /*String str = TextUtil.replace(tipMsgTxt, "^", "\n");
             String[] arrStr = TextUtil.split(str, g.getFontMetrics(), 864 - 634);
             for (int i = 0; i < arrStr.length; i++) {
                 int addPos = i * 18;
                 if (i > 1) { addPos += 18; }
                 g.drawString(arrStr[i], 634, 220 + addPos);
             }*/
             String[] tokens = TextUtil.tokenize(tipMsgTxt, "^");
             int addPos = -18;
             for (int i = 0; i < tokens.length; i++) {
                 String[] arrStr = TextUtil.split(tokens[i], g.getFontMetrics(), 864 - 634);
                 for (int j = 0; j < arrStr.length; j++) {
                     addPos += 18;
                     g.drawString(arrStr[j], 634, 220 + addPos);
                 }
                 addPos += 18;
             }
         } else  if (searchData != null && !searchData.isEmpty()) {
             g.setColor(Rs.C055055055);
             g.fillRect(602, 77, 296, 392);
             g.setColor(Rs.DVB102102102179);
             g.fillRect(602, 77, 296, 27);
             g.drawImage(listBgGapImg, 603, 100, this);

             int lineGap = 27;
             int addPos = 0;
             for (int i = 0; i < ROWS_PER_PAGE - 1; i++) {
                 addPos = i * lineGap;
                 g.drawImage(listLineImg, 612, 132 + addPos, this);
             }
             g.drawImage(listBgHighImg, 602, 77, this);

             String str = listTitleTxt + " (" + searchData.size() + ")";
             g.setFont(Rs.F17);
             g.setColor(Rs.C230230230);
             g.drawString(str, 613, 95);

             int totalCount = searchData.size();
             g.setFont(Rs.F17);
             int initNum = focusIndex - focusPosition;
             int lastNum = 0;
             if (totalCount < ROWS_PER_PAGE) { lastNum = totalCount - 1; }
             lastNum = focusIndex + ROWS_PER_PAGE - 1 - focusPosition;
             if (lastNum >= totalCount) { lastNum = totalCount - 1; }
             for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                 addPos = (j * lineGap);
                 if (i != focusIndex || stateFocus != STATE_LIST) {
                     Object item = searchData.elementAt(i);
                     paintItem(g, false, item, addPos);
                 }
             }
             if (stateFocus == STATE_LIST) {
                 paintItem(g, true, searchData.elementAt(focusIndex), (focusPosition * lineGap));
             }
             if (initNum > 0) {
                 g.drawImage(listShadowTopImg, 602, 104, this);
                 g.drawImage(arrTopImg, 739, 96, this);
             }
             if (lastNum < totalCount - 1) {
                 g.drawImage(listShadowBottomImg, 602, 439, this);
                 g.drawImage(arrBottomImg, 739, 460, this);
             }
         } else {
             g.setFont(Rs.F18);
             g.setColor(Rs.C184184184);
             String[] arrStr = TextUtil.split(noSearchTxt, g.getFontMetrics(), 864 - 634);
             for (int i = 0; i < arrStr.length; i++) {
                 int addPos = i * 18;
                 g.drawString(arrStr[i], 634, 220 + addPos);
             }
         }
     }

     /**
      * Paint item.
      *
      * @param g the Graphics
      * @param isFocus the is focus
      * @param item the item
      * @param addPos the add position
      */
     private void paintItem(Graphics g, boolean isFocus, Object item, int addPos) {
         FilmData data = (FilmData) item;
         String name = TextUtil.shorten(data.getTitle(), g.getFontMetrics(), 884 - 614);
         if (isFocus) {
             g.setColor(Rs.C000000000);
             g.drawImage(listFocusImg, 602, 104 + addPos, this);
             g.drawString(name, 614, 123 + addPos);
         } else {
             g.setColor(Rs.C254196014);
             String letters = searchLetters.trim();
             String str = name.substring(0, letters.length());
             int posX = 614;
             g.drawString(str, posX, 123 + addPos);
             if (name.length() != letters.length()) {
                 g.setColor(Rs.C255255255);
                 posX = 614 + g.getFontMetrics().stringWidth(str);
                 str = name.substring(letters.length());
                 g.drawString(str, posX, 123 + addPos);
             }
         }
     }

     /**
      * It does a key action.
      *
      * @param keyCode the key code
      * @return true, if successful
      */
     public boolean keyAction(int keyCode) {
         if (keyCode == Rs.KEY_EXIT || keyCode == KeyCodes.LAST || keyCode == KeyCodes.SEARCH) {
             searchId = -1;
             if (keyCode == Rs.KEY_EXIT) {
                 //clickEffect.start(826 - 220, 491 - 37, 582 - 553, 454 - 438);
                 footer.clickAnimation(BasicUI.btnName[ButtonIcons.BUTTON_CLOSE]);
             }
             scene.requestPopupClose();
             return true;
         }
         if (stateFocus == STATE_KEYBOARD) {
             if (keyCode == KeyCodes.COLOR_C) {
                 footer.clickAnimation(BasicUI.btnName[ButtonIcons.BUTTON_ERASE]);
             }
             if (keyboard.handleKey(keyCode)) {
                 searchWaitCount = 0;
                 repaint();
                 return true;
             } else { return false; }
         } else {
             switch (keyCode) {
             case Rs.KEY_LEFT:
                 stateFocus = STATE_KEYBOARD;
                 //keyboard.focusInit();
                 keyboard.setFocus(AlphanumericKeyboard.INDEX_INITIAL);
                 setFooterBtn();
                 repaint();
                 break;
             case Rs.KEY_UP:
                 if (focusIndex > 0) {
                     if (focusPosition != LIST_MIDDLE_POS || focusIndex == focusPosition) { focusPosition--; }
                     focusIndex--;
                     repaint();
                 }
                 break;
             case Rs.KEY_DOWN:
                 int totalCount = searchData.size();
                 if (focusIndex < totalCount - 1) {
                     if (focusPosition != LIST_MIDDLE_POS || focusPosition + focusIndex == totalCount - 1) {
                         focusPosition++;
                     }
                     focusIndex++;
                     repaint();
                 }
                 break;
             case Rs.KEY_OK:
                 searchId++;
                 clickEffect.start(602 - 220, 104 + (focusPosition * 27) - 37, 898 - 602, 217 - 188);
                 FilmData item = (FilmData) searchData.elementAt(focusIndex);
                 scene.requestPopupOk(item);
                 break;
             default:
                 return false;
             }
             searchWaitCount = 0;
             return true;
         }
     }

    /**
     * Implements the KeyboardListener method.
     *
     * @param position the position
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#cursorMoved(int)
     */
    public void cursorMoved(int position) throws RemoteException {
        Logger.debug(this, "called cursorMoved()-position : " + position);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param direction the direction
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#focusOut(int)
     */
    public void focusOut(int direction) throws RemoteException {
        Logger.debug(this, "called focusOut()-direction : " + direction);
        searchWaitCount = 0;
        switch (direction) {
        case Rs.KEY_UP:
        case Rs.KEY_DOWN:
        case Rs.KEY_LEFT:
            keyboard.setPreviousFocus();
            repaint();
            break;
        case Rs.KEY_RIGHT:
            if (searchData != null && !searchData.isEmpty()) {
                searchId++;
                stateFocus = STATE_LIST;
                setFooterBtn();
            } else { keyboard.setPreviousFocus(); }
            repaint();
            break;
        default:
            break;
        }
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputCanceled(java.lang.String)
     */
    public void inputCanceled(String text) throws RemoteException {
        Logger.debug(this, "called inputCanceled()-direction : " + text);
        searchId = -1;
        handleKey(Rs.KEY_EXIT);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputEnded(java.lang.String)
     */
    public void inputEnded(String text) throws RemoteException {
        Logger.debug(this, "called inputEnded()-text : " + text);
        Logger.debug(this, "inputEnded()-searchData : " + searchData);
        if (searchData != null && !searchData.isEmpty()) {
            Logger.debug(this, "inputEnded()-searchData is ready");
            searchId++;
            keyboard.setFocus(AlphanumericKeyboard.INDEX_FOCUS_LOST);
            stateFocus = STATE_LIST;
            repaint();
        } else { keyboard.setPreviousFocus(); }
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#inputCleared()
     */
    public void inputCleared() throws RemoteException {
        Logger.debug(this, "called inputCleared()");
        
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param mode the mode
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#modeChanged(int)
     */
    public void modeChanged(int mode) throws RemoteException {
        Logger.debug(this, "called modeChanged()-mode : " + mode);
    }

    /**
     * Implements the KeyboardListener method.
     *
     * @param text the text
     * @throws RemoteException the remote exception
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardListener#textChanged(java.lang.String)
     */
    public void textChanged(String text) throws RemoteException {
        Logger.debug(this, "called textChanged()-text : " + text);
        searchId++;
        if (text != null && text.length() > 0) {
            currentLetters = text.substring(0, 1).toUpperCase();
            if (text.length() > 1) {
                currentLetters += text.substring(1);
                String letters = currentLetters.trim();
                if (letters.length() > 1 && !searchLetters.equalsIgnoreCase(letters)) {
                    dataLoadingThread.setObj(new String[]{String.valueOf(searchId), letters});
                }
            }
        } else {
            currentLetters = "";
            searchLetters = "";
            searchData = null;
        }
        repaint();
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the obj
     */
    private void notifyFromDataLoadingThread(Object obj) {
        String[] reqData = (String[]) obj;
        int myId = Integer.parseInt(reqData[0]);
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (myId != searchId) { return; }
            if (++searchWaitCount >= SEARCH_WAIT_COUNT) { break; }
        }

        if (myId == searchId) {
            MainManager.getInstance().showLoadingAnimation(popupLoadingPoint);
            //MainManager.getInstance().showNotDelayLoadingAnimation(popupLoadingPoint);
            Vector data = DataManager.getInstance().getFilmsData(reqData[1], false);
            MainManager.getInstance().hideLoadingAnimation();
            if (myId == searchId) {
                searchLetters = reqData[1];
                searchData = data;
                focusIndex = 0;
                focusPosition = 0;
                repaint();
            }
        }
    }

    /**
     * <code>DataLoadingThread</code> This class process datas loaded in queue.
     *
     * @since   2011.02.25
     * @version 1.0, 2010.02.25
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class DataLoadingThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = null;

        /**
         * This is constructor to define variables.
         */
        public DataLoadingThread() {
            queue = new ArrayList();
        }

        /**
         * set Object for processing into queue.
         * @param obj   Object for processing.
         */
        public void setObj(Object obj) {
            Logger.debug(this, "called setObj()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "TitleSearch-DataLoadingThread");
                thread.start();
            }

            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    queue.notify();
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                //if (queue.isEmpty()) {
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //}
                if (thread == null) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object obj = queue.remove(0);
                    Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                    if (queue.isEmpty()) { notifyFromDataLoadingThread(obj); }
                }
            }
            queue.clear();
        }
    }
}

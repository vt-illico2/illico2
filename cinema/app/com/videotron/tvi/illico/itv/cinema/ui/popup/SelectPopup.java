/*
 *  @(#)SelectPopup.java 1.0 2011.03.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.gui.BasicRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>SelectPopup</code> Select Popup to be scaled up.
 *
 * @since   2011.03.04
 * @version $Revision: 1.6 $ $Date: 2011/07/05 18:26:19 $
 * @author  tklee
 */
public class SelectPopup extends Popup {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2941875915202666451L;

    /** The bg bottom img. */
    private Image bgBottomImg = null;
    /** The bg top img. */
    private Image bgTopImg = null;
    /** The bg mid img. */
    private Image bgMidImg = null;
    /** The shadow top img. */
    private Image shadowTopImg = null;
    /** The shadow bottom img. */
    private Image shadowBottomImg = null;
    /** The arrow up img. */
    private Image arrowUpImg = null;
    /** The arrow down img. */
    private Image arrowDownImg = null;
    /** The focus img. */
    private Image focusImg = null;

    /** The values. */
    private String[] values = null;
    /** The line. */
    private int line = 0;
    /** The half line. */
    private int halfLine = 1;
    /** The start idx. */
    private int startIdx = 0;
    /** The focus y. */
    private int focusY = 0;
    /** The title. */
    private String title = null;
    /** The focus. */
    private int focus = 0;
    /** The BasicRenderer. */
    private BasicRenderer renderer = null;
    /** The traslate postion. */
    private Point traslatePostion = null;

    /**
     * Instantiates a new select popup.
     */
    public SelectPopup() {
        setBounds(0, 0, 960, 540);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Instantiates.
     */
    public void init() {
        bgBottomImg = dataCenter.getImage("08_op_bg_b.png");
        bgTopImg = dataCenter.getImage("08_op_bg_t.png");
        bgMidImg = dataCenter.getImage("08_op_bg_m.png");
        shadowTopImg = dataCenter.getImage("08_op_sh_t.png");
        shadowBottomImg = dataCenter.getImage("08_op_sh_b.png");
        arrowUpImg = dataCenter.getImage("02_ars_t.png");
        arrowDownImg = dataCenter.getImage("02_ars_b.png");
        focusImg = dataCenter.getImage("08_op_foc.png");
    }

    /**
     * Sets the data.
     *
     * @param bRenderer the BasicRenderer
     * @param data the data
     * @param focusIndex the focus index
     * @param titleTxt the title txt
     * @param lineCount the line count
     * @param translatePos the translate position
     */
    public void setData(BasicRenderer bRenderer, String[] data, int focusIndex, String titleTxt, int lineCount,
            Point translatePos) {
        renderer = bRenderer;
        title = titleTxt;
        line = lineCount;
        values = data;
        focus = focusIndex;
        traslatePostion = translatePos;
        calulateIndex();
        focusY = 42 + 16 + (halfLine - 2) * 27;
    }

    /**
     * Calulate index.
     */
    private void calulateIndex() {
        halfLine = line / 2;
        startIdx = (values.length - halfLine + focus) % values.length;
        if (line % 2 == 1)
            halfLine += 1;
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    public boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (--focus < 0) { focus = values.length - 1; }
            calulateIndex();
            repaint();
            break;
        case Rs.KEY_DOWN:
            if (++focus == values.length) { focus = 0; }
            calulateIndex();
            repaint();
            break;
        case Rs.KEY_OK:
            clickEffect.start(traslatePostion.x + 4, traslatePostion.y + focusY, 230 - 4, 34);
            scene.requestPopupOk(new Integer(focus));
            break;
        case Rs.KEY_EXIT:
            scene.requestPopupClose();
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * paint.
     *
     * @param        g           Graphics 객체.
     */
     public void paint(Graphics g) {
         if (renderer != null) { renderer.paintSelectedItem(g, this); }
         if (traslatePostion != null) { g.translate(traslatePostion.x, traslatePostion.y); }

         g.drawImage(bgTopImg, 0, 42, this);
         int step = 61;
         for (int i = 1; i < line - 1; i++) {
             g.drawImage(bgMidImg, 0, step, this);
             step += 27;
         }
         g.drawImage(bgBottomImg, 0, step, this);

         Rectangle before = g.getClipBounds();
         g.clipRect(5, 44, 232, (line - 1) * 27 + 6);

         g.setFont(Rs.F18);
         g.setColor(Rs.C133133133);
         int dataPosY = 52;
         int idx = startIdx;
         for (int i = 0; i < line; i++) {
             g.drawString(values[idx], 19, dataPosY);
             dataPosY += 27;
             if (++idx == values.length) { idx = 0; }
         }
         g.drawImage(shadowTopImg, 4, 44, this);
         g.drawImage(shadowBottomImg, 4, step - 11, this);

         g.setColor(Rs.C1);
         g.drawImage(focusImg, 0, focusY, this);
         g.drawString(values[focus], 19, focusY + 21);
         g.setClip(before);

         // title
         g.setColor(Rs.C78);
         g.fillRect(2, 15, 230, 29);

         if (title != null) {
             title = TextUtil.shorten(title, g.getFontMetrics(), 200);
             g.setColor(Rs.C027024012);
             g.drawString(title, 16, 36);
             g.setColor(Rs.C214182055);
             g.drawString(title, 15, 35);
         }

         g.drawImage(arrowUpImg, 106, 0, this);
         g.drawImage(arrowDownImg, 106, step + 15, this);
         if (traslatePostion != null) { g.translate(-traslatePostion.x, -traslatePostion.y); }
     }
}

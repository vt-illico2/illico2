package com.videotron.tvi.illico.itv.cinema.ui.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class draw a Icon of HOTKey .
 * @version $Revision: 1.6 $ $Date: 2012/02/15 04:09:58 $
 * @author pellos
 */
public class ButtonIcons extends Component {
    /**
     *  Is the serialVersionID.
     */
    private static final long serialVersionUID = -202523543953500856L;

    /** Definds a button icon. */
    public static final int BUTTON_BACK = 0;
    /** The Constant BUTTON_SEARCH. */
    public static final int BUTTON_SEARCH = 1;
    /** The Constant BUTTON_OPTION. */
    public static final int BUTTON_OPTION = 2;
    /** The Constant BUTTON_SCROLL. */
    public static final int BUTTON_SCROLL = 3;
    /** The Constant BUTTON_PAGE_CHAANGE. */
    public static final int BUTTON_PAGE_CHAANGE = 4;
    /** The Constant BUTTON_EDIT_ORDER. */
    public static final int BUTTON_EDIT_ORDER = 5;
    /** The Constant BUTTON_CLEAR_FAVOR. */
    public static final int BUTTON_CLEAR_FAVOR = 6;
    /** The Constant BUTTON_PAGE_UPDWON. */
    public static final int BUTTON_PAGE_UPDWON = 7;
    /** The Constant BUTTON_ERASE. */
    public static final int BUTTON_ERASE = 8;
    /** The Constant BUTTON_CLOSE. */
    public static final int BUTTON_CLOSE = 9;
    /** The Constant BUTTON_EXIT. */
    public static final int BUTTON_EXIT = 10;

    /** Is a gap between button. */
    private final int buttonGap = 6;

    /** Is a button name. */
    public final String[] buttonNames = {"cine.LabelBack", "cine.LabelSearch", "cine.LabelOption",
            "cine.LabelScrollDesc", "cine.changePage", "cine.labelEditOrder", "cine.labelClearFavor",
            "cine.labelPageUD", "buttonNames.Erase", "buttonNames.close", "buttonNames.Exit"};

    /** Is a icon image. */
    private final String[] buttonIconNames = {"b_btn_back.png", "b_btn_search.png", "b_btn_d.png",
            "b_btn_page.png", "b_btn_skip.png", "btn_a.png", "b_btn_b.png", "b_btn_page.png", "btn_c.png",
            "b_btn_exit.png", "b_btn_exit.png"};
    /** The button hash names from UPP. */
    private final String[] buttonHashNames = {PreferenceService.BTN_BACK, PreferenceService.BTN_SEARCH,
            PreferenceService.BTN_D, PreferenceService.BTN_PAGE, PreferenceService.BTN_SKIP, PreferenceService.BTN_A,
            PreferenceService.BTN_B, PreferenceService.BTN_PAGE, PreferenceService.BTN_C, PreferenceService.BTN_EXIT,
            PreferenceService.BTN_EXIT};

    /** Slash image. */
    private Image imgSlash;

    /** Button icon image. */
    public Image[] imgButtonIcons;

    /** Button id. */
    private int[] btIds;

    /**
     * Initialize.
     */
    public void init() {
        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        imgButtonIcons = new Image[buttonIconNames.length];
        if (imgTable != null && !imgTable.isEmpty()) {
            for (int i = 0; i < buttonHashNames.length; i++) {
                imgButtonIcons[i] = (Image) imgTable.get(buttonHashNames[i]);
            }
        } else {
            for (int i = 0; i < buttonIconNames.length; i++) {
                imgButtonIcons[i] = DataCenter.getInstance().getImage(buttonIconNames[i]);
            }
        }
        if (imgSlash == null) { imgSlash = DataCenter.getInstance().getImage("btn_slash.png"); }
    }

    /**
     * dispose.
     */
    public void dispose() {
        if (imgButtonIcons != null) {
            for (int i = 0; i < buttonIconNames.length; i++) {
                DataCenter.getInstance().removeImage(buttonIconNames[i]);
                imgButtonIcons[i] = null;
            }
            imgButtonIcons = null;
        }
    }

    /**
     * Graphics.
     *
     * @param g the Graphics
     */
    public void paint(Graphics g) {
        if (btIds == null) {
            return;
        }
        g.setFont(Rs.F17);
        int btWth = 0;
        for (int i = 0; i < btIds.length; i++) {
            if (imgButtonIcons[btIds[i]] != null) {
                btWth += imgButtonIcons[btIds[i]].getWidth(this);
                btWth += buttonGap;
            }

            String txt = (String) DataCenter.getInstance().get(buttonNames[btIds[i]]);
            if (txt != null) {
                btWth += g.getFontMetrics().stringWidth(txt);
                btWth += buttonGap;
            }
            if (i == btIds.length - 1) {
                continue;
            }
            if (imgSlash != null) {
                btWth += imgSlash.getWidth(this);
                btWth += buttonGap;
            }
        }

        g.setColor(Rs.C241241241);
        int startX = getWidth() - btWth;
        for (int i = 0; i < btIds.length; i++) {
            Image imgButtonIcon = imgButtonIcons[btIds[i]];
            if (imgButtonIcon != null) {
                g.drawImage(imgButtonIcon, startX, 0 + 4, this);
                startX += imgButtonIcon.getWidth(this);
                startX += buttonGap;
            }
            String txt = (String) DataCenter.getInstance().get(buttonNames[btIds[i]]);
            if (txt != null) {
                g.drawString(txt, startX, 15 + 4);
                startX += g.getFontMetrics().stringWidth(txt);
                startX += buttonGap;
            }
            if (i == btIds.length - 1) {
                continue;
            }
            if (imgSlash != null) {
                g.drawImage(imgSlash, startX, 4 + 4, this);
                startX += imgSlash.getWidth(this);
                startX += buttonGap;
            }
        }
    }

    /**
     * Sets a button id.
     * @param btnIds is button id.
     */
    public void setButtonIds(int[] btnIds) {
        this.btIds = btnIds;
    }
}

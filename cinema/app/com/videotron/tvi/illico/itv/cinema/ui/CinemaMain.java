package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.CacheManager;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.itv.cinema.controller.HttpConnection;
import com.videotron.tvi.illico.itv.cinema.controller.LogDisplayer;
import com.videotron.tvi.illico.itv.cinema.controller.MainManager;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.data.RecentFilmData;
import com.videotron.tvi.illico.itv.cinema.gui.CinemaMainRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.comp.MainMenuUI;
import com.videotron.tvi.illico.itv.cinema.ui.popup.ConfirmPopup;
import com.videotron.tvi.illico.itv.cinema.ui.popup.NotiPopup;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class is the CinemaMain Scene.
 * @version $Revision: 1.20 $ $Date: 2012/02/15 04:09:59 $
 * @author pellos
 */
public class CinemaMain extends BasicUI implements MenuListener {
    /**
     * Is the serialVersionID.
     */
    private static final long serialVersionUID = 1L;
    /** The Constant MENU_TITLE ID. */
    private static final int MENU_TITLE = 0;
    /** The Constant MENU_THEATRE ID. */
    private static final int MENU_THEATRE = 1;
    /** The Constant MENU_PREFERENCES ID. */
    private static final int MENU_PREFERENCES = 2;

    /** The recent data. */
    private static final String ID_RECENT_DATA = "recentData";

    /** Is the CinemaMainRenderer instance. */
    private CinemaMainRenderer cinemaMainRenderer = null;
    /** Is the RecentFilmData. */
    private RecentFilmData[] recentFilmData = null;
    /** The ClickingEffect instance. */
    private ClickingEffect clickEffect = null;
    /** The main menu component. */
    private MainMenuUI mainMenu = null;
    /** The AlphaEffect instance. */
    private AlphaEffect fadeIn = null;
    /** The root menu item for D-Option. */
    private final MenuItem rootMenuItem = new MenuItem("D-Option", new MenuItem[] {OPTION_PREFERENCE, OPTION_HELP});

    /** Is a list index. */
    private int listIndex = 0;
    /** The widget index. */
    private int widgetIndex = 0;
    /** The widget position. */
    private int widgetPos = 0;
    /** Is a List focus or a widget focus. */
    private boolean isListFocus = true;
    /** The is main menu effect. */
    public boolean isMainMenuEffect = false;

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (cinemaMainRenderer == null) {
            cinemaMainRenderer = new CinemaMainRenderer();
            setRenderer(cinemaMainRenderer);
            clickEffect = new ClickingEffect(this, 5);
            fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
            mainMenu = new MainMenuUI();
            mainMenu.setBounds(48, 101, 304, 485 - 101);
            mainMenu.setRenderer(new MainMenuRenderer());
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset whether resources is reset or not
     */
    protected void startScene(boolean reset) {
        if (Log.DEBUG_ON) { Log.printDebug("[CinemaMain]start"); }
        Logger.debug(this, "Time : " + System.currentTimeMillis());
        if (reset) {
            listIndex = 0;
            widgetIndex = 0;
            widgetPos = 0;
            isListFocus = true;
        }
        addFooterBtn(new int[] {ButtonIcons.BUTTON_EXIT, ButtonIcons.BUTTON_SEARCH, ButtonIcons.BUTTON_OPTION});
        prepare();
        setVisible(false);
        boolean isTempThread = false;
        if (!reset) {
            setVisible(true);
            isTempThread = true;
        }
        isMainMenuEffect = false;
        notifyFromDataLoadingThread(new Object[]{ID_RECENT_DATA}, isTempThread);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        if (optScr != null) { optScr.stop(); }
        cinemaMainRenderer.stop();
        recentFilmData = null;
    }

    /**
     * Dispose the Scene.
     */
    protected void disposeScene() {
        recentFilmData = null;
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        //tklee
        //int totalCount = recentFilmData.length;
        int totalCount = 0;
        if (recentFilmData != null) { totalCount = recentFilmData.length; }
        //
        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (isListFocus) {
                listIndex--;
                if (listIndex < 0) {
                    listIndex = 0;
                }
            } else {
                isListFocus = true;
                widgetIndex = 0;
                widgetPos = 0;
                listIndex = 2;
                cinemaMainRenderer.setWidgetIndex(widgetIndex, widgetPos);
            }
            cinemaMainRenderer.setListIndex(listIndex, isListFocus);
            repaint();
            break;
        case KeyEvent.VK_DOWN:
            if (isListFocus) {
                listIndex++;
                if (listIndex > 2) {
                    listIndex = 2;
                    if (totalCount > 0) { isListFocus = false; }
                }
                cinemaMainRenderer.setListIndex(listIndex, isListFocus);
                repaint();
            }
            break;
        case KeyEvent.VK_LEFT:
            if (!isListFocus) {
                if (widgetIndex == 0) {
                    isListFocus = true;
                    cinemaMainRenderer.setListIndex(listIndex, isListFocus);
                }
                if (widgetIndex > 0) {
                    widgetIndex--;
                    if (widgetPos > 0) { widgetPos--; }
                    cinemaMainRenderer.setWidgetIndex(widgetIndex, widgetPos);
                }
                repaint();
            }
            break;
        case KeyEvent.VK_RIGHT:
            if (isListFocus) {
                if (totalCount > 0) { isListFocus = false; }
                cinemaMainRenderer.setListIndex(listIndex, isListFocus);
            } else {
                if (widgetIndex < totalCount - 1) {
                    widgetIndex++;
                    if (widgetPos < 2) { widgetPos++; }
                    cinemaMainRenderer.setWidgetIndex(widgetIndex, widgetPos);
                }
            }
            repaint();
            break;
        case HRcEvent.VK_ENTER:
            if (isListFocus) {
                clickEffect.start(52, 101 + (listIndex * 34), 348 - 52, 142 - 101);
                Object[] values = {getCurrentSceneId(), null, null};
                if (listIndex == MENU_TITLE) {
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_TITLE, true, values);
                } else if (listIndex == MENU_THEATRE) {
                    if (DataManager.region.isEmpty()) {
                        loadData(new Object[]{ID_REGION_DATA}, true);
                    } else { notifyFromDataLoadingThread(new Object[]{ID_REGION_DATA}, false); }
                } else if (listIndex == MENU_PREFERENCES) {
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_PREFERENCE, true, values);
                }
            } else {
                Object[] values = {getCurrentSceneId(), recentFilmData[widgetIndex], null};
                Log.printInfo("movieID = " + recentFilmData[widgetIndex].getID());
                if (Rs.vfavoriteTheatre.size() > 0) {
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_SHOWTIMES, true, values);
                } else {
                    confirmPopup.setMode(ConfirmPopup.MODE_NO_FAVORITES);
                    confirmPopup.resetFocus();
                    addPopup(confirmPopup);
                }
            }

            break;
        case KeyCodes.COLOR_D:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_OPTION]);
            if (optScr != null) { optScr.start(rootMenuItem, this); }
            break;
        case KeyCodes.LAST:
            if (Log.DEBUG_ON) {
                Log.printDebug("[CinemaMainUI.keyAction]keyCode is KeyCodes.LAST.");
            }
            //footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            //MainManager.getInstance().requestStartMenuApp();
            return true;
        case KeyCodes.COLOR_B:
        case KeyCodes.SEARCH:
            if (!Rs.IS_EMULATOR && keyCode == KeyCodes.COLOR_B) { return false; }
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_SEARCH]);
            addPopup(titleSearchPopup);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * D-Option canceled.
     */
    public void canceled() {
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * D-Option Selected.
     *
     * @param item the item
     */
    public void selected(MenuItem item) {
        if (optScr != null) { optScr.stop(); }
        if (item.equals(OPTION_PREFERENCE)) {
            Object[] values = {getCurrentSceneId(), null, null};
            SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_PREFERENCE, true, values);
        } else if (item.equals(OPTION_HELP)) {
            MainManager.getInstance().requestStartHelpApp();
        }
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, final boolean isFromThread) {
        DataManager dataManager = DataManager.getInstance();
        String procId = (String) obj[0];
        if (procId.equals(ID_RECENT_DATA)) {
            //tklee
            //recentFilmData = dataManager.getRecentMovie();
            //cinemaMainRenderer.setRecentFilmData(recentFilmData);

            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    MainManager.getInstance().hideLoadingAnimation();
                    cinemaMainRenderer.setListIndex(listIndex, isListFocus);
                    cinemaMainRenderer.setWidgetIndex(widgetIndex, widgetPos);
                    startComp();
                    if (!isFromThread) {
                        isMainMenuEffect = true;
                        setVisible(false);
                        fadeIn.start();
                        isMainMenuEffect = false;
                    }
                    setVisible(true);
                    add(mainMenu);
                    if (!isFromThread) {
                        mainMenu.setVisible(false);
                        isMainMenuEffect = true;
                        mainMenu.startEffect();
                        isMainMenuEffect = false;
                    }
                    mainMenu.setVisible(true);
                    repaint();
                    Logger.debug(this, "Time : " + System.currentTimeMillis());
                }
            });
            new Thread("CinemaMain|setPosterImage()") {
                public void run() {
                    //tklee
                    recentFilmData = DataManager.getInstance().getRecentMovie();
                    cinemaMainRenderer.setRecentFilmData(recentFilmData);
                    if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) { return; }
                    repaint();
                    //
                    setPosterImage(recentFilmData);
                }
            } .start();
        } else { super.notifyFromDataLoadingThread(obj, isFromThread); }
        repaint();
    }

    /**
     * Sets the widget poster.
     * @param data  widget data
     */
    public void setPosterImage(RecentFilmData[] data) {
        if (data != null) {
            Image[] bannerImage = cinemaMainRenderer.bannerImage;
            if (bannerImage == null) { return; }
            LogDisplayer logDisplayer = LogDisplayer.getInstance();
            logDisplayer.imgStartTime = new long[data.length];
            logDisplayer.imgEndTime = new long[data.length];
            //TODO test
            Hashtable newImgList = new Hashtable();
            Hashtable cachedList = DataManager.cachedImgList;
            boolean isNew = false;
            //
            for (int i = 0; i < data.length; i++) {
                String posterUrl = data[i].getPosterURL();
                if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) { break; }
                if (posterUrl == null || posterUrl.length() == 0) { continue; }
                Image posterImg = DataCenter.getInstance().getImage(posterUrl);
                if (posterImg == null && posterUrl != null && posterUrl.length() > 0) {
                    try {
                        byte[] imgByte = null;
                        //TODO test
                        if (cachedList != null) { imgByte = (byte[]) cachedList.get(posterUrl); }
                        //
                        if (imgByte == null) {
                            logDisplayer.imgStartTime[i] = System.currentTimeMillis();
                            imgByte = HttpConnection.getInstance().getBytes(posterUrl);
                            logDisplayer.imgEndTime[i] = System.currentTimeMillis();
                            //TODO test
                            //if (imgByte != null) { newImgList.put(posterUrl, imgByte); }
                            if (imgByte != null) { isNew = true; }
                        }
                        if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) { break; }
                        if (imgByte != null) {
                            //TODO test
                            newImgList.put(posterUrl, imgByte);
                            posterImg = FrameworkMain.getInstance().getImagePool().createImage(imgByte, posterUrl);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                bannerImage[i] = posterImg;
                if (posterImg != null) { FrameworkMain.getInstance().getImagePool().waitForAll(); }
                if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_MAIN) {
                    repaint();
                } else { break; }
            }
            if (SceneManager.getInstance().curSceneId == SceneManager.SCENE_ID_INVALID) {
                bannerImage = null;
                FrameworkMain.getInstance().getImagePool().clear();
                return;
            }
            //TODO test
            if (newImgList.size() > 0 && isNew) {
                //if (cachedList != null) { newImgList.putAll(cachedList); }
                CacheManager.getInstance().setRecentImgList(newImgList);
            }
            //
        }
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof NotiPopup) {
            removePopup();
            handleKey(Rs.KEY_EXIT);
        } else if (popup instanceof ConfirmPopup) {
            if (para instanceof Integer) {
                int btnFocus = ((Integer) para).intValue();
                if (btnFocus == ConfirmPopup.BTN_YES) {
                    Object[] values = {getCurrentSceneId(), null, null};
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_FAVORITE, true, values);
                } else {
                    RecentFilmData film = recentFilmData[widgetIndex];
                    /*String langType = FilmData.LANGUAGE_FRENCH;
                    if (film.getTitle() == null || film.getTitle().length() == 0) {
                        langType = FilmData.LANGUAGE_ENGLISH;
                    }*/
                    //String[] sendPara = {film.getID(), langType};
                    Object[] values = {getCurrentSceneId(), film, null};
                    SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_MOVIE_ASSET, true, values);
                }
            }
        } else { super.requestPopupOk(para); }
    }

    /**
     * <code>ChListRenderer</code> The class to paint a GUI for channel list information.
     *
     * @since   2011.03.20
     * @version $Revision: 1.20 $ $Date: 2012/02/15 04:09:59 $
     * @author  tklee
     */
    public class MainMenuRenderer extends Renderer {
        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return mainMenu.getBounds();
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) { }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {//mainMenu.setBounds(48, 101, 304, 485 - 101);
            g.translate(-48, -101);
            for (int i = 0; i < cinemaMainRenderer.list.length; i++) {
                int addedPos = i * 34;
                g.drawImage(cinemaMainRenderer.line, 66, 139 + addedPos, c);
                g.drawImage(cinemaMainRenderer.buttle01, 72, 119 + addedPos, c);
                if (i == listIndex && !isMainMenuEffect) {
                    g.setColor(Rs.C003003003);
                    g.setFont(Rs.F21);
                    g.drawImage(isListFocus ? cinemaMainRenderer.menuFocus : cinemaMainRenderer.menuFocusDim,
                            48, 101 + addedPos, c);
                } else {
                    g.setColor(Rs.C224224224);
                    g.setFont(Rs.F18);
                }
                g.drawString(cinemaMainRenderer.list[i], 95, 128 + addedPos);
            }
            g.translate(48, 101);
        }
    }
}

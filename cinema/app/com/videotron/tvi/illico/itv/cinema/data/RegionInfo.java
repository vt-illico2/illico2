package com.videotron.tvi.illico.itv.cinema.data;

import java.text.Collator;
import java.util.Locale;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
/**
 * The Class RegionInfo.
 * @version $Revision: 1.4 $ $Date: 2012/02/15 04:09:59 $
 * @author pellos
 */
public class RegionInfo implements Comparable {
    /** The Constant FR_COLLATOR. */
    protected static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);

    /** The Constant Montréal. */
    public static final int Montréal = 1; 
    
    /** The Constant Rive_Sud_Montérégie. */
    public static final int Rive_Sud_Montérégie = 3;
    
    /** The Constant Laval_Laurentides. */
    public static final int Laval_Laurentides = 4;
    
    /** The Constant Québec. */
    public static final int Québec = 5;
    
    /** The Constant Mauricie_Bois_Francs. */
    public static final int Mauricie_Bois_Francs  = 6;
    
    /** The Constant Cantons_de_lEst. */
    public static final int Cantons_de_lEst  = 7;
    
    /** The Constant Saguenay_Lac_St_Jean. */
    public static final int Saguenay_Lac_St_Jean = 8;
    
    /** The Constant Lanaudière. */
    public static final int Lanaudière = 9;
    
    /** The Constant Outaouais. */
    public static final int Outaouais = 10;
    
    /** The Constant Bas_St_Laurent. */
    public static final int Bas_St_Laurent = 11;
    
    /** The Constant Côte_Nord. */
    public static final int Côte_Nord = 12;
    
    /** The Constant Gaspésie. */
    public static final int Gaspésie = 13;
    
    /** The Constant Abitibi_Témiscamingue. */
    public static final int Abitibi_Témiscamingue = 14;
    
    /** The Constant Charlevoix. */
    public static final int Charlevoix = 15;
    
    /** The Constant RESIONID. */
    public static final int[] RESIONID = {Montréal, Rive_Sud_Montérégie, Laval_Laurentides,
        Québec, Mauricie_Bois_Francs, Cantons_de_lEst, Saguenay_Lac_St_Jean, Lanaudière, Outaouais,
        Bas_St_Laurent, Côte_Nord, Gaspésie, Abitibi_Témiscamingue, Charlevoix};
    
    /** The Constant RESIONNAME. */
    public static final String[] RESIONNAME = {DataCenter.getInstance().getString("cine.Montréal"),
        DataCenter.getInstance().getString("cine.Rive-Sud"), DataCenter.getInstance().getString("cine.Laval"),
        DataCenter.getInstance().getString("cine.Québec"), DataCenter.getInstance().getString("cine.Mauricie"),
        DataCenter.getInstance().getString("cine.Cantons"), DataCenter.getInstance().getString("cine.Saguenay"),
        DataCenter.getInstance().getString("cine.Lanaudière"), DataCenter.getInstance().getString("cine.Outaouais"),
        DataCenter.getInstance().getString("cine.Bas"), DataCenter.getInstance().getString("cine.Côte"),
        DataCenter.getInstance().getString("cine.Gaspésie"), DataCenter.getInstance().getString("cine.Abitibi"),
        DataCenter.getInstance().getString("cine.Charlevoix")
    };

    /** The region id. */
    private String regionId = null;
    /** The region name. */
    private String regionName = null;
    /** The theatres list. */
    private Vector theatreList = null;
    /** The theatre list for favorite. */
    private Vector theatreListForFavor = new Vector();

    /**
     * @return the theatreListForFavor
     */
    public Vector getTheatreListForFavor() {
        return theatreListForFavor;
    }

    /**
     * @param list the theatreListForFavor to set
     */
    public void setTheatreListForFavor(Vector list) {
        if (list != null) {
            this.theatreListForFavor = list;
        } else { theatreListForFavor.clear(); }
    }

    /**
     * Adds the TheatreData.
     *
     * @param theatreData the theatre data
     */
    public void addTheatreListForFavor(TheatreData theatreData) {
        theatreListForFavor.addElement(theatreData);
    }

    /**
     * @return the regionId
     */
    public String getRegionId() {
        return regionId;
    }
    /**
     * @param id the regionId to set
     */
    public void setRegionId(String id) {
        this.regionId = id;
    }
    /**
     * @return the regionName
     */
    public String getRegionName() {
        return regionName;
    }
    /**
     * @param name the regionName to set
     */
    public void setRegionName(String name) {
        this.regionName = name;
    }
    /**
     * @return the theatreList
     */
    public Vector getTheatreList() {
        return theatreList;
    }
    /**
     * @param theatres the theatreList to set
     */
    public void setTheatreList(Vector theatres) {
        this.theatreList = theatres;
    }

    /**
     * Adds the TheatreData.
     *
     * @param theatreData the theatre data
     */
    public void addTheatreData(TheatreData theatreData) {
        if (theatreList == null) { theatreList = new Vector(); }
        theatreList.addElement(theatreData);
    }

    /**
     * implements abstract method of Comparable Class.
     * <p>
     *
     * @param obj the object to compare (RegionInfo)
     * @return compared value.
     */
    public int compareTo(Object obj) {
        int selfId = Integer.parseInt(getRegionId());
        int paraId = Integer.parseInt(((RegionInfo) obj).getRegionId());
        int compareValue = selfId - paraId; //selfName.compareToIgnoreCase(paraName);
        return compareValue;
    }

}

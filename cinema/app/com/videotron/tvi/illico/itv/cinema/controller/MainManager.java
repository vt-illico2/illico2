package com.videotron.tvi.illico.itv.cinema.controller;

import java.awt.Point;
import java.io.File;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class is the MainManager class of Cinema.
 * @version $Revision: 1.15 $ $Date: 2012/02/15 04:09:58 $
 * @author Pellos
 */

public final class MainManager implements ScreenSaverConfirmationListener, DataUpdateListener, InbandDataListener {
    /** The Constant IB_SERVER_CONFIG. the name for inband path.*/
    private static final String IB_SERVER_CONFIG = "itvServerConfig";
    /** The Constant IB_FILE_NAME. */
    private static final String IB_FILE_NAME = "itv_server";

    /** The point of loading animation to show on screen. */
    private final Point loadingAniPoint = new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2);

    /** Instance of MainManager. */
    private static MainManager instance = new MainManager();

    /** Instance of LoadingAnimationService. */
    private LoadingAnimationService loadingAnimationService = null;
    /** Instance of MonitorService. */
    private MonitorService monitorSvc = null;
    /** The instance of ErrorMessageService. */
    private ErrorMessageService errMsgService = null;
    /** The err msg handler. */
    private ErrorMessageHandler errMsgHandler = null;

    /** whether the loading animation is activated. */
    private boolean isLoadingAnimation = false;
    /** The parameter. */
    private String[] startParameter = null;
    /** The inband version. */
    private int inbandVersion = -1;

    /**
     * Gets the singleton instance of MainManager.
     * @return MainManager
     */
    public static synchronized MainManager getInstance() {
        return instance;
    }

    /**
     * initial method after called Constructor.
     * @param context is XletContext.
     */
    public void init(XletContext context) {
        SceneManager.getInstance().init();
        DataCenter.getInstance().addDataUpdateListener(IB_SERVER_CONFIG, this);
        CommunicationManager.getInstance().start(context, this);
    }

    /**
     * This method starts a SceneManager.
     */
    public void start() {
        long startTime = System.currentTimeMillis();
        Logger.debug(this, "start()-startTime : " + startTime);
        isLoadingAnimation = false;
        startParameter = getStartParameter();
        VbmController.getInstance().writeStartLog(startParameter);
        CacheManager.getInstance().prepareCacheData();
        SceneManager.getInstance().start();
        addScreenSaverListener();
        long endTime = System.currentTimeMillis();
        Logger.debug(this, "start()-endTime : " + endTime);
        Logger.debug(this, "start()-processTime : " + (endTime - startTime));
    }

    /**
     * This method pause processes.
     */
    public void pause() {
        SceneManager.getInstance().pause();
        DataManager.getInstance().reset();
        removeScreenSaverListener();
        hideLoadingAnimation();
        VbmController.getInstance().writeEndLog();
    }

    /**
     * This method dispose a SceneManager.
     */
    public void destroy() {
        SceneManager.getInstance().dispose();
        PreferenceProxy.getInstance().dispose();
        removeScreenSaverListener();
    }

    /**
     * This method does a starting loadingAnimation.
     */
    public void showLoadingAnimation() {
        showLoadingAnimation(loadingAniPoint);
    }

    /**
     * This method does a starting loadingAnimation.
     *
     * @param point the point for x, y position
     */
    public void showLoadingAnimation(Point point) {
        Log.printInfo("Cinema showLoadingAnimation ");
        if (loadingAnimationService == null) { return; }
        try {
            Log.printInfo("showLoadingAnimation start");
            loadingAnimationService.showLoadingAnimation(point);
            isLoadingAnimation = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method does a starting loadingAnimation without delay.
     *
     * @param point the point for x, y position
     */
    /*public void showNotDelayLoadingAnimation(Point point) {
        Log.printInfo("Cinema showLoadingAnimation ");
        if (loadingAnimationService == null) { return; }
        try {
            Log.printInfo("showLoadingAnimation start");
            loadingAnimationService.showNotDelayLoadingAnimation(point);
            isLoadingAnimation = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * This method does a stopping loadingAnimation.
     */
    public void hideLoadingAnimation() {
        Log.printInfo("Cinema hideLoadingAnimation ");
        if (loadingAnimationService == null) { return; }
        try {
            loadingAnimationService.hideLoadingAnimation();
            isLoadingAnimation = false;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the isLoadingAnimation
     */
    public boolean isLoadingAnimation() {
        return isLoadingAnimation;
    }

    /**
     * ScreenSaver is Confirmed to listeners.
     *
     * @return true
     * @throws RemoteException the remote exception
     */
    public boolean confirmScreenSaver() throws RemoteException {
        return true;
    }

    /**
     * Called when data has been removed.
     *
     * @param key the key
     */
    public void dataRemoved(String key) {
    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        Log.printDebug("dataUpdated key = " + key + " value = " + value);
        if (key.equals(PreferenceService.IXC_NAME)) {
            PreferenceProxy.getInstance().addPreferenceListener();
        } else if (key.equals(MonitorService.IXC_NAME)) {
            monitorSvc = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
            try {
                monitorSvc.addInbandDataListener(this, Rs.APP_NAME);
            } catch (Exception e) {
                Logger.error(this, "dataUpdated()-" + e.getMessage());
            }
        } else if (key.equals(LoadingAnimationService.IXC_NAME)) {
            loadingAnimationService = (LoadingAnimationService) DataCenter.getInstance().get(
                LoadingAnimationService.IXC_NAME);
        } else if (key.equals(IB_SERVER_CONFIG) && value != null) {
            HttpConnection.getInstance().setServerInfo((File) value);
        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) DataCenter.getInstance().get(StcService.IXC_NAME);
            try {
                int currentLevel = stcService.registerApp(Rs.APP_NAME);
                Logger.debug(this, "stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
            Object logCacheObj = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
            if (logCacheObj != null) { DataCenter.getInstance().put("LogCache", logCacheObj); }
            try {
                stcService.addLogLevelChangeListener(Rs.APP_NAME, new LogLevelChangeHandler());
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
        } else if (key.equals(ErrorMessageService.IXC_NAME)) {
            errMsgService = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
            errMsgHandler = new ErrorMessageHandler();
        } else if (key.equals(VbmService.IXC_NAME)) {
            VbmService vbmService = (VbmService) DataCenter.getInstance().get(VbmService.IXC_NAME);
            VbmController.getInstance().init(vbmService);
        }
    }

    /**
     * Update a data via in-band when Monitor tuned a In-band channel for Data.
     *
     * @param locator The locator for in-band data channel.
     * @throws RemoteException the remote exception
     */
    public void receiveInbandData(String locator) throws RemoteException {
        Logger.info(this, "receiveInbandData()-locator : " + locator);
        if (monitorSvc == null) { return; }
        int version = monitorSvc.getInbandDataVersion(IB_FILE_NAME);
        Logger.debug(this, "receiveInbandData()-version : " + version);
        if (inbandVersion != version || version == MonitorService.VERSION_NOT_FOUND) {
            inbandVersion = version;
            DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        }
        monitorSvc.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
    }

    /**
     * Add ScreenSaverListener.
     */
    public void addScreenSaverListener() {
        try {
            if (monitorSvc != null) {
                if (Log.DEBUG_ON) { Log.printInfo("SearchController: addScreenSaverListener call!"); }
                monitorSvc.addScreenSaverConfirmListener(this, Rs.APP_NAME);
            }
        } catch (Exception e) {
            if (Log.DEBUG_ON) { Log.printInfo("SearchController: addScreenSaverListener Exception"); }
            e.printStackTrace();
        }
    }

    /**
     * Removes removeScreenSaverListener from MonitorService.
     */
    public void removeScreenSaverListener() {
        try {
            if (monitorSvc != null) { monitorSvc.removeScreenSaverConfirmListener(this, Rs.APP_NAME); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the start parameter.
     *
     * @return the start parameter
     */
    private String[] getStartParameter() {
        if (monitorSvc != null) {
            try {
                String[] startParm = monitorSvc.getParameter(Rs.APP_NAME);
                Logger.debug(this, "getStartParameter()-startParm : " + startParm);
                return startParm;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * request to start menu application.
     */
    public void requestStartMenuApp() {
        if (startParameter == null) { startParameter = getStartParameter(); }
        Logger.debug(this, "startParameter : " + startParameter);
        if (startParameter != null && startParameter.length > 0 && startParameter[0] != null) {
            Logger.debug(this, "startParams[0] : " + startParameter[0]);
            if (startParameter[0].equalsIgnoreCase(MonitorService.REQUEST_APPLICATION_MENU)) {
                //exitToChannel()
                requestStartUnboundApplication("Menu", new String[]{MonitorService.REQUEST_APPLICATION_LAST_KEY});
            }
        }
    }

    /**
     * request to start help application.
     */
    public void requestStartHelpApp() {
        requestStartUnboundApplication("Help", new String[]{Rs.APP_NAME, Rs.APP_NAME, null});
    }

    /**
     * Start Unbound Application.
     * @param reqAppName application Name.
     * @param reqParams null.
     */
    private void requestStartUnboundApplication(String reqAppName, String[] reqParams) {
        if (monitorSvc != null) {
            try {
                monitorSvc.startUnboundApplication(reqAppName, reqParams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Exit to channel from Cinema.
     */
    public void exitToChannel() {
        Log.printDebug("[SceneMgr.exitToChannel]start.");
        if (monitorSvc == null) {
            Logger.debug(this, "exitToChannel()-Monitor Service is null.");
            return;
        }
        try {
            monitorSvc.exitToChannel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Does a videoResize.
     * @param x x position
     * @param y y position
     * @param w width
     * @param h height
     */
    public void resizeScaledVideo(int x, int y, int w, int h) {
        try {
            EpgService epgSvc = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            if (epgSvc == null) { return; }
            VideoController vCnt = epgSvc.getVideoController();
            vCnt.resize(x, y, w, h, VideoController.SHOW);
            Log.printInfo("x=" + x + " y=" + y + " w=" + w + " h=" + h);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Change channel.
     */
    public void changeChannel() {
        try {
            EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            if (epgService != null) { epgService.changeChannel(-1); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Show error message.
     *
     * @param code the code
     */
    public void showErrorMessage(String code) {
        if (errMsgService == null) { return; }
        try {
            errMsgService.showErrorMessage(code, errMsgHandler);
            Logger.debug(this, "called errMsgService.showErrorMessage");
        } catch (Exception e) {
            Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
        }
    }

    /**
     * Hide error message.
     */
    public void hideErrorMessage() {
        if (errMsgService == null) { return; }
        try {
            errMsgService.hideErrorMessage();
        } catch (Exception e) {
            Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
        }
    }

    /**
     * The Class LogLevelChangeHandler implement LogLevelChangeListener.
     */
    class LogLevelChangeHandler implements LogLevelChangeListener {
        /**
         * implements LogLevelChangeListener method.
         * when log level is changed, this method is called.
         *
         * @param logLevel the log level
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener#logLevelChanged(int)
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * The Class ErrorMessageHandler implement ErrorMessageListener.
     */
    class ErrorMessageHandler implements ErrorMessageListener {
        /**
         * called when ErrorMessageService completed the action requested.
         *
         * @param buttonType the button type
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener#actionPerformed(int)
         */
        public void actionPerformed(int buttonType) throws RemoteException {
            Logger.info(this, "actionPerformed-buttonType : " + buttonType);
            //hideErrorMessage();
            if (buttonType == ErrorMessageListener.BUTTON_TYPE_CLOSE) {
                SceneManager sceneManager = SceneManager.getInstance();
                sceneManager.scenes[sceneManager.curSceneId].notifyErrorMessage();
            }
        }
    }
}

package com.videotron.tvi.illico.itv.cinema.data;

import java.text.Collator;
import java.util.Locale;
import java.util.Vector;

import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.ixc.upp.Definitions;

/**
 * This class is the FilmData.
 * @version $Revision: 1.4 $ $Date: 2012/02/15 04:09:59 $
 * @author pellos
 */
public class FilmData implements Comparable {
    /** The Constant FR_COLLATOR. */
    protected static final Collator FR_COLLATOR = Collator.getInstance(Locale.CANADA_FRENCH);

    /** Define a english. */
    public static final String LANGUAGE_ENGLISH = "va";
    /** Define a french. */
    public static final String LANGUAGE_FRENCH = "vf";
    /** Define a other. */
    public static final String LANGUAGE_OTHER = "vo";

    /** Define a 13 age. */
    public static final String THIRTEEN_PLUS = "13+";
    /** Define a 16 age. */
    public static final String SIXTEEN_PLUS = "16+";
    /** Define a 18 age. */
    public static final String EIGHTEEN_PLUS = "18+";
    /** Define a N/D. */
    public static final String ND = "N/D";
    /** Define a ALL. */
    public static final String ALL = "G";

    /** Defines a language of sorting. */
    public static final int SORT_LANG = 0;
    /** Defines a alphabet of sorting. */
    public static final int SORT_ALPHABET = 1;
    /** Defines a gener of sorting. */
    //public static final int GENER = 1;
    /** Defines a released of sorting. */
    //private static final int RELEASED = 2;

    /** The sort type. */
    public static int sortType = SORT_LANG;

    /** Is a release date. */
    private String releaseDate;

    /** Is a movie id. */
    private String id;
    /** The title. */
    private String title = "";
    /** Is a movie title En. */
    //private String titleEn;
    /** The title fr. */
    //private String titleFr;
    /** The title other. */
    //private String titleOther;
    /** Is a movie language. */
    private String langue;
    /** Is a movie genre. */
    private String genre;
    /** Is a movie classement. */
    private String classement;
    /** Is a movie duration. */
    private String duration;

    /** store a schedule data. */
    private Vector schedule = new Vector();
    /** Is a poster image url. */
    private String posterURL;
    /** The poster img. */
    //private Image posterImg = null;
    /** The film info data. */
    private FilmInfoData filmInfoData = null;

    /**
     * @return the titleEn
     */
    /*public String getTitleEn() {
        return titleEn;
    }*/

    /**
     * @param nameEn the titleEn to set
     */
    /*public void setTitleEn(String nameEn) {
        this.titleEn = nameEn;
    }*/

    /**
     * @return the titleFr
     */
    /*public String getTitleFr() {
        return titleFr;
    }*/

    /**
     * @param nameFr the titleFr to set
     */
    /*public void setTitleFr(String nameFr) {
        this.titleFr = nameFr;
    }*/

    /**
     * @return the titleOther
     */
    /*public String getTitleOther() {
        return titleOther;
    }*/

    /**
     * @param nameOther the titleOther to set
     */
    /*public void setTitleOther(String nameOther) {
        this.titleOther = nameOther;
    }*/

    /**
     * @return the filmInfoData
     */
    public FilmInfoData getFilmInfoData() {
        return filmInfoData;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @param infoData the filmInfoData to set
     */
    public void setFilmInfoData(FilmInfoData infoData) {
        this.filmInfoData = infoData;
    }

    /**
     * Gets the poster image.
     *
     * @return the posterImg
     */
    //public Image getPosterImage() {
    //    return posterImg;
    //}

    /**
     * Sets the poster image.
     *
     * @param posterImage the posterImg to set
     */
    //public void setPosterImage(Image posterImage) {
    //    this.posterImg = posterImage;
    //}

    /**
     * Get PosterURL.
     * @return posterURL
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     * Sets the poster url.
     * @param posrURL the new poster url
     */
    public void setPosterURL(String posrURL) {
        this.posterURL = posrURL;
    }

    /**
     * Gets the iD.
     * @return the iD
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the iD.
     * @param iD the new iD
     */
    public void setID(String iD) {
        id = iD;
    }

    /**
     * Gets the release date.
     * @return the release date
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * Sets the release date.
     * @param date the new release date
     */
    public void setReleaseDate(String date) {
        this.releaseDate = date;
    }

    /**
     * Gets the langue.
     * @return the langue
     */
    public String getLangue() {
        return langue;
    }

    /**
     * Sets the langue.
     * @param lang the new langue
     */
    public void setLangue(String lang) {
        this.langue = lang;
    }

    /**
     * Gets the genre.
     * @return the genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Sets the genre.
     * @param gen the new genre
     */
    public void setGenre(String gen) {
        this.genre = gen;
    }

    /**
     * Gets the classement.
     * @return the classement
     */
    public String getClassement() {
        return classement;
    }

    /**
     * Sets the classement.
     * @param classe the new classement
     */
    public void setClassement(String classe) {
        this.classement = classe;
    }

    /**
     * Gets the duration.
     * @return the duration
     */
    public String getDuration() {
        return duration;
    }

    /**
     * Sets the duration.
     * @param dur the new duration
     */
    public void setDuration(String dur) {
        this.duration = dur;
    }

    /**
     * Gets the schedule.
     * @return the schedule
     */
    public Vector getSchedule() {
        return schedule;
    }

    /**
     * Sets the schedule.
     * @param sche the new schedule
     */
    public void setSchedule(Vector sche) {
        this.schedule = (Vector) sche.clone();
    }

    /**
     * implements abstract method of Comparable Class.
     * <p>
     *
     * @param obj the object to compare
     * @return compared value.
     */
    public int compareTo(Object obj) {
        FilmData paraData = ((FilmData) obj);
        String paraLang = paraData.getLangue();
        if (langue.equals(paraLang) || sortType == SORT_ALPHABET) {
            String ownTxt = title;
            String paraTxt = paraData.getTitle();
            //int reuturnValue = ownTxt.compareToIgnoreCase(paraTxt);
            int reuturnValue = FR_COLLATOR.compare(ownTxt, paraTxt);
            return reuturnValue;
        } else {
            int reuturnValue = -1;
            if ((getSystemLang().equals(paraLang)) || LANGUAGE_OTHER.equals(langue)) { reuturnValue = 1; }
            return reuturnValue;
        }

        //language, genre/aphabetical 로 구분할 경우
        /*if (langue.equals(paraLang)) {
            String ownTxt = title;
            String paraTxt = paraData.getTitle();
            if (sortType == GENER) {
                ownTxt = genre + ownTxt;
                paraTxt = paraData.getGenre() + paraTxt;
            }
            int reuturnValue = ownTxt.compareToIgnoreCase(paraTxt);
            return reuturnValue;
        } else {
            int reuturnValue = -1;
            if ((getSystemLang().equals(paraLang)) || LANGUAGE_OTHER.equals(langue)) { reuturnValue = 1; }
            return reuturnValue;
        }*/

        //언어가 틀려도 같은 영화는 합치는 경우 처리
        /*String[] ownLangData = getLangData();
        FilmData paraData = (FilmData) obj;
        String[] paraLangData = paraData.getLangData();
        if (ownLangData[0].equals(paraLangData[0])) {
            Logger.debug(obj, "compareTo() - ownTxt  : " + ownLangData[1]);
            Logger.debug(obj, "compareTo() - paraTxt  : " + paraLangData[1]);
            if (sortType == GENER) {
                ownLangData[1] = genre + ownLangData[1];
                paraLangData[1] = paraData.getGenre() + paraLangData[1];
            }
            int reuturnValue = ownLangData[1].compareToIgnoreCase(paraLangData[1]);
            Logger.debug(obj, "compareTo() - reuturnValue  : " + reuturnValue);
            return reuturnValue;
        } else {
            int reuturnValue = -1;
            if ((getSystemLang().equals(paraLangData[0])) || LANGUAGE_OTHER.equals(ownLangData[0])) {
                reuturnValue = 1;
            }
            Logger.debug(obj, "compareTo() - reuturnValue  : " + reuturnValue);
            return reuturnValue;
        }*/
    }

    /**
     * Gets the lang data.
     *
     * @return the lang data
     */
    /*public String[] getLangData() {
        String systemLang = getSystemLang();
        String[] titles = {getTitleFr(), getTitleEn(), getTitleOther()};
        String[] langType = {LANGUAGE_FRENCH, LANGUAGE_ENGLISH, LANGUAGE_OTHER};
        int checkIdx = -1;
        for (int i = 0; i < titles.length; i++) {
            if (titles[i] != null) {
                if (checkIdx != -1 && i == 2) { break; }
                checkIdx = i;
                if (systemLang.equals(langType[i])) { break; }
            }
        }
        if (checkIdx < 0) { checkIdx = 2; }
        return new String[]{langType[checkIdx], titles[checkIdx]};
    }*/

    /**
     * Gets the system lang.
     *
     * @return the system lang
     */
    private String getSystemLang() {
        if (Definitions.LANGUAGE_FRENCH.equals(Rs.titleFirstLanguage)) { return LANGUAGE_FRENCH; }
        return LANGUAGE_ENGLISH;
    }
}

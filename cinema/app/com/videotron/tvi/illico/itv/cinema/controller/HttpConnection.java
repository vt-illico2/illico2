/**
 * @(#)HttpConnection.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.itv.cinema.controller;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;

import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.util.DataConverter;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is the Http connection.
 * @author pellos
 * @version $Revision: 1.12 $ $Date: 2012/02/23 06:11:11 $
 */
public final class HttpConnection {
    /** The Constant SERVER_TYPE at inband data. */
    private static final int SERVER_TYPE = 4;
    /** HttpConnection instance. */
    private static HttpConnection httpConnection = null;
    /** Server IP. */
    private String serverIP = "data.canoe.com/tvi_xml/illico2Test/";
    /** The proxy host. */
    private String[] proxyHost = null;
    /** The proxy port. */
    private int[] proxyPort = null;
    /** The config file. */
    public File configFile = null;

    /**
     * Constructor.
     */
    private HttpConnection() {
        /*serverIP = (String) DataCenter.getInstance().get("serverIP");
        DataCenter dc = DataCenter.getInstance();
        if ("true".equalsIgnoreCase(dc.getString("proxyUse"))) {
            proxyHost = TextUtil.tokenize(dc.getString("proxyHost"), "|");
            String[] ports = TextUtil.tokenize(dc.getString("proxyPort"), "|");
            proxyPort = new int[ports.length];
            for (int i = 0; i < ports.length; i++) {
                proxyPort[i] = Integer.parseInt(ports[i]);
            }
        }*/
    }

    /**
     * Gets the singleton instance of HttpConnection.
     * @return instance
     */
    public static synchronized HttpConnection getInstance() {
        if (httpConnection == null) {
            httpConnection = new HttpConnection();
        }
        return httpConnection;
    }

    /**
     * Sets the server info received from inband.
     *
     * @param file the new server info
     */
    public void setServerInfo(File file) {
        if (file == null) { return; }
        configFile = file;
        byte[] data = BinaryReader.read(file);
        if (data != null) {
            try {
                String serverUrl = "";
                String[] ibProxyHost = null;
                int[] ibProxyPort = null;
                int index = 0;
                int listCount = DataConverter.convertOneByteToInt(data, ++index);
                for (int i = 0; i < listCount; i++) {
                    int serverType = DataConverter.convertOneByteToInt(data, ++index);
                    int dnsLen = DataConverter.convertOneByteToInt(data, ++index);
                    if (serverType == SERVER_TYPE) {
                        serverUrl = DataConverter.convertToString(data, ++index, dnsLen);
                        index += dnsLen - 1;
                        serverUrl += ":" + DataConverter.convertTwoBytesToInt(data, ++index);
                        index++;
                    } else { index += dnsLen + 2; }

                    int contextLen = DataConverter.convertOneByteToInt(data, ++index);
                    if (serverType == SERVER_TYPE) {
                        serverUrl += DataConverter.convertToString(data, ++index, contextLen);
                        index += contextLen - 1;
                    } else { index += contextLen; }

                    int proxyCount = DataConverter.convertOneByteToInt(data, ++index);
                    if (serverType == SERVER_TYPE && proxyCount > 0) {
                        ibProxyHost = new String[proxyCount];
                        ibProxyPort = new int[proxyCount];
                    }
                    for (int j = 0; j < proxyCount; j++) {
                        int proxyIpLen = DataConverter.convertOneByteToInt(data, ++index);
                        if (serverType == SERVER_TYPE) {
                            ibProxyHost[j] = DataConverter.convertToString(data, ++index, proxyIpLen);
                            index += proxyIpLen - 1;
                            ibProxyPort[j] = DataConverter.convertTwoBytesToInt(data, ++index);
                            index++;
                        } else { index += proxyIpLen + 2; }
                    }
                    if (serverType == SERVER_TYPE) {
                        serverIP = serverUrl;
                        proxyHost = ibProxyHost;
                        proxyPort = ibProxyPort;
                        Logger.debug(this, "setServerInfo()-serverIP : " + serverIP);
                        for (int j = 0; proxyHost != null && j < proxyHost.length; j++) {
                            Logger.debug(this, "setServerInfo()-proxyHost[" + j + "] : " + proxyHost[j]);
                            Logger.debug(this, "setServerInfo()-proxyPort[" + j + "] : " + proxyPort[j]);
                        }
                        break;
                    }
                }
            } catch (Exception e) {
                Logger.error(this, "Inband data error during parse-" + e.getMessage());
                DataManager.getInstance().printError(DataManager.ERR_CONFIG_PARSING, e.getMessage());
            }
        } else {
            DataManager.getInstance().printError(DataManager.ERR_CONFIG_PARSING, "data is null.");
        }
    }

    /**
     * return HttpURLConnection connected with Server.
     *
     * @param path the path for server url
     * @return HttpURLConnection HttpURLConnection connected with Server
     * @throws Exception the exception
     */
    public HttpURLConnection getURLConnection(String path) throws Exception {
        Exception proxyException = null;
        String reqUrl = "http://" + serverIP + "/" + path + ".xml";
        Log.printInfo("getURLConnection url = " + reqUrl);
        //"true".equalsIgnoreCase(DataCenter.getInstance().getString("proxyUse"))
        if (proxyHost != null && proxyHost.length > 0) {
            for (int i = 0; i < proxyHost.length; i++) {
                try {
                    return getURLConnection(new URL("HTTP", proxyHost[i], proxyPort[i], reqUrl));
                } catch (Exception e) {
                    Logger.info(this, "Exception : " + e.getMessage());
                    proxyException = e;
                }
            }
        } else { return getURLConnection(new URL(reqUrl)); }
        throw proxyException;
    }

    /**
     * return byte[] connected with Server.
     *
     * @param path the path for server url
     * @return byte[] binary received from server
     * @throws Exception the exception
     */
    public byte[] getBytes(String path) throws Exception {
        Log.printInfo("getBytes url = " + path);
        return URLRequestor.getBytes(proxyHost, proxyPort, path, null);
    }

    /**
     * return HttpURLConnection connected with Server.
     *
     * @param url the URL object
     * @return the HttpURLConnection
     * @throws Exception the exception
     */
    private HttpURLConnection getURLConnection(URL url) throws Exception {
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestProperty("content-encoding", "UTF-8");
        Log.printInfo("Connection response code = " + con.getResponseCode());
        return con;
    }
}

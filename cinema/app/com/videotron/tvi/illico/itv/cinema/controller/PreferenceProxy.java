package com.videotron.tvi.illico.itv.cinema.controller;

import java.rmi.RemoteException;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 * @version $Revision: 1.7 $ $Date: 2011/07/08 21:32:47 $
 * @author Pellos
 */
public final class PreferenceProxy implements PreferenceListener {

    /** Is a instance of PreferenceProxy. */
    private static PreferenceProxy instance  = null;

    /** Set value for storing data. */
    public static final String FAVORITE_CINEMA = "FAVORITE_CINEMA";
    /** The Constant TITLE_FIRST_LANG. */
    public static final String TITLE_FIRST_LANG = "CINEMA_FIRST_LANG";

    /** Gets data in UPP. */
    private String[] prefNames = {PreferenceNames.LANGUAGE, FAVORITE_CINEMA, TITLE_FIRST_LANG};
    /** The preference values in UPP. */
    private String[] prefValues = {Definitions.LANGUAGE_FRENCH, "", Definitions.LANGUAGE_FRENCH};

    /** is a instance of PreferenceService. */
    private PreferenceService prefSvc = null;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) { instance = new PreferenceProxy(); }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() { }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (prefSvc != null) {
            try {
                prefSvc.removePreferenceListener(Rs.APP_NAME, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            prefSvc = null;
        }
        instance = null;
    }

    /**
     * Look up User Profile and Preference.
     */
    public void addPreferenceListener() {
        Logger.info(this, "called addPreferenceListener()");
        try {
            prefSvc = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
            Logger.debug(this, "addPreferenceListener()-prefSvc : " + prefSvc);
            if (prefSvc != null) {
                String[] uppData = prefSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues);
                for (int i = 0; uppData != null && i < uppData.length; i++) {
                    Logger.debug(this, "addPreferenceListener()-uppData[] : " + uppData[i]);
                    if (i == 0) {
                        Rs.language = uppData[i];
                        DataCenter.getInstance().put(prefNames[i], uppData[i]);
                    } else if (i == 1) {
                        Log.printInfo("uppData[i] =" + uppData[i]);
                        StringTokenizer strValue = new StringTokenizer(uppData[i], "|");
                        while (strValue.hasMoreElements()) {
                            String str = strValue.nextToken();
                            Log.printInfo("favorite data =" + str);
                            StringTokenizer st = new StringTokenizer(str, "^");
                            TheatreData thData = new TheatreData();
                            //String[] theatreStr = {"", "", ""};
                            for (int j = 0; st.hasMoreElements() && j < 3; j++) {
                                String value = st.nextToken();
                                if (j == 0) {
                                    thData.setTheatreID(value);
                                } else  if (j == 1) {
                                    thData.setDefaultName(value);
                                } else { thData.setRegionID(value); }
                            }
                            Rs.vfavoriteTheatre.addElement(thData);
                            Rs.hfavoriteTheatre.put(thData.getTheatreID(), thData);
                        }
                    } else if (i == 2) {
                        Rs.titleFirstLanguage = uppData[i];
                        DataCenter.getInstance().put(prefNames[i], uppData[i]);
                        Log.printInfo("Rs.titleFirstLanguage = " + Rs.titleFirstLanguage);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Save a favorite Theatre or first language for title into UP.
     *
     * @param name the name
     * @param value the value
     */
    public void setPreferenceValue(String name, String value) {
        Logger.info(this, "called setPreferenceValue()-prefSvc : " + prefSvc);
        if (prefSvc != null) {
            try {
                Log.printInfo("name = " + name + " value = " + value);
                prefSvc.setPreferenceValue(name, value);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        Log.printDebug("called receiveUpdatedPreference()");
        if (name == null || value == null) { return; }
        Log.printInfo("name = " + name + ", value =" + value);
        if (PreferenceNames.LANGUAGE.equals(name)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
            Rs.language = value;
        }
    }
}

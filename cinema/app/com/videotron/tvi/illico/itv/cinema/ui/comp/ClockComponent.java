package com.videotron.tvi.illico.itv.cinema.ui.comp;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Date;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Formatter;

/**
 * This class draw the ClockComponent.
 * @version $Revision: 1.3 $ $Date: 2011/03/10 16:11:55 $
 * @author pellos
 */
public class ClockComponent extends Container implements ClockListener {

    /**
     * Is the serialVersionID.
     */
    private static final long serialVersionUID = 1L;

    /** clock image. */
    private Image imgClock = null;
    /** The img clock bg. */
    private Image imgClockBg = null;
    /**
     * Initialize.
     */
    public void init() {
        imgClock = DataCenter.getInstance().getImage("clock.png");
        imgClockBg = DataCenter.getInstance().getImage("clock_bg.png");
    }

    /**
     * start a clock.
     */
    public void start() {
        Clock.getInstance().addClockListener(this);
    }

    /**
     * stop a clock.
     */
    public void stop() {
        Clock.getInstance().removeClockListener(this);
    }

    /**
     * Graphics.
     *
     * @param g the Graphics
     */
    public void paint(Graphics g) {
        g.drawImage(imgClockBg, 749, 37, this);

        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(Rs.F17);
                g.setColor(Rs.C255255255);
                longDateWth = g.getFontMetrics().stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
            }
        }

        if (imgClock != null) {
            int imgClockWth = imgClock.getWidth(this);
            g.drawImage(imgClock, 910 - longDateWth - 4 - imgClockWth, 44, this);
        }
    }

    /**
     * update a clock.
     *
     * @param date the date
     */
    public void clockUpdated(Date date) {
        repaint();
    }
}

/*
 *  @(#)PrefSetting.java 1.0 2011.03.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui;

import java.awt.Point;
import java.awt.event.KeyEvent;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.controller.SceneManager;
import com.videotron.tvi.illico.itv.cinema.controller.PreferenceProxy;
import com.videotron.tvi.illico.itv.cinema.gui.PrefSettingRenderer;
import com.videotron.tvi.illico.itv.cinema.ui.BasicUI;
import com.videotron.tvi.illico.itv.cinema.ui.comp.ButtonIcons;
import com.videotron.tvi.illico.itv.cinema.ui.popup.SelectPopup;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>PrefSetting</code> The class to set preference datas.
 *
 * @since   2011.03.04
 * @version $Revision: 1.9 $ $Date: 2011/07/08 21:32:47 $
 * @author  tklee
 */
public class PrefSetting extends BasicUI {

    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** Is the ShowtimesInFavoriteRenderer instance. */
    private PrefSettingRenderer renderer = null;

    /**
     * Initialize the Scene.
     */
    protected void initScene() {
        if (renderer == null) {
            renderer = new PrefSettingRenderer();
            setRenderer(renderer);
            clickEffect = new ClickingEffect(this, 5);
        }
    }

    /**
     * Start the Scene.
     *
     * @param reset the reset
     */
    protected void startScene(boolean reset) {
        //buttonIcons.setButtonIds(new int[] {ButtonIcons.BUTTON_BACK});
        addFooterBtn(new int[] {ButtonIcons.BUTTON_BACK});

        prepare();
        if (reset) { renderer.menuFocus = 0; }
        startComp();
        setVisible(true);
    }

    /**
     * Stop the Scene.
     */
    protected void stopScene() {
        renderer.stop();
        if (optScr != null) { optScr.stop(); }
    }

    /**
     * Dispose scene.
     */
    protected void disposeScene() {
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        Log.printDebug("[TheatreSelect]keyAction." + keyCode);

        switch (keyCode) {
        case KeyEvent.VK_UP:
            if (renderer.menuFocus > 0) {
                renderer.menuFocus--;
                repaint();
            }
            break;
        case KeyEvent.VK_DOWN:
            int totalCount = renderer.menuTxt.length;
            if (renderer.menuFocus < totalCount - 1) {
                renderer.menuFocus++;
                repaint();
            }
            break;
        case HRcEvent.VK_ENTER:
            if (renderer.menuFocus == PrefSettingRenderer.MENU_LANG) {
                clickEffect.start(339, 127, 210, 32);
                selectPopup.setData(renderer, renderer.langTxt, renderer.langFocus, renderer.menuTxt[0], 3,
                        new Point(339 + 174, 70));
                //selectPopup.setLocation(339 + 174, 70);
                addPopup(selectPopup);
            } else {
                clickEffect.start(339, 127 + 55, 210, 32);
                Object[] values = {getCurrentSceneId(), null, null};
                SceneManager.getInstance().goToNextScene(SceneManager.SCENE_ID_FAVORITE, true, values);
            }
            break;
        case KeyCodes.LAST:
            footer.clickAnimation(btnName[ButtonIcons.BUTTON_BACK]);
            SceneManager.getInstance().goToNextScene(previousSceneID, false, null);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * called when pressed OK key by Popup.
     *
     * @param para the parameter
     */
    public void requestPopupOk(Object para) {
        if (popup instanceof SelectPopup) {
            int selectFocus = ((Integer) para).intValue();
            if (selectFocus != renderer.langFocus) {
                if (selectFocus == PrefSettingRenderer.LANG_FR) {
                    Rs.titleFirstLanguage = Definitions.LANGUAGE_FRENCH;
                } else { Rs.titleFirstLanguage = Definitions.LANGUAGE_ENGLISH; }
                renderer.langFocus = selectFocus;
                PreferenceProxy.getInstance().setPreferenceValue(PreferenceProxy.TITLE_FIRST_LANG,
                        Rs.titleFirstLanguage);
            }
            removePopup();
        } else { super.requestPopupOk(para); }
    }
}

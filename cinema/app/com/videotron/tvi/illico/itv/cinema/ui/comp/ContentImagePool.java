package com.videotron.tvi.illico.itv.cinema.ui.comp;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.itv.cinema.common.Logger;
import com.videotron.tvi.illico.itv.cinema.controller.DataManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.SharedDataKeys;
import java.awt.Component;
import java.awt.Container;
import java.awt.MediaTracker;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Hashtable;

import java.net.URL;

/**
 * An image pool. This class caches images by name.
 *
 *
 * @version $Revision: 1.2 $ $Date: 2012/02/15 04:09:58 $
 * @author tklee
 */
public class ContentImagePool {
    /** Class instance. */
    private static ContentImagePool instance = new ContentImagePool();
    /** Toolkit to decode image. */
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    
    /**
     * Instantiates a ContentImagePool.
     */
    private ContentImagePool() {}
    
    /**
     * Gets the single instance of ContentImagePool.
     *
     * @return single instance of ContentImagePool
     */
    public static synchronized ContentImagePool getInstance() {
        return instance;
    }
    
    /**
     * Creates Image with bytes source and store Image with specified key.
     *
     * @param data source bytes of image file.
     * @param key hashtable key for the this Image instance.
     * @return Image instance.
     */
    public Image createImage(byte[] data, String key) {
        Image img = toolkit.createImage(data);
        if (img == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ContentImagePool fail to create image : " + key);
            }
            return null;
        }
        int imgWidth = img.getWidth(null);
        int imgHeight = img.getHeight(null);
        Logger.debug(this, "getImage()-imgWidth : " + imgWidth);
        Logger.debug(this, "getImage()-imgHeight : " + imgHeight);
        Logger.debug(this, "getImage()-posterImg : " + img);
        if (imgWidth > 960 || imgHeight > 540) {
            img = img.getScaledInstance(221, 332, Image.SCALE_DEFAULT);
            Logger.debug(this, "getImage()-scaledPosterImg : " + img);
        }
        FrameworkMain.getInstance().getImagePool().put(key, img);
        return img;
    }
}


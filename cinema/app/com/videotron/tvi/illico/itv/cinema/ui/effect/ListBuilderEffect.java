/*
 *  @(#)ListBuilderEffect.java 1.0 2011.05.31
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.ui.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;

/**
 * <code>ListBuilderEffect</code> This class is effect util for builder on list.
 *
 * @since   2011.05.31
 * @version $Revision: 1.3 $ $Date: 2011/07/08 21:32:47 $
 * @author  tklee
 */
public class ListBuilderEffect extends Effect {
    /** The Constant TYPE_ADD. */
    public static final int EFFECT_ADD = 0;
    /** The Constant TYPE_REMOVE. */
    public static final int EFFECT_REMOVE = 1;

    /** The source rectangle. */
    private Rectangle leftSrc = null;
    /** The right source Rectangle. */
    private Rectangle rightSrc = null;
    /** The left from point. */
    private Point leftFrom = null;
    /** The left to point. */
    private Point leftTo = null;
    /** The right from point. */
    private Point rightFrom = null;
    /** The right to point. */
    private Point rightTo = null;
    /** The effect listener. */
    private EffectListener effectListener = null;

    /** The effect type. */
    private int effectType = 0;
    /** The left top position. */
    private int[] leftTop = null;
    /** The right top position. */
    private int[] rightTop = null;

    /**
     * Initialize the Component.
     *
     * @param c the Component
     * @param frameCount the frame count
     * @param listener the listener
     */
    public ListBuilderEffect(final Component c, final int frameCount, final EffectListener listener) {
        super(c, frameCount);
        leftTop = new int[frameCount];
        rightTop = new int[frameCount];
        effectListener = listener;
        updateBackgroundBeforeStart(false);
    }

    /**
     * Start effect.
     *
     * @param type the effect type
     * @param leftSrcRec the left source rectangle
     * @param rightSrcRec the right source rectangle
     * @param leftFromY the left from y point
     * @param leftToY the left to y point
     * @param rightFromY the right from y point
     * @param rightToY the right to y point
     */
    public void start(int type, Rectangle leftSrcRec, Rectangle rightSrcRec, Point leftFromY, Point leftToY,
            Point rightFromY, Point rightToY) {
        effectType = type;
        leftSrc = leftSrcRec;
        rightSrc = rightSrcRec;
        leftFrom = leftFromY;
        leftTo = leftToY;
        rightFrom = rightFromY;
        rightTo = rightToY;

        if (Log.DEBUG_ON) {
            Log.printDebug("ListBuilderEffect|start() -leftFrom = " + leftFrom);
            Log.printDebug("ListBuilderEffect|start() -leftTo = " + leftTo);
        }

        int leftDiffY = 0;
        int rightDiffY = 0;
        if (leftFrom != null) { leftDiffY = leftTo.y - leftFrom.y; }
        if (rightFrom != null) { rightDiffY = rightTo.y - rightFrom.y; }
        for (int i = 0; i < frameCount; i++) {
            if (leftFrom != null) {
                leftTop[i] = leftFrom.y + Math.round((float) (leftDiffY * i) / (frameCount - 1));
            }
            if (rightFrom != null) {
                rightTop[i] = rightFrom.y + Math.round((float) (rightDiffY * i) / (frameCount - 1));
            }
        }
        super.start();
    }

    /**
     * Display a animation.
     * @param g Graphics.
     * @param image DVBBufferedImage to display.
     * @param frame a current frame.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        Rectangle oldClip = g.getClipBounds();
        int width = leftSrc.width;
        int height = leftSrc.height;
        if (effectType == EFFECT_ADD) {
            if (leftFrom != null) {
                g.setClip(leftTo.x, 0, leftTo.x + width, leftTo.y + height);
                g.drawImage(image, leftTo.x, 0, leftTo.x + width, leftTo.y,
                        leftTo.x, 0, leftTo.x + width, leftTo.y, null);
                g.drawImage(image, leftTo.x, leftTop[frame], leftTo.x + width, leftTop[frame] + height,
                        leftSrc.x, leftSrc.y, leftSrc.x + width, leftSrc.y + height, null);
                g.setClip(oldClip);
            }
            width = rightSrc.width;
            height = rightSrc.height;
            if (rightFrom != null) {
                g.setClip(rightFrom.x, 0, rightFrom.x + width, rightFrom.y + height);
                g.drawImage(image, rightFrom.x, 0, rightFrom.x + width, rightFrom.y,
                        rightFrom.x, 0, rightFrom.x + width, rightFrom.y, null);
                g.drawImage(image, rightTo.x, rightTop[frame], rightTo.x + width, rightTop[frame] + height,
                        rightSrc.x, rightSrc.y, rightSrc.x + width, rightSrc.y + height, null);
                g.setClip(oldClip);
            }
        } else {
            if (leftFrom != null) {
                g.setClip(leftFrom.x, 0, leftFrom.x + width, leftFrom.y + height);
                g.drawImage(image, leftFrom.x, 0, leftFrom.x + width, leftFrom.y,
                        leftFrom.x, 0, leftFrom.x + width, leftFrom.y, null);
                g.drawImage(image, leftTo.x, leftTop[frame], leftTo.x + width, leftTop[frame] + height,
                        leftSrc.x, leftSrc.y, leftSrc.x + width, leftSrc.y + height, null);
                g.setClip(oldClip);
            }
            width = rightSrc.width;
            height = rightSrc.height;
            if (rightFrom != null) {
                g.setClip(rightTo.x, 0, rightTo.x + width, rightTo.y + height);
                g.drawImage(image, rightTo.x, 0, rightTo.x + width, rightTo.y,
                        rightTo.x, 0, rightTo.x + width, rightTo.y, null);
                g.drawImage(image, rightTo.x, rightTop[frame], rightTo.x + width, rightTop[frame] + height,
                        rightSrc.x, rightSrc.y, rightSrc.x + width, rightSrc.y + height, null);
                g.setClip(oldClip);
            }
        }
        if (leftFrom == null) {
            g.drawImage(image, leftSrc.x, leftSrc.y, leftSrc.x + leftSrc.width, leftSrc.y + leftSrc.height,
                    leftSrc.x, leftSrc.y, leftSrc.x + leftSrc.width, leftSrc.y + leftSrc.height, null);
        }
        if (effectListener != null) { effectListener.requestPaint(g, component); }
    }
}

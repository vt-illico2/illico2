/*
 *  @(#)PrefSettingRenderer.java 1.0 2011.03.04
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.TheatreData;
import com.videotron.tvi.illico.itv.cinema.ui.popup.Popup;
import com.videotron.tvi.illico.ixc.upp.Definitions;

/**
 * <code>PrefSettingRenderer</code> The class to paint a GUI for the preferecne setting menu.
 *
 * @since   2011.03.03
 * @version $Revision: 1.5 $ $Date: 2011/06/24 23:21:24 $
 * @author  tklee
 */
public class PrefSettingRenderer extends BasicRenderer {
    /** The Constant MENU_LANG. */
    public static final int MENU_LANG = 0;
    /** The Constant MENU_FAVOR. */
    public static final int MENU_FAVOR = 1;

    /** The Constant LANG_FR. */
    public static final int LANG_FR = 0;
    /** The Constant LANG_EN. */
    public static final int LANG_EN = 1;

    /** The bg line img. */
    private Image bgLineImg = null;
    /** The sc line img. */
    private Image scLineImg = null;
    /** The top shadow img. */
    private Image topShadowImg = null;
    /** The input img. */
    private Image inputImg = null;
    /** The input focus img. */
    private Image inputFocusImg = null;
    /** The inbox img. */
    private Image inboxImg = null;
    /** The inbox focus img. */
    private Image inboxFocusImg = null;
    /** The input focus dim img. */
    private Image inputFocusDimImg = null;

    /** The title txt. */
    private String titleTxt = null;
    /** The menu txt. */
    public String[] menuTxt = new String[2];
    /** The language txt. */
    public String[] langTxt = new String[2];
    /** The favorrites txt. */
    private String favorTxt = null;

    /** The menu focus. */
    public int menuFocus = 0;
    /** The lang focus. */
    public int langFocus = 0;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgLineImg = getImage("08_bg_line.png");
        scLineImg = getImage("11_sc_line.png");
        topShadowImg = getImage("11_top_sh.png");
        inputImg = getImage("input_210_sc.png");
        inputFocusImg = getImage("input_210_foc2.png");
        inputFocusDimImg = getImage("input_210_foc_dim.png");
        inboxImg = getImage("inbox_210.png");
        inboxFocusImg = getImage("focus_05_05.png");
        super.prepare(c);

        titleTxt = DataCenter.getInstance().getString("cine.Preferences");
        menuTxt[0] = DataCenter.getInstance().getString("cine.prefResultFirst");
        menuTxt[1] = DataCenter.getInstance().getString("cine.favTheatres");
        langTxt[0] = DataCenter.getInstance().getString("cine.French");
        langTxt[1] = DataCenter.getInstance().getString("cine.English");
        favorTxt = DataCenter.getInstance().getString("cine.Favorites");

        langFocus = 0;
        if (Definitions.LANGUAGE_ENGLISH.equals(Rs.titleFirstLanguage)) { langFocus = LANG_EN; }
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("01_hotkeybg.png");
        removeImage("08_bg_line.png");
        removeImage("11_sc_line.png");
        removeImage("11_top_sh.png");
        removeImage("input_210_sc.png");
        removeImage("input_210_foc2.png");
        removeImage("inbox_210.png");
        removeImage("focus_05_05.png");
        removeImage("input_210_foc_dim.png");*/
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        g.drawImage(hotkeybgImg, 236, 466, 683, 17, c);
        g.drawImage(topShadowImg, 0, 77, c);
        g.drawImage(bgLineImg, 0, 167, c);
        Vector favoriteTheatre = Rs.vfavoriteTheatre;
        for (int i = 0; i < favoriteTheatre.size(); i++) {
            g.drawImage(scLineImg, 344, 257 + (i * 35), c);
        }
        g.setFont(Rs.F26);
        g.setColor(Rs.C255255255);
        g.drawString(titleTxt, 55, 113);

        String[] name = {langTxt[langFocus], favoriteTheatre.size() + " " + favorTxt};
        Image[][] menuImage = {{inputImg, inputFocusImg}, {inboxImg, inboxFocusImg}};
        for (int i = 0; i < name.length; i++) {
            int addPos = (i * 55);
            g.setFont(Rs.F17);
            g.setColor(Rs.C210);
            g.drawString(menuTxt[i], 56, 147 + addPos);
            g.setColor(Rs.C180);
            int imgFocus = 0;
            if (i == menuFocus) {
                imgFocus = 1;
                if (i == MENU_LANG) {
                    g.setColor(Rs.DVB90C255);
                } else { g.setColor(Rs.C000000000); }
            }
            g.drawImage(menuImage[i][imgFocus], 339, 127 + addPos, c);
            g.setFont(Rs.F18);
            g.drawString(name[i], 352, 148 + addPos);
        }

        g.setFont(Rs.F18);
        g.setColor(Rs.C160);
        for (int i = 0; i < favoriteTheatre.size() && i < 5; i++) {
            //String[] data = (String[]) favoriteTheatre.elementAt(i);
            TheatreData thData = (TheatreData) favoriteTheatre.elementAt(i);
            int addPos = (i * 35);
            g.drawString((i + 1) + ". " + thData.getDefaultName(), 352, 245 + addPos);
        }
        super.paint(g, c);
    }

    /**
     * Paint selected item.
     *
     * @param g the g
     * @param popup the popup
     */
    public void paintSelectedItem(Graphics g, Popup popup) {
        g.setFont(Rs.F17);
        g.setColor(Rs.C210);
        g.drawString(menuTxt[0], 56, 147);
        g.drawImage(inputFocusDimImg, 339, 127, popup);
        g.setColor(Rs.DVB90C255);
        g.drawString(langTxt[langFocus], 352, 148);
    }
}

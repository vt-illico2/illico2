/*
 *  @(#)BasicRenderer.java 1.0 2011.02.23
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.ui.popup.Popup;

/**
 * <code>BasicRenderer</code> This class the basic renderer class for each renderer.
 * Each renderder has to inherit this class as super class.
 * @since   2011.02.23
 * @version $Revision: 1.3 $ $Date: 2011/04/19 21:12:36 $
 * @author  tklee
 */
public class BasicRenderer extends Renderer {
    /** Log image. */
    //private static Image appLogoImg = null;
    /** The grade img name. */
    private static final String[] NAME_GRADE_IMG = {"icon_13.png", "icon_16.png", "icon_18.png", "icon_g.png"};
    /** Grade Image. */
    private static Image[] gradeImgs = null;
    /** Background image. */
    protected static Image bgImg = null;
    /** The hotkeybg. */
    public static Image hotkeybgImg = null;

    /**
     * Instantiates a new basic renderer.
     */
    public BasicRenderer() {
        //if (appLogoImg == null) { appLogoImg = getImage("app_logo_cinema.png"); }
    }

    /**
     * Gets the string value of UI from DataCenter. If the value is null, return a empty string as "".
     *
     * @param key the key
     * @return the string
     */
    public String getString(String key) {
        String value = DataCenter.getInstance().getString(key);
        if (value != null) { return value; }
        return "";
    }

    /**
     * Gets the image from DataCenter.
     *
     * @param imageName the image name
     * @return the image
     */
    public Image getImage(String imageName) {
        return DataCenter.getInstance().getImage(imageName);
    }

    /**
     * remove the image from DataCenter.
     *
     * @param imageName the image name
     */
    public void removeImage(String imageName) {
        DataCenter.getInstance().removeImage(imageName);
    }

    /**
     * Gets the grade image.
     *
     * @param classement the classement for rating
     * @return the grade image
     */
    protected Image getGradeImage(String classement) {
        if (classement != null && gradeImgs != null) {
            if (classement.equals(FilmData.THIRTEEN_PLUS)) {
                return gradeImgs[0];
            } else if (classement.equals(FilmData.SIXTEEN_PLUS)) {
                return gradeImgs[1];
            } else if (classement.equals(FilmData.EIGHTEEN_PLUS)) {
                return gradeImgs[2];
            } else if (classement.equals(FilmData.ALL)) { return gradeImgs[3]; }
        }
        return null;
    }

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the UIComponent
     */
    public void prepare(UIComponent c) {
        if (gradeImgs == null) {
            gradeImgs = new Image[NAME_GRADE_IMG.length];
            for (int i = 0; i < gradeImgs.length; i++) {
                gradeImgs[i] = getImage(NAME_GRADE_IMG[i]);
            }
            bgImg = getImage("bg.jpg");
            hotkeybgImg = getImage("01_hotkeybg.png");
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Get the renderer bound of UI .
     *
     * @param c the UIComponent
     * @return the preferred bounds
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        //g.drawImage(appLogoImg, 57, 482, c);
    }

    /**
     * Paint selected item.
     *
     * @param g the g
     * @param popup the popup
     */
    public void paintSelectedItem(Graphics g, Popup popup) { }
}

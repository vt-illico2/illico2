package com.videotron.tvi.illico.itv.cinema.common;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.Hashtable;
import java.util.Vector;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of cinema.
 * @version $Revision: 1.34 $ $Date: 2017/01/04 20:44:06 $
 * @author Pellos
 */
public final class Rs {
    /** Application Version. */
    public static final String APP_VERSION = "R4_1.1.0";//"1.0.4.0";
    /** Application Name. */
    public static final String APP_NAME = "Cinema";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;

    /** It is a application language that is received from UPP. */
    public static String language = Definitions.LANGUAGE_FRENCH;
    /** The title first language. */
    public static String titleFirstLanguage = Definitions.LANGUAGE_FRENCH;
    /** The favorite theatre data in Vector. */
    public static Vector vfavoriteTheatre = new Vector();
    /** The favorite theatre data in Hashtable. */
    public static Hashtable hfavoriteTheatre = new Hashtable();

    /** The Constant F15. */
    public static final Font F15 = FontResource.BLENDER.getFont(15);
    /** The Constant F16. */
    public static final Font F16 = FontResource.BLENDER.getFont(16);
    /** The Constant F17. */
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    /** The Constant F18. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    /** The Constant F19. */
    public static final Font F19 = FontResource.BLENDER.getFont(19);
    /** The Constant F20. */
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    /** The Constant F21. */
    public static final Font F21 = FontResource.BLENDER.getFont(21);
    /** The Constant F23. */
    public static final Font F23 = FontResource.BLENDER.getFont(23);
    /** The Constant F24. */
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    /** The Constant F26. */
    public static final Font F26 = FontResource.BLENDER.getFont(26);
    /** The Constant F28. */
    public static final Font F28 = FontResource.BLENDER.getFont(28);
    /** The Constant F14_D. */
    public static final Font F14_D = FontResource.DINMED.getFont(14);
    /** The Constant F13_D. */
    public static final Font F13_D = FontResource.DINMED.getFont(13);
    /** The Constant F14_D. */
    public static final Font F15_D = FontResource.DINMED.getFont(15);

    /** The Constant C000000000. */
    public static final Color C000000000 = new Color(0, 0, 0);
    /** The Constant C003003003. */
    public static final Color C003003003 = new Color(3, 3, 3);
    /** The Constant C035035035. */
    public static final Color C035035035 = new Color(35, 35, 35);
    /** The Constant C046046045. */
    public static final Color C046046045 = new Color(46, 46, 45);
    /** The Constant C055055055. */
    public static final Color C055055055 = new Color(55, 55, 55);
    /** The Constant C133133133. */
    public static final Color C133133133 = new Color(133, 133, 133);
    /** The Constant C143143143. */
    public static final Color C143143143 = new Color(143, 143, 143);
    /** The Constant C182182182. */
    public static final Color C182182182 = new Color(182, 182, 182);
    /** The Constant C184184184. */
    public static final Color C184184184 = new Color(184, 184, 184);
    /** The Constant C187189192. */
    public static final Color C187189192 = new Color(187, 189, 192);
    /** The Constant C214181061. */
    public static final Color C214181061 = new Color(214, 181, 61);
    /** The Constant C230230230. */
    public static final Color C230230230 = new Color(230, 230, 230);
    /** The Constant C241241241. */
    public static final Color C241241241 = new Color(241, 241, 241);
    /** The Constant C255204000. */
    public static final Color C255204000 = new Color(255, 204, 0);
    /** The Constant C252202004. */
    public static final Color C252202004 = new Color(252, 202, 4);
    /** The Constant C255255255. */
    public static final Color C255255255 = new Color(255, 255, 255);
    /** The Constant C236236236. */
    public static final Color C236236236 = new Color(236, 236, 236);
    /** The Constant C124124124. */
    public static final Color C124124124 = new Color(124, 124, 124);
    /** The Constant C224224224. */
    public static final Color C224224224 = new Color(224, 224, 224);
    /** The Constant C239. */
    public static final Color C239 = new Color(239, 239, 239);
    /** The Constant C027024012. */
    public static final Color C027024012 = new Color(27, 24, 12);
    /** The Constant C214182055. */
    public static final Color C214182055 = new Color(214, 182, 55);
    /** The Constant C193191191. */
    public static final Color C193191191 = new Color(193, 191, 191);
    /** The Constant C074072064. */
    public static final Color C074072064 = new Color(74, 72, 64);
    /** The Constant C255205012. */
    public static final Color C255205012 = new Color(255, 205, 12);
    /** The Constant C200. */
    public static final Color C200 = new Color(200, 200, 200);
    /** The Constant C210. */
    public static final Color C210 = new Color(210, 210, 210);
    /** The Constant C180. */
    public static final Color C180 = new Color(180, 180, 180);
    /** The Constant C160. */
    public static final Color C160 = new Color(160, 160, 160);
    /** The Constant C110. */
    public static final Color C110 = new Color(110, 110, 110);
    /** The Constant C1. */
    public static final Color C1 = new Color(1, 1, 1);
    /** The Constant C78. */
    public static final Color C78 = new Color(78, 78, 78);
    /** The Constant C81. */
    public static final Color C81 = new Color(81, 81, 81);
    /** The Constant C202174097. */
    public static final Color C202174097 = new Color(202, 174, 97);
    /** The Constant C254196014. */
    public static final Color C254196014 = new Color(254, 196, 14);
    /** The Constant C192. */
    public static final Color C192 = new Color(192, 192, 192);

    /** The Constant DVB102102102179. */
    public static final Color DVB102102102179 = new DVBColor(102, 102, 102, 179);
    /** The Constant DVB80C12. */
    public static final Color DVB80C12 = new DVBColor(12, 12, 12, 255 * 80 / 100);
    /** The Constant DVBC78. */
    public static final Color DVBC78 = new DVBColor(78, 78, 78, 255);
    /** The Constant DVB90C255. */
    public static final Color DVB90C255 = new DVBColor(255, 255, 255, 255 * 90 / 100);
    /** The Constant DVB40C255. */
    public static final Color DVB40C255 = new DVBColor(255, 255, 255, 255 * 40 / 100);
    /** The Constant DVB30C7. */
    public static final Color DVB30C7 = new DVBColor(7, 7, 7, 255 * 30 / 100);

    /** ******************************************************************************* Key Code-related *******************************************************************************. */
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    /** The Constant KEY_PAGEUP. */
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    /** The Constant KEY_CH_UP. */
    public static final int KEY_CH_UP = OCRcEvent.VK_CHANNEL_UP;
    /** The Constant KEY_CH_DOWN. */
    public static final int KEY_CH_DOWN = OCRcEvent.VK_CHANNEL_DOWN;

    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;

    /** Screen Size. */
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);

    /** It is a separator for http connection. */
    public static final String SALLESREGION = "sallesRegion/";
    /** The Constant HORIRESCINEMA. */
    public static final String HORIRESCINEMA = "horairesCinema/";
    /** The Constant RECENTMOVIE. */
    public static final String RECENTMOVIE = "recentMovie/";
    /** The Constant FILMSALL. */
    public static final String FILMSALL = "filmsLettre/";
    /** The Constant FILMSLETTRE. */
    public static final String FILMSLETTRE = "filmsLettre/letter";
    /** The Constant FICHEFILM. */
    public static final String FICHEFILM = "ficheFilm/";
    /** The Constant HORAIRESALLES. */
    public static final String HORAIRESALLES = "horaireSalles/";

    /**
     * Instantiates a contructor.
     */
    private Rs() { }
}

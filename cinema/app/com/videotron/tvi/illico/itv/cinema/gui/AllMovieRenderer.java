package com.videotron.tvi.illico.itv.cinema.gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.cinema.common.Rs;
import com.videotron.tvi.illico.itv.cinema.data.FilmData;
import com.videotron.tvi.illico.itv.cinema.data.FilmInfoData;
import com.videotron.tvi.illico.itv.cinema.ui.AllMovie;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class is the AllMovie Renderer.
 * @version $Revision: 1.11 $ $Date: 2011/06/07 02:08:24 $
 * @author tklee
 */

public class AllMovieRenderer extends BasicRenderer {
    /** The bg line. */
    private Image bgLineImg = null;
    /** The menu1 bg. */
    private Image menu1BgImg = null;
    /** The menu2 bg. */
    private Image menu2BgImg = null;
    /** The menu shadow. */
    private Image menuShadowImg = null;
    /** The menu grow. */
    private Image menuGrowImg = null;
    /** The menu line. */
    private Image menuLineImg = null;
    /** The menu arrow icon. */
    private Image menuArrIconImg = null;
    /** The menu focus. */
    private Image menuFocusImg = null;
    /** The poster focus. */
    public Image posterFocusImg = null;
    /** The poser focus dim image. */
    public Image poserFocusDimImg = null;
    /** The arrow left. */
    public Image arrLImg = null;
    /** The arrow right. */
    public Image arrRImg = null;
    /** The poster shadow. */
    public Image posterShadowImg = null;
    /** The poster dim. */
    public Image posterDimImg = null;
    /** The poster list shadow left. */
    public Image posterListShadowLImg = null;
    /** The poster list shadow right. */
    public Image posterListShadowRImg = null;
    /** The scroll shadow. */
    public Image scrollShadowImg = null;
    /** The scroll shadow up img. */
    public Image scrollShadowUpImg = null;
    /** The en icon. */
    private Image enIconImg = null;
    /** The fr icon. */
    private Image frIconImg = null;
    /** The default poster img. */
    public Image defaultPosterImg = null;
    /** The default focus poster img. */
    public Image defaultFocusPosterImg = null;

    /** Scene Title String. */
    //private String sceneTitle = getString("cine.title");
    /** Movie title String. */
    private String title = null;
    /** The sort fr txt. */
    private String sortFrTxt = null;
    /** The sort en txt. */
    private String sortEnTxt = null;
    /** The sort alphabetical txt. */
    private String sortAlphaTxt = null;
    /** The menu txt. */
    public String[] menuTxt = new String[2];
    /** Release String. */
    private String releaseDateTxt = null;
    /** Actors String. */
    private String actors = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgLineImg = getImage("01_vod_bgline.png");
        menu1BgImg = getImage("01_acbg_1st.png");
        menu2BgImg = getImage("01_acbg_2nd.png");
        menuShadowImg = getImage("01_ac_shadow.png");
        menuGrowImg = getImage("01_acgrow.png");
        menuLineImg = getImage("01_acline.png");
        menuArrIconImg = getImage("02_detail_ar.png");
        menuFocusImg = getImage("02_detail_bt_foc.png");
        posterFocusImg = getImage("01_nor_foc_v.png");
        poserFocusDimImg = getImage("01_nor_foc_v_dim.png");
        arrLImg = getImage("01_arrow_l.png");
        arrRImg = getImage("01_arrow_r.png");
        posterShadowImg = getImage("01_possh_96.png");
        posterDimImg = getImage("01_poshigh_96.png");
        posterListShadowLImg = getImage("01_poster_sha_l.png");
        posterListShadowRImg = getImage("01_poster_sha_r.png");
        scrollShadowImg = getImage("01_des_sh_bun.png");
        scrollShadowUpImg = getImage("01_des_sh_t01.png");
        enIconImg = getImage("icon_en.png");
        frIconImg = getImage("icon_fr.png");
        defaultPosterImg = getImage("11_default_96.png");
        defaultFocusPosterImg = getImage("11_default_144.png");
        super.prepare(c);

        //sceneTitle = getString("cine.title");
        title = getString("cine.Allmovies");
        sortFrTxt = getString("cine.SortFR");
        sortEnTxt = getString("cine.SortEN");
        menuTxt[0] = getString("cine.selectThea");
        menuTxt[1] = getString("cine.moreInfo");
        releaseDateTxt = DataCenter.getInstance().getString("cine.releaseDate");
        actors = DataCenter.getInstance().getString("cine.Actors");
        sortAlphaTxt = DataCenter.getInstance().getString("cine.SortAlpa");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() {
        /*removeImage("bg.jpg");
        removeImage("01_vod_bgline.png");
        removeImage("01_acbg_1st.png");
        removeImage("01_acbg_2nd.png");
        removeImage("01_ac_shadow.png");
        removeImage("01_acgrow.png");
        removeImage("01_acline.png");
        removeImage("02_detail_ar.png");
        removeImage("02_detail_bt_foc.png");
        removeImage("01_nor_foc_v.png");
        removeImage("01_arrow_l.png");
        removeImage("01_arrow_r.png");
        removeImage("01_possh_96.png");
        removeImage("01_poshigh_96.png");
        removeImage("01_poster_sha_l.png");
        removeImage("01_poster_sha_r.png");
        removeImage("01_des_sh_bun.png");
        removeImage("icon_en.png");
        removeImage("icon_fr.png");
        removeImage("01_hotkeybg.png");
        for (int i = 0; i < gradeImgName.length; i++) {
            removeImage(gradeImgName[i]);
        }*/
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        AllMovie ui = (AllMovie) c;
        Vector filmList = ui.filmList;
        if (filmList == null) { return; }
        g.drawImage(bgLineImg, 0, 250, c);
        FilmData focusFilm = (FilmData) filmList.elementAt(ui.posterFocus);
        FilmInfoData filmInfo = null;
        if (focusFilm != null) { filmInfo = focusFilm.getFilmInfoData(); }
        String str = "";
        int pos = 0;
      //poster
        /*int totalCount = filmList.size();
        int initNumber = 0;
        int halfPos = posterPerPage / 2;
        //int focusPos = halfPos + (posterPerPage % 2);
        if (totalCount >= posterPerPage) {
            initNumber =  posterFocus - halfPos;
            if (posterFocus < halfPos) {  initNumber = totalCount - (halfPos - posterFocus); }
        } else {
            if (posterFocus > halfPos) { initNumber = posterFocus - halfPos; }
        }
        int posterGap = 103;
        for (int i = initNumber, j = 0; j < posterPerPage; j++) {
            if (j == halfPos) {
                if (i > totalCount - 1) { i = 0; }
                i++;
                continue;
            } else if (totalCount < posterPerPage) {
                if (j < halfPos && halfPos - posterFocus > j) {
                    continue;
                } else if (i > totalCount - 1) { break; }
            }
            if (i > totalCount - 1) { i = 0; }
            FilmData film = (FilmData) filmList.elementAt(i);
            Image posterImage = null;
            if (film != null && film.getFilmInfoData() != null) {
                posterImage = film.getFilmInfoData().getPosterImage();
            }
            if (posterImage == null) { posterImage = defaultPosterImg; }
            if (posterImage != null) {
                int posterPos = (78 - posterGap) + (j * posterGap);
                if (j > halfPos) { posterPos = 580 + ((j - halfPos - 1) * posterGap); }
                g.drawImage(posterImage, posterPos, 132, 96, 144, c);
                g.drawImage(posterDimImg, posterPos, 132, 96, 141, c);
                g.drawImage(posterShadowImg, posterPos - 11, 274, c);
            }
            i++;
        }

        if (!isRightFocus) {
            g.drawImage(posterFocusImg, 405, 104, c);
            if (totalCount >= posterPerPage || posterFocus > 0) { g.drawImage(arrLImg, 385, 191, c); }
            if (totalCount >= posterPerPage || posterFocus < totalCount - 1) { g.drawImage(arrRImg, 557, 191, c); }
        } else { g.drawImage(poserFocusDimImg, 405, 104, c); }
        g.drawImage(posterListShadowLImg, 0, 6, c);
        g.drawImage(posterListShadowRImg, 856, 6, c);
        //g.drawImage(hotkeybgImg, 277, 466, c);
        FilmData focusFilm = (FilmData) filmList.elementAt(posterFocus);
        FilmInfoData filmInfo = null;
        Image posterImage = null;
        if (focusFilm != null) {
            filmInfo = focusFilm.getFilmInfoData();
            if (filmInfo != null) { posterImage = filmInfo.getPosterImage(); }
        }
        if (posterImage == null) { posterImage = defaultFocusPosterImg; }
        if (posterImage != null) { g.drawImage(posterImage, 408, 107, 144, 216, c); }
        int addWidth = 0;
        if (totalCount >= 100) { addWidth = 15; }
        g.setColor(Rs.C252202004);
        if (isRightFocus) { g.setColor(Rs.C192); }
        g.fillRect(505 - addWidth, 306, 47 + addWidth, 17);
        g.setFont(Rs.F16);
        g.setColor(Rs.C074072064);
        String str = String.valueOf(totalCount);
        if (totalCount < 10) { str = "0" + str; }
        str = "/" + str;
        //g.drawString(str, 549, 320);
        GraphicUtil.drawStringRight(g, str, 549, 320);
        int pos = 549 - g.getFontMetrics().stringWidth(str);
        g.setColor(Rs.C000000000);
        str = String.valueOf(posterFocus + 1);
        if (posterFocus < 9) { str = "0" + str; }
        GraphicUtil.drawStringRight(g, str, pos, 320);

        g.setFont(Rs.F23);
        g.setColor(Rs.C255205012);
        GraphicUtil.drawStringCenter(g, focusFilm.getTitle(), 480, 348);
*/
        //title
        g.setColor(Rs.C255255255);
        g.setFont(Rs.F26);
        g.drawString(title, 61, 98);
        g.setFont(Rs.F18);
        if (ui.sortType == FilmData.SORT_LANG) {
            str = sortFrTxt;
            if (Rs.titleFirstLanguage.equals(Definitions.LANGUAGE_ENGLISH)) { str = sortEnTxt; }
        } else { str = sortAlphaTxt; }

        GraphicUtil.drawStringRight(g, str, 908, 95);

        g.setColor(Rs.C200);
        g.setFont(Rs.F16);
        FontMetrics fm = g.getFontMetrics();
        str = focusFilm.getGenre();
        if (str == null) { str = ""; }
        String temp = focusFilm.getDuration();
        if (temp != null && temp.length() > 0) { str += " | " + temp; }
        pos = 61 ;
        String filmLang = focusFilm.getLangue();
        Image langIcon = null;
        if (filmLang.equals(FilmData.LANGUAGE_FRENCH)) {
            langIcon = frIconImg;
        } else if (filmLang.equals(FilmData.LANGUAGE_ENGLISH)) { langIcon = enIconImg; }
        Image gradeImg = getGradeImage(focusFilm.getClassement());
        if (langIcon != null || gradeImg != null) {
            if (str.length() > 0) {
                str += " | ";
                pos += fm.stringWidth(str);
            }
            if (langIcon != null) {
                g.drawImage(langIcon, pos, 355, c);
                pos += 28;
            }
            if (gradeImg != null) { g.drawImage(gradeImg, pos, 355, c); }
        }
        g.drawString(str, 61, 366);

        str = releaseDateTxt + " : ";
        if (filmInfo != null) { str += filmInfo.getReleaseDate(); }
        g.drawString(str, 61, 384);
        g.setColor(Rs.C182182182);
        g.drawString(actors + " :", 61, 408);
        if (filmInfo != null) {
            Vector actorList = filmInfo.getActors();
            str = "";
            for (int i = 0; actorList != null && i < actorList.size(); i++) {
                if (i != 0) { str += ", "; }
                str += actorList.elementAt(i);
            }
            g.setFont(Rs.F13_D);
            g.setColor(Rs.C255255255);
            g.drawString(TextUtil.shorten(str, g.getFontMetrics(), 582 - 116), 116, 408);
        }

      //menu
        paintMenu(g, c);
        super.paint(g, c);
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paintMenu(Graphics g, UIComponent c) {
        AllMovie ui = (AllMovie) c;
        g.drawImage(menu1BgImg, 626, 397, c);
        g.drawImage(menu2BgImg, 626, 440, c);
        g.drawImage(menuLineImg, 641, 437, c);
        g.drawImage(menuGrowImg, 625, 455, c);
        g.drawImage(menuShadowImg, 579, 467, c);
        for (int i = 0; i < menuTxt.length; i++) {
            int addedPos = i * 39;
            g.drawImage(menuArrIconImg, 649, 415 + addedPos, c);
            if (ui.isRightFocus && i == ui.menuFocus) {
                g.drawImage(menuFocusImg, 624, 397 + addedPos, c);
                g.setColor(Rs.C003003003);
                g.setFont(Rs.F21);
            } else {
                g.setColor(Rs.C230230230);
                g.setFont(Rs.F18);
            }
            g.drawString(menuTxt[i], 671, 423 + addedPos);
        }
    }
}

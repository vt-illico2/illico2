package com.videotron.tvi.illico.itv.cinema.data;


/**
 * The Class RecentFilmData.
 * @version $Revision: 1.5 $ $Date: 2011/03/21 16:13:16 $
 * @author pellos
 */
public class RecentFilmData implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /** The ID. */
    private String ID;

    /** The title. */
    private String title;

    /** The vfde. */
    private String vfde;

    /** The photo url. */
    private String photoURL;

    /** The poster url. */
    private String posterURL;

    /** The primeur. */
    private int primeur;

    /** The lang type. */
    private String langType = "";

    /**
     * @return the langType
     */
    public String getLangType() {
        return langType;
    }

    /**
     * @param type the langType to set
     */
    public void setLangType(String type) {
        this.langType = type;
    }

    /**
     * Gets the iD.
     * @return the iD
     */
    public String getID() {
        return ID;
    }

    /**
     * Sets the iD.
     * @param iD the new iD
     */
    public void setID(String iD) {
        ID = iD;
    }

    /**
     * Gets the title.
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title.
     * @param title the new title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the vfde.
     * @return the vfde
     */
    public String getVfde() {
        return vfde;
    }

    /**
     * Sets the vfde.
     * @param vfde the new vfde
     */
    public void setVfde(String vfde) {
        this.vfde = vfde;
    }

    /**
     * Gets the photo url.
     * @return the photo url
     */
    public String getPhotoURL() {
        return photoURL;
    }

    /**
     * Sets the photo url.
     * @param photoURL the new photo url
     */
    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    /**
     * Gets the poster url.
     * @return the poster url
     */
    public String getPosterURL() {
        return posterURL;
    }

    /**
     * Sets the poster url.
     * @param posterURL the new poster url
     */
    public void setPosterURL(String posterURL) {
        this.posterURL = posterURL;
    }

    /**
     * Gets the primeur.
     * @return the primeur
     */
    public int getPrimeur() {
        return primeur;
    }

    /**
     * Sets the primeur.
     * @param primeur the new primeur
     */
    public void setPrimeur(int primeur) {
        this.primeur = primeur;
    }
}

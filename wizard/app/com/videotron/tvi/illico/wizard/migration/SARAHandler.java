/*
 *  @(#)SARAHandler.java 1.0 2011.03.15
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.wizard.migration;

import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.locator.Locator;
import javax.tv.locator.LocatorFactory;

import org.dvb.application.AppID;
import org.ocap.dvr.OcapRecordingProperties;
import org.ocap.net.OcapLocator;
import org.ocap.storage.ExtendedFileAccessPermissions;

import com.videotron.migration.IllicoRecordingEntry;
import com.videotron.migration.IllicoScheduledRecordingEntry;
import com.videotron.migration.SaraPreference;
import com.videotron.migration.SaraRecordingEntry;
import com.videotron.migration.SaraScheduledRecordingEntry;
import com.videotron.migration.Upgrade;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.migration.communication.IXCManager;

/**
 * <code>SARAHandler</code> This class is to access SARA API on the Cisco box.
 *
 * @since   2011.03.15
 * @version $Revision: 1.18 $ $Date: 2017/01/09 21:08:33 $
 * @author  tklee
 */
public class SARAHandler {
    /** The theme value. */
    //private final int[] themeValue = {0, 15, 6, 8, 13, 16, 16, 17, 23, 24, 36, 26, 34, 5, 4, 14, 22, 11, 19, 18};
    /** The rating index. */
    private final int[] ratingIndex = {Constants.RATING_INDEX_8, Constants.RATING_INDEX_ALL, Constants.RATING_INDEX_G,
            Constants.RATING_INDEX_8, Constants.RATING_INDEX_G, Constants.RATING_INDEX_8, Constants.RATING_INDEX_13,
            Constants.RATING_INDEX_13, Constants.RATING_INDEX_16, Constants.RATING_INDEX_18, Constants.RATING_INDEX_16};
    /** The save days for schedule. */
    private final int[] saveDaysForSchedule = {1, 2, 7, 14, -1};
    /** The pvr app id. */
    private final AppID pvrAppID = new AppID(1, 8352);
    /**
     * The text of repeat option for illico2.
     * 0 Once, 1 Every Sunday, 2 Every Monday, 3 Every Tuesday, 4 Every Wednesday, 5 Every Thursday, 6 Every Friday,
     * 7 Every Saturday, 8 Weekends, 9 Weekdays, 10 Daily
     */
    private final String[] repeatText = {null, "0000001", "1000000", "0100000", "0010000", "0001000", "0000100",
            "0000010", "0000011", "1111100", "1111111"};
    /** The retry count. */
    private final int retryCount = 2;

    /** The up data for setting. */
    private Hashtable upData = new Hashtable();
    /** The sara recording entry. */
    private SaraRecordingEntry[] saraRecEntry = null;
    /** The sara scheduled recording entry. */
    private SaraScheduledRecordingEntry[] saraScheEntry = null;
    /** The illico recording entry. */
    private IllicoRecordingEntry[] illicoRecEntry = null;
    /** The illico schedule recording entry. */
    private IllicoScheduledRecordingEntry[] illicoScheRecEntry = null;
    /** The SaraPreference instance. */
    private SaraPreference saraPref = null;

    /**
     * Check if this box need to upgrade.
     *
     * @return true if upgrade is needed
     * @throws Throwable the throwable
     */
    public boolean needUpgrade() throws Throwable {
        return Upgrade.needUpgrade();
    }

    /**
     * set datas for setting.
     *
     * @throws Throwable the throwable
     */
    public void initData() throws Throwable {
        initUPData();
        if (Environment.SUPPORT_DVR) {
            initRecordingData();
        } else { Log.printInfo("SARAHandler|initData()-This box doesn't support DVR."); }
    }

    /**
     * Sets the up data for setting.
     *
     * @throws Throwable the throwable
     */
    private void initUPData() throws Throwable {
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("SARAHandler|called initUPData()");
        saraPref = Upgrade.getSaraPreference();
        if (saraPref == null) {
            Log.printInfo("SARAHandler|initUPData()-SaraPreference is null.");
            return;
        }

        int langType = saraPref.preferredScreenLang;
        Log.printInfo("SARAHandler|initUPData()- screen langType : " + langType);
        String str = Definitions.LANGUAGE_FRENCH;
        if (langType == SaraPreference.LangByte_English || langType == SaraPreference.LangByte_MiddleEnglish) {
            str = Definitions.LANGUAGE_ENGLISH;
        } else if (langType == SaraPreference.LangByte_Spanish || langType == SaraPreference.LangByte_Spanish2) {
            str = Definitions.LANGUAGE_SPANISH;
        }
        upData.put(PreferenceNames.LANGUAGE, str);

        Log.printInfo("SARAHandler|initUPData()- saraPref.describedVideoSupported : "
                + saraPref.describedVideoSupported);
        str = Definitions.OPTION_VALUE_OFF;
        if (saraPref.describedVideoSupported) { str = Definitions.OPTION_VALUE_ON; }
        upData.put(PreferenceNames.DESCRIBED_AUDIO_DISPLAY, str);

        langType = saraPref.audioLanguage;
        Log.printInfo("SARAHandler|initUPData()- saraPref.audioLanguage : " + langType);
        str = Definitions.LANGUAGE_FRENCH;
        if (langType == SaraPreference.LangByte_English || langType == SaraPreference.LangByte_MiddleEnglish) {
            str = Definitions.LANGUAGE_ENGLISH;
        } else if (langType == SaraPreference.LangByte_Spanish || langType == SaraPreference.LangByte_Spanish2) {
            str = Definitions.LANGUAGE_SPANISH;
        }
        upData.put(PreferenceNames.SAP_LANGUAGE_AUDIO, str);

        int status = saraPref.closedCaptionStatus;
        Log.printInfo("SARAHandler|initUPData()- closedCaptionStatus : " + status);
        str = Definitions.OPTION_VALUE_OFF;
        if (status == 1) {
            str = Definitions.OPTION_VALUE_ON;
        } else if (status == 2) { str = Definitions.OPTION_VALUE_ON_MUTE; }
        upData.put(PreferenceNames.CC_DISPLAY, str);

        int scanrate = saraPref.scanRate;
        Log.printInfo("SARAHandler|initUPData()- scanRate : " + scanrate);
        if (scanrate > 0) {
            str = null;
            if (scanrate == 4 || scanrate == 6) {
                str = Definitions.VIDEO_TV_PICTURE_FORMAT_480I;
            } else if (scanrate == 3 || scanrate == 5) {
                str = Definitions.VIDEO_TV_PICTURE_FORMAT_480P;
            } else if (scanrate == 0 || scanrate == 1) {
                str = Definitions.VIDEO_TV_PICTURE_FORMAT_1080I;
            } else if (scanrate == 2) { str = Definitions.VIDEO_TV_PICTURE_FORMAT_720P; }
            if (str != null) { upData.put(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, str); }

            /*str = Definitions.VIDEO_ZOOM_MODE_NORMAL;
            if (scanrate == 3 || scanrate == 4) { str = Definitions.VIDEO_ZOOM_MODE_WIDE; }
            upData.put(PreferenceNames.VIDEO_ZOOM_MODE, str);*/
        }
        printHashtable(upData);
    }

    /**
     * Sets the recording data.
     *
     * @throws Throwable the throwable
     */
    private void initRecordingData() throws Throwable {
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("SARAHandler|called initRecordingData()");
        saraRecEntry = Upgrade.getRecordingList();
        Log.printInfo("SARAHandler|initRecordingData()-saraRecEntry : " + saraRecEntry);
        if (saraRecEntry != null) {
            Log.printInfo("SARAHandler|initRecordingData()-saraRecEntry.length : " + saraRecEntry.length);
        }
        if (saraRecEntry != null && saraRecEntry.length > 0) {
            illicoRecEntry = new IllicoRecordingEntry[saraRecEntry.length];
            for (int i = 0; i < saraRecEntry.length; i++) {
                Log.printInfo("=============================================================");
                Hashtable appData = new Hashtable();
                int theme = saraRecEntry[i].theme;
                Log.printInfo("SARAHandler|initRecordingData()-theme : " + theme);
                /*if (theme < themeValue.length) {
                    appData.put(AppDataKeys.CATEGORY, new Integer(themeValue[theme]));
                }*/
                appData.put(AppDataKeys.CATEGORY, new Integer(theme));
                //
                int rating = saraRecEntry[i].rating;
                Log.printInfo("SARAHandler|initRecordingData()-rating : " + rating);
                if (rating < ratingIndex.length) {
                    appData.put(AppDataKeys.PARENTAL_RATING, Constants.PARENTAL_RATING_NAMES[ratingIndex[rating]]);
                }
                appData.put(AppDataKeys.EPISODE_TITLE, "");
                Log.printInfo("SARAHandler|initRecordingData()-title : " + saraRecEntry[i].title);
                appData.put(AppDataKeys.TITLE, saraRecEntry[i].title);
                Log.printInfo("SARAHandler|initRecordingData()-channelNumber : "
                        + saraRecEntry[i].channelNumber);
                appData.put(AppDataKeys.CHANNEL_NUMBER, new Integer(saraRecEntry[i].channelNumber));
                Log.printInfo("SARAHandler|initRecordingData()-longDescription : "
                        + saraRecEntry[i].longDescription);
                appData.put(AppDataKeys.FULL_DESCRIPTION, saraRecEntry[i].longDescription);

                Log.printInfo("SARAHandler|initRecordingData()-recStartTime : "
                        + saraRecEntry[i].recStartTime);
                Log.printInfo("SARAHandler|initRecordingData()-recDuration : " + saraRecEntry[i].recDuration);
                Log.printInfo("SARAHandler|initRecordingData()-epgStartTime : "
                        + saraRecEntry[i].epgStartTime);
                Log.printInfo("SARAHandler|initRecordingData()-epgDuration : " + saraRecEntry[i].epgDuration);

                long startTime, duration;
                if (saraRecEntry[i].epgStartTime > 0) {
                    startTime = saraRecEntry[i].epgStartTime * Constants.MS_PER_SECOND;
                    duration = saraRecEntry[i].epgDuration * Constants.MS_PER_SECOND;
                } else {
                    startTime = saraRecEntry[i].recStartTime * Constants.MS_PER_SECOND;
                    duration = saraRecEntry[i].recDuration * Constants.MS_PER_SECOND;
                }
                appData.put(AppDataKeys.PROGRAM_START_TIME, new Long(startTime));
                appData.put(AppDataKeys.PROGRAM_END_TIME, new Long(startTime + duration));
                appData.put(AppDataKeys.BLOCK, new Boolean(saraRecEntry[i].isBlocked));
                appData.put(AppDataKeys.VIEW_COUNT, new Integer(0));
                printHashtable(appData);

                illicoRecEntry[i] = new IllicoRecordingEntry();
                illicoRecEntry[i].appData = appData;
                illicoRecEntry[i].appId = pvrAppID;
                //saraRecEntry[i].requestIndex
                illicoRecEntry[i].id = i + 1;
                Log.printInfo("SARAHandler|initRecordingData()-saraRecEntry[i].requestIndex : "
                        + saraRecEntry[i].requestIndex);

                //long lifeTime = saraRecEntry[i].lifeTime * Constants.MS_PER_SECOND;
                long lifeTime = saraRecEntry[i].lifeTime;
                Log.printInfo("SARAHandler|initRecordingData()-saraRecEntry[i].lifeTime : " + lifeTime);
                long saveUntil = Long.MAX_VALUE;
                if (lifeTime > 0) {
                    long epgEndTime = saraRecEntry[i].epgStartTime + saraRecEntry[i].epgDuration;
                    saveUntil = (lifeTime - epgEndTime) * Constants.MS_PER_SECOND;
                }
                illicoRecEntry[i].recProp = createRecordingProperties(saveUntil);
            }
        }

        //scheduled recording list
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("SARAHandler|initRecordingData()-schedule recording start");
        saraScheEntry = Upgrade.getScheduledRecordingList();
        Log.printInfo("SARAHandler|initRecordingData()-saraScheEntry : " + saraScheEntry);
        if (saraScheEntry != null) {
            Log.printInfo("SARAHandler|initRecordingData()-saraScheEntry.length : " + saraScheEntry.length);
        }
        if (saraScheEntry != null && saraScheEntry.length > 0) {
            illicoScheRecEntry = new IllicoScheduledRecordingEntry[saraScheEntry.length];
            for (int i = 0; i < saraScheEntry.length; i++) {
                Log.printInfo("=============================================================");
                Hashtable appData = new Hashtable();
                appData.put(AppDataKeys.EPISODE_TITLE, saraScheEntry[i].title);
                appData.put(AppDataKeys.TITLE, saraScheEntry[i].title);
                appData.put(AppDataKeys.CHANNEL_NUMBER, new Integer(saraScheEntry[i].channelNumber));
                //appData.put(AppDataKeys.FULL_DESCRIPTION, saraScheEntry[i].);

                Log.printInfo("SARAHandler|initRecordingData()-recStartTime : "
                        + saraScheEntry[i].recStartTime);
                Log.printInfo("SARAHandler|initRecordingData()-epgStartTime : "
                        + saraScheEntry[i].epgStartTime);
                Log.printInfo("SARAHandler|initRecordingData()-recDuration : "
                        + saraScheEntry[i].recDuration);
                Log.printInfo("SARAHandler|initRecordingData()-epgDuration : "
                        + saraScheEntry[i].epgDuration);

                long startTime, duration;
                if (saraScheEntry[i].epgStartTime > 0) {
                    startTime = saraScheEntry[i].epgStartTime * Constants.MS_PER_SECOND;
                    duration = saraScheEntry[i].epgDuration * Constants.MS_PER_SECOND;
                } else {
                    startTime = saraScheEntry[i].recStartTime * Constants.MS_PER_SECOND;
                    duration = saraScheEntry[i].recDuration * Constants.MS_PER_SECOND;
                }
                appData.put(AppDataKeys.PROGRAM_START_TIME, new Long(startTime));
                appData.put(AppDataKeys.PROGRAM_END_TIME, new Long(startTime + duration));
                printHashtable(appData);

                illicoScheRecEntry[i] = new IllicoScheduledRecordingEntry();
                illicoScheRecEntry[i].appData = appData;
                illicoScheRecEntry[i].appId = pvrAppID;

                long saveUntil = saveDaysForSchedule[saraScheEntry[i].saveDays];
                Log.printInfo("SARAHandler|initRecordingData()-saraScheEntry.saveUntil : " + saveUntil);
                if (saveUntil == -1) {
                    saveUntil = Long.MAX_VALUE;
                } else { saveUntil = Constants.MS_PER_DAY * saveUntil; }
                illicoScheRecEntry[i].recProp = createRecordingProperties(saveUntil);
            }
        }
    }

    /**
     * Execute setting.
     *
     * @return true, if successful
     * @throws Throwable the throwable
     */
    public boolean executeSetting() throws Throwable {
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("=================================================================================");
        Log.printInfo("SARAHandler|called executeSetting()");
        //SaraPreference saraPref = Upgrade.getSaraPreference();
        Log.printInfo("SARAHandler|executeSetting()-saraPref : " + saraPref);
        boolean isCompleted = false;
        try {
            int[] favorChannels = null;
            if (saraPref != null) {
                favorChannels = saraPref.favouriteChannels;
                Log.printInfo("SARAHandler|initUPData()- saraPref.audioLanguage : " + saraPref.audioLanguage);
            }
            if ((favorChannels != null && favorChannels.length > 0)
                    || (illicoRecEntry != null && illicoRecEntry.length > 0)
                    || (illicoScheRecEntry != null && illicoScheRecEntry.length > 0)) {
                if (!verifyEpgChannels()) {
                    Log.printError("SARAHandler|executeSetting()- EPG channels is not ready. so return.");
                    //tklee - 20111114
                    //return false;
                }
            }
            //tklee - 20120224 : STC error code
            if (saraPref == null) {
                Log.printError("[WIZ500]Cannot communicate with the HTTP server used to test the HTTP connexion.");
            }
            if (saraRecEntry == null) {
                Log.printError("[WIZ501]Cannot retrieve recording information from a migrated Cisco OCAP Ready STB.");
            }
            //
            //UP
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            int upResult = setUpData(favorChannels);
            //recording list
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            int recordResult = setRecordedData();
            //scheduled recording list
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            Log.printInfo("=================================================================================");
            int scheduledResult = setScheduledData();
            //tklee - 20111114
            if (upResult == 0 && recordResult == 0 && scheduledResult == 0) {
                ////Upgrade.upgradeFinished(); // 사용안함.
                Log.printInfo("SARAHandler|upgradeFinished()-Update success");
                isCompleted = true;
            } else {
                Log.printInfo("SARAHandler|upgradeFinished()-Update failed");
            }
            PreferenceProxy.getInstance().setCompleteMigration(isCompleted);
            Upgrade.upgradeFinalized();
        } catch (Throwable t) {
            Log.printError("SARAHandler- executeSetting() error. : " + t);
            t.printStackTrace();
            PreferenceProxy.getInstance().setCompleteMigration(isCompleted);
            Upgrade.upgradeFinalized();
        }
        return isCompleted;
    }

    /**
     * Sets the up data.
     *
     * @param favorChannels the favor channels
     * @return int the result of setting. return 0 if succesful
     * @throws Throwable the Throwable
     */
    private int setUpData(int[] favorChannels) throws Throwable {
        Log.printInfo("SARAHandler|setUpData()-favorChannels : " + favorChannels);
        if (favorChannels != null) {
            Log.printInfo("SARAHandler|setUpData()-favorChannels.length : " + favorChannels.length);
        }
        if (favorChannels != null && favorChannels.length > 0) {
            StringBuffer strBuf = new StringBuffer();
            for (int i = 0; i < favorChannels.length; i++) {
                Log.printInfo("SARAHandler|setUpData()-favorChannels value : " + favorChannels[i]);
                if (favorChannels[i] == 0) { continue; }
                TvChannel tvChannel = IXCManager.getInstance().getChannelByNumber(favorChannels[i]);
                Log.printInfo("SARAHandler|setUpData()-tvChannel : " + tvChannel);
                if (tvChannel != null) {
                    String chName = tvChannel.getCallLetter();
                    Log.printInfo("SARAHandler|setUpData()-chName : " + chName);
                    if (chName != null) {
                        if (strBuf.length() > 0) { strBuf.append(PreferenceService.PREFERENCE_DELIMETER); }
                        strBuf.append(chName);
                    }
                }
            }
            String favorStr = strBuf.toString();
            Log.printInfo("SARAHandler|setUpData()-strBuf : " + favorStr);
            if (favorStr != null && favorStr.length() > 0) {
                upData.put(PreferenceNames.FAVOURITE_CHANNEL, favorStr);
            }
        }
        printHashtable(upData);

        int upResult = 0;
        Enumeration e = upData.keys();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            //if (PreferenceNames.SAP_LANGUAGE_AUDIO.equals(key)) { continue; }
            int result = setPreferenceValue(key);
            if (result != 0) { upResult = result; }
        }
        int result = setPreferenceValue(PreferenceNames.SAP_LANGUAGE_AUDIO);
        if (result != 0) { upResult = result; }
        return upResult;
    }
    
    /**
     * Sets the preference value.
     *
     * @param key the key
     * @return the result
     */
    private int setPreferenceValue(String key) {
        int result = 0;
        String value = (String) upData.get(key);
        for (int i = 0; i < retryCount; i++) {
            if (!IXCManager.getInstance().setPreferenceValue(key, value)) {
                Log.printError("SARAHandler|setPreferenceValue()-failed to set : " + key);
                if (i < retryCount - 1) {
                    try {
                        Thread.sleep(1000L);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else { result = -1; }
            } else { break; }
        }
        return result;
    }

    /**
     * Sets the recorded data.
     *
     * @return the result of setting. return 0 if succesful
     * @throws Throwable the Throwable
     */
    private int setRecordedData() throws Throwable  {
        int recordResult = 0;
        Log.printInfo("SARAHandler|setRecordedData()-illicoRecEntry : " + illicoRecEntry);
        if (illicoRecEntry != null && illicoRecEntry.length > 0) {
            for (int i = 0; i < illicoRecEntry.length; i++) {
                Log.printInfo("=====================================================");
                Hashtable appData = illicoRecEntry[i].appData;
                int chNum = ((Integer) appData.get(AppDataKeys.CHANNEL_NUMBER)).intValue();
                int sourceId = saraRecEntry[i].serviceID;
                Log.printInfo("SARAHandler|setRecordedData()-sourceId : " + sourceId);
                TvChannel tvChannel = IXCManager.getInstance().getChannelByNumber(chNum);
                Log.printInfo("SARAHandler|setRecordedData()-tvChannel : " + tvChannel);
                if (tvChannel != null) {
                    String chName = tvChannel.getCallLetter();
                    Log.printInfo("SARAHandler|setRecordedData()-chName : " + chName);
                    if (chName != null) { appData.put(AppDataKeys.CHANNEL_NAME, chName); }
                    illicoRecEntry[i].loc = getLocator(tvChannel);
                } else { illicoRecEntry[i].loc = new OcapLocator(sourceId); }
                printHashtable(appData);
                Log.printInfo("SARAHandler|setRecordedData()-rating : " + saraRecEntry[i].rating);
                Log.printInfo("SARAHandler|setRecordedData()-theme : " + saraRecEntry[i].theme);

                Log.printInfo("SARAHandler|setRecordedData()-saraRecEntry[" + i + "] : " + saraRecEntry[i]);
                Log.printInfo("SARAHandler|setRecordedData()-illicoRecEntry[" + i + "] : "
                        + illicoRecEntry[i]);

                int tempResult = Upgrade.restoreIllicoRecording(saraRecEntry[i], illicoRecEntry[i]);
                if (tempResult == 0) {
                    Log.printInfo("SARAHandler|restoreIllicoRecording()-success requestIndex : "
                            + saraRecEntry[i].requestIndex);
                } else {
                    Log.printError("SARAHandler|restoreIllicoRecording()-failed requestIndex : "
                            + saraRecEntry[i].requestIndex);
                    recordResult = tempResult;
                }
            }
        }
        return recordResult;
    }

    /**
     * Sets the scheduled recorded data.
     *
     * @return the result of setting. return 0 if succesful
     * @throws Throwable the Throwable
     */
    private int setScheduledData() throws Throwable  {
        int scheduledResult = 0;
        Log.printInfo("SARAHandler|setScheduledData()-illicoScheRecEntry : " + illicoScheRecEntry);
        if (illicoScheRecEntry != null && illicoScheRecEntry.length > 0) {
            for (int i = 0; i < illicoScheRecEntry.length; i++) {
                Log.printInfo("=====================================================");
                Hashtable appData = illicoScheRecEntry[i].appData;
                int chNum = ((Integer) appData.get(AppDataKeys.CHANNEL_NUMBER)).intValue();
                TvChannel tvChannel = IXCManager.getInstance().getChannelByNumber(chNum);
                Log.printInfo("SARAHandler|setScheduledData()-tvChannel : " + tvChannel);
                TvProgram tvPgm;
                if (tvChannel != null) {
                    String chName = tvChannel.getCallLetter();
                    Log.printInfo("SARAHandler|setScheduledData()-chName : " + chName);
                    if (chName != null) {
                        appData.put(AppDataKeys.CHANNEL_NAME, chName);
                    }
                    appData.put(AppDataKeys.SOURCE_ID, new Integer(tvChannel.getSourceId()));
                    illicoScheRecEntry[i].loc = getLocator(tvChannel);

                    long startTime = saraScheEntry[i].epgStartTime * 1000L ;
                    //saraScheEntry[i]
                    tvPgm = IXCManager.getInstance().getProgram(tvChannel, startTime);
                } else {
//                    illicoRecEntry[i].loc = new OcapLocator(saraScheEntry[i].serviceID);
                    tvPgm = null;
                }
                Log.printInfo("SARAHandler|setScheduledData()-tvPgm : " + tvPgm);
                if (tvPgm != null) {
                    appData.put(AppDataKeys.PROGRAM_TYPE, new Integer(tvPgm.getType()));

                    appData.put(AppDataKeys.CATEGORY, new Integer(tvPgm.getCategory()));
                    String rating = tvPgm.getRating();
                    if (rating != null) { appData.put(AppDataKeys.PARENTAL_RATING, tvPgm.getRating()); }

                    int hdType = 0;
//                    if (tvPgm.isHd()) { hdType = 1; }
                    // modify for 4K but this class is not called because do not need a migration in 4K.
                    if (tvPgm.getDefinition() != 0) { hdType = 1; }
                    appData.put(AppDataKeys.DEFINITION, new Integer(hdType));
                    String pgmId = tvPgm.getId();
                    if (pgmId != null) { appData.put(AppDataKeys.PROGRAM_ID, tvPgm.getId()); }
                    String seriesId = tvPgm.getSeriesId();
                    if (seriesId != null) { appData.put(AppDataKeys.SERIES_ID, tvPgm.getSeriesId()); }
                    String lang = tvPgm.getLanguage();
                    if (lang != null) { appData.put(AppDataKeys.LANGUAGE, tvPgm.getLanguage()); }
                    String desc = tvPgm.getFullDescription();
                    if (desc != null) { appData.put(AppDataKeys.FULL_DESCRIPTION, desc); }
                } else {
                    appData.put(AppDataKeys.PROGRAM_TYPE, new Integer(TvProgram.TYPE_UNKNOWN));
                }

                printHashtable(appData);
                Log.printInfo("SARAHandler|setScheduledData()-saraScheEntry[" + i + "] : "
                        + saraScheEntry[i]);
                Log.printInfo("SARAHandler|setScheduledData()-illicoScheRecEntry[" + i + "] : "
                        + illicoScheRecEntry[i]);

                int tempResult = 0;
                int manualRepeat = saraScheEntry[i].manualRepeat;
                Log.printInfo("SARAHandler|setScheduledData()-manualRepeat : " + manualRepeat);
                Log.printInfo("SARAHandler|setScheduledData()-isFakeRepReq : "
                        + saraScheEntry[i].isFakeRepReq);
                Log.printInfo("SARAHandler|setScheduledData()-isRepeating : "
                        + saraScheEntry[i].isRepeating);
                int allEpisodeDefinition = saraScheEntry[i].allEpisodeDefinition;
                Log.printInfo("SARAHandler|setScheduledData()-allEpisodeDefinition : " +
                        allEpisodeDefinition);
                if (saraScheEntry[i].isRepeating) {
                    long fromTime = ((Long) appData.get(AppDataKeys.PROGRAM_START_TIME)).longValue();
                    long toTime = ((Long) appData.get(AppDataKeys.PROGRAM_END_TIME)).longValue();
                    Log.printInfo("SARAHandler|setScheduledData()-fromTime : " + fromTime);
                    Log.printInfo("SARAHandler|setScheduledData()-toTime : " + toTime);
                    Log.printInfo("SARAHandler|setScheduledData()-fromTime : " + new Date(fromTime));
                    Log.printInfo("SARAHandler|setScheduledData()-toTime : " + new Date(toTime));
                    boolean isManual = saraScheEntry[i].isManual;
                    Log.printInfo("SARAHandler|setScheduledData()-isManual : " + isManual);
                    long pgmStartTime = 0;
                    String repeatOption = repeatText[manualRepeat];
                    if (!isManual) {
                        pgmStartTime = saraScheEntry[i].epgStartTime * 1000L;
                        if (allEpisodeDefinition == 2) { //any day
                            repeatOption = repeatText[10];
                        } else if (allEpisodeDefinition == 4) { //this day
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(new Date(fromTime));
                            int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK); //1~7 (SUN ~ SAT)
                            Log.printInfo("SARAHandler|setScheduledData()-dayOfWeek : " + dayOfWeek);
                            repeatOption = repeatText[dayOfWeek];
                        } else { repeatOption = "xxxxxxx"; } //any time
                    }
                    Log.printInfo("SARAHandler|setScheduledData()-pgmStartTime : " + pgmStartTime);
                    Log.printInfo("SARAHandler|setScheduledData()-repeatOption : " + repeatOption);

                    for (int j = 0; j < retryCount; j++) {
                        boolean isSeting =
                            IXCManager.getInstance().requestRepeatRecording(chNum, saraScheEntry[i].title, fromTime,
                                toTime, pgmStartTime, repeatOption, 0);
                        if (!isSeting) {
                            if (j < retryCount - 1) {
                                try {
                                    Thread.sleep(1000L);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            } else {
                                tempResult = -1;
                                Log.printError("SARAHandler|requestRepeatRecording()-"
                                        + "failed to set PVR repeat recording");
                            }
                        } else {
                            String groupKey = IXCManager.getInstance().getGroupKey();
                            Log.printInfo("SARAHandler|requestRepeatRecording()-"
                                    + "success to set PVR repeat recording, groupKey : " + groupKey);
                            if (groupKey != null) { appData.put(AppDataKeys.GROUP_KEY, groupKey); }
                            break;
                        }
                    }
                }

                if (tempResult == 0) {
                    tempResult = Upgrade.restoreIllicoScheduledRecording(saraScheEntry[i], illicoScheRecEntry[i]);
                    if (tempResult == 0) {
                        Log.printInfo("SARAHandler|restoreIllicoScheduledRecording()-success channelNumber : "
                                + saraScheEntry[i].channelNumber);
                    } else {
                        Log.printError("SARAHandler|restoreIllicoScheduledRecording()-failed channelNumber : "
                                + saraScheEntry[i].channelNumber);
                        scheduledResult = tempResult;
                    }
                } else { scheduledResult = tempResult; }
            }
        }
        return scheduledResult;
    }

    /**
     * Creates the recording properties.
     *
     * @param saveUntil the save until
     * @return OcapRecordingProperties object created
     */
    private static OcapRecordingProperties createRecordingProperties(long saveUntil) {
        Log.printInfo("SARAHandler|createRecordingProperties()-saveUntil : " + saveUntil);
        long exp;
        if (saveUntil == Long.MAX_VALUE) {
            exp = Constants.MS_PER_DAY * 365 * 100 / 1000;
        } else {
            exp = saveUntil / Constants.MS_PER_SECOND;
        }
        Log.printInfo("SARAHandler|createRecordingProperties()-exp : " + exp);
        return new OcapRecordingProperties(
                OcapRecordingProperties.HIGH_BIT_RATE,
                exp,
                OcapRecordingProperties.DELETE_AT_EXPIRATION,
                OcapRecordingProperties.RECORD_WITH_CONFLICTS,
                new ExtendedFileAccessPermissions(true, true,
                        true, true,
                        true, true,
                        null, null),
                        "Videotron",
                null);
    }

    /**
     * Gets the locator.
     *
     * @param ch the TvChannel instance
     * @return the locator
     */
    private Locator getLocator(TvChannel ch) {
        Locator locator = null;
        try {
            locator = LocatorFactory.getInstance().createLocator(ch.getLocatorString());
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (locator == null) { Log.printInfo("can't create locator."); }
        return locator;

    }

    /**
     * Verify epg channels.
     *
     * @return true, if exist epg channels in shared memory.
     */
    private boolean verifyEpgChannels() {
        return SharedMemory.getInstance().get("Epg.Channels") != null;
    }

    /**
     * Gets the tv channel.
     *
     * @param chNum the ch num
     * @return the tv channel
     */
//    private TvChannel getTvChannel(int chNum) {
//        TvChannel tvCh = null;
//        for (int i = 0; i < 20 && tvCh == null; i++) {
//            tvCh = IXCManager.getInstance().getChannelByNumber(chNum);
//            if (tvCh == null) {
//                try {
//                    Thread.sleep(2000L);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//        Log.printInfo("getTvChannel - tvCh : " + tvCh);
//        return tvCh;
//    }

    /**
     * Prints the hashtable.
     *
     * @param h the Hashtable
     */
    private void printHashtable(Hashtable h) {
        if (h == null || h.isEmpty()) { return; }
        Enumeration e = h.keys();
        while (e.hasMoreElements()) {
            Object key = e.nextElement();
            Object value = h.get(key);
            //if (value instanceof Long) { value = String.valueOf(((Long) value).longValue()); }
            Log.printInfo("SARAHandler|printHashtable()-key : " + key + ", value : " + value);
        }
    }
}

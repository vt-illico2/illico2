/*
 *  @(#)MigrationAdapter.java 1.0 2011.03.15
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.wizard.migration;

import java.lang.reflect.Method;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.wizard.migration.communication.IXCManager;

/**
 * <code>MigrationAdapter</code> This class is for migration from SARA to OCAP on the Cisco box.
 *
 * @since   2011.03.15
 * @version $Revision: 1.7 $ $Date: 2017/01/09 21:08:33 $
 * @author  tklee
 */
public final class MigrationAdapter {
    /** the instance of MigrationAdapter. */
    private static MigrationAdapter instance  = null;
    /** The handler class. */
    private Class handlerClass = null;
    /** The handler instances. */
    private Object handlerInstances = null;
    /** whether this box exist update items or not. */
    private boolean isNeedUpgrade = false;
    /** The is init data. */
    private boolean isInitData = false;

    /**
     * constructor.
     */
    private MigrationAdapter() { }

    /**
     * Gets the instance of MigrationAdapter.
     * @return the instance of MigrationAdapter
     */
    public static synchronized MigrationAdapter getInstance() {
        if (instance == null) { instance = new MigrationAdapter(); }
        return instance;
    }

    /**
     * initialize. check whether this box is possible to upgrade.
     */
    public void init() {
        Log.printDebug("MigrationAdapter-called init()-HW_VENDOR : " + Environment.HW_VENDOR);
        if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO) {
            try {
                Log.printDebug("MigrationAdapter-called init()");
                handlerClass = Class.forName("com.videotron.tvi.illico.wizard.migration.SARAHandler");
                handlerInstances = handlerClass.newInstance();
                Method method = handlerClass.getMethod("needUpgrade", null);
                isNeedUpgrade = ((Boolean) method.invoke(handlerInstances, null)).booleanValue();
                Log.printDebug("MigrationAdapter-init()-isNeedUpgrade : " + isNeedUpgrade);
                if (isNeedUpgrade) {
                    IXCManager.getInstance().start();
                    new Thread("MigrationAdapter - initDataThread") {
                        public void run() {
                            try {
                                Method method = handlerClass.getMethod("initData", null);
                                method.invoke(handlerInstances, null);
                            } catch (Throwable t) {
                                Log.printError("MigrationAdapter- init error. : " + t);
                                t.printStackTrace();
                            }
                            isInitData = true;
                            Log.printInfo("MigrationAdapter|initDataThread ended.-isInitData : " + isInitData);
                        }
                    } .start();
                }
            } catch (Throwable t) {
                Log.printError("MigrationAdapter- init error. : " + t);
                t.printStackTrace();
            }
        }
    }

    /**
     * Execute setting.
     *
     * @return true, if successful
     */
    public synchronized boolean executeSetting() {
        Log.printDebug("MigrationAdapter-called executeSetting()-isNeedUpgrade : " + isNeedUpgrade);
        boolean isCompleted = false;
        if (isNeedUpgrade) {
            try {
                Method method = handlerClass.getMethod("executeSetting", null);
                isCompleted = ((Boolean) method.invoke(handlerInstances, null)).booleanValue();

                method = handlerClass.getMethod("needUpgrade", null);
                isNeedUpgrade = ((Boolean) method.invoke(handlerInstances, null)).booleanValue();
            } catch (Throwable t) {
                Log.printError("MigrationAdapter- executeSetting() error. : " + t);
                t.printStackTrace();
            }
        }
        return isCompleted;
    }

    /**
     * Checks if is needed upgrade.
     *
     * @return true, if is needed upgrade
     */
    public boolean isNeedUpgrade() {
        Log.printDebug("MigrationAdapter-called isNeedUpgrade()-isNeedUpgrade : " + isNeedUpgrade);
        return isNeedUpgrade;
    }

    /**
     * Checks if is inits the data.
     *
     * @return true, if is inits the data
     */
    public boolean isInitData() {
        Log.printDebug("MigrationAdapter-called isInitData()-isInitData : " + isInitData);
        return isInitData;
    }
}

/*
 *  @(#)IXCManager.java 1.0 2011.03.15
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.wizard.migration.communication;

import java.awt.Point;
import java.rmi.Remote;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.wizard.App;

/**
 * This class connects the IXC.
 *
 * @since   2011.03.15
 * @version $Revision: 1.6 $ $Date: 2017/01/09 21:08:33 $
 * @author  tklee
 */
public class IXCManager {
    /** The lookup sleep time. */
    private final long lookupSleepTime = 2000;
    /** The point of loading animation to show on screen. */
    private final Point loadingPoint = new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2);

    /** Class instance. */
    private static IXCManager instance = new IXCManager();

    /** The pref service. */
    private PreferenceService prefService = null;
    /** The epg service. */
    private EpgService epgService = null;
    /** The loading service. */
    private LoadingAnimationService loadingService = null;
    /** The pvr service. */
    private PvrService pvrService = null;
    /** The group key. the return value of requestRepeatRecording.*/
    private String groupKey = null;

    /**
     * Get IXCManager instance.
     * @return IXCManager
     */
    public static IXCManager getInstance() {
        return instance;
    }

    /**
     * lookup EPG, UPP, LoadingAnimation.
     */
    public void start() {
        Log.printInfo("IXCManager: called start()");
        lookUpService("/1/2030/", PreferenceService.IXC_NAME);
        lookUpService("/1/2026/", EpgService.IXC_NAME);
        lookUpService("/1/2075/", LoadingAnimationService.IXC_NAME);
        lookUpService("/1/20a0/", PvrService.IXC_NAME);
    }

    /**
     * Look up service.
     * @param path path string
     * @param name IXC name
     */
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("Migration.lookUp." + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(App.xletContext, ixcName);
                    } catch (Exception ex) {
                        Log.printInfo("IXCManager: not bound - " + name);
                    }
                    if (remote != null) {
                        Log.printInfo("IXCManager: found - " + name);
                        if (name.equals(PreferenceService.IXC_NAME)) {
                            prefService = (PreferenceService) remote;
                        } else if (name.equals(EpgService.IXC_NAME)) {
                            epgService = (EpgService) remote;
                        } else if (name.equals(PvrService.IXC_NAME)) {
                            pvrService = (PvrService) remote;
                        } else { loadingService = (LoadingAnimationService) remote; }
                        break;
                    }
                    try {
                        Thread.sleep(lookupSleepTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    /**
     * Gets the channel name.
     *
     * @param chNum the ch number
     * @return the channel by number
     */
    public TvChannel getChannelByNumber(int chNum) {
        Log.printDebug("IXCManager: called getChannelByNumber()-chNum : " + chNum);
        if (epgService == null) { return null; }
        try {
            return epgService.getChannelByNumber(chNum);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Gets the channel name.
     *
     * @param ch the TvChannel
     * @param time the time
     * @return the TvProgram
     */
    public TvProgram getProgram(TvChannel ch, long time) {
        Log.printDebug("IXCManager: called getProgram()-time : " + time);
        if (epgService == null) { return null; }
        try {
            return epgService.getProgram(ch, time);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Requests a new repeat recording from OcapReady application.
     *
     * @param channel channel number to be recorded.
     * @param title title of recording.
     * @param from recording start time.
     * @param to recording end time
     * @param pgmStartTime start time of program to be recorded.
     * set to 0, if the recording is recorded manually.
     * @param repeatOption 7-length string to represent repeat option.
     * Each char represents days to be recorded. (Monday to Sunday)
     * For example, 0000011 represents every weekends, 0010000 represents
     * every Wednesday. And xxxxxxx represents any day & any time slots.
     * @param keepOption number of episodes to be kept.
     * 0 represents all episodes.
     * @return true, if successful
     */
    public boolean requestRepeatRecording(int channel, String title, long from, long to, long pgmStartTime,
            String repeatOption, int keepOption) {
        Log.printDebug("IXCManager: called requestRepeatRecording()");
        groupKey = null;
        if (pvrService == null) {
            Log.printDebug("IXCManager:requestRepeatRecording()-pvrService is null. so return false.");
            return false;
        }
        try {
            groupKey =
                pvrService.requestRepeatRecording(channel, title, from, to, pgmStartTime, repeatOption, keepOption);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Gets the group key.
     *
     * @return the group key
     */
    public String getGroupKey() {
        Log.printDebug("IXCManager: called getGroupKey()");
        return groupKey;
    }

    /**
     * Show loading animation.
     */
    public void showLoadingAnimation() {
        if (loadingService == null) { return; }
        try {
            loadingService.showLoadingAnimation(loadingPoint);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide loading animation.
     */
    public void hideLoadingAnimation() {
        if (loadingService == null) { return; }
        try {
            loadingService.hideLoadingAnimation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the preference value.
     *
     * @param name the name
     * @param value the value
     * @return true, if successful
     */
    public boolean setPreferenceValue(String name, String value) {
        Log.printDebug("IXCManager: setPreferenceValue()-name : " + name + ", value : " + value);
        Log.printDebug("IXCManager: setPreferenceValue()-prefService : " + prefService);
        if (prefService == null) { return false; }
        try {
            return prefService.setPreferenceValue(name, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}

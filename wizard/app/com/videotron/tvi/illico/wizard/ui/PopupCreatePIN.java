package com.videotron.tvi.illico.wizard.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class PopupCreatePIN extends Popup {
    private static final long serialVersionUID = 1L;
    private DataCenter dCenter = DataCenter.getInstance();
    
//    private Image imgPopBG;
    private Image imgPopGap;
//    private Image imgPopShadow;
//    private Image imgPopGlow;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    private Image imgButtonInactive;
    private Image imgPinBoxB;
    private Image imgPinBoxF;
    private Image imgIconCheck;
    private Image imgIconInvalid;
    
    private final String txtStar = "*";
    private final int areaGap = 38;
    private final int pinGap = 38;
    private static final int AREA_COUNT = 2;
    private static final int AREA_PIN_CODE = 0;
    private static final int AREA_REPEAT_PIN_CODE = 1;
    private final int PIN_MAX_LENGTH = 4;
    private String pinCode;
    private String rPinCode;
    //Button
    private boolean isButtonArea;
    public static final int BUTTON_TYPE_COUNT = 2;
    public static final int BUTTON_TYPE_CLEAR = 0;
    public static final int BUTTON_TYPE_ACTION = 1;
    private int buttonType;
    //Title
//    private String popTitle;
    //PIN_TYPE
    public static final int PIN_TYPE_ADMIN = 0;
    public static final int PIN_TYPE_ORDER = 1;
    private int pinType;
    //PIN State
    public static int PIN_STATE_NORMAL = 0;
    public static int PIN_STATE_VALID = 1;
    public static int PIN_STATE_INVALID = 2;
    public static int PIN_STATE_SAME_WITH_OTHER_PIN = 3;
    private int pinState;
    
    public PopupCreatePIN() {
//        imgPopBG = dCenter.getImage("pop_high_402.png");
        imgPopGap = dCenter.getImage("pop_gap_379.png");
//        imgPopShadow = dCenter.getImage("pop_sha.png");
//        imgPopGlow = dCenter.getImage("pop_d_glow_461.png");
        imgButtonBasic = dCenter.getImage("05_focus_dim.png");
        imgButtonFocus = dCenter.getImage("05_focus.png");
        imgButtonInactive = dCenter.getImage("05_focus_inactive.png");
        
        imgPinBoxB = dCenter.getImage("05_pin_box.png");
        imgPinBoxF = dCenter.getImage("05_pin_focus.png");
        imgIconCheck = dCenter.getImage("icon_g_check.png");
        imgIconInvalid = dCenter.getImage("icon_r_x__small.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    
    protected void disposePopup() {
//        if (imgPopBG != null) {
//            dCenter.removeImage("pop_high_402.png");
//            imgPopBG = null;
//        }
    	if (imgPopGap != null) {
    		dCenter.removeImage("pop_gap_379.png");
    		imgPopGap = null;
    	}
//        if (imgPopShadow != null) {
//            dCenter.removeImage("pop_sha.png");
//            imgPopShadow = null;
//        }
//        if (imgPopGlow != null) {
//            dCenter.removeImage("pop_d_glow_461.png");
//            imgPopGlow = null;
//        }
        if (imgButtonBasic != null) {
            dCenter.removeImage("05_focus_dim.png");
            imgButtonBasic = null;
        }
        if (imgButtonFocus != null) {
            dCenter.removeImage("05_focus.png");
            imgButtonFocus = null;
        }
        if (imgButtonInactive != null) {
            dCenter.removeImage("05_focus_inactive.png");
            imgButtonInactive = null;
        }
        if (imgPinBoxB != null) {
            dCenter.removeImage("05_pin_box.png");
            imgPinBoxB = null;
        }
        if (imgPinBoxF != null) {
            dCenter.removeImage("05_pin_focus.png");
            imgPinBoxF = null;
        }
        if (imgIconCheck != null) {
            dCenter.removeImage("icon_g_check.png");
            imgIconCheck = null;
        }
        if (imgIconInvalid != null) {
            dCenter.removeImage("icon_r_x.png");
            imgIconInvalid = null;
        }
    }
    protected void startPopup(boolean reset) {
        isButtonArea = false;
        buttonType = BUTTON_TYPE_CLEAR;
        pinCode = "";
        rPinCode = "";
        pinState = PIN_STATE_NORMAL;
    }
    protected void stopPopup() {
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
                if (isButtonArea) {
                    return true;
                }
                String inputData = String.valueOf((keyCode - KeyEvent.VK_0));
                if (pinCode.length() < PIN_MAX_LENGTH) {
                    pinCode += inputData;
                    if (pinCode.length() == PIN_MAX_LENGTH) {
                        switch(pinType) {
                            case PIN_TYPE_ADMIN:
                                boolean equalsWithOrderPIN = PreferenceProxy.getInstance().equalsWithOrderPIN(pinCode);
                                if (equalsWithOrderPIN) {
                                    pinCode = "";
                                    pinState = PIN_STATE_SAME_WITH_OTHER_PIN;
                                } else {
                                    pinState = PIN_STATE_NORMAL;
                                }
                                break;
                            case PIN_TYPE_ORDER:
                                boolean isEqualsWithAdminPIN = PreferenceProxy.getInstance().equalsWithAdminPIN(pinCode);
                                if (isEqualsWithAdminPIN) {
                                    pinCode = "";
                                    pinState = PIN_STATE_SAME_WITH_OTHER_PIN;
                                } else {
                                    pinState = PIN_STATE_NORMAL;
                                }
                                break;
                        }
                        repaint();
                        return true;
                    }
                } else if (rPinCode.length() < PIN_MAX_LENGTH) {
                    rPinCode += inputData;
                }
                if (rPinCode.length() == PIN_MAX_LENGTH) {
                    if (pinCode.equals(rPinCode)) {
                        isButtonArea = true;
                        buttonType = BUTTON_TYPE_ACTION;
                        pinState = PIN_STATE_VALID;
                    } else {
                        pinState = PIN_STATE_INVALID;
                        pinCode = "";
                        rPinCode = "";
                    }
                } else {
                    pinState = PIN_STATE_NORMAL;
                }
                repaint();
                return true;
            case Rs.KEY_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case KeyEvent.VK_UP:
                if (!isButtonArea) {
                    return true;  
                }
                if (pinCode.length() == PIN_MAX_LENGTH && rPinCode.length() == PIN_MAX_LENGTH) {
                    return true;
                }
                isButtonArea = false;  
                repaint();
                return true;
            case KeyEvent.VK_DOWN:
                if (isButtonArea) {
                    return true;  
                }
                isButtonArea = true;
                if (pinCode.length() == 0 || (pinCode.length() == PIN_MAX_LENGTH && rPinCode.length() == 0)) {
                    buttonType = BUTTON_TYPE_ACTION;
                }
                repaint();
                return true;
            case KeyEvent.VK_LEFT:
                if (isButtonArea) {
                    if (buttonType == BUTTON_TYPE_CLEAR) {
                        return true;
                    }
                    if (pinCode.length() == 0 || (pinCode.length() == PIN_MAX_LENGTH && rPinCode.length() == 0)) {
                        return true;
                    }
                    buttonType = BUTTON_TYPE_CLEAR;
                    repaint();
                    return true;  
                }
                return true;
            case KeyEvent.VK_RIGHT:
                if (isButtonArea) {
                    if (buttonType == BUTTON_TYPE_ACTION) {
                        return true;
                    }
                    buttonType = BUTTON_TYPE_ACTION;
                    repaint();
                    return true; 
                }
                return true;
            case KeyEvent.VK_ENTER:
                if (!isButtonArea) {
                    return true;
                }
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                effect.start(322 + (buttonType * 164), 353, 155, 33);
                switch(buttonType) {
                    case BUTTON_TYPE_CLEAR:
                        if (rPinCode.length() > 0) {
                            rPinCode = "";
                            pinState = PIN_STATE_NORMAL;
                        } else if (pinCode.length() > 0) {
                            pinCode = "";
                            pinState = PIN_STATE_NORMAL;
                        }
                        isButtonArea = false;
                        repaint();
                        break;
                    case BUTTON_TYPE_ACTION:
                        if (pListener != null) {
                            if (pinCode.length() == PIN_MAX_LENGTH && pinCode.equals(rPinCode)) {
                                pListener.popupOK(new PopupEvent(this));    
                            } else {
                                pListener.popupCancel(new PopupEvent(this));
                            }
                        }
                        break;
                }
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB000000000204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopGlow != null) {
//            g.drawImage(imgPopGlow, 250, 113, this);
//        }
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 280, 415, 404, 79, this);
//        }
        g.setColor(Rs.C035035035);
        g.fillRect(280, 143, 402, 276);
//        if (imgPopBG != null) {
//            g.drawImage(imgPopBG, 280, 143, this);
//        }
        if (imgPopGap != null) {
            g.drawImage(imgPopGap, 292, 181, this);
        }
        //Title
        String popTitle = null;
        switch(pinType) {
            case PIN_TYPE_ADMIN:
                popTitle = dCenter.getString("TxtQSW.Create_Admin_PIN_Code");
                break;
            case PIN_TYPE_ORDER:
                popTitle = dCenter.getString("TxtQSW.Create_Order_PIN_Code");
                break;
        }
        if (popTitle != null) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(popTitle, 480 - (Rs.FM24.stringWidth(popTitle)/2), 170);
        }
        //SubTitle
        g.setFont(Rs.F18);
        if (pinState == PIN_STATE_INVALID) {
            int low1Wth = 0;
            int imgIconInvalidWth = 0;
            if (imgIconInvalid != null) {
                imgIconInvalidWth = imgIconInvalid.getWidth(this);
                low1Wth = imgIconInvalidWth + 4;
            }
            String txtSubTitle0 = dCenter.getString("TxtQSW.Pop_Create_PIN_Desc_Invalid_0");
            int txtSubTitle0Wth = 0;
            if (txtSubTitle0 != null) {
                txtSubTitle0Wth = Rs.FM18.stringWidth(txtSubTitle0);
                low1Wth += txtSubTitle0Wth;
            }
            if (imgIconInvalid != null) {
                g.drawImage(imgIconInvalid, 480 - (low1Wth/2), 201, this);
            }
            g.setColor(Rs.C248063063);
            if (txtSubTitle0 != null) {
                g.drawString(txtSubTitle0, 480-(low1Wth/2) + imgIconInvalidWth + 4, 218);
            }
            String txtSubTitle1 = dCenter.getString("TxtQSW.Pop_Create_PIN_Desc_Invalid_1");
            if (txtSubTitle1 != null) {
                g.drawString(txtSubTitle1, 480-(Rs.FM18.stringWidth(txtSubTitle1)/2), 238);
            }
        } else if (pinState == PIN_STATE_SAME_WITH_OTHER_PIN) { 
            g.setColor(Rs.C229229229);
            String txtSubTitle = null;
            switch(pinType) {
                case PIN_TYPE_ADMIN:
                    txtSubTitle = dCenter.getString("TxtQSW.Create_Admin_PIN_Code_Same");
                    break;
                case PIN_TYPE_ORDER:
                    txtSubTitle = dCenter.getString("TxtQSW.Create_Order_PIN_Code_Same");
                    break;
            }
            if (txtSubTitle != null) {
                String[] txtSubTitles = TextUtil.split(txtSubTitle, Rs.FM18, 350, '|');
                int startY = 227;
                if (txtSubTitles.length > 1) {
                    startY = 218;
                }
                for (int i=0; i<txtSubTitles.length; i++) {
                    if (txtSubTitles[i] != null) {
                        g.drawString(txtSubTitles[i], 480-(Rs.FM18.stringWidth(txtSubTitles[i])/2), startY + (i * 20));
                    }
                }
            }
        } else {
            g.setColor(Rs.C229229229);
            String txtSubTitle = null;
            switch(pinType) {
                case PIN_TYPE_ADMIN:
                    txtSubTitle = dCenter.getString("TxtQSW.Create_Admin_PIN_Code_Desc");
                    break;
                case PIN_TYPE_ORDER:
                    txtSubTitle = dCenter.getString("TxtQSW.Create_Order_PIN_Code_Desc");
                    break;
            }
            if (txtSubTitle != null) {
                String[] txtSubTitles = TextUtil.split(txtSubTitle, Rs.FM18, 350, '|');
                int startY = 227;
                if (txtSubTitles.length > 1) {
                    startY = 218;
                }
                for (int i=0; i<txtSubTitles.length; i++) {
                    if (txtSubTitles[i] != null) {
                        g.drawString(txtSubTitles[i], 480-(Rs.FM18.stringWidth(txtSubTitles[i])/2), startY + (i * 20));
                    }
                }
            }
        }
        int pinCodeLth = pinCode.length();
        int rPinCodeLth = rPinCode.length();
        //Button
        g.setFont(Rs.F18);
        for (int i=0; i<BUTTON_TYPE_COUNT; i++) {
            Image imgButton = null;
            if (buttonType == i && isButtonArea) {
                imgButton = imgButtonFocus;
            } else {
                imgButton = imgButtonBasic;
            }
            if (i == BUTTON_TYPE_CLEAR) {
                if (pinCodeLth == 0) {
                    imgButton = imgButtonInactive;
                } else if (pinCodeLth == PIN_MAX_LENGTH && rPinCodeLth == 0) {
                    imgButton = imgButtonInactive;
                }
            }
            if (imgButton != null) {
                g.drawImage(imgButton, 321 + (164*i), 353, this);
            }
            String txtButton = null;
            switch(i) {
                case BUTTON_TYPE_CLEAR:
                    txtButton = dCenter.getString("TxtQSW.Clear");
                    if (txtButton != null) {
                        if (pinCodeLth == 0 || (pinCodeLth == PIN_MAX_LENGTH && rPinCodeLth == 0)) {
                            g.setColor(Rs.C110110110);
                            g.drawString(txtButton, 401 - (Rs.FM18.stringWidth(txtButton)/2) + (164*i), 376);
                            g.setColor(Rs.C050050050);
                            g.drawString(txtButton, 400 - (Rs.FM18.stringWidth(txtButton)/2) + (164*i), 375);
                        } else {
                            g.setColor(Rs.C003003003);
                            g.drawString(txtButton, 400 - (Rs.FM18.stringWidth(txtButton)/2) + (164*i), 375); 
                        }
                    }
                    break;
                case BUTTON_TYPE_ACTION:
                    if (pinCode.length() == PIN_MAX_LENGTH && pinCode.equals(rPinCode)) {
                        txtButton = dCenter.getString("TxtQSW.OK");
                    } else {
                        txtButton = dCenter.getString("TxtQSW.Cancel");
                    }
                    if (txtButton != null) {
                        g.setColor(Rs.C003003003);
                        g.drawString(txtButton, 400 - (Rs.FM18.stringWidth(txtButton)/2) + (164*i), 375);
                    }
                    break;
            }
        }
        //Label
        g.setFont(Rs.F17);
        g.setColor(Rs.DVB255255255128);
        for (int i=0; i<AREA_COUNT; i++) {
            String txtLable = null;
            if (i == AREA_PIN_CODE) {
                txtLable = dCenter.getString("TxtQSW.PIN_Code");
            } else if (i == AREA_REPEAT_PIN_CODE) {
                txtLable = dCenter.getString("TxtQSW.Confirm_PIN_Code");
            }
            if (txtLable != null) {
                g.drawString(txtLable, 326, 281+(i*areaGap)) ;
            }
        }
        //PIN Code
        g.setFont(Rs.F24);
        g.setColor(Rs.C182182182);
        
        int curArea = AREA_REPEAT_PIN_CODE;
        if (pinCode.length() < PIN_MAX_LENGTH) {
            curArea = AREA_PIN_CODE;
        }
        for (int i=0; i<AREA_COUNT; i++) {
            int pinLth = 0;
            if (i == 0) {
                pinLth =  pinCodeLth;
            } else {
                pinLth =  rPinCodeLth;
            }
            for (int j=0; j<PIN_MAX_LENGTH; j++) {
                Image imgPinBox = null;
                if (i == curArea && j == pinLth && !isButtonArea) {
                    imgPinBox = imgPinBoxF;
                } else {
                    imgPinBox = imgPinBoxB;
                }
                if (imgPinBox != null) {
                    g.drawImage(imgPinBox, 458+(j*pinGap), 261+(i*areaGap), this);
                }
            }
            for (int j=0; j<pinLth; j++) {
                if (pinCode != null) {
                    g.drawString(txtStar, 475+(j*pinGap)-(Rs.FM24.stringWidth(txtStar)/2), 288+(i*areaGap));  
                }
            }
        }
        if (pinState == PIN_STATE_VALID) {
            if (imgIconCheck != null) {
                g.drawImage(imgIconCheck, 614, 264, this);
                g.drawImage(imgIconCheck, 614, 302, this);
            }
        }
    }
    public void setPopupPinType(int pinType) {
        this.pinType = pinType;
    }
    
    public String getPinCode() {
        return pinCode;
    }
    
    public int getCurrentButtonType() {
        return buttonType;
    }
}

package com.videotron.tvi.illico.wizard.ui;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferencProxyUpdateListener;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.controller.DataManager;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererLanguage;

public class SceneLanguage extends Scene implements PreferencProxyUpdateListener {
    private static final long serialVersionUID = -8319423869576097504L;

    private ComponentStep compStep;
    private ComponentTitle compTitle;

    public static final int LANGUAGE_COUNT = 2;
    public static final int LANGUAGE_FRENCH = 0;
    public static final int LANGUAGE_ENGLISH = 1;
    private static final String[] LANGUAGE_VALUE = {Definitions.LANGUAGE_FRENCH, Definitions.LANGUAGE_ENGLISH};
    private boolean isActiveLanguageByOk = false;
    private int curLang;
    private String language;

    protected void initScene() {
        renderer = new RendererLanguage();
        setRenderer(renderer);
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(53, 17, 424, 72);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setCurrentStep(ComponentStep.STEP_ID_LANGUAGE);
            compStep.setBounds(0, 88, 960, 29);
            compStep.setStepIds(STEP_ID);
            add(compStep);
        }
    }

    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep.dispose();
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle.dispose();
            compTitle = null;
        }
        renderer = null;
    }

    protected void startScene(boolean isReset) {
        PreferenceProxy.getInstance().setPreferencProxyUpdateListener(this);
        isActiveLanguageByOk = false;
        if (isReset) {
            language = PreferenceProxy.getInstance().getCurrentLanguage();
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneLanguage.startScene]Current language : "+language);
            }
            if (language.equals(Definitions.LANGUAGE_ENGLISH)) {
                curLang = LANGUAGE_ENGLISH;
            } else {
                curLang = LANGUAGE_FRENCH;
            }
        }
        prepare();
    }

    protected void stopScene() {
        if (!isActiveLanguageByOk) {
            PreferenceProxy.getInstance().setCurrentLanguage(language);
        }
        PreferenceProxy.getInstance().removePreferencProxyUpdateListener();
    }

    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyCodes.LAST:
                if (!DataManager.getInstance().isCalledByMonitor()) {
                    String calledByValue = DataManager.getInstance().getCalldByValue();
                    if (calledByValue != null) {
                        CommunicationManager.getInstance().requestStartUnboundApplication(calledByValue, null);
                    }
                }
                return true;
            case Rs.KEY_LEFT:
                if (curLang == 0) {
                    return true;
                }
                curLang --;
                PreferenceProxy.getInstance().setCurrentLanguage(Definitions.LANGUAGE_FRENCH);
                return true;
            case Rs.KEY_RIGHT:
                if (curLang == LANGUAGE_COUNT-1) {
                    return true;
                }
                curLang ++;
                PreferenceProxy.getInstance().setCurrentLanguage(Definitions.LANGUAGE_ENGLISH);
                return true;
            case Rs.KEY_OK:
                new ClickingEffect(this, 5).start(82+(curLang* RendererLanguage.PANEL_GAP), 170, 372, 255);
                isActiveLanguageByOk = true;
                PreferenceProxy.getInstance().setCurrentLanguage(LANGUAGE_VALUE[curLang]);
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_ADMIN_PIN, true);
                return true;
//            case Rs.KEY_UP:
//                testParseXML();
//                return true;
        }
        return false;
    }
    public int getCurrentLanguage() {
        return curLang;
    }

    public void receiveUpdatedPreference(String name, String value) {
        if (name != null && name.equals(PreferenceNames.LANGUAGE)) {
            repaint();
        }
    }
    
//    private void testParseXML() {
//        File file = new File("resource/test_data/ms_data/IllicoActivate.do.xml");
//        
//        // Parse XML
//        DefaultHandler handler = new ActivatedResultHandler();
//        InputStream in = null;
//        try {
//            try{
//                in = new FileInputStream(file);
//            }catch(Exception e) {
//                if (Log.ERROR_ON) {
//                    Log.printError("[SceneCompletion.activatedSTB] Cannot communicate with the Activation service.");
//                }
//            }
//            try {
//                SAXParserFactory parserFactory = SAXParserFactory.newInstance();
//                parserFactory.setValidating(false);
//                parserFactory.setNamespaceAware(false);
//                SAXParser parser = parserFactory.newSAXParser();
//                parser.parse(in, handler);
//            } catch (Exception e) {
//                if (Log.ERROR_ON) {
//                    Log.printError("[SceneCompletion.activatedSTB] Cannot parse the response received from the Activation service.");
//                }
//                e.printStackTrace();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            if (in != null) {
//                try {
//                    in.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                in = null;
//            }
//        }
//        SAXHandler saxHandler = (SAXHandler) handler;
//        String successValue = null;
//        if (saxHandler != null) {
//            successValue = (String) saxHandler.getResultObject();
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[SceneCompletion.activatedSTB] Success value : "+successValue);
//        }
//    }
}

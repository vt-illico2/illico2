package com.videotron.tvi.illico.wizard.ui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class PopupNotification extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    private DataCenter dataCenter = DataCenter.getInstance();
    
//    private Image imgPopBG;
//    private Image imgPopGap;
//    private Image imgPopShadow;
    private Image imgPopIcon;
    private Image imgButtonB;
    private Image imgButtonF;
    
    private static final int POPUP_SIZE_VALID_W = 340;
    private static final int POPUP_SIZE_MAX_LINE = 6;
    private static final int[] POPUP_SIZE_Y_POS = {288, 280, 266, 254, 242, 232};
    private static final int CONTENT_GAP = 19;
    
    public static final int POPUP_TYPE_QUESTION = 0;
    public static final int POPUP_TYPE_INFORMATION = 1;
    private int curPopType = POPUP_TYPE_INFORMATION;
    
    private final int buttonGap = 157;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_OK = 0;
    public static final int BUTTON_CANCEL = 1;
    private int curButton;
    
    private String title;
    private String content;
    
    public PopupNotification() {
//        imgPopBG = dataCenter.getImage("pop_high_402.png");
//        imgPopGap = dataCenter.getImage("pop_gap_379.png");
//        imgPopShadow = dataCenter.getImage("pop_sha.png");
        imgPopIcon = dataCenter.getImage("icon_noti_or.png");
        imgButtonB = dataCenter.getImage("05_focus_dim.png");
        imgButtonF = dataCenter.getImage("05_focus.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void disposePopup() {
//        if (imgPopBG != null) {
//            dataCenter.removeImage("pop_high_402.png");
//            imgPopBG = null;
//        }
//        if (imgPopGap != null) {
//            dataCenter.removeImage("pop_gap_379.png");
//            imgPopGap = null;
//        }
//        if (imgPopShadow != null) {
//            dataCenter.removeImage("pop_sha.png");
//            imgPopShadow = null;
//        }
        if (imgPopIcon != null) {
            dataCenter.removeImage("icon_noti_or.png");
            imgPopIcon = null;
        }
        if (imgButtonB != null) {
            dataCenter.removeImage("05_focus_dim.png");
            imgButtonB = null;
        }
        if (imgButtonF != null) {
            dataCenter.removeImage("05_focus.png");
            imgButtonF = null;
        }
    }
    protected void startPopup(boolean reset) {
        curButton = BUTTON_OK;        
    }
    protected void stopPopup() {
        curPopType = POPUP_TYPE_INFORMATION;
    }
    
    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 280, 415, 404, 79, this);
//        }
        g.setColor(Rs.C035035035);
        g.fillRect(280, 143, 402, 276);
//        if (imgPopBG != null) {
//            g.drawImage(imgPopBG, 280, 143, this);
//        }
//        if (imgPopGap != null) {
//            g.drawImage(imgPopGap, 292, 181, this);
//        }
        //Icon
        final int titleGap = 6;
        int titleAreaWth = 0;
        int imgPopIconWth = 0;
        if (imgPopIcon !=null) {
            imgPopIconWth = imgPopIcon.getWidth(this);
            titleAreaWth += imgPopIconWth;
            titleAreaWth += titleGap;
        }
        int titleWth = 0;
        if (title != null) {
            titleWth = Rs.FM24.stringWidth(title);
            titleAreaWth += titleWth;
        }
        int xPos = 480 - (titleAreaWth/2);
        if (imgPopIcon !=null) {
            g.drawImage(imgPopIcon, xPos, 153, this) ;
            xPos += imgPopIconWth;
            xPos += titleGap;
        }
        if (titleWth > 0) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(title, xPos, 172);
        }
        //Button
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        int yPos = 368;
        switch(curPopType) {
            case POPUP_TYPE_QUESTION:
                for (int i=0; i<BUTTON_COUNT; i++) {
                    Image imgButton = null;
                    if (i == curButton) {
                        imgButton = imgButtonF;
                    } else {
                        imgButton = imgButtonB;
                    }
                    if (imgButton != null) {
                        g.drawImage(imgButton, 325+(i*buttonGap), yPos, this);
                    }
                    String txtButton = null;
                    if (i == BUTTON_OK) {
                        txtButton = (String) dataCenter.get("TxtQSW.OK");
                    } else if (i == BUTTON_CANCEL){
                        txtButton = (String) dataCenter.get("TxtQSW.Cancel");
                    }
                    if (txtButton != null) {
                        int txtButtonWth = Rs.FM18.stringWidth(txtButton);
                        g.drawString(txtButton, 403+(i*buttonGap)-(txtButtonWth/2), yPos +22);
                    }
                }
                break;
            case POPUP_TYPE_INFORMATION:
                g.drawImage(imgButtonF, 403, yPos, this);
                String txtOK = "OK";
                int txtOkWth = Rs.FM18.stringWidth(txtOK);
                g.drawString(txtOK, 480 -(txtOkWth/2), yPos+22);
                break;
        }
        //Content
        if (content != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C229229229);
            String[] conts = TextUtil.split(content, Rs.FM18, POPUP_SIZE_VALID_W, '|');
            int contsCnt = conts.length;
            if (contsCnt > POPUP_SIZE_MAX_LINE) {
                contsCnt = POPUP_SIZE_MAX_LINE;
            }
            int yCont = POPUP_SIZE_Y_POS[contsCnt-1];
            for (int i=0; i<contsCnt; i++){
                String cont = conts[i];
                int contWth = Rs.FM18.stringWidth(cont);
                g.drawString(cont, 480 - (contWth/2), yCont+(i*CONTENT_GAP));
            }
        }
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_LEFT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_OK) {
                    return true;
                }
                curButton = BUTTON_OK;
                repaint();
                return true;
            case Rs.KEY_RIGHT:
                if (curPopType == POPUP_TYPE_INFORMATION) {
                    return true;
                }
                if (curButton == BUTTON_CANCEL) {
                    return true;
                }
                curButton = BUTTON_CANCEL;
                repaint();
                return true;
            case Rs.KEY_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case Rs.KEY_OK:
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                switch(curPopType) {
                    case POPUP_TYPE_QUESTION:
                        effect.start(326+(curButton * buttonGap), 368, 155, 33);
                        break;
                    case POPUP_TYPE_INFORMATION:
                        effect.start(404, 368, 155, 33);
                        break;
                }
                if (curButton == BUTTON_OK) {
                    pListener.popupOK(new PopupEvent(this));
                } else {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    public int getCurrentButton() {
        return curButton;
    }
    public void setPopupType (int popupType) {
        curPopType = popupType;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setContent(String content) {
        this.content = content;
    }
}

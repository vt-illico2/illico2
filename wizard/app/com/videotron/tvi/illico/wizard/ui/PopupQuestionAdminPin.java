package com.videotron.tvi.illico.wizard.ui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class PopupQuestionAdminPin extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    private DataCenter dCenter = DataCenter.getInstance();
    
//    private Image imgPopBGBig;
//    private Image imgPopGap;
//    private Image imgPopShadow;
    private Image imgPopIcon;
    private Image imgButtonB;
    private Image imgButtonF;
    
    private static final int POPUP_SIZE_VALID_W = 320;
    private static final int POPUP_SIZE_MAX_LINE = 6;
    
    private final int buttonGap = 37;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_OK = 0;
    public static final int BUTTON_CANCEL = 1;
    private int curButton;
    
    private String title;
    private String content;
    
    public PopupQuestionAdminPin() {
//        imgPopBGBig = dCenter.getImage("pop_high_402.png");
//        imgPopGap = dCenter.getImage("pop_gap_379.png");
//        imgPopShadow = dCenter.getImage("pop_sha.png");
        imgPopIcon = dCenter.getImage("icon_noti_or.png");
        imgButtonB = dCenter.getImage("btn_293.png");
        imgButtonF = dCenter.getImage("btn_293_foc.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void disposePopup() {
//        if (imgPopBGBig != null) {
//            dCenter.removeImage("pop_high_402.png");
//            imgPopBGBig = null;
//        }
//        if (imgPopGap != null) {
//            dCenter.removeImage("pop_gap_379.png");
//            imgPopGap = null;
//        }
//        if (imgPopShadow != null) {
//            dCenter.removeImage("pop_sha.png");
//            imgPopShadow = null;
//        }
        if (imgPopIcon != null) {
            dCenter.removeImage("icon_noti_or.png");
            imgPopIcon = null;
        }
        if (imgButtonB != null) {
            dCenter.removeImage("btn_293.png");
            imgButtonB = null;
        }
        if (imgButtonF != null) {
            dCenter.removeImage("btn_293_foc.png");
            imgButtonF = null;
        }
    }
    protected void startPopup(boolean reset) {
        curButton = BUTTON_OK;        
    }
    protected void stopPopup() {
    }
    
    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB012012012204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 280, 415, 404, 79, this);
//        }
        g.setColor(Rs.C035035035);
        g.fillRect(280, 143, 402, 276);
//        if (imgPopBGBig != null) {
//            g.drawImage(imgPopBGBig, 280, 143, this);
//        }
//        if (imgPopGap != null) {
//            g.drawImage(imgPopGap, 292, 181, this);
//        }
        //Icon
        final int titleGap = 6;
        int titleAreaWth = 0;
        int imgPopIconWth = 0;
        if (imgPopIcon !=null) {
            imgPopIconWth = imgPopIcon.getWidth(this);
            titleAreaWth += imgPopIconWth;
            titleAreaWth += titleGap;
        }
        int titleWth = 0;
        if (title != null) {
            titleWth = Rs.FM24.stringWidth(title);
            titleAreaWth += titleWth;
        }
        int xPos = 480 - (titleAreaWth/2);
        if (imgPopIcon !=null) {
            g.drawImage(imgPopIcon, xPos, 153, this) ;
            xPos += imgPopIconWth;
            xPos += titleGap;
        }
        if (titleWth > 0) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(title, xPos, 172);
        }
        //Button
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<BUTTON_COUNT; i++) {
            Image imgButton = null;
            if (i == curButton) {
                imgButton = imgButtonF;
            } else {
                imgButton = imgButtonB;
            }
            if (imgButton != null) {
                g.drawImage(imgButton, 334, 321+(i*buttonGap), this);
            }
            String txtButton = null;
            if (i == BUTTON_OK) {
                txtButton = (String) dCenter.get("TxtQSW.Create_Administrator_PIN_Code");
            } else if (i == BUTTON_CANCEL){
                txtButton = (String) dCenter.get("TxtQSW.Cancel");
            }
            if (txtButton != null) {
                int txtButtonWth = Rs.FM18.stringWidth(txtButton);
                g.drawString(txtButton, 481-(txtButtonWth/2), 342+(i*buttonGap));
            }
        }
        //Content
        if (content != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C229229229);
            String[] conts = TextUtil.split(content, Rs.FM18, POPUP_SIZE_VALID_W, '|');
            int contsCnt = conts.length;
            if (POPUP_SIZE_MAX_LINE < contsCnt) {
                contsCnt = POPUP_SIZE_MAX_LINE;
            }
            int posY = 0;
            switch (contsCnt) {
                case 1:
                    posY = 254;
                    break;
                case 2:
                    posY = 240;
                    break;
                case 3:
                    posY = 240;
                    break;
                case 4:
                    posY = 224;
                    break;
                default:
                    posY = 218;
                    break;
            }
            for (int i = 0; i < contsCnt; i++) {
                if (conts[i] != null) {
                    int contWth = Rs.FM18.stringWidth(conts[i]);
                    g.drawString(conts[i], 480 - (contWth / 2), posY + (i * 22));
                }
            }
        }
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                if (curButton == BUTTON_OK) {
                    return true;
                }
                curButton = BUTTON_OK;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (curButton == BUTTON_CANCEL) {
                    return true;
                }
                curButton = BUTTON_CANCEL;
                repaint();
                return true;
            case Rs.KEY_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
            case Rs.KEY_OK:
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                effect.start(334, 321+(curButton * buttonGap), 294, 33); 
                if (curButton == BUTTON_OK) {
                    pListener.popupOK(new PopupEvent(this));
                } else {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    public int getCurrentButton() {
        return curButton;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    
    public void setContent(String content) {
        this.content = content;
    }
}

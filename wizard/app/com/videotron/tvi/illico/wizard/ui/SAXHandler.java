package com.videotron.tvi.illico.wizard.ui;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

abstract public class SAXHandler extends DefaultHandler {
    protected Object resultObject;
    private boolean isInvalidData;
    private String latestXMLTag;

    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        latestXMLTag = qName;
        if (qName != null) {
            latestXMLTag = qName.trim();
            startElement(latestXMLTag, attributes);
        }
    }
    public void endElement(String uri, String localName, String qName) {
        if (qName != null) {
            endElement(qName.trim());
        }
        latestXMLTag = null;
    }

    public void characters(char[] ch, int start, int length) {
        if (isInvalidData) {
            return;
        }
        if (ch != null) {
            parseCDData(latestXMLTag, new String(ch, start, length).trim());
        }
    }

    public Object getResultObject() {
        return resultObject;
    }

    public void setResultObject(Object resultObject) {
        this.resultObject = resultObject;
    }

    protected void setIsInvalidData(boolean isInvalidData) {
        this.isInvalidData = isInvalidData;
    }

//    public void error(SAXParseException e) {
//        System.out.println("saxHandler.error(SAXParseException) called");
//    }
//
//    public void fatalError(SAXParseException e) {
//        System.out.println("saxHandler.fatalError(SAXParseException) called");
//    }
//
//    public void warn(SAXParseException e) {
//        System.out.println("saxHandler.warn(SAXParseException) called");
//    }

    abstract public void startDocument();

    abstract public void endDocument();

    abstract public void startElement(String valueXMLTag, Attributes valueAttributes);

    abstract public void endElement(String valueXMLTag);

    abstract public void parseCDData(String latestXMLTag, String valueCDData);
}

package com.videotron.tvi.illico.wizard.ui;

import java.awt.Image;
import java.awt.Point;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.controller.DataManager;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererWelcome;
import com.videotron.tvi.illico.wizard.migration.MigrationAdapter;

public class SceneWelcome extends Scene {
    private static final long serialVersionUID = -8319423869576097504L;
    private boolean isNeedMigrationSetting;
    private boolean executedMigrationSetting;
    private MigrationTimer mTimer;

    private Image imgButtonOK;
    private ImageLabelButton imgLbBut;

    protected void initScene() {
        mTimer = new MigrationTimer();
        renderer = new RendererWelcome();
        setRenderer(renderer);
        imgButtonOK = DataCenter.getInstance().getImage("ok_btn.png");
        imgLbBut = new ImageLabelButton(imgButtonOK);
        imgLbBut.setBounds(440, 386, 81, 81);
    }
    protected void disposeScene() {
        if (imgLbBut != null) {
            imgLbBut.flush();
            imgLbBut = null;
        }
        if (imgButtonOK != null) {
            DataCenter.getInstance().removeImage("ok_btn.png");
            imgButtonOK = null;
        }
        renderer = null;
        mTimer = null;
    }
    protected void startScene(boolean isReset) {
        prepare();
        //executedMigrationSetting = false;
        isNeedMigrationSetting = DataManager.getInstance().isNeedMigrationSetting();
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneWelcome.startScene]Need migration setting : " + isNeedMigrationSetting);
        }
        if (isNeedMigrationSetting) {
            if (!executedMigrationSetting) {
                mTimer.startMigrationTimer();
            } else {
                new Thread(Rs.APP_NAME + " - Execute requestLoading") {
                    public void run() {
                        for (int i = 0; i < 7; i++) {
                            try {
                                Thread.sleep(1000L);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            if (!MigrationAdapter.getInstance().isNeedUpgrade()) { break; }
                            requestLoading(true);
                        }
                    }
                }.start();
                //CommunicationManager.getInstance().requestShowLoadingAnimation(new Point(805, 403));
            }
        } else {
        	add(imgLbBut);
        }
    }
    
    /**
     * Request loading animation to show or hide.
     *
     * @param isShowing the is showing
     */
    private synchronized void requestLoading(boolean isShowing) {
        if (isShowing) {
            if (MigrationAdapter.getInstance().isNeedUpgrade()) {
                CommunicationManager.getInstance().requestShowNotDelayLoadingAnimation(new Point(805, 403));
            }
        } else { CommunicationManager.getInstance().requestHideLoadingAnimation(); }
    }
    
    protected void stopScene() {
        mTimer.stopMigrationTimer();
        remove(imgLbBut);
        CommunicationManager.getInstance().requestHideLoadingAnimation();
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyCodes.LAST:
                if (!DataManager.getInstance().isCalledByMonitor()) {
                    String calledByValue = DataManager.getInstance().getCalldByValue();
                    if (calledByValue != null) {
                        CommunicationManager.getInstance().requestStartUnboundApplication(calledByValue, null);
                    }
                }
                return true;
            case Rs.KEY_OK:
                if (MigrationAdapter.getInstance().isNeedUpgrade() && executedMigrationSetting) { return true; }
                if (!isNeedMigrationSetting && imgLbBut != null) {
                    imgLbBut.startEffect();
                }
                if (isNeedMigrationSetting && !executedMigrationSetting) {
                    mTimer.stopMigrationTimer();
                    executeMigrationSetting(true);
                } else {
                    SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_NAVIGATION, true);
                }
                return true;
            case KeyCodes.MENU:
            case HRcEvent.VK_GUIDE:
            case KeyCodes.SETTINGS:
            case KeyCodes.LIST:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
            case KeyCodes.WIDGET:
            case KeyCodes.SEARCH:
            case KeyCodes.VOD:
                if (MigrationAdapter.getInstance().isNeedUpgrade() && executedMigrationSetting) { return true; }
                break;
        }
        return false;
    }
    
    public boolean isNeedMigrationSetting() {
    	return isNeedMigrationSetting;
    }
    
    synchronized private void executeMigrationSetting(final boolean isUserAction) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneWelcome.executeMigrationSetting]called. : " + executedMigrationSetting);
        }
        if (!executedMigrationSetting) {
            executedMigrationSetting = true;
            CommunicationManager.getInstance().requestShowNotDelayLoadingAnimation(new Point(805, 403));
            new Thread(Rs.APP_NAME + " - Execute migration settings") {
                public void run() {
                    while (true) {
                        if (MigrationAdapter.getInstance().isInitData()) { break; }
                        try {
                            Thread.sleep(1000L);
                        } catch (Exception e) {}
                    }
                    DataManager.getInstance().executeMigrationSetting();
                    requestLoading(false);
                    //CommunicationManager.getInstance().requestHideLoadingAnimation();
                    /*if (isUserAction) {
                        SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_NAVIGATION, true);
                    }*/
                    isNeedMigrationSetting = false;
                    add(imgLbBut);
                    repaint();
                }
            }.start();
        }
    }
    class MigrationTimer implements TVTimerWentOffListener {
        private TVTimerSpec timer;
        public MigrationTimer() {
            long mWMillis = 6000;
            try{
                String mWMillisValue = DataCenter.getInstance().getString("MIGRATION_WATING_MILLIS");
                mWMillis = Long.parseLong(mWMillisValue);
            }catch(Exception ignore) {
            }
            timer = new TVTimerSpec();
            timer.setDelayTime(mWMillis);
            timer.setRepeat(false);
            timer.addTVTimerWentOffListener(this);
        }
        public void startMigrationTimer() {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneWelcome.startMigrationTimer]called.");
            }
            stopMigrationTimer();
            if (timer != null) {
                try{
                    TVTimer.getTimer().scheduleTimerSpec(timer);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        public void stopMigrationTimer(){
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneWelcome.stopMigrationTimer]called.");
            }
            if (timer != null) {
                try{
                    TVTimer.getTimer().deschedule(timer);
                }catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        public void timerWentOff(TVTimerWentOffEvent arg0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneWelcome.timerWentOff]called.");
            }
            executeMigrationSetting(false);
        }
    }
}

package com.videotron.tvi.illico.wizard.ui;

import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ui.ImageLabel;

public class ImageLabelButton extends ImageLabel implements AnimationRequestor  {
    private static final long serialVersionUID = 5553909373896053147L;
    public ImageLabelButton(Image src) {
        super(src);
    }
    public void startEffect() {
        Rectangle r = getBounds();
        ClickingEffect effect = new ClickingEffect(this, 5);
        effect.updateBackgroundBeforeStart(false);
        effect.start(0, 0, r.width, r.height);
    }
    public boolean skipAnimation(Effect effect) {
        return false;
    }
    public void animationStarted(Effect effect) {
    }
    public void animationEnded(Effect effect) {
    }
}

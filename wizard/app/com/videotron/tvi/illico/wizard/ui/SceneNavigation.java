package com.videotron.tvi.illico.wizard.ui;

import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererNavigation;

public class SceneNavigation extends Scene {
    private static final long serialVersionUID = -8319423869576097504L;
    
    private ComponentTitle compTitle;
    
    protected void initScene() {
        renderer = new RendererNavigation();
        setRenderer(renderer);
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(53, 17, 424, 72);
            add(compTitle);
        }
    }
    protected void disposeScene() {
        if (compTitle!= null) {
            remove(compTitle);
            compTitle.dispose();
            compTitle = null;
        }
        renderer = null;
    }
    protected void startScene(boolean isReset) {
        prepare();
    }
    protected void stopScene() {
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_OK:
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LANGUAGE, true);
                return true;
        }
        return false;
    }
}

package com.videotron.tvi.illico.wizard.ui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.wizard.Rs;

public class ComponentStep extends Component {
    private static final long serialVersionUID = 1L;
    private DataCenter dCenter = DataCenter.getInstance();

    private static final int STEP_ID_COUNT = 5;
    public static final int STEP_ID_LANGUAGE = 0;
    public static final int STEP_ID_ADMIN_PIN = 1;
    public static final int STEP_ID_PARENTAL_CONTROL = 2;
    public static final int STEP_ID_ORDER_PIN = 3;
    public static final int STEP_ID_POWER_ON_CHANNEL = 4;
//    public static final int STEP_ID_COMPLETE = 5;
    private int curStep;
    
    public static final int STEP_GAP = 8;

    private Image imgStepSeparator;
    private Image[] imgStepNumberFs;
    private Image[] imgStepNumbers;
    private String[] txtKeySteps;
    private int[] stepIds;

    public ComponentStep() {
        if (imgStepSeparator == null) {
            imgStepSeparator = dCenter.getImage("12_sep.png");
        }
        if (imgStepNumberFs == null) {
            imgStepNumberFs = new Image[STEP_ID_COUNT];
            imgStepNumberFs[STEP_ID_LANGUAGE] = dCenter.getImage("12_1_foc.png");
            imgStepNumberFs[STEP_ID_ADMIN_PIN] = dCenter.getImage("12_2_foc.png");
            imgStepNumberFs[STEP_ID_PARENTAL_CONTROL] = dCenter.getImage("12_3_foc.png");
            imgStepNumberFs[STEP_ID_ORDER_PIN] = dCenter.getImage("12_4_foc.png");
            imgStepNumberFs[STEP_ID_POWER_ON_CHANNEL] = dCenter.getImage("12_5_foc.png");
        }
        if (imgStepNumbers == null) {
            imgStepNumbers = new Image[STEP_ID_COUNT];
            imgStepNumbers[STEP_ID_LANGUAGE] = dCenter.getImage("12_1.png");
            imgStepNumbers[STEP_ID_ADMIN_PIN] = dCenter.getImage("12_2.png");
            imgStepNumbers[STEP_ID_PARENTAL_CONTROL] = dCenter.getImage("12_3.png");
            imgStepNumbers[STEP_ID_ORDER_PIN] = dCenter.getImage("12_4.png");
            imgStepNumbers[STEP_ID_POWER_ON_CHANNEL] = dCenter.getImage("12_5.png");
        }
        if (txtKeySteps == null) {
            txtKeySteps = new String[STEP_ID_COUNT];
            txtKeySteps[STEP_ID_LANGUAGE] = "TxtQSW.Language";
            txtKeySteps[STEP_ID_ADMIN_PIN] = "TxtQSW.Admin_PIN";
            txtKeySteps[STEP_ID_PARENTAL_CONTROL] = "TxtQSW.Parental_Control";
            txtKeySteps[STEP_ID_ORDER_PIN] = "TxtQSW.Order_Pin";
            txtKeySteps[STEP_ID_POWER_ON_CHANNEL] = "TxtQSW.Power_On_Channel";
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        txtKeySteps = null;
        stepIds = null;
        dCenter.removeImage("12_sep.png");
        imgStepSeparator = null;
        if (imgStepNumberFs != null) {
            dCenter.removeImage("12_1_foc.png");
            imgStepNumberFs[STEP_ID_LANGUAGE] = null;
            dCenter.removeImage("12_2_foc.png");
            imgStepNumberFs[STEP_ID_ADMIN_PIN] = null;
            dCenter.removeImage("12_3_foc.png");
            imgStepNumberFs[STEP_ID_PARENTAL_CONTROL] = null;
            dCenter.removeImage("12_4_foc.png");
            imgStepNumberFs[STEP_ID_ORDER_PIN] = null;
            dCenter.removeImage("12_5_foc.png");
            imgStepNumberFs[STEP_ID_POWER_ON_CHANNEL] = null;
            imgStepNumberFs = null;
        }
        if (imgStepNumbers != null) {
            dCenter.removeImage("12_1.png");
            imgStepNumbers[STEP_ID_LANGUAGE] = null;
            dCenter.removeImage("12_2.png");
            imgStepNumbers[STEP_ID_ADMIN_PIN] = null;
            dCenter.removeImage("12_3.png");
            imgStepNumbers[STEP_ID_PARENTAL_CONTROL] = null;
            dCenter.removeImage("12_4.png");
            imgStepNumbers[STEP_ID_ORDER_PIN] = null;
            dCenter.removeImage("12_5.png");
            imgStepNumbers[STEP_ID_POWER_ON_CHANNEL] = null;
            imgStepNumbers = null;
        }
    }
    public void paint(Graphics g) {
        if (stepIds == null) {
            return;
        }
        //draw Step
        int xPos = 51;
        for (int i = 0; i < stepIds.length; i++) {
            String txtStep = dCenter.getString(txtKeySteps[stepIds[i]]);
            if (txtStep == null) {
                txtStep = "";
            }
            Color cStep = null;
            Font fStep = null;
            FontMetrics fmStep = null;
            if (stepIds[i] == curStep) {
                if (imgStepNumberFs[i] != null) {
                    g.drawImage(imgStepNumberFs[i], xPos, 0, this);
                    xPos += imgStepNumberFs[i].getWidth(this);
                    xPos += STEP_GAP;
                }
                cStep = Rs.C255204000;
                fStep = Rs.F30;
                fmStep = Rs.FM30;
            } else {
                if (imgStepNumbers[i] != null) {
                    g.drawImage(imgStepNumbers[i], xPos, 4, this);
                    xPos += imgStepNumbers[i].getWidth(this) - 3;
                    xPos += STEP_GAP;
                }
                cStep = Rs.C133133133;
                fStep = Rs.F20;
                fmStep = Rs.FM20;
            }
            g.setFont(fStep);
            g.setColor(cStep);
            g.drawString(txtStep, xPos, 22);
            if (i == stepIds.length-1) {
                continue;
            }
            xPos += fmStep.stringWidth(txtStep);
            xPos += STEP_GAP;
            if (imgStepSeparator != null) {
                g.drawImage(imgStepSeparator, xPos, 3, this);
                xPos += imgStepSeparator.getWidth(this);
            }
            xPos += STEP_GAP;
        }
    }
    public void setCurrentStep(int currentStep) {
        curStep = currentStep;
    }
    public void setStepIds(int[] stepIds) {
        this.stepIds = stepIds;
    }
}

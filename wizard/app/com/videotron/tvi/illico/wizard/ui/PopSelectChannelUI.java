/**
 * @(#)PopSelectChannelUI.java Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.wizard.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.popup.Popup;
import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;
import java.util.Hashtable;

/**
 * Display a channel data list to be selected.
 *
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectChannelUI extends Popup {

	/**
	 * A index of channel number in channel data.
	 */
	public static final int INDEX_CHANNEL_NUMBER = 0;
	/**
	 * A index of channel name in channel data.
	 */
	public static final int INDEX_CHANNEL_NAME = 1;
	/**
	 * A index of channel type in channel data.
	 */
	public static final int INDEX_CHANNEL_TYPE = 4;
	/**
	 * A index of channel source id in channel data.
	 */
	public static final int INDEX_CHANNEL_SOURCE_ID = 5;
	/**
	 * A index of channel full name in channel data.
	 */
	public static final int INDEX_CHANNEL_FULL_NAME = 6;

	public static final int INDEX_CHANNEL_SISTER_NAME = 7;
	public static final int INDEX_CHANNEL_DEFINITION = 8;

	public static final int ACTION_OK = 1;
	public static final int ACTION_CANCEL = 2;

	/**
	 * Renderer.
	 */
	private PopSelectChannelRenderer renderer = new PopSelectChannelRenderer();
	/**
	 * Parent UI.
	 */
	private ScenePowerOnChannel parent;
	/**
	 * Channel data.
	 */
	private Object[][] values;
	/**
	 * X coordinate to move.
	 */
	private int moveX = 0;
	/**
	 * Y coordinate to move.
	 */
	private int moveY = 0;
	/**
	 * Indicate How many lines display.
	 */
	private int line = 3;
	/**
	 * Indicate a half line.
	 */
	private int halfLine = 1;
	/**
	 * Indicate a start index.
	 */
	private int startIdx = 0;
	/**
	 * Indicate a y coordinate for focus.
	 */
	private int focusY = 0;
	/**
	 * Indicate a length of favourite channel.
	 */
	private String[] favoriteList;
	/**
	 * title.
	 */
	private String title;

	/**
	 * String buffer to handle a direct input.
	 */
	private StringBuffer numBuffer;

	private int count;
	private boolean runningAutoFinder;

	private Hashtable sisterTable;

	private int focus;

	/**
	 * Image for bottom part of background.
	 */
	Image bgBottomImg;
	/**
	 * Image for top part of background.
	 */
	Image bgTopImg;
	/**
	 * Image for middle part of background.
	 */
	Image bgMidImg;
	/** Image for top part of shadow. */
	// Image shadowTopImg;
	/** Image for bottom part of shadow. */
	// Image shadowBottomImg;
	/**
	 * Image for up arrow.
	 */
	Image arrowUpImg;
	/**
	 * Image for down arrow.
	 */
	Image arrowDownImg;
	/**
	 * Image for focus.
	 */
	Image focusImg;
	/**
	 * Image to indicate a favourite channel.
	 */
	Image starImg;
	/**
	 * Image to indicate a favourite channel when have a focus.
	 */
	Image starFocusImg;

	/**
	 * Color for focus (1, 1, 1).
	 */
	Color focusColor = new Color(1, 1, 1);
	/**
	 * Color for dimmed (193, 191, 191).
	 */
	Color dimmedColor = new Color(193, 191, 191);
	/**
	 * Color for background of title (78, 78, 78).
	 */
	Color titleBgColor = new Color(78, 78, 78);
	/**
	 * Color for foreground of title (214, 182, 55).
	 */
	Color titleFgColor = new Color(251, 217, 89);
	/**
	 * Color for shadow of title.
	 */
	Color titleShadowColor = new Color(27, 24, 12);
	/**
	 * Color for dimmed background.
	 */
	DVBColor dimmedBackColor = new DVBColor(0, 0, 0, 153);

	/**
	 * Set a renderer.
	 */
	public PopSelectChannelUI() {
	}

	/**
	 * Show a popup.
	 *
	 * @param com          parent UI.
	 * @param data         channel Data.
	 * @param current      current value.
	 * @param title        title
	 * @param favortieList favourite channel array
	 * @param x            x coordinate to move.
	 * @param y            y coordinate to move.
	 * @param line         how many line display.
	 */
	public void setData(ScenePowerOnChannel com, Object data, Object current, String title, String[] favortieList,
			int x, int y, int line) {
		this.moveX = x;
		this.moveY = y;
		this.title = title;
		this.favoriteList = favortieList;
		this.line = line;

		this.parent = com;
		values = (Object[][]) data;
		focus = findFocus((String) current);
		calculateIndex();
		focusY = 27 + 16 + (halfLine - 2) * 27;

		if (numBuffer != null) {
			numBuffer.setLength(0);
		}
	}

	/**
	 * Calculate a index.
	 */
	private void calculateIndex() {
		halfLine = line / 2;
		startIdx = (values.length - halfLine + focus) % values.length;
		if (line % 2 == 1)
			halfLine += 1;
	}

	/**
	 * Find a focus with current.
	 *
	 * @param current current channel.
	 * @return index of focus.
	 */
	private int findFocus(String current) {
		for (int i = 0; i < values.length; i++) {
			String name = (String) values[i][INDEX_CHANNEL_NAME];
			if (name.equals(current)) {
				return i;
			} else {
				if (sisterTable.get(name) != null && PreferenceProxy.getInstance().isChannelGrouped()) {
					Object[] sisterChannelData = (Object[]) sisterTable.get(name);
					String sisterCallLetter = null;
					sisterCallLetter = (String) sisterChannelData[INDEX_CHANNEL_NAME];
					if (sisterCallLetter != null && sisterCallLetter.equals(current)) {
						return i;
					}
				}
			}
		}

		return 0;
	}

	private int findFocusWithNumber(String targetNumber) {
		Integer targetInt = Integer.valueOf(targetNumber);
		for (int i = 0; i < values.length; i++) {
			Integer number = (Integer) values[i][INDEX_CHANNEL_NUMBER];
			if (number.intValue() == targetInt.intValue()) {
				return i;
			}

			if (PreferenceProxy.getInstance().isChannelGrouped()) {
				Object[] sisterChannelData = (Object[]) sisterTable.get(values[i][INDEX_CHANNEL_NAME]);

				try {
					if (sisterChannelData != null
							&& targetInt.intValue() == ((Integer) sisterChannelData[INDEX_CHANNEL_NUMBER]).intValue()) {
						return i;
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}

		return -1;
	}

	public void setSisterTable(Hashtable table) {
		sisterTable = table;
	}

	public void setRunningAutoFinder(boolean flag) {
		runningAutoFinder = flag;
	}

	public boolean isRunningAutoFinder() {
		return runningAutoFinder;
	}

	/**
	 * Get a selected value.
	 *
	 * @return selected value.
	 */
	public Object getSelectedValue() {
		return values[focus];
	}

	private boolean isFavoriteChannel(String name) {
		for (int i = 0; i < favoriteList.length; i++) {
			if (favoriteList[i].equals(name)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Handle a key code.
	 *
	 * @param code key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {

		switch (code) {
		case OCRcEvent.VK_0:
		case OCRcEvent.VK_1:
		case OCRcEvent.VK_2:
		case OCRcEvent.VK_3:
		case OCRcEvent.VK_4:
		case OCRcEvent.VK_5:
		case OCRcEvent.VK_6:
		case OCRcEvent.VK_7:
		case OCRcEvent.VK_8:
		case OCRcEvent.VK_9:
			count = 0;
			numericKeys(code);
			repaint();
			return true;
		case OCRcEvent.VK_UP:
			count = 0;
			if (numBuffer != null) {
				numBuffer.setLength(0);
			}
			if (--focus < 0) {
				focus = values.length - 1;
			}
			calculateIndex();
			repaint();
			return true;
		case OCRcEvent.VK_DOWN:
			count = 0;
			if (numBuffer != null) {
				numBuffer.setLength(0);
			}
			if (++focus == values.length) {
				focus = 0;
			}
			calculateIndex();
			repaint();
			return true;
		case OCRcEvent.VK_ENTER:
			runningAutoFinder = false;
			parent.popupClosed(true);
			return true;
		case OCRcEvent.VK_EXIT:
			runningAutoFinder = false;
			parent.popupClosed(false);
			return true;
		default:
			return true;
		}
	}

	/**
	 * add for R5.
	 * Handle a numeric keys to input directly.
	 *
	 * @param code
	 */
	private void numericKeys(int code) {
		int number = code - OCRcEvent.VK_0;

		if (numBuffer == null) {
			numBuffer = new StringBuffer();
		}

		numBuffer.append(number);

		if (numBuffer.length() > 4) {
			numBuffer.setLength(0);
			return;
		}
	}

	protected void disposePopup() {
		bgBottomImg = null;
		bgTopImg = null;
		bgMidImg = null;
		arrowUpImg = null;
		arrowDownImg = null;
		focusImg = null;
		starImg = null;
		starFocusImg = null;
	}

	protected void startPopup(boolean reset) {
		bgBottomImg = DataCenter.getInstance().getImage("08_op_bg_b_304.png");
		bgTopImg = DataCenter.getInstance().getImage("08_op_bg_t_304.png");
		bgMidImg = DataCenter.getInstance().getImage("08_op_bg_m_304.png");
		arrowUpImg = DataCenter.getInstance().getImage("02_ars_t.png");
		arrowDownImg = DataCenter.getInstance().getImage("02_ars_b.png");
		focusImg = DataCenter.getInstance().getImage("08_op_foc_274.png");
		starImg = DataCenter.getInstance().getImage("02_icon_fav.png");
		starFocusImg = DataCenter.getInstance().getImage("fav-icon_foc.png");

		FrameworkMain.getInstance().getImagePool().waitForAll();

		setVisible(true);

		new Thread("CHANNEL_FINDER") {
			public void run() {
				count = 0;
				runningAutoFinder = true;
				try {
					while (runningAutoFinder) {
						Log.printDebug("CHANNEL_FINDER is running");
						Thread.sleep(500L);

						count++;

						if (count == 3) {
							count = 0;
							if (numBuffer != null && numBuffer.length() > 0) {
								int findFocus = findFocusWithNumber(numBuffer.toString());
								numBuffer.setLength(0);
								if (findFocus != -1) {
									focus = findFocus;
									calculateIndex();
								}
								repaint();
							}
						}
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}.start();
	}

	protected void stopPopup() {
		setVisible(false);
	}

	public boolean keyAction(int keyCode) {
		return handleKey(keyCode);
	}

	public void paint(Graphics g) {

		g.setColor(dimmedBackColor);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
		parent.paintSelectedContent(g);

		g.translate(moveX, moveY);

		g.drawImage(bgTopImg, 0, 27, this);
		int step = 46;
		for (int i = 1; i < line - 1; i++) {
			g.drawImage(bgMidImg, 0, step, this);
			step += 27;
		}
		g.drawImage(bgBottomImg, 0, step, this);

		Rectangle before = g.getClipBounds();

		g.clipRect(2, 27, 300, (line - 1) * 27 + 8);

		g.setFont(Rs.F18);
		int dataPosY = 37;
		int idx = startIdx;
		for (int i = 0; i < line; i++) {
			if (idx == focus) {
				g.setColor(focusColor);
				g.drawImage(focusImg, 2, focusY, this);
			} else {
				g.setColor(dimmedColor);
			}

			if (idx == focus && numBuffer != null && numBuffer.length() > 0) {
				g.drawString(numBuffer.toString(), 41, dataPosY);
			} else {
				String name = (String) values[idx][INDEX_CHANNEL_NAME];
				String number = TextUtil.EMPTY_STRING;
				if (!name.equals(Definitions.LAST_CHANNEL)) {
					String chNumber = String.valueOf(((Integer) values[idx][INDEX_CHANNEL_NUMBER]).intValue());
					int definition = ((Integer) values[idx][INDEX_CHANNEL_DEFINITION]).intValue();
					if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
						Object[] sisterChannelData = (Object[]) sisterTable.get(name);
						if (sisterChannelData != null && PreferenceProxy.getInstance().isChannelGrouped()) {
							// has sister channel
							String sisNumber = String
									.valueOf(((Integer) sisterChannelData[INDEX_CHANNEL_NUMBER]).intValue());
							number = chNumber + " | " + sisNumber + " HD";
							name = (String) sisterChannelData[INDEX_CHANNEL_FULL_NAME];
						} else {
							if (definition == TvChannel.DEFINITION_4K) {
								number = chNumber + " UHD";
							} else {
								number = chNumber;
							}

							name = (String) values[idx][INDEX_CHANNEL_FULL_NAME];
						}
						g.drawString(number, 41, dataPosY);
						name = TextUtil.shorten(name, Rs.FM18, 145);
						g.drawString(name, 144, dataPosY);
					} else {
						name = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
						g.drawString(name, 615, 161 + step);
					}
				} else {
					number = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
					name = TextUtil.EMPTY_STRING;

					g.drawString(number, 41, dataPosY);
				}

				name = (String) values[idx][INDEX_CHANNEL_NAME];
				if (idx != 0 && isFavoriteChannel(name)) {
					if (idx == focus) {
						g.drawImage(starFocusImg, 11, dataPosY - 18, this);
					} else {
						g.drawImage(starImg, 11, dataPosY - 18, this);
					}
				}
			}

			dataPosY += 27;
			if (++idx == values.length) {
				idx = 0;
			}
		}

		g.setClip(before);

		// g.drawImage(shadowTopImg, 4, 29, c);
		// g.drawImage(shadowBottomImg, 4, step - 11, c);

		// title
		g.setColor(titleBgColor);
		g.fillRect(2, 0, 300, 29);

		if (title != null) {
			// g.setColor(titleShadowColor);
			// g.drawString(title, 16, 21);
			g.setColor(titleFgColor);
			g.drawString(title, 15, 20);
		}

		g.drawImage(arrowUpImg, 126, -15, this);
		g.drawImage(arrowDownImg, 126, step + 16, this);
		g.translate(-moveX, -moveY);

		super.paint(g);
	}

	/**
	 * This class render a list to select a channel.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class PopSelectChannelRenderer extends Renderer {
		/**
		 * Image for bottom part of background.
		 */
		Image bgBottomImg;
		/**
		 * Image for top part of background.
		 */
		Image bgTopImg;
		/**
		 * Image for middle part of background.
		 */
		Image bgMidImg;
		/** Image for top part of shadow. */
		// Image shadowTopImg;
		/** Image for bottom part of shadow. */
		// Image shadowBottomImg;
		/**
		 * Image for up arrow.
		 */
		Image arrowUpImg;
		/**
		 * Image for down arrow.
		 */
		Image arrowDownImg;
		/**
		 * Image for focus.
		 */
		Image focusImg;
		/**
		 * Image to indicate a favourite channel.
		 */
		Image starImg;
		/**
		 * Image to indicate a favourite channel when have a focus.
		 */
		Image starFocusImg;

		/**
		 * Color for focus (1, 1, 1).
		 */
		Color focusColor = new Color(1, 1, 1);
		/**
		 * Color for dimmed (193, 191, 191).
		 */
		Color dimmedColor = new Color(193, 191, 191);
		/**
		 * Color for background of title (78, 78, 78).
		 */
		Color titleBgColor = new Color(78, 78, 78);
		/**
		 * Color for foreground of title (214, 182, 55).
		 */
		Color titleFgColor = new Color(251, 217, 89);
		/**
		 * Color for shadow of title.
		 */
		Color titleShadowColor = new Color(27, 24, 12);
		/**
		 * Color for dimmed background.
		 */
		DVBColor dimmedBackColor = new DVBColor(0, 0, 0, 153);

		public Rectangle getPreferredBounds(UIComponent c) {
			return c.getBounds();
		}

		public void prepare(UIComponent c) {
			bgBottomImg = DataCenter.getInstance().getImage("08_op_bg_b_304.png");
			bgTopImg = DataCenter.getInstance().getImage("08_op_bg_t_304.png");
			bgMidImg = DataCenter.getInstance().getImage("08_op_bg_m_304.png");
			arrowUpImg = DataCenter.getInstance().getImage("02_ars_t.png");
			arrowDownImg = DataCenter.getInstance().getImage("02_ars_b.png");
			focusImg = DataCenter.getInstance().getImage("08_op_foc_274.png");
			starImg = DataCenter.getInstance().getImage("02_icon_fav.png");
			starFocusImg = DataCenter.getInstance().getImage("fav-icon_foc.png");
		}

		/**
		 * Paint a renderer.
		 *
		 * @param g Graphics to paint.
		 * @param c parent UI.
		 */
		protected void paint(Graphics g, UIComponent c) {
			g.setColor(dimmedBackColor);
			g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
			parent.paintSelectedContent(g);

			g.translate(moveX, moveY);

			g.drawImage(bgTopImg, 0, 27, c);
			int step = 46;
			for (int i = 1; i < line - 1; i++) {
				g.drawImage(bgMidImg, 0, step, c);
				step += 27;
			}
			g.drawImage(bgBottomImg, 0, step, c);

			Rectangle before = g.getClipBounds();

			g.clipRect(2, 27, 300, (line - 1) * 27 + 8);

			g.setFont(Rs.F18);
			int dataPosY = 37;
			int idx = startIdx;
			for (int i = 0; i < line; i++) {
				if (idx == focus) {
					g.setColor(focusColor);
					g.drawImage(focusImg, 2, focusY, c);
				} else {
					g.setColor(dimmedColor);
				}

				if (idx == focus && numBuffer != null && numBuffer.length() > 0) {
					g.drawString(numBuffer.toString(), 41, dataPosY);
				} else {
					String name = (String) values[idx][INDEX_CHANNEL_NAME];
					String number = TextUtil.EMPTY_STRING;
					if (!name.equals(Definitions.LAST_CHANNEL)) {
						String chNumber = String.valueOf(((Integer) values[idx][INDEX_CHANNEL_NUMBER]).intValue());
						int definition = ((Integer) values[idx][INDEX_CHANNEL_DEFINITION]).intValue();
						if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
							Object[] sisterChannelData = (Object[]) sisterTable.get(name);
							if (sisterChannelData != null && PreferenceProxy.getInstance().isChannelGrouped()) {
								// has sister channel
								String sisNumber = String
										.valueOf(((Integer) sisterChannelData[INDEX_CHANNEL_NUMBER]).intValue());
								number = chNumber + " | " + sisNumber + " HD";
								name = (String) sisterChannelData[INDEX_CHANNEL_FULL_NAME];
							} else {
								if (definition == TvChannel.DEFINITION_4K) {
									number = chNumber + " UHD";
								} else {
									number = chNumber;
								}

								name = (String) values[idx][INDEX_CHANNEL_FULL_NAME];
							}
							g.drawString(number, 41, dataPosY);
							name = TextUtil.shorten(name, Rs.FM18, 145);
							g.drawString(name, 144, dataPosY);
						} else {
							name = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
							g.drawString(name, 615, 161 + step);
						}
					} else {
						number = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
						name = TextUtil.EMPTY_STRING;

						g.drawString(number, 41, dataPosY);
					}

					name = (String) values[idx][INDEX_CHANNEL_NAME];
					if (idx != 0 && isFavoriteChannel(name)) {
						if (idx == focus) {
							g.drawImage(starFocusImg, 11, dataPosY - 18, c);
						} else {
							g.drawImage(starImg, 11, dataPosY - 18, c);
						}
					}
				}

				dataPosY += 27;
				if (++idx == values.length) {
					idx = 0;
				}
			}

			g.setClip(before);

			// g.drawImage(shadowTopImg, 4, 29, c);
			// g.drawImage(shadowBottomImg, 4, step - 11, c);

			// title
			g.setColor(titleBgColor);
			g.fillRect(2, 0, 300, 29);

			if (title != null) {
				// g.setColor(titleShadowColor);
				// g.drawString(title, 16, 21);
				g.setColor(titleFgColor);
				g.drawString(title, 15, 20);
			}

			g.drawImage(arrowUpImg, 126, -15, c);
			g.drawImage(arrowDownImg, 126, step + 16, c);
			g.translate(-moveX, -moveY);
		}
	}
}

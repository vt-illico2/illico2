package com.videotron.tvi.illico.wizard.ui;

import java.awt.Point;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.wizard.popup.PopupAdapter;
import com.videotron.tvi.illico.wizard.popup.PopupController;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;
import org.ocap.hardware.Host;
import org.xml.sax.helpers.DefaultHandler;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.Util;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.controller.DataManager;
import com.videotron.tvi.illico.wizard.data.ProxyInfo;
import com.videotron.tvi.illico.wizard.data.ServerInfo;
import com.videotron.tvi.illico.wizard.gui.RendererCompletion;

public class SceneCompletion extends Scene {
    private static final long serialVersionUID = 1719247830341282139L;
    //Component
    private ComponentTitle compTitle;
    //Competion type
    public static final int TYPE_INCOMPLETE_MIGRATION_PROBLEM = 0;
    public static final int TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM = 1;
    public static final int TYPE_INCOMPLETE_ACTIVATION_PROBLEM = 2;
    public static final int TYPE_COMPLETE_COMMON = 10;
    public static final int TYPE_COMPLETE_NON_PVR = 11;
    public static final int TYPE_COMPLETE_PVR = 12;
    
    private int curCompletionType;
    //Button type
    public static final int BUTTON_TYPE_BANNER = 0;
    public static final int BUTTON_TYPE_EXIT = 1;
    public static final int BUTTON_TYPE_RCU = 2;

    private int curButtonType;
    private String curLang;

    private boolean isCompletedProcess;
    private boolean isWorkingModem;
    private boolean isCompletedMigrationSetting;
    private boolean isActiavated;

    private PopupRCU popupRcu;
    private PopupAdapter popupAdapterRcu;

    protected void initScene() {
        renderer = new RendererCompletion();
        setRenderer(renderer);
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(53, 17, 424, 72);
            add(compTitle);
        }
    }
    protected void disposeScene() {
        if (compTitle!= null) {
            remove(compTitle);
            compTitle.dispose();
            compTitle = null;
        }
        renderer = null;
    }
    protected void startScene(boolean resetScene) {
        isCompletedProcess = false;
        isCompletedMigrationSetting=false;
        isWorkingModem=false;
        isActiavated=false;
        
        CommunicationManager.getInstance().requestShowLoadingAnimation(new Point(480, 270));
        curCompletionType = TYPE_COMPLETE_COMMON;
        new Thread() {
            public void run() {
                // Migration
                isCompletedMigrationSetting = PreferenceProxy.getInstance().isCompleteMigration();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[SceneCompletion.startScene]isCompletedMigrationSetting : "+isCompletedMigrationSetting);
                }
                if (!isCompletedMigrationSetting) {
                    PreferenceProxy.getInstance().setCompleteMigration(true);
                }
                // IP Communication
                isWorkingModem = checkModem();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[SceneCompletion.startScene]isWorkingModem : "+isWorkingModem);
                }
                //Test App Start
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[SceneCompletion.startScene]TEST MODE : always execute activedSTB method");
//                }
//                isActiavated = activatedSTB();
                //Test App End
                
                //Real App Start
             // The Activation Service shall only be contacted when the Wizard is started automatically at boot time
                boolean isCalledByMonitor=DataManager.getInstance().isCalledByMonitor();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[SceneCompletion.startScene]isCalledByMonitor : "+isCalledByMonitor);
                }
                if (isCalledByMonitor) {
                    isActiavated = activatedSTB();
                } else {
                    isActiavated = true;
                }
              //Real App End
                if (Log.DEBUG_ON) {
                    Log.printDebug("[SceneCompletion.startScene]isActiavated : "+isActiavated);
                }
                // Set type
                boolean isNeedMigrationSetting = PreferenceProxy.getInstance().isNeedMigration();
                if (!isCompletedMigrationSetting) {
                    curCompletionType = TYPE_INCOMPLETE_MIGRATION_PROBLEM;
                } else if (!isWorkingModem) {
                    curCompletionType = TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM;
                } else if (!isActiavated) {
                    curCompletionType = TYPE_INCOMPLETE_ACTIVATION_PROBLEM;
                } else {
//                    boolean isNeedMigrationSetting = DataManager.getInstance().isNeedMigrationSetting();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[SceneCompletion.startScene]isNeedMigrationSetting : " + isNeedMigrationSetting);
                    }
                    if (isNeedMigrationSetting) {
                        if (Environment.SUPPORT_DVR) {
                            curCompletionType = TYPE_COMPLETE_PVR;
                        } else {
                            curCompletionType = TYPE_COMPLETE_NON_PVR;
                        }
                    } else {
                        curCompletionType = TYPE_COMPLETE_COMMON;
                    }
                }
                if (isNeedMigrationSetting) {
                	PreferenceProxy.getInstance().setFlagNeedMigration(Definitions.OPTION_VALUE_NO);
                }
                //TODO test variable
//                curCompletionType = TYPE_INCOMPLETE_ACTIVATION_PROBLEM;
                switch(curCompletionType) {
                    case TYPE_INCOMPLETE_MIGRATION_PROBLEM:
                    case TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM:
                    case TYPE_INCOMPLETE_ACTIVATION_PROBLEM:
                        curButtonType = BUTTON_TYPE_EXIT;
                        break;
                    case TYPE_COMPLETE_COMMON:
                        curButtonType = BUTTON_TYPE_EXIT;
                        break;
                    case TYPE_COMPLETE_NON_PVR:
                    case TYPE_COMPLETE_PVR:
                        curButtonType = BUTTON_TYPE_BANNER;
                        break;
                }
                CommunicationManager.getInstance().requestHideLoadingAnimation();
                isCompletedProcess = true;
                repaint();
            }
        }.start();
        prepare();
    }
    protected void stopScene() {
        curLang = null;
        isCompletedProcess = false;
    }
    protected boolean keyAction(int keyCode) {
        if (CommunicationManager.getInstance().isLoadingAnimationService()) {
            return true;
        }
        switch (keyCode) {
            case Rs.KEY_UP:
                switch(curCompletionType) {
                    case TYPE_INCOMPLETE_MIGRATION_PROBLEM:
                    case TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM:
                    case TYPE_INCOMPLETE_ACTIVATION_PROBLEM:
                        return true;
                    case TYPE_COMPLETE_COMMON:
                        if (curButtonType == BUTTON_TYPE_BANNER) {
                            curButtonType = BUTTON_TYPE_RCU;
                            repaint();
                        } else if (curButtonType == BUTTON_TYPE_RCU) {
                            curButtonType = BUTTON_TYPE_EXIT;
                            repaint();
                        }
                        return true;
                }
                if (curButtonType == BUTTON_TYPE_BANNER) {
                    return true;
                }
                curButtonType = BUTTON_TYPE_BANNER;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                switch(curCompletionType) {
                    case TYPE_INCOMPLETE_MIGRATION_PROBLEM:
                    case TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM:
                    case TYPE_INCOMPLETE_ACTIVATION_PROBLEM:
                        return true;
                    case TYPE_COMPLETE_COMMON:
                        if (curButtonType == BUTTON_TYPE_EXIT) {
                            curButtonType = BUTTON_TYPE_RCU;
                            repaint();
                        } else if (curButtonType == BUTTON_TYPE_RCU) {
                            curButtonType = BUTTON_TYPE_BANNER;
                            repaint();
                        }
                    	return true;
                }
                if (curButtonType == BUTTON_TYPE_EXIT) {
                    return true;
                }
                curButtonType = BUTTON_TYPE_EXIT;
                repaint();
                return true;
                
            case Rs.KEY_LEFT:
            	return true;
            	
            case Rs.KEY_RIGHT:
            	return true;
            	
            case Rs.KEY_OK:
                switch(curButtonType) {
                    case BUTTON_TYPE_EXIT:
                    	if (curCompletionType == TYPE_COMPLETE_COMMON) {
                    		new ClickingEffect(this, 5).start(342, 321, 276, 38);
                    	} else {
                    		new ClickingEffect(this, 5).start(280, 377, 396, 42);
                    	}
                        if (DataManager.getInstance().isCalledByMonitor() && isWorkingModem) {
                            PreferenceProxy.getInstance().setCompleteWizard();
                        }
                        CommunicationManager.getInstance().requestExitToChannel(CommunicationManager.EXIT_TYPE_COMPLETION);
                        break;
                    case BUTTON_TYPE_BANNER:
                    	if (curCompletionType == TYPE_COMPLETE_COMMON) {
                    		new ClickingEffect(this, 5).start(342, 409, 276, 38);
                    	} else {
                    		new ClickingEffect(this, 5).start(280, 296, 394, 71);
                    	}
                        if (DataManager.getInstance().isCalledByMonitor() && isWorkingModem) {
                            PreferenceProxy.getInstance().setCompleteWizard();
                        }
                        String categoryId = DataManager.getInstance().getCategoryId();
//                        if (categoryId == null) {
//                            if (Log.ERROR_ON) {
//                                Log.printError("[SceneCompletion.keyAction]BUTTON_TYPE_BANNER - Category id is null. Do not execute banner button.");
//                            }
//                            return true;
//                        }
                        boolean result = CommunicationManager.getInstance().requestStartUnboundApplication(DataManager.APP_NAME_HELP,
                                (categoryId!=null) ? new String[]{null, Rs.APP_NAME, DataManager.APP_NAME_VOD, categoryId} : new String[]{null, Rs.APP_NAME});
                        if (!result) {
                            if (Log.ERROR_ON) {
                                Log.printError("["+Rs.ERROR_CODE_VOD_ACCESS_PROBLEM+"] Cannot contact the VOD application to launch intro video.");
                            } 
                            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_VOD_ACCESS_PROBLEM);
                        }
                        break;
                    case BUTTON_TYPE_RCU:
                        if (popupRcu == null) {
                            popupRcu = new PopupRCU();
                            popupRcu.setBounds(Constants.SCREEN_BOUNDS);
                        }
                        new ClickingEffect(this, 5).start(342, 365, 276, 38);
                        popupRcu.setPopupListener(getPopupAdapterRcu());
                        PopupController.getInstance().openPopup(popupRcu, this, true);

                        break;
                }
                return true;
        }
        return false;
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public int getButtonType() {
        return curButtonType;
    }
    public int getCompletionType() {
        return curCompletionType;
    }
    public String getCurrentLanguage() {
        if (curLang == null) {
            curLang = PreferenceProxy.getInstance().getCurrentLanguage();
        }
        return curLang;
    }
    public boolean isCompletedProcess() {
        return isCompletedProcess;
    }
    public boolean isWorkingModem() {
        return isWorkingModem;
    }
    public boolean isActiavated() {
        return isActiavated;
    }
    public boolean isCompletedMigrationSetting() {
        return isCompletedMigrationSetting;
    }

    public PopupAdapter getPopupAdapterRcu() {
        if (popupAdapterRcu == null) {
            popupAdapterRcu = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupRCU pop = (PopupRCU)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneCompletion.this);
                    }
                }

                public void popupCancel(PopupEvent popupEvent) {
                    PopupRCU pop = (PopupRCU)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneCompletion.this);
                    }
                }
            };
        }

        return popupAdapterRcu;
    }

    /*********************************************************************************
     * check Modem Status-related
     *********************************************************************************/
    private boolean checkModem() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneCompletion.isWorkingModem]Start.");
        }
        int httpReqCount = DataManager.getInstance().getHttpRequestCount();
        if (httpReqCount <= 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneCompletion.isWorkingModem]HTTP Request count is zero and less. return true.");
            }
            return true;
        }
        ServerInfo[] serverInfoList = DataManager.getInstance().getServerInfoList();
        if (serverInfoList == null) {
            if (Log.ERROR_ON) {
                Log.printError("Cannot parse configuration files received from the Management System.");
            }                       
            return false;
        }
        int serverInfoListCount = serverInfoList.length;
        if (serverInfoListCount <= 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneCompletion.isWorkingModem]Server info count is zero and less. return true.");
            }
            return true;
        }
        boolean isSuccess = true;
        for (int i=0; i<httpReqCount; i++) {
            for (int j=0; j<serverInfoListCount; j++) {
                if (serverInfoList[j] == null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[SceneCompletion.isWorkingModem]Server info ["+i+"-"+j+"] is null. continue.");
                    }
                    continue;
                }
                String urlPath = getFullUrlString(serverInfoList[j]);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[SceneCompletion.isWorkingModem]URL path ["+i+"-"+j+"] : "+urlPath);
                }
                byte[] result = null;
                try{
                    result = URLRequestor.getBytes(urlPath, null);
                }catch(Exception e) {
                    e.printStackTrace();
                    if (Log.ERROR_ON) {
                        Log.printError("["+Rs.ERROR_CODE_TEST_SERVER_ACCESS_PROBLEM+"] Cannot communicate with the HTTP server used to test the HTTP connexion.");
                    } 
                    return false;
                }
                if (result == null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[SceneCompletion.isWorkingModem]["+i+"-"+j+"]th Http test fail.");
                    }
                    isSuccess = false;
                    break;
                }
            }
            if (!isSuccess) {
                break;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneCompletion.isWorkingModem]Http test finished. : "+isSuccess);
        }
        return isSuccess;
    }
    private boolean activatedSTB() {
        if (Log.INFO_ON) {
            Log.printInfo("[SceneCompletion.activatedSTB] called.");
        }
        boolean needActivationService=false;
        String[] actPackNames=CommunicationManager.getInstance().requestGetActivationPackageNames();
        if (actPackNames!=null) {
            Log.printInfo("[SceneCompletion.activatedSTB] Activation Package is existed. continue checking activation.");
            int actPackNamesLth=actPackNames.length;
            if (actPackNamesLth==0) {
                Log.printInfo("[SceneCompletion.activatedSTB] Activation Package size is zero. execute activation.");
                needActivationService=true;
            } else {
                int authIdx=-1;
                for (int i=0; i<actPackNamesLth; i++) {
                    if (actPackNames[i]!=null) {
                        int checkRes=CommunicationManager.getInstance().requestCheckResourceAuthorization(actPackNames[i]);
                        if (checkRes==MonitorService.AUTHORIZED) {
                            Log.printInfo("[SceneCompletion.activatedSTB] "+actPackNames[i]+" is authorized. execute activation.");
                            authIdx=i;
                            break;
                        }
                    }
                }
                if (authIdx!=-1) {
                    needActivationService=true;
                } else {
                    Log.printDebug("[SceneCompletion.activatedSTB] There are no authrized activation package names. deny activation.");
                    needActivationService=false;
                }
            }
        } else {
            Log.printDebug("[SceneCompletion.activatedSTB] Activation package names are null. execute activation.");
            needActivationService=true;
        }
        if (!needActivationService) {
            if (Log.INFO_ON) {
                Log.printInfo("[SceneCompletion.activatedSTB] Don't need to call activation service.");
            }
            return true;
        }
        // Get MacAddress
        byte[] mac = null;
        if (Environment.HW_VENDOR != Environment.HW_VENDOR_SAMSUNG) {
            String hostMAC = Host.getInstance().getReverseChannelMAC();
            mac = Util.macAddressFromStringToByte(hostMAC);
        } else {
            mac = CommunicationManager.getInstance().reqeustGetCableCardMacAddress();
        }
        String macAddr = null;
        if (mac != null) {
            macAddr = TextUtil.replace(Util.getHexStringBYTEArray(mac),  " ", "");
        }
        if (macAddr == null) {
            if (Log.ERROR_ON) {
                Log.printError("[SceneCompletion.activatedSTB] Invalid Mac Address.");
            }
            return false;
        }
        macAddr=macAddr.toUpperCase();
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneCompletion.activatedSTB] Mac Address : "+macAddr);
        }
        String fileName= "/IllicoActivate.do?mac="+macAddr;
        URL activationURL=null;
        try {
            activationURL=getUrl(DataManager.getInstance().getActicationServerInfo(), fileName);
            if (activationURL==null) {
                throw new Exception();
            }
        } catch (Exception e1) {
            if (Log.ERROR_ON) {
                Log.printError("[SceneCompletion.activatedSTB] Cannot correctly parse server configuration received from the carousels.");
            }
            e1.printStackTrace();
            return false;
        }
//        String urlPath = getFullUrlString(sInfo);
//        if (urlPath==null) {
//            if (Log.ERROR_ON) {
//                Log.printError("[SceneCompletion.activatedSTB] Invalid URL Path Data.");
//            }
//            return false;
//        }
//        urlPath=urlPath.trim();
//        if (urlPath.length()==0) {
//            if (Log.ERROR_ON) {
//                Log.printError("[SceneCompletion.activatedSTB] Invalid URL Path Data Size.");
//            }
//            return false;
//        }
//        if (urlPath.endsWith("/")) {
//            urlPath = urlPath.substring(0, urlPath.length() - 1);
//        }
//        urlPath += "/IllicoActivate.do?macId="+macAddr;
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[SceneCompletion.activatedSTB] Request URL Path : ["+urlPath+"]");
//        }
        // Parse XML
        DefaultHandler handler = new ActivatedResultHandler();
        HttpURLConnection con = null;
        InputStream in = null;
        try {
            try{
                con = (HttpURLConnection) activationURL.openConnection();
                con.getResponseCode();
                in = activationURL.openStream();
            }catch(Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("[SceneCompletion.activatedSTB] Cannot communicate with the Activation service.");
                }
                return false;
            }
            try {
                SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                parser.parse(in, handler);
            } catch (Exception e) {
                if (Log.ERROR_ON) {
                    Log.printError("[SceneCompletion.activatedSTB] Cannot parse the response received from the Activation service.");
                }
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        String successValue = null;
        if (saxHandler != null) {
            successValue = (String) saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneCompletion.activatedSTB] Success value : "+successValue);
        }
        if (successValue!=null && successValue.trim().equals("1")) {
            return true;
        }
        if (Log.WARNING_ON) {
            Log.printWarning("[SceneCompletion.activatedSTB] •  The Activation service returns a negative action response .");
        }
        return false;
    }
    protected String getFullUrlString(ServerInfo serverInfo) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getURL]Start.");
        }
        if (serverInfo == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getURL]Server info is null. return null.");
            }
            return null;
        }
        String host = serverInfo.getHost();
        int port = serverInfo.getPort();
        String contextRoot = serverInfo.getContextRoot();
        if (contextRoot == null) {
            contextRoot = "";
        } else {
            contextRoot = contextRoot.trim();
        }
        if (contextRoot.length() > 0) {
            if (contextRoot.startsWith("/")) {
                contextRoot = contextRoot.substring(1);
            }
            if (contextRoot.endsWith("/")) {
                contextRoot = contextRoot.substring(0, contextRoot.length() - 1);
            }
        }
        String urlData = "http://" + host + ":" + port + "/" + contextRoot;
        return urlData;
    }
    
    private URL getUrl(ServerInfo serverInfo, String fileName) throws Exception{
        if (Log.INFO_ON) {
            Log.printInfo("[SceneCompletion.getURL] Called.");
        }
        if (serverInfo == null) {
            if (Log.DEBUG_ON) {
                Log.printError("[SceneCompletion.getURL]Server info is null. return null.");
            }
            return null;
        }
        if (fileName == null || fileName.trim().length() == 0) {
            if (Log.DEBUG_ON) {
                Log.printError("[SceneCompletion.getURL]File name is null. return null.");
            }
            return null;
        }
        if (fileName.startsWith("/")) {
            fileName = fileName.substring(1);
        }
        String host = serverInfo.getHost();
        if (host == null) {
            if (Log.DEBUG_ON) {
                Log.printError("[SceneCompletion.getURL]Host infomation is null. return null.");
            }
            return null;
        }
        String urlInfo = "http://" + host + ":" + serverInfo.getPort();
        String fileInfo = "";
        String contextRoot = serverInfo.getContextRoot();
        if (contextRoot != null && contextRoot.trim().length() > 0) {
            if (!contextRoot.startsWith("/")) {
                contextRoot = "/" + contextRoot;
            }
            if (contextRoot.endsWith("/")) {
                contextRoot = contextRoot.substring(1);
            }
            fileInfo = contextRoot;
        }
        fileInfo += "/" + fileName;
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneCompletion.getURL]URL info : " + urlInfo);
            Log.printDebug("[SceneCompletion.getURL]File info : " + fileInfo);
        }
        ProxyInfo[] pInfos = serverInfo.getProxyInfos();
        URL url = null;
        if (pInfos == null || pInfos.length == 0) {
            if (Log.INFO_ON) {
                Log.printInfo("[SceneCompletion.getURL] ProxyInfo Array is null or Size zero.");
            }
            try {
                url = new URL(urlInfo + fileInfo);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneCompletion.getURL]non-Proxy URL : " + url);
            }
        } else {
            if (Log.INFO_ON) {
                Log.printInfo("[SceneCompletion.getURL] ProxyInfo is one or more.");
            }
            for (int i = 0; i < pInfos.length; i++) {
                if (pInfos[i] == null) {
                    continue;
                }
                String proxyHost = pInfos[i].getProxyHost();
                int proxyPort = pInfos[i].getProxyPort();
                try {
                    url = new URL("http", proxyHost, proxyPort, urlInfo + fileInfo);
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
                if (url != null) {
                    break;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[SceneCompletion.getURL]Proxy URL : " + url);
            }
        }
        return url;
    }
}

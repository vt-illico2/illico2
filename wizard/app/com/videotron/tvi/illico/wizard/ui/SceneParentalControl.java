package com.videotron.tvi.illico.wizard.ui;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.parental.ParentalWizardListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererParentalControl;
import com.videotron.tvi.illico.wizard.popup.PopupAdapter;
import com.videotron.tvi.illico.wizard.popup.PopupController;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class SceneParentalControl extends Scene implements ParentalWizardListener  {
    private static final long serialVersionUID = 6088398007392057171L;
    private DataCenter dCenter = DataCenter.getInstance();

    private ComponentStep compStep;
    private ComponentTitle compTitle;
    private PopupQuestionAdminPin popQuestionAdminPin;
    private PopupNotification popNotification;
    private PopupCreatePIN popCreatePIN;
    private PopupAgeSelection popAgeSelection;
    private PopupOrderPIN popOrderPIN;
    private PopupAdapter popAdapter;
    private PopupAdapter popQueAdminPinAdapter;
    private PopupAdapter popAdapterCreatePIN;
    private PopupAdapter popAdapterAgeSelection;
    private PopupAdapter popAdapterOrderPIN;

    private final int prevSceneId = SceneTemplate.SCENE_ID_ADMIN_PIN;
    private final int nextSceneId = SceneTemplate.SCENE_ID_ORDER_PIN;

    private boolean existRatingValue;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_PREV = 0;
    public static final int BUTTON_NEXT = 1;
    private boolean isExtraButton;
    private int curButton;

    protected void initScene() {
        renderer = new RendererParentalControl();
        setRenderer(renderer);
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(53, 17, 424, 72);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setCurrentStep(ComponentStep.STEP_ID_PARENTAL_CONTROL);
            compStep.setBounds(0, 88, 960, 29);
            compStep.setStepIds(STEP_ID);
            add(compStep);
        }
        if (popNotification == null) {
            popNotification = new PopupNotification();
            popNotification.setBounds(0, 0, 960, 540);
        }
    }
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep.dispose();
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle.dispose();
            compTitle = null;
        }
        if (popNotification != null) {
            popNotification.dispose();
            popNotification = null;
            popAdapter = null;
        }
        if (popQuestionAdminPin != null) {
            popQuestionAdminPin.dispose();
            popQuestionAdminPin = null;
            popQueAdminPinAdapter = null;
        }
        if (popCreatePIN != null) {
            popCreatePIN.dispose();
            popCreatePIN = null;
            popAdapterCreatePIN = null;
        }
        if (popAgeSelection != null) {
            popAgeSelection.dispose();
            popAgeSelection = null;
            popAdapterAgeSelection = null;
        }
        if (popOrderPIN != null) {
            popOrderPIN.dispose();
            popOrderPIN = null;
            popAdapterOrderPIN = null;
        }
        popAdapter = null;
        renderer = null;
    }
    protected void startScene(boolean resetScene) {
        if (resetScene) {
            isExtraButton = true;
            curButton = BUTTON_NEXT;
        }
        existRatingValue = PreferenceProxy.getInstance().existRatingValue();
        prepare();
    }
    protected void stopScene() {
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification, this);
            popAdapter = null;
        }
        if (popQuestionAdminPin != null) {
            popQuestionAdminPin.removePopupListener();
            PopupController.getInstance().closePopup(popQuestionAdminPin, this);
            popQueAdminPinAdapter = null;
        }
        if (popCreatePIN != null) {
            popCreatePIN.removePopupListener();
            PopupController.getInstance().closePopup(popCreatePIN, this);
            popAdapterCreatePIN = null;
        }
        if (popAgeSelection != null) {
            popAgeSelection.removePopupListener();
            PopupController.getInstance().closePopup(popAgeSelection, this);
            popAdapterAgeSelection = null;
        }
        if (popOrderPIN != null) {
            popOrderPIN.removePopupListener();
            PopupController.getInstance().closePopup(popOrderPIN, this);
            popAdapterOrderPIN = null;
        }
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                if (isExtraButton) {
                    return true;
                }
                isExtraButton = true;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (!isExtraButton) {
                    return true;
                }
                isExtraButton = false;
                repaint();
                return true;
            case Rs.KEY_LEFT:
                if (isExtraButton) {
                    return true;
                }
                if (curButton == 0) {
                    return true;
                }
                curButton --;
                repaint();
                return true;
            case Rs.KEY_RIGHT:
                if (isExtraButton) {
                    return true;
                }
                if (curButton == BUTTON_COUNT-1) {
                    return true;
                }
                curButton ++;
                repaint();
                return true;
            case Rs.KEY_OK:
                if (isExtraButton) {
                    new ClickingEffect(this, 5).start(295, 346, 371, 50);
                    boolean isExistAdminPin = PreferenceProxy.getInstance().existAdminPIN();
                    if (isExistAdminPin) {
                        if (existRatingValue) {
                            boolean result = PreferenceProxy.getInstance().resetRatingValue();
                            if (result) {
                                existRatingValue = false;
                                repaint();
                            }
                        } else {
                            openPopupAgeSelection(true);
                        }
                    } else {
                        if (popQuestionAdminPin == null) {
                            popQuestionAdminPin = new PopupQuestionAdminPin();
                            popQuestionAdminPin.setBounds(0, 0, 960, 540);
                        }
                        popQuestionAdminPin.setPopupListener(getQuestionAdminPinPopupAdapter());
                        String txtPopSubTitle = dCenter.getString("TxtQSW.Question_Create_Admin_Pin_Title");
                        popQuestionAdminPin.setTitle(txtPopSubTitle);
                        String txtPopCont = dCenter.getString("TxtQSW.Question_Create_Admin_Pin_Content");
                        popQuestionAdminPin.setContent(txtPopCont);
                        PopupController.getInstance().openPopup(popQuestionAdminPin, this, true);
                    }
                } else {
                    new ClickingEffect(this, 5).start(339 + (curButton * RendererParentalControl.BUTTON_GAP), 432, 136, 32);
                    switch(curButton) {
                        case BUTTON_PREV:
                            SceneManager.getInstance().goToNextScene(prevSceneId, false);
                            break;
                        case BUTTON_NEXT:
                            boolean existRatingValue = PreferenceProxy.getInstance().existRatingValue();
                            if (existRatingValue) {
                                SceneManager.getInstance().goToNextScene(nextSceneId, true);
                            } else {
                                popNotification.setPopupListener(getNotiPopupAdapter());
                                popNotification.setPopupType(PopupNotification.POPUP_TYPE_INFORMATION);
                                String txtPopSubTitle = dCenter.getString("TxtQSW.Skip_Parental_Control_Popup_Title");
                                popNotification.setTitle(txtPopSubTitle);
                                String txtPopCont = dCenter.getString("TxtQSW.Skip_Parental_Control_Popup_Content");
                                popNotification.setContent(txtPopCont);
                                PopupController.getInstance().openPopup(popNotification, this, true);
                            }
                            break;
                    }
                }
                return true;
        }
        return false;
    }
    public boolean isExistRatingValue() {
        return existRatingValue;
    }
    public boolean isExtraButton() {
        return isExtraButton;
    }
    public int getCurrentButton() {
        return curButton;
    }
    /*********************************************************************************
     * Popup-related
     **********************************************************************************/
    private void openPopupAgeSelection(boolean reset) {
        if (popAgeSelection == null) {
            popAgeSelection = new PopupAgeSelection();
            popAgeSelection.setBounds(0, 0, 960, 540);
        }
        popAgeSelection.setPopupListener(getPopupAdapterAgeSelection());
        PopupController.getInstance().openPopup(popAgeSelection, SceneParentalControl.this, reset);
    }
    private void openPopupOrderPIN() {
        if (popOrderPIN == null) {
            popOrderPIN = new PopupOrderPIN();
            popOrderPIN.setBounds(0, 0, 960, 540);
        }
        popOrderPIN.setPopupListener(getPopupAdapterOrderPIN());
        PopupController.getInstance().openPopup(popOrderPIN, SceneParentalControl.this, true);
    }
    /*********************************************************************************
     * PopupAdapter-related
     **********************************************************************************/
    public PopupAdapter getNotiPopupAdapter() {
        if (popAdapter == null) {
            popAdapter = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                    SceneManager.getInstance().goToNextScene(nextSceneId, true);
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                }
            };
        }
        return popAdapter;
    }
    public PopupAdapter getQuestionAdminPinPopupAdapter() {
        if (popQueAdminPinAdapter == null) {
            popQueAdminPinAdapter = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupQuestionAdminPin pop = (PopupQuestionAdminPin)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                    if (popCreatePIN == null) {
                        popCreatePIN = new PopupCreatePIN();
                        popCreatePIN.setBounds(0, 0, 960, 540);
                    }
//                    String txtPopupTitle = dCenter.getString("TxtQSW.Create_An_Administrator_PIN_Code");
//                    if (txtPopupTitle != null) {
//                        popCreatePIN.setPopupTitle(txtPopupTitle);
//                    }
                    popCreatePIN.setPopupPinType(PopupCreatePIN.PIN_TYPE_ADMIN);
                    popCreatePIN.setPopupListener(getPopupAdapterCreatePIN());
                    PopupController.getInstance().openPopup(popCreatePIN, SceneParentalControl.this, true);
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupQuestionAdminPin pop = (PopupQuestionAdminPin)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                }
            };
        }
        return popQueAdminPinAdapter;
    }
    public PopupAdapter getPopupAdapterCreatePIN() {
        if (popAdapterCreatePIN == null) {
            popAdapterCreatePIN = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        String pinCode = pop.getPinCode();
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                        try {
                            PreferenceProxy.getInstance().setAdminPIN(pinCode);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            if (Log.ERROR_ON) {
                                Log.printError("["+Rs.ERROR_CODE_UPP_ACCESS_PROBLEM+"] Cannot contact the UPP module for PIN creation.");
                            } 
                            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_UPP_ACCESS_PROBLEM);
                            return;
                        }
                    }
                    openPopupAgeSelection(true);
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                }
            };
        }
        return popAdapterCreatePIN;
    }
    public PopupAdapter getPopupAdapterAgeSelection() {
        if (popAdapterAgeSelection == null) {
            popAdapterAgeSelection = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupAgeSelection pop = (PopupAgeSelection)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
//                    openPopupOrderPIN();
                    String selectedValue = null;
                    if (popAgeSelection != null) {
                        selectedValue = popAgeSelection.getSelectedAgeTypeValue();
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[SceneParentalControl.popupOK]selectedValue : "+selectedValue);
                    }
                    if (selectedValue != null) {
                        boolean result = PreferenceProxy.getInstance().setRatingValue(selectedValue);
                        if (result) {
                            existRatingValue = true;
                        }
                    }
                    if (existRatingValue) {
                        isExtraButton = false;
                        curButton = BUTTON_NEXT;
                    }
                    repaint();
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupAgeSelection pop = (PopupAgeSelection)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                    existRatingValue = PreferenceProxy.getInstance().existRatingValue();
                    repaint();
                }
            };
        }
        return popAdapterAgeSelection;
    }
    public PopupAdapter getPopupAdapterOrderPIN() {
        if (popAdapterOrderPIN == null) {
            popAdapterOrderPIN = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupOrderPIN pop = (PopupOrderPIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                    String selectedValue = null;
                    if (popAgeSelection != null) {
                        selectedValue = popAgeSelection.getSelectedAgeTypeValue();
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[SceneParentalControl.popupOK]selectedValue : "+selectedValue);
                    }
                    if (selectedValue != null) {
                        boolean result = PreferenceProxy.getInstance().setRatingValue(selectedValue);
                        if (result) {
                            existRatingValue = true;
                        }
                    }
                    if (existRatingValue) {
                        isExtraButton = false;
                        curButton = BUTTON_NEXT;
                    }
                    repaint();
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupOrderPIN pop = (PopupOrderPIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                    openPopupAgeSelection(false);
                }
                public void popupClose(PopupEvent popupEvent) {
                    PopupOrderPIN pop = (PopupOrderPIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneParentalControl.this);
                    }
                }
            };
        }
        return popAdapterOrderPIN;
    }
    /*********************************************************************************
     * ParentalWizardListener-implemented
     **********************************************************************************/
    public void requestCanceled() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneParentalControl.requestCanceled]start.");
        }
    }
    public void requestCompleted() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SceneParentalControl.requestCompleted]start.");
        }
        SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_ORDER_PIN, true);
    }
}

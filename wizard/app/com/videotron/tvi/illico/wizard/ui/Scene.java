package com.videotron.tvi.illico.wizard.ui;

import java.awt.event.KeyEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.controller.DataManager;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupController;

abstract public class Scene extends UIComponent {
    private static final long serialVersionUID = 1L;
    protected static final int[] STEP_ID = { ComponentStep.STEP_ID_LANGUAGE, ComponentStep.STEP_ID_ADMIN_PIN,
        ComponentStep.STEP_ID_PARENTAL_CONTROL, ComponentStep.STEP_ID_ORDER_PIN, ComponentStep.STEP_ID_POWER_ON_CHANNEL};
    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void init() {
        initScene();
    }
    public void dispose() {
        stopScene();
        disposeScene();
    }
    public void start(boolean reset) {
        startScene(reset);
    }
    public void stop() {
        stopScene();
    }
    /*****************************************************************
     * implement KeyListener
     *****************************************************************/
    public boolean handleKey(int code) {
        switch(code) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case KeyCodes.LAST:
            case KeyEvent.VK_UP:
            case KeyEvent.VK_DOWN:
            case KeyEvent.VK_LEFT:
            case KeyEvent.VK_RIGHT:
            case KeyEvent.VK_ENTER:
            case Rs.KEY_EXIT:
                boolean isActivePopup = PopupController.getInstance().isActivePopup();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Scene.handleKey]isActivePopup : "+isActivePopup);
                }
                if (isActivePopup) {
                    Popup activePop = PopupController.getInstance().getLastPopup();
                    activePop.keyAction(code);
                    return true;
                }
                if (code == Rs.KEY_EXIT) {
                    CommunicationManager.getInstance().requestExitToChannel(CommunicationManager.EXIT_TYPE_EXIT_KEY);
                } else {
                    keyAction(code);
                }
                return true;
            default :
                if (!DataManager.getInstance().isCalledByMonitor()) {
                    return false;
                }
        }
        return true;
    }
    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void initScene();

    abstract protected void disposeScene();

    abstract protected void startScene(boolean resetScene);

    abstract protected void stopScene();

    abstract protected boolean keyAction(int keyCode);
}

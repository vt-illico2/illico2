package com.videotron.tvi.illico.wizard.ui;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.log.Log;

public class ActivatedResultHandler extends SAXHandlerAdapter {
    public void startElement(String valueXMLTag, Attributes valueAttributes) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ActivatedResultHandler.startElement] XML Tag : "+valueXMLTag);
        }
        if (valueXMLTag == null) {
            return;
        }
        valueXMLTag=valueXMLTag.toUpperCase();
        if (Log.DEBUG_ON) {
            Log.printDebug("[ActivatedResultHandler.startElement] XMLTag (Upper Case) : "+valueXMLTag);
        }
        if (valueXMLTag.equalsIgnoreCase("ROOT")) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[ActivatedResultHandler.startElement] Attributes : "+valueAttributes);
            }
            if (valueAttributes != null) {
                int valueAttributesLth=valueAttributes.getLength();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ActivatedResultHandler.startElement] Attribute Count : "+valueAttributesLth);
                }
                for (int i=0; i<valueAttributesLth; i++) {
                    String lName = valueAttributes.getLocalName(i);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[ActivatedResultHandler.startElement] Attribute Local Name ["+i+"] : "+lName);
                    }
                    if (lName == null) {
                        continue;
                    }
                    lName=lName.toUpperCase();
                    if (lName.equalsIgnoreCase("SUCCESS")) {
                        String lValue = valueAttributes.getValue(i);
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ActivatedResultHandler.startElement] Attribute Value ["+i+"] : "+lValue);
                        }
                        resultObject=lValue;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ActivatedResultHandler.startElement] Result Object ["+i+"] : "+resultObject);
                        }
                        break;
                    }
                }
            }
        }
    }
}

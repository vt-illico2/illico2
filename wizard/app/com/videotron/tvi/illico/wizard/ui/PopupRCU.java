package com.videotron.tvi.illico.wizard.ui;

import com.sun.corba.se.impl.orbutil.graph.Graph;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

import java.awt.*;
import java.awt.event.KeyEvent;

/**
 * Copyright (c) 2016 Alticast Corp.
 * All rights reserved. http://www.alticast.com/
 * <p>
 * This software is the confidential and proprietary information of
 * Alticast Corp. ("Confidential Information"). You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Alticast.
 * <p>
 * Created by zesyman on 2016-07-21.
 */
public class PopupRCU extends Popup {

	private String titleStr;
	private String[] explainStrArr;
	private String okStr;

	private Image rcuAniImg;

	protected void disposePopup() {

	}

	protected void startPopup(boolean reset) {
		DataCenter dcCenter = DataCenter.getInstance();

		titleStr = dcCenter.getString("TxtQSW.Pop_Rcu_title");
		explainStrArr = TextUtil.split(dcCenter.getString("TxtQSW.Pop_Rcu_explain"), Rs.FM19, 390, "|");
		okStr = dcCenter.getString("TxtQSW.Close");

		rcuAniImg = dcCenter.getImage("rcu_ani.gif");

		FrameworkMain.getInstance().getImagePool().waitForAll();

	}

	protected void stopPopup() {

	}

	public boolean keyAction(int keyCode) {
		switch (keyCode) {
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_UP:
		case KeyEvent.VK_DOWN:
			return true;
		case KeyEvent.VK_ENTER:
			if (pListener != null) {
				pListener.popupOK(new PopupEvent(this));
			}
			return true;
		case Rs.KEY_EXIT:
			if (pListener != null) {
				pListener.popupCancel(new PopupEvent(this));
			}
			return true;
		}
		return false;
	}

	public void paint(Graphics g) {

		// dimmed background
		g.setColor(Rs.DVB000000000204);
		g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

		// background
		g.setColor(Rs.C044044044);
		g.fillRect(190, 103, 585, 319);
		g.setColor(Rs.C066066066);
		g.fillRect(210, 142, 545, 1);

		// ruc ani gif
		g.drawImage(rcuAniImg, 215, 166, this);

		// title
		g.setFont(Rs.F24);
		g.setColor(Rs.C250202000);
		GraphicUtil.drawStringCenter(g, titleStr, 480, 130);

		// explain
		g.setColor(Rs.C229229229);
		g.setFont(Rs.F19);

		int posY = 250;
		if (explainStrArr.length % 2 == 0) {
			posY = 250 - explainStrArr.length / 2 * 20 + 15;
		} else {
			posY = 250 - explainStrArr.length / 2 * 20 + 10;
		}

		for (int i = 0; i < explainStrArr.length; i++) {
			if ((explainStrArr.length - 1) == i) {
				// last line - url
				g.setColor(Rs.C255228148);
			}
			g.drawString(explainStrArr[i], 359, posY + i * 20);
		}


		// button
		g.setColor(Rs.C249194000);
		g.fillRect(400, 365, 165, 32);
		g.setFont(Rs.F18);
		g.setColor(Rs.C003003003);
		GraphicUtil.drawStringCenter(g, okStr, 482, 387);


		super.paint(g);
	}
}

package com.videotron.tvi.illico.wizard.ui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererPowerOnChannel;
import com.videotron.tvi.illico.wizard.popup.PopupController;
import org.ocap.ui.event.OCRcEvent;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Vector;

public class ScenePowerOnChannel extends Scene {
	private static final long serialVersionUID = 1719247830341282139L;
	//Component
	private ComponentStep compStep;
	private ComponentTitle compTitle;

	private final int PREV_SCENE_ID = SceneTemplate.SCENE_ID_ORDER_PIN;
	private final int NEXT_SCENE_ID = SceneTemplate.SCENE_ID_COMPLETE;

	//Button type
	public static final int BUTTON_TYPE_PREV = 0;
	public static final int BUTTON_TYPE_FINISH = 1;
	private int curButtonType;

	private boolean isFocusOnPowerOnChannel;

	/**
	 * Channel data from EPG via SharedMemory.
	 */
	private Object[][] channelData;
	/**
	 * Channel data to be rearrange.
	 */
	private Object[][] builtChannelData;
	/**
	 * Favourite channels.
	 */
	private String[] favouriteChannels;

	/**
	 * String buffer to handle a direct input.
	 */
	private StringBuffer numBuffer;
	/**
	 * Indicate whether current state is entering number.
	 */
	private boolean isEnteringNumber;
	/**
	 * Vector to have a founded channels with direct input.
	 */
	private Vector vFoundChannel;

	private Vector channelList = new Vector();
	;
	private Hashtable sisterTable = new Hashtable();
	private Thread channelThread;
	/**
	 * A key name of Channel data from EPG via SharedMemory.
	 */
	public static final String EPG_DATA_KEY = "Epg.Channels";

	/**
	 * Popup to select a channel .
	 */
	private PopSelectChannelUI selectChannelUI = new PopSelectChannelUI();

	/**
	 * Deciaml format for three size.
	 */
	private DecimalFormat df = new DecimalFormat("000");

	protected void initScene() {
		renderer = new RendererPowerOnChannel();
		setRenderer(renderer);
		if (compTitle == null) {
			compTitle = new ComponentTitle();
			compTitle.setBounds(53, 17, 424, 72);
			add(compTitle);
		}
		if (compStep == null) {
			compStep = new ComponentStep();
			compStep.setCurrentStep(ComponentStep.STEP_ID_POWER_ON_CHANNEL);
			compStep.setBounds(0, 88, 960, 29);
			compStep.setStepIds(STEP_ID);
			add(compStep);
		}

		selectChannelUI.setBounds(Constants.SCREEN_BOUNDS);
	}

	protected void disposeScene() {
		if (compStep != null) {
			remove(compStep);
			compStep.dispose();
			compStep = null;
		}
		if (compTitle != null) {
			remove(compTitle);
			compTitle.dispose();
			compTitle = null;
		}

		remove(selectChannelUI);
		selectChannelUI.setRunningAutoFinder(false);
		selectChannelUI = null;

		renderer = null;
	}

	protected void startScene(boolean resetScene) {
		prepare();

		favouriteChannels = TextUtil
				.tokenize(PreferenceProxy.getInstance().getFavoriteChannels(), PreferenceService.PREFERENCE_DELIMETER);

		channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);

		preparePowerOnChannel();

		isEnteringNumber = false;
		isFocusOnPowerOnChannel = true;
	}

	protected void stopScene() {
	}

	protected boolean keyAction(int keyCode) {

		if (selectChannelUI.isRunningAutoFinder()) {
			return selectChannelUI.handleKey(keyCode);
		}

		switch (keyCode) {
		case Rs.KEY_UP:
			isFocusOnPowerOnChannel = true;
			isEnteringNumber = false;
			repaint();
			return true;
		case Rs.KEY_DOWN:
			isFocusOnPowerOnChannel = false;
			isEnteringNumber = false;
			curButtonType = BUTTON_TYPE_FINISH;
			repaint();
			return true;

		case Rs.KEY_LEFT:
			curButtonType = BUTTON_TYPE_PREV;
			repaint();
			return true;

		case Rs.KEY_RIGHT:
			curButtonType = BUTTON_TYPE_FINISH;
			repaint();
			return true;

		case Rs.KEY_OK:
			if (isEnteringNumber) {
				String enteredNumber = numBuffer.toString();
				for (int i = 0; i < vFoundChannel.size(); i++) {
					Object[] cData = (Object[]) vFoundChannel.get(i);
					String number = ((Integer) cData[PopSelectChannelUI.INDEX_CHANNEL_NUMBER]).toString();
					String newNum = df.format(Integer.parseInt(number));
					int enterdInt = Integer.parseInt(enteredNumber);
					int numberInt = Integer.parseInt(number);

					if (enterdInt == numberInt || enteredNumber.equals(number) || enteredNumber.equals(newNum)) {
						EpgService eService = CommunicationManager.getInstance().getEpgService();
						String callLetter = (String) cData[PopSelectChannelUI.INDEX_CHANNEL_NAME];

						if (PreferenceProxy.getInstance().isChannelGrouped()) {
							try {
								TvChannel selectChannel = eService.getChannel(callLetter);
								TvChannel sisterChannel = selectChannel.getSisterChannel();
								if (selectChannel.isHd() && selectChannel.isSubscribed()) {
									callLetter = selectChannel.getCallLetter();
								} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
									callLetter = sisterChannel.getCallLetter();
								}

							} catch (Exception ex) {
								Log.printError("powerOnChannel, " + ex.getMessage());
							}
						}

						PreferenceProxy.getInstance().setPowerOnChannel(callLetter);

						// move a focus
						isFocusOnPowerOnChannel = false;
						curButtonType = BUTTON_TYPE_FINISH;

						break;
					} else {
						// check sister
						String callLetter = (String) cData[PopSelectChannelUI.INDEX_CHANNEL_NAME];
						if (sisterTable.get(callLetter) != null) {
							Object[] sisterChannelData = (Object[]) sisterTable.get(callLetter);
							int sisterNumber = ((Integer) sisterChannelData[PopSelectChannelUI.INDEX_CHANNEL_NUMBER])
									.intValue();
							if (sisterNumber == enterdInt) {
								EpgService eService = CommunicationManager.getInstance().getEpgService();

								try {
									TvChannel selectChannel = eService.getChannel(callLetter);
									TvChannel sisterChannel = selectChannel.getSisterChannel();
									if (selectChannel.isHd() && selectChannel.isSubscribed()) {
										callLetter = selectChannel.getCallLetter();
									} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
										callLetter = sisterChannel.getCallLetter();
									}

								} catch (Exception ex) {
									Log.printError("powerOnChannel, " + ex.getMessage());
								}

								PreferenceProxy.getInstance().setPowerOnChannel(callLetter);

								// move a focus
								isFocusOnPowerOnChannel = false;
								curButtonType = BUTTON_TYPE_FINISH;

								break;
							}
						}
					}
				}

				numBuffer = null;
				isEnteringNumber = false;
				repaint();
			} else {
				if (isFocusOnPowerOnChannel) {
					new ClickingEffect(this, 5).start(298, 330, 364, 42);
					// show popup
					selectChannelUI.setSisterTable(sisterTable);
					selectChannelUI.setData(this, builtChannelData, PreferenceProxy.getInstance().getPowerOnChannel(),
							DataCenter.getInstance().getString("TxtQSW.Power_On_Channel"), favouriteChannels, 585, 210,
							9);

					PopupController.getInstance().openPopup(selectChannelUI, this, true);
					repaint();
				} else {
					if (curButtonType == BUTTON_TYPE_PREV) {
						new ClickingEffect(this, 5).start(338, 432, 144, 40);
						// goto order PIN
						SceneManager.getInstance().goToNextScene(PREV_SCENE_ID, false);
					} else if (curButtonType == BUTTON_TYPE_FINISH) {
						new ClickingEffect(this, 5).start(484, 432, 144, 40);
						// goto Complete
						SceneManager.getInstance().goToNextScene(NEXT_SCENE_ID, false);
					}
				}
			}
			return true;
		case KeyEvent.VK_0:
		case KeyEvent.VK_1:
		case KeyEvent.VK_2:
		case KeyEvent.VK_3:
		case KeyEvent.VK_4:
		case KeyEvent.VK_5:
		case KeyEvent.VK_6:
		case KeyEvent.VK_7:
		case KeyEvent.VK_8:
		case KeyEvent.VK_9:
			if (isFocusOnPowerOnChannel) {
				numericKeys(keyCode);
				repaint();
			}
			return true;
		}
		return false;
	}

	public int getButtonType() {
		return curButtonType;
	}

	public void popupClosed(boolean ok) {
		PopupController.getInstance().closePopup(selectChannelUI, this);
		repaint();
		if (ok) {
			Object[] selectedValue = (Object[]) selectChannelUI.getSelectedValue();
			String name = (String) selectedValue[PopSelectChannelUI.INDEX_CHANNEL_NAME];

			if (PreferenceProxy.getInstance().isChannelGrouped()) {
				EpgService eService = CommunicationManager.getInstance().getEpgService();
				try {
					TvChannel selectChannel = eService.getChannel(name);
					TvChannel sisterChannel = selectChannel.getSisterChannel();
					if (selectChannel.isHd() && selectChannel.isSubscribed()) {
						name = selectChannel.getCallLetter();
					} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
						name = sisterChannel.getCallLetter();
					}

				} catch (Exception ex) {
					Log.printError("ScenePowerOnChannel, popupClosed, e= " + ex.getMessage());
				}
			}

			PreferenceProxy.getInstance().setPowerOnChannel(name);

			// move a focus
			isFocusOnPowerOnChannel = false;
			curButtonType = BUTTON_TYPE_FINISH;
			repaint();
		}
	}

	public void paintSelectedContent(Graphics g) {
		((RendererPowerOnChannel) renderer).paintSelectedContent(g, this);
	}

	/**
	 * Return a index of Favourite channels list.
	 *
	 * @param name The call letter
	 * @return return index, -1 means that channel is not a favourite channel.
	 */
	private int getFavouriteChannelIdx(String name) {
		for (int i = 0; i < favouriteChannels.length; i++) {
			if (favouriteChannels[i].equals(name)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Process a numeric keys for Power On channel.
	 *
	 * @param code The key code to be inputed.
	 */
	private void numericKeys(int code) {
		int number = code - OCRcEvent.VK_0;
		// build channel list
		Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

		if (vFoundChannel == null) {
			vFoundChannel = new Vector();
		}

		if (numBuffer == null) {
			numBuffer = new StringBuffer();
			vFoundChannel.clear();
		}

		numBuffer.append(number);

		if (numBuffer.length() > 4) {
			isEnteringNumber = false;
			numBuffer = null;
			return;
		} else {
			isEnteringNumber = true;
		}

		String enterNumber = numBuffer.toString();

		vFoundChannel.clear();
		for (int i = 1; i < builtChannelData.length; i++) {
			boolean isSubscribed = true;
			if (caTable != null) {

				Integer sourceId = (Integer) builtChannelData[i][PopSelectChannelUI.INDEX_CHANNEL_SOURCE_ID];

				Boolean subscribed = (Boolean) caTable.get(sourceId);

				if (subscribed != null) {
					isSubscribed = subscribed.booleanValue();
				}
			}

			Integer chType = (Integer) builtChannelData[i][PopSelectChannelUI.INDEX_CHANNEL_TYPE];

			if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed) {

				String chNum = ((Integer) builtChannelData[i][PopSelectChannelUI.INDEX_CHANNEL_NUMBER]).toString();
				Log.printDebug("ProgramGuildUI, numericKeys, chNum=" + chNum);
				if (chNum.startsWith(enterNumber)) {
					vFoundChannel.add(builtChannelData[i]);
				} else if (builtChannelData.equals("0") && chNum.length() < 4) {
					vFoundChannel.add(builtChannelData[i]);
				} else if (builtChannelData.equals("00") && chNum.length() < 2) {
					vFoundChannel.add(builtChannelData[i]);
				} else {
					String newChNum = df.format(Integer.parseInt(chNum));

					if (newChNum.indexOf(enterNumber) > -1) {
						vFoundChannel.add(builtChannelData[i]);
					}
				}

				// check sister
				if (!vFoundChannel.contains(builtChannelData[i])) {
					EpgService epgService = CommunicationManager.getInstance().getEpgService();
					try {
						TvChannel channel = epgService
								.getChannel((String) builtChannelData[i][PopSelectChannelUI.INDEX_CHANNEL_NAME]);
						Object[] sisChannelData = (Object[]) sisterTable.get(channel.getCallLetter());

						if (sisChannelData != null) {
							chNum = String.valueOf(
									((Integer) sisChannelData[PopSelectChannelUI.INDEX_CHANNEL_NUMBER]).intValue());
							Log.printDebug("ProgramGuildUI, numericKeys, sister chNum=" + chNum);
							if (chNum.startsWith(enterNumber)) {
								vFoundChannel.add(builtChannelData[i]);
							} else if (builtChannelData.equals("0") && chNum.length() < 4) {
								vFoundChannel.add(builtChannelData[i]);
							} else if (builtChannelData.equals("00") && chNum.length() < 2) {
								vFoundChannel.add(builtChannelData[i]);
							} else {
								String newChNum = df.format(Integer.parseInt(chNum));

								if (newChNum.indexOf(enterNumber) > -1) {
									vFoundChannel.add(builtChannelData[i]);
								}
							}
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}

				if (vFoundChannel.size() > 9) {
					break;
				}
			}
		}
	}

	/**
	 * Return a Vector to include a founded channels.
	 *
	 * @return Vector to include a founded channels.
	 */
	public Vector getFoundChannel() {
		return vFoundChannel;
	}

	/**
	 * Determine whether current state is entering number or not.
	 *
	 * @return true if is entering number, false otherwise.
	 */
	public boolean isEnteringNumber() {
		return isEnteringNumber;
	}

	public boolean isFocusOnPowerOnChannel() {
		return isFocusOnPowerOnChannel;
	}

	public Hashtable getSisterTable() {
		return sisterTable;
	}

	/**
	 * Get a entered number.
	 *
	 * @return entered number.
	 */
	public String getEnteringNumber() {
		if (numBuffer != null) {
			return numBuffer.toString();
		}
		return TextUtil.EMPTY_STRING;
	}

	private void preparePowerOnChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ScenePowerOnchannel, preparePowerOnChannel");
		}
		channelThread = new Thread(new Runnable() {
			public void run() {

				if (PreferenceProxy.getInstance().isChannelGrouped()) {
					try {
						channelList.clear();
						sisterTable.clear();
						builtChannelData = null;

						CommunicationManager.getInstance().requestShowLoadingAnimation(new Point(480, 270));

						Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

						if (Log.DEBUG_ON) {
							Log.printDebug("ScenePowerOnchannel: channelThread caTable size is " + caTable.size());
						}

						Vector normalChannelVec = new Vector();

						channelList.add(new Object[] { new Integer(0), Definitions.LAST_CHANNEL });

						for (int i = 0; i < channelData.length; i++) {

							boolean isSubscribed = true;
							if (caTable != null) {

								Integer sourceId = (Integer) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_SOURCE_ID];

								Boolean subscribed = (Boolean) caTable.get(sourceId);

								if (subscribed != null) {
									isSubscribed = subscribed.booleanValue();
								}
							}

							Integer chType = (Integer) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_TYPE];

							String callLetterStr = ((String) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_NAME]);
							String sisterCalletterStr = (String) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_SISTER_NAME];
							if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed && (
									sisterCalletterStr == null || sisterTable.get(sisterCalletterStr) == null)) {

								int idx = getFavouriteChannelIdx(callLetterStr);

								if (idx > -1) {
									if (idx + 1 > channelList.size()) {
										channelList.add(channelData[i]);
									} else {
										channelList.add(idx + 1, channelData[i]);
									}
								} else {
									normalChannelVec.add(channelData[i]);
								}
							}

							// if sister channel exist, put in sisterTable
							if (sisterCalletterStr != null && !sisterTable.containsKey(sisterCalletterStr)) {
								for (int sisterIdx = 0; sisterIdx < channelData.length; sisterIdx++) {
									if (sisterCalletterStr
											.equals(channelData[sisterIdx][PopSelectChannelUI.INDEX_CHANNEL_NAME])) {
										sisterTable.put(callLetterStr, channelData[sisterIdx]);
										break;
									}
								}
							}
						}

						for (int i = 0; i < normalChannelVec.size(); i++) {
							channelList.add(normalChannelVec.get(i));
						}

						normalChannelVec.clear();

						builtChannelData = new Object[channelList.size()][];

						for (int i = 0; i < channelList.size(); i++) {
							builtChannelData[i] = (Object[]) channelList.get(i);
						}

						if (Log.DEBUG_ON) {
							if (builtChannelData != null) {
								Log.printDebug("ScenePowerOnchannel: channelThread, builtChannelData : "
										+ builtChannelData.length);
							} else {
								Log.printDebug("ScenePowerOnchannel: channelThread, builtChannelData is null");
							}
						}

						repaint();
					} catch (Exception e) {
						e.printStackTrace();
					}
					CommunicationManager.getInstance().requestHideLoadingAnimation();
				} else {

					sisterTable.clear();
					Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

					if (Log.DEBUG_ON) {
						Log.printDebug("ScenePowerOnchannel: channelThread, caTable size is " + caTable.size());
					}

					Vector vChannels = new Vector();
					Vector vOtherChannel = new Vector();
					// VDTRMASTER-5789
					vChannels.add(new Object[] { new Integer(0), Definitions.LAST_CHANNEL });

					// Waring : ONLY comment off a log for Debug. if comment on a log, it will be make too slowly.
					for (int i = 0; i < channelData.length; i++) {
						boolean isSubscribed = true;
						if (caTable != null) {

							Integer sourceId = (Integer) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_SOURCE_ID];

							Boolean subscribed = (Boolean) caTable.get(sourceId);

							if (subscribed != null) {
								isSubscribed = subscribed.booleanValue();
							}
						}

						Integer chType = (Integer) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_TYPE];

						if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed) {

							int idx = getFavouriteChannelIdx(
									((String) channelData[i][PopSelectChannelUI.INDEX_CHANNEL_NAME]));
							if (idx > -1) {
								if (idx + 1 > vChannels.size()) {
									vChannels.add(channelData[i]);
								} else {
									vChannels.add(idx + 1, channelData[i]);
								}
							} else {
								vOtherChannel.add(channelData[i]);
							}
						}
					}

					for (int i = 0; i < vOtherChannel.size(); i++) {
						vChannels.add(vOtherChannel.get(i));
					}

					builtChannelData = new Object[vChannels.size()][];
					vChannels.copyInto(builtChannelData);
				}
			}
		});

		channelThread.start();

	}
}

package com.videotron.tvi.illico.wizard.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.wizard.Rs;

public class ComponentTitle extends Component {
    private static final long serialVersionUID = 1L;
    private DataCenter dCenter = DataCenter.getInstance();
    private final int gap = 11;
    private Image imgSeparator;
    
    public ComponentTitle() {
        if (imgSeparator == null) {
            imgSeparator = dCenter.getImage("his_over.png");
        }
    }
    public void dispose() {
        dCenter.removeImage("his_over.png");
        imgSeparator = null;
    }
    public void paint(Graphics g) {
        int xPos = 0;
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        if (imgLogo != null) {
            g.drawImage(imgLogo, 0, 0, this);
            xPos = imgLogo.getWidth(this);
            xPos+= gap;
        }
        String txtWizard = dCenter.getString("TxtQSW.Quick_Start_Wizard");
        Log.printDebug("txtWizard=" + txtWizard);
        if (txtWizard != null) {
            if (imgSeparator != null) {
                g.drawImage(imgSeparator, xPos, 24, this);
                xPos += imgSeparator.getWidth(this);
                xPos += gap;
            }
            g.setFont(Rs.F18);
            g.setColor(Rs.C236236237);
            g.drawString(txtWizard, xPos, 36);
        }
    }
}

package com.videotron.tvi.illico.wizard.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupAdapter;
import com.videotron.tvi.illico.wizard.popup.PopupController;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class PopupOrderPIN extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    private DataCenter dCenter = DataCenter.getInstance();
    //Image
//    private Image imgPopShadow;
//    private Image imgPopBG;
//    private Image imgPopTitle;
    private Image imgButtonLargeBasic;
    private Image imgButtonLargeFocus;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    private Image imgIconCheck;
//    private Image imgTxtShadow;
    
    //Area
    public static final int AREA_MAIN_BUTTON = 0;
    public static final int AREA_BUTTON = 1;
    private int curArea;
    
    //Button
    public static final int BUTTON_TYPE_COUNT = 2;
    public static final int BUTTON_TYPE_PREVIOUS = 0;
    public static final int BUTTON_TYPE_FINISH = 1;
    public static final String[] BUTTON_TYPE_TEXT_KEY={"TxtQSW.Previous", "TxtQSW.Finish"};
    private int curButtonType;
    
    //Coordinate
    private static final int BUTTON_GAP = 146;
    
    private boolean existOrderPIN;
    //Popup
    private PopupCreatePIN popCreatePIN;
    private PopupAdapter popAdapterCreatePIN;
    
    public PopupOrderPIN() {
//        imgPopShadow = dCenter.getImage("12_ppop_sha.png");
//        imgPopBG = dCenter.getImage("12_pop_high_625.png");
//        imgPopTitle = dCenter.getImage("07_respop_title.png");
        imgButtonLargeBasic = dCenter.getImage("12_pos_focus_dim.png");
        imgButtonLargeFocus = dCenter.getImage("12_pos_focus.png");
        imgButtonBasic = dCenter.getImage("12_focus_dim.png");
        imgButtonFocus = dCenter.getImage("12_focus.png");
        imgIconCheck = dCenter.getImage("icon_g_check.png");
//        imgTxtShadow = dCenter.getImage("12_txtsha.png");
        
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    
    protected void disposePopup() {
        if (popCreatePIN != null) {
            popCreatePIN.dispose();
            popCreatePIN = null;
            popAdapterCreatePIN = null;
        }
//        if (imgPopShadow != null) {
//            dCenter.removeImage("12_ppop_sha.png");
//            imgPopShadow = null;
//        }
//        if (imgPopBG != null) {
//            dCenter.removeImage("12_pop_high_625.png");
//            imgPopBG = null;
//        }
//        if (imgPopTitle != null) {
//            dCenter.removeImage("07_respop_title.png");
//            imgPopTitle = null;
//        }
        if (imgButtonLargeBasic != null) {
            dCenter.removeImage("12_pos_focus_dim.png");
            imgButtonLargeBasic = null;
        }
        if (imgButtonLargeFocus != null) {
            dCenter.removeImage("12_pos_focus.png");
            imgButtonLargeFocus = null;
        }
        if (imgButtonBasic != null) {
            dCenter.removeImage("12_focus_dim.png");
            imgButtonBasic = null;
        }
        if (imgButtonFocus != null) {
            dCenter.removeImage("12_focus.png");
            imgButtonFocus = null;
        }
        if (imgIconCheck != null) {
            dCenter.removeImage("icon_g_check.png");
            imgIconCheck = null;
        }
//        if (imgTxtShadow != null) {
//            dCenter.removeImage("12_txtsha.png");
//            imgTxtShadow = null;
//        }
    }

    protected void startPopup(boolean reset) {
        existOrderPIN = PreferenceProxy.getInstance().existOrderPIN();
        curArea = AREA_MAIN_BUTTON;
        curButtonType = BUTTON_TYPE_FINISH;
    }

    protected void stopPopup() {
        if (popCreatePIN != null) {
            popCreatePIN.removePopupListener();
            PopupController.getInstance().closePopup(popCreatePIN, this);
            popAdapterCreatePIN = null;
        }
    }
    
    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB000000000204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 170, 483, 625, 57, this);
//        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
//        if (imgPopBG != null) {
//            g.drawImage(imgPopBG, 170, 51, this);
//        }
//        if (imgPopTitle != null) {
//            g.drawImage(imgPopTitle, 197, 89, this);
//        }
        
        //Description
        String txtTitle = dCenter.getString("TxtQSW.Parental_Control_Order_PIN_Code");
        if (txtTitle != null) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(txtTitle, 485 - (Rs.FM24.stringWidth(txtTitle)/2), 78);
        }
        g.setFont(Rs.F18);
        g.setColor(Rs.C255255255);
        String txtDesc0 = dCenter.getString("TxtQSW.Parental_Control_Order_PIN_Content_0");
        if (txtDesc0 != null) {
            String[] txtDesc0s = TextUtil.split(txtDesc0, Rs.FM18, 500, 2);
            for (int i=0; i<txtDesc0s.length; i++) {
                if (txtDesc0s[i] != null) {
                    g.drawString(txtDesc0s[i], 479 - (Rs.FM18.stringWidth(txtDesc0s[i])/2), 154 + (i*20));
                }
            }
        }
        String txtDesc1 = dCenter.getString("TxtQSW.Parental_Control_Order_PIN_Content_1");
        if (txtDesc1 != null) {
            String[] txtDesc1s = TextUtil.split(txtDesc1, Rs.FM18, 500, 2);
            for (int i=0; i<txtDesc1s.length; i++) {
                if (txtDesc1s[i] != null) {
                    g.drawString(txtDesc1s[i], 479 - (Rs.FM18.stringWidth(txtDesc1s[i])/2), 222 + (i*20));
                }
            }
        }
        //Exist order pin
        if (existOrderPIN) {
//            if (imgTxtShadow != null) {
//                g.drawImage(imgTxtShadow, 331, 308, this);
//            }
            String txtDescCreated = dCenter.getString("TxtQSW.Created_Order_PIN");
            int txtDescCreatedWth = Rs.FM33.stringWidth(txtDescCreated);
            int imgIconCheckWth = 0;
            if (imgIconCheck != null) {
                imgIconCheckWth = imgIconCheck.getWidth(this);
            }
            if (txtDescCreated != null) {
                g.setFont(Rs.F33);
                g.setColor(Rs.C255204000);
                g.drawString(txtDescCreated, 480 - ((txtDescCreatedWth+imgIconCheckWth+4)/2), 305);
            }
            if (imgIconCheck != null) {
                g.drawImage(imgIconCheck, 480 - ((txtDescCreatedWth+imgIconCheckWth)/2) + 4 + txtDescCreatedWth , 284, this);
            }
        }
        
        //Main Button
        Image imgButtonLarge = null;
        if (curArea == AREA_MAIN_BUTTON) {
            imgButtonLarge = imgButtonLargeFocus;
        } else {
            imgButtonLarge = imgButtonLargeBasic;
        }
        if (imgButtonLarge != null) {
            g.drawImage(imgButtonLarge, 292, 333, this);
        }
        String txtButtonLarge = null;
        if (existOrderPIN) {
            txtButtonLarge = dCenter.getString("TxtQSW.Modify_Order_PIN");
        } else {
            txtButtonLarge = dCenter.getString("TxtQSW.Create_Order_PIN");
        }
        if (txtButtonLarge != null) {
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButtonLarge, 478 - (Rs.FM21.stringWidth(txtButtonLarge)/2), 360);
        }
        
        //Button
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<BUTTON_TYPE_COUNT; i++) {
            Image imgButton = null;
            if (i == curButtonType && curArea == AREA_BUTTON) {
                imgButton = imgButtonFocus;
            } else {
                imgButton = imgButtonBasic;
            }
            if (imgButton != null) {
                g.drawImage(imgButton, 338 + (BUTTON_GAP * i), 437, this);
            }
            String txtButton = dCenter.getString(BUTTON_TYPE_TEXT_KEY[i]);
            if (txtButton != null) {
                g.drawString(txtButton, 407 - (Rs.FM18.stringWidth(txtButton)/2) + (BUTTON_GAP * i), 458);
            }
        }
        super.paint(g);
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                if (curArea == AREA_MAIN_BUTTON) {
                    return true;
                }
                curArea = AREA_MAIN_BUTTON;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (curArea == AREA_BUTTON) {
                    return true;
                }
                curArea = AREA_BUTTON;
                repaint();
                return true;
            case Rs.KEY_LEFT:
                if (curArea == AREA_MAIN_BUTTON) {
                    return true;
                }
                if (curButtonType == BUTTON_TYPE_PREVIOUS) {
                    return true;
                }
                curButtonType --;
                repaint();
                return true;  
            case Rs.KEY_RIGHT:
                if (curArea == AREA_MAIN_BUTTON) {
                    return true;
                }
                if (curButtonType == BUTTON_TYPE_FINISH) {
                    return true;
                }
                curButtonType ++;
                repaint();
                return true;
            case Rs.KEY_OK:
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                switch(curArea) {
                    case AREA_MAIN_BUTTON:
                        effect.start(296, 333, 364, 42);
                        if (popCreatePIN == null) {
                            popCreatePIN = new PopupCreatePIN();
                            popCreatePIN.setBounds(0, 0, 960, 540);
                        }
                        popCreatePIN.setPopupPinType(PopupCreatePIN.PIN_TYPE_ORDER);
                        popCreatePIN.setPopupListener(getPopupAdapterCreatePIN());
                        PopupController.getInstance().openPopup(popCreatePIN, PopupOrderPIN.this, true);
                        break;
                    case AREA_BUTTON:
                        effect.start(339 + (BUTTON_GAP * curButtonType), 437, 138, 33);
                        if (pListener != null) {
                            switch(curButtonType) {
                                case BUTTON_TYPE_PREVIOUS:
                                    pListener.popupCancel(new PopupEvent(this));
                                    break;
                                case BUTTON_TYPE_FINISH:
                                    pListener.popupOK(new PopupEvent(this));
                                    break;
                            }
                        }
                        break;
                }
                return true;
            case Rs.KEY_EXIT:
                if (pListener != null) {
                    pListener.popupClose(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    /*********************************************************************************
     * PopupAdapter-related
     **********************************************************************************/
    public PopupAdapter getPopupAdapterCreatePIN() {
        if (popAdapterCreatePIN == null) {
            popAdapterCreatePIN = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        String pinCode = pop.getPinCode();
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, PopupOrderPIN.this);
                        try {
                            PreferenceProxy.getInstance().setOrderPIN(pinCode);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            if (Log.ERROR_ON) {
                                Log.printError("["+Rs.ERROR_CODE_UPP_ACCESS_PROBLEM+"] Cannot contact the UPP module for PIN creation.");
                            } 
                            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_UPP_ACCESS_PROBLEM);
                            return;
                        }
                    }
                    existOrderPIN = PreferenceProxy.getInstance().existOrderPIN();
                    if (existOrderPIN) {
                        curArea = AREA_BUTTON;
                        curButtonType = BUTTON_TYPE_FINISH;
                        repaint();
                    }
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, PopupOrderPIN.this);
                    }
                }
            };
        }
        return popAdapterCreatePIN;
    }
}

package com.videotron.tvi.illico.wizard.ui;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.controller.SceneManager;
import com.videotron.tvi.illico.wizard.controller.SceneTemplate;
import com.videotron.tvi.illico.wizard.gui.RendererOrderPin;
import com.videotron.tvi.illico.wizard.popup.PopupAdapter;
import com.videotron.tvi.illico.wizard.popup.PopupController;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class SceneOrderPin extends Scene {
    private static final long serialVersionUID = -8481153590286618987L;
    
    private ComponentStep compStep;
    private ComponentTitle compTitle;
    private PopupAdapter popAdapterNotification;
    private PopupCreatePIN popCreatePIN;
    private PopupAdapter popAdapterCreatePIN;
    
    private final int prevSceneId = SceneTemplate.SCENE_ID_PARENTAL_CONTROL;
    private final int nextSceneId = SceneTemplate.SCENE_ID_POWER_ON_CHANNEL;

    private boolean existOrderPin;
    public static final int BUTTON_COUNT = 2;
    public static final int BUTTON_PREV = 0;
    public static final int BUTTON_NEXT = 1;
    private boolean isExtraButton;
    private int curButton;
    
    protected void initScene() {
        renderer = new RendererOrderPin();
        setRenderer(renderer);
        if (compTitle == null) {
            compTitle = new ComponentTitle();
            compTitle.setBounds(53, 17, 424, 72);
            add(compTitle);
        }
        if (compStep == null) {
            compStep = new ComponentStep();
            compStep.setCurrentStep(ComponentStep.STEP_ID_ORDER_PIN);
            compStep.setBounds(0, 88, 960, 29);
            compStep.setStepIds(STEP_ID);
            add(compStep);
        }
    }
    
    protected void disposeScene() {
        if (compStep!= null) {
            remove(compStep);
            compStep.dispose();
            compStep = null;
        }
        if (compTitle!= null) {
            remove(compTitle);
            compTitle.dispose();
            compTitle = null;
        }
        if (popCreatePIN != null) {
            popCreatePIN.dispose();
            popCreatePIN = null;
            popAdapterCreatePIN = null;
        }
        renderer = null;  
    }

    protected void startScene(boolean resetScene) {
        if (resetScene) {
            isExtraButton = true;
            curButton = BUTTON_NEXT;
        }
        existOrderPin = PreferenceProxy.getInstance().existOrderPIN();
        prepare();
    }

    protected void stopScene() {
        if (popCreatePIN != null) {
            popCreatePIN.removePopupListener();
            PopupController.getInstance().closePopup(popCreatePIN, this);
            popAdapterCreatePIN = null;
        }
    }

    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
            case Rs.KEY_UP:
                if (isExtraButton) {
                    return true;
                }
                isExtraButton = true;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (!isExtraButton) {
                    return true;
                }
                isExtraButton = false;
                repaint();
                return true;
            case Rs.KEY_LEFT:
                if (isExtraButton) {
                    return true;
                }
                if (curButton == BUTTON_PREV) {
                    return true;
                }
                curButton = BUTTON_PREV;
                repaint();
                return true;
            case Rs.KEY_RIGHT:
                if (isExtraButton) {
                    return true;
                }
                if (curButton == BUTTON_NEXT) {
                    return true;
                }
                curButton = BUTTON_NEXT;
                repaint();
                return true;
            case Rs.KEY_OK:
                if (isExtraButton) {
                    new ClickingEffect(this, 5).start(295, 346, 371, 50);
                    if (popCreatePIN == null) {
                        popCreatePIN = new PopupCreatePIN();
                        popCreatePIN.setBounds(0, 0, 960, 540);
                    }
                    popCreatePIN.setPopupPinType(PopupCreatePIN.PIN_TYPE_ORDER);
                    popCreatePIN.setPopupListener(getPopupAdapterCreatePIN());
                    PopupController.getInstance().openPopup(popCreatePIN, this, true);
                } else {
                    new ClickingEffect(this, 5).start(339 + (curButton * RendererOrderPin.BUTTON_GAP), 432, 136, 32);
                    switch(curButton) {
                        case BUTTON_PREV:
                            SceneManager.getInstance().goToNextScene(prevSceneId, false);
                            break;
                        case BUTTON_NEXT:
                            SceneManager.getInstance().goToNextScene(nextSceneId, true);
                            break;
                    }
                }
                return true;
        }
        return false;
    }
    
    public boolean isExtraButton() {
        return isExtraButton;
    }
    
    public boolean isExistOrderPin() {
        return existOrderPin;
    }

    public int getCurrentButton() {
        return curButton;
    }

    public PopupAdapter getNotiPopupAdapter() {
        if (popAdapterNotification == null) {
            popAdapterNotification = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneOrderPin.this);
                    }
                    SceneManager.getInstance().goToNextScene(nextSceneId, true);
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneOrderPin.this);
                    }
                }
            };
        }
        return popAdapterNotification;
    }
    public PopupAdapter getPopupAdapterCreatePIN() {
        if (popAdapterCreatePIN == null) {
            popAdapterCreatePIN = new PopupAdapter() {
                public void popupOK(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        String pinCode = pop.getPinCode();
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneOrderPin.this);
                        try {
                            PreferenceProxy.getInstance().setOrderPIN(pinCode);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            if (Log.ERROR_ON) {
                                Log.printError("["+Rs.ERROR_CODE_UPP_ACCESS_PROBLEM+"] Cannot contact the UPP module for PIN creation.");
                            } 
                            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_UPP_ACCESS_PROBLEM);
                            return;
                        }
                    }
                    existOrderPin = PreferenceProxy.getInstance().existOrderPIN();
                    if (existOrderPin) {
                        isExtraButton = false;
                        curButton = BUTTON_NEXT;
                        repaint();
                    }
                }
                public void popupCancel(PopupEvent popupEvent) {
                    PopupCreatePIN pop = (PopupCreatePIN)popupEvent.getPopup();
                    if (pop != null) {
                        pop.removePopupListener();
                        PopupController.getInstance().closePopup(pop, SceneOrderPin.this);
                    }
                }
            };
        }
        return popAdapterCreatePIN;
    }
}

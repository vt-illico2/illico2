package com.videotron.tvi.illico.wizard.ui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.popup.Popup;
import com.videotron.tvi.illico.wizard.popup.PopupEvent;

public class PopupAgeSelection extends Popup {
    private static final long serialVersionUID = 5911355373218833154L;
    private DataCenter dCenter = DataCenter.getInstance();
    
    //Image
//    private Image imgPopShadow;
//    private Image imgPopBG;
//    private Image imgPopTitle;
    private Image imgBlockIconThumb;
    private Image imgBlockIconLock;
    private Image imgAgePanelSelected;
    private Image imgAgePanelBasic;
    private Image imgAgePanelFocus;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    private Image imgArrowLeft;
    private Image imgArrowRight;
    
    //Area
    public static final int AREA_AGE = 0;
    public static final int AREA_BUTTON = 1;
    private int curArea;
    
    //Age
    public static final int AGE_TYPE_COUNT = 4;
    public static final int AGE_TYPE_MORE_THAN_8 = 0;
    public static final int AGE_TYPE_MORE_THAN_13 = 1;
    public static final int AGE_TYPE_MORE_THAN_16 = 2;
    public static final int AGE_TYPE_MORE_THAN_18 = 3;
    public static final String[] AGE_TYPE_TEXTS={"8+", "13+", "16+", "18+"};
    public static final String[] AGE_TYPE_VALUES={
        Definitions.RATING_8, Definitions.RATING_13, Definitions.RATING_16, Definitions.RATING_18
    };
    private int curAgeType;
    
    //Button
    public static final int BUTTON_TYPE_COUNT = 2;
    public static final int BUTTON_TYPE_CANCEL = 0;
    public static final int BUTTON_TYPE_NEXT = 1;
    public static final String[] BUTTON_TYPE_TEXT_KEY={"TxtQSW.Cancel", "TxtQSW.OK"};
    private int curButtonType;
    
    //Coordinate
    private static final int AGE_PANEL_GAP = 144;
    private static final int BUTTON_GAP = 146;
    
    public PopupAgeSelection() {
//        imgPopShadow = dCenter.getImage("12_ppop_sha.png");
//        imgPopBG = dCenter.getImage("12_pop_high_625.png");
//        imgPopTitle = dCenter.getImage("07_respop_title.png");
        imgBlockIconThumb = dCenter.getImage("12_icn_thumb.png");
        imgBlockIconLock = dCenter.getImage("12_icn_lock.png");
        imgAgePanelSelected = dCenter.getImage("12_age_select.png");
        imgAgePanelBasic = dCenter.getImage("12_age_dim.png");
        imgAgePanelFocus = dCenter.getImage("12_age_foc.png");
        imgButtonBasic = dCenter.getImage("12_focus_dim.png");
        imgButtonFocus = dCenter.getImage("12_focus.png");
        imgArrowLeft = dCenter.getImage("02_ars_l.png");
        imgArrowRight = dCenter.getImage("02_ars_r.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void disposePopup() {
//        if (imgPopShadow != null) {
//            dCenter.removeImage("12_ppop_sha.png");
//            imgPopShadow = null;
//        }
//        if (imgPopBG != null) {
//            dCenter.removeImage("12_pop_high_625.png");
//            imgPopBG = null;
//        }
//        if (imgPopTitle != null) {
//            dCenter.removeImage("07_respop_title.png");
//            imgPopTitle = null;
//        }
        if (imgBlockIconThumb != null) {
            dCenter.removeImage("12_icn_thumb.png");
            imgBlockIconThumb = null;
        }
        if (imgBlockIconLock != null) {
            dCenter.removeImage("12_icn_lock.png");
            imgBlockIconLock = null;
        }
        if (imgAgePanelSelected != null) {
            dCenter.removeImage("12_age_select.png");
            imgAgePanelSelected = null;
        }
        if (imgAgePanelBasic != null) {
            dCenter.removeImage("12_age_dim.png");
            imgAgePanelBasic = null;
        }
        if (imgAgePanelFocus != null) {
            dCenter.removeImage("12_age_foc.png");
            imgAgePanelFocus = null;
        }
        if (imgButtonBasic != null) {
            dCenter.removeImage("12_focus_dim.png");
            imgButtonBasic = null;
        }
        if (imgButtonFocus != null) {
            dCenter.removeImage("12_focus.png");
            imgButtonFocus = null;
        }
        if (imgArrowLeft != null) {
            dCenter.removeImage("02_ars_l.png");
            imgArrowLeft = null;
        }
        if (imgArrowRight != null) {
            dCenter.removeImage("02_ars_r.png");
            imgArrowRight = null;
        }
    }
    protected void startPopup(boolean reset) {
        if (reset) {
            curAgeType = AGE_TYPE_MORE_THAN_13;
        }
        curArea = AREA_AGE;
        curButtonType = BUTTON_TYPE_NEXT;
    }
    protected void stopPopup() {
    }
    
    public void paint(Graphics g) {
        // Backgound
        g.setColor(Rs.DVB000000000204);
        g.fillRect(0, 0, 960, 540);
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 170, 483, 625, 57, this);
//        }
        g.setColor(Rs.C033033033);
        g.fillRect(170, 51, 625, 435);
//        if (imgPopBG != null) {
//            g.drawImage(imgPopBG, 170, 51, this);
//        }
//        if (imgPopTitle != null) {
//            g.drawImage(imgPopTitle, 197, 89, this);
//        }
        //Description
        String txtTitle = dCenter.getString("TxtQSW.Parental_Control_Age_Restrictions");
        if (txtTitle != null) {
            g.setFont(Rs.F24);
            g.setColor(Rs.C255204000);
            g.drawString(txtTitle, 485 - (Rs.FM24.stringWidth(txtTitle)/2), 78);
        }
        String txtDesc = dCenter.getString("TxtQSW.Block_Content_Rated_As");
        if (txtDesc != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtDesc, 485 - (Rs.FM18.stringWidth(txtDesc)/2), 130);
        }
        String txtRate = dCenter.getString("TxtQSW.Content_Rated_As");
        if (txtRate != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtRate, 484 - (Rs.FM18.stringWidth(txtRate)/2), 331);
        }
        String txtTail = dCenter.getString("TxtQSW.Tail");
        if (txtTail != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C255255255);
            g.drawString(txtTail, 482 - (Rs.FM18.stringWidth(txtTail)/2), 391);
        }
        String ageValue = AGE_TYPE_TEXTS[curAgeType];
        String txtAndOver = dCenter.getString("TxtQSW.And_Older");
        if (txtAndOver != null && txtAndOver.length() > 0) {
            ageValue += " "+txtAndOver;
        }
        g.setFont(Rs.F32);
        g.setColor(Rs.C255204000);
        g.drawString(ageValue, 479 - (Rs.FM32.stringWidth(ageValue)/2), 364);
        //Age selection
        g.setFont(Rs.F46);
        for (int i=0; i<AGE_TYPE_COUNT; i++) {
            Image imgAgePanel = null;
            if (i == curAgeType) {
                if (curArea == AREA_AGE && i != 0 && imgArrowLeft != null) {
                    g.drawImage(imgArrowLeft, 190 + (AGE_PANEL_GAP * i), 187, this);
                }
                if (curArea == AREA_AGE && i != AGE_TYPE_COUNT-1 && imgArrowRight != null) {
                    g.drawImage(imgArrowRight, 326 + (AGE_PANEL_GAP * i), 187, this);
                }
                if (curArea == AREA_AGE) {
                    imgAgePanel = imgAgePanelFocus;
                } else {
                    imgAgePanel = imgAgePanelSelected;
                }
                
                g.setColor(Rs.C003003003);
            } else {
                imgAgePanel = imgAgePanelBasic;
                g.setColor(Rs.C210210210);
            }
            if (imgAgePanel != null) {
                g.drawImage(imgAgePanel, 202 + (AGE_PANEL_GAP * i), 155, this);
            }
            g.drawString(AGE_TYPE_TEXTS[i], 269 - (Rs.FM48.stringWidth(AGE_TYPE_TEXTS[i])/2) + (AGE_PANEL_GAP * i), 211);
            Image imgBlockIcon;
            if (i < curAgeType) {
                imgBlockIcon = imgBlockIconThumb;
            } else {
                imgBlockIcon = imgBlockIconLock;
            }
            if (imgBlockIcon != null) {
                g.drawImage(imgBlockIcon, 243 + (AGE_PANEL_GAP * i), 248, this);
            }
        }
        //Button
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<BUTTON_TYPE_COUNT; i++) {
            Image imgButton = null;
            if (i == curButtonType && curArea == AREA_BUTTON) {
                imgButton = imgButtonFocus;
            } else {
                imgButton = imgButtonBasic;
            }
            if (imgButton != null) {
                g.drawImage(imgButton, 338 + (BUTTON_GAP * i), 437, this);
            }
            String txtButton = dCenter.getString(BUTTON_TYPE_TEXT_KEY[i]);
            if (txtButton != null) {
                g.drawString(txtButton, 407 - (Rs.FM18.stringWidth(txtButton)/2) + (BUTTON_GAP * i), 458);
            }
        }
    }
    
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_UP:
                if (curArea == AREA_AGE) {
                    return true;
                }
                curArea = AREA_AGE;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (curArea == AREA_BUTTON) {
                    return true;
                }
                curArea = AREA_BUTTON;
                repaint();
                return true;
            case Rs.KEY_LEFT:
                switch(curArea) {
                    case AREA_AGE:
                        if (curAgeType == AGE_TYPE_MORE_THAN_8) {
                            return true;
                        }
                        curAgeType --;
                        repaint();
                        return true;
                    case AREA_BUTTON:
                        if (curButtonType == BUTTON_TYPE_CANCEL) {
                            return true;
                        }
                        curButtonType --;
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_RIGHT:
                switch(curArea) {
                    case AREA_AGE:
                        if (curAgeType == AGE_TYPE_MORE_THAN_18) {
                            return true;
                        }
                        curAgeType ++;
                        repaint();
                        return true;
                    case AREA_BUTTON:
                        if (curButtonType == BUTTON_TYPE_NEXT) {
                            return true;
                        }
                        curButtonType ++;
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_OK:
                ClickingEffect effect = new ClickingEffect(this, 5);
                effect.updateBackgroundBeforeStart(false);
                switch(curArea) {
                    case AREA_AGE:
                        effect.start(206 + (AGE_PANEL_GAP * curAgeType), 159, 122, 80);
                        curArea = AREA_BUTTON;
                        curButtonType = BUTTON_TYPE_NEXT;
                        repaint();
                        break;
                    case AREA_BUTTON:
                        effect.start(339 + (BUTTON_GAP * curButtonType), 437, 137, 33);
                        if (pListener != null) {
                            switch(curButtonType) {
                                case BUTTON_TYPE_CANCEL:
                                    pListener.popupCancel(new PopupEvent(this));
                                    break;
                                case BUTTON_TYPE_NEXT:
                                    pListener.popupOK(new PopupEvent(this));
                                    break;
                            }
                        }
                        break;
                }
                return true;
            case Rs.KEY_EXIT:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    public void setSelectedAgeType(int ageType) {
        curAgeType = ageType;
    }
    public String getSelectedAgeTypeValue() {
        return AGE_TYPE_VALUES[curAgeType];
    }
}

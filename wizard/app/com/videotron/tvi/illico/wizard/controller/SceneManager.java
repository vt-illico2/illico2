package com.videotron.tvi.illico.wizard.controller;

import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.wizard.ui.*;
import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.wizard.Rs;

public class SceneManager {
    private static SceneManager instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lUIWizard;
    private LayeredWindow lWinWizard;
    private LayeredKeyWizard lKeyWizard;

    private Hashtable scenes;
    private Scene curScene;
    private int curSceneId;

    private SceneManager(){
    }
    public static synchronized SceneManager getInstance() {
        if (instance == null) {
            instance = new SceneManager();
        }
        return instance;
    }
    public void init() {
        if (lWinWizard == null) {
            lWinWizard = new LayeredWindowWizard();
            lWinWizard.setVisible(true);
        }
        if (lKeyWizard == null) {
            lKeyWizard = new LayeredKeyWizard();
        }
        lUIWizard = WindowProperty.WIZARD.createLayeredDialog(lWinWizard, lWinWizard.getBounds(), lKeyWizard);
        lUIWizard.deactivate();
        if (scenes == null) {
            scenes = new Hashtable();
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
    }
    public void dispose() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                Scene scene = (Scene)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.dispose();
                scene = null;
            }
            scenes = null;
        }
        curScene = null;
        if (lUIWizard != null) {
            lUIWizard.deactivate();
            WindowProperty.dispose(lUIWizard);
            lUIWizard = null;
        }
        lKeyWizard = null;
        if (lWinWizard != null) {
            lWinWizard.setVisible(false);
            lWinWizard = null;
        }
        ImagePool imgPool = FrameworkMain.getInstance().getImagePool();
        if (imgPool != null) {
            imgPool.clear();
        }
    }
    public void start() {
        if (lUIWizard != null) {
            lUIWizard.activate();
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
        goToNextScene(SceneTemplate.SCENE_ID_WELCOME, true);
    }
    public void stop() {
        if (curScene != null) {
            curScene.stop();
            lWinWizard.remove(curScene);
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
        if (lUIWizard != null) {
            lUIWizard.deactivate();
        }
        ImagePool imgPool = FrameworkMain.getInstance().getImagePool();
        if (imgPool != null) {
            imgPool.clear();
        }
//        ImagePool ip=FrameworkMain.getInstance().getImagePool();
//        Enumeration enu = ip.keys();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[SceneManager.stop]enu : "+enu);
//        }
//        while(enu.hasMoreElements()) {
//            if (Log.DEBUG_ON) {
//                String imageName=(String)enu.nextElement();
//                Log.printDebug("[SceneManager.stop]Image name : "+imageName);
//                Log.printDebug("[SceneManager.stop]Image object : "+ip.get(imageName));
//            }
//        }
    }
    public void goToNextScene(int reqSceneId, boolean isReset) {
        if (curSceneId == reqSceneId) {
            return;
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinWizard.remove(curScene);
        }

        //Start current scene
        Scene scene = getScene(reqSceneId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.goToNextScene]scene : "+scene);
        }
        if (scene != null) {
            scene.start(isReset);
            lWinWizard.add(scene);
            curSceneId = reqSceneId;
            curScene = scene;
        }
        lWinWizard.repaint();
    }

    private Scene getScene(int reqSceneId) {
        Scene res = (Scene)scenes.get(new Integer(reqSceneId));
        //Check Scene
        if (res == null) {
            res = createScene(reqSceneId);
            if (res != null) {
                scenes.put(new Integer(reqSceneId), res);
            }
        }
        return res;
    }

    private Scene createScene(int reqSceneId) {
        Scene scene = null;
        switch(reqSceneId) {
            case SceneTemplate.SCENE_ID_WELCOME:
                scene = new SceneWelcome();
                break;
            case SceneTemplate.SCENE_ID_NAVIGATION:
                scene = new SceneNavigation();
                break;
            case SceneTemplate.SCENE_ID_LANGUAGE:
                scene = new SceneLanguage();
                break;
            case SceneTemplate.SCENE_ID_PARENTAL_CONTROL:
                scene = new SceneParentalControl();
                break;
            case SceneTemplate.SCENE_ID_ADMIN_PIN:
                scene = new SceneAdminPin();
                break;
            case SceneTemplate.SCENE_ID_ORDER_PIN:
                scene = new SceneOrderPin();
                break;
            case SceneTemplate.SCENE_ID_COMPLETE:
                scene = new SceneCompletion();
                break;
            case SceneTemplate.SCENE_ID_POWER_ON_CHANNEL: // R7.2
                scene = new ScenePowerOnChannel();
                break;
        }
        if (scene != null) {
            scene.init();
        }
        return scene;
    }

    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowWizard extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowWizard() {
            setBounds(Rs.SCENE_BOUND);
        }
        public void notifyShadowed() {
        }
    }
    class LayeredKeyWizard implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
                return false;
            }
            if (curScene == null) {
                return false;
            }
            return curScene.handleKey(userEvent.getCode());
        }
    }
}

package com.videotron.tvi.illico.wizard.controller;

import java.io.File;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.Util;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.data.ProxyInfo;
import com.videotron.tvi.illico.wizard.data.ServerInfo;
import com.videotron.tvi.illico.wizard.migration.MigrationAdapter;

public class DataManager implements DataUpdateListener {
    private static DataManager instance;
    public static final String DATA_CONFIG_IB = "DATA_CONFIG_IB";

    private int latestVersion;
    public static final String APP_NAME_HELP = "Help";
    public static final String APP_NAME_VOD = "VOD";
    public static final String APP_NAME_MENU = "Menu";
    private String categoryId;
    private int httpRequestCount;
    private ServerInfo[] serverInfoList;
    
    private ServerInfo activationServerInfo;

    /*****************************************************************
     * CALLED_BY-related
     *****************************************************************/
    public static final String APPLICATION_NAME_MONITOR = "Monitor";
    private String curCalledByValue;

    private boolean flagIsNeedUpgrade;
    private boolean isNeedMigrationSetting;
    private boolean isCompletedMigrationSetting;

    private DataManager() {
    }
    public static synchronized DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }
    public void init() {
        latestVersion = -1;
        if (!Rs.IS_EMULATOR) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]use real data.(Inband, OOB)");
            }
            File loadedDtaaConfigFile = (File)DataCenter.getInstance().get(DATA_CONFIG_IB);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded DTAA config file : "+loadedDtaaConfigFile);
            }
            if (loadedDtaaConfigFile != null) {
                dataUpdated(DATA_CONFIG_IB, null, loadedDtaaConfigFile);
            }
            DataCenter.getInstance().addDataUpdateListener(DATA_CONFIG_IB, this);
        } else {
            if (Rs.TEST_BY_MS_DATA) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Test by ms data.");
                }
                File fileDtaaConfig = new File("resource/test_data/ms_data/dtaa_config.txt");
                byte[] bytesDtaaConfig = Util.getByteArrayFromFile(fileDtaaConfig);
                parseDtaaConfig(bytesDtaaConfig);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Emulator - Properties.");
                }
            }
        }
    }
    public void dispose() {
        DataCenter.getInstance().removeDataUpdateListener(DATA_CONFIG_IB, this);
    }
    public void start() {
        isNeedMigrationSetting = false;
        flagIsNeedUpgrade = false;
        curCalledByValue = null;
    }
    public void stop() {
    }
    private boolean parseDtaaConfig(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.parseDtaaConfig]start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            } 
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM);
            return false;
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (version == latestVersion) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseDtaaConfig]Version is same. return true.");
                }
                return true;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Version : " + version);
            }
            //Category id
            int categoryIdLth = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Category Id Length : " + categoryIdLth);
            }
            String tempCategoryId = new String(src, index, categoryIdLth);
            index += categoryIdLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Category Id : " + tempCategoryId);
            }
            //Http request time
            int tempHttpRequestCount = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]HTTP Request Count : " + tempHttpRequestCount);
            }
            //Test Server List Size
            int tempTestServerListSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Test Server List Size : " + tempTestServerListSize);
            }
            ServerInfo[] tempServerInfoList = new ServerInfo[tempTestServerListSize];
            for (int i=0; i<tempTestServerListSize; i++) {
                ServerInfo tempServerInfo = new ServerInfo();
                //IP_DNS
                int ipDnsLth = src[index++];
                String tempIpDns = new String(src, index, ipDnsLth);
                index += ipDnsLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseDtaaConfig]Ip Dns - "+i+" : " + tempIpDns);
                }
                //Port
                int tempPort = Util.twoBytesToInt(src, index, true);
                index += 2;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseDtaaConfig]Port - "+i+" : " + tempPort);
                }
                //Context root
                int contextRootLth = src[index++];
                String tempcontextRoot = new String(src, index, contextRootLth);
                index += contextRootLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseDtaaConfig]Context root - "+i+" : " + tempcontextRoot);
                }
                tempServerInfo.setHost(tempIpDns);
                tempServerInfo.setPort(tempPort);
                tempServerInfo.setContextRoot(tempcontextRoot);
                tempServerInfoList[i] = tempServerInfo;
            }
            
            //Activation Server Info
            ServerInfo tempActServerInfo=new ServerInfo();
            //IP_DNS
            int tempActIpDnsLth = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Activation IP DNS Lth : " + tempActIpDnsLth);
            }
            String tempActIpDns = new String(src, index, tempActIpDnsLth);
            index += tempActIpDnsLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Activation IP DNS : " + tempActIpDns);
            }
            //Port
            int tempActPort = Util.twoBytesToInt(src, index, true);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Activation Port : " + tempActPort);
            }
            //Context root
            int tempActContextRootLth = src[index++];
            String tempActContextRoot = new String(src, index, tempActContextRootLth);
            index += tempActContextRootLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Activation Context Root : " + tempActContextRoot);
            }
            //Proxy - related
            int proxyListSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseDtaaConfig]Activation Proxy List Size : " + proxyListSize);
            }
            ProxyInfo[] tempProxyInfoList = new ProxyInfo[proxyListSize];
            for (int i=0; i<proxyListSize; i++) {
                tempProxyInfoList[i] = new ProxyInfo();
                //Proxy ip
                int proxyIpLth = src[index++];
                String proxyIp = new String(src, index, proxyIpLth, "ISO8859_1");
                index += proxyIpLth;
                tempProxyInfoList[i].setProxyHost(proxyIp);
                //Proxy port
                int proxyPort = Util.twoBytesToInt(src, index, true);
                index += 2;
                tempProxyInfoList[i].setProxyPort(proxyPort);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseDtaaConfig]Activation Proxy IP ["+i+"] : " + proxyIp);
                    Log.printDebug("[DataMgr.parseDtaaConfig]Activation Proxy Port ["+i+"] : " + proxyPort);
                }
            }
            tempActServerInfo.setHost(tempActIpDns);
            tempActServerInfo.setPort(tempActPort);
            tempActServerInfo.setContextRoot(tempActContextRoot);
            tempActServerInfo.setProxyInfos(tempProxyInfoList);
            
            //set Data
            categoryId = tempCategoryId;
            serverInfoList = tempServerInfoList;
            httpRequestCount = tempHttpRequestCount;
            activationServerInfo = tempActServerInfo;
            latestVersion = version;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            } 
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM);
            return false;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.parseDtaaConfig]Parsed properly. return true.");
        }
        return true;
    }
    /*****************************************************************
     * DataUpdateListener-implemented
     *****************************************************************/
    public void dataRemoved(String key) {
    }
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataUpdated]key : " + key);
            Log.printDebug("[DataMgr.dataUpdated]old : " + old);
            Log.printDebug("[DataMgr.dataUpdated]value : " + value);
        }
        if (key == null) {
            return;
        }
        if (key.equals(DATA_CONFIG_IB)) {
            File fileTargetData = (File) value;
            if (fileTargetData != null) {
                byte[] bytesTargetData = Util.getByteArrayFromFile(fileTargetData);
                parseDtaaConfig(bytesTargetData);
            }
        }
    }
    public String getCategoryId() {
        return categoryId;
    }
    public ServerInfo[] getServerInfoList() {
        return serverInfoList;
    }
    public int getHttpRequestCount() {
        return httpRequestCount;
    }
    public ServerInfo getActicationServerInfo() {
        return activationServerInfo;
    }
    public boolean isCalledByMonitor() {
        boolean res = false;
        String calledByValue = getCalldByValue();
        if (calledByValue != null && calledByValue.equalsIgnoreCase(APPLICATION_NAME_MONITOR)) {
            res = true;
        }
        return res;
    }
    public String getCalldByValue() {
        if (curCalledByValue == null) {
            curCalledByValue = CommunicationManager.getInstance().requestCalledByValue();
        }
        return curCalledByValue;
    }
    /*****************************************************************
     * MigrationAdapter-related
     *****************************************************************/
    public boolean isNeedMigrationSetting() {
        //TODO tklee
        //TODO always return false.(added Temporarly)
        /*if (true) {
            return false;
        }*/
        if (!flagIsNeedUpgrade) {
            flagIsNeedUpgrade = true;
            if (isCalledByMonitor()) {
                isNeedMigrationSetting = MigrationAdapter.getInstance().isNeedUpgrade();
                // write flag about migration usage. If this value is true, this value will change to SceneCompletion.
                if (isNeedMigrationSetting) {
                	PreferenceProxy.getInstance().setFlagNeedMigration(Definitions.OPTION_VALUE_YES);
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.isNeedMigrationSetting]" + isNeedMigrationSetting);
        }
        return isNeedMigrationSetting;
    }
    public void executeMigrationSetting() {
        if (!isNeedMigrationSetting()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.executeMigrationSetting]No need to execute migration setting module.");
            }
            isCompletedMigrationSetting = true;
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.executeMigrationSetting]Execute migration settings module.");
            }
            isCompletedMigrationSetting = MigrationAdapter.getInstance().executeSetting();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.executeMigrationSetting]isCompletedMigrationSetting : "+isCompletedMigrationSetting);
        }
    }
    public boolean isCompletedMigrationSetting() {
        if (!isNeedMigrationSetting()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.isCompletedMigrationSetting]No need to execute migration setting module. so return true.");
            }
            return true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.isCompletedMigrationSetting]Executed migration settings module. result - " + isCompletedMigrationSetting);
        }
        return isCompletedMigrationSetting;
    }
}

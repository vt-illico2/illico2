package com.videotron.tvi.illico.wizard.controller;

public class SceneTemplate {
    public static final int SCENE_ID_INVALID = -1;
    public static final int SCENE_ID_WELCOME = 0;
    public static final int SCENE_ID_NAVIGATION = 1;
    public static final int SCENE_ID_LANGUAGE = 2;
    public static final int SCENE_ID_ADMIN_PIN = 3;
    public static final int SCENE_ID_PARENTAL_CONTROL = 4;
    public static final int SCENE_ID_ORDER_PIN = 5;
    public static final int SCENE_ID_COMPLETE = 6;
    public static final int SCENE_ID_POWER_ON_CHANNEL = 7;
}

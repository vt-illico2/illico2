package com.videotron.tvi.illico.wizard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * @version $Revision: 1.69 $ $Date: 2017/01/09 21:08:33 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "(R7.2+CYO).1.0";//"R4_1.1.0";
    /** Application Name. */
    public static final String APP_NAME = "Wizard";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    public static final boolean TEST_BY_MS_DATA = true;
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    public static final Font F15 = FontResource.BLENDER.getFont(15, true);
    public static final Font F17 = FontResource.BLENDER.getFont(17, true);
    public static final Font F18 = FontResource.BLENDER.getFont(18, true);
    public static final Font F19 = FontResource.BLENDER.getFont(19, true);
    public static final Font F20 = FontResource.BLENDER.getFont(20, true);
    public static final Font F21 = FontResource.BLENDER.getFont(21, true);
    public static final Font F22 = FontResource.BLENDER.getFont(22, true);
    public static final Font F23 = FontResource.BLENDER.getFont(23, true);
    public static final Font F24 = FontResource.BLENDER.getFont(24, true);
    public static final Font F25 = FontResource.BLENDER.getFont(25, true);
    public static final Font F30 = FontResource.BLENDER.getFont(30, true);
    public static final Font F32 = FontResource.BLENDER.getFont(32, true);
    public static final Font F33 = FontResource.BLENDER.getFont(33, true);
    public static final Font F34 = FontResource.BLENDER.getFont(34, true);
    public static final Font F46 = FontResource.BLENDER.getFont(46, true);
    public static final Font F48 = FontResource.BLENDER.getFont(48, true);

    public static final FontMetrics FM15 = FontResource.getFontMetrics(F15);
    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
    public static final FontMetrics FM23 = FontResource.getFontMetrics(F23);
    public static final FontMetrics FM24 = FontResource.getFontMetrics(F24);
    public static final FontMetrics FM25 = FontResource.getFontMetrics(F25);
    public static final FontMetrics FM30 = FontResource.getFontMetrics(F30);
    public static final FontMetrics FM32 = FontResource.getFontMetrics(F32);
    public static final FontMetrics FM33 = FontResource.getFontMetrics(F33);
    public static final FontMetrics FM34 = FontResource.getFontMetrics(F34);
    public static final FontMetrics FM46 = FontResource.getFontMetrics(F46);
    public static final FontMetrics FM48 = FontResource.getFontMetrics(F48);
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C033033033 = new Color(33, 33, 33);
    public static final Color C035035035 = new Color(35, 35, 35);
    public static final Color C050050050 = new Color(50, 50, 50);
    public static final Color C057057057 = new Color(57, 57, 57);
    public static final Color C110110110 = new Color(110, 110, 110);
    public static final Color C133133133 = new Color(133, 133, 133);
    public static final Color C161160160 = new Color(161, 160, 160);
    public static final Color C164164164 = new Color(164, 164, 164);
    public static final Color C171171171 = new Color(171, 171, 171);
    public static final Color C182182182 = new Color(182, 182, 182);
    public static final Color C198198198 = new Color(198, 198, 198);
    public static final Color C200200200 = new Color(200, 200, 200);
    public static final Color C210210210 = new Color(210, 210, 210);
    public static final Color C229229229 = new Color(229, 229, 229);
    public static final Color C236211143 = new Color(236, 211, 143);
    public static final Color C236236237 = new Color(236, 236, 237);
    public static final Color C248063063 = new Color(248, 63, 63);
    public static final Color C254234160 = new Color(254, 234, 160);
    public static final Color C255075079 = new Color(255, 75, 79);
    public static final Color C255204000 = new Color(255, 204, 0);
    public static final Color C255212000 = new Color(255, 212, 0);
    public static final Color C255234160 = new Color(255, 234, 160);
    public static final Color C255255255 = new Color(255, 255, 255);

    // R7.2
    public static final Color C250202000 = new Color(250, 202, 0);
    public static final Color C249194000 = new Color(249, 194, 0);
    public static final Color C150150150 = new Color(150, 150, 150);
    public static final Color C021021021 = new Color(21, 21, 21);
    public static final Color C044044044 = new Color(44, 44, 44);
    public static final Color C066066066 = new Color(66, 66, 66);
    public static final Color C255228148 = new Color(255, 228, 148);

    public static final  Color DVB000000000204 = new DVBColor(0, 0, 0, 204);
    public static final  Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    public static final  Color DVB255255255128 = new DVBColor(255, 255, 255, 128);
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;

    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);
    
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
    public static final String ERROR_CODE_TEST_SERVER_ACCESS_PROBLEM = "WIZ502";
    public static final String ERROR_CODE_UPP_ACCESS_PROBLEM = "WIZ503";
    public static final String ERROR_CODE_VOD_ACCESS_PROBLEM = "WIZ504";
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM = "WIZ505";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "WIZ506";


}

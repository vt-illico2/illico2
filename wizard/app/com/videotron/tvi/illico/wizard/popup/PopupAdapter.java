package com.videotron.tvi.illico.wizard.popup;

public class PopupAdapter implements PopupListener {
    /*****************************************************************
     * methods - PopupRequest-related
     *****************************************************************/
    public Object request(PopupEvent popupEvent) throws Exception {
        return null;
    }

    public void requestCompleted(PopupEvent pe, Object resultObj) {
    }

    public void requestFailed(PopupEvent pe, String errorMessage) {
    }

    /*****************************************************************
     * methods - PopupButton-related
     *****************************************************************/
    public void popupOK(PopupEvent popupEvent) {
    }

    public void popupCancel(PopupEvent popupEvent) {
    }
    
    public void popupClose(PopupEvent popupEvent) {
    }
}

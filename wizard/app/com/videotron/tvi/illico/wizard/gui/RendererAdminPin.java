package com.videotron.tvi.illico.wizard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.ui.SceneAdminPin;

public class RendererAdminPin extends Renderer {
    private Image imgBG;
    private Image imgButtonLB;
    private Image imgButtonLF;
    private Image imgButtonSB;
    private Image imgButtonSF;
    private Image imgIconCheck;
//    private Image imgTxtShadow;
    
    public static final int BUTTON_GAP = 146;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("12_bg.jpg");
        imgButtonLB = DataCenter.getInstance().getImage("12_pos_focus_dim.png");
        imgButtonLF = DataCenter.getInstance().getImage("12_pos_focus.png");
        imgButtonSB = DataCenter.getInstance().getImage("12_focus_dim.png");
        imgButtonSF = DataCenter.getInstance().getImage("12_focus.png");
        imgIconCheck = DataCenter.getInstance().getImage("icon_g_check.png");
//        imgTxtShadow = DataCenter.getInstance().getImage("12_txtsha.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        if (imgBG != null) { g.drawImage(imgBG, 0, 0, c); }
        
        SceneAdminPin scene = (SceneAdminPin)c;
        boolean isExistAdminPin = scene.isExistAdminPin();
        //Exist admin pin
        if (isExistAdminPin) {
//            if (imgTxtShadow != null) { g.drawImage(imgTxtShadow, 366, 179, c); }
            String txtTitleYes = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Title_Yes");
            if (txtTitleYes != null) {
                int txtTitleYesWth = Rs.FM33.stringWidth(txtTitleYes);
                int gap=0;
                int imgCheckWth=0;
                if (imgIconCheck!=null) { 
                    gap=4;
                    imgCheckWth=imgIconCheck.getWidth(c); 
                }
                g.setFont(Rs.F33);
                g.setColor(Rs.C255204000);
                int startX=480 - ((txtTitleYesWth+gap+imgCheckWth)/2);
                g.drawString(txtTitleYes, startX, 181);
                if (imgIconCheck!=null) { 
                    g.drawImage(imgIconCheck, startX+txtTitleYesWth+gap, 159, c);
                }
            }
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String txtDescYes0 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_Yes_0");
            if (txtDescYes0!=null) {
                String[] txtDescYes0s=TextUtil.split(txtDescYes0, Rs.FM21, 518);
                for (int i=0; i<txtDescYes0s.length; i++) {
                    if (txtDescYes0s[i]==null) {
                        continue;
                    }
                    g.drawString(txtDescYes0s[i], 479-((Rs.FM21.stringWidth(txtDescYes0s[i])/2)), 232+(i*25));
                }
            }
            String txtDescYes1 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_Yes_1");
            if (txtDescYes1!=null) { g.drawString(txtDescYes1,  479-((Rs.FM21.stringWidth(txtDescYes1)/2)), 307); }
        } else {
//            if (imgTxtShadow != null) { g.drawImage(imgTxtShadow, 366, 179, c); }
            String txtTitleNo = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Title_No");
            if (txtTitleNo != null) {
                int txtTitleNoWth = Rs.FM33.stringWidth(txtTitleNo);
                g.setFont(Rs.F33);
                g.setColor(Rs.C255204000);
                g.drawString(txtTitleNo, 480 - (txtTitleNoWth/2), 181);
            }
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String txtDescNo0 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_No_0");
            if (txtDescNo0!=null) { g.drawString(txtDescNo0, 95, 228); }
            String txtDescNo1 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_No_1");
            if (txtDescNo1!=null) { g.drawString(txtDescNo1, 95, 276); }
            String txtDescNo2 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_No_2");
            if (txtDescNo2!=null) { g.drawString(txtDescNo2, 95, 300); }
            String txtDescNo3 = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Desc_No_3");
            if (txtDescNo3!=null) { g.drawString(txtDescNo3, 95, 324); }
        }
        //Button
        boolean isExtraButton = scene.isExtraButton();
        int curButton = scene.getCurrentButton();
        Image imgButtonL = null;
        if (isExtraButton) {
            imgButtonL = imgButtonLF;
        } else {
            imgButtonL = imgButtonLB;
        }
        if (imgButtonL != null) {
            g.drawImage(imgButtonL, 295, 346, c);
        }
        String txtButtonL = null;
        if (isExistAdminPin) {
            txtButtonL = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Button_Yes");
        } else {
            txtButtonL = DataCenter.getInstance().getString("TxtQSW.Admin_PIN_Button_No");            
        }
        if (txtButtonL != null) {
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            int txtButtonLWth = Rs.FM21.stringWidth(txtButtonL);
            g.drawString(txtButtonL, 480-(txtButtonLWth/2), 373);
        }
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<SceneAdminPin.BUTTON_COUNT; i++) {
            Image imgButtonS = null;
            if (i == curButton && !isExtraButton) {
                imgButtonS = imgButtonSF; 
            } else {
                imgButtonS = imgButtonSB;
            }
            if (imgButtonS != null) {
                g.drawImage(imgButtonS, 338 + (i*BUTTON_GAP), 432, c);
            }
            String txtButtonS = null;
            if (i == SceneAdminPin.BUTTON_PREV) {
                txtButtonS = DataCenter.getInstance().getString("TxtQSW.Previous");
            } else if (i == SceneAdminPin.BUTTON_NEXT) {
                if (isExistAdminPin) {
                    txtButtonS = DataCenter.getInstance().getString("TxtQSW.Next");
                }else {
                    txtButtonS = DataCenter.getInstance().getString("TxtQSW.Skip");
                }
            }
            if (txtButtonS != null) {
                int txtButtonSWth = Rs.FM18.stringWidth(txtButtonS);
                g.drawString(txtButtonS, 406 + (i*BUTTON_GAP) - (txtButtonSWth/2), 453);
            }
        }
    }
}

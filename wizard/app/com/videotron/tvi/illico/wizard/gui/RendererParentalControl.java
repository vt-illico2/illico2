package com.videotron.tvi.illico.wizard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.ui.SceneParentalControl;

public class RendererParentalControl extends Renderer {
    private Image imgBG;
    private Image imgButtonSB;
    private Image imgButtonSF;
    private Image imgButtonLB;
    private Image imgButtonLF;
    private Image imgIconCheck;
//    private Image imgTxtShadow;
    
    public static final int BUTTON_GAP = 146;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }
    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("12_bg.jpg");
        imgButtonSB = DataCenter.getInstance().getImage("12_focus_dim.png");
        imgButtonSF = DataCenter.getInstance().getImage("12_focus.png");
        imgButtonLB = DataCenter.getInstance().getImage("12_pos_focus_dim.png");
        imgButtonLF = DataCenter.getInstance().getImage("12_pos_focus.png");
        imgIconCheck = DataCenter.getInstance().getImage("icon_g_check.png");
//        imgTxtShadow = DataCenter.getInstance().getImage("12_txtsha.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void paint(Graphics g, UIComponent c) {
        if (imgBG != null) {
            g.drawImage(imgBG, 0, 0, c);
        }
        //Description
        SceneParentalControl scene = (SceneParentalControl)c;
        boolean isExistRatingValue = scene.isExistRatingValue();
        //Exist rating value
        if (isExistRatingValue) {
//            if (imgTxtShadow != null) { g.drawImage(imgTxtShadow, 366, 179, c); }
        	
            String txtTitleYes = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Title_Yes");
            if (txtTitleYes != null) {
                int txtTitleYesWth = Rs.FM33.stringWidth(txtTitleYes);
                int gap=0;
                int imgCheckWth=0;
                if (imgIconCheck!=null) { 
                    gap=4;
                    imgCheckWth=imgIconCheck.getWidth(c); 
                }
                g.setFont(Rs.F33);
                g.setColor(Rs.C255204000);
                int startX=480 - ((txtTitleYesWth+gap+imgCheckWth)/2);
                g.drawString(txtTitleYes, startX, 181);
                if (imgIconCheck!=null) { 
                    g.drawImage(imgIconCheck, startX+txtTitleYesWth+gap, 159, c);
                }
            }
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String txtDescYes0 = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Desc_Yes_0");
            if (txtDescYes0!=null) {
                String[] txtDescYes0s=TextUtil.split(txtDescYes0, Rs.FM21, 518);
                for (int i=0; i<txtDescYes0s.length; i++) {
                    if (txtDescYes0s[i]==null) {
                        continue;
                    }
                    g.drawString(txtDescYes0s[i], 479-((Rs.FM21.stringWidth(txtDescYes0s[i])/2)), 232+(i*25));
                }
            }
        } else {
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String desc0 = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Desc_No_0");
            if (desc0 != null) {
                int desc0Wth = Rs.FM21.stringWidth(desc0);
                g.drawString(desc0, 480 - (desc0Wth/2), 168);
            }
            String desc1 =  DataCenter.getInstance().getString("TxtQSW.Parental_Control_Desc_No_1");
            if (desc1 != null) {
                int desc1Wth = Rs.FM21.stringWidth(desc1);
                g.drawString(desc1, 480 - (desc1Wth/2), 190);
            }
            String desc2 = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Desc_No_2");
            if (desc2 != null) {
                int desc2Wth = Rs.FM21.stringWidth(desc2);
                g.drawString(desc2, 480 - (desc2Wth/2), 234);
            }
        }
        //Button
        boolean isExtraButton = scene.isExtraButton();
        int curButton = scene.getCurrentButton();
        Image imgButtonL = null;
        if (isExtraButton) {
            imgButtonL = imgButtonLF;
        } else {
            imgButtonL = imgButtonLB;
        }
        if (imgButtonL != null) {
            g.drawImage(imgButtonL, 295, 346, c);
        }
        String txtButtonL = null;
        if (isExistRatingValue) {
            txtButtonL = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Button_Yes");
        } else {
            txtButtonL = DataCenter.getInstance().getString("TxtQSW.Parental_Control_Button_No");            
        }
        if (txtButtonL != null) {
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            int txtButtonLWth = Rs.FM21.stringWidth(txtButtonL);
            g.drawString(txtButtonL, 480-(txtButtonLWth/2), 373);
        }
        g.setFont(Rs.F18);
        g.setColor(Rs.C003003003);
        for (int i=0; i<SceneParentalControl.BUTTON_COUNT; i++) {
            Image imgButtonS = null;
            if (i == curButton && !isExtraButton) {
                imgButtonS = imgButtonSF; 
            } else {
                imgButtonS = imgButtonSB;
            }
            if (imgButtonS != null) {
                g.drawImage(imgButtonS, 338 + (i*BUTTON_GAP), 432, c);
            }
            String txtButtonS = null;
            if (i == SceneParentalControl.BUTTON_PREV) {
                txtButtonS = DataCenter.getInstance().getString("TxtQSW.Previous");
            } else if (i == SceneParentalControl.BUTTON_NEXT) {
                if (isExistRatingValue) {
                    txtButtonS = DataCenter.getInstance().getString("TxtQSW.Next");
                }else {
                    txtButtonS = DataCenter.getInstance().getString("TxtQSW.Skip");
                }
            }
            if (txtButtonS != null) {
                int txtButtonSWth = Rs.FM18.stringWidth(txtButtonS);
                g.drawString(txtButtonS, 406 + (i*BUTTON_GAP) - (txtButtonSWth/2), 453);
            }
        }
    }
}

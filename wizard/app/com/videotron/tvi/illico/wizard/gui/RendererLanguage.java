package com.videotron.tvi.illico.wizard.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.ui.SceneLanguage;


public class RendererLanguage extends Renderer {
    public static final int PANEL_GAP = 418;
    public static final int CONTENT_GAP = 25;
    private String[][] txtKeyDescs = {
            {"J'aimerais poursuivre", "mon expérience en"}, {"I prefer my experience in"}
    };
    private String[] txtKeyLanguage = {"Français", "English"};
    
    private Image imgBG;
//    private Image imgShadow;
    private Image imgPanelF;
    private Image imgPanel;
    private Image imgArrowR;
    private Image imgArrowL;
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }
    
    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("12_bg.jpg");
//        imgShadow = DataCenter.getInstance().getImage("12_shadow.png");
        imgPanelF = DataCenter.getInstance().getImage("12_panel_foc.png");
        imgPanel = DataCenter.getInstance().getImage("12_panel.png");
        imgArrowR = DataCenter.getInstance().getImage("02_ars_r.png");
        imgArrowL = DataCenter.getInstance().getImage("02_ars_l.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        if (imgBG != null) {
            g.drawImage(imgBG, 0, 0, c);
        }
        SceneLanguage sceneLang = (SceneLanguage)c;
        int curLang = sceneLang.getCurrentLanguage();
        for (int i=0; i<SceneLanguage.LANGUAGE_COUNT; i++) {
//            g.drawImage(imgShadow, 39+(i*PANEL_GAP), 412, c);
            Color cContent = null;
            Color cLanguage = null;
            if (i == curLang) {
                g.drawImage(imgPanelF, 82+(i*PANEL_GAP), 170, c);
                cContent = Rs.C255255255;
                cLanguage = Rs.C255204000;
            } else {
                cContent = Rs.C161160160;
                cLanguage = Rs.C161160160;
                
            }
            g.drawImage(imgPanel, 86+(i*PANEL_GAP), 174, c);
            
            g.setColor(cContent);
            g.setFont(Rs.F22);
            String[] txtKeyDesc = txtKeyDescs[i];
            int txtKeyDescLth = txtKeyDesc.length;
            for (int j=0; j<txtKeyDescLth; j++) {
                g.drawString(txtKeyDesc[j], 117+(i*PANEL_GAP), 270+(j*CONTENT_GAP));
            }
            g.setColor(cLanguage);
            g.setFont(Rs.F34);
            int txtLangWth = Rs.FM34.stringWidth(txtKeyLanguage[i]);
            g.drawString(txtKeyLanguage[i], 417+(i*PANEL_GAP)-txtLangWth, 358);
            
            //Arrow
            if (curLang != 0) {
                g.drawImage(imgArrowL, 470, 290, c);
            }
            if (curLang != SceneLanguage.LANGUAGE_COUNT-1) {
                g.drawImage(imgArrowR, 461, 290, c);
            }
        }
        if (Log.EXTRA_ON) {
            //Version
            g.setFont(Rs.F17);
            g.setColor(Rs.C255255255);
            String data = "Ver v"+Rs.APP_VERSION;
            g.drawString(data, 914-Rs.FM17.stringWidth(data), 60);
        }
    }
}

package com.videotron.tvi.illico.wizard.gui;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.communication.CommunicationManager;
import com.videotron.tvi.illico.wizard.communication.PreferenceProxy;
import com.videotron.tvi.illico.wizard.ui.PopSelectChannelUI;
import com.videotron.tvi.illico.wizard.ui.ScenePowerOnChannel;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Vector;

public class RendererPowerOnChannel extends Renderer {
	private Image imgBG;
	private Image imgButtonBasic;
	private Image imgButtonFocus;
	private Image bgTopImg;
	private Image bgMidImg;
	private Image bgBottomImg;
	private Image inputFocusImg, inputDimImg;

	private String subtitleStr, prevStr, finishStr;
	private String[] explainStrArr;

	public Rectangle getPreferredBounds(UIComponent c) {
		return Rs.SCENE_BOUND;
	}

	public void prepare(UIComponent c) {
		imgBG = DataCenter.getInstance().getImage("12_bg.jpg");
		imgButtonBasic = DataCenter.getInstance().getImage("12_focus_dim.png");
		imgButtonFocus = DataCenter.getInstance().getImage("12_focus.png");

		bgBottomImg = DataCenter.getInstance().getImage("08_op_bg_b_304.png");
		bgTopImg = DataCenter.getInstance().getImage("08_op_bg_t_304.png");
		bgMidImg = DataCenter.getInstance().getImage("08_op_bg_m_304.png");

		inputFocusImg = DataCenter.getInstance().getImage("input_364_foc2.png");
		inputDimImg = DataCenter.getInstance().getImage("input_364_foc_dim.png");

		FrameworkMain.getInstance().getImagePool().waitForAll();

		subtitleStr = DataCenter.getInstance().getString("TxtQSW.Power_On_Channel_Title");
		explainStrArr = TextUtil
				.split(DataCenter.getInstance().getString("TxtQSW.Power_On_Channel_Explain"), Rs.FM18, 385, "|");
		prevStr = DataCenter.getInstance().getString("TxtQSW.Previous");
		finishStr = DataCenter.getInstance().getString("TxtQSW.Finish");
	}

	protected void paint(Graphics g, UIComponent c) {
		if (imgBG != null) {
			g.drawImage(imgBG, 0, 0, c);
		}
		ScenePowerOnChannel ui = (ScenePowerOnChannel) c;

		// subtitle
		g.setFont(Rs.F32);
		g.setColor(Rs.C250202000);
		GraphicUtil.drawStringCenter(g, subtitleStr, 480, 194);

		// explain
		g.setFont(Rs.F21);
		g.setColor(Rs.C200200200);
		for (int i = 0; i < explainStrArr.length; i++) {
			GraphicUtil.drawStringCenter(g, explainStrArr[i], 482, 241 + i * 26);
		}

		// Power-on Channel
		g.setColor(Rs.C255255255);
		if (ui.isFocusOnPowerOnChannel()) {
			g.drawImage(inputFocusImg, 298, 330, c);
		} else {
			g.drawImage(inputDimImg, 298, 330, c);
		}
		if (ui.isEnteringNumber()) {
			g.drawString(ui.getEnteringNumber(), 313, 357);
		} else {
			String value = PreferenceProxy.getInstance().getPowerOnChannel();

			String chNumber = TextUtil.EMPTY_STRING;
			String chName = TextUtil.EMPTY_STRING;
			EpgService epgService = CommunicationManager.getInstance().getEpgService();
			TvChannel powerOnChannel = null;
			try {
				powerOnChannel = epgService.getChannel(value);
				if (powerOnChannel != null) {
					chNumber = String.valueOf(powerOnChannel.getNumber());
				}
			} catch (RemoteException e) {
				Log.print(e);
			}

			if (!chNumber.equals(TextUtil.EMPTY_STRING) && !value.equals(Definitions.LAST_CHANNEL)) {
				try {
					TvChannel sisterChannel = powerOnChannel.getSisterChannel();
					if (sisterChannel != null && PreferenceProxy.getInstance().isChannelGrouped()) {
						// has sister channel
						if (powerOnChannel.isHd()) {
							chName = powerOnChannel.getFullName();
							chNumber = sisterChannel.getNumber() + " | " + powerOnChannel.getNumber() + " HD";
						} else {
							chName = sisterChannel.getFullName();
							chNumber = powerOnChannel.getNumber() + " | " + sisterChannel.getNumber() + " HD";
						}
					} else {
						try {
							if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_4K) {
								chNumber = powerOnChannel.getNumber() + " UHD";
							} else if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_HD) {
								chNumber = powerOnChannel.getNumber() + " HD";
							}
							chName = powerOnChannel.getFullName();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}

					String mixedStr = chNumber + "   " + chName;
					mixedStr = TextUtil.shorten(mixedStr, Rs.FM17, 250);
					g.drawString(mixedStr, 313, 357);
				} catch (RemoteException e) {
					Log.print(e);
				}
			} else {
				value = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
				g.drawString(value, 313, 357);
			}
		}

		// button
		if (ui.isFocusOnPowerOnChannel()) {
			g.drawImage(imgButtonBasic, 338, 432, c);
			g.drawImage(imgButtonBasic, 484, 432, c);
		} else if (ui.getButtonType() == ui.BUTTON_TYPE_PREV) {
			g.drawImage(imgButtonFocus, 338, 432, c);
			g.drawImage(imgButtonBasic, 484, 432, c);
		} else if (ui.getButtonType() == ui.BUTTON_TYPE_FINISH) {
			g.drawImage(imgButtonBasic, 338, 432, c);
			g.drawImage(imgButtonFocus, 484, 432, c);
		}
		g.setFont(Rs.F18);
		g.setColor(Rs.C003003003);
		GraphicUtil.drawStringCenter(g, prevStr, 408, 453);
		GraphicUtil.drawStringCenter(g, finishStr, 554, 453);

		if (ui.isEnteringNumber()) {
			g.setColor(Rs.C182182182);
			Vector v = ui.getFoundChannel();
			int i = 0;
			int step = 0;
			int posX = 585;
			g.drawImage(bgTopImg, posX, 210, posX + 304, 213, 0, 0, 304, 3, c);
			for (i = 0; i < 9; i++) {
				step = i * 27;

				g.drawImage(bgMidImg, posX, 213 + step, c);

				if (i < v.size()) {
					Object[] channelData = (Object[]) v.get(i);
					String name = (String) channelData[PopSelectChannelUI.INDEX_CHANNEL_NAME];
					String number = TextUtil.EMPTY_STRING;
					if (!name.equals(Definitions.LAST_CHANNEL)) {
						String chNumber = TextUtil.EMPTY_STRING;
						EpgService epgService = CommunicationManager.getInstance().getEpgService();
						TvChannel ch = null;
						try {
							ch = epgService.getChannel(name);
							if (ch != null) {
								chNumber = String.valueOf(ch.getNumber());
							}
						} catch (RemoteException e) {
							Log.print(e);
						}

						if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
							try {
								TvChannel sisterChannel = ch.getSisterChannel();
								if (sisterChannel != null && PreferenceProxy.getInstance().isChannelGrouped()) {
									// has sister channel
									if (ch.isHd()) {
										name = ch.getFullName();
										// VDTRMASTER-5697
										number = sisterChannel.getNumber() + " | " + ch.getNumber() + " HD";
									} else {
										name = sisterChannel.getFullName();
										// VDTRMASTER-5697
										number = ch.getNumber() + " | " + sisterChannel.getNumber() + " HD";
									}
								} else {
									try {
										if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
											number = String.valueOf(ch.getNumber()) + " UHD";
										} else if (ch.getDefinition() == TvChannel.DEFINITION_HD) {
											number = String.valueOf(ch.getNumber()) + " HD";
										} else {
											number = String.valueOf(ch.getNumber());
										}
										name = ch.getFullName();
									} catch (RemoteException e) {
										e.printStackTrace();
									}
								}
								name = TextUtil.shorten(name, Rs.FM17, 175);
							} catch (RemoteException e) {
								Log.print(e);
							}
						} else {
							number = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
							name = TextUtil.EMPTY_STRING;
						}
					} else {
						number = DataCenter.getInstance().getString("TxtQSW." + Definitions.LAST_CHANNEL);
						name = TextUtil.EMPTY_STRING;

					}

					g.drawString(number, posX + 16, 232 + step);
					g.drawString(name, posX + 119, 232 + step);
				}
			}
			g.drawImage(bgBottomImg, posX, 227 + step + 10, posX + 304, 227 + step + 37, 0, 11, 304, 38, c);
		}
	}

	/**
	 * Paint a selected content.
	 *
	 * @param g
	 *            Graphics to paint.
	 * @param parent
	 *            parent UI.
	 */
	public void paintSelectedContent(Graphics g, UIComponent parent) {
		ScenePowerOnChannel ui = (ScenePowerOnChannel) parent;

		g.setColor(Rs.C255255255);
		g.setFont(Rs.F21);

		if (ui.isEnteringNumber()) {
			g.drawString(ui.getEnteringNumber(), 313, 357);
		} else {
			String value = PreferenceProxy.getInstance().getPowerOnChannel();

			String chNumber = TextUtil.EMPTY_STRING;
			String chName = TextUtil.EMPTY_STRING;
			EpgService epgService = CommunicationManager.getInstance().getEpgService();
			TvChannel powerOnChannel = null;
			try {
				powerOnChannel = epgService.getChannel(value);
				if (powerOnChannel != null) {
					chNumber = String.valueOf(powerOnChannel.getNumber());
				}
			} catch (RemoteException e) {
				Log.print(e);
			}
			if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
				try {
					TvChannel sisterChannel = powerOnChannel.getSisterChannel();
					if (sisterChannel != null && PreferenceProxy.getInstance().isChannelGrouped()) {
						// has sister channel
						if (powerOnChannel.isHd()) {
							chName = powerOnChannel.getFullName();
							chNumber =  sisterChannel.getNumber() + " | " + powerOnChannel.getNumber() + " HD";
						} else {
							chName = sisterChannel.getFullName();
							chNumber =  powerOnChannel.getNumber() + " | " + sisterChannel.getNumber() + " HD";
						}
					} else {
						try {
							if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_4K) {
								chNumber = powerOnChannel.getNumber() + " UHD";
							} else if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_HD) {
								chNumber = powerOnChannel.getNumber() + " HD";
							}
							chName = powerOnChannel.getFullName();
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}

					String mixedStr = chNumber + "   " + chName;
					mixedStr = TextUtil.shorten(mixedStr, Rs.FM17, 250);
					g.drawString(mixedStr, 313, 357);
				} catch (RemoteException e) {
					Log.print(e);
				}
			} else {
				value = DataCenter.getInstance().getString(Definitions.LAST_CHANNEL);
				g.drawString(value, 313, 357);
			}
		}
	}
}

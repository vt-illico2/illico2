package com.videotron.tvi.illico.wizard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.wizard.Rs;

public class RendererNavigation extends Renderer {
    private final String txtButtonFr = "Appuyez sur OK/Select pour poursuivre.";
    private final String txtButtonEn = "Press OK/Select to continue.";
    private final String[] txtDescFr = {
            "Naviguez à l'aide des flèches de la télécommande et",
            "confirmez votre choix en appuyant sur OK/Select."
    };
    private final String[] txtDescEn = {
            "Navigate using the arrows of the remote control and",
            "confirm your selection by pressing OK/Select."
    };
    private Image imgBG;
    private Image imgPanel;
    private Image imgKey;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }
    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("bg.jpg");
        imgPanel = DataCenter.getInstance().getImage("12_panel02.png");
        imgKey = DataCenter.getInstance().getImage("key_img.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void paint(Graphics g, UIComponent c) {
        if (imgBG != null) {
            g.drawImage(imgBG, 0, 0, c);
        }
        if (imgPanel != null) {
            g.drawImage(imgPanel, 0, -30, c);
        }
        if (imgKey != null) {
            g.drawImage(imgKey, 95, 145, c);
        }

        g.setFont(Rs.F23);
        g.setColor(Rs.C255255255);
        for (int i=0; i<txtDescFr.length; i++) {
            g.drawString(txtDescFr[i], 294, 186 + (i * 23));
        }
        g.setFont(Rs.F23);
        for (int i=0; i<txtDescEn.length; i++) {
            g.drawString(txtDescEn[i], 294, 256 + (i * 20));
        }
        g.setFont(Rs.F23);
        g.setColor(Rs.C255204000);
        g.drawString(txtButtonFr, 298, 407);
        g.setFont(Rs.F20);
        g.drawString(txtButtonEn, 365, 432);
    }
}

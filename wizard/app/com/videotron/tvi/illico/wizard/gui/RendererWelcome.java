package com.videotron.tvi.illico.wizard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.ui.SceneWelcome;

public class RendererWelcome extends Renderer {
    private final String txtButtonEn = "Start";
    private final String txtButtonFr = "Débuter";
    private final String txtDesNewIllico2Terminal = "Bienvenue / Welcome";
//    private final String txtDesUpdatedNewVersionFr = "Bienvenue dans le nouvel univers d’illico télé.";
//    private final String txtDesUpdatedNewVersionEn = "Welcome to the world of illico TV.";
    
    private final String txtMigTitleFr="Mise à jour de votre terminal en cours";
    private final String txtMigTitleEn="Terminal update in progress";
    private final String txtMigNoticeFr="ATTENTION : ";
    private final String txtMigNoticeEn="NOTICE : ";
    private final String txtMigNoticeContFr0="La mise à jour peut prendre jusqu’à 60 minutes.";
    private final String txtMigNoticeContFr1="Le terminal ne doit pas être débranché.";
    private final String txtMigNoticeContEn0="The update may take up to 60 minutes.";
    private final String txtMigNoticeContEn1="Do not disconnect the terminal.";
    
    private Image imgBG;
    private Image imgBGMig;
    private Image imgLogo;
    private Image imgButtonFocus;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }
    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("bg.jpg");
        imgBGMig = DataCenter.getInstance().getImage("WIZZARD_03-2.jpg");
        imgLogo = DataCenter.getInstance().getImage("12_logo.png");
        imgButtonFocus = DataCenter.getInstance().getImage("12_pos_focus00.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    protected void paint(Graphics g, UIComponent c) {
        SceneWelcome scene = (SceneWelcome)c;
        boolean isNeedMigrationSetting = scene.isNeedMigrationSetting();
        //Description
        if (isNeedMigrationSetting) {
            if (imgBGMig != null) {
                g.drawImage(imgBGMig, 0, 0, c);
            }
            g.setFont(Rs.F30);
            g.setColor(Rs.C255212000);
            g.drawString(txtMigTitleFr, 480 - (Rs.FM30.stringWidth(txtMigTitleFr)/2), 82);
            g.setFont(Rs.F25);
            g.drawString(txtMigTitleEn, 480 - (Rs.FM25.stringWidth(txtMigTitleEn)/2), 118);
            
            g.setFont(Rs.F30);
            g.setColor(Rs.C255212000);
            int txtMigNoticeFrWth = Rs.FM30.stringWidth(txtMigNoticeFr);
            int txtMigNoticeContFr0Wth = Rs.FM30.stringWidth(txtMigNoticeContFr0);
            int txtMigNoticeFrX=480 - ((txtMigNoticeFrWth+txtMigNoticeContFr0Wth)/2);
            g.drawString(txtMigNoticeFr, txtMigNoticeFrX, 354);
            g.setColor(Rs.C255255255);
            g.drawString(txtMigNoticeContFr0, txtMigNoticeFrX + txtMigNoticeFrWth, 354);
            g.drawString(txtMigNoticeContFr1, 480 - (Rs.FM30.stringWidth(txtMigNoticeContFr1)/2), 384);
            
            g.setFont(Rs.F25);
            g.setColor(Rs.C255212000);
            int txtMigNoticeEnWth = Rs.FM25.stringWidth(txtMigNoticeEn);
            int txtMigNoticeContEn0Wth = Rs.FM25.stringWidth(txtMigNoticeContEn0);
            int txtMigNoticeEnX=480 - ((txtMigNoticeEnWth+txtMigNoticeContEn0Wth)/2);
            g.drawString(txtMigNoticeEn, txtMigNoticeEnX, 440);
            g.setColor(Rs.C255255255);
            g.drawString(txtMigNoticeContEn0, txtMigNoticeEnX + txtMigNoticeEnWth, 440);
            g.drawString(txtMigNoticeContEn1, 480 - (Rs.FM25.stringWidth(txtMigNoticeContEn1)/2), 466);
        } else {
            if (imgBG != null) {
                g.drawImage(imgBG, 0, 0, c);
            }
            if (imgLogo != null) {
                g.drawImage(imgLogo, 176, 47, c);
            }
            if (imgButtonFocus != null) {
                g.drawImage(imgButtonFocus, 352, 409, c);
            }
            g.setFont(Rs.F19);
            g.setColor(Rs.C003003003);
            g.drawString(txtButtonFr, 402 - (Rs.FM19.stringWidth(txtButtonFr)/2), 433);
            g.drawString(txtButtonEn, 559 - (Rs.FM19.stringWidth(txtButtonEn)/2), 433);
            g.setFont(Rs.F24);
            g.setColor(Rs.C255212000);
            g.drawString(txtDesNewIllico2Terminal, 482 - (Rs.FM24.stringWidth(txtDesNewIllico2Terminal)/2), 109);
        }
        //Version
        if (Log.EXTRA_ON) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C255255255);
            String data = "Ver v"+Rs.APP_VERSION;
            g.drawString(data, 914-Rs.FM17.stringWidth(data), 60);
        }
     }
}

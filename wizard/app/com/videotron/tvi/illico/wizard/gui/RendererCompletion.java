package com.videotron.tvi.illico.wizard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.ui.SceneCompletion;

public class RendererCompletion extends Renderer {
    private Image imgBG;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    private Image imgIconResultCompletion;
    private Image imgIconResultIncompletion;
//    private Image imgTxtShadow;
    private Image imgBannerBasic;
    private Image imgBannerFocus;
    
    private Image imgRcuAni;
//    private Image imgRcuAniBg;
//    private Image imgRcuPanel;
//    private Image imgRcuShadow;

    private Image imgButtonBox;
   
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("12_bg.jpg");
        imgButtonBasic = DataCenter.getInstance().getImage("12_pos_focus02_dim.png");
        imgButtonFocus = DataCenter.getInstance().getImage("12_pos_focus02.png");
        imgIconResultCompletion = DataCenter.getInstance().getImage("icon_g_check.png");
        imgIconResultIncompletion = DataCenter.getInstance().getImage("icon_r_x.png");
//        imgTxtShadow = DataCenter.getInstance().getImage("12_txtsha.png");
        imgBannerBasic = DataCenter.getInstance().getImage("12_com_banner.png");
        imgBannerFocus = DataCenter.getInstance().getImage("12_com_banner_f.png");
        
        imgRcuAni = DataCenter.getInstance().getImage("rcu_ani.gif");
//        imgRcuAniBg = DataCenter.getInstance().getImage("rcu_panel_foc.png");
//        imgRcuPanel = DataCenter.getInstance().getImage("12_rcu_panel.png");
//        imgRcuShadow = DataCenter.getInstance().getImage("12_rcu_shadow.png");

        // R7.2
        imgButtonBox = DataCenter.getInstance().getImage("12_compl_box.png");

        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    protected void paint(Graphics g, UIComponent c) {
        if (imgBG != null) {
            g.drawImage(imgBG, 0, 0, c);
        }

        // background
        g.setColor(Rs.C021021021);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

        SceneCompletion scene = (SceneCompletion)c;
        boolean isCompletedProcess = scene.isCompletedProcess();
        if (!isCompletedProcess) {
            return;
        }
        int completionType = scene.getCompletionType();
        int buttonType = scene.getButtonType();
        //Result text
        if (completionType == SceneCompletion.TYPE_COMPLETE_COMMON) {
            // R7.2
            String titleStr = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Common_Status_New");
            String[] explainStrArr = TextUtil.split(DataCenter.getInstance().getString(
                    "TxtQSW.Completion_Comp_Common_Explain"), Rs.FM21, 400, "|" );
            String selectExplainStr = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Common_Select_Explain");
            String select1Str = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Common_Select1");
            String select2Str = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Common_Select2");
            String select3Str = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Common_Select3");

            // title
            g.setFont(Rs.F32);
            g.setColor(Rs.C250202000);
            int x = GraphicUtil.drawStringCenter(g, titleStr, 464, 144);
            g.drawImage(imgIconResultCompletion, x + 10 + Rs.FM32.stringWidth(titleStr), 123, c);

            // explain
            g.setFont(Rs.F21);
            g.setColor(Rs.C236236237);
            for (int i = 0;i < explainStrArr.length; i++) {
                GraphicUtil.drawStringCenter(g, explainStrArr[i], 482, 181 + i * 26);
            }

            // button box
            g.drawImage(imgButtonBox, 280, 243, c);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            GraphicUtil.drawStringCenter(g, selectExplainStr, 481, 289);

            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                // focus on the button to exit to channel
                g.setColor(Rs.C249194000);
                g.fillRect(342, 321, 276, 38);
                g.setColor(Rs.C150150150);
                g.fillRect(342, 365, 276, 38);
                g.fillRect(342, 409, 276, 38);
            } else if (buttonType == SceneCompletion.BUTTON_TYPE_RCU) {
                // focus on the button to show RCU popup
                g.setColor(Rs.C150150150);
                g.fillRect(342, 321, 276, 38);
                g.setColor(Rs.C249194000);
                g.fillRect(342, 365, 276, 38);
                g.setColor(Rs.C150150150);
                g.fillRect(342, 409, 276, 38);
            } else if (buttonType == SceneCompletion.BUTTON_TYPE_BANNER) {
                // focus on the button to go to Help
                g.setColor(Rs.C150150150);
                g.fillRect(342, 321, 276, 38);
                g.fillRect(342, 365, 276, 38);
                g.setColor(Rs.C249194000);
                g.fillRect(342, 409, 276, 38);
            }
            g.setFont(Rs.F20);
            g.setColor(Rs.C003003003);
            GraphicUtil.drawStringCenter(g, select1Str, 482, 346);
            GraphicUtil.drawStringCenter(g, select2Str, 482, 390);
            GraphicUtil.drawStringCenter(g, select3Str, 482, 434);

        } else  if (completionType == SceneCompletion.TYPE_COMPLETE_NON_PVR) {
            String txtResult = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_Status");
            int txtResultWth = Rs.FM33.stringWidth(txtResult);
            int imgIconResultWth = imgIconResultCompletion.getWidth(c);
//            g.drawImage(imgTxtShadow, 331, 196, c);
            g.setFont(Rs.F33);
            g.setColor(Rs.C255204000);
            g.drawString(txtResult, 480 - ((txtResultWth + imgIconResultWth + 4) / 2), 147);
            g.drawImage(imgIconResultCompletion, 480 - ((txtResultWth + imgIconResultWth + 4) / 2) + 4 + txtResultWth, 126, c);
            
            g.setFont(Rs.F23);
            g.setColor(Rs.C255255255);
            String txtSubtitle = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_Desc");
            g.drawString(txtSubtitle, 480 - (Rs.FM23.stringWidth(txtSubtitle) / 2), 194);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String[] txtDesc = TextUtil.split(DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_SubDesc"), Rs.FM21, 740);
            for (int i=0; i<txtDesc.length; i++) {
                g.drawString(txtDesc[i], 480 - (Rs.FM21.stringWidth(txtDesc[i]) / 2), 250+(i*20));
            }
            if (buttonType == SceneCompletion.BUTTON_TYPE_BANNER) {
                g.drawImage(imgBannerFocus, 280, 296, c);
            } else {
                g.drawImage(imgBannerBasic, 280, 296, c);
            }
            g.setFont(Rs.F21);
            String txtBanner0 = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_Button_Banner_0");
            g.setColor(Rs.C057057057);
            g.drawString(txtBanner0, 449 - (Rs.FM21.stringWidth(txtBanner0)/2), 328);
            String txtBanner1 = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_Button_Banner_1");
            g.setColor(Rs.C003003003);
            g.drawString(txtBanner1, 452 - (Rs.FM21.stringWidth(txtBanner1)/2), 349);
            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                g.drawImage(imgButtonFocus, 276, 377, c);
            } else {
                g.drawImage(imgButtonBasic, 276, 377, c);
            }
            String txtButton = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_Non_PVR_Button_Exit");
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 480 - (Rs.FM21.stringWidth(txtButton)/2), 404);
        } else  if (completionType == SceneCompletion.TYPE_COMPLETE_PVR) {
            String txtResult = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_Status");
            int txtResultWth = Rs.FM33.stringWidth(txtResult);
            int imgIconResultWth = imgIconResultCompletion.getWidth(c);
//            g.drawImage(imgTxtShadow, 331, 196, c);
            g.setFont(Rs.F33);
            g.setColor(Rs.C255204000);
            g.drawString(txtResult, 480 - ((txtResultWth + imgIconResultWth + 4) / 2), 147);
            g.drawImage(imgIconResultCompletion, 480 - ((txtResultWth + imgIconResultWth + 4) / 2) + 4 + txtResultWth, 126, c);
            
            g.setFont(Rs.F23);
            g.setColor(Rs.C255255255);
            String txtSubtitle = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_Desc");
            g.drawString(txtSubtitle, 480 - (Rs.FM23.stringWidth(txtSubtitle) / 2), 194);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String[] txtDesc = TextUtil.split(DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_SubDesc"), Rs.FM21, 740);
            for (int i=0; i<txtDesc.length; i++) {
                g.drawString(txtDesc[i], 480 - (Rs.FM21.stringWidth(txtDesc[i]) / 2), 250+(i*20));
            }
            if (buttonType == SceneCompletion.BUTTON_TYPE_BANNER) {
                g.drawImage(imgBannerFocus, 280, 296, c);
            } else {
                g.drawImage(imgBannerBasic, 280, 296, c);
            }
            g.setFont(Rs.F21);
            String txtBanner0 = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_Button_Banner_0");
            g.setColor(Rs.C057057057);
            g.drawString(txtBanner0, 449 - (Rs.FM21.stringWidth(txtBanner0)/2), 328);
            String txtBanner1 = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_Button_Banner_1");
            g.setColor(Rs.C003003003);
            g.drawString(txtBanner1, 452 - (Rs.FM21.stringWidth(txtBanner1)/2), 349);
            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                g.drawImage(imgButtonFocus, 276, 377, c);
            } else {
                g.drawImage(imgButtonBasic, 276, 377, c);
            }
            String txtButton = DataCenter.getInstance().getString("TxtQSW.Completion_Comp_PVR_Button_Exit");
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 480 - (Rs.FM21.stringWidth(txtButton)/2), 404);
        } else  if (completionType == SceneCompletion.TYPE_INCOMPLETE_MIGRATION_PROBLEM) {
            String txtResult = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_Status");
            int txtResultWth = Rs.FM33.stringWidth(txtResult);
            int imgIconResultWth = imgIconResultIncompletion.getWidth(c);
//            g.drawImage(imgTxtShadow, 331, 196, c);
            g.setFont(Rs.F33);
            g.setColor(Rs.C255075079);
            g.drawString(txtResult, 480 - ((txtResultWth + imgIconResultWth + 4) / 2), 147);
            g.drawImage(imgIconResultIncompletion, 480 - ((txtResultWth + imgIconResultWth + 4) / 2) + 4 + txtResultWth, 126, c);
            
            g.setFont(Rs.F23);
            String txtDescF = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_Desc_F");
            int txtDescFWth = Rs.FM23.stringWidth(txtDescF);
            String txtDescB = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_Desc_B");
            int txtDescBWth = Rs.FM23.stringWidth(txtDescB);
            String txtCallCenter = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_Call_Center");
            int txtCallCenterWth = Rs.FM23.stringWidth(txtCallCenter);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescF, 480 - ((txtDescFWth+txtCallCenterWth)/2), 223);
            g.setColor(Rs.C255204000);
            g.drawString(txtCallCenter, 484 - ((txtDescFWth+txtCallCenterWth)/2) + txtDescFWth, 223);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescB, 480 - (txtDescBWth/2), 300);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String[] txtSubDesc = TextUtil.split(DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_SubDesc"), Rs.FM21, 740);
            for (int i=0; i<txtSubDesc.length; i++) {
                g.drawString(txtSubDesc[i], 480 - (Rs.FM21.stringWidth(txtSubDesc[i])/2), 298 + (i * 22));
            }
            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                g.drawImage(imgButtonFocus, 276, 377, c);
            } else {
                g.drawImage(imgButtonBasic, 276, 377, c);
            }
            String txtButton = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Migration_Button_Exit");
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 480 - (Rs.FM21.stringWidth(txtButton)/2), 404);
        } else  if (completionType == SceneCompletion.TYPE_INCOMPLETE_IP_COMMUNICATION_PROBLEM) {
            String txtResult = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_Status");
            int txtResultWth = Rs.FM33.stringWidth(txtResult);
            int imgIconResultWth = imgIconResultIncompletion.getWidth(c);
//            g.drawImage(imgTxtShadow, 331, 196, c);
            g.setFont(Rs.F33);
            g.setColor(Rs.C255075079);
            g.drawString(txtResult, 480 - ((txtResultWth + imgIconResultWth + 4) / 2), 147);
            g.drawImage(imgIconResultIncompletion, 480 - ((txtResultWth + imgIconResultWth + 4) / 2) + 4 + txtResultWth, 126, c);
            
            g.setFont(Rs.F23);
            String txtDescF = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_Desc_F");
            int txtDescFWth = Rs.FM23.stringWidth(txtDescF);
            String txtDescB = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_Desc_B");
            int txtDescBWth = Rs.FM23.stringWidth(txtDescB);
            String txtCallCenter = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_Call_Center");
            int txtCallCenterWth = Rs.FM23.stringWidth(txtCallCenter);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescF, 480 - ((txtDescFWth+txtCallCenterWth)/2), 223);
            g.setColor(Rs.C255204000);
            g.drawString(txtCallCenter, 484 - ((txtDescFWth+txtCallCenterWth)/2) + txtDescFWth, 223);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescB, 480 - (txtDescBWth/2), 250);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
            String[] txtSubDesc = TextUtil.split(DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_SubDesc"), Rs.FM21, 740);
            for (int i=0; i<txtSubDesc.length; i++) {
                g.drawString(txtSubDesc[i], 480 - (Rs.FM21.stringWidth(txtSubDesc[i])/2), 298 + (i * 22));
            }
            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                g.drawImage(imgButtonFocus, 276, 377, c);
            } else {
                g.drawImage(imgButtonBasic, 276, 377, c);
            }
            String txtButton = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Signal_Button_Exit");
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 480 - (Rs.FM21.stringWidth(txtButton)/2), 404);
        } else  if (completionType == SceneCompletion.TYPE_INCOMPLETE_ACTIVATION_PROBLEM) {
            String txtResult = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_Status");
            int txtResultWth = Rs.FM33.stringWidth(txtResult);
            int imgIconResultWth = imgIconResultIncompletion.getWidth(c);
//            g.drawImage(imgTxtShadow, 331, 224, c);
            g.setFont(Rs.F33);
            g.setColor(Rs.C255075079);
            g.drawString(txtResult, 480 - ((txtResultWth + imgIconResultWth + 4) / 2), 175);
            g.drawImage(imgIconResultIncompletion, 480 - ((txtResultWth + imgIconResultWth + 4) / 2) + 4 + txtResultWth, 154, c);
            
            g.setFont(Rs.F23);
            String txtDescF = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_Desc_F");
            int txtDescFWth = Rs.FM23.stringWidth(txtDescF);
            String txtDescB = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_Desc_B");
            int txtDescBWth = Rs.FM23.stringWidth(txtDescB);
            String txtCallCenter = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_Call_Center");
            int txtCallCenterWth = Rs.FM23.stringWidth(txtCallCenter);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescF, 480 - ((txtDescFWth+txtCallCenterWth)/2), 260);
            g.setColor(Rs.C255204000);
            g.drawString(txtCallCenter, 484 - ((txtDescFWth+txtCallCenterWth)/2) + txtDescFWth, 260);
            g.setColor(Rs.C255255255);
            g.drawString(txtDescB, 480 - (txtDescBWth/2), 286);
            g.setFont(Rs.F21);
            g.setColor(Rs.C200200200);
//            String[] txtSubDesc = TextUtil.split(DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_SubDesc"), Rs.FM21, 740);
//            for (int i=0; i<txtSubDesc.length; i++) {
//                g.drawString(txtSubDesc[i], 480 - (Rs.FM21.stringWidth(txtSubDesc[i])/2), 348 + (i * 22));
//            }
            if (buttonType == SceneCompletion.BUTTON_TYPE_EXIT) {
                g.drawImage(imgButtonFocus, 276, 377, c);
            } else {
                g.drawImage(imgButtonBasic, 276, 377, c);
            }
            String txtButton = DataCenter.getInstance().getString("TxtQSW.Completion_Incomp_Activation_Button_Exit");
            g.setFont(Rs.F21);
            g.setColor(Rs.C003003003);
            g.drawString(txtButton, 480 - (Rs.FM21.stringWidth(txtButton)/2), 404);
        }
    }
}

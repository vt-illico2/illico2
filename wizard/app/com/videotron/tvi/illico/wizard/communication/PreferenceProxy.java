package com.videotron.tvi.illico.wizard.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.util.TextUtil;
import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.App;
import com.videotron.tvi.illico.wizard.Rs;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.<p>
 * Try to look up UPP Service at the time of initializing this class.
 * If successful, upp data get from the PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    private static PreferenceProxy instance;

    private static final String UPP_KEY_COMPLETE_WIZARD = "COMPLETE_WIZARD";
    private static final String UPP_VALUE_COMPLETE_WIZARD = "true";
    //for migration
    private static final String UPP_KEY_COMPLETE_MIGRATION = "COMPLETE_MIGRATION";
    private static final String UPP_VALUE_COMPLETE_MIGRATION = "true";
    private static final String UPP_VALUE_INCOMPLETE_MIGRATION = "false";
    
    private static final String UPP_KEY_FLAG_NEED_MIGRATION="FLAG_NEED_MIGRATION";

    // R5-TANK
    private static final int UPP_INDEX_COUNT = 9;
    private static final int UPP_INDEX_LANGUAGE = 0;
    private static final int UPP_INDEX_BLOCK_BY_RATING = 1;
    private static final int UPP_INDEX_PARENTAL_CONTROL = 2;
    private static final int UPP_INDEX_COMPLETE_MIGRATION = 3;
    private static final int UPP_INDEX_FLAG_NEED_MIGRATION = 4;
    // R5-TANK
    private static final int UPP_INDEX_DISPLAY_MENU = 5;
    // R7.2
    private static final int UPP_INDEX_CHANNELS_GROUPING = 6;
    private static final int UPP_INDEX_POWER_ON_CHANNEL = 7;
    private static final int UPP_INDEX_FAVORITE_CHANNELS = 8;

    private String[] prefNames;
    private String[] prefValues;
    private PreferenceService prefSvc;

    private PreferencProxyUpdateListener listener;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
        prefNames[UPP_INDEX_BLOCK_BY_RATING] = RightFilter.BLOCK_BY_RATINGS;
        prefNames[UPP_INDEX_PARENTAL_CONTROL] = RightFilter.PARENTAL_CONTROL;
        prefNames[UPP_INDEX_COMPLETE_MIGRATION] = UPP_KEY_COMPLETE_MIGRATION;
        prefNames[UPP_INDEX_FLAG_NEED_MIGRATION] = UPP_KEY_FLAG_NEED_MIGRATION;
        // R5-TANK
        prefNames[UPP_INDEX_DISPLAY_MENU] = PreferenceNames.DISPLAY_MENU_AT_POWER_ON;
        // R7.2
        prefNames[UPP_INDEX_CHANNELS_GROUPING] = PreferenceNames.SD_HD_CHANNELS_GROUPING;
        prefNames[UPP_INDEX_POWER_ON_CHANNEL] = PreferenceNames.POWER_ON_CHANNEL;
        prefNames[UPP_INDEX_FAVORITE_CHANNELS] = PreferenceNames.FAVOURITE_CHANNEL;

        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_FRENCH;
        prefValues[UPP_INDEX_BLOCK_BY_RATING] = Definitions.RATING_G;
        prefValues[UPP_INDEX_PARENTAL_CONTROL] = Definitions.OPTION_VALUE_OFF;
        prefValues[UPP_INDEX_COMPLETE_MIGRATION] = UPP_VALUE_COMPLETE_MIGRATION;
        prefValues[UPP_INDEX_FLAG_NEED_MIGRATION] = Definitions.OPTION_VALUE_NO;
        // R5-TANK
        prefValues[UPP_INDEX_DISPLAY_MENU] = Definitions.OPTION_VALUE_YES;
        // R7.2
        prefValues[UPP_INDEX_CHANNELS_GROUPING] = Definitions.SD_HD_CHANNEL_GROUPED;
        prefValues[UPP_INDEX_POWER_ON_CHANNEL] = Definitions.LAST_CHANNEL;
        prefValues[UPP_INDEX_FAVORITE_CHANNELS] = TextUtil.EMPTY_STRING;
    }

    public void init() {
        new Thread() {
            public void run() {
                getPreferenceService();
            }
        }.start();
    }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (prefSvc != null) {
            try{
                prefSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception ignore) {
            }
            prefSvc = null;
        }
        prefNames = null;
        prefValues = null;
        instance = null;
    }

    /**
     * Start PreferenceProxy.
     */
    public void start() {
    }

    /**
     * Stop PreferenceProxy.
     */
    public void stop() {
    }

    public void setPreferencProxyUpdateListener(PreferencProxyUpdateListener listener) {
        this.listener = listener;
    }
    public void removePreferencProxyUpdateListener() {
        listener = null;
    }

    /**
     * Look up User Profile and Preference.
     */
    private synchronized PreferenceService getPreferenceService() {
        if (prefSvc == null) {
            try {
                String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                prefSvc = (PreferenceService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getPreferenceService]not bound - " + PreferenceService.IXC_NAME);
                }
            }
            if (prefSvc != null) {
                String[] uppData = null;
                try {
                    uppData = prefSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues) ;
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
//                try {
//                    receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    receiveUpdatedPreference(RightFilter.BLOCK_BY_RATINGS, uppData[UPP_INDEX_BLOCK_BY_RATING]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    receiveUpdatedPreference(RightFilter.PARENTAL_CONTROL, uppData[UPP_INDEX_PARENTAL_CONTROL]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    receiveUpdatedPreference(UPP_KEY_COMPLETE_MIGRATION, uppData[UPP_INDEX_COMPLETE_MIGRATION]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    receiveUpdatedPreference(UPP_KEY_FLAG_NEED_MIGRATION, uppData[UPP_INDEX_FLAG_NEED_MIGRATION]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    receiveUpdatedPreference(PreferenceNames.DISPLAY_MENU_AT_POWER_ON, uppData[UPP_INDEX_DISPLAY_MENU]);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                // R7.2
                if (uppData != null) {
                    for (int i = 0; i < uppData.length; i++) {
                        try {
                            receiveUpdatedPreference(prefNames[i], uppData[i]);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return prefSvc;
    }

    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            prefValues[UPP_INDEX_LANGUAGE] = value;
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE , value);
        } else if (name.equals(RightFilter.BLOCK_BY_RATINGS)) {
            prefValues[UPP_INDEX_BLOCK_BY_RATING] = value;
        } else if (name.equals(RightFilter.PARENTAL_CONTROL)) {
            prefValues[UPP_INDEX_PARENTAL_CONTROL] = value;
        } else if (name.equals(UPP_KEY_COMPLETE_MIGRATION)) {
            prefValues[UPP_INDEX_COMPLETE_MIGRATION] = value;
        } else if (name.equals(UPP_KEY_FLAG_NEED_MIGRATION)) {
            prefValues[UPP_INDEX_FLAG_NEED_MIGRATION] = value;
        } else if (name.equals(PreferenceNames.DISPLAY_MENU_AT_POWER_ON)) {
        	prefValues[UPP_INDEX_DISPLAY_MENU] = value;
        } else if (name.equals(PreferenceNames.SD_HD_CHANNELS_GROUPING)) { // R7.2
            prefValues[UPP_INDEX_CHANNELS_GROUPING] = value;
        } else if (name.equals(PreferenceNames.POWER_ON_CHANNEL)) { // R7.2
            prefValues[UPP_INDEX_POWER_ON_CHANNEL] = value;
        } else if (name.equals(PreferenceNames.FAVOURITE_CHANNEL)) { // R7.2
            prefValues[UPP_INDEX_FAVORITE_CHANNELS] = value;
        }
        if (listener != null) {
            listener.receiveUpdatedPreference(name, value);
        }
    }
    /*****************************************************************
     * Language - related
     *****************************************************************/
    public boolean setCurrentLanguage(String reqLang) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setCurrentLanguage]Parameter reqLang : " + reqLang);
        }
        if (reqLang == null) {
            return false;
        }
        if (reqLang.equals(prefValues[UPP_INDEX_LANGUAGE])) {
            return true;
        }
        PreferenceService pSvc = getPreferenceService();
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setCurrentLanguage]PreferenceService : " + pSvc);
        }
        if (pSvc == null) {
            return false;
        }
        try{
            pSvc.setPreferenceValue(PreferenceNames.LANGUAGE, reqLang);
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE , reqLang);
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public String getCurrentLanguage() {
        getPreferenceService();
        return prefValues[UPP_INDEX_LANGUAGE];
    }
    /*****************************************************************
     * PIN - related
     *****************************************************************/
    public boolean existAdminPIN() {
        PreferenceService pSvc = getPreferenceService();
        boolean existAdminPIN = false;
        if (pSvc != null) {
            try{
                existAdminPIN = pSvc.isExistedAdminPIN();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.existAdminPIN]Result - existAdminPIN : "+existAdminPIN);
        }
        return existAdminPIN;
    }
    public void setAdminPIN(String rPIN) throws RemoteException {
        PreferenceService pSvc = getPreferenceService();
        if (pSvc == null) {
            throw new RemoteException();
        }
        pSvc.setAdminPIN(rPIN);
    }
    public boolean existOrderPIN() {
        PreferenceService pSvc = getPreferenceService();
        boolean existOrderPIN = false;
        if (pSvc != null) {
            try{
                existOrderPIN = pSvc.isExistedFamilyPIN();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.existOrderPIN]Result - existOrderPIN : "+existOrderPIN);
        }
        return existOrderPIN;
    }
    public void setOrderPIN(String rPIN) throws RemoteException {
        PreferenceService pSvc = getPreferenceService();
        if (pSvc == null) {
            throw new RemoteException();
        }
        pSvc.setFamilyPIN(rPIN);
    }
    public boolean equalsWithAdminPIN(String rPIN) {
        boolean equalsWithAdminPIN = true;
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                equalsWithAdminPIN = pSvc.equalsWithAdminPIN(rPIN);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return equalsWithAdminPIN;
    }
    public boolean equalsWithOrderPIN(String rPIN) {
        boolean equalsWithOrderPIN = true;
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                equalsWithOrderPIN = pSvc.equalsWithFamilyPIN(rPIN);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return equalsWithOrderPIN;
    }
    /*****************************************************************
     * Rating - related
     *****************************************************************/
    public boolean existRatingValue() {
        getPreferenceService();
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.existRatingValue]prefValues[UPP_INDEX_RATING] : "+prefValues[UPP_INDEX_BLOCK_BY_RATING]);
        }
        if (prefValues[UPP_INDEX_BLOCK_BY_RATING] == null) {
            return false;
        }
        if (prefValues[UPP_INDEX_BLOCK_BY_RATING].length() == 0) {
            return false;
        }
        boolean result = false;
        if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_8)) {
            result =  true;
        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_13)) {
            result = true;
        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_16)) {
            result = true;
        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_18)) {
            result = true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.existRatingValue]result : "+result);
        }
        return result;
    }
    public boolean setRatingValue(String rRating) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setRatingValue]Request block by rating : "+rRating);
        }
        if (rRating == null) {
            return false;
        }
        boolean resultBlockByRating = false;
        boolean resultParentalControl = false;
        boolean resultBlockByRatingDisplay = false;
        boolean resultHideAdultControl = false;
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                resultParentalControl = pSvc.setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
            }catch(Exception e) {
                e.printStackTrace();
            }
            try{
                resultBlockByRatingDisplay = pSvc.setPreferenceValue(RightFilter.BLOCK_BY_RATINGS_DISPLAY, Definitions.OPTION_VALUE_ENABLE);
            }catch(Exception e) {
                e.printStackTrace();
            }
            try{
                resultBlockByRating = pSvc.setPreferenceValue(RightFilter.BLOCK_BY_RATINGS, rRating);
            }catch(Exception e) {
                e.printStackTrace();
            }
            try{
                resultHideAdultControl = pSvc.setPreferenceValue(RightFilter.HIDE_ADULT_CONTENT, Definitions.OPTION_VALUE_YES);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setRatingValue]Result block by rating : "+resultBlockByRating);
            Log.printDebug("[PrefProxy.setRatingValue]Result parental control : "+resultParentalControl);
            Log.printDebug("[PrefProxy.setRatingValue]Result parental control display : "+resultBlockByRatingDisplay);
            Log.printDebug("[PrefProxy.setRatingValue]Result hide adult control : "+resultHideAdultControl);
        }
        if (resultBlockByRating && resultParentalControl && resultBlockByRatingDisplay && resultHideAdultControl) {
            return true;
        }
        return false;
    }
    public boolean resetRatingValue() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.resetRatingValue]Start.");
        }
        boolean resultBlockByRating = false;
        boolean resultParentalControl = false;
        boolean resultBlockByRatingDisplay = false;
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                resultParentalControl = pSvc.setPreferenceValue(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_OFF);
            }catch(Exception e) {
                e.printStackTrace();
            }
            try{
                resultBlockByRatingDisplay = pSvc.setPreferenceValue(RightFilter.BLOCK_BY_RATINGS_DISPLAY, Definitions.OPTION_VALUE_DISABLE);
            }catch(Exception e) {
                e.printStackTrace();
            }
            try{
                resultBlockByRating = pSvc.setPreferenceValue(RightFilter.BLOCK_BY_RATINGS, Definitions.RATING_G);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.resetRatingValue]Result block by rating : "+resultBlockByRating);
            Log.printDebug("[PrefProxy.resetRatingValue]Result parental control : "+resultParentalControl);
            Log.printDebug("[PrefProxy.resetRatingValue]Result parental control display : "+resultBlockByRatingDisplay);
        }
        if (resultBlockByRating && resultParentalControl && resultBlockByRatingDisplay) {
            return true;
        }
        return false;
    }
    /*****************************************************************
     * Monitor - related
     *****************************************************************/
    public void setCompleteWizard() {
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                pSvc.setPreferenceValue(UPP_KEY_COMPLETE_WIZARD, UPP_VALUE_COMPLETE_WIZARD);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public boolean isCompleteMigration() {
        getPreferenceService();
        if (prefValues[UPP_INDEX_COMPLETE_MIGRATION]!=null && prefValues[UPP_INDEX_COMPLETE_MIGRATION].equals(UPP_VALUE_INCOMPLETE_MIGRATION)){
            return false;
        }
        return true;
    }
    
    public void setFlagNeedMigration(String flag) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setFlagNeedMigration] Start - Flag : "+flag);
        }
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                pSvc.setPreferenceValue(UPP_KEY_FLAG_NEED_MIGRATION, flag);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public boolean isNeedMigration() {
        getPreferenceService();
        boolean result = false;
        if (prefValues[UPP_INDEX_FLAG_NEED_MIGRATION]!=null && prefValues[UPP_INDEX_FLAG_NEED_MIGRATION].equals(Definitions.OPTION_VALUE_YES)){
        	result = true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isNeedMigration] Result : "+result);
        }
        return result;
    }
    
    /**
     * Sets the complete migration.
     *
     * @param isCompleted the new complete migration
     */
    public void setCompleteMigration(boolean isCompleted) {
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try {
                String value = UPP_VALUE_INCOMPLETE_MIGRATION;
                if (isCompleted) { value = UPP_VALUE_COMPLETE_MIGRATION; }
                pSvc.setPreferenceValue(UPP_KEY_COMPLETE_MIGRATION, value);
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    // R5-TANK
    public boolean isDisplayMenu() {
    	if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isDisplayMenu]Display Menu : "+prefValues[UPP_INDEX_DISPLAY_MENU]);
        }
    	if (prefValues[UPP_INDEX_DISPLAY_MENU].equals(Definitions.OPTION_VALUE_YES)) {
    		return true;
    	}
    	return false;
    }

    // R7.2
    public boolean isChannelGrouped() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isChannelGrouped] value : " + prefValues[UPP_INDEX_CHANNELS_GROUPING]);
        }

        if (prefValues[UPP_INDEX_CHANNELS_GROUPING].equals(Definitions.SD_HD_CHANNEL_GROUPED)) {
            return true;
        }

        return false;
    }

    public String getPowerOnChannel() {
        return prefValues[UPP_INDEX_POWER_ON_CHANNEL];
    }

    public void setPowerOnChannel(String callLetter) {

        prefValues[UPP_INDEX_POWER_ON_CHANNEL] = callLetter;

        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                pSvc.setPreferenceValue(PreferenceNames.POWER_ON_CHANNEL, callLetter);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getFavoriteChannels() {
        return prefValues[UPP_INDEX_FAVORITE_CHANNELS];
    }
}

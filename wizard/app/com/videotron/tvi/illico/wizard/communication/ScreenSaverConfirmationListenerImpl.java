package com.videotron.tvi.illico.wizard.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;

public class ScreenSaverConfirmationListenerImpl implements ScreenSaverConfirmationListener{
    public boolean confirmScreenSaver() throws RemoteException {
        return true;
    }
}
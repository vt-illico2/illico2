package com.videotron.tvi.illico.wizard.communication;

import java.awt.Point;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import org.dvb.io.ixc.IxcRegistry;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.wizard.App;
import com.videotron.tvi.illico.wizard.Rs;
import com.videotron.tvi.illico.wizard.controller.DataManager;

public class CommunicationManager {
    private static CommunicationManager instance;
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS_MONITOR = 100;

    public static final int EXIT_TYPE_COMPLETION = 0;
    public static final int EXIT_TYPE_EXIT_KEY = 1;


    private InbandDataListenerImpl iImpl;
    private static final int MAX_LOOP_COUNT = 10;

    private MonitorService mSvc;
    private ScreenSaverConfirmationListenerImpl ssclImpl;
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private StcService stcSvc;
    private LogLevelChangeListenerImpl llclImpl;
    /*********************************************************************************
     * Loading Animation Service-related
     *********************************************************************************/
    private LoadingAnimationService lSvc;
    private boolean isLoadingAnimationService;
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    private ErrorMessageService emSvc;

    // R7.2
    private EpgService epgService;
    
    private CommunicationManager() {
    }
    public synchronized static CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }
    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommunicationMgr.init]start.");
        }
        if (iImpl == null) {
            iImpl = new InbandDataListenerImpl();
        }
        new Thread() {
            public void run() {
                try {
                    int count = 0;
                    MonitorService svc = null;
                    while (true) {
                        if (count == MAX_LOOP_COUNT) {
                            break;
                        }
                        count++;
                        svc = getMonitorService();
                        if (svc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommunicationMgr.init]MonitorService looked up.");
                            }
                            break;
                        }
                    }
                    requestAddInbandDataListener();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();

        llclImpl = new LogLevelChangeListenerImpl();
        lookupStcService();
    }
    public void dispose() {
        requestRemoveInbandDataListener();
        mSvc = null;
        iImpl = null;
        if (stcSvc != null) {
            try{
                stcSvc.removeLogLevelChangeListener(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
            stcSvc = null;
        }
        llclImpl = null;
        lSvc=null;
        emSvc=null;
    }
    public void start() {
        if (mSvc != null) {
            if (ssclImpl == null) {
                ssclImpl = new ScreenSaverConfirmationListenerImpl();
            }
            try {
                mSvc.addScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void stop() {
        if (mSvc != null && ssclImpl != null) {
            try {
                mSvc.removeScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ssclImpl = null;
        }
    }
    /*********************************************************************************
     * MonitorService-related
     *********************************************************************************/
    public MonitorService getMonitorService() {
        if (mSvc == null) {
            try {
                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                mSvc = (MonitorService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommunicationMgr.getMonitorService]not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
        return mSvc;
    }
    public void requestAddInbandDataListener() {
        MonitorService svc = getMonitorService();
        if (svc != null) {
            if (!Rs.IS_EMULATOR) {
                try {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[CommunicationMgr.requestAddInbandDataListener]start.");
                    }
                    svc.addInbandDataListener(iImpl, Rs.APP_NAME);
                } catch (Exception ignore) {
                }
            }
        }
    }
    public void requestRemoveInbandDataListener() {
        MonitorService svc = getMonitorService();
        if (svc != null) {
            if (!Rs.IS_EMULATOR) {
                try {
                    svc.removeInbandDataListener(iImpl, Rs.APP_NAME);
                } catch (Exception ignore) {
                }
            }
        }
    }
    public void requestExitToChannel(int exitType) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommunicationMgr.requestExitToChannel]start.");
        }
        MonitorService svc = getMonitorService();
        if (svc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommunicationMgr.requestExitToChannel]Monitor Service is null.");
            }
            return;
        }
        if (DataManager.getInstance().isCalledByMonitor()) {
            if (exitType == EXIT_TYPE_COMPLETION) {
            	// R5-TANK
            	// VDTRMASTER-5529
            	try {
	            	if (PreferenceProxy.getInstance().isDisplayMenu()) {
	            		svc.startUnboundApplication("Menu", new String[] {MonitorService.REQUEST_APPLICATION_MENU});
	            	} else {
		                svc.exitToChannel(-1);
	            	}
            	} catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // If this application called by monitor and exit type is EXIT_TYPE_EXIT_KEY, then action nothing.
        } else {
            try {
                svc.exitToChannel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public String requestCalledByValue() {
        MonitorService svc = getMonitorService();
        if (svc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommunicationMgr.requestCalledByValue]Monitor Service is null.");
            }
            return null;
        }
        String calledByValue = null;
        try{
            String[] params = svc.getParameter(Rs.APP_NAME);
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommunicationMgr.requestCalledByValue]getParameter("+Rs.APP_NAME+") : "+params);
            }
            if (params != null && params.length > 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[CommunicationMgr.requestCalledByValue]Parameter-0 exist param[0] : ["+params[0]+"]");
                }
                calledByValue = params[0];
            }
        }catch(Exception e) {
            e.printStackTrace();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommunicationMgr.requestCalledByValue]end - Called by value : "+calledByValue);
        }
        return calledByValue;
    }
    public String[] getParameters() {
        String[] params = null;
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try{
                params = svc.getParameter(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return params;
    }
    public boolean requestStartUnboundApplication(String reqAppName, String[] reqParams) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.requestStartUnboundApplication]Param - App name : "
                    + reqAppName);
            if (reqParams != null) {
                for (int i = 0; i < reqParams.length; i++) {
                    Log.printDebug("[InbandDataListenerImpl.requestStartUnboundApplication]Param - Param["
                            + i + "] : " + reqParams[i]);
                }
            }
        }
        MonitorService svc = getMonitorService();
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.requestStartUnboundApplication]MonitorService : " + svc);
        }
        if (svc != null) {
            try {
                return svc.startUnboundApplication(reqAppName, reqParams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }
    public void requestCompleteReceivingData() {
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try {
                mSvc.completeReceivingData(Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public boolean reqeustExistMandatoryPackageName() throws Exception {
        MonitorService svc = getMonitorService();
        String[] manPackNames=svc.getMandatoryPackageNames();
        if (manPackNames==null || manPackNames.length==0) {
            return false;
        }
        return true;
    }
    public String[] requestGetActivationPackageNames() {
        if (Log.INFO_ON) {
            Log.printInfo("[InbandDataListenerImpl.requestGetActivationPackageNames] Called.");
        }
        MonitorService svc = getMonitorService();
        String[] actPackNames=null;
        if (svc != null) {
            try {
                actPackNames=svc.getActivationPackageNames();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.requestGetActivationPackageNames] Activation Package Names : " + actPackNames);
        }
        if (actPackNames!=null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[InbandDataListenerImpl.requestGetActivationPackageNames] Activation Package Names Size : " + actPackNames.length + "");
            }
        }
        return actPackNames;
    }
    public int requestCheckResourceAuthorization(String pkgId) {
        if (Log.INFO_ON) {
            Log.printInfo("[InbandDataListenerImpl.requestCheckResourceAuthorization] Called.");
        }
        MonitorService svc = getMonitorService();
        int result=MonitorService.NOT_AUTHORIZED;
        if (svc != null) {
            try {
                result=svc.checkResourceAuthorization(pkgId);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.requestCheckResourceAuthorization] CheckResourceAuthorization Result : " + result);
        }
        return result;
    }
    
    public byte[] reqeustGetCableCardMacAddress() {
        MonitorService svc = getMonitorService();
        byte[] ccMacAddress=null;
        if (svc != null) {
            try {
                ccMacAddress=mSvc.getCableCardMacAddress();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ccMacAddress;
    }
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private void lookupStcService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupStcService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (stcSvc == null) {
                            try {
                                String lookupName = "/1/2100/" + StcService.IXC_NAME;
                                stcSvc = (StcService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupStcService]not bound - " + StcService.IXC_NAME);
                                }
                            }
                        }
                        if (stcSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupStcService]looked up.");
                            }
                            try {
                                int currentLevel = stcSvc.registerApp(Rs.APP_NAME);
                                Log.printDebug("stc log level = " + currentLevel);
                                Log.setStcLevel(currentLevel);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                            if (o != null) {
                                DataCenter.getInstance().put("LogCache", o);
                            }
                            try {
                                stcSvc.addLogLevelChangeListener(Rs.APP_NAME, llclImpl);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupStcService]retry lookup.");
                        }
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*********************************************************************************
     * LoadingAnimationService-related
     *********************************************************************************/
    public LoadingAnimationService getLoadingAnimationService() {
        if (lSvc == null) {
            final String LOOK_UP_NAME = "/1/2075/" + LoadingAnimationService.IXC_NAME;
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommMgr.getLoadingAnimationService]Loading animation service look up name : " + LOOK_UP_NAME);
            }
            try {
                lSvc = (LoadingAnimationService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), LOOK_UP_NAME);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getLoadingAnimationService]not bound - " + LoadingAnimationService.IXC_NAME);
                }
            }
        }
        return lSvc;
    }
    public boolean requestShowLoadingAnimation(Point point) {
        if (isLoadingAnimationService) {
            return true;
        }
        boolean result = false;
        LoadingAnimationService svc = getLoadingAnimationService();
        if (svc != null) {
            try{
                svc.showLoadingAnimation(point);
                isLoadingAnimationService = true;
                result = true;
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean requestShowNotDelayLoadingAnimation(Point point) {
        boolean result = false;
        LoadingAnimationService svc = getLoadingAnimationService();
        if (svc != null) {
            try{
                svc.showNotDelayLoadingAnimation(point);
                isLoadingAnimationService = true;
                result = true;
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean requestHideLoadingAnimation() {
        LoadingAnimationService svc = getLoadingAnimationService();
        boolean result = false;
        if (svc != null) {
            try{
                svc.hideLoadingAnimation();
                isLoadingAnimationService = false;
                result = true;
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean isLoadingAnimationService() {
        return isLoadingAnimationService;
    }
    
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(String errorCode) {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.showErrorMessage(errorCode, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // R7.2
    public EpgService getEpgService() {
        if (epgService == null) {
            try {
                String lookupName = "/1/2026/" + EpgService.IXC_NAME;
                epgService = (EpgService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getEpgService]not bound - " + EpgService.IXC_NAME);
                }
            }
        }

        return epgService;
    }
}

package com.videotron.tvi.illico.wizard.communication;

public interface PreferencProxyUpdateListener {
    void receiveUpdatedPreference(String name, String value);
}

Quick_Start_Wizard	Assistant de réglage rapide	Quick Start Wizard

Language	Langue	Language
Admin_PIN	NIP admin	Admin PIN
Parental_Control	Contrôle parental	Parental control
Order_Pin	NIP de commande	Order PIN
Power_On_Channel	Chaîne à l'allumage	Power-on Channel
Previous	Précédent	Back
Skip	Sauter l’étape	Skip
Next	Suivant	Next
PIN_Code	NIP	PIN
Confirm_PIN_Code	Confirmer le NIP	Confirm your PIN
Erase	Effacer	Erase
Clear	Supprimer	Clear
Cancel	Annuler	Cancel
OK	OK	OK
Close	Fermer	Close
Admin_PIN_Title_No	NIP administrateur (confidentiel)	Administrator PIN (confidential)
Admin_PIN_Desc_No_0	Le NIP administrateur vous donne accès à tout l’univers d’illico télé. Il vous permet de :	The Administrator PIN gives you unlimited access to the world of illico TV. It allows you to:
Admin_PIN_Desc_No_1	- Commander du contenu illico	- Order illico content
Admin_PIN_Desc_No_2	- Modifier vos services	- Modify your services
Admin_PIN_Desc_No_3	- Accéder aux contrôles et limites.	- Access to Controls and Limits.
Admin_PIN_Button_No	Créez votre NIP administrateur	Create your Administrator PIN

Admin_PIN_Title_Yes	NIP administrateur créé	Administrator PIN created
Admin_PIN_Desc_Yes_0	Vous pouvez maintenant commander du contenu illico, modifier vos services et accéder aux contrôles et limites.	You can now order illico content, modify your services and access Controls and Limits.
Admin_PIN_Desc_Yes_1	Nous vous recommandons de ne pas partager votre NIP administrateur.	We recommend that you do not share your Administrator PIN with others.
Admin_PIN_Button_Yes	Modifiez votre NIP administrateur	Modify your Administrator PIN

Parental_Control_Desc_No_0	Le contrôle parental vous permet de restreindre l’accès à certains contenus	Parental Control allow you to restrict access to inappropriate content
Parental_Control_Desc_No_1	afin d’offrir à vos enfants une expérience télé qui leur convient.	in order to offer your kids the best and most suitable TV experience.
Parental_Control_Desc_No_2	Votre NIP administrateur sera nécessaire pour accéder au contenu bloqué.	You will be required to enter your Administrator PIN to access blocked content.
Parental_Control_Button_No	Configurer le Contrôle parental	Set-up Parental Control

Parental_Control_Title_Yes	Contrôle parental activé	Parental Control is enabled
Parental_Control_Desc_Yes_0	Vous devrez  maintenant fournir votre NIP administrateur pour accéder à du contenu restreint par le contrôle parental.	You must now enter your Administrator PIN to access content blocked by your Parental Control settings.
Parental_Control_Button_Yes	Désactiver le contrôle parental	Disable Parental Control

Order_PIN_Title_No	NIP de commande (à partager)	Order PIN (to share)
Order_PIN_Desc_No_0	Le NIP de commande est un 2e NIP que vous pouvez partager avec les membres de votre famille.|En partageant ce NIP, vous:	The Order PIN is a 2nd PIN that you can share with family members. This PIN allows:
Order_PIN_Desc_No_1	- Permettez aux membres de votre famille de commander du contenu	- Your family members to order content 
Order_PIN_Desc_No_2	- Conservez la confidentialité de votre NIP administrateur.	- Your Administrator PIN to stay confidential.
Order_PIN_Button_No	Créer votre NIP de commande	Create your Order PIN

Order_PIN_Title_Yes	NIP de commande créé	Order PIN has been created
Order_PIN_Desc_Yes_0	Vous pouvez maintenant partager le NIP de commande avec les membres de votre famille.	You can now share the Order PIN with your family members.
Order_PIN_Desc_Yes_1	Ceux qui ont accès à ce NIP pourront commander du contenu non restreint par le contrôle parental.	Those who have access to this PIN will be able to order content that is not blocked by your Parental Control settings.
Order_PIN_Button_Yes	Modifier votre NIP de commande	Modify your Order PIN



Create_Administrator_PIN_Code	Créer votre NIP administrateur	Create your Administrator PIN
Modify_Administrator_PIN_Code	Modifier le NIP administrateur	Modify Administrator PIN
Admin_PIN_Created	Votre NIP administrateur a été créé	Your Administrator PIN has been created

Skip_Administrator_PIN_Popup_Title	NIP admin non créé	Admin PIN not created
Skip_Administrator_PIN_Popup_Content	Le NIP administrateur est requis pour effectuer des actions spécifiques (commander du contenu, modifier les paramètres, etc.).||Comme vous n’avez pas de NIP admin, il vous sera demandé d’en créer un si nécessaire.	You need an Administrator PIN to perform specific actions (e.g. to order content, modify settings, etc.). ||Though you do not currently have an Admin PIN, you may eventually be asked to create one.

Create_Admin_PIN_Code	Créer votre NIP admin	Create your Admin PIN
Create_Order_PIN_Code	Créer votre NIP de commande	Create your Order PIN
Create_Admin_PIN_Code_Desc	Veuillez créer votre NIP administrateur|à 4 chiffres.	Please create your 4-digit Administrator PIN.
Create_Order_PIN_Code_Desc	Veuillez entrer un NIP à 4 chiffres.|Ce code doit être différent du NIP administrateur.	Please enter a 4-digit PIN.|Order PIN must be different than your Admin PIN.
Create_Admin_PIN_Code_Same	Le NIP admin doit être différent du NIP de commande. Veuillez entrer un nouveau NIP.	Admin PIN must differ from the Order PIN. Please enter a new PIN. 
Create_Order_PIN_Code_Same	Le NIP de commande doit être différent du NIP admin. Veuillez entrer un nouveau NIP.	Order PIN must differ from the Admin PIN. Please enter a new PIN.

Pop_Create_PIN_Desc_Invalid_0	Le NIP et le NIP confirmé sont différents.	Confirmation does not match your PIN.
Pop_Create_PIN_Desc_Invalid_1	Veuillez entrer votre NIP à nouveau.	Please reconfirm your PIN.




Enabled_Parental_Control	Le contrôle parental est maintenant activé	Parental Control is now enabled

Disable_Parental_Control	Désactiver le Contrôle parental	Disable Parental Control

Skip_Parental_Control_Popup_Title	Contrôle parental non activé	Parental Control not activated
Skip_Parental_Control_Popup_Content	Sans contrôle parental, certains contenus pourraient ne pas convenir à un jeune public.||Vous pourrez activer le contrôle parental en tout temps en accédant à la section Paramètres.	Without Parental Control, some content may not be suitable for a younger audience.||You can add Parental Control at any time via the Settings section.

Question_Create_Admin_Pin_Title	NIP Administrateur requis	 Admin PIN required
Question_Create_Admin_Pin_Content	Le NIP administrateur est requis pour configurer le contrôle parental.||Veuillez créer votre NIP administrateur maintenant.	You are required to enter an Administrator PIN  to set up Parental Control.||Please create your Administrator PIN now.

Parental_Control_Age_Restrictions	Contrôle parental : restrictions de contenu	Parental Control : Content Restrictions
Block_Content_Rated_As	Bloquer le contenu classé :	Block content rated as:
Content_Rated_As	Tout contenu classé comme	All content rated as
And_Older	et plus	and older
Tail	nécessitera votre NIP administrateur.	will require your Administrator PIN.

Parental_Control_Order_PIN_Code	NIP de commande	Order PIN
Parental_Control_Order_PIN_Content_0	Le NIP familial est un deuxième NIP que vous pouvez donner aux membres de votre famille afin qu'ils puissent UNIQUEMENT commander du contenu.	The Order PIN is a second PIN that you can share with your family members, and which they can use to order content ONLY.
Parental_Control_Order_PIN_Content_1	Vous pourrez donc conserver votre NIP admin pour tout autre action.	You can keep your Admin PIN to perform all other actions.

Created_Order_PIN	Le NIP de commande a été créé	Your Order PIN has been created
Create_Order_PIN	Créer votre NIP de commande (facultatif)	Create your Order PIN (optional)
Modify_Order_PIN	Modifier le NIP de commande	Change your Order PIN


# Complete Scene
Completion_Comp_Common_Status	La configuration est terminée.	Set-up complete.
Completion_Comp_Common_Desc	Vous avez configuré votre|tterminal illico avec succès.	You have successfully completed|the set-up of your terminal.
Completion_Comp_Common_Button_Banner_0	illico télé vous en offre toujours plus.	illico TV always gives you more.
Completion_Comp_Common_Button_Banner_1	Visionnez une vidéo de démonstration!	Watch a video demonstration!
Completion_Comp_Common_Button_Exit	Quitter et regarder la télé	Exit and watch TV
#Completion_Comp_Common_RCU	N’oubliez pas de programmer la télécommande de votre terminal à votre téléviseur afin de profiter des différentes fonctionnalités comme éteindre ou augmenter le volume. Pour se faire, vous devez suivre les instructions imprimées au dos de votre télécommande.	Don’t forget to program your remote control to your TV in order to experience different functionalities such as turning off or increasing the volume. To do such, please follow the instructions printed on the back of your remote control.
#Completion_Comp_Common_RCU_Instruction	Ces instructions se retrouvent aussi dans votre guide de l’utilisateur fourni lors de l’achat de votre terminal ou sur notre site web au	These instructions can also be found in your user guide provided with your terminal or on our website at
#Completion_Comp_Common_RCU_Site	http://soutien.videotron.com/residentiel/television/telecommandes/illico-nouvelle-generation	http://support.videotron.com/residential/television/remotes-control/illico-tv-new-generation

#Completion_Comp_Common_RCU	Assurez-vous de programmer la télécommande à votre téléviseur pour mieux contrôler votre expérience télé.||Pour se faire, vous devez suivre les instructions imprimées au dos de votre télécommande.	To take full advantage of your viewing experience, make sure to program the remote to your TV.||To do such, please follow the instructions printed on the back of your remote control.
Completion_Comp_Common_RCU	Programmer votre télécommande illico pour contrôler le volume, le choix de sources et l’allumage de votre téléviseur !	Program your illico remote in order to control the volume, the input and turning on your TV.
#Completion_Comp_Common_RCU_Instruction	Ces instructions se retrouvent aussi dans votre guide de l’utilisateur fourni lors de l’achat de votre terminal  ou sur notre site web au	These instructions can also be found in your user guide provided with your terminal or on our website at
Completion_Comp_Common_RCU_Instruction	Les instructions sont disponibles :||- au dos de votre télécommande;||- dans le guide de l’utilisateur;||- ainsi qu’au :	Instructions are available :||- On the back of your remote||- In your user guide||- And online :
#Completion_Comp_Common_RCU_Site	http://soutien.videotron.com/residentiel/television/telecommandes/illico-nouvelle-generation	http://support.videotron.com/residential/television/remotes-control/illico-tv-new-generation
Completion_Comp_Common_RCU_Site	www.videotron.com/telecommande	www.videotron.com/remotecontrol

Completion_Comp_Non_PVR_Status	La configuration est terminée.	Set-up complete.
Completion_Comp_Non_PVR_Desc	Par contre, certains de vos paramètres n’ont pu être sauvegardés.	However, some  of your settings were not saved.
Completion_Comp_Non_PVR_SubDesc	Vous pouvez accéder à la section Paramètres du menu afin de vérifier vos options.	You can access the “Settings” section  from the menu to review your options.	
Completion_Comp_Non_PVR_Button_Banner_0	illico télé vous en offre toujours plus.	illico TV always gives you more.
Completion_Comp_Non_PVR_Button_Banner_1	Visionnez une vidéo de démonstration!	Watch a video demonstration!
Completion_Comp_Non_PVR_Button_Exit	Quitter et regarder la télé	Exit and watch TV

Completion_Comp_PVR_Status	La configuration est terminée.	Set-up complete.
Completion_Comp_PVR_Desc	Par contre, certains de vos enregistrements et paramètres n’ont pu être sauvegardés.	However some of your recordings and settings were not saved.
Completion_Comp_PVR_SubDesc	Veuillez vérifier la programmation de vos enregistrements à venir  en accédant au Menu de l’Enregistreur HD.	Access your HD PVR’s menu to review your recordings.
Completion_Comp_PVR_Button_Banner_0	illico télé vous en offre toujours plus.	illico TV always gives you more.
Completion_Comp_PVR_Button_Banner_1	Visionnez une vidéo de démonstration!	Watch a video demonstration!
Completion_Comp_PVR_Button_Exit	Quitter et regarder la télé	Exit and watch TV

Completion_Incomp_Migration_Status	La configuration est incomplète.	Set-up incomplete.
Completion_Incomp_Migration_Desc_F	Veuillez communiquer avec le Soutien Technique  au 	Please call Technical Support at 
Completion_Incomp_Migration_Desc_B	pour compléter la configuration de votre terminal.	to  complete the set-up of your terminal.
Completion_Incomp_Migration_Call_Center	1 877 380-2611	1 877 380-2611
Completion_Incomp_Migration_SubDesc	Remarque : vous pouvez regarder la télé, mais certaines fonctionnalités pourraient ne pas fonctionner correctement.	Please note: You can still watch TV, but certain features may not work properly.
Completion_Incomp_Migration_Button_Exit	Quitter et regarder la télé	Exit and watch TV

Completion_Incomp_Signal_Status	La configuration est incomplète.	Set-up incomplete.
Completion_Incomp_Signal_Desc_F	Veuillez communiquer avec le Soutien Technique  au 	Please call Technical Support at 
Completion_Incomp_Signal_Desc_B	pour compléter la configuration de votre terminal.	to  complete the set-up of your terminal.
Completion_Incomp_Signal_Call_Center	1 877 380-2611	1 877 380-2611
Completion_Incomp_Signal_SubDesc	Remarque : vous pouvez regarder la télé, mais certaines fonctionnalités pourraient ne pas fonctionner correctement.	Please note: You can still watch TV, but certain features may not work properly.
Completion_Incomp_Signal_Button_Exit	Quitter et regarder la télé	Exit and watch TV

Completion_Incomp_Activation_Status	L’activation de votre terminal n’a pas fonctionné.	Your terminal could not be activated.
Completion_Incomp_Activation_Desc_F	Veuillez communiquer avec le Service à la clientèle au 	Please call Customer Service at 
Completion_Incomp_Activation_Desc_B	pour utiliser ce terminal.	in order to be able to use this terminal. 	
Completion_Incomp_Activation_Call_Center	1-888-433-6876	1-888-433-6876
Completion_Incomp_Activation_Button_Exit	Quitter	Exit

Power_On_Channel_Title	Configuration de la chaîne à l'allumage	Power-on Channel configuration
Power_On_Channel_Explain	Sélectionnez la chaîne qui sera syntonisée|lorsque vous allumerez votre terminal illico.	You can select the channel you want to appear|when you turn on your illico terminal.
Finish	Terminer	Finish
Last Channel	Dernière chaîne vue	Last Channel

Completion_Comp_Common_Status_New	La configuration est terminée	Set-up complete
Completion_Comp_Common_Explain	Vous avez configuré votre|Terminal illico avec succès.	You have successfully completed|the set-up of your illico Terminal.
Completion_Comp_Common_Select_Explain	Que voulez-vous faire?	What do you want to do?
Completion_Comp_Common_Select1	Regarder la télé dès maintenant	Watch TV now
Completion_Comp_Common_Select2	Programmer ma télécommande	Program your remote
Completion_Comp_Common_Select3	En apprendre plus sur illico	Learn more about illico

Pop_Rcu_title	Programmer ma télécommande illico	Program your illico remote
Pop_Rcu_explain	Programmez votre télécommande afin de contrôler|le volume, le choix de sources|et l’allumage de votre télé.| |Les instructions se trouvent :|- au dos de la télécommande;|- dans le guide de l’utilisateur;|- au www.videotron.com/telecommande	Programming your remote allows you to control|the volume and the choice of sources as well as|the powering on of your TV.| |Instructions are available :|- On the back of your remote|- In your user guide|- at www.videotron.com/remotecontrol

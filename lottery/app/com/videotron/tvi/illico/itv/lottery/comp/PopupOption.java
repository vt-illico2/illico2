package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.comp.popup.Popup;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.util.FontResource;

public class PopupOption extends Popup {
    private static final long serialVersionUID = -1430573623555773316L;
    private ImageManager imgMgr = ImageManager.getInstance();
    
    private Font F19 = FontResource.BLENDER.getFont(19, true);
    private  Color C001001001 = new Color(1, 1, 1);
    private  Color C027024012 = new Color(27, 24, 12);
    private  Color C078078078 = new Color(78, 78, 78);
    private  Color C193191191 = new Color(193, 191, 191);
    private  Color C214182055 = new Color(214, 182, 55);
    private  Color DVB000000000153 = new DVBColor(0, 0, 0, 153);
    
    private Image imgOptionBgBot;
    private Image imgOptionBgMid;
    private Image imgOptionBgTop;
//    private Image imgOptionBgHigh;
    private Image imgOptionBgMask;
//    private Image imgOptionShadowBot;
//    private Image imgOptionShadowTop;
    private Image imgOptionFocus;
    private Image imgArrowTop;
    private Image imgArrowBot;
    
    private PopupOptionItemBg popOptionItemBg;
    private PopupOptionItemText popOptionItemTxt;
    
    private OptionItem[] optionItems;
    private int optionItemCount;
    private int curIdx;
    
    private String popTitle;
    /*********************************************************************************
     * Coordinate-related
     *********************************************************************************/
    private int startX;
    private int startY;
    private static final int ITEM_WIDTH = 232;
    private static final int POPUP_HEIGHT_TOP = 44;
    
    public PopupOption() {
        if (imgOptionBgBot == null) {
            imgOptionBgBot = imgMgr.getImage("08_op_bg_b.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgOptionBgMid == null) {
            imgOptionBgMid = imgMgr.getImage("08_op_bg_m.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgOptionBgTop == null) {
            imgOptionBgTop = imgMgr.getImage("08_op_bg_t.png", ImageManager.TYPE_TEMPORARY);
        }
//        if (imgOptionBgHigh == null) {
//            imgOptionBgHigh = imgMgr.getImage("11_op_high.png", ImageManager.TYPE_TEMPORARY);
//        }
        if (imgOptionBgMask == null) {
            imgOptionBgMask = imgMgr.getImage("option_mask.png", ImageManager.TYPE_TEMPORARY);
        }
//        if (imgOptionShadowBot == null) {
//            imgOptionShadowBot = imgMgr.getImage("08_op_sh_b.png", ImageManager.TYPE_TEMPORARY);
//        }
//        if (imgOptionShadowTop == null) {
//            imgOptionShadowTop = imgMgr.getImage("08_op_sh_t.png", ImageManager.TYPE_TEMPORARY);
//        }
        if (imgOptionFocus == null) {
            imgOptionFocus = imgMgr.getImage("08_op_foc.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgArrowTop == null) {
            imgArrowTop = imgMgr.getImage("02_ars_t.png", ImageManager.TYPE_TEMPORARY);
        }
        if (imgArrowBot == null) {
            imgArrowBot = imgMgr.getImage("02_ars_b.png", ImageManager.TYPE_TEMPORARY);
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (popOptionItemBg == null) {
            popOptionItemBg = new PopupOptionItemBg();
            add(popOptionItemBg);
        }
        if (popOptionItemTxt == null) {
            popOptionItemTxt = new PopupOptionItemText();
            add(popOptionItemTxt, 0);
        }
    }
    protected void disposePopup() {
        imgOptionBgBot = null;
        imgOptionBgMid = null;
        imgOptionBgTop = null;
//        imgOptionBgHigh = null;
        imgOptionBgMask = null;
//        imgOptionShadowBot = null;
//        imgOptionShadowTop = null;
        imgOptionFocus = null;
        imgArrowTop = null;
        imgArrowBot = null;
    }
    protected void startPopup() {
        if (popOptionItemBg != null) {
            popOptionItemBg.setBounds(startX, startY, ITEM_WIDTH, 126);
        }
        if (popOptionItemTxt != null) {
            popOptionItemTxt.setBounds(startX, startY + POPUP_HEIGHT_TOP, ITEM_WIDTH, 60);
        }
    }
    protected void stopPopup() {
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case HRcEvent.VK_UP:
                curIdx = (optionItems.length + curIdx - 1) % optionItems.length;
                repaint();
                return true;
            case HRcEvent.VK_DOWN:
                curIdx = (curIdx + 1) % optionItems.length;
                repaint();
                return true;
            case HRcEvent.VK_ENTER:
                if (pListener != null) {
                    pListener.popupOK(new PopupEvent(this));
                }
                return true;
            case OCRcEvent.VK_LAST:
                if (pListener != null) {
                    pListener.popupCancel(new PopupEvent(this));
                }
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
        //Draws background.
        g.setColor(DVB000000000153);
        g.fillRect(0, 0, 960, 540);
        super.paint(g);
    }
    /*********************************************************************************
     * Option item-related
     *********************************************************************************/
    public void setFocusLocation(int x, int y) {
        startX = x - 2;
        startY = y - 58;
    }
    public void setOptionItems(OptionItem[] optionItems, int reqIdx) {
        this.optionItems = optionItems;
        if (optionItems != null) {
            optionItemCount = optionItems.length;
        }
        curIdx = reqIdx;
    }
    public void setPopupTitle(String popTitle) {
        this.popTitle = popTitle;
    }
    public OptionItem getSelectedOptionItem() {
        OptionItem oItem = null;
        if (optionItems != null && curIdx < optionItemCount) {
            oItem = optionItems[curIdx];
        }
        return oItem;
    }
    /*********************************************************************************
     * PopupOptionItem background-related
     *********************************************************************************/
    private class PopupOptionItemBg extends Component {
        private static final long serialVersionUID = 3438300937937332227L;
        public void paint(Graphics g) {
            g.drawImage(imgOptionBgBot, 0, 88, this);
            g.drawImage(imgOptionBgMid, 0, 61, this);
            g.drawImage(imgOptionBgTop, 0, 42, this);
            g.setColor(C078078078);
            g.fillRect(2, 15, 230, 29);
//            g.drawImage(imgOptionBgHigh, 5, 30, this);
            g.drawImage(imgOptionBgMask, 4, 44, 226, 60, this);
//            g.drawImage(imgOptionShadowBot, 4, 77, 226, 27, this);
//            g.drawImage(imgOptionShadowTop, 4, 44, 226, 28, this);
            g.drawImage(imgArrowTop, 106, 0, this);
            g.drawImage(imgArrowBot, 106, 104, this);
            g.drawImage(imgOptionFocus, 2, 58, this);
            if (popTitle != null) {
                g.setFont(F19);
                g.setColor(C027024012);
                g.drawString(popTitle, 16, 36);
                g.setColor(C214182055);
                g.drawString(popTitle, 15, 35);
            }
        }
    }
    /*********************************************************************************
     * PopupOptionItem text area-related
     *********************************************************************************/
    private class PopupOptionItemText extends Component{
        private static final long serialVersionUID = 6295610833217421422L;
        public void paint(Graphics g) {
            if (optionItems == null) {
                return;
            }
            g.setFont(F19);
            int optionItemsLth = optionItems.length;
            int startIdx = (optionItemsLth + curIdx - 1) % optionItemsLth;
            for (int i=0; i<3; i++) {
                int idx = (startIdx + i) % optionItemsLth;
                if (optionItems[idx] == null) {
                    continue;
                }
                String txtDisp = optionItems[idx].getDisplayText();
                if (txtDisp == null) {
                    continue;
                }
                if (idx == curIdx) {
                    g.setColor(C001001001);
                } else {
                    g.setColor(C193191191);
                }
                g.drawString(txtDisp, 19, 8 + (i*27));
            }
        }
    }
}

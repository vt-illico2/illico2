package com.videotron.tvi.illico.itv.lottery.controller;

import java.io.File;
import java.util.Properties;

import com.videotron.tvi.illico.itv.lottery.Util;

public class DataConverter {
    /** The Constant TEST_PROPERTIES_BYTE_BUFFER. */
    private static final int TEST_PROPERTIES_BYTE_BUFFER = 4000;

    public static byte[] getBytesITvServer() {
        File fileITvServer = new File("resource/test_data/prop_data/itv_server.prop");
        Properties propITvServer = Util.loadProperties(fileITvServer);
        byte[] bytesITvServer = new byte[TEST_PROPERTIES_BYTE_BUFFER];
        
        int index = 0;
        //Server type
        String tempVersion = propITvServer.getProperty("version");
        byte version = Byte.parseByte(tempVersion);
        bytesITvServer[index++] = version;
        //Server list size
        String tempServerListSize = propITvServer.getProperty("server_list_size");
        byte serverListSize = Byte.parseByte(tempServerListSize);
        bytesITvServer[index++] = serverListSize;
        for (int i=0; i<serverListSize; i++) {
            //Server type
            String tempServerType = propITvServer.getProperty("server_type_"+i);
            byte serverType = Byte.parseByte(tempServerType);
            bytesITvServer[index++] = serverType;
            //Ip DNS
            String tempIpDns = propITvServer.getProperty("ip_dns_"+i);
            byte[] ipDns = tempIpDns.getBytes();
            int ipDnsLth = ipDns.length;
            bytesITvServer[index++] = (byte) ipDnsLth;
            System.arraycopy(ipDns, 0, bytesITvServer, index, ipDnsLth);
            index += ipDnsLth; 
            //Port
            String tempPort = propITvServer.getProperty("port_"+i);
            byte[] port = Util.intTo2Byte(Integer.parseInt(tempPort));
            System.arraycopy(port, 0, bytesITvServer, index, 2);
            index += 2;
            //Context root
            String tempContextRoot = propITvServer.getProperty("context_root_"+i);
            byte[] contextRoot = tempContextRoot.getBytes();
            int contextRootLth = contextRoot.length;
            bytesITvServer[index++] = (byte) contextRootLth;
            System.arraycopy(contextRoot, 0, bytesITvServer, index, contextRootLth);
            index += contextRootLth; 
            //Proxy list
            String tempProxyListSize = propITvServer.getProperty("proxy_list_size_"+i);
            byte proxyListSize = Byte.parseByte(tempProxyListSize);
            bytesITvServer[index++] = proxyListSize;
            for (int j=0; j<proxyListSize; j++) {
                //Proxy Ip
                String tempProxyIp = propITvServer.getProperty("proxy_ip_"+i+"_"+j);
                byte[] proxyIp = tempProxyIp.getBytes();
                int proxyIpLth = proxyIp.length;
                bytesITvServer[index++] = (byte) proxyIpLth;
                System.arraycopy(proxyIp, 0, bytesITvServer, index, proxyIpLth);
                index += proxyIpLth; 
                //Proxy Port
                String tempProxyPort = propITvServer.getProperty("proxy_port_"+i+"_"+j);
                byte[] proxyPort = Util.intTo2Byte(Integer.parseInt(tempProxyPort));
                System.arraycopy(proxyPort, 0, bytesITvServer, index, 2);
                index += 2;
            }
        }
        return bytesITvServer;
    }
}

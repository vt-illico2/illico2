package com.videotron.tvi.illico.itv.lottery.controller;

public class SceneTemplate {
    public static final int SCENE_ID_INVALID = -1;
    public static final int SCENE_ID_LOTTERY_MAIN = 0;
    public static final int SCENE_ID_LOTTERY_SUMMARY_RESULTS = 1;
    public static final int SCENE_ID_LOTTERY_DETAILED_RESULTS = 2;
    public static final int SCENE_ID_LOTTERY_PREFERENCE = 3;
    public static final int SCENE_ID_LOTTERY_PREFERENCE_FAVORITE_LOTTERY = 4;
}

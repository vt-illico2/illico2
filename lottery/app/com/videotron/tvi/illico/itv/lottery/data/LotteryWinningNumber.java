package com.videotron.tvi.illico.itv.lottery.data;

public class LotteryWinningNumber {
    private String lotterySubName;
    private String winningNumber;
    
    public String getLotterySubName() {
        return lotterySubName;
    }
    public void setLotterySubName(String lotterySubName) {
        this.lotterySubName = lotterySubName;
    }
    
    public String getWinningNumber() {
        return winningNumber;
    }
    public void setWinningNumber(String winningNumber) {
        this.winningNumber = winningNumber;
    }
}

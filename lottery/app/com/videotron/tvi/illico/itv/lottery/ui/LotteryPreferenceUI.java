package com.videotron.tvi.illico.itv.lottery.ui;

import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.Breadcrumbs;
import com.videotron.tvi.illico.itv.lottery.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.lottery.comp.OptionItem;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.gui.LotteryPreferenceRenderer;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class LotteryPreferenceUI extends BaseUI {
    private static final long serialVersionUID = 7265273891328816283L;
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Breadcrumbs bc;
    private String[] bcTxts;
    private Footer footer;
    private LegalDisclaimer legalDisclaimer;
    /*********************************************************************************
     * Component - Option item-related
     *********************************************************************************/
    public static final int OPTION_ITEM_COUNT = 2;
    public static final int OPTION_ITEM_FRENCH = 0;
    public static final int OPTION_ITEM_ENGLISH = 1;
    public static final String[] OPTION_ITEM_PRIMARY_KEYS = {Definitions.LANGUAGE_FRENCH, Definitions.LANGUAGE_ENGLISH};
    public static final String[] OPTION_ITEM_DISPLAY_TEXTS = {"TxtLottery.French", "TxtLottery.English"};
    public OptionItem[] optItems;
    private Lottery[] favLots;

    protected void initScene() {
        renderer = new LotteryPreferenceRenderer();
        setRenderer(renderer);
        if (bc == null) {
            bc = new Breadcrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (bcTxts == null) {
            bcTxts = new String[1];
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 487, 550, 30);
            legalDisclaimer.setTextKeyLegalDisclaimer("TxtLottery.Desc_Legal_Disclaimer");
            legalDisclaimer.setVisible(true);
            add(legalDisclaimer, 0);
        }
        if (optItems == null) {
            optItems = new OptionItem[OPTION_ITEM_COUNT];
            for (int i=0; i<OPTION_ITEM_COUNT; i++) {
                optItems[i] = new OptionItem();
                optItems[i].setPrimaryKey(OPTION_ITEM_PRIMARY_KEYS[i]);
            }
        }
    }
    protected void disposeScene() {
        optItems = null;
        if (legalDisclaimer!= null) {
            remove(legalDisclaimer);
            legalDisclaimer.dispose();
            legalDisclaimer = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        renderer = null;
    }
    protected void startScene(boolean reset, String[] params) {
        CommunicationManager.getInstance().requestResizeScreen(0, 0, 0, 0);
        prepare();
        if (legalDisclaimer != null) {
            legalDisclaimer.start();
        }
        if (bc != null) {
            bcTxts[0] = DataCenter.getInstance().getString("TxtLottery.Lottery");
            bc.start(bcTxts);
        }
        if (footer != null) {
            footer.reset();
            footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
        }
        CommunicationManager.getInstance().requestShowLoadingAnimation();
        new Thread() {
            public void run() {
                try{
                    favLots = dMgr.getFavoriteLotteryList();
                } catch(RPManagerException rpe) {
                    CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    CommunicationManager.getInstance().requestHideLoadingAnimation();
                    repaint();
                }
            }
        }.start();
    }
    protected void stopScene() {
        if (legalDisclaimer != null) {
            legalDisclaimer.stop();
        }
        if (bc != null) {
            bc.stop();
        }
        CommunicationManager.getInstance().requestHideLoadingAnimation();
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_LEFT:
            case Rs.KEY_RIGHT:
                return true;
            case Rs.KEY_OK:
                new ClickingEffect(this, 5).start(339, 127, 212, 33);
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE_FAVORITE_LOTTERY, true, null);
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation(0);                    
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_MAIN, false, null);
                return true;
        }
        return false;
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
//    public int getCurrentIndex() {
//        return curIdx;
//    }
//    public String getCurrentDisplayTitleResultFirst() {
//        return dCenter.getString(OPTION_ITEM_DISPLAY_TEXTS[optItemIdx]);
//    }
    public Lottery[] getFavoriteLotteries() {
        return favLots;
    }
    /*********************************************************************************
     * ChoiceItem-related
     *********************************************************************************/
//    private int getCurrentDisplayTitleResultFirstIndex() {
//        int res = 0;
//        String dispTitleResFirst = dMgr.getDisplayTitleResultFirst();
//        if (dispTitleResFirst!= null) {
//            for (int i=0; i<OPTION_ITEM_COUNT; i++) {
//                if (OPTION_ITEM_PRIMARY_KEYS[i].equals(dispTitleResFirst)) {
//                    res = i;
//                    break;
//                }
//            }
//        }
//        return res;
//    }
    /*********************************************************************************
     * PopupAdapter-related
     *********************************************************************************/
//    public PopupAdapter getPopupAdapterPopupOption() {
//        if (popAdptDispTitleResFirst == null) {
//            popAdptDispTitleResFirst = new PopupAdapter() {
//                
//                public void popupOK(PopupEvent popupEvent) {
//                    PopupOption pop = (PopupOption)popupEvent.getPopup();
//                    OptionItem selectedOptItem = null;
//                    if (pop != null) {
//                        selectedOptItem = pop.getSelectedOptionItem();
//                        pop.removePopupListener();
//                        PopupController.getInstance().closePopup(pop);
//                    }
//                    if (selectedOptItem != null) {
//                        String primaryKey = selectedOptItem.getPrimaryKey();
//                        if (primaryKey != null) {
//                            String reqDispTitleResFirst = null;
//                            if (primaryKey.equals(OPTION_ITEM_PRIMARY_KEYS[OPTION_ITEM_FRENCH])) {
//                                reqDispTitleResFirst = OPTION_ITEM_PRIMARY_KEYS[OPTION_ITEM_FRENCH];
//                            } else if (primaryKey.equals(OPTION_ITEM_PRIMARY_KEYS[OPTION_ITEM_ENGLISH])){
//                                reqDispTitleResFirst = OPTION_ITEM_PRIMARY_KEYS[OPTION_ITEM_ENGLISH];
//                            }
//                            if (reqDispTitleResFirst != null) {
//                                boolean res = dMgr.setDisplayTitleResultFirst(reqDispTitleResFirst);
//                                if (res) {
//                                    optItemIdx = getCurrentDisplayTitleResultFirstIndex();
//                                    repaint();
//                                }
//                            }
//                        }
//                    }
//                }
//                
//                public void popupCancel(PopupEvent popupEvent) {
//                    PopupOption pop = (PopupOption)popupEvent.getPopup();
//                    if (pop != null) {
//                        pop.removePopupListener();
//                        PopupController.getInstance().closePopup(pop);
//                    }
//                }
//            };
//        }
//        return popAdptDispTitleResFirst;
//    }
}

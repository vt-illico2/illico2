package com.videotron.tvi.illico.itv.lottery.comp;

public class OptionItem {
    private String primaryKey;
    private String txtDisp;
    
    public String getPrimaryKey() {
        return primaryKey;
    }
    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }
    public String getDisplayText() {
        return txtDisp;
    }
    public void setDisplayText(String txtDisp) {
        this.txtDisp = txtDisp;
    }
}

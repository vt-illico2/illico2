package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.ui.LotterySummaryResultsUI;

public class LotterySummaryResultsRenderer extends BaseRenderer {
    private Image imgFocus;
    private Image imgShadowT;
    private Image imgShadowB;
    private Image imgArrowT;
    private Image imgArrowB;
    
    protected void prepareChild(){
        imgFocus = ImageManager.getInstance().getImage("00_banner_focus.png", ImageManager.TYPE_TEMPORARY);
        imgShadowT = ImageManager.getInstance().getImage("11_lotto_sh_t.png", ImageManager.TYPE_TEMPORARY);
        imgShadowB = ImageManager.getInstance().getImage("11_lotto_sh_b.png", ImageManager.TYPE_TEMPORARY);
        imgArrowT = ImageManager.getInstance().getImage("11_ars_t.png", ImageManager.TYPE_TEMPORARY);
        imgArrowB = ImageManager.getInstance().getImage("11_ars_b.png", ImageManager.TYPE_TEMPORARY);
    }
    
    protected void paintRenderer(Graphics g, UIComponent c) {
        // Text - Title
        String txtResSummary = DataCenter.getInstance().getString("TxtLottery.Results_Summary");
        if (txtResSummary != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtResSummary, 61, 98);
        }
        LotterySummaryResultsUI scene = (LotterySummaryResultsUI)c;
        int reqStatus = scene.getRequestStatus();
        switch(reqStatus) {
            case LotterySummaryResultsUI.REQUEST_STATUS_LOADING:
                return;
            case LotterySummaryResultsUI.REQUEST_STATUS_INVALID:
                String resMsg = scene.getResultMessage();
                if (resMsg != null) {
                    g.setFont(Rs.F27);
                    g.setColor(Rs.C245212016);
                    int resMsgWth = Rs.FM27.stringWidth(resMsg);
                    g.drawString(resMsg, 480 - (resMsgWth / 2), 294);
                    return;
                }
                return;
        }
        // Text - No summary result image 
        String txtSummaryResImg = DataCenter.getInstance().getString("TxtLottery.No_Summary_Result_Image");
        Image[] lotReses = scene.getLotteryResults();
        Lottery[] lotList = scene.getLotteryList();
        int startIdx = scene.getStartIndex();
        int dispCnt = scene.getDisplayCount();
        int curIdx = scene.getCurrentIndex();
        int ttCnt = scene.getTotalCount();
        g.setFont(Rs.F20);
        for (int i=0; i<dispCnt; i++) {
            int idx = startIdx+i;
            if (idx == curIdx) {
                g.drawImage(imgFocus, 239, 116 + (i * 73), 483, 76, c);
            }
            if (lotReses[idx] != null) {
                g.drawImage(lotReses[idx], 246, 121 + (i * 73), 468, 62, c);
            } else {
                String txtDisp = txtSummaryResImg;
                String lotName = null;
                if (lotList[idx] != null) {
                    lotName = lotList[idx].getLotteryProductName();
                }
                if (lotName != null) {
                    txtDisp += " ("+ lotName + ")";
                }
                g.setColor(Rs.C086086086);
                g.fillRect(246, 121 + (i * 73), 468, 62);
                g.setColor(Rs.C050050050);
                g.drawRect(246, 121 + (i * 73), 468, 62);
                if (txtDisp != null) {
                    int txtDispWth = Rs.FM20.stringWidth(txtDisp);
                    g.setColor(Rs.C110110110);
                    g.drawString(txtDisp, 480 - (txtDispWth/2), 160 + (i * 73));
                    g.setColor(Rs.C050050050);
                    g.drawString(txtDisp, 480 - (txtDispWth/2) - 1, 159 + (i * 73));
                }
            }
        }
        if (ttCnt > LotterySummaryResultsUI.PAGE_COUNT) {
            if (curIdx != 0) {
                g.drawImage(imgShadowT, 0, 104, c);
                g.drawImage(imgArrowT, 466, 101 + ((curIdx - startIdx) * 73), c);
            }
            if (curIdx != ttCnt - 1) {
                g.drawImage(imgShadowB, 0, 399, c);
                g.drawImage(imgArrowB, 466, 184 + ((curIdx - startIdx) * 73), c);
            }
        }
    }
}

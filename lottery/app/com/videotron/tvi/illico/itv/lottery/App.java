package com.videotron.tvi.illico.itv.lottery;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.controller.PreferenceProxy;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.log.Log;

/**
 * The initial class of Main Menu.
 * @author Sangjoon Kwon
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    private XletContext xletContext;
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        Log.setApplication(this);
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.initXlet]start.");
        }
        FrameworkMain.getInstance().init(this);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.startXlet]start.");
        }
        PopupController.getInstance().init();
        SceneManager.getInstance().init();

        FrameworkMain.getInstance().start();
        ImageManager.getInstance().start();
        CommunicationManager.getInstance().start();
        PreferenceProxy.getInstance().start();
        DataManager.getInstance().start();
        SceneManager.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.pauseXlet]start.");
        }
        SceneManager.getInstance().stop();
        DataManager.getInstance().stop();
        PreferenceProxy.getInstance().stop();
        CommunicationManager.getInstance().stop();
        ImageManager.getInstance().stop();
        FrameworkMain.getInstance().pause();
        //Remove reses
        SceneManager.getInstance().dispose();
        PopupController.getInstance().dispose();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional
     *            If unconditional is true when this method is called, requests by the Xlet to not enter the destroyed
     *            state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.destroyXlet]start.");
        }
        DataManager.getInstance().dispose();
        PreferenceProxy.getInstance().dispose();
        CommunicationManager.getInstance().dispose();
        ImageManager.getInstance().dispose();
        FrameworkMain.getInstance().destroy();
    }
    
    /*********************************************************************************
     * ApplicationConfig-implemented
     *********************************************************************************/
	public void init() {
        CommunicationManager.getInstance().init();
        PreferenceProxy.getInstance().init();
        DataManager.getInstance().init();
	}
    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }
    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }
    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

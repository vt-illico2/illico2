package com.videotron.tvi.illico.itv.lottery.controller;

import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.ImagePool;
import com.videotron.tvi.illico.log.Log;

public class ImageManager {
    private static ImageManager instance;
    private DataCenter dCenter;
    public static final int TYPE_PERMANENT = 0; //Dispose -> Remove
    public static final int TYPE_TEMPORARY = 1; //Stop, Dispose -> Remove
    public static final int TYPE_LOTTERY_RELATED = 2; //Stop, Dispose -> Remove
    private Vector perImgNameVec;
    private Vector tempImgNameVec;
    private Vector lotImgNameVec;
    private ImageManager() {
        init();
    }
    synchronized public static ImageManager getInstance() {
        if (instance == null) {
            instance = new ImageManager();
        }
        return instance;
    }
    public void init() {
        dispose();
        dCenter = DataCenter.getInstance();
        perImgNameVec = new Vector();
        tempImgNameVec = new Vector();
        lotImgNameVec = new Vector();
    }
    public void dispose() {
        stop();
        if (perImgNameVec != null) {
            int perImgNameVecSz = perImgNameVec.size();
            for (int i=0; i<perImgNameVecSz; i++) {
                String perImgName = (String)perImgNameVec.elementAt(i);
                if (perImgName != null) {
                    dCenter.removeImage(perImgName);
                }
            }
            perImgNameVec.clear();
            perImgNameVec = null;
        }
        dCenter = null;
    }
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ImageManager.stop]Stop called.");
        }
        if (tempImgNameVec != null) {
            int tempImgNameVecSz = tempImgNameVec.size();
            for (int i=0; i<tempImgNameVecSz; i++) {
                String tempImgName = (String)tempImgNameVec.elementAt(i);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ImageManager.stop]tempImgName["+i+"] = " + tempImgName);
                }
                if (tempImgName != null) {
                    dCenter.removeImage(tempImgName);
                }
            }
            tempImgNameVec.clear();
            tempImgNameVec = null;
        }
        if (lotImgNameVec != null) {
            int lotImgNameVecSz = lotImgNameVec.size();
            for (int i=0; i<lotImgNameVecSz; i++) {
                String lotImgName = (String)lotImgNameVec.elementAt(i);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ImageManager.stop]lotImgName["+i+"] = " + lotImgName);
                }
                if (lotImgName != null) {
            		ImagePool ip = FrameworkMain.getInstance().getImagePool();
            		ip.removeAbsolutely(lotImgName);
                }
            }
            lotImgNameVec.clear();
            lotImgNameVec = null;
        }
    }
    public void start() {
        stop();
        tempImgNameVec = new Vector();
        lotImgNameVec = new Vector();
    }
    public Image getImage(String key, int type) {
        if (key == null) {
            return null;
        }
        Vector checkVec = null;
        switch(type) {
            case TYPE_PERMANENT:
                checkVec = perImgNameVec;
                break;
            case TYPE_TEMPORARY:
                checkVec = tempImgNameVec;
                break;
            case TYPE_LOTTERY_RELATED:
                checkVec = lotImgNameVec;
                break;
        }
        if (checkVec == null) {
            return null;
        }
        synchronized(checkVec) {
            if (!checkVec.contains(key)) {
                checkVec.addElement(key);
            }
        }
        return dCenter.getImage(key);
    }
    public Image createImage(byte[] src, String key, int type) {
        if (src==null || key == null) {
            return null;
        }
        Vector checkVec = null;
        switch(type) {
            case TYPE_PERMANENT:
                checkVec = perImgNameVec;
                break;
            case TYPE_TEMPORARY:
                checkVec = tempImgNameVec;
                break;
            case TYPE_LOTTERY_RELATED:
                checkVec = lotImgNameVec;
                break;
        }
        if (checkVec == null) {
            return null;
        }
        synchronized(checkVec) {
            if (!checkVec.contains(key)) {
                checkVec.addElement(key);
            }
        }
        return FrameworkMain.getInstance().getImagePool().createImage(src, key);
    }
    synchronized public void removeImage(String key) {
        if (key == null) {
            return;
        }
        if (perImgNameVec != null && perImgNameVec.contains(key)) {
            perImgNameVec.removeElement(key);
            dCenter.removeImage(key);
        }
        if (tempImgNameVec != null && tempImgNameVec.contains(key)) {
            tempImgNameVec.removeElement(key);
            dCenter.removeImage(key);
        }
        if (lotImgNameVec != null && lotImgNameVec.contains(key)) {
        	lotImgNameVec.removeElement(key);
    		ImagePool ip = FrameworkMain.getInstance().getImagePool();
    		ip.removeAbsolutely(key);
        }
    }
}

package com.videotron.tvi.illico.itv.lottery.data;

import java.util.Calendar;
import java.util.Vector;

public class LatestResult {
    private String lotteryLanguage;
    private String lotteryName;
    private String lotProdNum;
    private String drawingDateString;
    private Calendar drawingDate;
    private Vector winNumsVec;
    private Vector subProdsVec;
    private LotteryWinningNumber[] winNums;
    private LatestResult[] subProds;
    
    public String getLotteryLanguage() {
        return lotteryLanguage;
    }
    public void setLotteryLanguage(String lotteryLanguage) {
        this.lotteryLanguage = lotteryLanguage;
    }
    
    public String getLotteryName() {
        return lotteryName;
    }
    public void setLotteryName(String lotteryName) {
        this.lotteryName = lotteryName;
    }
    
    public String getLotteryProductNumber() {
        return lotProdNum;
    }
    public void setLotteryProductNumber(String lotProdNum) {
        this.lotProdNum = lotProdNum;
    }
    
    public Calendar getDrawingDate() {
        return drawingDate;
    }
    
    public String getDrawingDateString() {
        return drawingDateString;
    }
    public void setDrawingDate(String drawingDateString) {
        this.drawingDateString = drawingDateString;
        if (drawingDateString != null) {
            int year = -1;
            int month = -1;
            int day = -1;
            String remainTxt = drawingDateString;
            int yearIdx = remainTxt.indexOf("-");
            if (yearIdx >= 0) {
                String txtYear = remainTxt.substring(0, yearIdx);
                try{
                    year = Integer.parseInt(txtYear);
                }catch(Exception e) {
                    e.printStackTrace();
                }
                remainTxt = remainTxt.substring(yearIdx+1);
            }
            int monthIdx = remainTxt.indexOf("-");
            if (monthIdx >= 0) {
                String txtMonth = remainTxt.substring(0, monthIdx);
                try{
                    month = Integer.parseInt(txtMonth);
                }catch(Exception e) {
                    e.printStackTrace();
                }
                remainTxt = remainTxt.substring(monthIdx+1);
            }
            try{
                day = Integer.parseInt(remainTxt);
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (year != -1 && month != -1 && day != -1) {
                drawingDate = Calendar.getInstance();
                drawingDate.clear();
                drawingDate.set(Calendar.YEAR, year);
                drawingDate.set(Calendar.MONTH, month-1);
                drawingDate.set(Calendar.DATE, day);
            }
        }
    }
    
    public LotteryWinningNumber[] getWinningNumbers() {
        if (winNums == null) {
            if (winNumsVec!=null) {
                int winNumsVecSz = winNumsVec.size();
                winNums = new LotteryWinningNumber[winNumsVecSz];
                winNums = (LotteryWinningNumber[])winNumsVec.toArray(winNums);
            }
        }
        return winNums;
    }
    public void addWinningNumber(LotteryWinningNumber winNum) {
        if (winNumsVec == null) {
            winNumsVec = new Vector();
        }
        winNumsVec.addElement(winNum);
    }
    
    public LatestResult[] getSubLotteryResults() {
        if (subProds == null) {
            if (subProdsVec!=null) {
                int subProdsVecSz = subProdsVec.size();
                subProds = new LatestResult[subProdsVecSz];
                subProds = (LatestResult[])winNumsVec.toArray(subProds);
            }
        }
        return subProds;
    }
    public void addSubLotteryResult(LatestResult subProd) {
        if (subProdsVec == null) {
            subProdsVec = new Vector();
        }
        subProdsVec.addElement(subProd);
    }
}

package com.videotron.tvi.illico.itv.lottery.controller;

import java.awt.Image;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.Util;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.data.ProxyInfo;
import com.videotron.tvi.illico.itv.lottery.data.ServerInfo;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;

public class DataManager implements DataUpdateListener {
    //Naming rule
    //Lottery -> Lot
    //Favorite -> Fav
    //Version -> Ver
    //Product -> Prod
    //Number --> No
    //Image -> Img
    //Current -> Cur
    //Index -> Idx
    //Result -> Res
    //Request -> Req
    //Respose -> Rep
    //Specific -> Spec
    //Duration -> Dur
    //PromotionXX ->Promo
    //Option -> Opt
    //Adapter -> Adpt
    //Preference -> Pref
    //Display -> Disp
    /*****************************************************************
     * Manager-related
     *****************************************************************/
    private static DataManager instance;
    private PreferenceProxy pProxy = PreferenceProxy.getInstance();
    private final StringBuffer sb = new StringBuffer();
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    private RPManager rpMgr;

    public static final int FAVORITE_LOTTERY_COUNT = 5;
    public static final String LANGUAGE_TAG_EN = "en";
    public static final String LANGUAGE_TAG_FR = "fr";
    private String curLangTag;
//    private String curDispTitleResFirst;

    private boolean isValidData;
    private Hashtable lotteryHash;
    private Hashtable promoBannerImgHash;

    private Lottery[] lotList;
    private Lottery[] sortedLotList;
    /*********************************************************************************
     * Favorite lottery-related
     *********************************************************************************/
    public static final String DEFAULT_SEPARATOR = "|";
    private Vector favLotVec;
    private Lottery[] favLots;
    /*********************************************************************************
     * IB-related
     *********************************************************************************/
    public static final int ITV_SERVER_TYPE = 1;
    private static final String ITV_SERVER_DATA_IB = "ITV_SERVER_DATA_IB";
    private ServerInfo serverInfo;
    private int latestVer;

    private DataManager() {
    }
    public synchronized static DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }
    public void init() {
        latestVer = -1;
        if (Rs.INSTEAD_OF_IB_OOB) {
            if (Rs.USE_PROP_DATA_INSTEAD_OF_IB_OOB) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Emulator - Properties.");
                }
                byte[] bytesITvServer = DataConverter.getBytesITvServer();
                parseITvServerData(bytesITvServer);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.init]Emulator - IB.");
                }
                File fileITvServer = new File("resource/test_data/ms_data/itv_server.txt");
                byte[] bytesITvServer = Util.getByteArrayFromFile(fileITvServer);
                parseITvServerData(bytesITvServer);
            }
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Use real data.(Inband, OOB)");
            }
            File fileLoadedITvServer = (File)DataCenter.getInstance().get(ITV_SERVER_DATA_IB);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded ITV Server file : "+fileLoadedITvServer);
            }
            if (fileLoadedITvServer != null) {
                dataUpdated(ITV_SERVER_DATA_IB, null, fileLoadedITvServer);
            }
            DataCenter.getInstance().addDataUpdateListener(ITV_SERVER_DATA_IB, this);
        }
    }
    public void dispose() {
        stop();
        DataCenter.getInstance().removeDataUpdateListener(ITV_SERVER_DATA_IB, this);
        latestVer = -1;
    }
    public void start() {
        if (promoBannerImgHash == null) {
            promoBannerImgHash = new Hashtable();
        }
    }
    public void stop() {
        if (promoBannerImgHash != null) {
            promoBannerImgHash.clear();
            promoBannerImgHash = null;
        }
        favLotVec = null;
        favLots = null;
        if (rpMgr != null) {
            rpMgr.dispose();
            rpMgr = null;
        }
        if (lotteryHash != null) {
            lotteryHash.clear();
            lotteryHash = null;
        }
        if (favLotVec != null) {
            favLotVec.clear();
            favLotVec = null;
        }
        curLangTag = null;
//        curDispTitleResFirst = null;
        lotList = null;
        sortedLotList = null;
        favLots = null;
    }
    /*********************************************************************************
     * IB data-related
     *********************************************************************************/
    private boolean parseITvServerData(byte[] src) {
        if (src == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }  
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM);
            return false;
        }
        try {
            int index = 0;
            //Version
            int version = src[index++];
            if (version == latestVer) {
                return true;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseITVServerData]version : " + version);
            }
            //Server list size
            int serverListSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseITVServerData]Server list size : " + serverListSize);
            }
            ServerInfo tempServerInfo = null;
            for (int i=0; i<serverListSize; i++) {
                //Server type
                int serverType = src[index++];
                //IpDns
                int ipDnsLth = src[index++];
                String ipDns = new String(src, index, ipDnsLth, "ISO8859_1");
                index += ipDnsLth;
                //Port
                int port = Util.twoBytesToInt(src, index, true);
                index += 2;
                //Context root
                int contextRootLth = src[index++];
                String contextRoot = new String(src, index, contextRootLth, "ISO8859_1");
                index += contextRootLth;
                //Proxy list size
                int proxyListSize = src[index++];
                ProxyInfo[] tempProxyInfoList = new ProxyInfo[proxyListSize];
                for (int j=0; j<proxyListSize; j++) {
                    tempProxyInfoList[j] = new ProxyInfo();
                    //Proxy ip
                    int proxyIpLth = src[index++];
                    String proxyIp = new String(src, index, proxyIpLth, "ISO8859_1");
                    index += proxyIpLth;
                    tempProxyInfoList[j].setProxyHost(proxyIp);
                    //Proxy port
                    int proxyPort = Util.twoBytesToInt(src, index, true);
                    index += 2;
                    tempProxyInfoList[j].setProxyPort(proxyPort);
                }
                if (serverType == ITV_SERVER_TYPE) {
                    tempServerInfo = new ServerInfo();
                    tempServerInfo.setHost(ipDns);
                    tempServerInfo.setPort(port);
                    tempServerInfo.setContextRoot(contextRoot);
                    tempServerInfo.setProxyInfos(tempProxyInfoList);
                    break;
                }
            }
            latestVer = version;
            serverInfo = tempServerInfo;
            if (serverInfo != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseITVServerData]Host : " + tempServerInfo.getHost());
                    Log.printDebug("[DataMgr.parseITVServerData]Port : " + tempServerInfo.getPort());
                    Log.printDebug("[DataMgr.parseITVServerData]Context root : " + tempServerInfo.getContextRoot());
                    Log.printDebug("[DataMgr.parseITVServerData]Proxy info : " + tempServerInfo.getProxyInfos());
                    if (tempServerInfo.getProxyInfos() != null) {
                        Log.printDebug("[DataMgr.parseITVServerData]Proxy info length : " + tempServerInfo.getProxyInfos().length);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }              
            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM);
            return false;
        }
        return true;
    }
    /*********************************************************************************
     * Lottery-related
     *********************************************************************************/
    synchronized public Lottery[] getLotteryList() throws RPManagerException {
        if (lotList == null) {
            lotList = getRPManager().getLotteryList();
        }
        return lotList;
    }
    synchronized public Lottery[] getSortedLotteryList() throws RPManagerException {
        if (sortedLotList == null) {
//            Lottery[] tempLotList = getLotteryList();
//            if (tempLotList == null) {
//                return null;
//            }
//            Vector tempVec = new Vector();
//            for (int i=0; i<tempLotList.length; i++) {
//                if (tempLotList[i] != null) {
//                    tempVec.addElement(tempLotList[i]);
//                }
//            }
//            Collections.sort(tempVec);
//            int tempVecSz = tempVec.size();
//            sortedLotList = new Lottery[tempVecSz];
//            sortedLotList = (Lottery[])tempVec.toArray(sortedLotList);
//            for (int i=0; i<sortedLotList.length; i++) {
//                if (sortedLotList[i] != null) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.parseITVServerData]sortedLotList["+i+"] : " + sortedLotList[i].getLotteryName());
//                    }
//                }
//            }
            Lottery[] tempLotList = getLotteryList();
            Lottery[] tempFavLotList = getFavoriteLotteryList();
            Vector tempSortVec = new Vector();
            if (tempFavLotList != null) {
                for (int i=0; i<tempFavLotList.length; i++) {
                    if (tempFavLotList[i] != null) {
                        tempSortVec.addElement(tempFavLotList[i]);
                    }
                }
                for (int i=0; i<tempLotList.length; i++) {
                    if (tempLotList[i] != null && !tempSortVec.contains(tempLotList[i])) {
                        tempSortVec.addElement(tempLotList[i]);
                    }
                }
                int tempSortVecSz = tempSortVec.size();
                sortedLotList = new Lottery[tempSortVecSz];
                sortedLotList = (Lottery[])tempSortVec.toArray(sortedLotList);
            } else {
                sortedLotList = (Lottery[])tempLotList.clone();
            }
        }
        return sortedLotList;
    }
    synchronized public Lottery[] getSortedValidLotteryList() throws RPManagerException {
    	Lottery[] lots= getSortedLotteryList();
        if (lots==null) {
            return null;
        }
        ArrayList al=new ArrayList();
        int sortedLotListCnt = lots.length;
        for (int i=0; i<sortedLotListCnt; i++) {
            if (lots[i]==null || !lots[i].isValidLottery()){
                continue;
            }
            al.add(lots[i]);
        }
        int alCnt=al.size();
        Lottery[] validLot=new Lottery[alCnt];
        validLot=(Lottery[]) al.toArray(validLot);
        return validLot;
    }
    public Lottery getLotteryByProductNumber(String reqProdNo) throws RPManagerException {
        if (lotteryHash == null) {
        	Lottery[] tempLotList = getSortedLotteryList();
            if (tempLotList != null) {
                lotteryHash = new Hashtable();
                for (int i=0; i<tempLotList.length; i++) {
                    if (tempLotList[i] == null) {
                        continue;
                    }
                    String prodNo = tempLotList[i].getProductNumber();
                    if (prodNo == null) {
                        continue;
                    }
                    lotteryHash.put(prodNo, tempLotList[i]);
                }
            }
        }
        return (Lottery)lotteryHash.get(reqProdNo);
    }
    /*********************************************************************************
     * Favorite lottery-related
     *********************************************************************************/
    synchronized public void initFavoriteLotteryList() throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.initFavoriteLotteries]start.");
        }
        favLotVec = new Vector();
        String favLotProdNoSrc = pProxy.getFavoriteLotteryProductNumbers();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.initFavoriteLotteries]Favorite lottery product number [" + favLotProdNoSrc + "]");
        }
        if (favLotProdNoSrc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.initFavoriteLotteries]Favorite lottery product number is null. end");
            }
            return;
        }
        setFavoriteLotteryListByUppData(favLotProdNoSrc);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.initFavoriteLotteries]end.");
        }
    }
    synchronized public void setFavoriteLotteryListByUppData(String reqFavLotListUppData) throws RPManagerException {
        if (favLotVec != null) {
            favLotVec.clear();
        }
        StringTokenizer st = new StringTokenizer(reqFavLotListUppData, DEFAULT_SEPARATOR);
        int stCount = st.countTokens();
        for (int i=0; i<stCount; i++) {
            String prodNo = st.nextToken();
            Lottery lot = getLotteryByProductNumber(prodNo);
            if (lot == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.initFavoriteLotteries]Favorite lottery is null. continue.");
                }
                continue;
            }
            favLotVec.addElement(lot);
        }
        sortedLotList = null;
        favLots = null;
    }
    synchronized public Lottery[] getFavoriteLotteryList() throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getFavoriteLotteries]start.");
        }
        if (favLots == null) {
            if (favLotVec == null) {
                initFavoriteLotteryList();
            }
            int favLotVecSz = favLotVec.size();
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getFavoriteLotteries]favLotVecSz : "+favLotVecSz);
            }
            favLots = new Lottery[favLotVecSz];
            favLots = (Lottery[])favLotVec.toArray(favLots);
        }
        if (Log.DEBUG_ON) {
            if (favLots != null) {
                Log.printDebug("[DataMgr.getFavoriteLotteries]Favorite lottery count : "+favLots.length);
            }
            Log.printDebug("[DataMgr.getFavoriteLotteries]end. : "+favLots);
        }
        return favLots;
    }
    synchronized public boolean setFavoriteLotteryList(Lottery[] reqData) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteLotteries]start.");
        }
        if (reqData == null) {
            return false;
        }
        sb.setLength(0);
        for (int i=0; i<reqData.length; i++) {
            if (reqData[i] == null) {
                continue;
            }
            String prodNo = reqData[i].getProductNumber();
            sb.append(prodNo);
            if (i != reqData.length-1) {
                sb.append(DEFAULT_SEPARATOR);
            }
        }
        String src = sb.toString();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteLotteries]Favorite lottery product number [" + src + "]");
        }
        boolean result = pProxy.setFovoriteLotteryProductNumbers(src);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteLotteries]setFovoriteLotteryProductNumbers result : " + result);
        }
        if (result) {
            setFavoriteLotteryListByUppData(src);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setFavoriteLotteries]end. : "+result);
        }
        return result;
    }
    /*********************************************************************************
     * Lottery-related
     *********************************************************************************/
    synchronized public Image getSummaryResultImage(String reqProdNo) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]start.");
            Log.printDebug("[DataMgr.getSummaryResultImage]Parameter - Request product number : "+reqProdNo);
        }
        if (reqProdNo == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Request parameter is invalid. return null.");
            }
            return null;
        }
        String langTag = getCurrentLanguageTag();
        if (langTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Current language tag is null. return null.");
            }
            return null;
        }
        String summaryResImgKey = Rs.IMG_KEY_SUMMARY_RESULT + langTag + DEFAULT_SEPARATOR + reqProdNo + DEFAULT_SEPARATOR + "latest";
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]Summary result image key : "+summaryResImgKey);
        }
        Image summaryResImg = ImageManager.getInstance().getImage(summaryResImgKey, ImageManager.TYPE_LOTTERY_RELATED);
        if (summaryResImg != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]return existed summary result image about "+reqProdNo+". end.");
            }
            return summaryResImg;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]summary image is null. get image from server.");
        }
        byte[] summaryResImgSrc = getRPManager().getSummaryResult(langTag, reqProdNo, null);
        if (summaryResImgSrc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Summary result image source is null. return null.");
            }
            return null;
        }
        summaryResImg = ImageManager.getInstance().createImage(summaryResImgSrc, summaryResImgKey, ImageManager.TYPE_LOTTERY_RELATED);
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]End : "+summaryResImg);
        }
        return summaryResImg;
    }
    synchronized public Image getSummaryResultImage(String reqProdNo, String reqDate) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]start.");
            Log.printDebug("[DataMgr.getSummaryResultImage]Parameter - Request product number : "+reqProdNo);
            Log.printDebug("[DataMgr.getSummaryResultImage]Parameter - Request Date : "+reqDate);
        }
//        if (reqProdNo == null || reqDate == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getSummaryResultImage]Request parameter is invalid. return null.");
//            }
//            return null;
//        }
        if (reqProdNo == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Request parameter is invalid. return null.");
            }
            return null;
        }
        String langTag = getCurrentLanguageTag();
        if (langTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Current language tag is null. return null.");
            }
            return null;
        }
        String summaryResImgKey = Rs.IMG_KEY_SUMMARY_RESULT + langTag + DEFAULT_SEPARATOR + reqProdNo + DEFAULT_SEPARATOR + "latest";
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]Summary result image key : "+summaryResImgKey);
        }
        Image summaryResImg = ImageManager.getInstance().getImage(summaryResImgKey, ImageManager.TYPE_LOTTERY_RELATED);
        if (summaryResImg != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]return existed summary result image about "+reqProdNo+". end.");
            }
            return summaryResImg;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]summary image is null. get image from server.");
        }
        byte[] summaryResImgSrc = getRPManager().getSummaryResult(langTag, reqProdNo, reqDate);
        if (summaryResImgSrc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Summary result image source is null. return null.");
            }
            return null;
        }
        summaryResImg = ImageManager.getInstance().createImage(summaryResImgSrc, summaryResImgKey, ImageManager.TYPE_LOTTERY_RELATED);
        FrameworkMain.getInstance().getImagePool().waitForAll();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]End : "+summaryResImg);
        }
        return summaryResImg;
    }
    public Image getSummaryResultImage(String reqProdNo, Calendar reqDrawingDateCal) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getSummaryResultImage]start.");
            Log.printDebug("[DataMgr.getSummaryResultImage]Parameter - Product number : "+reqProdNo);
            Log.printDebug("[DataMgr.getSummaryResultImage]Parameter - Drawing Date calendar : "+reqDrawingDateCal);
        }
//        if (reqProdNo == null || reqDrawingDateCal == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getSummaryResultImage]Product number or date is null. end.");
//            }
//            return null;
//        }
        if (reqProdNo == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getSummaryResultImage]Product number or date is null. end.");
            }
            return null;
        }
        String reqDrawingDate = sdf.format(reqDrawingDateCal.getTime());
        return getSummaryResultImage(reqProdNo, reqDrawingDate);
    }
//    public Image getDetailedResultImage(String reqProdNo, Calendar reqDrawingDateCal) throws RPManagerException {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]start.");
//            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Product number : "+reqProdNo);
//            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Drawing Date calendar : "+reqDrawingDateCal);
//        }
//        if (reqProdNo == null || reqDrawingDateCal == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]Product number or date is null. end.");
//            }
//            return null;
//        }
//        String reqDrawingDate = sdf.format(reqDrawingDateCal.getTime());
//        return getDetailedResultImage(reqProdNo, reqDrawingDate);
//    }
//    public Image getDetailedResultImage(String reqProdNo, String reqDate) throws RPManagerException {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]start.");
//            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Product number : "+reqProdNo);
//            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Drawing date : "+reqDate);
//        }
//        if (reqProdNo == null || reqDate == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]Request parameter is invalid. return null.");
//            }
//            return null;
//        }
//        String langTag = getCurrentLanguageTag();
//        if (langTag == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]Current language tag is null. return null.");
//            }
//            return null;
//        }
//        String detailedResImgKey = Rs.IMG_KEY_DETAILED_RESULT + langTag + DEFAULT_SEPARATOR + reqProdNo + DEFAULT_SEPARATOR + reqDate;
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getSummaryResultImage]Detailed result image key : "+detailedResImgKey);
//        }
//        Image detailedResImg = ImageManager.getInstance().getImage(detailedResImgKey, ImageManager.TYPE_TEMPORARY);
//        if (detailedResImg != null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]return existed detailed result image about "+reqProdNo+". end.");
//            }
//            return detailedResImg;
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]Detailed result image is null. get image from server.");
//        }
//        byte[] detailedResImgSrc = getRPManager().getDetailedResult(langTag, reqProdNo, reqDate);
//        if (detailedResImgSrc == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]Detailed result image source is null. return null.");
//            }
//            return null;
//        }
//        detailedResImg = ImageManager.getInstance().createImage(detailedResImgSrc, detailedResImgKey, ImageManager.TYPE_TEMPORARY);
//        FrameworkMain.getInstance().getImagePool().waitForAll();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]End : "+detailedResImg);
//        }
//        return detailedResImg;
//    }
    public byte[] getDetailedResultImageSrc(String reqProdNo, Calendar reqDrawingDateCal) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getDetailedResultImage]start.");
            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Product number : "+reqProdNo);
            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Drawing Date calendar : "+reqDrawingDateCal);
        }
        if (reqProdNo == null || reqDrawingDateCal == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getDetailedResultImage]Product number or date is null. end.");
            }
            return null;
        }
        String reqDrawingDate = sdf.format(reqDrawingDateCal.getTime());
        return getDetailedResultImageSrc(reqProdNo, reqDrawingDate);
    }
    public byte[] getDetailedResultImageSrc(String reqProdNo, String reqDate) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getDetailedResultImage]start.");
            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Product number : "+reqProdNo);
            Log.printDebug("[DataMgr.getDetailedResultImage]Parameter - Drawing date : "+reqDate);
        }
        if (reqProdNo == null || reqDate == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getDetailedResultImage]Request parameter is invalid. return null.");
            }
            return null;
        }
        String langTag = getCurrentLanguageTag();
        if (langTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getDetailedResultImage]Current language tag is null. return null.");
            }
            return null;
        }
//        String detailedResImgKey = Rs.IMG_KEY_DETAILED_RESULT + langTag + DEFAULT_SEPARATOR + reqProdNo + DEFAULT_SEPARATOR + reqDate;
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getSummaryResultImage]Detailed result image key : "+detailedResImgKey);
//        }
//        Image detailedResImg = ImageManager.getInstance().getImage(detailedResImgKey, ImageManager.TYPE_TEMPORARY);
//        if (detailedResImg != null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getDetailedResultImage]return existed detailed result image about "+reqProdNo+". end.");
//            }
//            return detailedResImg;
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]Detailed result image is null. get image from server.");
//        }
        byte[] detailedResImgSrc = getRPManager().getDetailedResult(langTag, reqProdNo, reqDate);
        if (detailedResImgSrc == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getDetailedResultImage]Detailed result image source is null. return null.");
            }
            return null;
        }
//        detailedResImg = ImageManager.getInstance().createImage(detailedResImgSrc, detailedResImgKey, ImageManager.TYPE_TEMPORARY);
//        FrameworkMain.getInstance().getImagePool().waitForAll();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getDetailedResultImage]End : "+detailedResImg);
//        }
        return detailedResImgSrc;
    }
    public Image[] getPromotionalBannerImages() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]start.");
        }
        String langTag = getCurrentLanguageTag();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Current language tag : "+langTag);
        }
        if (langTag == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getPromotionalBannerImages]Current language tag is null. return null.");
            }
            return null;
        }
        String fpPromoBannerImgKey = Rs.IMG_KEY_PROMOTIONAL_BANNER + RPManager.BANNER_TYPE_FUTURE_PRODUCTS + DEFAULT_SEPARATOR + langTag;
        Image[] fPPromoBannerImgs = (Image[]) promoBannerImgHash.get(fpPromoBannerImgKey);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Future product image key : "+fpPromoBannerImgKey);
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Cached future product image : "+fpPromoBannerImgKey);
        }
        if (fPPromoBannerImgs == null) {
            Hashtable fpPromoBannerHash = null;
            try{
                fpPromoBannerHash = getRPManager().getPromotionalBanner(langTag, RPManager.BANNER_TYPE_FUTURE_PRODUCTS);
            }catch(RPManagerException ignore) {
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getPromotionalBannerImages]Server future product hash : "+fpPromoBannerHash);
            }
            if (fpPromoBannerHash != null) {
                int size = fpPromoBannerHash.size();
                fPPromoBannerImgs = new Image[size];
                Enumeration enu = fpPromoBannerHash.keys();
                boolean isValid = true;
                for (int i=0; i<size; i++) {
                    String key = (String) enu.nextElement();
                    byte[] promoBannerImgSrc = (byte[]) fpPromoBannerHash.get(key);
                    fPPromoBannerImgs[i] = ImageManager.getInstance().createImage(promoBannerImgSrc, key, ImageManager.TYPE_LOTTERY_RELATED);
                    if (fPPromoBannerImgs[i] ==null) {
                        isValid = false;
                    }
                }
                if (isValid) {
                    promoBannerImgHash.put(fpPromoBannerImgKey, fPPromoBannerImgs);
                }
            }
        }
        String ndPromoBannerImgKey = Rs.IMG_KEY_PROMOTIONAL_BANNER + RPManager.BANNER_TYPE_NEXT_DRAW + DEFAULT_SEPARATOR + langTag;
        Image[] ndPromoBannerImgs = (Image[]) promoBannerImgHash.get(ndPromoBannerImgKey);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Next draw image key : "+ndPromoBannerImgKey);
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Cached next draw image : "+ndPromoBannerImgs);
        }
        if (ndPromoBannerImgs == null) {
            Hashtable ndPromoBannerHash = null;
            try{
                ndPromoBannerHash = getRPManager().getPromotionalBanner(langTag, RPManager.BANNER_TYPE_NEXT_DRAW);
            }catch(RPManagerException ignore) {
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getPromotionalBannerImages]Server next draw hash : "+ndPromoBannerHash);
            }
            if (ndPromoBannerHash != null) {
                boolean isValid = true;
                int size = ndPromoBannerHash.size();
                ndPromoBannerImgs = new Image[size];
                Enumeration enu = ndPromoBannerHash.keys();
                for (int i=0; i<size; i++) {
                    String key = (String) enu.nextElement();
                    byte[] promoBannerImgSrc = (byte[]) ndPromoBannerHash.get(key);
                    ndPromoBannerImgs[i] = ImageManager.getInstance().createImage(promoBannerImgSrc, key, ImageManager.TYPE_LOTTERY_RELATED);
                    if (ndPromoBannerImgs[i] == null) {
                        isValid = false;
                    }
                }
                if (isValid) {
                    promoBannerImgHash.put(ndPromoBannerImgKey, ndPromoBannerImgs);
                }
            }
        }
        int fPPromoBannerImgsLth = 0;
        if (fPPromoBannerImgs != null) {
            fPPromoBannerImgsLth = fPPromoBannerImgs.length;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Future product image length : "+fPPromoBannerImgsLth);
        }
        int ndPromoBannerImgsLth = 0;
        if (ndPromoBannerImgs != null) {
            ndPromoBannerImgsLth = ndPromoBannerImgs.length;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getPromotionalBannerImages]Next draw image length : "+ndPromoBannerImgsLth);
        }
        Image[] promoBannerImgs = new Image[fPPromoBannerImgsLth + ndPromoBannerImgsLth];
        for (int i=0; i<fPPromoBannerImgsLth; i++) {
            promoBannerImgs[i] = fPPromoBannerImgs[i];
        }
        for (int i=0; i<ndPromoBannerImgsLth; i++) {
            promoBannerImgs[fPPromoBannerImgsLth+i] = ndPromoBannerImgs[i];
        }
        return promoBannerImgs;
    }
    /*********************************************************************************
     * Drawing date-related
     *********************************************************************************/
//    public Calendar getLatestDrawingDate(String reqProdNum) {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getLatestDrawingDate]start.");
//            Log.printDebug("[DataMgr.getLatestDrawingDate]Parameter - Product number : "+reqProdNum);
//        }
//        if (reqProdNum == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getLatestDrawingDate]Product number is null. end.");
//            }
//            return null;
//        }
//        //gets lottery latest result from server.
//        LatestResult latestRes = (LatestResult)lotLatestResHash.get(reqProdNum);
//        if (latestRes == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DataMgr.getLatestDrawingDate]Latest result is null. get data from RP module.");
//            }
//            latestRes = getRPManager().getLatestResult(reqProdNum);
//            if (latestRes == null) {
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DataMgr.getLatestDrawingDate]Latest result can not get from server. end.");
//                }
//                return null;
//            }
//            lotLatestResHash.put(reqProdNum, latestRes);
//        }
//        Calendar latestDrawingDate = latestRes.getDrawingDate();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.getLatestDrawingDate]Latest drawing date : "+latestDrawingDate);
//        }
//        return latestDrawingDate;
//    }
    public boolean isValidData() {
        return isValidData;
    }
    public String getLotteryName(String reqLotProdNum) throws RPManagerException {
    	Lottery lot = getLotteryByProductNumber(reqLotProdNum);
        if (lot == null) {
            return null;
        }
        return lot.getLotteryProductName();
    }
    public boolean isFavoriteLotteryProductNumber(String reqProdNum) {
//        if (favLotProdNums == null) {
//            return false;
//        }
//        for (int i=0; i<favLotProdNums.length; i++) {
//            if (favLotProdNums[i] == null) {
//                continue;
//            }
//            if (favLotProdNums[i].equalsIgnoreCase(pNum)) {
//                return true;
//            }
//        }
        return false;
    }
//    public String[] getFavoriteLotteryProductNumbers() {
//        if (favLotProdNums == null) {
//            favLotProdNums = new String[FAVORITE_LOTTERY_COUNT];
//        }
//        return favLotProdNums;
//    }
    public void putFavoriteLotteryProductNumber(int index, String lotProdNum) {
//        if (favLotProdNums == null) {
//            favLotProdNums = new String[FAVORITE_LOTTERY_COUNT];
//        }
//        favLotProdNums[index] = lotProdNum;
//        sb.setLength(0);
//        for (int i=0; i<FAVORITE_LOTTERY_COUNT; i++) {
//            if (favLotProdNums[i] == null) {
//                sb.append("");
//            } else {
//                sb.append(favLotProdNums[i]);
//            }
//            if (i != FAVORITE_LOTTERY_COUNT-1) {
//                sb.append(FAVORITE_LOTTERY_SEPARATOR);
//            }
//        }
        pProxy.setFovoriteLotteryProductNumbers(sb.toString());
//        orderByFavoriteLottery();
    }
//    public String getDisplayTitleResultFirst() {
//        if (curDispTitleResFirst == null) {
//            curDispTitleResFirst = pProxy.getDisplayTitleResultFirst();
//        }
//        return curDispTitleResFirst;
//    }
//    public boolean setDisplayTitleResultFirst(String reqData) {
//        boolean res = pProxy.setDisplayTitleResultFirst(reqData);
//        if (res) {
//            curDispTitleResFirst = reqData;
//            sortedLotList = null;
//        }
//        return res;
//    }
    public ServerInfo getServerInfo() {
        return serverInfo;
    }
    synchronized public String getCurrentLanguageTag() {
        if (curLangTag == null) {
            String language = pProxy.getLanguage();
            if (language != null && language.equals(Definitions.LANGUAGE_FRENCH)) {
                curLangTag = LANGUAGE_TAG_FR;
            } else {
                curLangTag = LANGUAGE_TAG_EN;
            }
        }
        return curLangTag;
    }
    public void updatedPreferenceLanguage() {
        curLangTag = null;
    }
    /*********************************************************************************
     * Manager-related
     *********************************************************************************/
    private RPManager getRPManager() {
        if (rpMgr == null) {
            if (Rs.USE_LOCAL_SERVER_FILE) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.getRPManager]Create RPManager - local data.");
                }
                rpMgr = new RPManagerLocal();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.getRPManager]Create RPManager - server data.");
                }
                rpMgr = new RPManagerServer();
            }
            rpMgr.init();
        }
        return rpMgr;
    }

    /*********************************************************************************
     * DataUpdateListener-implemented
     *********************************************************************************/
    /**
     * Called when data has been removed.
     * @param key key of data.
     */
    public void dataRemoved(String key){
    }
    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value){
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataUpdated]key : " + key);
            Log.printDebug("[DataMgr.dataUpdated]old : " + old);
            Log.printDebug("[DataMgr.dataUpdated]value : " + value);
        }
        if (key == null) {
            return;
        }
        if (key.equals(ITV_SERVER_DATA_IB)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated iTv server data.");
            }
            File fileITvServerData = (File) value;
            if (fileITvServerData != null) {
                byte[] bytesITvServerData = Util.getByteArrayFromFile(fileITvServerData);
                parseITvServerData(bytesITvServerData);
            }
        }
    }
}

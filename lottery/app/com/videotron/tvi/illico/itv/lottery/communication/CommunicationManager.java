package com.videotron.tvi.illico.itv.lottery.communication;

import java.awt.Point;
import java.rmi.RemoteException;
import org.dvb.io.ixc.IxcRegistry;
import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.LotteryVbmController;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;

public class CommunicationManager {
    private static CommunicationManager instance;
    private static boolean forcedStop;
    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    private MonitorService mSvc;
    private InbandDataListenerImpl idlImpl;
    private ScreenSaverConfirmationListenerImpl ssclImpl;
    /*********************************************************************************
     * Epg Service-related
     *********************************************************************************/
    private EpgService eSvc;
    private VideoController vCnt;
    private VideoResizeListenerImpl vImpl;
    /*********************************************************************************
     * Loading Animation Service-related
     *********************************************************************************/
    private LoadingAnimationService lSvc;
    private boolean isLoadingAnimationService;
    /*********************************************************************************
     * Stc Service-related
     *********************************************************************************/
    private StcService stcSvc;
    private LogLevelChangeListenerImpl llclImpl;
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    /** The ErrorMessage service. */
    private ErrorMessageService emSvc;
    /*********************************************************************************
     * VBM Service-related
     *********************************************************************************/
    private VbmService vSvc;
    
    private CommunicationManager() {
    }
    public synchronized static CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }
    public void init() {
        forcedStop = false;
        initMonitorService();
        lookupVbmService();
        if (!Rs.IS_EMULATOR) {
            initStcService();
        }
    }
    public void dispose() {
        forcedStop = true;
        if (vCnt != null && vImpl != null) {
            try{
                vCnt.removeVideoResizeListener(vImpl);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        vCnt = null;
        vImpl = null;
        eSvc = null;
        lSvc=null;
        if (vSvc!=null) {
        	LotteryVbmController.getInstance().dispose();
            vSvc=null;
        }
        disposeStcService();
        disposeMonitorService();
    }
    public void start() {
        if (mSvc != null) {
            if (ssclImpl == null) {
                ssclImpl = new ScreenSaverConfirmationListenerImpl();
            }
            try {
                mSvc.addScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void stop() {
        if (mSvc != null && ssclImpl != null) {
            try {
                mSvc.removeScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
            ssclImpl = null;
        }
    }
    /*********************************************************************************
     * MonitorService-related
     *********************************************************************************/
    private void initMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.initMonitorService]start");
        }
        final long LOOKUP_RETRY_WAIT_MILLIS = 2000L;
        final int LOOKUP_MAX_RETRY_COUNT = 100;
        final String LOOK_UP_NAME = "/1/1/" + MonitorService.IXC_NAME;
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.initMonitorService]Monitor service look up name : " + LOOK_UP_NAME);
        }
        try {
            new Thread() {
                public void run() {
                    int nThTry = 0;
                    while (nThTry++ < LOOKUP_MAX_RETRY_COUNT) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.initMonitorService]Try to look up -"+nThTry+"th.");
                        }
                        if (forcedStop) {
                            break;
                        }
                        if (mSvc == null) {
                            try {
                                mSvc = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), LOOK_UP_NAME);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.initMonitorService]not bound - " + MonitorService.IXC_NAME);
                                }
                            }
                        }
                        if (mSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.initMonitorService]looked up.");
                            }
                            try {
                                mSvc.addInbandDataListener((idlImpl = new InbandDataListenerImpl()), Rs.APP_NAME);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;
                        }
                        try {
                            Thread.sleep(LOOKUP_RETRY_WAIT_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void disposeMonitorService() {
        if (!Rs.IS_EMULATOR) {
            if (mSvc != null && idlImpl != null) {
                try {
                    mSvc.removeInbandDataListener(idlImpl, Rs.APP_NAME);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        mSvc = null;
        idlImpl = null;
    }
    public void requestCompleteReceivingData() {
        try {
            mSvc.completeReceivingData(Rs.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void requestExitToChannel() {
        if (mSvc != null) {
            try {
                mSvc.exitToChannel();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public boolean isCalledFromMenu() {
        boolean isCalledFromMonitor = false;
        if (mSvc != null) {
            try{
                String[] params = mSvc.getParameter(Rs.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[CommMgr.isCalledFromMenu]getParameter("+Rs.APP_NAME+") : "+params);
                }
                if (params != null && params.length > 0) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[CommMgr.isCalledFromMenu]params[0] : "+params[0]);
                    }
                    if (params[0] != null && params[0].equalsIgnoreCase("Menu")) {
                        isCalledFromMonitor = true;
                    }
                }
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.isCalledFromMonitor]isCalledFromMonitor : "+isCalledFromMonitor);
        }
        return isCalledFromMonitor;
    }
    public void requestStartUnboundApplication(String reqAppName, String[] reqParams) {
        if (mSvc != null) {
            try {
                mSvc.startUnboundApplication(reqAppName, reqParams);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public String[] requestGetParameter() {
        String[] params=null;
        if (mSvc != null) {
            try {
                params = mSvc.getParameter(Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return params;
    }
    /*********************************************************************************
     * EPGService-related
     *********************************************************************************/
    public EpgService getEpgService() {
        if (eSvc == null) {
            final String LOOK_UP_NAME = "/1/2026/" + EpgService.IXC_NAME;
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommMgr.getEpgService]Epg service look up name : " + LOOK_UP_NAME);
            }
            try {
                eSvc = (EpgService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), LOOK_UP_NAME);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getEpgService]not bound - " + EpgService.IXC_NAME);
                }
            }
        }
        return eSvc;
    }
    public VideoController getVideoController() {
        if (vCnt == null) {
            EpgService svc = getEpgService();
            if (svc != null) {
                try{
                    vCnt = svc.getVideoController();
                    if (vCnt != null) {
                        vImpl = new VideoResizeListenerImpl();
                        vCnt.addVideoResizeListener(vImpl);
                    }
                }catch(Exception e) {
                    vCnt = null;
                    e.printStackTrace();
                }
            }
        }
        return vCnt;
    }
    public boolean requestResizeScreen(int x, int y, int w, int h) {
        boolean result = false;
        VideoController ctl = getVideoController();
        if (ctl != null) {
            try {
                ctl.resize(x, y, w, h, VideoController.SHOW);
                result = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean requestChangeCurrentChannel() {
        boolean result = false;
        try{
            EpgService svc = getEpgService();
            if (svc != null) {
                TvChannel tvChannel = svc.getCurrentChannel();
                if (tvChannel != null) {
                    svc.getChannelContext(0).changeChannel(tvChannel);
                }
            }
            result = true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    /*********************************************************************************
     * LoadingAnimationService-related
     *********************************************************************************/
    public LoadingAnimationService getLoadingAnimationService() {
        if (lSvc == null) {
            final String LOOK_UP_NAME = "/1/2075/" + LoadingAnimationService.IXC_NAME;
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommMgr.getLoadingAnimationService]Loading animation service look up name : " + LOOK_UP_NAME);
            }
            try {
                lSvc = (LoadingAnimationService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), LOOK_UP_NAME);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getLoadingAnimationService]not bound - " + LoadingAnimationService.IXC_NAME);
                }
            }
        }
        return lSvc;
    }
    public boolean requestShowLoadingAnimation() {
        if (isLoadingAnimationService) {
            return true;
        }
        boolean result = false;
        LoadingAnimationService svc = getLoadingAnimationService();
        if (svc != null) {
            try{
                svc.showLoadingAnimation(new Point(480, 270));
                isLoadingAnimationService = true;
                result = true;
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean requestHideLoadingAnimation() {
        LoadingAnimationService svc = getLoadingAnimationService();
        boolean result = false;
        if (svc != null) {
            try{
                svc.hideLoadingAnimation();
                isLoadingAnimationService = false;
                result = true;
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean isLoadingAnimationService() {
        return isLoadingAnimationService;
    }
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private void initStcService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.initStcService]start");
        }
        final long LOOKUP_RETRY_WAIT_MILLIS = 2000L;
        final int LOOKUP_MAX_RETRY_COUNT = 100;
        final String LOOK_UP_NAME = "/1/2100/" + StcService.IXC_NAME;
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.initStcService]Stc service look up name : " + LOOK_UP_NAME);
        }
        llclImpl = new LogLevelChangeListenerImpl();
        try {
            new Thread() {
                public void run() {
                    int nThTry = 0;
                    while (nThTry++ < LOOKUP_MAX_RETRY_COUNT) {
                        if (forcedStop) {
                            break;
                        }
                        if (stcSvc == null) {
                            try {
                                stcSvc = (StcService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), LOOK_UP_NAME);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.initStcService]not bound - " + StcService.IXC_NAME);
                                }
                            }
                        }
                        if (stcSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupStcService]looked up.");
                            }
                            try {
                                int currentLevel = stcSvc.registerApp(Rs.APP_NAME);
                                Log.printDebug("stc log level = " + currentLevel);
                                Log.setStcLevel(currentLevel);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                            if (o != null) {
                                DataCenter.getInstance().put("LogCache", o);
                            }
                            try {
                                stcSvc.addLogLevelChangeListener(Rs.APP_NAME, llclImpl);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.initStcService]retry lookup.");
                        }
                        try {
                            Thread.sleep(LOOKUP_RETRY_WAIT_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void disposeStcService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.disposeStcService]start.");
        }
        if (stcSvc != null) {
            try{
                stcSvc.removeLogLevelChangeListener(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
            stcSvc = null;
        }
        llclImpl = null;
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.disposeStcService]end.");
        }
    }
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(String errorCode) {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.showErrorMessage(errorCode, null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /*********************************************************************************
     * Vbm Service-related
     *********************************************************************************/
    private void lookupVbmService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupVbmService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (vSvc == null) {
                            try {
                                String lookupName = "/1/2085/" + VbmService.IXC_NAME;
                                vSvc = (VbmService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupVbmService]not bound - " + VbmService.IXC_NAME);
                                }
                            }
                        }
                        if (vSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupVbmService]looked up.");
                            }
                            LotteryVbmController.getInstance().init(vSvc);
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupVbmService]retry lookup.");
                        }
                        try {
                            Thread.sleep(2000L);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.videotron.tvi.illico.itv.lottery.comp;


public interface FavoriteLotteryPanelListener {
    void lostFocusFavoriteLotteryPanel();
    void selectedFavoriteLottery(int bannerType, String prodNum);
}

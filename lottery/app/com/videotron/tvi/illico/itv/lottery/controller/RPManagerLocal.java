package com.videotron.tvi.illico.itv.lottery.controller;

import java.io.File;
import java.util.Hashtable;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.data.LotteryListHandler;
import com.videotron.tvi.illico.log.Log;

public class RPManagerLocal extends RPManager {
    public void initSubManager() {
    }
    public void disposeSubManager() {
    }
    public Lottery[] getLotteryList() throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryList]start");
        }
        threadSleep(500);
        DefaultHandler handler = new LotteryListHandler();
        String filePath = "resource/test_data/server_data/lotteryList.xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryList]Request file path : ["+filePath+"]");
        }
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getLotteryList]Source file is null. return null.");
            }
            return null;
        }
        Lottery[] lotList = null;
        try{
            lotList = (Lottery[])getObejctFromFileByParser(file, handler);
        }catch(Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            e.printStackTrace();
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryList]Response lottery list : "+lotList);
        }
        
        if (lotList==null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        
        long curMillis=System.currentTimeMillis();
        /** Check lottery list whether valid or not **/
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryList]lotList.length : "+lotList.length);
        }
        
//        for (int i=0; i<lotList.length; i++){
//            if (lotList[i]==null || !lotList[i].isSpecificDurationLottery()) {
//                continue;
//            }
//            Date eDate=lotList[i].getEndDate();
//            if (eDate==null || eDate.getTime()<curMillis) {
//                lotList[i].setInvalidLottery(true);
//                continue;
//            }
//            Date bDate=lotList[i].getBeginDate();
//            if (bDate==null || bDate.getTime()>curMillis) {
//                lotList[i].setInvalidLottery(true);
//                continue;
//            }
//        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryList]Response lottery list : "+lotList);
        }
        return lotList;
    }
    public byte[] getSummaryResult(String reqLangTag, String reqLotProdNo, String reqDate) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getSummaryResult]start");
            Log.printDebug("[RPMgrLocal.getSummaryResult]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrLocal.getSummaryResult]Param - request lottery product number : ["+reqLotProdNo+"]");
            Log.printDebug("[RPMgrLocal.getSummaryResult]Param - request drawing date : ["+reqDate+"]");
        }
        if (reqLangTag == null || reqLotProdNo == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getSummaryResult]Request parameter is invalid. return null.");
            }
            return null;
        }
        threadSleep(200);
        String filePath = "resource/test_data/server_data/summary_" + reqLangTag + "_" + reqLotProdNo + "_latest.gif";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getSummaryResult]Request file path : ["+filePath+"]");
        }
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getSummaryResult]Source file is null. return null.");
            }
            return null;
        }
        byte[] result = null;
        try{
            result = getBytesFromFile(file);
        }catch(Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    e.printStackTrace();
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getSummaryResult]End : "+result);
        }
        return result;
    }
    public byte[] getDetailedResult(String reqLangTag, String reqLotProdNum, String reqDate) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getDetailedResult]start");
            Log.printDebug("[RPMgrLocal.getDetailedResult]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrLocal.getDetailedResult]Param - request lottery product number : ["+reqLotProdNum+"]");
            Log.printDebug("[RPMgrLocal.getDetailedResult]Param - request drawing date : ["+reqDate+"]");
        }
        if (reqLangTag == null || reqLotProdNum == null || reqDate == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getDetailedResult]Request parameter is invalid. return null.");
            }
            return null;
        }
        threadSleep(500);
        String filePath = "resource/test_data/server_data/previous_" + reqLangTag + "_" + reqLotProdNum + "_" + reqDate + ".gif";
//        String filePath = "resource/test_data/server_data/previous_en_38_2011-03-25.gif";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getDetailedResult]Request file path : ["+filePath+"]");
        }
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getDetailedResult]Source file is null. return null.");
            }
            return null;
        }
        byte[] result = null;
        try{
            result = getBytesFromFile(file);
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }                
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getDetailedResult]End : "+result);
        }
        return result;
    }
    public byte[] getLotteryIcon(String reqLotProdNum) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryIcon]start");
            Log.printDebug("[RPMgrLocal.getLotteryIcon]Param - request lottery product number : ["+reqLotProdNum+"]");
        }
        if (reqLotProdNum == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getLotteryIcon]Request parameter is invalid. return null.");
            }
            return null;
        }
        threadSleep(500);
        String filePath = "resource/test_data/server_data/icons_" + reqLotProdNum + ".gif";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryIcon]Request file path : ["+filePath+"]");
        }
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getLotteryIcon]Source file is null. return null.");
            }
            return null;
        }
        byte[] result = getBytesFromFile(file);
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getLotteryIcon]End : "+result);
        }
        return result;
    }
    public Hashtable getPromotionalBanner(String reqLangTag, String reqBannerType) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getPromotionalBanner]start");
            Log.printDebug("[RPMgrLocal.getPromotionalBanner]Param - request language type : [" + reqLangTag + "]");
            Log.printDebug("[RPMgrLocal.getPromotionalBanner]Param - request banner type : [" + reqBannerType + "]");
        }
        if (reqLangTag == null || reqBannerType == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getPromotionalBanner]Request parameter is invalid. return null.");
            }
            return null;
        }
        threadSleep(500);
        String filePath = "resource/test_data/server_data/banners_" + reqBannerType + "_" + reqLangTag + ".zip";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getPromotionalBanner]Request file path : [" + filePath + "]");
        }
        File file = new File(filePath);
        if (file == null || !file.isFile()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getPromotionalBanner]Source file is null. return null.");
            }
            return null;
        }
        byte[] result = null;
        try{
            result = getBytesFromFile(file);
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }                
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (result == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrLocal.getPromotionalBanner]Promotional banner data is null. return null.");
            }
            return null;
        }
        Hashtable zipFileHash = ZipFileReader.read(result);
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrLocal.getPromotionalBanner]End : " + zipFileHash);
        }
        return zipFileHash;
    }
    private void threadSleep(long longSleep) {
        try{
            Thread.sleep(longSleep);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}

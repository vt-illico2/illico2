package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryPreferenceUI;

public class LotteryPreferenceRenderer extends BaseRenderer {
//    private Image imgShadowT;
    private Image imgItemLine;
    private Image imgBoxArrowFocus;
    
    protected void prepareChild(){
//        imgShadowT = ImageManager.getInstance().getImage("11_top_sh.png", ImageManager.TYPE_TEMPORARY);
        imgItemLine = ImageManager.getInstance().getImage("11_sc_line.png", ImageManager.TYPE_TEMPORARY);
        imgBoxArrowFocus = ImageManager.getInstance().getImage("focus_05_05.png", ImageManager.TYPE_TEMPORARY);
    }
    
    protected void paintRenderer(Graphics g, UIComponent c) {
//        if (imgShadowT != null) {
//            g.drawImage(imgShadowT, 0, 77, c);
//        }
        // Text
        String txtPreferences = DataCenter.getInstance().getString("TxtLottery.Preferences");
        if (txtPreferences != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtPreferences, 55, 113);
        }
        String txtFavoriteLotteries = DataCenter.getInstance().getString("TxtLottery.Favorite_Lotteries");
        if (txtFavoriteLotteries != null) {
            g.setFont(Rs.F17);
            g.setColor(Rs.C210210210);
            g.drawString(txtFavoriteLotteries, 56, 147);
        }
        LotteryPreferenceUI scene = (LotteryPreferenceUI)c;
        // Favorite lottery list
        Lottery[] favLots = scene.getFavoriteLotteries();
        int favLotsCount = 0;
        if (favLots != null) {
            favLotsCount = favLots.length;
            g.setFont(Rs.F18);
            g.setColor(Rs.C160160160);
            for (int i=0; i<favLotsCount; i++) {
                if (favLots[i] == null) {
                    continue;
                }
                String favLotName = favLots[i].getLotteryProductName();
                if (favLotName == null) {
                    favLotName = "";
                }
                g.drawString((i+1)+". "+favLotName, 352, 245 + (i * 35) - 55);
                if (i != favLotsCount-1) {
                    g.drawImage(imgItemLine, 344, 257 + (i * 35) - 55, c);
                }
            }
        }
        g.setFont(Rs.F18);
        g.setColor(Rs.C035035035);
        if (imgBoxArrowFocus != null) {
            g.drawImage(imgBoxArrowFocus, 339, 182 - 55, c);
        }
        String txtFavourites = DataCenter.getInstance().getString("TxtLottery.Favourites");
        if (txtFavourites != null) {
            g.drawString(favLotsCount + " " + txtFavourites, 352, 203 - 55);
        }
    }
}

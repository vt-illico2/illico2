package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;

abstract class BaseRenderer extends Renderer {
    private Image imgMainBG;
    private Image imgHotKeyMainBG;
    private Image imgSubBG;
    private Image imgHotKeySubBG;
    
    private Color c50 = new Color(50, 50, 50);
    private Color c70 = new Color(70, 70, 70);
    
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    public void prepare(UIComponent c) {
        imgMainBG = ImageManager.getInstance().getImage("07_bg_main.jpg", ImageManager.TYPE_TEMPORARY);
        imgHotKeyMainBG = ImageManager.getInstance().getImage("01_hotkeybg_main.png", ImageManager.TYPE_TEMPORARY);
        imgSubBG = ImageManager.getInstance().getImage("bg.jpg", ImageManager.TYPE_TEMPORARY);
//        imgHotKeySubBG = ImageManager.getInstance().getImage("01_hotkeybg.png", ImageManager.TYPE_TEMPORARY);
        prepareChild();
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    
    abstract protected void prepareChild();
    
    protected void paint(Graphics g, UIComponent c) {
        paintBG(g, c);
        paintRenderer(g, c);
        paintCommon(g, c);
    }
    private void paintBG(Graphics g, UIComponent c) {
        int sceneId = SceneManager.getInstance().getCurrentSceneId();
        if (sceneId == SceneTemplate.SCENE_ID_LOTTERY_MAIN) {
            if (imgMainBG != null) {
                g.drawImage(imgMainBG, 0, 0, c);
            }
        } else {
            if (imgSubBG != null) {
                g.drawImage(imgSubBG, 0, 0, c);
            }
        }
    }
    private void paintCommon(Graphics g, UIComponent c) {
        int sceneId = SceneManager.getInstance().getCurrentSceneId();
        if (sceneId == SceneTemplate.SCENE_ID_LOTTERY_MAIN) {
            if (imgHotKeyMainBG != null) {
                g.drawImage(imgHotKeyMainBG, 405, 466, 513, 17, c);
            }
            
            g.setColor(c70);
			g.fillRect(373, 477, 587, 1);
        } else {
//            if (imgHotKeySubBG != null) {
//                g.drawImage(imgHotKeySubBG, 236, 466, 683, 17, c);
//            }
        	g.setColor(c50);
        	g.fillRect(0, 483, 960, 1);
        }
    }
    abstract void paintRenderer(Graphics g, UIComponent c);
}

package com.videotron.tvi.illico.itv.lottery.controller;

import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;
import org.dvb.event.UserEvent;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.ui.BaseUI;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryDetailedResultUI;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryMainUI;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryPreferenceFavoriteLotteryUI;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryPreferenceUI;
import com.videotron.tvi.illico.itv.lottery.ui.LotterySummaryResultsUI;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

public class SceneManager {
    private static SceneManager instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lUILottery;
    private LayeredWindow lWinLottery;
    private LayeredKeyWizard lKeyLottery;

    private Hashtable scenes;
    private BaseUI curScene;
    private int curSceneId;
    private String[] curSceneParams;

    private int prevSceneId;
    private String[] prevSceneParams;

    private SceneManager(){
    }

    public static synchronized SceneManager getInstance() {
        if (instance == null) {
            instance = new SceneManager();
        }
        return instance;
    }

    public void init() {
        if (lWinLottery == null) {
            lWinLottery = new LayeredWindowWizard();
            lWinLottery.setVisible(true);
        }
        if (lKeyLottery == null) {
            lKeyLottery = new LayeredKeyWizard();
        }
        lUILottery = WindowProperty.LOTTERY.createLayeredDialog(lWinLottery, lWinLottery.getBounds(), lKeyLottery);
        lUILottery.deactivate();
        if (scenes == null) {
            scenes = new Hashtable();
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
    }
    public void dispose() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                BaseUI scene = (BaseUI)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.dispose();
                scene = null;
            }
            scenes = null;
        }
        if (lUILottery != null) {
            lUILottery.deactivate();
            WindowProperty.dispose(lUILottery);
            lUILottery=null;
        }
    }
    public void start() {
        if (lUILottery != null) {
            lUILottery.activate();
        }
        LotteryVbmController.getInstance().writeLotteryAppStart();
        goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_MAIN, true, null);
    }
    public void stop() {
        if (scenes != null) {
            Enumeration enu = scenes.keys();
            while (enu.hasMoreElements()) {
                Integer key = (Integer)enu.nextElement();
                if (key == null) {
                    continue;
                }
                BaseUI scene = (BaseUI)scenes.get(key);
                if (scene == null) {
                    continue;
                }
                scene.stop();
            }
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinLottery.remove(curScene);
            curScene = null;
        }
        curSceneId = SceneTemplate.SCENE_ID_INVALID;
        if (lUILottery != null) {
            lUILottery.deactivate();
        }
        LotteryVbmController.getInstance().writeLotteryAppExit();
    }
    public void goToNextScene(int reqSceneId, boolean isReset, String[] params) {
        if (curSceneId == reqSceneId) {
            return;
        }
        //Stop before scene
        if (curScene != null) {
            curScene.stop();
            lWinLottery.remove(curScene);
        }
        //Start current scene
        BaseUI scene = getScene(reqSceneId);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyWizard.handleKeyEvent]scene : "+scene);
        }
        if (scene != null) {
            scene.start(isReset, params);
            lWinLottery.add(scene);
            prevSceneId = curSceneId;
            prevSceneParams = curSceneParams;
            curSceneId = reqSceneId;
            curSceneParams = params;
            curScene = scene;
        }
        //Repaint();
        lWinLottery.repaint();
    }
    public void goToPreviousScene() {
        goToNextScene(prevSceneId, false, prevSceneParams);
    }
    private BaseUI getScene(int reqSceneId) {
        BaseUI res = (BaseUI)scenes.get(new Integer(reqSceneId));
        //Check Scene
        if (res == null) {
            res = createScene(reqSceneId);
            if (res != null) {
                scenes.put(new Integer(reqSceneId), res);
            }
        }
        return res;
    }
    private BaseUI createScene(int reqSceneId) {
        BaseUI scene = null;
        switch(reqSceneId) {
            case SceneTemplate.SCENE_ID_LOTTERY_MAIN:
                scene = new LotteryMainUI();
                break;
            case SceneTemplate.SCENE_ID_LOTTERY_SUMMARY_RESULTS:
                scene = new LotterySummaryResultsUI();
                break;
            case SceneTemplate.SCENE_ID_LOTTERY_DETAILED_RESULTS:
                scene = new LotteryDetailedResultUI();
                break;
            case SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE:
                scene = new LotteryPreferenceUI();
                break;
            case SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE_FAVORITE_LOTTERY:
                scene = new LotteryPreferenceFavoriteLotteryUI();
                break;
        }
        if (scene != null) {
            scene.init();
        }
        return scene;
    }

    public int getCurrentSceneId() {
        return curSceneId;
    }

    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowWizard extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowWizard() {
            setBounds(Rs.SCENE_BOUND);
        }
        public void notifyShadowed() {
        }
    }
    class LayeredKeyWizard implements LayeredKeyHandler {
        public boolean handleKeyEvent(UserEvent userEvent) {
            if (userEvent == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredKeyWizard.handleKeyEvent]User Event is null.");
                }
                return false;
            }
            if (curScene == null) {
                return false;
            }
            //Block PVR-related keys at the scene without sacled video.
            int keyCode = userEvent.getCode();
            switch(curSceneId) {
                case SceneTemplate.SCENE_ID_LOTTERY_SUMMARY_RESULTS:
                case SceneTemplate.SCENE_ID_LOTTERY_DETAILED_RESULTS:
                case SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE:
                case SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE_FAVORITE_LOTTERY:
                    switch(keyCode) {
                        case KeyCodes.REWIND:
                        case KeyCodes.FAST_FWD:
                        case KeyCodes.PLAY:
                        case KeyCodes.PAUSE:
                        case KeyCodes.STOP:
                        case KeyCodes.RECORD:
                        case KeyCodes.LIVE:
                            return true;
                    }
                    break;
            }
            int keyType = userEvent.getType();
            if (keyType != KeyEvent.KEY_PRESSED) {
                return false;
            }
            return curScene.handleKey(userEvent.getCode());
        }
    }
}

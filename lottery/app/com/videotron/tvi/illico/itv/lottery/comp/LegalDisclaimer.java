package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.util.TextUtil;

public class LegalDisclaimer extends Component {
    private static final long serialVersionUID = -271138887086349168L;
    private Image imgProviderLogo;
    private String txtKeyLegalDisclaimer;
    private String[] descLegalDisclaimer;
    
    public void init() {
        if (imgProviderLogo == null) {
            imgProviderLogo = ImageManager.getInstance().getImage("app_logo_s2.png", ImageManager.TYPE_TEMPORARY);
        }
    }
    public void dispose() {
        descLegalDisclaimer = null;
        txtKeyLegalDisclaimer = null;
        imgProviderLogo = null;
    }
    public void start() {
        setDescriptionLegalDisclaimer();
    }
    public void stop() {
        descLegalDisclaimer = null;
    }
    public void paint(Graphics g) {
        final int x = 52;
        final int y = 482;
        if (imgProviderLogo != null) {
            g.drawImage(imgProviderLogo, 52 - x, 482 - y, this);
        }
        if (descLegalDisclaimer != null) {
            g.setFont(Rs.F15);
            g.setColor(Rs.C160160160);
            for (int i=0; i<descLegalDisclaimer.length; i++) {
                if (descLegalDisclaimer[i] != null) {
                    g.drawString(descLegalDisclaimer[i], 165 - x, 496 - y + (i*12));
                }
            }
        }
    }
    private void setDescriptionLegalDisclaimer() {
        if (txtKeyLegalDisclaimer == null) {
            descLegalDisclaimer = null;
            return;
        }
        String ld = DataCenter.getInstance().getString(txtKeyLegalDisclaimer);
        if (ld == null || ld.length() == 0) {
            descLegalDisclaimer = null;
            return;
        }
        descLegalDisclaimer = TextUtil.split(ld, Rs.FM15, 440, 2); 
    }
    /*********************************************************************************
     * LegalDisclaimer Attribute-related
     **********************************************************************************/
    public void setTextKeyLegalDisclaimer(String txtKeyLegalDisclaimer) {
        this.txtKeyLegalDisclaimer = txtKeyLegalDisclaimer;
    }
}

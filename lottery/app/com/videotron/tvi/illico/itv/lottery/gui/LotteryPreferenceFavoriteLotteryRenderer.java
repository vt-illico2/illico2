package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryPreferenceFavoriteLotteryUI;
import com.videotron.tvi.illico.log.Log;

public class LotteryPreferenceFavoriteLotteryRenderer extends BaseRenderer  {
//    private Image imgShadowT;
    private Image imgListTitleShadow;
    private Image imgListBgT;
    private Image imgListBgM;
//    private Image imgListBgShadow;
//    private Image imgListShadowT;
//    private Image imgListShadowB;
    private Image imgListLine;
//    private Image imgListEmptyShadowT;
//    private Image imgListEmptyShadowB;

    private Image imgArrowT;
    private Image imgArrowB;
    private Image imgListBasic;
    private Image imgListFocus;
    private Image imgButtonBasic;
    private Image imgButtonFocus;
    private Image imgButtonFocusDim;

    private Image[] imgButtonArrowBasic;
    private Image[] imgButtonArrowFocus;
    private Image imgIconStar;

    private final int[] xButtonArrow = {533, 419};
    private final int[] xButtonTxt = {480, 487};
    private final int listCount = 2;
    private final int listGap = 505;

    protected void prepareChild(){
//        imgShadowT = ImageManager.getInstance().getImage("08_top_sh.png", ImageManager.TYPE_TEMPORARY);
        imgListTitleShadow = ImageManager.getInstance().getImage("08_list_titlesh.png", ImageManager.TYPE_TEMPORARY);
        imgListBgT = ImageManager.getInstance().getImage("08_listbg_t.png", ImageManager.TYPE_TEMPORARY);
        imgListBgM = ImageManager.getInstance().getImage("08_listbg_m.png", ImageManager.TYPE_TEMPORARY);
//        imgListBgShadow = ImageManager.getInstance().getImage("08_listshadow.png", ImageManager.TYPE_TEMPORARY);
//        imgListShadowT = ImageManager.getInstance().getImage("08_list_sh_t.png", ImageManager.TYPE_TEMPORARY);
//        imgListShadowB = ImageManager.getInstance().getImage("08_list_sh_b.png", ImageManager.TYPE_TEMPORARY);
        imgListLine = ImageManager.getInstance().getImage("08_listline.png", ImageManager.TYPE_TEMPORARY);
//        imgListEmptyShadowT = ImageManager.getInstance().getImage("11_listglow_t.png", ImageManager.TYPE_TEMPORARY);
//        imgListEmptyShadowB = ImageManager.getInstance().getImage("11_listglow_b.png", ImageManager.TYPE_TEMPORARY);
        imgArrowT = ImageManager.getInstance().getImage("02_ars_t.png", ImageManager.TYPE_TEMPORARY);
        imgArrowB = ImageManager.getInstance().getImage("02_ars_b.png", ImageManager.TYPE_TEMPORARY);
        imgListBasic = ImageManager.getInstance().getImage("08_list_foc_dim.png", ImageManager.TYPE_TEMPORARY);
        imgListFocus = ImageManager.getInstance().getImage("08_list_foc.png", ImageManager.TYPE_TEMPORARY);
        imgButtonBasic = ImageManager.getInstance().getImage("08_bt_off_dim.png", ImageManager.TYPE_TEMPORARY);
        imgButtonFocus = ImageManager.getInstance().getImage("05_focus.png", ImageManager.TYPE_TEMPORARY);
        imgButtonFocusDim = ImageManager.getInstance().getImage("05_focus_dim.png", ImageManager.TYPE_TEMPORARY);
        imgButtonArrowBasic = new Image[] {
                ImageManager.getInstance().getImage("08_add_ar_r_dim.png", ImageManager.TYPE_TEMPORARY),
                ImageManager.getInstance().getImage("08_add_ar_l_dim.png", ImageManager.TYPE_TEMPORARY)
        };
        imgButtonArrowFocus = new Image[] {
                ImageManager.getInstance().getImage("08_add_ar_r.png", ImageManager.TYPE_TEMPORARY),
                ImageManager.getInstance().getImage("08_add_ar_l.png", ImageManager.TYPE_TEMPORARY)
        };
        imgIconStar = ImageManager.getInstance().getImage("08_sc_icon_fav.png", ImageManager.TYPE_TEMPORARY);
    }

    protected void paintRenderer(Graphics g, UIComponent c) {
        // Shadow
//        g.drawImage(imgShadowT, 0, 77, c);
        // Title
        String txtTitle = DataCenter.getInstance().getString("TxtLottery.Modify_Your_Favourite_Lotteries");
        if (txtTitle != null) {
            g.setFont(Rs.F26);
            g.setColor(Rs.C255255255);
            g.drawString(txtTitle, 55, 113);
        }
        // List Background
        for (int i=0; i<listCount; i++) {
//            g.drawImage(imgListBgShadow, 7 + (i * listGap), 455, c);
            g.drawImage(imgListBgM, 54 + (i * listGap), 304, 347, 160, c);
            g.drawImage(imgListBgT, 54 + (i * listGap), 136, c);
        }

        LotteryPreferenceFavoriteLotteryUI scene = (LotteryPreferenceFavoriteLotteryUI)c;
        int curArea = scene.getCurrentArea();
        // Button area
        g.setFont(Rs.F18);
        String[] txtButtons = new String[LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_COUNT];
        txtButtons[LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_ADD] = DataCenter.getInstance().getString("TxtLottery.Add");
        txtButtons[LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_REMOVE] = DataCenter.getInstance().getString("TxtLottery.Remove");
        int curButtonType = scene.getCurrentButtonType();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getFavoriteLotteries]curArea : "+curArea);
        }
        for (int i=0; i<LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_COUNT; i++) {
            boolean isFocusButton = false;
            boolean isFocusButtonArrow = false;
            switch(curArea) {
                case LotteryPreferenceFavoriteLotteryUI.AREA_AVAILABLE:
                    isFocusButton = false;
                    if (i == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_ADD) {
                        isFocusButtonArrow = true;
                    } else {
                        isFocusButtonArrow = false;
                    }
                    break;
                case LotteryPreferenceFavoriteLotteryUI.AREA_BUTTON:
                    if (i == curButtonType) {
                        isFocusButton = true;
                        isFocusButtonArrow = true;
                    } else {
                        isFocusButton = false;
                        isFocusButtonArrow = false;
                    }
                    break;
                case LotteryPreferenceFavoriteLotteryUI.AREA_FAVOURITE:
                    isFocusButton = false;
                    if (i == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_REMOVE) {
                        isFocusButtonArrow = true;
                    } else {
                        isFocusButtonArrow = false;
                    }
                    break;
            }
            if (isFocusButton) {
                g.drawImage(imgButtonFocus, 410, 268 + (i *43), c);
            } else {
                if (isFocusButtonArrow) {
                    g.drawImage(imgButtonFocusDim, 410, 268 + (i *43), c);
                } else {
                    g.drawImage(imgButtonBasic, 410, 268 + (i *43), c);
                }
            }
            if (txtButtons[i] == null) {
                continue;
            }
            int txtButtonWth = Rs.FM18.stringWidth(txtButtons[i]);
            if (isFocusButtonArrow) {
                g.drawImage(imgButtonArrowFocus[i], xButtonArrow[i], 277 + (i *43), c);
                g.setColor(Rs.C003003003);
                g.drawString(txtButtons[i], xButtonTxt[i] - (txtButtonWth/2), 289 + (i *43));
            } else {
                g.drawImage(imgButtonArrowBasic[i], xButtonArrow[i], 277 + (i *43), c);
                g.setColor(Rs.C110110110);
                int xPos = xButtonTxt[i] - (txtButtonWth/2);
                g.drawString(txtButtons[i], xPos, 289 + (i * 43));
                g.setColor(Rs.C056056056);
                g.drawString(txtButtons[i], xPos -1, 289 + (i * 43) - 1);
            }
        }
        // Available lotteries
        Vector availLotVec = scene.getAvailableLotteryVector();
        int curAvailIdx = scene.getCurrentAvailableIndex();
        int availLotVecSz = 0;
        if (availLotVec != null) {
            availLotVecSz = availLotVec.size();
            int startIdx = -1;
            int dispCnt = -1;
            if (availLotVecSz <= LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT) {
                startIdx = 0;
                dispCnt = availLotVecSz;
            } else {
                int halfDispCnt = LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT /2;
                if (curAvailIdx < halfDispCnt) {
                    startIdx = 0;
                } else if (curAvailIdx >= availLotVecSz - halfDispCnt) {
                    startIdx = availLotVecSz - LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT;
                } else {
                    startIdx = curAvailIdx - halfDispCnt;
                }
                dispCnt = LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT;
            }
            g.setFont(Rs.F18);
            for (int i=0; i<dispCnt; i++) {
                int idx = startIdx + i;
                if (i != dispCnt-1) {
                    g.drawImage(imgListLine, 69, 207 + (i * 32), c);
                }
                if (idx == curAvailIdx) {
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_AVAILABLE) {
                        g.drawImage(imgListFocus, 52, 172 + (i * 32), c);
                        g.setColor(Rs.C003003003);
                    } else if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_BUTTON
                            && curButtonType == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_ADD) {
                        g.drawImage(imgListBasic, 52, 172 + (i * 32), c);
                        g.setColor(Rs.C003003003);
                    } else {
                        g.setColor(Rs.C224224224);
                    }
                } else {
                    g.setColor(Rs.C224224224);
                }
                Lottery lot = (Lottery)availLotVec.elementAt(idx);
                String lotName = null;
                if (lot != null) {
                    lotName = lot.getLotteryProductName();
                }
                if (lotName != null) {
//                    g.drawString(lotName, 75, 197 + (i * 32));
                }
            }
            // Arrow
            if (availLotVecSz > LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT) {
                if (startIdx != 0) {
//                    g.drawImage(imgListShadowT, 55, 172, c);
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_AVAILABLE) {
                        g.drawImage(imgArrowT, 216, 164, c);
                    }
                }
                if (startIdx + dispCnt < availLotVecSz) {
//                    g.drawImage(imgListShadowB, 54, 404, c);
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_AVAILABLE) {
                        g.drawImage(imgArrowB, 216, 456, c);
                    }
                }
            }
        }
        // Available lotteries title
        String txtAvailableLotteries = DataCenter.getInstance().getString("TxtLottery.Available_Lotteries");
        if (txtAvailableLotteries != null) {
            txtAvailableLotteries = txtAvailableLotteries + " ("+availLotVecSz+")";
            g.setFont(Rs.F20);
            g.setColor(Rs.C046046046);
            g.drawString(txtAvailableLotteries, 68, 161);
            g.setColor(Rs.C214182055);
            g.drawString(txtAvailableLotteries, 67, 160);
        }
        boolean isChangeOrderMode = scene.isChangeOrderMode();
        // Favorite lotteries
        Vector favLotVec = scene.getFavoriteLotteryVector();
        int curFavIdx = scene.getCurrentFavoriteIndex();
        int favLotVecSz = 0;
        if (favLotVec != null) {
            g.setFont(Rs.F18);
            favLotVecSz = favLotVec.size();
            for (int i=0; i<favLotVecSz; i++) {
            	if (i != favLotVecSz - 1) {
                    g.drawImage(imgListLine, 574, 207 + (i * 32), c);
                }
                String lotName = null;
                Lottery lot = (Lottery)favLotVec.elementAt(i);
                if (lot != null) {
                    lotName = lot.getLotteryProductName();
                }
                if (i == curFavIdx) {
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_FAVOURITE) {
                        g.drawImage(imgListFocus, 557, 172 + (i * 32), c);
                        g.setColor(Rs.C003003003);
                    } else if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_BUTTON
                            && curButtonType == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_REMOVE) {
                        g.drawImage(imgListBasic, 558, 172 + (i * 32), c);
                        g.setColor(Rs.C003003003);
                    } else {
                        g.setColor(Rs.C224224224);
                    }
                } else {
                    g.setColor(Rs.C224224224);
                }
                if (lotName != null) {
//                    g.drawString(lotName, 580, 197 + (i * 32));
                }
            }
        }
        String txtEmpty = DataCenter.getInstance().getString("TxtLottery.Empty");
        int emptyCount = DataManager.FAVORITE_LOTTERY_COUNT - favLotVecSz;
        for (int i=0; i<emptyCount; i++) {
            int idx = favLotVecSz + i;
            g.setColor(Rs.C086086086);
            g.drawString(txtEmpty, 580, 197 + (idx * 32));
            if (i != emptyCount - 1) {
                g.drawImage(imgListLine, 574, 207 + (idx * 32), c);
            }
        }
        if (emptyCount == DataManager.FAVORITE_LOTTERY_COUNT) {
            g.setColor(Rs.DVB007007007077);
            g.fillRect(559, 172, 347, 164);
//            g.drawImage(imgListEmptyShadowT, 559, 136, c);
//            g.drawImage(imgListEmptyShadowB, 560, 336, c);
        } else if (emptyCount > 0) {
            g.setColor(Rs.DVB007007007077);
            g.fillRect(559, 176 + (favLotVecSz * 32), 347, (emptyCount * 32));
//            g.drawImage(imgListEmptyShadowT, 560, 169 + (favLotVecSz * 32), c);
//            g.drawImage(imgListEmptyShadowB, 560, 336, c);
        }
        for (int i=0; i<listCount; i++) {
            g.drawImage(imgListTitleShadow, 54 + (i * listGap), 168, c);
        }
        if (isChangeOrderMode && curArea == LotteryPreferenceFavoriteLotteryUI.AREA_FAVOURITE) {
            g.drawImage(imgArrowT, 721, 158 + (curFavIdx * 32), c);
            g.drawImage(imgArrowB, 721, 207 + (curFavIdx * 32), c);
        }
        //Favorite lotteries title
        if (imgIconStar != null) {
            g.drawImage(imgIconStar, 565, 143, c);
        }
        String txtFavoriteLotteries = DataCenter.getInstance().getString("TxtLottery.Favorite_Lotteries");
        if (txtFavoriteLotteries != null) {
            txtFavoriteLotteries = txtFavoriteLotteries + " ("+favLotVecSz+"/"+DataManager.FAVORITE_LOTTERY_COUNT+")";
            g.setFont(Rs.F20);
            g.setColor(Rs.C046046046);
            g.drawString(txtFavoriteLotteries, 601, 161);
            g.setColor(Rs.C214182055);
            g.drawString(txtFavoriteLotteries, 600, 160);
        }
    }
}

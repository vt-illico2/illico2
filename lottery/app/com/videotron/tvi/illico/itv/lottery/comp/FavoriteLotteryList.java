package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.util.Vector;

import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryPreferenceFavoriteLotteryUI;

public class FavoriteLotteryList extends Component implements AnimationRequestor {
    private static final long serialVersionUID = -624611792882523308L;
    private LotteryPreferenceFavoriteLotteryUI lotPrefFavKLotUI;
    public FavoriteLotteryList(LotteryPreferenceFavoriteLotteryUI lotPrefFavKLotUI) {
        this.lotPrefFavKLotUI = lotPrefFavKLotUI;
    }
    
    public void paint(Graphics g) {
        int curArea = lotPrefFavKLotUI.getCurrentArea();
        int curButtonType = lotPrefFavKLotUI.getCurrentButtonType();
        // Available lotteries
        Vector availLotVec = lotPrefFavKLotUI.getAvailableLotteryVector();
        int curAvailIdx = lotPrefFavKLotUI.getCurrentAvailableIndex();
        int availLotVecSz = 0;
        if (availLotVec != null) {
            availLotVecSz = availLotVec.size();
            int startIdx = -1;
            int dispCnt = -1;
            if (availLotVecSz <= LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT) {
                startIdx = 0;
                dispCnt = availLotVecSz;
            } else {
                int halfDispCnt = LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT /2;
                if (curAvailIdx < halfDispCnt) {
                    startIdx = 0;
                } else if (curAvailIdx >= availLotVecSz - halfDispCnt) {
                    startIdx = availLotVecSz - LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT;
                } else {
                    startIdx = curAvailIdx - halfDispCnt;
                }
                dispCnt = LotteryPreferenceFavoriteLotteryUI.PAGE_COUNT;
            }
            g.setFont(Rs.F18);
            for (int i=0; i<dispCnt; i++) {
                int idx = startIdx + i;
                if (idx == curAvailIdx) {
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_AVAILABLE) {
                        g.setColor(Rs.C003003003);
                    } else if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_BUTTON
                            && curButtonType == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_ADD) {
                        g.setColor(Rs.C003003003);
                    } else {
                        g.setColor(Rs.C224224224);                        
                    }
                } else {
                    g.setColor(Rs.C224224224);                        
                }
                Lottery lot = (Lottery)availLotVec.elementAt(idx);
                String lotName = null;
                if (lot != null) {
                    lotName = lot.getLotteryProductName();
                }
                if (lotName != null) {
                    g.drawString(lotName, 75, 197 + (i * 32));
                }
            }
        }
        // Favorite lotteries
        Vector favLotVec = lotPrefFavKLotUI.getFavoriteLotteryVector();
        int curFavIdx = lotPrefFavKLotUI.getCurrentFavoriteIndex();
        int favLotVecSz = 0;
        if (favLotVec != null) {
            g.setFont(Rs.F18);
            favLotVecSz = favLotVec.size();
            for (int i=0; i<favLotVecSz; i++) {
                String lotName = null;
                Lottery lot = (Lottery)favLotVec.elementAt(i);
                if (lot != null) {
                    lotName = lot.getLotteryProductName();
                }
                if (i == curFavIdx) {
                    if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_FAVOURITE) {
                        g.setColor(Rs.C003003003);
                    } else if (curArea == LotteryPreferenceFavoriteLotteryUI.AREA_BUTTON
                            && curButtonType == LotteryPreferenceFavoriteLotteryUI.BUTTON_TYPE_REMOVE) {
                        g.setColor(Rs.C003003003);
                    } else {
                        g.setColor(Rs.C224224224);
                    }
                } else {
                    g.setColor(Rs.C224224224);
                }
                if (lotName != null) {
                    g.drawString(lotName, 580, 197 + (i * 32));
                }
            }
        }
//        String txtEmpty = DataCenter.getInstance().getString("TxtLottery.Empty");
//        int emptyCount = DataManager.FAVORITE_LOTTERY_COUNT - favLotVecSz;
//        g.setColor(Rs.C086086086);
//        for (int i=0; i<emptyCount; i++) {
//            int idx = favLotVecSz + i;
//            g.drawString(txtEmpty, 580, 197 + (idx * 32));
//        }
    }

    public void animationEnded(Effect effect) {
        // TODO Auto-generated method stub
        
    }

    public void animationStarted(Effect effect) {
        // TODO Auto-generated method stub
        
    }

    public boolean skipAnimation(Effect effect) {
        // TODO Auto-generated method stub
        return false;
    }
}

package com.videotron.tvi.illico.itv.lottery.controller;

import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.log.Log;

public class LotteryVbmController extends VbmController {
    public static final long LOTTERY_APP_START = 1013000001L;
    public static final long LOTTERY_APP_EXIT = 1013000002L;
    public static final long LOTTERY_PARENT_APP = 1013000003L;
    
    private String sessionId;
    
    protected static LotteryVbmController instance = new LotteryVbmController();

    public static LotteryVbmController getInstance() {
        return instance;
    }
    
    protected LotteryVbmController() {
    }
    
    public void writeLotteryAppStart() {
        Log.printInfo("[LotteryVbmController.writeLotteryAppStart] start.");
        sessionId=Long.toString(System.currentTimeMillis());
        Log.printInfo("[LotteryVbmController.writeLotteryAppStart] Session ID : "+sessionId);
        String[] params = CommunicationManager.getInstance().requestGetParameter();
        Log.printInfo("[LotteryVbmController.writeLotteryAppStart] Parameters from monitor service : "+params);
        String parentAppName = "";
        if (params != null && params.length > 0) { parentAppName = params[0]; }
        Log.printInfo("[LotteryVbmController.writeLotteryAppStart] Parent app name : "+parentAppName);
        write(LOTTERY_PARENT_APP, DEF_MEASUREMENT_GROUP, parentAppName);
        write(LOTTERY_APP_START, DEF_MEASUREMENT_GROUP, sessionId);
        Log.printInfo("[LotteryVbmController.writeLotteryAppStart] end.");
    }
    
    public void writeLotteryAppExit() {
        Log.printInfo("[LotteryVbmController.writeLotteryAppExit] start.");
        Log.printInfo("[LotteryVbmController.writeLotteryAppExit] Session ID : "+sessionId);
        write(LOTTERY_APP_EXIT, DEF_MEASUREMENT_GROUP, sessionId);
        Log.printInfo("[LotteryVbmController.writeLotteryAppExit] end.");
    }
}

package com.videotron.tvi.illico.itv.lottery.controller;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Hashtable;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.comp.SAXHandler;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.log.Log;

abstract class RPManager {
    private SAXParser parser;
    public static final String BANNER_TYPE_FUTURE_PRODUCTS="produits";
    public static final String BANNER_TYPE_NEXT_DRAW="groslots";
    /*********************************************************************************
     * Life cycle-related
     *********************************************************************************/
    public void init() {
        if (parser == null) {
            try {
                parser = SAXParserFactory.newInstance().newSAXParser();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        initSubManager();
    }
    public void dispose(){
        disposeSubManager();
        parser = null;
    }
    abstract protected void initSubManager();
    abstract protected void disposeSubManager();
    /*********************************************************************************
     * Service-related
     *********************************************************************************/
    abstract Lottery[] getLotteryList() throws RPManagerException;
    abstract byte[] getSummaryResult(String reqLangTag, String reqLotProdNum, String reqDate) throws RPManagerException;
    abstract byte[] getDetailedResult(String reqLangTag, String reqLotProdNum, String reqDate) throws Exception;
//    abstract byte[] getLotteryIcon(String reqLotProdNum) throws Exception;
    abstract Hashtable getPromotionalBanner(String reqLangTag, String reqBannerType) throws RPManagerException;

    private Object read(InputStream in, DefaultHandler handler) throws RPManagerException{
        if (in == null || handler == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }  
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        try {
            parser.parse(in, handler);
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }              
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        Object obj = null;
        if (saxHandler != null) {
            obj = saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("RPMgr.getLotteryList]Response lottery list : "+obj);
        }
        return obj;
    }
    private byte[] read(InputStream in) throws RPManagerException{
        byte[] result = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(in);
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }                
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    protected Object getObejctFromUrlByParser(URL reqURL, DefaultHandler reqHandler) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getObejctFromURLByParser]Start.");
            Log.printDebug("[RPManager.getObejctFromURLByParser]Param - request URL : [" + reqURL + "]");
            Log.printDebug("[RPManager.getObejctFromURLByParser]Param - request Handler : [" + reqHandler + "]");
        }
        if (reqURL == null || reqHandler == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }  
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        Object result = null;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) reqURL.openConnection();
            // VDTRMASTER-5846
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes] reponseCode=" + code);
            }
            result = read(con.getInputStream(), reqHandler);
        } catch (IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Loto HTTP server.");
            }            
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        } catch (RPManagerException e) {
            throw e;
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    protected Object getObejctFromFileByParser(File reqFile, DefaultHandler reqHandler) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqFile + "]");
        }
        if (reqFile == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        Object result = null;
        try {
            result = read(new FileInputStream(reqFile), reqHandler);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    protected byte[] getBytesFromUrl(URL reqUrl) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqUrl + "]");
        }
        if (reqUrl == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        byte[] result = null;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) reqUrl.openConnection();
            // VDTRMASTER-5846
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes] reponseCode=" + code);
            }

            // VDTRMASTER-5858
            if (code == HttpURLConnection.HTTP_NOT_FOUND) {
                throw new FileNotFoundException(reqUrl.toString());
            }

            result = read(reqUrl.openStream());
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    protected byte[] getBytesFromFile(File reqFile) throws RPManagerException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqFile + "]");
        }
        if (reqFile == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        byte[] result = null;
        try {
            result = read(new FileInputStream(reqFile));
        } catch (IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }          
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        } catch (RPManagerException e) {
            throw e;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
}

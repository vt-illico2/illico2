package com.videotron.tvi.illico.itv.lottery.ui;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.util.Calendar;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.Breadcrumbs;
import com.videotron.tvi.illico.itv.lottery.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.gui.LotterySummaryResultsRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class LotterySummaryResultsUI extends BaseUI {
    private static final long serialVersionUID = -5003774379597597686L;
    private DataCenter dCenter = DataCenter.getInstance();
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    public static final int REQUEST_STATUS_LOADING = 0;
    public static final int REQUEST_STATUS_VALID = 1;
    public static final int REQUEST_STATUS_INVALID = 2;
    private int curReqStatus;
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Breadcrumbs bc;
    private Footer footer;
    private LegalDisclaimer legalDisclaimer;

    private String resMsg;

    public static final int PAGE_COUNT = 5;
    private Image[] imgSummaryReses;
    private Lottery[] sortedValidLotList;
    private int ttCnt;
    private int startIdx;
    private int dispCnt;
    private int curIdx;

    protected void initScene() {
        renderer = new LotterySummaryResultsRenderer();
        setRenderer(renderer);
        if (bc == null) {
            bc = new Breadcrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 487, 550, 30);
            legalDisclaimer.setTextKeyLegalDisclaimer("TxtLottery.Desc_Legal_Disclaimer");
            legalDisclaimer.setVisible(true);
            add(legalDisclaimer, 0);
        }
    }
    protected void disposeScene() {
        if (legalDisclaimer!= null) {
            remove(legalDisclaimer);
            legalDisclaimer.dispose();
            legalDisclaimer = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        renderer = null;
    }
    protected void startScene(boolean reset, String[] params) {
        CommunicationManager.getInstance().requestResizeScreen(0, 0, 0, 0);
        prepare();
        if (legalDisclaimer != null) {
            legalDisclaimer.start();
        }
        if (bc != null) {
            bc.start(new String[]{DataCenter.getInstance().getString("TxtLottery.Lottery")});
        }
        if (footer != null) {
            footer.reset();
            footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
        }
        if (reset) {
            curReqStatus = REQUEST_STATUS_LOADING;
            CommunicationManager.getInstance().requestShowLoadingAnimation();
            new Thread() {
                public void run() {
                    try{
                        sortedValidLotList = dMgr.getSortedValidLotteryList();
                    }catch(RPManagerException rpe) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.init]rpe : "+rpe.getMessage());
                        }
                        CommunicationManager.getInstance().requestHideLoadingAnimation();
                        CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
                    }
                    if (sortedValidLotList != null && sortedValidLotList.length > 0) {
                        ttCnt = sortedValidLotList.length;
                        imgSummaryReses = new Image[ttCnt];
                        for (int i=0; i<ttCnt; i++) {
//                            if (sortedValidLotList[i] == null || sortedValidLotList[i].isInvalidLottery()) {
//                                continue;
//                            }
                            if (sortedValidLotList[i] == null) {
                                continue;
                            }
                            String prodNo = sortedValidLotList[i].getProductNumber();
//                            Calendar latestCal = sortedValidLotList[i].getLatestDrawingDay();
                            try{
//                                imgSummaryReses[i] = dMgr.getSummaryResultImage(prodNo, latestCal);
                            	imgSummaryReses[i] = dMgr.getSummaryResultImage(prodNo);
                            }catch(RPManagerException rpe) {
                            }
                        }
                        resMsg = null;
                        curIdx = 0;
                        setDisplaySummaryResultImages();
                        CommunicationManager.getInstance().requestHideLoadingAnimation();
                        curReqStatus = REQUEST_STATUS_VALID;
                        repaint();
                        return;
                    }
                    //Invalid lottery data
                    resMsg = dCenter.getString("TxtLottery.No_Lottery_List");
                    ttCnt = 0;
                    CommunicationManager.getInstance().requestHideLoadingAnimation();
                    curReqStatus = REQUEST_STATUS_INVALID;
                    repaint();
                }
            }.start();
        }
    }
    protected void stopScene() {
        if (legalDisclaimer != null) {
            legalDisclaimer.stop();
        }
        if (bc != null) {
            bc.stop();
        }
        CommunicationManager.getInstance().requestHideLoadingAnimation();
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_UP:
                if (ttCnt == 0) {
                    return true;
                }
                if (curIdx == 0){
                    return true;
                }
                curIdx --;
                setDisplaySummaryResultImages();
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (ttCnt == 0) {
                    return true;
                }
                if (curIdx == ttCnt - 1) {
                    return true;
                }
                curIdx ++ ;
                setDisplaySummaryResultImages();
                repaint();
                return true;
            case Rs.KEY_LEFT:
            case Rs.KEY_RIGHT:
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation(0);
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_MAIN, false, null);
                return true;
            case Rs.KEY_OK:
                String lotProdNum = null;
//                if (sortedValidLotList != null && sortedValidLotList[curIdx] != null && !sortedValidLotList[curIdx].isInvalidLottery()) {
//                    lotProdNum = sortedValidLotList[curIdx].getProductNumber();
//                }
                if (sortedValidLotList != null && sortedValidLotList[curIdx] != null) {
                    lotProdNum = sortedValidLotList[curIdx].getProductNumber();
                }
                if (lotProdNum == null) {
                    return true;
                }
                new ClickingEffect(this, 5).start(241, 116 + ((curIdx-startIdx) * 73), 478, 73);
                sortedValidLotList[curIdx].setDrawingDate();
                int drawingDayCnt = sortedValidLotList[curIdx].getDrawingDayCount();
                if (drawingDayCnt > 0) {
                	SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_DETAILED_RESULTS, true, new String[]{lotProdNum});	
                }
                return true;
        }
        return false;
    }
    private void setDisplaySummaryResultImages() {
        int tempstartIdx = -1;
        int tempDispCnt = -1;
        int imgLotResultsLth = 0;
        if (imgSummaryReses != null) {
            imgLotResultsLth = imgSummaryReses.length;
        }
        if (imgLotResultsLth <= PAGE_COUNT) {
            tempstartIdx = 0;
            tempDispCnt = imgLotResultsLth;
        } else {
            int halfDispCnt = PAGE_COUNT /2;
            if (curIdx < halfDispCnt) {
                tempstartIdx = 0;
            } else if (curIdx >= imgLotResultsLth - halfDispCnt) {
                tempstartIdx = imgLotResultsLth - PAGE_COUNT;
            } else {
                tempstartIdx = curIdx - halfDispCnt;
            }
            tempDispCnt = PAGE_COUNT;
        }
        startIdx = tempstartIdx;
        dispCnt = tempDispCnt;
    }

    /*********************************************************************************
     * Renderer Communication-related
     *********************************************************************************/
    public String getResultMessage() {
        return resMsg;
    }
    public Image[] getLotteryResults() {
        return imgSummaryReses;
    }
    public Lottery[] getLotteryList() {
        return sortedValidLotList;
    }
    public int getStartIndex() {
        return startIdx;
    }
    public int getDisplayCount() {
        return dispCnt;
    }
    public int getCurrentIndex() {
        return curIdx;
    }
    public int getTotalCount() {
        return ttCnt;
    }
    public int getRequestStatus() {
        return curReqStatus;
    }
}

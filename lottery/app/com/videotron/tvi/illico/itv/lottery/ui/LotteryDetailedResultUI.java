package com.videotron.tvi.illico.itv.lottery.ui;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.util.Calendar;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.Breadcrumbs;
import com.videotron.tvi.illico.itv.lottery.comp.DetailedResultImageViewer;
import com.videotron.tvi.illico.itv.lottery.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.gui.LotteryDetailedResultRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class LotteryDetailedResultUI extends BaseUI {
    private static final long serialVersionUID = -9062523695516153738L;
    private DataCenter dCenter = DataCenter.getInstance();
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    public static final int REQUEST_STATUS_LOADING = 0;
    public static final int REQUEST_STATUS_VALID = 1;
    public static final int REQUEST_STATUS_INVALID = 2;
    private int curReqStatus;
    private String statusMeg;
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Breadcrumbs bc;
    private String[] bcTxts;
    private Footer footer;
    private LegalDisclaimer legalDisclaimer;
    private DetailedResultImageViewer detResImgViewer;

    private Lottery curLot;
    private String curLotName;
    private Calendar standardDate;
    private Calendar curDrawingDate;
    private Calendar prevDrawingDate;
    private Calendar nextDrawingDate;

    private static final String IMG_PROD_NO = "IMG_PROD_NO";
    private static final String IMG_SPI_PROD_NO = "IMG_SPI_PROD_NO";

    protected void initScene() {
        renderer = new LotteryDetailedResultRenderer();
        setRenderer(renderer);
        if (bc == null) {
            bc = new Breadcrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (bcTxts == null) {
            bcTxts = new String[2];
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 487, 550, 30);
            legalDisclaimer.setTextKeyLegalDisclaimer("TxtLottery.Desc_Legal_Disclaimer");
            legalDisclaimer.setVisible(true);
            add(legalDisclaimer, 0);
        }
        if (detResImgViewer == null) {
            detResImgViewer = new DetailedResultImageViewer();
            detResImgViewer.init();
            detResImgViewer.setBounds(173, 176, 493, 296);
        }
    }
    protected void disposeScene() {
        if (detResImgViewer != null) {
            remove(detResImgViewer);
            detResImgViewer.dispose();
            detResImgViewer = null;
        }
        if (legalDisclaimer!= null) {
            remove(legalDisclaimer);
            legalDisclaimer.dispose();
            legalDisclaimer = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        renderer = null;
    }
    protected void startScene(boolean reset, final String[] params) {
        /** get last previous day (before 8 days) **/
        CommunicationManager.getInstance().requestResizeScreen(0, 0, 0, 0);
        
        Calendar sCal=Lottery.getTodayCalendar();
        sCal.setTimeInMillis(((long)(sCal.getTimeInMillis()/3600000))*(3600000));
        sCal.set(Calendar.HOUR_OF_DAY, 0);
        sCal.add(Calendar.SECOND, -1);
        standardDate=(Calendar)sCal.clone();
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryDetailedResultUI.startScene]standardDate : " + standardDate);
        }
        
        prepare();
        if (legalDisclaimer != null) {
            legalDisclaimer.start();
        }
        //Invalid data
        if (params == null || params.length <= 0 || params[0] == null) {
            if (bc != null) {
                bc.start(new String[]{DataCenter.getInstance().getString("TxtLottery.Lottery"), "N/A"});
            }
            statusMeg = dCenter.getString("TxtLottery.No_Request_Product_Number");
            curReqStatus = REQUEST_STATUS_INVALID;
            return;
        }
        try{
            curLot = dMgr.getLotteryByProductNumber(params[0]);
        }catch(RPManagerException rpe) {
            CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
        }
        if (curLot == null) {
            statusMeg = dCenter.getString("TxtLottery.No_Lottery_Data");
            curReqStatus = REQUEST_STATUS_INVALID;
            return;
        }
//        curLot.setDrawingDate(); // at Summary
        curLotName = curLot.getLotteryProductName();
        if (bc != null) {
            bc.start(new String[]{DataCenter.getInstance().getString("TxtLottery.Lottery"), curLotName});
        }
        requestDetailedResultImageByLatestDrawingDate();
    }
    protected void stopScene() {
        ImageManager.getInstance().removeImage(IMG_PROD_NO);
        ImageManager.getInstance().removeImage(IMG_SPI_PROD_NO);
        if (bc != null) {
            bc.stop();
        }
        if (legalDisclaimer != null) {
            legalDisclaimer.stop();
        }
        if (detResImgViewer != null) {
            remove(detResImgViewer);
            detResImgViewer.stop();
        }
        curLotName = null;
        curLot = null;
        curDrawingDate = null;
        prevDrawingDate = null;
        nextDrawingDate = null;
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_PAGEUP:
                if (curReqStatus == REQUEST_STATUS_LOADING || curReqStatus == REQUEST_STATUS_INVALID) {
                    return true;
                }
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Page_Up_Down");
                }
                if (detResImgViewer != null) {
                    detResImgViewer.moveUp();
                }
                return true;
            case Rs.KEY_PAGEDOWN:
                if (curReqStatus == REQUEST_STATUS_LOADING || curReqStatus == REQUEST_STATUS_INVALID) {
                    return true;
                }
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Page_Up_Down");
                }
                if (detResImgViewer != null) {
                    detResImgViewer.moveDown();
                }
                return true;
            case Rs.KEY_LEFT:
                if (curReqStatus == REQUEST_STATUS_LOADING) {
                    return true;
                }
                if (prevDrawingDate == null || curDrawingDate == null || curLot == null) {
                    return true;
                }
                new ClickingEffect(this, 5).start(66, 120, 17, 25);
                requestDetailedResultImage(prevDrawingDate);
                return true;
            case Rs.KEY_RIGHT:
                if (curReqStatus == REQUEST_STATUS_LOADING) {
                    return true;
                }
                if (nextDrawingDate == null || curDrawingDate == null || curLot == null) {
                    return true;
                }
                new ClickingEffect(this, 5).start(877, 120, 17, 25);
                requestDetailedResultImage(nextDrawingDate);
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Back");
                }
                SceneManager.getInstance().goToPreviousScene();
                return true;
        }
        return false;
    }
    private void requestDetailedResultImageByLatestDrawingDate() {
        Calendar latestDrawingDate = curLot.getLatestDrawingDay();
        requestDetailedResultImage(latestDrawingDate);
    }
    private void requestDetailedResultImage(final Calendar reqDrawingDate) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryDetailedResultUI.requestDetailedResultImage]Start.");
            Log.printDebug("[LotteryDetailedResultUI.requestDetailedResultImage]Param - Request drawing date : " + reqDrawingDate);
        }
        curReqStatus = REQUEST_STATUS_LOADING;
        statusMeg = null;
        CommunicationManager.getInstance().requestShowLoadingAnimation();
        new Thread() {
            public void run() {
                if (curLot != null) {
                    try {
                        Log.printWarning("[LotteryDetailedResultUI.requestDetailedResultImage]latestDrawingDate : "+reqDrawingDate.getTime());
//                        Log.printWarning("[LotteryDetailedResultUI.requestDetailedResultImage]lastPrevDate : "+lastPrevDate);
//                        if ((reqDrawingDate.getTime()).before(lastPrevDate)) {
//                            if (Log.ERROR_ON) {
//                                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
//                            }    
//                            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
//                        }
                        
                        if (detResImgViewer != null) {
                            remove(detResImgViewer);
                        }
                        // Get prodNo image
                        String prodNo = curLot.getProductNumber();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LotteryDetailedResultUI.requestDetailedResultImage]Prod no : "+prodNo);
                        }
                        ImageManager.getInstance().removeImage(IMG_PROD_NO);
                        ImageManager.getInstance().removeImage(IMG_SPI_PROD_NO);
                        byte[] imgDetResProdNoSrc = dMgr.getDetailedResultImageSrc(prodNo, reqDrawingDate);
                        if (imgDetResProdNoSrc == null) {
                            if (Log.ERROR_ON) {
                                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
                            }    
                            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
                        }
                        Image imgDetResProdNo = ImageManager.getInstance().createImage(imgDetResProdNoSrc, IMG_PROD_NO, ImageManager.TYPE_LOTTERY_RELATED);
                        // Get sp1 image
                        String sp1ProdNo = curLot.getRelatedLotteryProduct1();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LotteryDetailedResultUI.requestDetailedResultImage]sp1ProdNo : "+sp1ProdNo);
                        }
                        Image imgDetResSp1 = null;
                        if (sp1ProdNo != null) {
                            byte[] imgDetResSp1Src = dMgr.getDetailedResultImageSrc(sp1ProdNo, reqDrawingDate);
                            if (imgDetResSp1Src == null) {
                                if (Log.ERROR_ON) {
                                    Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
                                }    
                                throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
                            }
                            imgDetResSp1 = ImageManager.getInstance().createImage(imgDetResSp1Src, IMG_SPI_PROD_NO, ImageManager.TYPE_LOTTERY_RELATED);
                        }
                        if (detResImgViewer != null) {
                            detResImgViewer.start(new Image[] { imgDetResProdNo, imgDetResSp1 });
                            remove(detResImgViewer);
                            add(detResImgViewer);
                            int totalPageCount = detResImgViewer.getTotalPageCount();
                            if (footer != null) {
                                footer.reset();
                                if (totalPageCount > 1) {
                                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                                    footer.addButton(PreferenceService.BTN_PAGE, "TxtLottery.Page_Up_Down");
                                } else {
                                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                                }
                            }
                        }
                        curReqStatus = REQUEST_STATUS_VALID;
                    } catch (Exception e) {
                        remove(detResImgViewer);
                        if (footer != null) {
                            footer.reset();
                            footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                        }
                        curReqStatus = REQUEST_STATUS_INVALID;
                        statusMeg = dCenter.getString("TxtLottery.No_Detailed_Result_Image");
                        CommunicationManager.getInstance().requestHideLoadingAnimation();
                        
                        // If lottery's image file does not exist to server, then do not show error message popup.
                        if (!(e instanceof FileNotFoundException)) { 
                        	CommunicationManager.getInstance().requestShowErrorMessage(e.getMessage());
                        }
                    } finally {
                        curDrawingDate = reqDrawingDate;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LotteryDetailedResultUI.requestDetailedResultImage]finally called : "+curDrawingDate);
                        }
                        setPrevDrawingDate(curDrawingDate);
                        setNextDrawingDate(curDrawingDate);
                        repaint();
                        CommunicationManager.getInstance().requestHideLoadingAnimation();
                    }
                }
            }
        }.start();
    }
    private void setPrevDrawingDate(Calendar reqDrawingDate) {
        prevDrawingDate = curLot.getPrevDrawingDay(reqDrawingDate);
        if (prevDrawingDate==null) {
            Log.printDebug("[LotteryDetailedResultUI.setPrevDrawingDate] Previous drawing date is null. return");
            return;
        }
//        if (prevDrawingDate.getTime().before(lastPrevDate)) {
//            if (Log.WARNING_ON) {
//                Log.printWarning("[LotteryDetailedResultUI.setPrevDrawingDate]past last valid day.");
//            }
//            prevDrawingDate=null;
//        }
    }
    private void setNextDrawingDate(Calendar reqDrawingDate) {
        Log.printDebug("[LotteryDetailedResultUI.setNextDrawingDate]Param - reqDrawingDate : "+reqDrawingDate.getTime());
        nextDrawingDate = curLot.getNextDrawingDay(reqDrawingDate);
        if (nextDrawingDate==null) {
            Log.printDebug("[LotteryDetailedResultUI.setNextDrawingDate] Next drawing date is null. return");
            return;
        }
        Log.printDebug("[LotteryDetailedResultUI.setNextDrawingDate]nextDrawingDate : "+nextDrawingDate.getTime());
        Log.printDebug("[LotteryDetailedResultUI.setNextDrawingDate]standardDate : "+standardDate);
        if (Rs.USE_LOCAL_SERVER_FILE) {
            Calendar currentDate = Lottery.getTodayCalendar();
            currentDate.set(2011, Calendar.MARCH, 25, 1, 1, 1) ;
            if (currentDate.before(nextDrawingDate)) {
                nextDrawingDate = null;
            }
        }
//        if (standardDate.before(nextDrawingDate.getTime())) {
//            nextDrawingDate = null;
//        }
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public String getLotteryName() {
        return curLotName;
    }
    public Calendar getCurrentDrawingDate() {
        return curDrawingDate;
    }
    public Calendar getPreviousDrawingDate() {
        return prevDrawingDate;
    }
    public Calendar getNextDrawingDate() {
        return nextDrawingDate;
    }
    public int getCurrentRequestStatus() {
        return curReqStatus;
    }
    public String getStatusMessage() {
        return statusMeg;
    }
}

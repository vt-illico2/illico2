package com.videotron.tvi.illico.itv.lottery.data;

import java.util.Vector;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.itv.lottery.comp.SAXHandlerAdapter;
import com.videotron.tvi.illico.log.Log;

public class LotteryListHandler extends SAXHandlerAdapter {
//    private static final String ENCODING = "iso-8859-1";
    private static final String ENCODING = "UTF-8";
    private Vector lotteryVec;
    private Lottery lot;
    private String curXMLTag;

    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        curXMLTag = valueXMLTag;
        if (valueXMLTag.equalsIgnoreCase("loteries")) {
            lotteryVec = new Vector();
        } else if (valueXMLTag.equalsIgnoreCase("loto")) {
            lot = getLottery(attr);
        } else if (valueXMLTag.equalsIgnoreCase("period")) {
            String beginDate = null;
            String endDate = null;
            int attrLth = attr.getLength();
            for (int i=0; i<attrLth; i++) {
                String lName = attr.getLocalName(i);
                if (lName == null) {
                    continue;
                }
                String lValue = attr.getValue(i);
                try{
                    lValue = new String(lValue.getBytes(), ENCODING);
                }catch(Exception e) {
                    e.printStackTrace();
                }
                if (lName.equalsIgnoreCase("BeginDate")) {
                    beginDate = lValue;
                } else if (lName.equalsIgnoreCase("EndDate")) {
                    endDate = lValue;
                }
            }
            if (lot != null) {
//                lot.setSpecificDurationLottery(true);
                lot.addPeriod(beginDate, endDate);
//                lot.setBeginDate(beginDate);
//                lot.setEndDate(endDate);
            }
        }
    }
    public void endElement(String valueXMLTag) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryListHandler.endElement]valueXMLTag : "+valueXMLTag);
        }
        if (valueXMLTag != null) {
            if (valueXMLTag.equalsIgnoreCase("loteries")) {
                Lottery[] lotteries = null;
                if (lotteryVec != null) {
                    int lotteryVecSz = lotteryVec.size();
                    lotteries = new Lottery[lotteryVecSz];
                    for (int i=0; i<lotteryVecSz; i++) {
                        lotteries[i] = (Lottery)lotteryVec.elementAt(i);
                    }
                }
                setResultObject(lotteries);
            } else if (valueXMLTag.equalsIgnoreCase("loto")) {
                if (lot != null) {
                    if (Log.INFO_ON) {
                        Log.printInfo("[LotteryListHandler.endElement]Lottery : "+lot);
                    }
                    lotteryVec.addElement(lot);
                    lot = null;
                }
            }
        }
        curXMLTag = null;
    }
    public void parseCDData(String valueCDData) {
        if (curXMLTag != null && curXMLTag.equals("date")) {
            if (lot != null) {
                lot.addSingleDay(valueCDData);
            }
        }
    }
    private Lottery getLottery(Attributes attr) {
        if (attr == null) {
            return null;
        }
        Lottery lot = new Lottery();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), ENCODING);
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("N")) {
                lot.setLotteryProductName(lValue);
            } else if (lName.equalsIgnoreCase("P")) {
                lot.setProductNumber(lValue);
            } else if (lName.equalsIgnoreCase("SP1")) {
                lot.setRelatedLotteryProduct1(lValue);
            } else if (lName.equalsIgnoreCase("SP2")) {
                lot.setRelatedLotteryProduct2(lValue);
            } else if (lName.equalsIgnoreCase("F")) {
                lot.setLotteryFrequency(lValue);
            } else if (lName.equalsIgnoreCase("Code")) {
                lot.setProductCode(lValue);
            }
        }
        return lot;
    }
}

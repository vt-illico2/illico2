package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Calendar;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.ui.LotteryDetailedResultUI;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.TextUtil;

public class LotteryDetailedResultRenderer extends BaseRenderer {
    private Formatter formatter = Formatter.getCurrent();
    private StringBuffer sb = new StringBuffer();
    
    private Image imgTitleBG;
    private Image imgArrowL;
    private Image imgArrowR;
    private Image imgDescT;
    private Image imgDescB;
    private Image imgDescM;
    
    protected void prepareChild(){
        imgTitleBG = ImageManager.getInstance().getImage("11_titlebg.png", ImageManager.TYPE_TEMPORARY);
        imgArrowL = ImageManager.getInstance().getImage("02_ars_l.png", ImageManager.TYPE_TEMPORARY);
        imgArrowR = ImageManager.getInstance().getImage("02_ars_r.png", ImageManager.TYPE_TEMPORARY);
        imgDescT = ImageManager.getInstance().getImage("11_lot_info_t.png", ImageManager.TYPE_TEMPORARY);
        imgDescB = ImageManager.getInstance().getImage("11_lot_info_b.png", ImageManager.TYPE_TEMPORARY);
        imgDescM = ImageManager.getInstance().getImage("11_lot_info_m.png", ImageManager.TYPE_TEMPORARY);
    }
    
    protected void paintRenderer(Graphics g, UIComponent c) {
        LotteryDetailedResultUI scene = (LotteryDetailedResultUI)c;
        // Title
        sb.setLength(0);
        String lotName = scene.getLotteryName();
        if (lotName == null) {
            lotName = "N/A";
        }
        sb.append(lotName);
        String txtDetRes = DataCenter.getInstance().getString("TxtLottery.Detailed_Results");
        if (txtDetRes != null) {
            sb.append(" - ").append(txtDetRes);
        }
        String title = sb.toString();
        g.setFont(Rs.F26);
        g.setColor(Rs.C255255255);
        g.drawString(title, 61, 98);
        // Date
        if (imgTitleBG != null) {
            g.drawImage(imgTitleBG, 0, 82, c);
        }
        sb.setLength(0);
        String txtDraw = DataCenter.getInstance().getString("TxtLottery.Draw");
        if (txtDraw == null) {
            txtDraw = "";
        }
        sb.append(txtDraw).append(" - ");
        Calendar curDrawingDate = scene.getCurrentDrawingDate();
        if (curDrawingDate != null) {
//            sb.append(formatter.getDayText(curDrawingDate)).append(", ").append(curDrawingDate.get(Calendar.YEAR));
            sb.append(formatter.getLongDayText(curDrawingDate.getTime())).append(" ").append(curDrawingDate.get(Calendar.YEAR));
        } else {
            sb.append("N/A");
        }
        String txtDate = sb.toString();
        g.setFont(Rs.F24);
        g.setColor(Rs.C249198000);
        int txtDateWth = Rs.FM24.stringWidth(txtDate);
        g.drawString(txtDate, 479 - (txtDateWth/2), 140);
        
        // Previous results
        g.setFont(Rs.F17);
        g.setColor(Rs.C210210210);
        Calendar prevDrawingDate = scene.getPreviousDrawingDate();
        if (prevDrawingDate != null) {
            if (imgArrowL != null) {
                g.drawImage(imgArrowL, 66, 120, c);
            }
            String txtPrevResults = DataCenter.getInstance().getString("TxtLottery.Prev_Results");
            if (txtPrevResults != null) {
                g.drawString(txtPrevResults, 93, 137);
            }
        }
        // Next results
        Calendar nextDrawingDate = scene.getNextDrawingDate();
        if (nextDrawingDate != null) {
            if (imgArrowR != null) {
                g.drawImage(imgArrowR, 877, 120, c);
            }
            String txtNextResults = DataCenter.getInstance().getString("TxtLottery.Next_Results");
            if (txtNextResults != null) {
                g.drawString(txtNextResults, 867 - (Rs.FM17.stringWidth(txtNextResults)), 137);
            }
        }
        
        //Status
        int curReqStatus = scene.getCurrentRequestStatus();
        switch(curReqStatus) {
            case LotteryDetailedResultUI.REQUEST_STATUS_LOADING:
                return;
            case LotteryDetailedResultUI.REQUEST_STATUS_VALID:
                break;
            case LotteryDetailedResultUI.REQUEST_STATUS_INVALID:
                String statusMsg = scene.getStatusMessage();
                if (statusMsg != null) {
                    g.setFont(Rs.F24);
                    g.setColor(Rs.C255234160);
                    int statusMsgWth = Rs.FM24.stringWidth(statusMsg);
                    g.drawString(statusMsg, 479 - (statusMsgWth/2), 325);
                }
                return;
        }
        // Description
        String txtDesc = DataCenter.getInstance().getString("TxtLottery.Deteiled_Results_Desc");
        if (txtDesc != null) {
            String[] txtDescs = TextUtil.split(txtDesc, Rs.FM17, 182);
            int txtDescsLth = txtDescs.length;
            if (txtDescsLth > 0) {
                int imgDescMH = 18 * txtDescsLth;
                if (imgDescM != null) {
                    g.drawImage(imgDescM, 689, 187, 213, imgDescMH, c);
                }
                if (imgDescB != null) {
                    g.drawImage(imgDescB, 689, 187 + imgDescMH, c);
                }
                if (imgDescT != null) {
                    g.drawImage(imgDescT, 689, 177, c);
                }
                g.setFont(Rs.F17);
                g.setColor(Rs.C210210210);
                for (int i=0; i<txtDescsLth; i++) {
                    if (txtDescs[i] != null) {
                        g.drawString(txtDescs[i], 705, 201 +(18*i));
                    }
                }
            }
        }
    }
}

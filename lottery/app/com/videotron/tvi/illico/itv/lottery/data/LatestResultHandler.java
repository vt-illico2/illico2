package com.videotron.tvi.illico.itv.lottery.data;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.itv.lottery.comp.SAXHandlerAdapter;
import com.videotron.tvi.illico.log.Log;

public class LatestResultHandler extends SAXHandlerAdapter{
    private LatestResult latestRes;
    private LatestResult subLatestRes;
    private String valueXMLTag;
    private LotteryWinningNumber winNum;
    public void startDocument() {
        latestRes = new LatestResult();
    }
    public void endDocument() {
        setResultObject(latestRes);
    }

    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        this.valueXMLTag = valueXMLTag;
        if (valueXMLTag.equalsIgnoreCase("WinningNumbers")) {
            winNum = getLotteryWinningNumber(attr);
        } else if (valueXMLTag.equalsIgnoreCase("SubProduct")) {
            subLatestRes = new LatestResult();
        }
    }
    public void endElement(String valueXMLTag) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("SubProduct")) {
            if (latestRes != null && subLatestRes!= null) {
                latestRes.addSubLotteryResult(subLatestRes);
                subLatestRes = null;
            }
        }
    }
    public void parseCDData(String valueCDData) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueCDData != null) {
            try{
                valueCDData = new String(valueCDData.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        if (valueXMLTag.equalsIgnoreCase("Language")) {
            latestRes.setLotteryLanguage(valueCDData);
        } else if (valueXMLTag.equalsIgnoreCase("ProductName")) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryResultHandler.parseCDData]valueCDData : "+valueCDData);
                Log.printDebug("[LotteryResultHandler.parseCDData]subLotRes : "+subLatestRes);
                Log.printDebug("[LotteryResultHandler.parseCDData]lotRes : "+latestRes);
            }
            if (subLatestRes != null) {
                subLatestRes.setLotteryName(valueCDData);
            } else {
                if (latestRes != null) {
                    latestRes.setLotteryName(valueCDData);
                }
            }
        } else if (valueXMLTag.equalsIgnoreCase("ProductNumber")) {
            if (subLatestRes != null) {
                subLatestRes.setLotteryProductNumber(valueCDData);
            } else {
                if (latestRes != null) {
                    latestRes.setLotteryProductNumber(valueCDData);
                }
            }
        } else if (valueXMLTag.equalsIgnoreCase("DrawingDate")) {
            if (subLatestRes != null) {
                subLatestRes.setDrawingDate(valueCDData);
            } else {
                if (latestRes != null) {
                    latestRes.setDrawingDate(valueCDData);
                }
            }
        } else if (valueXMLTag.equalsIgnoreCase("WinningNumbers")) {
            if (winNum == null) {
                winNum = new LotteryWinningNumber();
            }
            winNum.setWinningNumber(valueCDData);
            if (subLatestRes != null) {
                subLatestRes.addWinningNumber(winNum);
            } else {
                if (latestRes != null) {
                    latestRes.addWinningNumber(winNum);
                }
            }
            winNum = null;
        }
    }
    private LotteryWinningNumber getLotteryWinningNumber(Attributes attr) {
        if (attr == null) {
            return null;
        }
        LotteryWinningNumber res = new LotteryWinningNumber();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("Lot")) {
                res.setLotterySubName(lValue);
            }
        }
        return res;
    }
}

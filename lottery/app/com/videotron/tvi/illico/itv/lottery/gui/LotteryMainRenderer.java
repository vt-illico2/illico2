package com.videotron.tvi.illico.itv.lottery.gui;

import java.awt.Graphics;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.log.Log;

public class LotteryMainRenderer extends BaseRenderer {
    protected void prepareChild(){
    }
    protected void paintRenderer(Graphics g, UIComponent c) {
        //Version
        if (Log.EXTRA_ON) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C245212016);
            String version = "Ver."+Rs.APP_VERSION;
            g.drawString(version, 690 - Rs.FM18.stringWidth(version), 58);
        }
    }
}

package com.videotron.tvi.illico.itv.lottery.ui;

import java.awt.event.KeyEvent;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.Breadcrumbs;
import com.videotron.tvi.illico.itv.lottery.comp.FavoriteLotteryList;
import com.videotron.tvi.illico.itv.lottery.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.lottery.comp.ListEffect;
import com.videotron.tvi.illico.itv.lottery.comp.PopupNotification;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.gui.LotteryPreferenceFavoriteLotteryRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.KeyCodes;

public class LotteryPreferenceFavoriteLotteryUI extends BaseUI{
    private static final long serialVersionUID = 7458887584076986159L;
    private DataCenter dCenter = DataCenter.getInstance();
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Breadcrumbs bc;
    private String[] bcTxts;
    private Footer footer;
    private LegalDisclaimer legalDisclaimer;
    private FavoriteLotteryList favLotList;
    /*********************************************************************************
     * Area-related
     *********************************************************************************/
    public static final int AREA_AVAILABLE = 0;
    public static final int AREA_BUTTON = 1;
    public static final int AREA_FAVOURITE = 2;
    private int curArea;
    /*********************************************************************************
     * Button-related
     *********************************************************************************/
    public static final int BUTTON_TYPE_COUNT = 2;
    public static final int BUTTON_TYPE_ADD = 0;
    public static final int BUTTON_TYPE_REMOVE = 1;
    private int curButtonType;
    /*********************************************************************************
     * Lottery-related
     *********************************************************************************/
    public static final int PAGE_COUNT = 9;
    private Lottery[] lots;
    private Vector availLotVec;
    private Vector favLotVec;
    private int ttAvailCount;
    private int ttFavCount;
    private int curAvailIdx;
    private int curFavIdx;

    private int startIdxAvail;
    private int dispCntAvail;
    /*********************************************************************************
     * Lottery-related
     *********************************************************************************/
    private boolean isChangeOrderMode;
    private boolean isActiveAnimation;

    private ListEffect listEffect;

    protected void initScene() {
        renderer = new LotteryPreferenceFavoriteLotteryRenderer();
        setRenderer(renderer);
        availLotVec = new Vector();
        favLotVec = new Vector();
        if (favLotList == null) {
            favLotList = new FavoriteLotteryList(this);
            favLotList.setBounds(0, 0, 960, 540);
            add(favLotList);
        }
        if (bc == null) {
            bc = new Breadcrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (bcTxts == null) {
            bcTxts = new String[1];
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 487, 550, 30);
            legalDisclaimer.setVisible(true);
            add(legalDisclaimer, 0);
        }
        listEffect = new ListEffect(favLotList);
    }
    protected void disposeScene() {
        if (legalDisclaimer!= null) {
            remove(legalDisclaimer);
            legalDisclaimer.dispose();
            legalDisclaimer = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        if (favLotList != null) {
            remove(favLotList);
            favLotList = null;
        }
        renderer = null;
    }
    protected void startScene(boolean reset, String[] params) {
        prepare();
        if (legalDisclaimer != null) {
            legalDisclaimer.start();
        }
        if (bc != null) {
            bcTxts[0] = dCenter.getString("TxtLottery.Lottery");
            bc.start(bcTxts);
        }
        isChangeOrderMode = false;
        CommunicationManager.getInstance().requestShowLoadingAnimation();
        new Thread() {
            public void run() {
                try{
                    lots = dMgr.getSortedLotteryList();
                    Lottery[] favLots = dMgr.getFavoriteLotteryList();
                    initLotteryData(lots, favLots);
                    if (favLots != null && favLots.length < DataManager.FAVORITE_LOTTERY_COUNT) {
                        setArea(AREA_AVAILABLE);
                        curButtonType = BUTTON_TYPE_ADD;
                    } else {
                        setArea(AREA_FAVOURITE);
                        curButtonType = BUTTON_TYPE_REMOVE;
                    }
                    curAvailIdx = 0;
                    curFavIdx = 0;
                } catch(RPManagerException rpe) {
                    CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
                } catch (Exception e) {
                } finally {
                    CommunicationManager.getInstance().requestHideLoadingAnimation();
                    repaint();
                }
            }
        }.start();
    }
    protected void stopScene() {
    }
    protected boolean keyAction(int keyCode) {
        switch(keyCode) {
            case KeyEvent.VK_0:
            case KeyEvent.VK_1:
            case KeyEvent.VK_2:
            case KeyEvent.VK_3:
            case KeyEvent.VK_4:
            case KeyEvent.VK_5:
            case KeyEvent.VK_6:
            case KeyEvent.VK_7:
            case KeyEvent.VK_8:
            case KeyEvent.VK_9:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            case Rs.KEY_UP:
                if (isChangeOrderMode) {
                    if (curFavIdx != 0) {
                        boolean result = moveFavoriteLottery(curFavIdx, curFavIdx - 1);
                        if (!result) {
                            return true;
                        }
                    }
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        if (ttAvailCount == 0 || curAvailIdx == 0) {
                            return true;
                        }
                        curAvailIdx -- ;
                        repaint();
                        return true;
                    case AREA_BUTTON:
                        return true;
                    case AREA_FAVOURITE:
                        if (ttFavCount == 0 || curFavIdx == 0) {
                            return true;
                        }
                        curFavIdx -- ;
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_DOWN:
                if (isChangeOrderMode) {
                    if (curFavIdx < ttFavCount-1) {
                        boolean result = moveFavoriteLottery(curFavIdx, curFavIdx + 1);
                        if (!result) {
                            return true;
                        }
                    }
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        if (ttAvailCount == 0 || curAvailIdx == ttAvailCount - 1) {
                            return true;
                        }
                        curAvailIdx ++ ;
                        repaint();
                        return true;
                    case AREA_BUTTON:
                        return true;
                    case AREA_FAVOURITE:
                        if (ttFavCount == 0 || curFavIdx == ttFavCount - 1) {
                            return true;
                        }
                        curFavIdx ++ ;
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_LEFT:
                if (ttAvailCount == 0) {
                    return true;
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        return true;
                    case AREA_BUTTON:
                        if (ttFavCount < DataManager.FAVORITE_LOTTERY_COUNT) {
                            setArea(AREA_AVAILABLE);
                            repaint();
                        }
                        return true;
                    case AREA_FAVOURITE:
                        setArea(AREA_BUTTON);
                        curButtonType = BUTTON_TYPE_REMOVE;
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_RIGHT:
                if (ttAvailCount == 0) {
                    return true;
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        setArea(AREA_BUTTON);
                        curButtonType = BUTTON_TYPE_ADD;
                        repaint();
                        return true;
                    case AREA_BUTTON:
                        if (ttFavCount > 0) {
                            setArea(AREA_FAVOURITE);
                            repaint();
                        }
                        return true;
                    case AREA_FAVOURITE:
                        return true;
                }
                return true;
            case Rs.KEY_OK:
                if (ttAvailCount == 0) {
                    return true;
                }
                if (isChangeOrderMode) {
                    isChangeOrderMode = false;
                    repaint();
                    return true;
                }
                if (curArea == AREA_AVAILABLE || (curArea == AREA_BUTTON && curButtonType == BUTTON_TYPE_ADD)) {
                    if (ttAvailCount > 0 && ttFavCount < DataManager.FAVORITE_LOTTERY_COUNT) {
                        boolean result = addFavoriteLottery((Lottery)availLotVec.elementAt(curAvailIdx));
                        if (result) {
                            if (ttAvailCount == 0 || ttFavCount == DataManager.FAVORITE_LOTTERY_COUNT) {
                                setArea(AREA_FAVOURITE);
                                curFavIdx = 0;
                            } else {
                                if (curAvailIdx >= ttAvailCount) {
                                    curAvailIdx = ttAvailCount - 1;
                                }
                            }
                            boolean isDown = false;
                            if (curAvailIdx == ttAvailCount-1) {
                                isDown = true;
                            }
                            listEffect.updateBackgroundBeforeStart(false);
                            listEffect.start(ListEffect.MODE_FROM_LEFT, curAvailIdx, curFavIdx, isDown);
                            setButton();
                        }
                    }
                } else if (curArea == AREA_FAVOURITE || (curArea == AREA_BUTTON && curButtonType == BUTTON_TYPE_REMOVE)) {
                    if (ttFavCount > 0) {
                        boolean result = removeFavoriteLottery((Lottery)favLotVec.elementAt(curFavIdx));
                        if (result) {
                            if (ttFavCount == 0) {
                                setArea(AREA_AVAILABLE);
                                curAvailIdx = 0;
                            } else {
                                if (curFavIdx >= ttFavCount) {
                                    curFavIdx = ttFavCount - 1;
                                }
                            }
                            listEffect.updateBackgroundBeforeStart(false);
                            listEffect.start(ListEffect.MODE_FROM_RIGHT, curAvailIdx, curFavIdx, false);
                            setButton();
                        }
                    } else {
                        setArea(AREA_AVAILABLE);
                    }
                }
                return true;
            case KeyCodes.LAST:
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Back");
                }
                SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE, false, null);
                return true;
            case Rs.KEY_PAGEUP:
                if (isChangeOrderMode) {
                    return  true;
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        if (ttAvailCount == 0) {
                            return true;
                        }
                        if (ttAvailCount <= PAGE_COUNT) {
                            return true;
                        }
                        if (footer != null) {
                            footer.clickAnimation("TxtLottery.Page_Up_Down");
                        }
                        curAvailIdx -= PAGE_COUNT;
                        if (curAvailIdx < 0) {
                            curAvailIdx = 0;
                        }
                        repaint();
                        return true;
                }
                return true;
            case Rs.KEY_PAGEDOWN:
                if (isChangeOrderMode) {
                    return  true;
                }
                switch(curArea) {
                    case AREA_AVAILABLE:
                        if (ttAvailCount == 0) {
                            return true;
                        }
                        if (ttAvailCount <= PAGE_COUNT) {
                            return true;
                        }
                        if (footer != null) {
                            footer.clickAnimation("TxtLottery.Page_Up_Down");
                        }
                        curAvailIdx += PAGE_COUNT;
                        if (curAvailIdx >= ttAvailCount) {
                            curAvailIdx = ttAvailCount - 1;
                        }
                        repaint();
                        return true;
                }
                return true;
            case KeyCodes.COLOR_A:
                if (ttFavCount <= 1) {
                    return true;
                }
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Edit_Order");
                }
                isChangeOrderMode = true;
                repaint();
                return true;
            case KeyCodes.COLOR_B:
                if (ttFavCount == 0) {
                    return true;
                }
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Clear_Favourite");
                }
                String title = dCenter.getString("TxtLottery.Popup_Clear_Favorites_Title");
                String content = dCenter.getString("TxtLottery.Popup_Clear_Favorites_Content");
                openPopupQuestion(getPopupAdapterClearFavorites(), title, content);
                return true;
        }
        return false;
    }
    /*********************************************************************************
     * Favorite lottery-related
     *********************************************************************************/
    private void initLotteryData(Lottery[] lots, Lottery[] favLots) {
        favLotVec.clear();
        if (favLots != null) {
            for (int i=0; i<favLots.length; i++) {
                if (favLots[i] == null) {
                    continue;
                }
                favLotVec.addElement(favLots[i]);
            }
        }
        ttFavCount = favLotVec.size();
        setAvailableLottery();
    }
    private void setAvailableLottery() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setAvailableLottery]availLotVec : "+availLotVec);
        }
        availLotVec.clear();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setAvailableLottery]lots : "+lots);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.setAvailableLottery]favLotVec : "+favLotVec);
        }
        for (int i=0; i<lots.length; i++) {
            if (lots[i] == null) {
                continue;
            }
            if (favLotVec.contains(lots[i])) {
                continue;
            }
            availLotVec.addElement(lots[i]);
        }
        ttAvailCount = availLotVec.size();
    }
    private boolean addFavoriteLottery(Lottery reqData) {
        Vector tempFavLotVec = (Vector)favLotVec.clone();
        tempFavLotVec.addElement(reqData);
        boolean result = addFavoriteLotteriesToUPP(tempFavLotVec);
        if (result) {
            favLotVec = tempFavLotVec;
            ttFavCount = favLotVec.size();
            setAvailableLottery();
        }
        return result;
    }
    private boolean removeFavoriteLottery(Lottery reqData) {
        Vector tempFavLotVec = (Vector)favLotVec.clone();
        tempFavLotVec.removeElement(reqData);
        boolean result = addFavoriteLotteriesToUPP(tempFavLotVec);
        if (result) {
            favLotVec = tempFavLotVec;
            ttFavCount = favLotVec.size();
            setAvailableLottery();
        }
        return result;
    }
    private boolean clearFavoriteLotteries() {
        Vector tempFavLotVec = (Vector)favLotVec.clone();
        tempFavLotVec.clear();
        boolean result = addFavoriteLotteriesToUPP(tempFavLotVec);
        if (result) {
            favLotVec = tempFavLotVec;
            ttFavCount = favLotVec.size();
            setAvailableLottery();
        }
        return result;
    }
    private boolean moveFavoriteLottery(int from, int to) {
        if (from == to) {
            return true;
        }
        Vector tempFavLotVec = (Vector)favLotVec.clone();
        Lottery lot = (Lottery)tempFavLotVec.elementAt(from);
        tempFavLotVec.removeElementAt(from);
        tempFavLotVec.add(to, lot);
        boolean result = addFavoriteLotteriesToUPP(tempFavLotVec);
        if (result) {
            favLotVec = tempFavLotVec;
        }
        return result;
    }
    private void setArea(int reqArea) {
        curArea = reqArea;
        if (reqArea != AREA_FAVOURITE) {
            if (isChangeOrderMode) {
                isChangeOrderMode = false;
                addFavoriteLotteriesToUPP(favLotVec);
            }
        }
        setButton();
    }
    private void setButton() {
        if (footer != null) {
            footer.reset();
            if (curArea == AREA_FAVOURITE) {
                if (ttFavCount <= 1) {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                    footer.addButton(PreferenceService.BTN_B, "TxtLottery.Clear_Favourite");
                } else {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                    footer.addButton(PreferenceService.BTN_A, "TxtLottery.Edit_Order");
                    footer.addButton(PreferenceService.BTN_B, "TxtLottery.Clear_Favourite");
                }
            } else {
                if (ttFavCount == 0) {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                    if (ttAvailCount > PAGE_COUNT) {
                        footer.addButton(PreferenceService.BTN_PAGE, "TxtLottery.Page_Up_Down");
                    }
                } else {
                    footer.addButton(PreferenceService.BTN_BACK, "TxtLottery.Back");
                    if (ttAvailCount > PAGE_COUNT) {
                        footer.addButton(PreferenceService.BTN_PAGE, "TxtLottery.Page_Up_Down");
                    }
                    footer.addButton(PreferenceService.BTN_B, "TxtLottery.Clear_Favourite");
                }
            }
        }
    }
    private boolean addFavoriteLotteriesToUPP(Vector reqData) {
        int favLotVecSz = reqData.size();
        Lottery[] favLots = new Lottery[favLotVecSz];
        favLots = (Lottery[])reqData.toArray(favLots);
        boolean res = false;
        try{
            res = dMgr.setFavoriteLotteryList(favLots);
        }catch(RPManagerException rpe) {
            CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
        }
        return res;
    }
    /*********************************************************************************
     * Popup-related
     *********************************************************************************/
    private PopupAdapter getPopupAdapterClearFavorites() {
        PopupAdapter popAdaptClearFavorites = new PopupAdapter() {
            public void popupOK(PopupEvent popupEvent) {
                PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
                boolean result = clearFavoriteLotteries();
                if (result) {
                    setArea(AREA_AVAILABLE);
                    curAvailIdx = 0;
                    curFavIdx = 0;
                    repaint();
                }
            }
            public void popupCancel(PopupEvent popupEvent) {
                PopupNotification pop = (PopupNotification)popupEvent.getPopup();
                if (pop != null) {
                    pop.removePopupListener();
                    PopupController.getInstance().closePopup(pop);
                }
            }
        };
        return popAdaptClearFavorites;
    }
    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public int getCurrentArea() {
        return curArea;
    }
    public Vector getAvailableLotteryVector() {
        return availLotVec;
    }
    public Vector getFavoriteLotteryVector() {
        return favLotVec;
    }
    public int getCurrentAvailableIndex() {
        return curAvailIdx;
    }
    public int getCurrentFavoriteIndex() {
        return curFavIdx;
    }
    public int getCurrentButtonType() {
        return curButtonType;
    }
    public boolean isChangeOrderMode() {
        return isChangeOrderMode;
    }
    public int getAvailableStartIndex() {
        return startIdxAvail;
    }
    public int getAvailableDisplayCount() {
        return dispCntAvail;
    }
    public boolean isActiveAnimation() {
        return isActiveAnimation;
    }
    public void setActiveAnimation(boolean isActiveAnimation) {
        this.isActiveAnimation = isActiveAnimation;
    }
    public void animationEnded(Effect effect) {
        isActiveAnimation = false;
    }
}

package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.log.Log;

public class PromotionalBanner extends Container {
    private static final long serialVersionUID = 6708212178693793073L;
    /*********************************************************************************
     * Timer-related
     *********************************************************************************/
    private long cycleSecs = -1;
    private TVTimerSpec cycleTimer;
    private CycleTimerImpl cycleTimerImpl;

    private boolean readyComponent;
    private Image[] promoBannerImgs;
    private int ttCnt;
    private int curIdx;

    public void init() {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        promoBannerImgs = null;
        if (cycleTimer != null) {
            cycleTimer.removeTVTimerWentOffListener(cycleTimerImpl);
            cycleTimer = null;
        }
        cycleTimerImpl = null;
    }
    public void start() {
        if (cycleSecs > 0) {
            if (cycleTimerImpl == null) {
                cycleTimerImpl = new CycleTimerImpl();
            }
            if (cycleTimer == null) {
                cycleTimer = new TVTimerSpec();
                cycleTimer.setDelayTime(cycleSecs * 1000);
                cycleTimer.setRepeat(true);
                cycleTimer.addTVTimerWentOffListener(cycleTimerImpl);
            }
        }
        new Thread() {
            public void run() {
                readyComponent = false;
                ttCnt = 0;
                promoBannerImgs = DataManager.getInstance().getPromotionalBannerImages();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[PromotionalBanner.start]promoBannerImgs : "+promoBannerImgs);
                }
                if (promoBannerImgs != null) {
                    ttCnt = promoBannerImgs.length;
                    if (ttCnt > 0) {
                        curIdx = (int)(Math.random() * 10) % ttCnt;
                        startCycleTimer();
                    } else {
                        curIdx = 0;
                    }
                }
                readyComponent = true;
                repaint();
            }
        }.start();
    }
    public void stop() {
        stopCycleTimer();
        ttCnt = 0;
    }
    public void paint(Graphics g){
        if (!readyComponent) {
            String txtLoadingData = DataCenter.getInstance().getString("TxtLottery.Loading_Data");
            paintBlankPanel(g, txtLoadingData, Rs.F28, Rs.FM28);
            return;
        }
        if (ttCnt == 0) {
            String txtMessage = DataCenter.getInstance().getString("TxtLottery.No_Promotional_Banner");
            paintBlankPanel(g, txtMessage, Rs.F28, Rs.FM28);
            return;
        }
        if (promoBannerImgs != null && curIdx < ttCnt && promoBannerImgs[curIdx] != null) {
            g.drawImage(promoBannerImgs[curIdx], 0, 0, this);
        }
    }
    private void paintBlankPanel(Graphics g, String content, Font f, FontMetrics fm) {
        if (content != null) {
            g.setColor(Rs.C086086086);
            g.fillRect(0, 0, 468, 62);
            g.setColor(Rs.C050050050);
            g.drawRect(0, 0, 467, 61);
            int w = getWidth();
            int txtMessageWth = fm.stringWidth(content);
            int txtMessageX = (w-txtMessageWth)/2;
            g.setFont(f);
            g.setColor(Rs.C110110110);
            g.drawString(content, txtMessageX, 42);
            g.setColor(Rs.C050050050);
            g.drawString(content, txtMessageX - 1, 41);
        }
    }
    public void setCycleSeconds(long cycleSecs) {
        this.cycleSecs = cycleSecs;
    }
    private void startCycleTimer() {
        stopCycleTimer();
        if (cycleTimer != null) {
            try{
                TVTimer.getTimer().scheduleTimerSpec(cycleTimer);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void stopCycleTimer() {
        if (cycleTimer != null) {
            try{
                TVTimer.getTimer().deschedule(cycleTimer);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    class CycleTimerImpl implements TVTimerWentOffListener {
        public void timerWentOff(TVTimerWentOffEvent arg0) {
            curIdx = (curIdx+1) % ttCnt;
            repaint();
        }
    }
}

package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Component;
import java.awt.Graphics;

import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.util.TextUtil;

public class MenuTreeDescriptor extends Component {
    private static final long serialVersionUID = 6601858557845184360L;
    
    private String[] title;
    private String[] desc;

    public void init() {
    }
    public void dispose() {
    }
    public void start() {
    }
    public void stop() {
    }
    public void paint(Graphics g) {
        int startY = 18;
        if (title != null) {
            g.setFont(Rs.F28);
            g.setColor(Rs.C187189192);
            int titleLth = title.length;
            for (int i=0; i<titleLth; i++) {
                g.drawString(title[i], 2, startY);
                if (i != titleLth - 1) {
                    startY += 25;
                }
            }
            startY += 25;
        }
        if (desc != null) {
            g.setFont(Rs.F15);
            g.setColor(Rs.C143143143);
            for (int i=0; i<desc.length; i++) {
                g.drawString(desc[i], 0, startY);
                startY += 18;
            }
        }
    }
    public void setTitle(String reqTitle) {
        if (reqTitle == null) {
            title = null;
        } else {
            title = TextUtil.split(reqTitle, Rs.FM28, 210, 2);
        }
    }
    public void setDescription(String reqDesc) {
        if (reqDesc == null) {
            desc = null;
        } else {
            desc = TextUtil.split(reqDesc, Rs.FM15, 186, 4);
        }
    }
}

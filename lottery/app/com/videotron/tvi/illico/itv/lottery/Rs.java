package com.videotron.tvi.illico.itv.lottery;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * 
 * @version $Revision: 1.39 $ $Date: 2017/01/12 19:38:29 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*****************************************************s****************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "R7.2.1";//"1.0.25.1";
    /** Application Name. */
    public static final String APP_NAME = "Lotto";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    public static final boolean USE_LOCAL_SERVER_FILE = IS_EMULATOR && false;
    public static final boolean INSTEAD_OF_IB_OOB = IS_EMULATOR && true;
    public static final boolean USE_PROP_DATA_INSTEAD_OF_IB_OOB = false; //MS_DATA or USER_PROP_DATA
    
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    public static final Font F14 = FontResource.BLENDER.getFont(14, true);
    public static final Font F15 = FontResource.BLENDER.getFont(15, true);
    public static final Font F17 = FontResource.BLENDER.getFont(17, true);
    public static final Font F18 = FontResource.BLENDER.getFont(18, true);
    public static final Font F19 = FontResource.BLENDER.getFont(19, true);
    public static final Font F20 = FontResource.BLENDER.getFont(20, true);
    public static final Font F21 = FontResource.BLENDER.getFont(21, true);
    public static final Font F22 = FontResource.BLENDER.getFont(22, true);
    public static final Font F24 = FontResource.BLENDER.getFont(24, true);
    public static final Font F25 = FontResource.BLENDER.getFont(25, true);
    public static final Font F26 = FontResource.BLENDER.getFont(26, true);
    public static final Font F27 = FontResource.BLENDER.getFont(27, true);
    public static final Font F28 = FontResource.BLENDER.getFont(28, true);
    public static final Font F30 = FontResource.BLENDER.getFont(30, true);
    public static final Font F32 = FontResource.BLENDER.getFont(32, true);
    public static final Font F34 = FontResource.BLENDER.getFont(34, true);
    public static final Font F48 = FontResource.BLENDER.getFont(48, true);
    
    public static final FontMetrics FM14 = FontResource.getFontMetrics(F14);
    public static final FontMetrics FM15 = FontResource.getFontMetrics(F15);
    public static final FontMetrics FM17 = FontResource.getFontMetrics(F17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(F18);
    public static final FontMetrics FM19 = FontResource.getFontMetrics(F19);
    public static final FontMetrics FM20 = FontResource.getFontMetrics(F20);
    public static final FontMetrics FM21 = FontResource.getFontMetrics(F21);
    public static final FontMetrics FM22 = FontResource.getFontMetrics(F22);
    public static final FontMetrics FM24 = FontResource.getFontMetrics(F24);
    public static final FontMetrics FM25 = FontResource.getFontMetrics(F25);
    public static final FontMetrics FM26 = FontResource.getFontMetrics(F26);
    public static final FontMetrics FM27 = FontResource.getFontMetrics(F27);
    public static final FontMetrics FM28 = FontResource.getFontMetrics(F28);
    public static final FontMetrics FM30 = FontResource.getFontMetrics(F30);
    public static final FontMetrics FM32 = FontResource.getFontMetrics(F32);
    public static final FontMetrics FM34 = FontResource.getFontMetrics(F34);
    public static final FontMetrics FM48 = FontResource.getFontMetrics(F48);
    
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C035035035 = new Color(35, 35, 35);
    public static final Color C046046046 = new Color(46, 46, 46);
    public static final Color C050050050 = new Color(50, 50, 50);
    public static final Color C056056056 = new Color(56, 56, 56);
    public static final Color C086086086 = new Color(86, 86, 86);
    public static final Color C110110110 = new Color(110, 110, 110);
    public static final Color C132132132 = new Color(132, 132, 132);
    public static final Color C133133133 = new Color(133, 133, 133);
    public static final Color C143143143 = new Color(143, 143, 143);
    public static final Color C160160160 = new Color(160, 160, 160);
    public static final Color C164164164 = new Color(164, 164, 164);
    public static final Color C171171171 = new Color(171, 171, 171);
    public static final Color C180180180 = new Color(180, 180, 180);
    public static final Color C187189192 = new Color(187, 189, 192);
    public static final Color C198198198 = new Color(198, 198, 198);
    public static final Color C210210210 = new Color(210, 210, 210);
    public static final Color C214182055 = new Color(214, 182, 55);
    public static final Color C224224224 = new Color(224, 224, 224);
    public static final Color C236236237 = new Color(236, 236, 237);
    public static final Color C240240240 = new Color(240, 240, 240);
    public static final Color C245212016 = new Color(245, 212, 16);
    public static final Color C249198000 = new Color(249, 198, 0);
    public static final Color C255204000 = new Color(255, 204, 0);
    public static final Color C255234160 = new Color(255, 234, 160);
    public static final Color C255255255 = new Color(255, 255, 255);
    
    public static final  Color DVB007007007077 = new DVBColor(7, 7, 7, 77);
    public static final  Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    public static final  Color DVB255255255230 = new DVBColor(255, 255, 255, 230);
    
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    public static final int KEY_PAGEUP = OCRcEvent.VK_PAGE_UP;
    public static final int KEY_PAGEDOWN = OCRcEvent.VK_PAGE_DOWN;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, SCREEN_SIZE_WIDTH, SCREEN_SIZE_HEIGHT);
    /*********************************************************************************
     * Image key-related
     *********************************************************************************/
    public static final String IMG_KEY_PROMOTIONAL_BANNER = "PB_";
    public static final String IMG_KEY_SUMMARY_RESULT = "SR_";
    public static final String IMG_KEY_DETAILED_RESULT = "DR_";
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
    public static final String ERROR_CODE_SERVER_ACCESS_PROBLEM = "LOT500";
    public static final String ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM = "LOT501";
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM= "LOT502";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "LOT503";
}

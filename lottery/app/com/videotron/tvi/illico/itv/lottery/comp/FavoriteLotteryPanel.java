package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Calendar;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.log.Log;

public class FavoriteLotteryPanel extends Container {
    private static final long serialVersionUID = 6708212178693793073L;
    private DataManager dMgr = DataManager.getInstance();
    /*********************************************************************************
     * Timer-related
     *********************************************************************************/
    private long cycleSecs = -1;
    private TVTimerSpec cycleTimer;
    private CycleTimerImpl cycleTimerImpl;
    
    public static final int FAVOTITE_LOTTERY_MAX_COUNT = 5;
    
    public static final int BANNER_TYPE_NORMAL = 0;
    public static final int BANNER_TYPE_PREFERENCE = 1;
    
    private Image imgArrowTop;
    private Image imgArrowBot;
    private Image imgFocus;
    private Image imgBanner;
    
    private Lottery[] favLots;
    private FavoriteLotteryData[] favLotList;
    private int ttCnt;
    private int curIdx;
    
    private boolean readyComponent;
    private boolean isFocusComponent;
    private FavoriteLotteryPanelListener l;
    
    public void init() {
        imgArrowTop = ImageManager.getInstance().getImage("11_widget_t.png", ImageManager.TYPE_TEMPORARY);
        imgArrowBot = ImageManager.getInstance().getImage("11_widget_b.png", ImageManager.TYPE_TEMPORARY);
        imgFocus = ImageManager.getInstance().getImage("00_banner_focus_long.png", ImageManager.TYPE_TEMPORARY);
        imgBanner = ImageManager.getInstance().getImage("11_lotto_banner_fav.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        if (cycleTimer != null) {
            if (cycleTimerImpl != null) {
                cycleTimer.removeTVTimerWentOffListener(cycleTimerImpl);
                cycleTimerImpl = null;
            }
            cycleTimer = null;
        }
        favLotList = null;
        imgBanner = null;
        imgFocus = null;
        imgArrowBot = null;
        imgArrowTop = null;
    }
    public void start() {
        new Thread() {
            public void run() {
                readyComponent = false;
                try{
                    favLots = dMgr.getFavoriteLotteryList();
                }catch(RPManagerException rpe) {
                    CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
                }
                Image[] favLotSummaryResImgs = null;
                if (favLots != null) {
                    int favLotsLth = favLots.length;
                    favLotSummaryResImgs = new Image[favLotsLth];
                    for (int i=0; i<favLotsLth; i++) {
//                        if (favLots[i] == null || favLots[i].isInvalidLottery()) {
//                            continue;
//                        }
                        if (favLots[i] == null) {
                            continue;
                        }
//                        String favLotProdNo = favLots[i].getProductNumber();
//                        Calendar latestDrawDate = favLots[i].getLatestDrawingDay();
//                        try{
//                            favLotSummaryResImgs[i] = dMgr.getSummaryResultImage(favLotProdNo, latestDrawDate);
//                        }catch(RPManagerException ignore) {
//                            ignore.printStackTrace();
//                        }
                        String favLotProdNo = favLots[i].getProductNumber();
                        try{
                            favLotSummaryResImgs[i] = dMgr.getSummaryResultImage(favLotProdNo);
                        }catch(RPManagerException ignore) {
                            ignore.printStackTrace();
                        }
                    }
                }
                setFavoriteLotteryData(favLots, favLotSummaryResImgs);
                curIdx = 0;
                readyComponent = true;
                repaint();
            }
        }.start();
    }
    public void stop() {
        
        favLotList = null;
        ttCnt = 0;
    }
    public boolean keyAction(int keyCode) {
        if (ttCnt == 0) {
            return true;
        }
        int tempValue = -1;
        switch(keyCode) {
            case Rs.KEY_UP:
                tempValue = (ttCnt + curIdx - 1) % ttCnt;
                if (tempValue != curIdx) {
                    curIdx = tempValue;
                    repaint();
                }
                return true;
            case Rs.KEY_DOWN:
                tempValue = (curIdx + 1) % ttCnt;
                if (tempValue != curIdx) {
                    curIdx = tempValue;
                    repaint();
                }
                return true;
            case Rs.KEY_LEFT:
                if (l != null) {
                    l.lostFocusFavoriteLotteryPanel();
                }
                return true;
            case Rs.KEY_RIGHT:
                return true;
            case Rs.KEY_OK:
                if (l != null && favLotList[curIdx] != null) {
                    ClickingEffect effect = new ClickingEffect(this, 5);
                    effect.updateBackgroundBeforeStart(false);
                    effect.start(2, 12, 478, 72);
                    int banType = favLotList[curIdx].getFavoriteLotteryType(); 
                    String prodNum = favLotList[curIdx].getFavoriteLotteryProductNumber();
                    l.selectedFavoriteLottery(banType, prodNum);
                }
                return true;  
        }
        return false;
    }
    public void paint(Graphics g){
        final int x = 368;
        final int y = 375;
        String txtMMFL = DataCenter.getInstance().getString("TxtLottery.Favourite_Lotteries");
        if (txtMMFL != null) {
            g.setFont(Rs.F19);
            g.setColor(Rs.C255255255);
            g.drawString(txtMMFL, 376 - 368, 385 - 375);
        }
        if (!readyComponent) {
            String txtLoadingData = DataCenter.getInstance().getString("TxtLottery.Loading_Data");
            paintBlankPanel(g, txtLoadingData, Rs.F28, Rs.FM28);
            return;
        }
        if (favLotList != null && favLotList.length > 0 && favLotList.length > curIdx) {
            if (favLotList[curIdx] != null) {
                Image favLotSummaryImg = favLotList[curIdx].getFavoriteLotterySummaryImage();
                if (favLotSummaryImg != null) {
                    g.drawImage(favLotSummaryImg, 375 - x, 392 - y, 468, 62, this);
                    int favLotType = favLotList[curIdx].getFavoriteLotteryType();
                    if (favLotType == BANNER_TYPE_PREFERENCE) {
                        String txtDesc = DataCenter.getInstance().getString("TxtLottery.Favorite_Lotteries_Desc");
                        if (txtDesc != null) {
                            g.setFont(Rs.F18);
                            g.setColor(Rs.C255255255);
                            g.drawString(txtDesc, 609 - 368 -(Rs.FM18.stringWidth(txtDesc)/2), 427 - 375);
                        }
                    }
                } else {
                    String txtDisp = DataCenter.getInstance().getString("TxtLottery.No_Summary_Result_Image");
                    String lotName = favLotList[curIdx].getFavoriteLotteryName();
                    if (lotName != null) {
                        txtDisp += " ("+lotName+")";
                    }
                    paintBlankPanel(g, txtDisp, Rs.F20, Rs.FM20);
                }
            } 
        }
        if (isFocusComponent) {
            if (imgFocus != null) {
                g.drawImage(imgFocus, 368- x, 387 - y, this);
            }
            if (ttCnt > 1 && imgArrowBot != null) {
                g.drawImage(imgArrowBot, 595- x, 454 - y, this);
            }
            if (ttCnt > 1 && imgArrowTop != null) {
                g.drawImage(imgArrowTop, 595- x, 375 - y, this);
            }
        }
    }
    private void paintBlankPanel(Graphics g, String content, Font f, FontMetrics fm) {
        if (content != null) {
            final int x = 368;
            final int y = 375;
            g.setColor(Rs.C086086086);
            g.fillRect(375 - x, 392 - y, 468, 62);
            g.setColor(Rs.C050050050);
            g.drawRect(375 - x, 392 - y, 467, 61);
            int w = getWidth();
            int txtLoadingDataWth = fm.stringWidth(content);
            int txtLoadingDataX = (w-txtLoadingDataWth)/2;
            g.setFont(f);
            g.setColor(Rs.C110110110);
            g.drawString(content, txtLoadingDataX, 56);
            g.setColor(Rs.C050050050);
            g.drawString(content, txtLoadingDataX - 1, 55);
        }
    }
    /*********************************************************************************
     * FavoriteLotteryListener-related
     *********************************************************************************/
    public void setFavoriteLotteryData(Lottery[] favLots, Image[] favLotSummaries) {
        FavoriteLotteryData favLotPref = new FavoriteLotteryData();
        favLotPref.setFavoriteLotterySummaryImage(imgBanner);
        favLotPref.setFavoriteLotteryType(BANNER_TYPE_PREFERENCE);
        if (favLotSummaries == null) {
            favLotList = new FavoriteLotteryData[]{favLotPref};
        } else {
            ArrayList temp=new ArrayList();
            int favLotSummariesLth = favLotSummaries.length;
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryDetailedResultUI.setFavoriteLotteryData]favLotSummariesLth : "+favLotSummariesLth);
            }
            for (int i=0; i<favLotSummariesLth; i++) {
                if (favLotSummaries[i]==null) { 
                    continue;
                }
                FavoriteLotteryData favLot = new FavoriteLotteryData();
                favLot.setFavoriteLotterySummaryImage(favLotSummaries[i]);
                favLot.setFavoriteLotteryType(BANNER_TYPE_NORMAL);
                if (favLots[i] != null) {
                    favLot.setFavoriteLotteryProductNumber(favLots[i].getProductNumber());
                    favLot.setFavoriteLotteryName(favLots[i].getLotteryProductName());
                }
                temp.add(favLot);
            }
            if (favLotSummariesLth < FAVOTITE_LOTTERY_MAX_COUNT) {
                temp.add(favLotPref);
            }
            int tempSz=temp.size();
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryDetailedResultUI.setFavoriteLotteryData]tempSz : "+tempSz);
            }
            favLotList=new FavoriteLotteryData[tempSz];
            favLotList=(FavoriteLotteryData[])temp.toArray(favLotList);
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryDetailedResultUI.setFavoriteLotteryData]cycleSecs : "+cycleSecs);
            }
            if (tempSz > 1 && cycleSecs > 0) {
                if (cycleTimer == null) {
                    cycleTimer = new TVTimerSpec();
                    if (cycleTimerImpl == null) {
                        cycleTimerImpl = new CycleTimerImpl();
                    }
                    cycleTimer.setDelayTime(cycleSecs * 1000);
                    cycleTimer.setRepeat(true);
                    cycleTimer.addTVTimerWentOffListener(cycleTimerImpl);
                }
                startCycleTimer();
            }
        }
        ttCnt = favLotList.length;
    }
    public void setComponentListener(FavoriteLotteryPanelListener l) {
        this.l = l;
    }
    public void removeComponentListener() {
        this.l = null;
    }
    /*********************************************************************************
     * Focus-related
     *********************************************************************************/
    public boolean isReadyComponent() {
        return readyComponent;
    }
    public void setFocusComponent(boolean isFocusComponent) {
        this.isFocusComponent = isFocusComponent;
    }
    public boolean isFocusComponent() {
        return isFocusComponent;
    }
    
    class FavoriteLotteryData {
        private String favLotProdNum;
        private String favLotName;
        private Image favLotSummaryImg;
        private int favLotType;
        
        public String getFavoriteLotteryName() {
            return favLotName;
        }
        public void setFavoriteLotteryName(String favLotName) {
            this.favLotName = favLotName;
        }

        public String getFavoriteLotteryProductNumber() {
            return favLotProdNum;
        }
        public void setFavoriteLotteryProductNumber(String favLotProdNum) {
            this.favLotProdNum = favLotProdNum;
        }
        public Image getFavoriteLotterySummaryImage() {
            return favLotSummaryImg;
        }
        public void setFavoriteLotterySummaryImage(Image favLotSummaryImg) {
            this.favLotSummaryImg = favLotSummaryImg;
        }
        public int getFavoriteLotteryType() {
            return favLotType;
        }
        public void setFavoriteLotteryType(int favLotType) {
            this.favLotType = favLotType;
        }
    }
    /*********************************************************************************
     * Timer-related
     *********************************************************************************/
    public void setCycleSeconds(long cycleSecs) {
        this.cycleSecs = cycleSecs;
    }
    private void startCycleTimer() {
        stopCycleTimer();
        if (cycleTimer != null) {
            try {
                TVTimer.getTimer().scheduleTimerSpec(cycleTimer);    
            } catch (TVTimerScheduleFailedException tvtsfe) {
                tvtsfe.printStackTrace();
            }
        }
    }
    private void stopCycleTimer() {
        if (cycleTimer != null) {
            TVTimer.getTimer().deschedule(cycleTimer);
        }
    }
    class CycleTimerImpl implements TVTimerWentOffListener{
        public void timerWentOff(TVTimerWentOffEvent event) {
            if (ttCnt<=0) {
                return;
            }
            int tempValue = (curIdx + 1) % ttCnt;
            if (tempValue != curIdx) {
                curIdx = tempValue;
                repaint();
            }            
        }
    }
}

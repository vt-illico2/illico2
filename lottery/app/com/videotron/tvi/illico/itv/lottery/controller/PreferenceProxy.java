package com.videotron.tvi.illico.itv.lottery.controller;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.<p>
 * Try to look up UPP Service at the time of initializing this class.
 * If successful, upp data get from the PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    private static PreferenceProxy instance;
    private PreferenceService prefSvc;

    private static final String UPP_KEY_FAVORITE_LOTTERIES = "LOTTERY_FAVORITE_LOTTERIES";
    private static final String UPP_KEY_DISPLAY_TITLE_RESULT_FIRST = "LOTTERY_DISPLAY_TITLE_RESULT_FIRST";
    private static final int UPP_INDEX_COUNT = 3;
    private static final int UPP_INDEX_LANGUAGE = 0;
    private static final int UPP_INDEX_FAVORITE_LOTTERIES = 1;
    private static final int UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST = 2;
    private String[] prefNames;
    private String[] prefValues;

    private PreferenceProxy() {
        init();
    }
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }
    public void init() {
        if (prefNames == null) {
            prefNames = new String[UPP_INDEX_COUNT];
            prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
            prefNames[UPP_INDEX_FAVORITE_LOTTERIES] = UPP_KEY_FAVORITE_LOTTERIES;
            prefNames[UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST] = UPP_KEY_DISPLAY_TITLE_RESULT_FIRST;
        }
        if (prefValues == null) {
            prefValues = new String[UPP_INDEX_COUNT];
            prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_FRENCH;
            prefValues[UPP_INDEX_FAVORITE_LOTTERIES] = "";
            prefValues[UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST] = "";
        }
    }
    public void dispose() {
        if (prefSvc != null) {
            try{
                prefSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception ignore) {
            }
            prefSvc = null;
        }
        prefNames = null;
        prefValues = null;
        instance = null;
    }
    public void start() {
        if (prefSvc == null) {
            setPreferenceService();
        }
    }
    public void stop() {
    }
    private void checkPreferenceService() {
        if (prefSvc == null) {
            setPreferenceService();
        }
    }
    private void setPreferenceService() {
        try {
            String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
            prefSvc = (PreferenceService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
        } catch (Exception e) {
            if (Log.INFO_ON) {
                Log.printInfo("[PreferenceProxy.setPreferenceService]not bound - " + PreferenceService.IXC_NAME);
            }
        }
        if (prefSvc != null) {
            String[] uppData = null;
            try {
                uppData = prefSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues) ;
                receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
                receiveUpdatedPreference(UPP_KEY_FAVORITE_LOTTERIES, uppData[UPP_INDEX_FAVORITE_LOTTERIES]);
                receiveUpdatedPreference(UPP_KEY_DISPLAY_TITLE_RESULT_FIRST, uppData[UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private PreferenceService getPreferenceService() {
        checkPreferenceService();
        return prefSvc;
    }
    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE , value);
            prefValues[UPP_INDEX_LANGUAGE] = value;
            DataManager.getInstance().updatedPreferenceLanguage();
        } else if (name.equals(UPP_KEY_FAVORITE_LOTTERIES)) {
            prefValues[UPP_INDEX_FAVORITE_LOTTERIES] = value;
        } else if (name.equals(UPP_KEY_DISPLAY_TITLE_RESULT_FIRST)) {
            if (!value.equals(Definitions.LANGUAGE_FRENCH) && !value.equals(Definitions.LANGUAGE_ENGLISH)) {
                setDisplayTitleResultFirst(prefValues[UPP_INDEX_LANGUAGE]);
            } else {
                prefValues[UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST] = value;
            }
        }
    }
    public String getFavoriteLotteryProductNumbers() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_FAVORITE_LOTTERIES];
    }
    public boolean setFovoriteLotteryProductNumbers(String reqData) {
        if (reqData == null) {
            return false;
        }
        boolean result = false;
        PreferenceService pSvc = getPreferenceService();
        if (pSvc != null) {
            try{
                result = pSvc.setPreferenceValue(UPP_KEY_FAVORITE_LOTTERIES, reqData);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public String getLanguage() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_LANGUAGE];
    }
    public String getDisplayTitleResultFirst() {
        checkPreferenceService();
        return prefValues[UPP_INDEX_DISPLAY_TITLE_RESULT_FIRST];
    }
    public boolean setDisplayTitleResultFirst(String reqData) {
        if (reqData == null) {
            return false;
        }
        PreferenceService pSvc = getPreferenceService();
        boolean result = false;
        if (pSvc != null) {
            try{
                result = pSvc.setPreferenceValue(UPP_KEY_DISPLAY_TITLE_RESULT_FIRST, reqData);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}

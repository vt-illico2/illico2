package com.videotron.tvi.illico.itv.lottery.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.Breadcrumbs;
import com.videotron.tvi.illico.itv.lottery.comp.FavoriteLotteryPanel;
import com.videotron.tvi.illico.itv.lottery.comp.FavoriteLotteryPanelListener;
import com.videotron.tvi.illico.itv.lottery.comp.LegalDisclaimer;
import com.videotron.tvi.illico.itv.lottery.comp.MenuTree;
import com.videotron.tvi.illico.itv.lottery.comp.MenuTreeDescriptor;
import com.videotron.tvi.illico.itv.lottery.comp.MenuTreeItem;
import com.videotron.tvi.illico.itv.lottery.comp.MenuTreeListener;
import com.videotron.tvi.illico.itv.lottery.comp.PromotionalBanner;
import com.videotron.tvi.illico.itv.lottery.controller.DataManager;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.itv.lottery.controller.RPManagerException;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.gui.LotteryMainRenderer;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;

public class LotteryMainUI extends BaseUI implements MenuTreeListener, FavoriteLotteryPanelListener, MenuListener {
    private static final long serialVersionUID = -4152969367611819304L;
    /*********************************************************************************
     * Component-related
     *********************************************************************************/
    private Footer footer;
    private Breadcrumbs bc;
    private String[] bcTxts;
    private FavoriteLotteryPanel favLotPnl;
    private PromotionalBanner promoBanner;
    private LegalDisclaimer legalDisclaimer;
    /*********************************************************************************
     * Option-related
     *********************************************************************************/
    private OptionScreen optScr;
    private static final MenuItem OPTION_PREFERENCES = new MenuItem("TxtLottery.Option_Preference");
    private static final MenuItem OPTION_HELP = new MenuItem("TxtLottery.Option_Help");
    /*********************************************************************************
     * TxtKey-related
     *********************************************************************************/
    private static final int MENU_ITEM_COUNT = 2;
    private static final int MENU_INDEX_LATEST_RESULT = 0;
    private static final int MENU_INDEX_PREFERENCE = 1;
    private final String txtKeyMenuTreeTitle = "TxtLottery.Lottery";
    private Image imgMenuTreeIcon;
    private MenuTree menuTree;
    private MenuTreeDescriptor mtDesc;
    private MenuTreeItem[] mtItemList;
    private final String[] txtKeyMenuItemTitles = { "TxtLottery.Latest_Results", "TxtLottery.Preferences" };
    private final static String[] TXT_KEY_MENU_TITLES = { "TxtLottery.Latest_Results", "TxtLottery.Preferences" };
    private final static String[] TXT_KEY_MENU_DESCS = { "TxtLottery.Desc_Latest_Results",
            "TxtLottery.Desc_Preferences" };
    private int curMenuIdx;

    private ClickingEffect clickEffect;

    protected void initScene() {
        renderer = new LotteryMainRenderer();
        setRenderer(renderer);
        if (bc == null) {
            bc = new Breadcrumbs();
            bc.setBounds(52, 17, 590, 72);
            add(bc);
        }
        if (footer == null) {
            footer = new Footer();
            add(footer);
        }
        if (bcTxts == null) {
            bcTxts = new String[1];
        }
        if (legalDisclaimer == null) {
            legalDisclaimer = new LegalDisclaimer();
            legalDisclaimer.init();
            legalDisclaimer.setBounds(57, 487, 600, 30);
            legalDisclaimer.setTextKeyLegalDisclaimer("TxtLottery.Desc_Legal_Disclaimer");
            legalDisclaimer.setVisible(true);
            add(legalDisclaimer, 0);
        }
        if (mtItemList == null) {
            mtItemList = new MenuTreeItem[MENU_ITEM_COUNT];
            for (int i = 0; i < MENU_ITEM_COUNT; i++) {
                mtItemList[i] = new MenuTreeItem();
                mtItemList[i].setMenuTreePrimaryKey(i);
                mtItemList[i].setMenuTreeTitleTextKey(txtKeyMenuItemTitles[i]);
            }
        }
        if (imgMenuTreeIcon == null) {
            imgMenuTreeIcon = ImageManager.getInstance().getImage("lotto.png", ImageManager.TYPE_TEMPORARY);
        }
        if (menuTree == null) {
            menuTree = new MenuTree();
            menuTree.init();
            menuTree.setBounds(0, 0, 960, 540);
            menuTree.setMenuTreeTitleKey(txtKeyMenuTreeTitle);
            menuTree.setMenuTreeIconImage(imgMenuTreeIcon);
            menuTree.setMenuTreeItems(mtItemList);
            menuTree.setMenuTreeListener(this);
        }
        if (mtDesc == null) {
            mtDesc = new MenuTreeDescriptor();
            mtDesc.init();
            mtDesc.setBounds(373, 79, 204, 166);
            add(mtDesc);
        }
        if (favLotPnl == null) {
            favLotPnl = new FavoriteLotteryPanel();
            favLotPnl.init();
            favLotPnl.setBounds(368, 375, 483, 95);
            favLotPnl.setComponentListener(this);
            String tempCycleSecs = DataCenter.getInstance().getString("FAVORITE_LOTTERY_CYCLE_SECONDS");
            long cycleSecs = -1;
            try{
                cycleSecs = Long.parseLong(tempCycleSecs);
            } catch (NumberFormatException nfe) {
                nfe.printStackTrace();
            }
            favLotPnl.setCycleSeconds(cycleSecs);
            add(favLotPnl);
        }
        if (promoBanner == null) {
            promoBanner = new PromotionalBanner();
            promoBanner.init();
            promoBanner.setBounds(374, 291, 468, 62);
            String tempCycleSecs = DataCenter.getInstance().getString("PROMOTIONAL_BANNER_CYCLE_SECONDS");
            long cycleSecs = -1;
            try {
                cycleSecs = Long.parseLong(tempCycleSecs);
            } catch (Exception ignore) {
            }
            promoBanner.setCycleSeconds(cycleSecs);
            add(promoBanner);
        }
        if (optScr == null) {
            optScr = new OptionScreen();
        }
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
            clickEffect.setClipBounds(0, 0, 360, 540);
        }
    }
    protected void disposeScene() {
        clickEffect = null;
        optScr = null;
        if (promoBanner == null) {
            remove(promoBanner);
            promoBanner.dispose();
            promoBanner = null;
        }
        if (favLotPnl != null) {
            remove(favLotPnl);
            favLotPnl.removeComponentListener();
            favLotPnl.dispose();
            favLotPnl = null;
        }
        if (mtDesc != null) {
            remove(mtDesc);
            mtDesc.dispose();
            mtDesc = null;
        }
        if (menuTree != null) {
            remove(menuTree);
            menuTree.removeMenuTreeListener();
            menuTree.dispose();
            menuTree = null;
        }
        if (legalDisclaimer != null) {
            remove(legalDisclaimer);
            legalDisclaimer.dispose();
            legalDisclaimer = null;
        }
        if (bc != null) {
            remove(bc);
            bc.dispose();
            bc = null;
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }   
        renderer = null;
    }
    protected void startScene(boolean resetScene, String[] params) {
        prepare();
        if (resetScene) {
            curMenuIdx = 0;
            if (menuTree != null) {
                menuTree.setFocusComponent(true);
            }
            if (favLotPnl != null) {
                favLotPnl.setFocusComponent(false);
            }
            setVisible(false);
            AlphaEffect fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
            fadeIn.start();
            if (menuTree != null) {
                add(menuTree);
                menuTree.start(true);
            }
        } else {
            if (menuTree != null) {
                add(menuTree);
            }
            if (favLotPnl.isFocusComponent()) {
                curMenuIdx = 0;
                if (menuTree != null) {
                    menuTree.setFocusComponent(true);
                }
                if (favLotPnl != null) {
                    favLotPnl.setFocusComponent(false);
                }
                if (favLotPnl != null) {
                    favLotPnl.start();
                }
                if (promoBanner != null) {
                    promoBanner.start();
                }
                setMenuTreeDescription();
            }
        }
        if (bc != null) {
            bcTxts[0] = DataCenter.getInstance().getString("TxtLottery.Lottery");
            bc.start(bcTxts);
        }
        if (legalDisclaimer != null) {
            legalDisclaimer.start();
        }
        if (favLotPnl != null) {
            favLotPnl.start();
        }
        if (promoBanner != null) {
            promoBanner.start();
        }
        setMenuTreeDescription();
        if (resetScene) {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    CommunicationManager.getInstance().requestChangeCurrentChannel();
                    CommunicationManager.getInstance().requestResizeScreen(587, 72, 322, 181);
                }
            });
        } else {
            CommunicationManager.getInstance().requestResizeScreen(587, 72, 322, 181);
        }
        if (footer != null) {
            footer.reset();
            footer.addButton(PreferenceService.BTN_EXIT, "TxtLottery.Exit");
            footer.addButton(PreferenceService.BTN_D, "TxtLottery.Options");
        }
    }

    protected void stopScene() {
        if (menuTree != null) {
            remove(menuTree);
            menuTree.stop();
        }
        if (optScr != null) {
            optScr.stop();
        }
        if (promoBanner != null) {
            promoBanner.stop();
        }
        if (favLotPnl != null) {
            favLotPnl.stop();
        }
        if (legalDisclaimer != null) {
            legalDisclaimer.stop();
        }
        if (bc != null) {
            bc.stop();
        }
    }

    protected boolean keyAction(int keyCode) {
        if (keyCode == KeyCodes.LAST) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryMainUI.keyAction]keyCode is KeyCodes.LAST.");
            }
            boolean isCalledFromMenu = CommunicationManager.getInstance().isCalledFromMenu();
            if (isCalledFromMenu) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LotteryMainUI.keyAction]Return to Menu app.");
                }
                CommunicationManager.getInstance().requestStartUnboundApplication("Menu", null);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LotteryMainUI.keyAction]No action about last key.");
                }
            }
            return true;
        }
        switch(keyCode) {
            case KeyCodes.SEARCH:
                return true;
            case Rs.KEY_EXIT:
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Exit");
                }
                CommunicationManager.getInstance().requestExitToChannel();
                return true;
            case KeyCodes.COLOR_D:
                if (footer != null) {
                    footer.clickAnimation("TxtLottery.Options");
                }
                MenuItem root = new MenuItem("LotteryMain");
                root.add(OPTION_PREFERENCES);
                root.add(OPTION_HELP);
                optScr.start(root, this);
                return true;
        }
        if (menuTree != null && menuTree.isFocusComponent()) {
            return menuTree.keyAction(keyCode);
        }
        if (favLotPnl != null && favLotPnl.isFocusComponent()) {
            return favLotPnl.keyAction(keyCode);
        }
        return false;
    }

    /*********************************************************************************
     * Effect-related
     *********************************************************************************/
    public void animationEnded(Effect effect) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryMainUI.keyAction]a");
        }
        setVisible(true);
    }

    /*********************************************************************************
     * Renderer-related
     *********************************************************************************/
    public void setMenuTreeDescription() {
        if (mtDesc != null) {
            mtDesc.setTitle(DataCenter.getInstance().getString(TXT_KEY_MENU_TITLES[curMenuIdx]));
            mtDesc.setDescription(DataCenter.getInstance().getString(TXT_KEY_MENU_DESCS[curMenuIdx]));
        }
    }

    /*********************************************************************************
     * MenuTreeListener-implemented
     *********************************************************************************/
    public void selectedMenuTreeItem(MenuTreeItem selectedItem, Rectangle selItemArea) {
        if (selectedItem == null) {
            return;
        }
        if (selItemArea != null) {
            if (clickEffect != null) {
                clickEffect.start(selItemArea.x, selItemArea.y, selItemArea.width, selItemArea.height);
            }
        }
        int pKey = selectedItem.getMenuTreePrimaryKey();
        switch (pKey) {
        case MENU_INDEX_LATEST_RESULT:
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_SUMMARY_RESULTS, true, null);
            break;
        case MENU_INDEX_PREFERENCE:
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE, true, null);
            break;
        }
    }

    public void lostMenuTreeFocus() {
        if (menuTree != null) {
            menuTree.setFocusComponent(false);
        }
        if (favLotPnl != null) {
            if (favLotPnl.isReadyComponent()) {
                favLotPnl.setFocusComponent(true);
            } else {
                if (menuTree != null) {
                    menuTree.setFocusComponent(true);
                }
            }
        }
        setMenuTreeDescription();
        repaint();
    }

    public void changedMenuTreeItem(MenuTreeItem changedItem) {
        if (changedItem == null) {
            return;
        }
        curMenuIdx = changedItem.getMenuTreePrimaryKey();
        setMenuTreeDescription();
        repaint();
    }

    /*********************************************************************************
     * FavoriteLotteryPanelListener-implemented
     *********************************************************************************/
    public void lostFocusFavoriteLotteryPanel() {
        if (menuTree != null) {
            menuTree.setFocusComponent(true);
        }
        if (favLotPnl != null) {
            favLotPnl.setFocusComponent(false);
        }
        setMenuTreeDescription();
        repaint();
    }

    public void selectedFavoriteLottery(int bannerType, String prodNum) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryMainUI.selectedFavoriteLottery]Param-Banner type : " + bannerType);
            Log.printDebug("[LotteryMainUI.selectedFavoriteLottery]Param-Product number : " + prodNum);
        }
        switch (bannerType) {
        case FavoriteLotteryPanel.BANNER_TYPE_NORMAL:
            if (prodNum == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LotteryMainUI.selectedFavoriteLottery]Selected favorite lottery is null.");
                }
                return;
            }
            
            Lottery curLot = null;
            try{
                curLot = DataManager.getInstance().getLotteryByProductNumber(prodNum);
            }catch(RPManagerException rpe) {
                CommunicationManager.getInstance().requestShowErrorMessage(rpe.getMessage());
            }
            
            if (curLot == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LotteryMainUI.selectedFavoriteLottery]Current lottery is null.");
                }
                return;
            }
            
        	curLot.setDrawingDate();	
            int drawingDayCnt = curLot.getDrawingDayCount();
            if (drawingDayCnt <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LotteryMainUI.selectedFavoriteLottery]Drawing day is zero or less.");
                }
                return;
            }
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_DETAILED_RESULTS, true, new String[] { prodNum });
            break;
        case FavoriteLotteryPanel.BANNER_TYPE_PREFERENCE:
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE, true, null);
            break;
        }
    }
    /*********************************************************************************
     * MenuListener-implemented
     *********************************************************************************/
    public void selected(MenuItem item) {
        if (item == null) {
            if (optScr != null) {
                optScr.stop();
            }
            return;
        }
        if (item.equals(OPTION_PREFERENCES)) {
            SceneManager.getInstance().goToNextScene(SceneTemplate.SCENE_ID_LOTTERY_PREFERENCE, true, null);
        } else if (item.equals(OPTION_HELP)) {
            String appNameHelp = DataCenter.getInstance().getString("APP_NAME_HELP");
            CommunicationManager.getInstance().requestStartUnboundApplication(appNameHelp, new String[] {null, "Lotto"});
        }
    }
    public void canceled() {
        if (optScr != null) {
            optScr.stop();
        }
    }
}

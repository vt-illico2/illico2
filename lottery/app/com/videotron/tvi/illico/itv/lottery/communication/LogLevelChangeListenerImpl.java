package com.videotron.tvi.illico.itv.lottery.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.log.Log;

public class LogLevelChangeListenerImpl implements LogLevelChangeListener {
    public void logLevelChanged(int logLevel) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LogLevelChangeListenerImpl.logLevelChanged]called.");
            Log.printDebug("[LogLevelChangeListenerImpl.logLevelChanged]chaged log level : "+logLevel);
        }
        Log.setStcLevel(logLevel);
    }
}
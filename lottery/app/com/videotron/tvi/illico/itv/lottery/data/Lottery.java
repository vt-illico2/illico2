package com.videotron.tvi.illico.itv.lottery.data;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.log.Log;

public class Lottery {
	private static final int VALID_DRAWING_DAY_COUNT = 8; //8개 이하로 
	/***************************************************************
	 * XML data
	 ****************************************************************/
	private String prodNum; // Product number
	private String prodCode; // Product code
	private String lotProdName; // Lottery product name
	private String relatedLotProduct1; // Related lottery product 1
	private String relatedLotProduct2; // Related lottery product 2
	private final Vector lotFreqVec; //<Integer> type
	private boolean isSpecificTermLottery = false; //Period or singday exist or not.(Every day or not)
	private final Vector singleDayVec; //<Calendar> type
	private final Vector periodVec; //<Lottery.Period> type
	// private Date[] specialDates;
	// private LotteryPeriod[] periods;

	/***************************************************************
	 * 
	 ****************************************************************/
	private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd");
	private Calendar[] drawingDayCals; // Consisted drawing date array //Desc
	private Calendar latestSuccessCal; // latest success date
	
	public Lottery() {
		singleDayVec = new Vector(); // Calendar type of <Date> tags
		periodVec = new Vector(); // Calendar type of <Period> tags

		lotFreqVec = new Vector();
		lotFreqVec.add(new Integer(Calendar.SUNDAY));
		lotFreqVec.add(new Integer(Calendar.MONDAY));
		lotFreqVec.add(new Integer(Calendar.TUESDAY));
		lotFreqVec.add(new Integer(Calendar.WEDNESDAY));
		lotFreqVec.add(new Integer(Calendar.THURSDAY));
		lotFreqVec.add(new Integer(Calendar.FRIDAY));
		lotFreqVec.add(new Integer(Calendar.SATURDAY));
	}

	public void setProductNumber(String prodNum) {
		this.prodNum = prodNum;
	}

	public String getProductNumber() {
		return prodNum;
	}

	public void setProductCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getProductCode() {
		return prodCode;
	}

	public void setLotteryProductName(String lotProdName) {
		this.lotProdName = lotProdName;
	}

	public String getLotteryProductName() {
		return lotProdName;
	}

	public void setRelatedLotteryProduct1(String relatedLotProduct1) {
		this.relatedLotProduct1 = relatedLotProduct1;
	}

	public String getRelatedLotteryProduct1() {
		return relatedLotProduct1;
	}

	public void setRelatedLotteryProduct2(String relatedLotProduct2) {
		this.relatedLotProduct2 = relatedLotProduct2;
	}

	public String getRelatedLotteryProduct2() {
		return relatedLotProduct2;
	}

	public void setLotteryFrequency(String lotteryFrequency) {
		if (lotteryFrequency != null) {
			lotteryFrequency = lotteryFrequency.trim();
			if (lotteryFrequency.equals("*")) {
				lotFreqVec.add(new Integer(Calendar.SUNDAY));
				lotFreqVec.add(new Integer(Calendar.MONDAY));
				lotFreqVec.add(new Integer(Calendar.TUESDAY));
				lotFreqVec.add(new Integer(Calendar.WEDNESDAY));
				lotFreqVec.add(new Integer(Calendar.THURSDAY));
				lotFreqVec.add(new Integer(Calendar.FRIDAY));
				lotFreqVec.add(new Integer(Calendar.SATURDAY));
			} else {
				lotFreqVec.clear();
				StringTokenizer st = new StringTokenizer(lotteryFrequency, " ");
				int stCnt = st.countTokens();
				for (int i = 0; i < stCnt; i++) {
					String token = st.nextToken();
					if (token != null) {
						try {
							lotFreqVec.add(new Integer(token));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	public void addSingleDay(String singleDayStr) {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.addSingleDay]["+lotProdName+"]Single day = " + singleDayStr);
		}
		isSpecificTermLottery = true;
		Calendar singleDayCal = convertDateStringToCalendar(singleDayStr);
		if (singleDayCal != null && !singleDayVec.contains(singleDayCal)) {
			singleDayVec.add(singleDayCal);
			Collections.sort(singleDayVec, new Comparator() {
				public int compare(Object o1, Object o2) {
					Calendar c1 = (Calendar)o1;
					Calendar c2 = (Calendar)o2;
					if (c1.equals(c2)) {
						return 0;
					}
					return c1.after(c2) ? 1 : -1;
				}
			} );
		}
	}

	public void addPeriod(String beginDateStr, String endDateStr) {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.addPeriod]["+lotProdName+"]Begin Date = " + beginDateStr + "End Date = " + endDateStr);
		}
		isSpecificTermLottery = true;
		Calendar reqBeginCal = convertDateStringToCalendar(beginDateStr);
		Calendar reqEndCal = convertDateStringToCalendar(endDateStr);
		
		// Period is valid.
		int periodVecSz = periodVec.size();
		if (periodVecSz > 0) {
			for (int i = 0; i < periodVecSz; i++) {
				Period period = (Period)periodVec.get(i);
				Calendar beginCal = period.getBeginDate();
				Calendar endCal = period.getEndDate();
				if (compareCalendar(beginCal, reqBeginCal) != 0 && compareCalendar(endCal, reqEndCal)!=0) {
					periodVec.add(new Period(reqBeginCal, reqEndCal));
					Collections.sort(periodVec);
					break;
				}
			}			
		} else {
			periodVec.add(new Period(reqBeginCal, reqEndCal));
			Collections.sort(periodVec);
		}
	}

	public boolean isValidLottery() {
		if (prodNum == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[Lottery.isValidLottery]Invalid product number.");
            }
			return false;
		}
		
		if (lotProdName == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[Lottery.isValidLottery]Invalid lottery product name.");
            }
			return false;
		}
		
		//Check period
		int periodVecSz = periodVec.size();
		if (periodVecSz > 0) {
			for (int i = 0; i < periodVecSz; i++) {
				Period period = (Period)periodVec.get(i);
				Calendar beginCal = period.getBeginDate();
				Calendar endCal = period.getEndDate();
				if (compareCalendar(beginCal, endCal) >= 0) {
		            if (Log.WARNING_ON) {
		                Log.printWarning("[Lottery.isValidLottery]Begin date value must before to end date value.");
		            }
					return false;
				}

				long term = endCal.getTimeInMillis() - beginCal.getTimeInMillis();
				if (term < (1000 * 60 * 60 * 24 * 7)) { // check 7 days
					boolean isValidPeriod = false;
					Calendar tempCal = (Calendar) endCal.clone();
					int dow = tempCal.get(Calendar.DAY_OF_WEEK);
					while (true) {
						if (lotFreqVec.contains(new Integer(dow))) {
							isValidPeriod = true;
							break;
						}
						if (compareCalendar(tempCal, beginCal) == 0) {
							break;
						}
						tempCal.add(Calendar.DATE, -1);
						dow = tempCal.get(Calendar.DAY_OF_WEEK);
					}
					if (!isValidPeriod) {
						if (Log.WARNING_ON) {
							Log.printWarning("[Lottery.isValidLottery]There is no drawing day between begin date and end date.");
						}
						return false;
					}
				}
			}
		}
		//TODO 1.Periods shall not overlap with each other.
		//TODO 2.Periods shall not overlap with dates.
		//TODO 3. No duplicate dates.
		return true;
	}

	public void setDrawingDate() {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.setDrawingDate]Start.");
		}
		Calendar todayCal = getTodayCalendar();
		Calendar tempCal = (Calendar) todayCal.clone();
		boolean needToSetDrawingDate = needToSetDrawingDate(todayCal);
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.setDrawingDate]needToSetDrawingDate = " + needToSetDrawingDate);
		}
		if (needToSetDrawingDate) {
			Vector temp = new Vector();
			for (int i = 0; i < VALID_DRAWING_DAY_COUNT; i++) {
				if (isSpecificTermLottery) {
					Calendar singleDayCal = getCalendarFromSingleDays(tempCal);
					Calendar periodCal = getCalendarFromPeriods(tempCal);
					if (singleDayCal == null && periodCal == null) {
						break;
					}
					if (singleDayCal == null) {
						tempCal = (Calendar)periodCal.clone();
						temp.add(tempCal);
					} else if (periodCal == null) {
						tempCal = (Calendar)singleDayCal.clone();
						temp.add(tempCal);
					} else {
						tempCal = compareCalendar(singleDayCal, periodCal) > 0 ? (Calendar)singleDayCal.clone() : (Calendar)periodCal.clone();
						temp.add(tempCal);
					}
				} else {
					//어제날짜부터 검색을 시작한다. 
					while (true) {
						tempCal.add(Calendar.DATE, -1);
						int dow = tempCal.get(Calendar.DAY_OF_WEEK);
						if (lotFreqVec.contains(new Integer(dow))) {
							temp.add(tempCal.clone());
							break;
						}
					}
				}
			}
			drawingDayCals = new Calendar[temp.size()];
			drawingDayCals = (Calendar[]) temp.toArray(drawingDayCals);
			latestSuccessCal = (Calendar)todayCal.clone();
		}
		if (Log.INFO_ON) {
			if (drawingDayCals != null) {
				int drawingDayCalsLth = drawingDayCals.length;
				Log.printInfo("[Lottery.setDrawingDate] Valid drawing day count is " + drawingDayCalsLth);		
				for (int i=0; i<drawingDayCalsLth; i++) {
					Log.printInfo("[Lottery.setDrawingDate] Drawing day["+i+"] " + drawingDayCals[i].getTime());
				}
			}
		}
	}
	
	public int getDrawingDayCount() {
		int drawingDayCalsLth = drawingDayCals!= null ? drawingDayCals.length : 0;
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getDrawingDayCount] Valid drawing day count is " + drawingDayCalsLth);
		}
		return drawingDayCalsLth;
	}
	
	public Calendar getLatestDrawingDay() {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getLatestDrawingDay]Start.");
		}
		Calendar latestDrawingDay= null;
		if (drawingDayCals != null && drawingDayCals.length > 0) {
			latestDrawingDay = drawingDayCals[0];
		}
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getLatestDrawingDay]Latest drawing day is " + latestDrawingDay);
		}
		return latestDrawingDay;
	}
	
	public Calendar getNextDrawingDay(Calendar reqCal) {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getNextDrawingDay]Request day = "
					+ reqCal.getTime());
		}
		if (drawingDayCals != null && drawingDayCals.length > 0) {
			for (int i = drawingDayCals.length - 1; i >= 0; i--) {
				if (compareCalendar(drawingDayCals[i],reqCal) > 0) {
					if (Log.INFO_ON) {
						Log.printInfo("[Lottery.getNextDrawingDay]Next drawing day = "
								+ drawingDayCals[i].getTime());
					}
					return drawingDayCals[i];
				}
			}
		}
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getNextDrawingDay]No more next drawing day.");
		}
		return null;
	}
	
	public Calendar getPrevDrawingDay(Calendar reqCal) {
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getPrevDrawingDay]Request day = " + reqCal.getTime());
		}
		if (drawingDayCals != null && drawingDayCals.length > 0) {
			for (int i = 0; i < drawingDayCals.length; i++) {
				if (compareCalendar(drawingDayCals[i], reqCal) < 0) {
					if (Log.INFO_ON) {
						Log.printInfo("[Lottery.getPrevDrawingDay]Previous drawing day = "
								+ drawingDayCals[i].getTime());
					}
					return drawingDayCals[i];
				}
			}
		}
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getPrevDrawingDay]No more previous drawing day.");
		}
		return null;
	}
	
	private Calendar getCalendarFromSingleDays(Calendar reqCal) {
		if (reqCal == null) {
			return null;
		}
		int singleDayVecSz = singleDayVec.size();
		for (int i = singleDayVecSz - 1; i >= 0; i--) {
			Calendar singleDay = (Calendar) singleDayVec.get(i);
			if (compareCalendar(singleDay, reqCal) < 0) {
				return singleDay;
			}
		}
		return null;
	}
	
	private Calendar getCalendarFromPeriods(Calendar reqCal) {
		if (reqCal == null) {
			return null;
		}
		
		int periodVecSz = periodVec.size();
		for (int i = periodVecSz - 1; i >= 0; i--) {
			Period period = (Period) periodVec.get(i);
			Calendar endCal = period.getEndDate();
			Calendar beginCal = period.getBeginDate();
			boolean isContains = false;
			Calendar tempCal = (Calendar) endCal.clone();
			int dow = tempCal.get(Calendar.DAY_OF_WEEK);
			while (true) {
				if (compareCalendar(tempCal, reqCal) < 0 && lotFreqVec.contains(new Integer(dow))) {
					isContains = true;
					break;
				}
//				if (tempCal.equals(beginCal)) {
//					break;
//				}
				if (compareCalendar(tempCal, beginCal) == 0) {
					break;
				}
				tempCal.add(Calendar.DATE, -1);
				dow = tempCal.get(Calendar.DAY_OF_WEEK);
			}
			if (isContains) {
				return tempCal;
			}
		}
		return null;
	}
	
	private boolean needToSetDrawingDate(Calendar reqCal) {
		return latestSuccessCal == null || compareCalendar(latestSuccessCal, reqCal) != 0; 
	}

	public static Calendar getTodayCalendar() {
		Calendar todayCal = Calendar.getInstance();
		if (Rs.IS_EMULATOR) {
			todayCal.set(Calendar.YEAR, 2013);
			todayCal.set(Calendar.MONTH, Calendar.MAY);
			todayCal.set(Calendar.DATE, 15);
		}
		if (Log.INFO_ON) {
			Log.printInfo("[Lottery.getTodayCalendar] Today calendar = " + todayCal.getTime());
		}
		return todayCal;
	}
    
    //년월일 비교시 A가 B보다 크다면 1, A가 B보다 작다면 -1, 같으면 0
    private int compareCalendar(Calendar a, Calendar b) {
    	if (a == null && b == null) {
    		return 0;
    	}
    	if (a == null) {
    		return -1;
    	}
    	if (b == null) {
    		return 1;
    	}
    	
    	if (a.get(Calendar.YEAR) != b.get(Calendar.YEAR)) {
    		return a.get(Calendar.YEAR) - b.get(Calendar.YEAR);
    	}
    	if (a.get(Calendar.MONTH) != b.get(Calendar.MONTH)) {
    		return a.get(Calendar.MONTH) - b.get(Calendar.MONTH);
    	}
    	if (a.get(Calendar.DATE) != b.get(Calendar.DATE)) {
    		return a.get(Calendar.DATE) - b.get(Calendar.DATE);
    	}
    	return 0;
    }

	private static Calendar convertDateStringToCalendar(String reqDateStr) {
		if (reqDateStr == null) {
			return null;
		}
		Calendar res = null;
		try {
			res = Calendar.getInstance();
			res.setTime(SDF.parse(reqDateStr));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}
	
	class Period implements Comparable {
	    private final Calendar beginCal;
	    private final Calendar endCal; 
	    
	    public Period(Calendar beginCal, Calendar endCal) {
	    	this.beginCal = beginCal;
	    	this.endCal = endCal;
	    }
	    
	    public Calendar getBeginDate() {
	    	return beginCal;
	    }
	    
	    public Calendar getEndDate() {
	    	return endCal;
	    }

		public int compareTo(Object o) {
			Calendar tempBeginCal = ((Period)o).getBeginDate();
			return compareCalendar(beginCal, tempBeginCal);
		}
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("\n"+"************************************************" + "\n");
		sb.append("Product number = " + prodNum + "\n");
		sb.append("Product code = " + prodCode + "\n");
		sb.append("Lottery product name = " + lotProdName + "\n");
		sb.append("Related lottery product 1 = " + relatedLotProduct1 + "\n");
		sb.append("Related lottery product 2 = " + relatedLotProduct2 + "\n");
		int lotFreqVecSz =lotFreqVec.size();
		sb.append("Lottery frequency size = " + lotFreqVecSz + "\n");
		for (int i=0; i<lotFreqVecSz; i++) {
			sb.append("Lottery frequency["+i+"] = " + lotFreqVec.get(i) + "\n");
		}
		sb.append("Is specific term lottery = " + isSpecificTermLottery + "\n");
		int singleDayVecSz =singleDayVec.size();
		sb.append("Single day size = " + singleDayVecSz + "\n");
		for (int i=0; i<singleDayVecSz; i++) {
			sb.append("Single day["+i+"] = " + ((Calendar)singleDayVec.get(i)).getTime() + "\n");
		}
		int periodVecSz =periodVec.size();
		sb.append("Period size = " + periodVecSz + "\n");
		for (int i=0; i<periodVecSz; i++) {
			Period p = (Period)periodVec.get(i);
			sb.append("Period begin date["+i+"] = " + (p.getBeginDate()).getTime() + "\n");
			sb.append("Period end date["+i+"] = " + (p.getEndDate()).getTime() + "\n");
		}
		sb.append("************************************************" + "\n");
		return sb.toString();
	}

//	public static void main(String[] args) {
//		System.out.println("test");
//		String[] t ={"a", "b", "c" ,"d", "e"};
//		System.out.println("t.length = " + t.length);
//		for (int i=t.length-1; i>=0; i--) {
//			System.out.println(i + " = " +t[i]);
//		}
//	}

	
//	public static void main(String[] args) {
//		System.out.println("sort test");
//		Vector temp = new Vector();
//		temp.add(convertDateStringToCalendar("2012-11-30"));
//		temp.add(convertDateStringToCalendar("2011-10-29"));
//		temp.add(convertDateStringToCalendar("2013-5-1"));
//		Collections.sort(temp);
//		for (int i = 0; i <temp.size(); i++) {
//			System.out.println(i + " = " + ((Calendar)temp.get(i)).getTime());
//		}
//	}
	
//	public static void main(String[] args) {
//		System.out.println("period sort test");
//		Vector temp = new Vector();
//		temp.add(new Lottery2.Period(convertDateStringToCalendar("2011-01-01"), convertDateStringToCalendar("2011-02-23")));
//		temp.add(new Lottery2.Period(convertDateStringToCalendar("2010-01-03"), convertDateStringToCalendar("2010-03-29")));
//		temp.add(new Lottery2.Period(convertDateStringToCalendar("2012-03-30"), convertDateStringToCalendar("2012-10-29")));
//		Collections.sort(temp);
//		for (int i = 0; i <temp.size(); i++) {
////			System.out.println(i + " = " + ((Calendar)temp.get(i)).getTime());
//			System.out.println(i + " = " + ((Lottery2.Period)temp.get(i)).getBeginDate().getTime());
//		}
//	}
	
//	public static void main(String[] args) {
//		System.out.println("Clone test");
//		Vector temp = new Vector();
//		Calendar testCal = Calendar.getInstance();
//		for (int i = 0; i <10; i++) {
//			temp.add(testCal.clone());
//			testCal.add(Calendar.DATE, 1);
//		}
//		for (int i = 0; i <temp.size(); i++) {
//			System.out.println(i + " = " + ((Calendar)temp.get(i)).getTime());
//		}
//	}
	
//	public static void main(String[] args) {
//		System.out.println("Time test");
//		
//		long curMillis = System.currentTimeMillis();
//		System.out.println("curMillis = " + curMillis);
//		int days = (int)(curMillis/86400000);
//		System.out.println("Days = " + days);
//		
//		Calendar cal = Calendar.getInstance();
//		cal.clear();
//		System.out.println("Cal1 = " + cal.getTime());
//		cal.set(Calendar.DATE, days);
//		System.out.println("Cal1 = " + cal.getTime());
//		
//		Calendar tempCal = Calendar.getInstance();
//		tempCal.clear();
//		
//		System.out.println("tempCal = " + tempCal.getTime());
//		System.out.println("tempCalM = " + tempCal.getTimeInMillis());
//		System.out.println("tempCalDate = " + (int)(tempCal.getTimeInMillis()/86400000));
//		System.out.println("tempCalRemain = " + (int)(tempCal.getTimeInMillis()%86400000));
//
//		
//		Calendar aCal = (Calendar)tempCal.clone();
//		aCal.set(Calendar.YEAR, 2013);
//		aCal.set(Calendar.MONTH, Calendar.MAY);
//		aCal.set(Calendar.DATE, 11);
////		aCal.set(Calendar.HOUR, 0);
////		aCal.set(Calendar.MINUTE, 0);
//		System.out.println("aCal = " + aCal.getTime());
//		System.out.println("aCalDate = " + (int)(aCal.getTimeInMillis()/86400000));
//		System.out.println("aCalRemain = " + (int)(aCal.getTimeInMillis()%86400000));
//		
//		Calendar bCal = (Calendar)tempCal.clone();
//		bCal.set(Calendar.YEAR, 2013);
//		bCal.set(Calendar.MONTH, Calendar.MAY);
//		bCal.set(Calendar.DATE, 12);
//		bCal.set(Calendar.HOUR, 0);
//		bCal.set(Calendar.MINUTE, 1);
//		System.out.println("bCal" + bCal.getTime());
//		System.out.println("bCalDate = " + (int)(bCal.getTimeInMillis()/86400000));
//		System.out.println("bCalRemain = " + (int)(bCal.getTimeInMillis()%86400000));
//	}
}

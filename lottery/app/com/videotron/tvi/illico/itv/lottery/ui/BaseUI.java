package com.videotron.tvi.illico.itv.lottery.ui;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.communication.CommunicationManager;
import com.videotron.tvi.illico.itv.lottery.comp.DateAndTime;
import com.videotron.tvi.illico.itv.lottery.comp.PopupNotification;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupAdapter;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupController;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupEvent;
import com.videotron.tvi.illico.itv.lottery.comp.popup.PopupListener;
import com.videotron.tvi.illico.itv.lottery.controller.SceneManager;
import com.videotron.tvi.illico.itv.lottery.controller.SceneTemplate;
import com.videotron.tvi.illico.log.Log;

abstract public class BaseUI extends UIComponent {
    private static final long serialVersionUID = 1L;
    private DateAndTime dateAndTime;
    /*****************************************************************
     * variables - Popup-related
     *****************************************************************/
    private PopupNotification popNotification;
    private PopupAdapter popAdaptDefault;

    protected static StringBuffer dateBuffer;

    /*****************************************************************
     * methods - Life Cycle
     *****************************************************************/
    public void init() {
        if (dateBuffer == null) {
            dateBuffer = new StringBuffer();
        }
        if (dateAndTime == null) {
            dateAndTime = new DateAndTime();
            dateAndTime.setBounds(700, 43, 210, 17);
            add(dateAndTime);
        }
        if (popAdaptDefault == null) {
            popAdaptDefault = new PopupAdapter() {
                public void popupOK(PopupEvent pe) {
                    PopupController.getInstance().closePopup(popNotification);
                }
            };
        }
        initScene();
    }
    public void dispose() {
        stop();
        disposeScene();
        if (popNotification != null) {
            popNotification.dispose();
            popNotification = null;
        }
        if (dateAndTime != null) {
            remove(dateAndTime);
            dateAndTime = null;
        }
        popAdaptDefault=null;
        dateBuffer = null;
    }
    public void start(boolean reset, String[] params) {
        prepare();
        if (dateAndTime != null) {
            dateAndTime.start();
        }
        startScene(reset, params);
    }
    public void stop() {
        stopScene();
        closePopup();
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification);
        }
        if (dateAndTime != null) {
            dateAndTime.stop();
        }
    }

    /*****************************************************************
     * implement KeyListener
     *****************************************************************/
    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Scene.handleKey]code : "+code);
        }
        int curSceneId = SceneManager.getInstance().getCurrentSceneId();
        if (curSceneId != SceneTemplate.SCENE_ID_LOTTERY_MAIN) {
            if (code == Rs.KEY_EXIT) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Scene.handleKey]Exit Key Pressed.");
                }
                CommunicationManager.getInstance().requestExitToChannel();
                return true;
            }
        }
        if (CommunicationManager.getInstance().isLoadingAnimationService()) {
            return true;
        }
        return keyAction(code);
    }
    /*****************************************************************
     * methods - Popup-related
     *****************************************************************/
    private void createPopupInfo(PopupListener l) {
        if (popNotification == null) {
            popNotification = new PopupNotification();
            popNotification.setBounds(0, 0, 960, 540);
        }
        popNotification.setPopupListener(l);
    }
    protected void openPopupQuestion(PopupListener l, String sTitle, String cont) {
        openPopupQuestion(l, sTitle, cont, PopupNotification.POPUP_SIZE_SMALL);
    }
    protected void openPopupQuestion(PopupListener l, String sTitle, String cont, int size) {
        openPopup(l, sTitle, cont, size, PopupNotification.POPUP_TYPE_QUESTION);
    }
    private void openPopup(PopupListener l, String sTitle, String cont, int size, int type) {
        createPopupInfo(l);
        popNotification.setPopupSize(size);
        popNotification.setPopupType(type);
        if (sTitle != null) {
            popNotification.setTitle(sTitle);
        }
        popNotification.setContent(cont);
        PopupController.getInstance().openPopup(popNotification);
    }
    protected void closePopup() {
        if (popNotification != null) {
            popNotification.removePopupListener();
            PopupController.getInstance().closePopup(popNotification);
        }
    }
    /*****************************************************************
     * methods - abstract
     *****************************************************************/
    abstract protected void initScene();

    abstract protected void disposeScene();

    abstract protected void startScene(boolean reset, String[] params);

    abstract protected void stopScene();

    abstract protected boolean keyAction(int keyCode);
}

package com.videotron.tvi.illico.itv.lottery.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Hashtable;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.itv.lottery.Rs;
import com.videotron.tvi.illico.itv.lottery.data.Lottery;
import com.videotron.tvi.illico.itv.lottery.data.LotteryListHandler;
import com.videotron.tvi.illico.itv.lottery.data.ProxyInfo;
import com.videotron.tvi.illico.itv.lottery.data.ServerInfo;
import com.videotron.tvi.illico.log.Log;

public class RPManagerServer extends RPManager {
    private static final String SERVER_PROTOCOL = "HTTP";
    /*********************************************************************************
     * Life cycle-related
     *********************************************************************************/
    public void initSubManager() {
    }
    public void disposeSubManager() {
    }
    /*********************************************************************************
     * Service-related
     *********************************************************************************/
    public Lottery[] getLotteryList() throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLotteryList]Start");
        }
        DefaultHandler handler = new LotteryListHandler();
        String filePath = "lotteryList.xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getSummaryResult]Request file path : ["+filePath+"]");
        }
        URL reqURL = null;
        try{
            reqURL = getUrl(filePath);
        } catch(RPManagerException e) {
            e.printStackTrace();
            throw e;
        }
        if (reqURL == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        Lottery[] lotList = (Lottery[])getObejctFromUrlByParser(reqURL, handler);
        if (lotList==null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        long curMillis=System.currentTimeMillis();
        /** Check lottery list whether valid or not **/
//        for (int i=0; i<lotList.length; i++){
//            if (lotList[i]==null || !lotList[i].isSpecificDurationLottery()) {
//                continue;
//            }
//            Date eDate=lotList[i].getEndDate();
//            if (eDate==null || eDate.getTime()<curMillis) {
//                lotList[i].setInvalidLottery(true);
//                continue;
//            }
//            Date bDate=lotList[i].getBeginDate();
//            if (bDate==null || bDate.getTime()>curMillis) {
//                lotList[i].setInvalidLottery(true);
//                continue;
//            }
//        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getLotteryList]Response lottery list : "+lotList);
        }
        return lotList;
    }
    public byte[] getSummaryResult(String reqLangTag, String reqLotProdNo, String reqDate) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getSummaryResult]start");
            Log.printDebug("[RPMgrServer.getSummaryResult]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrServer.getSummaryResult]Param - request lottery product number : ["+reqLotProdNo+"]");
            Log.printDebug("[RPMgrServer.getSummaryResult]Param - request drawing date : ["+reqDate+"]");
        }
        if (reqLangTag == null || reqLotProdNo == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String filePath = "summary/" + reqLangTag + "/" + reqLotProdNo + "/latest.gif";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getSummaryResult]Request file path : ["+filePath+"]");
        }
        byte[] result = null;
        try{
            URL reqURL = getUrl(filePath);
            result = getBytesFromUrl(reqURL);
        } catch(RPManagerException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Loto HTTP server.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getSummaryResult]End : "+result);
        }
        return result;
    }
    public byte[] getDetailedResult(String reqLangTag, String reqLotProdNo, String reqDate)throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getDetailedResult]start");
            Log.printDebug("[RPMgrServer.getDetailedResult]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[RPMgrServer.getDetailedResult]Param - request lottery product number : ["+reqLotProdNo+"]");
            Log.printDebug("[RPMgrServer.getDetailedResult]Param - request drawing date : ["+reqDate+"]");
        }
        if (reqLangTag == null || reqLotProdNo == null || reqDate == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String filePath = "previous/" + reqLangTag + "/" + reqLotProdNo + "/" + reqDate + ".gif";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getDetailedResult]Request file path : ["+filePath+"]");
        }
        URL reqURL = getUrl(filePath);
        byte[] result = getBytesFromUrl(reqURL);
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getDetailedResult]End : "+result);
        }
        return result;
    }
    public Hashtable getPromotionalBanner(String reqLangTag, String reqBannerType) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getPromotionalBanner]start");
            Log.printDebug("[RPMgrServer.getPromotionalBanner]Param - request language type : [" + reqLangTag + "]");
            Log.printDebug("[RPMgrServer.getPromotionalBanner]Param - request banner type : [" + reqBannerType + "]");
        }
        if (reqLangTag == null || reqBannerType == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String filePath = "ads/" + reqBannerType + "-" + reqLangTag + ".zip";
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getPromotionalBanner]Request file path : [" + filePath + "]");
        }
        byte[] result = null;
        try{
            URL reqURL = getUrl(filePath);
            result = getBytesFromUrl(reqURL);
        } catch(RPManagerException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Loto HTTP server.");
            }
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
        }
        if (result == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        Hashtable zipFileHash = ZipFileReader.read(result);
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getPromotionalBanner]End : " + zipFileHash);
        }
        return zipFileHash;
    }
    protected URL getUrl(String fileName) throws RPManagerException{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getURL]Start.");
        }
        ServerInfo serverInfo = DataManager.getInstance().getServerInfo();
        if (serverInfo == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (fileName == null || fileName.trim().length() == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        if (fileName.startsWith("/")) {
            fileName = fileName.substring(1);
        }
        String host = serverInfo.getHost();
        if (host == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"] Cannot retrieve loto images from server.");
            }    
            throw new RPManagerException(Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM);
        }
        String urlInfo = SERVER_PROTOCOL + "://" + host + ":" + serverInfo.getPort();
        String fileInfo = "";
        String contextRoot = serverInfo.getContextRoot();
        if (contextRoot != null && contextRoot.trim().length() > 0) {
            if (!contextRoot.startsWith("/")) {
                contextRoot = "/" + contextRoot;
            }
            if (contextRoot.endsWith("/")) {
                contextRoot = contextRoot.substring(1);
            }
            fileInfo = contextRoot;
        }
        fileInfo += "/" + fileName;
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPMgrServer.getURL]URL info : " + urlInfo);
            Log.printDebug("[RPMgrServer.getURL]File info : " + fileInfo);
        }
        ProxyInfo[] pInfos = serverInfo.getProxyInfos();
        URL url = null;
        if (Rs.IS_EMULATOR || pInfos == null || pInfos.length == 0) {
            try {
                url = new URL(urlInfo + fileInfo);
            } catch (IOException e) {
                e.printStackTrace();
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Loto HTTP server.");
                }
                throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]non-Proxy URL : " + url);
            }
        } else {
            for (int i = 0; i < pInfos.length; i++) {
                if (pInfos[i] == null) {
                    continue;
                }
                String proxyHost = pInfos[i].getProxyHost();
                int proxyPort = pInfos[i].getProxyPort();
                try {
                    url = new URL(SERVER_PROTOCOL, proxyHost, proxyPort, urlInfo + fileInfo);
                } catch (IOException e) {
                    e.printStackTrace();
                    if (Log.ERROR_ON) {
                        Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"] Cannot contact Loto HTTP server.");
                    }
                    throw new RPManagerException(Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM);
                }
                if (url != null) {
                    break;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPMgrServer.getURL]Proxy URL : " + url);
            }
        }
        return url;
    }
}

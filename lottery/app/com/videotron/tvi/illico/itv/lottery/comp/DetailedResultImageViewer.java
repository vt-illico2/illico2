package com.videotron.tvi.illico.itv.lottery.comp;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.itv.lottery.controller.ImageManager;
import com.videotron.tvi.illico.log.Log;

public class DetailedResultImageViewer extends Container {
    private static final long serialVersionUID = 7769151586899887564L;

    private Image imgLotDetailShadowTop;
    private Image imgLotDetailShadowBot;
    private Image imgScrollBG;
    private Image imgScroll;
    private DetailedResultImage detResImg;
    private int imgSrcH;
    private Image[] imgSrces;
    // Scroll
    private static final int SCROLL_BAR_HEIGHT = 292;
    private static final int LOTTERY_IMAGE_HEIGHT = 301;
    private static final int MOVE_PIXEL = 72;
    private int totalPage;
    private int curPageIdx;
    private int scrollBarGap;

    public void init() {
        if (detResImg == null) {
            detResImg = new DetailedResultImage();
            detResImg.setBounds(0, 0, 470, LOTTERY_IMAGE_HEIGHT);
            add(detResImg);
        }
        imgLotDetailShadowTop = ImageManager.getInstance().getImage("11_detail_sh_t.png", ImageManager.TYPE_TEMPORARY);
        imgLotDetailShadowBot = ImageManager.getInstance().getImage("11_detail_sh.png", ImageManager.TYPE_TEMPORARY);
        imgScrollBG = ImageManager.getInstance().getImage("scrbg_lottery.png", ImageManager.TYPE_TEMPORARY);
        imgScroll = ImageManager.getInstance().getImage("scr_bar.png", ImageManager.TYPE_TEMPORARY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public void dispose() {
        if (detResImg != null) {
            remove(detResImg);
            detResImg = null;
        }
        imgLotDetailShadowTop = null;
        imgLotDetailShadowBot = null;
        imgScrollBG = null;
        imgScroll = null;
    }
    public void start(Image[] imgSrces) {
        this.imgSrces = imgSrces;
        curPageIdx = 0;
        if (imgSrces == null) {
            imgSrcH = 0;
            totalPage = 0;
            return;
        }
        imgSrcH = 0;
        for (int i=0; i<imgSrces.length; i++) {
            if (imgSrces[i] == null) {
                continue;
            }
            imgSrcH += imgSrces[i].getHeight(this);
        }
        totalPage = 0;
        int imgSrcOverHht = 0;
        if (imgSrcH > 0) {
            totalPage = 1;
            imgSrcOverHht = imgSrcH - LOTTERY_IMAGE_HEIGHT;
            if (imgSrcOverHht > 0) {
                totalPage += imgSrcOverHht / MOVE_PIXEL;
                if ((imgSrcOverHht % MOVE_PIXEL) != 0) {
                    totalPage++;
                }
            }
        }
        if (totalPage > 1) {
            scrollBarGap = SCROLL_BAR_HEIGHT / (totalPage - 1);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryImageViewer.start]imgSrcHht : "+imgSrcH);
            Log.printDebug("[LotteryImageViewer.start]LOTTERY_IMAGE_HEIGHT : "+LOTTERY_IMAGE_HEIGHT);
            Log.printDebug("[LotteryImageViewer.start]imgSrcOverHht : "+imgSrcOverHht);
            Log.printDebug("[LotteryImageViewer.start]totalPage : "+totalPage);
            Log.printDebug("[LotteryImageViewer.start]SCROLL_BAR_HEIGHT : "+SCROLL_BAR_HEIGHT);
            Log.printDebug("[LotteryImageViewer.start]totalPage : "+totalPage);
            Log.printDebug("[LotteryImageViewer.start]scrollBarGap : "+scrollBarGap);
        }
    }
    public void stop() {
        imgSrces = null;
        imgSrcH = 0;
        totalPage = 0;
        curPageIdx = 0;
        scrollBarGap = 0;
    }
    public void paint(Graphics g) {
        final int x = 173;
        final int y = 176;
        // Scroll
        if (totalPage > 1) {
            if (imgScrollBG != null) {
                g.drawImage(imgScrollBG, 658 - x, 133 - y, 8, 338, this);
            }
            if (imgScroll != null) {
                int yPos = 0;
                if (curPageIdx == totalPage - 1) {
                    yPos = 271;
                } else {
                    yPos = (curPageIdx * scrollBarGap);
                }
                g.drawImage(imgScroll, 655 - x, yPos, this);
            }


//            if (imgScrollBarMid != null) {
//                g.drawImage(imgScrollBarMid, 873 - 129, 190 - 181, 7, 272, this);
//            }
//            if (imgScrollBarUp != null) {
//                g.drawImage(imgScrollBarUp, 873 - 129, 181 - 181, this);
//            }
//            if (imgScrollBarDown != null) {
//                g.drawImage(imgScrollBarDown, 873 - 129, 462 - 181, this);
//            }
//            if (imgScrollBar != null) {
//                int yPos = 0;
//                if (curPageIdx == totalPage - 1) {
//                    yPos = 266;
//                } else {
//                    yPos = (curPageIdx * scrollBarGap);
//                }
//                g.drawImage(imgScrollBar, 870 - 129, yPos, this);
//            }
        }
        super.paint(g);
    }
    public void moveDown() {
        if (totalPage <= 1) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[LotteryImageViewer.start]curPageIdx : "+curPageIdx);
        }
        if (curPageIdx >= totalPage-1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LotteryImageViewer.start]ret : "+curPageIdx);
            }
            return;
        }
//        int compHht = getHeight();
//        yPos -= MOVE_PIXEL;
        curPageIdx ++;
//        if (yPos < compHht - imgSrcHht) {
//            yPos = compHht - imgSrcHht;
//            curPage = 0;
//        }
        repaint();
    }
    public void moveUp() {
        if (totalPage <= 1) {
            return;
        }
        if (curPageIdx <= 0) {
            return;
        }
//        yPos += MOVE_PIXEL;
        curPageIdx --;
//        if (yPos > 0) {
//            yPos = 0;
//            curPage = totalPage -1;
//        }
        repaint();
    }
    class DetailedResultImage extends Component {
        private static final long serialVersionUID = 6064217403201497093L;
        public void paint(Graphics g) {
            if (imgSrces != null) {
                int yPos = 0;
                if (curPageIdx == 0) {
                    yPos = 0;
                } else if (curPageIdx == totalPage - 1) {
                    yPos = getHeight() - imgSrcH;
                } else {
                    yPos = curPageIdx * (-1) * MOVE_PIXEL;
                }
                int yPos2 = yPos;
                for (int i=0; i<imgSrces.length; i++) {
                    int imgSrcHth = 0;
                    if (imgSrces[i] != null) {
                        imgSrcHth = imgSrces[i].getHeight(this);
                        g.drawImage(imgSrces[i], 0, yPos2, getWidth(), imgSrcHth, this);
                    }
                    yPos2 += imgSrcHth;
                }
            }
            //Shadow
            final int x = 173;
            final int y = 176;
            if (curPageIdx != 0) {
                if (imgLotDetailShadowTop != null) {
                    g.drawImage(imgLotDetailShadowTop, 173 - x, 176 - y, this);
                }
            }
            if (totalPage > (curPageIdx + 1)) {
                if (imgLotDetailShadowBot != null) {
                    g.drawImage(imgLotDetailShadowBot, 173 - x, 442 - y, this);
                }
            }
        }
    }

    public int getTotalPageCount() {
        return totalPage;
    }
}

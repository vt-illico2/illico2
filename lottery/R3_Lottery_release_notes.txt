﻿This is Lottery App Release Notes.
GENERAL RELEASE INFORMATION
 - Release Details
    - Release version
      : version R7.1.1
    - Release file name
      : Lottery.zip
    - Release type (official, debug, test,...)
      : official       
    - Release date
      : 2016.xx.xx

***
version R7.1.x - 2016.xx.xx
    - New Feature   
        N/A
    - Resolved Issues
       + VDTRMASTER-5846 [prod][R7.1] Weather service sends multiple request simultaneously
***
version 4K.4.0 - 2015.07.31
    - New Feature   
        + Applying Flat design modification.
    - Resolved Issues
        N/A
***
version 4K.3.0 - 2015.06.12
    - New Feature   
        + update a 02_d_shadow.png
    - Resolved Issues
        N/A
***
version 4K.2.0 - 2015.05.13
    - New Feature   
        + update a 00_mini_line.png
        + update a 07_bg_main.jpg
        + update a bg.jpg
    - Resolved Issues
        N/A
***
version 4K.1.0 - 2015.04.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
    - Resolved Issues
        N/A
      
*** 
version 1.0.25.1
    - New Feature 
        N/A
    - Resolved Issues 
		+ VDTRMASTER-4950 Lottery - Summary list of lottery is out of date, but ok after a reset          
*** 
version 1.0.24.1
    - New Feature 
        N/A
    - Resolved Issues 
		+ VDTRMASTER-4788 [CQ][R3][R2][LOTTO]: CLONE -DDC6451 - Error code LOT500 when trying to access unavailable result          
*** 
version 1.0.23.0
    - New Feature 
        + Fixed sort module about lottery single day. (Related to VDTRSUPPORT-250  Lottery error LOT500 when trying to display past results)
    - Resolved Issues 
		N/A        
*** 
version 1.0.22.0
    - New Feature 
        + Fixed the lottery banner module.
    - Resolved Issues 
		N/A      
*** 
version 1.0.21.1
    - New Feature 
        N/A
    - Resolved Issues 
		+ VDTRSUPPORT-250  Lottery error LOT500 when trying to display past results
*** 
version 1.0.20.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-4583 [R3][Lotto][REGR-R2]: Page Up/Down buttons do not provide visual feedback to user      
*** 
version 1.0.19.0
    - New Feature 
        + Applied changed ApplicationConfig module.
    - Resolved Issues 
        N/A
*** 
version 1.0.18.0
    - New Feature 
        N/A    
    - Resolved Issues 
        + VDTRMASTER-4353 [R3] [Lotterie] [INTGR] - 'Add Favourite Lotteries' button should not be active in preferences        
*** 
version 1.0.17.0
    - New Feature
        + changed VBM parameter. ("Unknown" to "") 
    - Resolved Issues 
        N/A      
*** 
version 1.0.16.0
    - New Feature
        + added VBM-group id module.
        + wrote version info to application.prop        
    - Resolved Issues 
        N/A
*** 
version 1.0.15.0
    - New Feature
        + modified VBM module.(modified MID-1013000003)
    - Resolved Issues 
        N/A
*** 
version 1.0.14.0
    - New Feature 
        + fixed the issue that does not dispose of LayeredUI. 
    - Resolved Issues 
        N/A
*** 
version 1.0.13.0
    - New Feature 
        + LOT501 is added newly 
        + LOT502 is added newly
        + LOT503 is added newly
    - Resolved Issues 
        N/A          
*** 
version 1.0.12.0
    - New Feature
        + Merged from R2 source.
        + integrated with VBM log.
    - Resolved Issues
        N/A           
*** 
version 1.0.11.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-3242 LOTTERY - Day is missing in the header of the detailed results          
*** 
version 1.0.10.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-2816 LOTTERY Some results are missing for La Quotidienne       
*** 
version 1.0.9.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-2815 LOTTERY Last line of results is not displayed     
*** 
version 1.0.8.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VTBACKEND-175 CLONE -DDC6451 - Error code LOT500 when trying to access unavailable result     
*** 
version 1.0.7.0
    - New Feature 
        + Changed the promotional banner URL.(banners -> ads) 
    - Resolved Issues 
        N/A
*** 
version 1.0.6.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 1.0.5.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2492 [UI/GUI] [Lottery] - Clear all favourites : TOC to review
        + VDTRMASTER-2500 [UI/GUI] [Lottery] - Last results / Preferences
*** 
version 1.0.4.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2492 [UI/GUI] [Lottery] - Clear all favourites : TOC to review
        + VDTRMASTER-2500 [UI/GUI] [Lottery] - Last results / Preferences
*** 
version 1.0.3.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2404 DDC6451 - 6/49 is not the default favorite Lottery
*** 
version 1.0.2.1
    - New Feature 
        + DDC6451 - No results for Lottery exept Lotomax
    - Resolved Issues 
        + VDTRMASTER-2457  [UI/GUI] LOTTERY Text is not displayed at the right place in previous results
*** 
version 1.0.1.0
    - New Feature 
        + added background image of D-Option
    - Resolved Issues 
        N/A
*** 
version 0.1.50.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-3653 Display error for details result of a lottery draw      
*** 
version 0.1.49.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-3653 Display error for details result of a lottery draw
*** 
version 0.1.48.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-3524 [LOTTERY] Wrong syntax in favorites loto delete in French       
*** 
version 0.1.47.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-3242 LOTTERY - Day is missing in the header of the detailed results  
*** 
version 0.1.46.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-2816 LOTTERY Some results are missing for La Quotidienne         
*** 
version 0.1.45.0
    - New Feature 
        + Modified request parameter to move help app. (Lottery -> Lotto) 
    - Resolved Issues 
        N/A         
*** 
version 0.1.44.0
    - New Feature 
        + Modified request parameter to move help app. 
    - Resolved Issues 
        N/A       
*** 
version 0.1.43.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-2815 LOTTERY Last line of results is not displayed     
*** 
version 0.1.42.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VTBACKEND-175 CLONE -DDC6451 - Error code LOT500 when trying to access unavailable result     
*** 
version 0.1.41.0
    - New Feature 
        + Changed the promotional banner URL.(banners -> ads) 
    - Resolved Issues 
        N/A
*** 
version 0.1.40.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 0.1.39.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2492 [UI/GUI] [Lottery] - Clear all favourites : TOC to review
        + VDTRMASTER-2500 [UI/GUI] [Lottery] - Last results / Preferences
*** 
version 0.1.38.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2492 [UI/GUI] [Lottery] - Clear all favourites : TOC to review
        + VDTRMASTER-2500 [UI/GUI] [Lottery] - Last results / Preferences
*** 
version 0.1.37.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2404 DDC6451 - 6/49 is not the default favorite Lottery
*** 
version 0.1.36.1
    - New Feature 
        + DDC6451 - No results for Lottery exept Lotomax
    - Resolved Issues 
        + VDTRMASTER-2457  [UI/GUI] LOTTERY Text is not displayed at the right place in previous results
*** 
version 0.1.35.0
    - New Feature 
        + added Background image of D-Option
    - Resolved Issues 
        N/A
*** 
version 0.1.34.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2139  [UI/GUI] [LOT] Lottery has no 'D' options
        + VDTRMASTER-2168  [UI/GUI] [ITV] - Missing a "exit" button to get out of the application
*** 
version 0.1.33.1
    - New Feature 
        + Clicking animation
    - Resolved Issues 
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
*** 
version 0.1.32.0
    - New Feature 
        + Fixed network module bug.
    - Resolved Issues 
        N/A
*** 
version 0.1.31.0
    - New Feature 
        + Applied modified Framework(OC unload)
    - Resolved Issues 
        N/A
*** 
version 0.1.30.0
    - New Feature 
        + Fixed the animation module of weather main scene.
        + Fixed the animation module of weather preference scene.
    - Resolved Issues 
        N/A
*** 
version 0.1.29.0
    - New Feature 
        + Removed DCA keys at scene without scaled video.
    - Resolved Issues 
        N/A
*** 
version 0.1.28.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1945  [UI/GUI] [Lottery] Add a favourite lottery when there is already 5
        + VDTRMASTER-1958  [UI/GUI] [Lottery] Page Up/Down should only be display when item requires scrolling
*** 
version 0.1.27.3
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1960 [UI/GUI] [Lottery] Missing "u" in Clear Favourites
        + VDTRMASTER-1820 [VT] [Lottery] Legal disclaimer and the 'Back' button icon were overlap on detailed results.
        + VDTRMASTER-1958 [UI/GUI] [Lottery] Page Up/Down should only be display when item requires scrolling
*** 
version 0.1.26.0
    - New Feature 
        + Added lottery application information to the "ocap.host.application" file .
    - Resolved Issues 
        N/A
*** 
version 0.1.25.0
    - New Feature 
        + Added the footer animation.
    - Resolved Issues 
        N/A
*** 
version 0.1.24.0
    - New Feature 
        + Fixed the animation of preference scene (add / remove lottery).
    - Resolved Issues 
        N/A
*** 
version 0.1.23.1
    - New Feature 
        + Fixed the animation of preference scene (add / remove lottery).
    - Resolved Issues 
        + VDTRMASTER-1725  [UI/GUI][TOC] Weather & Lottery
*** 
version 0.1.22.0
    - New Feature 
        + Fixed the animation of main scene (scaled video - related).
        + Blocked key event of PVR-related  at the scene without scaled video.
    - Resolved Issues 
        N/A
*** 
version 0.1.21
    - New Feature 
        + Applied the latest TOC data.
    - Resolved Issues 
        N/A
*** 
version 0.1.20
    - New Feature 
        + Fixed animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.19
    - New Feature 
        + Fixed animation module of the main screen.
        + Fixed renderer module for preventing memory leak.
    - Resolved Issues 
        N/A
*** 
version 0.1.18
    - New Feature 
        + Fixed animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.17
    - New Feature 
        + Applied animation module of the main screen.
    - Resolved Issues 
        N/A
*** 
version 0.1.16
    - New Feature 
        + Fixed proxy server module.
    - Resolved Issues 
        + VDTRMASTER-1303  [VT] iTV - Does not display the Kind of Lottery in Lottery preference.
*** 
version 0.1.15
    - New Feature 
        + Fixed SAX parser handler module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.14
    - New Feature 
        + Fixed Stc service module.
        + Fixed GUI issue of favorite lottery screen.
    - Resolved Issues 
        N/A 
*** 
version 0.1.13
    - New Feature 
        + Fixed scaled video module (show info bar and do not hide).
    - Resolved Issues 
        N/A 
*** 
version 0.1.12
    - New Feature 
        + Fixed IB data parsing error.
        + Added click effect.
    - Resolved Issues 
        N/A 
*** 
version 0.1.11
    - New Feature 
        + Added detailed result lottery image shadow.
        + Fixed some bugs.
    - Resolved Issues 
        N/A 
*** 
version 0.1.10
    - New Feature 
        + Fixed Screen saver module.
        + Fixed lottery arrange module.
    - Resolved Issues 
        N/A 
*** 
version 0.1.9
    - New Feature 
        + Fixed promotional banner cache module.
        + Fixed page-up and page-down key action module of the detailed result screen.
    - Resolved Issues 
        N/A  
*** 
version 0.1.8
    - New Feature 
        + Fixed detailed result image module. (NPE occurred.)
        + Fixed drawing date changing module of the detailed result screen.
        + Added page-up and page-down key action module of the preference screen.
        + Fixed action module of the preference screen (favorite lottery).
    - Resolved Issues 
        N/A  
*** 

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed IB parsing module.
- Release name : version 0.1.7
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed IB data module.
- Release name : version 0.1.6
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version : changes IB data module (synchronousLoad -> asynchronousLoad).
  + Fixed : fixed scaled video module (Do not show info bar).
- Release name : version 0.1.5
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.31
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : applied  <Period>, <Date> tag of Lottery list XML.
  + Fixed : fixed scaled video module (Do not show info bar).
- Release name : version 0.1.4
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Fixed ib.prop file (fixed ib file path).
- Release name : version 0.1.3
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version.
- Release name : version 0.1.2
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed frequency module.
- Release name : version 0.1.1
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.29
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Initial release.
- Release name : version 0.1.0
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.28
-----------------------------------------------------------------------------------


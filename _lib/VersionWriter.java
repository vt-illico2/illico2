import java.io.*;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import java.lang.reflect.*;

public class VersionWriter {

    public static void main(String args[]) throws Exception {
        String initialClass = findInitialClass(new File(args[0]).getCanonicalFile().getName(), args[3]);
        if (initialClass != null) {
            Class c = Class.forName(initialClass);
            Method m = c.getMethod("getVersion", new Class[] {});
            Object ret = m.invoke(c.newInstance(), new Object[] {});

            if (ret != null) {
                System.out.println("Found version = \"" + ret + "\" on " + c);
                writeVersion(ret.toString(), args[1], args[2]);
                System.out.println("Writing version on application.prop, done.");
                return;
            } else {
                System.out.println("Found " + c + ", but VERSION not found.");
            }
        }
        throw new Exception("Writing version fail. You MUST write the version on application.prop manually.");
    }

    private static void writeVersion(String ver, String applicationProp, String target)
        throws Exception
    {
    	String lineSep = "\r\n";
    	
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(applicationProp)));
        PrintStream ps = new PrintStream(new FileOutputStream(target));
        ps.print("#version_id = " + ver+lineSep);
        String line;
        while((line = br.readLine()) != null)
            if(!line.startsWith("#version_id"))
                ps.print(line.trim()+lineSep);
        ps.close();
        br.close();
    }

    private static String getAttribute(NamedNodeMap map, String name) {
        Node node = map.getNamedItem(name);
        if (node != null) {
            return node.getNodeValue();
        }
        return "";
    }

    private static String findInitialClass(String baseDir, String ocapHostPath) throws Exception {
        // ocap host app
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new File(ocapHostPath));
        Node root = doc.getChildNodes().item(0);

        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node n = children.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE && "application".equals(n.getNodeName())) {
                NamedNodeMap map = n.getAttributes();
                String foundDir = getAttribute(map, "basedir");
                if (baseDir.equals(foundDir)) {
                    return getAttribute(map, "initial_class");
                }
            }
        }
        System.out.println("Can't find initial class");
        return null;
    }


}

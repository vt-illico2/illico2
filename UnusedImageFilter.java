import java.io.*;
import java.util.*;

public class UnusedImageFilter {
    private static UnusedImageFilter instance = null;
    private static String appName = null;
    private BufferedWriter writer = null;
    public static void main(String[] args) throws Exception {
        instance = new UnusedImageFilter();
        if (args != null && args.length > 0 && args[0] != null) {
            appName = args[0];
        }
        instance.startFiltering();
    }
    
    private void startFiltering() {
        String imgPath = "resource/image/";
        Vector<String> imgList = instance.shellCmd("ls "+imgPath);
        if (imgList == null || imgList.size() == 0) {
            System.out.println("No image list.");
            return;
        }
        try {
            writer = new BufferedWriter(new FileWriter("../unused_image_filtering_result/filtering_result_of_"+appName+".txt"));
            writeLine("###########################");
            writeLine("#["+appName+"]");
            writeLine("###########################");
            writeLine("");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int first = 0;
        int second = 0;
        int third = 0;
        int fourth = 0;
        int fifth = 0;
        int sixth = 0;
        for (int i = 0 ; i < imgList.size() ; i++) {
            String aImg = imgList.get(i);
            
            // framework 사용여부 추가.
            Vector<String> grepResultFramework = instance.shellCmd("grep -r " + aImg + " ../framework/");
            if (grepResultFramework != null && grepResultFramework.size() > 0) {
            	writeLine("[" + aImg + "] is used by framework");
            	writeLine("found at = " + grepResultFramework.get(0));
            }
            
            Vector<String> grepResult = instance.shellCmd("grep -r "+aImg);
            if (grepResult == null || grepResult.size() == 0) {
                continue;
            }
            boolean referedBySrc = false;
            for (int j = 0 ; j < grepResult.size() ; j++) {
                String aLine = grepResult.get(j);
                if (aLine.indexOf("illico") != -1) {
                    referedBySrc = true;
                    break;
                }
            }
            String msg = "";
            if (referedBySrc) {
                msg = "["+aImg + "]";
                writeLine(msg);
            } else {
                first ++;
                msg = "["+aImg + "] ----> NOT using!!";
                writeLine(msg);
                try {
                    // NOT using 의 경우 다시 한번 필터링
                    int idx = aImg.indexOf(".");
                    if (idx > 0) {
                        String tmp = aImg.substring(0, idx);
                        String withoutDot = tmp;
                        Vector<String> result = instance.shellCmd("grep -r "+tmp);
                        boolean refered = false;
                        for (int j = 0 ; j < result.size() ; j++) {
                            String aLine = result.get(j);
                            if (aLine.indexOf("illico") != -1) {
                                refered = true;
                                break;
                            }
                        }
                        if (refered) {
                            String msg2 = "             recheck --> ["+tmp+"] OK";
                            writeLine("");
                            writeLine(msg2);
                            writeLine("");
                        } else {
                            second ++;
                            String msg2 = "             recheck --> ["+tmp+"] NOT OK XXXXXXXXXXXXXXXXXX";
                            writeLine("");
                            writeLine(msg2);
                            try {
                                // NOT OK 의 경우 다시 한번 필터링
                                idx = tmp.lastIndexOf("_");
                                if (idx > 0) {
                                    tmp = aImg.substring(0, idx+1);
                                    result = instance.shellCmd("grep -r "+tmp);
                                    refered = false;
                                    for (int j = 0 ; j < result.size() ; j++) {
                                        String aLine = result.get(j);
                                        if (aLine.indexOf("illico") != -1) {
                                            refered = true;
                                            break;
                                        }
                                    }
                                    if (refered) {
                                        String msg3 = "             recheck --> ["+tmp+"] OK";
                                        writeLine(msg3);
                                    } else {
                                        third ++;
                                        String msg3 = "             recheck --> ["+tmp+"] NOT OK XXXXXXXXXXXXXXXXXX";
                                        writeLine(msg3);
                                    }
                                    ///////////////////////////////////
                                    // 다시 한번 _ 빼고 필터링
                                    tmp = aImg.substring(0, idx);
                                    result = instance.shellCmd("grep -r "+tmp);
                                    refered = false;
                                    for (int j = 0 ; j < result.size() ; j++) {
                                        String aLine = result.get(j);
                                        if (aLine.indexOf("illico") != -1) {
                                            refered = true;
                                            break;
                                        }
                                    }
                                    if (refered) {
                                        String msg3 = "             recheck --> ["+tmp+"] OK";
                                        writeLine(msg3);
                                    } else {
                                        fourth ++;
                                        String msg3 = "             recheck --> ["+tmp+"] NOT OK XXXXXXXXXXXXXXXXXX";
                                        writeLine(msg3);
                                    }
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                            try {
                                // NOT OK 의 경우 다시 한번 필터링
                                idx = withoutDot.indexOf("_");
                                if (idx > 0) {
                                    tmp = withoutDot.substring(idx, withoutDot.length());
                                    result = instance.shellCmd("grep -r "+tmp);
                                    refered = false;
                                    for (int j = 0 ; j < result.size() ; j++) {
                                        String aLine = result.get(j);
                                        if (aLine.indexOf("illico") != -1) {
                                            refered = true;
                                            break;
                                        }
                                    }
                                    if (refered) {
                                        String msg3 = "             recheck --> ["+tmp+"] OK";
                                        writeLine(msg3);
                                    } else {
                                        fifth ++;
                                        String msg3 = "             recheck --> ["+tmp+"] NOT OK XXXXXXXXXXXXXXXXXX";
                                        writeLine(msg3);
                                    }
                                    ///////////////////////////////////
                                    // 다시 한번 _ 빼고 필터링
                                    tmp = withoutDot.substring(idx+1, withoutDot.length());
                                    result = instance.shellCmd("grep -r "+tmp);
                                    refered = false;
                                    for (int j = 0 ; j < result.size() ; j++) {
                                        String aLine = result.get(j);
                                        if (aLine.indexOf("illico") != -1) {
                                            refered = true;
                                            break;
                                        }
                                    }
                                    if (refered) {
                                        String msg3 = "             recheck --> ["+tmp+"] OK";
                                        writeLine(msg3);
                                    } else {
                                        sixth ++;
                                        String msg3 = "             recheck --> ["+tmp+"] NOT OK XXXXXXXXXXXXXXXXXX";
                                        writeLine(msg3);
                                    }
                                    writeLine("");
                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        writeLine("");
        writeLine("");
        writeLine("");
        writeLine("");
        writeLine("###########################################################");
        writeLine("#");
        writeLine("# --> RESULT");
        writeLine("#");
        writeLine("# TOTAL IMAGE COUNT = "+imgList.size());
        writeLine("# 1st Filtered(ex: \"aa_bb_cc_dd.png\") = "+first+" (No Reference)");
        writeLine("# 2nd Filtered(ex: \"aa_bb_cc_dd\") = "+second+" (No Reference)");
        writeLine("# 3rd Filtered(ex: \"aa_bb_cc_\") = "+third+" (No Reference)");
        writeLine("# 4th Filtered(ex: \"aa_bb_cc\") = "+fourth+" (No Reference)");
        writeLine("# 5th Filtered(ex: \"_bb_cc_dd\") = "+fifth+" (No Reference)");
        writeLine("# 6th Filtered(ex: \"bb_cc_dd\") = "+sixth+" (No Reference)");
        writeLine("#");
        writeLine("###########################################################");
        try {
            if (writer != null) {
                writer.close();
                writer = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void writeLine(String msg) {
        System.out.println(msg);
        try {
            if (writer != null) {
                writer.write(msg);
                writer.newLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Vector shellCmd(String command) {
        Vector<String> buf = new Vector<String>();
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec(command);
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while((line = br.readLine()) != null) {
                //System.out.println(line);
                buf.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return buf;
    }
}

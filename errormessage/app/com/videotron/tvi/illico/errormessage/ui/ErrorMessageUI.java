package com.videotron.tvi.illico.errormessage.ui;

import java.awt.Image;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.errormessage.communication.CommunicationManager;
import com.videotron.tvi.illico.errormessage.communication.PreferenceProxy;
import com.videotron.tvi.illico.errormessage.controller.DataManager;
import com.videotron.tvi.illico.errormessage.controller.ErrorMessageManager;
import com.videotron.tvi.illico.errormessage.gui.RendererErrorMessage;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageButton;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

public class ErrorMessageUI extends UIComponent{
    private static final long serialVersionUID = 7252933701401174099L;

//    private ErrorMessageTimer timer;

    private ErrorMessageListener errMsgListener;
    private Hashtable errMsgDynamicTagList;
    private int errMsgType = -1;
    private String errCode;
    private String errMsgTitle;
    private String errMsgContent;
    private String errMsgIconName;
    private Image imgIcon;
    private int errMsgButCount;
    private ErrorMessageButton[] errMsgButList;
    private int curIdx;
    private Image imgDefaultIcon;

    public ErrorMessageUI() {
        renderer = new RendererErrorMessage();
    }
    public void start(String errCode, ErrorMessage errMsg, ErrorMessageListener errMsgLstnr) {
        start(errCode, errMsg, errMsgListener, null);
    }
    public void start(String errCode, ErrorMessageListener errMsgListener, Hashtable errMsgDynamicTagList) {
        start(errCode, DataManager.getInstance().getErrorMessage(errCode), errMsgListener, errMsgDynamicTagList);
    }
    private void start(String errCode, ErrorMessage errMsg, ErrorMessageListener errMsgLstnr, Hashtable errMsgDynamicTagList) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageUI.start]Param - Error code : " + errCode);
            Log.printDebug("[ErrorMessageUI.start]Param - Error message : " + errMsg);
            Log.printDebug("[ErrorMessageUI.start]Param - Error message listener : " + errMsgLstnr);
        }
        this.errMsgListener = errMsgLstnr;
        this.errMsgDynamicTagList = errMsgDynamicTagList;
        this.errCode = errCode;

        if (renderer != null) {
            setRenderer(renderer);
        }
        prepare();
        if (imgDefaultIcon == null) {
            imgDefaultIcon = DataCenter.getInstance().getImage("icon_noti_red.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
        if (errMsg != null) {
            //Error message type
            try{
                errMsgType = errMsg.getErrorMessageType();
            }catch(Exception e) {
                Log.print(e);
            }
            //Error message title
            int langType = PreferenceProxy.getInstance().getLanguageType();
            String tempErrMsgTitle = null;
            try{
                switch(langType) {
                    case PreferenceProxy.LANGUAGE_TYPE_ENGLISH:
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.start]Current language type is English.");
                        }
                        tempErrMsgTitle = errMsg.getEnglishTitle();
                        break;
                    case PreferenceProxy.LANGUAGE_TYPE_FRENCH:
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.start]Current language type is French.");
                        }
                        tempErrMsgTitle = errMsg.getFrenchTitle();
                        break;
                }
            }catch(Exception e) {
                Log.print(e);
            }
            if (tempErrMsgTitle != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ErrorMessageUI.start]Error message title is exist.");
                }
                if (errMsgDynamicTagList != null) {
                    Enumeration enu = errMsgDynamicTagList.keys();
                    while(enu.hasMoreElements()){
                        String key =(String)enu.nextElement();
                        if (key == null) {
                            continue;
                        }
                        if (tempErrMsgTitle.indexOf(key)!= -1) {
                            String dynamicValue = (String)errMsgDynamicTagList.get(key);
                            if (dynamicValue != null) {
                                tempErrMsgTitle = replaceAll(tempErrMsgTitle, key, dynamicValue);
                            }
                        }
                    }
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ErrorMessageUI.start]Error message title is null.");
                }
                tempErrMsgTitle = "N/A";
            }
            errMsgTitle = tempErrMsgTitle;
            //Error message content
            String tempErrMsgContent = null;
            try{
                switch(langType) {
                    case PreferenceProxy.LANGUAGE_TYPE_ENGLISH:
                        tempErrMsgContent = errMsg.getEnglishContent();
                        break;
                    case PreferenceProxy.LANGUAGE_TYPE_FRENCH:
                        tempErrMsgContent = errMsg.getFrenchContent();
                        break;
                }
            }catch(Exception e) {
                Log.print(e);
            }
            if (tempErrMsgContent != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ErrorMessageUI.start]Error message content is exist.");
                }
                if (errMsgDynamicTagList != null) {
                    Enumeration enu = errMsgDynamicTagList.keys();
                    while(enu.hasMoreElements()){
                        String key =(String)enu.nextElement();
                        if (key == null) {
                            continue;
                        }
                        if (tempErrMsgContent.indexOf(key)!= -1) {
                            String dynamicValue = (String)errMsgDynamicTagList.get(key);
                            if (dynamicValue != null) {
                                tempErrMsgContent = replaceAll(tempErrMsgContent, key, dynamicValue);
                            }
                        }
                    }
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ErrorMessageUI.start]Error message content is null.");
                }
                tempErrMsgContent = "N/A";
            }
            errMsgContent = tempErrMsgContent;
            //Error message icon
            try{
                errMsgIconName = errMsg.getIconName();
            }catch(Exception e) {
                e.printStackTrace();
            }
            Image tempImgIcon = null;
            if (errMsgIconName != null && (errMsgIconName.trim()).length() > 0) {
                byte[] iconImgSrc = DataManager.getInstance().getErrorMessageIconImageSource(errMsgIconName);
                if (iconImgSrc != null) {
                    tempImgIcon = FrameworkMain.getInstance().getImagePool().createImage(iconImgSrc, errMsgIconName);
                    FrameworkMain.getInstance().getImagePool().waitForAll();
                }
            }
            if (tempImgIcon == null) {
//                imgIcon = imgDefaultIcon;
            } else {
                imgIcon = tempImgIcon;
            }
            //Error message buttons
            errMsgButList = errMsg.getErrorMessageButtonList();
            errMsgButCount = 0;
            if (errMsgButList != null ){
                errMsgButCount = errMsgButList.length;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[ErrorMessageUI.start]Error message button count : " + errMsgButCount);
            }
        }
        curIdx = 0;
        CommunicationManager.getInstance().requestAddScreenSaverConfirmListener();
    }
    
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageUI.stop]errMsgListener : " + errMsgListener);
        }
        CommunicationManager.getInstance().requestRemoveScreenSaverConfirmListener();
        CommunicationManager.getInstance().requestResetScreenSaverTimer(); //Timer Reset하도록 추가
        if (errMsgDynamicTagList != null) {
            errMsgDynamicTagList.clear();
            errMsgDynamicTagList = null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageUI.stop]errMsgDynamicTags : " + errMsgDynamicTagList);
        }
        errMsgType = -1;
        errCode = null;
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageUI.stop]errorCode : " + errCode);
        }
        errMsgTitle = null;
        errMsgContent = null;
        if (errMsgIconName != null) {
            DataCenter.getInstance().removeImage(errMsgIconName);
            imgIcon = null;
            errMsgIconName = null;
        }
        if (imgDefaultIcon != null) {
            DataCenter.getInstance().removeImage("icon_noti_red.png");
            imgDefaultIcon = null;
        }
        errMsgButList = null;
        errMsgListener = null;
    }
    public boolean handleKey(int keyCode) {
        switch(keyCode) {
            case  HRcEvent.VK_UP:
            case  HRcEvent.VK_DOWN:
                return true;
            case  HRcEvent.VK_LEFT:
                if (errMsgButList == null) {
                    return true;
                }
                if (curIdx == 0) {
                    return true;
                }
                curIdx --;
                repaint();
                return true;
            case  HRcEvent.VK_RIGHT:
                if (errMsgButList == null) {
                    return true;
                }
                if (curIdx == errMsgButList.length-1) {
                    return true;
                }
                curIdx ++;
                repaint();
                return true;
            case  HRcEvent.VK_ENTER:
                if (errMsgButList == null || errMsgButList.length==0 || errMsgButList[curIdx] == null) {
                    return true;
                }
                new ClickingEffect(this, 5).start(((errMsgButList.length == 1) ? 403 : 321) + (curIdx * 164), 360, 154, 33);
                String buttonType = errMsgButList[curIdx].getButtonType();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[ErrorMessageUI.handleKey]Button type : " + buttonType);
                }
                if (buttonType != null) {
                	

                    //R7.4 freelife VDTRMASTER-6024
                    if (errCode.startsWith("SDV")) {
                    	if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey] ");
                        }
                    	
                    	try {
                    		EpgService es = CommunicationManager.getInstance().getEpgService();
                        	if (es != null) {
                            	es.sdvErrorPopupClosed(errCode);
                        	}
                    	} catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                    
                    if (buttonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_APPLICATION)){
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey]Button type is application.");
                        }
                        if (errMsgListener != null) {
                            try{
                                errMsgListener.actionPerformed(ErrorMessageListener.BUTTON_TYPE_LAUNCH_APP);
                            }catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                                                
                        String appName = errMsgButList[curIdx].getTargetApplicationName();
                        String[] appParams = errMsgButList[curIdx].getTargetApplicationParameterList();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey]App name : " + appName);
                            Log.printDebug("[ErrorMessageUI.handleKey]App params : " + appParams);
                        }
                        
                        //R7.4 freelife VDTRMASTER-6128
                        if (errCode.startsWith("PVR715")) {
                        	if (Log.DEBUG_ON) {
                                Log.printDebug("[ErrorMessageUI.handleKey]Button type is application.");
                            }
                        	try {
                        		String channelCallLetter = "";
                        	
                        		if (Log.DEBUG_ON) {
                                    Log.printDebug("[ErrorMessageUI.handleKey] this.errMsgDynamicTagList " + this.errMsgDynamicTagList);
                                }
                        		if (this.errMsgDynamicTagList != null) {
                                    Enumeration enu = this.errMsgDynamicTagList.keys();
                                    while(enu.hasMoreElements()){
                                        String key =(String)enu.nextElement();
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[ErrorMessageUI.handleKey] key " + key);
                                        }
                                        if (key == null) {
                                            continue;
                                        }
                                        if ("<NUMBER>".indexOf(key)!= -1) {
                                            String dynamicValue = (String)this.errMsgDynamicTagList.get(key);
                                            
                                            if (Log.DEBUG_ON) {
                                                Log.printDebug("[ErrorMessageUI.handleKey] dynamicValue " + dynamicValue);
                                            }
                                            
                                            if (dynamicValue != null) {
                                            	
                                            	EpgService es = CommunicationManager.getInstance().getEpgService();
                                            	TvChannel channel = es.getChannelByNumber(Integer.parseInt(dynamicValue));
                                            	channelCallLetter = channel.getCallLetter();
                                            }
                                        }
                                    }
                                }
                        		if (Log.DEBUG_ON) {
                                    Log.printDebug("[ErrorMessageUI.handleKey] channelCallLetter " + channelCallLetter);
                                }
                                ISAService is = CommunicationManager.getInstance().getISAService();
                                is.showSubscriptionPopup(channelCallLetter, ISAService.ENTRY_POINT_EPG_E01);                                
                            } catch (Exception ex) {
                                Log.print(ex);
                            }
                        } else
                        	CommunicationManager.getInstance().requestStartUnboundApplication(appName, appParams);
                    } else if (buttonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CHANNEL)){
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey]Button type is channel.");
                        }
                        if (errMsgListener != null) {
                            try{
                                errMsgListener.actionPerformed(ErrorMessageListener.BUTTON_TYPE_SELECT_CHANNEL);
                            }catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                        String callLetter = errMsgButList[curIdx].getTargetCallLetter();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey]Call letter : " + callLetter);
                        }
                        CommunicationManager.getInstance().requestChangeChannel(callLetter);
                    } else if (buttonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CLOSE)){
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey]Button type is close.");
                        }
                        if (errMsgListener != null) {
                            try{
                                errMsgListener.actionPerformed(ErrorMessageListener.BUTTON_TYPE_CLOSE);
                            }catch(Exception e) {
                                e.printStackTrace();
                            }
                        }
                        
                        //R7.4 freelife VDTRMASTER-6128
                        if (errCode.startsWith("PVR715")) {
                        	if (Log.DEBUG_ON) {
                                Log.printDebug("[ErrorMessageUI.handleKey] delete upcoming recordings");
                            }
                        	try {
                        		String channelCallLetter = "";
                            		
                        		if (Log.DEBUG_ON) {
                                    Log.printDebug("[ErrorMessageUI.handleKey] this.errMsgDynamicTagList " + this.errMsgDynamicTagList);
                                }
                        		if (this.errMsgDynamicTagList != null) {
                                    Enumeration enu = this.errMsgDynamicTagList.keys();
                                    while(enu.hasMoreElements()){
                                        String key =(String)enu.nextElement();
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[ErrorMessageUI.handleKey] key " + key);
                                        }
                                        if (key == null) {
                                            continue;
                                        }                                        
                                        if ("<NUMBER>".indexOf(key)!= -1) {
                                            String dynamicValue = (String)this.errMsgDynamicTagList.get(key);
                                            
                                            if (Log.DEBUG_ON) {
                                                Log.printDebug("[ErrorMessageUI.handleKey] dynamicValue " + dynamicValue);
                                            }
                                            
                                            if (dynamicValue != null) {
                                            	
                                            	EpgService es = CommunicationManager.getInstance().getEpgService();
                                            	if (es != null) {
	                                            	TvChannel channel = es.getChannelByNumber(Integer.parseInt(dynamicValue));
	                                            	channelCallLetter = channel.getCallLetter();
                                            	} else
                                            		Log.printDebug("EpgService is null");
                                            }
                                        }
                                    }
                                }
                        		if (Log.DEBUG_ON) {
                                    Log.printDebug("[ErrorMessageUI.handleKey] channelCallLetter " + channelCallLetter);
                                }
                                
                        		//delete upcoming recordings
                        		PvrService pservice = CommunicationManager.getInstance().getPvrService();
                        		if (pservice != null)
                        			pservice.removeAllUpcomingRecordings(channelCallLetter);
                        		else
                        			Log.printDebug("PvrService is null");
                            } catch (Exception ex) {
                                Log.print(ex);
                            }
                        } else if (errCode.startsWith("VOD110")) {
                        	try {
                        		MonitorService ms = CommunicationManager.getInstance().getMonitorService();
                        		if (ms != null)
                        			ms.exitToChannel();
                        		else
                        			Log.printDebug("MonitorService is null");
                            } catch (Exception ex) {
                                Log.print(ex);
                            }
                        }
                    } else if (buttonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_REBOOT)) {
                    	if (Log.DEBUG_ON) {
                    		Log.printDebug("[ErrorMessageUI.hanldeKey]Button type is reboot.");
                    	}
                    	
                    	Host.getInstance().reboot();
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[ErrorMessageUI.handleKey]Request hide error message by OK key.");
                    }
                    ErrorMessageManager.getInstance().hideErrorMessage();
                }
                return true;
            case  OCRcEvent.VK_EXIT:
                if (errMsgButCount > 0) {
                    if (errMsgListener != null) {
                        try {
                            errMsgListener.actionPerformed(ErrorMessageListener.BUTTON_TYPE_EXIT);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[ErrorMessageUI.handleKey]Request hide error message by Exit key.");
                    }
                    //R7.4 freelife VDTRMASTER-6024
                    if (errCode.startsWith("SDV")) {
                    	if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey] ");
                        }
                    	
                    	try {
                    		EpgService es = CommunicationManager.getInstance().getEpgService();
                        	if (es != null) {
                            	es.sdvErrorPopupClosed(errCode);
                        	}
                    	} catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                    //R7.4 freelife VDTRMASTER-6128
                    if (errCode.startsWith("PVR715")) {
                    	if (Log.DEBUG_ON) {
                            Log.printDebug("[ErrorMessageUI.handleKey] delete upcoming recordings");
                        }
                    	try {
                    		String channelCallLetter = "";
                        		
                    		if (Log.DEBUG_ON) {
                                Log.printDebug("[ErrorMessageUI.handleKey] this.errMsgDynamicTagList " + this.errMsgDynamicTagList);
                            }
                    		if (this.errMsgDynamicTagList != null) {
                                Enumeration enu = this.errMsgDynamicTagList.keys();
                                while(enu.hasMoreElements()){
                                    String key =(String)enu.nextElement();
                                    if (Log.DEBUG_ON) {
                                        Log.printDebug("[ErrorMessageUI.handleKey] key " + key);
                                    }
                                    if (key == null) {
                                        continue;
                                    }                                        
                                    if ("<NUMBER>".indexOf(key)!= -1) {
                                        String dynamicValue = (String)this.errMsgDynamicTagList.get(key);
                                        
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[ErrorMessageUI.handleKey] dynamicValue " + dynamicValue);
                                        }
                                        
                                        if (dynamicValue != null) {
                                        	
                                        	EpgService es = CommunicationManager.getInstance().getEpgService();
                                        	if (es != null) {
                                            	TvChannel channel = es.getChannelByNumber(Integer.parseInt(dynamicValue));
                                            	channelCallLetter = channel.getCallLetter();
                                        	} else
                                        		Log.printDebug("EpgService is null");
                                        }
                                    }
                                }
                            }
                    		if (Log.DEBUG_ON) {
                                Log.printDebug("[ErrorMessageUI.handleKey] channelCallLetter " + channelCallLetter);
                            }
                            
                    		//delete upcoming recordings
                    		PvrService pservice = CommunicationManager.getInstance().getPvrService();
                    		if (pservice != null)
                    			pservice.removeAllUpcomingRecordings(channelCallLetter);
                    		else
                    			Log.printDebug("PvrService is null");
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                    ErrorMessageManager.getInstance().hideErrorMessage();
                }
                return true;
                // DDC-113 Visually Impaired : “STAR” key is blocked.
            case KeyCodes.STAR:
            	return true;
        }
        return false;
    }
    /*****************************************************************
     * methods - GUI-related
     *****************************************************************/
    public String getErrorCode() {
        return errCode;
    }
    public int getErrorMessageType() {
        return errMsgType;
    }
    public String getErrorMessageTitle() {
        return errMsgTitle;
    }
    public String getErrorMessageContent() {
        return errMsgContent;
    }
    public Image getErrorMessageIconImage() {
        return imgIcon;
    }
    public ErrorMessageButton[] getErrorMessageButtonList() {
        return errMsgButList;
    }
    public int getCurrentIndex() {
        return curIdx;
    }
    public Hashtable getErrorMessageDynamicTagList() {
        return errMsgDynamicTagList;
    }
    public String replaceAll(String src, String regex, String replacement) {
        if (src == null) {
            return null;
        }
        if (regex == null || replacement == null || regex.length() == 0 || replacement.length() == 0) {
            return src;
        }
        String dest = src;
        int idx = -1;
        int regexLth = regex.length();
        while ((idx = dest.indexOf(regex)) != -1) {
           String frontStr = dest.substring(0, idx);
           String rearStr = dest.substring(idx + regexLth);
           dest = frontStr + replacement + rearStr;
        }
        return dest;
    }
}

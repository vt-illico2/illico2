/**
 * 
 */
package com.videotron.tvi.illico.errormessage.ui;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.errormessage.communication.CommunicationManager;
import com.videotron.tvi.illico.errormessage.controller.DataManager;
import com.videotron.tvi.illico.errormessage.controller.ErrorMessageManager;
import com.videotron.tvi.illico.errormessage.data.CommonMessageImpl;
import com.videotron.tvi.illico.errormessage.gui.CommonMessageRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * @author zestyman
 *
 */
public class CommonMessageUI extends UIComponent {
	
	private CommonMessageRenderer renderer;
	private ErrorMessageListener emListener;
	private CommonMessage commonMessage;
	
	// DDC-114
	private String codeToc;

	/**
	 * 
	 */
	public CommonMessageUI() {
		renderer = new CommonMessageRenderer();
		setRenderer(renderer);
	}

	public void start(String commonCode) {
		commonMessage = DataManager.getInstance().getCommonMessage(commonCode);
		
		// DDC-114
		codeToc = null;
		if (commonCode != null) {
			if (commonCode.indexOf("850") > -1) {
				codeToc = DataCenter.getInstance().getString("TxtErrorMessage.Code850");
			} else if (commonCode.indexOf("851") > -1) {
				codeToc = DataCenter.getInstance().getString("TxtErrorMessage.Code851");
			} else {
				codeToc = DataCenter.getInstance().getString("TxtErrorMessage.NonUHD");
			}
		}
		
		if (Log.DEBUG_ON) {
			Log.printDebug("CommonMessageUI, start(), commonCode=" + commonCode + ", codeToc=" + codeToc);
		}
		
		super.start();
		CommunicationManager.getInstance().requestAddScreenSaverConfirmListener();
	}
	
	public void stop() {
		CommunicationManager.getInstance().requestRemoveScreenSaverConfirmListener();
		CommunicationManager.getInstance().requestResetScreenSaverTimer();
		commonMessage = null;
	}
	
	public void setErrorMessageListener(ErrorMessageListener listener) {
		this.emListener = listener;
	}
	
	public boolean handleKey(int keyCode) {
		
		switch (keyCode) {
		case KeyEvent.VK_ENTER:
			if (emListener != null) {
				try {
					emListener.actionPerformed(ErrorMessageListener.BUTTON_TYPE_CLOSE);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			ErrorMessageManager.getInstance().hideCommonMessage();
			return true;
			// DDC-113 Visually Impaired : “STAR” key is blocked.
		case KeyCodes.STAR:
        	return true;
		}
		
		return false;
	}
	
	public CommonMessage getCommonMessage() {
		return commonMessage;
	}
	
	// DDC-114
	public String getCodeToc() {
		return codeToc;
	}
}

package com.videotron.tvi.illico.errormessage;

import com.videotron.tvi.illico.util.Environment;

/**
 * This class is the Resource class of main menu.
 * 
 * @version $Revision: 1.22.6.2 $ $Date: 2017/09/27 16:38:26 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    //public static final String APP_VERSION = "R5.1.0";
	public static final String APP_VERSION = "(R7.4).2.1";
    /** Application Name. */
    public static final String APP_NAME = "ErrorMessage";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    public static final boolean IS_MS_DATA = false;
    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;
    /*********************************************************************************
     * Look up Milis-related
     *********************************************************************************/
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS_MONITOR = 2000;
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS_VBM = 2000;
    /** Retry Look up Millis - User Profile and Preference. */
    public static final int RETRY_LOOKUP_MILLIS_UPP = 1000;
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM= "ERR501";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "ERR502";
}
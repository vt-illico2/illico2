package com.videotron.tvi.illico.errormessage;
import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.errormessage.communication.CommunicationManager;
import com.videotron.tvi.illico.errormessage.communication.PreferenceProxy;
import com.videotron.tvi.illico.errormessage.controller.DataManager;
import com.videotron.tvi.illico.errormessage.controller.ErrorMessageManager;
import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    private XletContext xletContext;
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        Log.setApplication(this);
        if (Log.INFO_ON) {
            Log.printInfo("[App.initXlet]Start.");
        }
        FrameworkMain.getInstance().init(this);

        CommunicationManager.getInstance().init();
        PreferenceProxy.getInstance().init();
        ErrorMessageManager.getInstance().init();
        if (Log.INFO_ON) {
            Log.printInfo("[App.initXlet]End.");
        }
    }
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("[App.startXlet]Start.");
        }
        FrameworkMain.getInstance().start();
        if (Log.INFO_ON) {
            Log.printInfo("[App.startXlet]End.");
        }
    }
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("[App.pauseXlet]Start.");
        }
        FrameworkMain.getInstance().pause();
        if (Log.INFO_ON) {
            Log.printInfo("[App.pauseXlet]End.");
        }
    }
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("[App.destroyXlet]Start.");
        }
        ErrorMessageManager.getInstance().dispose();
        PreferenceProxy.getInstance().dispose();
        CommunicationManager.getInstance().dispose();
        DataManager.getInstance().dispose();
        FrameworkMain.getInstance().destroy();
        if (Log.INFO_ON) {
            Log.printInfo("[App.destroyXlet]End.");
        }
    }
    
    /*********************************************************************************
     * ApplicationConfig-implemented
     *********************************************************************************/
	public void init() {
        DataManager.getInstance().init();
	}
    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }
    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }
    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

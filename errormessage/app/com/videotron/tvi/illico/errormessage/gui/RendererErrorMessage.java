package com.videotron.tvi.illico.errormessage.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.errormessage.Rs;
import com.videotron.tvi.illico.errormessage.communication.PreferenceProxy;
import com.videotron.tvi.illico.errormessage.ui.ErrorMessageUI;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageButton;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class RendererErrorMessage extends Renderer {
    private static final Rectangle BOUND = new Rectangle(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
    
    private static final int TEXT_MAX_WIDTH = 300;
    private static final int BUTTON_MAX_WIDTH = 146;
    
//    private Image imgPopRed;
//    private Image imgPopGlow;
//    private Image imgPopShadow;
//    private Image imgPopHigh;
    private Image imgPopGap;
    private Image imgPopTxtBG;
    private Image imgButtonFocus;
    private Image imgButtonBasic;

    private  Color c003003003;
    private  Color c035035035;
    private  Color c160160160;
    private  Color c229229229;
    private  Color c252202004;
    private  Color c000000000204;
    private Color c255100100;
    
    private Font f16;
    private Font f18;
    private Font f24;
    private FontMetrics fm16;
    private FontMetrics fm18;
    private FontMetrics fm24;
    
    public void prepare(UIComponent c) {
//        imgPopRed = DataCenter.getInstance().getImage("pop_red_538.png");
//        imgPopGlow = DataCenter.getInstance().getImage("05_pop_glow_04.png");
//        imgPopShadow = DataCenter.getInstance().getImage("pop_sha.png");
//        imgPopHigh = DataCenter.getInstance().getImage("pop_high_402.png");
        imgPopGap = DataCenter.getInstance().getImage("pop_gap_379.png");
        imgPopTxtBG = DataCenter.getInstance().getImage("error_txt_bg.png");
        imgButtonFocus = DataCenter.getInstance().getImage("05_focus.png");
        imgButtonBasic = DataCenter.getInstance().getImage("05_focus_dim.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
        
        c003003003 = new Color(3, 3, 3);
        c035035035 = new Color(35, 35, 35);
        c160160160 = new Color(160, 160, 160);
        c229229229 = new Color(229, 229, 229);
        c252202004 = new Color(252, 202, 4);
        c000000000204 = new DVBColor(0, 0, 0, 204);
        c255100100 = new Color (255, 100, 100);
        
        f16 = FontResource.BLENDER.getFont(16, true);
        f18 = FontResource.BLENDER.getFont(18, true);
        f24 = FontResource.BLENDER.getFont(24, true);
        fm16 = FontResource.getFontMetrics(f16);
        fm18 = FontResource.getFontMetrics(f18);
        fm24 = FontResource.getFontMetrics(f24);
    }
    
    protected void paint(Graphics g, UIComponent c) {
        //Gets error message data
        ErrorMessageUI scene = (ErrorMessageUI)c;
        int emType = scene.getErrorMessageType();
        String emCode = scene.getErrorCode();
        String title = scene.getErrorMessageTitle();
        String message = scene.getErrorMessageContent();

        //Popup background
        g.setColor(c000000000204);
        g.fillRect(0, 0, 960, 540);
//        if (emType == ErrorMessage.TYPE_ALERT) {
//            if (imgPopRed != null) {
//                g.drawImage(imgPopRed, 216, 56, c);
//            }
//        }else {
//            if (imgPopGlow != null) {
//                g.drawImage(imgPopGlow, 250, 107, c);
//            }
//        }
//        if (imgPopShadow != null) {
//            g.drawImage(imgPopShadow, 280, 407, 404, 79, c);
//        }
        g.setColor(c035035035);
        g.fillRect(280, 135, 402, 276);
//        if (imgPopHigh != null) {
//            g.drawImage(imgPopHigh, 280, 135, c);
//        }
        if (imgPopGap != null) {
            g.drawImage(imgPopGap, 292, 173, c);
        }
        if (imgPopTxtBG != null) {
            g.drawImage(imgPopTxtBG, 293, 187, c);
        }
        //Title
        int totalTitleWth = 0;
        Image imgIcon = scene.getErrorMessageIconImage();
        int imgIconWth = 0;
        if (imgIcon != null) {
            imgIconWth = 27;
            totalTitleWth = imgIconWth + 4;
        }
        if (title != null) {
            int titleWth = fm24.stringWidth(title);
            if (titleWth > TEXT_MAX_WIDTH) {
                title = TextUtil.shorten(title, fm24, TEXT_MAX_WIDTH);
                titleWth = TEXT_MAX_WIDTH;
            }
            totalTitleWth += titleWth;
        }
        if (imgIcon != null) {
            g.drawImage(imgIcon, 480 - (totalTitleWth/2), 144, c);
        }
        if (title != null) {
            g.setFont(f24);
            if (emType == ErrorMessage.TYPE_ALERT) {
            	g.setColor(c255100100);
            } else {
            	g.setColor(c252202004);
            }
            g.drawString(title, 480 - (totalTitleWth/2) + 4 + imgIconWth, 164);
        }
        //Error code
        g.setFont(f16);
        g.setColor(c160160160);
        String txtErrorCode = DataCenter.getInstance().getString("TxtErrorMessage.ErrorCode");
        if (txtErrorCode != null) {
            txtErrorCode = txtErrorCode + " : ";
            g.drawString(txtErrorCode, 297, 347);
        }
        if (emCode != null) {
            int txtErrorCodeWth = fm16.stringWidth(txtErrorCode);
            g.drawString(emCode, 297 + txtErrorCodeWth, 347);
        }
        //Error text  
        g.setFont(f18);
        g.setColor(c229229229);
        String[] messages = TextUtil.split(message, fm18, 350, 6);
        if (messages != null) {
            int messagesLth = messages.length;
            int startY = 0;
            switch(messagesLth) {
                case 1:
                    startY = 260;
                    break;
                case 2:
                    startY = 254;
                    break;
                case 3:
                    startY = 240;
                    break;
                case 4:
                    startY = 234;
                    break;
                case 5:
                    startY = 220;
                    break;
                case 6:
                    startY = 214;
                    break;
            }
            for (int i=0; i<messagesLth; i++) {
                if (messages[i] == null) {
                    continue;
                }
                g.drawString(messages[i], 482 - (fm18.stringWidth(messages[i])/2), startY + (i*20));
            }
        }
        //Buttons
        ErrorMessageButton[] buttons = scene.getErrorMessageButtonList();
        int curIdx = scene.getCurrentIndex();
        if (buttons != null) {
            g.setFont(f18);
            g.setColor(c003003003);
            int buttonsLth = buttons.length;
            int startX = 396;
            switch(buttonsLth) {
                case 1:
                    startX = 402;
                    break;
                default:
                    startX = 320;
                    break;
            }
            for (int i=0; i<buttonsLth; i++) {
                if (buttons[i] == null) {
                    continue;
                }
                Image imgButton = null;
                if (i == curIdx) {
                    imgButton = imgButtonFocus;
                } else {
                    imgButton = imgButtonBasic;
                }
                if (imgButton != null) {
                    g.drawImage(imgButton, startX + (i*164), 360, c);
                }
                String btName = null;
                int langType = PreferenceProxy.getInstance().getLanguageType();
                switch(langType) {
                    case PreferenceProxy.LANGUAGE_TYPE_ENGLISH:
                        btName = buttons[i].getEnglishButtonName();
                        break;
                    case PreferenceProxy.LANGUAGE_TYPE_FRENCH:
                        btName = buttons[i].getFrenchButtonName();
                        break;
                }
                if (btName != null) {
                    Hashtable errMsgDynamicTags = scene.getErrorMessageDynamicTagList();
                    if (errMsgDynamicTags != null) {
                        Enumeration enu = errMsgDynamicTags.keys();
                        while(enu.hasMoreElements()){
                            String key =(String)enu.nextElement();
                            if (key == null) {
                                continue;
                            }
                            if (btName.indexOf(key)!= -1) {
                                String dynamicValue = (String)errMsgDynamicTags.get(key);
                                if (dynamicValue != null) {
                                    btName = scene.replaceAll(btName, key, dynamicValue);
                                }
                            }
                        }
                    }
                } else {
                    btName = "N/A";
                }
                int btNameWth = fm18.stringWidth(btName);
                if (btNameWth > BUTTON_MAX_WIDTH) {
                    btName = TextUtil.shorten(btName, fm18, BUTTON_MAX_WIDTH);
                    btNameWth = BUTTON_MAX_WIDTH;
                }
                g.drawString(btName, startX + 78 - (btNameWth / 2) + (i * 164), 381);
            }
        }
    }
    public Rectangle getPreferredBounds(UIComponent c) {
        return BOUND;
    }
}

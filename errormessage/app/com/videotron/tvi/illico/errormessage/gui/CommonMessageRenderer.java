/**
 * 
 */
package com.videotron.tvi.illico.errormessage.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.errormessage.communication.PreferenceProxy;
import com.videotron.tvi.illico.errormessage.controller.DataManager;
import com.videotron.tvi.illico.errormessage.data.CommonMessageImpl;
import com.videotron.tvi.illico.errormessage.ui.CommonMessageUI;
import com.videotron.tvi.illico.errormessage.ui.ErrorMessageUI;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageButton;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class CommonMessageRenderer extends Renderer {
	
    private static final int BUTTON_MAX_WIDTH = 146;
    
    private Image buttonImg;
    private Image bannerImg;
    private Color dimmedBgColor;
    private Color bgPopupColor;
    private Color lineColor;
    private Color titleColor;
    private Color descColor;
    private Color buttonColor;
    
    private Font f18;
    private Font f24;
    private FontMetrics fm18;
    private FontMetrics fm24;

	/**
	 * 
	 */
	public CommonMessageRenderer() {
	}

	public Rectangle getPreferredBounds(UIComponent c) {
		return Constants.SCREEN_BOUNDS;
	}

	public void prepare(UIComponent c) {
        
        dimmedBgColor = new DVBColor(12, 12, 12, 230);
        bgPopupColor = new Color(44, 44, 44);
        lineColor = new Color(57, 57, 57);
        titleColor = new Color(252, 202, 4);
        descColor = new Color(229, 229, 229);
        buttonColor = new Color(3, 3, 3);
        
        buttonImg = DataCenter.getInstance().getImage("05_focus.png");
        
        
        f18 = FontResource.BLENDER.getFont(18, true);
        f24 = FontResource.BLENDER.getFont(24, true);
        fm18 = FontResource.getFontMetrics(f18);
        fm24 = FontResource.getFontMetrics(f24);
        
        CommonMessageUI ui = (CommonMessageUI)c;
		CommonMessageImpl cm = (CommonMessageImpl) ui.getCommonMessage();
		
		byte[] src = DataManager.getInstance().getErrorMessageIconImageSource(cm.getBannerName());
		if (src != null) {
			bannerImg = FrameworkMain.getInstance().getImagePool().createImage(src, cm.getBannerName());
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("CommonMessageRenderer, there is no banner image.");
			}
		}
        
        FrameworkMain.getInstance().getImagePool().waitForAll();
	}

	protected void paint(Graphics g, UIComponent c) {
		CommonMessageUI ui = (CommonMessageUI)c;
		CommonMessage cm = ui.getCommonMessage();
        String title = TextUtil.EMPTY_STRING;
		try {
			title = cm.getTitle();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
        String message = TextUtil.EMPTY_STRING;
		try {
			message = cm.getFunctionalText();
		} catch (RemoteException e) {
			e.printStackTrace();
		}

        //Popup background
        g.setColor(dimmedBgColor);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        g.setColor(bgPopupColor);
        g.fillRect(225, 93, 510, 344);
        g.setColor(lineColor);
        g.fillRect(265, 131, 430, 1);
		
        //Title
        if (title != null) {
            g.setFont(f24);
            g.setColor(titleColor);
            if (fm24.stringWidth(title) > 430) {
            	title = TextUtil.shorten(title, fm24, 430);
            }
            
            GraphicUtil.drawStringCenter(g, title, 481, 119, fm24);
        }
        
        // message text  
        g.setFont(f18);
        g.setColor(descColor);
        String[] messages = TextUtil.split(message, fm18, 410, "\n");
        
        // DDC-114
        String codeToc = ui.getCodeToc();
        
        if (messages != null) {
        	if (codeToc == null) {
	            int startY = 199;
	            
	            startY -= ((messages.length - 1) * 21 / 2);
	            
	            for (int i = 0; i < messages.length; i++) {
	            	GraphicUtil.drawStringCenter(g, messages[i], 481, startY + i * 21, fm18);
	            }
        	} else {
        		int startY = 189;
	            
	            startY -= ((messages.length - 1) * 21 / 2);
	            
	            for (int i = 0; i < messages.length; i++) {
	            	GraphicUtil.drawStringCenter(g, messages[i], 481, startY + i * 21, fm18);
	            }
	            
	            g.setColor(titleColor);
	            int codeTocY = 189 + ((messages.length - 1) * 21 / 2) + 26;
	            GraphicUtil.drawStringCenter(g, codeToc, 481, codeTocY, fm18);
        	}
        }
        
        // banner
        if (bannerImg != null) {
        	g.drawImage(bannerImg, 256, 253, 448, 114, c);
        }
        
        
        // button
        g.setFont(f18);
        g.setColor(buttonColor);

        if (buttonImg != null) {
            g.drawImage(buttonImg, 399, 388, c);
        }
        String btName = TextUtil.EMPTY_STRING;
		try {
			btName = cm.getActionButtonText();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
        int btNameWth = fm18.stringWidth(btName);
        if (btNameWth > BUTTON_MAX_WIDTH) {
            btName = TextUtil.shorten(btName, fm18, BUTTON_MAX_WIDTH);
            btNameWth = BUTTON_MAX_WIDTH;
        }
        GraphicUtil.drawStringCenter(g, btName, 479, 409, fm18);
	}

}

package com.videotron.tvi.illico.errormessage.data;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.util.Constants;

public class CommonMessageImpl implements CommonMessage {

	private String[] titles = new String[2];
	private String[] functionalTexts = new String[2];
	private String[] shortFunctionalTexts = new String[2];
	private String[] actionButtonTexts = new String[2];
	
	private String bannerName;
	
	public CommonMessageImpl(int langSize) {
		titles = new String[langSize];
		functionalTexts = new String[langSize];
		shortFunctionalTexts = new String[langSize];
		actionButtonTexts = new String[langSize];
	}

	public String getTitle() throws RemoteException {
		return titles[getLanguageIndex()];
	}

	public String getFunctionalText() throws RemoteException {
		return functionalTexts[getLanguageIndex()];
	}

	public String getShortFunctionalText() throws RemoteException {
		return shortFunctionalTexts[getLanguageIndex()];
	}

	public String getActionButtonText() throws RemoteException {
		return actionButtonTexts[getLanguageIndex()];
	}

	public String[] getTitles() throws RemoteException {
		return titles;
	}

	public String[] getFunctionalTexts() throws RemoteException {
		return functionalTexts;
	}

	public String[] getShortFunctionalTexts() throws RemoteException {
		return shortFunctionalTexts;
	}

	public String[] getActionButtonTexts() throws RemoteException {
		return actionButtonTexts;
	}

	private int getLanguageIndex() {
		String curLang = FrameworkMain.getInstance().getCurrentLanguage();
		
		if (curLang.equals(Constants.LANGUAGE_FRENCH)) {
			return 0;
		} else if (curLang.equals(Constants.LANGUAGE_ENGLISH)) {
			return 1;
		}
		
		return 0;
	}
	
	public String getBannerName() {
		return bannerName;
	}
	
	public void setTitle(int langIdx, String title) {
		titles[langIdx] = title;
	}
	
	public void setFunctionalTexts(int langIdx, String message) {
		functionalTexts[langIdx] = message;
	}
	
	public void setShortFunctionalTexts(int langIdx, String message) {
		shortFunctionalTexts[langIdx] = message;
	}
	
	public void setActionButtonTexts(int langIdx, String btnName) {
		actionButtonTexts[langIdx] = btnName;
	}
	
	public void setBanner(String bannerName) {
		this.bannerName = bannerName;
	}
}

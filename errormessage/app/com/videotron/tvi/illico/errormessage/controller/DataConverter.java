package com.videotron.tvi.illico.errormessage.controller;

import java.io.File;
import java.util.Properties;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.errormessage.Util;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageButton;
import com.videotron.tvi.illico.log.Log;

public class DataConverter {
    public static byte[] getBytesGeneralConfig() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesGeneralConfig]start.");
        }
        File fileGeneralConfigIB = new File("resource/test_data/prop_data/general_config.prop");
        Properties prop = Util.loadProperties(fileGeneralConfigIB);

        byte[] src = new byte[4000];
        int index = 0;
        //Version
        String tempVersion = prop.getProperty("version");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesGeneralConfig]Version : "+tempVersion);
        }
        byte version = 0;
        if (tempVersion != null) {
            version = Byte.parseByte(tempVersion);
        }
        src[index++] = version;
        //Display time
        String tempDisplayTime = prop.getProperty("display_time");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesGeneralConfig]Display time : "+tempDisplayTime);
        }
        int displayTimeI = -1;
        if (tempDisplayTime != null) {
            try{
                displayTimeI = Integer.parseInt(tempDisplayTime);
            }catch(Exception ignore) {
            }
        }
        byte[] displayTime = Util.intTo2Byte(displayTimeI);
        System.arraycopy(displayTime, 0, src, index, 2);
        index += 2;
        return src;
    }

    public static byte[] getBytesErrorCode() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]start.");
        }
        File fileErrorCodeIB = new File("resource/test_data/prop_data/error_code.prop");
        Properties prop = Util.loadProperties(fileErrorCodeIB);

        byte[] src = new byte[4000];
        int index = 0;
        //Version
        String tempVersion = prop.getProperty("version");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Version : "+tempVersion);
        }
        byte version = 0;
        if (tempVersion != null) {
            version = Byte.parseByte(tempVersion);
        }
        src[index++] = version;
        //Language type size
        String tempLangTypeSize = prop.getProperty("language_type_size");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Language type size : "+tempLangTypeSize);
        }
        byte langTypeSize = -1;
        if (tempLangTypeSize != null) {
            try{
                langTypeSize = Byte.parseByte(tempLangTypeSize);
            }catch(Exception ignore) {
            }
        }
        src[index++] = langTypeSize;
        //Error list
        String tempErrorList = prop.getProperty("error_list");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Error list : "+tempErrorList);
        }
        StringTokenizer st=new StringTokenizer(tempErrorList, ",");
        int stCount = st.countTokens();
        byte[] errorListSize = Util.intTo2Byte(stCount);
        System.arraycopy(errorListSize, 0, src, index, 2);
        index += 2;
        for (int i=0; i<stCount; i++) {
            //Error id
            String errorId = st.nextToken();
            byte[] errorIdBytes = Util.intTo2Byte(Integer.parseInt(errorId));
            System.arraycopy(errorIdBytes, 0, src, index, 2);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error id - "+i+" : "+errorId);
            }
            //Error message type
            String tempErrMsgType = prop.getProperty("error_message_type_"+errorId);
            src[index++] = (byte) Byte.parseByte(tempErrMsgType);
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message type - "+i+" : "+tempErrMsgType);
            }
            for (int j=0; j<langTypeSize; j++) {
                //Language type
                String tempLangType = prop.getProperty("language_type_" + errorId + "_" + j);
                byte[] langType = Util.stringToNByte(tempLangType, 2);
                System.arraycopy(langType, 0, src, index, 2);
                index += 2;
                //Title
                String tempTitle = prop.getProperty("title_"+errorId+"_"+j);
                byte[] title = tempTitle.getBytes();
                int titleLth = title.length;
                src[index++] = (byte)titleLth;
                System.arraycopy(title, 0, src, index, titleLth);
                index += titleLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message title - "+j+" : "+tempTitle);
                }
                //Message
                String tempMessage = prop.getProperty("message_"+errorId+"_"+j);
                byte[] message = tempMessage.getBytes();
                int messageLthI = message.length;
                byte[] messageLth = Util.intTo2Byte(messageLthI);
                System.arraycopy(messageLth, 0, src, index, 2);
                index += 2;
                System.arraycopy(message, 0, src, index, messageLthI);
                index += messageLthI;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message - "+j+" : "+tempMessage);
                }
            }
            //Image name
            String tempImgName = prop.getProperty("img_name_"+errorId);
            byte[] imgName = tempImgName.getBytes();
            int imgNameLth = imgName.length;
            src[index++] = (byte)imgNameLth;
            System.arraycopy(imgName, 0, src, index, imgNameLth);
            index += imgNameLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Image name - "+i+" : "+tempImgName);
            }
            //Button list
            String tempButtonList = prop.getProperty("button_list_"+errorId);
            StringTokenizer stButton=new StringTokenizer(tempButtonList, ",");
            int stButtonCount = stButton.countTokens();
            src[index++] = (byte)stButtonCount;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Button list - "+i+" : "+tempButtonList);
            }
            for (int j=0; j<stButtonCount; j++) {
                //Button id
                String buttonId = stButton.nextToken();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Button id - "+i+" - "+ j+" : "+buttonId);
                }
                //Button name
                for (int k=0; k<langTypeSize; k++) {
                    //Language type
                    String tempLangType = prop.getProperty("button_language_type_" + buttonId + "_" + k);
                    byte[] langType = Util.stringToNByte(tempLangType, 2);
                    System.arraycopy(langType, 0, src, index, 2);
                    index += 2;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Lang type - "+i+" - "+ j+" : "+tempLangType);
                    }
                    //Button label
                    String tempButtonLabel = prop.getProperty("button_label_"+buttonId+"_"+k);
                    byte[] buttonLabel = tempButtonLabel.getBytes();
                    int buttonLabelLth = buttonLabel.length;
                    src[index++] = (byte)buttonLabelLth;
                    System.arraycopy(buttonLabel, 0, src, index, buttonLabelLth);
                    index += buttonLabelLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Button label - "+i+" - "+ j+" : "+tempButtonLabel);
                    }
                }
                //Button type
                String tempButtonType = prop.getProperty("button_type_"+buttonId);
                byte[] buttonType = Util.stringToNByte(tempButtonType, 1);
                src[index++] = buttonType[0];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Button type - "+i+" : "+tempButtonType);
                }
                if (tempButtonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_APPLICATION)) {
                    //App name
                    String tempTargetAppName = prop.getProperty("app_name_"+buttonId);
                    byte[] targetAppName = tempTargetAppName.getBytes();
                    int targetAppNameLth = targetAppName.length;
                    src[index++] = (byte)targetAppNameLth;
                    System.arraycopy(targetAppName, 0, src, index, targetAppNameLth);
                    index += targetAppNameLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target app name - "+i+" : "+tempTargetAppName);
                    }
                    //App parameters
                    String tempTargetAppParams = prop.getProperty("app_param_"+buttonId);
                    byte[] targetAppParams = tempTargetAppParams.getBytes();
                    int targetAppParamsLth = targetAppParams.length;
                    src[index++] = (byte)targetAppParamsLth;
                    System.arraycopy(targetAppParams, 0, src, index, targetAppParamsLth);
                    index += targetAppParamsLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target app param - "+i+" : "+tempTargetAppParams);
                    }
                } else if (tempButtonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CHANNEL)) {
                    //Call letter
                    String tempCallLetter = prop.getProperty("call_letter_"+buttonId);
                    byte[] callLetter = tempCallLetter.getBytes();
                    int callLetterLth = callLetter.length;
                    src[index++] = (byte)callLetterLth;
                    System.arraycopy(callLetter, 0, src, index, callLetterLth);
                    index += callLetterLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target caller id - "+i+" : "+tempCallLetter);
                    }
                }
            }
            //Error code list
            String tempErrorCodeList = prop.getProperty("error_code_"+errorId);
            StringTokenizer stErrCodeList=new StringTokenizer(tempErrorCodeList, ",");
            int stErrCodeListCount = stErrCodeList.countTokens();
            byte[] errCodeListCount = Util.intTo2Byte(stErrCodeListCount);
            System.arraycopy(errCodeListCount, 0, src, index, 2);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error code list - "+i+" : "+tempErrorCodeList);
            }
            for (int j=0; j<stErrCodeListCount; j++) {
                //Error code
                String tempErrorCode = stErrCodeList.nextToken();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error code - "+i+"-"+j+" : "+tempErrorCode);
                }
                byte[] errorCode = tempErrorCode.getBytes();
                int errorCodeLth = errorCode.length;
                src[index++] = (byte)errorCodeLth;
                System.arraycopy(errorCode, 0, src, index, errorCodeLth);
                index += errorCodeLth;
            }
        }
        return src;
    }

    public static byte[] getBytesEmergencyErrorMessage() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesEmergencyErrorMessage]start.");
        }
        File file = new File("resource/test_data/prop_data/emergency_error_message.prop");
        Properties prop = Util.loadProperties(file);

        byte[] src = new byte[4000];
        int index = 0;
        //Version
        String tempVersion = prop.getProperty("version");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Version : "+tempVersion);
        }
        byte version = 0;
        if (tempVersion != null) {
            version = Byte.parseByte(tempVersion);
        }
        src[index++] = version;
        //Language type size
        String tempLangTypeSize = prop.getProperty("language_type_size");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Language type size : "+tempLangTypeSize);
        }
        byte langTypeSize = -1;
        if (tempLangTypeSize != null) {
            try{
                langTypeSize = Byte.parseByte(tempLangTypeSize);
            }catch(Exception ignore) {
            }
        }
        src[index++] = langTypeSize;
        //Error list
        String tempErrorList = prop.getProperty("error_list");
        if (Log.DEBUG_ON) {
            Log.printDebug("[EMDataConverter.getBytesErrorCode]Error list : "+tempErrorList);
        }
        StringTokenizer st=new StringTokenizer(tempErrorList, ",");
        int stCount = st.countTokens();
        byte[] errorListSize = Util.intTo2Byte(stCount);
        System.arraycopy(errorListSize, 0, src, index, 2);
        index += 2;
        for (int i=0; i<stCount; i++) {
            //Error id
            String errorId = st.nextToken();
            byte[] errorIdBytes = Util.intTo2Byte(Integer.parseInt(errorId));
            System.arraycopy(errorIdBytes, 0, src, index, 2);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error id - "+i+" : "+errorId);
            }
            //Error message type
            String tempErrMsgType = prop.getProperty("error_message_type_"+errorId);
            src[index++] = (byte) Byte.parseByte(tempErrMsgType);
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message type - "+i+" : "+tempErrMsgType);
            }
            for (int j=0; j<langTypeSize; j++) {
                //Language type
                String tempLangType = prop.getProperty("language_type_" + errorId + "_" + j);
                byte[] langType = Util.stringToNByte(tempLangType, 2);
                System.arraycopy(langType, 0, src, index, 2);
                index += 2;
                //Title
                String tempTitle = prop.getProperty("title_"+errorId+"_"+j);
                byte[] title = tempTitle.getBytes();
                int titleLth = title.length;
                src[index++] = (byte)titleLth;
                System.arraycopy(title, 0, src, index, titleLth);
                index += titleLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message title - "+j+" : "+tempTitle);
                }
                //Message
                String tempMessage = prop.getProperty("message_"+errorId+"_"+j);
                byte[] message = tempMessage.getBytes();
                int messageLthI = message.length;
                byte[] messageLth = Util.intTo2Byte(messageLthI);
                System.arraycopy(messageLth, 0, src, index, 2);
                index += 2;
                System.arraycopy(message, 0, src, index, messageLthI);
                index += messageLthI;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error message - "+j+" : "+tempMessage);
                }
            }
            //Image name
            String tempImgName = prop.getProperty("img_name_"+errorId);
            byte[] imgName = tempImgName.getBytes();
            int imgNameLth = imgName.length;
            src[index++] = (byte)imgNameLth;
            System.arraycopy(imgName, 0, src, index, imgNameLth);
            index += imgNameLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Image name - "+i+" : "+tempImgName);
            }
            //Button list
            String tempButtonList = prop.getProperty("button_list_"+errorId);
            StringTokenizer stButton=new StringTokenizer(tempButtonList, ",");
            int stButtonCount = stButton.countTokens();
            src[index++] = (byte)stButtonCount;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Button list - "+i+" : "+tempButtonList);
            }
            for (int j=0; j<stButtonCount; j++) {
                //Button id
                String buttonId = stButton.nextToken();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Button id - "+i+" - "+ j+" : "+buttonId);
                }
                //Button name
                for (int k=0; k<langTypeSize; k++) {
                    //Language type
                    String tempLangType = prop.getProperty("button_language_type_" + buttonId + "_" + k);
                    byte[] langType = Util.stringToNByte(tempLangType, 2);
                    System.arraycopy(langType, 0, src, index, 2);
                    index += 2;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Lang type - "+i+" - "+ j+" : "+tempLangType);
                    }
                    //Button label
                    String tempButtonLabel = prop.getProperty("button_label_"+buttonId+"_"+k);
                    byte[] buttonLabel = tempButtonLabel.getBytes();
                    int buttonLabelLth = buttonLabel.length;
                    src[index++] = (byte)buttonLabelLth;
                    System.arraycopy(buttonLabel, 0, src, index, buttonLabelLth);
                    index += buttonLabelLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Button label - "+i+" - "+ j+" : "+tempButtonLabel);
                    }
                }
                //Button type
                String tempButtonType = prop.getProperty("button_type_"+buttonId);
                byte[] buttonType = Util.stringToNByte(tempButtonType, 1);
                src[index++] = buttonType[0];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Button type - "+i+" : "+tempButtonType);
                }
                if (tempButtonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_APPLICATION)) {
                    //App name
                    String tempTargetAppName = prop.getProperty("app_name_"+buttonId);
                    byte[] targetAppName = tempTargetAppName.getBytes();
                    int targetAppNameLth = targetAppName.length;
                    src[index++] = (byte)targetAppNameLth;
                    System.arraycopy(targetAppName, 0, src, index, targetAppNameLth);
                    index += targetAppNameLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target app name - "+i+" : "+tempTargetAppName);
                    }
                    //App parameters
                    String tempTargetAppParams = prop.getProperty("app_param_"+buttonId);
                    byte[] targetAppParams = tempTargetAppParams.getBytes();
                    int targetAppParamsLth = targetAppParams.length;
                    src[index++] = (byte)targetAppParamsLth;
                    System.arraycopy(targetAppParams, 0, src, index, targetAppParamsLth);
                    index += targetAppParamsLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target app param - "+i+" : "+tempTargetAppParams);
                    }
                } else if (tempButtonType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CHANNEL)) {
                    //Call letter
                    String tempCallLetter = prop.getProperty("call_letter_"+buttonId);
                    byte[] callLetter = tempCallLetter.getBytes();
                    int callLetterLth = callLetter.length;
                    src[index++] = (byte)callLetterLth;
                    System.arraycopy(callLetter, 0, src, index, callLetterLth);
                    index += callLetterLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[EMDataConverter.getBytesErrorCode]Target caller id - "+i+" : "+tempCallLetter);
                    }
                }
            }
            //Error code list
            String tempErrorCodeList = prop.getProperty("error_code_"+errorId);
            StringTokenizer stErrCodeList=new StringTokenizer(tempErrorCodeList, ",");
            int stErrCodeListCount = stErrCodeList.countTokens();
            byte[] errCodeListCount = Util.intTo2Byte(stErrCodeListCount);
            System.arraycopy(errCodeListCount, 0, src, index, 2);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[EMDataConverter.getBytesErrorCode]Error code list - "+i+" : "+tempErrorCodeList);
            }
            for (int j=0; j<stErrCodeListCount; j++) {
                //Error code
                String tempErrorCode = stErrCodeList.nextToken();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[EMDataConverter.getBytesErrorCode]Error code - "+i+"-"+j+" : "+tempErrorCode);
                }
                byte[] errorCode = tempErrorCode.getBytes();
                int errorCodeLth = errorCode.length;
                src[index++] = (byte)errorCodeLth;
                System.arraycopy(errorCode, 0, src, index, errorCodeLth);
                index += errorCodeLth;
            }
        }
        return src;
    }
}

package com.videotron.tvi.illico.errormessage.controller;

import com.videotron.tvi.illico.log.Log;

public class ErrorMessageVbmController extends VbmController {
    public static final long ERROR_MESSAGE_DISPLAY = 1006000001L;
    
    protected static ErrorMessageVbmController instance = new ErrorMessageVbmController();

    public static ErrorMessageVbmController getInstance() {
        return instance;
    }
    
    protected ErrorMessageVbmController() {
    }
    
    public void writeErrorMessageDisplay(String errCode) {
        Log.printInfo("[ErrorMessageVbmController.writeErrorMessageDisplay] start.");
        Log.printInfo("[ErrorMessageVbmController.writeErrorMessageDisplay] Param - Error code : " + errCode);
        write(ERROR_MESSAGE_DISPLAY, DEF_MEASUREMENT_GROUP, errCode);
        Log.printInfo("[ErrorMessageVbmController.writeErrorMessageDisplay] end.");
    }
}

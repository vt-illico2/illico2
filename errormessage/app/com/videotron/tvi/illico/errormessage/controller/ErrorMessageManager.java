package com.videotron.tvi.illico.errormessage.controller;

import java.awt.event.KeyEvent;
import java.util.Hashtable;

import org.dvb.event.UserEvent;
import org.dvb.io.ixc.IxcRegistry;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.errormessage.Rs;
import com.videotron.tvi.illico.errormessage.communication.ErrorMessageServiceImpl;
import com.videotron.tvi.illico.errormessage.ui.CommonMessageUI;
import com.videotron.tvi.illico.errormessage.ui.ErrorMessageUI;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.WindowProperty;

public class ErrorMessageManager implements LayeredKeyHandler {
	private static ErrorMessageManager instance;
	/*****************************************************************
	 * variables - Layered-related
	 *****************************************************************/
	public static LayeredUI layeredDialog;
	public static LayeredWindow lwErrorMessage;
	/*****************************************************************
	 * variables - UI-related
	 *****************************************************************/
	private ErrorMessageUI errorMessageUI;
	private boolean isActivated;
	/*****************************************************************
	 * variables - Monitor mode-related
	 *****************************************************************/
	private boolean isStbModeNormal;

	// 4K
	private LayeredUI cmLayeredDialog;
	private LayeredWindow cmLayeredWindow;
	private CommonMessageUI commonMessageUI;

	private ErrorMessageManager() {
	}

	public static synchronized ErrorMessageManager getInstance() {
		if (instance == null) {
			instance = new ErrorMessageManager();
		}
		return instance;
	}

	public void init() {
		ErrorMessageServiceImpl.getInstance().init();
		try {
			IxcRegistry.bind(FrameworkMain.getInstance().getXletContext(), ErrorMessageService.IXC_NAME,
					ErrorMessageServiceImpl.getInstance());
		} catch (Exception e) {
			e.printStackTrace();
		}
		errorMessageUI = new ErrorMessageUI();
		lwErrorMessage = new LayeredWindowErrorMessage();
		lwErrorMessage.add(errorMessageUI);
		lwErrorMessage.setVisible(true);
		errorMessageUI.setVisible(true);
		layeredDialog = WindowProperty.ERROR_MESSAGE.createLayeredDialog(lwErrorMessage, lwErrorMessage.getBounds(),
				this);
		layeredDialog.deactivate();

		commonMessageUI = new CommonMessageUI();
		cmLayeredWindow = new LayeredWindowCommonMessage();
		cmLayeredWindow.add(commonMessageUI);
		cmLayeredWindow.setVisible(true);
		commonMessageUI.setVisible(true);
		cmLayeredDialog = WindowProperty.COMMON_MESSAGE.createLayeredDialog(cmLayeredWindow,
				cmLayeredWindow.getBounds(), new LayeredKeyHandlerCommonMessage());
		cmLayeredDialog.deactivate();
	}

	public void dispose() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.dispose]Request hide error message by dispose.");
		}
		hideErrorMessage();
		if (lwErrorMessage != null) {
			lwErrorMessage.removeAll();
			lwErrorMessage = null;
		}
		errorMessageUI = null;
		if (layeredDialog != null) {
			layeredDialog.deactivate();
			layeredDialog = null;
		}
		try {
			IxcRegistry.unbind(FrameworkMain.getInstance().getXletContext(), ErrorMessageService.IXC_NAME);
		} catch (Exception e) {
			e.printStackTrace();
		}
		ErrorMessageServiceImpl.getInstance().dispose();
		isActivated = false;
	}

	synchronized public void showErrorMessage(String errCode, ErrorMessageListener errMsgListener, Hashtable errMsgTags) {
		if (Log.INFO_ON) {
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - Error Code : " + errCode);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - ErrorMessageListener : " + errMsgListener);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - ErrorMessageTags : " + errMsgTags);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]isActivated : " + isActivated);
		}
		if (isActivated) {
			executeActivatedAction(errCode, errMsgListener);
		}
		// Do not activate when stb mode is invalid.
		if (!isStbModeNormal) {
			if (Log.INFO_ON) {
				Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Current STB mode is not normal.");
			}
			if (errCode == null || !(errCode.toUpperCase()).startsWith("MOT")) {
				if (Log.INFO_ON) {
					Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Do not allow this request.[" + errCode + "]");
				}
				return;
			}
			if (Log.INFO_ON) {
				Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Allow this request. (Monitor requested)[" + errCode + "]");
			}
		}
		isActivated = true;
		if (errorMessageUI != null) {
			errorMessageUI.start(errCode, errMsgListener, errMsgTags);
		}
		if (layeredDialog != null) {
			layeredDialog.activate();
		}
		ErrorMessageVbmController.getInstance().writeErrorMessageDisplay(errCode);
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.showErrorMessage]end.");
		}
	}

	synchronized public void showErrorMessage(String errCode, ErrorMessage msg, ErrorMessageListener errMsgLstnr) {
		if (Log.INFO_ON) {
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - Error Code : " + errCode);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - ErrorMessage : " + msg);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]Param - ErrorMessageListener : " + errMsgLstnr);
			Log.printInfo("[ErrMsgMgr.showErrorMessage]isActivated : " + isActivated);
		}
		if (isActivated) {
			executeActivatedAction(errCode, errMsgLstnr);
		}
		if (!isStbModeNormal) {
			if (Log.INFO_ON) {
				Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Current STB mode is not normal.");
			}
			if (errCode == null || !(errCode.toUpperCase()).startsWith("MOT")) {
				if (Log.INFO_ON) {
					Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Do not allow this request.[" + errCode + "]");
				}
				return;
			}
			if (Log.INFO_ON) {
				Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Allow this request. (Monitor requested)[" + errCode + "]");
			}
		}
		isActivated = true;
		if (errorMessageUI != null) {
			if (msg != null) {
				errorMessageUI.start(errCode, msg, errMsgLstnr);
			} else {
				errorMessageUI.start(errCode, errMsgLstnr, null);
			}
		}
		if (layeredDialog != null) {
			layeredDialog.activate();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.showErrorMessage]end.");
		}
	}

	private void executeActivatedAction(String errCode, ErrorMessageListener errMsgLstnr) {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.executeActivatedAction]Error message is activated.");
		}
		boolean isEmergencyErrMesgOn = DataManager.getInstance().isEmergencyModeOn();
		if (!isEmergencyErrMesgOn) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ErrMsgMgr.executeActivatedAction]Emergency error message is off. return. [" + errCode
						+ "]");
			}
			return;
		}
		if (errCode == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ErrMsgMgr.executeActivatedAction]Emergency error message is null.");
			}
			return;
		}
		String emergencyErrCode = DataManager.getInstance().getEmergencyErrorCode();
		if (emergencyErrCode == null || !emergencyErrCode.equals(errCode)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ErrMsgMgr.executeActivatedAction]Emergency error message is not same. return. ["
						+ errCode + "]");
			}
			return;
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.executeActivatedAction]Request hide error message by Emergency error code.");
		}
		hideErrorMessage();
	}

	synchronized public void hideErrorMessage() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.hideErrorMessage]start - Activate Dashboard : " + isActivated);
		}
		if (!isActivated) {
			if (Log.DEBUG_ON) {
				Log.printDebug("[ErrMsgMgr.hideErrorMessage]Error message did not activate.");
			}
			return;
		}
		if (errorMessageUI != null) {
			errorMessageUI.stop();
		}
		if (layeredDialog != null) {
			layeredDialog.deactivate();
		}
		isActivated = false;
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgMgr.hideErrorMessage]end.");
		}
	}

	/*****************************************************************
	 * methods - STB mode-related
	 *****************************************************************/
	public void setSTBModeNormal(boolean isSTBModeNormal) {
		if (Log.INFO_ON) {
			Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Called.");
			Log.printInfo("[ErrMsgMgr.setSTBModeNormal]Param - STB mode normal : " + isSTBModeNormal);
		}
		this.isStbModeNormal = isSTBModeNormal;
	}

	/*****************************************************************
	 * methods - LayeredKeyHandler-implemented
	 *****************************************************************/
	public boolean handleKeyEvent(UserEvent userEvent) {
		if (userEvent == null) {
			return false;
		}
		if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
		int keyCode = userEvent.getCode();
		if (errorMessageUI != null) {
			errorMessageUI.handleKey(keyCode);
		}
		// Every keys are blocked.
		return true;
	}

	/*****************************************************************
	 * methods - LayeredWindow-related
	 *****************************************************************/
	class LayeredWindowErrorMessage extends LayeredWindow {
		private static final long serialVersionUID = 1L;

		public LayeredWindowErrorMessage() {
			setBounds(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
		}

		public void notifyShadowed() {
		}
	}

	/**
	 * for 4K
	 * @author zestyman
	 *
	 */
	class LayeredKeyHandlerCommonMessage implements LayeredKeyHandler {

		public boolean handleKeyEvent(UserEvent userEvent) {
			if (userEvent == null) {
	            return false;
	        }
	        if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
	            return false;
	        }
	        int keyCode = userEvent.getCode();
	        if (commonMessageUI != null) {
	        	commonMessageUI.handleKey(keyCode);
	        }
			return true;
		}
	}

	/**
	 * for 4k
	 * @author zestyman
	 *
	 */
	class LayeredWindowCommonMessage extends LayeredWindow {
		public LayeredWindowCommonMessage() {
			setBounds(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
		}

		public void notifyShadowed() {
		}
	}
	
	public void showCommonMessage(String commonCode, ErrorMessageListener l) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ErrorMessageManager, showCommonMessage(), commonCode=" + commonCode + ", l=" + l);
		}
		
		
		CommonMessage commonMessage = DataManager.getInstance().getCommonMessage(commonCode);
		
		if (commonMessage != null) {
			if (commonMessageUI != null) {
				commonMessageUI.start(commonCode);
			}
			
			if (cmLayeredDialog != null) {
				cmLayeredDialog.activate();
			}
			
			ErrorMessageVbmController.getInstance().writeErrorMessageDisplay(commonCode);
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("ErrorMessageManager, showCommonMessage(), commonMessage=" + commonMessage);
			}
		}
	}
	
	public void hideCommonMessage() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ErrorMessageManager, hideCommonMessage");
		}
		
		// VDTRMASTER-5411
		if (commonMessageUI != null) {
			commonMessageUI.stop();
		}
		
		if (cmLayeredDialog != null) {
			cmLayeredDialog.deactivate();
		}
	}
}

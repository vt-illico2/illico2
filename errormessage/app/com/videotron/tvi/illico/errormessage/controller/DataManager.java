package com.videotron.tvi.illico.errormessage.controller;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.videotron.tvi.illico.errormessage.Rs;
import com.videotron.tvi.illico.errormessage.Util;
import com.videotron.tvi.illico.errormessage.communication.ErrorMessageServiceImpl;
import com.videotron.tvi.illico.errormessage.data.CommonMessageImpl;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageButton;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

public class DataManager implements DataUpdateListener {
    private static DataManager instance;
    /*****************************************************************
     * Framework-related
     *****************************************************************/
    private DataCenter dCenter = DataCenter.getInstance();
    /*****************************************************************
     * File Name-related
     *****************************************************************/
    public static final String IB_GENERAL_CONFIG = "IB_GENERAL_CONFIG";
    public static final String IB_ERROR_CODES = "IB_ERROR_CODES";
    public static final String IB_ERROR_ICONS = "IB_ERROR_ICONS";
    public static final String OOB_EMERGENCY_ERROR_MESSAGE = "OOB_EMERGENCY_ERROR_MESSAGE";

    public static final String PROP_DEFAULT_ERROR_MESSAGE = "DEFAULT_ERROR_MESSAGE";

    private static final Integer DEFAULT_ERROR_MESSAGE_ID = new Integer(0);

    private Hashtable errMsgHash;
    private Hashtable errCodeHash;
    private Hashtable errMsgIconHash;

    private int latestVerGeneralConfig;
    private int latestVerErrCodes;
    private int displayTime;

    private ErrorMessage localDefaultErrMsg;

    /*****************************************************************
     * Emergency error message-related
     *****************************************************************/
    private int latestVerEmerErrMsg;
    private boolean isEmergencyModeOn;
    private String emergencyErrCode;
    private ErrorMessage emergencyErrMsgData;
    
    // add for 4k
    private final String INCOM = "INCOM";
    private Hashtable commonHash;

    private DataManager(){
    }
    public static synchronized DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }
    public void init() {
        if (Log.INFO_ON) {
            Log.printInfo("[DataMgr.init]Called.");
        }
        latestVerGeneralConfig = -1;
        latestVerErrCodes = -1;
        latestVerEmerErrMsg = -1;
        isEmergencyModeOn = false;
        errMsgHash = new Hashtable();
        errCodeHash = new Hashtable();
        errMsgIconHash = new Hashtable();
        commonHash = new Hashtable();
        createLocalDefaultErrorMessage();
        if (!Rs.IS_EMULATOR) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]use real data.(Inband, OOB)");
            }
            //IB_GENERAL_CONFIG
            File fileLoadedGeneralConfig = (File)DataCenter.getInstance().get(IB_GENERAL_CONFIG);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded general config file : "+fileLoadedGeneralConfig);
            }
            if (fileLoadedGeneralConfig != null) {
                dataUpdated(IB_GENERAL_CONFIG, null, fileLoadedGeneralConfig);
            }
            dCenter.addDataUpdateListener(IB_GENERAL_CONFIG, this);
            //IB_ERROR_CODES
            File fileLoadedErrCodes = (File)DataCenter.getInstance().get(IB_ERROR_CODES);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded error codes file : "+fileLoadedErrCodes);
            }
            if (fileLoadedErrCodes != null) {
                dataUpdated(IB_ERROR_CODES, null, fileLoadedErrCodes);
            }
            dCenter.addDataUpdateListener(IB_ERROR_CODES, this);
            //IB_ERROR_ICONS
            File fileLoadedErrIcons = (File)DataCenter.getInstance().get(IB_ERROR_ICONS);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded error icons file : "+fileLoadedErrIcons);
            }
            if (fileLoadedErrCodes != null) {
                dataUpdated(IB_ERROR_ICONS, null, fileLoadedErrIcons);
            }
            dCenter.addDataUpdateListener(IB_ERROR_ICONS, this);
            //OOB_EMERGENCY_ERROR_MESSAGE
            File fileLoadedEmergencyErrMsg = (File)DataCenter.getInstance().get(OOB_EMERGENCY_ERROR_MESSAGE);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Loaded emergency error message file : "+fileLoadedEmergencyErrMsg);
            }
            if (fileLoadedEmergencyErrMsg != null) {
                dataUpdated(OOB_EMERGENCY_ERROR_MESSAGE, null, fileLoadedEmergencyErrMsg);
            }
            dCenter.addDataUpdateListener(OOB_EMERGENCY_ERROR_MESSAGE, this);
        }else {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]Emulator - Properties.");
            }
            if (Rs.IS_MS_DATA) {
                File fileGeneralConfigIB = new File("resource/test_data/ms_data/general_config.txt");
                byte[] bytesGeneralConfigIB = Util.getByteArrayFromFile(fileGeneralConfigIB);
                parseGeneralConfig(bytesGeneralConfigIB);

                File fileErrorCodesIB = new File("resource/test_data/ms_data/error_codes.txt");
                byte[] bytesErrorCodesIB = Util.getByteArrayFromFile(fileErrorCodesIB);
                parseErrorCodes(bytesErrorCodesIB);

                File fileErrorIcon = new File("resource/test_data/ms_data/error_images.zip");
                resetImageSourcesFromZipFile(fileErrorIcon);

                File fileEmergencyErrMsg = new File("resource/test_data/ms_data/emergency_error_message.txt");
                byte[] bytesEmergencyErrMsg = Util.getByteArrayFromFile(fileEmergencyErrMsg);
                parseEmergencyErrorMessage(bytesEmergencyErrMsg);
            } else {
                byte[] bytesGeneralConfigIB = DataConverter.getBytesGeneralConfig();
                parseGeneralConfig(bytesGeneralConfigIB);
                byte[] bytesErrorCodesIB = DataConverter.getBytesErrorCode();
                parseErrorCodes(bytesErrorCodesIB);
                File fileErrorIcon = (File) dCenter.get(IB_ERROR_ICONS);
                resetImageSourcesFromZipFile(fileErrorIcon);
            }
            
            CommonMessageImpl cmImpl = new CommonMessageImpl(2);
            cmImpl.setActionButtonTexts(0, "OK0");
            cmImpl.setActionButtonTexts(1, "OK1");
            cmImpl.setFunctionalTexts(0, "0 test test test test");
            //cmImpl.setFunctionalTexts(1, "1 test test test test test test test test test test test test test test test test test test test test 1 test test test test test test test test test test test\n \n test test test");
            cmImpl.setFunctionalTexts(1, "1 test test test test");
            cmImpl.setShortFunctionalTexts(0, "0 short message test");
            cmImpl.setShortFunctionalTexts(1, "1 short message test");
            cmImpl.setTitle(0, "0 title");
            cmImpl.setTitle(1, "1 title");
            
            commonHash.put(INCOM, cmImpl);
        }
    }
    public void dispose() {
        dCenter.removeDataUpdateListener(IB_ERROR_ICONS, this);
        dCenter.removeDataUpdateListener(IB_ERROR_CODES, this);
        dCenter.removeDataUpdateListener(IB_GENERAL_CONFIG, this);
        if (errMsgHash != null) {
            errMsgHash.clear();
            errMsgHash = null;
        }
        if (errCodeHash != null) {
            errCodeHash.clear();
            errCodeHash = null;
        }
        instance = null;
    }

    /*****************************************************************
     * Methods - DataUpdateListener-implemented
     *****************************************************************/
    public void dataRemoved(String key) {
    }
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataUpdated]key : " + key);
            Log.printDebug("[DataMgr.dataUpdated]old : " + old);
            Log.printDebug("[DataMgr.dataUpdated]value : " + value);
        }
        if (key == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated key is null. return.");
            }
            return;
        }
        if (value == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated value is null. return.");
            }
            return;
        }
        if (key.equals(IB_GENERAL_CONFIG)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated general config.");
            }
            File fileGeneralConfig = (File) value;
            if (fileGeneralConfig != null) {
                byte[] bytesGeneralConfig = Util.getByteArrayFromFile(fileGeneralConfig);
                parseGeneralConfig(bytesGeneralConfig);
            }
        } else if (key.equals(IB_ERROR_CODES)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated error codes.");
            }
            File fileErrorCode = (File) value;
            if (fileErrorCode != null) {
                byte[] bytesErrorCode = Util.getByteArrayFromFile(fileErrorCode);
                parseErrorCodes(bytesErrorCode);
            }
        } else if (key.equals(IB_ERROR_ICONS)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated error Icons.");
            }
            File fileErrorIcon = (File) value;
            resetImageSourcesFromZipFile(fileErrorIcon);
        } else if (key.equals(OOB_EMERGENCY_ERROR_MESSAGE)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated]Updated emergency error message.");
            }
            File fileEmergencyErrMsg = (File) value;
            if (fileEmergencyErrMsg != null) {
                byte[] bytesEmergencyErrMsg = Util.getByteArrayFromFile(fileEmergencyErrMsg);
                parseEmergencyErrorMessage(bytesEmergencyErrMsg);
            }
        }
    }
    private void resetErrorMessageHashtable() {
        if (errMsgHash != null) {
            errMsgHash.clear();
        }
        if (errCodeHash != null) {
            errCodeHash.clear();
        }
    }
    private void createLocalDefaultErrorMessage() {
        try {
            ErrorMessage tempErrMsg = new ErrorMessage();
            String errMsgTypeStr = dCenter.getString("DEFAULT_ERROR_MESSAGE_TYPE");
            if (Log.DEBUG_ON) {
                Log.printDebug("[ErrorMessageDataManager.setDefaultErrorMessage]errMsgTypeStr : " + errMsgTypeStr);
            }
            int errMsgType = Integer.parseInt(errMsgTypeStr);
            if (errMsgType != -1) {
                tempErrMsg.setErrorMessageType(errMsgType);
            }
            String errTitleEn = dCenter.getString("DEFAULT_ERROR_MESSAGE_TITLE_EN");
            tempErrMsg.setEnglishTitle(errTitleEn);
            String errTitleFr = dCenter.getString("DEFAULT_ERROR_MESSAGE_TITLE_FR");
            tempErrMsg.setFrenchTitle(errTitleFr);
            String errMsgEn = dCenter.getString("DEFAULT_ERROR_MESSAGE_CONTENT_EN");
            tempErrMsg.setEnglishContent(errMsgEn);
            String errMsgFr = dCenter.getString("DEFAULT_ERROR_MESSAGE_CONTENT_FR");
            tempErrMsg.setFrenchContent(errMsgFr);
            String iconName = dCenter.getString("DEFAULT_ERROR_MESSAGE_ICON_NAME");
            tempErrMsg.setIconName(iconName);

            ErrorMessageButton errMsgButton = new ErrorMessageButton();
            errMsgButton.setButtonType(ErrorMessageButton.BUTTON_TYPE_CLOSE);
            errMsgButton.setEnglishButtonName(dCenter.getString("DEFAULT_ERROR_MESSAGE_BUTTON_LABEL_EN"));
            errMsgButton.setFrenchButtonName(dCenter.getString("DEFAULT_ERROR_MESSAGE_BUTTON_LABEL_FR"));
            tempErrMsg.setErrorMessageButtonList(new ErrorMessageButton[]{errMsgButton});
            localDefaultErrMsg = tempErrMsg;
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    /*****************************************************************
     * Methods - ErrorMessageData-related
     *****************************************************************/
    public ErrorMessage getErrorMessage(String reqErrorCode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageDataManager.getErrorMessage]reqErrorCode : " + reqErrorCode);
        }
        if (reqErrorCode == null) {
            ErrorMessage defaultErrMsg = (ErrorMessage)errMsgHash.get(DEFAULT_ERROR_MESSAGE_ID);
            if (defaultErrMsg == null) {
                return localDefaultErrMsg;
            }
            return defaultErrMsg;
        }
        if (isEmergencyModeOn && emergencyErrCode != null && reqErrorCode.equals(emergencyErrCode)) {
            return emergencyErrMsgData;
        }
        Integer errMsgId = (Integer)errCodeHash.get(reqErrorCode);
        if (errMsgId != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[ErrorMessageDataManager.getErrorMessage]errMsgId : " + errMsgId.intValue());
            }
        }
        if (errMsgId == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[ErrorMessageDataManager.getErrorMessage]errMsgId is null.");
            }
            errMsgId = DEFAULT_ERROR_MESSAGE_ID;
        }
        ErrorMessage res = (ErrorMessage)errMsgHash.get(errMsgId);
        if (res == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[ErrorMessageDataManager.getErrorMessage]ErrorMessageData is null.");
            }
            res = (ErrorMessage)errMsgHash.get(DEFAULT_ERROR_MESSAGE_ID);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[ErrorMessageDataManager.getErrorMessage]ErrorMessageData : "+res);
        }
        if (res == null) {
            return localDefaultErrMsg;
        }
        return res;
    }
    public int getDisplayTimeSec() {
        return displayTime;
    }
    public byte[] getErrorMessageIconImageSource(String reqIconName) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.getErrorMessageIconImageSource]Param - Request icon name : " + reqIconName);
        }
        if (reqIconName == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getErrorMessageIconImageSource]["+reqIconName+"]Request icon name is null. return null.");
            }
            return null;
        }
        if (errMsgIconHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.getErrorMessageIconImageSource]["+reqIconName+"]Icon hash is null. return null.");
            }
            return null;
        }
        return (byte[])errMsgIconHash.get(reqIconName);
    }
    /*****************************************************************
     * Methods - Parse-related
     *****************************************************************/
    private boolean parseGeneralConfig(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.parseGeneralConfig]start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (version == latestVerGeneralConfig) {
                return true;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseGeneralConfig]Version : " + version);
            }
            //Display time
            displayTime = Util.twoBytesToInt(src, index, true);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseGeneralConfig]Display time : " + displayTime);
            }
            latestVerGeneralConfig = version;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        return true;
    }
    private boolean parseErrorCodes(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.parseErrorCodes]start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseErrorCodes]Version : " + version);
            }
            if (version == latestVerErrCodes) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes]Version is not changed. end");
                }
                return true;
            }
            resetErrorMessageHashtable();
            //Language type size
            int langTypeSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseErrorCodes]Language type size : " + langTypeSize);
            }
            //Error message list size
            int errMsgListSz = Util.twoBytesToInt(src, index, true);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseErrorCodes]Error message list size : " + errMsgListSz);
            }
            for (int i=0; i<errMsgListSz; i++) {
                ErrorMessage errMsg = new ErrorMessage();
                //Error message id
                int errMsgId = Util.twoBytesToInt(src, index, true);
                Integer errMsgIdInt = new Integer(errMsgId);
                index += 2;
                errMsg.setErrorMessageId(errMsgId);
                //Error message type
                int errMsgType = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes]Error message type-" + i + " : " + errMsgType);
                }
                errMsg.setErrorMessageType(errMsgType);
                for (int j=0; j<langTypeSize; j++) {
                    //Language type
                    String langType = new String(src, index, 2);
                    if (langType != null) {
                        langType = langType.trim();
                    }
                    index += 2;
                    //Error title
                    int errTitleLth = src[index++];
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]Error title Lth-" + i + " : " + errTitleLth);
                    }
                    String errTitle = new String(src, index, errTitleLth, "ISO8859_1");
                    index += errTitleLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]Error title-" + i + " : " + errTitle);
                    }
                    //Error content
                    int errContLth = Util.twoBytesToInt(src, index, true);
                    index += 2;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]Error content Lth-" + i + " : " + errContLth);
                    }
                    String errCont = new String(src, index, errContLth, "ISO8859_1");
                    index += errContLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]Error content-" + i + " : " + errCont);
                    }
                    if (langType.equals("en")) {
                        errMsg.setEnglishTitle(errTitle);
                        errMsg.setEnglishContent(errCont);
                    } else if (langType.equals("fr")) {
                        errMsg.setFrenchTitle(errTitle);
                        errMsg.setFrenchContent(errCont);
                    }
                }
                //Image name
                int imgNameLth = src[index++];
                String imgName = new String(src, index, imgNameLth, "ISO8859_1");
                index += imgNameLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes]Image name-" + i + " : " + imgName);
                }
                errMsg.setIconName(imgName);
                //Button list size
                int buttonListSz = src[index++];
                ErrorMessageButton[] errMsgButtons = new ErrorMessageButton[buttonListSz];
                for (int j=0; j<buttonListSz; j++) {
                    errMsgButtons[j] = new ErrorMessageButton();
                    //Button name
                    for (int k=0; k<langTypeSize; k++) {
                        //Language type
                        String langType = new String(src, index, 2);
                        if (langType != null) {
                            langType = langType.trim();
                        }
                        index += 2;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Lang type-" + k + " : " + langType);
                        }
                        //Button name
                        int btNameLth = src[index++];
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Button name Lth-" + k + " : " + btNameLth);
                        }
                        String btName = new String(src, index, btNameLth, "ISO8859_1");
                        index += btNameLth;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Button name-" + k + " : " + btName);
                        }
                        if (langType.equals("en")) {
                            errMsgButtons[j].setEnglishButtonName(btName);
                        } else if (langType.equals("fr")) {
                            errMsgButtons[j].setFrenchButtonName(btName);
                        }

                    }
                    //Button type
                    String btType = new String(src, index++, 1);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]Button type-" + i + " : " + btType);
                    }
                    errMsgButtons[j].setButtonType(btType);
                    if (btType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_APPLICATION)) {
                        //Target app name
                        int targetAppNameLth = src[index++];
                        String targetAppName = new String(src, index, targetAppNameLth, "ISO8859_1");
                        index += targetAppNameLth;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Target app name-" + i + " : " + targetAppName);
                        }
                        errMsgButtons[j].setTargetApplicationName(targetAppName);
                        //Target app params
                        int targetAppParamLth = src[index++];
                        String targetAppParams = new String(src, index, targetAppParamLth, "ISO8859_1");
                        if (targetAppParams != null) {
                            targetAppParams = targetAppParams.trim();
                        }
                        index += targetAppParamLth;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Target app param-" + i + " : " + targetAppParams);
                        }
                        StringTokenizer st = new StringTokenizer(targetAppParams, "|");
                        int stCount = st.countTokens();
                        if (stCount > 0) {
                            String[] appParams = new String[stCount];
                            for (int k=0; k<stCount; k++) {
                                appParams[k] = st.nextToken();
                            }
                            errMsgButtons[j].setTargetApplicationParameterList(appParams);
                        }
                    } else if (btType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CHANNEL)) {
                        //Target call letter
                        int targetCallLetterLth = src[index++];
                        String targetCallLetter = new String(src, index, targetCallLetterLth, "ISO8859_1");
                        index += targetCallLetterLth;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DataMgr.parseErrorCodes]Target call letter-" + i + " : " + targetCallLetter);
                        }
                        errMsgButtons[j].setTargetCallLetter(targetCallLetter);
                    }
                }
                errMsg.setErrorMessageButtonList(errMsgButtons);
                //Error code list size
                int errCodeListSz = Util.twoBytesToInt(src, index, true);
                index += 2;
                for (int j=0; j<errCodeListSz; j++) {
                    //Error code
                    int errCodeLth = src[index++];
                    String errCode = new String(src, index, errCodeLth, "ISO8859_1");
                    index += errCodeLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseErrorCodes]ErrCode-ErrID : " + errCode + "-" + errMsgId);
                    }
                    errCodeHash.put(errCode, errMsgIdInt);
                }
                errMsgHash.put(errMsgIdInt, errMsg);
            }
            
            // add for 4K
            CommonMessageImpl cm = new CommonMessageImpl(langTypeSize);
            for (int i = 0; i < langTypeSize; i++) {
            	// language type
            	String languageType = new String(src, index, 2);
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] languageType=" + languageType);
                }
            	index += 2;
            	int langIdx = languageType.equals(Constants.LANGUAGE_FRENCH) ? 0 : 1;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] langIdx=" + langIdx);
                }
            	// title length
            	int titleLength = src[index++];
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] titleLength=" + titleLength);
                }
            	// title
            	String title = new String(src, index, titleLength);
            	index += titleLength;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] title=" + title);
                }
            	// Functional Text length
            	int ftLength = Util.twoBytesToInt(src, index, true);;
            	index += 2;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] ftLength=" + ftLength);
                }
            	// Funtional Text
            	String functinalText = new String(src, index, ftLength);
            	index += ftLength;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] functinalText=" + functinalText);
                }
            	
            	// Short text length
            	int stLength = Util.twoBytesToInt(src, index, true);;
            	index += 2;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] stLength=" + stLength);
                }
            	// Short text
            	String shortText = new String(src, index, stLength);
            	index += stLength;
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] shortText=" + shortText);
                }
            	
            	// button text length
            	int btLength = src[index++];
            	if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseErrorCodes] btLength=" + btLength);
                }
            	// button text
            	String buttonText = new String(src, index, btLength);
            	index += btLength;
            	if (Log.DEBUG_ON) {
            	    Log.printDebug("[DataMgr.parseErrorCodes] buttonText=" + buttonText);
                }
            	
            	cm.setTitle(langIdx, title);
            	cm.setFunctionalTexts(langIdx, functinalText);
            	cm.setShortFunctionalTexts(langIdx, shortText);
            	cm.setActionButtonTexts(langIdx, buttonText);
            }
            
            // banner image name;
        	int bannerNameLength = src[index++];
        	String bannerName = new String(src, index, bannerNameLength);
        	cm.setBanner(bannerName);
        	if (Log.DEBUG_ON) {
        	    Log.printDebug("[DataMgr.parseErrorCodes] bannerName=" + bannerName);
            }
            
            commonHash.put(INCOM, cm);
            latestVerErrCodes = version;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        return true;
    }
    private boolean parseEmergencyErrorMessage(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.parseEmergencyErrorMessage]start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Version : " + version);
            }
            if (version == latestVerEmerErrMsg) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Version is not changed. end");
                }
                return true;
            }
            ErrorMessage tempEmerErrMsg = new ErrorMessage();
            //Language type size
            int langTypeSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Language type size : " + langTypeSize);
            }
            //Emergency mode
            int tempEmergencyMode = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Emergency mode : " + tempEmergencyMode);
            }
            //Error code
            int errCodeLth = src[index++];
            String errCode = new String(src, index, errCodeLth, "ISO8859_1");
            index += errCodeLth;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Error code : " + errCode);
            }
            for (int i=0; i<langTypeSize; i++) {
                //Language type
                String langType = new String(src, index, 2);
                if (langType != null) {
                    langType = langType.trim();
                }
                index += 2;
                //Error title
                int errTitleLth = src[index++];
                String errTitle = new String(src, index, errTitleLth, "ISO8859_1");
                index += errTitleLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Error title - " + langType + " : " + errTitle);
                }
                //Error content
                int errMsgLth = Util.twoBytesToInt(src, index, true);
                index += 2;
                String errCont = new String(src, index, errMsgLth, "ISO8859_1");
                index += errMsgLth;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Error message-" + langType + " : " + errCont);
                }
                if (langType.equals("en")) {
                    tempEmerErrMsg.setEnglishTitle(errTitle);
                    tempEmerErrMsg.setEnglishContent(errCont);
                } else if (langType.equals("fr")) {
                    tempEmerErrMsg.setFrenchTitle(errTitle);
                    tempEmerErrMsg.setFrenchContent(errCont);
                }
            }
            //Button list size
            int buttonListSz = src[index++];
            ErrorMessageButton[] errMsgButtons = new ErrorMessageButton[buttonListSz];
            for (int i=0; i<buttonListSz; i++) {
                errMsgButtons[i] = new ErrorMessageButton();
                //Button name
                for (int k=0; k<langTypeSize; k++) {
                    //Language type
                    String langType = new String(src, index, 2);
                    if (langType != null) {
                        langType = langType.trim();
                    }
                    index += 2;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Lang type-" + k + " : " + langType);
                    }
                    //Button name
                    int btNameLth = src[index++];
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Button name Lth-" + k + " : " + btNameLth);
                    }
                    String btName = new String(src, index, btNameLth, "ISO8859_1");
                    index += btNameLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Button name-" + k + " : " + btName);
                    }
                    if (langType.equals("en")) {
                        errMsgButtons[i].setEnglishButtonName(btName);
                    } else if (langType.equals("fr")) {
                        errMsgButtons[i].setFrenchButtonName(btName);
                    }
                }
                //Button type
                String btType = new String(src, index++, 1);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Button type-" + i + " : " + btType);
                }
                errMsgButtons[i].setButtonType(btType);
                if (btType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_APPLICATION)) {
                    //Target app name
                    int targetAppNameLth = src[index++];
                    String targetAppName = new String(src, index, targetAppNameLth, "ISO8859_1");
                    index += targetAppNameLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Target app name-" + i + " : " + targetAppName);
                    }
                    errMsgButtons[i].setTargetApplicationName(targetAppName);
                    //Target app params
                    int targetAppParamLth = src[index++];
                    String targetAppParams = new String(src, index, targetAppParamLth, "ISO8859_1");
                    if (targetAppParams != null) {
                        targetAppParams = targetAppParams.trim();
                    }
                    index += targetAppParamLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Target app param-" + i + " : " + targetAppParams);
                    }
                    String[] appParams = TextUtil.tokenize(targetAppParams, '|');
                    errMsgButtons[i].setTargetApplicationParameterList(appParams);
                } else if (btType.equalsIgnoreCase(ErrorMessageButton.BUTTON_TYPE_CHANNEL)) {
                    //Target call letter
                    int targetCallLetterLth = src[index++];
                    String targetCallLetter = new String(src, index, targetCallLetterLth, "ISO8859_1");
                    index += targetCallLetterLth;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DataMgr.parseEmergencyErrorMessage]Target call letter-" + i + " : " + targetCallLetter);
                    }
                    errMsgButtons[i].setTargetCallLetter(targetCallLetter);
                }
            }
            tempEmerErrMsg.setErrorMessageButtonList(errMsgButtons);
            emergencyErrMsgData = tempEmerErrMsg;
            emergencyErrCode = errCode;
            if (tempEmergencyMode == 1) {
                isEmergencyModeOn = true;
            } else {
                isEmergencyModeOn = false;
            }
            latestVerEmerErrMsg = version;
        } catch (Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            try {
                ErrorMessageServiceImpl.getInstance().showErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
            return false;
        }
        return true;
    }
    /*****************************************************************
     * Methods - ZipFile-related
     *****************************************************************/
    private void resetImageSourcesFromZipFile(File reqFile) {
        if (reqFile == null) {
            return;
        }
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(reqFile);
            Enumeration enu = zipFile.entries();
            if (errMsgIconHash != null) {
                errMsgIconHash.clear();
            }
            while(enu.hasMoreElements()) {
                ZipEntry zipEntry = (ZipEntry)enu.nextElement();
                if (zipEntry == null) {
                    continue;
                }
                String zipEntryName = zipEntry.getName();
                if (zipEntryName == null) {
                    continue;
                }
                byte[] zipEntrySrc = getBytesFromZip(zipFile, zipEntry);
                if (zipEntrySrc == null) {
                    continue;
                }
                errMsgIconHash.put(zipEntryName, zipEntrySrc);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (zipFile != null) {
                try {
                    zipFile.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                zipFile = null;
            }
        }
    }
    private byte[] getBytesFromZip(ZipFile zipFile, ZipEntry zipEntry) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Util.getBytesFromZip]zipFile : " + zipFile);
            Log.printDebug("[Util.getBytesFromZip]zipEntry : " + zipEntry);
        }
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }
        if (zipEntry == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(zipFile.getInputStream(zipEntry));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
    public boolean isEmergencyModeOn() {
        return isEmergencyModeOn;
    }
    public String getEmergencyErrorCode() {
        return emergencyErrCode;
    }
    
    public CommonMessage getCommonMessage(String commonCode) {
    	// currently ONLY INCOMPATIBILITY popup.
    	Object object = commonHash.get(INCOM);
    	
    	if (object != null) {
    		return (CommonMessage) object;
    	} else {
    		return null;
    	}
    }
}

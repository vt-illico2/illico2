package com.videotron.tvi.illico.errormessage.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.log.Log;
public class LogLevelChangeListenerImpl implements LogLevelChangeListener {
    public void logLevelChanged(int logLevel) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("[LogLevelChangeListenerImpl.logLevelChanged]Called.");
            Log.printInfo("[LogLevelChangeListenerImpl.logLevelChanged]Param - Log level : "+logLevel);
        }
        Log.setStcLevel(logLevel);
    }
}

package com.videotron.tvi.illico.errormessage.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.errormessage.controller.ErrorMessageManager;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.log.Log;

public class STBModeChangeListenerImpl implements STBModeChangeListener {
    public void modeChanged(int newMode) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("[STBModeChangeListenerImpl.modeChanged]Called.");
            Log.printInfo("[STBModeChangeListenerImpl.modeChanged]Param - New Mode : "+newMode);
        }
        setSTBMode(newMode);
    }
    public static void setSTBMode(int reqMode) {
        if (Log.INFO_ON) {
            Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]Called.");
            Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]Param - Mode : "+reqMode);
        }
        switch(reqMode) {
            case MonitorService.STB_MODE_NORMAL:
                Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]MonitorService.STB_MODE_NORMAL");
                ErrorMessageManager.getInstance().setSTBModeNormal(true);
                break;
            case MonitorService.STB_MODE_STANDALONE:
                Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]MonitorService.STB_MODE_STANDALONE");
                ErrorMessageManager.getInstance().setSTBModeNormal(false);
                break;
            case MonitorService.STB_MODE_NO_SIGNAL:
                Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]MonitorService.STB_MODE_NO_SIGNAL");
                ErrorMessageManager.getInstance().setSTBModeNormal(false);
                break;
            case MonitorService.STB_MODE_MANDATORY_BLOCKED:
                Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]MonitorService.STB_MODE_MANDATORY_BLOCKED");
                ErrorMessageManager.getInstance().setSTBModeNormal(false);
                break;
            case MonitorService.STB_MODE_BOOTING:
                Log.printInfo("[STBModeChangeListenerImpl.setSTBMode]MonitorService.STB_MODE_BOOTING");
                ErrorMessageManager.getInstance().setSTBModeNormal(false);
                break;
        }
    }
}

package com.videotron.tvi.illico.errormessage.communication;

import java.rmi.RemoteException;
import org.dvb.io.ixc.IxcRegistry;
import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.errormessage.Rs;
import com.videotron.tvi.illico.errormessage.controller.ErrorMessageVbmController;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;

public class CommunicationManager implements InbandDataListener {
    private static CommunicationManager instance;
    /** The monitor service. */
    private MonitorService mSvc;
    /** The PVR service */
    private PvrService pvrSvc;
    /** The ISA service */
    private ISAService isaSvc;
    private ScreenSaverConfirmationListenerImpl ssclImpl;
    private STBModeChangeListenerImpl sImpl;
    /** The epg service. */
    private EpgService eSvc;
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private StcService stcSvc;
    private LogLevelChangeListenerImpl llclImpl;
    /*********************************************************************************
     * VBM Service-related
     *********************************************************************************/
    private VbmService vSvc;

    private CommunicationManager() {
    }

    public static synchronized CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }
    public void init() {
        lookupMonitorService();
        lookupVbmService();
        llclImpl = new LogLevelChangeListenerImpl();
        sImpl = new STBModeChangeListenerImpl();
        ssclImpl=new ScreenSaverConfirmationListenerImpl();
        lookupStcService();
    }
    public void dispose() {
        if (mSvc != null) {
            try {
                mSvc.removeSTBModeChangeListener(sImpl, Rs.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
            mSvc = null;
        }
        ssclImpl = null;
        mSvc = null;
        eSvc = null;
        isaSvc = null;
        pvrSvc = null;
        if (stcSvc != null) {
            try{
                stcSvc.removeLogLevelChangeListener(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
            stcSvc = null;
        }
        llclImpl = null;
        if (vSvc!=null) {
        	ErrorMessageVbmController.getInstance().dispose();
            vSvc=null;
        }
    }
    public void start() {
    }
    public void stop() {
    }
    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    private void lookupMonitorService() {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.lookupMonitorService]called");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (mSvc == null) {
                            try {
                                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                                mSvc = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupMonitorService]not bound - " + MonitorService.IXC_NAME);
                                }
                            }
                        }
                        if (mSvc != null) {
                            if (Log.INFO_ON) {
                                Log.printInfo("[CommMgr.lookupMonitorService]looked up.");
                            }
                            try{
                                STBModeChangeListenerImpl.setSTBMode(mSvc.getSTBMode());
                            }catch(Exception e) {
                                e.printStackTrace();
                            }
                            try{
                                mSvc.addSTBModeChangeListener(sImpl, Rs.APP_NAME);
                            }catch(Exception e) {
                                e.printStackTrace();
                            }
                            if (!Rs.IS_EMULATOR) {
                                try {
                                    mSvc.addInbandDataListener(CommunicationManager.this, Rs.APP_NAME);
                                } catch (Exception ignore) {
                                }
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("CommMgr.lookupMinitorService: retry MonitorService-lookup.");
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_MONITOR);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupVbmService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupVbmService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (vSvc == null) {
                            try {
                                String lookupName = "/1/2085/" + VbmService.IXC_NAME;
                                vSvc = (VbmService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupVbmService]not bound - " + VbmService.IXC_NAME);
                                }
                            }
                        }
                        if (vSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupVbmService]looked up.");
                            }
                            ErrorMessageVbmController.getInstance().init(vSvc);
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupVbmService]retry lookup.");
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_VBM);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public MonitorService getMonitorService() {
        if (mSvc == null) {
            try {
                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                mSvc = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getMonitorService]Not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
        return mSvc;
    }
    public PvrService getPvrService() {
        if (pvrSvc == null) {
            try {
                String lookupName = "/1/20A0/" + PvrService.IXC_NAME;
                pvrSvc = (PvrService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getPvrService]Not bound - " + PvrService.IXC_NAME);
                }
            }
        }
        return pvrSvc;
    }
    public ISAService getISAService() {
        if (isaSvc == null) {
            try {
                String lookupName = "/1/2200/" + ISAService.IXC_NAME;
                isaSvc = (ISAService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getISAService]Not bound - " + ISAService.IXC_NAME);
                }
            }
        }
        return isaSvc;
    }
    public void requestStartUnboundApplication(String appName, String[] extendedParams) {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.requestStartUnboundApplication]Called.");
            Log.printInfo("[CommMgr.requestStartUnboundApplication]Param - Request app name : "+appName);
            Log.printInfo("[CommMgr.requestStartUnboundApplication]Param - Request params : "+extendedParams);
        }
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try {
                svc.startUnboundApplication(appName, extendedParams);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getMonitorService]Not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
    }
    public void requestAddScreenSaverConfirmListener() {
        Log.printInfo("[CommMgr.requestAddScreenSaverConfirmListener] Called.");
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try {
                svc.addScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getMonitorService]Not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
    }
    public void requestRemoveScreenSaverConfirmListener() {
        Log.printInfo("[CommMgr.requestRemoveScreenSaverConfirmListener] Called.");
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try {
                svc.removeScreenSaverConfirmListener(ssclImpl, Rs.APP_NAME);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getMonitorService]Not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
    }
    public void requestResetScreenSaverTimer() {
        Log.printInfo("[CommMgr.requestResetScreenSaverTimer] Called.");
        MonitorService svc = getMonitorService();
        if (svc != null) {
            try {
                svc.resetScreenSaverTimer();
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getMonitorService]Not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
    }
    
    /*********************************************************************************
     * Epg Service-related
     *********************************************************************************/
    public EpgService getEpgService() {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.getEpgService]Called.");
        }
        if (eSvc == null) {
            try {
                String lookupName = "/1/2026/" + EpgService.IXC_NAME;
                eSvc = (EpgService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getEpgService]Not bound - " + EpgService.IXC_NAME);
                }
            }
        }
        return eSvc;
    }
    public void requestChangeChannel(String callLetter) {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.requestChangeChannel]Called.");
            Log.printInfo("[CommMgr.requestChangeChannel]Param - Request call letter : "+callLetter);
        }
        EpgService svc = getEpgService();
        if (svc != null) {
            try{
                TvChannel tvChannel = svc.getChannel(callLetter);
                if (tvChannel != null) {
                    svc.getChannelContext(0).changeChannel(tvChannel);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[CommMgr.requestChangeChannel]Execute sucessfully.");
                    }
                }
            }catch(Exception e) {
                Log.print(e);
            }
        }
    }
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private void lookupStcService() {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.lookupStcService]Called.");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (stcSvc == null) {
                            try {
                                String lookupName = "/1/2100/" + StcService.IXC_NAME;
                                stcSvc = (StcService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupStcService]Not bound - " + StcService.IXC_NAME);
                                }
                            }
                        }
                        if (stcSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupStcService]looked up.");
                            }
                            try {
                                int currentLevel = stcSvc.registerApp(Rs.APP_NAME);
                                Log.printDebug("stc log level = " + currentLevel);
                                Log.setStcLevel(currentLevel);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                            if (o != null) {
                                DataCenter.getInstance().put("LogCache", o);
                            }
                            try {
                                stcSvc.addLogLevelChangeListener(Rs.APP_NAME, llclImpl);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupStcService]retry lookup.");
                        }
                        try {
                            Thread.sleep(2000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            Log.print(e);
        }
    }
    /*********************************************************************************
     * InbandDataListener-implemented
     *********************************************************************************/
    public void receiveInbandData(String locator) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("[CommMgr.receiveInbandData]Called.");
            Log.printInfo("[CommMgr.receiveInbandData]Param - Locator : " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        if (mSvc != null) {
            mSvc.completeReceivingData(Rs.APP_NAME);
            if (Log.DEBUG_ON) {
                Log.printDebug("[CommMgr.receiveInbandData]called completeReceivingData.");
            }
        }
    }
}

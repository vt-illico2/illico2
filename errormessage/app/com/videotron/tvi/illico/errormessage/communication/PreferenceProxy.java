package com.videotron.tvi.illico.errormessage.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.errormessage.Rs;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

public class PreferenceProxy implements PreferenceListener{
    /** The instance - PreferenceProxy. */
    private static PreferenceProxy instance;
    /** The preference service. */
    private PreferenceService uppSvc;
    /** The Constant LANGUAGE_TYPE_ENGLISH. */
    public static final int LANGUAGE_TYPE_ENGLISH = 0;
    /** The Constant LANGUAGE_TYPE_FRENCH. */
    public static final int LANGUAGE_TYPE_FRENCH = 1;
    private String curLangType;

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
    }
    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }

    public void init() {
        lookupPreferenceService();
    }
    public void dispose() {
        curLangType = null;
        if (uppSvc != null) {
            try{
                uppSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception e) {
                e.printStackTrace();
            }
            uppSvc = null;
        }
        instance = null;
    }

    /**
     * Look up User Profile and Preference.
     */
    private void lookupPreferenceService() {
        if (Log.INFO_ON) {
            Log.printInfo("[PrefProxy.lookupPreferenceService]Called.");
        }
        /** IXC Binding */
        try {
            new Thread() {
                public void run() {
                    String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                    while (true) {
                        if (uppSvc == null) {
                            try {
                                uppSvc = (PreferenceService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[PrefProxy.lookupPreferenceService]Not bound - " + PreferenceService.IXC_NAME);
                                }
                            }
                        }
                        if (uppSvc != null) {
                            if (Log.INFO_ON) {
                                Log.printInfo("[PrefProxy.lookupPreferenceService]Looked up - PreferenceService.");
                            }
                            String[] uppData = null;
                            try {
                                curLangType = Definitions.LANGUAGE_FRENCH;
                                uppData = uppSvc.addPreferenceListener(PreferenceProxy.this,
                                        Rs.APP_NAME, new String[] { PreferenceNames.LANGUAGE },
                                        new String[] { curLangType });
                            } catch (Exception e) {
                                Log.print(e);
                            }
                            if (uppData != null) {
                                try {
                                    receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[0]);
                                } catch (Exception e) {
                                    Log.print(e);
                                }
                            }
                            break;
                        }
                        try {
                            Thread.sleep(5000);
                        } catch (Exception ignore) {
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            Log.print(e);
        }
    }
    /**
     * invoked when UPP application has been finished to check Rights and Filters.
     * @param result
     *            result checks ring filter
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveCheckRightFilter(String result) throws RemoteException {
    }

    /**
     * invoked when the value of the preference has changed.
     *
     * @param name
     *            Updated Preference Name
     * @param value
     *            Updated value of Preference
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("[PrefProxy.receiveUpdatedPreference]Name:[" + name + "]-Value:[" + value + "]");
        }
        if (name == null || value == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[PrefProxy.receiveUpdatedPreference]Parameter is invalid. end.");
            }
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            curLangType = value;
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
        }
    }
    public int getLanguageType() {
        if (Log.INFO_ON) {
            Log.printInfo("[PrefProxy.getLanguageType]Called.");
        }
        if (curLangType != null && curLangType.equals(Definitions.LANGUAGE_ENGLISH)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.getLanguageType]Current language type is english.");
            }
            return LANGUAGE_TYPE_ENGLISH;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.getLanguageType]Current language type is french.");
        }
        return LANGUAGE_TYPE_FRENCH;
    }
}

package com.videotron.tvi.illico.errormessage.communication;

import java.rmi.RemoteException;
import java.util.Hashtable;

import com.videotron.tvi.illico.errormessage.controller.DataManager;
import com.videotron.tvi.illico.errormessage.controller.ErrorMessageManager;
import com.videotron.tvi.illico.ixc.errormessage.CommonMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.log.Log;

public class ErrorMessageServiceImpl implements ErrorMessageService {
	private static ErrorMessageServiceImpl instance;
	private static ErrorMessageManager emMgr = ErrorMessageManager.getInstance();

	private ErrorMessageServiceImpl() {
	}

	public static synchronized ErrorMessageServiceImpl getInstance() {
		if (instance == null) {
			instance = new ErrorMessageServiceImpl();
		}
		return instance;
	}

	public void init() {
	}

	public void dispose() {
	}

	public void showErrorMessage(String errorCode, ErrorMessageListener l, Hashtable dth) throws RemoteException {
		if (Log.ERROR_ON) {
			Log.printError(errorCode);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgSvcImpl.showErrorMessage]Param - Error code : " + errorCode);
			Log.printDebug("[ErrMsgSvcImpl.showErrorMessage]Param - Listener : " + l);
			Log.printDebug("[ErrMsgSvcImpl.showErrorMessage]Param - Dynamic tag hash : " + dth);
		}
		emMgr.showErrorMessage(errorCode, l, dth);
	}

	public void showErrorMessage(String errorCode, ErrorMessageListener l) throws RemoteException {
		showErrorMessage(errorCode, l, null);
	}

	public void showErrorMessage(String errorCode, ErrorMessage message, ErrorMessageListener l) throws RemoteException {
		emMgr.showErrorMessage(errorCode, message, l);
	}

	public void hideErrorMessage() throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgSvcImpl.hideErrorMessage]Request hide error message by IXC.");
		}
		emMgr.hideErrorMessage();
	}

	public ErrorMessage getErrorMessage(String errorCode) throws RemoteException {
		if (Log.ERROR_ON) {
			Log.printError(errorCode);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgSvcImpl.getErrorMessage]Param - Error code : " + errorCode);
		}
		return DataManager.getInstance().getErrorMessage(errorCode);
	}

	public void showCommonMessage(String commonCode) throws RemoteException {
		showCommonMessage(commonCode, null);
	}
	
	public void showCommonMessage(String commonCode, ErrorMessageListener l) throws RemoteException {
		if (Log.ERROR_ON) {
			Log.printError(commonCode);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgSvcImpl.showCommonMessage]Param - commonCode : " + commonCode);
			Log.printDebug("[ErrMsgSvcImpl.showCommonMessage]Param - Listener : " + l);
		}
		emMgr.showCommonMessage(commonCode, l);
	}

	public CommonMessage getCommonMessage(String commonCode) throws RemoteException {
		if (Log.ERROR_ON) {
			Log.printError(commonCode);
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("[ErrMsgSvcImpl.showCommonMessage]Param - commonCode : " + commonCode);
		}
		return DataManager.getInstance().getCommonMessage(commonCode);
	}
}

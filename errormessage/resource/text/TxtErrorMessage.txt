ErrorCode	Le code d'erreur	Error code
Code850	Code 850 – Télé ou câble HDMI non compatible UHD/4K	Code 850 – TV or HDMI cable not UHD/4K compatible
Code851	Code 851 – Télé ou port HDMI non compatible HDCP 2.2	Code 851 – TV or HDMI port not HDCP 2.2 compatible
NonUHD	Votre terminal n’est pas compatible UHD/4K	Your terminal is not UHD/4K compatible
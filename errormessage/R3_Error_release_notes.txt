﻿This is Error App Release Notes.
GENERAL RELEASE INFORMATION
 - Release Details
    - Release version
      : version 1.0.10.0
    - Release file name
      : R2_Error.zip
    - Release type (official, debug, test,...)
      : official       
    - Release date
      : 2013.02.25

*** 
version 1.0.10.0
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-4510 [R3][TRT] Screensaver displayed over video feed (no application visible)
*** 
version 1.0.9.0
    - New Feature 
        + Applied changed ApplicationConfig module.
    - Resolved Issues 
        N/A
*** 
version 1.0.7.0
    - New Feature
        + added VBM-group id module.
        + wrote version info to application.prop        
    - Resolved Issues 
        N/A
*** 
version 1.0.6.0
    - New Feature
        + If vbm parameters are null, then write as blank to vbm.
    - Resolved Issues 
        N/A            
*** 
version 1.0.5.0
    - New Feature 
        + ERR501 is added newly
        + ERR502 is added newly
    - Resolved Issues 
        N/A      
*** 
version 1.0.4.0
    - New Feature
        + Merged from R2 source.
        + integrated with VBM log.
    - Resolved Issues
        N/A     
*** 
version 1.0.3.0
    - New Feature 
        + Modified the error message icon logic. 
    - Resolved Issues 
        N/A
*** 
version 1.0.2.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 1.0.1.0
    - New Feature 
        + Fixed encoding module of ms data parser.
    - Resolved Issues 
        N/A
*** 
version 0.3.26.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3618 [regression] [ui/gui] [Screen Saver] - Screen saver should be displayed after user inactivity over all user interfaces screens  
*** 
version 0.3.25.1
    - New Feature 
        + Change contant name from ACTION_TYPE_CLOSED to BUTTON_TYPE_EXIT.
    - Resolved Issues 
        + VDTRMASTER-3433 Pressing exit when in an error popup does not return to catalogue      
*** 
version 0.3.24.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3433 Pressing exit when in an error popup does not return to catalogue
*** 
version 0.3.23.0
    - New Feature 
        + Modified the error message icon logic. 
    - Resolved Issues 
        N/A
*** 
version 0.3.22.0
    - New Feature 
        + modified retry milliseconds for IXC lookup. 
    - Resolved Issues 
        N/A
*** 
version 0.3.21.0
    - New Feature 
        + Fixed encoding module of ms data parser.
    - Resolved Issues 
        N/A
*** 
version 0.3.20.0
    - New Feature 
        + Added ErrorMessageService interface method.
    - Resolved Issues 
        N/A
*** 
version 0.3.19.0
    - New Feature 
        + Aplied the chnaged Fremework-Log
    - Resolved Issues 
        N/A
*** 
version 0.3.18.1
    - New Feature 
        + Clicking animation(Buttons)
    - Resolved Issues 
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
*** 
version 0.3.17.0
    - New Feature 
        + Applied modified Framework(OC unload)
    - Resolved Issues 
        N/A
*** 
version 0.3.16.0
    - New Feature 
        + added STB mode-related module.
    - Resolved Issues 
        N/A
*** 
version 0.3.15.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1799  Log error codes in ErrorMessage app at the ERROR log level
*** 
version 0.3.14
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1669 [Error Message] Select key is responsive (in the "background" app) when an error message with no button is displayed
*** 
version 0.3.13
    - New Feature 
        + Removed error message timer.
    - Resolved Issues 
        N/A
*** 
version 0.3.12
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1321  Can't exit (EXIT button) from an error popup   
*** 
version 0.3.11
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1517 [VT][ErrorMessage] A icon in error pop-up is not display when connecting and disconnecting RF cable to STB   
*** 
version 0.3.10
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1345  DDC 6451 - Setting-Parental control : english text in french mode  
*** 
version 0.3.9
    - New Feature 
        + Fixed Stc service module.
    - Resolved Issues 
        N/A  
*** 
version 0.3.8
    - New Feature 
        + Fixed local default error message module.
    - Resolved Issues 
        N/A  
*** 
version 0.3.7
    - New Feature 
        + Fixed default error message module.
    - Resolved Issues 
        N/A  
*** 
version 0.3.6
    - New Feature 
        + Fixed error message close action bug.
    - Resolved Issues 
        N/A  
*** 
version 0.3.5
    - New Feature 
        + Fixed error message stop module.
    - Resolved Issues 
        N/A  
*** 
version 0.3.4
    - New Feature 
        + Fixed start module bugs.
    - Resolved Issues 
        N/A  
*** 
version 0.3.3
    - New Feature 
        + Fixed error message close action bug.
    - Resolved Issues 
        N/A  
*** 
version 0.3.2
    - New Feature 
        + Added Emergency error message module.
    - Resolved Issues 
        N/A  
*** 




-----------------------------------------------------------------------------------
- Release details
  + New feature : applied STC service module.
  + New feature : modified parsing module as ms data format(IB, OOB).
- Release name : version 0.3.1
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.23
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Nothing
- Release name : version 0.3.00
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.18
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : changes popup background image.
- Release name : version 0.2.21
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.26
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : [VDTRMASTER-939]fixed IB data parsing module.
- Release name : version 0.2.20
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.24
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed Error message IB update module.
- Release name : version 0.2.19
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.14
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Error message application is shown on the Screen saver application.
- Release name : version 0.2.18
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.09
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : 같은 메시지가 반복되는 부분 수정
- Release name : version 0.2.17
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.21
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Dynamic tag를 general하게 처리 할 수 있도록, 고정값 사용을 제거
- Release name : version 0.2.16
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.21
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : added dynamic tag module.
- Release name : version 0.2.15
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.20
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed image width measurement errors.
- Release name : version 0.2.14
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.10
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : added close button about ms default error message.
  + Fixed : added close button about local default error message.
- Release name : version 0.2.13
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.09
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed icon image module of error message popup
- Release name : version 0.2.12
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.07
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed Error popup deactivation module.(When error message popup activate, then next error message skip.)
- Release name : version 0.2.11
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.06
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed Error popup deactivation module.
- Release name : version 0.2.10
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.05
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed Error popup deactivation module.
- Release name : version 0.2.09
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2011.01.04
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : When you first launch a popup, french was always represented.
  + Fixed : If the title is more than 23 characters, then applied shorten module.
- Release name : version 0.2.08
- Release type (official, debug, test,...) : test
- Release date : 2010.12.28
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : added logs about channel tuning.
- Release name : version 0.2.07
- Release type (official, debug, test,...) : test
- Release date : 2010.12.28
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : a pop-up disappears immediately
  + Fixed : added to shorten modlue of error title.
- Release name : version 0.2.06
- Release type (official, debug, test,...) : test
- Release date : 2010.12.27
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed close timer module.
  + Fixed : fixed to execute other application, then deactivated.
- Release name : version 0.2.05
- Release type (official, debug, test,...) : test
- Release date : 2010.12.24
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : change IB data path as a "Error_DataIB"
- Release name : version 0.2.04
- Release type (official, debug, test,...) : test
- Release date : 2010.12.23
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : change IB data path as a "Comm_DataIB"
  + Fixed : Local�� Default Error Message �߰� 
- Release name : version 0.2.03
- Release type (official, debug, test,...) : test
- Release date : 2010.12.22
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : change IB data path as a "Error_DataIB"
- Release name : version 0.2.02
- Release type (official, debug, test,...) : test
- Release date : 2010.12.22
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Initial Release
- Release name : version 0.2.01
- Release type (official, debug, test,...) : test
- Release date : 2010.12.21
-----------------------------------------------------------------------------------


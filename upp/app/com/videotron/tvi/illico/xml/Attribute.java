package com.videotron.tvi.illico.xml;

/**
 * This class represent a attribute of Element in XML. Attributes provide additional information about an element.
 * @author Woojung Kim
 * @version 1.1
 */
public class Attribute {

    /** The name of attribute. */
    private String name;
    /** The value of attribute. */
    private String value;

    /**
     * Get a name of Attribute.
     * @return The name of Attribute.
     */
    public String getName() {
        return name;
    }

    /**
     * Set a name of Attribute.
     * @param name The name of Attribute to be set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get a value of Attribute.
     * @return The value of Attribute.
     */
    public String getValue() {
        return value;
    }

    /**
     * Set a value of Attribute.
     * @param value The value of Attribute to be set.
     */
    public void setValue(String value) {
        this.value = value;
    }
}

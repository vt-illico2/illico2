package com.videotron.tvi.illico.xml;

/**
 * This class represent a Element in XML. An element can contain other elements, simple text or a mixture of both.
 * Elements can also have attributes.
 * @author Woojung Kim
 * @version 1.1
 */
public class Element {

    /** The name of Element. */
    private String name;
    /** The text of Element. */
    private String text;
    /** The attribute array for Element. */
    private Attribute[] attrs;
    /** The elements array for Element. */
    private Element[] elems;

    /**
     * Get a name of Element.
     * @return The name of Element.
     */
    public String getName() {
        return name;
    }

    /**
     * Set a name of Element.
     * @param n The name of Element.
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * Set a text of Element.
     * @param n The text of Element.
     */
    public void setText(String t) {
        this.text = t;
    }

    /**
     * Get a text of Element.
     * @return The text of Element.
     */
    public String getText() {
        return text;
    }

    /**
     * Get a attributes of Element.
     * @return The attributes to provide additional information about an element.
     */
    public Attribute[] getAttributes() {
        return attrs;
    }

    /**
     * Set a attributes of Element.
     * @param attributes The attributes to provide additional information about an element.
     */
    public void setAttributes(Attribute[] attributes) {
        this.attrs = attributes;
    }

    /**
     * Get a sub elements of Element.
     * @return The sub elements of Element.
     */
    public Element[] getElements() {
        return elems;
    }

    /**
     * Set a sub elements of Elements.
     * @param elements The sub elements of Element.
     */
    public void setElements(Element[] elements) {
        this.elems = elements;
    }
}

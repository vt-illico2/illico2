package com.videotron.tvi.illico.xml;

/**
 * This class make a XML to include a informations of Response for Request.
 * @author Woojung Kim
 * @version 1.1
 */
public class XMLMaker {

    /** The private instance to make a singleton instance. */
    private static XMLMaker instance;

    /** The value to calculate a depth of Elements. */
    private int deep;

    /** the private constructor for singleton instance. */
    private XMLMaker() {
    }

    /**
     * Return a instance to be made for singleton.
     * @return The singleton instance of this class.
     */
    public static XMLMaker getInstance() {
        if (instance == null) {
            instance = new XMLMaker();
        }

        return instance;
    }

    /**
     * Make a string of Response XML using parameter to include a informations.
     * @param encode The encode type. for example ISO-8859-1, UTF-8 etc.
     * @param elements The elements used to make a xml.
     * @return A string of XML.
     */
    public String makeResponseXML(String encode, Element[] elements) {
        StringBuffer sf = new StringBuffer();
        deep = 0;
        // header
        sf.append("<?xml version=\"1.0\" encoding=\"");
        sf.append(encode);
        sf.append("\"?>\n");

        addElement(sf, elements);

        return sf.toString();
    }

    /**
     * Add a information using parameter.
     * @param sf StringBuffer which add a elements.
     * @param elements elements to be added.
     */
    private void addElement(StringBuffer sf, Element[] elements) {

        for (int j = 0; j < elements.length; j++) {
            deep++;

            for (int i = 1; i < deep; i++) {
                sf.append("\t");
            }
            sf.append("<");
            sf.append(elements[j].getName());

            if (elements[j].getAttributes() != null) {
                Attribute[] attrs = elements[j].getAttributes();

                addAttribute(sf, attrs);
            }

            if (elements[j].getElements() != null || elements[j].getText() != null) {
                Element[] elems = elements[j].getElements();

                if (elems != null) {
                    sf.append(">\n");
                    addElement(sf, elems);
                }

                if (elements[j].getText() != null) {
                    sf.append(">");
                    sf.append(elements[j].getText());
                } else {
                    for (int i = 1; i < deep; i++) {
                        sf.append("\t");
                    }
                }

                sf.append("</");
                sf.append(elements[j].getName());
                sf.append(">\n");
            } else {
                sf.append("/>\n");
            }

            deep--;
        }
    }

    /**
     * Add attribute.
     * @param sf StringBuffer which add a attributes.
     * @param attrs attributes to be added.
     */
    private void addAttribute(StringBuffer sf, Attribute[] attrs) {
        for (int i = 0; i < attrs.length; i++) {
            sf.append(" ");
            sf.append(attrs[i].getName());
            sf.append("=\"");
            sf.append(attrs[i].getValue());
            sf.append("\" ");
        }
    }
}

/**
 * @(#)GeneralSettingsRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.GeneralSettingsUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a preferences for General Settings.
 * @author Woojung Kim
 * @version 1.1
 */
public class GeneralSettingsRenderer extends NormalSettingsBackRenderer {

    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for secondly input. */
    private Image input2Img;
    /** a image for secondly inpout to have a focus. */
    private Image inputFocus2Img;
    /** a image for dimmed input. */
    private Image inputDimImg;
    /** a image for dimmed panel. */
    private Image dimmedPanelImg;
    /** a image for panel to have a focus. */
    private Image focusPanelImg;

    /** a color for dimmed setup. */
    private Color setupDimmedColor = new Color(185, 185, 185);

    /** a string array for preference ids. */
    private String[] preferenceIds;
    /** a string for setup wizard. */
    private String setupWizardName;
    /** a string to start setup wizard. */
    private String startSetupWizard;
    /** a string for restart terminal. */
    private String restartTerminalName;
    /** a string to restart. */
    private String restartTerminalButton;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        GeneralSettingsUI ui = (GeneralSettingsUI) c;
        preferenceIds = ui.preferenceIds;
        setupWizardName = BaseUI.getMenuText(KeyNames.SETUP_WIZARD);
        startSetupWizard = BaseUI.getMenuText(KeyNames.START_SETUP_WIZARD);
        restartTerminalName = BaseUI.getMenuText(KeyNames.RESTART_TERMINAL);
        restartTerminalButton = BaseUI.getMenuText(KeyNames.RESTART_TERMINAL_BUTTON);

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inputDimImg = BaseUI.getImage("input_210_foc_dim.png");
        dimmedPanelImg = BaseUI.getImage("08_dimmed_panel.png");
        focusPanelImg = BaseUI.getImage("08_dimmed_panel_foc.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        GeneralSettingsUI ui = (GeneralSettingsUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        Category category = (Category) BaseUI.peekHistory();
        String title = BaseUI.getMenuText(category.getCategoryId());
        g.drawString(title, 55, 113);

        for (int i = 0; i < preferenceIds.length; i++) {
            int step = i * 37;
            Preference pre = (Preference) preTable.get(preferenceIds[i]);
            g.setFont(f17);
            g.setColor(nameColor);
            if (pre != null) {
                String name = pre.getName();
                String[] names = TextUtil.split(name, fm17, 225);
                if (names.length > 1) {
                    g.drawString(names[0], NAME_X, 161 + step - 9);
                    g.drawString(names[1], NAME_X, 161 + step + 9);
                } else {
                    g.drawString(name, NAME_X, 161 + step);
                }

                g.setFont(f18);
                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    if (i == 1) { // Postal code
                        g.drawImage(inputFocus2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                    }
                } else {
                    g.setColor(dimmedNameColor);
                    if (i == 1) { // Postal code
                        g.drawImage(input2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputImg, INPUT_X, 141 + step, c);
                    }
                }
                String value = pre.getCurrentValue();
                value = BaseUI.getOptionText(value);
                g.drawString(value, VALUE_X, 162 + step);
            } else {
            	
            	if (i == 3) {
	                // Set up wizard
	                g.drawString(setupWizardName, NAME_X, 161 + step);
            	} else if (i == 4) {
            		// restart terminal
            		g.drawString(restartTerminalName, NAME_X, 161 + step);
            	}
                if (i == c.getFocus()) {
                    g.setColor(focusName2Color);
                    g.drawImage(focusPanelImg, INPUT_X - 1, 141 + step, c);
                } else {
                    g.setColor(setupDimmedColor);
                    g.drawImage(dimmedPanelImg, INPUT_X - 1, 141 + step, c);
                }
                g.setFont(f18);
                if (i == 3) {
                	GraphicUtil.drawStringCenter(g, startSetupWizard, 444, 162 + step);
                } else if (i == 4) {
                	GraphicUtil.drawStringCenter(g, restartTerminalButton, 444, 162 + step);
                	
                }
            }
        }
    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        GeneralSettingsUI ui = (GeneralSettingsUI) parent;
        int idx = parent.getFocus();
        int step = idx * 37;
        Hashtable preTable = ui.getPreferences();
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();

            String[] names = TextUtil.split(name, fm17, 225);
            if (names.length > 1) {
                g.drawString(names[0], NAME_X, 161 + step - 9);
                g.drawString(names[1], NAME_X, 161 + step + 9);
            } else {
                g.drawString(name, NAME_X, 161 + step);
            }

            g.setFont(f18);
            g.drawImage(inputDimImg, INPUT_X, 141 + step, ui);
            String value = pre.getCurrentValue();
            value = BaseUI.getOptionText(value);
            g.drawString(value, VALUE_X, 162 + step);
        }
    }
}

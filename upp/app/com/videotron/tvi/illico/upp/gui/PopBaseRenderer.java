/**
 * @(#)PopBaseRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class is a base renderer for popup style.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopBaseRenderer extends BaseRenderer {

    /** a constant to indicate nothing will be used. */
    public static final int ICON_TYPE_NONE = 0;
    /** a constant to indicate a red icon will be used. */
    public static final int ICON_TYPE_RED = 1;
    /** a constant to indicate a yellow icon will be used. */
    public static final int ICON_TYPE_YELLOW = 2;
    /** a constant to indicate a orange icon will be used. */
    public static final int ICON_TYPE_ORANGE = 3;
    /** a image for top background. */
//    private Image topImg = dataCenter.getImage("05_pop_glow_t.png");
    /** a image for middle background. */
//    private Image midImg = dataCenter.getImage("05_pop_glow_m.png");
    /** a image for bottom background. */
//    private Image bottomImg = dataCenter.getImage("05_pop_glow_b.png");
    /** a image for shadow. */
//    private Image shadowImg = dataCenter.getImage("pop_sha.png");
    /** a image for gap. */
    private Image gapImg = dataCenter.getImage("pop_gap_379.png");
    /** a image for high. */
//    private Image highImg = dataCenter.getImage("pop_high_350.png");
    /** a image for red background. */
//    private Image redBgImg = dataCenter.getImage("pop_red_478.png");
    /** a image for red icon. */
    private Image redIconImg = dataCenter.getImage("icon_noti_red.png");
    /** a image for yellow icon. */
    private Image yellowIconImg = dataCenter.getImage("icon_noti_yellow.png");
    /** a image for orange icon. */
    private Image orangeIconImg = dataCenter.getImage("icon_noti_or.png");
    /** a image for current icon. */
    protected Image currentIconImg;

    /** The color of title. */
    protected Color titleColor = new Color(255, 203, 0);
    /** a color for fill rect. */
    protected Color fillRectColor = new DVBColor(35, 35, 35, 255);
    /** a color for button. */
    protected Color btnColor = new Color(3, 3, 3);
    /** a font for size 24. */
    protected Font f24 = FontResource.BLENDER.getFont(24);
    
    /** a string for title. */
    protected String title = "";
    /** a int for iconType. */
    protected static int iconType = ICON_TYPE_NONE;
    /** a int for icon gap. */
    protected static int iconGap = 0;

    /**
     * Set a title and icon type.
     * @param t The title of popup.
     * @param icon The icon type of popup's title if need.
     */
    public void setTitle(String t, int icon) {
        this.title = t;
        
        iconType = icon;

        if (iconType == ICON_TYPE_NONE) {
            currentIconImg = null;
        } else if (iconType == ICON_TYPE_RED) {
            currentIconImg = redIconImg;
        } else if (iconType == ICON_TYPE_YELLOW) {
            currentIconImg = yellowIconImg;
        } else if (iconType == ICON_TYPE_ORANGE) {
            currentIconImg = orangeIconImg;
        }
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        g.setColor(dimmedColor);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

//        if (iconType == ICON_TYPE_RED) {
//            g.drawImage(redBgImg, 242, 79, c);
//        }

//        g.drawImage(topImg, 276, 113, c);
//        g.drawImage(midImg, 276, 193, c);
//        g.drawImage(bottomImg, 276, 317, c);
        g.setColor(fillRectColor);
        g.fillRect(306, 143, 350, 224);
        g.drawImage(gapImg, 285, 181, c);
//        g.drawImage(highImg, 306, 143, c);
//        g.drawImage(shadowImg, 306, 364, 350, 79, c);

        g.setColor(titleColor);
        g.setFont(f24);
        int x = 0;
        if (iconType == ICON_TYPE_NONE) {
            iconGap = 0;
            x = GraphicUtil.drawStringCenter(g, title, 480, 169);
        } else {
            iconGap = currentIconImg.getWidth(c) + 7;
            x = GraphicUtil.drawStringCenter(g, title, 494, 169);
        }

        if (iconType != ICON_TYPE_NONE) {
            g.drawImage(currentIconImg, x - iconGap, 163 - currentIconImg.getHeight(c) / 2, c); // 149 ~ 152
        }
    }
}

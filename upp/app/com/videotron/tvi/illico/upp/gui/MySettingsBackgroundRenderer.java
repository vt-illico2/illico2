/**
 * @(#)MySettingsBackgourndRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.MySettingsUI;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class render a background for Settings's main.
 * @author Woojung Kim
 * @version 1.1
 */
public class MySettingsBackgroundRenderer extends BaseRenderer {

    /** a image for background. */
    private Image bgImg;
    /** a image for hotkey background. */
//    private Image hotkeybgImg;
    /** a image for button of Exit. */
    private Image btnExitImg;
    /** a image for line. */
    private Image lineImg;
    /** a color for exit string. */
    private Color exitColor = new Color(236, 211, 143);
    /** a string for exit. */
    private String exitString;
    /** a x coordinate for exit image. */
    private int exitBtnX;

    /**
     * Constructor. set a background image.
     */
    public MySettingsBackgroundRenderer() {
    }

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        exitString = BaseUI.getMenuText(KeyNames.TO_EXIT_SETTINGS);
        exitBtnX = 70 + fm18.stringWidth(exitString);

        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        btnExitImg = (Image) imgTable.get(PreferenceService.BTN_EXIT);

        bgImg = BaseUI.getImage("07_bg_main.jpg");
//        hotkeybgImg = BaseUI.getImage("01_hotkeybg_setting.png");
        lineImg = BaseUI.getImage("00_mini_line.png");
        
        setBackground(bgImg);
        
        // pre-loaded for entering a second depth smoothly .
        BaseUI.getImage("bg.jpg");
        BaseUI.getImage("input_210_sc.png");
        BaseUI.getImage("input_210_foc2.png");
        BaseUI.getImage("input_210_foc_dim.png");
        BaseUI.getImage("inbox_210.png");
        BaseUI.getImage("focus_05_05.png");

        BaseUI.getImage("08_op_bg_b.png");
        BaseUI.getImage("08_op_bg_t.png");
        BaseUI.getImage("08_op_bg_m.png");
        BaseUI.getImage("08_bg_line.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

        MySettingsUI ui = (MySettingsUI) c;

//        g.drawImage(hotkeybgImg, 367, 466, c);

        Category[] categoies = ui.getMenuUI().getCategories();

        for (int i = 0; categoies != null && i < categoies.length; i++) {
            int step = i * 34;
            g.drawImage(lineImg, 66, 139 + step, c);
        }

        g.setColor(exitColor);
        g.setFont(f18);
        if (exitString != null) {
            g.drawString(exitString, 64, 503);
            g.drawImage(btnExitImg, exitBtnX, 487, c);
        }

        if (Log.EXTRA_ON) {
            g.setFont(f17);
            g.setColor(Color.WHITE);
            g.drawString(FrameworkMain.getInstance().getApplicationVersion(), 360, 460);
        }
    }
}

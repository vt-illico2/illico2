/**
 * @(#)TimeBasedRestrictionsRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.Time;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.TimeBasedRestrictionsUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class render a times for Time based restrictions.
 * @author Woojung Kim
 * @version 1.1
 */
public class TimeBasedRestrictionsRenderer extends NormalSettingsBackRenderer {
    /** a image for initial background. */
    private Image initBgImg;
    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for secondly input. */
    private Image input2Img;
    /** a image for secondly input to have a focus. */
    private Image inputFocus2Img;
    /** a image for dimmed input. */
    private Image inputDimImg;
    /** a image for small input. */
    private Image inputSmallImg;
    /** a image for small input to have a focus. */
    private Image inputSmallFocusImg;
    /** a image for button to have a focus. */
    private Image btnFocusImg;
    /** a image for dimmed button. */
    private Image btnDimImg;
    /** a image for up arrow. */
    private Image arrowUpImg;
    /** a image for down arrow. */
    private Image arrowDownImg;
    /** a image for background and blue button. */
    private Image backImg, blueBtnImg;
    /** a string for start time. */
    private String startTimeStr;
    /** a string for end time. */
    private String endTimeStr;
    /** a string for day. */
    private String dayStr;
    /** a string for repeat. */
    private String repeatStr;
    /** a string for remove . */
    private String removeStr;
    /** a string for period. */
    private String periodStr;
    /** a string for every week . */
    private String everyWeekStr;
    /** a string for none. */
    private String noneStr;
    /** a string for no period. */
    private String noPeriod;
    /** a boolean to indicate whether language is English or not. */
    private boolean isEnglish;
    /** a renderer for period on first. */
    private PeriodRenderer firstRenderer = new PeriodRenderer(true);
    /** a renderer for period on second. */
    private PeriodRenderer secondRenderer = new PeriodRenderer(false);
    /** a string buffer. */
    private StringBuffer sf = new StringBuffer();

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        backImg = dataCenter.getImage("08_period_bg.png");
        Hashtable imgHash = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        blueBtnImg = (Image) imgHash.get(PreferenceService.BTN_B);

        startTimeStr = BaseUI.getMenuText(KeyNames.START_TIME);
        endTimeStr = BaseUI.getMenuText(KeyNames.END_TIME);
        dayStr = BaseUI.getMenuText(KeyNames.DAY);
        repeatStr = BaseUI.getMenuText(KeyNames.REPEAT);
        removeStr = BaseUI.getMenuText(KeyNames.BTN_REMOVE);
        periodStr = BaseUI.getMenuText(KeyNames.RESTRICTED_PERIOD);
        noPeriod = BaseUI.getMenuText(KeyNames.ADD_PERIOD);

        everyWeekStr = BaseUI.getPopupText(KeyNames.EVERY_WEEK);
        noneStr = BaseUI.getPopupText(KeyNames.NONE);

        initBgImg = BaseUI.getImage("08_period_bg_2.png");
        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inputDimImg = BaseUI.getImage("input_210_foc_dim.png");
        inputSmallImg = BaseUI.getImage("input_38.png");
        inputSmallFocusImg = BaseUI.getImage("input_38_foc.png");
        btnFocusImg = BaseUI.getImage("08_bt_foc.png");
        btnDimImg = BaseUI.getImage("08_bt_dim.png");
        arrowUpImg = BaseUI.getImage("02_ars_t.png");
        arrowDownImg = BaseUI.getImage("02_ars_b.png");

        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE);
        if (pre.getCurrentValue().equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent ui.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        TimeBasedRestrictionsUI ui = (TimeBasedRestrictionsUI) c;
        g.setColor(titleColor);
        g.setFont(f26);
        Preference parentPre = (Preference) BaseUI.peekHistory();
        g.drawString(parentPre.getName(), 55, 113);

        if (ui.getTimeDataSize() == 0) {
            g.drawImage(initBgImg, 7, 136, c);
            g.setFont(f18);
            g.setColor(dimmedNameColor);
            int x = GraphicUtil.drawStringCenter(g, noPeriod, 470, 224);
            g.drawImage(blueBtnImg, x + fm18.stringWidth(noPeriod) + 5, 207, c);
        } else {

            int whoHasFocus = ui.getWhoHasFocus();

            Time[] times = ui.displayTimes();

            if (times[0] != null) {
                if (whoHasFocus == 0) {
                    firstRenderer.paint(g, c, times[0], ui.getFocus());
                } else {
                    firstRenderer.paint(g, c, times[0], -1);
                }
            }

            if (times.length > 1 && times[1] != null) {
                if (whoHasFocus == 1) {
                    secondRenderer.paint(g, c, times[1], ui.getFocus());
                } else {
                    secondRenderer.paint(g, c, times[1], -1);
                }
            }

            if (ui.drawArrowUp()) {
                g.drawImage(arrowUpImg, 468, 122, c);
            }

            if (ui.drawArrowDown()) {
                g.drawImage(arrowDownImg, 468, 464, c);
            }

        }
    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        TimeBasedRestrictionsUI ui = (TimeBasedRestrictionsUI) parent;
        int whoHasFocus = ui.getWhoHasFocus();

        Time[] times = ui.displayTimes();

        if (times[0] != null) {
            if (whoHasFocus == 0) {
                firstRenderer.paintSelectedContent(g, parent, times[0], ui.getFocus());
            }
        }

        if (times.length > 1 && times[1] != null) {
            if (whoHasFocus == 1) {
                secondRenderer.paintSelectedContent(g, parent, times[1], ui.getFocus());
            }
        }

    }

    /**
     * This class render a time for one of Time based restrictions.
     * @author Woojung Kim
     * @version 1.1
     */
    class PeriodRenderer extends BaseRenderer {
        /** a boolean to indicate whether is fist line or not. */
        private boolean isFirstLine;

        /**
         * Constructor.
         * @param isFirst true if is at first line, false otherwise.
         */
        PeriodRenderer(boolean isFirst) {
            isFirstLine = isFirst;
        }

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         * @param time include a informations.
         * @param focus index of focus.
         */
        public void paint(Graphics g, UIComponent c, Time time, int focus) {
            TimeBasedRestrictionsUI ui = (TimeBasedRestrictionsUI) c;
            if (!isFirstLine) {
                g.translate(0, 176);
            }
            g.drawImage(backImg, 7, 136, c);
            g.setColor(dimmedNameColor);
            g.setFont(f20);
            GraphicUtil.drawStringCenter(g, ":", 269, 221);
            GraphicUtil.drawStringCenter(g, ":", 697, 221);

            // field names
            g.setFont(f17);
            g.setColor(nameColor);
            g.drawString(startTimeStr, 92, 221);
            g.drawString(endTimeStr, 520, 221);
            g.drawString(dayStr, 92, 261);
            g.drawString(repeatStr, 520, 261);

            g.setFont(f18);
            int drawLength = 2;
            if (isEnglish) {
                drawLength = 3;
            }
            // start time
            for (int i = 0; i < drawLength; i++) {
                int step = i * 52;
                if (focus == i + 1) {
                    g.drawImage(inputSmallFocusImg, 224 + step, 201, c);
                    g.setColor(focusNameColor);
                } else {
                    g.drawImage(inputSmallImg, 224 + step, 201, c);
                    g.setColor(dimmedNameColor);
                }

                if (i == 0) { // Hour
                    String timeValue;
                    if (ui.isInputMode() && focus == i + 1) {
                        timeValue = ui.getInputValue();
                    } else {
                        timeValue = time.getStartHour();
                        if (isEnglish && timeValue.equals("00")) {
                            timeValue = "12";
                        } else if (!isEnglish && time.getStartAmPm().equals("pm")) {
                            int timeInt = Integer.parseInt(timeValue);
                            timeInt += 12;
                            timeValue = df2.format(timeInt);
                        }
                    }
                    GraphicUtil.drawStringCenter(g, timeValue, 243, 223);
                } else if (i == 1) { // Min
                    if (ui.isInputMode() && focus == i + 1) {
                        String inputValue = ui.getInputValue();
                        GraphicUtil.drawStringCenter(g, inputValue, 295, 223);
                    } else {
                        GraphicUtil.drawStringCenter(g, time.getStartMin(), 295, 223);
                    }
                } else { // AMPM
                    GraphicUtil.drawStringCenter(g, time.getStartAmPm(), 347, 222);
                }
            }
            // end time
            for (int i = 0; i < drawLength; i++) {
                int step = i * 52;
                if (focus == i + 4) {
                    g.drawImage(inputSmallFocusImg, 652 + step, 201, c);
                    g.setColor(focusNameColor);
                } else {
                    g.drawImage(inputSmallImg, 652 + step, 201, c);
                    g.setColor(dimmedNameColor);
                }

                if (i == 0) { // Hour
                    String timeValue;
                    if (ui.isInputMode() && focus == i + 4) {
                        timeValue = ui.getInputValue();
                    } else {
                        timeValue = time.getEndHour();
                        if (isEnglish && timeValue.equals("00")) {
                            timeValue = "12";
                        } else if (!isEnglish && time.getEndAmPm().equals("pm")) {
                            int timeInt = Integer.parseInt(timeValue);
                            timeInt += 12;
                            timeValue = df2.format(timeInt);
                        }
                    }
                    GraphicUtil.drawStringCenter(g, timeValue, 671, 223);
                } else if (i == 1) { // Min
                    if (ui.isInputMode() && focus == i + 4) {
                        String inputValue = ui.getInputValue();
                        GraphicUtil.drawStringCenter(g, inputValue, 724, 223);
                    } else {
                        GraphicUtil.drawStringCenter(g, time.getEndMin(), 724, 223);
                    }
                } else { // AMPM
                    GraphicUtil.drawStringCenter(g, time.getEndAmPm(), 775, 222);
                }
            }

            // day
            if (focus == 7) {
                g.drawImage(inputFocus2Img, 224, 241, c);
                g.setColor(focusNameColor);
            } else {
                g.drawImage(input2Img, 224, 241, c);
                g.setColor(dimmedNameColor);
            }
            g.drawString(ui.getDaysText(time), 238, 262);

            // repeat
            if (focus == 8) {
                g.drawImage(inputFocusImg, 652, 241, c);
                g.setColor(focusNameColor);
            } else {
                g.drawImage(inputImg, 652, 241, c);
                g.setColor(dimmedNameColor);
            }
            if (time.getRepeat().equals(Definitions.OPTION_VALUE_YES)) {
                g.drawString(everyWeekStr, 666, 262);
            } else {
                g.drawString(noneStr, 666, 262);
            }

            if (time.getActive().equals(Definitions.OPTION_VALUE_DISABLE)) {
                g.setColor(overlapColor);
                g.fillRect(55, 137, 850, 156);
            }

            // remove
            if (focus == 9) {
                g.drawImage(btnFocusImg, 723, 143, c);
            } else {
                g.drawImage(btnDimImg, 723, 143, c);
            }
            g.setColor(focusName2Color);
            GraphicUtil.drawStringCenter(g, removeStr, 793, 164);

            // Period N
            sf.setLength(0);
            sf.append(periodStr);
            sf.append(" ");
            sf.append(ui.getTimeDataIndex(time) + 1);
            g.setColor(fontBgColor);
            g.drawString(sf.toString(), 74, 167);
            g.setColor(fontFgColor);
            g.drawString(sf.toString(), 73, 166);

            if (focus == 0) {
                g.drawImage(inputFocusImg, 224, 143, c);
                g.setColor(focusNameColor);
            } else {
                g.drawImage(inputImg, 224, 143, c);
                g.setColor(dimmedNameColor);
            }
            String value = BaseUI.getOptionText(time.getActive() + "d");
            g.drawString(value, 238, 164);

            if (!isFirstLine) {
                g.translate(0, -176);
            }
        }

        /**
         * Paint a selected content.
         * @param g Graphics to paint.
         * @param parent    parent UI.
         * @param time  include a informations.
         * @param focus index of focus.
         */
        public void paintSelectedContent(Graphics g, BaseUI parent, Time time, int focus) {
            TimeBasedRestrictionsUI ui = (TimeBasedRestrictionsUI) parent;
            if (!isFirstLine) {
                g.translate(0, 176);
            }

            // field names
            g.setFont(f17);
            g.setColor(nameColor);
            if (focus == 1 || focus == 2 || focus == 3) {
                g.drawString(startTimeStr, 92, 221);
            } else if (focus == 4 || focus == 5 || focus == 6) {
                g.drawString(endTimeStr, 520, 221);
            } else if (focus == 7) {
                g.drawString(dayStr, 92, 261);
            } else if (focus == 8) {
                g.drawString(repeatStr, 520, 261);
            }

            g.setFont(f18);
            g.setColor(focusNameColor);
            if (focus == 0) {
                g.drawImage(inputDimImg, 224, 143, parent);
                String value = BaseUI.getOptionText(time.getActive() + "d");
                g.drawString(value, 238, 164);
            }

            if (focus == 7) {
                g.drawImage(inputDimImg, 224, 241, parent);
                g.drawString(ui.getDaysText(time), 238, 262);
            }
            if (focus == 8) {
                g.drawImage(inputDimImg, 652, 241, parent);
                if (time.getRepeat().equals(Definitions.OPTION_VALUE_YES)) {
                    g.drawString(everyWeekStr, 666, 262);
                } else {
                    g.drawString(noneStr, 666, 262);
                }
            }
            if (!isFirstLine) {
                g.translate(0, -176);
            }
        }
    }
}

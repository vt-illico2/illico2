/**
 * @(#)PinEnablerRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.PinEnablerUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The PinEnablerRenderer class render PinEnablerUI.
 * @author Woojung Kim
 * @version 1.1
 */
public class PinEnablerRenderer extends PopBaseRenderer {

    /** The gap of each pin box. */
    private static final int PIN_GAP = 38;
    /** The gap of each stars. */
    private static final int STAR_GAP = PIN_GAP;

    /** The image of pin box. */
    private Image pinBoxImage = dataCenter.getImage("05_pin_box.png");
    /** The image of pin focus. */
    private Image pinFocusImage = dataCenter.getImage("05_pin_focus.png");
    /** The image of button when not selected. */
    private Image btnDimImage = dataCenter.getImage("05_focus_dim.png");
    /** The image of button when selected. */
    private Image btnFocusImage = dataCenter.getImage("05_focus.png");
    /** The image of button when can't select. */
    private Image btnOffImage = dataCenter.getImage("05_focus_off.png");
    /** The Image of focused button for forgot pin code. */
    private Image btnForgotFocusImg = dataCenter.getImage("05_btn_152_f.png");
    /** The Image of dimmed button for forgot pin code. */
    private Image btnForgotDimImg = dataCenter.getImage("05_btn_152.png");
    /** a image of red icon. */
    private Image iconRedXImg = dataCenter.getImage("icon_r_x__small.png");
    /** a image of backgournd. */
//    private Image bgImg = dataCenter.getImage("05_pop_glow_353.png");
    /** a image of shadow. */
//    private Image shadowImg = dataCenter.getImage("pop_sha.png");
    /** a image of high. */
//    private Image highImg = dataCenter.getImage("pop_high_402.png");
    /** a image of gap. */
    private Image gapImg = dataCenter.getImage("pop_gap_379.png");
    /** a image of halo. */
//    private Image haloImg = dataCenter.getImage("pop_red_478.png");
    /** a image of big halo. */
//    private Image halo2Img = dataCenter.getImage("pop_red_538.png");

    /** The color of explain strings. */
    private Color starDimColor = new Color(181, 181, 181);
    /** a color of forgot string. */
    private Color forgotColor = new Color(240, 240, 240);
    /** a color of red string. */
    private Color redColor = new Color(248, 63, 63);

    /** The font that use to paint a title. */
    private Font f24 = FontResource.BLENDER.getFont(24);

    /** a string array for ask. */
    private String[] ask;
    /** a string array for pin lock. */
    private String[] pinLock;
    /** a string array for explain */
    private String[] explain;
    /** a string arrary for explain retry */
    private String[] explainRetry;
    /** a string for explain how to change. */
    private String[] explainPinForget;
    /** a string for wrong pin code. */
    private String wrongPinCode;
    /** a string of buttons. */
    private String btnClear, btnCancel, btnOK, btnForgot;
    /** a boolean to indicate a language. */
    private boolean isEnglish;

    /**
     * Paint a background and pin box and text for PinEnablerUI.
     * @see com.videotron.tvi.illico.framework.Renderer#paint(Graphics,
     *      UIComponent)
     * @param g The Graphics to paint.
     * @param c The UIComponent that handle a PinEnablerRenerer.
     */
    protected void paint(Graphics g, UIComponent c) {
    	/**
    	 * R7.4 Blocked Admin PIN
    	 */        
        PinEnablerUI ui = (PinEnablerUI) c;
        if (ui.getState() != PinEnablerUI.STATE_PIN_CODE_LOCK) {
            g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
//            g.drawImage(bgImg, 250, 95, c);
//            g.drawImage(shadowImg, 280, 414, 402, 79, c);
            g.setColor(fillRectColor);
            g.fillRect(280, 125, 402, 295);
//            g.drawImage(highImg, 280, 125, c);
            g.drawImage(gapImg, 292, 163, c);

            g.setColor(titleColor);
            g.setFont(f24);

            int x = GraphicUtil.drawStringCenter(g, title, 484, 151);

            if (iconType != ICON_TYPE_NONE) {
            	iconGap = currentIconImg.getWidth(c) + 7;            	
                g.drawImage(currentIconImg, x - iconGap, 145 - currentIconImg.getHeight(c) / 2, c); 
            }
            
            g.setFont(f18);
            g.setColor(Color.WHITE);

            int count = ui.getPinString().length();
            int gapY = 270;
                
            int posY = 225;
            
            if (ui.getState() == PinEnablerUI.STATE_INPUT_WRONG)
            	explain = explainRetry;
            
            if (explain != null) {
            	
            	if (ui.getState() != PinEnablerUI.STATE_INPUT) {
            		posY -= 33;
            		
            		if (explain.length == 4 && ui.getState() == PinEnablerUI.STATE_INPUT_LIMIT) {
            			posY -= 6;
            		}
            	} else {
            		if (explain.length == 2)
                		posY -= 11;
                	else if (explain.length == 3)
                		posY -= 22;
                	else if (explain.length == 4) {
                		posY -= 33;                		
                	}
            	}
                
                for (int i = 0; i < explain.length; i++) {
                    GraphicUtil.drawStringCenter(g, explain[i], 482, posY + i * 22);
                }
            }

            if (ui.getState() != PinEnablerUI.STATE_INPUT) {
            	posY += 65;
            	if (explain.length == 4 && ui.getState() == PinEnablerUI.STATE_INPUT_LIMIT)
            		posY += 20;
                g.setColor(redColor);
                
                if ((5 - ui.tryTime) > 1)
                	GraphicUtil.drawStringCenter(g, String.valueOf(5-ui.tryTime)+" " 
                				+ BaseUI.getPopupText(KeyNames.EXPLAIN_PIN_REMAINS), 482, posY);
                else
                	GraphicUtil.drawStringCenter(g, String.valueOf(5-ui.tryTime)+" "
                				+ BaseUI.getPopupText(KeyNames.EXPLAIN_PIN_REMAIN), 482, posY);                	
            }
                                      
            gapY = 280;
            
            g.setFont(f24);
            g.setColor(Color.WHITE);
            for (int i = 0; i < 4; i++) {
                int step = i * PIN_GAP;
                if (ui.getFocus() == 0 && count == i) {
                    g.drawImage(pinFocusImage, 408 + step, gapY, c);
                } else {
                    g.drawImage(pinBoxImage, 408 + step, gapY, c);
                }

                if (count > i) {
                    GraphicUtil.drawStringCenter(g, "*", 425 + step, gapY + 27);
                }
            }

            if (ui.getFocus() == 1) {
                if (count == 0) {
                    g.drawImage(btnOffImage, 321, 373, c);
                } else {
                    g.drawImage(btnFocusImage, 321, 373, c);
                }
                g.drawImage(btnDimImage, 485, 373, c);
            } else if (ui.getFocus() == 2) {
                if (count == 0) {
                    g.drawImage(btnOffImage, 321, 373, c);
                } else {
                    g.drawImage(btnDimImage, 321, 373, c);
                }
                g.drawImage(btnFocusImage, 485, 373, c);
            } else {
                if (count == 0) {
                    g.drawImage(btnOffImage, 321, 373, c);
                } else {
                    g.drawImage(btnDimImage, 321, 373, c);
                }
                g.drawImage(btnDimImage, 485, 373, c);
            }
            g.setFont(f18);
            g.setColor(btnColor);
            GraphicUtil.drawStringCenter(g, btnCancel, 564, 394);

            if (count == 0) {
                g.setColor(btnOffShColor);
                GraphicUtil.drawStringCenter(g, btnClear, 400, 394);
                g.setColor(btnOffColor);
                GraphicUtil.drawStringCenter(g, btnClear, 399, 394);
            } else {
                GraphicUtil.drawStringCenter(g, btnClear, 399, 394);
            }
            
            gapY = 305;
            g.setColor(Color.GRAY);
            if (explainPinForget != null && ui.getState() != PinEnablerUI.STATE_INPUT) {                   
                for (int i = 0; i < explainPinForget.length; i++) {
                	gapY = i * 18;
                    GraphicUtil.drawStringCenter(g, explainPinForget[i], 484, 334 + gapY);
                }
            }

        } else if (ui.getState() == PinEnablerUI.STATE_PIN_CODE_LOCK) {
        	
        	//super.paint(g, c);
        	g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

            g.setColor(fillRectColor);
            g.fillRect(306-10, 143, 350+20, 224);
            g.drawImage(gapImg, 285, 181, c);

            g.setColor(titleColor);
            g.setFont(f24);
            int x = 0;
            if (iconType == ICON_TYPE_NONE) {
                iconGap = 0;
                x = GraphicUtil.drawStringCenter(g, title, 480, 169);
            } else {
                iconGap = currentIconImg.getWidth(c) + 7;
                x = GraphicUtil.drawStringCenter(g, title, 494, 169);
            }

            if (iconType != ICON_TYPE_NONE) {
                g.drawImage(currentIconImg, x - iconGap, 163 - currentIconImg.getHeight(c) / 2, c); // 149 ~ 152
            }
            
        	
        	g.setColor(titleColor);
            g.setFont(f24);
        	GraphicUtil.drawStringCenter(g, title, 494, 169);
        	
            g.setFont(f18);
            g.setColor(Color.WHITE);
                        
            for (int i = 0; pinLock != null && i < pinLock.length; i++) {
                GraphicUtil.drawStringCenter(g, pinLock[i], 480, 222 + i * 19);
            }
            g.drawImage(btnFocusImage, 403, 318, c);
            g.setColor(btnColor);
            GraphicUtil.drawStringCenter(g, btnOK, 481, 339);            
        }
    }

    /**
     * Prepare a something before to paint.
     * @see com.videotron.tvi.illico.framework.Renderer#prepare(UIComponent)
     * @param c The UIComponent that handle a PinEnablerRenderer.
     */
    public void prepare(UIComponent c) {
    	/**
    	 * R7.4 Blocked Admin PIN
    	 */
        PinEnablerUI ui = (PinEnablerUI) c;
        String tempAsk = BaseUI.getPopupText(KeyNames.EXPLAIN_FORGOT_PIN);
        ask = TextUtil.split(tempAsk, fm18, 310);

        String lock = BaseUI.getPopupText(KeyNames.EXPLAIN_PIN_ERROR);
        pinLock = TextUtil.split(lock, fm18, 340, '|');

        explain = ui.getExplain();
        String tempExplain = BaseUI.getPopupText(KeyNames.EXPLAIN_RETRY);
        explainRetry = TextUtil.split(tempExplain, fm18, 360, "|");        
        tempExplain = BaseUI.getPopupText(KeyNames.EXPLAIN_PIN_FORGET);
        explainPinForget = TextUtil.split(tempExplain, fm18, 360, "|");
        
        wrongPinCode = BaseUI.getPopupText(KeyNames.EXPLAIN_RETRY);

        btnClear = BaseUI.getPopupText(KeyNames.BTN_CLEAR);
        btnCancel = BaseUI.getPopupText(KeyNames.BTN_CANCEL);
        btnOK = BaseUI.getPopupText(KeyNames.BTN_OK);
        btnForgot = BaseUI.getPopupText(KeyNames.BTN_FORGOT);

        String language = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE).getCurrentValue();

        if (language.equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }
    }
}

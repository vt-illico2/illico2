/**
 * @(#)TVPictureFormatRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.TVPictureFormatUI;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a ui for TV picture format.
 * @author Woojung Kim
 * @version 1.1
 */
public class TVPictureFormatRenderer extends NormalSettingsBackRenderer {
    /** a image for number 1. */
    private Image num1Img;
    /** a image for number 2. */
    private Image num2Img;
    /** a image for number 3. */
    private Image num3Img;
    /** a image for number 1 to have a focus. */
    private Image num1FocusImg;
    /** a image for number 2 to have a focus. */
    private Image num2FocusImg;
    /** a image for number 3 to have a focus. */
    private Image num3FocusImg;
    /** a image for number 3 to be dimmed. */
    private Image num3DimImg;
    /** a image for line. */
    private Image lineImg;
    /** a image for dimmed input. */
    private Image inputDimImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed input to have a focus. */
    private Image inputFocusDimImg;
    /** a image for button to have a focus. */
    private Image btnFocusImg;
    /** a image for dimmed button. */
    private Image btnDimImg;
    /** a image for off button. */
    private Image btnOffImg;
    /** a image for preview button. */
    private Image previewBtnImg;
    /** a image for preview button to have a focus. */
    private Image previewBtnFocusImg;
    /** a image for colon. */
    private Image colonImg;
    /** a image for preview background. */
    private Image previewBgImg;
    /** a font for size 14. */
    private Font f14 = FontResource.BLENDER.getFont(14);
    /** a font for size 22. */
    private Font f22 = FontResource.BLENDER.getFont(22);
    /** a font for size 30. */
    private Font f30 = FontResource.BLENDER.getFont(30);
    /** a font metrics for font 14. */
    private FontMetrics fm14 = FontResource.BLENDER.getFontMetrics(f14);
    /** a font metrics for font 20. */
    private FontMetrics fm20 = FontResource.BLENDER.getFontMetrics(f20);

    /** a color for dimmed. */
    private Color dimmedColor = new Color(110, 110, 110);
    /** a color for explain. */
    private Color explainColor = new Color(233, 233, 233);
    /** a color for button. */
    private Color btnColor = new Color(3, 3, 3);
    /** a color preview. */
    private Color previewColor = new Color(252, 202, 4);
    /** a string for choose format. */
    private String strChooseFormat;
    /** a string for Generate preview. */
    private String strGeneratePreview;
    /** a string for Apply format. */
    private String strApplyFormat;
    /** a string for Preview. */
    private String strPreview;
    /** a string for Apply . */
    private String strApply;
    /** a string array for Generate Preview Explain. */
    private String[] generatePreviewExplain;
    /** a string for OK. */
    private String okStr;
    /** a string for Cancel. */
    private String cancelStr;
    /** a string for Preview explain. */
    private String previewExplainStr;
    /** a string for current format. */
    private String currentFormatStr;
    /** a integer for choose format length. */
    private int chooseFormatLength;

    /**
     * Prepare a Renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
    	TVPictureFormatUI ui = (TVPictureFormatUI) c;
    	
        strChooseFormat = BaseUI.getMenuText(KeyNames.SUBTITLE_CHOOSE_FORMAT);
        chooseFormatLength = fm20.stringWidth(strChooseFormat);
        strGeneratePreview = BaseUI.getMenuText(KeyNames.SUBTITLE_GENERATE_PREVIEW);
        strApplyFormat = BaseUI.getMenuText(KeyNames.SUBTITLE_APPLY_FORMAT);
        strPreview = BaseUI.getMenuText(KeyNames.BTN_PREVIEW);
        strApply = BaseUI.getMenuText(KeyNames.BTN_APPLY);
        String data = BaseUI.getExplainText(KeyNames.EXPLAIN_GENERATE_PREVIEW);
        generatePreviewExplain = TextUtil.split(data, fm14, 830, '|');

        okStr = BaseUI.getPopupText(KeyNames.BTN_NOW);
        cancelStr = BaseUI.getPopupText(KeyNames.BTN_CANCEL);
        previewExplainStr = BaseUI.getExplainText(KeyNames.EXPLAIN_PREVIEW);

        
        Preference parentPre = (Preference) BaseUI.peekHistory();
        
        //R7.4 freelife VDTRMASTER-6166
        //currentFormatStr = "(" + BaseUI.getMenuText(KeyNames.CURRENT_FORMAT) + ": " + parentPre.getCurrentValue() + ")";                        
        currentFormatStr = "(" + BaseUI.getMenuText(KeyNames.CURRENT_FORMAT) + ": ";
        currentFormatStr += ui.getCurrentValueMsg(parentPre.getCurrentValue()) + ")";
        //<--
        num1Img = BaseUI.getImage("08_vset_num1.png");
        num2Img = BaseUI.getImage("08_vset_num2.png");
        num3Img = BaseUI.getImage("08_vset_num3.png");
        num1FocusImg = BaseUI.getImage("08_vset_num1_foc.png");
        num2FocusImg = BaseUI.getImage("08_vset_num2_foc.png");
        num3FocusImg = BaseUI.getImage("08_vset_num3_foc.png");
        num3DimImg = BaseUI.getImage("08_vset_num3_dim.png");
        lineImg = BaseUI.getImage("08_bg_line.png");
        inputDimImg = BaseUI.getImage("input_210.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        btnFocusImg = BaseUI.getImage("08_vset_foc.png");
        btnDimImg = BaseUI.getImage("08_vset_dim.png");
        btnOffImg = BaseUI.getImage("08_vset_off.png");

        previewBtnImg = BaseUI.getImage("05_focus_dim.png");
        previewBtnFocusImg = BaseUI.getImage("05_focus.png");
        colonImg = BaseUI.getImage("08_tv_colon.png");
        previewBgImg = BaseUI.getImage("08_tv_prebg.png");

        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        TVPictureFormatUI ui = (TVPictureFormatUI) c;

        if (ui.getState() == TVPictureFormatUI.STATE_SELECT) {
        	
            super.paint(g, c);

            g.drawImage(lineImg, 0, 228, c);
            g.drawImage(lineImg, 0, 360, c);

            g.setColor(Color.WHITE);
            g.setFont(f26);
            Preference parentPre = (Preference) BaseUI.peekHistory();
            g.drawString(parentPre.getName(), 55, 113);

            if (generatePreviewExplain != null) {
                g.setFont(f14);
                g.setColor(explainColor);
                for (int i = 0; i < generatePreviewExplain.length; i++) {
                    g.drawString(generatePreviewExplain[i], 81, 279 + i * 16);
                }
            }

            g.setFont(f20);

            g.drawString(strChooseFormat, 81, 162);
            g.drawString(strGeneratePreview, 81, 260);

            if (ui.canSelect()) {
                g.setColor(Color.WHITE);
            } else {
                g.setColor(dimmedColor);
            }
            g.drawString(strApplyFormat, 81, 391);

            g.setFont(f18);

            g.setColor(dimmedNameColor);
            g.drawString(currentFormatStr, 81 + chooseFormatLength + 10, 162);

            if (c.getFocus() == 0) {
                g.drawImage(num1FocusImg, 53, 148, c);
                g.drawImage(inputFocusImg, 81, 178, c);
                g.setColor(Color.WHITE);
            } else {
                g.drawImage(num1Img, 51, 145, c);
                g.drawImage(inputDimImg, 81, 178, c);
                g.setColor(dimmedColor);
            }
            g.drawString(ui.getCurrentValue(), 94, 199);

            g.setColor(btnColor);
            if (c.getFocus() == 1) {
                g.drawImage(num2FocusImg, 53, 245, c);
                g.drawImage(btnFocusImg, 80, 310, c);
            } else {
                g.drawImage(num2Img, 51, 243, c);
                g.drawImage(btnDimImg, 80, 310, c);
            }
            GraphicUtil.drawStringCenter(g, strPreview, 186, 331);

            if (ui.canSelect() && c.getFocus() == 2) {
                g.drawImage(num3FocusImg, 53, 377, c);
                g.drawImage(btnFocusImg, 80, 407, c);
                GraphicUtil.drawStringCenter(g, strApply, 186, 428);
            } else if (ui.canSelect()) {
                g.drawImage(num3Img, 52, 375, c);
                g.drawImage(btnDimImg, 80, 407, c);
                GraphicUtil.drawStringCenter(g, strApply, 186, 428);
            } else {
                g.setColor(dimmedColor);
                g.drawImage(num3DimImg, 52, 375, c);
                g.drawImage(btnOffImg, 80, 407, c);

                g.setColor(btnOffShColor);
                GraphicUtil.drawStringCenter(g, strApply, 187, 429);
                g.setColor(btnOffColor);
                GraphicUtil.drawStringCenter(g, strApply, 186, 428);
            }
        } else {
            g.drawImage(previewBgImg, 116, 424, c);
            g.drawImage(colonImg, 146, 462, c);

            g.setColor(previewColor);
            // time
            g.setFont(f30);
            g.drawString(df2.format(ui.getRemainTime()), 155, 477);

            // preview
            g.setFont(f20);
            g.drawString(strPreview, 212, 463);

            // explain
            g.setFont(f18);
            g.setColor(titleColor);
            g.drawString(previewExplainStr, 212, 482);

            // button
            g.setColor(btnColor);
            if (ui.getPreviewFocus() == 0) {
                g.drawImage(previewBtnFocusImg, 497, 452, c);
                g.drawImage(previewBtnImg, 661, 452, c);
            } else {
                g.drawImage(previewBtnImg, 497, 452, c);
                g.drawImage(previewBtnFocusImg, 661, 452, c);
            }
            GraphicUtil.drawStringCenter(g, okStr, 575, 473);
            GraphicUtil.drawStringCenter(g, cancelStr, 740, 473);
        }

    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        TVPictureFormatUI ui = (TVPictureFormatUI) parent;

        if (ui.getFocus() == 0) {
            g.setFont(f18);
            g.drawImage(inputFocusDimImg, 81, 178, ui);
            g.setColor(Color.WHITE);
            g.drawString(ui.getCurrentValue(), 94, 199);
        }
    }
}

/**
 * @(#)BaseRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * The BaseRenderer is a parent class of all renderer.
 * @author Woojung Kim
 * @version 1.1
 */
public class BaseRenderer extends Renderer {
    /** a instance of DataCenter. */
    protected DataCenter dataCenter = DataCenter.getInstance();
    /** a image of background. */
    public Image bgImg;
    /** a image of clock. */
    protected Image clockImg = dataCenter.getImage("clock.png");
    /** a image of clock background. */
//    protected Image clockBgImg = dataCenter.getImage("clock_bg.png");
    /** a image of history dimmed. */
    protected Image hisDimImg = dataCenter.getImage("his_dim.png");
    /** a image of histroy over. */
    protected Image hisOverImg = dataCenter.getImage("his_over.png");
    /** a logo of illico service. */
    protected Image logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);

    /** a font of size 17. */
    protected Font f17 = FontResource.BLENDER.getFont(17);
    /** a font of size 18. */
    protected static Font f18 = FontResource.BLENDER.getFont(18);
    /** a font of size 20. */
    protected Font f20 = FontResource.BLENDER.getFont(20);
    /** a font of size 26. */
    protected static Font f26 = FontResource.BLENDER.getFont(26);
    /** a font metrics for font 17. */
    protected FontMetrics fm17 = FontResource.BLENDER.getFontMetrics(f17);
    /** a font metrics for font 18. */
    public static FontMetrics fm18 = FontResource.BLENDER.getFontMetrics(f18);
    /** a font metrics for font 26. */
    public static FontMetrics fm26 = FontResource.BLENDER.getFontMetrics(f26);
    /** a color of 239, 239, 239. */
    protected Color hisOverColor = new Color(239, 239, 239);
    /** a color of 124, 124, 124. */
    protected Color hisDimColor = new Color(124, 124, 124);
    /** a color for title (255, 255, 255). */
    protected Color titleColor = new Color(255, 255, 255);
    /** a color for name (210, 210, 210). */
    protected Color nameColor = new Color(210, 210, 210);
    /** a color for dimmed name (182, 182, 182). */
    protected Color dimmedNameColor = new Color(182, 182, 182);
    /** a color for name to have a focus (240, 240, 240). */
    protected Color focusNameColor = new Color(240, 240, 240);
    /** a color for name to have a focus (3, 3, 3). */
    protected Color focusName2Color = new Color(3, 3, 3);
    /** a color of off (90, 90, 90). */
    protected Color offColor = new Color(90, 90, 90);
    /** a color of item (180, 180, 180). */
    protected Color itemColor = new Color(180, 180, 180);
    /** a color of item to have a focus (0, 0, 0). */
    protected Color itemFocusColor = new Color(0, 0, 0);
    /** a color of font background (46, 46, 46). */
    protected Color fontBgColor = new Color(46, 46, 46);
    /** a color of font foreground (214, 182, 55). */
    protected Color fontFgColor = new Color(214, 182, 55);
    /** a color of button is off (50, 50, 50). */
    protected Color btnOffColor = new Color(50, 50, 50);
    /** a color of button shadow (110, 110, 110). */
    protected Color btnOffShColor = new Color(110, 110, 110);
    /** a color of overlap (29, 29, 29, 217). */
    protected DVBColor overlapColor = new DVBColor(29, 29, 29, 217);
    /** The color to be dimmed a PinEnablerUI's back. */
    protected Color dimmedColor = new DVBColor(0, 0, 0, 204);

    /** Constant for x coordinate of name. */
    public static final int NAME_X = 56;
    /** Constant for x coordinate of input. */
    public static final int INPUT_X = 339;
    /** Constant for x coordinate of value. */
    public static final int VALUE_X = 353;

    /** a decimal format for XXX. */
    public static DecimalFormat df = new DecimalFormat("000");
    /** a decima format for XX. */
    public static DecimalFormat df2 = new DecimalFormat("00");

    /**
     * Get a bounds for renderer.
     * @param c parent UI.
     * @return bound of renderer.
     * @see Renderer#getPreferredBounds
     *      (com.videotron.tvi.illico.framework.UIComponent)
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    /**
     * Set a image for background.
     * @param bg background image.
     */
    protected void setBackground(Image bg) {
        bgImg = bg;
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     * @see Renderer#paint(Graphics,
     *      UIComponent)
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);

        logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        if (logoImg == null) {
            if (Log.INFO_ON) {
                Log.printInfo("log image from main menu via SharedMemory is null.");
            }
            logoImg = dataCenter.getImage("logo_en.png");
        }
        g.drawImage(logoImg, 53, 21, c);
        g.setFont(f17);
        g.setColor(Color.WHITE);
        String clock = ((BaseUI) c).getClockString();
        if (clock != null) {
//            g.drawImage(clockBgImg, 749, 37, c);
            GraphicUtil.drawStringRight(g, clock, 910, 57);
            g.drawImage(clockImg, 910 - fm17.stringWidth(clock) - 24, 44, c);
        }

        paintHistory(g, c);
    }

    /**
     * Paint a breadcrumbs.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    private void paintHistory(Graphics g, UIComponent c) {
        String[] names = BaseUI.getHistory();

        int stepX = 68 + logoImg.getWidth(c);
        g.setFont(f18);

        if (names.length > 2) {

            g.setColor(hisDimColor);
            g.drawImage(hisDimImg, stepX, 45, c);
            g.drawString(names[0], stepX + 16, 57);

            stepX = stepX + 16 + fm18.stringWidth(names[0]) + 16;

            int overCount = names.length - 2;
            int step = 0;
            for (int i = 0; i < overCount; i++) {
                step = i * 6;
                g.drawImage(hisOverImg, stepX + step, 45, c);
            }

            g.setColor(hisOverColor);
            g.drawString(names[names.length - 2], stepX + step + 16, 57);
        } else {
            g.setColor(hisOverColor);
            g.drawImage(hisOverImg, stepX, 45, c);
            g.drawString(names[0], stepX + 16, 57);
        }
    }

    /**
     * Paint a selected content when display a select popup.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
    }

    /**
     * Paint a decoration to use when paint a animation.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintDecoOfList(Graphics g, BaseUI parent) {

    }

    /**
     * Prepare a renderer.
     * @param c The component to get a renderer.
     * @see Renderer#prepare(UIComponent)
     */
    public void prepare(UIComponent c) {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

}

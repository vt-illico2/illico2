/**
 * @(#)MenuRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.MenuUI;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class render a menu for Settings.
 * @author Woojung Kim
 * @version 1.1
 */
public class MenuRenderer extends BaseRenderer {

    /** a bounds. */
    private Rectangle bounds = new Rectangle(52, 71, 298, 469);
    /** a image for icon of Settings. */
    private Image mySettingsImg;
    /** a image for focus. */
    private Image focusImg;
    /** a image for bullet. */
    private Image bulletImg;
    /** a color of focus (3, 3, 3) . */
    private Color focusColor = new Color(3, 3, 3);
    /** a color of dimmed color (230, 230, 230). */
    private Color dimmedColor = new Color(230, 230, 230);
    /** a color of title (202, 174, 97). */
    private Color titleColor = new Color(202, 174, 97);
    /** a color of title shadow. (46, 46, 46). */
    private Color titleShColor = new Color(46, 46, 45);
    /** a font of size 20. */
    private Font f20 = FontResource.BLENDER.getFont(20);
    /** a font of size 21. */
    private Font f21 = FontResource.BLENDER.getFont(21);

    /** a string for title. */
    private String titleString;

    public void prepare(UIComponent c) {
        mySettingsImg = BaseUI.getImage("08_main_icon.png");
        focusImg = BaseUI.getImage("00_menufocus.png");
        bulletImg = BaseUI.getImage("01_bullet01.png");
        super.prepare(c);
    }
    
    /**
     * Get a bounds.
     * @param c parent UI.
     * @return bounds.
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Update a title.
     */
    public void updateTitle() {
        String[] histories = BaseUI.getHistory();
        titleString = histories[histories.length - 1];
    }

    /**
     * Paint a renderer.
     * @param Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        g.translate(-bounds.x, -bounds.y);

        MenuUI ui = (MenuUI) c;

        // g.drawImage(bgImg, 52, 71, 298, 408, c);
        g.drawImage(mySettingsImg, 58, 76, c);

        g.setFont(f20);
        g.setColor(titleShColor);
        g.drawString(titleString, 92, 95);
        g.setColor(titleColor);
        g.drawString(titleString, 91, 94);

        Category[] categoies = ui.getCategories();

        if (categoies != null) {
            if (ui.isVisible()) {
                for (int i = 0; i < categoies.length; i++) {
                    int step = i * 34;
                    if (i == c.getFocus()) {
                        g.setFont(f21);
                        g.setColor(focusColor);
                        g.drawImage(focusImg, 49, 102 + step, c);

                    } else {
                        g.setFont(f18);
                        g.setColor(dimmedColor);
                        g.drawImage(bulletImg, 72, 119 + step, c);
                    }

                    g.drawString(categoies[i].getName(), 95, 128 + step);
                }
            } else {
                for (int i = 0; i < categoies.length; i++) {
                    int step = i * 34;

                    g.setFont(f18);
                    g.setColor(dimmedColor);
                    g.drawImage(bulletImg, 72, 119 + step, c);
                    g.drawString(categoies[i].getName(), 95, 128 + step);
                }
            }
        }

        g.translate(+bounds.x, +bounds.y);
    }
}

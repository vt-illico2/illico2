/**
 * @(#)MenuInfoRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.MenuInfoUI;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class render a menu information in Settings's main menu.
 * @author Woojung Kim
 * @version 1.1
 */
public class MenuInfoRenderer extends BaseRenderer {

    /** a bounds of renderer. */
    private Rectangle bounds = new Rectangle(321, 69, 639, 407);

    /** a image for icon background. */
    private Image iconBgImg = dataCenter.getImage("01_tooliconbg.png");
    /** a image for block icon. */
    private Image blockIconImg = dataCenter.getImage("01_blockicon.png");
    /** a image for option icon. */
    private Image optionIconImg = dataCenter.getImage("01_optionicon.png");
    /** a image for admin background. */
    private Image adminBgImg = dataCenter.getImage("08_admin_bg.png");
    /** a color for explain text. */
    private Color explainsColor = new Color(221, 221, 221);
    /** a color for title . */
    private Color titleColor = new Color(187, 189, 192);
    /** a font of size 28. */
    private Font f28 = FontResource.BLENDER.getFont(28);
    /** a font of size 19. */
    private Font f19 = FontResource.BLENDER.getFont(19);

    /** a string for title. */
    private String title;
    /** a string array for explain texts. */
    private String[] explains;
    /** a string for welcome message of admin. */
    private String welcomAdmin;
    /** a boolean whether User is admin or not. */
    private boolean isAdmin;

    /**
     * Get a bounds for rederer.
     * @param c parent UI
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        MenuInfoUI ui = (MenuInfoUI) c;
        title = ui.getTitle();
        explains = ui.getExplains();
        isAdmin = ui.isAdministrator();
        welcomAdmin = BaseUI.getMenuText(KeyNames.WELCOME_ADMINISTRATOR);
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        g.translate(-bounds.x, -bounds.y);
        MenuInfoUI ui = (MenuInfoUI) c;
        g.drawImage(iconBgImg, 483, 119, c);

        if (ui.getCurrentId().equals("modify.parental.control") || ui.getCurrentId().equals("modify.admin.pin.code")
                || ui.getCurrentId().equals("family.pin.code")) {
            g.drawImage(blockIconImg, 535, 168, c);

        } else {
            g.drawImage(optionIconImg, 535, 168, c);
        }

        g.setFont(f28);
        g.setColor(titleColor);
        if (title != null) {
            g.drawString(title, 384, 98);
        }

        if (explains != null) {
            g.setFont(f19);
            g.setColor(explainsColor);
            for (int i = 0; i < explains.length; i++) {
                int step = i * 20;
                //GraphicUtil.drawStringCenter(g, explains[i], 615, 375 + step);
                g.drawString(explains[i], 416, 370 + step);
            }
        }

        if (isAdmin) {
            g.setFont(f18);
            g.setColor(explainsColor);
            g.drawImage(adminBgImg, 716, 69, ui.getParent());
            GraphicUtil.drawStringRight(g, welcomAdmin, 909, 95);
        }

        g.translate(bounds.x, bounds.y);
    }
}

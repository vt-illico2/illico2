/**
 * @(#)VODDetailRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.VODUI;

/**
 * This class render a preferences for VOD.
 * @author Woojung Kim
 * @version 1.1
 */
public class VODDetailRenderer extends NormalSettingsBackRenderer {

    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed input to have a focus. */
    private Image inputFocusDimImg;
    /** a string array for preference id's. */
    private String[] preferenceIds;
    /** a string for title. */
    private String title;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        VODUI ui = (VODUI) c;
        preferenceIds = ui.preferenceIds;
        Category category = (Category) BaseUI.peekHistory();
        title = BaseUI.getMenuText(category.getCategoryId());

        inputImg = dataCenter.getImage("input_210_sc.png");
        inputFocusImg = dataCenter.getImage("input_210_foc2.png");
        inputFocusDimImg = dataCenter.getImage("input_210_foc_dim.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        VODUI ui = (VODUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        // Category category = (Category) BaseUI.peekHistory();
        // g.drawString(category.getName(), 54, 116);
        g.drawString(title, 55, 113);

        for (int i = 0; i < preferenceIds.length; i++) {
            int step = i * 37;
            Preference pre = (Preference) preTable.get(preferenceIds[i]);
            g.setFont(f17);
            g.setColor(nameColor);
            if (pre != null) {
                String name = pre.getName();
                g.drawString(name, NAME_X, 161 + step);

                g.setFont(f18);
                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                } else {
                    g.setColor(dimmedNameColor);
                    g.drawImage(inputImg, INPUT_X, 141 + step, c);
                }
                String value = pre.getCurrentValue();
                value = BaseUI.getOptionText(value);
                if (value != null) {
                    g.drawString(value, VALUE_X, 162 + step);
                }
            }
        }
    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        VODUI ui = (VODUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = parent.getFocus();
        int step = idx * 37;
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            g.drawString(name, NAME_X, 161 + step);

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
            String value = pre.getCurrentValue();
            value = BaseUI.getOptionText(value);
            g.drawString(value, VALUE_X, 162 + step);
        }
    }
}

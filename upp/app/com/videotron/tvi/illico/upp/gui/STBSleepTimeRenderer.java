/**
 * @(#)STBSleepTimeRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.Time;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.STBSleepTimeUI;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class render a preferences for STB sleep time.
 * @author Woojung Kim
 * @version 1.1
 */
public class STBSleepTimeRenderer extends NormalSettingsBackRenderer {
    /** a image for background. */
    private Image bgImg;
    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for secondly input. */
    private Image input2Img;
    /** a image for secondly input to have a focus. */
    private Image inputFocus2Img;
    /** a image for dimmed input. */
    private Image inputDimImg;
    /** a image for small input. */
    private Image inputSmallImg;
    /** a image for small input to have a focus. */
    private Image inputSmallFocusImg;
    /** a time to have a information. */
    private Time time;
    /** a string for time. */
    private String timeStr;
    /** a string for start time. */
    private String startTimeStr;
    /** a string for day. */
    private String dayStr;
    /** a string for repeat. */
    private String repeatStr;
    /** a string for every week. */
    private String everyWeekStr;
    /** a string for none . */
    private String noneStr;
    /** a boolean to indicate whether language is english or not. */
    private boolean isEnglish;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        time = ((STBSleepTimeUI) c).getTime();

        timeStr = BaseUI.getMenuText(KeyNames.SLEEP_TIME);
        startTimeStr = BaseUI.getMenuText(KeyNames.START_TIME);
        dayStr = BaseUI.getMenuText(KeyNames.DAY);
        repeatStr = BaseUI.getMenuText(KeyNames.REPEAT);
        everyWeekStr = BaseUI.getPopupText(KeyNames.EVERY_WEEK);
        noneStr = BaseUI.getPopupText(KeyNames.NONE);

        bgImg = BaseUI.getImage("08_period_bg.png");
        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inputDimImg = BaseUI.getImage("input_210_foc_dim.png");
        inputSmallImg = BaseUI.getImage("input_38.png");
        inputSmallFocusImg = BaseUI.getImage("input_38_foc.png");

        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE);
        if (pre.getCurrentValue().equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        STBSleepTimeUI ui = (STBSleepTimeUI) c;

        g.setColor(titleColor);
        g.setFont(f26);
        Preference parentPre = (Preference) BaseUI.peekHistory();
        g.drawString(parentPre.getName(), 55, 113);

        g.drawImage(bgImg, 7, 136, c);
        g.setColor(dimmedNameColor);
        g.setFont(f20);
        GraphicUtil.drawStringCenter(g, ":", 269, 221);

        // field names
        g.setFont(f17);
        g.setColor(nameColor);
        g.drawString(startTimeStr, 92, 221);
        g.drawString(dayStr, 92, 261);
        g.drawString(repeatStr, 520, 261);

        g.setFont(f18);
        // start time
        int drawLength = 2;
        if (isEnglish) {
            drawLength = 3;
        }
        for (int i = 0; i < drawLength; i++) {
            int step = i * 52;
            if (c.getFocus() == i + 1) {
                g.drawImage(inputSmallFocusImg, 224 + step, 201, c);
                g.setColor(focusNameColor);
            } else {
                g.drawImage(inputSmallImg, 224 + step, 201, c);
                g.setColor(dimmedNameColor);
            }

            if (i + 1 == 1) { // Hour
                String timeValue;
                if (ui.isInputMode() && c.getFocus() == i + 1) {
                    timeValue = ui.getInputValue();
                } else {
                    timeValue = time.getStartHour();
                    if (isEnglish && timeValue.equals("00")) {
                        timeValue = "12";
                    } else if (!isEnglish && time.getStartAmPm().equals("pm")) {
                        int timeInt = Integer.parseInt(timeValue);
                        timeInt += 12;
                        timeValue = df2.format(timeInt);
                    }
                }

                GraphicUtil.drawStringCenter(g, timeValue, 243, 223);
            } else if (i + 1 == 2) { // Min
                if (ui.isInputMode() && c.getFocus() == i + 1) {
                    String inputValue = ui.getInputValue();
                    GraphicUtil.drawStringCenter(g, inputValue, 295, 223);
                } else {
                    GraphicUtil.drawStringCenter(g, time.getStartMin(), 295, 223);
                }
            } else { // AMPM
                GraphicUtil.drawStringCenter(g, time.getStartAmPm(), 347, 222);
            }
        }

        // day
        if (c.getFocus() == 4) {
            g.drawImage(inputFocus2Img, 224, 241, c);
            g.setColor(focusNameColor);
        } else {
            g.drawImage(input2Img, 224, 241, c);
            g.setColor(dimmedNameColor);
        }
        g.drawString(ui.getDaysText(), 238, 262);

        // repeat
        if (c.getFocus() == 5) {
            g.drawImage(inputFocusImg, 652, 241, c);
            g.setColor(focusNameColor);
        } else {
            g.drawImage(inputImg, 652, 241, c);
            g.setColor(dimmedNameColor);
        }
        if (time.getRepeat().equals(Definitions.OPTION_VALUE_YES)) {
            g.drawString(everyWeekStr, 666, 262);
        } else {
            g.drawString(noneStr, 666, 262);
        }

        if (time.getActive().equals(Definitions.OPTION_VALUE_DISABLE)) {
            g.setColor(overlapColor);
            g.fillRect(55, 137, 850, 156);
        }

        // Sleep time
        g.setColor(fontBgColor);
        g.drawString(timeStr, 74, 167);
        g.setColor(fontFgColor);
        g.drawString(timeStr, 73, 166);

        if (c.getFocus() == 0) {
            g.drawImage(inputFocusImg, 224, 143, c);
            g.setColor(focusNameColor);
        } else {
            g.drawImage(inputImg, 224, 143, c);
            g.setColor(dimmedNameColor);
        }
        String value = BaseUI.getOptionText(time.getActive() + "d");
        g.drawString(value, 238, 164);

    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        STBSleepTimeUI ui = (STBSleepTimeUI) parent;

        int idx = ui.getFocus();

        // field names
        g.setFont(f17);
        g.setColor(nameColor);
        if (idx == 1 || idx == 2 || idx == 3) {
            g.drawString(startTimeStr, 92, 221);
        } else if (idx == 4) {
            g.drawString(dayStr, 92, 261);
        } else if (idx == 5) {
            g.drawString(repeatStr, 520, 261);
        }

        g.setFont(f18);
        g.setColor(focusNameColor);
        if (idx == 0) {
            g.drawImage(inputDimImg, 224, 143, parent);
            String value = BaseUI.getOptionText(time.getActive() + "d");
            g.drawString(value, 238, 164);
        }

        if (idx == 4) {
            g.drawImage(inputDimImg, 224, 241, parent);
            g.drawString(ui.getDaysText(), 238, 262);
        }
        if (idx == 5) {
            g.drawImage(inputDimImg, 652, 241, parent);
            if (time.getRepeat().equals(Definitions.OPTION_VALUE_YES)) {
                g.drawString(everyWeekStr, 666, 262);
            } else {
                g.drawString(noneStr, 666, 262);
            }
        }

    }
}

/**
 * @(#)CallerIDRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.CallerIDUI;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a preferences for Caller ID.
 * @author Woojung Kim
 * @version 1.1
 */
public class CallerIDRenderer extends NormalSettingsBackRenderer {

    /** a image of input field. */
    private Image inputImg;
    /** a image of input field to have a focus. */
    private Image inputFocusImg;
    /** a image of secondly input field. */
    private Image input2Img;
    /** a image of secondly input field to have a focus. */
    private Image inputFocus2Img;
    /** a image of dimmed focus image. */
    private Image inputFocusDimImg;
    /** a image of input field to have a color as off. */
    private Image inputOffImg;
    /** a image of secondly input field to have a color as off. */
    private Image inputOff2Img;
    /** a image of line. */
    private Image lineImg;

    /** a preference ids of Caller ID. */
    private String[] preferenceIds;
    /** a string of title. */
    private String title;
    /** a string of none. */
    private String noneStr;
    /** a string of multi line. */
    private String multiLineStr;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        CallerIDUI ui = (CallerIDUI) c;
        preferenceIds = ui.preferenceIds;
        String[] history = BaseUI.getHistory();

        title = history[history.length - 1];

        noneStr = BaseUI.getMenuText(KeyNames.NONE);
        multiLineStr = BaseUI.getMenuText(KeyNames.MULTI_LINE);

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        inputOffImg = BaseUI.getImage("input_210_sc_dim.png");
        inputOff2Img = BaseUI.getImage("input_210_dim.png");
        lineImg = BaseUI.getImage("08_bg_line.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        CallerIDUI ui = (CallerIDUI) c;
        Hashtable preTable = ui.getPreferences();

        g.drawImage(lineImg, 0, 183, c);

        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, 55, 113);

        for (int i = 0; i < preferenceIds.length; i++) {

            Preference pre = (Preference) preTable.get(preferenceIds[i]);

            if (pre != null) {
                int step = 0;
                if (i == 0) {
                    step = i * 37;
                } else {
                    step = i * 37 + 18;
                }

                g.setFont(f17);
                if (i > 0 && !ui.canSelectMenu()) {
                    g.setColor(offColor);
                } else {
                    g.setColor(nameColor);
                }
                String name = pre.getName();
                g.drawString(name, NAME_X, 161 + step);

                String value = pre.getCurrentValue();
                value = BaseUI.getOptionText(value);

                g.setFont(f18);
                String[] lines = ui.getLines();
                if ((i != 1 && i == c.getFocus()) || (i == 1 && i == c.getFocus() && lines != null && lines.length > 1)) {
                    g.setColor(focusNameColor);
                    if (i == 1) {
                        g.drawImage(inputFocus2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                    }
                } else if (i == 1 && i == c.getFocus() && (lines == null || lines.length == 1)) {
                    g.setColor(focusNameColor);
                    g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, c);
                } else {
                    if ((i > 0 && !ui.canSelectMenu()) || (i == 1 && (lines == null || lines.length == 1))) {
                        g.setColor(offColor);
                        if (i == 1) {
                            g.drawImage(inputOff2Img, INPUT_X, 141 + step, c);
                        } else {
                            g.drawImage(inputOffImg, INPUT_X, 141 + step, c);
                        }
                    } else {
                        g.setColor(dimmedNameColor);
                        if (i == 1) {
                            g.drawImage(input2Img, INPUT_X, 141 + step, c);
                        } else {
                            g.drawImage(inputImg, INPUT_X, 141 + step, c);
                        }
                    }
                }

                if (i == 1 && ui.isMultiLine()) {
                    value = multiLineStr;
                }
                if (!value.equals(TextUtil.EMPTY_STRING)) {
                    g.drawString(value, VALUE_X, 162 + step);
                } else {
                    g.drawString(noneStr, VALUE_X, 162 + step);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.videotron.tvi.illico.upp.gui.BaseRenderer#paintSelectedContent(java.awt.Graphics,
     * com.videotron.tvi.illico.upp.ui.BaseUI)
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        CallerIDUI ui = (CallerIDUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = parent.getFocus();
        int step = 0;
        if (idx == 0) {
            step = idx * 37;
        } else {
            step = idx * 37 + 18;
        }
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            g.drawString(name, NAME_X, 161 + step);

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
            String value = pre.getCurrentValue();
            value = BaseUI.getOptionText(value);

            if (idx == 1 && ui.isMultiLine()) {
                value = multiLineStr;
            }

            if (!value.equals(TextUtil.EMPTY_STRING)) {
                g.drawString(value, VALUE_X, 162 + step);
            } else {
                g.drawString(noneStr, VALUE_X, 162 + step);
            }
        }
    }
}

/**
 * @(#)ClosedCaptioningRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ClosedCaptioningUI;

/**
 * This class render a preferences for Closed Captioning.
 * @author Woojung Kim
 * @version 1.1
 */
public class ClosedCaptioningRenderer extends NormalSettingsBackRenderer {

    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed input to have a focus. */
    private Image inputFocusDimImg;
    /** a image for off input. */
    private Image inputOffImg;
    /** a image for line. */
    private Image lineImg;
    /** a string array for preference ids. */
    private String[] preferenceIds;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        preferenceIds = ((ClosedCaptioningUI) c).preferenceIds;

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        inputOffImg = BaseUI.getImage("input_210_sc_dim.png");
        lineImg = BaseUI.getImage("08_bg_line.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        ClosedCaptioningUI ui = (ClosedCaptioningUI) c;
        Hashtable preTable = ui.getPreferences();

        g.drawImage(lineImg, 0, 167, c);

        g.setColor(titleColor);
        g.setFont(f26);
        Category category = (Category) BaseUI.peekHistory();
        g.drawString(category.getName(), 55, 113);

        for (int i = 0; i < preferenceIds.length; i++) {
            int step = 0;
            if (i == 0) {
                step = 0;
            } else {
                step = i * 37 + 14;
            }

            Preference pre = (Preference) preTable.get(preferenceIds[i]);

            if (pre != null) {
                g.setFont(f17);
                if (i > 1 && !ui.isSetByViewer()) {
                    g.setColor(offColor);
                } else if (i > 0 && !ui.canSelectMenu()) {
                    g.setColor(offColor);
                } else {
                    g.setColor(nameColor);
                }
                String name = pre.getName();
                if (i > 1) {
                    g.drawString(name, NAME_X + 23, 147 + step);
                } else {
                    g.drawString(name, NAME_X, 147 + step);
                }

                g.setFont(f18);
                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    g.drawImage(inputFocusImg, INPUT_X, 127 + step, c);
                } else {
                    if (i > 1 && !ui.isSetByViewer()) {
                        g.setColor(offColor);
                        g.drawImage(inputOffImg, INPUT_X, 127 + step, c);
                    } else if (i > 0 && !ui.canSelectMenu()) {
                        g.setColor(offColor);
                        g.drawImage(inputOffImg, INPUT_X, 127 + step, c);
                    } else {
                        g.setColor(dimmedNameColor);
                        g.drawImage(inputImg, INPUT_X, 127 + step, c);
                    }

                }
                String value = pre.getCurrentValue();
                if (i == 0) {
                    if (value.equals(Definitions.OPTION_VALUE_ON) || value.equals(Definitions.OPTION_VALUE_OFF)) {
                        value = BaseUI.getOptionText(value + "3");
                    } else {
                        value = BaseUI.getOptionText(value);
                    }
                } else {
                    value = BaseUI.getOptionText(value);
                }
                if (value != null) {
                    g.drawString(value, VALUE_X, 148 + step);
                }
            }
        }
    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        ClosedCaptioningUI ui = (ClosedCaptioningUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = parent.getFocus();
        int step = 0;
        if (idx == 0) {
            step = 0;
        } else {
            step = idx * 37 + 14;
        }
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            if (idx > 1) {
                g.drawString(name, NAME_X + 23, 147 + step);
            } else {
                g.drawString(name, NAME_X, 147 + step);
            }

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X, 127 + step, ui);
            String value = pre.getCurrentValue();
            if (ui.getFocus() == 0
                    && (value.equals(Definitions.OPTION_VALUE_ON) || value.equals(Definitions.OPTION_VALUE_OFF))) {
                value = BaseUI.getOptionText(value + "3");
            } else {
                value = BaseUI.getOptionText(value);
            }
            g.drawString(value, VALUE_X, 148 + step);
        }
    }
}

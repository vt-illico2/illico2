/**
 * @(#)FavouriteChannelRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.FavouriteChannelUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a background for Favourite Channel.
 * @author Woojung Kim
 * @version 1.1
 */
public class FavouriteChannelRenderer extends NormalSettingsBackRenderer {

    /** a image for top part of list background. */
    private Image listBgTopImg;
    /** a image for middle part of list background. */
    private Image listBgMidImg;
    /** a image for shadow of list. */
//    private Image listShadowImg;
    /** a image for shadow of list title. */
    private Image listTitleShImg;
    /** a image for focus on list. */
    private Image listFocusImg;
    /** a image for dimmed focuson list. */
    private Image listFocusDimImg;
    /** a image for left arrow. */
    private Image arrowLeftImg;
    /** a image for dimmed left arrow. */
    private Image arrowLeftDimImg;
    /** a image for right arrow. */
    private Image arrowRightImg;
    /** a image for dimmed right arrow. */
    private Image arrowRightDimImg;
    /** a image for icon. */
    private Image iconImg;
    /** a image for button to have a focus. */
    private Image btnFocusImg;
    /** a image for dimmed button to have a focus. */
    private Image btnFocusDimImg;
    /** a image for off button. */
    private Image btnFocusOffImg;
    /** a image for line on list. */
    private Image listLineImg;

    /** a string for title. */
    private String titleName;
    // VDTRMASTER-5767
    /** a string of title's sub */
    private String titleSub;
    /** a string for sub title at left. */
    private String leftSubTitle;
    /** a string for sub title at right. */
    private String rightSubTitle;

    /** a string for add button. */
    private String addStr;
    /** a string for remove button. */
    private String removeStr;

    /** a string array for initial message. */
    private String[] initMsgStr;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        // VDTRMASTER-5767
        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.SD_HD_CHANNELS_GROUPING);
        titleName = BaseUI.getMenuText(KeyNames.FAVOURITE_CHANNEL_TITLE + "."+ pre.getCurrentValue());
        titleSub = BaseUI.getMenuText(KeyNames.FAVOURITE_CHANNEL_TITLE_SUB + "."+ pre.getCurrentValue());
        leftSubTitle = BaseUI.getMenuText(KeyNames.FAVOURITE_CHANNEL_LEFT_SUBTITLE);
        rightSubTitle = BaseUI.getMenuText(KeyNames.FAVOURITE_CHANNEL_RIGHT_SUBTITLE);
        String msg = BaseUI.getMenuText(KeyNames.FAVOURITE_CHANNEL_INIT_MSG);
        initMsgStr = TextUtil.split(msg, fm18, 285);
        addStr = BaseUI.getMenuText(KeyNames.BTN_ADD);
        removeStr = BaseUI.getMenuText(KeyNames.BTN_REMOVE);

        listBgTopImg = BaseUI.getImage("08_listbg_t.png");
        listBgMidImg = BaseUI.getImage("08_listbg_m.png");
//        listShadowImg = BaseUI.getImage("08_listshadow.png");
        listTitleShImg = BaseUI.getImage("08_list_titlesh.png");
        listFocusImg = BaseUI.getImage("08_list_foc.png");
        listFocusDimImg = BaseUI.getImage("08_list_foc_dim.png");
        arrowLeftImg = BaseUI.getImage("08_add_ar_l.png");
        arrowLeftDimImg = BaseUI.getImage("08_add_ar_l_dim.png");
        arrowRightImg = BaseUI.getImage("08_add_ar_r.png");
        arrowRightDimImg = BaseUI.getImage("08_add_ar_r_dim.png");
        iconImg = BaseUI.getImage("08_sc_icon_fav.png");
        btnFocusImg = BaseUI.getImage("08_bt_foc.png");
        btnFocusDimImg = BaseUI.getImage("08_bt_dim.png");
        btnFocusOffImg = BaseUI.getImage("08_bt_off.png");
        listLineImg = BaseUI.getImage("08_listline.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        FavouriteChannelUI ui = (FavouriteChannelUI) c;

        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(titleName, 55, 113);
        // VDTRMASTER-5767
        g.setFont(f17);
        g.setColor(itemColor);
        g.drawString(titleSub, 55 + fm26.stringWidth(titleName) + 10, 113);

        g.setFont(f18);
        g.setColor(itemColor);
        // list bg
        for (int j = 0; j < 2; j++) {
            int stepX = j * 502;
            g.drawImage(listBgTopImg, 54 + stepX, 136, c);
            for (int i = 0; i < 5; i++) {
                int step = i * 32;
                g.drawImage(listBgMidImg, 54 + stepX, 304 + step, c);
            }
//            g.drawImage(listShadowImg, 7 + stepX, 456, c);
            g.drawImage(listTitleShImg, 56 + stepX, 168, c);

            if (j == 1 && ui.getRightListData().size() == 0) {
                int step = 197;
                for (int i = 0; i < initMsgStr.length; i++) {
                    step += 32;
                    g.drawString(initMsgStr[i], 585, step);
                }
            }
        }

        // list title
        g.setColor(fontBgColor);
        g.setFont(f20);
        g.drawString(ui.getLeftListSubTitle(leftSubTitle), 68, 161);
        g.drawString(ui.getRightListSubTitle(rightSubTitle), 600, 161);
        g.setColor(fontFgColor);
        g.drawString(ui.getLeftListSubTitle(leftSubTitle), 67, 160);
        g.drawString(ui.getRightListSubTitle(rightSubTitle), 599, 160);

        g.drawImage(iconImg, 572, 144, c);

        // button
        if (ui.getState() == ui.STATE_ADD) {
            g.drawImage(btnFocusImg, 410, 268, 146, 40, c);
            g.drawImage(btnFocusOffImg, 410, 311, 146, 40, c);
        } else if (ui.getState() == ui.STATE_REMOVE) {
            g.drawImage(btnFocusOffImg, 410, 268, 146, 40, c);
            g.drawImage(btnFocusImg, 410, 311, 146, 40, c);
        } else if (ui.getState() == ui.STATE_LEFT) {
            g.drawImage(btnFocusDimImg, 410, 268, 146, 40, c);
            g.drawImage(btnFocusOffImg, 410, 311, 146, 40, c);
        } else if (ui.getState() == ui.STATE_RIGHT) {
            g.drawImage(btnFocusOffImg, 410, 268, 146, 40, c);
            g.drawImage(btnFocusDimImg, 410, 311, 146, 40, c);
        }
        // arrow on button
        if (ui.getState() == ui.STATE_LEFT || ui.getState() == ui.STATE_ADD) {
            g.drawImage(arrowRightImg, 533, 277, c);
            g.setColor(itemFocusColor);
            GraphicUtil.drawStringCenter(g, addStr, 480, 290);
        } else {
            g.drawImage(arrowRightDimImg, 533, 277, c);
            g.setColor(btnOffShColor);
            GraphicUtil.drawStringCenter(g, addStr, 481, 291);
            g.setColor(btnOffColor);
            GraphicUtil.drawStringCenter(g, addStr, 480, 290);
        }

        if (ui.getState() == ui.STATE_RIGHT || ui.getState() == ui.STATE_REMOVE) {
            g.drawImage(arrowLeftImg, 419, 320, c);
            g.setColor(itemFocusColor);
            GraphicUtil.drawStringCenter(g, removeStr, 480, 333);
        } else {
            g.drawImage(arrowLeftDimImg, 419, 320, c);
            g.setColor(btnOffShColor);
            GraphicUtil.drawStringCenter(g, removeStr, 481, 334);
            g.setColor(btnOffColor);
            GraphicUtil.drawStringCenter(g, removeStr, 480, 333);
        }

        // left focus item
        Vector leftListData = ui.getLeftListData();
        int leftFocus = ui.getLeftListFocus();
        int leftListStartIdx = ui.getLeftListStartIdx();
        int step = 0;

        for (int i = 0; i < 8 && (leftListStartIdx + i) < leftListData.size() -1; i++) {
            step = i * 32;
            g.drawImage(listLineImg, 69, 207 + step, c);
        }

        if (ui.getState() == ui.STATE_LEFT || ui.getState() == ui.STATE_ADD) {
            step = leftFocus * 32;
            if (ui.getState() == ui.STATE_LEFT) {
                g.drawImage(listFocusImg, 53, 173 + step, c);
            } else if (ui.getState() == ui.STATE_ADD) {
                g.drawImage(listFocusDimImg, 53, 173 + step, c);
            }
        }

        // right focus item
        Vector rightListData = ui.getRightListData();
        int rightFocus = ui.getRightListFocus();
        int rightListStartIdx = ui.getRightListStartIdx();

        g.setColor(itemColor);

        for (int i = 0; i < 8 && (rightListStartIdx + i) < rightListData.size() -1; i++) {
            step = i * 32;
            g.drawImage(listLineImg, 571, 207 + step, c);
        }

        if (ui.getState() == ui.STATE_RIGHT || ui.getState() == ui.STATE_REMOVE) {
            step = rightFocus * 32;
            if (ui.getState() == ui.STATE_RIGHT) {
                g.drawImage(listFocusImg, 555, 173 + step, c);
            } else if (ui.getState() == ui.STATE_REMOVE) {
                g.drawImage(listFocusDimImg, 555, 173 + step, c);
            }
        }
    }
}

/**
 * @(#)FavouriteChannelListRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.FavouriteChannelListUI;
import com.videotron.tvi.illico.upp.ui.FavouriteChannelUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a list of Favourite channels.
 * @author Woojung Kim
 * @version 1.1
 */
public class FavouriteChannelListRenderer extends NormalSettingsBackRenderer {

    /** a image for top part of list shadow. */
//    private Image listShTopImg;
    /** a image for bottom part of list shadow. */
//    private Image listShBottomImg;
    /** a image for arrow on top. */
    private Image arrowTopImg;
    /** a image for arrow on bottom. */
    private Image arrowBottomImg;
    /** a image for dimmed arrow on top. */
    private Image arrowTopDimImg;
    /** a image for dimmed arrow on bottom. */
    private Image arrowBottomDimImg;
    /** a image for list line. */
    private Image listLineImg;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
//        listShTopImg = BaseUI.getImage("08_list_sh_t.png");
//        listShBottomImg = BaseUI.getImage("08_list_sh_b.png");

        arrowTopImg = BaseUI.getImage("02_ars_t.png");
        arrowBottomImg = BaseUI.getImage("02_ars_b.png");
        arrowTopDimImg = BaseUI.getImage("02_ars_t_dim.png");
        arrowBottomDimImg = BaseUI.getImage("02_ars_b_dim.png");
        listLineImg = BaseUI.getImage("08_listline.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        FavouriteChannelListUI myUI = (FavouriteChannelListUI) c;
        FavouriteChannelUI ui = myUI.getParentUI();

        int limit = 9;
        if (ui.isAnimating()) {
            limit = 10;
        }
        
        Hashtable sisterTable = ui.getSisterTable();

        // left list
        g.setFont(f18);
        Vector leftListData = ui.getLeftListData();
        int leftFocus = ui.getLeftListFocus();
        int leftListStartIdx = ui.getLeftListStartIdx();
        g.setColor(itemColor);
        int step = 0;
        for (int i = 0; i < limit && (leftListStartIdx + i) < leftListData.size(); i++) {
        	step = i * 32;
			Object[] data = (Object[]) leftListData.get(leftListStartIdx + i);
			String chNumber = String.valueOf(((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
			String chName = (String) data[BaseUI.INDEX_CHANNEL_NAME];
			String chFullName = TextUtil
					.shorten((String) data[BaseUI.INDEX_CHANNEL_FULL_NAME], g.getFontMetrics(), 105);
			Image logo = ui.getCurrentChannelLogo(chName);

			if (sisterTable.get(chName) != null) {
				// has sister channel
				Object[] sisterChannelData = (Object[]) sisterTable.get(chName);
				String mixedNumber;
				String sisterNumber = String.valueOf(((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
				mixedNumber = chNumber + " | " + sisterNumber + " HD";
				g.drawString(mixedNumber, 75, 197 + step);
				
				int definition = ((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				
				if (logo == null || definition >= TvChannel.DEFINITION_HD) {
					logo = ui.getCurrentChannelLogo((String) sisterChannelData[BaseUI.INDEX_CHANNEL_NAME]);
				}
				
				String sisterFullName = (String) data[BaseUI.INDEX_CHANNEL_FULL_NAME];
				if (definition >= TvChannel.DEFINITION_HD && sisterFullName != null&& sisterFullName.length() > 0) {
					chFullName = TextUtil.shorten(sisterFullName, g.getFontMetrics(), 105);
				}

			} else {
				int definition = ((Integer) data[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				if (definition == TvChannel.DEFINITION_4K) {
					g.drawString(chNumber + " UHD", 75, 197 + step);
				} else if (definition == TvChannel.DEFINITION_HD) {
					g.drawString(chNumber + " HD", 75, 197 + step);
				} else {
					g.drawString(chNumber, 75, 197 + step);
				}
			}

			g.drawImage(logo, 164, 177 + step, c);
			g.drawString(chFullName, 274, 197 + step);
			// g.drawImage(listLineImg, 69, 207 + step, c)
        }

        // left focus item
        if (ui.getState() == ui.STATE_LEFT || ui.getState() == ui.STATE_ADD) {
        	Object[] data = (Object[]) leftListData.get(leftListStartIdx + leftFocus);
        	String chNumber = String.valueOf(((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
			String chName = (String) data[BaseUI.INDEX_CHANNEL_NAME];
			String chFullName = TextUtil
					.shorten((String) data[BaseUI.INDEX_CHANNEL_FULL_NAME], g.getFontMetrics(), 105);
			Image logo = ui.getCurrentChannelLogo(chName);

			step = leftFocus * 32;
			if (ui.getState() == ui.STATE_LEFT) {
				g.setColor(itemFocusColor);
			} else if (ui.getState() == ui.STATE_ADD) {
				g.setColor(itemFocusColor);
			}
			if (sisterTable.get(chName) != null) {
				// has sister channel
				Object[] sisterChannelData = (Object[]) sisterTable.get(chName);
				String mixedNumber;
				String sisterNumber = String.valueOf(((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
				mixedNumber = chNumber + " | " + sisterNumber + " HD";
				g.drawString(mixedNumber, 75, 197 + step);
				
				int definition = ((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				
				if (logo == null || definition >= TvChannel.DEFINITION_HD) {
					logo = ui.getCurrentChannelLogo((String) sisterChannelData[BaseUI.INDEX_CHANNEL_NAME]);
				}
				
				String sisterFullName = (String) data[BaseUI.INDEX_CHANNEL_FULL_NAME];
				if (definition >= TvChannel.DEFINITION_HD && sisterFullName != null&& sisterFullName.length() > 0) {
					chFullName = TextUtil.shorten(sisterFullName, g.getFontMetrics(), 105);
				}

			} else {
				int definition = ((Integer) data[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				if (definition == TvChannel.DEFINITION_4K) {
					g.drawString(chNumber + " UHD", 75, 197 + step);
				} else if (definition == TvChannel.DEFINITION_HD) {
					g.drawString(chNumber + " HD", 75, 197 + step);
				} else {
					g.drawString(chNumber, 75, 197 + step);
				}
			}

			g.drawImage(logo, 164, 177 + step, c);
			g.drawString(chFullName, 274, 197 + step);
            if (!ui.isAnimating()) {
                if (leftListStartIdx > 0) {
//                    if (leftFocus > 0) {
//                        g.drawImage(listShTopImg, 55, 172, c);
//                    }
                    g.drawImage(arrowTopImg, 216, 164, c);
                }

                if (leftListStartIdx + 9 < leftListData.size()) {
//                    if (leftFocus != 8) {
//                        g.drawImage(listShBottomImg, 54, 405, c);
//                    }
                    g.drawImage(arrowBottomImg, 216, 456, c);
                }
            }
        } 
//        else if (!ui.isAnimating()) {
//            if (leftListStartIdx > 0) {
//                if (leftFocus > 0) {
//                    g.drawImage(listShTopImg, 55, 172, c);
//                }
//            }

//            if (leftListStartIdx + 9 < leftListData.size()) {
//                if (leftFocus != 8) {
//                    g.drawImage(listShBottomImg, 54, 405, c);
//                }
//            }
//        }

        // right list
        Vector rightListData = ui.getRightListData();
        int rightFocus = ui.getRightListFocus();
        int rightListStartIdx = ui.getRightListStartIdx();

        g.setColor(itemColor);

        for (int i = 0; i < limit && (rightListStartIdx + i) < rightListData.size(); i++) {
        	step = i * 32;
			Object[] data = (Object[]) rightListData.get(rightListStartIdx + i);
			String chNumber = String.valueOf(((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
			String chName = (String) data[BaseUI.INDEX_CHANNEL_NAME];
			String chFullName = TextUtil
					.shorten((String) data[BaseUI.INDEX_CHANNEL_FULL_NAME], g.getFontMetrics(), 105);
			Image logo = ui.getCurrentChannelLogo(chName);

			if (sisterTable.get(chName) != null) {
				// has sister channel
				Object[] sisterChannelData = (Object[]) sisterTable.get(chName);
				String mixedNumber;
				String sisterNumber = String.valueOf(((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
				mixedNumber = chNumber + " | " + sisterNumber + " HD";
				g.drawString(mixedNumber, 580, 197 + step);
				
				int definition = ((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				
				if (logo == null || definition >= TvChannel.DEFINITION_HD) {
					logo = ui.getCurrentChannelLogo((String) sisterChannelData[BaseUI.INDEX_CHANNEL_NAME]);
				}
				
				String sisterFullName = (String) data[BaseUI.INDEX_CHANNEL_FULL_NAME];
				if (definition >= TvChannel.DEFINITION_HD && sisterFullName != null&& sisterFullName.length() > 0) {
					chFullName = TextUtil.shorten(sisterFullName, g.getFontMetrics(), 105);
				}

			} else {
				int definition = ((Integer) data[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				if (definition == TvChannel.DEFINITION_4K) {
					g.drawString(chNumber + " UHD", 580, 197 + step);
				} else if (definition == TvChannel.DEFINITION_HD) {
					g.drawString(chNumber + " HD", 580, 197 + step);
				} else {
					g.drawString(chNumber, 580, 197 + step);
				}
				
			}

			g.drawImage(logo, 669, 177 + step, c);
			g.drawString(chFullName, 779, 197 + step);
            // g.drawImage(listLineImg, 571, 207 + step, c);
        }

        // right focus item
        if (ui.getState() == ui.STATE_RIGHT || ui.getState() == ui.STATE_REMOVE) {
        	Object[] data = (Object[]) rightListData.get(rightListStartIdx + rightFocus);
        	String chNumber = String.valueOf(((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
			String chName = (String) data[BaseUI.INDEX_CHANNEL_NAME];
			String chFullName = TextUtil
					.shorten((String) data[BaseUI.INDEX_CHANNEL_FULL_NAME], g.getFontMetrics(), 105);
			Image logo = ui.getCurrentChannelLogo(chName);
			step = rightFocus * 32;
			if (ui.getState() == ui.STATE_RIGHT) {
				g.setColor(itemFocusColor);
			} else if (ui.getState() == ui.STATE_REMOVE) {
				g.setColor(itemFocusColor);
			}
			if (sisterTable.get(chName) != null) {
				// has sister channel
				Object[] sisterChannelData = (Object[]) sisterTable.get(chName);
				String mixedNumber;
				String sisterNumber = String.valueOf(((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
				mixedNumber = chNumber + " | " + sisterNumber + " HD";
				g.drawString(mixedNumber, 580, 197 + step);

				int definition = ((Integer) sisterChannelData[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				
				if (logo == null || definition >= TvChannel.DEFINITION_HD) {
					logo = ui.getCurrentChannelLogo((String) sisterChannelData[BaseUI.INDEX_CHANNEL_NAME]);
				}
				
				String sisterFullName = (String) data[BaseUI.INDEX_CHANNEL_FULL_NAME];
				if (definition >= TvChannel.DEFINITION_HD && sisterFullName != null&& sisterFullName.length() > 0) {
					chFullName = TextUtil.shorten(sisterFullName, g.getFontMetrics(), 105);
				}

			} else {
				int definition = ((Integer) data[BaseUI.INDEX_CHANNEL_DEFINITION]).intValue();
				if (definition == TvChannel.DEFINITION_4K) {
					g.drawString(chNumber + " UHD", 580, 197 + step);
				} else if (definition == TvChannel.DEFINITION_HD) {
					g.drawString(chNumber + " HD", 580, 197 + step);
				} else {
					g.drawString(chNumber, 580, 197 + step);
				}
			}

			g.drawImage(logo, 669, 177 + step, c);
			g.drawString(chFullName, 779, 197 + step);

            if (!ui.isAnimating()) {
                if (rightListStartIdx > 0) {
//                    if (rightFocus > 0) {
//                        g.drawImage(listShTopImg, 557, 172, c);
//                    }
                    if (ui.isEditOrderMode()) {
                        g.drawImage(arrowTopDimImg, 721, 164, c);
                    } else {
                        g.drawImage(arrowTopImg, 721, 164, c);
                    }
                }

                if (rightListStartIdx + 9 < rightListData.size()) {
//                    if (rightFocus != 8) {
//                        g.drawImage(listShBottomImg, 556, 405, c);
//                    }
                    if (ui.isEditOrderMode()) {
                        g.drawImage(arrowBottomDimImg, 721, 456, c);
                    } else {
                        g.drawImage(arrowBottomImg, 721, 456, c);
                    }
                }
            }
        } 
//        else if (!ui.isAnimating()) {
//            if (rightListStartIdx > 0) {
//                if (rightFocus > 0) {
//                    g.drawImage(listShTopImg, 557, 172, c);
//                }
//            }

//            if (rightListStartIdx + 9 < rightListData.size()) {
//                if (rightFocus != 8) {
//                    g.drawImage(listShBottomImg, 556, 405, c);
//                }
//            }
//        }
    }

    /**
     * Paint a decoration.
     * @param g Graphcis to paint.
     * @param parent parent UI.
     */
    public void paintDecoOfList(Graphics g, BaseUI parent) {
        FavouriteChannelListUI myUI = (FavouriteChannelListUI) parent;
        FavouriteChannelUI ui = myUI.getParentUI();

        Vector leftListData = ui.getLeftListData();
        int leftFocus = ui.getLeftListFocus();
        int leftListStartIdx = ui.getLeftListStartIdx();
        // left focus item
        if (ui.getState() == ui.STATE_LEFT || ui.getState() == ui.STATE_ADD) {
            if (leftListStartIdx > 0) {
//                if (leftFocus > 0) {
//                    g.drawImage(listShTopImg, 55, 172, myUI);
//                }
                g.drawImage(arrowTopImg, 216, 164, myUI);
            }

            if (leftListStartIdx + 9 < leftListData.size()) {
//                if (leftFocus != 8) {
//                    g.drawImage(listShBottomImg, 54, 405, myUI);
//                }
                g.drawImage(arrowBottomImg, 216, 456, myUI);
            }
        } 
//        else {
//            if (leftListStartIdx > 0) {
//                if (leftFocus > 0) {
//                    g.drawImage(listShTopImg, 55, 172, myUI);
//                }
//            }

//            if (leftListStartIdx + 9 < leftListData.size()) {
//                if (leftFocus != 8) {
//                    g.drawImage(listShBottomImg, 54, 405, myUI);
//                }
//            }
//        }

        // right list
        Vector rightListData = ui.getRightListData();
        int rightFocus = ui.getRightListFocus();
        int rightListStartIdx = ui.getRightListStartIdx();
        // right focus item
        if (ui.getState() == ui.STATE_RIGHT || ui.getState() == ui.STATE_REMOVE) {
            if (rightListStartIdx > 0) {
//                if (rightFocus > 0) {
//                    g.drawImage(listShTopImg, 557, 172, myUI);
//                }
                if (ui.isEditOrderMode()) {
                    g.drawImage(arrowTopDimImg, 721, 164, myUI);
                } else {
                    g.drawImage(arrowTopImg, 721, 164, myUI);
                }
            }

            if (rightListStartIdx + 9 < rightListData.size()) {
//                if (rightFocus != 8) {
//                    g.drawImage(listShBottomImg, 556, 405, myUI);
//                }
                if (ui.isEditOrderMode()) {
                    g.drawImage(arrowBottomDimImg, 721, 456, myUI);
                } else {
                    g.drawImage(arrowBottomImg, 721, 456, myUI);
                }
            }
        } 
//        else {
//            if (rightListStartIdx > 0) {
//                if (rightFocus > 0) {
//                    g.drawImage(listShTopImg, 557, 172, myUI);
//                }
//            }

//            if (rightListStartIdx + 9 < rightListData.size()) {
//                if (rightFocus != 8) {
//                    g.drawImage(listShBottomImg, 556, 405, myUI);
//                }
//            }
//        }
    }
}

/**
 * @(#)ParentalControlsRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ParentalControlsUI;

/**
 * This class render a preferences for Controls & Limits.
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalControlsRenderer extends NormalSettingsBackRenderer {

    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed focus to have a focus. */
    private Image inputFocusDimImg;
    /** a image for off focus. */
    private Image inputOffImg;
    /** a image for inbox to have a focus. */
    private Image inboxFocusImg;
    /** a image for inbox. */
    private Image inboxImg;
    /** a image for off inbox. */
    private Image inboxOffImg;
    /** a image for line. */
    private Image lineImg;
    /** a image for down arrow. */
    private Image arrowDownImg;
    /** a image for up arrow. */
    private Image arrowUpImg;

    /** a string array for preference ids. */
    private String[] preferenceIds;
    /** a string for title. */
    private String title;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        preferenceIds = ((ParentalControlsUI) c).currentIds;
        title = BaseUI.getMenuText("modify.parental.control");

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        inputOffImg = BaseUI.getImage("input_210_sc_dim.png");
        inboxFocusImg = BaseUI.getImage("focus_05_05.png");
        inboxImg = BaseUI.getImage("inbox_210.png");
        inboxOffImg = BaseUI.getImage("inbox_210_off.png");
        lineImg = BaseUI.getImage("08_bg_line.png");
        arrowDownImg = BaseUI.getImage("02_ars_b.png");
        arrowUpImg = BaseUI.getImage("02_ars_t.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        ParentalControlsUI ui = (ParentalControlsUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, 55, 113);

        g.drawImage(lineImg, 0, 167, c);

        int step = 0;
        int idx = 0;
        for (int i = 0; i < 7; i++) {
            if (i == 0) {
                step = i * 51 - 14;
            } else {
                step = i * 37;
            }
            if (i > 0) {
                idx = ui.getStartIdx() + i;
            }
            Preference pre = (Preference) preTable.get(preferenceIds[idx]);
            g.setFont(f17);
            if (idx == 2 && ui.canDrawRatings()) { // hide adult content
                g.setColor(offColor);
            } else if (ui.canSelectMenu() || i == 0) {
                g.setColor(nameColor);
            } else {
                g.setColor(offColor);
            }

            if (pre != null) {
                String name = pre.getName();
                if (idx == 2 || idx == 4) { // hide audlt & Pin code unblocks channel
                    g.drawString(name, NAME_X + 23, 161 + step);
                } else {
                    g.drawString(name, NAME_X, 161 + step);
                }
            }

            if (idx == 3 || idx == 5) {

                if (i == c.getFocus()) {
                    g.setColor(focusName2Color);
                    g.drawImage(inboxFocusImg, INPUT_X, 141 + step, c);
                } else if (ui.canSelectMenu()) {
                    g.setColor(dimmedNameColor);
                    g.drawImage(inboxImg, INPUT_X, 141 + step, c);
                } else {
                    g.setColor(offColor);
                    g.drawImage(inboxOffImg, INPUT_X, 141 + step, c);
                }

                String value = ui.getPreferenceValueText(idx);
                g.drawString(value, VALUE_X, 162 + step);

            } else if (pre != null) {
                g.setFont(f18);
                if (idx == 2 && ui.canDrawRatings()) { // hide adult content
                    g.setColor(offColor);
                    if (idx == c.getFocus()) {
                        g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputOffImg, INPUT_X, 141 + step, c);
                    }
                } else if (i == c.getFocus() && ui.getRatingFocus() == 0) {
                    g.setColor(focusNameColor);
                    g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                } else if (ui.canSelectMenu()) {
                    g.setColor(dimmedNameColor);
                    g.drawImage(inputImg, INPUT_X, 141 + step, c);
                } else {
                    g.setColor(offColor);
                    g.drawImage(inputOffImg, INPUT_X, 141 + step, c);
                }
                String value = pre.getCurrentValue();
                if (i == 0) {
                    if (value.equals(Definitions.OPTION_VALUE_ON) || value.equals(Definitions.OPTION_VALUE_OFF)) {
                        value = BaseUI.getOptionText(value + "2");
                    } else {
                        value = BaseUI.getOptionText(value);
                    }
                } else {
                    value = BaseUI.getOptionText(value);
                }
                g.drawString(value, VALUE_X, 162 + step);

                if (idx == 1 && ui.canDrawRatings()) {
                    if (ui.getRatingFocus() == 1) {
                        g.setColor(focusName2Color);
                        g.drawImage(inboxFocusImg, INPUT_X + 270, 141 + step, c);
                    } else if (ui.canSelectMenu()) {
                        g.setColor(dimmedNameColor);
                        g.drawImage(inboxImg, INPUT_X + 270, 141 + step, c);
                    } else {
                        g.setColor(offColor);
                        g.drawImage(inboxOffImg, INPUT_X + 270, 141 + step, c);
                    }

                    value = ui.getPreferenceValueText(idx);
                    g.drawString(value, VALUE_X + 270, 162 + step);
                }
            }
        }

        if (ui.currentIds.length > 7 && ui.canSelectMenu()) {
            if (ui.getStartIdx() == 0 || ui.getFocus() + ui.getStartIdx() < 4) {
                g.drawImage(arrowDownImg, 432, 393, c);
            } else {
                g.drawImage(arrowUpImg, 432, 162, c);
            }
        }
    }

    /**
     * Paint a selected content.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        ParentalControlsUI ui = (ParentalControlsUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = parent.getFocus();
        int step = 0;
        if (idx == 0) {
            step = idx * 51 - 14;
        } else {
            step = idx * 37;
        }
        
        idx = ui.getStartIdx() + idx;
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);
        
        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            if (idx == 2 || idx == 4) { // hide audlt & Pin code unblocks channel
                g.drawString(name, NAME_X + 23, 161 + step);
            } else {
                g.drawString(name, NAME_X, 161 + step);
            }

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
            String value = pre.getCurrentValue();
            if (idx == 0) {
                if (value.equals(Definitions.OPTION_VALUE_ON) || value.equals(Definitions.OPTION_VALUE_OFF)) {
                    value = BaseUI.getOptionText(value + "2");
                } else {
                    value = BaseUI.getOptionText(value);
                }
            } else {
                value = BaseUI.getOptionText(value);
            }
            g.drawString(value, VALUE_X, 162 + step);
        }
    }
}

/**
 * @(#)AudioAccessibilityRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.AudioAccessibilityUI;
import com.videotron.tvi.illico.upp.ui.BaseUI;

/**
 * This class render a preferences for Audio Accessibility (SAP & Audio described).
 * @author Woojung Kim
 * @version 1.1
 */
public class AudioAccessibilityRenderer extends NormalSettingsBackRenderer {

    /** a image for current value. */
    private Image inputImg;
    /** a image for focus. */
    private Image inputFocusImg;
    /** a image for dimmed focus. */
    private Image inputFocusDimImg;

    /** a preference id's. */
    private String[] preferenceIds;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        preferenceIds = ((AudioAccessibilityUI) c).preferenceIds;

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");

        super.prepare(c);
    }

    /**
     * Paint a items.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        AudioAccessibilityUI ui = (AudioAccessibilityUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        Category category = (Category) BaseUI.peekHistory();
        g.drawString(category.getName(), 55, 113);

        for (int i = 0; i < preferenceIds.length; i++) {
            int step = i * 37;
            Preference pre = (Preference) preTable.get(preferenceIds[i]);
            g.setFont(f17);
            g.setColor(nameColor);

            if (pre != null) {
                String name = pre.getName();
                g.drawString(name, NAME_X, 161 + step);
            }

            g.setFont(f18);
            if (pre != null) {

                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                } else {
                    g.setColor(dimmedNameColor);
                    g.drawImage(inputImg, INPUT_X, 141 + step, c);
                }
                String value = pre.getCurrentValue();
                
                if (pre.getID().equals(PreferenceNames.DESCRIBED_AUDIO_DISPLAY)) {
                    value = BaseUI.getOptionText(value + "4");
                } else {
                    value = BaseUI.getOptionText(value);
                }
                if (value != null) {
                    g.drawString(value, VALUE_X, 162 + step);
                }
            }
        }
    }

    /**
     * Paint a current item to have a focus when display a select Popup.
     * @param g Graphics to paint.
     * @param parent Parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        AudioAccessibilityUI ui = (AudioAccessibilityUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = ui.getFocus();
        int step = idx * 37;
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            g.drawString(name, NAME_X, 161 + step);

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
            String value = pre.getCurrentValue();
            if (pre.getID().equals(PreferenceNames.DESCRIBED_AUDIO_DISPLAY)) {
                value = BaseUI.getOptionText(value + "4");
            } else {
                value = BaseUI.getOptionText(value);
            }
            g.drawString(value, VALUE_X, 162 + step);
        }
    }
}

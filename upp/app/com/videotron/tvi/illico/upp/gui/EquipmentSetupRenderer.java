/**
 * @(#)EquipmentSetupRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.EquipmentSetupUI;

/**
 * This class render a preferences of Equipment Setup.
 * @author Woojung Kim
 * @version 1.1
 */
public class EquipmentSetupRenderer extends NormalSettingsBackRenderer {

    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed input to have a focus. */
    private Image inputFocusDimImg;
    /** a image for secondly input. */
    private Image input2Img;
    /** a image for secondly input to have a focus. */
    private Image inputFocus2Img;
    /** a image for inbox to have a focus. */
    private Image inboxFocusImg;
    /** a image for inbox. */
    private Image inboxImg;
    /** a image for up arrow. */
    private Image arrowUpImg;
    /** a image for down arrow. */
    private Image arrowDownImg;

    /** a string array for preference ids.*/
    private String[] preferenceIds;

    private String title;
    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        preferenceIds = ((EquipmentSetupUI) c).getPreferenceIds();

        //Category category = (Category) BaseUI.peekHistory();
        title = DataCenter.getInstance().getString("menu.equipment.setup");
        
        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inboxFocusImg = BaseUI.getImage("focus_05_05.png");
        inboxImg = BaseUI.getImage("inbox_210.png");
        arrowUpImg = BaseUI.getImage("02_ars_t.png");
        arrowDownImg = BaseUI.getImage("02_ars_b.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        EquipmentSetupUI ui = (EquipmentSetupUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        //Category category = (Category) BaseUI.peekHistory();
        //g.drawString(category.getName(), 55, 113);
        g.drawString(title, 55, 113);

        int idx = ui.getStartIdx();
        for (int i = 0; i < 7; i++) {
            if (idx == preferenceIds.length) {
                break;
            }
            int step = i * 37;
            Preference pre = (Preference) preTable.get(preferenceIds[idx]);
            g.setFont(f17);
            g.setColor(nameColor);

            if (pre != null) {
                String name = pre.getName();
                g.drawString(name, NAME_X, 161 + step);
            }

            g.setFont(f18);
            if (preferenceIds[idx].equals(PreferenceNames.STB_SLEEP_TIME)
                    || preferenceIds[idx].equals(PreferenceNames.STB_WAKE_UP_TIME)
                    || preferenceIds[idx].equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)) {
                // sleep time / auto power off

                if (i == c.getFocus()) {
                    g.setColor(focusName2Color);
                    g.drawImage(inboxFocusImg, INPUT_X, 141 + step, c);
                } else {
                    g.setColor(dimmedNameColor);
                    g.drawImage(inboxImg, INPUT_X, 141 + step, c);
                }

                if (preferenceIds[idx].equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)) {
                    String value = ui.getTVPictureFormatText();
                    if (value != null) {
                        g.drawString(value, VALUE_X, 162 + step);
                    }
                } else if (preferenceIds[idx].equals(PreferenceNames.STB_SLEEP_TIME)) {
                    String value = ui.getSleepTimeInfo();
                    if (value != null) {
                        g.drawString(value, VALUE_X, 162 + step);
                    }
                } else if (preferenceIds[idx].equals(PreferenceNames.STB_WAKE_UP_TIME)) {
                    String value = ui.getWakeUpTimeInfo();
                    if (value != null) {
                        g.drawString(value, VALUE_X, 162 + step);
                    }
                }
            } else if (pre != null) {

                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    if (idx == 0 && ui.canDisplayAPDTime()) {
                		if (ui.getFocusAPD() == 0) {
                			g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                		} else {
                			g.drawImage(inputImg, INPUT_X, 141 + step, c);
                		}
                    } else if (i == 2) {
                        g.drawImage(inputFocus2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                    }
                } else {
                    g.setColor(dimmedNameColor);
                    if (i == 2) {
                        g.drawImage(input2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputImg, INPUT_X, 141 + step, c);
                    }
                }
                String value = pre.getCurrentValue();
                
                if (preferenceIds[idx].equals(PreferenceNames.VOD_1080P_SUPPORT)) {
                	// VDTRMASTER-5822
                	if (Definitions.OPTION_VALUE_ENABLE.equals(value)) {
                		value = BaseUI.getOptionText(Definitions.OPTION_VALUE_ENABLE + "d");
                	} else {
                		value = BaseUI.getOptionText(Definitions.OPTION_VALUE_DISABLE + "d");
                	}
                } else {
                	value = BaseUI.getOptionText(value);
                }
                if (value != null) {
                    g.drawString(value, VALUE_X, 162 + step);
                }
                
                // display APD time
                if (idx == 0 && ui.canDisplayAPDTime()) {
                	if (ui.getFocus() == idx && ui.getFocusAPD() == 1) {
                		g.setColor(focusNameColor);
                		g.drawImage(inputFocusImg, INPUT_X + 229, 141 + step, c);
                	} else {
                		g.setColor(dimmedNameColor);
                		g.drawImage(inputImg, INPUT_X + 229, 141 + step, c);
                	}
                	
                	Preference adpTimePre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME);
                	value = BaseUI.getOptionText(adpTimePre.getCurrentValue());
                	
                	g.drawString(value, VALUE_X + 243, 162 + step);
                }
            }
            idx++;
        }

        if (ui.getStartIdx() > 0) {
            g.drawImage(arrowUpImg, 432, 125, c);
        }

        if (ui.getStartIdx() + 7 < preferenceIds.length) {
            g.drawImage(arrowDownImg, 432, 393, c);
        }
    }

    /**
     * Paint a selected content when display a select popup.
     * @param g Graphics to paint.
     * @param parent parent UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        EquipmentSetupUI ui = (EquipmentSetupUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = ui.getStartIdx() + ui.getFocus();
        int step = ui.getFocus() * 37;
        int moreX = 0;
        Preference pre = null;
        Preference parentPre = null;
        		
        if (ui.getFocus() == 0 && ui.getFocusAPD() == 1) {
        	pre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME);
        	parentPre = (Preference) preTable.get(preferenceIds[idx]);
        	moreX = 229;
        } else {
        	pre = (Preference) preTable.get(preferenceIds[idx]);
        }
        		

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = "";
            if (moreX != 0) {
            	name = parentPre.getName();
            } else {
            	name = pre.getName();
            }
            g.drawString(name, NAME_X, 161 + step);

            g.setFont(f18);
            g.drawImage(inputFocusDimImg, INPUT_X + moreX, 141 + step, ui);
            String value = pre.getCurrentValue();
            // VDTRMASTER-5822
            if (preferenceIds[idx].equals(PreferenceNames.VOD_1080P_SUPPORT)) {
            	if (Definitions.OPTION_VALUE_ENABLE.equals(value)) {
            		value = BaseUI.getOptionText(Definitions.OPTION_VALUE_ENABLE + "d");
            	} else {
            		value = BaseUI.getOptionText(Definitions.OPTION_VALUE_DISABLE + "d");
            	}
            } else {
            	value = BaseUI.getOptionText(value);
            }
            
            if (moreX != 0) {
            	g.drawString(value, VALUE_X + moreX + 14, 162 + step);
            } else {
            	g.drawString(value, VALUE_X, 162 + step);
            }
        }
    }
}

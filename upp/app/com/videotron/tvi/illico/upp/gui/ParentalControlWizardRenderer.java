/**
 * @(#)ParentalControlWizardRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ParentalControlWizardUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class render a age list to be blocked.
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalControlWizardRenderer extends PopBaseRenderer {

    /** a image for shadow. */
//    private Image shadowImg;
    /** a image for high. */
//    private Image highImg;
    /** a image for gap. */
    private Image gapImg;
    /** a image for button to have a focus. */
    private Image btnOnImg;
    /** a image for button. */
    private Image btnOffImg;
    /** a image for left arrow. */
    private Image arrowLeftImg;
    /** a image for right arrow. */
    private Image arrowRightImg;
    /** a image for age focus. */
    private Image ageFocusImg;
    /** a image for age background. */
    private Image ageBgImg;
    /** a image for selected age. */
    private Image ageSelectedImg;
    /** a image for unlock. */
    private Image unlockImg;
    /** a image for lock. */
    private Image lockImg;

    /** a color for subtitle. */
    private Color subTitleColor = new Color(255, 204, 0);
    /** a font of size 24. */
    private Font f24 = FontResource.BLENDER.getFont(24);
    /** a font of size 32. */
    private Font f32 = FontResource.BLENDER.getFont(32);
    /** a font of size 46. */
    private Font f46 = FontResource.BLENDER.getFont(46);

    /** a string for subtitle. */
    private String subTitle;
    /** a string for sub explain. */
    private String subExplain;
    /** a string array for scope. */
    private String[] scope = new String[4];
    /** a string for cancel button. */
    private String btnCancel;
    /** a string for confirm button. */
    private String btnConfirm;

    /**
     * Prepare a renderer.
     * @param c parent ui.
     */
    public void prepare(UIComponent c) {
        title = BaseUI.getPopupText(KeyNames.TITLE_PARENTAL_CONTROL_WIZARD);
        subTitle = BaseUI.getPopupText(KeyNames.SUBTITLE_BLOCK_BY_RATING);
        subExplain = BaseUI.getPopupText(KeyNames.EXPLAIN_REQUIRE_ADMIN_PIN);

        btnCancel = BaseUI.getPopupText(KeyNames.BTN_CANCEL);
        btnConfirm = BaseUI.getPopupText(KeyNames.BTN_CONFIRM);

        scope[0] = BaseUI.getPopupText(KeyNames.SCOPE_8);
        scope[1] = BaseUI.getPopupText(KeyNames.SCOPE_13);
        scope[2] = BaseUI.getPopupText(KeyNames.SCOPE_16);
        scope[3] = BaseUI.getPopupText(KeyNames.SCOPE_18);

//        shadowImg = BaseUI.getImage("12_ppop_sha.png");
//        highImg = BaseUI.getImage("12_pop_high_625.png");
        gapImg = BaseUI.getImage("07_respop_title.png");
        btnOnImg = BaseUI.getImage("12_focus.png");
        btnOffImg = BaseUI.getImage("12_focus_dim.png");
        arrowLeftImg = BaseUI.getImage("02_ars_l.png");
        arrowRightImg = BaseUI.getImage("02_ars_r.png");
        ageFocusImg = BaseUI.getImage("12_age_foc.png");
        ageBgImg = BaseUI.getImage("12_age_dim.png");
        ageSelectedImg = BaseUI.getImage("12_age_select.png");
        unlockImg = BaseUI.getImage("12_icn_thumb.png");
        lockImg = BaseUI.getImage("12_icn_lock.png");

        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent ui.
     */
    protected void paint(Graphics g, UIComponent c) {
        ParentalControlWizardUI ui = (ParentalControlWizardUI) c;
        g.setColor(dimmedColor);
        g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        g.setColor(fillRectColor);
        g.fillRect(170, 51, 625, 435);
//        g.drawImage(shadowImg, 170, 483, 625, 57, c);
//        g.drawImage(highImg, 170, 51, c);
        g.drawImage(gapImg, 197, 89, c);

        g.setColor(titleColor);
        g.setFont(f24);

        GraphicUtil.drawStringCenter(g, title, 485, 78);

        g.setColor(Color.WHITE);
        g.setFont(f18);

        GraphicUtil.drawStringCenter(g, ui.getPreferenceName(0), 482, 126);

        // Ratings
        String[] ratings = ui.getRatings();
        g.setFont(f46);
        for (int i = 0; i < ratings.length; i++) {
            int stepX = i * 144;
            if (ui.getFocus() == i) {
                g.setColor(focusName2Color);
                if (ui.getState() == ParentalControlWizardUI.STATE_RATING) {
                    g.drawImage(ageFocusImg, 202 + stepX, 155, c);
                    if (i != 0) {
                        g.drawImage(arrowLeftImg, 190 + stepX, 187, c);
                    }
                    if (i != 3) {
                        g.drawImage(arrowRightImg, 326 + stepX, 187, c);
                    }
                } else {
                    g.drawImage(ageSelectedImg, 202 + stepX, 155, c);
                }
            } else {
                g.setColor(nameColor);
                g.drawImage(ageBgImg, 202 + stepX, 155, c);
            }

            GraphicUtil.drawStringCenter(g, ratings[i], 269 + stepX, 211);

            if (i < ui.getFocus()) {
                g.drawImage(unlockImg, 243 + stepX, 248, c);
            } else {
                g.drawImage(lockImg, 243 + stepX, 248, c);
            }
        }

        // explain
        g.setColor(Color.WHITE);
        g.setFont(f18);
        GraphicUtil.drawStringCenter(g, subTitle, 484, 331);
        GraphicUtil.drawStringCenter(g, subExplain, 482, 391);

        g.setColor(subTitleColor);
        g.setFont(f32);
        GraphicUtil.drawStringCenter(g, scope[ui.getFocus()], 479, 364);

        g.setFont(f18);
        g.setColor(focusName2Color);
        // button
        if (ui.getState() == ParentalControlWizardUI.STATE_BUTTON) {
            if (ui.getButtonFocus() == 0) {
                g.drawImage(btnOnImg, 338, 437, c);
                g.drawImage(btnOffImg, 484, 437, c);
            } else {
                g.drawImage(btnOffImg, 338, 437, c);
                g.drawImage(btnOnImg, 484, 437, c);
            }
        } else {
            g.drawImage(btnOffImg, 338, 437, c);
            g.drawImage(btnOffImg, 484, 437, c);
        }

        GraphicUtil.drawStringCenter(g, btnCancel, 407, 458);
        GraphicUtil.drawStringCenter(g, btnConfirm, 553, 458);
    }
}

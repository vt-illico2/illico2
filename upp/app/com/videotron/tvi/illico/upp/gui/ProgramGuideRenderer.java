/**
 * @(#)ProgramGuideRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ProgramGuideUI;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a preferences for Programe guide.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class ProgramGuideRenderer extends NormalSettingsBackRenderer {

	/** a image for input. */
	private Image inputImg;
	/** a image for input to have a focus. */
	private Image inputFocusImg;
	/** a image for dimmed input to have a focus. */
	private Image inputFocusDimImg;
	/** a image for inbox. */
	private Image inBoxImg;
	/** a image for inbox to have a focus. */
	private Image inBoxFocusImg;
	/** a image for background at bottom. */
	private Image bgBottomImg;
	/** a image for background at top. */
	private Image bgTopImg;
	/** a image for background at middle. */
	private Image bgMidImg;
	/** a image for line. */
	private Image lineImg;
	
	// R7
    /** a image for down arrow. */
    private Image arrowDownImg;
    /** a image for up arrow. */
    private Image arrowUpImg;
    
	/** a format for three decimal. */
	private DecimalFormat df = new DecimalFormat("000");
	/** a string array for preference id's. */
	private String[] preferenceIds;
	/** a string for title. */
	private String title;

	/**
	 * Prepare a renderer.
	 * 
	 * @param c
	 *            parent UI.
	 */
	public void prepare(UIComponent c) {
		ProgramGuideUI ui = (ProgramGuideUI) c;
		preferenceIds = ui.getPreferenceIds();
		String[] history = BaseUI.getHistory();
		title = history[history.length - 1];

		inputImg = BaseUI.getImage("input_281_sc.png");
		inputFocusImg = BaseUI.getImage("input_280_foc2.png");
		inputFocusDimImg = BaseUI.getImage("input_281_high.png");
		inBoxImg = BaseUI.getImage("inbox_281.png");
		inBoxFocusImg = BaseUI.getImage("inbox_281_foc.png");

		bgBottomImg = BaseUI.getImage("08_op_bg_b_304.png");
		bgTopImg = BaseUI.getImage("08_op_bg_t_304.png");
		bgMidImg = BaseUI.getImage("08_op_bg_m_304.png");
		lineImg = BaseUI.getImage("08_bg_line.png");
		
		// R7
        arrowDownImg = BaseUI.getImage("02_ars_b.png");
        arrowUpImg = BaseUI.getImage("02_ars_t.png");
        
		super.prepare(c);
	}

	/**
	 * Paint a renderer.
	 * 
	 * @param g
	 *            Graphics to paint.
	 * @param c
	 *            parent UI.
	 */
	protected void paint(Graphics g, UIComponent c) {
		super.paint(g, c);
		ProgramGuideUI ui = (ProgramGuideUI) c;
		Hashtable preTable = ui.getPreferences();

		g.setColor(titleColor);
		g.setFont(f26);
		g.drawString(title, 55, 113);

		int step = 0;
		int startIdx = ui.getStartIdx();
		int count = 0;
		int focusIdx = startIdx + c.getFocus();
		
		if (preferenceIds.length > 4) {
			g.drawImage(lineImg, 0, 314 - startIdx * 37, c);
		}
		
		for (int i = startIdx; i < startIdx + 7; i++) {
			if (i < 5) {
				step = count * 37 - 14;
			} else {
				step = count * 37 + 10 - 14;
			}
			
			Preference pre = (Preference) preTable.get(preferenceIds[i]);

			g.setFont(f17);
			g.setColor(nameColor);
			if (preferenceIds[i].equals(PreferenceNames.FAVOURITE_CHANNEL)
					|| preferenceIds[i].equals(PreferenceNames.FAVOURITE_CHANNEL_FILTER)) {

				if (pre != null) {
					String name = pre.getName();

					if (name.indexOf("|") > -1) {
						String[] names = TextUtil.tokenize(name, "|");
						g.drawString(names[0], NAME_X, 161 + step - 9);
						g.drawString(names[1], NAME_X, 161 + step + 9);
					} else {
						g.drawString(name, NAME_X, 161 + step);
					}

					if (i == focusIdx) {
						g.setColor(focusName2Color);
						g.drawImage(inBoxFocusImg, INPUT_X, 141 + step, c);
					} else {
						g.setColor(dimmedNameColor);
						g.drawImage(inBoxImg, INPUT_X, 141 + step, c);
					}

					String value = ui.getPreferenceValueText(i);
					g.drawString(value, VALUE_X, 162 + step);
				}
			} else {
				if (pre != null) {
					String name = pre.getName();
					if (name.indexOf("|") > -1) {
						String[] names = TextUtil.tokenize(name, "|");
						g.drawString(names[0], NAME_X, 161 + step - 9);
						g.drawString(names[1], NAME_X, 161 + step + 9);
					} else {
						String[] names = TextUtil.split(name, g.getFontMetrics(), 260);
						if (names.length > 1) {
							g.drawString(names[0], NAME_X, 161 + step - 9);
							g.drawString(names[1], NAME_X, 161 + step + 9);
						} else {
							g.drawString(name, NAME_X, 161 + step);
						}
					}

					g.setFont(f18);
					if (i == focusIdx) {
						g.setColor(focusNameColor);
						g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
					} else {
						g.setColor(dimmedNameColor);
						g.drawImage(inputImg, INPUT_X, 141 + step, c);
					}

					String value = pre.getCurrentValue();

					if (preferenceIds[i].equals(PreferenceNames.POWER_ON_CHANNEL)) {
						if (ui.isEnteringNumber()) {
							g.drawString(ui.getEnteringNumber(), VALUE_X, 161 + step);
						} else {
							String chNumber = TextUtil.EMPTY_STRING;
							String chName = TextUtil.EMPTY_STRING;
							EpgService epgService = CommunicationManager.getInstance().getEpgService();
							TvChannel powerOnChannel = null;
							try {
								powerOnChannel = epgService.getChannel(value);
								if (powerOnChannel != null) {
									chNumber = String.valueOf(powerOnChannel.getNumber());
								}
							} catch (RemoteException e) {
								Log.print(e);
							}

							if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
								try {
									TvChannel sisterChannel = powerOnChannel.getSisterChannel();
									if (sisterChannel != null && PreferenceController.getInstance().isChannelGrouped()) {
										// has sister channel
										if (powerOnChannel.isHd()) {
											chName = powerOnChannel.getFullName();
											chNumber =  sisterChannel.getNumber() + " | " + powerOnChannel.getNumber() + " HD";
										} else {
											chName = sisterChannel.getFullName();
											chNumber =  powerOnChannel.getNumber() + " | " + sisterChannel.getNumber() + " HD";
										}
									} else {
										try {
											if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_4K) {
												chNumber = powerOnChannel.getNumber() + " UHD";
											} else if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_HD) {
												chNumber = powerOnChannel.getNumber() + " HD";
											}
											chName = powerOnChannel.getFullName();
										} catch (RemoteException e) {
											e.printStackTrace();
										}
									}
									
									String mixedStr = chNumber + "   " + chName;
									mixedStr = TextUtil.shorten(mixedStr, fm17, 250);
									g.drawString(mixedStr, VALUE_X, 161 + step);
								} catch (RemoteException e) {
									Log.print(e);
								}
							} else {
								value = BaseUI.getOptionText(Definitions.LAST_CHANNEL);
								g.drawString(value, VALUE_X, 161 + step);
							}
						}
					} else {
						value = BaseUI.getOptionText(value);
						g.drawString(value, VALUE_X, 161 + step);
					}
				}
			}
			count++;
		}
		
		if (preferenceIds.length > 7) {
            if (startIdx + 6 < preferenceIds.length - 1) {
                g.drawImage(arrowDownImg, 467, 393, c);
            }
            if (startIdx > 0) {
                g.drawImage(arrowUpImg, 467, 112, c);
            }
        }

		if (ui.isEnteringNumber()) {
			Vector v = ui.getFoundChannel();
			int i = 0;
			step = 0;
			g.drawImage(bgTopImg, 547, 110, 851, 113, 0, 0, 304, 3, c);
			for (i = 0; i < 9; i++) {
				step = i * 27;

				g.drawImage(bgMidImg, 547, 113 + step, c);

				if (i < v.size()) {
					Object[] channelData = (Object[]) v.get(i);
					String name = (String) channelData[BaseUI.INDEX_CHANNEL_NAME];
					String number = TextUtil.EMPTY_STRING;
					if (!name.equals(Definitions.LAST_CHANNEL)) {
						String chNumber = TextUtil.EMPTY_STRING;
						EpgService epgService = CommunicationManager.getInstance().getEpgService();
						TvChannel ch = null;
						try {
							ch = epgService.getChannel(name);
							if (ch != null) {
								chNumber = String.valueOf(ch.getNumber());
							}
						} catch (RemoteException e) {
							Log.print(e);
						}

						if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
							try {
								TvChannel sisterChannel = ch.getSisterChannel();
								if (sisterChannel != null && PreferenceController.getInstance().isChannelGrouped()) {
									// has sister channel
									if (ch.isHd()) {
										name = ch.getFullName();
										// VDTRMASTER-5697
										number =  sisterChannel.getNumber() + " | " + ch.getNumber() + " HD";
									} else {
										name = sisterChannel.getFullName();
										// VDTRMASTER-5697
										number =  ch.getNumber() + " | " + sisterChannel.getNumber() + " HD";
									}
								} else {
									try {
										if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
											number = String.valueOf(ch.getNumber()) + " UHD";
										} else if (ch.getDefinition() == TvChannel.DEFINITION_HD) {
											number = String.valueOf(ch.getNumber()) + " HD";
										} else {
											number = String.valueOf(ch.getNumber());
										}
										name = ch.getFullName();
									} catch (RemoteException e) {
										e.printStackTrace();
									}
								}
								name = TextUtil.shorten(name, fm17, 175);
							} catch (RemoteException e) {
								Log.print(e);
							}
						} else {
							number = BaseUI.getOptionText(Definitions.LAST_CHANNEL);
						}
					} else {
						number = BaseUI.getOptionText(Definitions.LAST_CHANNEL);
						name = TextUtil.EMPTY_STRING;

					}

					g.drawString(number, 563, 132 + step);
					g.drawString(name, 666, 132 + step);
				}
			}
			g.drawImage(bgBottomImg, 547, 127 + step + 10, 851, 127 + step + 37, 0, 11, 304, 38, c);
		}
	}

	/**
	 * Paint a selected content.
	 * 
	 * @param g
	 *            Graphics to paint.
	 * @param parent
	 *            parent UI.
	 */
	public void paintSelectedContent(Graphics g, BaseUI parent) {
		ProgramGuideUI ui = (ProgramGuideUI) parent;
		Hashtable preTable = ui.getPreferences();

		int idx = ui.getStartIdx() + parent.getFocus();
		int step = 0;
		if (idx < 5) {
			step = parent.getFocus() * 37 - 14;
		} else {
			step = parent.getFocus() * 37 + 10 - 14;
		}
		
		Preference pre = (Preference) preTable.get(preferenceIds[idx]);

		if (pre != null) {
			g.setFont(f17);
			g.setColor(focusNameColor);
			String name = pre.getName();
			if (name.indexOf("|") > -1) {
				String[] names = TextUtil.tokenize(name, "|");
				g.drawString(names[0], NAME_X, 161 + step - 9);
				g.drawString(names[1], NAME_X, 161 + step + 9);
			} else {
				String[] names = TextUtil.split(name, g.getFontMetrics(), 260);
				if (names.length > 1) {
					g.drawString(names[0], NAME_X, 161 + step - 9);
					g.drawString(names[1], NAME_X, 161 + step + 9);
				} else {
					g.drawString(name, NAME_X, 161 + step);
				}
			}

			g.setFont(f18);
			g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
			String value = pre.getCurrentValue();
			if (preferenceIds[idx].equals(PreferenceNames.POWER_ON_CHANNEL)) {

				if (ui.isEnteringNumber()) {
					g.drawString(ui.getEnteringNumber(), VALUE_X, 161 + step);
				} else {
					String chNumber = TextUtil.EMPTY_STRING;
					String chName = TextUtil.EMPTY_STRING;
					EpgService epgService = CommunicationManager.getInstance().getEpgService();
					TvChannel powerOnChannel = null;
					try {
						powerOnChannel = epgService.getChannel(value);
						if (powerOnChannel != null) {
							chNumber = String.valueOf(powerOnChannel.getNumber());
						}
					} catch (RemoteException e) {
						Log.print(e);
					}
					if (!chNumber.equals(TextUtil.EMPTY_STRING)) {
						try {
							TvChannel sisterChannel = powerOnChannel.getSisterChannel();
							if (sisterChannel != null && PreferenceController.getInstance().isChannelGrouped()) {
								// has sister channel
								if (powerOnChannel.isHd()) {
									chName = powerOnChannel.getFullName();
									chNumber =  sisterChannel.getNumber() + " | " + powerOnChannel.getNumber() + " HD";
								} else {
									chName = sisterChannel.getFullName();
									chNumber =  powerOnChannel.getNumber() + " | " + sisterChannel.getNumber() + " HD";
								}
							} else {
								try {
									if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_4K) {
										chNumber = powerOnChannel.getNumber() + " UHD";
									} else if (powerOnChannel.getDefinition() == TvChannel.DEFINITION_HD) {
										chNumber = powerOnChannel.getNumber() + " HD";
									}
									chName = powerOnChannel.getFullName();
								} catch (RemoteException e) {
									e.printStackTrace();
								}
							}
							
							String mixedStr = chNumber + "   " + chName;
							mixedStr = TextUtil.shorten(mixedStr, fm17, 250);
							g.drawString(mixedStr, VALUE_X, 161 + step);
						} catch (RemoteException e) {
							Log.print(e);
						}
					} else {
						value = BaseUI.getOptionText(Definitions.LAST_CHANNEL);
						g.drawString(value, VALUE_X, 161 + step);
					}
				}
			} else {
				value = BaseUI.getOptionText(value);
				g.drawString(value, VALUE_X, 161 + step);
			}
		}
	}
}

/**
 * @(#)PVRRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.PVRUI;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class render a preferences for PVR.
 * @author Woojung Kim
 * @version 1.1
 */
public class PVRRenderer extends NormalSettingsBackRenderer {
    /** a image for input. */
    private Image inputImg;
    /** a image for input to have a focus. */
    private Image inputFocusImg;
    /** a image for dimmed input to have a focus. */
    private Image inputFocusDimImg;
    /** a image for secondly input. */
    private Image input2Img;
    /** a image for secondly input to have a focus. */
    private Image inputFocus2Img;
    /** a image for secondly dimmed input to have a focus. */
    private Image inputFocus2DimImg;
    /** a image for secondly off input. */
    private Image input2OffImg;
    /** a image for off input. */
    private Image inputOffImg;

    /** a string array for preference id's. */
    private String[] preferenceIds;
    /** a string for title. */
    private String title;

    /**
     * Prepare a renderer.
     * @param c parent UI.
     */
    public void prepare(UIComponent c) {
        PVRUI ui = (PVRUI) c;
        preferenceIds = ui.preferenceIds;
        String[] history = BaseUI.getHistory();
        title = history[history.length - 1];

        inputImg = BaseUI.getImage("input_210_sc.png");
        inputFocusImg = BaseUI.getImage("input_210_foc2.png");
        inputFocusDimImg = BaseUI.getImage("input_210_foc_dim.png");
        input2Img = BaseUI.getImage("input_210.png");
        inputFocus2Img = BaseUI.getImage("input_210_foc.png");
        inputFocus2DimImg = BaseUI.getImage("input_210_foc_dim.png");
        input2OffImg = BaseUI.getImage("input_210_dim.png");
        inputOffImg = BaseUI.getImage("input_210_sc_dim.png");
        super.prepare(c);
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        PVRUI ui = (PVRUI) c;
        Hashtable preTable = ui.getPreferences();

        g.setColor(titleColor);
        g.setFont(f26);
        g.drawString(title, 55, 116);

        for (int i = 0; i < preferenceIds.length; i++) {
            int step = i * 48;
            Preference pre = (Preference) preTable.get(preferenceIds[i]);
            g.setFont(f17);
            g.setColor(nameColor);

            if (pre != null) {
                String name = pre.getName();
                if (name.indexOf("|") > -1) {
                    String[] names = TextUtil.tokenize(name, "|");
                    g.drawString(names[0], NAME_X, 161 + step - 9);
                    g.drawString(names[1], NAME_X, 161 + step + 9);
                } else {
                    g.drawString(name, NAME_X, 161 + step);
                }

                g.setFont(f18);
                if (i == c.getFocus()) {
                    g.setColor(focusNameColor);
                    if (i == 4) {
                        g.drawImage(inputFocus2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputFocusImg, INPUT_X, 141 + step, c);
                    }
                } else {
                    g.setColor(dimmedNameColor);
                    if (i == 4) {
                        g.drawImage(input2Img, INPUT_X, 141 + step, c);
                    } else {
                        g.drawImage(inputImg, INPUT_X, 141 + step, c);
                    }
                }

                String value = pre.getCurrentValue();
//                if (pre.getID().equals(PreferenceNames.TERMINAL_NAME) && value.equals(Definitions.PVR_OTHER_NAME)) {
//                    Preference nPre = (Preference) preTable.get(PreferenceNames.TERMINAL_OTHER_NAME);
//                    if (nPre != null && nPre.getCurrentValue() != null) {
//                        value = nPre.getCurrentValue();
//                    } else {
//                        value = BaseUI.getOptionText(value);
//                    }
//                } else {
//                    value = BaseUI.getOptionText(value);
//                }
                value = BaseUI.getOptionText(value);
                if (value != null) {
                    g.drawString(value, VALUE_X, 162 + step);
                }
            }
        }
    }

    /**
     * Paint a selected content.
     * @param Graphics to paint.
     * @param parent preant UI.
     */
    public void paintSelectedContent(Graphics g, BaseUI parent) {
        PVRUI ui = (PVRUI) parent;
        Hashtable preTable = ui.getPreferences();

        int idx = parent.getFocus();
        int step = idx * 48;
        Preference pre = (Preference) preTable.get(preferenceIds[idx]);

        if (pre != null) {
            g.setFont(f17);
            g.setColor(focusNameColor);
            String name = pre.getName();
            if (name.indexOf("|") > -1) {
                String[] names = TextUtil.tokenize(name, "|");
                g.drawString(names[0], NAME_X, 161 + step - 9);
                g.drawString(names[1], NAME_X, 161 + step + 9);
            } else {
                g.drawString(name, NAME_X, 161 + step);
            }

            g.setFont(f18);
            if (idx == 4) {
                g.drawImage(inputFocus2DimImg, INPUT_X, 141 + step, ui);
            } else {
                g.drawImage(inputFocusDimImg, INPUT_X, 141 + step, ui);
            }
            String value = pre.getCurrentValue();

//            if (pre.getID().equals(PreferenceNames.TERMINAL_NAME) && value.equals(Definitions.PVR_OTHER_NAME)) {
//                Preference nPre = (Preference) preTable.get(PreferenceNames.TERMINAL_OTHER_NAME);
//                if (nPre != null && nPre.getCurrentValue() != null) {
//                    value = nPre.getCurrentValue();
//                } else {
//                    value = BaseUI.getOptionText(value);
//                }
//            } else {
//                value = BaseUI.getOptionText(value);
//            }
            value = BaseUI.getOptionText(value);
            if (value != null) {
            	g.drawString(value, VALUE_X, 162 + step);
            }
        }
    }
}

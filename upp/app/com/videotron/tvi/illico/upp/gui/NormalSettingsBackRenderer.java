/**
 * @(#)NormalSettingsBackRenderer.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.ui.BaseUI;

/**
 * This class render a background.
 * @author Woojung Kim
 * @version 1.1
 */
public class NormalSettingsBackRenderer extends BaseRenderer {
    /** a image for background. */
    private Image bgImg;
    /** a image for hotkey background. */
    private Image hotkeyBgImg;
    /** a image for description shadow. */
    private Image descShImg;
    /** a image for shadow on top. */
//    private Image topShImg;

    /** a boolean to indicate whether description is painted or not. */
    private boolean hasExplain;

    /**
     * Constructor. set a backgournd image and hasExplain set a true by default.
     */
    public NormalSettingsBackRenderer() {
        hasExplain = true;
    }

    public void prepare(UIComponent c) {
        bgImg = BaseUI.getImage("bg.jpg");
        hotkeyBgImg = BaseUI.getImage("01_hotkeybg.png");
        descShImg = BaseUI.getImage("08_descr_sh.png");
//        topShImg = BaseUI.getImage("08_top_sh.png");
        setBackground(bgImg);
        
        super.prepare(c);
    }
    
    /**
     * Set a value of time style. if hasExplain is true, display a image for description space.
     * @param explain true if screen has a explain. false otherwise.
     */
    public void setHasExpalin(boolean explain) {
        this.hasExplain = explain;
    }

    /**
     * Paint a renderer.
     * @param g Graphics to paint.
     * @param c parent UI.
     */
    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

//        g.drawImage(topShImg, 0, 77, c);
        g.drawImage(hotkeyBgImg, 367, 466, c);

        if (hasExplain) {
            g.drawImage(descShImg, 0, 411, c);
        }
    }
}

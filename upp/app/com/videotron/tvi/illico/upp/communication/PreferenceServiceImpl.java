/**
 * @(#)PreferenceServiceImpl.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.communication;

import java.awt.EventQueue;
import java.awt.Point;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.BlockTime;
import com.videotron.tvi.illico.ixc.upp.BlockTimeUpdateListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.AutomaticPowerDownManager;
import com.videotron.tvi.illico.upp.controller.BlockChannelController;
import com.videotron.tvi.illico.upp.controller.ChannelPreferenceController;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PinEnablerController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.controller.ProxyController;
import com.videotron.tvi.illico.upp.controller.STBTimerController;
import com.videotron.tvi.illico.upp.controller.VbmLogController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The PreferenceServiceImpl class implements a PreferenceService to
 * communication with internal and external.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class PreferenceServiceImpl implements PreferenceService {
	private CheckRightThread checkRightThread;
	private CheckRightFilterThread checkRightFilterThread;

	private PinEnablerListener targetListener;
	private PinEnablerListenerImpl pinListener = new PinEnablerListenerImpl();

	private boolean isDispalyedAboutNeedAdmin = false;

	private String lastZoomMode;
	
	private String lastTVPictureFormat;
	
	private boolean is1080pMode;

	/**
	 * Implements to set the value of preference name.
	 * 
	 * @see PreferenceService#setPreferenceValue(String,
	 *      String)
	 * @param preferenceName
	 *            The preference name to save
	 * @param value
	 *            the value to saved
	 * @return the result to save the value;
	 * @throws RemoteException
	 *             The remote exception
	 */
	public boolean setPreferenceValue(String preferenceName, String value) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setPreferenceValue(): name=" + preferenceName + "\t value=" + value);
		}
		if (PreferenceController.getInstance().setPreferenceValue(preferenceName, value)) {
			PreferenceActionController.getInstance().doAction(preferenceName, value);
			return true;
		}

		return false;
	}
	
	
	/**
	 * Implements to get the value of preference name.
	 * 
	 * @see PreferenceService#getPreferenceValue(String)
	 * 
	 * @param prefernceNmae the preference name to get.
	 * @return the value of preference name
	 * @throws RemoteException the remote exception.
	 */
	public String getPreferenceValue(String preferenceName) throws RemoteException {
		
		return PreferenceController.getInstance().getPreferenceValue(preferenceName);
	}

	/**
	 * Implements to add the preference listener.
	 * 
	 * @param pListener
	 *            the listener
	 * @param appId
	 *            the app id
	 * @param preferenceNames
	 *            the preference names
	 * @param defaultValues
	 *            the default values
	 * @return the preference values to request
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#addPreferenceListener(com.videotron.tvi.illico.upp.ixc.PreferenceListener,
	 *      String, String[], String[])
	 */
	public String[] addPreferenceListener(final PreferenceListener pListener, final String appId,
			String[] preferenceNames, String[] defaultValues) throws RemoteException {

		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addPreferenceListener()");
		}
		if (defaultValues == null) {
			defaultValues = new String[preferenceNames.length];
		}

		String[] retValues = PreferenceController.getInstance().getAppPreferenceData(appId, preferenceNames,
				defaultValues);

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: addPreferenceListener: App : " + appId);

			if (retValues != null) {
				Log.printDebug("PreferenceServiceImpl: addPreferenceListener: retValues size : " + retValues.length);
				for (int i = 0; i < retValues.length; i++) {
					Log.printDebug("PreferenceServiceImpl: addPreferenceListener: retValues[" + i + "]=" + retValues[i]);
				}
			} else {
				Log.printDebug("PreferenceServiceImpl: addPreferenceListener: retValues is null");
			}
		}

		if (pListener != null) {
			// use invokeLater because in getAppPreferenceData
			// notifyPreferenceUpdated method can be called.
			EventQueue.invokeLater(new Runnable() {

				public void run() {
					if (Log.INFO_ON) {
						Log.printInfo("PreferenceServeiceImpl: call addProxyListsnser");
					}
					ProxyController.getInstance().addProxyListener(appId, pListener);

				}
			});
		}

		return retValues;
	}

	/**
	 * Implements to check that has the right and filter.
	 * 
	 * @param l
	 *            The listener to receive a response.
	 * @param appName
	 *            Application name to be check a access right.
	 * @param filters
	 *            the filter list
	 * @param callLetter
	 *            if {@link RightFilter#BLOCKED_CHANNELS} be used, UPP needs a
	 *            current call letter to be used to compare.
	 * @param rating
	 *            if (@link {@link RightFilter#BLOCKED_RATING} be used, UPP
	 *            needs a current rating to be used to compare.
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#checkRightFilter(String,
	 *      String[] , String[])
	 */
	public void checkRightFilter(RightFilterListener l, String appName, String[] filters, String callLetter,
			String rating) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: checkRightFilter");
		}

		checkRightFilterThread = new CheckRightFilterThread(l, appName, filters, callLetter, rating);
		checkRightFilterThread.start();
	}

	/**
	 * Implements to check that has a the right.
	 * 
	 * @param l
	 *            The listener to receive a response.
	 * @param appName
	 *            Application name to be check a access right.
	 * @throws RemoteException
	 *             the remote exception
	 */
	public void checkRight(RightFilterListener l, String appName) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: checkRight");
		}

		checkRightThread = new CheckRightThread(l, appName);
		checkRightThread.start();
	}

	/**
	 * Implements to hide PinEnablerUI.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#hidePinEnabler()
	 */
	public void hidePinEnabler() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: hidePinEnabler");
		}
		PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
	}

	/**
	 * Implements to hide settings menu.
	 * 
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#hideSettingsMenu()
	 */
	public void hideSettingsMenu() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: hideSettingsMenu");
		}
		PreferenceController.getInstance().hideUIComponent();
	}

	/**
	 * Implements to remove the preference listener.
	 * 
	 * @param pListener
	 *            the listener
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#removePreferenceListener(com.videotron.tvi.illico.upp.
	 *      ixc.PreferenceListener)
	 */
	public void removePreferenceListener(String appId, PreferenceListener pListener) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: removePreferenceListener");
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: removePreferenceListener: appId = " + appId + ", pListener = "
					+ pListener);
		}

		if (appId == null || pListener == null) {
			return;
		}

		ProxyController.getInstance().removeProxyListener(appId, pListener);
	}

	/**
	 * Implements to show a PinEnablerUI. This PinEnabler handle a PIN of ADMIN.
	 * if inputed pin is not for ADMIN, you will receive a FAIL.
	 * 
	 * @param listener
	 *            The listener to receive a result of PinEnablerUI.
	 * @param explain
	 *            The explain why need a PIN code.
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#showEnabler()
	 */
	public void showPinEnabler(PinEnablerListener listener, String[] explain) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: showPinEnabler");
		}
		isDispalyedAboutNeedAdmin = false;

		targetListener = listener;
		String title = BaseUI.getPopupText(KeyNames.TITLE_ENTER_PIN);
		PinEnablerController.getInstance().addPinEnablerListener(pinListener);
		PinEnablerController.getInstance().showPinEnabler(title, explain, false);
	}

	/**
	 * Implements to show settings menu.
	 * 
	 * @param params
	 *            the parameters to include a informations(request application
	 *            etc)
	 * @throws RemoteException
	 *             the remote exception
	 * @see com.videotron.tvi.illico.upp.ixc.PreferenceService#showSettingsMenu()
	 */
	public void showSettingsMenu(String[] params) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: showSettingsMenu");

			Log.printInfo("PreferenceServiceImpl: " + new java.io.OutputStreamWriter(System.out).getEncoding());
		}

		if (Log.DEBUG_ON) {
			if (params != null) {
				Log.printDebug("PerferenceServiceImpl: showSettingsMenu(): params length : " + params.length);
				if (params.length > 0) {
					for (int i = 0; i < params.length; i++) {
						Log.printDebug("PerferenceServiceImpl: showSettingsMenu(): param : " + params[i]);
					}
				}
			} else {
				Log.printDebug("PerferenceServiceImpl: showSettingsMenu(): param : " + params);
			}
		}

		PreferenceController.getInstance().showLoading();

		if (params != null) {
			DataCenter.getInstance().put("PARAMS", params);
			if (params.length > 0 && params[0] != null) {
				DataCenter.getInstance().put(VbmLogController.PARENT_APPLICATION_NAME, params[0]);
			}
		}

		if (params != null && params.length > 1) {
			if (params[0] == null) {
				if (params[1].equals(PreferenceService.PARENTAL_CONTROLS)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_PARENTAL_CONTROLS, false);
				} else if (params[1].equals(PreferenceService.PROGRAM_GUIDE)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_PROGRAM_GUIDE, false);
				} else {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, false);
				}
			} else {
				if (params[1].equals(PreferenceService.PARENTAL_CONTROLS)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_PARENTAL_CONTROLS, false);
				} else if (params[0].equalsIgnoreCase("EPG") && params[1].equals(PreferenceService.PROGRAM_GUIDE)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_PROGRAM_GUIDE, false);
				} else if (params[0].equalsIgnoreCase(MonitorService.REQUEST_APPLICATION_HOT_KEY)
						&& params[1].equals("ASPECT")) {
					// PreferenceController.getInstance().showUIComponent(BaseUI.UI_PROGRAM_GUIDE,
					// false);
					PreferenceActionController.getInstance().requestNextZoomMode();
				} else if (params[0].equalsIgnoreCase("VOD") && params[1].equals(PreferenceService.VOD)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_VOD, false);
				} else if (params[0].equals("Callerid") && params[1].equals(PreferenceService.CALLER_ID)) {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_CALLER_ID, false);
				} else if (params[0].equals("NOTIFICATION") && params[1].equals(PreferenceService.EQUIPMENT)) {
					PreferenceController.getInstance().prepareUIComponent(BaseUI.UI_MY_SETTINGS, true);
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, false);
				} else {
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, false);
				}
			}
		} else {
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, false);
		}

		PreferenceController.getInstance().hideLoading();
		
		VbmLogController.getInstance().sendLog(VbmLogController.PARENT_APPLICATION_NAME);
		VbmLogController.getInstance().sendLog(VbmLogController.SESSION_BEGIN_DATE_TIME);
	}

	/**
	 * Implements a {@link PreferenceService#isExistedAdminPIN()}.
	 * 
	 * @see PreferenceService#isExistedAdminPIN()
	 * @return true if Admin PIN exsited, false otherwise.
	 * @throws RemoteException
	 *             the remote exception
	 */
	public boolean isExistedAdminPIN() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: isExistedAdminPIN");
		}
		User admin = UserManager.getInstance().loadUser(User.ID_ADMIN);
		if (admin.hasPin()) {
			return true;
		}
		return false;
	}

	/**
	 * Implements a {@link PreferenceService#setAdminPIN(String)}.
	 * 
	 * @param pin
	 *            The String to include a 4 digit number.
	 * @throws RemoteException
	 *             the remote exception
	 */
	public void setAdminPIN(String pin) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setAdminPIN: pin=" + pin);
		}
		User admin = UserManager.getInstance().loadUser(User.ID_ADMIN);
		admin.changePin(pin);
	}

	/**
	 * Implements a {@link PreferenceService#equalsWithAdminPIN(String)}.
	 * 
	 * @param pin
	 *            4-digit string which want to compare with Admin PIN.
	 * @return true if equals Admin PIN, false otherwise.
	 * @throws RemoteException
	 */
	public boolean equalsWithAdminPIN(String pin) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: equalsWithAdminPIN: pin=" + pin);
		}
		User admin = UserManager.getInstance().loadUser(User.ID_ADMIN);
		if (admin.hasPin() && admin.isValidate(pin)) {
			return true;
		}

		return false;
	}

	/**
	 * Return a result to check whether exist a Admin PIN or not.
	 * 
	 * @return true if Admin PIN exist, false otherwise.
	 * @throws RemoteException
	 *             the remote exception
	 */
	public boolean isExistedFamilyPIN() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: isExistedFamilyPIN");
		}
		User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
		if (family.hasPin()) {
			return true;
		}
		return false;
	}

	/**
	 * Set a Family PIN. Implements a
	 * {@link PreferenceService#setFamilyPIN(String)}.
	 * 
	 * @param pin
	 *            a PIN String for Family PIN
	 * @throws RemoteException
	 *             The remote exception;
	 */
	public void setFamilyPIN(String pin) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setAdminPIN: pin=" + pin);
		}
		User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
		family.changePin(pin);
	}

	/**
	 * Return whether inputed PIN string equals Family PIN or not. Implements a
	 * {@link PreferenceService#equalsWithFamilyPIN(String)}
	 * 
	 * @param pin
	 *            4-digit string witch want to compare with Family PIN.
	 * @return true if equals Family PIN. false otherwise.
	 * @throws RemoteException
	 *             The remote exception
	 */
	public boolean equalsWithFamilyPIN(String pin) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: equalsWithFamilyPIN: pin=" + pin);
		}
		User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
		if (family.hasPin() && family.isValidate(pin)) {
			return true;
		}

		return false;
	}

	/**
	 * Adds a BlockTimeUpdateListener. Only use by Monitor.
	 * 
	 * @param l
	 *            listener to be receive a event of update.
	 * @return BlockTime[] a object to have a information of Block.
	 * @throws RemoteException
	 *             remote Exception.
	 */
	public BlockTime[] addBlockTimeUpdateListener(BlockTimeUpdateListener l) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addBlockTimeUpdateListener : " + l);
		}
		ProxyController.getInstance().setBlockTimeUpdateListener(l);

		BlockTime[] blockTimes = STBTimerController.getInstance().getBlockTimes();

		return blockTimes;
	}

	/**
	 * Set a postal code.
	 * 
	 * @param postalCode
	 *            a string of postal code.
	 * @throws RemoteException
	 *             The remote exception.
	 */
	public void setPostalCode(String postalCode) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setPostalCode : " + postalCode);
		}
		DataManager.getInstance().setPostalCode(postalCode);
	}

	/**
	 * Get a Postal code.
	 * 
	 * @return a postal code string to be saved.
	 * @throws RemoteException
	 *             The remote exception.
	 */
	public String getPostalCode() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: getPostalCode");
		}
		return DataManager.getInstance().getPostalCode();
	}

	/**
	 * Save a current zoom mode.
	 * 
	 * @see {@link #restoreZoomMode()}, {@link #setNormalZoomMode()}
	 * @throws RemoteException
	 */
	public void saveZoomMode() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: saveZoomMode()");
		}
		String currentZoomMode = DataManager.getInstance().getPreference(PreferenceNames.VIDEO_ZOOM_MODE)
				.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: saveZoomMode: currentZoomMode : " + currentZoomMode);
		}
		lastZoomMode = currentZoomMode;
	}

	/**
	 * Set a Normal for Zoom mode.
	 * 
	 * @see {@link #restoreZoomMode()}
	 * @throws RemoteException
	 */
	public void setNormalZoomMode() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setNormalZoomMode()");
		}
		//R7.3 VDTRMASTER-6047, VDTRMASTER-6044
		DataManager.getInstance().setPreference(PreferenceNames.VIDEO_ZOOM_MODE, Definitions.VIDEO_ZOOM_MODE_NORMAL);
		PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_ZOOM_MODE,
				Definitions.VIDEO_ZOOM_MODE_NORMAL);
	}

	/**
	 * Restore a last zoom mode which is before call
	 * {@link #setNormalZoomMode()};
	 * 
	 * @throws RemoteException
	 */
	public void restoreZoomMode() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: restoreZoomMode()");
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: restoreTVPictureFormat: lastZoomMode : " + lastZoomMode);
		}

		if (lastZoomMode != null) {
			DataManager.getInstance().setPreference(PreferenceNames.VIDEO_ZOOM_MODE, lastZoomMode);
			PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_ZOOM_MODE, lastZoomMode);
			lastZoomMode = null;
		}
	}
	
	public void saveTVPictureFormat() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: saveTVPictureFormat()");
		}
		String currentlastTVPictureFormat = DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)
				.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: saveTVPictureFormat: currentlastTVPictureFormat : " + currentlastTVPictureFormat);
		}
		lastTVPictureFormat = currentlastTVPictureFormat;
	}

	public void restoreTVPictureFormat() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: restoreTVPictureFormat()");
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceServiceImpl: restoreTVPictureFormat: lastTVPictureFormat : " + lastTVPictureFormat);
		}

		is1080pMode = false;
		if (lastTVPictureFormat != null) {
			DataManager.getInstance().setPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, lastTVPictureFormat);
			PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, lastTVPictureFormat);
			lastTVPictureFormat = null;
		}
	}
	
	public void setFullHDTVPictureFormat() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: setNormalZoomMode()");
		}
		PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
				Definitions.VIDEO_TV_PICTURE_FORMAT_1080P);
		is1080pMode = true;
	}
	
	public boolean is1080pMode() {
		
		return is1080pMode;
	}

	/**
	 * This class run a
	 * {@link PreferenceController#checkRightFilter(RightFilterListener, String, String[], String, String)}
	 * via Thread.
	 * 
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class CheckRightFilterThread extends Thread {
		/** a listener to receive a result. */
		RightFilterListener listener;
		/** a application name. */
		String appName;
		/** a filters to want to check. */
		String[] filters;
		/** a call letter to want to check. */
		String callLetter;
		/** a raing to want to check. */
		String rating;

		/**
		 * Constructor has a parameter to use when call
		 * {@link PreferenceController#checkRightFilter(RightFilterListener, String, String[], String, String)}
		 * .
		 * 
		 * @param l
		 *            a listener to receive a result.
		 * @param aName
		 *            a application name
		 * @param f
		 *            a filter name.
		 * @param cLetter
		 *            a current call letter.
		 * @param r
		 *            a current rating.
		 */
		CheckRightFilterThread(final RightFilterListener l, final String aName, final String[] f, final String cLetter,
				final String r) {
			this.listener = l;
			this.appName = aName;
			this.filters = f;
			this.callLetter = cLetter;
			this.rating = r;
		}

		/**
		 * Call a
		 * {@link PreferenceController#checkRightFilter(RightFilterListener, String, String[], String, String)}
		 * .
		 */
		public void run() {
			try {
				PreferenceController.getInstance().checkRightFilter(listener, appName, filters, callLetter, rating);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * This class call a
	 * {@link PreferenceController#checkRight(RightFilterListener, String)} via
	 * Thread.
	 * 
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class CheckRightThread extends Thread {
		/** a listener to receive a result. */
		RightFilterListener listener;
		/** a application name. */
		String appName;

		/**
		 * Constructor has a parameter to use when call run().
		 * 
		 * @param l
		 *            listener to receive a result.
		 * @param appName
		 *            application name.
		 */
		CheckRightThread(RightFilterListener l, String appName) {
			this.listener = l;
			this.appName = appName;
		}

		/**
		 * Calls
		 * {@link PreferenceController#checkRight(RightFilterListener, String)}.
		 */
		public void run() {
			try {
				PreferenceController.getInstance().checkRight(listener, appName);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Implement a PinEnablerListener.
	 * 
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class PinEnablerListenerImpl implements PinEnablerListener {
		/**
		 * When called showPinEnabler, received Pin event and if PIN means
		 * Admin, return Sucesss, return FAIL otherwise.
		 * 
		 * @param response
		 *            a result of PinEnabler.
		 * @param detail
		 *            a detail of result.
		 * @throws RemoteException
		 *             a remote exception.
		 */
		public void receivePinEnablerResult(int response, String detail) throws RemoteException {
			if (Log.INFO_ON) {
				Log.printInfo("PreferenceServiceImpl: PinEnablerListenerImpl: response = " + response + "\t detail = "
						+ detail);
			}
			if (targetListener != null) {
				if (response == PreferenceService.RESPONSE_SUCCESS) {
					if (detail.equals(User.ID_ADMIN)) {
						targetListener.receivePinEnablerResult(response, detail);
					} else {
						// targetListener.receivePinEnablerResult(PreferenceService.RESPONSE_FAIL,
						// detail);

						PinEnablerController.getInstance().addPinEnablerListener(new PinEnablerListenerImpl());
						String temp = BaseUI.getPopupText(KeyNames.EXPLAIN_CONTENT_NEED_ADMIN_PIN);
						String[] explain = TextUtil.split(temp, BaseRenderer.fm18, 310, '|');
						String title = BaseUI.getPopupText(KeyNames.TITLE_ENTER_ADMIN_PIN);

						if (isDispalyedAboutNeedAdmin) {
							PinEnablerController.getInstance().showPinEnablerInWrongPin(title, explain, false);
						} else {
							isDispalyedAboutNeedAdmin = true;
							PinEnablerController.getInstance().showPinEnabler(title, explain, false);
						}
					}
				} else {
					targetListener.receivePinEnablerResult(response, detail);
				}
			}
		}
	}

	public void addBlockChannel(String callLetter) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addBlockChannel");
		}
		ChannelPreferenceController.getInstance().addBlockChannel(callLetter);
	}

	public void removeBlockChannel(String callLetter) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: removeBlockChannel");
		}
		ChannelPreferenceController.getInstance().removeBlockChannel(callLetter);
	}

	public boolean resetAPDTimer() throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: resetAPDTimer");
		}
		return AutomaticPowerDownManager.getInstance().resetAPDTimer();
	}
	
	public boolean resetAPDTimerWithKeycode(int keycode) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: resetAPDTimerWithKeycode");
		}
		return AutomaticPowerDownManager.getInstance().resetAPDTimerWithKeycode(keycode);
	}

	public void addFavoriteChannel(String callLetter) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addFavoriteChannel");
		}
		ChannelPreferenceController.getInstance().addFavoriteChannel(callLetter);
	}

	public void removeFavoriteChannel(String callLetter) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: removeFavoriteChannel");
		}
		ChannelPreferenceController.getInstance().removeFavoriteChannel(callLetter);
	}


	public void addBlockChannels(String[] callLetters) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addBlockChannels");
		}
		ChannelPreferenceController.getInstance().addBlockChannels(callLetters);
	}


	public void removeBlockChannels(String[] callLetters) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: removeBlockChannels");
		}
		ChannelPreferenceController.getInstance().removeBlockChannels(callLetters);
	}


	public void addFavoriteChannels(String[] callLetters) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: addFavoriteChannels");
		}
		ChannelPreferenceController.getInstance().addFavoriteChannels(callLetters);
	}


	public void removeFavoriteChannels(String[] callLetters) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceServiceImpl: removeFavoriteChannels");
		}
		ChannelPreferenceController.getInstance().removeFavoriteChannels(callLetters);
	}

}

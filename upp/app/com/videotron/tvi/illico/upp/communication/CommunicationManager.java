/**
 * @(#)CommunicationManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.communication;

import java.rmi.Remote;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * The CommunicationManager class lookup a remote Service of Other Applcation.
 * @author Woojung Kim
 * @version 1.1
 */
public class CommunicationManager {

    /** The XletContext of UPP Application. */
    private XletContext xletContext;
    /** The instance of CommunicationManager. */
    private static CommunicationManager instance = new CommunicationManager();

    /** The DaemonService to communicate with Daemon App. */
    DaemonService daemonService;

    /** The MonitorService to communicate with Monitor App. */
    MonitorService monitorService;

    /** The EpgService to communication with EPG App. */
    EpgService epgService;

    /** The KeyboardService to communication with Keyboard App. */
    KeyboardService keyboardService;

    /** The NotificationService to communication with Notification App. */
    NotificationService notiService;

    IxcLookupWorker ixcWorker;

    /**
     * Instantiates a new CommunicationManager.
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Start a lookups.
     * @param xletContext
     */
    public void start(XletContext xletContext) {
        this.xletContext = xletContext;
        this.ixcWorker = new IxcLookupWorker(xletContext);

        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME);
        ixcWorker.lookup("/1/2040/", DaemonService.IXC_NAME);
        ixcWorker.lookup("/1/2026/", EpgService.IXC_NAME);
        ixcWorker.lookup("/1/2050/", KeyboardService.IXC_NAME);
        ixcWorker.lookup("/1/2110/", NotificationService.IXC_NAME);
        ixcWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME);
        ixcWorker.lookup("/1/2100/", StcService.IXC_NAME);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME);
        ixcWorker.lookup("/1/2085/", VbmService.IXC_NAME);
        if (Environment.SUPPORT_DVR) {
            ixcWorker.lookup("/1/20A0/", PvrService.IXC_NAME);
        }
    }

    /**
     * Dispose a resources.
     */
    public void dispose() {
        daemonService = null;
    }

    /**
     * Return a DaemonService.
     * @return The DaemonService object to be lookup.
     */
    public DaemonService getDaemonService() {
        if (daemonService == null) {
            daemonService = (DaemonService) DataCenter.getInstance().get(DaemonService.IXC_NAME);
        }
        return daemonService;
    }

    /**
     * Return a MonitorService.
     * @return The MonitorService object to be lookup.
     */
    public MonitorService getMonitorService() {
        if (monitorService == null) {
            monitorService = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        }
        return monitorService;
    }

    /**
     * Return a EpgService.
     * @return The EpgService object to be lookup.
     */
    public EpgService getEpgService() {
        if (epgService == null) {
            epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        }

        return epgService;
    }

    /**
     * Return a KeyboardService.
     * @return The KeyboardService object to be lookup.
     */
    public KeyboardService getKeyboardService() {
        if (keyboardService == null) {
            keyboardService = (KeyboardService) DataCenter.getInstance().get(KeyboardService.IXC_NAME);
        }

        return keyboardService;
    }
}

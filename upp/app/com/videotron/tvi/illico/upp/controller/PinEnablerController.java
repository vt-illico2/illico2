/**
 * @(#)PinEnablerController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.PinEnablerUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * The PinEnablerController class handle a PinEnablerUI. The PinEnablerController show and hide a PinEanbler. And notify
 * a result to a application that requested a PinEanbler..
 * @author Woojung Kim
 * @version 1.1
 */
public final class PinEnablerController extends LayeredWindow implements LayeredKeyHandler {

    /** */
    private static final long serialVersionUID = 1L;

    /** The instance of PinEnablerController. */
    private static PinEnablerController instance = new PinEnablerController();

    /** The component of PinEnablerUI. */
    private UIComponent pinEnablerUI;
    /** The component of CreateAdminUI. */
    private UIComponent createAdminUI;
    /** The listener to receive a response of PinEnablerUI. */
    private PinEnablerListener pListener;

    /** The LayeredUI to use PinEnablerUI. */
    private LayeredUI layeredUI;

    /** if exist, a current UIcomponet. */
    private UIComponent currentUI;

    /**
     * Instantiates a new PinEnablerUI Controller.
     */
    private PinEnablerController() {
        setBounds(Constants.SCREEN_BOUNDS);
        pinEnablerUI = PreferenceController.getInstance().createUIComponent(BaseUI.UI_PINENABLER);
        createAdminUI = PreferenceController.getInstance().createUIComponent(BaseUI.UI_CREATE_ADMIN);
        layeredUI = WindowProperty.PINENABLER.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
    }

    /**
     * Gets the single instance of PinEnablerController.
     * @return instance of PinEnablerController
     */
    public static PinEnablerController getInstance() {
        return instance;
    }

    /**
     * clean up a resource.
     */
    public void dispose() {
        pListener = null;
        pinEnablerUI = null;
        createAdminUI = null;
        layeredUI = null;

    }

    /**
     * Shows PinEnablerUI.
     * @param title a title of current PIN popup.
     * @param explain texts to explain why PinEnablerUI is used.
     */
    public synchronized void showPinEnabler(String title, String[] explain) {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: showPinEnabler: title : " + title + "  explain : " + explain);
        }
        this.removeAll();

        User user = UserManager.getInstance().loadUser(User.ID_ADMIN);

        if (user.hasPin()) {
            ((PinEnablerUI) pinEnablerUI).setText(title, explain);
            ((PinEnablerUI) pinEnablerUI).setInnerRequest(true);
            currentUI = pinEnablerUI;

            currentUI.start();

            addComponent(currentUI);
            layeredUI.activate();
        } else {
            // create admin
            currentUI = createAdminUI;
            currentUI.start();
            addComponent(currentUI);
            layeredUI.activate();
        }
    }

    /**
     * Shows PinEnablerUI.
     * @param title a title of current PIN popup.
     * @param explain texts to explain why PinEnablerUI is used.
     * @param innerRequest The true if request is from Inner, false if not.
     */
    public synchronized void showPinEnabler(String title, String[] explain, boolean innerRequest) {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: showPinEnabler: title : " + title + "  explain : " + explain
                    + "\t innerRequest : " + innerRequest);
        }
        this.removeAll();

        User user = UserManager.getInstance().loadUser(User.ID_ADMIN);

        if (user.hasPin()) {
            ((PinEnablerUI) pinEnablerUI).setText(title, explain);
            ((PinEnablerUI) pinEnablerUI).setInnerRequest(innerRequest);
            currentUI = pinEnablerUI;
            currentUI.start();
            addComponent(currentUI);
            layeredUI.activate();
        } else {
            // create admin
            currentUI = createAdminUI;
            currentUI.start();
            addComponent(currentUI);
            layeredUI.activate();
        }
    }
    
    /**
     * Shows PinEnablerUI.
     * @param title a title of current PIN popup.
     * @param explain texts to explain why PinEnablerUI is used.
     * @param innerRequest The true if request is from Inner, false if not.
     */
    public synchronized void showPinEnablerInWrongPin(String title, String[] explain, boolean innerRequest) {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: showPinEnabler: title : " + title + "  explain : " + explain
                    + "\t innerRequest : " + innerRequest);
        }
        this.removeAll();

        User user = UserManager.getInstance().loadUser(User.ID_ADMIN);

        if (user.hasPin()) {
            ((PinEnablerUI) pinEnablerUI).setText(title, explain);
            ((PinEnablerUI) pinEnablerUI).setInnerRequest(innerRequest);
            ((PinEnablerUI) pinEnablerUI).startInWrongPin();
            currentUI = pinEnablerUI;
            addComponent(currentUI);
            layeredUI.activate();
        } else {
            // create admin
            currentUI = createAdminUI;
            currentUI.start();
            addComponent(currentUI);
            layeredUI.activate();
        }
    }

    /**
     * Hides PinEnablerUI.
     */
    public synchronized void hidePinEnabler() {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: hidePinEnabler");
        }
        if (currentUI != null) {
            removeComponent(currentUI);
            currentUI.stop();
            layeredUI.deactivate();
            currentUI = null;
        }
    }

    /**
     * Add a UIComponent into LayeredUI.
     * @param ui The UICompoent to add.
     */
    private void addComponent(UIComponent ui) {
        this.add(ui);
    }

    /**
     * Remove a UIComponent from LayeredUI.
     * @param ui The UICompoent to remove.
     */
    private void removeComponent(UIComponent ui) {
        this.remove(ui);
    }

    /**
     * Notify a response from PinEnablerUI.
     * @param response The one of {@link PreferenceService#RESPONSE_SUCCESS}, {@link PreferenceService#RESPONE_FAIL},
     *            {@link PreferenceService#RESPONSE_ERROR}, {@link PreferenceService#RESPONSE_CANCEL}
     * @param detail The detail message of error.
     */
    public void notifyPinEnablerResult(int response, String detail) {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: notifyPinEnablerReuslt: response : " + response
                    + "  detail : " + detail);
            Log.printInfo("PinEnablerController: notifyPinEnablerReuslt: pListener = " + pListener);
        }
        hidePinEnabler();
        try {
            if (pListener != null) {
            	// Can add a new PinEnablerListener in curListenr.receivePinenablerResult()
            	// so use a temporary refer value for PinEnablerListener.
            	PinEnablerListener curListener = pListener;
            	removePinEnablerListener();
            	
            	curListener.receivePinEnablerResult(response, detail);
            }

        } catch (RemoteException e) {
            Log.printError(e);
        }
    }

    /**
     * Add a listener to receive a response from PinEnablerUI.
     * @param l the listener to receive a response.
     */
    public void addPinEnablerListener(PinEnablerListener l) {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: addPinEnablerListener(): pListener = " + l);
        }
        pListener = l;
    }

    /**
     * Remove a current listener of PinEnablerListener.
     */
    public void removePinEnablerListener() {
        if (Log.INFO_ON) {
            Log.printInfo("PinEnablerController: removePinEnablerListener()");
        }
        pListener = null;
    }

    /**
     * Process a key event base on LayeredUI.
     * @param event The key event to input from RCU.
     * @return true if a key is used by PinEnablerController, false otherwise.
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int keyCode = event.getCode();

        switch (keyCode) {
        case OCRcEvent.VK_UP:
        case OCRcEvent.VK_DOWN:
        case OCRcEvent.VK_LEFT:
        case OCRcEvent.VK_RIGHT:
        case OCRcEvent.VK_ENTER:
        case OCRcEvent.VK_EXIT:
        case OCRcEvent.VK_CHANNEL_UP:
        case OCRcEvent.VK_CHANNEL_DOWN:
        case KeyCodes.LAST:
        case KeyCodes.MENU:
        case KeyCodes.PIP:
        case KeyCodes.VOD:
        case KeyCodes.SEARCH:
        case KeyCodes.SETTINGS:
        case KeyCodes.WIDGET:
        case KeyCodes.RECORD:
        case KeyCodes.STOP:
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_D:
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            if (currentUI != null) {
                return currentUI.handleKey(keyCode);
            }
            return true;
        default:
            return false;
        }
    }
}

/**
 * @(#)PreferenceController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.hardware.VideoOutputPort;
import org.ocap.hardware.device.FixedVideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputPortListener;
import org.ocap.hardware.device.VideoOutputSettings;
import org.ocap.hardware.device.VideoResolution;
import org.ocap.media.ClosedCaptioningAttribute;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.data.AccessRight;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.ui.AudioAccessibilityUI;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ParentalControlInfoUI;
import com.videotron.tvi.illico.upp.ui.TimeBasedRestrictionsUI;
import com.videotron.tvi.illico.upp.ui.CallerIDUI;
import com.videotron.tvi.illico.upp.ui.ChannelRestrictionUI;
import com.videotron.tvi.illico.upp.ui.ClosedCaptioningUI;
import com.videotron.tvi.illico.upp.ui.EquipmentSetupUI;
import com.videotron.tvi.illico.upp.ui.FavouriteChannelUI;
import com.videotron.tvi.illico.upp.ui.GeneralSettingsUI;
import com.videotron.tvi.illico.upp.ui.MySettingsUI;
import com.videotron.tvi.illico.upp.ui.PVRUI;
import com.videotron.tvi.illico.upp.ui.ParentalControlsUI;
import com.videotron.tvi.illico.upp.ui.PinEnablerUI;
import com.videotron.tvi.illico.upp.ui.CreateAdminUI;
import com.videotron.tvi.illico.upp.ui.ProgramGuideUI;
import com.videotron.tvi.illico.upp.ui.STBSleepTimeUI;
import com.videotron.tvi.illico.upp.ui.STBWakeUpTimeUI;
import com.videotron.tvi.illico.upp.ui.VODUI;
import com.videotron.tvi.illico.upp.ui.TVPictureFormatUI;
import com.videotron.tvi.illico.upp.util.ClosedCaptioningUtil;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Class PreferenceController is a starting point and handle a functions of
 * UPP..
 *
 * @author Woojung Kim
 * @version 1.3
 */
public final class PreferenceController implements KeyListener, DataUpdateListener, ServiceStateListener,
		ChannelEventListener, PowerModeChangeListener {

	/** The mode while check a Access Right. */
	public static final byte MODE_CHECK_RIGHT_ONLY = 0x00;
	/** The mode while check a Access Right and Filter. */
	public static final byte MODE_CHECK_RIGHT_AND_FILTER = 0x01;

	/** The instance of PreferenceController. */
	private static PreferenceController instance = new PreferenceController();

	/** The current UICompoent. */
	private BaseUI currentUI;
	/** The Hashtable to have UICompoent list. */
	private Hashtable uiTable;

	/** The listener to notify a result of Access Right and Filter. */
	private RightFilterListener rightListener;
	/** The right name of Access Right. */
	private String rightName;
	/** The filters to check for right name. */
	private String[] filterNames;
	/** The channel to check with BLOCKED_CHANNELS filter. */
	private String filterChannel;
	/** The rating to check with BLOCKED_RATINGS filter. */
	private String filterRating;

	/** The mode to check a Access Right and Filters. */
	private byte checkMode = MODE_CHECK_RIGHT_ONLY;

	/** To response to ScreenSaver app. */
	private ScreenSaverResponsor ssResponsor;

	/** The implement class for InbandDataListener. */
	private InbandDataListenerImpl inbandListner;

	private LogLevelAdapter logListener;
	
	private STBModeChangeListenerImpl stbModeChangeListener;

	/** The version of last in-band data. */
	private int currentVersion = -1;

	private AlphaEffect fadeInEffect;
	private AlphaEffect fadeOutEffect;

	private boolean isStarted = false;

	private boolean isBooted = false;

	private boolean isDisplayedAboutNeedAdminPin = false;
	private VideoOutputPortListenerImpl videoOutputPortListener;

	/**
	 * Instantiates a new preference controller.
	 */
	private PreferenceController() {
	}

	/**
	 * Initialize a PreferenceController.
	 *
	 * @param xletContext
	 *            The XletContext of UPP
	 */
	public void init(XletContext xletContext) {
		uiTable = new Hashtable();
		ssResponsor = new ScreenSaverResponsor();
		inbandListner = new InbandDataListenerImpl();
		logListener = new LogLevelAdapter();
		stbModeChangeListener = new STBModeChangeListenerImpl();

		DataCenter.getInstance().addDataUpdateListener(DataManager.POSTAL_CODE, inbandListner);
		DataCenter.getInstance().addDataUpdateListener(DaemonService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(MonitorService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(EpgService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(StcService.IXC_NAME, this);
		DataCenter.getInstance().addDataUpdateListener(VbmService.IXC_NAME, VbmLogController.getInstance());

		CommunicationManager.getInstance().start(xletContext);

		DataManager.getInstance().init();
	}

	/**
	 * Gets the single instance of PreferenceController.
	 *
	 * @return single instance of PreferenceController
	 */
	public synchronized static PreferenceController getInstance() {
		return instance;
	}

	/**
	 * Start.
	 */
	public void start() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: start()");
		}
		DataManager.getInstance().start();

		// By request from VT, after booted, parent control will be make a On
		// DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL,
		// Definitions.OPTION_VALUE_ON);
		Hashtable table = UserManager.getInstance().getCurrentUser().getAccessFilters();
		Preference pre = (Preference) table.get(RightFilter.PARENTAL_CONTROL);
		if (!pre.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
			DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
		}

		// ready a footer images
		Preference remotePre = DataManager.getInstance().getPreference(PreferenceNames.REMOTE_CONTROL_TYPE);
		PreferenceActionController.getInstance().doAction(remotePre.getID(), remotePre.getCurrentValue());

		ProxyController.getInstance().start();
		STBTimerController.getInstance().start();
		ParentalController.getInstance().start();

		Host.getInstance().addPowerModeChangeListener(this);
		isStarted = true;
		
		try {
			CommunicationManager.getInstance().getMonitorService().addSTBModeChangeListener(stbModeChangeListener, "Profile");
		} catch (RemoteException re) {
			Log.printError(re);
		}
	}

	/**
	 * Stop.
	 */
	public void stop() {
	}

	/**
	 * Dispose a resources to use.
	 */
	public void dispose() {
		ProxyController.getInstance().dispose();
		DataManager.getInstance().dispose();
		STBTimerController.getInstance().stop();
		ParentalController.getInstance().stop();
		VbmLogController.getInstance().dispose();

		UserManager.getInstance().dispose();

		Host.getInstance().removePowerModeChangeListener(this);

		Enumeration enums = uiTable.elements();

		while (enums.hasMoreElements()) {
			UIComponent ui = (UIComponent) enums.nextElement();
			if (ui != null) {
				ui.stop();
				ui = null;
			}
		}
		currentUI = null;
		uiTable.clear();
	}

	/**
	 * Check right and filter.
	 *
	 * @param l
	 *            The listener to receive a response which check a Access Right
	 *            or Filter.
	 * @param rName
	 *            The right name to check
	 * @param fNames
	 *            The filter name to check
	 * @param callLetter
	 *            if {@link RightFilter#ALLOW_BYPASS_BLOCKED_CHANNEL} be used, UPP needs a
	 *            current call letter to be used to compare.
	 * @param rating
	 *            if (@link {@link RightFilter#BLOCK_BY_RATINGS} be used, UPP
	 *            needs a current rating to be used to compare.
	 * @throws RemoteException
	 *             the remote exception
	 */
	public void checkRightFilter(RightFilterListener l, String rName, String[] fNames, String callLetter, String rating)
			throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: checkRightFilter: rName : " + rName + "\t fnames : " + fNames
					+ "\t callLetter : " + callLetter + "\t rating : " + rating);
		}
		// clear if PinEnablerUI is shown.
		PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);

		// zestyman : other application was known a value of anonymous' filters.
		// so don't need a follows.
		// if (hasAccessRight(User.ID_ANONYMOUS, rName)) {
		// if (Log.DEBUG_ON) {
		// Log.printDebug("Anonymouse has a access right for " + rName);
		// }
		// if (hasAccessFilter(User.ID_ANONYMOUS, fNames, callLetter, rating)) {
		// if (Log.DEBUG_ON) {
		// Log.printDebug("Anonymouse's filter allowed.");
		// }
		// l.receiveCheckRightFilter(PreferenceService.RESPONE_SUCCESS);
		// return;
		// }
		// }

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: Anonymouse has not a right and don't allowed a filters");
		}

		checkMode = MODE_CHECK_RIGHT_AND_FILTER;
		// need a pin
		isDisplayedAboutNeedAdminPin = false;
		rightListener = l;
		rightName = rName;
		this.filterNames = fNames;
		this.filterChannel = callLetter;
		this.filterRating = rating;
		boolean isRatingG = false;
		if (rating != null && rating.equals(Definitions.RATING_G)) {
			// do not need to display a explain text that user can modify a
			// access.
			isRatingG = true;
		}
		String title = BaseUI.getPopupText(KeyNames.TITLE_ENTER_PIN);
		PinEnablerController.getInstance().addPinEnablerListener(new RightFilterPinEnablerReceiver());
		PinEnablerController.getInstance().showPinEnabler(title, l.getPinEnablerExplain(), isRatingG);
	}

	/**
	 * Check right without Filters.
	 *
	 * @param l
	 *            The listener to receive a response which check a right or
	 *            filter.
	 * @param rName
	 *            The right name to check
	 * @throws RemoteException
	 *             the remote exception
	 */
	public void checkRight(RightFilterListener l, String rName) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: checkRight: rName : " + rName);
		}
		// clear if PinEnablerUI is shown.
		PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);

		if (hasAccessRight(User.ID_ANONYMOUS, rName)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController: Anonymouse has a access right for " + rName);
			}
			l.receiveCheckRightFilter(PreferenceService.RESPONSE_SUCCESS);
			return;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: Anonymouse has not a right and don't allowed a filters");
		}

		checkMode = MODE_CHECK_RIGHT_ONLY;
		// need a pin
		isDisplayedAboutNeedAdminPin = false;
		rightListener = l;
		rightName = rName;
		String title = BaseUI.getPopupText(KeyNames.TITLE_ENTER_PIN);
		PinEnablerController.getInstance().addPinEnablerListener(new RightFilterPinEnablerReceiver());
		PinEnablerController.getInstance().showPinEnabler(title, l.getPinEnablerExplain(), false);
	}

	/**
	 * Check to have a Access Right for User.
	 *
	 * @param userId
	 *            The user id.
	 * @param rName
	 *            The Right name.
	 * @return true if has a Right, false otherwise.
	 */
	private boolean hasAccessRight(String userId, String rName) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: hasAccessRight()");
		}
		if (userId.equals(User.ID_ADMIN)) {
			return true;
		}

		// check a user' right
		Hashtable rights = DataManager.getInstance().getAccessRight(userId);

		AccessRight right = (AccessRight) rights.get(rName.toUpperCase());
		if (right != null) {
			return right.hasAccessRight();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: right is null, so return true " + rName);
		}
		return true;
	}

	/**
	 * Check a User's Filter with Filter Name, channel, rating.
	 *
	 * @param userId
	 *            The user id
	 * @param filterNames
	 *            The filter names to check.
	 * @param callLetter
	 *            The call letter if want to check a CHANNEL_RESTRICTION.
	 * @param rating
	 *            The rating for current contents if want to check a
	 *            BLOCK_BY_RATINGS.
	 * @return true if all filters is validated. false otherwise.
	 */
	private boolean hasAccessFilter(String userId, String[] filterNames, String callLetter, String rating) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: hasAccessFilter()");
		}

		if (Log.DEBUG_ON && filterNames != null) {
			for (int i = 0; i < filterNames.length; i++) {
				Log.printDebug("PreferenceController: filter[" + i + "] = " + filterNames[i]);
			}
		} else if (filterNames == null) {
			return false;
		}
		if (userId.equals(User.ID_ADMIN)) {

			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController: user is Admin so return true");
			}
			return true;
		} else if (userId.equals(User.ID_FAMILY)) {
			if (filterNames[0].equals(RightFilter.ALLOW_PURCHASE) && rating != null
					&& !rating.equals(Definitions.RATING_18)) {
				return true;
			} else if (filterNames[0].equals(RightFilter.ALLOW_PURCHASE) && rating == null) {
				return true;
			} else {
				return false;
			}
		} else {

			Hashtable filters = DataManager.getInstance().getFilterList(userId);

			// check "Parental control"
			Hashtable anonyfilters = UserManager.getInstance().getCurrentUser().getAccessFilters();
			Preference parentalFilter = (Preference) anonyfilters.get(RightFilter.PARENTAL_CONTROL);

			// parental control is off
			if (parentalFilter.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PreferenceController: parental control is Off");
				}
				return true;
			}

			if (filterNames != null) {
				for (int i = 0; i < filterNames.length; i++) {

					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceController: filter[" + i + "] = " + filterNames[i]);
					}
					Preference filter = (Preference) filters.get(filterNames[i]);

					if (filter == null) {
						if (Log.DEBUG_ON) {
							Log.printDebug("PreferenceController: filter is null so return false.");
						}
						return false;
					}

					if (filterNames[i].equals(RightFilter.HIDE_ADULT_CONTENT)
							|| filterNames[i].equals(RightFilter.PROTECT_PVR_RECORDINGS)
							|| filterNames[i].equals(RightFilter.PROTECT_SETTINGS_MODIFICATIONS)) {
						if (filter.getCurrentValue().equals(Definitions.OPTION_VALUE_YES)) {
							continue;
						} else {
							return false;
						}
						// } else if
						// (filterNames[i].equals(RightFilter.PIN_CODE_UNBLOCKS_CHANNEL))
						// {
						// Preference timePre = (Preference)
						// filters.get(RightFilter.PIN_CODE_UNBLOCKS_CHANNEL_TIME);
						// long savedTime =
						// Long.parseLong(timePre.getCurrentValue());
						// if
						// (filter.getCurrentValue().equals(Definitions.FOR_4_HOURS))
						// {
						// if (savedTime + 14400000 >
						// System.currentTimeMillis()) {
						// continue;
						// } else {
						// return false;
						// }
						// } else if
						// (filter.getCurrentValue().equals(Definitions.FOR_8_HOURS))
						// {
						// if (savedTime + 28800000 >
						// System.currentTimeMillis()) {
						// continue;
						// } else {
						// return false;
						// }
						// } else { // Until Channel is changed
						// continue;
						// }
					} else if (filterNames[i].equals(RightFilter.CHANNEL_RESTRICTION)) {
						String currentValue = filter.getCurrentValue();
						if (currentValue.indexOf(callLetter) < 0) {
							continue;
						} else {
							return false;
						}
					} else if (filterNames[i].equals(RightFilter.BLOCK_BY_RATINGS)) {
						int currentIndex = findRatingIndex(filter.getCurrentValue());
						int requestIndex = findRatingIndex(rating);
						if (requestIndex < currentIndex) {
							continue;
						} else {
							return false;
						}
					} else if (filter.getCurrentValue().equals(Definitions.OPTION_VALUE_YES)) {
						continue;
					} else {
						return false;
					}
				}
			} else { // filterNames is null
				return false;
			}
		}

		return true;
	}

	/**
	 * Find a index of target rating.
	 *
	 * @param rating
	 *            target rating.
	 * @return index of target rating.
	 */
	private int findRatingIndex(String rating) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: findRatingIndex()");
		}

		int idx = 0;
		String[] ratingList = { Definitions.RATING_G, Definitions.RATING_8, Definitions.RATING_13,
				Definitions.RATING_16, Definitions.RATING_18 };

		for (idx = 0; idx < ratingList.length; idx++) {
			if (ratingList[idx].equals(rating)) {
				break;
			}
		}

		return idx;
	}

	/**
	 * Gets the preference value.
	 *
	 * @param preferenceName
	 *            the preference name
	 * @return the preference value
	 */
	public String getPreferenceValue(String preferenceName) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: getPreferenceValue: name : " + preferenceName);
		}
        Preference pref = DataManager.getInstance().getPreference(preferenceName);
        if (pref == null) {
            return "";
        }
		return pref.getCurrentValue();
	}

	/**
	 * Sets the preference value.
	 *
	 * @param preferenceName
	 *            the preference name
	 * @param value
	 *            the value
	 * @return true, if successful
	 */
	public boolean setPreferenceValue(String preferenceName, String value) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: setPreferenceValue: name : " + preferenceName + "\t value = " + value);
		}
		return DataManager.getInstance().setPreference(preferenceName, value);
	}

	/**
	 * Returns a values of a requested preferences. The values made by using
	 * preferenceNames and defaultValues.
	 *
	 * @param appId
	 *            The application id to request a preferences.
	 * @param preferenceNames
	 *            The preference names to request.
	 * @param defaultValues
	 *            The default values of requested preferences.
	 * @return the values of a requested preferences.
	 */
	public String[] getAppPreferenceData(String appId, String[] preferenceNames, String[] defaultValues) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: getAppPreferenceData()");
		}
		int length = preferenceNames.length;
		String[] retValues = new String[length];

		for (int i = 0; i < length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("App : " + appId + "\t preferenceNames[" + i + "] = " + preferenceNames[i]);
			}
			Preference pre = DataManager.getInstance().getPreference(preferenceNames[i]);

			// for Access Filters.
			if (pre == null) {
				pre = (Preference) (UserManager.getInstance().getCurrentUser().getAccessFilters()
						.get(preferenceNames[i]));
			}

			String currentValue = null;
			if (pre != null) {
				pre.addRelatedApp(appId);
				currentValue = pre.getCurrentValue();
				if (Log.DEBUG_ON) {
					Log.printDebug("currentValue : " + currentValue);
				}
			}

			if (currentValue == null || currentValue.equals("") || currentValue.equals("null")) {
				if (Log.INFO_ON) {
					Log.printInfo("set default value");
				}
				if (defaultValues[i] != null) {
					retValues[i] = defaultValues[i];
				} else {
					retValues[i] = TextUtil.EMPTY_STRING;
				}
				// memory a default value.
				DataManager.getInstance().setPreference(preferenceNames[i], retValues[i]);
				pre = DataManager.getInstance().getPreference(preferenceNames[i]);
				pre.addRelatedApp(appId);
			} else {
				if (Log.INFO_ON) {
					Log.printInfo("set current value");
				}
				retValues[i] = currentValue;
			}
		}

		return retValues;
	}

	/**
	 * Show a UIComponent that UPP has a component like Setting.
	 *
	 * @param uiId
	 *            The UIComponent id to show
	 */
	public void showUIComponent(int uiId, boolean back) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: showUIComponent()");
		}

		if (currentUI != null) {
			currentUI.removeKeyListener(this);
			// animation commented
			// if (currentUI instanceof MySettingsUI) {
			// currentUI.startFadeOutEffect(currentUI);
			// }
			currentUI.stop();
			FrameworkMain.getInstance().removeComponent(currentUI);

		} else {
			UserEventManager.getInstance().start();
			if (uiId != BaseUI.UI_PINENABLER && uiId != BaseUI.UI_CREATE_ADMIN) {
				MonitorService mService = CommunicationManager.getInstance().getMonitorService();
				try {
					mService.addScreenSaverConfirmListener(ssResponsor, FrameworkMain.getInstance()
							.getApplicationName());
				} catch (RemoteException e) {
					Log.print(e);
				}
			}
		}

		BaseUI requestUI;

		if (uiTable.containsKey(String.valueOf(uiId))) {
			requestUI = (BaseUI) uiTable.get(String.valueOf(uiId));
		} else {
			requestUI = createUIComponent(uiId);
			if (requestUI == null) {
				if (Log.INFO_ON) {
					Log.printInfo("requestUIComponent is null.");
				}
				return;
			}
			uiTable.put(String.valueOf(uiId), requestUI);
		}

		currentUI = requestUI;
		// if (uiId != BaseUI.UI_MY_SETTINGS) {
		// currentUI.start(back);
		// FrameworkMain.getInstance().addComponent(currentUI);
		// currentUI.startFadeInEffect(currentUI);
		// } else {
		// currentUI.start(back);
		// FrameworkMain.getInstance().addComponent(currentUI);
		// }
		if (back) {
			FrameworkMain.getInstance().addComponent(currentUI);
			currentUI.start(back);
		} else {
			FrameworkMain.getInstance().getHScene().add(currentUI);
			currentUI.start(back);
			FrameworkMain.getInstance().getHScene().setVisible(true);
		}
		Log.printDebug("PreferenceController: before addKeyListener and requestFocus");
		currentUI.addKeyListener(this);
		currentUI.requestFocus();
	}
	
	public void prepareUIComponent(int uiId, boolean init) {
		BaseUI requestUI;
		
		if (uiTable.containsKey(String.valueOf(uiId))) {
			requestUI = (BaseUI) uiTable.get(String.valueOf(uiId));
		} else {
			requestUI = createUIComponent(uiId);
			if (requestUI == null) {
				if (Log.INFO_ON) {
					Log.printInfo("requestUIComponent is null.");
				}
				return;
			}
			uiTable.put(String.valueOf(uiId), requestUI);
			
			if (init) {
				requestUI.init();
			}
		}
	}

	/**
	 * Create a UIComponent to use uiId.
	 *
	 * @param uiId
	 *            The Id of UIComponent to create.
	 * @return The UIComponent to create.
	 */
	public BaseUI createUIComponent(int uiId) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: createUIComponent: uiId = " + uiId);
		}
		if (uiId == BaseUI.UI_PINENABLER) {
			return new PinEnablerUI();
		} else if (uiId == BaseUI.UI_MY_SETTINGS) {
			return new MySettingsUI();
		} else if (uiId == BaseUI.UI_GENERAL_SETTINGS) {
			return new GeneralSettingsUI();
		} else if (uiId == BaseUI.UI_PARENTAL_CONTROLS) {
			return new ParentalControlsUI();
		} else if (uiId == BaseUI.UI_TIME_BASED_RESTRICTIONS) {
			return new TimeBasedRestrictionsUI();
		} else if (uiId == BaseUI.UI_CREATE_ADMIN) {
			return new CreateAdminUI();
		} else if (uiId == BaseUI.UI_PARENTAL_INFO) {
			return new ParentalControlInfoUI();
		} else if (uiId == BaseUI.UI_CHANNEL_RESTRICTION) {
			return new ChannelRestrictionUI();
		} else if (uiId == BaseUI.UI_PROGRAM_GUIDE) {
			return new ProgramGuideUI();
		} else if (uiId == BaseUI.UI_FAVOURITE_CHANNEL) {
			return new FavouriteChannelUI();
		} else if (uiId == BaseUI.UI_PVR) {
			return new PVRUI();
		} else if (uiId == BaseUI.UI_VOD) {
			return new VODUI();
		} else if (uiId == BaseUI.UI_EQUIPMENT) {
			return new EquipmentSetupUI();
		} else if (uiId == BaseUI.UI_SLEEP_TIME) {
			return new STBSleepTimeUI();
		} else if (uiId == BaseUI.UI_WAKE_UP_TIME) {
			return new STBWakeUpTimeUI();
		} else if (uiId == BaseUI.UI_AUDIO_ACCESSIBILITY) {
			return new AudioAccessibilityUI();
		} else if (uiId == BaseUI.UI_CC) {
			return new ClosedCaptioningUI();
		} else if (uiId == BaseUI.UI_CALLER_ID) {
			return new CallerIDUI();
		} else if (uiId == BaseUI.UI_TV_PICTURE_FORMAT) {
			return new TVPictureFormatUI();
		}

		return null;
	}

	/**
	 * Hide a current UIComponent.
	 */
	public void hideUIComponent() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: hideUIComponent()");
		}
		PinEnablerController.getInstance().hidePinEnabler();
		ParentalController.getInstance().hideParentalInfo();
		BaseUI.clearImages();
		BaseUI.clearHistory();
		if (currentUI != null) {
			MonitorService mService = CommunicationManager.getInstance().getMonitorService();
			if (mService != null) {
				try {
					mService.removeScreenSaverConfirmListener(ssResponsor, FrameworkMain.getInstance()
							.getApplicationName());
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			} else if (Log.ERROR_ON) {
				Log.printError("PreferenceController : hideUIComponent(): MonitorService is null");
			}
			UserEventManager.getInstance().dispose();
			FrameworkMain.getInstance().removeComponent(currentUI);
			currentUI.stop();
			currentUI.removeKeyListener(this);
			currentUI = null;

			// notify RightFilter.TIME_BASED_RESTRICTIONS to apply by monitor.
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController: hideUIComponent: getUpdatedBlockTime = "
						+ STBTimerController.getInstance().getUpdatedBlockTime());
			}
			if (STBTimerController.getInstance().getUpdatedBlockTime()) {
				STBTimerController.getInstance().setUpdatedBlockTime(false);
				STBTimerController.getInstance().updateTimeBasedRestrictions();
			}

			VbmLogController.getInstance().sendLog(VbmLogController.SESSION_END_DATE_TIME);
		}
	}

	public boolean isStarted() {
		return isStarted;
	}

	public boolean isBooted() {
		return isBooted;
	}

	/**
	 * Process a key event.
	 *
	 * @param keyCode
	 *            a key code.
	 */
	public void processKeyEvent(int keyCode) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: called processKeyEvent");
		}

		if (Log.DEBUG_ON) {
			if (currentUI != null) {
				Log.printInfo("PreferenceController: currentUI : " + currentUI.getClass().toString());
				Log.printInfo("PreferenceController: currentUI's popup : " + currentUI.getPopup());
			} else {
				Log.printInfo("PreferenceController: currentUI is null.");
			}
		}

		if (keyCode == OCRcEvent.VK_EXIT && currentUI != null && currentUI.handleKey(keyCode)) {
			return;
		}

		if (keyCode == OCRcEvent.VK_EXIT || keyCode == KeyCodes.SETTINGS) {
			MonitorService mService = CommunicationManager.getInstance().getMonitorService();

			if (mService != null) {
				try {
					mService.exitToChannel();
				} catch (RemoteException e1) {
					Log.print(e1);
				}
			} else {
				hideUIComponent();
			}
		} else if (currentUI != null) {
			currentUI.handleKey(keyCode);
		}
	}

	/**
	 * Receives a KeyEvent of keyPressed and pass keyCode to UPP via
	 * handleKey(int).
	 *
	 * @param e
	 *            The key event to occur
	 * @see KeyListener#keyPressed(KeyEvent)
	 */
	public void keyPressed(KeyEvent e) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: called keyPressed");
		}
		if (FrameworkMain.getInstance().getHScene().isShowing()) {
			int keyCode = e.getKeyCode();

			if (Log.DEBUG_ON) {
				Log.printDebug("keyCode : " + keyCode);
			}

			processKeyEvent(keyCode);
		}
	}

	/**
	 * Don't use keyReleased() in the case of UPP application.
	 *
	 * @param e
	 *            tThe key event to occur
	 * @see KeyListener#keyReleased(KeyEvent)
	 */
	public void keyReleased(KeyEvent e) {
	}

	/**
	 * Don't use keyTyped() in the case of UPP application.
	 *
	 * @param e
	 *            tThe key event to occur
	 * @see KeyListener#keyReleased(KeyEvent)
	 */
	public void keyTyped(KeyEvent e) {
	}

	/**
	 * While checking Access Right or Filter, if anonymous user don't have a
	 * Access Right or Filter, UPP Will show a PinEnablerUI. This class
	 * implements a listener for that case.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class RightFilterPinEnablerReceiver implements PinEnablerListener {
		/**
		 * While checking Access Right or Filter, if anonymous user don't have a
		 * Access Right or Filter, UPP Will show a PinEnablerUI and check a
		 * right or filter for User that has a PIN to input via PinEnablerUI.
		 *
		 * @param response
		 *            The one of {@link PreferenceService#RESPONSE_SUCCESS},
		 *            {@link PreferenceService#RESPONSE_FAIL} ,
		 *            {@link PreferenceService#RESPONSE_ERROR},
		 *            {@link PreferenceService#RESPONSE_CANCEL}
		 * @param detail
		 *            The detail message. if success, detail is user Id. if
		 *            fail, detail is a message.
		 * @throws RemoteException
		 *             The remote exception
		 * @see PinEnablerListener#receivePinEnablerResult(int,
		 *      String)
		 */
		public void receivePinEnablerResult(int response, String detail) throws RemoteException {
			if (Log.INFO_ON) {
				Log.printInfo("RightFilterPinEnablerReceiver: receivePinEnablerResult: response = " + response
						+ "\t detail = " + detail);
			}
			if (detail != null && response == PreferenceService.RESPONSE_SUCCESS) {

				if (checkMode == MODE_CHECK_RIGHT_ONLY) {
					if (hasAccessRight(detail, rightName)) {
						rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_SUCCESS);
					} else {
						rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_FAIL);
					}

				} else {
					if (hasAccessRight(detail, rightName)) {
						if (hasAccessFilter(detail, filterNames, filterChannel, filterRating)) {
							rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_SUCCESS);
						} else {
							// rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_FAIL);
							boolean isRatingG = false;
							if (filterRating != null && filterRating.equals(Definitions.RATING_G)) {
								isRatingG = true;
							}
							String title = BaseUI.getPopupText(KeyNames.TITLE_ENTER_ADMIN_PIN);
							String temp = BaseUI.getPopupText(KeyNames.EXPLAIN_CONTENT_NEED_ADMIN_PIN);
							String[] explain = TextUtil.split(temp, BaseRenderer.fm18, 310, '|');
							PinEnablerController.getInstance().addPinEnablerListener(
									new RightFilterPinEnablerReceiver());

							if (Log.DEBUG_ON) {
								Log.printDebug("RightFilterPinEnablerReceiver: receivePinEnablerResult: isDisplayedAboutNeedAdminPin = "
										+ isDisplayedAboutNeedAdminPin);
							}

							if (isDisplayedAboutNeedAdminPin) {
								PinEnablerController.getInstance().showPinEnablerInWrongPin(title, explain, isRatingG);
							} else {
								PinEnablerController.getInstance().showPinEnabler(title, explain, isRatingG);
								isDisplayedAboutNeedAdminPin = true;
							}
							return;
						}
					} else {
						rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_FAIL);
					}
				}
			} else if (response == PreferenceService.RESPONSE_CANCEL) {
				rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_CANCEL);
			} else {
				rightListener.receiveCheckRightFilter(PreferenceService.RESPONSE_FAIL);
			}

			rightListener = null;
			rightName = null;
			filterNames = null;
			filterChannel = null;
			filterRating = null;
			PinEnablerController.getInstance().removePinEnablerListener();
		}
	}

	public void dataRemoved(String key) {

	}

	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: dataUpdated: key = " + key + ", value = " + value);
		}

		if (key.equals(DaemonService.IXC_NAME)) {
			DaemonService service = (DaemonService) value;

			try {
				service.addListener(FrameworkMain.getInstance().getApplicationName(),
						RemoteRequestController.getInstance());
			} catch (RemoteException e) {
				Log.printWarning(e);
			}
		} else if (key.equals(MonitorService.IXC_NAME)) {
			MonitorService mService = (MonitorService) value;

			try {
				mService.addInbandDataListener(inbandListner, FrameworkMain.getInstance().getApplicationName());
				mService.addStateListener(this, FrameworkMain.getInstance().getApplicationName());
			} catch (RemoteException e) {
				Log.printWarning(e);
			}

		} else if (key.equals(EpgService.IXC_NAME)) {
			EpgService epgService = (EpgService) value;

			try {
				epgService.getChannelContext(0).addChannelEventListener(this);
			} catch (RemoteException e) {
				Log.printWarning(e);
			}
		} else if (key.equals(StcService.IXC_NAME)) {
			StcService stcService = (StcService) value;
			String appName = FrameworkMain.getInstance().getApplicationName();
			try {
				int currentLevel = stcService.registerApp(appName);
				Log.printDebug("stc log level = " + currentLevel);
				Log.setStcLevel(currentLevel);
			} catch (RemoteException ex) {
				Log.print(ex);
			}
			Object o = SharedMemory.getInstance().get("stc-" + appName);
			if (o != null) {
				DataCenter.getInstance().put("LogCache", o);
			}
			try {
				stcService.addLogLevelChangeListener(appName, logListener);
			} catch (RemoteException ex) {
				Log.print(ex);
			}

		}
	}

	/**
	 * When receive a selection event firstly, UPP make sure to load and set
	 * ClosedCaption values and TV Picture format and Sleep time and Wake up
	 * Time.
	 *
	 * @param id
	 *            source id of channel
	 * @param type
	 *            channel type
	 * @throws RemoteException
	 *             the remote exception
	 */
	public void selectionRequested(int id, short type) throws RemoteException {
		
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: selectionRequested: id = " + id + "\t type = " + type);
		}
		isBooted = true;

		CommunicationManager.getInstance().getEpgService().getChannelContext(0).removeChannelEventListener(this);

		try {
			// Sleep time
			Preference sleepTimePre = DataManager.getInstance().getPreference(PreferenceNames.STB_SLEEP_TIME);
			PreferenceActionController.getInstance().doAction(PreferenceNames.STB_SLEEP_TIME,
					sleepTimePre.getCurrentValue());
		} catch (Exception e) {
			Log.printWarning(e);
		}

		try {
			// Wake up time
			Preference wakeUpTimePre = DataManager.getInstance().getPreference(PreferenceNames.STB_WAKE_UP_TIME);
			PreferenceActionController.getInstance().doAction(PreferenceNames.STB_WAKE_UP_TIME,
					wakeUpTimePre.getCurrentValue());
		} catch (Exception e) {
			Log.printWarning(e);
		}

		try {
			// TV Picture format
			if (!Environment.EMULATOR) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PreferenceController, selectionRequested: SUPPORT_UHD=" + Environment.SUPPORT_UHD);
				}
				
				if (Environment.SUPPORT_UHD) {
					Preference autoPre = (Preference) DataManager.getInstance().getPreference(
							PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT);
					String value = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT))
							.getCurrentValue();

					// forcely call to add a VideoOuputPortListener.
					String foundValue = findTVPictureFormat();

					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceController, selectionRequested: AUTO_DETECTED_TV_PICTURE_FORMAT"
								+ "=" + (autoPre == null ? "null" : autoPre.getCurrentValue()));
						Log.printDebug("PreferenceController, selectionRequested: TV_PICTURE_FORMAT=" + value);
						Log.printDebug("PreferenceController, selectionRequested: foundValue=" + foundValue);
					}

					if (autoPre == null
							|| (autoPre != null && autoPre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE))) { // initial

						if (autoPre == null) {
							DataManager.getInstance().setPreference(PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT,
									Definitions.OPTION_VALUE_ENABLE);
						}

						DataManager.getInstance().setPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, foundValue);

						PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
								foundValue);
					} else if (autoPre != null) {
						PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
								value);
					}

				} else {
					String value = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT))
							.getCurrentValue();
					
					// VDTRMASTER-5617
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceController, selectionRequested: TV_PICTURE_FORMAT=" + value);
					}
					
					if (value.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_1080P)) {
						value = Definitions.VIDEO_TV_PICTURE_FORMAT_1080I;
						DataManager.getInstance().setPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, value);
					}
					
					PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, value);
				}
			}
		} catch (Exception e) {
			Log.printWarning(e);
		}

		Hashtable preTable = DataManager.getInstance().getPreferences("cc");

		try {
			// CC display
			// String ccDisplay = ((Preference)
			// preTable.get(PreferenceNames.CC_DISPLAY)).getCurrentValue();
			PreferenceActionController.getInstance().doAction(PreferenceNames.CC_DISPLAY, Definitions.OPTION_VALUE_ON);
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController: selectionRequested: CC display set ON forcely.");
			}
		} catch (Exception e) {
			Log.printWarning(e);
		}

		// CC Attribute
		String style = ((Preference) preTable.get(PreferenceNames.CC_STYLE)).getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: selectionRequested: cc style is " + style);
		}
		if (style.equals(Definitions.CC_STYLE_SET_BY_VIEWER)) {
			String[] ccPreferenceNames = { PreferenceNames.CC_CHARACTERS_SIZE, PreferenceNames.CC_BACKGROUND_SHADINGS };

			int[] attributeList = { ClosedCaptioningAttribute.CC_ATTRIBUTE_PEN_SIZE,
					ClosedCaptioningAttribute.CC_ATTRIBUTE_WINDOW_FILL_OPACITY };

			try {
				for (int i = 0; i < ccPreferenceNames.length; i++) {
					String currentValue = ((Preference) preTable.get(ccPreferenceNames[i])).getCurrentValue();
					String[] values = ((Preference) preTable.get(ccPreferenceNames[i])).getSelectableValues();
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceController: selectionRequested: CC attribute name : "
								+ ccPreferenceNames[i]);
					}

					int j;
					for (j = 0; j < values.length; j++) {
						if (values[j].equals(currentValue)) {
							break;
						}
					}

					ClosedCaptioningUtil.getInstance().updateCCAttribute(attributeList[i], ccPreferenceNames[i], j);
				}
			} catch (Exception e) {
				Log.printWarning(e);
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: selectionRequested: To set CC Attributes completed.");
		}

		try {
			// CC display
			String ccDisplay = ((Preference) preTable.get(PreferenceNames.CC_DISPLAY)).getCurrentValue();
			PreferenceActionController.getInstance().doAction(PreferenceNames.CC_DISPLAY, ccDisplay);
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController: selectionRequested: To set CC display completed.");
			}
		} catch (Exception e) {
			Log.printWarning(e);
		}

		VbmLogController.getInstance().start();

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController: selectionRequested: proccess a sister channel for Block channels");
		}

		// R7
		if (isChannelGrouped()) {
			ChannelPreferenceController.getInstance().processSisterChannels(true);
		}

		AutomaticPowerDownManager.getInstance().start();

		// try {
		// // Power-outlet
		// Preference powerOutletPre =
		// DataManager.getInstance().getPreference(PreferenceNames.TERMINAL_POWER_OUTLET);
		// if
		// (powerOutletPre.getCurrentValue().equals(Definitions.OPTION_VALUE_ON))
		// {
		// Host.getInstance().setACOutlet(true);
		// } else {
		// Host.getInstance().setACOutlet(false);
		// }
		// } catch (Exception e) {
		// Log.printWarning(e);
		// }
	}

	/**
	 * To do nothing.
	 */
	public void selectionBlocked(int id, short type) throws RemoteException {
	}

	/**
	 * As need screen-saver, the monitor asks other applications to confirm.
	 * <P>
	 * If any application return false, screen-saver is not launching. In other
	 * words All applications return true, screen-saver can launch.
	 *
	 * @return if true, screen-saver will be run, if false, will be cancel.
	 */
	class ScreenSaverResponsor implements ScreenSaverConfirmationListener {
		public boolean confirmScreenSaver() throws RemoteException {
			if (Log.INFO_ON) {
				Log.printInfo("PerferenceController: confirmScreenSaver().");
			}
			return true;
		}
	}

	/**
	 * Recevice a event when STB status is changed.
	 *
	 * @param newState
	 *            a new state of STB.
	 * @throws RemoteException
	 *             remote exception.
	 */
	public void stateChanged(int newState) throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("PerferenceController: stateChanged(): newState = " + newState);
		}

		if (newState == MonitorService.TV_VIEWING_STATE) {
			hideUIComponent();
		}
	}

	/**
	 * Implements a InbandDataListener.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class InbandDataListenerImpl implements InbandDataListener, DataUpdateListener {

		/**
		 * Update a data via in-band when Monitor tuned a In-band channel for
		 * Data.
		 *
		 * @param locator
		 *            The locator for in-band data channel.
		 */
		public void receiveInbandData(String locator) throws RemoteException {
			if (Log.INFO_ON) {
				Log.printInfo("InbandDataListenerImpl: receiveInbandData: locator = " + locator);
			}
			MonitorService monitorService = CommunicationManager.getInstance().getMonitorService();
			int newVersion = monitorService.getInbandDataVersion(MonitorService.postal_code);

			if (Log.DEBUG_ON) {
				Log.printDebug("InbandDataListenerImpl: receiveInbandData: postal code currentVersion = "
						+ currentVersion);
				Log.printDebug("InbandDataListenerImpl: receiveInbandData: postal code newVersion = " + newVersion);
			}

			if (currentVersion != newVersion) {
				currentVersion = newVersion;
				DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
			}

			try {
				monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
			} catch (RemoteException e) {
				Log.printError(e);
			}

			// power on channel
			new PowerOnChannelChecker().start();
		}

		public void dataRemoved(String key) {
		}

		/**
		 * Process a updated data.
		 *
		 * @param key
		 *            The key of update data.
		 * @param old
		 *            The old value
		 * @param value
		 *            The new value
		 */
		public void dataUpdated(String key, Object old, Object value) {
			if (Log.INFO_ON) {
				Log.printInfo("InbandDataListenerImpl: dataUpdated: key = " + key);
			}
			if (key.equals(DataManager.POSTAL_CODE) && value != null) {
				byte[] data = BinaryReader.read((java.io.File) value);
				String postalCode = null;
				try {
					postalCode = new String(data, 1, data.length - 1, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					Log.printWarning(e);
				}

				if (postalCode != null) {
					DataManager.getInstance().setDefaultPostalCode(postalCode);
				} else {
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceController: in-band postal code is null.");
					}
				}
			}
		}
	}

	/**
	 * Receives a log level from STC application and apply.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class LogLevelAdapter implements LogLevelChangeListener {

		/**
		 * Receives a log level from STC application and apply.
		 *
		 * @param logLevel
		 *            logLevel to be updated.
		 * @throws RemoteException
		 *             throws exception
		 */
		public void logLevelChanged(int logLevel) throws RemoteException {
			Log.setStcLevel(logLevel);
		}
	}

	/**
	 * If new mode is {@link Host#LOW_POWER}, Controls & Limits should be set
	 * On.
	 *
	 * @param mode
	 *            new power mode of STB.
	 */
	public void powerModeChanged(int mode) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: powerModeChanged: mode = " + mode);
		}
		if (mode == Host.LOW_POWER) {
			Hashtable table = UserManager.getInstance().getCurrentUser().getAccessFilters();
			Preference pre = (Preference) table.get(RightFilter.PARENTAL_CONTROL);
			if (!pre.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
				DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
			}
			
			if (isChannelGrouped()) {
				updatePowerOnChannel();
			}
			
		} else if (mode == Host.FULL_POWER) {
			AutomaticPowerDownManager.getInstance().resetAPDTimer();
		}
	}

	/**
	 * On booting, Profile check whether can found current power on channel or
	 * not in channel ring. For example, normal zones = MAHD but quebec ->
	 * MAQHD.
	 *
	 * @author Woojung Kim
	 * @version 1.1
	 */
	class PowerOnChannelChecker extends Thread {

		public void run() {
			if (Log.INFO_ON) {
				Log.printInfo("PowerOnChannelChecker: run().");
			}

			Thread.yield();

			Object[][] channelData = (Object[][]) SharedMemory.getInstance().get(EpgService.DATA_KEY_CHANNELS);

			if (Log.DEBUG_ON) {
				Log.printDebug("PowerOnChannelChecker: channelData = " + channelData);
			}

			// check whether current Power On channel is in Channel ring.
			Preference pre = DataManager.getInstance().getPreference(PreferenceNames.POWER_ON_CHANNEL);
			String curCallLetter = pre.getCurrentValue();

			if (Log.DEBUG_ON) {
				Log.printDebug("PowerOnChannelChecker: current value = " + curCallLetter);
			}

			if (curCallLetter.equals(Definitions.LAST_CHANNEL)) {
				Log.printDebug("PowerOnChannelChecker: current value is last channel.");
				return;
			}

			EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

			if (Log.DEBUG_ON) {
				Log.printDebug("PowerOnChannelChecker: eService = " + eService);
			}

			if (eService == null) {
				return;
			}

			try {
				TvChannel defaultChannel = eService.getChannel(curCallLetter);
				if (defaultChannel != null && defaultChannel.isSubscribed()) {
					if (Log.DEBUG_ON) {
						Log.printDebug("PowerOnChannelChecker: found current value in Channel ring so do nothing.");
					}
					return;
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("PowerOnChannelChecker: power.on.channel.special.zones start.");
			}
			String callLetterData = DataCenter.getInstance().getString("power.on.channel.special.zones");

			if (callLetterData == null || callLetterData.equals(TextUtil.EMPTY_STRING)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PowerOnChannelChecker: callLetter data is null or empty.");
				}
				return;
			}

			String[] callLetters = TextUtil.tokenize(callLetterData, PreferenceService.PREFERENCE_DELIMETER);

			String foundCallLetter = null;

			for (int i = 0; i < callLetters.length; i++) {
				try {
					TvChannel ch = eService.getChannel(callLetters[i]);
					if (ch != null && ch.isSubscribed()) {
						foundCallLetter = callLetters[i];
						break;
					}
				} catch (Exception e) {
					Log.printDebug("PowerOnChannelChecker: Exception occured");
					Log.print(e);
				}
				if (foundCallLetter != null) {
					break;
				}
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("PowerOnChannelChecker: foundCallLetter = " + foundCallLetter);
			}

			if (foundCallLetter != null && !foundCallLetter.equals(TextUtil.EMPTY_STRING)) {
				// DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL,
				// foundCallLetter);
				setPowerOnChannelWithoutSave(foundCallLetter);
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("PowerOnChannelChecker: set Last channel for POWER_ON_CHANNEL");
				}
				// DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL,
				// Definitions.LAST_CHANNEL);
				setPowerOnChannelWithoutSave(Definitions.LAST_CHANNEL);
			}
		}

		private void setPowerOnChannelWithoutSave(String value) {
			Hashtable preTable = UserManager.getInstance().getCurrentUser().getPreferences();
			Preference pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);

			pre.setCurrentValue(value);
			preTable.put(PreferenceNames.POWER_ON_CHANNEL, pre);
			ProxyController.getInstance().notifyUpdatedPreference(PreferenceNames.POWER_ON_CHANNEL, value);
		}
	}

	public void showLoading() {
		LoadingAnimationService loadingService = (LoadingAnimationService) DataCenter.getInstance().get(
				LoadingAnimationService.IXC_NAME);

		if (Log.DEBUG_ON) {
			Log.printDebug("showLoading, loadingService=" + loadingService);
		}

		if (loadingService != null) {
			try {
				loadingService.showLoadingAnimation(new Point(480, 270));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public void hideLoading() {
		LoadingAnimationService loadingService = (LoadingAnimationService) DataCenter.getInstance().get(
				LoadingAnimationService.IXC_NAME);

		if (Log.DEBUG_ON) {
			Log.printDebug("hideLoading, loadingService=" + loadingService);
		}

		if (loadingService != null) {
			try {
				loadingService.hideLoadingAnimation();
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
	}

	public String findTVPictureFormat() {
		String foundValue = null;

		// step 1 - check HDMI
		// 2160P / 1080P / 1080i / 720P / 480P / 480i

		Enumeration e = Host.getInstance().getVideoOutputPorts();
		int idx = 0;
		// VDTRMASTER-5733
		String nativeValue = null;
		
		while (e.hasMoreElements()) {
			VideoOutputPort port = (VideoOutputPort) e.nextElement();
			VideoOutputSettings setting = (VideoOutputSettings) port;
			if (port.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
				if (videoOutputPortListener == null) {
					videoOutputPortListener = new VideoOutputPortListenerImpl();
					setting.addListener(videoOutputPortListener);
				}
				if (port.status() == false) {
					Log.printDebug("PreferenceController, findTVPictureFormat(), HDMI port is disabled");
				} else if (setting.isDisplayConnected() == false) {
					Log.printDebug("PreferenceController, findTVPictureFormat(), HDMI port is disconnected");
				} else {
					VideoOutputConfiguration[] config = setting.getSupportedConfigurations();
					
					// VDTRMASTER-5733
					Hashtable attrHash = setting.getDisplayAttributes();
					if (attrHash.containsKey("Native TV resolution")) {
						nativeValue = (String) attrHash.get("Native TV resolution");
						Log.printDebug("PreferenceController, findTVPictureFormat(), nativeValue=" + nativeValue);
					} else {
						Log.printDebug("PreferenceController, findTVPictureFormat(), can't find a Native TV resolution "
								+ "key in DisplayAttributes");
					}

					if (Log.EXTRA_ON) {
						for (int i = 0; i < config.length; i++) {
							FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
							VideoResolution res = fixed.getVideoResolution();
							Dimension dim = res.getPixelResolution();
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i + "] = " + fixed);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], resolution = " + res);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], dimension = " + dim);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], scanmode = " + res.getScanMode());
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i + "], rate = "
									+ res.getRate() + ", rounded = " + Math.round(res.getRate()));
						}
					}

					final int[][] SEARCH_LIST = { { VideoResolution.SCANMODE_PROGRESSIVE, 2160 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 1080 },
							{ VideoResolution.SCANMODE_INTERLACED, 1080 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 720 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 480 }, { VideoResolution.SCANMODE_INTERLACED, 480 } };
					final String[] RESULT_LIST = { Definitions.VIDEO_TV_PICTURE_FORMAT_UHD,
							Definitions.VIDEO_TV_PICTURE_FORMAT_1080P, Definitions.VIDEO_TV_PICTURE_FORMAT_1080I,
							Definitions.VIDEO_TV_PICTURE_FORMAT_720P, Definitions.VIDEO_TV_PICTURE_FORMAT_480P,
							Definitions.VIDEO_TV_PICTURE_FORMAT_480I };

					int foundIndex = -1;
					for (int i = 0; i < SEARCH_LIST.length; i++) {
						foundIndex = findResolution(config, SEARCH_LIST[i][0], SEARCH_LIST[i][1]);
						if (foundIndex != -1) {
							foundValue = RESULT_LIST[i];
							Log.printDebug("PreferenceController, findTVPictureFormat(), found=" + foundValue);
							// VDTRMASTER-5733
							if (nativeValue != null && !nativeValue.equals(TextUtil.EMPTY_STRING) 
									&& !nativeValue.equals(foundValue)) {
								Log.printDebug("PreferenceController, findTVPictureFormat(), set the native value to the foundValue");

								if (nativeValue.equalsIgnoreCase("2160p")) {
									foundValue = Definitions.VIDEO_TV_PICTURE_FORMAT_UHD;
								} else {
									foundValue = nativeValue.toLowerCase();
								}
							}
							
							return foundValue;
						}
					}
				}
			} else {
				Log.printDebug("PreferenceController, findTVPictureFormat(), port[" + idx + "] is not HDMI, type = "
						+ port.getType() + ", state = " + port.status() + ", connected = "
						+ setting.isDisplayConnected());
			}
			idx++;
		}

		// step 2 - check Havi configuration
		// 1080i / 720P / 480P / 480i
		if (foundValue == null) {
			Log.printDebug("PreferenceController, findTVPictureFormat(), foundValue is null, so use a default(1080I)");

			foundValue = Definitions.VIDEO_TV_PICTURE_FORMAT_1080I;
		}
		
		return foundValue;
	}

	private int findResolution(VideoOutputConfiguration[] config, int scanMode, int height) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController, findResolution(), scanMode=" + scanMode + ", height=" + height);
		}
		for (int i = 0; i < config.length; i++) {
			FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
			VideoResolution res = fixed.getVideoResolution();
			Dimension dim = res.getPixelResolution();
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "] = " + fixed);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], resolution = " + res);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], dimension = " + dim);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], scanmode = "
						+ res.getScanMode());
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], rate = " + res.getRate()
						+ ", rounded = " + Math.round(res.getRate()));
			}

			if (res.getScanMode() == scanMode && dim.height == height) {
				return i;
			}
		}

		return -1;
	}

	class VideoOutputPortListenerImpl implements VideoOutputPortListener {

		public void configurationChanged(VideoOutputPort source, VideoOutputConfiguration oldConfig,
				VideoOutputConfiguration newConfig) {
		}

		public void connectionStatusChanged(VideoOutputPort source, boolean status) {
			if (source.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
				VideoOutputSettings setting = (VideoOutputSettings) source;
				if (source.status() == false) {
					Log.printDebug("VideoOutputPortListenerImpl, connectionStatusChanged(), HDMI port is disabled");
				} else if (setting.isDisplayConnected() == false) {
					Log.printDebug("VideoOutputPortListenerImpl, connectionStatusChanged(), HDMI port is disconnected");
				} else {

					Preference autoPre = (Preference) DataManager.getInstance().getPreference(
							PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT);
					String value = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT))
							.getCurrentValue();

					if (Log.DEBUG_ON) {
						Log.printDebug("VideoOutputPortListenerImpl, connectionStatusChanged(), AUTO_DETECTED_TV_PICTURE_FORMAT"
								+ "=" + (autoPre == null ? "null" : autoPre.getCurrentValue()));
						Log.printDebug("VideoOutputPortListenerImpl, connectionStatusChanged(), TV_PICTURE_FORMAT="
								+ value);
					}
					if (autoPre == null
							|| (autoPre != null && autoPre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE))) { // initial
						String foundValue = findTVPictureFormat();
						
						if (autoPre == null) {
							DataManager.getInstance().setPreference(PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT,
									Definitions.OPTION_VALUE_ENABLE);
						}
						
						DataManager.getInstance().setPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, foundValue);
						PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
								foundValue);
					}
				}
			}
		}

		public void enabledStatusChanged(VideoOutputPort source, boolean status) {
		}

	}
	
	// R7
	/**
	 * update and modify a information according with Channel after changed SD/HD Grouping
	 * target list is Favorite channel, power on channel
	 */
	public void updateChannelPreferences() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController, updateChannelPreferences");
		}
		if (isChannelGrouped()) {
			ChannelPreferenceController.getInstance().processSisterChannels(true);
		} else {
			ChannelPreferenceController.getInstance().processSisterChannels(false);
		}
	}
	
	// R7
	public boolean isChannelGrouped() {
		Preference groupingPre = (Preference) DataManager.getInstance().getPreference(PreferenceNames.SD_HD_CHANNELS_GROUPING);
		
		if (groupingPre.getCurrentValue().equals(Definitions.SD_HD_CHANNEL_GROUPED)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController, isChannelGrouped=true");
			}
			return true;
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController, isChannelGrouped=false");
			}
			return false;
		}
	}
	
	// R7
	public void updatePowerOnChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController, updatePowerOnChannel");
		}
		// VDTRMASTER-5769
		Preference pocPre = (Preference) DataManager.getInstance().getPreference(PreferenceNames.POWER_ON_CHANNEL);
		String callLetter = pocPre.getCurrentValue();
		
		EpgService eService = CommunicationManager.getInstance().getEpgService();
		
		try {
			TvChannel channel = eService.getChannel(callLetter);
			
			if (channel.isHd()) {
				if (channel.isSubscribed() == false) {
					TvChannel sisChannel = channel.getSisterChannel();
					if (sisChannel != null && sisChannel.isSubscribed()) {
						callLetter = sisChannel.getCallLetter();
					}
				}
			} else { // in the case of SD channel
				TvChannel sisChannel = channel.getSisterChannel();
				if (sisChannel != null && sisChannel.isSubscribed()) {
					callLetter = sisChannel.getCallLetter();
				}
			}
			
			pocPre.setCurrentValue(callLetter);
			
			DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL,callLetter);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * R7.3.1.1 
	 */
	private void setAudioOutput() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[setAudioOutput] run");
		}
		try {
			Preference audioOutput = (Preference) DataManager.getInstance().getPreference(
					PreferenceNames.AUDIO_OUTPUT);
			String value = audioOutput.getCurrentValue();
	
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController, setAudioOutput: foundValue=" + value);
			}

			if (value != null) {
				PreferenceActionController.getInstance().doAction(PreferenceNames.AUDIO_OUTPUT,
						value);
			}
		} catch (Exception e) {
			Log.printError(e);
		}
	}
	
	/**
	 * R7.3 Tech channel remove from fav, block channels, and remove it from boot channel.
	 */
	private void resetChannelPreference() {
		if (Log.DEBUG_ON) {
			Log.printDebug("[resetChannelPreference] run");
		}
		try {
			Object[][] channelData = (Object[][]) SharedMemory.getInstance().get(currentUI.EPG_DATA_KEY);
			
			// Favourite channels
			boolean isModified = false;
			StringBuffer sb = new StringBuffer();	
			Preference pre = DataManager.getInstance().getPreference(PreferenceNames.FAVOURITE_CHANNEL);
			if (pre != null) {
				String storedData = pre.getCurrentValue();
				if (Log.DEBUG_ON) {
					Log.printDebug("stored favourite channel Data = " + storedData);
				}
				if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
					String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
					Integer chType;
					String callLetterStr;
					
					for (int j = 0; j < parsedData.length; j++) {
						for (int i = 0; i < channelData.length; i++) {
							chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
							callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
							
							if (parsedData[j].equals(callLetterStr)) { 
								if (chType.intValue() == TvChannel.TYPE_TECH) {
									//if It is a Tech channel
									isModified = true;
								} else {
	                        		                        	
		                        	if (sb.toString().length() > 0) {
		                                sb.append(PreferenceService.PREFERENCE_DELIMETER);
		                            }
		                            sb.append(parsedData[j]);									
								}
								break;
							}
						}
					}	
				}
			}
			if (isModified) {
				if (Log.DEBUG_ON) {
					Log.printDebug("modified favourite channel Data = " + sb.toString());
				}
				pre.setCurrentValue(sb.toString());
		        DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
			}			
			// Blocked channels	
			isModified = false;
			sb = new StringBuffer();			
			pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
                    .get(RightFilter.CHANNEL_RESTRICTION);

			if (pre != null) {
                String storedData = pre.getCurrentValue();
                if (Log.DEBUG_ON) {
                    Log.printDebug("stored blocked channel Data = " + storedData);
                }
                if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
                    String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
                    Integer chType;
					String callLetterStr;
					 
					for (int j = 0; j < parsedData.length; j++) {
						for (int i = 0; i < channelData.length; i++) {
							chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
							callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
							
							if (parsedData[j].equals(callLetterStr)) { 
								if (chType.intValue() == TvChannel.TYPE_TECH) {
									//if It is a Tech channel
									isModified = true;
								} else {
	                        		                        	
									if (sb.toString().length() > 0) {
		                                sb.append(PreferenceService.PREFERENCE_DELIMETER);
		                            }
		                            sb.append(parsedData[j]);									
								}
								break;
							}
						}
					}					
                }
			}
			if (isModified) {
				if (Log.DEBUG_ON) {
					Log.printDebug("modified blocked channel Data = " + sb.toString());
				}
				pre.setCurrentValue(sb.toString());
		        DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
			}
			// Power on channel
			Preference pocPre = (Preference) DataManager.getInstance().getPreference(PreferenceNames.POWER_ON_CHANNEL);
			String powerOnChannel = pocPre.getCurrentValue();
			if (Log.DEBUG_ON) {
                Log.printDebug("stored power on channel Data = " + powerOnChannel);
            }
			Integer chType;
			String callLetterStr;
			String sisterCalletterStr;
			
			for (int i = 0; i < channelData.length; i++) {
				
				chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
				callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
				sisterCalletterStr = (String) channelData[i][BaseUI.INDEX_CHANNEL_SISTER_NAME];
				
				if (powerOnChannel != null && 
						callLetterStr != null &&
						powerOnChannel.equals(callLetterStr) && 
						chType.intValue() == TvChannel.TYPE_TECH) {
					//set to default channel
					Preference defPre = DataManager.getInstance().getDefaultPreferenceForSettings(pocPre);
					
					if (Log.DEBUG_ON) {
						Log.printDebug("power on default channel " + defPre.getCurrentValue());
					}
					DataManager.getInstance().setPreference(pocPre.getID(), defPre.getCurrentValue());
					
					if (Log.DEBUG_ON) {
	                    Log.printDebug("store new power on channel Data = " + pocPre.getCurrentValue());
	                }
					break;
				}
			}
		} catch (Exception e) {
			Log.printError(e);
		}
	}
	
	class STBModeChangeListenerImpl implements STBModeChangeListener {
		
		int stbMode = -1;

		public void modeChanged(int newMode) throws RemoteException {
			if (Log.DEBUG_ON) {
				Log.printDebug("[STBModeChangeListenerImpl] modeChanged newMode " + newMode);
			}
			
			//It has to run only at the very first time after booting
			if (stbMode == -1 && newMode != -1) {
				stbMode = newMode;
				
				Log.printDebug("[STBModeChangeListenerImpl] run firstJob ");
				resetChannelPreference();
				// VDTRMASTER-6143 R7.3 SAMSUNG UHD에서 기본값인 AC3로 설정하는 경우 HDMI통한 오디오가나오지않는 이슈가 있어서,
				// 주석처리한다. 기존 FW의 기본값은 'DISPLAY'였음  
				// R7.4 에서 다시 풀림 
				setAudioOutput(); //R7.4 sykim Audio Output VDTRMASTER-5277 ,
				try {
					Log.printDebug("[STBModeChangeListenerImpl] removeSTBModeChangeListener");
					CommunicationManager.getInstance().getMonitorService().removeSTBModeChangeListener(stbModeChangeListener, "Profile");
				} catch (RemoteException re) {
					Log.printError(re);
				}
			}
			
		}
		
	}
}

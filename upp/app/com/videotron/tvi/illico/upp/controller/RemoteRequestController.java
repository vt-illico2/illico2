/**
 * @(#)RemoteRequestController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.daemon.RemoteRequestListener;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.data.AccessRight;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.xml.Attribute;
import com.videotron.tvi.illico.xml.Element;
import com.videotron.tvi.illico.xml.XMLMaker;

/**
 * This class handle a remote requests via DAEMON App. This class handle a
 * methods(resetPin, getSettings)
 * 
 * http://localhost:8080/support/resetPin?admin=1111
 * 
 * http://localhost:8080/stc/getSettings
 * http://localhost:8080/stc/getSettings?name=allow.purchase
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class RemoteRequestController implements RemoteRequestListener {

	/** a object of RemoteRequestController for singleton. */
	private static RemoteRequestController instance;

	/** a value to indication that request want to reset a PIN. */
	final String METHOD_RESET_PIN = "resetPin";
	/** a value to indication that request want to get a values of Settings. */
	final String METHOD_GET_SETTINGS = "getSettings";

	/** a value indicate a success. */
	final int STC_E000 = 0; // success
	/** a value indicate a Invalid method name. */
	final int STC_E001 = -1; // Invalid method name.
	/** a value indicate a Unknown parameter(s) pscified. */
	final int STC_E002 = -2; // Unknown parameter(s) specified.
	/** a value indicate At least one of the parameter vlaues is invalid. */
	final int STC_E003 = -3; // At least one of the parameter values is invalid.
	/** a value indicate Invalid PIN identifier specified. */
	final int STC_E004 = -4; // Invalid PIN identifier specified.
	/** a value indicate Invalid PIN spcified. */
	final int STC_E005 = -5; // Invalid PIN specified.

	NotificationOptionImpl notiOptionImpl;

	private RemoteRequestController() {
		notiOptionImpl = new NotificationOptionImpl();
	}

	/** Get a singleton object of RemoteRequestContoller. */
	public static RemoteRequestController getInstance() {
		if (instance == null) {
			instance = new RemoteRequestController();
		}

		return instance;
	}

	/**
	 * Receives a message to change a PIN from Daemon and update a PIN.
	 * 
	 * @param path
	 *            The method name
	 * @param body
	 *            parameters
	 * @return response to request
	 * @throws RemoteException
	 *             during the execution of a remote method call
	 */
	public byte[] request(String path, byte[] body) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceController: request(): path=" + path);
			Log.printInfo("PreferenceController: request(): body=" + new String(body));
		}

		int retCode = STC_E000; // success

		if (path.equals(METHOD_RESET_PIN)) {
			retCode = processResetPin(new String(body));
		} else if (path.equals(METHOD_GET_SETTINGS)) {
			// return a byte array to be made with XML.
			Request[] requests = getRequests(new String(body));

			if (requests != null && requests.length > 0) {
				for (int i = 0; i < requests.length; i++) {
					if (!requests[i].parameter.equals("name")) {
						retCode = STC_E002;
					} else if (requests[i].value == null || requests[i].value.equals(TextUtil.EMPTY_STRING)) {
						retCode = STC_E003;
					}
				}
			}

			if (retCode == STC_E000) {
				return processGetSettings(requests).getBytes();
			}
		} else {
			retCode = STC_E001;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("RemoteRequestController: request(): return retCode=" + retCode);
		}
		return ("result=" + retCode).getBytes();
	}

	/**
	 * Process a request to reset a PIN.
	 * 
	 * @param requestStr
	 *            a string to have a information to reset a PIN.
	 * @return a result code.
	 */
	private int processResetPin(String requestStr) {
		if (Log.INFO_ON) {
			Log.printInfo("RemoteRequestController: processResetPin()");
		}
		int retCode = STC_E000;
		Request[] requests = getRequests(requestStr);

		if (requests != null) {
			if (requests.length != 0 && requests.length < 3) {
				for (int i = 0; i < requests.length; i++) {
					if (requests[i].value.length() != 4 || !isNumber(requests[i].value)) {
						retCode = STC_E005;
						break;
					}

					if (!requests[i].parameter.equals(User.ID_ADMIN) && !requests[i].parameter.equals(User.ID_FAMILY)) {
						retCode = STC_E004;
						break;
					}
				}

				boolean isSucceed = false;
				for (int i = 0; i < requests.length; i++) {
					if (Log.DEBUG_ON) {
						Log.printDebug("Remote PIN reset: user=" + requests[i].parameter + "\t value="
								+ requests[i].value);
					}

					User user = UserManager.getInstance().loadUser(requests[i].parameter);

					if (user != null) {
						user.changePin(requests[i].value);
						UserManager.getInstance().modifyUser(user);
						if (Log.DEBUG_ON) {
							Log.printDebug("Remote PIN reset: completed.");
						}

						// reset a locked time to unlock.
						DataManager.getInstance().setPreference("lockedTime", "0");
						isSucceed = true;
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("Remote PIN reset: user is null.");
						}
					}
				}

				if (isSucceed) {
					NotificationService nService = (NotificationService) DataCenter.getInstance().get(
							NotificationService.IXC_NAME);
					try {
						nService.setPriority(0);
						String messageId = nService.setNotify(notiOptionImpl);
						if (Log.DEBUG_ON) {
							Log.printDebug("Remote PIN reset: notification messageId = " + messageId);
						}
					} catch (RemoteException e) {
						if (Log.WARNING_ON) {
							Log.printWarning("Remote PIN reset: notification " + e);
						}
					}
				}
			} else {
				retCode = STC_E001;
			}
		}
		return retCode;
	}

	/**
	 * Process a request to get a Settings.
	 * 
	 * @param requests
	 *            a object to have a each information of wanted preferences.
	 * @return a information of preferences to want with XML.
	 */
	private String processGetSettings(Request[] requests) {
		if (Log.INFO_ON) {
			Log.printInfo("RemoteRequestController: processGetSettings()");
		}

		PvrService pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
		if (pvrService != null) {
			try {
				String terminalName = pvrService.getDeviceName();

				if (Log.DEBUG_ON) {
					Log.printDebug("RemoteRequestController: processGetSettings: pvrService.getDeviceName = "
							+ terminalName);
				}

				DataManager.getInstance().setPreference(PreferenceNames.TERMINAL_NAME, terminalName);

			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}

		if (requests != null && requests.length > 0) {

			User user = UserManager.getInstance().loadUser(User.ID_ANONYMOUS);

			Element settingsElement = new Element();

			settingsElement.setName("settings");

			Attribute attr = new Attribute();
			attr.setName("user");
			attr.setValue(user.getUserId());

			settingsElement.setAttributes(new Attribute[] { attr });

			Vector elementVector = new Vector();
			for (int i = 0; i < requests.length; i++) {
				Element s1 = new Element();
				Attribute a1 = new Attribute();
				s1.setName("s");
				a1.setName("n");
				a1.setValue(requests[i].value);

				s1.setAttributes(new Attribute[] { a1 });

				Element s2 = new Element();

				s2.setName("v");
				s2.setText(findSettingValue(requests[i].value));

				s1.setElements(new Element[] { s2 });

				elementVector.add(s1);
			}

			Element[] elements = new Element[elementVector.size()];
			elements = (Element[]) elementVector.toArray(elements);

			settingsElement.setElements(elements);

			Element rootElement = new Element();
			rootElement.setName("root");
			Attribute rootAttr = new Attribute();
			rootAttr.setName("result");
			rootAttr.setValue("0");
			rootElement.setAttributes(new Attribute[] { rootAttr });
			rootElement.setElements(new Element[] { settingsElement });

			return XMLMaker.getInstance().makeResponseXML("ISO-8859-1", new Element[] { rootElement });

		} else { // return all Settings

			User user = UserManager.getInstance().loadUser(User.ID_ANONYMOUS);

			Element settingsElement = new Element();

			settingsElement.setName("settings");

			Attribute attr = new Attribute();
			attr.setName("user");
			attr.setValue(user.getUserId());

			settingsElement.setAttributes(new Attribute[] { attr });

			Element[] elements = getElementsForUser(user);

			settingsElement.setElements(elements);

			Element rootElement = new Element();
			rootElement.setName("root");
			Attribute rootAttr = new Attribute();
			rootAttr.setName("result");
			rootAttr.setValue("0");
			rootElement.setAttributes(new Attribute[] { rootAttr });
			rootElement.setElements(new Element[] { settingsElement });

			return XMLMaker.getInstance().makeResponseXML("ISO-8859-1", new Element[] { rootElement });
		}

	}

	/**
	 * Find a current value according to preference name.
	 * 
	 * @param name
	 *            preference name
	 * @return value of preference.
	 */
	private String findSettingValue(String name) {
		if (Log.INFO_ON) {
			Log.printInfo("RemoteRequestController: findSettingValue: name : " + name);
		}
		User user = UserManager.getInstance().loadUser(User.ID_ANONYMOUS);

		// case of Preference
		Preference pre = (Preference) user.getPreferences().get(name);
		if (pre != null) {

			String value = pre.getCurrentValue();

			if (name.equals(PreferenceNames.FAVOURITE_CHANNEL)) {
				value = TextUtil.replace(value, "&", "&amp;");
				value = TextUtil.replace(value, "<", "&lt;");
				value = TextUtil.replace(value, ">", "&gt;");
				value = TextUtil.replace(value, "\"", "&quot;");
				value = TextUtil.replace(value, "\'", "&apos;");
			}

			return value;
		}

		// case of Right Filter
		pre = (Preference) user.getAccessFilters().get(name);
		if (pre != null) {

			String value = pre.getCurrentValue();

			if (name.equals(RightFilter.CHANNEL_RESTRICTION)) {
				value = TextUtil.replace(value, "&", "&amp;");
				value = TextUtil.replace(value, "<", "&lt;");
				value = TextUtil.replace(value, ">", "&gt;");
				value = TextUtil.replace(value, "\"", "&quot;");
				value = TextUtil.replace(value, "\'", "&apos;");
			}

			return value;
		}

		// case of Right
		AccessRight ar = (AccessRight) user.getAccessRights().get(name);

		if (ar != null) {
			return String.valueOf(ar.hasAccessRight());
		}
		return null;
	}

	/**
	 * Make a element for XML.
	 * 
	 * @param user
	 *            a use to want to know.
	 * @return Element array to have a preference values of user.
	 */
	private Element[] getElementsForUser(User user) {
		if (Log.INFO_ON) {
			Log.printInfo("RemoteRequestController: getElementsForUser: user : " + user.getUserId());
		}
		Vector elementVector = new Vector();

		Hashtable accessTable = user.getAccessRights();

		Enumeration keys = accessTable.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			AccessRight ar = (AccessRight) accessTable.get(key);

			Element s1 = new Element();
			Attribute a1 = new Attribute();
			s1.setName("s");
			a1.setName("n");
			a1.setValue(ar.getName());

			s1.setAttributes(new Attribute[] { a1 });

			Element s2 = new Element();

			s2.setName("v");
			s2.setText(String.valueOf(ar.hasAccessRight()));

			s1.setElements(new Element[] { s2 });

			elementVector.add(s1);
		}

		Hashtable filterTable = user.getAccessFilters();

		keys = filterTable.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			Preference pre = (Preference) filterTable.get(key);

			Element s1 = new Element();
			Attribute a1 = new Attribute();
			s1.setName("s");
			a1.setName("n");
			a1.setValue(pre.getID());

			s1.setAttributes(new Attribute[] { a1 });

			Element s2 = new Element();

			s2.setName("v");
			s2.setText(findSettingValue(pre.getID()));

			s1.setElements(new Element[] { s2 });

			elementVector.add(s1);
		}

		Hashtable preTable = user.getPreferences();

		keys = preTable.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			Preference pre = (Preference) preTable.get(key);

			Element s1 = new Element();
			Attribute a1 = new Attribute();
			s1.setName("s");
			a1.setName("n");
			a1.setValue(pre.getID());

			s1.setAttributes(new Attribute[] { a1 });

			Element s2 = new Element();

			s2.setName("v");
			s2.setText(findSettingValue(pre.getID()));

			s1.setElements(new Element[] { s2 });

			elementVector.add(s1);
		}

		Element[] elements = new Element[elementVector.size()];
		elements = (Element[]) elementVector.toArray(elements);

		return elements;
	}

	/**
	 * Parse a request.
	 * 
	 * @param src
	 *            a string to include a requests.
	 * @return a request to be parsed.
	 */
	private Request[] getRequests(String src) {
		if (Log.INFO_ON) {
			Log.printInfo("RemoteRequestController: getRequests(): src " + src);
		}
		Request[] requestArray;
		Vector rVector = new Vector();

		String[] parsedString = TextUtil.tokenize(src, "&");

		for (int i = 0; parsedString != null && i < parsedString.length; i++) {
			int idx = parsedString[i].indexOf("=");

			Request r = new Request();
			if (idx != -1) {
				r.parameter = parsedString[i].substring(0, idx);
				r.value = parsedString[i].substring(idx + 1);
			} else {
				r.parameter = parsedString[i];
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("RemoteRequestController: getRequests(): request name = " + r.parameter);
				Log.printDebug("RemoteRequestController: getRequests(): request value = " + r.value);
			}

			rVector.add(r);

		}
		requestArray = new Request[rVector.size()];
		requestArray = (Request[]) rVector.toArray(requestArray);

		if (Log.DEBUG_ON) {
			Log.printDebug("RemoteRequestController: getRequests(): requestArray size = " + requestArray.length);
		}

		return requestArray;
	}

	/**
	 * judge that parameter is number.
	 * 
	 * @param number
	 *            number to be judged.
	 * @return true if parameter is number, false otherwise.
	 */
	private boolean isNumber(String number) {
		try {
			Long.parseLong(number);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Represent a request from STC via Daemon.
	 * 
	 * @author Woojung Kim
	 * @version 1.1
	 */
	private class Request {
		public String parameter;
		public String value;
	}

	class NotificationOptionImpl implements NotificationOption {

		public String getApplicationName() throws RemoteException {
			return FrameworkMain.getInstance().getApplicationName();
		}

		public long getCreateDate() throws RemoteException {
			return 0;
		}

		public boolean isViewed() throws RemoteException {
			return false;
		}

		public int getDisplayCount() throws RemoteException {
			return 0;
		}

		public String getMessageID() throws RemoteException {
			return null;
		}

		public int getCategories() throws RemoteException {
			return 0;
		}

		public long getDisplayTime() throws RemoteException {
			return System.currentTimeMillis();
		}

		public int getRemainderTime() throws RemoteException {
			return 0;
		}

		public long getRemainderDelay() throws RemoteException {
			return 0;
		}

		public boolean isCountDown() throws RemoteException {
			return false;
		}

		public String getMessage() throws RemoteException {
			return "";
		}

		public String getSubMessage() throws RemoteException {
			return null;
		}

		public int getNotificationPopupType() throws RemoteException {
			return STC_POPUP;
		}

		public int getNotificationAction() throws RemoteException {
			return ACTION_NONE;
		}

		public String[] getNotificationActionData() throws RemoteException {
			return null;
		}

		public String getLargePopupMessage() throws RemoteException {
			return null;
		}

		public String getLargePopupSubMessage() throws RemoteException {
			return null;
		}

		public String[][] getButtonNameAction() throws RemoteException {
			return null;
		}
	}
}

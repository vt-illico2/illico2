/**
 * @(#)ParentalController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.upp.ui.ParentalControlInfoUI;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class handle to display a active or deactive of Parental control and add a notify to Notification application.
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalController extends LayeredWindow implements LayeredKeyHandler, NotificationOption,
        TVTimerWentOffListener, PreferenceListener {

    /** a singleton object of this class. */
    private static ParentalController instance;

    /** a ui to display a current information of Parental Control. */
    private ParentalControlInfoUI parentInfoUI;
    /** a layeredUI to display. */
    private LayeredUI layeredUI;

    /** a millisecond value of four hours. */
    private long FOUR_HOURS = 4 * 60 * 60 * 1000;
    /** a millisecond value of six hours. */
    private long EIGHT_HOURS = 8 * 60 * 60 * 1000;

    /** a current value of Parental Control. */
    private String parental;
    /** a last time to be updated. */
    private long parentalTime;
    /** The message id to be displayed via Notification. */
    private String messageId;

    /** a timer spec to be called after time over. */
    private TVTimerSpec tvSpec;

    /** Initialize a Objects and Fields. */
    private ParentalController() {
        setBounds(Constants.SCREEN_BOUNDS);
        parentInfoUI = (ParentalControlInfoUI) PreferenceController.getInstance().createUIComponent(
                BaseUI.UI_PARENTAL_INFO);
        layeredUI = WindowProperty.PINENABLER.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);

        tvSpec = new TVTimerSpec();
        tvSpec.addTVTimerWentOffListener(this);

        if (DataCenter.getInstance().getBoolean("testMode")) {
            FOUR_HOURS = 4 * 60 * 1000;
            EIGHT_HOURS = 8 * 60 * 1000;
        } else {
            FOUR_HOURS = 4 * 60 * 60 * 1000;
            EIGHT_HOURS = 8 * 60 * 60 * 1000;
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ParentalController : FOUR_HOURS = " + FOUR_HOURS);
            Log.printDebug("ParentalController : EIGHT_HOURS = " + EIGHT_HOURS);
        }
    }

    /**
     * Gets a object of this class.
     * @return ParentalController a singleton object.
     */
    public static ParentalController getInstance() {
        if (instance == null) {
            instance = new ParentalController();
        }

        return instance;
    }

    /**
     * Starts a Controller.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : start()");
        }

        PreferenceController.getInstance().getAppPreferenceData(FrameworkMain.getInstance().getApplicationName(),
                new String[] {RightFilter.PARENTAL_CONTROL}, new String[] {Definitions.OPTION_VALUE_ON});
        ProxyController.getInstance().addProxyListener(FrameworkMain.getInstance().getApplicationName(), this);
        proccessParentalControl();
    }

    /**
     * Stops a Controller.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : stop()");
        }
        cancelNotification();
        cancelResetTimer();
    }

    /**
     * Load a information for Parental control.
     */
    private void loadParentalInfo() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : loadParentalInfo()");
        }
        Hashtable filterTable = UserManager.getInstance().getCurrentUser().getAccessFilters();
        parental = ((Preference) filterTable.get(RightFilter.PARENTAL_CONTROL)).getCurrentValue();
        Preference parentalTimePre = (Preference) filterTable.get(RightFilter.PARENTAL_CONTROL_TIME);
        try {
            parentalTime = Long.parseLong(parentalTimePre.getCurrentValue());
        } catch (Exception e) {
            e.printStackTrace();
            parentalTime = 0;
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ParentalController : parental = " + parental);
            Log.printDebug("ParentalController : parentalTime = " + parentalTime);
        }
    }

    /**
     * Set a notification and finish time with Parental Control preference.
     */
    private void proccessParentalControl() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : proccessParentalControl()");
        }
        loadParentalInfo();
        if (parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ParentalController : proccessParentalControl() : parentalTime = "
                        + (parentalTime + FOUR_HOURS));
                Log.printDebug("ParentalController : proccessParentalControl() : currentTimeMillis = "
                        + System.currentTimeMillis());
            }
            if (parentalTime + FOUR_HOURS <= System.currentTimeMillis()) {
                // parent control wiil be make a On
                DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
                // if has a registered msg, remove it.
                cancelNotification();
                cancelResetTimer();
            } else {
                // Notification center & set a timer to be make a On of Parental Control's value
                sendNotification();
                startResetTimer(parentalTime + FOUR_HOURS);
            }
        } else if (parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ParentalController : proccessParentalControl() : parentalTime = "
                        + (parentalTime + EIGHT_HOURS));
                Log.printDebug("ParentalController : proccessParentalControl() : currentTimeMillis = "
                        + System.currentTimeMillis());
            }
            if (parentalTime + EIGHT_HOURS <= System.currentTimeMillis()) {
                // parent control will be make a On
                DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
                // if has a registered msg, remove it.
                cancelNotification();
                cancelResetTimer();
            } else {
                // Notification center & set a time to be make a On of Parental Control's value
                sendNotification();
                startResetTimer(parentalTime + EIGHT_HOURS);
            }
        } else {
            // if has a registered msg, remove it.
            cancelNotification();
            cancelResetTimer();
        }
    }

    /**
     * Sends a information of current parental control to Notification application.
     */
    private void sendNotification() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : sendNotification()");
        }
        NotificationService notiService = (NotificationService) DataCenter.getInstance().get(
                NotificationService.IXC_NAME);
        if (notiService != null) {
            try {
                messageId = notiService.setNotify(this);
            } catch (RemoteException e) {
                Log.printWarning(e);
            }
        } else if (Log.ERROR_ON) {
            Log.printError("ParentalController : sendNotification(): NotificationService is null");
        }

    }

    /**
     * Cancel a current notification.
     */
    private void cancelNotification() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : cancelNotification()");
        }
        NotificationService notiService = (NotificationService) DataCenter.getInstance().get(
                NotificationService.IXC_NAME);

        if (Log.DEBUG_ON) {
            Log.printDebug("ParentalController : cancelNotification(): messsageId = " + messageId);
        }
        if (messageId != null && notiService != null) {
            try {
                notiService.removeNotify(messageId);
            } catch (RemoteException e) {
                Log.printWarning(e);
            }
        } else if (notiService == null && Log.ERROR_ON) {
            Log.printError("ParentalController : sendNotification(): NotificationService is null");
        }
    }

    /**
     * Reset a finish time of Parental Control.
     * @param when a time to be finished.
     */
    private void startResetTimer(long when) {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : startResetTimer()");
        }
        tvSpec.addTVTimerWentOffListener(this);
        tvSpec.setAbsoluteTime(when);
        try {
            TVTimer.getTimer().scheduleTimerSpec(tvSpec);
        } catch (TVTimerScheduleFailedException e) {
            Log.printWarning(e);
        }
    }

    /**
     * Cancel a finish time of Parental Control.
     */
    private void cancelResetTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : cancelResetTimer()");
        }
        TVTimer.getTimer().deschedule(tvSpec);
        tvSpec.removeTVTimerWentOffListener(this);
    }

    /**
     * Set a default time of suspend hours.
     * @param value current suspend time. one of {@link Definitions#OPTION_VALUE_SUSPENDED_FOR_4_HOURS} or
     *            {@link Definitions#OPTION_VALUE_SUSPENDED_FOR_8_HOURS}.
     */
    private void saveSuspendDefault(final String value) {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : saveSuspendDefault: value = " + value);
        }
        new Thread(new Runnable() {
            public void run() {
                DataManager.getInstance().setPreference(RightFilter.SUSPEND_DEFAULT_TIME, value);
            }
        }).start();
    }

    /**
     * Receives a time event.
     * @param event timer event.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : timerWentOff()");
        }
        if (event.getTimerSpec().equals(tvSpec)) {
            loadParentalInfo();
            if (parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS)
                    || parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
                DataManager.getInstance().setPreference(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
                if (FrameworkMain.getInstance().getHScene().isShowing()) {
                    FrameworkMain.getInstance().getHScene().repaint();
                }
            }
        }
    }

    /**
     * Reveives a updated event of Preference. this case is only for Parental Control.
     * @param name a preference name.
     * @param value a updated value.
     * @throws RemoteException remote exception.
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("ParentalController : receiveUpdatedPreference: name " + name + "\t value = " + value);
        }
        if (name.equals(RightFilter.PARENTAL_CONTROL)) {

            if (value.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS)
                    || value.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
                saveSuspendDefault(value);
            }

            proccessParentalControl();
        }
    }

    // /////////////////////////////////////////////////////////////////////////////
    // impelemnts NotificationOption
    // /////////////////////////////////////////////////////////////////////////////
    public String getApplicationName() throws RemoteException {
        return FrameworkMain.getInstance().getApplicationName();
    }

    public int getCategories() throws RemoteException {
        return 0;
    }

    public long getDisplayTime() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("ParentalController : getDisplayTime()");
        }
        if (parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ParentalController : parentalTime + FOUR_HOURS = " + parentalTime + FOUR_HOURS);
            }
            return parentalTime + FOUR_HOURS;
        } else if (parental.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ParentalController : parentalTime + EIGHT_HOURS = " + parentalTime + EIGHT_HOURS);
            }
            return parentalTime + EIGHT_HOURS;
        }
        return 0;
    }

    public String getMessage() throws RemoteException {
        return "";
    }

    public String getMessageID() throws RemoteException {
        return null;
    }

    public int getNotificationAction() throws RemoteException {
        return ACTION_PIN_POPUP;
    }

    public String[] getNotificationActionData() throws RemoteException {
        return null;
    }

    public int getNotificationPopupType() throws RemoteException {
        return PARENTAL_CONTROLS_POPUP;
    }

    public long getRemainderDelay() throws RemoteException {
        return 0;
    }

    public int getRemainderTime() throws RemoteException {
        return 0;
    }

    public void setApplicationName(String name) throws RemoteException {
    }

    public void setCategories(int categories) throws RemoteException {
    }

    public void setDisplayTime(long time) throws RemoteException {
    }

    public void setMessage(String message) throws RemoteException {
    }

    public void setMessageID(String message) throws RemoteException {
    }

    public void setNotificationAction(int action) throws RemoteException {
    }

    public void setNotificationPopupType(int status) throws RemoteException {
    }

    public void setRemainderDelay(long delay) throws RemoteException {
    }

    public void setRemainderTime(int time) throws RemoteException {
    }

    public long getCreateDate() throws RemoteException {
        return 0;
    }

    public int getDisplayCount() throws RemoteException {
        return 0;
    }

    public void setViewed(boolean b) throws RemoteException {

    }

    public boolean isViewed() throws RemoteException {
        return false;
    }

    public void setCreateDate(long createData) throws RemoteException {

    }

    public void setDisplayCount(int count) throws RemoteException {

    }

    public String getLargePopupMessage() throws RemoteException {
        return null;
    }

    public String getSubMessage() throws RemoteException {
        return null;
    }

    public void setLargePopupMessage(String message) throws RemoteException {

    }

    public void setSubMessage(String message) throws RemoteException {

    }

    public String[][] getButtonNameAction() throws RemoteException {
        return null;
    }

    public String getLargePopupSubMessage() throws RemoteException {
        return null;
    }

    public void setButtonNameAction(String[][] action) throws RemoteException {

    }

    public void setLargePopupSubMessage(String message) throws RemoteException {

    }

    public void setNotificationActionData(String[] param) throws RemoteException {
    }

    public boolean isCountDown() throws RemoteException {
        return true;
    }

    public void setCountDown(boolean b) throws RemoteException {
    }

    /**
     * Show a UI to have a information of current parental control.
     * @param value a current value of parent control.
     */
    public void showParentalInfo(String value) {

        if (!value.equals(Definitions.OPTION_VALUE_OFF) && Host.getInstance().getPowerMode() == Host.FULL_POWER) {
            add(parentInfoUI);
            layeredUI.activate();
            if (value.equals(Definitions.OPTION_VALUE_ON)) {
                parentInfoUI.show(parentInfoUI.STATE_ON);
            } else if (value.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS)) {
                parentInfoUI.show(parentInfoUI.STATE_SUSPEND_4);
            } else if (value.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
                parentInfoUI.show(parentInfoUI.STATE_SUSPEND_8);
            }
        }
    }

    /**
     * Hides a ParentalInfo UI.
     */
    public void hideParentalInfo() {
        layeredUI.deactivate();
        remove(parentInfoUI);
    }

    /**
     * Handle a key event.
     * @param event a current event.
     * @return boolean true if use, false otherwise.
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        return parentInfoUI.handleKey(event.getCode());
    }
}

/**
 * @(#)VbmLogController.java
 *
 * Copyright 1999-2011 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class control a log to be sent via VBM.
 * @author Woojung Kim
 * @version 1.1
 */
public class VbmLogController implements DataUpdateListener, LogCommandTriggerListener {

    public static String SESSION_BEGIN_DATE_TIME = "1003000001";
    public static String SESSION_END_DATE_TIME = "1003000002";
    public static String PARENT_APPLICATION_NAME = "1003000003";
    
    public static String APD_GOTO_STANBY = "1003000187";
    public static String APD_GOTO_SETTINGS = "1003000184";
    public static String APD_DISMISSED = "1003000183";
    
    private static VbmLogController instance;

    private final String VBM_SM_INTF = VbmService.VBM_SM_INTF;
    private final String SEPARATOR = VbmService.SEPARATOR;
    private final long SEPARATOR_ID = 1000000000;

    private final String ID_ADMIN = "3000156";
    private final String ID_CHANNEL_RESTRICTION = "3000161";
    private final String ID_FAVOURITE_CHANNEL = "3000166";
    private final String ID_TIME_BASED_RESTRICTIONS = "3000163";
    private final String ID_SLEEP_TIME = "3000174";
    private final String ID_WAKE_UP_TIME = "3000175";
    private final String ID_POSTAL_CODE = "3000154";
        
    private final String ID_APD_ENABLED		= "1003000185"; //"Enabled", "Disabled"
    private final String ID_APD_DURATION	= "1003000186"; //"1 hour", "2 hours", "3 hours", "4 hours

    private final String[] PREFRENCE_LIST = {
            PreferenceNames.LANGUAGE,
            PreferenceNames.POSTAL_CODE,
            PreferenceNames.DISPLAY_MENU_AT_POWER_ON,
            null, // Admin created
            RightFilter.PARENTAL_CONTROL, RightFilter.BLOCK_BY_RATINGS_DISPLAY, RightFilter.BLOCK_BY_RATINGS,
            RightFilter.HIDE_ADULT_CONTENT, RightFilter.CHANNEL_RESTRICTION, RightFilter.PIN_CODE_UNBLOCKS_CHANNEL,
            RightFilter.TIME_BASED_RESTRICTIONS, RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
            PreferenceNames.POWER_ON_CHANNEL, PreferenceNames.FAVOURITE_CHANNEL,
            PreferenceNames.AUTO_SHOW_DESCIRIPTION, PreferenceNames.VIDEO_ZOOM_MODE, PreferenceNames.WISHLIST_DISPLAY,
            PreferenceNames.PREFERENCES_ON_FORMAT, PreferenceNames.PREFERENCES_ON_LANGUAGE,
            //R7.4 VDTRMASTER-6038
            /*PreferenceNames.SHORTCUT_MENU_DISPLAY, PreferenceNames.SHORTCUT_MENU_LIST,*/ 
            PreferenceNames.STB_SLEEP_TIME,
            PreferenceNames.STB_WAKE_UP_TIME, PreferenceNames.REMOTE_CONTROL_TYPE,
            PreferenceNames.VIDEO_TV_PICTURE_FORMAT, PreferenceNames.AUDIO_VOLUME_CONTROL,
            PreferenceNames.SAP_LANGUAGE_AUDIO, PreferenceNames.DESCRIBED_AUDIO_DISPLAY, PreferenceNames.CC_DISPLAY,
            PreferenceNames.SD_HD_CHANNELS_GROUPING,
            PreferenceNames.AUTOMATIC_POWER_DOWN, PreferenceNames.AUTOMATIC_POWER_DOWN_TIME
    };

    private final String[] LOG_ID_LIST = {"3000153", "3000154", "3000155", "3000156", "3000157", "3000158", "3000159",
            "3000160", "3000161", "3000162", "3000163", "3000164", "3000165", "3000166", "3000167", "3000168",
            "3000169", "3000170", "3000171",
            //R7.4 VDTRMASTER-6038
            /*"3000172", "3000173",*/ 
            "3000174", "3000175", "3000176", "3000177",
            "3000178", "3000179", "3000180", "3000181", "3000182", 
            "1003000185", "1003000186"};
    
    private Hashtable idTable;

    private VbmLogController() {
        idTable = new Hashtable();       
    }

    public static synchronized VbmLogController getInstance() {
        if (instance == null) {
            instance = new VbmLogController();
        }
        return instance;
    }

    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : start()");
        }
    }

    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : stop()");
        }
    }

    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : dispose()");
        }

        VbmService vService = (VbmService) DataCenter.getInstance().get(VbmService.IXC_NAME);

        if (vService != null) {
            try {
                vService.removeLogCommandChangeListener(FrameworkMain.getInstance().getApplicationName());
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    private String getValue(String id, int idx) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : getValue()");
        }
        if (id.equals(SESSION_BEGIN_DATE_TIME)) {
        	String time = String.valueOf(System.currentTimeMillis());
            DataCenter.getInstance().put(SESSION_BEGIN_DATE_TIME, time);
            
        	return time;
        } else if (id.equals(SESSION_END_DATE_TIME)) {
            String time = DataCenter.getInstance().getString(SESSION_BEGIN_DATE_TIME);
            
            if (time != null && time != TextUtil.EMPTY_STRING) {
                return time;
            } else {
                return TextUtil.EMPTY_STRING;
            }
        } else if (id.equals(PARENT_APPLICATION_NAME)) {
            String parentAppName = DataCenter.getInstance().getString(PARENT_APPLICATION_NAME);
            DataCenter.getInstance().remove(PARENT_APPLICATION_NAME);
            if (parentAppName.equals(MonitorService.REQUEST_APPLICATION_HOT_KEY)) {
            	parentAppName = "Remote";
            }
            
            return parentAppName;
        } else if (id.equals(ID_ADMIN)) {
            User adminUser = UserManager.getInstance().loadUser(User.ID_ADMIN);
            return String.valueOf(adminUser.hasPin());
        } else if (id.equals(ID_FAVOURITE_CHANNEL)) {
            Preference pre = DataManager.getInstance().getPreference(PreferenceNames.FAVOURITE_CHANNEL);

            if (pre != null) {
            	return pre.getCurrentValue();
            }
        } else if (id.equals(ID_CHANNEL_RESTRICTION)) {
            Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
                    .get(RightFilter.CHANNEL_RESTRICTION);

            if (pre != null) {
                return pre.getCurrentValue();
            }
        } else if (id.equals(ID_TIME_BASED_RESTRICTIONS) || id.equals(ID_SLEEP_TIME) || id.equals(ID_WAKE_UP_TIME)) {
            Preference pre = DataManager.getInstance().getPreference(PREFRENCE_LIST[idx]);

            if (pre != null) {
                if (pre.getCurrentValue().indexOf(Definitions.OPTION_VALUE_ENABLE) > -1) {
                    return Definitions.OPTION_VALUE_ENABLE;
                }
            }
            return Definitions.OPTION_VALUE_DISABLE;
        } else if (id.equals(ID_POSTAL_CODE)) {
        	return DataManager.getInstance().getPostalCode();
        } else if (id.equals(ID_APD_ENABLED)) {
        	Preference pre = DataManager.getInstance().getPreference(PREFRENCE_LIST[idx]);

            if (pre != null) {
                return pre.getCurrentValue();
            }
        } else if (id.equals(ID_APD_DURATION)) {
        	Preference pre = DataManager.getInstance().getPreference(PREFRENCE_LIST[idx]);

            if (pre != null) {
                return pre.getCurrentValue();
            }
        } else if (id.equals(APD_GOTO_STANBY) || id.equals(APD_GOTO_SETTINGS)) {
        	String time = String.valueOf(System.currentTimeMillis());
            
            if (time != null && time != TextUtil.EMPTY_STRING) {
                return time;
            } else {
                return TextUtil.EMPTY_STRING;
            }
        } else if (idx > -1) {
            Preference pre = DataManager.getInstance().getPreference(PREFRENCE_LIST[idx]);

            if (pre != null) {
                return pre.getCurrentValue();
            }
        }
        return TextUtil.EMPTY_STRING;
    }

    public void sendLog(String id, String reqValue) {
    	if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : sendLog(String): id = " + id);
        }
        sendLog(Long.parseLong(id), reqValue);
    }
    
    public void sendLog(String id) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : sendLog(String): id = " + id);
        }
        sendLog(Long.parseLong(id), null);
    }

//    public void sendLog(String id, String value) {
//        if (Log.INFO_ON) {
//            Log.printInfo("VbmLogController : sendLog(String, String): id = " + id + " | value = " + value);
//        }
//        sendLog(Long.parseLong(id), value);
//    }

    private void sendLog(long measurementId, String reqValue) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : sendLog(long): measurementId = " + measurementId);
        }

        if (measurementId < SEPARATOR_ID || idTable.contains(new Long(measurementId))) {
            if (Log.DEBUG_ON) {
                Log.printDebug("VbmLogController : sendLog : contains in check list or id is under SEPARASTOR_ID");
            }
            // get shared log buffer object
            SharedMemory sm = SharedMemory.getInstance();
            Vector vbmLogBuffer = (Vector) sm.get(VBM_SM_INTF);

            if (Log.DEBUG_ON) {
                Log.printDebug("VbmLogController : sendLog : vbmLogBuffer = " + vbmLogBuffer);
            }

            if (vbmLogBuffer != null) {
                String oneLog = null;
                oneLog = Long.toString(measurementId) + SEPARATOR + "-1";
                int index = findIndex(measurementId);

                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmLogController : sendLog : index = " + index);
                }

                if (reqValue != null) {
                	oneLog += SEPARATOR + reqValue; // put data
                } else {
	                String value = getValue(Long.toString(measurementId), index);
	
	                if (value != null) {
	                    oneLog += SEPARATOR + value; // put data
	                }
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmLogController : sendLog : oneLog = " + oneLog);
                }
                // put one log into vbm log buffer
                vbmLogBuffer.addElement(oneLog);
            }
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("VbmLogController : sendLog : NOT contains in check list");
            }
        }
    }

//    private void sendLog(long measurementId, String value) {
//        if (Log.INFO_ON) {
//            Log.printInfo("VbmLogController : sendLog(long, String): measurementId = " + measurementId + " | value = "
//                    + value);
//        }
//
//        if (measurementId < SEPARATOR_ID || idTable.contains(new Long(measurementId))) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("VbmLogController : sendLog : contains in check list or id is under SEPARASTOR_ID");
//            }
//            // get shared log buffer object
//            SharedMemory sm = SharedMemory.getInstance();
//            Vector vbmLogBuffer = (Vector) sm.get(VBM_SM_INTF);
//
//            if (Log.DEBUG_ON) {
//                Log.printDebug("VbmLogController : sendLog : vbmLogBuffer = " + vbmLogBuffer);
//            }
//
//            if (vbmLogBuffer != null) {
//                String oneLog = null;
//                oneLog = Long.toString(measurementId);
//                int index = findIndex(measurementId);
//
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("VbmLogController : sendLog : index = " + index);
//                }
//
//                if (value != null) {
//                    oneLog += SEPARATOR + value; // put data
//                }
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("VbmLogController : sendLog : oneLog = " + oneLog);
//                }
//                // put one log into vbm log buffer
//                vbmLogBuffer.addElement(oneLog);
//            }
//        } else {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("VbmLogController : sendLog : NOT contains in check list");
//            }
//        }
//    }

    private int findIndex(long mid) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : findIndex");
        }
        for (int i = 0; i < LOG_ID_LIST.length; i++) {
            long logId = Long.parseLong(LOG_ID_LIST[i]);
            if (logId == mid) {
                return i;
            }
        }

        return -1;
    }

    public void dataRemoved(String key) {

    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : dataUpdated: key = " + key + "\t value = " + value);
        }
        if (key.equals(VbmService.IXC_NAME)) {
            VbmService vService = (VbmService) value;
            long[] actionItems = null;
            try {
                actionItems = vService.checkLogCommand(FrameworkMain.getInstance().getApplicationName());
                vService.addLogCommandChangeListener(FrameworkMain.getInstance().getApplicationName(), this);
            } catch (RemoteException e) {
                Log.print(e);
            }

            if (actionItems != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmLogController : dataUpdated : checkList length = " + actionItems.length);
                }
                for (int i = 0; i < actionItems.length; i++) {
                	if (Log.DEBUG_ON) {
                        Log.printDebug("VbmLogController : dataUpdated : item = " + actionItems[i]);
                    }
                    Long id = new Long(actionItems[i]);
                    idTable.put(id, id);
                }
            }
        }
    }

    public void updatedPreferenceData(String name, String value) {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : updatedPreferenceData: name = " + name + "\t value = " + value);
        }
        int index = -1;

        for (int i = 0; i < PREFRENCE_LIST.length; i++) {
            if (PREFRENCE_LIST[i] != null && PREFRENCE_LIST[i].equals(name)) {
                index = i;
                break;
            }
        }

        if (index > -1) {
            long id = Long.parseLong(LOG_ID_LIST[index]);
            
            if (Log.DEBUG_ON) {
            	Log.printDebug("VbmLogController : updatedPreferenceData: find id = " + id);
            }
            
            // check this event id  belong with EVENT_ID and contains in required id list.
            if (id >= SEPARATOR_ID && idTable.contains(new Long(id))) {
            	sendLog(id, null);
            } else {
            	if (Log.DEBUG_ON) {
                	Log.printDebug("VbmLogController : updatedPreferenceData: id is not a event id or in requred id list.");
                }
            }
            
        }
    }

    public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : logCommandChanged: measurementId = " + measurementId + "\t command = "
                    + command);
        }
        if (command) {
            Long id = new Long(measurementId);
            idTable.put(id, id);
        } else {
            idTable.remove(new Long(measurementId));
        }

    }

    public void triggerLogGathering(long measurementId) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("VbmLogController : triggerLogGathering: measurementId = " + measurementId);
        }
        //R7.4 freelife VDTRMASTER-6038
        for (int i=0; i<LOG_ID_LIST.length; i++) {
        	if (LOG_ID_LIST[i].equals(String.valueOf(measurementId))) {
        		sendLog(measurementId, null);
        		break;
        	}
        }
    }
}

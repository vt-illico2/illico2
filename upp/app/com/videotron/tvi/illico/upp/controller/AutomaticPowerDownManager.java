/**
 * 
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hardware.Host;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverStateListener;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.App;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.ui.BaseUI;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class AutomaticPowerDownManager implements TVTimerWentOffListener, PreferenceListener, NotificationOption,
		NotificationListener, ScreenSaverStateListener {

	private static AutomaticPowerDownManager instance;

	private TVTimerSpec goToStandbyTimer;
	private TVTimerSpec showAletAPDTimer;

	private String apdValue = Definitions.OPTION_VALUE_DISABLE;
	private String apdTimeValue = Definitions.FOUR_HOURS;

	private final long ONE_HOUR_TIME = 1 * 60 * 60 * 1000;
	private final long TWO_HOURS_TIME = 2 * 60 * 60 * 1000;
	private final long THREE_HOURS_TIME = 3 * 60 * 60 * 1000;
	private final long FOUR_HOURS_TIME = 4 * 60 * 60 * 1000;

	private final long TWO_MIN_TIME = 2 * 60 * 1000;

	// VDTRMASTER-5652
	private String messageId;

	private long fireTime = 0;
	
	// VDTRMASTER-6115
	private boolean showingScreenSaver = false;
	private boolean showingAPDPopup = false;

	/**
	 * 
	 */
	private AutomaticPowerDownManager() {
		goToStandbyTimer = new TVTimerSpec();
		goToStandbyTimer.addTVTimerWentOffListener(this);
		showAletAPDTimer = new TVTimerSpec();
		showAletAPDTimer.addTVTimerWentOffListener(this);
	}

	public static synchronized AutomaticPowerDownManager getInstance() {
		if (instance == null) {
			instance = new AutomaticPowerDownManager();
		}

		return instance;
	}

	public void start() {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, start()");
		}

		String[] preValues = PreferenceController.getInstance().getAppPreferenceData("APD",
				new String[] { PreferenceNames.AUTOMATIC_POWER_DOWN, PreferenceNames.AUTOMATIC_POWER_DOWN_TIME },
				new String[] { Definitions.OPTION_VALUE_ENABLE, Definitions.FOUR_HOURS });
		ProxyController.getInstance().addProxyListener("APD", instance);
		apdValue = preValues[0];
		apdTimeValue = preValues[1];

		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, readyPreferenceService(), apdValue=" + apdValue
					+ ", apdTimeValue=" + apdTimeValue);
		}

		if (apdValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
			startTimer();
		}
		
		// R7.3 sangyong : VDTRMASTER-6115
		MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (ms != null) {
            try {
                ms.addScreenSaverStateListener(this, FrameworkMain.getInstance()
						.getApplicationName());
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
	}

	public boolean resetAPDTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, resetAPDTimer()");
		}
		showingAPDPopup = false;
		stopTimer();
		// disappear a current
		NotificationService nService = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
		if (nService != null) {
			if (messageId != null) {
				try {
					nService.removeNotify(messageId);
					messageId = null;
				} catch (RemoteException e) {
					Log.print(e);
				}
			}
		}
			
		if (apdValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
			startTimer();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, resetAPDTimer(), APD is " + apdValue + ", so nothing to do");
			}
		}

		return false;
	}
	
	public boolean resetAPDTimerWithKeycode(int keycode) {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, resetAPDTimerWithKeycode()");
		}
		stopTimer();
		boolean hasNotification = false;
		showingAPDPopup = false;
		
		// disappear a current
		NotificationService nService = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
		if (nService != null) {
			if (messageId != null) {
				try {
					hasNotification = true;
					nService.removeNotify(messageId);
					messageId = null;
				} catch (RemoteException e) {
					Log.print(e);
				}
			}
		}

		if (apdValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
			startTimer();
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, resetAPDTimer(), APD is " + apdValue + ", so nothing to do");
			}
		}

		if (hasNotification) {
			hasNotification = false;
			if (keycode == KeyCodes.COLOR_D) {
				try {
					//first, Need to check current status whether Settings is active or not.
					//If currently Settings is inactive, call monitor api.
					if (BaseUI.getHistory() != null && BaseUI.getHistory().length > 1) {
						BaseUI.clearHistory();
						PreferenceController.getInstance().hideUIComponent();
						DataCenter.getInstance().put("PARAMS", new String[]{"NOTIFICATION", PreferenceService.EQUIPMENT});
						PreferenceController.getInstance().prepareUIComponent(BaseUI.UI_MY_SETTINGS, true);
						PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, false);
					} else {
						//goto settings
						try {
							CommunicationManager.getInstance().getMonitorService().
							startUnboundApplication("SETTINGS", new String[] {
			                                    "NOTIFICATION", PreferenceService.EQUIPMENT } );
			            } catch (Exception ex) {
			                Log.print(ex);
			            }
					}
					VbmLogController.getInstance().sendLog(VbmLogController.APD_GOTO_SETTINGS);
					
					return true;
				} catch (Exception e) {
					Log.print(e);
				}
			} else {
				if (keycode == KeyCodes.COLOR_C)
					VbmLogController.getInstance().sendLog(VbmLogController.APD_DISMISSED, "C button");
				else
					VbmLogController.getInstance().sendLog(VbmLogController.APD_DISMISSED, "Other button");
			}
		}
		return false;
	}

	private void startTimer() {

		long delay = 0;
		if (apdTimeValue.equals(Definitions.ONE_HOUR)) {
			delay = ONE_HOUR_TIME; // 2 * 60 * 1000;
		} else if (apdTimeValue.equals(Definitions.TWO_HOURS)) {
			delay = TWO_HOURS_TIME; // 4 * 60 * 1000;
		} else if (apdTimeValue.equals(Definitions.THREE_HOURS)) {
			delay = THREE_HOURS_TIME; // 6 * 60 * 1000;
		} else if (apdTimeValue.equals(Definitions.FOUR_HOURS)) {
			delay = FOUR_HOURS_TIME; // 8 * 60 * 1000;
		}

		// for test (4/5/6/7 min)
		if (Environment.EMULATOR) {
			if (apdTimeValue.equals(Definitions.ONE_HOUR)) {
				delay = 4 * 60 * 1000;
			} else if (apdTimeValue.equals(Definitions.TWO_HOURS)) {
				delay = 5 * 60 * 1000;
			} else if (apdTimeValue.equals(Definitions.THREE_HOURS)) {
				delay = 6 * 60 * 1000;
			} else if (apdTimeValue.equals(Definitions.FOUR_HOURS)) {
				delay = 7 * 60 * 1000;
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, startTimer(), delay " + delay);
		}

		fireTime = System.currentTimeMillis() + delay;
		goToStandbyTimer.setDelayTime(delay);

		try {
			TVTimer.getTimer().scheduleTimerSpec(goToStandbyTimer);
		} catch (TVTimerScheduleFailedException e) {
			Log.printDebug("AutomaticPowerDownManager, startTimer(), can't start a timer, cause " + e.getMessage());
			Log.print(e);
		}

		showAletAPDTimer.setDelayTime(delay - TWO_MIN_TIME);

		try {
			TVTimer.getTimer().scheduleTimerSpec(showAletAPDTimer);
		} catch (TVTimerScheduleFailedException e) {
			Log.printDebug("AutomaticPowerDownManager, startTimer(), can't start a timer to show an alert, cause="
					+ e.getMessage());
			Log.print(e);

		}
	}

	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, stopTimer()");
		}
		if (goToStandbyTimer != null) {
			TVTimer.getTimer().deschedule(goToStandbyTimer);
		}

		if (showAletAPDTimer != null) {
			TVTimer.getTimer().deschedule(showAletAPDTimer);
		}
	}

	private void goToStanbyMode() {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, goToStanbyMode(), mode=" + Host.getInstance().getPowerMode());
		}
		if (Host.getInstance().getPowerMode() == Host.FULL_POWER) {
			
			VbmLogController.getInstance().sendLog(VbmLogController.APD_GOTO_STANBY);
			
			Host.getInstance().setPowerMode(Host.LOW_POWER);
		}
	}
	//R7.3 Sangyong - APD
	private boolean isEnabledState() {
		byte curChState = ChannelContext.STOPPED;

		MonitorService mService = CommunicationManager.getInstance().getMonitorService();

		try {
			boolean isVideoState = mService.isSpecialVideoState();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current isSpecialVideoState="
							+ isVideoState);
			}
			
			if (isVideoState) {
				if (Log.DEBUG_ON) {
					Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current isSpecialVideoState=true");
				}
				
				if (showingScreenSaver) {
					if (Log.DEBUG_ON) {
						Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current showingScreenSaver=true");
					}	
					return true;
				}
				return false;
			}
		} catch (RemoteException e) {
			Log.print(e);
		}
		
		EpgService epgService = CommunicationManager.getInstance().getEpgService();
		try {
			ChannelContext chContext = epgService.getChannelContext(0);
			curChState = chContext.getState();
			
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current STB state=" + mService.getState());
			}
			
			if (mService.getState() == MonitorService.FULL_SCREEN_APP_STATE) {
				if (Log.DEBUG_ON) {
					Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current state is FULL_SCREEN_APP_STATE");
				}
				return true;
			}
		} catch (RemoteException e) {
			Log.print(e);
		}

		if (curChState == ChannelContext.CLEAR) {
			if (epgService != null) {
				TvChannel curChannel;
				int number = 0;
				int type = 0;
				try {
					curChannel = epgService.getCurrentChannel();
					number = curChannel.getNumber();
					type = curChannel.getType();
					
					
					if (Log.DEBUG_ON) {
						Log.printDebug("AutomaticPowerDownManager, isEnabledState(), curChannel type=" + type + ", number="
								+ number);
					}

					if (curChannel.apdDisabled()) {
						if (Log.DEBUG_ON) {
							Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current channel is disabled for APD");
						}

						return false;
					}
					
				} catch (RemoteException e) {
					Log.print(e);
				}
				
			} else {
				if (Log.DEBUG_ON) {
					Log.printDebug("AutomaticPowerDownManager, isEnabledState(), epgService is null");
				}
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, isEnabledState(), current channel is audio channel");
		}
		return true;
	}

	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, timerWentOff(), event=" + event);
		}

		if (event.getTimerSpec().equals(goToStandbyTimer)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, timerWentOff(), timer is goToStanbyTimer");
			}
			if (apdValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
				
				// R7.3 sangyong VDTRMASTER-6115
				if (showingAPDPopup) {
					goToStanbyMode();
				}
				showingAPDPopup = false;
			}
		} else if (event.getTimerSpec().equals(showAletAPDTimer)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("AutomaticPowerDownManager, timerWentOff(), timer is showAletAPDTimer");
			}
			/**
			 * 1. not display on top of Radio and Stingray Music channels
			 * 2. not display on top of Stingray Video channels
			 * 3. not display on top of VOD playback
			 * 4. not display on top of PVR playback
			 * 5. not display on top of INDIGO channels
			 * 6. not display on top of disabled channels(on MS ui) 
			 */
			if (isEnabledState()) {
				NotificationService nService = (NotificationService) DataCenter.getInstance().get(
						NotificationService.IXC_NAME);
				if (nService != null) {
					if (messageId != null) {
						try {
							nService.removeNotify(messageId);
							messageId = null;
						} catch (RemoteException e) {
							Log.print(e);
						}
					}

					try {
						nService.setPriority(0);
						messageId = nService.setNotify(this);
						nService.setRegisterNotificaiton("APD", this);
					} catch (RemoteException e) {
						Log.print(e);
					}
				}
				showingAPDPopup = true;
			}
		}
	}

	public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, notifyUpdatePreferenceValue(), preferenceName=" + preferenceName
					+ ", value=" + value);
		}
		if (preferenceName.equals(PreferenceNames.AUTOMATIC_POWER_DOWN)) {
			apdValue = value;
			resetAPDTimer();
		} else if (preferenceName.equals(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME)) {
			apdTimeValue = value;
			resetAPDTimer();
		}
	}

	public String getApplicationName() throws RemoteException {
		return "APD";
	}

	public long getCreateDate() throws RemoteException {
		return 0;
	}

	public boolean isViewed() throws RemoteException {
		return false;
	}

	public int getDisplayCount() throws RemoteException {
		return 0;
	}

	public String getMessageID() throws RemoteException {
		return null;
	}

	public int getCategories() throws RemoteException {
		return 0;
	}

	public long getDisplayTime() throws RemoteException {
		return fireTime;
	}

	public int getRemainderTime() throws RemoteException {
		return 0;
	}

	public long getRemainderDelay() throws RemoteException {
		return 0;
	}

	public boolean isCountDown() throws RemoteException {
		return true;
	}

	public String getMessage() throws RemoteException {
		// TODO Auto-generated method stub
		return "";
	}

	public String getSubMessage() throws RemoteException {
		return "";
	}

	public int getNotificationPopupType() throws RemoteException {
		return NotificationOption.APD_POPUP;
	}

	public int getNotificationAction() throws RemoteException {
		return NotificationOption.ACTION_APPLICATION;
	}

	public String[] getNotificationActionData() throws RemoteException {
		return null;
	}

	public String getLargePopupMessage() throws RemoteException {
		return null;
	}

	public String getLargePopupSubMessage() throws RemoteException {
		return null;
	}

	public String[][] getButtonNameAction() throws RemoteException {
		return null;
	}

	public void action(String id) throws RemoteException {
		// if (messageId != null && messageId.equals(id)) {
		// resetAPDTimer();
		// }
	}

	public void cancel(String id) throws RemoteException {
		// if (messageId != null && messageId.equals(id)) {
		// resetAPDTimer();
		// }
	}

	public void directShowRequested(String platform, String[] action) throws RemoteException {
		// Nothing
	}

	public void startScreenSaver() throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, startScreenSaver()");
		}
		showingScreenSaver = true;
	}

	public void stopScrenSaver() throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("AutomaticPowerDownManager, stopScrenSaver()");
		}
		showingScreenSaver = false;
	}
}

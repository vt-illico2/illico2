/**
 * @(#)STBTimerController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.upp.BlockTime;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.Time;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class control a Sleep Time and Wake Up Time for STB.
 * @author Woojung Kim
 * @version 1.1
 */
public class STBTimerController implements PowerModeChangeListener, TVTimerWentOffListener, NotificationOption,
        NotificationListener {
    /** a constant value indicate a seven days. */
    private final long SEVEN_DAYS = 604800000;
    /** a constant value indicate a one day. */
    private final long ONE_DAY = 86400000;

    /** a object of this class for singleton. */
    private static STBTimerController instance;

    /** a DecimalFormat to covert a value by format. */
    private DecimalFormat df = new DecimalFormat("00");

    /** a TVTimerSpec for Sleep time. */
    private TVTimerSpec sleepTvSpec;
    /** a TVTimerSpec for Wake up time. */
    private TVTimerSpec wakeUpTvSpec;
    /** a TVTimerSpec for Block time. */
    private TVTimerSpec blockTvSpec;

    /** a time to include a information of sleep time. */
    private Time sleepTime;
    /** a time to include a information of wake-up time. */
    private Time wakeUpTime;

    /** a message id of Notificastion application for Sleep time. */
    private String messageId;
    /** a next time of Sleep time. */
    private long nextSleepTime;
    /** a boolean to indicate whether add a NotificationListener or not. */
    private boolean wasAddedNotiListener;
    /** a boolean to indicate whether updated a block time or not. */
    private boolean updatedBlockTime;

    /**
     * Constructor. Create a object for fields.
     */
    private STBTimerController() {
        sleepTvSpec = new TVTimerSpec();
        sleepTvSpec.addTVTimerWentOffListener(this);
        wakeUpTvSpec = new TVTimerSpec();
        wakeUpTvSpec.addTVTimerWentOffListener(this);
        blockTvSpec = new TVTimerSpec();
        blockTvSpec.addTVTimerWentOffListener(this);
        blockTvSpec.setDelayTime(ONE_DAY);
        blockTvSpec.setRepeat(true);
    }

    /**
     * Get a singleton instance of STBTimerController.
     * @return a object of STBTimerController.
     */
    public static STBTimerController getInstance() {
        if (instance == null) {
            instance = new STBTimerController();
        }
        return instance;
    }

    /**
     * Starts a controller.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: start()");
        }
        Host.getInstance().addPowerModeChangeListener(this);
        try {
            TVTimer.getTimer().scheduleTimerSpec(blockTvSpec);
        } catch (TVTimerScheduleFailedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops a controller.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: stop()");
        }
        Host.getInstance().removePowerModeChangeListener(this);
        sleepTvSpec.removeTVTimerWentOffListener(this);
        wakeUpTvSpec.removeTVTimerWentOffListener(this);
        blockTvSpec.removeTVTimerWentOffListener(this);
    }

    /**
     * update a data for Sleep time.
     * @param value a data of sleep time.
     */
    public void updateSleepTimer(String value) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: updateSleepTimer : " + value);
        }
        if (sleepTime == null) {
            sleepTime = new Time();
        }

        sleepTime.setData(value);
        startSleepTimer(System.currentTimeMillis());
    }

    /**
     * update a data for Wake up time.
     * @param value a data of wake up time.
     */
    public void updateWakeUpTimer(String value) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: updateWakeUpTimer : " + value);
        }
        if (wakeUpTime == null) {
            wakeUpTime = new Time();
        }

        wakeUpTime.setData(value);
        startWakeUpTimer();
    }

    /**
     * Starts a sleep timer.
     */
    private void startSleepTimer(long baseTime) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: startSleepTimer()");
        }
        stopSleepTimer();

        NotificationService nService = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
        if (nService != null) {
            if (messageId != null) {
                try {
                    nService.removeNotify(messageId);
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
        }

        if (sleepTime.getActive().equals(Definitions.OPTION_VALUE_ENABLE)) {
            nextSleepTime = getNextTime(sleepTime, baseTime);

            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: startSleepTimer: nextSleepTime = " + nextSleepTime);
            }

            if (nextSleepTime > 0) {
                sleepTvSpec.setAbsoluteTime(nextSleepTime);

                try {
                    TVTimer.getTimer().scheduleTimerSpec(sleepTvSpec);
                } catch (TVTimerScheduleFailedException e) {
                    Log.print(e);
                }

                if (nService != null) {
                    try {
                        nService.setPriority(0);
                        messageId = nService.setNotify(this);
                        if (!wasAddedNotiListener) {
                            nService.setRegisterNotificaiton(FrameworkMain.getInstance().getApplicationName(), this);
                        }
                    } catch (RemoteException e) {
                        Log.print(e);
                    }
                }
            } else {
                // make a Disable
                if (Log.DEBUG_ON) {
                    Log.printDebug("STBTimerController: startSleepTimer: can not find a next time so do nothing.");
                }

                // sleepTime.setActive(Definitions.OPTION_VALUE_DISABLE);
                // DataManager.getInstance().setPreference(PreferenceNames.STB_SLEEP_TIME, sleepTime.getStringData());
            }

        } else {
            messageId = null;
        }

    }

    /**
     * Stop a sleep timer.
     */
    private void stopSleepTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: stopSleepTimer()");
        }
        TVTimer.getTimer().deschedule(sleepTvSpec);
    }

    /**
     * Start a wake up timer.
     */
    private void startWakeUpTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: startWakeUpTimer()");
        }
        stopWakeUpTimer();

        if (wakeUpTime.getActive().equals(Definitions.OPTION_VALUE_ENABLE)) {
            long next = getNextTime(wakeUpTime, System.currentTimeMillis());
            if (next > 0) {
                wakeUpTvSpec.setAbsoluteTime(next);

                try {
                    TVTimer.getTimer().scheduleTimerSpec(wakeUpTvSpec);
                } catch (TVTimerScheduleFailedException e) {
                    Log.print(e);
                }
            } else {
                // make a Disable
                if (Log.DEBUG_ON) {
                    Log.printDebug("STBTimerController: startWakeUpTimer: can not find a next time so do nothing.");
                }

                // wakeUpTime.setActive(Definitions.OPTION_VALUE_DISABLE);
                // DataManager.getInstance().setPreference(PreferenceNames.STB_WAKE_UP_TIME,
                // wakeUpTime.getStringData());
            }
        }
    }

    /**
     * Stop a wake up timer.
     */
    private void stopWakeUpTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: stopWakeUpTimer()");
        }
        TVTimer.getTimer().deschedule(wakeUpTvSpec);
    }

    /**
     * Receive a event of Power Mode.
     */
    public void powerModeChanged(int state) {
        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController: powerModeChanged: state = " + state);
        }
    }

    /**
     * Receive a timer event.
     * @param event a timer event.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: timerWentOff()");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController: timerWentOff: Host power mode = " + Host.getInstance().getPowerMode());
        }

        if (event.getTimerSpec().equals(sleepTvSpec)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: timerWentOff: this time event is for Sleep time");
            }
            int mode = MonitorService.STB_MODE_NORMAL;

            MonitorService mService = CommunicationManager.getInstance().getMonitorService();

            if (mService != null) {
                try {
                    mode = mService.getSTBMode();
                } catch (RemoteException e) {
                    Log.printWarning(e);
                }
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: timerWentOff: stb mode = " + mode);
            }

            if (mode == MonitorService.STB_MODE_NORMAL && Host.getInstance().getPowerMode() == Host.FULL_POWER) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("STBTimerController: timerWentOff: Host power mode change FULL_POWER TO LOW_POWER");
                }
                Host.getInstance().setPowerMode(Host.LOW_POWER);
                startSleepTimer(System.currentTimeMillis());
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Host power mode is LOW_POWER, so set a next Sleep time if it exist");
                }
                startSleepTimer(System.currentTimeMillis());
            }
        } else if (event.getTimerSpec().equals(wakeUpTvSpec)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: timerWentOff: this time event is for Wake-up Time");
            }
            if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("STBTimerController: timerWentOff: Host power mode change LOW_POWER TO FULL_POWER");
                }
                Host.getInstance().setPowerMode(Host.FULL_POWER);
                startWakeUpTimer();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Host power mode is FULL_POWER, so set a next Wake-up time if it exist");
                }
                startWakeUpTimer();
            }

        } else if (event.getTimerSpec().equals(blockTvSpec)) {
            updateTimeBasedRestrictions();
        }
    }

    /**
     * Get a next Time.
     * @param time current time.
     * @return next time.
     */
    private long getNextTime(Time time, long baseTime) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: getNextTime: save time : " + new Date(time.getSaveTime()));
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController: getNextTime: baseTime = " + new Date(baseTime));
        }

        boolean repeat = time.getRepeat().equals(Definitions.OPTION_VALUE_YES);
        if (repeat || (time.getSaveTime() + SEVEN_DAYS) > System.currentTimeMillis()) {

            int idx = Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
            String[] days = TextUtil.tokenize(time.getDays(), PreferenceService.PREFERENCE_DELIMETER);
            for (int i = 0; i < 7; i++) {
                if (days[idx].equals(Definitions.OPTION_VALUE_YES)) {
                    long next = getDayTime(time, i);
                    if (next > baseTime) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("STBTimerController: getNextTime: find a next");
                        }
                        return next;
                    }
                }

                if (++idx == 7) {
                    idx = 0;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: getNextTime: can't find a next time");
            }
        }
        return 0;
    }

    /**
     * Get a time of target day.
     * @param time a time to have a information.
     * @param day a target day.
     * @return a found time.
     */
    private long getDayTime(Time time, int day) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: getDayTime()");
        }
        Calendar cal = Calendar.getInstance();
        int hour = Integer.parseInt(time.getStartHour());
        int min = Integer.parseInt(time.getStartMin());
        if (time.getStartAmPm().equals(Definitions.TIME_PM) && hour < 12) {
            hour += 12;
        } else if (time.getStartAmPm().equals(Definitions.TIME_AM) && hour == 12) {
        	hour = 0;
        }

        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, 0);
        cal.add(Calendar.DAY_OF_WEEK, day);

        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController: getDayTime: next : " + cal.getTime());
        }
        return cal.getTimeInMillis();
    }

    /**
     * Update a time based retrictions.
     */
    public void updateTimeBasedRestrictions() {
        BlockTime[] blockTimes = getBlockTimes();
        if (blockTimes != null && blockTimes.length > 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("STBTimerController: updateTimeBasedRestrictions: blockTimes : " + blockTimes.length);
            }
        }
        ProxyController.getInstance().notifyUpdateBlockTimeData(blockTimes);
    }

    /**
     * Returns a true if Time-based Restriction is updated, false otherwise.
     * @return boolean
     */
    public boolean getUpdatedBlockTime() {
        return updatedBlockTime;
    }

    /**
     * Sets a boolean to be updated a Time-based Restriction.
     * @param update
     */
    public void setUpdatedBlockTime(boolean update) {
        updatedBlockTime = update;
    }

    /**
     * Get a block times.
     * @return block times.
     */
    public BlockTime[] getBlockTimes() {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: getBlockTimes");
        }
        BlockTime[] blockTimes = null;

        Preference pre = DataManager.getInstance().getPreference(RightFilter.TIME_BASED_RESTRICTIONS);
        String value = pre.getCurrentValue();

        Vector timeVector = new Vector();
        String[] timeData = TextUtil.tokenize(value, '^');

        if (timeData != null && timeData.length > 0) {
            Time[] times = new Time[timeData.length];
            for (int i = 0; i < timeData.length; i++) {
                times[i] = new Time();
                times[i].setData(timeData[i]);

                if (times[i].getActive().equals(Definitions.OPTION_VALUE_ENABLE)) {
                    if (times[i].getRepeat().equals(Definitions.OPTION_VALUE_YES)
                            || times[i].getSaveTime() + SEVEN_DAYS > System.currentTimeMillis()) {

                        if (times[i].getDays().indexOf(Definitions.OPTION_VALUE_YES) > -1) {
                            timeVector.add(times[i]);
                        } else {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("STBTimerController: getBlockTimes: all day is No");
                            }
                        }
                    }
                }
            }

            StringBuffer sf = new StringBuffer();

            Time[] realData = new Time[timeVector.size()];
            timeVector.copyInto(realData);

            blockTimes = new BlockTime[timeVector.size()];

            for (int i = 0; i < realData.length; i++) {
                blockTimes[i] = new BlockTime();
                sf.setLength(0);

                // start time
                sf.append(getCalculateHour(realData[i].getStartHour(), realData[i].getStartAmPm()));
                sf.append(realData[i].getStartMin());

                blockTimes[i].setStartTime(sf.toString());

                sf.setLength(0);

                // end time
                sf.append(getCalculateHour(realData[i].getEndHour(), realData[i].getEndAmPm()));
                sf.append(realData[i].getEndMin());

                blockTimes[i].setEndTime(sf.toString());

                blockTimes[i].setRepeat(realData[i].getDays());
            }
        }
        return blockTimes;
    }

    /**
     * Get a hour by 24 hours.
     * @param hour
     * @param amPm
     * @return
     */
    private String getCalculateHour(String hour, String amPm) {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController: getCalculateHour: hour = " + hour + "\t amPm = " + amPm);
        }
        int intHour = Integer.parseInt(hour);
        if (amPm.equals(Definitions.TIME_PM) && intHour < 12) {
            intHour += 12;
        }
        return df.format(intHour);
    }

    public String getMessage() throws RemoteException {
        return "";
    }

    public String getMessageID() throws RemoteException {
        return null;
    }

    public int getNotificationAction() throws RemoteException {
        return ACTION_SLEEP_TIMER;
    }

    public String[] getNotificationActionData() throws RemoteException {
        return null;
    }

    public int getNotificationPopupType() throws RemoteException {
        return SLEEP_TIMER_REMAINER_POPUP;
    }

    public long getRemainderDelay() throws RemoteException {
        return 0;
    }

    public int getRemainderTime() throws RemoteException {
        return 0;
    }

    public void setApplicationName(String name) throws RemoteException {
    }

    public void setCategories(int categories) throws RemoteException {
    }

    public void setDisplayTime(long time) throws RemoteException {
    }

    public void setMessage(String message) throws RemoteException {
    }

    public void setMessageID(String message) throws RemoteException {
    }

    public void setNotificationAction(int action) throws RemoteException {
    }

    public void setNotificationPopupType(int status) throws RemoteException {
    }

    public void setRemainderDelay(long delay) throws RemoteException {
    }

    public void setRemainderTime(int time) throws RemoteException {
    }

    public long getCreateDate() throws RemoteException {
        return 0;
    }

    public int getDisplayCount() throws RemoteException {
        return 0;
    }

    public void setViewed(boolean b) throws RemoteException {

    }

    public boolean isViewed() throws RemoteException {
        return false;
    }

    public void setCreateDate(long createData) throws RemoteException {

    }

    public void setDisplayCount(int count) throws RemoteException {

    }

    public String getLargePopupMessage() throws RemoteException {
        return null;
    }

    public String getSubMessage() throws RemoteException {
        return null;
    }

    public void setLargePopupMessage(String message) throws RemoteException {

    }

    public void setSubMessage(String message) throws RemoteException {

    }

    public String[][] getButtonNameAction() throws RemoteException {
        return null;
    }

    public String getLargePopupSubMessage() throws RemoteException {
        return null;
    }

    public void setButtonNameAction(String[][] action) throws RemoteException {

    }

    public void setLargePopupSubMessage(String message) throws RemoteException {

    }

    public void setNotificationActionData(String[] param) throws RemoteException {
    }

    public boolean isCountDown() throws RemoteException {
        return true;
    }

    public void setCountDown(boolean b) throws RemoteException {
    }

    public String getApplicationName() throws RemoteException {
        return FrameworkMain.getInstance().getApplicationName();
    }

    public int getCategories() throws RemoteException {
        return 0;
    }

    /**
     * Get a next Sleep time.
     * @param a long of next sleep time.
     */
    public long getDisplayTime() throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController : getDisplayTime()");
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController : nextSleepTime" + nextSleepTime);
        }
        return nextSleepTime;
    }

    /**
     * Called When user enter on Notification popup. if id is for Sleep time, restart sleep timer.
     * @param id message id.
     */
    public void action(String id) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("STBTimerController : action() : id = " + id);
        }

        NotificationService notiService = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
        long notiTime = notiService.getDisplayTime();
        
        if (Log.DEBUG_ON) {
            Log.printDebug("STBTimerController : action() : messageId = " + messageId);
            Log.printDebug("STBTimerController : action() : notiTime = " + notiTime);
        }
        
        
        
        if (messageId != null && messageId.equals(id)) {
            // stop current sleep time.
            startSleepTimer(nextSleepTime + notiTime);
        }
    }

    public void cancel(String id) throws RemoteException {
    }

    public void directShowRequested(String platform, String[] action) throws RemoteException {

    }
}

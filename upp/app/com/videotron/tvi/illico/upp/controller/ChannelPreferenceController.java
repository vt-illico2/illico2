/**
 * 
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class ChannelPreferenceController {

	private static ChannelPreferenceController instance;

	/**
	 * 
	 */
	private ChannelPreferenceController() {
	}

	public static ChannelPreferenceController getInstance() {

		if (instance == null) {
			instance = new ChannelPreferenceController();
		}

		return instance;
	}

	public void addBlockChannel(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addBlockChannel, callLetter=" + callLetter);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);

		addChannel(pre, callLetter);
	}

	public void removeBlockChannel(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeBlockChannel, callLetter=" + callLetter);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);

		removeChannel(pre, callLetter);
	}
	
	public void addBlockChannels(String[] callLetters) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addBlockChannel, callLetters=" + callLetters);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);

		addChannels(pre, callLetters);
	}

	public void removeBlockChannels(String[] callLetters) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeBlockChannel, callLetters=" + callLetters);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);

		removeChannels(pre, callLetters);
	}

	public void addFavoriteChannel(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addFavoriteChannel, callLetter=" + callLetter);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
				.get(PreferenceNames.FAVOURITE_CHANNEL);

		addFavoriteChannel(pre, callLetter);
	}

	public void removeFavoriteChannel(String callLetter) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeFavoriteChannel, callLetter=" + callLetter);
		}
		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
				.get(PreferenceNames.FAVOURITE_CHANNEL);

		removeChannel(pre, callLetter);
	}
	
	public void addFavoriteChannels(String[] callLetters) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addFavoriteChannels, callLetters=" + callLetters);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
				.get(PreferenceNames.FAVOURITE_CHANNEL);

		addFavoriteChannels(pre, callLetters);
	}

	public void removeFavoriteChannels(String[] callLetters) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeFavoriteChannels, callLetters=" + callLetters);
		}
		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
				.get(PreferenceNames.FAVOURITE_CHANNEL);

		removeChannels(pre, callLetters);
	}

	private void addChannel(Preference pre, String callLetter) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addChannel, preference.id=" + pre.getID() + ", currentValue="
					+ storedData);
		}

		String[] parsedData = null;
		boolean isFound = false;
		String sisterCallLetter = null;

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

			isFound = isExisted(parsedData, callLetter);

			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addChannel, isFound = " + isFound);
			}
		}

		try {
			EpgService eService = CommunicationManager.getInstance().getEpgService();
			TvChannel channel = eService.getChannel(callLetter);

			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addChannel, channel=" + channel + ", sisterChannel="
						+ (channel != null ? channel.getSisterChannel() : channel));
			}
			
			if (channel == null || channel.getType() == TvChannel.TYPE_VIRTUAL) {
				// To hide a loading in VOD
				DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
				return;
			}

			if (channel != null && channel.getSisterChannel() != null) {
				boolean isFoundSister = isExisted(parsedData, channel.getSisterChannel().getCallLetter());
				if (!isFoundSister) {
					sisterCallLetter = channel.getSisterChannel().getCallLetter();
				}
			}

		} catch (RemoteException e) {
			Log.print(e);
		} catch (NullPointerException e) {
			Log.print(e);
		}

		StringBuffer sb = new StringBuffer();
		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			sb.append(storedData);
		}

		if (!isFound) {
			if (sb.length() > 1) {
				sb.append(PreferenceService.PREFERENCE_DELIMETER);
			}
			sb.append(callLetter);
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addChannel, sister CallLetter=" + sisterCallLetter);
		}

		if (sisterCallLetter != null) {
			if (sb.length() > 1) {
				sb.append(PreferenceService.PREFERENCE_DELIMETER);
			}
			sb.append(sisterCallLetter);
		}

//		rearrangeAndSaveChannels(pre, sb.toString());
		pre.setCurrentValue(sb.toString());
		DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
	}
	
	private void addChannels(Preference pre, String[] callLetters) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addChannels, preference.id=" + pre.getID() + ", currentValue="
					+ storedData);
		}

		String[] parsedData = null;
		boolean isFound = false;
		String sisterCallLetter = null;

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < callLetters.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addChannels, callLetters[" + i + "]=" + callLetters[i]);
			}
			sb.setLength(0);
			isFound = false;
			sisterCallLetter = null;
			if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
				parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
	
				isFound = isExisted(parsedData, callLetters[i]);
	
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addChannels, isFound=" + isFound);
				}
			}
	
			try {
				EpgService eService = CommunicationManager.getInstance().getEpgService();
				TvChannel channel = eService.getChannel(callLetters[i]);
	
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addChannels, channel=" + channel + ", sisterChannel="
							+ (channel != null ? channel.getSisterChannel() : channel));
				}
				
				if (channel == null || channel.getType() == TvChannel.TYPE_VIRTUAL) {
					continue;
				}
	
				if (channel != null && channel.getSisterChannel() != null) {
					boolean isFoundSister = isExisted(parsedData, channel.getSisterChannel().getCallLetter());
					if (!isFoundSister) {
						sisterCallLetter = channel.getSisterChannel().getCallLetter();
					}
				}
	
			} catch (RemoteException e) {
				Log.print(e);
			} catch (NullPointerException e) {
				Log.print(e);
			}
	
			if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
				sb.append(storedData);
			}
	
			if (!isFound) {
				if (sb.length() > 1) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append(callLetters[i]);
			}
	
			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addChannels, sister CallLetter=" + sisterCallLetter);
			}
	
			if (sisterCallLetter != null) {
				if (sb.length() > 1) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append(sisterCallLetter);
			}
			
			storedData = sb.toString();
		}

//		rearrangeAndSaveChannels(pre, sb.toString());
		pre.setCurrentValue(storedData);
		DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
	}
	
	private void addFavoriteChannel(Preference pre, String callLetter) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addFavoriteChannel, preference.id=" + pre.getID() + ", currentValue="
					+ storedData);
		}

		String[] parsedData = null;
		boolean isFound = false;
		String sisterCallLetter = null;

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

			isFound = isExisted(parsedData, callLetter);

			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addFavoriteChannel, isFound = " + isFound);
			}
		}

		try {
			EpgService eService = CommunicationManager.getInstance().getEpgService();
			TvChannel channel = eService.getChannel(callLetter);

			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addFavoriteChannel, channel=" + channel + ", sisterChannel="
						+ (channel != null ? channel.getSisterChannel() : channel));
			}
			
			if (channel == null || channel.getType() == TvChannel.TYPE_VIRTUAL) {
				return;
			}

			// R7
			if (PreferenceController.getInstance().isChannelGrouped()) {
				if (channel != null && channel.getSisterChannel() != null) {
					boolean isFoundSister = isExisted(parsedData, channel.getSisterChannel().getCallLetter());
					if (!isFoundSister) {
						sisterCallLetter = channel.getSisterChannel().getCallLetter();
					}
				}
			}

		} catch (RemoteException e) {
			Log.print(e);
		} catch (NullPointerException e) {
			Log.print(e);
		}

		StringBuffer sb = new StringBuffer();
		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			sb.append(storedData);
		}

		if (!isFound) {
			if (sb.length() > 1) {
				sb.append(PreferenceService.PREFERENCE_DELIMETER);
			}
			sb.append(callLetter);
		}

		// R7
		if (PreferenceController.getInstance().isChannelGrouped()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addFavoriteChannel, sister CallLetter=" + sisterCallLetter);
			}
	
			if (sisterCallLetter != null) {
				if (sb.length() > 1) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append(sisterCallLetter);
			}
		}

//		rearrangeAndSaveChannels(pre, sb.toString());
		pre.setCurrentValue(sb.toString());
		DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
	}
	
	private void addFavoriteChannels(Preference pre, String[] callLetters) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, addFavoriteChannels, preference.id=" + pre.getID() + ", currentValue="
					+ storedData);
		}

		String[] parsedData = null;
		boolean isFound = false;
		String sisterCallLetter = null;
		boolean isChannelGrouped = PreferenceController.getInstance().isChannelGrouped();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < callLetters.length; i++) {
			if (Log.DEBUG_ON) {
				Log.printDebug("ChannelPreferenceController, addFavoriteChannels, callLetters[" + i + "]=" + callLetters[i]);
			}
			sb.setLength(0);
			isFound = false;
			sisterCallLetter = null;
			if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
				parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
	
				isFound = isExisted(parsedData, callLetters[i]);
	
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addFavoriteChannels, isFound=" + isFound);
				}
			}
	
			TvChannel channel = null;
			try {
				EpgService eService = CommunicationManager.getInstance().getEpgService();
				channel = eService.getChannel(callLetters[i]);
	
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addFavoriteChannels, channel=" + channel + ", sisterChannel="
							+ (channel != null ? channel.getSisterChannel() : channel));
				}
				
				if (channel == null || channel.getType() == TvChannel.TYPE_VIRTUAL) {
					continue;
				}
	
				if (channel != null && channel.getSisterChannel() != null) {
					boolean isFoundSister = isExisted(parsedData, channel.getSisterChannel().getCallLetter());
					if (!isFoundSister || !isChannelGrouped) {
						sisterCallLetter = channel.getSisterChannel().getCallLetter();
					}
				}
	
			} catch (RemoteException e) {
				Log.print(e);
			} catch (NullPointerException e) {
				Log.print(e);
			}
	
			if (isChannelGrouped) {
				if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
					sb.append(storedData);
				}
				
				if (!isFound) {
					if (sb.length() > 1) {
						sb.append(PreferenceService.PREFERENCE_DELIMETER);
					}
					sb.append(callLetters[i]);
				}
		
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addFavoriteChannels, sister CallLetter=" + sisterCallLetter);
				}
		
				if (sisterCallLetter != null) {
					if (sb.length() > 1) {
						sb.append(PreferenceService.PREFERENCE_DELIMETER);
					}
					sb.append(sisterCallLetter);
				}
			} else {
				
				// remove for duplicate
				if (parsedData != null) {
					boolean isAdded = false;
					for (int j = 0; j < parsedData.length; j++) {
		
						if (parsedData[j].equals(callLetters[i])) {
							continue;
						} else if (parsedData[j].equals(sisterCallLetter)) {
							continue;
						}
		
						if (isAdded) {
							sb.append(PreferenceService.PREFERENCE_DELIMETER);
						}
						sb.append(parsedData[j]);
						isAdded = true;
					}
				}
				
				if (channel != null) {
					try {
						if (channel.isHd() && channel.isSubscribed()) {
							if (sb.length() > 1) {
								sb.append(PreferenceService.PREFERENCE_DELIMETER);
							}
							sb.append(channel.getCallLetter());
						} else if (channel.getSisterChannel() != null && channel.getSisterChannel().isSubscribed()) {
							if (sb.length() > 1) {
								sb.append(PreferenceService.PREFERENCE_DELIMETER);
							}
							sb.append(channel.getSisterChannel().getCallLetter());
						} else if (channel.isHd() == false && channel.isSubscribed()) {
							if (sb.length() > 1) {
								sb.append(PreferenceService.PREFERENCE_DELIMETER);
							}
							sb.append(channel.getCallLetter());
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}
			}
			
			
			storedData = sb.toString();
		}

//		rearrangeAndSaveChannels(pre, sb.toString());
		pre.setCurrentValue(storedData);
		DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
	}

	private void removeChannel(Preference pre, String callLetter) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeChannel, preference.id=" + pre.getID()
					+ ", currentValue=" + storedData);
		}

		String sisterCallLetter = "";

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

			try {
				EpgService eService = CommunicationManager.getInstance().getEpgService();
				TvChannel channel = eService.getChannel(callLetter);

				if (channel != null && channel.getSisterChannel() != null) {
					sisterCallLetter = channel.getSisterChannel().getCallLetter();
				}

				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, removeChannel, sister CallLetter=" + sisterCallLetter);
				}
			} catch (RemoteException e) {
				Log.print(e);
			} catch (NullPointerException e) {
				Log.print(e);
			}

			StringBuffer sb = new StringBuffer();
			boolean isAdded = false;
			for (int i = 0; i < parsedData.length; i++) {

				if (parsedData[i].equals(callLetter)) {
					continue;
				} else if (parsedData[i].equals(sisterCallLetter)) {
					continue;
				}

				if (isAdded) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append(parsedData[i]);
				isAdded = true;
			}

//			rearrangeAndSaveChannels(pre, sb.toString());
			pre.setCurrentValue(sb.toString());
			DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
		}
	}
	
	private void removeChannels(Preference pre, String[] callLetters) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, removeChannels, preference.id=" + pre.getID()
					+ ", currentValue=" + storedData);
		}

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < callLetters.length; i++) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, addChannels, callLetters[" + i + "]=" + callLetters[i]);
				}
				String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
	
				String sisterCallLetter = "";
				sb.setLength(0);
				try {
					EpgService eService = CommunicationManager.getInstance().getEpgService();
					TvChannel channel = eService.getChannel(callLetters[i]);
	
					if (channel != null && channel.getSisterChannel() != null) {
						sisterCallLetter = channel.getSisterChannel().getCallLetter();
					}
	
					if (Log.DEBUG_ON) {
						Log.printDebug("ChannelPreferenceController, removeChannel, sister CallLetter=" + sisterCallLetter);
					}
				} catch (RemoteException e) {
					Log.print(e);
				} catch (NullPointerException e) {
					Log.print(e);
				}
	
				boolean isAdded = false;
				for (int j = 0; j < parsedData.length; j++) {
	
					if (parsedData[j].equals(callLetters[i])) {
						continue;
					} else if (parsedData[j].equals(sisterCallLetter)) {
						continue;
					}
	
					if (isAdded) {
						sb.append(PreferenceService.PREFERENCE_DELIMETER);
					}
					sb.append(parsedData[j]);
					isAdded = true;
				}
				storedData = sb.toString();
			}

//			rearrangeAndSaveChannels(pre, sb.toString());
			pre.setCurrentValue(storedData);
			DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
		}
	}

	public void processSisterChannels(boolean grouped) {
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, processSisterChannels");
		}

		if (grouped) {
			Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
					.get(RightFilter.CHANNEL_RESTRICTION);
	
			processChannels(pre);
		}

		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
				.get(PreferenceNames.FAVOURITE_CHANNEL);

		if (grouped) {
			processChannels(pre);
		} else {
			proccessFavoriteChannel(pre);
		}
	}

	private void processChannels(Preference pre) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, processChannels, preference.id=" + pre.getID()
					+ ", currentValue=" + storedData);
		}

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

			Vector newCallLetters = new Vector();
			for (int i = 0; i < parsedData.length; i++) {
				try {
					EpgService eService = CommunicationManager.getInstance().getEpgService();
					TvChannel channel = eService.getChannel(parsedData[i]);

					if (channel != null && channel.getSisterChannel() != null) {
						String sisterCallLetter = channel.getSisterChannel().getCallLetter();

						if (Log.DEBUG_ON) {
							Log.printDebug("ChannelPreferenceController, processChannels, sister CallLetter="
									+ sisterCallLetter);
						}

						if (!isExisted(parsedData, sisterCallLetter)) {
							newCallLetters.add(sisterCallLetter);
						}
					}
				} catch (RemoteException e) {
					Log.print(e);
				} catch (NullPointerException e) {
					Log.print(e);
				}
			}

			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < parsedData.length; i++) {

				if (i != 0) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append(parsedData[i]);
			}

			for (int i = 0; i < newCallLetters.size(); i++) {
				sb.append(PreferenceService.PREFERENCE_DELIMETER);
				sb.append((String) newCallLetters.get(i));
			}

			newCallLetters.clear();

//			rearrangeAndSaveChannels(pre, sb.toString());
			pre.setCurrentValue(sb.toString());
			DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());

		}
	}
	
	// R7
	/**
	 * This method have to be called when SD/HD grouping is ungrouped
	 * 
	 * @param pre
	 */
	private void proccessFavoriteChannel(Preference pre) {
		String storedData = pre.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("ChannelPreferenceController, proccessFavoriteChannel, preference.id=" + pre.getID()
					+ ", currentValue=" + storedData);
		}

		if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
			Vector newCallLetters = new Vector();
			
			String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

			for (int i = 0; i < parsedData.length; i++) {
				try {
//					EpgService eService = CommunicationManager.getInstance().getEpgService();
//					TvChannel channel = eService.getChannel(parsedData[i]);
//
//					if (channel != null && channel.isSubscribed()) {
//						if (Log.DEBUG_ON) {
//							Log.printDebug("ChannelPreferenceController, proccessFavoriteChannel, channel.isSubscribed="
//									+ channel.isSubscribed());
//						}
//
//						newCallLetters.add(parsedData[i]);
//					}
					
					EpgService eService = CommunicationManager.getInstance().getEpgService();
					String callLetter = (parsedData[i]);
					TvChannel channel = eService.getChannel(parsedData[i]);
					if (channel.isHd()) {
						if (channel.isSubscribed() == false) {
//							TvChannel sisChannel = channel.getSisterChannel();
//							if (sisChannel != null && sisChannel.isSubscribed()) {
//								continue;
//							} else {
//								continue;
//							}
							continue;
						}
					} else { // in the case of SD channel
						TvChannel sisChannel = channel.getSisterChannel();
						if (sisChannel != null && sisChannel.isSubscribed()) {
							continue;
						}
					}
					
					if (!newCallLetters.contains(callLetter)) {
						newCallLetters.add(callLetter);
					}
				} catch (RemoteException e) {
					Log.print(e);
				} catch (NullPointerException e) {
					Log.print(e);
				}
			}

			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < newCallLetters.size(); i++) {
				if (i != 0) {
					sb.append(PreferenceService.PREFERENCE_DELIMETER);
				}
				sb.append((String) newCallLetters.get(i));
			}

			newCallLetters.clear();

//			rearrangeAndSaveChannels(pre, sb.toString());
			pre.setCurrentValue(sb.toString());
			DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());

		}
	}

//	private void rearrangeAndSaveChannels(Preference pre, String data) {
//		if (Log.DEBUG_ON) {
//			Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels, preference.id=" + pre.getID()
//					+ ", data=[" + data + "]");
//		}
//		String[] parsedData = TextUtil.tokenize(data, PreferenceService.PREFERENCE_DELIMETER);
//
//		Vector newListVector = new Vector();
//
//		EpgService eService = CommunicationManager.getInstance().getEpgService();
//
//		try {
//			for (int i = 0; i < parsedData.length; i++) {
//				Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels: parsedData[i]=[" + data + "]");
//				if (newListVector.size() == 0) {
//					newListVector.add(parsedData[i]);
//				} else {
//					TvChannel tChannel = eService.getChannel(parsedData[i]);
//
//					Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels: tChannel=" + tChannel);
//					boolean isAdded = false;
//					if (tChannel != null) {
//						for (int j = 0; j < newListVector.size(); j++) {
//							String callLetter = (String) newListVector.get(j);
//	
//							TvChannel channel = eService.getChannel(callLetter);
//							Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels: channel=" + channel);
//							if (channel != null && tChannel.getNumber() < channel.getNumber()) {
//								newListVector.add(j, tChannel.getCallLetter());
//								isAdded = true;
//								break;
//							}
//						}
//	
//						if (!isAdded) {
//							newListVector.add(tChannel.getCallLetter());
//						}
//					} else {
//						newListVector.add(parsedData[i]);
//					}
//				}
//			}
//		} catch (RemoteException e) {
//			Log.print(e);
//		}
//
//		Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels: newListVector.size=["
//				+ newListVector.size() + "]");
//		String[] newData = new String[newListVector.size()];
//
//		newListVector.copyInto(newData);
//
//		StringBuffer sb = new StringBuffer();
//		for (int i = 0; i < newData.length; i++) {
//
//			if (i != 0) {
//				sb.append(PreferenceService.PREFERENCE_DELIMETER);
//			}
//			Log.printDebug("ChannelPreferenceController, rearrangeAndSaveChannels: newData[i]=[" + newData[i] + "]");
//			sb.append(newData[i]);
//		}
//
//		pre.setCurrentValue(sb.toString());
//		DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
//	}

	private boolean isExisted(String[] list, String callLetter) {
		if (list == null)
			return false;
		for (int i = 0; i < list.length; i++) {
			if (list[i].equals(callLetter)) {
				if (Log.DEBUG_ON) {
					Log.printDebug("ChannelPreferenceController, isExisted, find in current list");
				}
				return true;
			}
		}

		return false;
	}
}

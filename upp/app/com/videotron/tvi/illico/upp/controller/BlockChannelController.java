/**
 * 
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.RemoteException;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author zestyman
 *
 */
public class BlockChannelController {

	private static BlockChannelController instance;

	/**
	 * 
	 */
	private BlockChannelController() {
	}

	public static BlockChannelController getInstance() {

		if (instance == null) {
			instance = new BlockChannelController();
		}

		return instance;
	}

	public void addBlockChannel(String callLetter) {
		if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, addBlockChannel, callLetter=" + callLetter);
        }
		
		EpgService eService = CommunicationManager.getInstance().getEpgService();
		
		
		// check that callLetter is existed in current list
		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);
		
		
		String storedData = pre.getCurrentValue();
        if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, addBlockChannel, channel restriction = " + storedData);
        }
        
        String[] parsedData = null;
        boolean isFound = false;
        String sisterCallLetter = null;
        
        if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
            parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
            
            isFound = isExisted(parsedData, callLetter);
            
            if (Log.DEBUG_ON) {
                Log.printDebug("BlockChannelController, addBlockChannel, found in Restriction channel list");
            }
        }
        
        try {
			TvChannel channel = eService.getChannel(callLetter);
			
			if (Log.DEBUG_ON) {
                Log.printDebug("BlockChannelController, channel=" + channel + ", sisterChannel=" + channel!=null?channel.getSisterChannel():channel);
            }
			
			if (channel != null && channel.getSisterChannel() != null) {
				boolean isFoundSister = isExisted(parsedData, channel.getSisterChannel().getCallLetter());
				if (!isFoundSister) {
					sisterCallLetter = channel.getSisterChannel().getCallLetter();
				}
			}
			
		} catch (RemoteException e) {
			Log.print(e);
		} catch (NullPointerException e) {
			Log.print(e);
		}
		
		// add in channel restriction list
        
        StringBuffer sb = new StringBuffer();
        if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
        	sb.append(storedData);
        	sb.append(PreferenceService.PREFERENCE_DELIMETER);
        }
        
        if (!isFound) {
        	sb.append(callLetter);
        }
        
        if (Log.DEBUG_ON) {
        	Log.printDebug("BlockChannelController, addBlockChannel, sister CallLetter=" + sisterCallLetter);
        }
        
        if (sisterCallLetter != null) {
        	if (sb.length() > 1) {
        		sb.append(PreferenceService.PREFERENCE_DELIMETER);
        	}
        	sb.append(sisterCallLetter);
        }
        
        rearrangeAndSaveBlockChannel(sb.toString());

	}

	public void removeBlockChannel(String callLetter) {
		if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, removeBlockChannel, callLetter=" + callLetter);
        }
		
		EpgService eService = CommunicationManager.getInstance().getEpgService();
		
		
		// check that callLetter is existed in current list
		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);
		
		
		String storedData = pre.getCurrentValue();
        if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, removeBlockChannel, channel restriction = " + storedData);
        }
        
        String sisterCallLetter = "";
        
        if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
        	String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
        	
        	try {
				TvChannel channel = eService.getChannel(callLetter);
				
				if (channel != null && channel.getSisterChannel() != null) {
					sisterCallLetter = channel.getSisterChannel().getCallLetter();
				}
				
				if (Log.DEBUG_ON) {
		        	Log.printDebug("BlockChannelController, removeBlockChannel, sister CallLetter=" + sisterCallLetter);
		        }
			} catch (RemoteException e) {
				Log.print(e);
			} catch (NullPointerException e) {
				Log.print(e);
			}
        	
        	StringBuffer sb = new StringBuffer();
        	boolean isAdded = false;
            for (int i = 0; i < parsedData.length; i++) {
            	
            	if (parsedData[i].equals(callLetter)) {
            		continue;
            	} else if (parsedData[i].equals(sisterCallLetter)) {
            		continue;
            	}
            	
                if (isAdded) {
                	sb.append(PreferenceService.PREFERENCE_DELIMETER);
                }
                sb.append(parsedData[i]);
                isAdded = true;
            }

            rearrangeAndSaveBlockChannel(sb.toString());
        }
	}
	
	public void processSisterChannels() {
		if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, processSisterChannels");
        }
		
		EpgService eService = CommunicationManager.getInstance().getEpgService();
		
		Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);
		
		
		String storedData = pre.getCurrentValue();
        if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, processSisterChannels, channel restriction = " + storedData);
        }
        
        if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
        	String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
        	
        	Vector newCallLetters = new Vector();
        	for (int i = 0; i < parsedData.length; i++) {
        		try {
    				TvChannel channel = eService.getChannel(parsedData[i]);
    				
    				if (channel != null && channel.getSisterChannel() != null) {
    					String sisterCallLetter = channel.getSisterChannel().getCallLetter();
    					
    					if (Log.DEBUG_ON) {
        		        	Log.printDebug("BlockChannelController, processSisterChannels, sister CallLetter=" + sisterCallLetter);
        		        }
    					
    					if (!isExisted(parsedData, sisterCallLetter)) {
    						newCallLetters.add(sisterCallLetter);
    					}
    				}
    			} catch (RemoteException e) {
    				Log.print(e);
    			} catch (NullPointerException e) {
    				Log.print(e);
    			}
        	}
        	
        	StringBuffer sb = new StringBuffer();
            for (int i = 0; i < parsedData.length; i++) {
            	
            	if (i != 0) {
            		sb.append(PreferenceService.PREFERENCE_DELIMETER);
            	}
                sb.append(parsedData[i]);
            }
            
            for (int i = 0; i < newCallLetters.size(); i++) {
            	sb.append(PreferenceService.PREFERENCE_DELIMETER);
            	sb.append((String)newCallLetters.get(i));
            }
            
            newCallLetters.clear();
            
            rearrangeAndSaveBlockChannel(sb.toString());
        	
        }
	}
	
	private void rearrangeAndSaveBlockChannel(String data) {
		if (Log.DEBUG_ON) {
            Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: data=" + data);
        }
		String[] parsedData = TextUtil.tokenize(data, PreferenceService.PREFERENCE_DELIMETER);
		
		Vector newListVector = new Vector();
		
		EpgService eService = CommunicationManager.getInstance().getEpgService();
		
		try {
			for (int i = 0; i < parsedData.length; i++) {
				Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: parsedData[i]=[" + data + "]");
				if (newListVector.size() == 0) {
					newListVector.add(parsedData[i]);
				} else {
					TvChannel tChannel = eService.getChannel(parsedData[i]);
					
					Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: tChannel=" + tChannel);
					boolean isAdded = false;
					for (int j = 0; j < newListVector.size(); j++) {
						String callLetter = (String) newListVector.get(j);
						
						TvChannel channel = eService.getChannel(callLetter);
						Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: channel=" + channel);
						if (tChannel.getNumber() < channel.getNumber()) {
							newListVector.add(j, tChannel.getCallLetter());
							isAdded = true;
							break;
						}
					}
					
					if (!isAdded) {
						newListVector.add(tChannel.getCallLetter());
					}
				}
			}
		} catch (RemoteException e) {
			Log.print(e);
		}
		
		Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: newListVector.size=[" + newListVector.size() + "]");
		String[] newData = new String[newListVector.size()];
		
		newListVector.copyInto(newData);
		
		StringBuffer sb = new StringBuffer();
        for (int i = 0; i < newData.length; i++) {
        	
        	if (i != 0) {
        		sb.append(PreferenceService.PREFERENCE_DELIMETER);
        	}
        	Log.printDebug("BlockChannelController, rearrangeAndSaveBlockChannel: newData[i]=[" + newData[i] + "]");
            sb.append(newData[i]);
        }
		
        Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.CHANNEL_RESTRICTION);
		pre.setCurrentValue(sb.toString());
        DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
		
	}
	
	private boolean isExisted(String[] list, String callLetter) {
		if (list == null) return false;
		for (int i = 0; i < list.length; i++) {
            if (list[i].equals(callLetter)) {
            	if (Log.DEBUG_ON) {
                    Log.printDebug("BlockChannelController, isExisted, find in current restriction list");
                }
                return true;
            }
        }
		
		return false;
	}
}

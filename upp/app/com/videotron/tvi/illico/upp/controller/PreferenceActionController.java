/**
 * @(#)ExternalUpdatecontroller.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.media.Control;
import javax.media.Player;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;

import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.dvb.user.GeneralPreference;
import org.dvb.user.UnsupportedPreferenceException;
import org.dvb.user.UserPreferenceManager;
import org.havi.ui.HScreen;
import org.havi.ui.HScreenConfigTemplate;
import org.havi.ui.HScreenRectangle;
import org.havi.ui.HVideoConfigTemplate;
import org.havi.ui.HVideoConfiguration;
import org.havi.ui.HVideoDevice;
import org.ocap.hardware.*;
import org.ocap.hardware.device.*;
import org.ocap.media.ClosedCaptioningControl;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * this class handle a action to need after being a preference from self or
 * other application(EPG etc).
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class PreferenceActionController implements ResourceClient {

	/** The value array of 480 for TV Picture format. */
	private static final Object[][] info480 = {
			{ new Dimension(720, 480), new Dimension(32, 27), new HScreenRectangle(0.0f, 0.0f, 1.0f, 1.0f) },
			{ new Dimension(720, 480), new Dimension(4, 3), new HScreenRectangle(-0.0625f, 0.0f, 1.125f, 1.0f) },
			{ new Dimension(720, 480), new Dimension(32, 27), new HScreenRectangle(-0.0625f, -0.0625f, 1.125f, 1.125f) } };

	/** The value array of 720p for TV Picture format. */
	private static Object[][] info720p = {
			{ new Dimension(1280, 720), new Dimension(32, 27), new HScreenRectangle(0.0f, 0.0f, 1.0f, 1.0f) },
			{ new Dimension(1280, 720), new Dimension(4, 3), new HScreenRectangle(-0.16666667f, 0.0f, 1.3333333f, 1.0f) },
			{ new Dimension(1280, 720), new Dimension(32, 27),
					new HScreenRectangle(-0.16666667f, -0.16666667f, 1.3333333f, 1.3333333f) } };

	/** The value array of 1080 for TV Picture format. */
	private static Object[][] info1080 = {
			{ new Dimension(1920, 1080), new Dimension(32, 27), new HScreenRectangle(0.0f, 0.0f, 1.0f, 1.0f) },
			{ new Dimension(1920, 1080), new Dimension(4, 3),
					new HScreenRectangle(-0.16666667f, 0.0f, 1.3333333f, 1.0f) },
			{ new Dimension(1920, 1080), new Dimension(32, 27),
					new HScreenRectangle(-0.16666667f, -0.16666667f, 1.3333333f, 1.3333333f) } };
	
	/** The value array of 1080p for TV Picture format. */
	private static Object[][] info2160p = {
			{ new Dimension(3840, 2160), new Dimension(1, 1), new HScreenRectangle(0.0f, 0.0f, 1.0f, 1.0f) },
			{ new Dimension(3840, 2160), new Dimension(4, 3),
					new HScreenRectangle(-0.16666667f, 0.0f, 1.3333333f, 1.0f) },
			{ new Dimension(3840, 2160), new Dimension(1, 1),
					new HScreenRectangle(-0.16666667f, -0.16666667f, 1.3333333f, 1.3333333f) } };

	/** The instance of PreferenceActionController. */
	private static PreferenceActionController instance;

	/**
	 * A Hashtable to include a images for remote control Type A. This Hashtable
	 * will be used in {@link #updateFooterImages(String)}.
	 */
	private Hashtable typeATable;
	/**
	 * A Hashtable to include a images for remote control Type B. This Hashtable
	 * will be used in {@link #updateFooterImages(String)}.
	 */
	private Hashtable typeBTable;

	/** AudioOutputPort. */
    //private AudioOutputPort audioOutput = null;
    
	/**
	 * Instantiates a PrferenceActionController.
	 */
	private PreferenceActionController() {
		typeATable = new Hashtable();
		typeBTable = new Hashtable();
	}

	/**
	 * Gets the single instance of PreferenceActionController.
	 * 
	 * @return single instance of PreferenceActionController
	 */
	public static PreferenceActionController getInstance() {
		if (instance == null) {
			instance = new PreferenceActionController();
		}

		return instance;
	}

	/**
	 * Check a current configuration with saved configuration. Becuase when
	 * program change from SD to HD, Video is out on screen if zoom mode is
	 * Wide.<br>
	 * if configurations is different, zoom mode set a Normal.
	 */
	public void checkScreenConfiguration() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: checkScreenConfiguration()");
		}
		String zoomMode = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_ZOOM_MODE)).getCurrentValue();
		String resolution = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT))
				.getCurrentValue();

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: zoomMode = " + zoomMode);
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: resolution = " + resolution);
		}

		if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: zoomMode is Normal, so do nothing.");
			}
			return;
		}
		
		// check to set 1080p mode temporarily by VOD
		if (ProxyController.getInstance().is1080pMode()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: checkScreenConfiguration is set 1080p temporarily by VOD, so ignore a current resolution.");
			}

			resolution = Definitions.VIDEO_TV_PICTURE_FORMAT_1080P;
		}

		Object[] param = null;
		boolean isInterlacedDisplay = false;
		if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_UHD)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info2160p[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info2160p[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info2160p[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_720P)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info720p[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info720p[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info720p[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_1080I)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info1080[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info1080[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info1080[2];
			}
			isInterlacedDisplay = true;
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_1080P)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info1080[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info1080[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info1080[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_480I)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info480[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info480[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info480[2];
			}
			isInterlacedDisplay = true;
		} else {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info480[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info480[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info480[2];
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: found Resolution : " + param[0]);
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: found Aspect ratio : " + param[1]);
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: found ScreenArea : " + param[2]);
			Log.printDebug("PreferenceActionController: checkScreenConfiguration: isInterlacedDisplay : "
					+ isInterlacedDisplay);
		}

		HScreen screen = HScreen.getDefaultHScreen();
		HVideoDevice videoDevice = screen.getDefaultHVideoDevice();

		HVideoConfiguration currentConfig = videoDevice.getCurrentConfiguration();

		HVideoConfiguration savedConfig = null;

		if (param != null) {
			HVideoConfigTemplate hvct = new HVideoConfigTemplate();
			hvct.setPreference(HScreenConfigTemplate.PIXEL_RESOLUTION, param[0], HScreenConfigTemplate.REQUIRED);
			hvct.setPreference(HScreenConfigTemplate.PIXEL_ASPECT_RATIO, param[1], HScreenConfigTemplate.PREFERRED);
			hvct.setPreference(HScreenConfigTemplate.SCREEN_RECTANGLE, param[2], HScreenConfigTemplate.PREFERRED);
			hvct.setPreference(HScreenConfigTemplate.INTERLACED_DISPLAY,
					isInterlacedDisplay ? HScreenConfigTemplate.REQUIRED : HScreenConfigTemplate.REQUIRED_NOT);
			savedConfig = videoDevice.getBestConfiguration(hvct);
		}

		if (savedConfig != null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: savedConfig : resolution : "
						+ savedConfig.getPixelResolution());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: savedConfig : aspect ratio : "
						+ savedConfig.getPixelAspectRatio());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: savedConfig : screen area : "
						+ savedConfig.getScreenArea());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: savedConfig : interlaced : "
						+ savedConfig.getInterlaced());

				Log.printDebug("PreferenceActionController: checkScreenConfiguration: currentConfig : resolution : "
						+ currentConfig.getPixelResolution());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: currentConfig : aspect ratio : "
						+ currentConfig.getPixelAspectRatio());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: currentConfig : screen area : "
						+ currentConfig.getScreenArea());
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: currentConfig : interlaced : "
						+ currentConfig.getInterlaced());
			}

			if (!savedConfig.getPixelResolution().equals(currentConfig.getPixelResolution())
					|| !savedConfig.getPixelAspectRatio().equals(currentConfig.getPixelAspectRatio())
					|| !savedConfig.getScreenArea().equals(currentConfig.getScreenArea())
					|| !(savedConfig.getInterlaced() == currentConfig.getInterlaced())) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PreferenceActionController: checkScreenConfiguration: zoom mode forcely set a Normal.");
				}
				DataManager.getInstance().setPreference(PreferenceNames.VIDEO_ZOOM_MODE,
						Definitions.VIDEO_ZOOM_MODE_NORMAL);
			}

		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: checkScreenConfiguration: can't find a config.");
			}

		}

	}

	/**
	 * if needs a action according to preference name, do a something. ex)
	 * register a sleep time if preference name is
	 * {@link PreferenceNames#STB_SLEEP_TIME}
	 * 
	 * @param pName
	 *            a preference name of value to be updated.
	 * @param value
	 *            a value to be updated.
	 */
	public void doAction(final String pName, final String value) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: doAction: pName : " + pName + "\t value : " + value);
		}

		LoadingAnimationService loadingService = (LoadingAnimationService) DataCenter.getInstance().get(
				LoadingAnimationService.IXC_NAME);

		if (PreferenceController.getInstance().isBooted()) {
			if (loadingService != null) {
				try {
					loadingService.showLoadingAnimation(new Point(480, 270));
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
		}

		if (pName.equals(PreferenceNames.STB_SLEEP_TIME)) {

			STBTimerController.getInstance().updateSleepTimer(value);

		} else if (pName.equals(PreferenceNames.STB_WAKE_UP_TIME)) {

			STBTimerController.getInstance().updateWakeUpTimer(value);
		} else if (pName.equals(PreferenceNames.CC_DISPLAY) || pName.equals(PreferenceNames.CC_LANGUAGE)
				|| pName.equals(PreferenceNames.CC_ANALOG_SOURCE) || pName.equals(PreferenceNames.CC_DIGITAL_SOURCE)) {

			setCCPreference(pName, value);

		} else if (pName.equals(PreferenceNames.VIDEO_ZOOM_MODE)
				|| pName.equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)) {

			setTVPictureFormat(pName, value);

		} else if (pName.equals(PreferenceNames.SAP_LANGUAGE_AUDIO)
				|| pName.equals(PreferenceNames.DESCRIBED_AUDIO_DISPLAY)) {

			updateToGeneralPreference();
		} else if (pName.equals(PreferenceNames.REMOTE_CONTROL_TYPE)) {
			updateFooterImages(value);
			
		} else if (pName.equals(PreferenceNames.AUDIO_OUTPUT)) { //R7.3 audio output VDTRMASTER-5277
			setAudioOutput(value);
		}

		if (PreferenceController.getInstance().isBooted()) {
			if (loadingService != null) {
				try {
					loadingService.hideLoadingAnimation();
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
		}
	}

	/**
	 * Request a next zoom mode step by step.
	 */
	public void requestNextZoomMode() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: requestNextZoomMode");
		}

		PreferenceActionController.getInstance().checkScreenConfiguration();

		Hashtable preTable = DataManager.getInstance().getPreferences("program.guide");

		Preference pre = (Preference) preTable.get(PreferenceNames.VIDEO_ZOOM_MODE);

		String[] values = pre.getSelectableValues();
		String currentValue = pre.getCurrentValue();

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: requestNextZoomMode: current value = " + currentValue);
		}

		int idx = 0;
		for (int i = 0; i < values.length; i++) {
			if (values[i].equals(currentValue)) {
				idx = i;
				break;
			}
		}

		if (++idx == values.length) {
			idx = 0;
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: requestNextZoomMode: next value = " + values[idx]);
		}
		DataManager.getInstance().setPreference(pre.getID(), values[idx]);
		doAction(pre.getID(), values[idx]);
	}

	/**
	 * Set a ClosedCaption environment according to user' selection.
	 * 
	 * @param pName
	 *            The preference name of related Closed Caption
	 * @param value
	 *            The value of preference.
	 */
	private void setCCPreference(final String pName, final String value) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: setCCPreference");
		}
		ClosedCaptioningControl ccc = getClosedCaptioningControl();

		if (ccc != null) {
			if (pName.equals(PreferenceNames.CC_DISPLAY)) {
				if (value.equals(Definitions.OPTION_VALUE_ON)) {
					ccc.setClosedCaptioningState(ClosedCaptioningControl.CC_TURN_ON);
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceActionController: ClosedCaptionState update 'turn on");
					}

				} else if (value.equals(Definitions.OPTION_VALUE_ON_MUTE)) {
					ccc.setClosedCaptioningState(ClosedCaptioningControl.CC_TURN_ON_MUTE);
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceActionController: ClosedCaptionState update 'turn on Mute");
					}
				} else {
					ccc.setClosedCaptioningState(ClosedCaptioningControl.CC_TURN_OFF);
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceActionController: ClosedCaptionState update 'turn off");
					}
				}
			} else if (pName.equals(PreferenceNames.CC_ANALOG_SOURCE)
					|| pName.equals(PreferenceNames.CC_DIGITAL_SOURCE)) {
				String analog = DataManager.getInstance().getPreference(PreferenceNames.CC_ANALOG_SOURCE)
						.getCurrentValue();
				String digital = DataManager.getInstance().getPreference(PreferenceNames.CC_DIGITAL_SOURCE)
						.getCurrentValue();

				int analogServiceNumber = -1;
				if (analog.equals(Definitions.CC_SOURCE_ANALOG_CC1)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_CC1;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_CC2)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_CC2;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_CC3)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_CC3;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_CC4)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_CC4;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_T1)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_T1;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_T2)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_T2;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_T3)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_T3;
				} else if (analog.equals(Definitions.CC_SOURCE_ANALOG_T4)) {
					analogServiceNumber = ClosedCaptioningControl.CC_ANALOG_SERVICE_T4;
				}

				int digitalServiceNumber = -1;
				if (digital.equals(Definitions.CC_SOURCE_DIGITAL_1)) {
					digitalServiceNumber = 1;
				} else if (digital.equals(Definitions.CC_SOURCE_DIGITAL_2)) {
					digitalServiceNumber = 2;
				} else if (digital.equals(Definitions.CC_SOURCE_DIGITAL_3)) {
					digitalServiceNumber = 3;
				} else if (digital.equals(Definitions.CC_SOURCE_DIGITAL_4)) {
					digitalServiceNumber = 4;
				} else if (digital.equals(Definitions.CC_SOURCE_DIGITAL_5)) {
					digitalServiceNumber = 5;
				} else if (digital.equals(Definitions.CC_SOURCE_DIGITAL_6)) {
					digitalServiceNumber = 6;
				}

				ccc.setClosedCaptioningServiceNumber(analogServiceNumber, digitalServiceNumber);
				if (Log.DEBUG_ON) {
					Log.printDebug("PreferenceActionController: ClosedCaptionServiceNumber set " + analogServiceNumber
							+ " for analog service");
					Log.printDebug("PreferenceActionController: ClosedCaptionServiceNumber set " + digitalServiceNumber
							+ " for digital service");
				}
			}
		} else if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: ClosedCaptioningControl is null.");
		}

	}

	// VDTRMASTER-5804
	public void resetClosedCaptioningControl() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: setCCPreference");
		}
		ClosedCaptioningControl ccc = getClosedCaptioningControl();

		if (ccc != null) {
			ccc.setClosedCaptioningServiceNumber(ClosedCaptioningControl.CC_ANALOG_SERVICE_CC1, 1);
		} else if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: ClosedCaptioningControl is null.");
		}
	}

	/**
	 * Get a ClosedCaptioningControl of current ServiceContext.
	 * 
	 * @return a ClosedCaptioningControl
	 */
	private ClosedCaptioningControl getClosedCaptioningControl() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: getClosedCaptioningControl()");
		}

		ServiceContext sc = Environment.getServiceContext(0);

		if (sc != null) {
			ServiceContentHandler[] handlers = sc.getServiceContentHandlers();
			if ((handlers != null) && (handlers.length > 0)) {

				Player player = (Player) handlers[0];

				if (player == null) {
					if (Log.DEBUG_ON) {
						Log.printDebug("PreferenceActionController: player is null.");
					}
					return null;
				}
				if (Log.DEBUG_ON) {
					Control[] control = player.getControls();
					for (int i = 0; i < control.length; i++) {
						Log.printDebug("PreferenceActionController: control[" + i + "] = " + control[i]);
					}
				}

				ClosedCaptioningControl ccc = (ClosedCaptioningControl) player
						.getControl("org.ocap.media.ClosedCaptioningControl");

				return ccc;
			}
		} else if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: ServiceContext is null.");
		}
		return null;
	}

	/**
	 * Sync a language between UPP Preferences and DVB Preference acrroding to
	 * SAP and Described Audio.
	 */
	private void updateToGeneralPreference() {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: updateToGeneralPreference");
		}
		final String UP_NAME_USER_LANGUAGE = "User Language";

		UserPreferenceManager upManager = UserPreferenceManager.getInstance();
		GeneralPreference pref = new GeneralPreference(UP_NAME_USER_LANGUAGE);
		upManager.read(pref);

		String[] savedValues = pref.getFavourites();
		String sapLanguage = DataManager.getInstance().getPreference(PreferenceNames.SAP_LANGUAGE_AUDIO)
				.getCurrentValue();
		String adDisplay = DataManager.getInstance().getPreference(PreferenceNames.DESCRIBED_AUDIO_DISPLAY)
				.getCurrentValue();
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: updateToGeneralPreference: SAP language : " + sapLanguage);
			Log.printDebug("PreferenceActionController: updateToGeneralPreference: AD display : " + adDisplay);

			for (int i = 0; savedValues != null && i < savedValues.length; i++) {
				Log.printDebug("PreferenceActionController: updateToGeneralPreference: savedValues[" + i + "] : "
						+ savedValues[i]);
			}
		}

		String[] newValues = null;
		if (adDisplay.equals(Definitions.OPTION_VALUE_OFF)) {
			if (sapLanguage.equals(Definitions.LANGUAGE_ENGLISH)) {
				newValues = new String[] { "eng", "enm", "fre", "frm", "spa", "spm" };
			} else if (sapLanguage.equals(Definitions.LANGUAGE_FRENCH)) {
				newValues = new String[] { "fre", "frm", "eng", "enm", "spa", "spm" };
			} else if (sapLanguage.equals(Definitions.LANGUAGE_SPANISH)) {
				newValues = new String[] { "spa", "spm", "eng", "enm", "fre", "frm" };
			}
		} else {
			if (sapLanguage.equals(Definitions.LANGUAGE_ENGLISH)) {
				newValues = new String[] { "enm", "eng", "frm", "fre", "spm", "spa" };
			} else if (sapLanguage.equals(Definitions.LANGUAGE_FRENCH)) {
				newValues = new String[] { "frm", "fre", "enm", "eng", "spm", "spa" };
			} else if (sapLanguage.equals(Definitions.LANGUAGE_SPANISH)) {
				newValues = new String[] { "spm", "spa", "enm", "eng", "frm", "fre" };
			}
		}

		if (Log.DEBUG_ON) {
			for (int i = 0; newValues != null && i < newValues.length; i++) {
				Log.printDebug("PreferenceActionController: updateToGeneralPreference: newValues[" + i + "] : "
						+ newValues[i]);
			}
		}

		pref.removeAll();

		if (newValues != null) {
			pref.add(newValues);
			try {
				upManager.write(pref);
			} catch (UnsupportedPreferenceException e) {
				Log.print(e);
			} catch (IOException e) {
				Log.print(e);
			}
			if (Log.INFO_ON) {
				Log.printInfo("PreferenceActionController: a language of GeneralPreference update a newValues");
			}
		} else {
			if (Log.INFO_ON) {
				Log.printInfo("PreferenceActionController: a language of GeneralPreference update fail.");
			}
		}
	}

	/**
	 * Apply a TV Picture format according to selected value.
	 * 
	 * @param name
	 *            a preference name.
	 * @param value
	 *            a prference's value.
	 */
	private void setTVPictureFormat(String name, String value) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: setTVPictureFormat: name = " + name + "\t value = " + value);
		}
		HScreen screen = HScreen.getDefaultHScreen();
		HVideoDevice videoDevice = screen.getDefaultHVideoDevice();
		HVideoConfiguration defaultConfig = videoDevice.getDefaultConfiguration();
		HVideoConfiguration newConfig = null;

		if (Log.DEBUG_ON) {
			HVideoConfiguration[] configs = videoDevice.getConfigurations();

			for (int i = 0; i < configs.length; i++) {

				Log.printDebug("PreferenceActionController: setTVPictureFormat: configs ScreenArea : "
						+ configs[i].getScreenArea());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: configs Resolution : "
						+ configs[i].getPixelResolution());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: configs AspectRatio : "
						+ configs[i].getPixelAspectRatio());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: configs Interlaced : "
						+ configs[i].getInterlaced());
			}
		}
		
		if (Environment.EMULATOR) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: setTVPictureFormat: do not anything in Emulator");
			}
		}
		
		String resolution, zoomMode;

		if (name.equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)) {
			resolution = value;
			zoomMode = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_ZOOM_MODE)).getCurrentValue();
		} else {
			resolution = (DataManager.getInstance().getPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT))
					.getCurrentValue();
			zoomMode = value;
		}

		// check to set 1080p mode temporarily by VOD
		if (ProxyController.getInstance().is1080pMode()) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: is set 1080p temporarily by VOD, so ignore a current resolution.");
			}

			resolution = Definitions.VIDEO_TV_PICTURE_FORMAT_1080P;
		}

		Object[] param = null;
		boolean isInterlacedDisplay = false;
		if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_UHD)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info2160p[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info2160p[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info2160p[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_720P)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info720p[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info720p[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info720p[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_1080I)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info1080[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info1080[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info1080[2];
			}
			isInterlacedDisplay = true;
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_1080P)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info1080[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info1080[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info1080[2];
			}
		} else if (resolution.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_480I)) {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info480[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info480[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info480[2];
			}
			isInterlacedDisplay = true;
		} else {
			if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_NORMAL)) {
				param = info480[0];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_WIDE)) {
				param = info480[1];
			} else if (zoomMode.equals(Definitions.VIDEO_ZOOM_MODE_ZOOM)) {
				param = info480[2];
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("selected value of resolution : " + resolution);
			Log.printDebug("selected value of zoomMode : " + zoomMode);

			Log.printDebug("found Resolution : " + param[0]);
			Log.printDebug("found Aspect ratio : " + param[1]);
			Log.printDebug("found ScreenArea : " + param[2]);
			Log.printDebug("isInterlacedDisplay : " + isInterlacedDisplay);
		}

		if (param != null) {
			HVideoConfigTemplate hvct = new HVideoConfigTemplate();
			hvct.setPreference(HScreenConfigTemplate.PIXEL_RESOLUTION, param[0], HScreenConfigTemplate.REQUIRED);
			hvct.setPreference(HScreenConfigTemplate.PIXEL_ASPECT_RATIO, param[1], HScreenConfigTemplate.PREFERRED);
			hvct.setPreference(HScreenConfigTemplate.SCREEN_RECTANGLE, param[2], HScreenConfigTemplate.PREFERRED);
			hvct.setPreference(HScreenConfigTemplate.INTERLACED_DISPLAY,
					isInterlacedDisplay ? HScreenConfigTemplate.REQUIRED : HScreenConfigTemplate.REQUIRED_NOT);
			newConfig = videoDevice.getBestConfiguration(hvct);
		}

		if (newConfig != null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: setTVPictureFormat: new config : resolution : "
						+ newConfig.getPixelResolution());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: new config : aspect ratio : "
						+ newConfig.getPixelAspectRatio());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: new config : screen area : "
						+ newConfig.getScreenArea());
				Log.printDebug("PreferenceActionController: setTVPictureFormat: new config : interlaced : "
						+ newConfig.getInterlaced());
			}
			try {
				videoDevice.reserveDevice(this);
				boolean result = videoDevice.setVideoConfiguration(newConfig);
				videoDevice.releaseDevice();

			} catch (Exception e) {
				Log.printError(e);
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceActionController: setTVPictureFormat: new config is null");
			}

			try {
				videoDevice.reserveDevice(this);
				boolean result = videoDevice.setVideoConfiguration(defaultConfig);
				videoDevice.releaseDevice();

			} catch (Exception e) {
				Log.printError(e);
			}
		}
	}

	/**
	 * According to Remote Control Type, set a images for Footer into
	 * SharedMemory.
	 * 
	 * @param value
	 *            a type of current remote control.
	 */
	private void updateFooterImages(String value) {
		if (Log.INFO_ON) {
			Log.printInfo("PreferenceActionController: updateFooterImages: value = " + value);
		}

		DataCenter dataCenter = DataCenter.getInstance();
		final String[] footerNames = { PreferenceService.BTN_A, PreferenceService.BTN_B, PreferenceService.BTN_C,
				PreferenceService.BTN_D, PreferenceService.BTN_B_BIG, PreferenceService.BTN_ARROW,
				PreferenceService.BTN_BACK, PreferenceService.BTN_EXIT, PreferenceService.BTN_GUIDE,
				PreferenceService.BTN_INFO, PreferenceService.BTN_LIST, PreferenceService.BTN_MENU,
				PreferenceService.BTN_OK, PreferenceService.BTN_PAGE, PreferenceService.BTN_PRO,
				PreferenceService.BTN_SEARCH, PreferenceService.BTN_SKIP, PreferenceService.BTN_WIDGET,
				PreferenceService.BTN_POUND};

		String prefix = "footer/a_";
		String subfix = ".png";
		Hashtable imgTable = typeATable;

		if (value.equals(Definitions.REMOTE_CONTROL_TYPE_B)) {
			prefix = "footer/b_";
			imgTable = typeBTable;
		}

		if (imgTable.size() == 0) {
			StringBuffer sf = new StringBuffer();
			for (int i = 0; i < footerNames.length; i++) {
				sf.setLength(0);
				sf.append(prefix);
				sf.append(footerNames[i]);
				sf.append(subfix);
				Image img = dataCenter.getImage(sf.toString());
				if (Log.DEBUG_ON) {
					Log.printDebug("PreferenceActionController: updateFooterImages: key = " + sf.toString()
							+ "\t img = " + img);
				}
				imgTable.put(footerNames[i], img);
			}
		} else if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceActionController: updateFooterImages: Hashtable alread loaded.");
		}
		FrameworkMain.getInstance().getImagePool().waitForAll();
		SharedMemory.getInstance().remove(PreferenceService.FOOTER_IMG);
		SharedMemory.getInstance().put(PreferenceService.FOOTER_IMG, imgTable);
	}
	
	/**
	 * R7.4 sykim audio output VDTRMASTER-5277
	 * @param value
	 */
	private void setAudioOutput(String value) {
		if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceActionController: setAudioOutput = " + value);
        }
		HostSettings hostSettings = (HostSettings) Host.getInstance();
        Enumeration en = hostSettings.getAudioOutputs();
        AudioOutputPort audioOutput;
        
        while (en.hasMoreElements()) {
        	audioOutput = (AudioOutputPort) en.nextElement();
        
	        if (Log.DEBUG_ON) {
	        	
	        	Enumeration	videoEn = audioOutput.getConnectedVideoOutputPorts();
	        	
	        	Log.printDebug("PreferenceActionController: AudioOutputPort = " + audioOutput);
	            Log.printDebug("PreferenceActionController: AudioOutputPort.getEncoding() " + audioOutput.getEncoding());
	            
	        	while(videoEn.hasMoreElements()) {
	        		Log.printDebug("PreferenceActionController: VideoOutputPort of AudioOutputPort = " 
	        				+ ((VideoOutputPort)videoEn.nextElement()).getType());
	        	}	        	
	            
	        }
	        
	        try {
	        	if (value.equals(Definitions.AUDIO_OUTPUT_PCM)) {
		        	if (Log.DEBUG_ON) {
		                Log.printDebug("PreferenceActionController: setAudioOutput = AudioOutputPort.ENCODING_PCM");
		            }
		        	audioOutput.setEncoding(AudioOutputPort.ENCODING_PCM);	
		        } else if (value.equals(Definitions.AUDIO_OUTPUT_DOLBY)) {
		        	if (Log.DEBUG_ON) {
		                Log.printDebug("PreferenceActionController: setAudioOutput = AudioOutputPort.ENCODING_AC3");
		            }
		        	audioOutput.setEncoding(AudioOutputPort.ENCODING_AC3);
		        } else if (value.equals(Definitions.AUDIO_OUTPUT_DEFAULT)) {
		        	if (Log.DEBUG_ON) {
		                Log.printDebug("PreferenceActionController: setAudioOutput = AudioOutputPort.ENCODING_DISPLAY");
		            }
		        	audioOutput.setEncoding(AudioOutputPort.ENCODING_DISPLAY);
		        }
		        
		        if (Log.DEBUG_ON) {
		            Log.printDebug("PreferenceActionController: getEoncoding = " + audioOutput.getEncoding());
		        }   
		        
	        } catch(IllegalArgumentException ie) {
	        	Log.printError(ie);
	        } catch (FeatureNotSupportedException fe) {
	        	Log.printError(fe);
	        }
        }
        
	}

	public void notifyRelease(ResourceProxy proxy) {
	}

	public void release(ResourceProxy proxy) {
	}

	public boolean requestRelease(ResourceProxy proxy, Object obj) {
		return false;
	}
}

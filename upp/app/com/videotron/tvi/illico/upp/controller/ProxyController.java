/**
 * @(#)ProxyController.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.controller;

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.ixc.upp.BlockTime;
import com.videotron.tvi.illico.ixc.upp.BlockTimeUpdateListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.PreferenceServiceImpl;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;

/**
 * The ProxyController controls the Proxy module and manages a list of PreferenceListener. The ProxyController bind
 * PreferenceService to provide a interface that refer preferences. if one preference update, UPP will send notification
 * to the related applications via ProxyController
 * @author Woojung Kim
 * @version 1.1
 */
public class ProxyController {

    /** The instance of ProxyController. */
    private static ProxyController instance = new ProxyController();

    /** The instance of PrefereceService. */
    private PreferenceService pService;

    /** The xlet context. */
    private XletContext xletContext;

    /** The proxy listener. */
    private Hashtable proxyListener;

    private BlockTimeUpdateListener blockTimeListener;

    /**
     * Instantiates a new proxy controller.
     */
    private ProxyController() {
    }

    /**
     * Gets the single instance of ProxyController.
     * @return single instance of ProxyController
     */
    public static ProxyController getInstance() {
        return instance;
    }

    /**
     * Inits the ProxyController.
     * @param xc the XletContext of Application.
     */
    public void init(XletContext xc) {
        xletContext = xc;
        proxyListener = new Hashtable();
        initPreferenceProxy();
    }

    /**
     * when ProxyController start, it will execute the functions for initializing ProxyController.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: start");
        }
    }

    /**
     * when ProxyController stop, it will be execute the functions and clear up a resources if needs.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: stop");
        }
    }

    /**
     * when ProxyController dispose, it will be called. it will clear up a resources.
     */
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: dispose");
        }

        unbind();
        proxyListener.clear();
        proxyListener = null;
    }

    /**
     * Inits the preference proxy. create PreferenceServiceImpl and bind it.
     */
    public void initPreferenceProxy() {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: initPreferenceProxy");
        }
        pService = new PreferenceServiceImpl();
        try {
            IxcRegistry.bind(xletContext, PreferenceService.IXC_NAME, pService);
        } catch (AlreadyBoundException e) {
            e.printStackTrace();
        }

    }

    /**
     * Unbind PreferenceService.
     */
    public void unbind() {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: unbind");
        }
        try {
            IxcRegistry.unbind(xletContext, PreferenceService.IXC_NAME);
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds the proxy listener.
     * @param appId the application id is a owner of listener.
     * @param listener the listener
     */
    public void addProxyListener(String appId, PreferenceListener listener) {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: addProxyListener");
        }
        proxyListener.remove(appId);
        proxyListener.put(appId, listener);
    }

    /**
     * Removes the proxy listener.
     * @param appId the application name to have a listener.
     * @param listener the listener
     */
    public void removeProxyListener(String appId, PreferenceListener listener) {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: removeProxyListener");
        }
        Object obj = proxyListener.get(appId);

        if (obj != null) {
	        if (obj.equals(listener)) {
	            proxyListener.remove(appId);
	            if (Log.DEBUG_ON) {
	                Log.printDebug("ProxyController: removeProxyListener: remove PreferenceListener.");
	            }
	        } else {
	            if (Log.DEBUG_ON) {
	                Log.printDebug("ProxyController: removeProxyListener: skip to remove a PreferenceListener"
	                        + " because listener is not equals.");
	            }
	        }
        } else {
        	if (Log.DEBUG_ON) {
                Log.printDebug("ProxyController: removeProxyListener: can't find a PreferenceListener");
            }
        }
    }

    /**
     * Notify updated preference.
     * @param preferenceName the preference name
     * @param value the value
     */
    public void notifyUpdatedPreference(final String preferenceName, final String value) {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: notifyUpdatedPreference");
        }
        new Thread(new Runnable() {

            public void run() {
                Preference pre = DataManager.getInstance().getPreference(preferenceName);

                // for Access Filters.
                if (pre == null) {
                    pre = (Preference) (UserManager.getInstance().getCurrentUser().getAccessFilters()
                            .get(preferenceName));
                }

                if (Log.DEBUG_ON) {
                    Log.printDebug("ProxyController: notifyUpdatedPreference: preferenceName : "
                            + preferenceName);
                    Log.printDebug("ProxyController: notifyUpdatedPreference: value : " + value);
                }
                Enumeration keys = proxyListener.keys();

                while (keys.hasMoreElements()) {
                    // notify a updated data to a only related application
                    String appId = (String) keys.nextElement();
                    if (pre.isRelatedApp(appId)) {
                        PreferenceListener listener = (PreferenceListener) proxyListener.get(appId);
                        try {
                            listener.receiveUpdatedPreference(preferenceName, value);
                            if (Log.DEBUG_ON) {
                                Log.printDebug("ProxyController: notifyUpdatedPreference: update appId : "
                                        + appId);
                            }
                        } catch (RemoteException e) {
                            if (Log.ERROR_ON) {
                                Log.printError("notifyUpdatedPreference: listener.receiveUpdatedPreference failed.");
                                Log.printError(e);
                            }
                        }
                    }
                }
            }

        }).start();

        VbmLogController.getInstance().updatedPreferenceData(preferenceName, value);
    }

    /**
     * Notify a updated data for block times.
     * @param times a data of block time.
     */
    public void notifyUpdateBlockTimeData(BlockTime[] times) {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: notifyUpdateBlockTimeData");
        }
        try {
            if (blockTimeListener != null) {
                blockTimeListener.updateBlockTimeData(times);
            } else {
                if (Log.INFO_ON) {
                    Log.printInfo("ProxyController: notifyUpdateBlockTimeData: blockTimeListener is null");
                }
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Set a listener to receive a event of updated for Block time.
     * @param l a listener.
     */
    public void setBlockTimeUpdateListener(BlockTimeUpdateListener l) {
        if (Log.INFO_ON) {
            Log.printInfo("ProxyController: setBlockTimeUpdateListener");
        }
        blockTimeListener = l;
    }
    
    public boolean is1080pMode() {
    	return ((PreferenceServiceImpl)pService).is1080pMode();
    }
}

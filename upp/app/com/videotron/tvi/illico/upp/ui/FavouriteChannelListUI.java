/**
 * @(#)FavouriteChannelListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.upp.effect.ListEffect;
import com.videotron.tvi.illico.upp.gui.FavouriteChannelListRenderer;

/**
 * This class handle view of favourite channel as list style.
 * @author Woojung Kim
 * @version 1.1
 */
public class FavouriteChannelListUI extends BaseUI {

    /** Parent UI. */
    FavouriteChannelUI parentUI;
    /** Effect for animation. */
    ListEffect listEffect;

    /**
     * Set a renderer and create a effect.
     * @param parent parent UI.
     */
    public FavouriteChannelListUI(FavouriteChannelUI parent) {
        parentUI = parent;
        setRenderer(new FavouriteChannelListRenderer());
        listEffect = new ListEffect(this);
    }

    /**
     * Start UI.
     * @param back true if come from lower depth, false otherwise.
     */
    public void start(boolean back) {
        prepare();
        super.start(back);
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        super.stop();
    }

    /**
     * Get a parent UI.
     * @return parent UI.
     */
    public FavouriteChannelUI getParentUI() {
        return parentUI;
    }

    /**
     * Start a animation.
     * @param mode one of {@link ListEffect#MODE_FROM_LEFT} or {@link ListEffect#MODE_FROM_RIGHT}
     * @param lIdx index of left list.
     * @param rIdx index of right list.
     * @param down true if need to scroll down, false otherwise.
     */
    public void startEffect(int mode, int lIdx, int rIdx, boolean down) {
        listEffect.start(mode, lIdx, rIdx, down);
    }

    /**
     * Handle a action after effect is started.
     * @param effect list effect.
     */
    public void animationStarted(Effect effect) {
        super.animationStarted(effect);
        setVisible(false);
    }

    /**
     * Handle a action after effect is ended.
     * @param effect list effect.
     */
    public void animationEnded(Effect effect) {
        super.animationEnded(effect);
        setVisible(true);
    }
}

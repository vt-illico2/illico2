/**
 * @(#)ChannelRestrictionUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.effect.ListEffect;
import com.videotron.tvi.illico.upp.gui.ChannelRestrictionRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This Class handle a mode and data for Channel Restriction.
 * @author Woojung Kim
 * @version 1.1
 */
public class ChannelRestrictionUI extends BaseUI {

    /** Constant to indicate that left list has a focus. */
    public final int STATE_LEFT = 0;
    /** Constant to indicate that add button has a focus. */
    public final int STATE_ADD = 1;
    /** Constant to indicate that remove button has a focus. */
    public final int STATE_REMOVE = 2;
    /** Constant to indicate that right list has a focus. */
    public final int STATE_RIGHT = 3;
    /** Constant to indicate that User is editing in right list. */
    public final int STATE_ORDER = 4;

    /** Renderer. */
    private ChannelRestrictionRenderer renderer = new ChannelRestrictionRenderer();
    /** UI to display a data as list. */
    private ChannelRestrictionListUI listUI;

    /** A channel data from EPG via SharedMemory. */
    private Object[][] channelData;
    /** A Vector to have a data for right list. */
    private Vector rightListVector = new Vector();
    /** A Vector to have a data for left list. */
    private Vector leftListVector = new Vector();
    /** A vector to have a sister channel list */
    private Hashtable sisterTable = new Hashtable();
    /** a start index for left list. */
    private int leftListStartIdx;
    /** a start index for right list. */
    private int rightListStartIdx;
    /** a index of left focus. */
    private int leftFocus;
    /** a index of right focus. */
    private int rightFocus;
    /** a default logo for channel. */
    private Image defaultChannelLogo;
    
    private boolean isLoading;
    private Thread channelThread;
    
    /**
     * Set a renderer.
     */
    public ChannelRestrictionUI() {
        renderer.setHasExpalin(false);
        setRenderer(renderer);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        super.prepare();
    }

    /**
     * Start a UI.
     * @param back true if come from higher depth.
     */
    public void start(boolean back) {
        if (!back) {
            prepare();
            addButton(BTN_BACK);
            addButton(BTN_PAGE);
        }
        
        if (listUI == null) {
            listUI = new ChannelRestrictionListUI(this);
        }
        
        super.start();
        
        channelThread = new Thread(new Runnable() {
        	public void run() {
        		try {
	        		isLoading = true;
	        		PreferenceController.getInstance().showLoading();
	                
	                defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
	                channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);
	                
	                for (int i = 0; i < channelData.length; i++) {
	
	                    Integer chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
	
						if (chType.intValue() == TvChannel.TYPE_TECH)
							continue;
						
	                    String callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
						String sisterCalletterStr = (String) channelData[i][BaseUI.INDEX_CHANNEL_SISTER_NAME];
	                    if (chType.intValue() != TvChannel.TYPE_VIRTUAL) {
	                    	
                        	// case of not existed in sisterTable
                        	if (sisterCalletterStr == null || !sisterTable.containsKey(sisterCalletterStr)) {
                                leftListVector.add(channelData[i]);
                        	}

                        	// if sister channel exist, put in sisterTable
                        	if (sisterCalletterStr != null && !sisterTable.containsKey(sisterCalletterStr)) {
                        		for (int sisterIdx = 0; sisterIdx < channelData.length; sisterIdx++) {
    								if (sisterCalletterStr.equals(channelData[sisterIdx][BaseUI.INDEX_CHANNEL_NAME])) {
    									sisterTable.put(callLetterStr, channelData[sisterIdx]);
    									break;
    								}
    							}
                        	}
	                    }
	                }
	
	                if (Log.DEBUG_ON) {
	                    if (channelData != null) {
	                        Log.printDebug("ChannelRestrictionUI: prepare(): channelData : " + channelData.length);
	                        // for (int i = 0; i < channelData.length; i++) {
	                        // Log.printDebug("ID :" + channelData[i][INDEX_CHANNLE_NUMBER]);
	                        // Log.printDebug("ID :" + channelData[i][INDEX_CHANNEL_NAME]);
	                        // }
	                    } else {
	                        Log.printDebug("ChannelRestrictionUI: prepare(): channelData is null");
	                    }
	                }
	                
	                Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
	                        .get(RightFilter.CHANNEL_RESTRICTION);
	
	                if (pre != null) {
	                    String storedData = pre.getCurrentValue();
	                    if (Log.DEBUG_ON) {
	                        Log.printDebug("storedData = " + storedData);
	                    }
	                    if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
	                        String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
	
//	                        for (int i = 0; i < parsedData.length; i++) {
//	                            for (int j = 0; j < channelData.length; j++) {
//	                            	if (parsedData[i].equals(channelData[j][INDEX_CHANNEL_NAME])) {
//        	                        	String calleter = (String) channelData[j][INDEX_CHANNEL_NAME];
//        	                        	
//        	                        	if (channelData[j][INDEX_CHANNEL_SISTER_NAME] == null 
//        	                        			|| sisterTable.containsKey(calleter)) {
//        		                            rightListVector.add(channelData[j]);
//        		                            leftListVector.remove(channelData[j]);
//        	                        	}
//										break;
//									}
//	                            }
//	                        }
	                        
	                        for (int i = 0; i < channelData.length; i++) {
								for (int j = 0; j < parsedData.length; j++) {
									if (parsedData[j].equals(channelData[i][INDEX_CHANNEL_NAME])) {
        	                        	String calleter = (String) channelData[i][INDEX_CHANNEL_NAME];
        	                        	
        	                        	if (channelData[i][INDEX_CHANNEL_SISTER_NAME] == null 
        	                        			|| sisterTable.containsKey(calleter)) {
        		                            rightListVector.add(channelData[i]);
        		                            leftListVector.remove(channelData[i]);
        	                        	}
										break;
									}
								}
							}
	                    }
	                }
	                
	                if (leftListVector.size() == 0) {
	                    state = STATE_RIGHT;
	                } else {
	                    state = STATE_LEFT;
	                }
	                if (rightListVector.size() > 0) {
	                    addButton(BTN_CLEAR_CHANNEL_RESTRICTIONS);
	                }
	                leftListStartIdx = 0;
	                rightListStartIdx = 0;
	                leftFocus = 0;
	                rightFocus = 0;
	                
	                listUI.start(true);
	                add(listUI);
	                isLoading = false;
	                repaint();
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
                PreferenceController.getInstance().hideLoading();
        	}
        });
        
        channelThread.start();
        
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        remove(listUI);
        storeChannelData();
        leftListVector.clear();
        rightListVector.clear();
        clearButtons();
        clearPopup();
        super.stop();
        
        PreferenceController.getInstance().hideLoading();
		if (channelThread != null) {
			channelThread.interrupt();
		}
		channelThread = null;
		sisterTable.clear();
    }

    /**
     * Handle a key code.
     * @param code key code.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case KeyEvent.VK_PAGE_UP:
            clickButtons(BTN_PAGE);
            keyPageUp();
            return true;
        case KeyEvent.VK_PAGE_DOWN:
            clickButtons(BTN_PAGE);
            keyPageDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyOK();
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_PARENTAL_CONTROLS, true);
            return true;
        case KeyCodes.COLOR_B:
            clickButtons(BTN_CLEAR_CHANNEL_RESTRICTIONS);
            if (rightListVector.size() > 0) {
                setPopup(notiUI);
                String explain = BaseUI.getPopupText(KeyNames.EXPLAIN_CLEAR_RESTRICTED_CHANNELS);
                notiUI.show(this, BaseUI.getPopupText(KeyNames.TITLE_CONFIRM_DELETION),
                        TextUtil.split(explain, renderer.fm18, 310), PopBaseRenderer.ICON_TYPE_NONE, true);
                repaint();
            }
            return true;
        default:
            return false;
        }
    }

    /**
     * Handle a left key.
     */
    private void keyLeft() {
        if ((state == STATE_ADD || state == STATE_REMOVE) && leftListVector.size() > 0) {
            state = STATE_LEFT;
        } else if (state == STATE_RIGHT) {
            state = STATE_REMOVE;
        }
        repaint();
    }

    /**
     * Handle a right key.
     */
    private void keyRight() {
        if (state == STATE_LEFT) {
            state = STATE_ADD;
        } else if ((state == STATE_ADD || state == STATE_REMOVE) && rightListVector.size() > 0) {
            state = STATE_RIGHT;
        }
        repaint();
    }

    /**
     * Handle a up key.
     */
    private void keyUp() {
        if (state == STATE_LEFT) {
            if (leftListStartIdx + leftFocus > 0) {
                if (leftFocus > 4) {
                    leftFocus--;
                } else if (leftListStartIdx > 0) {
                    if (leftFocus < 5) {
                        leftListStartIdx--;
                    } else {
                        leftFocus--;
                    }
                } else {
                    leftFocus--;
                }
            }
        } else if (state == STATE_RIGHT) {
            if (rightListStartIdx + rightFocus > 0) {
                if (rightFocus > 4) {
                    rightFocus--;
                } else if (rightListStartIdx > 0) {
                    if (rightFocus < 5) {
                        rightListStartIdx--;
                    } else {
                        rightFocus--;
                    }
                } else {
                    rightFocus--;
                }
            }
        }
        repaint();
    }

    /**
     * Handle a down key.
     */
    private void keyDown() {
        if (state == STATE_LEFT) {
            if (leftListStartIdx + leftFocus < leftListVector.size() - 1) {
                if (leftFocus < 4) {
                    leftFocus++;
                } else if (leftListStartIdx + 9 < leftListVector.size()) {
                    if (leftFocus > 3) {
                        leftListStartIdx++;
                    } else {
                        leftFocus++;
                    }
                } else {
                    leftFocus++;
                }
            }
        } else if (state == STATE_RIGHT) {
            if (rightListStartIdx + rightFocus < rightListVector.size() - 1) {
                if (rightFocus < 4) {
                    rightFocus++;
                } else if (rightListStartIdx + 9 < rightListVector.size()) {
                    if (rightFocus > 3) {
                        rightListStartIdx++;
                    } else {
                        rightFocus++;
                    }
                } else {
                    rightFocus++;
                }
            }
        }
        repaint();
    }

    /**
     * Handle a OK key.
     */
    private void keyOK() {
        boolean down = false;
        if ((state == STATE_LEFT || state == STATE_ADD) && leftListVector.size() > 0) {
            int addedIdx = moveLeftToRight(leftListVector.get(leftListStartIdx + leftFocus));
            if (leftListStartIdx + leftFocus >= leftListVector.size()) {
                if (leftListStartIdx + leftFocus > 0) {
                    if (leftFocus > 4) {
                        leftFocus--;
                    } else if (leftListStartIdx > 0) {
                        if (leftFocus <= 4) {
                            leftListStartIdx--;
                        } else {
                            leftFocus--;
                        }
                    } else {
                        leftFocus--;
                    }
                    down = true;
                }
            }

            if (leftListVector.size() > 0) {
                addedIdx = addedIdx - rightListStartIdx;
                listUI.startEffect(ListEffect.MODE_FROM_LEFT, leftFocus, addedIdx, down);
            }

            if (leftListVector.size() == 0) {
                clearButtons();
                addButton(BTN_BACK);
                addButton(BTN_PAGE);
                addButton(BTN_CLEAR_CHANNEL_RESTRICTIONS);
                state = STATE_RIGHT;
            } else if (rightListVector.size() > 0) {
                clearButtons();
                addButton(BTN_BACK);
                addButton(BTN_PAGE);
                addButton(BTN_CLEAR_CHANNEL_RESTRICTIONS);
            }
        } else if ((state == STATE_RIGHT || state == STATE_REMOVE) && rightListVector.size() > 0) {
            int addedIdx = moveRightToLeft(rightListVector.get(rightListStartIdx + rightFocus));
            if (rightListStartIdx + rightFocus >= rightListVector.size()) {
                if (rightListStartIdx + rightFocus > 0) {
                    if (rightFocus > 4) {
                        rightFocus--;
                    } else if (rightListStartIdx > 0) {
                        if (rightFocus <= 4) {
                            rightListStartIdx--;
                        } else {
                            rightFocus--;
                        }
                    } else {
                        rightFocus--;
                    }
                    down = true;
                }
            }

            if (rightListVector.size() > 0) {
                addedIdx = addedIdx - leftListStartIdx;
                listUI.startEffect(ListEffect.MODE_FROM_RIGHT, addedIdx, rightFocus, down);
            }

            if (rightListVector.size() == 0) {
                clearButtons();
                addButton(BTN_BACK);
                addButton(BTN_PAGE);
                state = STATE_LEFT;
            }
        }
        repaint();
    }

    /**
     * Handle a page up key.
     */
    private void keyPageUp() {
        if (state == STATE_LEFT) {
            leftListStartIdx -= 9;
            if (leftListStartIdx < 0) {
                leftListStartIdx = 0;
            }
        } else if (state == STATE_RIGHT) {
            rightListStartIdx -= 9;
            if (rightListStartIdx < 0) {
                rightListStartIdx = 0;
            }
        }
        repaint();
    }

    /**
     * Handle a page down key.
     */
    private void keyPageDown() {
        if (state == STATE_LEFT) {
        	
        	if (leftListStartIdx + 9 < leftListVector.size()) {
            	leftListStartIdx += 9;
            }
            
            if (leftListStartIdx + 9 >= leftListVector.size()) {
            	leftFocus = leftListVector.size() - leftListStartIdx - 1;
            	
            	if (leftFocus < 0) {
            		leftFocus = 0;
            	}
            }
            
            if (Log.DEBUG_ON) {
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): leftListStartIdx = " + leftListStartIdx);
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): leftFocus = " + leftFocus);
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): leftListVector = " + leftListVector.size());
        	}
        } else if (state == STATE_RIGHT) {
        	
        	if (rightListStartIdx + 9 < rightListVector.size()) {
        		rightListStartIdx += 9;
        	}
        	
        	if (rightListStartIdx + 9 >= rightListVector.size()) {
        		rightFocus = rightListVector.size() - rightListStartIdx - 1;
        		
        		if (rightFocus < 0) {
        			rightFocus = 0;
            	}
            }
        	
        	if (Log.DEBUG_ON) {
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): rightListStartIdx = " + rightListStartIdx);
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): rightFocus = " + rightFocus);
        		Log.printDebug("ChannelRestrictionUI: keyPageDown(): rightListVector = " + rightListVector.size());
        	}
        }
        repaint();
    }

    /**
     * Store a data of channel restriction.
     */
    private void storeChannelData() {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < rightListVector.size(); i++) {
            Object[] data = (Object[]) rightListVector.get(i);
            if (i != 0) {
                sb.append(PreferenceService.PREFERENCE_DELIMETER);
            }
            sb.append((String) data[INDEX_CHANNEL_NAME]);
            
            Object[] sisterChannelData = (Object[]) sisterTable.get((String) data[INDEX_CHANNEL_NAME]);
            
            if (sisterChannelData != null) {
            	String callLetter = (String) sisterChannelData[INDEX_CHANNEL_NAME];
            	sb.append(PreferenceService.PREFERENCE_DELIMETER);
				sb.append(callLetter);
            }
        }
        
//        // add a sister channel
//        for (int i = 0; i < rightListVector.size(); i++) {
//            Object[] data = (Object[]) rightListVector.get(i);
//            
//            TvChannel sisterChannel = (TvChannel) sisterTable.get((String) data[INDEX_CHANNEL_NAME]);
//            
//            if (sisterChannel != null) {
//	            
//	            try {
//	            	String callLetter = sisterChannel.getCallLetter();
//	            	sb.append(PreferenceService.PREFERENCE_DELIMETER);
//					sb.append(callLetter);
//				} catch (RemoteException e) {
//					Log.print(e);
//				}
//            }
//        }
        
        Preference pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
                .get(RightFilter.CHANNEL_RESTRICTION);
        pre.setCurrentValue(sb.toString());
        DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
    }

    /**
     * Get a current state.
     * @return one of {@link #STATE_ADD} or {@link #STATE_LEFT} or {@link #STATE_ORDER} or {@link #STATE_REMOVE} or
     *         {@link #STATE_RIGHT}
     */
    public int getCurrentState() {
        return state;
    }

    /**
     * Get a start index of left list to paint.
     * @return index.
     */
    public int getLeftListStartIdx() {
        return leftListStartIdx;
    }

    /**
     * Get a start index of right list to paint.
     * @return index
     */
    public int getRightListStartIdx() {
        return rightListStartIdx;
    }

    /**
     * Move a data from left list to right list.
     * @param o channel data to be blocked.
     * @return index to be added.
     */
    public int moveLeftToRight(Object o) {
        leftListVector.remove(o);
        return addRightList(o);
    }

    /**
     * Move a data from right list to left list.
     * @param o channel data to be unblocked.
     * @return index to be added.
     */
    public int moveRightToLeft(Object o) {
        rightListVector.remove(o);
        return addLeftList(o);
    }

    /**
     * Add a data into left list.
     * @param o channel data.
     * @return index to be added.
     */
    public int addLeftList(Object o) {
        Object[] source = (Object[]) o;
        int sourceChNo = ((Integer) source[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
        int size = leftListVector.size();
        for (int i = 0; i < size; i++) {
            Object[] data = (Object[]) leftListVector.get(i);
            int dataChNo = ((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
            if (dataChNo > sourceChNo) {
                leftListVector.add(i, o);
                return i;
            }
        }
        leftListVector.add(o);
        return leftListVector.size() - 1;
    }

    /**
     * Add a data into right list.
     * @param o channel data.
     * @return index to be added.
     */
    public int addRightList(Object o) {
        Object[] source = (Object[]) o;
        int sourceChNo = ((Integer) source[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
        int size = rightListVector.size();
        for (int i = 0; i < size; i++) {
            Object[] data = (Object[]) rightListVector.get(i);
            int dataChNo = ((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
            if (dataChNo > sourceChNo) {
                rightListVector.add(i, o);
                return i;
            }
        }
        rightListVector.add(o);
        return rightListVector.size() - 1;
    }

    /**
     * Get a vector to have a unblock channels in left list.
     * @return vector to have a unblock channels.
     */
    public Vector getLeftListData() {
        return leftListVector;
    }

    /**
     * Get a vector to have a blocked channels in right list.
     * @return vector to have a blocked channels.
     */
    public Vector getRightListData() {
        return rightListVector;
    }
    
    public Hashtable getSisterTable() {
    	return sisterTable;
    }

    /**
     * Get a focus of left list.
     * @return index of focus.
     */
    public int getLeftListFocus() {
        return leftFocus;
    }

    /**
     * Get a focus of right list.
     * @return index of focus.
     */
    public int getRightListFocus() {
        return rightFocus;
    }

    /**
     * Get a subtitle of left list.
     * @param prefix string to be added at head.
     * @return subtitle for left list.
     */
    public String getLeftListSubTitle(String prefix) {
    	
    	int countSister = 0;
    	for (int i = 0; i < leftListVector.size(); i++) {
    		Object[] data = (Object[]) leftListVector.get(i);
            String callLetter = (String) data[BaseUI.INDEX_CHANNEL_NAME];
            
            if (sisterTable.containsKey(callLetter)) {
            	countSister++;
            }
    	}
    	
        StringBuffer sf = new StringBuffer();
        sf.append(prefix);
        sf.append(" (");
        if (isLoading) {
        	sf.append(0);
        } else {
        	sf.append(leftListVector.size() + countSister);
        }
        sf.append(")");

        return sf.toString();
    }

    /**
     * Get subtitle of right list.
     * @param prefix string to be added at head.
     * @return subtitle for right list.
     */
    public String getRightListSubTitle(String prefix) {
    	
    	int countSister = 0;
    	for (int i = 0; i < rightListVector.size(); i++) {
    		Object[] data = (Object[]) rightListVector.get(i);
            String callLetter = (String) data[BaseUI.INDEX_CHANNEL_NAME];
            
            if (sisterTable.containsKey(callLetter)) {
            	countSister++;
            }
    	}
    	
        StringBuffer sf = new StringBuffer();
        sf.append(prefix);
        sf.append(" (");
        if (isLoading) {
        	sf.append(0);
        } else {
        	sf.append(rightListVector.size() + countSister);
        }
        sf.append(")");

        return sf.toString();
    }

    /**
     * Get a logo image of current channel. if can't find, return a default logo.
     * @param name channel name.
     * @return logo image.
     */
    public Image getCurrentChannelLogo(String name) {
        Image logo;
        logo = (Image) SharedMemory.getInstance().get("logo." + name);

        if (logo == null) {
            return defaultChannelLogo;
        } else {
            return logo;
        }
    }

    /**
     * Handle a action after closed popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {
        removePopup(pop);
        int action = ((Integer) msg).intValue();
        repaint();
        if (pop.equals(notiUI)) {
            if (action == BaseUI.ACTION_OK) {
                for (; rightListVector.size() > 0;) {
                    moveRightToLeft(rightListVector.get(0));
                }
                rightListStartIdx = 0;
                rightFocus = 0;
                leftListStartIdx = 0;
                leftFocus = 0;
                clearButtons();
                addButton(BTN_BACK);
                addButton(BTN_PAGE);
                state = STATE_LEFT;
                repaint();
            }
        }
    }
    
    public boolean isLoading() {
    	return isLoading;
    }
}

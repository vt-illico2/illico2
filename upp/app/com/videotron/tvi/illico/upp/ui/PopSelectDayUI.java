/**
 * @(#)PopSelectDayUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.Calendar;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a UI to select a day.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectDayUI extends BaseUI {

    /** Renderer. */
    private PopSelectDayRenderer renderer = new PopSelectDayRenderer();
    /** Name for every days. */
    public String EVERY_DAYS;
    /** Name for week days. */
    public String WEEK_DAYS;
    /** Name for weekends. */
    public String WEEK_ENDS;
    /** Name for select days. */
    public String SELECT_DAYS;
    /** Days to be select. */
    private String[] DAYS;
    /** Name of days. */
    private String[] NAME_OF_DAYS;
    /** Parent UI. */
    private BaseUI parent;
    /** values to be stored.. */
    private String[] values = new String[7];
    /** Indicate that user select all. */
    private boolean everyDay;
    /** explain text. */
    private String explain;
    /** X coordinate to move. */
    private int moveX = 0;
    /** Y coordinate to move. */
    private int moveY = 0;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;

    /**
     * Set renderer.
     */
    public PopSelectDayUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Show a Popup.
     * @param ui parent UI.
     * @param data data to be stored.
     * @param explain explain text.
     * @param x x coordinate to move.
     * @param y y coordinate to move.
     */
    public void show(BaseUI ui, Object data, String explain, int x, int y) {
        prepare();
        this.moveX = x;
        this.moveY = y;
        this.explain = explain;

        this.parent = ui;
        String rowData = (String) data;
        Log.printDebug("PopSelectDayUI: show(): data = " + rowData);

        if (rowData.indexOf(Definitions.OPTION_VALUE_NO) == -1) {
            everyDay = true;
        } else {
            everyDay = false;
        }
        String[] tempValues = TextUtil.tokenize(rowData, PreferenceService.PREFERENCE_DELIMETER);

        for (int i = 0; i < tempValues.length; i++) {
            if (i == 0) {
                values[6] = tempValues[i];
            } else {
                values[i - 1] = tempValues[i];
            }
        }

        Log.printDebug("PopSelectDayUI: show(): values length : " + values.length);
        focus = findFocus();
        setVisible(true);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        NAME_OF_DAYS = new String[] {getPopupText(KeyNames.SUN), getPopupText(KeyNames.MON),
                getPopupText(KeyNames.TUE), getPopupText(KeyNames.WED), getPopupText(KeyNames.THU),
                getPopupText(KeyNames.FRI), getPopupText(KeyNames.SAT)};
        DAYS = new String[] {getPopupText(KeyNames.EVERY_DAY), getPopupText(KeyNames.MONDAY),
                getPopupText(KeyNames.TUESDAY), getPopupText(KeyNames.WEDNESDAY), getPopupText(KeyNames.THURSDAY),
                getPopupText(KeyNames.FRIDAY), getPopupText(KeyNames.SATURDAY), getPopupText(KeyNames.SUNDAY)};
        EVERY_DAYS = getPopupText(KeyNames.EVERY_DAY);
        WEEK_DAYS = getPopupText(KeyNames.WEEKDAYS);
        WEEK_ENDS = getPopupText(KeyNames.WEEKENDS);
        SELECT_DAYS = getPopupText(KeyNames.SELECT_DAYS);
        super.prepare();
    }

    /**
     * Find a focus.
     * @return index of focus.
     */
    private int findFocus() {
        return Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * Get a data to be selected and formated.
     * @return data to be selected.
     */
    public String getSelectedValue() {
        StringBuffer sb = new StringBuffer();
        if (everyDay) {
            for (int i = 0; i < 7; i++) {
                if (i > 0) {
                    sb.append(PreferenceService.PREFERENCE_DELIMETER);
                }
                sb.append(Definitions.OPTION_VALUE_YES);

            }
            return sb.toString();
        } else {
            if (!checkAnySelectedDay() && findFocus() == 0) {
                sb.append(Definitions.OPTION_VALUE_YES);
            } else {
                sb.append(values[6]);
            }
            for (int i = 0; i < 6; i++) {
                sb.append(PreferenceService.PREFERENCE_DELIMETER);

                if (!checkAnySelectedDay() && findFocus() == i + 1) {
                    sb.append(Definitions.OPTION_VALUE_YES);
                } else {
                    sb.append(values[i]);
                }

            }
            return sb.toString();
        }
    }

    /**
     * Get a selected value that build with days and be formated.
     * @param days data to be formated.
     * @return data to be formated.
     */
    public String getSelectedValue(String days) {
        String[] tempValues = TextUtil.tokenize(days, PreferenceService.PREFERENCE_DELIMETER);

        for (int i = 0; i < tempValues.length; i++) {
            if (i == 0) {
                values[6] = tempValues[i];
            } else {
                values[i - 1] = tempValues[i];
            }
        }

        return getSelectedValue();
    }

    /**
     * Check if one day is selected at least.
     * @return true if one day is selected at least, false otherwise.
     */
    private boolean checkAnySelectedDay() {
        int i = 0;
        for (; i < values.length; i++) {
            if (values[i].equals(Definitions.OPTION_VALUE_YES)) {
                break;
            }
        }

        if (i < values.length) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get a text to be displayed for data.
     * @param data to be selected.
     * @return text to be displayed
     */
    public String getDisplayText(String data) {
        prepare();
        if (data == null || data.equals(TextUtil.EMPTY_STRING)) {
            return "NONE";
        }

        int idx = data.indexOf(Definitions.OPTION_VALUE_YES);
        if (idx < 0) {
            return SELECT_DAYS;
        }

        // Everydays
        idx = data.indexOf(Definitions.OPTION_VALUE_NO);
        if (idx < 0) {
            return EVERY_DAYS;
        }

        String[] checks = TextUtil.tokenize(data, PreferenceService.PREFERENCE_DELIMETER);

        // Weekends
        if (checks[0].equals(Definitions.OPTION_VALUE_YES) && checks[6].equals(Definitions.OPTION_VALUE_YES)
                && checks[1].equals(Definitions.OPTION_VALUE_NO) && checks[2].equals(Definitions.OPTION_VALUE_NO)
                && checks[3].equals(Definitions.OPTION_VALUE_NO) && checks[4].equals(Definitions.OPTION_VALUE_NO)
                && checks[5].equals(Definitions.OPTION_VALUE_NO)) {
            return WEEK_ENDS;
        }

        // Weekdays
        if (checks[0].equals(Definitions.OPTION_VALUE_NO) && checks[6].equals(Definitions.OPTION_VALUE_NO)
                && checks[1].equals(Definitions.OPTION_VALUE_YES) && checks[2].equals(Definitions.OPTION_VALUE_YES)
                && checks[3].equals(Definitions.OPTION_VALUE_YES) && checks[4].equals(Definitions.OPTION_VALUE_YES)
                && checks[5].equals(Definitions.OPTION_VALUE_YES)) {
            return WEEK_DAYS;
        }

        // others
        int count = 0;
        StringBuffer sb = new StringBuffer();
        for (int i = 1; i <= checks.length; i++) {
            idx = i % 7;
            if (checks[idx].equals(Definitions.OPTION_VALUE_YES)) {
                if (count > 2) {
                    sb.append(",...");
                    break;
                } else if (count > 0) {
                    sb.append(", ");
                }
                sb.append(NAME_OF_DAYS[idx]);
                count++;
            }
        }
        return sb.toString();
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (focus < DAYS.length) {
                if (++focus == DAYS.length) {
                    focus = 8;
                }
            }
            repaint();
            return true;
        case OCRcEvent.VK_LEFT:
            if (focus == 9) {
                focus = 8;
                repaint();
            }

            return true;
        case OCRcEvent.VK_RIGHT:
            if (focus == 8) {
                focus = 9;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (focus == 8) {
                clickEffect.start(45 + moveX, 375 + moveY, 162, 40);
                parent.popupClosed(this, new Integer(ACTION_OK));
            } else if (focus == 9) {
                clickEffect.start(209 + moveX, 375 + moveY, 162, 40);
                parent.popupClosed(this, new Integer(ACTION_CANCEL));
            } else {
                if (focus == 0) {
                    if (everyDay) {
                        everyDay = false;
                        for (int i = 0; i < values.length; i++) {
                            values[i] = Definitions.OPTION_VALUE_NO;
                        }
                    } else {
                        everyDay = true;
                        for (int i = 0; i < values.length; i++) {
                            values[i] = Definitions.OPTION_VALUE_YES;
                        }
                    }
                } else {
                    if (values[focus - 1].equals(Definitions.OPTION_VALUE_YES)) {
                        values[focus - 1] = Definitions.OPTION_VALUE_NO;
                        everyDay = false;
                    } else {
                        values[focus - 1] = Definitions.OPTION_VALUE_YES;
                        if (isCheckAllDay()) {
                            everyDay = true;
                        }
                    }
                }
                repaint();
            }
            return true;
            // case KeyCodes.LAST:
            // parent.popupClosed(this, new Integer(ACTION_CANCEL));
            // return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * Determine that all days is selected.
     * @return true if all days is selected, false otherwise.
     */
    private boolean isCheckAllDay() {
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(Definitions.OPTION_VALUE_NO)) {
                return false;
            }
        }

        return true;
    }

    /**
     * This class render a day list to be selected.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopSelectDayRenderer extends BaseRenderer {
        /** Image for top part of background. */
//        Image bgTopImg;
        /** Image for middle part of background. */
//        Image bgMidImg;
        /** Image for bottom part of background. */
//        Image bgBottomImg;
        /** Image for gap. */
        Image gapImg;
        /** Image for highlight. */
//        Image highImg;
        /** Image for background of list. */
        Image listBgImg;
        /** Image for focus of list. */
        Image listFocusImg;
        /** Image for check box. */
        Image checkBoxImg;
        /** Image for check box to have a focus. */
        Image checkBoxFocusImg;
        /** Image for check. */
        Image checkImg;
        /** Image for check to have a focus. */
        Image checkFocusImg;
        /** Image for button to have a focus. */
        Image btnFocusImg;
        /** Image for dimmed button. */
        Image btnDimImg;
        /** Font for size 24. */
        Font f24 = FontResource.BLENDER.getFont(24);
        /** Color for title (252, 202, 4). */
        Color titleColor = new Color(251, 217, 89);
        /** Color for focus (1, 1, 1). */
        Color focusColor = new Color(1, 1, 1);
        /** Color for dimmed (214, 214, 214). */
        Color dimmedColor = new Color(214, 214, 214);
        /** Color for background (35, 35, 35). */
        Color bgColor = new Color(35, 35, 35);
        /** Color for dimmed background. (12, 12, 0, 204). */
        DVBColor dimmedBackColor = new DVBColor(12, 12, 0, 204);
        /** Name of OK button. */
        String btnOK;
        /** Name of cancel button . */
        String btnCancel;
        /** Name of title. */
        String title;

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
//            bgTopImg = BaseUI.getImage("05_pop_glow_t.png");
//            bgMidImg = BaseUI.getImage("05_pop_glow_m.png");
//            bgBottomImg = BaseUI.getImage("05_pop_glow_b.png");
            gapImg = BaseUI.getImage("pop_gap_379.png");
//            highImg = BaseUI.getImage("pop_high_350.png");
            listBgImg = BaseUI.getImage("pop_list272x220.png");
            listFocusImg = BaseUI.getImage("pop_list272x220_f.png");
            checkBoxImg = BaseUI.getImage("check_box.png");
            checkBoxFocusImg = BaseUI.getImage("check_box_foc.png");
            checkImg = BaseUI.getImage("check.png");
            checkFocusImg = BaseUI.getImage("check_foc.png");
            btnFocusImg = BaseUI.getImage("05_focus.png");
            btnDimImg = BaseUI.getImage("05_focus_dim.png");
            
            btnOK = getPopupText(KeyNames.BTN_OK);
            btnCancel = getPopupText(KeyNames.BTN_CANCEL);
            title = getPopupText(KeyNames.TITLE_SELECT_DAYS);
        };

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedBackColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            parent.paintSelectedContent(g, parent);

            g.translate(moveX, moveY);

            // 344, 42 //344, 57
            // background
//            g.drawImage(bgTopImg, 0, 0, c);
//            g.drawImage(bgMidImg, 0, 80, 410, 294, c);
//            g.drawImage(bgBottomImg, 0, 374, c);
            g.setColor(bgColor);
            g.fillRect(30, 30, 350, 394);
            g.drawImage(gapImg, 9, 68, c);
//            g.drawImage(highImg, 30, 30, c);
            g.drawImage(listBgImg, 67, 112, c);

            g.setFont(f24);
            g.setColor(titleColor);
            GraphicUtil.drawStringCenter(g, title, 202, 56);
            g.setFont(f18);
            g.setColor(Color.WHITE);
            GraphicUtil.drawStringCenter(g, explain, 205, 97);

            for (int i = 0; i < DAYS.length; i++) {
                int step = i * 31;

                if (c.getFocus() == i) {
                    g.drawImage(listFocusImg, 67, 113 + step, c);
                    g.drawImage(checkBoxFocusImg, 80, 121 + step, c);
                    g.setColor(focusColor);
                } else {
                    g.setColor(dimmedColor);
                    g.drawImage(checkBoxImg, 80, 121 + step, c);
                }

                if ((i == 0 && everyDay) || (i > 0 && values[i - 1].equals(Definitions.OPTION_VALUE_YES))) {
                    if (c.getFocus() == i) {
                        g.drawImage(checkFocusImg, 83, 120 + step, c);
                    } else {
                        g.drawImage(checkImg, 83, 120 + step, c);
                    }
                }
                g.drawString(DAYS[i], 107, 134 + step);
            }
            g.setColor(focusColor);
            if (focus == 8) {
                g.drawImage(btnFocusImg, 45, 375, c);
                g.drawImage(btnDimImg, 209, 375, c);
            } else if (focus == 9) {
                g.drawImage(btnDimImg, 45, 375, c);
                g.drawImage(btnFocusImg, 209, 375, c);
            } else {
                g.drawImage(btnDimImg, 45, 375, c);
                g.drawImage(btnDimImg, 209, 375, c);
            }
            GraphicUtil.drawStringCenter(g, btnOK, 123, 396);
            GraphicUtil.drawStringCenter(g, btnCancel, 288, 396);

            g.translate(-(moveX), -(moveY));
        }
    }
}

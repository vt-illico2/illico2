/**
 * @(#)ChannelRestrictionListUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.upp.effect.ListEffect;
import com.videotron.tvi.illico.upp.gui.ChannelRestrictionListRenderer;

/**
 * This class handle a channel list for Channel restrictions.
 * @author Woojung Kim
 * @version 1.1
 */
public class ChannelRestrictionListUI extends BaseUI {

    /** A renderer. */
    ChannelRestrictionUI parentUI;
    /** A effect. */
    ListEffect listEffect;

    /**
     * Constructor. Set a renderer and create a effect for list.
     * @param parent parent UI.
     */
    public ChannelRestrictionListUI(ChannelRestrictionUI parent) {
        parentUI = parent;
        setRenderer(new ChannelRestrictionListRenderer());
        listEffect = new ListEffect(this);
    }

    /**
     * Start a UI.
     * @param back true if back from lower UI, false otherwise.
     */
    public void start(boolean back) {
        prepare();
        super.start(back);
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        super.stop();
    }

    /**
     * Get a parent UI.
     * @return parent UI.
     */
    public ChannelRestrictionUI getParentUI() {
        return parentUI;
    }

    /**
     * Start a effect.
     * @param mode One of {@link ListEffect#MODE_FROM_LEFT} or {@link ListEffect#MODE_FROM_RIGHT}
     * @param lIdx index of left list.
     * @param rIdx index of right list.
     * @param down true if animation need a scroll from up, false toehrwise.
     */
    public void startEffect(int mode, int lIdx, int rIdx, boolean down) {
        listEffect.start(mode, lIdx, rIdx, down);
    }

    /**
     * Handle after started a animation.
     * @param effect list effect.
     */
    public void animationStarted(Effect effect) {
        super.animationStarted(effect);
        setVisible(false);
    }

    /**
     * Handle after ended a animation.
     * @param effect list effect.
     */
    public void animationEnded(Effect effect) {
        super.animationEnded(effect);
        setVisible(true);
    }
}

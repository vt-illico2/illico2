/**
 * @(#)AudioAccessibilityUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.gui.AudioAccessibilityRenderer;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class handle a data for AudioAccessibility.
 * @author Woojung Kim
 * @version 1.1
 */
public class AudioAccessibilityUI extends BaseUI {

    /** A renderer. */
    private AudioAccessibilityRenderer renderer = new AudioAccessibilityRenderer();
    /** A UI to diplay a explain text for each preferences. */
    private ExplainUI explainUI = new ExplainUI();
    /** A popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** A id's for AudioAccessibility. */
    public final String[] preferenceIds = {PreferenceNames.SAP_LANGUAGE_AUDIO, PreferenceNames.DESCRIBED_AUDIO_DISPLAY};
    /** A Hashtable to have a preferences. */
    private Hashtable preTable;
    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Constructor. set a renderer and add explainUI.
     */
    public AudioAccessibilityUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        Category gsCategory = (Category) BaseUI.peekHistory();

        preTable = DataManager.getInstance().getPreferences(gsCategory.getCategoryId());

        super.prepare();
    }

    /**
     * Start a UI.
     * @param back true if come from lower UI, false otherwise.
     */
    public void start(boolean back) {
        prepare();

        setExplainText();
        addButton(BTN_BACK);
        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        super.stop();
    }

    /**
     * Get a Hashtable to include a prefrences.
     * @return Hashtable
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Handle a key event.
     * @param code key code.
     * @return true if hande a key evnet, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:

            if (++focus == preferenceIds.length) {
                focus = preferenceIds.length - 1;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37, 211, 33);
            setPopup(selectUI);
            Preference pre = (Preference) preTable.get(preferenceIds[focus]);
            
            if (pre.getID().equals(PreferenceNames.DESCRIBED_AUDIO_DISPLAY)) {
                String onStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_ON + "4");
                String OffStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_OFF + "4");
                String focusValue = pre.getCurrentValue();
                focusValue = BaseUI.getOptionText(focusValue + "4");
                
                selectUI.show(this, new String[] {onStr, OffStr},  focusValue, pre.getName(), 519, 83 + focus * 37,
                        3);
            } else {
                selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519, 83 + focus * 37,
                        3);
            }
            repaint();
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
            return true;
        default:
            return false;
        }
    }

    /**
     * Set a explain text for current focus.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Handle a data after close popup.
     * @param pop target popup
     * @param msg message from target.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        if (pop.equals(selectUI)) {
            int action = ((Integer) msg).intValue();
            removePopup(selectUI);
            repaint();
            if (action == BaseUI.ACTION_OK) {
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                String selectedValue;
                if (focus == 0) {
                    selectedValue = selectUI.getSelectedValue();
                } else {
                    if (selectUI.getFocus() == 0) {
                        selectedValue = Definitions.OPTION_VALUE_ON;
                    } else {
                        selectedValue = Definitions.OPTION_VALUE_OFF;
                    }
                }
                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);

                PreferenceActionController.getInstance().doAction(pre.getID(), selectedValue);
            }
        }
    }

}

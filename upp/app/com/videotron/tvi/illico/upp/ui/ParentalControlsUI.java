/**
 * @(#)ParentalControlsUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.ParentalControlsRenderer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalControlsUI extends BaseUI {

    private static final long serialVersionUID = 8936382169737565687L;

    private ParentalControlsRenderer renderer = new ParentalControlsRenderer();
    private ExplainUI explainUI = new ExplainUI();

    private Hashtable filterTable;

    private PopSelectValueUI selectUI = new PopSelectValueUI();
    private ParentalControlWizardUI parentalUI = new ParentalControlWizardUI();

    /** PVR */
    public final String[] pvrIds = {RightFilter.PARENTAL_CONTROL, RightFilter.BLOCK_BY_RATINGS_DISPLAY,
            RightFilter.HIDE_ADULT_CONTENT, RightFilter.CHANNEL_RESTRICTION, RightFilter.PIN_CODE_UNBLOCKS_CHANNEL,
            RightFilter.TIME_BASED_RESTRICTIONS, RightFilter.PROTECT_PVR_RECORDINGS,
            RightFilter.PROTECT_SETTINGS_MODIFICATIONS};
    /** NON-PVR */
    public final String[] nonPvrIds = {RightFilter.PARENTAL_CONTROL, RightFilter.BLOCK_BY_RATINGS_DISPLAY,
            RightFilter.HIDE_ADULT_CONTENT, RightFilter.CHANNEL_RESTRICTION, RightFilter.PIN_CODE_UNBLOCKS_CHANNEL,
            RightFilter.TIME_BASED_RESTRICTIONS, RightFilter.PROTECT_SETTINGS_MODIFICATIONS};

    public String[] currentIds;
    private int startIdx;
    private boolean isFromOutSide;
    private int ratingFocus;
    private Preference ratingPre;
    private Preference hideAdultContentPre;
    private String outSideName;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     *
     */
    public ParentalControlsUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    public void prepare() {

        String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
        if (params != null && params.length > 1 && params[1].equals(PreferenceService.PARENTAL_CONTROLS)) {
            DataCenter.getInstance().remove("PARAMS");
            isFromOutSide = true;
            outSideName = params[0];
            addHistory(params[0]);
        } else {
            isFromOutSide = false;
        }

        filterTable = UserManager.getInstance().getCurrentUser().getAccessFilters();

        ratingPre = (Preference) filterTable.get(RightFilter.BLOCK_BY_RATINGS);
        hideAdultContentPre = (Preference) filterTable.get(RightFilter.HIDE_ADULT_CONTENT);
        if (Environment.SUPPORT_DVR) {
            currentIds = pvrIds;
        } else {
            currentIds = nonPvrIds;
        }
        super.prepare();
    }

    public void start(boolean back) {
        if (!back) {
            prepare();
            focus = 0;
            startIdx = 0;
            ratingFocus = 0;
        }
        addButton(BTN_BACK);
        add(explainUI);
        setExplainText();

        super.start();
    }

    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        super.stop();
    }

    public Hashtable getPreferences() {
        return filterTable;
    }

    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (!canSelectMenu()) {
                return true;
            }
            if (focus == 3 && startIdx > 0) {
                startIdx = 0;
                repaint();
            } else if (--focus < 0) {
                focus = 0;
            }

            // hide adult content
            if (canDrawRatings() && (startIdx + focus) == 2) {
                focus--;
            }

            if (startIdx + focus != 1) {
                ratingFocus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (!canSelectMenu()) {
                return true;
            }
            if (currentIds.length > 7 && focus == 4 && startIdx == 0) {
                startIdx = 1;
            } else if (startIdx + (++focus) == currentIds.length) {
                focus -= 1;
            }

            // hide adult content
            if (canDrawRatings() && (startIdx + focus) == 2) {
                focus++;
            }

            if (startIdx + focus != 1) {
                ratingFocus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_LEFT:
            if (canDrawRatings()) {
                ratingFocus = 0;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (canDrawRatings() && startIdx + focus == 1) {
                ratingFocus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            int idx = startIdx + focus;
            if (idx > 0) {
                if (idx == 1 && ratingFocus == 1) {
                    clickEffect.start(BaseRenderer.INPUT_X + 270, 141 + focus * 37, 211, 33);
                } else {
                    clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37, 211, 33);
                }
            } else {
                clickEffect.start(BaseRenderer.INPUT_X, 127, 211, 33);
            }
            if (idx == 1) { // block by ratings
                if (ratingFocus == 0) {
                    setPopup(selectUI);
                    Preference pre = (Preference) filterTable.get(currentIds[idx]);
                    pre.setSelectableValues(DataManager.getInstance().getSelectableValues(pre.getID()));

                    selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
                            69 + focus * 37 + 14, 3);
                } else {
                    parentalUI.start();
                    setPopup(parentalUI);
                }
                repaint();
            } else if (idx == 3) { // channel restriction
                Preference pre = (Preference) filterTable.get(currentIds[idx]);
                addHistory(pre);
                PreferenceController.getInstance().showUIComponent(BaseUI.UI_CHANNEL_RESTRICTION, false);
            } else if (idx == 5) { // time based restriction
                Preference pre = (Preference) filterTable.get(currentIds[idx]);
                addHistory(pre);
                PreferenceController.getInstance().showUIComponent(BaseUI.UI_TIME_BASED_RESTRICTIONS, false);
            } else {

                if (idx == 2 && canDrawRatings()) {
                    return true;
                }
                setPopup(selectUI);
                Preference pre = (Preference) filterTable.get(currentIds[idx]);
                pre.setSelectableValues(DataManager.getInstance().getSelectableValues(pre.getID()));

                if (focus > 1) {
                    selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
                            69 + focus * 37 + 14, 3);
                } else {
                    String activeStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_ON + "2");
                    String deactiveStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_OFF + "2");
                    String fourHourStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS);
                    String eightHourStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS);
                    String focusStr;
                    if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ON)
                            || pre.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
                        focusStr = BaseUI.getOptionText(pre.getCurrentValue() + "2");
                    } else {
                        focusStr = BaseUI.getOptionText(pre.getCurrentValue());
                    }
                    selectUI.show(this, new String[] {activeStr, deactiveStr, fourHourStr, eightHourStr}, focusStr,
                            pre.getName(), 519, 69 + focus * 37, 3);
                }
                repaint();
            }
            return true;
        case KeyCodes.LAST:
            if (isFromOutSide) {
                popHistory();
                MonitorService mService = CommunicationManager.getInstance().getMonitorService();

                if (outSideName != null && !outSideName.equals("TV")) {
                    String[] parameters = new String[1];
                    parameters[0] = MonitorService.REQUEST_APPLICATION_LAST_KEY;

                    try {
                        mService.startUnboundApplication(outSideName, parameters);
                    } catch (RemoteException e) {
                        Log.print(e);
                    }
                } else {
                    try {
                        mService.exitToChannel();
                    } catch (RemoteException e) {
                        Log.print(e);
                    }
                }
                return true;
            } else {
                clickButtons(BTN_BACK);
                // startFadeOutEffect(this);
                popHistory();
                PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
                return false;
            }

        default:
            return false;
        }
    }

    public boolean canSelectMenu() {
        Preference pre = (Preference) filterTable.get(RightFilter.PARENTAL_CONTROL);
        if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ON)) {
            return true;
        }

        return false;
    }

    public boolean canDrawRatings() {
        Preference pre = (Preference) filterTable.get(RightFilter.BLOCK_BY_RATINGS_DISPLAY);
        if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE)) {
            return true;
        }

        return false;
    }

    public Preference getRatingPreference() {
        return ratingPre;
    }

    public int getRatingFocus() {
        return ratingFocus;
    }

    public int getStartIdx() {
        return startIdx;
    }

    public String getPreferenceValueText(int idx) {
        if (idx == 1) { // block by ratings
            String value = ((Preference) filterTable.get(RightFilter.BLOCK_BY_RATINGS)).getCurrentValue();

            if (value.equals(Definitions.RATING_G)) {
                return BaseUI.getMenuText(KeyNames.NONE);
            } else {
                return value;
            }
        } else if (idx == 3) { // channel restrictions
            String value = ((Preference) filterTable.get(RightFilter.CHANNEL_RESTRICTION)).getCurrentValue();
            int length = (TextUtil.tokenize(value, "|")).length;
            StringBuffer sf = new StringBuffer();
            if (length < 2) {
                sf.append(length);
                sf.append(" ");
                sf.append(BaseUI.getMenuText(KeyNames.PROTECTED_CHANNEL));
            } else {
                sf.append(length);
                sf.append(" ");
                sf.append(BaseUI.getMenuText(KeyNames.PROTECTED_CHANNELS));
            }

            return sf.toString();
        } else if (idx == 5) { // Time-based restrictions
            String value = ((Preference) filterTable.get(RightFilter.TIME_BASED_RESTRICTIONS)).getCurrentValue();
            StringBuffer sf = new StringBuffer();

            int length = (TextUtil.tokenize(value, "^")).length;
            if (length < 2) {
                sf.append(length);
                sf.append(" ");
                sf.append(BaseUI.getMenuText(KeyNames.PERIOD));
            } else {
                sf.append(length);
                sf.append(" ");
                sf.append(BaseUI.getMenuText(KeyNames.PERIODS));
            }
            return sf.toString();
        }

        return ((Preference) filterTable.get(RightFilter.BLOCK_BY_RATINGS)).getCurrentValue();
    }

    private void setExplainText() {
        String text = dataCenter.getString("explain." + currentIds[startIdx + focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    public void popupClosed(BaseUI pop, Object msg) {

        if (pop.equals(selectUI)) {
            int action = ((Integer) msg).intValue();
            removePopup(selectUI);

            if (action == BaseUI.ACTION_OK) {
                String selectedValue;
                if (focus == 0) {
                    int selectedFocus = selectUI.getFocus();
                    if (selectedFocus == 0) {
                        selectedValue = Definitions.OPTION_VALUE_ON;
                    } else if (selectedFocus == 1) {
                        selectedValue = Definitions.OPTION_VALUE_OFF;
                    } else if (selectedFocus == 2) {
                        selectedValue = Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS;
                    } else {
                        selectedValue = Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS;
                    }
                } else {
                    selectedValue = selectUI.getSelectedValue();
                }
                Preference pre = (Preference) filterTable.get(currentIds[startIdx + focus]);
                pre.setCurrentValue(selectedValue);
                filterTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);

                if (startIdx + focus == 1) {
                    if (selectedValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
                        ratingPre.setCurrentValue(Definitions.RATING_13);
                        hideAdultContentPre.setCurrentValue((Definitions.OPTION_VALUE_YES));
                    } else {
                        ratingPre.setCurrentValue(Definitions.RATING_G);
                    }

                    filterTable.put(ratingPre.getID(), ratingPre);
                    DataManager.getInstance().setPreference(ratingPre.getID(), ratingPre.getCurrentValue());
                    filterTable.put(hideAdultContentPre.getID(), hideAdultContentPre);
                    DataManager.getInstance().setPreference(hideAdultContentPre.getID(),
                            hideAdultContentPre.getCurrentValue());
                }
            }
            repaint();
        } else if (pop.equals(parentalUI)) {
            removePopup(parentalUI);
            repaint();
        }
    }
}

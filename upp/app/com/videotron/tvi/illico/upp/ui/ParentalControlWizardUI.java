/**
 * @(#)parentalControlWizard.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.ParentalControlWizardRenderer;

/**
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalControlWizardUI extends BaseUI {
    /** Constant to indicate that focus is on ratings. */
    public static final int STATE_RATING = 0x0;
    /** Constant to indicate that focus is on buttons. */
    public static final int STATE_BUTTON = 0x1;

    /** Renderer. */
    private ParentalControlWizardRenderer renderer = new ParentalControlWizardRenderer();
    /** ids. */
    private final String[] filterIds = {RightFilter.BLOCK_BY_RATINGS};
    /** Ratings. */
    private String[] ratings = {"8+", "13+", "16+", "18+"};
    /** Selected values. */
    private String[] values = new String[filterIds.length];
    /** Hashtable to include a filters. */
    private Hashtable filterTable;
    /** Index of button. */
    private int btnFocus = 0;
    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer.
     */
    public ParentalControlWizardUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        filterTable = UserManager.getInstance().getCurrentUser().getAccessFilters();
        for (int i = 0; i < filterIds.length; i++) {
            Preference pre = (Preference) filterTable.get(filterIds[i]);

            values[i] = pre.getCurrentValue();
        }
        super.prepare();
    }

    /**
     * Start a UI.
     */
    public void start() {
        prepare();

        if (values[0].equals(Definitions.RATING_G)) {
            focus = 1;
            values[0] = Definitions.RATING_G;
        } else if (values[0].equals(Definitions.RATING_8)) {
            focus = 0;
        } else if (values[0].equals(Definitions.RATING_13)) {
            focus = 1;
        } else if (values[0].equals(Definitions.RATING_16)) {
            focus = 2;
        } else if (values[0].equals(Definitions.RATING_18)) {
            focus = 3;
        }
        btnFocus = 1;
        state = STATE_RATING;
        setVisible(true);
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_LEFT:
            keyLeft();
            return true;
        case OCRcEvent.VK_RIGHT:
            keyRight();
            return true;
        case OCRcEvent.VK_UP:
            keyUp();
            return true;
        case OCRcEvent.VK_DOWN:
            keyDown();
            return true;
        case OCRcEvent.VK_ENTER:
            keyOK();
            return true;
        case OCRcEvent.VK_EXIT:
            ((BaseUI) getParent()).popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * Handle a left key.
     */
    private void keyLeft() {
        if (state == STATE_RATING) {
            if (--focus < 0) {
                focus = 0;
            }
            values[0] = ratings[focus];
        } else {
            btnFocus = 0;
        }
        repaint();
    }

    /**
     * Handle a right key.
     */
    private void keyRight() {
        if (state == STATE_RATING) {
            if (++focus > 3) {
                focus = 3;
            }
            values[0] = ratings[focus];
        } else {
            btnFocus = 1;
        }
        repaint();
    }

    /**
     * Handle a up key.
     */
    private void keyUp() {
        state = STATE_RATING;
        repaint();
    }

    /**
     * Handle a down key.
     */
    private void keyDown() {
        state = STATE_BUTTON;
        repaint();
    }

    /**
     * Handle a ok key.
     */
    private void keyOK() {
        if (state == STATE_BUTTON) {
            if (btnFocus == 0) { // cancel
                clickEffect.start(338, 437, 144, 40);
                ((BaseUI) getParent()).popupClosed(this, new Integer(ACTION_CANCEL));
            } else { // confirm
                setValues();
                clickEffect.start(484, 437, 144, 40);
                ((BaseUI) getParent()).popupClosed(this, new Integer(ACTION_OK));
            }
        } else {
            state = STATE_BUTTON;
            btnFocus = 1;
            repaint();
        }
    }

    /**
     * Set a values to be selected.
     */
    private void setValues() {
        for (int i = 0; i < filterIds.length; i++) {
            Preference pre = (Preference) filterTable.get(filterIds[i]);

            pre.setCurrentValue(values[i]);

            filterTable.put(pre.getID(), pre);

            DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
        }
    }

    /**
     * Get a name of preference.
     * @param idx index of preference
     * @return name of preferece.
     */
    public String getPreferenceName(int idx) {
        String value = ((Preference) filterTable.get(filterIds[idx])).getName();
        return value;
    }

    /**
     * Get a preference of index.
     * @param idx index.
     * @return preference
     */
    public Preference getPreference(int idx) {
        return ((Preference) filterTable.get(filterIds[idx]));
    }

    /**
     * Get a string array of ratings.
     * @return ratings string array.
     */
    public String[] getRatings() {
        return ratings;
    }

    /**
     * Get a index of button to have a focus.
     * @return index
     */
    public int getButtonFocus() {
        return btnFocus;
    }
}

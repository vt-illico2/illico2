/**
 * @(#)ExplainUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a explain text to be displayed on bottom.
 * @author Woojung Kim
 * @version 1.1
 */
public class ExplainUI extends BaseUI {
    /** Renderer. */
    private ExplainRenderer renderer = new ExplainRenderer();
    /** Thread for auto scroll. */
    private Thread autoScrollThread;
    /** explain text. */
    private String explain;
    /** delay for auto scroll. */
    private long scrollTime;

    /**
     * Set a renderer and load a scroll time from application properties.
     */
    public ExplainUI() {
        setRenderer(renderer);
        scrollTime = dataCenter.getLong("scrollTime");
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        explain = null;
        if (autoScrollThread != null) {
            autoScrollThread.interrupt();
            autoScrollThread = null;
        }
    }

    /**
     * Set a explain.
     * @param text explain
     */
    public void setText(String text) {
        stop();
        explain = text;
        prepare();
        focus = 0;

        if (renderer.texts.length > 2) {
            autoScrollThread = new Thread(new Runnable() {
                public boolean isStart = true;

                public void run() {
                    setName("AUTO_SCROLL");
                    while (isStart) {

                        try {
                            Log.printDebug("Auto scroll thread sleep.");
                            Thread.sleep(scrollTime);
                        } catch (InterruptedException e) {
                            Log.printDebug("Auto scroll thread is interrupted.");
                            isStart = false;
                            continue;
                        }
                        if (focus + 2 < renderer.texts.length) {
                            focus += 2;
                        } else {
                            focus = 0;
                        }
                        repaint();
                    }
                }
            });
            autoScrollThread.start();
        }
    }

    /**
     * This class render a explain text.
     * @author Woojung Kim
     * @version 1.1
     */
    class ExplainRenderer extends BaseRenderer {
        /** Bounds. */
        private Rectangle bounds = new Rectangle(56, 423, 850, 40);
        /** Font of size 15. */
        private Font f15 = FontResource.BLENDER.getFont(15);
        /** FontMetrics for size 15. */
        private FontMetrics fm15 = FontResource.BLENDER.getFontMetrics(f15);
        /** text color (218, 205, 162). */
        private Color textColor = new Color(218, 205, 162);
        /** text array. */
        private String[] texts;

        /**
         * Return a bounds.
         * @param c parent UI.
         * @return bounds.
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return bounds;
        }

        /**
         * Prepare a UI.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {

            texts = TextUtil.split(explain, fm15, 850, "¶");

            super.prepare(c);
        }

        /**
         * Paint a UI.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            if (texts == null) {
                return;
            }
            g.setFont(f15);
            g.setColor(textColor);

            for (int i = 0; i < 2; i++) {
                int step = i * 20;
                int idx = focus + i;

                if (idx < texts.length) {
                    if (texts[idx] != null) {
                        g.drawString(texts[idx], 0, 17 + step);
                    }
                }
            }
        }
    }

}

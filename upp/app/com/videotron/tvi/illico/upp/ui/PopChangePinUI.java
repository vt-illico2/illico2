/**
 * @(#)PopChangePinUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent to change a PIN.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopChangePinUI extends BaseUI {
    /** State to modify admin PIN. */
    public final int STATE_MODIFY_ADMIN_PIN = 0x0;
    /** State to create family PIN. */
    public final int STATE_CREATE_FAMILY_PIN = 0x1;
    /** State to modify family PIN. */
    public final int STATE_MODIFY_FAMILY_PIN = 0x2;
    /** State to create a PIN. */
    public final int STATE_NEW_PIN = 0x3;
    /** State to modify a PIN. */
    public final int STATE_MODIFY_PIN = 0x4;
    /** Max length for PIN. */
    private final int PIN_MAX_LENGTH = 4;
    /** Renderer. */
    private ChangePinRenderer renderer = new ChangePinRenderer();
    /** Parent UI to have this popup. */
    private BaseUI parent;
    /** Target user to be updated. */
    private String targetUserId;
    /** PIN string. */
    private String pinString;
    /** New PIN string. */
    private String newPinString;
    /** Repeat PIN String. */
    private String repeatPinString;
    /** Indicate whether text draw with red color or not. */
    private boolean drawRedText;
    /** Indicate whether inputed PIN is matched with current User's PIN or not. */
    private boolean isMatched;
    /** Target User. */
    private User targetUser;
    /** Admin User. */
    private User adminUser;

    /** Explain text to modify Admin PIN. */
    private String[] adminPinExplain;
    /** Explain text to modify family PIN. */
    private String[] familyPinModifyExplain;
    /** Explain text to create family PIN. */
    private String[] familyPinCreateExplain;
    /** Explain text to create a PIN. */
    private String[] createExplain;
    /** Explain text to modify a PIN. */
    private String[] modifyExplain;
    /** Explain text that inputed PIN is not matched. */
    private String[] notMatchAdminExplain;
    /** Explain text that inputed PIN is not matched with admin PIN. */
    private String[] wrongAdminPinCode;
    /** Explain text that inputed family PIN is wrong. */
    private String[] wrongFamilyPinCode;
    /** Explain text that inputed PIN is matched with current PIN. */
    private String[] matchedCurrentPinCode;
    /** Explain text that inputed PIN should be differed with admin PIN. */
    private String[] differFromAdmin;
    /** Explain text that inputed PIN should be differed with family PIN. */
    private String[] differFromFamily;
    /** Explain text for default. */
    private String[] explainText;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;

    /**
     * Set a rednerer.
     */
    public PopChangePinUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     * @param targetState one of {@link #STATE_CREATE_FAMILY_PIN}, {@link #STATE_MODIFY_ADMIN_PIN},
     *            {@link #STATE_MODIFY_FAMILY_PIN}, {@link #STATE_MODIFY_PIN}, {@link #STATE_NEW_PIN}.
     * @param userId id of target user.
     */
    public void show(BaseUI com, int targetState, String userId) {
        prepare();
        parent = com;
        targetUserId = userId;
        targetUser = UserManager.getInstance().loadUser(userId);
        adminUser = UserManager.getInstance().loadUser(User.ID_ADMIN);

        focus = 0;
        state = targetState;

        if (userId.equals(User.ID_ADMIN)) {
            String title = getPopupText(KeyNames.TITLE_MODIFY_ADMIN_PIN);
            renderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
            explainText = adminPinExplain;
        } else if (userId.equals(User.ID_FAMILY)) {
            String title;
            if (targetUser != null && targetUser.hasPin()) {
                title = getPopupText(KeyNames.TITLE_MODIFY_FAMILY_PIN);
                explainText = familyPinModifyExplain;
            } else {
                title = getPopupText(KeyNames.TITLE_CREATE_FAMILY_PIN);
                explainText = familyPinCreateExplain;
            }
            renderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);

        } else {
            String title;
            if (targetUser != null && targetUser.hasPin()) {
                title = getPopupText(KeyNames.TITLE_MODIFY_PIN);
                explainText = createExplain;
            } else {
                title = getPopupText(KeyNames.TITLE_CREATE_PIN);
                explainText = modifyExplain;
            }
            renderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        }
        pinString = TextUtil.EMPTY_STRING;
        newPinString = TextUtil.EMPTY_STRING;
        repeatPinString = TextUtil.EMPTY_STRING;
        drawRedText = false;
        isMatched = false;
        setVisible(true);
    }

    /**
     * Get a target user id.
     * @return user id.
     */
    public String getTargetUserId() {
        return targetUserId;
    }

    /**
     * Determine whether PIN equals a family PIN.
     * @param pin target PIN.
     * @return true if PIN equals a family PIN, false otherwise.
     */
    public boolean equalsWithFamilyPIN(String pin) {
        if (Log.INFO_ON) {
            Log.printInfo("PreferenceServiceImpl: equalsWithFamilyPIN: pin=" + pin);
        }
        User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
        if (family.hasPin() && family.isValidate(pin)) {
            return true;
        }

        return false;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            if (targetUserId.equals(User.ID_ADMIN)) {
                if (focus == 0 && pinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(pinString);
                    sf.append(code - KeyEvent.VK_0);
                    pinString = sf.toString();

                    if (pinString.length() == PIN_MAX_LENGTH && targetUser.isValidate(pinString)) {
                        focus = 1;
                        drawRedText = false;
                        explainText = adminPinExplain;
                    } else if (pinString.length() == PIN_MAX_LENGTH && !targetUser.isValidate(pinString)) {
                        pinString = TextUtil.EMPTY_STRING;
                        focus = 0;
                        drawRedText = true;
                        explainText = notMatchAdminExplain;
                    }
                    repaint();
                } else if (focus == 1 && newPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(newPinString);
                    sf.append(code - KeyEvent.VK_0);
                    newPinString = sf.toString();

                    if (newPinString.length() == PIN_MAX_LENGTH && !equalsWithFamilyPIN(newPinString)) {
                        focus = 2;
                        drawRedText = false;
                        explainText = adminPinExplain;
                    } else if (equalsWithFamilyPIN(newPinString)) {
                        newPinString = TextUtil.EMPTY_STRING;
                        drawRedText = true;
                        explainText = differFromFamily;
                    }
                    repaint();
                } else if (focus == 2 && repeatPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(repeatPinString);
                    sf.append(code - KeyEvent.VK_0);
                    repeatPinString = sf.toString();

                    if (repeatPinString.length() == PIN_MAX_LENGTH && repeatPinString.equals(newPinString)) {
                        focus = 4;
                    } else if (repeatPinString.length() == PIN_MAX_LENGTH && !repeatPinString.equals(newPinString)) {
                        newPinString = TextUtil.EMPTY_STRING;
                        repeatPinString = TextUtil.EMPTY_STRING;
                        focus = 1;
                        drawRedText = true;
                        explainText = wrongAdminPinCode;
                    }
                    repaint();
                }
            } else {
                if (focus == 0 && newPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(newPinString);
                    sf.append(code - KeyEvent.VK_0);
                    newPinString = sf.toString();

                    if (newPinString.length() == PIN_MAX_LENGTH && targetUser.isValidate(newPinString)) {
                        drawRedText = true;
                        isMatched = true;
                        newPinString = TextUtil.EMPTY_STRING;
                        explainText = matchedCurrentPinCode;
                    } else if (newPinString.length() == PIN_MAX_LENGTH && !adminUser.isValidate(newPinString)) {
                        focus = 1;
                        drawRedText = false;
                        isMatched = false;
                        if (state == STATE_CREATE_FAMILY_PIN) {
                            explainText = familyPinCreateExplain;
                        } else {
                            explainText = familyPinModifyExplain;
                        }
                    } else if (adminUser.isValidate(newPinString)) {
                        focus = 0;
                        newPinString = TextUtil.EMPTY_STRING;
                        explainText = differFromAdmin;
                        drawRedText = true;
                    }
                    repaint();
                } else if (focus == 1 && repeatPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(repeatPinString);
                    sf.append(code - KeyEvent.VK_0);
                    repeatPinString = sf.toString();

                    if (repeatPinString.length() == PIN_MAX_LENGTH && repeatPinString.equals(newPinString)) {
                        focus = 3;
                    } else if (repeatPinString.length() == PIN_MAX_LENGTH && !repeatPinString.equals(newPinString)) {
                        focus = 0;
                        newPinString = TextUtil.EMPTY_STRING;
                        repeatPinString = TextUtil.EMPTY_STRING;
                        drawRedText = true;
                        explainText = wrongFamilyPinCode;
                    }
                    repaint();
                }
            }
            return true;
        case KeyEvent.VK_UP:

            if (targetUserId.equals(User.ID_ADMIN)) {
                if (focus > 2) {
                    if (pinString.length() < PIN_MAX_LENGTH) {
                        focus = 0;
                    } else if (newPinString.length() < PIN_MAX_LENGTH) {
                        focus = 1;
                    } else if (repeatPinString.length() < PIN_MAX_LENGTH) {
                        focus = 2;
                    }
                    repaint();
                }
            } else {
                if (focus > 1) {
                    if (newPinString.length() < PIN_MAX_LENGTH) {
                        focus = 0;
                    } else if (repeatPinString.length() < PIN_MAX_LENGTH) {
                        focus = 1;
                    }
                    repaint();
                }
            }
            return true;
        case KeyEvent.VK_DOWN:
            if (targetUserId.equals(User.ID_ADMIN)) {
                if (focus < 3) {
                    if ((pinString.length() == PIN_MAX_LENGTH && newPinString.length() == PIN_MAX_LENGTH
                            && repeatPinString.length() < PIN_MAX_LENGTH && repeatPinString.length() != 0)
                            || (pinString.length() == PIN_MAX_LENGTH && newPinString.length() < PIN_MAX_LENGTH && newPinString
                                    .length() != 0) || (pinString.length() < PIN_MAX_LENGTH && pinString.length() != 0)) {
                        focus = 3;
                    } else {
                        focus = 4;
                    }
                    repaint();
                }
            } else {
                if (focus < 2) {
                    if ((newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() < PIN_MAX_LENGTH && repeatPinString
                            .length() != 0) || (newPinString.length() < PIN_MAX_LENGTH && newPinString.length() != 0)) {
                        focus = 2;
                    } else {
                        focus = 3;
                    }
                    repaint();
                }
            }
            return true;
        case KeyEvent.VK_LEFT:
            if (targetUserId.equals(User.ID_ADMIN)) {
                if ((pinString.length() == PIN_MAX_LENGTH && newPinString.length() == PIN_MAX_LENGTH
                        && repeatPinString.length() < PIN_MAX_LENGTH && repeatPinString.length() != 0)
                        || (pinString.length() == PIN_MAX_LENGTH && newPinString.length() < PIN_MAX_LENGTH && newPinString
                                .length() != 0)
                        || (pinString.length() < PIN_MAX_LENGTH && pinString.length() != 0)
                        && focus == 4) {
                    focus = 3;
                    repaint();
                }
            } else {
                if ((newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() < PIN_MAX_LENGTH && repeatPinString
                        .length() != 0)
                        || (newPinString.length() < PIN_MAX_LENGTH && newPinString.length() != 0)
                        && focus == 3) {
                    focus = 2;
                    repaint();
                }
            }
            return true;
        case KeyEvent.VK_RIGHT:
            if (targetUserId.equals(User.ID_ADMIN)) {
                if (focus == 3) {
                    focus = 4;
                    repaint();
                }
            } else {
                if (focus == 2) {
                    focus = 3;
                    repaint();
                }
            }
            return true;
        case KeyEvent.VK_ENTER:

            if (targetUserId.equals(User.ID_ADMIN)) {
                if (focus == 3) {
                    clickingAnimation();
                    if (repeatPinString.length() > 0 && repeatPinString.length() < PIN_MAX_LENGTH) {
                        repeatPinString = TextUtil.EMPTY_STRING;
                        focus = 2;
                    } else if (newPinString.length() > 0 && newPinString.length() < PIN_MAX_LENGTH) {
                        newPinString = TextUtil.EMPTY_STRING;
                        focus = 1;
                        drawRedText = false;
                    } else if (pinString.length() < PIN_MAX_LENGTH) {
                        pinString = TextUtil.EMPTY_STRING;
                        focus = 0;
                    }
                    repaint();
                } else if (focus == 4) {
                    clickingAnimation();
                    if (pinString.length() == PIN_MAX_LENGTH && newPinString.length() == PIN_MAX_LENGTH
                            && repeatPinString.length() == PIN_MAX_LENGTH) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("PopChangePinUI: will save a new PIN :" + newPinString);
                            Log.printDebug("PopChangePinUI: target id :" + targetUserId);
                        }
                        User user = UserManager.getInstance().loadUser(targetUserId);
                        user.changePin(newPinString);
                        parent.popupClosed(this, new Integer(ACTION_OK));
                    } else {
                        // Cancel
                        parent.popupClosed(this, new Integer(ACTION_CANCEL));
                    }
                }
            } else {
                if (focus == 2) {
                    clickingAnimation();
                    if (repeatPinString.length() > 0 && repeatPinString.length() < PIN_MAX_LENGTH) {
                        repeatPinString = TextUtil.EMPTY_STRING;
                        focus = 1;
                    } else if (newPinString.length() < PIN_MAX_LENGTH) {
                        newPinString = TextUtil.EMPTY_STRING;
                        focus = 0;
                        drawRedText = false;
                    }
                    repaint();
                } else if (focus == 3) {
                    clickingAnimation();
                    if (newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("PopChangePinUI: will save a new PIN :" + newPinString);
                            Log.printDebug("PopChangePinUI: target id :" + targetUserId);
                        }
                        User user = UserManager.getInstance().loadUser(targetUserId);
                        user.changePin(newPinString);
                        parent.popupClosed(this, new Integer(ACTION_OK));
                    } else {
                        // Cancel
                        parent.popupClosed(this, new Integer(ACTION_CANCEL));
                    }
                }
            }
            return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * Animate a buttons when select.
     */
    private void clickingAnimation() {
        if (targetUserId.equals(User.ID_ADMIN)) {
            if (focus == 3) {
                clickEffect.start(320, 363, 162, 40);
            } else if (focus == 4) {
                clickEffect.start(484, 363, 162, 40);
            }
        } else {
            if (focus == 2) {
                clickEffect.start(321, 353, 162, 40);
            } else if (focus == 3) {
                clickEffect.start(485, 353, 162, 40);
            }
        }
    }

    /**
     * This class render to change a PIN.
     * @author Woojung Kim
     * @version 1.1
     */
    class ChangePinRenderer extends PopBaseRenderer {
        /** The gap of each pin box. */
        private static final int PIN_GAP = 38;
        /** Image for pin box. */
        private Image pinBoxImg = dataCenter.getImage("05_pin_box.png");
        /** Image for focus of pin box. */
        private Image pinFocusImg = dataCenter.getImage("05_pin_focus.png");
        /** Image for dimmed button. */
        private Image btnDimImg = dataCenter.getImage("05_focus_dim.png");
        /** Image for focus button. */
        private Image btnFocusImg = dataCenter.getImage("05_focus.png");
        /** Image for off button. */
        private Image btnOffImg = dataCenter.getImage("05_focus_off.png");
        /** Image for background. */
//        private Image bgImg = dataCenter.getImage("pop_d_glow_461.png");
        /** Image for shadow. */
//        private Image shadowImg = dataCenter.getImage("pop_sha.png");
        /** Image for highlight. */
//        private Image highImg = dataCenter.getImage("pop_high_402.png");
        /** Image for gap . */
        private Image gapImg = dataCenter.getImage("pop_gap_379.png");
        /** Image for green icon. */
        private Image iconGreenCheckImg = dataCenter.getImage("icon_g_check.png");
        /** Image for red icon. */
        private Image iconRedXImg = dataCenter.getImage("icon_r_x__small.png");

        /** Color of button strings (155, 155, 155). */
        private Color subColor = new Color(155, 155, 155);
        /** Color of start in pin box(181, 181, 181). */
        private Color starDimColor = new Color(181, 181, 181);
        /** Color of red (248, 63, 63). */
        private Color redColor = new Color(248, 63, 63);
        /** Font of size 24. */
        private Font f24 = FontResource.BLENDER.getFont(24);
        /** current password. */
        private String currentPassword;
        /** new password. */
        private String newPassword;
        /** repeat password. */
        private String repeatPassword;
        /** pin code. */
        private String pinCode;
        /** confirm pin code. */
        private String confirmPinCode;
        /** Name for button. */
        private String btnCancel, btnClear, btnOK;

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {

            String tempCurrent = getPopupText(KeyNames.EXPLAIN_MODIFY_ADMIN_PIN);
            adminPinExplain = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = getPopupText(KeyNames.EXPLAIN_CREATE_FAMILY_PIN);
            familyPinCreateExplain = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = getPopupText(KeyNames.EXPLAIN_MODIFY_FAMILY_PIN);
            familyPinModifyExplain = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = getPopupText(KeyNames.EXPLAIN_CREATE_NEW_PIN);
            createExplain = TextUtil.split(tempCurrent, fm18, 350, '|');
            tempCurrent = getPopupText(KeyNames.EXPLAIN_MODIFY_NEW_PIN);
            modifyExplain = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = getPopupText(KeyNames.EXPLAIN_NOT_MATCHED_ADMIN);
            notMatchAdminExplain = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = BaseUI.getPopupText(KeyNames.EXPLAIN_NOT_MATCHED_ADMIN_NEW);
            wrongAdminPinCode = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = BaseUI.getPopupText(KeyNames.EXPLAIN_NOT_MATCHED_FAMILY_NEW);
            wrongFamilyPinCode = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = BaseUI.getPopupText(KeyNames.EXPLAIN_MATCHED_CURRENT_PIN);
            matchedCurrentPinCode = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = BaseUI.getPopupText(KeyNames.EXPLAIN_ADMIN_DIFFER_FROM_FAMILY);
            differFromFamily = TextUtil.split(tempCurrent, fm18, 350, '|');

            tempCurrent = BaseUI.getPopupText(KeyNames.EXPLAIN_FAMILY_DIFFER_FROM_ADMIN);
            differFromAdmin = TextUtil.split(tempCurrent, fm18, 350, '|');

            currentPassword = getPopupText(KeyNames.SUBTITLE_CURRENT_PIN);
            newPassword = getPopupText(KeyNames.SUBTITLE_NEW_PIN);
            repeatPassword = getPopupText(KeyNames.SUBTITLE_REPEAT);
            pinCode = getPopupText(KeyNames.SUBTITLE_PIN_CODE);
            confirmPinCode = getPopupText(KeyNames.SUBTITLE_CONFIRM_PIN_CODE);

            btnCancel = getPopupText(KeyNames.BTN_CANCEL);
            btnClear = getPopupText(KeyNames.BTN_CLEAR);
            btnOK = getPopupText(KeyNames.BTN_CONFIRM);
        }

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

            if (targetUserId.equals(User.ID_ADMIN)) {
//                g.drawImage(bgImg, 250, 113, c);
//                g.drawImage(shadowImg, 280, 409, 402, 79, c);
                g.setColor(fillRectColor);
                g.fillRect(280, 143, 402, 269);
                g.drawImage(gapImg, 292, 181, c);
//                g.drawImage(highImg, 280, 143, c);

                g.setColor(titleColor);
                g.setFont(f24);
                GraphicUtil.drawStringCenter(g, title, 482, 171);

                g.setFont(f18);

                if (drawRedText) {
                    g.setColor(redColor);
                } else {
                    g.setColor(Color.WHITE);
                }

                // explain
                if (explainText.length == 1) {
                    if (drawRedText) {
                        int x = GraphicUtil.drawStringCenter(g, explainText[0], 494, 220);
                        g.drawImage(iconRedXImg, x - 28, 204, c);
                    } else {
                        GraphicUtil.drawStringCenter(g, explainText[0], 480, 220);
                    }
                } else {
                    for (int i = 0; i < explainText.length; i++) {
                        if (drawRedText && i == 0) {
                            int x = GraphicUtil.drawStringCenter(g, explainText[i], 490, 206 + i * 20);
                            g.drawImage(iconRedXImg, x - 28, 190, c);
                        } else {
                            int x = GraphicUtil.drawStringCenter(g, explainText[i], 480, 206 + i * 20);
                        }
                    }
                }

                // PIN BOX
                g.setFont(f17);
                g.setColor(subColor);
                g.drawString(currentPassword, 333, 262);
                g.drawString(newPassword, 333, 300);
                g.drawString(repeatPassword, 333, 338);

                g.setFont(f24);
                g.setColor(starDimColor);
                for (int i = 0; i < 4; i++) {
                    int step = i * PIN_GAP;

                    if (focus == 0 && pinString.length() == i) {
                        g.drawImage(pinFocusImg, 464 + step, 242, c);
                    } else {
                        g.drawImage(pinBoxImg, 464 + step, 242, c);
                    }

                    if (focus == 1 && newPinString.length() == i) {
                        g.drawImage(pinFocusImg, 464 + step, 279, c);
                    } else {
                        g.drawImage(pinBoxImg, 464 + step, 279, c);
                    }

                    if (focus == 2 && repeatPinString.length() == i) {
                        g.drawImage(pinFocusImg, 464 + step, 316, c);
                    } else {
                        g.drawImage(pinBoxImg, 464 + step, 316, c);
                    }

                    if (pinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 481 + step, 269);
                    }

                    if (newPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 481 + step, 306);
                    }

                    if (repeatPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 481 + step, 343);
                    }
                }

                if (pinString.length() == PIN_MAX_LENGTH && targetUser.isValidate(pinString)) {
                    g.drawImage(iconGreenCheckImg, 620, 245, c);
                }

                if (newPinString.length() == PIN_MAX_LENGTH) {
                    g.drawImage(iconGreenCheckImg, 620, 282, c);
                }

                if (repeatPinString.length() == PIN_MAX_LENGTH && repeatPinString.equals(newPinString)) {
                    g.drawImage(iconGreenCheckImg, 620, 319, c);
                }

                g.setFont(f18);
                if (pinString.length() == 0
                        || (pinString.length() == PIN_MAX_LENGTH && newPinString.length() == 0)
                        || (pinString.length() == PIN_MAX_LENGTH && newPinString.length() == PIN_MAX_LENGTH && repeatPinString
                                .length() == 0)
                        || (pinString.length() == PIN_MAX_LENGTH && newPinString.length() == PIN_MAX_LENGTH && repeatPinString
                                .length() == PIN_MAX_LENGTH)) {

                    g.drawImage(btnOffImg, 320, 363, c);
                    g.setColor(btnOffShColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 399, 384);
                    g.setColor(btnOffColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 398, 383);

                    if (focus == 4) {
                        g.drawImage(btnFocusImg, 484, 363, c);
                    } else {
                        g.drawImage(btnDimImg, 484, 363, c);
                    }
                } else {
                    g.setColor(btnColor);

                    if (focus == 3) {
                        g.drawImage(btnFocusImg, 320, 363, c);
                    } else {
                        g.drawImage(btnDimImg, 320, 363, c);
                    }

                    if (focus == 4) {
                        g.drawImage(btnFocusImg, 484, 363, c);
                    } else {
                        g.drawImage(btnDimImg, 484, 363, c);
                    }
                    GraphicUtil.drawStringCenter(g, btnClear, 398, 383);
                }

                g.setColor(btnColor);
                if (pinString.length() == PIN_MAX_LENGTH && targetUser.isValidate(pinString)
                        && newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH
                        && newPinString.equals(repeatPinString)) {
                    GraphicUtil.drawStringCenter(g, btnOK, 563, 383);
                } else {
                    GraphicUtil.drawStringCenter(g, btnCancel, 563, 383);
                }
            } else {
                // not admin
                g.drawImage(bgImg, 250, 113, c);
//                g.drawImage(shadowImg, 280, 399, 402, 79, c);
                g.setColor(fillRectColor);
                g.fillRect(280, 143, 402, 259);
                g.drawImage(gapImg, 292, 181, c);
//                g.drawImage(highImg, 280, 143, c);

                g.setColor(titleColor);
                g.setFont(f24);
                GraphicUtil.drawStringCenter(g, title, 482, 171);

                g.setFont(f18);

                if (drawRedText) {
                    g.setColor(redColor);
                } else {
                    g.setColor(Color.WHITE);
                }

                // explain
                if (explainText.length == 1) {
                    if (drawRedText) {
                        int x = GraphicUtil.drawStringCenter(g, explainText[0], 494, 226);
                        g.drawImage(iconRedXImg, x - 28, 210, c);
                    } else {
                        GraphicUtil.drawStringCenter(g, explainText[0], 480, 226);
                    }
                } else {
                    int posY = 206;
                    if (explainText.length == 2) {
                        posY = 216;
                    }
                    for (int i = 0; i < explainText.length; i++) {
                        if (drawRedText && i == 0) {
                            int x = GraphicUtil.drawStringCenter(g, explainText[i], 490, posY + i * 20);
                            g.drawImage(iconRedXImg, x - 28, posY - 16, c);
                        } else {
                            int x = GraphicUtil.drawStringCenter(g, explainText[i], 480, posY + i * 20);
                        }
                    }
                }

                // PIN BOX
                g.setFont(f17);
                g.setColor(subColor);
                g.drawString(newPassword, 326, 281);
                g.drawString(confirmPinCode, 326, 320);

                g.setFont(f24);
                g.setColor(starDimColor);
                for (int i = 0; i < 4; i++) {
                    int step = i * PIN_GAP;

                    if (focus == 0 && newPinString.length() == i) {
                        g.drawImage(pinFocusImg, 458 + step, 261, c);
                    } else {
                        g.drawImage(pinBoxImg, 458 + step, 261, c);
                    }

                    if (focus == 1 && repeatPinString.length() == i) {
                        g.drawImage(pinFocusImg, 458 + step, 299, c);
                    } else {
                        g.drawImage(pinBoxImg, 458 + step, 299, c);
                    }

                    if (newPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 475 + step, 288);
                    }

                    if (repeatPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 475 + step, 326);
                    }
                }

                if (repeatPinString.length() == PIN_MAX_LENGTH && repeatPinString.equals(newPinString)) {
                    g.drawImage(iconGreenCheckImg, 614, 264, c);
                    g.drawImage(iconGreenCheckImg, 614, 302, c);
                }

                g.setFont(f18);

                if (newPinString.length() == 0
                        || (newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == 0)
                        || (newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH)) {

                    g.drawImage(btnOffImg, 321, 353, c);
                    g.setColor(btnOffShColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 401, 376);
                    g.setColor(btnOffColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 400, 375);

                    if (focus == 3) {
                        g.drawImage(btnFocusImg, 485, 353, c);
                    } else {
                        g.drawImage(btnDimImg, 485, 353, c);
                    }
                } else {
                    g.setColor(btnColor);

                    if (focus == 2) {
                        g.drawImage(btnFocusImg, 321, 353, c);
                    } else {
                        g.drawImage(btnDimImg, 321, 353, c);
                    }

                    if (focus == 3) {
                        g.drawImage(btnFocusImg, 485, 353, c);
                    } else {
                        g.drawImage(btnDimImg, 485, 353, c);
                    }
                    GraphicUtil.drawStringCenter(g, btnClear, 400, 375);
                }

                g.setColor(btnColor);
                if (newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH) {
                    GraphicUtil.drawStringCenter(g, btnOK, 564, 375);
                } else {
                    GraphicUtil.drawStringCenter(g, btnCancel, 564, 375);
                }
            }
        }
    }
}

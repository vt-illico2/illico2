/**
 * @(#)PinEnablerUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.PinEnablerController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.PinEnablerRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a UI to get a PIN from user.
 * @author Woojung Kim
 * @version 1.1
 */
public class PinEnablerUI extends BaseUI {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** Indicate a state to input a pin. */
    public static final int STATE_INPUT = 0x00;
    /** Indicate a state to input a pin after a wrong pin. */
    public static final int STATE_INPUT_WRONG = 0x01;
    
    /** Indicate a state to input a pin with a limitation */
    public static final int STATE_INPUT_LIMIT = 0x02;
    
    /** Indicate a forget pin state of PinEnablerUI. */
    //public static final int STATE_FORGET_PIN = 0x02;
    /** Indicate a pinCode lock. */
    public static final int STATE_PIN_CODE_LOCK = 0x03;

    /** Indicate a max length of pin string to input by User. */
    private static final int PIN_MAX_LENGTH = 4;
    /** The renderer that paint a PinEnablerUI. */
    private PinEnablerRenderer pinRenderer = new PinEnablerRenderer();

    /** The pin string to input by user. */
    private String pinString;

    /** The text to explain why PinEnablerUI is used. */
    private String[] explain;
    /** Time of locked. */
    private long lockedTime = 0;
    /** How many retry. */
    public int tryTime = 0;
    /** Limit of try. */
    private final int tryLimitTime = 5;
    /** How long time locked. */
    private long limitTimeOfLock = 60 * 60 * 1000;
    /** When try the latest*/
    private long tryDateLast = 0;
    /** Indicate that request is inner or not. */
    private boolean isInnerRequest = true;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;
    
    private String[] defaultExplain;

    /**
     * Set a renderer to paint and load default limit time for locked.
     */
    public PinEnablerUI() {
        setRenderer(pinRenderer);
        limitTimeOfLock = DataCenter.getInstance().getLong("limitTimeOfLock");
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Called when PinEnablerUI start.
     */
    public void start() {
    	/**
    	 * R7.4 Blocked Admin PIN
    	 */
        pinString = TextUtil.EMPTY_STRING;
        focus = 0;
        long currentTime = System.currentTimeMillis();
        
        // every time reload for language.
        String tmp = BaseUI.getPopupText(KeyNames.EXPLAIN_NEED_PIN);
        defaultExplain = TextUtil.split(tmp, BaseRenderer.fm18, 310, '|');
        Preference pre = DataManager.getInstance().getPreference("lockedTime");
        if (pre != null && pre.getCurrentValue() != null) {
            try {
                lockedTime = Long.parseLong(pre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                lockedTime = 0;
            }
        } else {
            lockedTime = 0;
        }
        
        Preference tryTimePre = DataManager.getInstance().getPreference("tryTime");
        if (tryTimePre != null && tryTimePre.getCurrentValue() != null) {
            try {
                tryTime = Integer.parseInt(tryTimePre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                tryTime = 0;
            }
        } else {
            tryTime = 0;
        }
        
        Preference tryDateLastPre = DataManager.getInstance().getPreference("tryDateLast");
        if (tryDateLastPre != null && tryDateLastPre.getCurrentValue() != null) {
            try {
                tryDateLast = Long.parseLong(tryDateLastPre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                tryDateLast = 0;
            }
        } else {
        	tryDateLast = 0;
        }
        
        if(Log.DEBUG_ON) {
            Log.printDebug("PinEnabler: start(): lockedTime = " + lockedTime);
            Log.printDebug("PinEnabler: start(): limitTimeOfLock = " + limitTimeOfLock);
            Log.printDebug("PinEnabler: start(): TartgetTime = " + (lockedTime + limitTimeOfLock));
            Log.printDebug("PinEnabler: start(): currentTime = " + currentTime);
            Log.printDebug("PinEnabler: start(): remainTime = " + ((lockedTime + limitTimeOfLock) - currentTime));
            Log.printDebug("PinEnabler: start(): tryTime = " + tryTime);
            Log.printDebug("PinEnabler: start(): tryDateLast = " + tryDateLast);
            Log.printDebug("PinEnabler: start(): remainTimeRefresh = " + (currentTime - tryDateLast + limitTimeOfLock ));
        }
        
        if (tryTime == 0 || 
        		(tryTime < tryLimitTime && (currentTime - tryDateLast) >= limitTimeOfLock)) {
        	tryTime = 0;
        	state = STATE_INPUT;
        	String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);            
        } else if (tryTime < tryLimitTime && (currentTime - tryDateLast) < limitTimeOfLock){
        	state = STATE_INPUT_LIMIT;
        	String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        } else if (tryTime >= tryLimitTime && lockedTime + limitTimeOfLock > currentTime) {
        	state = STATE_PIN_CODE_LOCK;
        	String title = getPopupText(KeyNames.TITLE_PIN_ERROR);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
            
        } else if (tryTime >= tryLimitTime && lockedTime + limitTimeOfLock < currentTime) {
        	tryTime = 0;
        	state = STATE_INPUT;
        	String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        }
        
        prepare();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        setVisible(true);
    }
    
    /**
     * Called when PinEnablerUI start.
     */
    public void startInWrongPin() {
    	/**
    	 * R7.4 Blocked Admin PIN
    	 */
    	pinString = TextUtil.EMPTY_STRING;
        focus = 0;
        long currentTime = System.currentTimeMillis();
        
        // every time reload for language.
        String tmp = BaseUI.getPopupText(KeyNames.EXPLAIN_NEED_PIN);
        defaultExplain = TextUtil.split(tmp, BaseRenderer.fm18, 310, '|');
        Preference pre = DataManager.getInstance().getPreference("lockedTime");
        if (pre != null && pre.getCurrentValue() != null) {
            try {
                lockedTime = Long.parseLong(pre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                lockedTime = 0;
            }
        } else {
            lockedTime = 0;
        }
        
        Preference tryTimePre = DataManager.getInstance().getPreference("tryTime");
        if (tryTimePre != null && tryTimePre.getCurrentValue() != null) {
            try {
                tryTime = Integer.parseInt(tryTimePre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                tryTime = 0;
            }
        } else {
            tryTime = 0;
        }
        
        Preference tryDateLastPre = DataManager.getInstance().getPreference("tryDateLast");
        if (tryDateLastPre != null && tryDateLastPre.getCurrentValue() != null) {
            try {
                tryDateLast = Long.parseLong(tryDateLastPre.getCurrentValue());
            } catch (Exception e) {
                Log.printWarning(e);
                tryDateLast = 0;
            }
        } else {
        	tryDateLast = 0;
        }
        
        if(Log.DEBUG_ON) {
            Log.printDebug("PinEnabler: startInWrongPin(): lockedTime = " + lockedTime);
            Log.printDebug("PinEnabler: startInWrongPin(): limitTimeOfLock = " + limitTimeOfLock);
            Log.printDebug("PinEnabler: startInWrongPin(): TartgetTime = " + (lockedTime + limitTimeOfLock));
            Log.printDebug("PinEnabler: startInWrongPin(): currentTime = " + currentTime);
            Log.printDebug("PinEnabler: startInWrongPin(): remainTime = " + ((lockedTime + limitTimeOfLock) - currentTime));
            Log.printDebug("PinEnabler: startInWrongPin(): tryTime = " + tryTime);
            Log.printDebug("PinEnabler: startInWrongPin(): tryDateLast = " + tryDateLast);
            Log.printDebug("PinEnabler: startInWrongPin(): remainTimeRefresh = " + (currentTime - tryDateLast + limitTimeOfLock ));
        }
        
        if (tryTime < tryLimitTime && (currentTime - tryDateLast) < limitTimeOfLock){
        	state = STATE_INPUT_WRONG;
        	String title = getPopupText(KeyNames.TITLE_PIN_ERROR);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
            
        } else if (tryTime == tryLimitTime && lockedTime + limitTimeOfLock > currentTime) {
        	state = STATE_PIN_CODE_LOCK;
        	String title = getPopupText(KeyNames.TITLE_PIN_ERROR);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
        } else if (tryTime == tryLimitTime && lockedTime + limitTimeOfLock < currentTime) {
        	tryTime = 0;
        	state = STATE_INPUT;
        	String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
            pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        }
        
        prepare();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        setVisible(true);
    }

    /**
     * Called when PinEnablerUI stop.
     */
    public void stop() {
        explain = null;
        pinString = null;
        isInnerRequest = true;
        setVisible(false);
    }

    /**
     * Called when PinEnablerUI dispose. Clean up a resources.
     */
    public void dispose() {
        stop();
    }

    /**
     * Handle a key event for PinEnablerUI.
     * @param code the code of key event.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
        	if (focus == 0 && pinString.length() < PIN_MAX_LENGTH) {
                StringBuffer sf = new StringBuffer(pinString);
                sf.append(code - KeyEvent.VK_0);
                pinString = sf.toString();

                if (pinString.length() == PIN_MAX_LENGTH) {
                    repaint();
                    validatePin();
                } else {
                    prepare();
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_LEFT:
        	if (pinString.length() > 0 && focus == 2) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
        	if (focus == 1) {
                focus = 2;
                repaint();
            }
            return true;
        case KeyEvent.VK_UP:
        	if (focus == 1 && pinString.length() == 4) {
                focus = 0;
                pinString = TextUtil.EMPTY_STRING;
            } else if (focus == 1 || focus == 2) {
                focus = 0;
            }
            repaint();
            return true;

        case KeyEvent.VK_DOWN:
        	if (pinString.length() == 0) {
                focus = 2;
            } else {
                focus = 1;
            }
            repaint();
            return true;
        case KeyEvent.VK_ENTER:
            if (state == STATE_INPUT || 
            		state == STATE_INPUT_WRONG ||
            		state == STATE_INPUT_LIMIT) {
                if (focus == 1 & pinString.length() > 0) { // delete
                    clickingAnimation();
                    pinString = TextUtil.EMPTY_STRING;
                    focus = 0;
                    repaint();
                } else if (focus == 2) { // cancel
                    clickingAnimation();
                    PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
                }
            } else if (state == STATE_PIN_CODE_LOCK) {
                clickingAnimation();
                PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
            }
            return true;
        case OCRcEvent.VK_EXIT:
            // case KeyCodes.LAST:
            PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
            return true;
        default:
            if (Log.INFO_ON) {
                Log.printInfo("PinEnabler: handleKey: just keep a other keys");
            }
            return true;
        }
    }

    /**
     * Animate a buttons when select.
     */
    private void clickingAnimation() {
        if (state != STATE_PIN_CODE_LOCK) {
            if (focus == 1) {
                clickEffect.start(321, 368, 162, 40);
            } else if (focus == 2) {
                clickEffect.start(485, 368, 162, 40);
            }
        } else {
            clickEffect.start(403, 318, 162, 40);
        }        
    }

    /**
     * validate by pin string.
     */
    private void validatePin() {
        User user = UserManager.getInstance().validatePin(pinString);

        if (user != null) {        
        	tryTime = 0;
        	DataManager.getInstance().setPreference("tryTime", String.valueOf(tryTime));
            DataManager.getInstance().setPreference("tryDateLast", String.valueOf(0));
            
            PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_SUCCESS,
                    user.getUserId());
        } else {
            if (tryTime < tryLimitTime-1) {
                tryTime++;
                if (state != STATE_INPUT_WRONG) {
                    state = STATE_INPUT_WRONG;
                    pinString = TextUtil.EMPTY_STRING;
                    String title = getPopupText(KeyNames.TITLE_INVALID_PIN);
                    pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
                    focus = 0;
                } else {
                	String title = getPopupText(KeyNames.TITLE_INVALID_PIN);
                    pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
                    pinString = TextUtil.EMPTY_STRING;
                    focus = 0;
                }
                tryDateLast = System.currentTimeMillis();
                DataManager.getInstance().setPreference("tryTime", String.valueOf(tryTime));
                DataManager.getInstance().setPreference("tryDateLast", String.valueOf(tryDateLast));
            } else {
            	tryTime++;
                lockedTime = System.currentTimeMillis();
                tryDateLast = lockedTime;
                DataManager.getInstance().setPreference("lockedTime", String.valueOf(lockedTime));
                DataManager.getInstance().setPreference("tryTime", String.valueOf(tryTime));
                DataManager.getInstance().setPreference("tryDateLast", String.valueOf(tryDateLast));
                state = STATE_PIN_CODE_LOCK;
                pinString = TextUtil.EMPTY_STRING;
                String title = getPopupText(KeyNames.TITLE_PIN_ERROR);
                pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_RED);
            }
            repaint();
        }
    }

    /**
     * Get a pin string to input by User.
     * @return The pin string to input by User.
     */
    public String getPinString() {
        return pinString;
    }

    /**
     * Set a text to explain why PinEnablerUI is used.
     * @param title title of PIN enabler
     * @param texts explain texts.
     */
    public void setText(String title, String[] texts) {
        if (title == null) {
            title = getPopupText(KeyNames.TITLE_ENTER_PIN);
        }
        pinRenderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        this.explain = texts;
    }

    /**
     * Get a explain text.
     * @see #setExplain(String[])
     * @return the explain texts.
     */
    public String[] getExplain() {
        if (explain == null) {
            return defaultExplain;
        }
        return explain;
    }

    /**
     * Return a true if PinEnabler is called from UPP self or return a false if not.
     * @return The boolean if called from UPP or not.
     */
    public boolean isInnerRequest() {
        return isInnerRequest;
    }

    /**
     * Set a true if request is from UPP self or set a false if not.
     * @param fromUpp true or false.
     */
    public void setInnerRequest(boolean fromUpp) {
        this.isInnerRequest = fromUpp;
    }
}

/**
 * @(#)STBSleepTimeUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.text.DecimalFormat;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.Time;
import com.videotron.tvi.illico.upp.gui.STBSleepTimeRenderer;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class represent a UI to modify a sleep time.
 * @author Woojung Kim
 * @version 1.1
 */
public class STBSleepTimeUI extends BaseUI {

    /** Renderer. */
    private STBSleepTimeRenderer renderer = new STBSleepTimeRenderer();
    /** Popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** Popup to select a time. */
    private PopSelectSmallUI selectTimeUI = new PopSelectSmallUI();
    /** Popup to select a days. */
    private PopSelectDayUI selectDayUI = new PopSelectDayUI();
    /** Buffer for directly input. */
    private StringBuffer inputBuffer = new StringBuffer();
    /** Decimal format for time. */
    private DecimalFormat df = new DecimalFormat("00");
    /** Time object which is made with Preference data. */
    private Time time;
    /** Preference to have a information for Sleep Time. */
    private Preference pre;
    /** Indicate whether language is a English. */
    private boolean isEnglish;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer and create a Time object and ClickEffect.
     */
    public STBSleepTimeUI() {
        time = new Time();
        renderer.setHasExpalin(false);
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        pre = (Preference) BaseUI.peekHistory();

        if (Log.DEBUG_ON) {
            Log.printDebug("STBSleepTimeDetaulUI: prepare(): time data : " + pre.getCurrentValue());
        }
        time.setData(pre.getCurrentValue());

        focus = 0;
        inputBuffer.setLength(0);

        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE);
        if (pre.getCurrentValue().equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }

        super.prepare();
    }

    /**
     * Start a UI.
     */
    public void start(boolean back) {
        prepare();
        addButton(BTN_BACK);
        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        storeTimeData();
        super.stop();
    }

    /**
     * Get a Preference for Sleep time.
     * @return preference for sleep time.
     */
    public Preference getPreference() {
        return pre;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            if (focus == 1 || focus == 2) {
                updateTime(code);
            }
            return true;
        case OCRcEvent.VK_UP:
            updateTime(code);
            if (canSelect()) {
                if (focus < 4) {
                    focus = 0;
                } else {
                    focus = 1;
                }
                repaint();
            }
            return true;
        case OCRcEvent.VK_DOWN:
            updateTime(code);
            if (canSelect()) {
                if (focus == 0) {
                    focus = 1;
                } else if (focus < 4) {
                    focus = 4;
                }
                repaint();
            }
            return true;
        case OCRcEvent.VK_LEFT:
            updateTime(code);
            if (canSelect()) {
                if (focus == 2 || focus == 3) {
                    focus--;
                    repaint();
                } else if (focus == 5) {
                    focus = 4;
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            updateTime(code);
            if (canSelect()) {
                if (focus == 1 || focus == 2) {
                    focus++;
                    if (!isEnglish && focus == 3) {
                        focus = 2;
                    }
                    repaint();
                } else if (focus == 4) {
                    focus = 5;
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (focus == 0) { // Active
                clickEffect.start(224, 143, 211, 33);
                setPopup(selectUI);
                String enabled = BaseUI.getOptionText(Definitions.OPTION_VALUE_ENABLE + "d");
                String disabled = BaseUI.getOptionText(Definitions.OPTION_VALUE_DISABLE + "d");
                String focused = BaseUI.getOptionText(time.getActive() + "d");
                selectUI.show(this, new String[] {enabled, disabled}, focused, BaseUI.getMenuText(KeyNames.SLEEP_TIME),
                        373, 85, 3);
                repaint();
            } else if (focus == 1) { // start hour
                clickEffect.start(224, 201, 38, 32);
                setPopup(selectTimeUI);
                if (isEnglish) {
                    if (time.getStartAmPm().equals(Definitions.TIME_AM)) {
                        selectTimeUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_AM, time.getStartHour(), 224, 176);
                    } else {
                        selectTimeUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_PM, time.getStartHour(), 224, 176);
                    }
                } else {
                    String hourStr = time.getStartHour();
                    int hour = Integer.parseInt(time.getStartHour());
                    if (time.getStartAmPm().equals("pm")) {
                        hour += 12;
                        hourStr = df.format(hour);
                    }
                    selectTimeUI.show(this, 24, PopSelectSmallUI.TYPE_HOURS_PM, hourStr, 224, 176);
                }
                repaint();
            } else if (focus == 2) { // start min
                clickEffect.start(276, 201, 38, 32);
                setPopup(selectTimeUI);
                selectTimeUI.show(this, 60, PopSelectSmallUI.TYPE_MINS, time.getStartMin(), 276, 176);
                repaint();
            } else if (focus == 3) { // start am/pm
                clickEffect.start(328, 201, 38, 32);
                setPopup(selectTimeUI);
                selectTimeUI.show(this, new String[] {Definitions.TIME_AM, Definitions.TIME_PM}, time.getStartAmPm(),
                        328, 176);
                repaint();
            } else if (focus == 4) { // day
                clickEffect.start(224, 241, 211, 33);
                setPopup(selectDayUI);
                selectDayUI
                        .show(this, time.getDays(), getExplainText(KeyNames.EXPLAIN_SELECT_DAYS_SLEEP_TIME), 344, 57);
                repaint();
            } else if (focus == 5) { // repeat
                clickEffect.start(652, 241, 211, 33);
                setPopup(selectUI);
                String everyWeek = BaseUI.getPopupText(KeyNames.EVERY_WEEK);
                String none = BaseUI.getPopupText(KeyNames.NONE);
                String focused = none;
                if (time.getRepeat().equals("Yes")) {
                    focused = everyWeek;
                }
                selectUI.show(this, new String[] {everyWeek, none}, focused, BaseUI.getMenuText(KeyNames.REPEAT), 725,
                        183, 3);
                repaint();
            }
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, true);
            return true;
        default:
            return false;
        }
    }

    /**
     * Update a time according to key code.
     * @param code key code.
     */
    private void updateTime(int code) {
        if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
            int input = code - OCRcEvent.VK_0;

            int limitCode = 1;

            if (!isEnglish) {
                limitCode = 2;
            }

            if (focus == 1) { // hour
                if (input > limitCode && input < 10 && inputBuffer.length() == 0) { // 2 ~ 9
                    String value = df.format(input);
                    time.setStartHour(value);
                    focus++;
                    inputBuffer.setLength(0);
                    repaint();
                } else { // 1 , 10, 11, 2, 20, 21, 22, 23, 24
                    inputBuffer.append(input);
                    if (inputBuffer.length() == 2) {
                        input = Integer.parseInt(inputBuffer.toString());
                        if (input < 12) {
                            String value = df.format(input);
                            time.setStartHour(value);
                            time.setStartAmPm("am");
                            focus++;
                        } else if (input == 12 && isEnglish) {
                            String value = df.format(00);
                            time.setStartHour(value);
                            time.setStartAmPm("am");
                            focus++;
                        } else if (input < 24 && !isEnglish) {
                            String value = df.format(input - 12);
                            time.setStartHour(value);
                            time.setStartAmPm("pm");
                            focus++;
                        }
                        inputBuffer.setLength(0);
                    }
                    repaint();
                }
            } else if (focus == 2) { // min
                if (input > 5 && input < 10 && inputBuffer.length() == 0) { // 6 ~ 9
                    String value = df.format(input);
                    time.setStartMin(value);
                    focus++;
                    if (!isEnglish) {
                        focus++;
                    }
                    repaint();
                } else {
                    inputBuffer.append(input);
                    if (inputBuffer.length() == 2) {
                        input = Integer.parseInt(inputBuffer.toString());
                        if (input < 60) {
                            String value = df.format(input);
                            time.setStartMin(value);
                            focus++;
                            if (!isEnglish) {
                                focus++;
                            }
                        }
                        inputBuffer.setLength(0);
                    }
                    repaint();
                }
            }
        } else { // Left, Right Key
            if (inputBuffer.length() > 0) {
                if (focus == 1) {
                    int input = Integer.parseInt(inputBuffer.toString());
                    if (input < 12) {
                        String value = df.format(input);
                        time.setStartHour(value);
                    }
                } else if (focus == 2) {
                    int input = Integer.parseInt(inputBuffer.toString());
                    if (input < 60) {
                        String value = df.format(input);
                        time.setStartMin(value);
                    }
                }
                inputBuffer.setLength(0);
            }
        }
    }

    /**
     * Determine whether direct input mode is using or not.
     * @return true if direct input mode is using, false otherwise.
     */
    public boolean isInputMode() {
        if (inputBuffer.length() == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get a value of direct input.
     * @return value which is inputed.
     */
    public String getInputValue() {
        return inputBuffer.toString();
    }

    /**
     * Get a time.
     * @return time to have a information which is saved a data of sleep time.
     */
    public Time getTime() {
        return time;
    }

    /**
     * Store a data.
     */
    private void storeTimeData() {
        String curData = time.getStringData();
        String savedData = pre.getCurrentValue();
        
        if (Log.DEBUG_ON) {
            Log.printDebug("STBSleepTimeUI: storeTimeData: curData=" + curData);
            Log.printDebug("STBSleepTimeUI: storeTimeData: savedData=" + savedData);
        }
        
        if (!curData.equals(savedData)) {
            time.setSaveTime(System.currentTimeMillis());
            if (time.getDays().indexOf(Definitions.OPTION_VALUE_YES) == -1) {
                time.setDays(selectDayUI.getSelectedValue(time.getDays()));
            }
            pre.setCurrentValue(time.getStringData());
            DataManager.getInstance().setPreference(PreferenceNames.STB_SLEEP_TIME, time.getStringData());
            
            PreferenceActionController.getInstance().doAction(pre.getID(), pre.getCurrentValue());
        }
    }

    /**
     * Return a display text according to be a selected days. ex) Every day, Weekend
     * @return The text to display for selected days.
     */
    public String getDaysText() {
        if (Log.DEBUG_ON) {
            Log.printDebug("time = " + time.getDays());
            Log.printDebug("select = " + selectDayUI.getDisplayText(time.getDays()));
        }
        return selectDayUI.getDisplayText(time.getDays());
    }

    /**
     * Determine whether user can select a value for time.
     * @return true if user can select a value, false otherwise.
     */
    public boolean canSelect() {
        if (time.getActive().equals(Definitions.OPTION_VALUE_ENABLE)) {
            return true;
        }
        return false;
    }

    /**
     * Handle a action after popup closed.
     * @param pop popup is closed.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {
        removePopup(pop);
        int action = ((Integer) msg).intValue();
        if (pop.equals(selectUI)) {
            if (action == BaseUI.ACTION_OK) {
                int selectedFocus = selectUI.getFocus();
                if (focus == 0) {
                    if (selectedFocus == 0) {
                        time.setActive(Definitions.OPTION_VALUE_ENABLE);
                    } else {
                        time.setActive(Definitions.OPTION_VALUE_DISABLE);
                    }
                } else if (focus == 5) {
                    if (selectedFocus == 0) {
                        time.setRepeat(Definitions.OPTION_VALUE_YES);
                    } else {
                        time.setRepeat(Definitions.OPTION_VALUE_NO);
                    }
                    time.setSaveTime(System.currentTimeMillis());
                }
            }
            repaint();
        } else if (pop.equals(selectTimeUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectTimeUI.getSelectedValue();

                if (focus == 1) { // hour
                    if (isEnglish) {
                        if (selectedValue.equals("12")) {
                            time.setStartHour("00");
                        } else {
                            time.setStartHour(selectedValue);
                        }
                    } else {
                        int selectedInt = Integer.valueOf(selectedValue).intValue();
                        if (selectedInt > 11) {
                            selectedInt -= 12;
                            time.setStartHour(df.format(selectedInt));
                            time.setStartAmPm("pm");
                        } else {
                            time.setStartHour(selectedValue);
                            time.setStartAmPm("am");
                        }
                    }
                } else if (focus == 2) { // min
                    time.setStartMin(selectedValue);
                } else if (focus == 3) { // AM_PM
                    time.setStartAmPm(selectedValue);
                }
            }
            repaint();
        } else if (pop.equals(selectDayUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectDayUI.getSelectedValue();

                time.setDays(selectedValue);
            }

            repaint();
        }
    }
}

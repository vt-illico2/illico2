/**
 * @(#)ParentalControlInfoUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ui.ImageLabel;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.ParentalController;
import com.videotron.tvi.illico.upp.effect.LockEffect;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent to display a current parent controls.
 * @author Woojung Kim
 * @version 1.1
 */
public class ParentalControlInfoUI extends BaseUI {
    /** Indicate that parent control is On. */
    public final int STATE_ON = 0x0;
    /** Indicate that parental control is suspended for 4 hours. */
    public final int STATE_SUSPEND_4 = 0x1;
    /** Indicate that parental control is suspended for 8 hours. */
    public final int STATE_SUSPEND_8 = 0x2;
    /** Indicate that parental control is off. */
    public final int STATE_OFF = 0x2;
    /** Renderer. */
    private ParentalControlInfoRenderer renderer = new ParentalControlInfoRenderer();

    /** Effect to display unlocking. */
    private Effect unlockEffect;
    /** Effect to display locking. */
    private Effect lockEffect;
    /** ImageLable for opened icon. */
    private ImageLabel openIcon;
    /** ImageLabel for closed icon. */
    private ImageLabel closeIcon;
    /** Current ImageLable. */
    private ImageLabel current;
    /** Image for open icon . */
    private Image openImg;
    /** Image for close icon. */
    private Image closeImg;
    /** Title. */
    private String myTitle;
    /** Explain . */
    private String[] explain;
    /** Indicate whether animation is finished. */
    private boolean isFinished;

    /**
     * Set a renderer and load a images and prepare a ImageLabels. Create a Effect for animtation.
     */
    public ParentalControlInfoUI() {
        setRenderer(renderer);
        openImg = DataCenter.getInstance().getImage("lock02.png");
        closeImg = DataCenter.getInstance().getImage("lock02_1.png");
        openIcon = new ImageLabel(openImg);
        closeIcon = new ImageLabel(closeImg);
        FrameworkMain.getInstance().getImagePool().waitForAll();
        openIcon.setLocation(441, 213);
        openIcon.setVisible(true);
        closeIcon.setLocation(422, 213);
        closeIcon.setVisible(true);

        unlockEffect = new LockEffect(openIcon, 25, 34, 0, -30);
        // unlockEffect.updateBackgroundBeforeStart(true);
        lockEffect = new LockEffect(closeIcon, 25, 34, 0, 30);
        // lockEffect.updateBackgroundBeforeStart(true);

        setVisible(true);

    }

    /**
     * Show a UI.
     * @param targetState One of {@link #STATE_ON} or {@link #STATE_OFF} or {@link #STATE_SUSPEND_4} or
     *            {@link #STATE_SUSPEND_8}
     */
    public void show(int targetState) {
        this.remove(openIcon);
        this.remove(closeIcon);
        focus = 0;
        state = targetState;
        isFinished = false;
        current = openIcon;
        if (targetState == STATE_ON) {
            myTitle = BaseUI.getPopupText(KeyNames.TITLE_PARENTAL_CONTROL_ON);
            String tmp = BaseUI.getPopupText(KeyNames.EXPLAIN_PARENTAL_CONTROL_ON);
            explain = TextUtil.split(tmp, BaseRenderer.fm18, 320, '|');
            add(closeIcon);
            lockEffect.setRequestor(this);
            lockEffect.start();
            current = closeIcon;
        } else if (targetState == STATE_SUSPEND_4) {
            myTitle = BaseUI.getPopupText(KeyNames.TITLE_PARENTAL_CONTROL_SUSPEND);
            String tmp = BaseUI.getPopupText(KeyNames.EXPLAIN_PARENTAL_CONTROL_SUSPEND_4);
            explain = TextUtil.split(tmp, BaseRenderer.fm18, 320, '|');
            add(openIcon);
            unlockEffect.setRequestor(this);
            unlockEffect.start();
        } else if (targetState == STATE_SUSPEND_8) {
            myTitle = BaseUI.getPopupText(KeyNames.TITLE_PARENTAL_CONTROL_SUSPEND);
            String tmp = BaseUI.getPopupText(KeyNames.EXPLAIN_PARENTAL_CONTROL_SUSPEND_8);
            explain = TextUtil.split(tmp, BaseRenderer.fm18, 320, '|');
            add(openIcon);
            unlockEffect.setRequestor(this);
            unlockEffect.start();
        }
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        default:
            ParentalController.getInstance().hideParentalInfo();
            return true;
        }
    }

    /**
     * Determine whether animation can skip or not.
     * @param effect effect.
     * @return false. It means animation never be skiped.
     */
    public boolean skipAnimation(Effect effect) {
        return false;
    }

    /**
     * Handle a action after animation started.
     * @param effect effect
     */
    public void animationStarted(Effect effect) {
    }

    /**
     * Handle a action after animation ended.
     * @param effect effect
     */
    public void animationEnded(Effect effect) {
        isFinished = true;
        ParentalController.getInstance().hideParentalInfo();
    }

    /**
     * This class render a information to update a parental control.
     * @author Woojung Kim
     * @version 1.1
     */
    class ParentalControlInfoRenderer extends Renderer {
        /** Image for a top part of background. */
//        private Image topImg = dataCenter.getImage("05_pop_glow_t.png");
        /** Image for a middle part of background. */
//        private Image midImg = dataCenter.getImage("05_pop_glow_m.png");
        /** Image for a bottom part of background. */
//        private Image bottomImg = dataCenter.getImage("05_pop_glow_b.png");
        /** Image for shadow. */
//        private Image shadowImg = dataCenter.getImage("pop_sha.png");
        /** Image for gap. */
        private Image gapImg = dataCenter.getImage("pop_gap_379.png");
        /** Image for highlight. */
//        private Image highImg = dataCenter.getImage("pop_high_350.png");

        /** The color of title. */
        private Color titleColor = new Color(255, 203, 0);
        /** The color to be dimmed a PinEnablerUI's back. */
        private Color dimmedColor = new DVBColor(0, 0, 0, 204);
        /** Color to draw a background. */
        private Color fillRectColor = new DVBColor(35, 35, 35, 255);
        /** Font of size 24. */
        private Font f24 = FontResource.BLENDER.getFont(24);
        /** font of size 18. */
        private Font f18 = FontResource.BLENDER.getFont(18);

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
        }

        /**
         * Paint a contents.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            // background
//            g.drawImage(topImg, 276, 113, c);
//            g.drawImage(midImg, 276, 193, 410, 124, c);
//            g.drawImage(bottomImg, 276, 317, c);
            g.setColor(fillRectColor);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(gapImg, 285, 181, c);
//            g.drawImage(highImg, 306, 143, c);
//            g.drawImage(shadowImg, 306, 364, 350, 79, c);

            g.setColor(titleColor);
            g.setFont(f24);
            GraphicUtil.drawStringCenter(g, myTitle, 480, 169);

            if (isFinished) {
                if (state == STATE_ON) {
                    g.drawImage(openImg, 441, 213, c);
                } else {
                    g.drawImage(closeImg, 422, 213, c);
                }
            }
            g.setColor(Color.white);
            g.setFont(f18);
            if (explain.length == 1) {
                GraphicUtil.drawStringCenter(g, explain[0], 483, 326);
            } else {
                GraphicUtil.drawStringCenter(g, explain[0], 483, 316);
                GraphicUtil.drawStringCenter(g, explain[1], 483, 336);
            }
        }

        /**
         * Get a bounds.
         * @param c parent UI.
         * @return bounds.
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }
    }
}

/**
 * @(#)MenuUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Point;
import java.rmi.RemoteException;
import java.util.Stack;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.MainCategory;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.effect.MenuMovingEffect;
import com.videotron.tvi.illico.upp.gui.MenuRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a menu tree for Settings' main.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class MenuUI extends BaseUI {
	/** Renderer. */
	private MenuRenderer renderer = new MenuRenderer();
	/** MenuInfo UI. */
	private MenuInfoUI menuInfoUI;
	/** Popup to change PIN. */
	private PopChangePinUI changePinUI = new PopChangePinUI();
	/** Effect to animate when select a menu. */
	private ClickingEffect clickEffect;
	/** Category for Settings's main. */
	private Category[] categories;
	/** Category for Application preferences. */
	private Category[] applicationPreferences;
	/** Stack for menu depth. */
	private Stack menuStack = new Stack();
	/** Indicate whether user is admin. */
	private boolean isAdmin;
	/** Indicate whether user already have a right to access. */
	private boolean alreadyAccess;
	/** Last explain text. */
	private String lastExplainStr;
	/** Effect for fade-in. */
	private AlphaEffect fadeInEffect;
	/** Effect animate that menu come as fade-in. */
	private MovingEffect leftInEffect;
	/** Effect animate that menu go out as fade-out. */
	private MovingEffect leftOutEffect;
	/** Effect animate that menu come as fade-in. */
	private MovingEffect rightInEffect;
	/** Effect animate that menu go out as fade-out. */
	private MovingEffect rightOutEffect;
	/** for animation to go to the 2nd depth. */
	private MovingEffect leftOutEffect2;
	/** for animation to come from the 2nd depth. */
	private MovingEffect leftInEffect2;
	/** Indicate a visible of MenuUI. */
	private boolean isVisible;
	/** Indicate a first time to enter a Setting's main. */
	private boolean isFirst;

	/**
	 * Set a renderer and create a effects.
	 */
	public MenuUI() {
		setRenderer(renderer);
		clickEffect = new ClickingEffect(this, 5);

		leftOutEffect = new MenuMovingEffect(this, 10, new Point(0, 0), new Point(-12, 0), MovingEffect.FADE_OUT,
				MovingEffect.GRAVITY);
		leftInEffect = new MenuMovingEffect(this, 10, new Point(-12, 0), new Point(0, 0), MovingEffect.FADE_IN,
				MovingEffect.GRAVITY);
		rightInEffect = new MenuMovingEffect(this, 10, new Point(12, 0), new Point(0, 0), MovingEffect.FADE_IN,
				MovingEffect.GRAVITY);
		rightOutEffect = new MenuMovingEffect(this, 10, new Point(0, 0), new Point(12, 0), MovingEffect.FADE_OUT,
				MovingEffect.GRAVITY);

		leftOutEffect2 = new MenuMovingEffect(this, 10, new Point(0, 0), new Point(-12, 0), MovingEffect.FADE_OUT,
				MovingEffect.GRAVITY);
		leftInEffect2 = new MenuMovingEffect(this, 10, new Point(-12, 0), new Point(0, 0), MovingEffect.FADE_IN,
				MovingEffect.GRAVITY);
	}

	/**
	 * Start a UI.
	 * 
	 * @param back
	 *            true if come from higher depth, false otherwise.
	 */
	public void start(boolean back) {
		if (!back) {
			clearHistory();
			prepare();
			loadCategoriesNames();
			focus = 0;
			isAdmin = false;
			isVisible = false;
			alreadyAccess = false;
			isFirst = true;
			setVisible(isVisible);

		} else {
			loadCategoriesNames();
			isVisible = false;
			setVisible(false);
			leftInEffect2.start();
		}

		if (menuStack.size() > 0) {
			addButton(BTN_BACK);
		}

		renderer.updateTitle();
		menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
	}
	
	public void init() {
		prepare();
		categories = DataManager.getInstance().getMainCategoryList();
		loadCategoriesNames();
	}

	/**
	 * Stop a UI.
	 */
	public void stop() {
		clearButtons();
		super.stop();
		isVisible = false;
	}

	/**
	 * Prepare a UI.
	 */
	public void prepare() {
		if (Log.DEBUG_ON) {
			Log.printDebug("MenuUI: prepare(): menuStack size : " + menuStack.size());
		}
		if (menuStack.size() == 0) {
			categories = DataManager.getInstance().getMainCategoryList();
			for (int i = 0; i < categories.length; i++) {
				if (categories[i].getCategoryId().equals("application.preferences")) {
					applicationPreferences = ((MainCategory) categories[i]).getSubcategories();
					break;
				}
			}
		} else {
			Object[] objs = (Object[]) menuStack.pop();

			categories = (Category[]) objs[0];
			focus = ((Integer) objs[1]).intValue();
		}
		super.prepare();
	}

	/**
	 * Handle a key code.
	 * 
	 * @param code
	 *            key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {

		if (isAnimating) {
			if (Log.DEBUG_ON) {
				Log.printDebug("MenuUI: handleKey(): isAnimating : " + isAnimating);
			}
			return true;
		}

		switch (code) {
		case OCRcEvent.VK_UP:
			if (categories != null) {
				if (--focus < 0) {
					focus = 0;
				}
				menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
				repaint();
			}
			return true;
		case OCRcEvent.VK_DOWN:
			if (categories != null) {
				if (++focus == categories.length) {
					focus = categories.length - 1;
				}
				menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
				repaint();
			}
			return true;
		case KeyCodes.LAST:
			if (menuStack.size() > 0) {
				clickButtons(BaseUI.BTN_BACK);
				startBackMenu();
			}
			return true;
		case OCRcEvent.VK_ENTER:
			clickEffect.start(49 - 52, 102 + 34 * focus - 71, 302, 46);
			keyOK();
			return true;
		default:
			return false;

		}
	}

	/**
	 * Handle a action for Ok key.
	 */
	private void keyOK() {

		if (categories[focus].getCategoryId().equals("general.settings")) {
			if (canAccessMenu()) {
				startGotoSecondDepth();
				addHistory(categories[focus]);
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_GENERAL_SETTINGS, false);
			} else {
				String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_GENERAL);
				showProtectedSettings(explain);
			}
		} else if (categories[focus].getCategoryId().equals("control.and.limits")) {

			if (alreadyAccess) {
				startNextMenu();
			} else {
				String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_MENU);
				showPinEnbler(explain);
			}
		} else if (categories[focus].getCategoryId().equals("application.preferences")
				|| categories[focus].getCategoryId().equals("accessibility")) {
			if (canAccessMenu()) {
				startNextMenu();
			} else {
				String explain;
				if (categories[focus].getCategoryId().equals("application.preferences")) {
					explain = getPopupText(KeyNames.EXPLAIN_ACCESS_APPLICATION);
				} else {
					explain = getPopupText(KeyNames.EXPLAIN_ACCESS_ACCESSIBILITY);
				}
				showProtectedSettings(explain);
			}

		} else if (categories[focus].getCategoryId().equals("equipment.setup")) {
			if (canAccessMenu()) {
				startGotoSecondDepth();
				addHistory(categories[focus]);
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, false);
			} else {
				String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_EQUIPMENT);
				showProtectedSettings(explain);
			}
		} else if (categories[focus].getCategoryId().equals("shortcut.menu")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_SHORTCUT_MENU, false);
		} else if (categories[focus].getCategoryId().equals("program.guide")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_PROGRAM_GUIDE, false);
		} else if (categories[focus].getCategoryId().equals("pvr")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_PVR, false);
		} else if (categories[focus].getCategoryId().equals("vod")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_VOD, false);
		} else if (categories[focus].getCategoryId().equals("audio.accessibility")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_AUDIO_ACCESSIBILITY, false);
		} else if (categories[focus].getCategoryId().equals("cc")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_CC, false);
		} else if (categories[focus].getCategoryId().equals("caller.id")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_CALLER_ID, false);
		} else if (categories[focus].getCategoryId().equals("modify.parental.control")) {
			startGotoSecondDepth();
			addHistory(categories[focus]);
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_PARENTAL_CONTROLS, false);
		} else if (categories[focus].getCategoryId().equals("modify.admin.pin.code")) {
			((BaseUI) getParent()).setPopup(changePinUI);
			changePinUI.show(this, changePinUI.STATE_MODIFY_ADMIN_PIN, User.ID_ADMIN);
			getParent().repaint();
		} else if (categories[focus].getCategoryId().equals("family.pin.code")) {
			User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
			((BaseUI) getParent()).setPopup(changePinUI);

			if (family.hasPin()) {
				changePinUI.show(this, changePinUI.STATE_MODIFY_FAMILY_PIN, User.ID_FAMILY);
			} else {
				changePinUI.show(this, changePinUI.STATE_CREATE_FAMILY_PIN, User.ID_FAMILY);
			}
			getParent().repaint();
		}
	}

	/**
	 * Start a effect for fade-in.
	 */
	public void startFadeInEffect() {
		if (fadeInEffect == null) {
			fadeInEffect = new AlphaEffect(this, 15, MovingEffect.FADE_IN);
		}
		fadeInEffect.start();
	}

	/**
	 * Start a animation to enter a menu items in second depth.
	 */
	private void startNextMenu() {
		updateApplicationPreferences();
		isVisible = false;
		setVisible(isVisible);
		leftOutEffect.start();
	}

	/**
	 * Start a animation to back a menu item in first depth.
	 */
	private void startBackMenu() {
		isVisible = false;
		setVisible(isVisible);
		rightOutEffect.start();
	}

	/**
	 * Start a animation to enter a detail.
	 */
	private void startGotoSecondDepth() {
		isVisible = false;
		setVisible(false);
		leftOutEffect2.start();
	}

	/**
	 * Determine a visible.
	 * 
	 * @return true if menu UI is showing, false otherwise.
	 */
	public boolean isVisible() {
		return isVisible;
	}

	/**
	 * Show a PinEnabler to have a explain which need a admin PIN.
	 * 
	 * @param explain
	 */
	public void showProtectedSettings(String explain) {
		// lastExplainStr = explain;
		// PinEnablerController.getInstance().addPinEnablerListener(new
		// PinEnablerListenerImpl2());
		// String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
		// PinEnablerController.getInstance().showPinEnabler(title,
		// TextUtil.split(explain, renderer.fm18, 310));

		lastExplainStr = explain;
		try {
			PreferenceController.getInstance().checkRightFilter(new RightFilterListenerImpl2(), "",
					new String[] { RightFilter.PROTECT_SETTINGS_MODIFICATIONS }, null, null);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Check whether can access menu or not. if not, user should input a admin
	 * PIN.
	 */
	public void checkAccessMenu() {
		String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
		DataCenter.getInstance().remove("PARAMS");
		if (params != null && params.length > 0 && params[0].equals(MonitorService.REQUEST_APPLICATION_HOT_KEY)) {
			if (!canAccessMenu()) {
				String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_GENERAL);
				showProtectedSettings(explain);
			}
		} else {
			Preference control = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
					.get(RightFilter.PARENTAL_CONTROL);
			Preference protect = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
					.get(RightFilter.PROTECT_SETTINGS_MODIFICATIONS);
			String controlValue = control.getCurrentValue();
			String protectValue = protect.getCurrentValue();

			// if enter from other application and protect access to settings is
			// Yes, it means that enter from other
			// application by admin.
			if (controlValue.equals(Definitions.OPTION_VALUE_ON) && protectValue.equals(Definitions.OPTION_VALUE_YES)) {
				isAdmin = true;
				alreadyAccess = true;
				menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
			}
		}
	}

	/**
	 * Handle a action after animation ended.
	 * 
	 * @param effect
	 *            effect to end.
	 */
	public void animationEnded(Effect effect) {
		super.animationEnded(effect);
		if (effect.equals(fadeInEffect)) {
			isVisible = true;
			setVisible(isVisible);
			menuInfoUI.startEffect();
		} else if (effect.equals(leftOutEffect)) {
			addHistory(categories[focus]);
			pushCurrentMenuData();
			categories = ((MainCategory) categories[focus]).getSubcategories();
			loadCategoriesNames();
			focus = 0;
			renderer.updateTitle();
			menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);

			rightInEffect.start();
		} else if (effect.equals(rightOutEffect)) {
			Object[] objs = (Object[]) popPrevMenuData();
			categories = (Category[]) objs[0];
			loadCategoriesNames();

			focus = ((Integer) objs[1]).intValue();

			if (categories[focus].getCategoryId().equals("control.and.limits") && !alreadyAccess) {
				isAdmin = false;
			}

			menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
			popHistory();
			renderer.updateTitle();

			leftInEffect.start();
		} else if (effect.equals(rightInEffect) || effect.equals(leftInEffect)) {
			isVisible = true;
			setVisible(isVisible);
		} else if (effect.equals(leftInEffect2)) {
			isVisible = true;
			setVisible(isVisible);
		}
	}

	/**
	 * Update a items of application preferences.
	 */
	private void updateApplicationPreferences() {
		if (categories[focus].getCategoryId().equals("application.preferences")) {

			Vector itemVec = new Vector();

			for (int i = 0; i < applicationPreferences.length; i++) {
				String appName = DataCenter.getInstance().getString(applicationPreferences[i].getCategoryId());
				if (hasRightForApp(appName)) {
					itemVec.add(applicationPreferences[i]);
				}
			}

			SubCategory[] subCategories = new SubCategory[itemVec.size()];
			itemVec.copyInto(subCategories);
			((MainCategory) categories[focus]).setSubCategories(subCategories);
		}
	}

	/**
	 * Validate that current STB is subscribed for application.
	 * 
	 * @param appName
	 *            target application name.
	 * @return true if is subscribed, false otherwise.
	 */
	private boolean hasRightForApp(String appName) {
		if (Log.INFO_ON) {
			Log.printInfo("MenuUI: hasRightForApp()");
		}
		MonitorService mService = CommunicationManager.getInstance().getMonitorService();

		if (Log.DEBUG_ON) {
			Log.printDebug("MenuUI: hasRightForApp(): appName = " + appName);
		}

		if (appName == null || appName.equals(TextUtil.EMPTY_STRING)) {
			return true;
		}

		// test code
		// if (appName.equals("VOD")) {
		// return false;
		// }

		if (mService != null) {
			try {
				int auth = mService.checkAppAuthorization(appName);

				if (Log.DEBUG_ON) {
					Log.printDebug("DataManager: hasRightForApp(): auth = " + auth);
				}

				if (auth != MonitorService.NOT_AUTHORIZED) {
					return true;
				}
			} catch (RemoteException e) {
				Log.printWarning(e);
			}
		}
		return false;
	}

	/**
	 * if User protect settings modification, general settings. need a PIN if
	 * want to enter a one of all Settings menu.
	 * 
	 * @return true if Protect settings modification is No, false otherwise.
	 */
	private boolean canAccessMenu() {
		if (Log.INFO_ON) {
			Log.printInfo("MenuUI: canAccessMenu()");
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("MenuUI: canAccessMenu(): alreadyAccess = " + alreadyAccess);
		}

		if (alreadyAccess) {
			return true;
		}

		Preference control = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.PARENTAL_CONTROL);
		Preference protect = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters()
				.get(RightFilter.PROTECT_SETTINGS_MODIFICATIONS);
		String controlValue = control.getCurrentValue();
		String protectValue = protect.getCurrentValue();

		if (controlValue.equals(Definitions.OPTION_VALUE_ON) && protectValue.equals(Definitions.OPTION_VALUE_YES)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Show PinEnabler with explain.
	 * 
	 * @param explain
	 *            why need a PIN.
	 */
	private void showPinEnbler(String explain) {
		// lastExplainStr = explain;
		// PinEnablerController.getInstance().addPinEnablerListener(new
		// PinEnablerListenerImpl());
		// String title = getPopupText(KeyNames.TITLE_ENTER_PIN);
		// PinEnablerController.getInstance().showPinEnabler(title,
		// TextUtil.split(explain, renderer.fm18, 310));

		try {
			PreferenceController.getInstance().checkRightFilter(new RightFilterListenerImpl(), "",
					new String[] { "control.and.limits" }, null, null);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Load a category names.
	 */
	private void loadCategoriesNames() {
		for (int i = 0; i < categories.length; i++) {
			String name = dataCenter.getString("menu." + categories[i].getCategoryId());
			if (!name.equals(TextUtil.EMPTY_STRING)) {
				if (categories[i].getCategoryId().equals("family.pin.code")) {
					User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
					if (family.hasPin()) {
						String prefix = dataCenter.getString("menu.modify");
						categories[i].setName(prefix + name);
					} else {
						String prefix = dataCenter.getString("menu.create");
						categories[i].setName(prefix + name);
					}
				} else {
					categories[i].setName(name);
				}
			}
		}
	}

	/**
	 * Get a categories of menu.
	 * 
	 * @return current categories.
	 */
	public Category[] getCategories() {
		return categories;
	}

	/**
	 * Set a menu info UI.
	 * 
	 * @param ui
	 *            MenuInfoUI
	 */
	public void setMenuInfoUI(MenuInfoUI ui) {
		menuInfoUI = ui;
	}

	/**
	 * Push a current menu data in stack.
	 */
	public void pushCurrentMenuData() {
		Object[] objs = new Object[2];
		objs[0] = categories;
		objs[1] = new Integer(focus);

		menuStack.push(objs);

		if (menuStack.size() > 0) {
			clearButtons();
			addButton(BTN_BACK);
		} else {
			clearButtons();
		}
	}

	/**
	 * Pop a last menu data in stack.
	 * 
	 * @return last menu data.
	 */
	public Object popPrevMenuData() {
		Object obj = menuStack.pop();
		if (menuStack.size() > 0) {
			clearButtons();
			addButton(BTN_BACK);
		} else {
			clearButtons();
		}
		return obj;
	}

	/**
	 * Get a size of menu stack.
	 * 
	 * @return a size of menu stack.
	 */
	public int getMenuDataSize() {
		return menuStack.size();
	}

	/**
	 * Handle a action after popup closed.
	 * 
	 * @param pop
	 *            popup to close.
	 * @param msg
	 *            message from popup.
	 */
	public void popupClosed(BaseUI pop, Object msg) {

		if (pop.equals(changePinUI)) {
			((BaseUI) getParent()).removePopup(changePinUI);
			int action = ((Integer) msg).intValue();

			if (action == ACTION_OK) {
				((BaseUI) getParent()).setPopup(notiUI);
				String title, explain;
				String userId = changePinUI.getTargetUserId();
				if (userId.equals(User.ID_ADMIN)) {
					title = getPopupText(KeyNames.TITLE_NEW_PIN_ACCEPTED);
					explain = getPopupText(KeyNames.EXPLAIN_ADMIN_PIN_ACCEPTED);
				} else if (userId.equals(User.ID_FAMILY)) {
					title = getPopupText(KeyNames.TITLE_NEW_PIN_ACCEPTED);
					if (changePinUI.getState() == changePinUI.STATE_CREATE_FAMILY_PIN) {
						explain = getPopupText(KeyNames.EXPLAIN_FAMILY_NEW_PIN_ACCEPTED);
					} else {
						explain = getPopupText(KeyNames.EXPLAIN_FAMILY_PIN_ACCEPTED);
					}
					loadCategoriesNames();
				} else {
					title = getPopupText(KeyNames.TITLE_NEW_PIN_ACCEPTED);
					explain = getPopupText(KeyNames.EXPLAIN_NEW_PIN_ACCEPTED);
				}
				notiUI.show(this, title, TextUtil.split(explain, renderer.fm18, 310), PopBaseRenderer.ICON_TYPE_NONE);

			}
			renderer.prepare(this);
			getParent().repaint();
		} else if (pop.equals(notiUI)) {
			((BaseUI) getParent()).removePopup(notiUI);
			getParent().repaint();
		}
	}

	class RightFilterListenerImpl implements RightFilterListener {

		public void receiveCheckRightFilter(int response) throws RemoteException {
			// TODO Auto-generated method stub
			if (response == PreferenceService.RESPONSE_SUCCESS) {
				isAdmin = true;
				startNextMenu();
			}
		}

		public String[] getPinEnablerExplain() throws RemoteException {
			String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_MENU);
			String[] explainArray = TextUtil.split(explain, renderer.fm18, 310);
			return explainArray;
		}

	}

	class RightFilterListenerImpl2 implements RightFilterListener {

		public void receiveCheckRightFilter(int response) throws RemoteException {
			if (response == PreferenceService.RESPONSE_SUCCESS) {
				isAdmin = true;
				alreadyAccess = true;
				if (!isFirst) {
					isFirst = false;
					if (categories[focus].getCategoryId().equals("general.settings")) {
						startGotoSecondDepth();
						addHistory(categories[focus]);
						PreferenceController.getInstance().showUIComponent(BaseUI.UI_GENERAL_SETTINGS, false);
					} else if (categories[focus].getCategoryId().equals("application.preferences")
							|| categories[focus].getCategoryId().equals("accessibility")) {
						startNextMenu();
					} else if (categories[focus].getCategoryId().equals("equipment.setup")) {
						startGotoSecondDepth();
						addHistory(categories[focus]);
						PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, false);
					}
				} else {
					menuInfoUI.update(categories[focus].getCategoryId(), categories[focus].getName(), isAdmin);
				}
				repaint();
			} else if (response == PreferenceService.RESPONSE_CANCEL) {
				if (FrameworkMain.getInstance().getHScene().isShowing()) {
					PreferenceController.getInstance().processKeyEvent(OCRcEvent.VK_EXIT);
				}
			}
		}

		public String[] getPinEnablerExplain() throws RemoteException {
			String explain = lastExplainStr;
			String[] explainArray = TextUtil.split(explain, renderer.fm18, 310);
			return explainArray;
		}

	}
}

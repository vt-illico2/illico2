/**
 * @(#)ClosedCaptioningUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.util.Hashtable;

import org.ocap.media.ClosedCaptioningAttribute;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.ClosedCaptioningRenderer;
import com.videotron.tvi.illico.upp.util.ClosedCaptioningUtil;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class handle a preferences for Closed Captioning.
 * @author Woojung Kim
 * @version 1.1
 */
public class ClosedCaptioningUI extends BaseUI {

    /** A Renderer. */
    private ClosedCaptioningRenderer renderer = new ClosedCaptioningRenderer();
    /** A explain ui to display a explain text on bottom. */
    private ExplainUI explainUI = new ExplainUI();
    /** A select popup. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();

    // public final String[] preferenceIds = {PreferenceNames.CC_DISPLAY, PreferenceNames.CC_STYLE,
    // PreferenceNames.CC_CHARACTERS_SIZE, PreferenceNames.CC_BACKGROUND_SHADINGS,
    // PreferenceNames.CC_ANALOG_SOURCE, PreferenceNames.CC_DIGITAL_SOURCE};
    //
    /** A preferences ids for Closed Captioning. */
    public final String[] preferenceIds = {PreferenceNames.CC_DISPLAY, PreferenceNames.CC_STYLE,
            PreferenceNames.CC_BACKGROUND_SHADINGS, PreferenceNames.CC_ANALOG_SOURCE, PreferenceNames.CC_DIGITAL_SOURCE};
    /** A preferences ids to need a initialization when CC_STYLE is changed. */
    public final String[] initilizeList = {PreferenceNames.CC_CHARACTERS_SIZE, PreferenceNames.CC_BACKGROUND_SHADINGS};
    /** A constant array for CC_ATTRIBUTE. */
    public final int[] attributeList = {ClosedCaptioningAttribute.CC_ATTRIBUTE_PEN_SIZE,
            ClosedCaptioningAttribute.CC_ATTRIBUTE_WINDOW_FILL_OPACITY};
    /** A Hashtable to have a preferences from {@link DataManager}. */
    private Hashtable preTable;
    /** Indicate whether CC_STYLE is Set by Viewer or not. */
    private boolean isSetByViewer;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer and add a explainUI.
     */
    public ClosedCaptioningUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        Category gsCategory = (Category) BaseUI.peekHistory();

        preTable = DataManager.getInstance().getPreferences(gsCategory.getCategoryId());

        Preference pre = (Preference) preTable.get(PreferenceNames.CC_STYLE);

        if (pre.getCurrentValue().equals(Definitions.CC_STYLE_SET_BY_VIEWER)) {
            isSetByViewer = true;
        } else {
            isSetByViewer = false;
        }
        super.prepare();
    }

    /**
     * Start a UI.
     */
    public void start(boolean back) {

        prepare();
        addButton(BTN_BACK);
        focus = 0;

        setExplainText();

        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        super.stop();
    }

    /**
     * Get a Hashtable to have a preferences.
     * @return Hastable.
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Return a result to determine a CC_STYLE.
     * @return true if CC_STYLE is Set by Viewer, false otherwise.
     */
    public boolean isSetByViewer() {
        return isSetByViewer;
    }

    /**
     * Return whether can select a menu or not.
     * @return true if CC_DISPLAY is On, false otherwise.
     */
    public boolean canSelectMenu() {
        Preference pre = (Preference) preTable.get(PreferenceNames.CC_DISPLAY);

        if (!pre.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Initialize a {@link #initilizeList}.
     * @param init true if initialize a default, false otherwise.
     */
    private void initializeValue(boolean init) {
        if (Log.INFO_ON) {
            Log.printInfo("ClosedCaptioningUI: initializedValue: init = " + init);
        }
        for (int i = 0; i < initilizeList.length; i++) {
            Preference pre = (Preference) preTable.get(initilizeList[i]);

            if (init) {
                ClosedCaptioningUtil.getInstance().updateCCAttribute(attributeList[i], pre.getName(), 0);
            } else {
                String currentValue = pre.getCurrentValue();
                String[] values = pre.getSelectableValues();

                int j;
                for (j = 0; j < values.length; j++) {
                    if (values[j].equals(currentValue)) {
                        break;
                    }
                }

                ClosedCaptioningUtil.getInstance().updateCCAttribute(attributeList[i], pre.getName(), j);
            }
        }

        // VDTRMASTER-5804
        if (init) {
            PreferenceActionController.getInstance().resetClosedCaptioningControl();
        } else {
            Preference pre = (Preference) preTable.get(PreferenceNames.CC_ANALOG_SOURCE);
            PreferenceActionController.getInstance().doAction(PreferenceNames.CC_ANALOG_SOURCE, pre.getCurrentValue());
        }
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (canSelectMenu()) {
                if (--focus < 0) {
                    focus = 0;
                }
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (canSelectMenu()) {
                if (isSetByViewer) {
                    if (++focus > 4) {
                        focus = 4;
                    }
                } else {
                    if (++focus > 1) {
                        focus = 1;
                    }
                }
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            Preference pre = (Preference) preTable.get(preferenceIds[focus]);
            if (focus < 1) {
                String activeStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_ON + "3");
                String deactiveStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_OFF + "3");
                String muteStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_ON_MUTE);
                String focusStr;
                if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ON)
                        || pre.getCurrentValue().equals(Definitions.OPTION_VALUE_OFF)) {
                    focusStr = BaseUI.getOptionText(pre.getCurrentValue() + "3");
                } else {
                    focusStr = BaseUI.getOptionText(pre.getCurrentValue());
                }
                clickEffect.start(BaseRenderer.INPUT_X, 127 + focus * 37, 211, 33);
                setPopup(selectUI);
                selectUI.show(this, new String[] {activeStr, deactiveStr, muteStr}, focusStr, pre.getName(), 519,
                        69 + focus * 37, 3);
            } else {
                clickEffect.start(BaseRenderer.INPUT_X, 127 + focus * 37 + 14, 211, 33);
                setPopup(selectUI);
                selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
                        69 + focus * 37 + 14, 3);
            }
            repaint();
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
            return true;
        default:
            return false;
        }
    }

    /**
     * Set a explain text for focus.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Handle a action after closed popup.
     * @param pop popup to be closed.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        if (pop.equals(selectUI)) {
            int action = ((Integer) msg).intValue();
            int selectedFocus = 0;
            String selectedValue;

            removePopup(selectUI);
            selectedValue = selectUI.getSelectedValue();
            selectedFocus = selectUI.getFocus();

            if (action == BaseUI.ACTION_OK) {
                int idx = focus;

                Preference pre = (Preference) preTable.get(preferenceIds[idx]);
                if (focus == 0) {
                    if (selectedFocus == 0) {
                        selectedValue = Definitions.OPTION_VALUE_ON;
                    } else if (selectedFocus == 1) {
                        selectedValue = Definitions.OPTION_VALUE_OFF;
                    } else {
                        selectedValue = Definitions.OPTION_VALUE_ON_MUTE;
                    }
                }

                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);

                if (idx == 1) {
                    if (selectedValue.equals(Definitions.CC_STYLE_SET_BY_VIEWER)) {
                        initializeValue(false);
                        isSetByViewer = true;
                    } else {
                        initializeValue(true);
                        isSetByViewer = false;
                    }
                } else if (idx == 2) {
                    ClosedCaptioningUtil.getInstance().updateCCAttribute(
                            ClosedCaptioningAttribute.CC_ATTRIBUTE_WINDOW_FILL_OPACITY, pre.getName(), selectedFocus);
                } else {
                    PreferenceActionController.getInstance().doAction(pre.getID(), selectedValue);
                }
            }
            repaint();
        }
    }

}

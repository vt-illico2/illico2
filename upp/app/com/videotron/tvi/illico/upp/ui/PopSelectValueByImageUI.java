/**
 * @(#)PopSelectValueByImageUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class represent a values for pip size and pip postion and remote control type by images.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectValueByImageUI extends BaseUI {

    /** State to indicate that can select a preference of PIP size. */
    public final int STATE_PIP_SIZE = 0;
    /** State to indicate that can select a preference of PIP position. */
    public final int STATE_PIP_POSITION = 1;
    /** State to indicate that can select a preference of Remote Control type. */
    public final int STATE_REMOTE_CONTROL_TYPE = 2;

    /** Renderer. */
    private PopSelectValueRenderer renderer = new PopSelectValueRenderer();
    /** Parent UI. */
    private BaseUI parent;
    /** Values that User. */
    private String[] values;
    /** Title */
    private String title;

    /** Item index on Top. */
    private int topIdx = 0;
    /** Item index on Bottom . */
    private int bottomIdx = 0;
    /** X coordinate to move. */
    private int moveX = 0;
    /** Y coordinate to move. */
    private int moveY = 0;

    /**
     * Set a renderer.
     */
    public PopSelectValueByImageUI() {
        setRenderer(renderer);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     * @param data String array for data.
     * @param current current value.
     * @param state One of {@link #STATE_PIP_POSITION} or {@link #STATE_PIP_SIZE}.
     * @param x X coordinate to move.
     * @param y Y coordinate to move.
     */
    public void show(BaseUI com, Object data, Object current, int state, int x, int y) {
        this.moveX = x;
        this.moveY = y;
        this.state = state;

        this.parent = com;
        values = (String[]) data;

        focus = findFocus(values, (String) current);
        calculateIndex();
        prepare();
        setVisible(true);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     * @param data String array for data.
     * @param current current value.
     * @param title title to be displayed on top
     * @param state only {@link #STATE_REMOTE_CONTROL_TYPE}
     * @param x X coordinate to move.
     * @param y Y coordinate to move.
     */
    public void show(BaseUI com, Object data, Object current, String title, int state, int x, int y) {
        this.moveX = x;
        this.moveY = y;
        this.state = state;
        this.title = title;
        this.parent = com;
        values = (String[]) data;

        focus = findFocus(values, (String) current);
        calculateIndex();

        prepare();
        setVisible(true);
    }

    /**
     * Calculate a index.
     */
    private void calculateIndex() {
        topIdx = (values.length - 1 + focus) % values.length;
        bottomIdx = (focus + 1) % values.length;
    }

    /**
     * Find a focus.
     * @param values values array
     * @param current current value.
     * @return index of focus.
     */
    private int findFocus(String[] values, String current) {
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(current)) {
                return i;
            }
        }

        return 0;
    }

    /**
     * Get a selected value.
     * @return selected value.
     */
    public String getSelectedValue() {
        return values[focus];
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_UP:
            if (state == STATE_REMOTE_CONTROL_TYPE) {
            } else {
                if (--focus < 0) {
                    focus = values.length - 1;
                }
                calculateIndex();
            }
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (state == STATE_REMOTE_CONTROL_TYPE) {
            } else {
                if (++focus == values.length) {
                    focus = 0;
                }
                calculateIndex();
            }
            repaint();
            return true;
        case OCRcEvent.VK_LEFT:
            if (state == STATE_REMOTE_CONTROL_TYPE) {
                focus = 0;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (state == STATE_REMOTE_CONTROL_TYPE) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            parent.popupClosed(this, new Integer(ACTION_OK));
            return true;
            // case KeyCodes.LAST:
            // parent.popupClosed(this, new Integer(ACTION_CANCEL));
            // return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * This class render a preferences by images.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopSelectValueRenderer extends BaseRenderer {
        /** Image for middle part of background. */
        Image bgMidImg;
        /** Image for top part of background. */
        Image bgTopImg;
        /** Image for bottom part of background. */
        Image bgBottomImg;
        /** Image for focus. */
        Image focusImg;
        /** Image for top part of shadow. */
//        Image shadowTopImg;
        /** Image for bottom part of shadow. */
//        Image shadowBottomImg;
        /** Image for up arrow. */
        Image arrowUpImg;
        /** Image for down arrow. */
        Image arrowDownImg;
        /** Image for top part of secondly background. */
//        Image bgTop2Img;
        /** Image for middle part of secondly background. */
//        Image bgMid2Img;
        /** Image for bottom part of secondly background. */
//        Image bgBottom2Img;
        /** Image for shadow. */
//        Image shadowImg;
        /** Image for gap. */
        Image gapImg;
        /** Image for highlight. */
//        Image highImg;
        /** Image for button to have a focus. */
        Image btnFocusImg;
        /** Image for dimmed button.*/
        Image btnDimImg;
        /** Image for focus of remote control. */
        Image remoteFocusImg;
        /** Image for dimmed focus of remote control. */
        Image remoteFocusDimImg;
        /** Image for type A of remote control .*/
        Image remoteTypeAImg;
        /** Image for tyep B of remote control . */
        Image remoteTypeBImg;
        /** Image array for PIP size. */
        Image[] pipSizeImg;
        /** Image array for PIP size to be dimmed. */
        Image[] pipSizeDimImg;
        /** Image array for PIP position to have a focus. */
        Image[] pipPosFcousImg;
        /** Image array for PIP postion to be dimmed. */
        Image[] pipPosImg;
        /** Color for focused contents (3, 3, 3). */
        Color focusColor = new Color(3, 3, 3);
        /** Color for dimmed contents (130, 130, 130). */
        Color dimmedColor = new Color(130, 130, 130);
        /** Color for dimmed back (0, 0, 0, 153). */
        DVBColor dimmedBackColor = new DVBColor(0, 0, 0, 153);
        /** Color for background (35, 35, 35). */
        Color bgColor = new Color(35, 35, 35);
        /** Color for title (252, 202, 4). */
        Color titleColor = new Color(251, 217, 89);
        /** Color for dimmed type (180, 180, 180). */
        Color typeDimColor = new Color(180, 180, 180);
        /** Color for dimmed remote (44, 44, 44, 153). */
        DVBColor remoteDimColor = new DVBColor(44, 44, 44, 153);
        /** Font of size 24. */
        Font f24 = FontResource.BLENDER.getFont(24);
        /** Subtitle. */
        String subTitle;
        /** Name of OK button. */
        String btnOK;
        /** Name of Cancle button. */
        String btnCancel;

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
            btnOK = getPopupText(KeyNames.BTN_OK);
            btnCancel = getPopupText(KeyNames.BTN_CANCEL);
            subTitle = getPopupText(KeyNames.EXPLAIN_REMOTE_CONTROL);
            
            bgMidImg = BaseUI.getImage("08_op2_bg_m2.png");
            bgTopImg = BaseUI.getImage("08_op2_bg_t.png");
            bgBottomImg = BaseUI.getImage("08_op2_bg_b.png");
            focusImg = BaseUI.getImage("08_op2_foc.png");
//            shadowTopImg = BaseUI.getImage("08_op2_sh_t.png");
//            shadowBottomImg = BaseUI.getImage("08_op2_sh_b.png");
            arrowUpImg = BaseUI.getImage("02_ars_t.png");
            arrowDownImg = BaseUI.getImage("02_ars_b.png");
//            bgTop2Img = BaseUI.getImage("05_pop_glow_t.png");
//            bgMid2Img = BaseUI.getImage("05_pop_glow_m.png");
//            bgBottom2Img = BaseUI.getImage("05_pop_glow_b.png");
//            shadowImg = BaseUI.getImage("pop_sha.png");
            gapImg = BaseUI.getImage("pop_gap_379.png");
//            highImg = BaseUI.getImage("pop_high_350.png");
            btnFocusImg = BaseUI.getImage("05_focus.png");
            btnDimImg = BaseUI.getImage("05_focus_dim.png");
            remoteFocusImg = BaseUI.getImage("08_remote_foc.png");
            remoteFocusDimImg = BaseUI.getImage("08_remote_foc_dim.png");
            remoteTypeAImg = BaseUI.getImage("08_remote_a.png");
            remoteTypeBImg = BaseUI.getImage("08_remote_b.png");
            
            if (pipSizeImg == null) {
                pipSizeImg = new Image[2];
            }
            pipSizeImg[0] = BaseUI.getImage("08_pip_size01.png");
            pipSizeImg[1] = BaseUI.getImage("08_pip_size02.png");
            
            if (pipSizeDimImg == null) {
                pipSizeDimImg = new Image[2];
            }
            pipSizeDimImg[0] = BaseUI.getImage("08_pip_size01_dim.png");
            pipSizeDimImg[1] = BaseUI.getImage("08_pip_size02_dim.png");
            
            if (pipPosFcousImg == null) {
                pipPosFcousImg = new Image[4];
            }
            pipPosFcousImg[0] = BaseUI.getImage("08_pip_pos01.png");
            pipPosFcousImg[1] = BaseUI.getImage("08_pip_pos02.png");
            pipPosFcousImg[2] = BaseUI.getImage("08_pip_pos03.png");
            pipPosFcousImg[3] = BaseUI.getImage("08_pip_pos04.png");
            
            if (pipPosImg == null) {
                pipPosImg = new Image[4];
            } 
            pipPosImg[0] = BaseUI.getImage("08_pip_pos01_dim.png");
            pipPosImg[1] = BaseUI.getImage("08_pip_pos02_dim.png");
            pipPosImg[2] = BaseUI.getImage("08_pip_pos03_dim.png");
            pipPosImg[3] = BaseUI.getImage("08_pip_pos04_dim.png");
            
            super.prepare(c);
        }

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedBackColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            parent.paintSelectedContent(g, parent);

            if (state != STATE_REMOTE_CONTROL_TYPE) {
                g.translate(moveX, moveY);
                // 480, 219 - 148
                g.drawImage(bgTopImg, 0, 15, c);
                g.drawImage(bgMidImg, 0, 30, 99, 113, c);
                g.drawImage(bgBottomImg, 0, 143, c);

                // g.fillRect(4, 17, 91, 139);
                Rectangle origin = g.getClipBounds();
                g.clipRect(2, 17, 91, 139);

                if (state == STATE_PIP_SIZE) {
                    g.drawImage(pipSizeDimImg[topIdx], 9, 0, c);
                    g.drawImage(pipSizeDimImg[bottomIdx], 9, 124, c);
                } else {
                    g.drawImage(pipPosImg[topIdx], 9, 0, c);
                    g.drawImage(pipPosImg[bottomIdx], 9, 124, c);
                }
                g.setClip(origin);
//                g.drawImage(shadowTopImg, 4, 17, c);
//                g.drawImage(shadowBottomImg, 4, 129, c);
                g.drawImage(arrowUpImg, 38, 0, c);
                g.drawImage(arrowDownImg, 38, 156, c);
                g.drawImage(focusImg, 2, 56, c);

                if (state == STATE_PIP_SIZE) {
                    g.drawImage(pipSizeImg[focus], 9, 63, c);
                } else {
                    g.drawImage(pipPosFcousImg[focus], 9, 63, c);
                }
                g.translate(-(moveX), -(moveY));

            } else { // TYPE_REMOTE_CONTROL_TYPE
                g.setColor(bgColor);
                g.fillRect(513, 87, 350, 345);
//                g.drawImage(bgTop2Img, 483, 57, c);
//                g.drawImage(bgMid2Img, 483, 137, 410, 245, c);
//                g.drawImage(bgBottom2Img, 483, 382, c);
//                g.drawImage(shadowImg, 513, 429, 350, 60, c);
                g.drawImage(gapImg, 492, 125, c);
//                g.drawImage(highImg, 513, 87, c);

                g.setFont(f24);
                g.setColor(titleColor);
                GraphicUtil.drawStringCenter(g, title, 685, 113);
                g.setColor(Color.WHITE);
                g.setFont(f18);
                GraphicUtil.drawStringCenter(g, subTitle, 688, 154);

                g.drawImage(remoteTypeAImg, 520, 190, c);
                g.drawImage(remoteTypeBImg, 684, 190, c);

                g.drawImage(remoteFocusImg, 529 + focus * 164, 190, c);

                g.setColor(remoteDimColor);
                if (focus == 0) {
                    g.fillRect(694, 191, 152, 225);
                } else {
                    g.fillRect(530, 191, 152, 225);
                }

                for (int i = 0; i < 2; i++) {
                    if (focus == i) {
                        g.setColor(titleColor);
                    } else {
                        g.setColor(typeDimColor);
                    }
                    GraphicUtil.drawStringCenter(g, values[i], 607 + i * 166, 182);
                }
            }
        }
    }
}

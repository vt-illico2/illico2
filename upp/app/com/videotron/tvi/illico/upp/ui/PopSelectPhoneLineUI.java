/**
 * @(#)PopSelectValueUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a UI which display to select a phone line for caller Id.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectPhoneLineUI extends BaseUI {
    /** State to indicate that list has a focus. */
    private static final int STATE_LIST = 0;
    /** State to indicate that button has a focus. */
    private static final int STATE_BUTTON = 1;
    /** Renderer. */
    private PopSelectPhoneLineRenderer renderer = new PopSelectPhoneLineRenderer();
    /** Parent UI. */
    private BaseUI parent;
    /** Explain text. */
    private String explain;
    /** X coordinate to move. */
    private int moveX = 0;
    /** Y coordinate to move. */
    private int moveY = 0;
    /** line array. */
    private String[] lines;
    /** Indicate to be selected. */
    private boolean[] selects;
    /** Focus index of button. */
    private int btnFocus = 0;
    /** start index to be drawn at first line. */
    private int startIdx;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer.
     */
    public PopSelectPhoneLineUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Show a popup.
     * @param parent parent UI.
     * @param data data to be stored.
     * @param x x coordinate to move.
     * @param y y coordinate to move.
     */
    public void show(BaseUI parent, String data, int x, int y) {
        CallerIDUI ui = (CallerIDUI) parent;
        prepare();
        this.moveX = x;
        this.moveY = y;

        btnFocus = 0;
        startIdx = 0;
        state = STATE_LIST;
        this.parent = ui;

        Log.printDebug("PopSelectPhoneLineUI: show(): data = " + data);

        lines = ui.getLines();
        selects = new boolean[lines.length];

        focus = 0;

        if (data != null && !data.equals(TextUtil.EMPTY_STRING)) {
            for (int i = 0; i < lines.length; i++) {
                if (data.indexOf(lines[i]) > -1) {
                    selects[i] = true;
                }

//                if (data.equals(lines[i])) {
//                    focus = i;
//                }
            }
        }

        Log.printDebug("PopSelectDayUI: show(): lines length : " + lines.length);
        setVisible(true);
    }

    /**
     * Prepare a popup.
     */
    public void prepare() {
        super.prepare();
    }

    /**
     * Get a selected values.
     * @return data to be formated for save.
     */
    public String getSelectedValue() {
        StringBuffer sf = new StringBuffer();
        boolean isFirst = true;
        for (int i = 0; i < selects.length; i++) {
            if (selects[i]) {
                if (!isFirst) {
                    sf.append(PreferenceService.PREFERENCE_DELIMETER);
                } else {
                    isFirst = false;
                }
                sf.append(lines[i]);
            }
        }

        // if (sf.length() == 0) {
        // sf.append(lines[0]);
        // }

        if (Log.DEBUG_ON) {
            Log.printDebug("PopSeelctPhoneLineUI: getSelectedValue: value = " + sf.toString());
        }

        return sf.toString();
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_UP:
            if (state == STATE_LIST) {
                if (startIdx + focus > 0) {
                    if (focus > 3) {
                        focus--;
                    } else if (startIdx > 0) {
                        if (focus < 3) {
                            startIdx--;
                        } else {
                            focus--;
                        }
                    } else {
                        focus--;
                    }
                }
            } else {
                state = STATE_LIST;
                btnFocus = 0;
            }
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (focus + startIdx == lines.length - 1) {
                state = STATE_BUTTON;
            } else {
                if (startIdx + focus < lines.length - 1) {
                    if (focus < 2) {
                        focus++;
                    } else if (startIdx + 6 < lines.length) {
                        if (focus > 2) {
                            startIdx++;
                        } else {
                            focus++;
                        }
                    } else {
                        focus++;
                    }
                }
            }
            repaint();
            return true;
        case OCRcEvent.VK_LEFT:
            if (state == STATE_BUTTON) {
                btnFocus = 0;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (state == STATE_BUTTON) {
                btnFocus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (state == STATE_LIST) {
                int curIdx = startIdx + focus;
                selects[curIdx] = !selects[curIdx];

                if (Log.DEBUG_ON) {
                    Log.printDebug("PopSelectPhoneLineUI: line no = " + lines[curIdx]);
                    Log.printDebug("PopSelectPhoneLineUI: select = " + selects[curIdx]);
                }
                repaint();
            } else if (state == STATE_BUTTON) {
                if (btnFocus == 0) {
                    clickEffect.start(45 + moveX, 343 + moveY, 162, 40);
                    parent.popupClosed(this, new Integer(ACTION_OK));
                } else {
                    clickEffect.start(209 + moveX, 343 + moveY, 162, 40);
                    parent.popupClosed(this, new Integer(ACTION_CANCEL));
                }
            }
            return true;
            // case KeyCodes.LAST:
            // parent.popupClosed(this, new Integer(ACTION_CANCEL));
            // return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * This class render a phone lines.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopSelectPhoneLineRenderer extends BaseRenderer {
        /** Image for top part of background. */
//        Image bgTopImg;
        /** Image for middle part of background. */
//        Image bgMidImg;
        /** Image for bottom part of background. */
//        Image bgBottomImg;
        /** Image for gap. */
        Image gapImg;
        /** Image for highlight. */
//        Image highImg;
        /** Image for background of list. */
        Image listBgImg;
        /** Image for focus of list. */
        Image listFocusImg;
        /** Image for check box. */
        Image checkBoxImg;
        /** Image for check box to have a focus. */
        Image checkBoxFocusImg;
        /** Image for check. */
        Image checkImg;
        /** Image for check to have a focus. */
        Image checkFocusImg;
        /** Image for button to have a focus. */
        Image btnFocusImg;
        /** Image for dimmed button . */
        Image btnDimImg;
        /** Image for up arrow. */
        Image arrowTopImg;
        /** Image for down arrow. */
        Image arrowBottomImg;
        /** Image for dimmed up arrow. */
        Image arrowTopDimImg;
        /** Image for dimmed down arrow. */
        Image arrowBottomDimImg;
        /** Font for size 24. */
        Font f24 = FontResource.BLENDER.getFont(24);
        /** Color for title (252, 202, 4) */
        Color titleColor = new Color(251, 217, 89);
        /** Color for focus (1, 1, 1). */
        Color focusColor = new Color(1, 1, 1);
        /** Color for dimmed (214, 214, 214). */
        Color dimmedColor = new Color(214, 214, 214);
        /** Color for background (35, 35, 35). */
        Color bgColor = new Color(35, 35, 35);
        /** Color for dimmed background (12, 12, 0, 204). */
        DVBColor dimmedBackColor = new DVBColor(12, 12, 0, 204);
        /** Name for OK button . */
        String btnOK;
        /** Name for cancel button . */
        String btnCancel;
        /** Name for title. */
        String title;

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
//            bgTopImg = BaseUI.getImage("05_pop_glow_t.png");
//            bgMidImg = BaseUI.getImage("05_pop_glow_m.png");
//            bgBottomImg = BaseUI.getImage("05_pop_glow_b.png");
            gapImg = BaseUI.getImage("pop_gap_379.png");
//            highImg = BaseUI.getImage("pop_high_350.png");
            listBgImg = BaseUI.getImage("pop_list272x189.png");
            listFocusImg = BaseUI.getImage("pop_list272x220_f.png");
            checkBoxImg = BaseUI.getImage("check_box.png");
            checkBoxFocusImg = BaseUI.getImage("check_box_foc.png");
            checkImg = BaseUI.getImage("check.png");
            checkFocusImg = BaseUI.getImage("check_foc.png");
            btnFocusImg = BaseUI.getImage("05_focus.png");
            btnDimImg = BaseUI.getImage("05_focus_dim.png");
            arrowTopImg = BaseUI.getImage("02_ars_t.png");
            arrowBottomImg = BaseUI.getImage("02_ars_b.png");
            arrowTopDimImg = BaseUI.getImage("02_ars_t_dim.png");
            arrowBottomDimImg = BaseUI.getImage("02_ars_b_dim.png");
            
            btnOK = getPopupText(KeyNames.BTN_OK);
            btnCancel = getPopupText(KeyNames.BTN_CANCEL);
            title = getPopupText(KeyNames.TITLE_SELECT_LINE);
            explain = getPopupText(KeyNames.EXPLAIN_SELECT_LINE);
        }

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedBackColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            parent.paintSelectedContent(g, parent);

            g.translate(moveX, moveY);

            // 463, 25
            // background
//            g.drawImage(bgTopImg, 0, 0, c);
//            g.drawImage(bgMidImg, 0, 80, 410, 260, c);
//            g.drawImage(bgBottomImg, 0, 340, c);
            g.setColor(bgColor);
            g.fillRect(30, 30, 350, 360);
            g.drawImage(gapImg, 9, 68, c);
//            g.drawImage(highImg, 30, 30, c);
            g.drawImage(listBgImg, 67, 125, c);

            g.setFont(f24);
            g.setColor(titleColor);
            GraphicUtil.drawStringCenter(g, title, 202, 56);
            g.setFont(f18);
            g.setColor(Color.WHITE);
            GraphicUtil.drawStringCenter(g, explain, 205, 97);

            if (lines != null) {
                int limit = lines.length > 6 ? 6 : lines.length; 
                for (int i = 0; i < limit; i++) {
                    int step = i * 31;
                    int curIdx = startIdx + i;
                    if (startIdx + focus == curIdx && c.getState() == STATE_LIST) {
                        g.drawImage(listFocusImg, 67, 126 + step, c);
                        g.drawImage(checkBoxFocusImg, 80, 134 + step, c);
                        g.setColor(focusColor);
                    } else {
                        g.setColor(dimmedColor);
                        g.drawImage(checkBoxImg, 80, 134 + step, c);
                    }

                    if (selects[curIdx]) {
                        if (startIdx + focus == curIdx && c.getState() == STATE_LIST) {
                            g.drawImage(checkFocusImg, 83, 133 + step, c);
                        } else {
                            g.drawImage(checkImg, 83, 133 + step, c);
                        }
                    }
                    g.drawString(lines[curIdx], 107, 147 + step);
                }
            }
            
            // arrow
            if (state == STATE_LIST) {
                if (startIdx > 0) {
                    g.drawImage(arrowTopImg, 198, 104, c);
                }
                
                if (startIdx + 6 < lines.length) {
                    g.drawImage(arrowBottomImg, 198, 318, c);
                }
            } else {
                if (startIdx > 0) {
                    g.drawImage(arrowTopDimImg, 198, 104, c);
                }
                
                if (startIdx + 6 < lines.length) {
                    g.drawImage(arrowBottomDimImg, 198, 318, c);
                }
            }
            
            g.setColor(focusColor);
            if (state == STATE_BUTTON) {
                if (btnFocus == 0) {
                    g.drawImage(btnFocusImg, 45, 343, c);
                    g.drawImage(btnDimImg, 209, 343, c);
                } else if (btnFocus == 1) {
                    g.drawImage(btnDimImg, 45, 343, c);
                    g.drawImage(btnFocusImg, 209, 343, c);
                }
            } else {
                g.drawImage(btnDimImg, 45, 343, c);
                g.drawImage(btnDimImg, 209, 343, c);
            }
            GraphicUtil.drawStringCenter(g, btnOK, 123, 364);
            GraphicUtil.drawStringCenter(g, btnCancel, 288, 364);

            g.translate(-(moveX), -(moveY));
        }
    }
}

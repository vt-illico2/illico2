/**
 * @(#)NormalSettingsBackUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import com.videotron.tvi.illico.upp.gui.NormalSettingsBackRenderer;

/**
 * This class represent a background of Not main.
 * @author Woojung Kim
 * @version 1.1
 */
public class NormalSettingsBackUI extends BaseUI {

    /** Renderer. */
    private NormalSettingsBackRenderer renderer = new NormalSettingsBackRenderer();

    /**
     * Start a UI.
     * @param back true if come from higher depth.
     */
    public void start(boolean back) {
        setRenderer(renderer);
        prepare();
        setVisible(true);
    }
}

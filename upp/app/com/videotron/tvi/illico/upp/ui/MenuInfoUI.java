/**
 * @(#)MenuInfoUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Font;
import java.awt.FontMetrics;

import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.MenuInfoRenderer;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The MenuInfoUI class represent a right screen side offers in text format and icon image, which is abount information
 * on menu item with focus located.
 * @author Woojung Kim
 * @version 1.1
 */
public class MenuInfoUI extends BaseUI {
    /** Renderer. */
    private MenuInfoRenderer renderer = new MenuInfoRenderer();
    /** Font of size 23. */
    private Font f23 = FontResource.BLENDER.getFont(23);
    /** FontMetics of size 23. */
    private FontMetrics fm23 = FontResource.BLENDER.getFontMetrics(f23);
    /** current menu id. */
    private String currentId;
    /** explain text for current menu. */
    private String[] explains;
    /** title for current menu. */
    private String title;
    /** Indicate whether user is admin. */
    private boolean isAdministrator;
    /** Effect to animate by fade-in. */
    private AlphaEffect initStart;
    /** Menu UI to have a focus and menu id. */
    private MenuUI menuUI;

    /**
     * Set a renderer.
     */
    public MenuInfoUI() {
        setRenderer(renderer);
    }

    /**
     * Start a UI.
     * @param back true if come from higher depth, false otherwise.
     */
    public void start(boolean back) {
        if (!back) {
            prepare();
            setVisible(false);
        } else {
            setVisible(true);
        }
    }

    /**
     * Update a information for current menu.
     * @param id menu id
     * @param name menu name
     * @param isAdmin true if user input a PIN and PIN means a admin, false otherwise.
     */
    public void update(String id, String name, boolean isAdmin) {
        isAdministrator = isAdmin;
        currentId = id;
        String temp = "";
        if (id.equals("family.pin.code")) {
            User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
            if (family.hasPin()) {
                temp = dataCenter.getString("explain.modify." + id);
            } else {
                temp = dataCenter.getString("explain.create." + id);
            }
        } else {
            temp = dataCenter.getString("explain." + id);
        }
        this.title = name;
        explains = TextUtil.split(temp, fm23, 520, '|');
        renderer.prepare(this);
        repaint();
    }

    /**
     * Start a effect (fade-in).
     */
    public void startEffect() {
        if (initStart == null) {
            initStart = new AlphaEffect(this, 15, MovingEffect.FADE_IN);
        }
        initStart.start();
    }

    /**
     * Handle a action after effect ended.
     * @param effect current effect.
     */
    public void animationEnded(Effect effect) {
        super.animationEnded(effect);
        setVisible(true);
        getParent().requestFocus();

        menuUI.checkAccessMenu();
    }

    /**
     * Get a explain.
     * @return explain.
     */
    public String[] getExplains() {
        return explains;
    }

    /**
     * Get a Title.
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Determine whether user is admin.
     * @return true if user input a PIN and PIN means a admin, false otherwise.
     */
    public boolean isAdministrator() {
        return isAdministrator;
    }

    /**
     * Get a current menu id.
     * @return menu id.
     */
    public String getCurrentId() {
        return currentId;
    }

    /**
     * Get Menu UI.
     * @param ui menu UI.
     */
    public void setMenuUI(MenuUI ui) {
        this.menuUI = ui;
    }
}

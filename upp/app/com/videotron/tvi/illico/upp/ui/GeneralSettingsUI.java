/**
 * @(#)GeneralSettingsDetailUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Point;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PinEnablerController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.MainCategory;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.GeneralSettingsRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a preferences for General Settings.
 * @author Woojung Kim
 * @version 1.1
 */
public class GeneralSettingsUI extends BaseUI implements KeyboardOption, KeyboardListener {

    /** Renderer */
    private GeneralSettingsRenderer renderer = new GeneralSettingsRenderer();
    /** a explain UI to display on bottom. */
    private ExplainUI explainUI = new ExplainUI();
    /** a dimmed popup */
    private PopDimmedUI dimmedUI = new PopDimmedUI();
    /** a Hashtable to include a preferences. */
    private Hashtable preTable;
    /** Popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** Popup to reboot a STB with countDown */
    private PopRestartTerminalUI restartTerminalUI = new PopRestartTerminalUI();

    // public final String[] preferenceIds = {PreferenceNames.LANGUAGE, PreferenceNames.AUDIO_FEEDBACK,
    // PreferenceNames.POSTAL_CODE, PreferenceNames.DASHBOARD_MENU_DISPLAY, PreferenceNames.SETUP_WIZARD};

    /** Preference id's. */
    public final String[] preferenceIds = {PreferenceNames.LANGUAGE, PreferenceNames.POSTAL_CODE,
            PreferenceNames.DISPLAY_MENU_AT_POWER_ON, PreferenceNames.SETUP_WIZARD, KeyNames.RESTART_TERMINAL};
    /** Indicate whether User input a wrong format for postal code or not. */
    private boolean isWrongPostalCode;
    /** Indicate whether Keyboard is displaying or not. */
    private boolean isShowingKeyboard;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer and add a explain UI.
     */
    public GeneralSettingsUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        MainCategory gsCategory = (MainCategory) BaseUI.peekHistory();

        preTable = DataManager.getInstance().getPreferences(gsCategory.getCategoryId());

        Preference pre = (Preference) preTable.get(PreferenceNames.POSTAL_CODE);

        pre.setCurrentValue(DataManager.getInstance().getPostalCode());

        addButton(BTN_BACK);

        super.prepare();
    }

    /**
     * Start a UI.
     * @param back true if come from higher depth, false otherwise.
     */
    public void start(boolean back) {
        prepare();
        focus = 0;
        isShowingKeyboard = false;
        setExplainText();
        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearButtons();
        clearPopup();
        explainUI.stop();
        KeyboardService keyService = CommunicationManager.getInstance().getKeyboardService();
        if (keyService != null) {
            try {
                removePopup(dimmedUI);
                keyService.hidePopupKeyboard(this);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        super.stop();
    }

    /**
     * Get a Hashtable to include a preferences.
     * @return Hashtable to include a preferences.
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:

            if (++focus == preferenceIds.length) {
                focus = preferenceIds.length - 1;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37, 211, 33);
            if (preferenceIds[focus].equals(PreferenceNames.POSTAL_CODE)) {
                setPopup(dimmedUI);
                isWrongPostalCode = false;
                KeyboardService keyService = CommunicationManager.getInstance().getKeyboardService();
                try {
                    dimmedUI.show(this);
                    keyService.showPopupKeyboard(this, this);
                    isShowingKeyboard = true;
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                repaint();
            } else if (focus == 3) { // Quick start wizard
                showPinEnbler();
            } else if (focus == 4) { // Restart terminal
            	// show countdown Popup
            	setPopup(restartTerminalUI);
            	restartTerminalUI.start(this);
            } else {
                setPopup(selectUI);
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                String title = pre.getName();
                if (pre.getID().equals(PreferenceNames.DISPLAY_MENU_AT_POWER_ON)) {
                    title = getMenuShortText(PreferenceNames.DISPLAY_MENU_AT_POWER_ON);
                }

                selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), title, 519, 83 + focus * 37, 3);
                repaint();
            }
            return true;
        case KeyCodes.SEARCH:
            KeyboardService keyService = CommunicationManager.getInstance().getKeyboardService();
            try {
                keyService.hidePopupKeyboard(this);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            return false;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
            return true;
        default:
            return false;
        }
    }

    /**
     * Set a explain text for focus.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Determine whether Keyboard is showing or not.
     * @return true if keyboard is showing, false otherwise.
     */
    public boolean isShowingKeyboard() {
        return isShowingKeyboard;
    }

    /**
     * Handle a action after popup closed.
     * @param pop popup to be closed.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        int action = ((Integer) msg).intValue();
        removePopup(pop);
        repaint();
        if (pop.equals(selectUI)) {

            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectUI.getSelectedValue();

                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);

                if (preferenceIds[focus].equals(PreferenceNames.LANGUAGE)) {
                    // update a screen for language.
                    renderer.prepare(this);
                    addClockListener();
                    setExplainText();
                    clearButtons();
                    addButton(BTN_BACK);
                    repaint();
                }
            }
        } else if (pop.equals(notiUI)) {
            setPopup(dimmedUI);
            KeyboardService keyService = CommunicationManager.getInstance().getKeyboardService();
            try {
                dimmedUI.show(this);
                keyService.showPopupKeyboard(this, this);
                isShowingKeyboard = true;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Show a PinEnabler.
     */
    private void showPinEnbler() {
        try {
            PreferenceController.getInstance().checkRightFilter(new RightFilterListenerImpl(), "", new String[]{"allow.access.wizard"}, null, null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Return a result to check a format of Postal code.
     * @param code postal code to be inputed.
     * @return true if postal code is current with format, false otherwise.
     */
    private boolean checkPostalCode(String code) {
        if (code == null || code.equals("") || code.length() < 6) {
            return false;
        }

        for (int i = 0; i < code.length(); i++) {
            char ch = code.charAt(i);
            System.out.println("char : " + ch);
            if (i % 2 == 0) {
                if (ch < 'A' || ch > 'Z') { // 65 - 90
                    return false;
                }
            } else {
                if (ch < '0' || ch > '9') { // 48 ~ 57
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Handle a action after Keyaboard which postal code is inputed is finished.
     * @param text postal code.
     * @throws RemoteException remote exception.
     */
    public void inputEnded(String text) throws RemoteException {

        if (checkPostalCode(text.toUpperCase())) {
            isShowingKeyboard = false;
            removePopup(dimmedUI);
            Preference pre = (Preference) preTable.get(PreferenceNames.POSTAL_CODE);
            pre.setCurrentValue(text.toUpperCase());
            DataManager.getInstance().setPreference(pre.getID(), pre.getCurrentValue());
            repaint();
        } else {
            isShowingKeyboard = false;
            removePopup(dimmedUI);
            setPopup(notiUI);
            String title, explain;
            isWrongPostalCode = true;
            title = getPopupText(KeyNames.TITLE_INVALID_POSTAL_CODE);
            explain = getPopupText(KeyNames.EXPLAIN_INVALID_POSTAL_CODE);
            notiUI.show(this, title, TextUtil.split(explain, renderer.fm18, 310, '|'), PopBaseRenderer.ICON_TYPE_ORANGE);
            getParent().repaint();
        }
    }

    /**
     * Handle a action after keyboard is canceled.
     * @param text postal code.
     * @throws RemoteException remote exception.
     */
    public void inputCanceled(String text) throws RemoteException {
        removePopup(dimmedUI);
        isShowingKeyboard = false;
        repaint();
    }

    /** Do nothing. */
    public void cursorMoved(int position) throws RemoteException {
    }

    /** Do nothing. */
    public void textChanged(String ext) throws RemoteException {
    }

    /** Do nothing. */
    public void focusOut(int direction) throws RemoteException {
    }

    /** Do nothing. */
    public void modeChanged(int mode) throws RemoteException {
    }

    /**
     * Get a default text for postal code.
     * @return default text.
     */
    public String getDefaultText() throws RemoteException {
        if (isWrongPostalCode) {
            return TextUtil.EMPTY_STRING;
        }

        Preference pre = (Preference) preTable.get(PreferenceNames.POSTAL_CODE);
        return pre.getCurrentValue();
    }

    /**
     * Determine whether default text will be used or not.
     * @return true if default text will be used, false otherwise.
     * @throws RemoteException remote exception.
     */
    public boolean getTempDefaultText() throws RemoteException {
        return true;
    }

    /** Do nothing. */
    public String getInputPattern() throws RemoteException {
        return null;
    }

    /**
     * Return a max length for Postal code.
     * @return max length
     * @throws RemoteException remote exception.
     */
    public int getMaxChars() throws RemoteException {
        return 6;
    }

    /** Do nothing. */
    public char getEchoChars() throws RemoteException {
        return 0;
    }

    /**
     * Determine whether keyboard will be closed if User input a characters to max length.
     * @return true if keyboard will be closed if user input a characters to max length, false otherwise.
     * @throws RemoteException remote exception.
     */
    public boolean isClosedWithMaxInput() throws RemoteException {
        return true;
    }

    /**
     * Get a wanted keyboard type.
     * @return {@link KeyboardOption.TYPE_ALPHANUMERIC}
     * @throws RemoteException remote exception
     */
    public int getType() throws RemoteException {
        return KeyboardOption.TYPE_ALPHANUMERIC;
    }

    /**
     * Get a mode of keyboard.
     * @return {@link KeyboardOption.MODE_POPUP}
     * @throws RemoteException remote exception
     */
    public short getMode() throws RemoteException {
        return KeyboardOption.MODE_POPUP;
    }

    /**
     * Return a target coordinate.
     * @return Point(525, 96)
     * @throws RemoteException remote exception
     */
    public Point getPosition() throws RemoteException {
        return new Point(525, 96);
    }

    /**
     * Return a keyboard title.
     * @return title for postal code.
     * @throws RemoteException remote exception
     */
    public String getTitle() throws RemoteException {
        return BaseUI.getPopupText(KeyNames.ENTER_POSTAL_CODE);
    }

    /**
     * Get a type for alphanumeric mode.
     * @return {@link KeyboardOption.ALPHANUMERIC_LARGE}
     * @throws RemoteException remote exception
     */
    public int getAlphanumericType() throws RemoteException {
        return KeyboardOption.ALPHANUMERIC_LARGE;
    }

    /** Do nothing. */
    public void inputCleared() throws RemoteException {
    }
    
    public boolean isKeyboardChangedDeactivate() throws RemoteException {
        return true;
    }
    
    class RightFilterListenerImpl implements RightFilterListener {

		public void receiveCheckRightFilter(int response) throws RemoteException {
			if (response == PreferenceService.RESPONSE_SUCCESS) {
				MonitorService mService = CommunicationManager.getInstance().getMonitorService();

                if (mService != null) {
                    try {
                        mService.startUnboundApplication("Wizard", new String[] {"SETTINGS"});
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
			}
		}

		public String[] getPinEnablerExplain() throws RemoteException {
			String explain = getPopupText(KeyNames.EXPLAIN_ACCESS_MENU);
			String[] explainArray = TextUtil.split(explain, renderer.fm18, 310);
			return explainArray;
		}
    	
    }
}

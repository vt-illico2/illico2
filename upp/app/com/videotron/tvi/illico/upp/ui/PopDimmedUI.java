/**
 * @(#)PopDimmedUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Graphics;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;

/**
 * Display a three items of selectable.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopDimmedUI extends BaseUI {

    private PopDimmedRenderer renderer = new PopDimmedRenderer();
    private BaseUI parent;

    /**
     * Set a renderer.
     */
    public PopDimmedUI() {
        setRenderer(renderer);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     */
    public void show(BaseUI com) {
        parent = com;
        setVisible(true);
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {
        switch (code) {
        default:
            return true;
        }
    }

    /**
     * This class render a dimmed background.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopDimmedRenderer extends BaseRenderer {
        /**
         * Paint a dimmed background.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);

            parent.paintSelectedContent(g, parent);
        }
    }
}

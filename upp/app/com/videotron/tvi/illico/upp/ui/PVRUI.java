/**
 * @(#)PVRUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Point;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.PVRRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a preferences for PVR.
 * @author Woojung Kim
 * @version 1.1
 */
public class PVRUI extends BaseUI {
    /** Renderer. */
    private PVRRenderer renderer = new PVRRenderer();
    /** Explain UI to display at buttom. */
    private ExplainUI explainUI = new ExplainUI();
    /** Popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** Implements a Keyboard option for Terminal name. */
    private KeyboardOptionImpl keyboardOption = new KeyboardOptionImpl();
    /** Implements a KeyboardListener for Terminal name. */
    private ReceiverKeyboardEvent keyboardReceiver = new ReceiverKeyboardEvent();
    /** ID array for preferences of PVR. */
    public final String[] preferenceIds = {PreferenceNames.RECORD_BUFFER, PreferenceNames.DEFAULT_SAVE_TIME,
            PreferenceNames.DEFAULT_SAVE_TIME_REPEAT, PreferenceNames.TERMINAL_NAME};
    /** Hashtable to include a preference for PVR. */
    private Hashtable preTable;
    /** KeyboardService. */
    private KeyboardService keyboardService;
    /** PvrService. */
    private PvrService pvrService;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    private String defaultOfOtherName;
    
    private String[] multiroomOptions;
    
    private PopNotificationUI notiPopupUI = new PopNotificationUI();

    /**
     * Set a renderer and add explainUI and create clickingEffect.
     */
    public PVRUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {
        Category gsCategory = (Category) BaseUI.peekHistory();

        preTable = DataManager.getInstance().getPreferences(gsCategory.getCategoryId());
        
        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.TERMINAL_OTHER_NAME);
        
        preTable.put(pre.getID(), pre);

        pvrService = (PvrService) DataCenter.getInstance().get(PvrService.IXC_NAME);
        
        if (pvrService != null) {
        	try {
				String terminalName = pvrService.getDeviceName();
				
				if (Log.DEBUG_ON) {
					Log.printDebug("PVRUI: prepare: pvrService.getDeviceName = " + terminalName);
				}
				
				// VDTRMASTER-5702
				if (terminalName != null) {
					Preference pre2 = (Preference) preTable.get(PreferenceNames.TERMINAL_NAME);
					pre2.setCurrentValue(terminalName);
					
					preTable.put(pre2.getID(), pre2);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
        }
        
        resetMultiroomOptions();

        keyboardService = CommunicationManager.getInstance().getKeyboardService();

        focus = 0;

        defaultOfOtherName = BaseUI.getOptionText("Other name default");
        super.prepare();
    }

    /**
     * Start a UI.
     * @param back true if come into higher depth, false otherwise.
     */
    public void start(boolean back) {
        prepare();
        addButton(BTN_BACK);
        setExplainText();

        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        if (keyboardService != null) {
            try {
                keyboardService.hidePopupKeyboard(keyboardReceiver);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
        super.stop();
    }

    /**
     * Get a Hashtable to include a preferences.
     * @return Hashtable to include a preferences.
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:

            if (++focus == preferenceIds.length) {
                focus = preferenceIds.length - 1;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:

            clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 48, 211, 33);
            if (preferenceIds[focus].equals(PreferenceNames.TERMINAL_NAME)) {
            	setPopup(selectUI);
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                String curValue = pre.getCurrentValue();
                
//                if (curValue.equals(Definitions.PVR_OTHER_NAME)) {
//                	Preference pre2 = DataManager.getInstance().getPreference(PreferenceNames.TERMINAL_OTHER_NAME);
//                	curValue = pre2.getCurrentValue();
//                }
                selectUI.show(this, multiroomOptions, curValue, pre.getName(), 519,
                        83 + focus * 48, 3);
                repaint();
            } else {
                setPopup(selectUI);
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
                        83 + focus * 48, 3);
                repaint();
            }
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
            return true;
        default:
            return false;
        }
    }
    
    private void resetMultiroomOptions() {
    	String[] options = null;
    	
    	Preference pre = DataManager.getInstance().getPreference(PreferenceNames.TERMINAL_OTHER_NAME);
    	// TODO
    	if (pre != null && pre.getCurrentValue().length() > 0) {
    		options = new String[7];
    	} else {
    		options = new String[6];
    	}
    	
    	for (int i = 0; i < options.length; i++) {
    		if (i == 6) {
    			options[i] = pre.getCurrentValue();
    		} else {
    			options[i] = DataCenter.getInstance().getString("option.multiroom.name" + i);
    		}
    		
    		if (Log.DEBUG_ON) {
    			Log.printDebug("PVRUI: resetMultiroomOptions(): options = " + options[i]);
    		}
    	}
    	
    	multiroomOptions = options;
    }

    /**
     * Set a explain text.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Handle a action after popup closed.
     * @param pop popup to be closed.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        if (pop.equals(selectUI)) {
            int action = ((Integer) msg).intValue();
            removePopup(selectUI);
            repaint();
            if (action == BaseUI.ACTION_OK) {
	            String selectedValue = selectUI.getSelectedValue();
	            if (focus == 3) { // case of terminal name
	            	if (selectUI.getFocus() == 0 || selectUI.getFocus() == 6) { // custom name || other name
	            		if (keyboardService != null) {
	                        try {
	                            keyboardService.showPopupKeyboard(keyboardOption, keyboardReceiver);
	                        } catch (RemoteException e) {
	                            Log.print(e);
	                        }
	                    }
	            	} else {
	            		if (pvrService != null) {
	                        try {
	                            pvrService.setDeviceName(selectedValue);
	                        } catch (RemoteException e) {
	                            e.printStackTrace();
	                        }
	                    }
	            		Preference pre = (Preference) preTable.get(preferenceIds[focus]);
	            		pre.setCurrentValue(selectedValue);
	            		preTable.put(pre.getID(), pre);
	            		DataManager.getInstance().setPreference(pre.getID(), selectedValue);
	            		
	            		// reseet other terminal name
//	            		pre = (Preference) preTable.get(PreferenceNames.TERMINAL_OTHER_NAME);
//	                    if (pre != null) {
//	                        pre.setCurrentValue(TextUtil.EMPTY_STRING);
//	                        DataManager.getInstance().setPreference(PreferenceNames.TERMINAL_OTHER_NAME, TextUtil.EMPTY_STRING);
//	                    }
	                    
	                    resetMultiroomOptions();
	                    repaint();
	            	}
	            } else {
	            	 Preference pre = (Preference) preTable.get(preferenceIds[focus]);
	                 pre.setCurrentValue(selectedValue);
	                 preTable.put(pre.getID(), pre);
	                 DataManager.getInstance().setPreference(pre.getID(), selectedValue);
	                 
	                 if (focus == 0) {
	                	 
	                	 int firstSpace = selectedValue.indexOf(" ");
	                	 String minute = selectedValue.substring(0, firstSpace);
		                 String title = TextUtil.replace(BaseUI.getPopupText(KeyNames.TITLE_BUFFER_CHANGE), "%%", minute);
		                 
		                 if (minute.equals("0") || minute.equals("1")) {
		                	 title = title.substring(0, title.length() - 1);
		                 }
		                 
		                 String desc = BaseUI.getPopupText(KeyNames.EXPLAIN_BUFFER_CHANGE);
		                 
		                 setPopup(notiPopupUI);
		                 notiPopupUI.show(this, title, TextUtil.split(desc, renderer.fm18, 310, '|'), PopBaseRenderer.ICON_TYPE_NONE);
		                 notiPopupUI.startEffect();
	                 }
	            }
            }
        } else {
        	removePopup(pop);
            repaint();
        }
    }

    /**
     * This class implements KeyboardOption.
     * @author Woojung Kim
     * @version 1.1
     */
    class KeyboardOptionImpl implements KeyboardOption {
        /**
         * Keyboard display a empty string when start.
         * @return default text.
         * @throws RemoteException remote exception.
         */
        public String getDefaultText() throws RemoteException {
            Preference pre = (Preference) preTable.get(PreferenceNames.TERMINAL_OTHER_NAME);

            if (pre != null && pre.getCurrentValue() != null) {
                return pre.getCurrentValue();
            } else {
                return defaultOfOtherName;
            }
        }

        /**
         * Keyboard use a start as echo character.
         * @return echo character.
         * @throws RemoteException remote exception
         */
        public char getEchoChars() throws RemoteException {
            return '*';
        }

        /**
         * Keyboard don't use a pattern.
         * @return pattern.
         * @throws RemoteException remote exception
         */
        public String getInputPattern() throws RemoteException {
            return null;
        }

        /**
         * Keyboard can be inputed to 15 characters.
         * @return max chars
         * @throws RemoteException remote exception.
         */
        public int getMaxChars() throws RemoteException {
            return 15;
        }

        /**
         * Keyboard display with popup style.
         * @return keyboard mode.
         * @throws RemoteException remote exception
         */
        public short getMode() throws RemoteException {
            return KeyboardOption.MODE_POPUP;
        }

        /**
         * Keyboard display with default type.
         * @return keyboard type.
         * @throws RemoteException remote exception
         */
        public int getType() throws RemoteException {
            return KeyboardOption.TYPE_DEFAULT;
        }

        /**
         * Keboard will be closed when the size of inputed characters is Max.
         * @return true
         * @throws RemoteException remote exception.
         */
        public boolean isClosedWithMaxInput() throws RemoteException {
            return true;
        }

        /**
         * Keyboard display at (494, 154).
         * @return coordinate that Keyboard will be displayed.
         * @throws RemoteException remote exception.
         */
        public Point getPosition() throws RemoteException {
            return new Point(494, 154);
        }

        /**
         * Keyboard don't use a temporally default text.
         * @return false
         * @throws RemoteException remote exception.
         */
        public boolean getTempDefaultText() throws RemoteException {
            return true;
        }

        /**
         * Keyboard display a title is null.
         * @return title
         * @throws RemoteException remote exception.
         */
        public String getTitle() throws RemoteException {
            // TODO Auto-generated method stub
            return null;
        }

        /**
         * Keyboard display a {@link KeyboardOption#ALPHANUMERIC_ALL}.
         * @return {@link KeyboardOption#ALPHANUMERIC_ALL}
         * @throws RemoteException remote exception.
         */
        public int getAlphanumericType() throws RemoteException {
            return KeyboardOption.ALPHANUMERIC_ALL;
        }

        public boolean isKeyboardChangedDeactivate() throws RemoteException {
            return false;
        }
    }

    /**
     * This class implements KeyboardListener.
     * @author Woojung Kim
     * @version 1.1
     */
    class ReceiverKeyboardEvent implements KeyboardListener {
        /**
         * Do nothing.
         */
        public void cursorMoved(int position) throws RemoteException {

        }

        /**
         * Do nothing.
         */
        public void inputCanceled(String text) throws RemoteException {
            Log.printDebug("ReceiverKeyboardEvent: called inputCanceled.");
        }

        /**
         * Receive a inputed text then set a value of Terminal name.
         * @param text inputed text.
         * @throws RemoteException remote exception.
         */
        public void inputEnded(String text) throws RemoteException {
            Log.printDebug("ReceiverKeyboardEvent: called inputEnded : " + text);
            String name;
            if (text.length() == 0) {
            	name = defaultOfOtherName;
            } else {
            	name = text.trim();
            }
            DataManager.getInstance().setPreference(PreferenceNames.TERMINAL_NAME, name);
            Preference pre = (Preference) preTable.get(PreferenceNames.TERMINAL_NAME);
            pre.setCurrentValue(name);
            DataManager.getInstance().setPreference(PreferenceNames.TERMINAL_OTHER_NAME, name);

            if (pvrService != null) {
                try {
                    pvrService.setDeviceName(name);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            pre = (Preference) preTable.get(PreferenceNames.TERMINAL_OTHER_NAME);
            if (pre != null) {
                pre.setCurrentValue(name);
            } else {
                pre = DataManager.getInstance().getPreference(PreferenceNames.TERMINAL_OTHER_NAME);
                if (pre != null) {
                    preTable.put(pre.getID(), pre);
                }
            }
            getParent().repaint();
            resetMultiroomOptions();
        }

        /**
         * Do nothing.
         */
        public void textChanged(String ext) throws RemoteException {
        }

        /**
         * Do nothing.
         */
        public void focusOut(int direction) throws RemoteException {

        }

        /**
         * Do nothing.
         */
        public void modeChanged(int mode) throws RemoteException {

        }

        /**
         * Do nothing.
         */
        public void inputCleared() throws RemoteException {

        }

    }
}

/**
 * @(#)TimeBasedRestrictionsUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.text.DecimalFormat;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.controller.STBTimerController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.Time;
import com.videotron.tvi.illico.upp.gui.TimeBasedRestrictionsRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a UI to modify a time-based restrictions.
 * @author Woojung Kim
 * @version 1.1
 */
public class TimeBasedRestrictionsUI extends BaseUI {

    /** Max length which can be added. */
    private static final int MAX_PERIODS = 5;
    /** Renderer. */
    private TimeBasedRestrictionsRenderer renderer = new TimeBasedRestrictionsRenderer();
    /** Popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** Popup to select a time. */
    private PopSelectSmallUI selectSmallUI = new PopSelectSmallUI();
    /** Popup to select a days. */
    private PopSelectDayUI selectDaysUI = new PopSelectDayUI();
    /** Buffer for directly input. */
    private StringBuffer inputBuffer = new StringBuffer();
    /** Decimal format for time. */
    private DecimalFormat df = new DecimalFormat("00");
    /** Preference of Time-based Restrictions. */
    private Preference pre;
    /** Vector to have a Time's. */
    private Vector timeVector;
    /** Constant to indicate a current period to have a focus. */
    private int focusedPeriodIdx = 0;
    /** Indicate whether language is English or not. */
    private boolean isEnglish;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer and create a Vector which have a Time's objects and create a ClickEffect.
     */
    public TimeBasedRestrictionsUI() {
        renderer.setHasExpalin(false);
        setRenderer(renderer);
        timeVector = new Vector();
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {

        pre = DataManager.getInstance().getPreference(RightFilter.TIME_BASED_RESTRICTIONS);

        buildTimes();

        if (timeVector.size() > 0) {
            focusedPeriodIdx = 0;
        } else {
            focusedPeriodIdx = -1;
        }

        if (timeVector.size() < MAX_PERIODS) {
            clearButtons();
            addButton(BaseUI.BTN_BACK);
            addButton(BaseUI.BTN_ADD_PERIOD);
        }
        inputBuffer.setLength(0);

        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE);
        if (pre.getCurrentValue().equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }

        super.prepare();
    }

    /**
     * Start a UI.
     */
    public void start(boolean back) {
        prepare();

        focus = 0;

        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        saveBlockTimes();
        super.stop();
    }

    /**
     * Get a Preference to have a current value.
     * @return Preference to have a current value.
     */
    public Preference getPreferences() {
        return pre;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popup = getPopup();
            if (popup.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            if (focus == 1 || focus == 2 || focus == 4 || focus == 5) {
                updateTime(code);
            }
            return true;
        case OCRcEvent.VK_UP:
            updateTime(code);
            keyUp();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            updateTime(code);
            keyDown();
            repaint();
            return true;
        case OCRcEvent.VK_LEFT:
            updateTime(code);
            keyLeft();
            repaint();
            return true;
        case OCRcEvent.VK_RIGHT:
            updateTime(code);
            keyRight();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            keyOK();
            return true;
        case KeyCodes.COLOR_B:
            clickButtons(BTN_ADD_PERIOD);
            addNewPeriod();
            return true;
        case KeyCodes.LAST:
            clickButtons(BTN_BACK);
            // startFadeOutEffect(this);
            popHistory();
            PreferenceController.getInstance().showUIComponent(BaseUI.UI_PARENTAL_CONTROLS, true);
            return true;
        default:
            return false;
        }
    }

    /**
     * Handle a up key.
     */
    private void keyUp() {
        if (focus == 0 || focus == 9) {
            if (focusedPeriodIdx > 0) {
                focusedPeriodIdx--;
                if (focus == 9) {
                    if (canSelectMenu()) {
                        focus = 8;
                    } else {
                        focus = 9;
                    }
                } else {
                    if (canSelectMenu()) {
                        focus = 7;
                    } else {
                        focus = 0;
                    }
                }
            }
        } else if (focus < 4) {
            focus = 0;
        } else if (focus < 7) {
            focus = 9;
        } else if (focus == 7) {
            focus = 1;
        } else if (focus == 8) {
            focus = 4;
        }
        repaint();
    }

    /**
     * Handle a down key.
     */
    private void keyDown() {
        if (focus == 0 && !canSelectMenu()) {
            if (focusedPeriodIdx + 1 < timeVector.size()) {
                focusedPeriodIdx++;
                focus = 0;
            }
        } else if (focus == 9 && !canSelectMenu()) {
            if (focusedPeriodIdx + 1 < timeVector.size()) {
                focusedPeriodIdx++;
                focus = 9;
            }
        } else if (canSelectMenu()) {
            if (focus == 0) {
                focus = 1;
            } else if (focus < 4) {
                focus = 7;
            } else if (focus < 7) {
                focus = 8;
            } else if (focus == 9) {
                focus = 4;
            } else {
                if (focusedPeriodIdx + 1 < timeVector.size()) {
                    focusedPeriodIdx++;
                    if (focus == 7) {
                        focus = 0;
                    } else if (focus == 8) {
                        focus = 9;
                    }
                }
            }
        }
        repaint();
    }

    /**
     * Handle a left key.
     */
    private void keyLeft() {
        if (focus == 9) {
            focus = 0;
        } else if (focus > 1 && focus < 7) {
            focus--;
            if (!isEnglish && focus == 3) {
                focus = 2;
            }
        } else if (focus == 8) {
            focus = 7;
        }
        repaint();
    }

    /**
     * Handle a right key.
     */
    private void keyRight() {
        if (focus == 0) {
            focus = 9;
        } else if (focus < 6) {
            focus++;
            if (!isEnglish && focus == 3) {
                focus = 4;
            }

            if (!isEnglish && focus == 6) {
                focus = 5;
            }
        } else if (focus == 7) {
            focus = 8;
        }
        repaint();
    }

    /**
     * Handle a OK key.
     */
    private void keyOK() {
        if (timeVector.size() > 0) {
            int stepY = 0;
            if (getWhoHasFocus() == 1) {
                stepY = 176;
            }
            if (focus == 0) {
                clickEffect.start(224, 143 + stepY, 211, 33);
                setPopup(selectUI);
                Time time = getTime(focusedPeriodIdx);
                StringBuffer sf = new StringBuffer();
                sf.append(BaseUI.getMenuText(KeyNames.RESTRICTED_PERIOD));
                sf.append(" ");
                sf.append(focusedPeriodIdx + 1);
                String enabled = BaseUI.getOptionText(Definitions.OPTION_VALUE_ENABLE + "d");
                String disabled = BaseUI.getOptionText(Definitions.OPTION_VALUE_DISABLE + "d");
                String focused = BaseUI.getOptionText(time.getActive() + "d");
                selectUI.show(this, new String[] {enabled, disabled}, focused, sf.toString(), 373, 85 + stepY, 3);
                repaint();
            } else if (focus < 4) {
                int stepX = (focus - 1) * 52;
                clickEffect.start(224 + stepX, 201 + stepY, 38, 32);
                setPopup(selectSmallUI);
                Time time = getTime(focusedPeriodIdx);
                if (focus == 1) {
                    if (isEnglish) {
                        if (time.getStartAmPm().equals(Definitions.TIME_AM)) {
                            selectSmallUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_AM, time.getStartHour(), 224,
                                    176 + stepY);
                        } else {
                            selectSmallUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_PM, time.getStartHour(), 224,
                                    176 + stepY);
                        }
                    } else {
                        String hourStr = time.getStartHour();
                        int hour = Integer.parseInt(time.getStartHour());
                        if (time.getStartAmPm().equals("pm")) {
                            hour += 12;
                            hourStr = df.format(hour);
                        }
                        selectSmallUI.show(this, 24, PopSelectSmallUI.TYPE_HOURS_PM, hourStr, 224, 176 + stepY);
                    }
                } else if (focus == 2) {
                    selectSmallUI.show(this, 60, PopSelectSmallUI.TYPE_MINS, time.getStartMin(), 276, 176 + stepY);
                } else if (focus == 3) {
                    selectSmallUI.show(this, new String[] {Definitions.TIME_AM, Definitions.TIME_PM},
                            time.getStartAmPm(), 328, 176 + stepY);
                }
                repaint();
            } else if (focus < 7) {
                int stepX = (focus - 4) * 52;
                clickEffect.start(652 + stepX, 201 + stepY, 38, 32);
                setPopup(selectSmallUI);
                Time time = getTime(focusedPeriodIdx);
                if (focus == 4) {
                    if (isEnglish) {
                        if (time.getEndAmPm().equals(Definitions.TIME_AM)) {
                            selectSmallUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_AM, time.getEndHour(), 652,
                                    176 + stepY);
                        } else {
                            selectSmallUI.show(this, 12, PopSelectSmallUI.TYPE_HOURS_PM, time.getEndHour(), 652,
                                    176 + stepY);
                        }
                    } else {
                        String hourStr = time.getEndHour();
                        int hour = Integer.parseInt(time.getEndHour());
                        if (time.getEndAmPm().equals("pm")) {
                            hour += 12;
                            hourStr = df.format(hour);
                        }
                        selectSmallUI.show(this, 24, PopSelectSmallUI.TYPE_HOURS_PM, hourStr, 652, 176 + stepY);
                    }
                } else if (focus == 5) {
                    selectSmallUI.show(this, 60, PopSelectSmallUI.TYPE_MINS, time.getEndMin(), 704, 176 + stepY);
                } else if (focus == 6) {
                    selectSmallUI.show(this, new String[] {Definitions.TIME_AM, Definitions.TIME_PM},
                            time.getEndAmPm(), 756, 176 + stepY);
                }
                repaint();
            } else if (focus == 7) {
                clickEffect.start(224, 241 + stepY, 211, 33);
                setPopup(selectDaysUI);
                Time time = getTime(focusedPeriodIdx);
                selectDaysUI.show(this, time.getDays(),
                        getExplainText(KeyNames.EXPLAIN_SELECT_DAYS_TIME_BASED_RESTRICTIONS), 344, 57);
                repaint();
            } else if (focus == 8) {
                clickEffect.start(652, 241 + stepY, 211, 33);
                setPopup(selectUI);
                Time time = getTime(focusedPeriodIdx);
                String everyWeek = BaseUI.getPopupText(KeyNames.EVERY_WEEK);
                String none = BaseUI.getPopupText(KeyNames.NONE);
                String focused = none;
                if (time.getRepeat().equals("Yes")) {
                    focused = everyWeek;
                }
                selectUI.show(this, new String[] {everyWeek, none}, focused, BaseUI.getMenuText(KeyNames.REPEAT), 725,
                        183 + stepY, 3);
                repaint();

            } else if (focus == 9) {
                clickEffect.start(723, 143 + stepY, 211, 33);
                removeCurrentPeriod();
            }
            repaint();
        }
    }

    /**
     * Update a time data according to direct input.
     * @param code
     */
    private void updateTime(int code) {
        if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
            int input = code - OCRcEvent.VK_0;
            if (focus == 1 || focus == 4) { // hour

                int limitCode = 1;

                if (!isEnglish) {
                    limitCode = 2;
                }

                if (input > limitCode && input < 10 && inputBuffer.length() == 0) { // 2 ~ 9
                    String value = df.format(input);
                    if (focus == 1) {
                        getTime(focusedPeriodIdx).setStartHour(value);
                    } else {
                        getTime(focusedPeriodIdx).setEndHour(value);
                    }
                    focus++;
                    inputBuffer.setLength(0);
                    repaint();
                } else { // 1 , 10, 11, 2, 20, 21, 22, 23, 24
                    inputBuffer.append(input);
                    if (inputBuffer.length() == 2) {
                        input = Integer.parseInt(inputBuffer.toString());
                        if (input < 12) {
                            String value = df.format(input);
                            if (focus == 1) {
                                getTime(focusedPeriodIdx).setStartHour(value);
                                getTime(focusedPeriodIdx).setStartAmPm("am");
                            } else {
                                getTime(focusedPeriodIdx).setEndHour(value);
                                getTime(focusedPeriodIdx).setEndAmPm("am");
                            }
                            focus++;

                        } else if (input == 12 && isEnglish) {
                            String value = df.format(00);
                            if (focus == 1) {
                                getTime(focusedPeriodIdx).setStartHour(value);
                                getTime(focusedPeriodIdx).setStartAmPm("am");
                            } else {
                                getTime(focusedPeriodIdx).setEndHour(value);
                                getTime(focusedPeriodIdx).setEndAmPm("am");
                            }
                            focus++;
                        } else if (input < 24 && !isEnglish) {
                            String value = df.format(input - 12);
                            if (focus == 1) {
                                getTime(focusedPeriodIdx).setStartHour(value);
                                getTime(focusedPeriodIdx).setStartAmPm("pm");
                            } else {
                                getTime(focusedPeriodIdx).setEndHour(value);
                                getTime(focusedPeriodIdx).setEndAmPm("pm");
                            }
                            focus++;
                        }
                        inputBuffer.setLength(0);
                    }
                    repaint();
                }
            } else if (focus == 2 || focus == 5) { // min
                if (input > 5 && input < 10 && inputBuffer.length() == 0) { // 6 ~ 9
                    String value = df.format(input);
                    if (focus == 2) {
                        getTime(focusedPeriodIdx).setStartMin(value);
                    } else {
                        getTime(focusedPeriodIdx).setEndMin(value);
                    }
                    focus++;
                    if (!isEnglish) {
                        focus++;
                    }
                    repaint();
                } else {
                    inputBuffer.append(input);
                    if (inputBuffer.length() == 2) {
                        input = Integer.parseInt(inputBuffer.toString());
                        if (input < 60) {
                            String value = df.format(input);
                            if (focus == 2) {
                                getTime(focusedPeriodIdx).setStartMin(value);
                            } else {
                                getTime(focusedPeriodIdx).setEndMin(value);
                            }
                            focus++;
                            if (!isEnglish) {
                                focus++;
                            }
                        }
                        inputBuffer.setLength(0);
                    }
                    repaint();
                }
            }
        } else { // Left, Right Key
            if (inputBuffer.length() > 0) {
                if (focus == 1 || focus == 4) {
                    int input = Integer.parseInt(inputBuffer.toString());
                    int limit = 12;
                    if (!isEnglish) {
                        limit = 24;
                    }
                    if (input < limit) {
                        String value = df.format(input);
                        if (focus == 1) {
                            getTime(focusedPeriodIdx).setStartHour(value);
                        } else {
                            getTime(focusedPeriodIdx).setEndHour(value);
                        }
                    }
                } else if (focus == 2 || focus == 5) {
                    int input = Integer.parseInt(inputBuffer.toString());
                    if (input < 60) {
                        String value = df.format(input);
                        if (focus == 2) {
                            getTime(focusedPeriodIdx).setStartMin(value);
                        } else {
                            getTime(focusedPeriodIdx).setEndMin(value);
                        }
                    }
                }
                inputBuffer.setLength(0);
            }
        }
    }

    /**
     * Determine whether direct input mode is using or not.
     * @return true if direct input mode is using, false otherwise.
     */
    public boolean isInputMode() {
        if (inputBuffer.length() == 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get a value of direct input.
     * @return value which is inputed.
     */
    public String getInputValue() {
        return inputBuffer.toString();
    }

    /**
     * Add a new period.
     */
    private void addNewPeriod() {
        if (timeVector.size() < MAX_PERIODS) {
            Time time = new Time();

            timeVector.add(time);
            focusedPeriodIdx = timeVector.size() - 1;
            focus = 0;

            if (timeVector.size() == MAX_PERIODS) {
                clearButtons();
                addButton(BaseUI.BTN_BACK);
            }
            repaint();
        }
    }

    /**
     * Remove a current period.
     */
    private void removeCurrentPeriod() {
        if (focusedPeriodIdx < timeVector.size()) {
            timeVector.remove(focusedPeriodIdx);
            focusedPeriodIdx--;
            if (timeVector.size() > 0 && focusedPeriodIdx < 0) {
                focusedPeriodIdx = 0;
            }
            focus = 0;

            if (timeVector.size() < MAX_PERIODS) {
                clearButtons();
                addButton(BaseUI.BTN_BACK);
                addButton(BaseUI.BTN_ADD_PERIOD);
            }
        }
    }

    /**
     * Get a index who has focus on Screen(Not list). if index value is 0, a first period on screen has a focus, if
     * index value is 1, a second period on screen has a focus.
     * @return index of period to have a focus.
     */
    public int getWhoHasFocus() {
        int whoHasFocus = 0;
        if (timeVector.size() == 1) {
            whoHasFocus = 0;
        } else {
            if (focusedPeriodIdx + 1 == timeVector.size()) {
                whoHasFocus = 1;
            } else {
                whoHasFocus = 0;
            }
        }
        return whoHasFocus;
    }

    /**
     * Get a index where is time in Vector.
     * @param time want to know a index.
     * @return index of time.
     */
    public int getTimeDataIndex(Time time) {
        return timeVector.indexOf(time);
    }

    /**
     * Determine whether user can select a value for time to have a focus.
     * @return true if user can select a value, false otherwise.
     */
    public boolean canSelectMenu() {
        Time time = (Time) timeVector.get(focusedPeriodIdx);

        if (time.getActive().equals(Definitions.OPTION_VALUE_ENABLE)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get a index of focused period.
     * @return index of focused period.
     */
    public int getFocusedPeriodIdx() {
        return focusedPeriodIdx;
    }

    /**
     * Get a times to be display on current screen.
     * @return One or two times object.
     */
    public Time[] displayTimes() {

        if (timeVector.size() == 1) {
            return new Time[] {getTime(0)};
        } else {
            if (focusedPeriodIdx + 1 == timeVector.size()) {
                return new Time[] {getTime(focusedPeriodIdx - 1), getTime(focusedPeriodIdx)};
            } else {
                return new Time[] {getTime(focusedPeriodIdx), getTime(focusedPeriodIdx + 1)};
            }
        }
    }

    /**
     * Get a text for display.
     * @param days data string to want to know.
     * @return a string to be made with data string.
     */
    public String getDisplayText(String days) {
        return selectDaysUI.getDisplayText(days);
    }

    /**
     * Get a size of time data (vector size).
     * @return size of time data.
     */
    public int getTimeDataSize() {
        return timeVector.size();
    }

    /**
     * Get a Time for index.
     * @param idx index to get.
     * @return Time Time is in {@link #timeVector}
     */
    public Time getTime(int idx) {
        Log.printDebug("TimeBasedRestrictionUI: getTime(): idx " + idx);
        if (idx < timeVector.size()) {
            Time time = (Time) timeVector.get(idx);

            return time;
        }
        return null;
    }

    /**
     * Determine whether up arrow should draw or not.
     * @return true if up arrow should draw, false otherwise.
     */
    public boolean drawArrowUp() {
        if (timeVector.size() > 2 && focusedPeriodIdx > 0) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether down arrow should draw or not.
     * @return true if down arrow should draw, false otherwise.
     */
    public boolean drawArrowDown() {
        if (timeVector.size() > 2 && focusedPeriodIdx + 2 < timeVector.size()) {
            return true;
        }
        return false;
    }

    /**
     * Build a Time objects from Preference.
     */
    private void buildTimes() {
        timeVector.clear();

        String[] timeData = TextUtil.tokenize(pre.getCurrentValue(), '^');

        if (timeData != null && timeData.length > 0) {
            for (int i = 0; i < timeData.length; i++) {
                Time time = new Time();
                time.setData(timeData[i]);

                timeVector.add(time);
            }
        }
    }

    /**
     * Save a time of Time-based restrictions.
     */
    private void saveBlockTimes() {
        Time[] times = new Time[timeVector.size()];

        timeVector.copyInto(times);
        long currentTime = System.currentTimeMillis();
        StringBuffer sf = new StringBuffer();
        for (int i = 0; i < times.length; i++) {
            if (times[i].getDays().indexOf(Definitions.OPTION_VALUE_YES) == -1) {
                times[i].setDays(selectDaysUI.getSelectedValue(times[i].getDays()));
            }
            
            sf.append(times[i].getStringData());
            if (i + 1 < times.length) {
                sf.append("^");
            }
        }
        if (!sf.toString().equals(pre.getCurrentValue())) {
            sf.setLength(0);
            for (int i = 0; i < times.length; i++) {
                times[i].setSaveTime(currentTime);
                
                sf.append(times[i].getStringData());
                if (i + 1 < times.length) {
                    sf.append("^");
                }
            }
            
            STBTimerController.getInstance().setUpdatedBlockTime(true);
            DataManager.getInstance().setPreference(RightFilter.TIME_BASED_RESTRICTIONS, sf.toString());
        } else {
            STBTimerController.getInstance().setUpdatedBlockTime(false);
        }
    }

    /**
     * Return a display text according to be a selected days. ex) Every day, Weekend
     * @param time want to know.
     * @return The text to display for selected days.
     */
    public String getDaysText(Time time) {
        if (Log.DEBUG_ON) {
            Log.printDebug("time = " + time.getDays());
            Log.printDebug("select = " + selectDaysUI.getDisplayText(time.getDays()));
        }
        return selectDaysUI.getDisplayText(time.getDays());
    }

    /**
     * Handle a action after popup closed.
     * @param pop popup is closed.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        int action = ((Integer) msg).intValue();
        removePopup(pop);
        if (pop.equals(selectUI)) {
            if (action == BaseUI.ACTION_OK) {
                int selectedFocus = selectUI.getFocus();

                Time time = getTime(focusedPeriodIdx);
                if (focus == 0) {
                    if (selectedFocus == 0) {
                        time.setActive(Definitions.OPTION_VALUE_ENABLE);
                    } else {
                        time.setActive(Definitions.OPTION_VALUE_DISABLE);
                    }
                } else if (focus == 8) {
                    if (selectedFocus == 0) {
                        time.setRepeat(Definitions.OPTION_VALUE_YES);
                    } else {
                        time.setRepeat(Definitions.OPTION_VALUE_NO);
                    }
                    time.setSaveTime(System.currentTimeMillis());
                }
            }
            repaint();
        } else if (pop.equals(selectSmallUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectSmallUI.getSelectedValue();

                Time time = getTime(focusedPeriodIdx);
                if (focus == 1) { // start hour
                    if (isEnglish) {
                        if (selectedValue.equals("12")) {
                            time.setStartHour("00");
                        } else {
                            time.setStartHour(selectedValue);
                        }
                    } else {
                        int selectedInt = Integer.valueOf(selectedValue).intValue();
                        if (selectedInt > 11) {
                            selectedInt -= 12;
                            time.setStartHour(df.format(selectedInt));
                            time.setStartAmPm("pm");
                        } else {
                            time.setStartHour(selectedValue);
                            time.setStartAmPm("am");
                        }
                    }
                } else if (focus == 2) { // start min
                    time.setStartMin(selectedValue);
                } else if (focus == 3) { // start AM_PM
                    time.setStartAmPm(selectedValue);
                } else if (focus == 4) {
                    if (isEnglish) {
                        if (selectedValue.equals("12")) {
                            time.setEndHour("00");
                        } else {
                            time.setEndHour(selectedValue);
                        }
                    } else {
                        int selectedInt = Integer.parseInt(selectedValue);
                        if (selectedInt > 11) {
                            selectedInt -= 12;
                            time.setEndHour(df.format(selectedInt));
                            time.setEndAmPm("pm");
                        } else {
                            time.setEndHour(selectedValue);
                            time.setEndAmPm("am");
                        }
                    }
                } else if (focus == 5) {
                    time.setEndMin(selectedValue);
                } else if (focus == 6) {
                    time.setEndAmPm(selectedValue);
                }
            }
            repaint();
        } else if (pop.equals(selectDaysUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectDaysUI.getSelectedValue();
                Time time = getTime(focusedPeriodIdx);
                time.setDays(selectedValue);
            }
            repaint();
        }
    }
}

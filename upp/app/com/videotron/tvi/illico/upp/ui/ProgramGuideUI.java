/**
 * @(#)ProgramGuideUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Image;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.ProgramGuideRenderer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a preferences for Program guide.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class ProgramGuideUI extends BaseUI {

	/** Renderer. */
	private ProgramGuideRenderer renderer = new ProgramGuideRenderer();
	/** Explain UI at bottom . */
	private ExplainUI explainUI = new ExplainUI();
	/** Popup to select a value. */
	private PopSelectValueUI selectUI = new PopSelectValueUI();
	/** Popup to select a value by image. */
	private PopSelectValueByImageUI selectImgUI = new PopSelectValueByImageUI();
	/** Popup to select a channel . */
	private PopSelectChannelUI selectChannelUI = new PopSelectChannelUI();

	/** The id list for support DVR. */
	private final String[] preferenceIds1 = { PreferenceNames.POWER_ON_CHANNEL, PreferenceNames.FAVOURITE_CHANNEL,
			PreferenceNames.GRID_FORMAT, PreferenceNames.SD_HD_CHANNELS_GROUPING, PreferenceNames.AUTO_SHOW_DESCIRIPTION, 
			PreferenceNames.PIP_SIZE, PreferenceNames.PIP_POSITION, PreferenceNames.VIDEO_ZOOM_MODE };

	/** The id list for unsupported DVR. */
	private final String[] preferenceIds2 = { PreferenceNames.POWER_ON_CHANNEL, PreferenceNames.FAVOURITE_CHANNEL,
			PreferenceNames.GRID_FORMAT, PreferenceNames.SD_HD_CHANNELS_GROUPING, PreferenceNames.AUTO_SHOW_DESCIRIPTION, 
			PreferenceNames.VIDEO_ZOOM_MODE };

	/** Size array of PIP. */
	public final String[] pipSizeArray = { Definitions.PIP_SIZE_OVERLAY, Definitions.PIP_SIZE_SIDE_BY_SIDE };

	/** Position array of PIP. */
	public final String[] pipPositionArray = { Definitions.PIP_POSITION_TOP_LEFT, Definitions.PIP_POSITION_TOP_RIGHT,
			Definitions.PIP_POSITION_BOTTOM_LEFT, Definitions.PIP_POSITION_BOTTOM_RIGHT };

	/** Deciaml format for three size. */
	private DecimalFormat df = new DecimalFormat("000");
	/** Hashtable to have a preference for Program gudie. */
	private Hashtable preTable;

	/** Channel data from EPG via SharedMemory. */
	private Object[][] channelData;
	/** Channel data to be rearrange. */
	private Object[][] builtChannelData;
	/** Favourite channels. */
	private String[] favouriteChannels;
	/** Preferences array for current environment (PVR or Non-PVR). */
	private String[] currentPreferenceIds;

	/** Indicate whether enter from EPG. */
	private boolean isFromEPG;
	/** Application name if enter from other application. */
	private String fromName;

	/** String buffer to handle a direct input. */
	private StringBuffer numBuffer;
	/** Indicate whether current state is entering number. */
	private boolean isEnteringNumber;
	/** Vector to have a founded channels with direct input. */
	private Vector vFoundChannel;

	/** Clicking Effect. */
	private ClickingEffect clickEffect;

	private boolean isLoading;

	private Vector channelList = new Vector();;
	private Hashtable sisterTable = new Hashtable();
	Preference favouritePre;
	private Thread channelThread;
	
	// R7
	private int startIdx;

	/**
	 * Set a render and add explain UI.
	 */
	public ProgramGuideUI() {
		setRenderer(renderer);
		add(explainUI);
		clickEffect = new ClickingEffect(this, 5);
	}

	/**
	 * Prepare a UI.
	 */
	public void prepare() {

		String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
		String cId;
		if (params != null && params.length > 1) {
			DataCenter.getInstance().remove("PARAMS");
			isFromEPG = true;
			fromName = params[0];
			SubCategory sub = new SubCategory();
			sub.setCategoryId("program.guide");
			SubCategory sub2 = new SubCategory();
			sub2.setCategoryId("preference");
			addHistory(sub);
			addHistory(sub2);
			cId = "program.guide";
		} else {
			isFromEPG = false;
			fromName = null;
			cId = ((Category) peekHistory()).getCategoryId();
		}

		preTable = DataManager.getInstance().getPreferences(cId);

		channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);

		if (Environment.SUPPORT_PIP) {
			currentPreferenceIds = preferenceIds1;
		} else {
			currentPreferenceIds = preferenceIds2;
		}
		super.prepare();
	}

	/**
	 * Return a index of Favourite channels list.
	 * 
	 * @param name
	 *            The call letter
	 * @return return index, -1 means that channel is not a favourite channel.
	 */
	private int getFavouriteChannelIdx(String name) {
		for (int i = 0; i < favouriteChannels.length; i++) {
			if (favouriteChannels[i].equals(name)) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Start a UI.
	 */
	public void start(boolean back) {
		if (favouritePre == null) {
			favouritePre = DataManager.getInstance().getPreference(PreferenceNames.FAVOURITE_CHANNEL);
		}
		favouriteChannels = TextUtil.tokenize(favouritePre.getCurrentValue(), PreferenceService.PREFERENCE_DELIMETER);
		
		if (!back) {
			prepare();

			focus = 0;
			// R7
			startIdx = 0;
			preparePowerOnChannel();
			
		}
		addButton(BTN_BACK);
		setExplainText();
		isEnteringNumber = false;

		PreferenceActionController.getInstance().checkScreenConfiguration();

		super.start();
	}

	/**
	 * Stop a UI.
	 */
	public void stop() {
		clearPopup();
		clearButtons();
		explainUI.stop();
		numBuffer = null;
		isEnteringNumber = false;

		selectChannelUI.setRunningAutoFinder(false);
		favouritePre = null;
		super.stop();
		
		PreferenceController.getInstance().hideLoading();
		if (channelThread != null) {
			channelThread.interrupt();
		}
		channelThread = null;

		// remove - need those when came back from favorite channel
//		channelList.clear();
//		sisterTable.clear();
//		builtChannelData = null;
	}

	/**
	 * Get a Hashtable to have a prefrences.
	 * 
	 * @return
	 */
	public Hashtable getPreferences() {
		return preTable;
	}

	/**
	 * Get a id of preferences.
	 * 
	 * @return return ids.
	 */
	public String[] getPreferenceIds() {
		return currentPreferenceIds;
	}

	/**
	 * Handle a key code.
	 * 
	 * @param code
	 *            key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {

		if (getPopup() != null) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_0:
		case OCRcEvent.VK_1:
		case OCRcEvent.VK_2:
		case OCRcEvent.VK_3:
		case OCRcEvent.VK_4:
		case OCRcEvent.VK_5:
		case OCRcEvent.VK_6:
		case OCRcEvent.VK_7:
		case OCRcEvent.VK_8:
		case OCRcEvent.VK_9:
			// R7
			int currentFocus = startIdx + focus;
			if (currentPreferenceIds[currentFocus].equals(PreferenceNames.POWER_ON_CHANNEL)) {
				numericKeys(code);
				repaint();
			}
			return true;
		case OCRcEvent.VK_UP:
			numBuffer = null;
			isEnteringNumber = false;
			
			// R7
			if (startIdx > 0 && focus > 3) {
				focus--;
			} else if (startIdx > 0) {
				startIdx--;
			} else if (startIdx == 0 && focus > 0) {
				focus--;
			}
			
			setExplainText();
			repaint();
			return true;
		case OCRcEvent.VK_DOWN:
			numBuffer = null;
			isEnteringNumber = false;
			
			// R7
			if (focus < 3) {
				focus++;
			} else if (startIdx + 6 < currentPreferenceIds.length - 1) {
				startIdx++;
			} else if (startIdx + focus < currentPreferenceIds.length - 1) {
				focus++;
			}
//			if (++focus == currentPreferenceIds.length) {
//				focus = currentPreferenceIds.length - 1;
//			}
			setExplainText();
			repaint();
			return true;
		case OCRcEvent.VK_ENTER:
			if (isEnteringNumber) {
				String enteredNumber = numBuffer.toString();
				for (int i = 0; i < vFoundChannel.size(); i++) {
					Object[] cData = (Object[]) vFoundChannel.get(i);
					String number = ((Integer) cData[BaseUI.INDEX_CHANNEL_NUMBER]).toString();
					String newNum = df.format(Integer.parseInt(number));
					int enterdInt = Integer.parseInt(enteredNumber);
					int numberInt = Integer.parseInt(number);

					if (enterdInt == numberInt || enteredNumber.equals(number) || enteredNumber.equals(newNum)) {
						EpgService eService = CommunicationManager.getInstance().getEpgService();
						String callLetter = (String) cData[INDEX_CHANNEL_NAME];
						
						if (PreferenceController.getInstance().isChannelGrouped()) {
							try {
								TvChannel selectChannel = eService.getChannel(callLetter);
								TvChannel sisterChannel = selectChannel.getSisterChannel();
								if (selectChannel.isHd() && selectChannel.isSubscribed()) {
									callLetter = selectChannel.getCallLetter();
								} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
									callLetter = sisterChannel.getCallLetter();
								}
	
							} catch (Exception ex) {
								Log.printError("powerOnChannel, " + ex.getMessage());
							}
						}

						Preference pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);
						pre.setCurrentValue(callLetter);

						DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL, callLetter);
						break;
					} else {
						// check sister
						String callLetter = (String) cData[INDEX_CHANNEL_NAME];
						if (sisterTable.get(callLetter) != null) {
							Object[] sisterChannelData = (Object[]) sisterTable.get(callLetter);
							int sisterNumber = ((Integer)sisterChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
							if ( sisterNumber == enterdInt) {
								EpgService eService = CommunicationManager.getInstance().getEpgService();

								try {
									TvChannel selectChannel = eService.getChannel(callLetter);
									TvChannel sisterChannel = selectChannel.getSisterChannel();
									if (selectChannel.isHd() && selectChannel.isSubscribed()) {
										callLetter = selectChannel.getCallLetter();
									} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
										callLetter = sisterChannel.getCallLetter();
									}

								} catch (Exception ex) {
									Log.printError("powerOnChannel, " + ex.getMessage());
								}

								Preference pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);
								pre.setCurrentValue(callLetter);

								DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL, callLetter);
								
								break;
							}
						}
					}
				}

				numBuffer = null;
				isEnteringNumber = false;
				repaint();
			} else {
				
				currentFocus = startIdx + focus;
				
				if (currentFocus < 5) {
					clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37 - 14, 281, 33);
				} else {
					clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37 + 10 - 14, 281, 33);
				}
				
				if (currentPreferenceIds[currentFocus].equals(PreferenceNames.FAVOURITE_CHANNEL)) {
					Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
					addHistory(pre);
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_FAVOURITE_CHANNEL, false);
				} else if (currentPreferenceIds[currentFocus].equals(PreferenceNames.PIP_SIZE)) {
					setPopup(selectImgUI);
					Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
					selectImgUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(),
							selectImgUI.STATE_PIP_SIZE, 585, 71 + focus * 37 + 10);
					repaint();
				} else if (currentPreferenceIds[currentFocus].equals(PreferenceNames.PIP_POSITION)) {
					setPopup(selectImgUI);
					Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
					selectImgUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(),
							selectImgUI.STATE_PIP_POSITION, 585, 71 + focus * 37 + 10);
					repaint();
				} else if (currentPreferenceIds[currentFocus].equals(PreferenceNames.POWER_ON_CHANNEL)) {
					// // build channel list
					// Hashtable caTable = (Hashtable)
					// SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);
					//
					// if (Log.DEBUG_ON) {
					// Log.printDebug("ProgramGuideUI: caTable size is " +
					// caTable.size());
					// }
					//
					// Vector vChannels = new Vector();
					// Vector vOtherChannel = new Vector();
					// vChannels.add(new Object[] {TextUtil.EMPTY_STRING,
					// Definitions.LAST_CHANNEL});
					//
					// // Waring : ONLY comment off a log for Debug. if comment
					// on a log, it will be make too slowly.
					// for (int i = 0; i < channelData.length; i++) {
					// boolean isSubscribed = true;
					// if (caTable != null) {
					//
					// Integer sourceId = (Integer)
					// channelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];
					//
					// Boolean subscribed = (Boolean) caTable.get(sourceId);
					//
					// if (subscribed != null) {
					// isSubscribed = subscribed.booleanValue();
					// }
					// }
					//
					// Integer chType = (Integer)
					// channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
					//
					// if (chType.intValue() != TvChannel.TYPE_VIRTUAL &&
					// isSubscribed) {
					//
					// int idx = getFavouriteChannelIdx(((String)
					// channelData[i][BaseUI.INDEX_CHANNEL_NAME]));
					// if (idx > -1) {
					// if (idx + 1 > vChannels.size()) {
					// vChannels.add(channelData[i]);
					// } else {
					// vChannels.add(idx + 1, channelData[i]);
					// }
					// } else {
					// vOtherChannel.add(channelData[i]);
					// }
					// }
					// }
					//
					// for (int i = 0; i < vOtherChannel.size(); i++) {
					// vChannels.add(vOtherChannel.get(i));
					// }
					//
					// builtChannelData = new Object[vChannels.size()][];
					// vChannels.copyInto(builtChannelData);

					Preference pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);

					setPopup(selectChannelUI);
					selectChannelUI.setSisterTable(sisterTable);
					selectChannelUI.show(this, builtChannelData, pre.getCurrentValue(), pre.getName(),
							favouriteChannels, 585, 86, 9);
					repaint();
				} else {
					int step = 0;
					if (currentFocus > 5) {
						step = 10 - 14;
					} else {
						step = -14;
					}
					setPopup(selectUI);
					Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
					selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519, 83
							+ focus * 37 + step, 3);
					repaint();
				}
			}
			return true;
		case KeyCodes.LAST:
			if (isFromEPG) {
				popHistory();
				MonitorService mService = CommunicationManager.getInstance().getMonitorService();

				if (fromName != null) {
					String[] parameters = new String[1];
					parameters[0] = MonitorService.REQUEST_APPLICATION_LAST_KEY;

					try {
						mService.startUnboundApplication(fromName, parameters);
					} catch (RemoteException e) {
						Log.print(e);
					}
				} else {
					try {
						mService.exitToChannel();
					} catch (RemoteException e) {
						Log.print(e);
					}
				}
				return true;
			} else {
				clickButtons(BTN_BACK);
				// startFadeOutEffect(this);
				popHistory();
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
				return true;
			}
		default:
			return false;
		}
	}

	/**
	 * Process a numeric keys for Power On channel.
	 * 
	 * @param code
	 *            The key code to be inputed.
	 */
	private void numericKeys(int code) {
		int number = code - OCRcEvent.VK_0;
		// build channel list
		Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

		if (vFoundChannel == null) {
			vFoundChannel = new Vector();
		}

		if (numBuffer == null) {
			numBuffer = new StringBuffer();
			vFoundChannel.clear();
		}

		numBuffer.append(number);

		if (numBuffer.length() > 4) {
			isEnteringNumber = false;
			numBuffer = null;
			return;
		} else {
			isEnteringNumber = true;
		}

		String enterNumber = numBuffer.toString();

		vFoundChannel.clear();
		for (int i = 1; i < builtChannelData.length; i++) {
			boolean isSubscribed = true;
			if (caTable != null) {

				Integer sourceId = (Integer) builtChannelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];

				Boolean subscribed = (Boolean) caTable.get(sourceId);

				if (subscribed != null) {
					isSubscribed = subscribed.booleanValue();
				}
			}

			Integer chType = (Integer) builtChannelData[i][BaseUI.INDEX_CHANNEL_TYPE];

			if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed) {

				String chNum = ((Integer) builtChannelData[i][BaseUI.INDEX_CHANNEL_NUMBER]).toString();
				Log.printDebug("ProgramGuildUI, numericKeys, chNum=" + chNum);
				if (chNum.startsWith(enterNumber)) {
					vFoundChannel.add(builtChannelData[i]);
				} else if (builtChannelData.equals("0") && chNum.length() < 4) {
					vFoundChannel.add(builtChannelData[i]);
				} else if (builtChannelData.equals("00") && chNum.length() < 2) {
					vFoundChannel.add(builtChannelData[i]);
				} else {
					String newChNum = df.format(Integer.parseInt(chNum));

					if (newChNum.indexOf(enterNumber) > -1) {
						vFoundChannel.add(builtChannelData[i]);
					}
				}
				
				// check sister
				if (!vFoundChannel.contains(builtChannelData[i])) {
					EpgService epgService = CommunicationManager.getInstance().getEpgService();
					try {
						TvChannel channel = epgService.getChannel((String) builtChannelData[i][INDEX_CHANNEL_NAME]);
						Object[] sisChannelData = (Object[]) sisterTable.get(channel.getCallLetter());
						
						if (sisChannelData != null) {
							chNum = String.valueOf(((Integer)sisChannelData[BaseUI.INDEX_CHANNEL_NUMBER]).intValue());
							Log.printDebug("ProgramGuildUI, numericKeys, sister chNum=" + chNum);
							if (chNum.startsWith(enterNumber)) {
								vFoundChannel.add(builtChannelData[i]);
							} else if (builtChannelData.equals("0") && chNum.length() < 4) {
								vFoundChannel.add(builtChannelData[i]);
							} else if (builtChannelData.equals("00") && chNum.length() < 2) {
								vFoundChannel.add(builtChannelData[i]);
							} else {
								String newChNum = df.format(Integer.parseInt(chNum));
	
								if (newChNum.indexOf(enterNumber) > -1) {
									vFoundChannel.add(builtChannelData[i]);
								}
							}
						}
					} catch (RemoteException e) {
						e.printStackTrace();
					}
				}

				if (vFoundChannel.size() > 9) {
					break;
				}
			}
		}
	}

	/**
	 * Return a Vector to include a founded channels.
	 * 
	 * @return Vector to include a founded channels.
	 */
	public Vector getFoundChannel() {
		return vFoundChannel;
	}

	/**
	 * Determine whether current state is entering number or not.
	 * 
	 * @return true if is entering number, false otherwise.
	 */
	public boolean isEnteringNumber() {
		return isEnteringNumber;
	}

	/**
	 * Get a entered number.
	 * 
	 * @return entered number.
	 */
	public String getEnteringNumber() {
		if (numBuffer != null) {
			return numBuffer.toString();
		}
		return TextUtil.EMPTY_STRING;
	}
	
	// R7
    public int getStartIdx() {
        return startIdx;
    }

	/**
	 * Get a text to be displayed for favourite channels.
	 * 
	 * @param idx
	 *            currently onley 1
	 * @return text to be displayed.
	 */
	public String getPreferenceValueText(int idx) {
		StringBuffer sf = new StringBuffer();
		if (idx == 1) { // Favourite channel
			String value = favouritePre.getCurrentValue();
			int length = (TextUtil.tokenize(value, "|")).length;

			sf.append(length);
			sf.append(" ");
			if (length > 1) {
				sf.append(BaseUI.getMenuText(KeyNames.SUB_FIX_FAVOURITE_CHANNELS));
			} else {
				sf.append(BaseUI.getMenuText(KeyNames.SUB_FIX_FAVOURITE_CHANNEL));
			}
		}

		return sf.toString();
	}

	/**
	 * Set a explain text for focused preference.
	 */
	private void setExplainText() {
		int currentFocus = startIdx + focus;
		String text = getExplainText(currentPreferenceIds[currentFocus]);
		explainUI.setText(text);
		explainUI.start(false);
	}

	public Hashtable getSisterTable() {
		return sisterTable;
	}

	/**
	 * Handle a action after popup closed.
	 * 
	 * @param pop
	 *            popup to be closed.
	 * @param msg
	 *            message from popup.
	 */
	public void popupClosed(BaseUI pop, Object msg) {

		if (pop.equals(selectUI)) {
			int action = ((Integer) msg).intValue();
			removePopup(selectUI);
			if (action == BaseUI.ACTION_OK) {
				String selectedValue = selectUI.getSelectedValue();

				int currentFocus = startIdx + focus;
				Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
				pre.setCurrentValue(selectedValue);
				preTable.put(pre.getID(), pre);
				DataManager.getInstance().setPreference(pre.getID(), selectedValue);
				repaint();
				if (pre.getID().equals(PreferenceNames.VIDEO_ZOOM_MODE)) {
					PreferenceActionController.getInstance().doAction(pre.getID(), selectedValue);
				}
				
				// R7
				if (pre.getID().equals(PreferenceNames.SD_HD_CHANNELS_GROUPING)) {
					PreferenceController.getInstance().updateChannelPreferences();
					preparePowerOnChannel();
					PreferenceController.getInstance().updatePowerOnChannel();
					pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);
					Preference originPre = DataManager.getInstance().getPreference(PreferenceNames.POWER_ON_CHANNEL);
					pre.setCurrentValue(originPre.getCurrentValue());
					
					repaint();
				}

			} else {
				repaint();
			}
		} else if (pop.equals(selectImgUI)) {
			int action = ((Integer) msg).intValue();
			removePopup(selectImgUI);
			if (action == BaseUI.ACTION_OK) {
				String selectedValue = selectImgUI.getSelectedValue();

				int currentFocus = startIdx + focus;
				Preference pre = (Preference) preTable.get(currentPreferenceIds[currentFocus]);
				pre.setCurrentValue(selectedValue);
				preTable.put(pre.getID(), pre);
				DataManager.getInstance().setPreference(pre.getID(), selectedValue);
			}
			repaint();
		} else if (pop.equals(selectChannelUI)) {
			int action = ((Integer) msg).intValue();
			removePopup(selectChannelUI);

			if (action == BaseUI.ACTION_OK) {
				Object[] selectedValue = (Object[]) selectChannelUI.getSelectedValue();
				String name = (String) selectedValue[BaseUI.INDEX_CHANNEL_NAME];

				if (PreferenceController.getInstance().isChannelGrouped()) {
					EpgService eService = CommunicationManager.getInstance().getEpgService();
	
					try {
						TvChannel selectChannel = eService.getChannel(name);
						TvChannel sisterChannel = selectChannel.getSisterChannel();
						if (selectChannel.isHd() && selectChannel.isSubscribed()) {
							name = selectChannel.getCallLetter();
						} else if (sisterChannel != null && sisterChannel.isSubscribed()) {
							name = sisterChannel.getCallLetter();
						}
	
					} catch (Exception ex) {
						Log.printError("powerOnChannel, " + ex.getMessage());
					}
				}

				Preference pre = (Preference) preTable.get(PreferenceNames.POWER_ON_CHANNEL);
				pre.setCurrentValue(name);

				DataManager.getInstance().setPreference(PreferenceNames.POWER_ON_CHANNEL, name);

			}
			repaint();
		}
	}
	
	private void preparePowerOnChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("ProgramGuideUI, preparePowerOnChannel");
		}
		channelThread = new Thread(new Runnable() {
			public void run() {
				
				if (PreferenceController.getInstance().isChannelGrouped()) {
					try {
						channelList.clear();
						sisterTable.clear();
						builtChannelData = null;
						
						isLoading = true;
						PreferenceController.getInstance().showLoading();
	
						channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);
						Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);
	
						if (Log.DEBUG_ON) {
							Log.printDebug("ProgramGuideUI: channelThread caTable size is " + caTable.size());
						}
	
						Vector normalChannelVec = new Vector();
	
						channelList.add(new Object[] { new Integer(0), Definitions.LAST_CHANNEL });
	
						for (int i = 0; i < channelData.length; i++) {
	
							boolean isSubscribed = true;
							if (caTable != null) {
	
								Integer sourceId = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];
	
								Boolean subscribed = (Boolean) caTable.get(sourceId);
	
								if (subscribed != null) {
									isSubscribed = subscribed.booleanValue();
								}
							}
	
							Integer chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
							
							String callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
							String sisterCalletterStr = (String) channelData[i][BaseUI.INDEX_CHANNEL_SISTER_NAME];
							if (chType.intValue() != TvChannel.TYPE_VIRTUAL 
									&& chType.intValue() != TvChannel.TYPE_TECH 
									&& isSubscribed
									&& (sisterCalletterStr == null || sisterTable.get(sisterCalletterStr) == null)) {
	
								int idx = getFavouriteChannelIdx(callLetterStr);
	
								if (idx > -1) {
									if (idx + 1 > channelList.size()) {
										channelList.add(channelData[i]);
									} else {
										channelList.add(idx + 1, channelData[i]);
									}
								} else {
									normalChannelVec.add(channelData[i]);
								}
							}
	
							// if sister channel exist, put in sisterTable
							if (sisterCalletterStr != null && !sisterTable.containsKey(sisterCalletterStr)) {
								for (int sisterIdx = 0; sisterIdx < channelData.length; sisterIdx++) {
									if (sisterCalletterStr.equals(channelData[sisterIdx][BaseUI.INDEX_CHANNEL_NAME])) {
										sisterTable.put(callLetterStr, channelData[sisterIdx]);
										break;
									}
								}
							}
						}
	
						for (int i = 0; i < normalChannelVec.size(); i++) {
							channelList.add(normalChannelVec.get(i));
						}
	
						normalChannelVec.clear();
	
						builtChannelData = new Object[channelList.size()][];
						for (int i = 0; i < channelList.size(); i++) {
							builtChannelData[i] = (Object[]) channelList.get(i);
						}
						
						if (Log.DEBUG_ON) {
							if (builtChannelData != null) {
								Log.printDebug("ProgramGuideUI: channelThread, builtChannelData : " + builtChannelData.length);
							} else {
								Log.printDebug("ProgramGuideUI: channelThread, builtChannelData is null");
							}
						}
	
						isLoading = false;
						repaint(); 
					} catch (Exception e) {
						e.printStackTrace();
					}
					PreferenceController.getInstance().hideLoading();
				} else {
					
					sisterTable.clear();
					Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

                    if (Log.DEBUG_ON) {
                        Log.printDebug("ProgramGuideUI: channelThread, caTable size is " + caTable.size());
                    }

                    Preference favouritePre = DataManager.getInstance()
                            .getPreference(PreferenceNames.FAVOURITE_CHANNEL);
                    favouriteChannels = TextUtil.tokenize(favouritePre.getCurrentValue(),
                            PreferenceService.PREFERENCE_DELIMETER);

                    Vector vChannels = new Vector();
                    Vector vOtherChannel = new Vector();
                    // VDTRMASTER-5789
                    vChannels.add(new Object[] { new Integer(0), Definitions.LAST_CHANNEL});

                    // Waring : ONLY comment off a log for Debug. if comment on a log, it will be make too slowly.
                    for (int i = 0; i < channelData.length; i++) {
                        boolean isSubscribed = true;
                        if (caTable != null) {

                            Integer sourceId = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];

                            Boolean subscribed = (Boolean) caTable.get(sourceId);

                            if (subscribed != null) {
                                isSubscribed = subscribed.booleanValue();
                            }
                        }

                        Integer chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];

                        if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed) {

                            int idx = getFavouriteChannelIdx(((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]));
                            if (idx > -1) {
                                if (idx + 1 > vChannels.size()) {
                                    vChannels.add(channelData[i]);
                                } else {
                                    vChannels.add(idx + 1, channelData[i]);
                                }
                            } else {
                                vOtherChannel.add(channelData[i]);
                            }
                        }
                    }

                    Integer chType;
                    for (int i = 0; i < vOtherChannel.size(); i++) {
                    	chType = (Integer)((Object[])vOtherChannel.get(i))[BaseUI.INDEX_CHANNEL_TYPE];
						if (chType.intValue() == TvChannel.TYPE_TECH)
							continue;
						
                        vChannels.add(vOtherChannel.get(i));
                    }

                    builtChannelData = new Object[vChannels.size()][];
                    vChannels.copyInto(builtChannelData);
				}
			}
		});
		
		channelThread.start();
		
	}
}

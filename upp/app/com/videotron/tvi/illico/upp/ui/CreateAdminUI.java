/**
 * @(#)CreateAdminUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.controller.PinEnablerController;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.data.UserManager;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class offer a UI to create a admin PIN.
 * @author Woojung Kim
 * @version 1.1
 */
public class CreateAdminUI extends BaseUI {

    /** A renderer. */
    private CreateAdminRenderer renderer = new CreateAdminRenderer();
    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;
    /** Constant to indicate that notification is displayed. */
    private final int STATE_NOTIFICATION = 0;
    /** Constant to indicate that To create a admin PIn is desplayed. */
    private final int STATE_CREATION = 1;
    /** Constant to indicate a max length of PIN. */
    private final int PIN_MAX_LENGTH = 4;
    /** String for new PIN. */
    private String newPinString;
    /** String for repeat PIN. */
    private String repeatPinString;
    /** Indicate whether text is displayed with red. */
    private boolean drawRedText;
    /** The explain text. */
    private String[] explain;
    /** The explain text for create. */
    private String[] createExplain;
    /** The explain text for not matched. */
    private String[] notMatchedExplain;
    /** The explain text for Must differ from Family PIN. */
    private String[] differFromFamily;

    /**
     * Set a renderer.
     */
    public CreateAdminUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Start a UI.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CreateAdminUI: start()");
        }
        prepare();
        
        String tempExplain = getPopupText(KeyNames.EXPLAIN_CREATE_ADMIN);
        explain = TextUtil.split(tempExplain, renderer.fm18, 320, '|');

        tempExplain = getPopupText(KeyNames.EXPLAIN_CREATE_ADMIN_PIN);
        createExplain = TextUtil.split(tempExplain, renderer.fm18, 320, '|');
        tempExplain = getPopupText(KeyNames.EXPLAIN_NOT_MATCHED_ADMIN_NEW);
        notMatchedExplain = TextUtil.split(tempExplain, renderer.fm18, 300, '|');
        tempExplain = BaseUI.getPopupText(KeyNames.EXPLAIN_ADMIN_DIFFER_FROM_FAMILY);
        differFromFamily = TextUtil.split(tempExplain, renderer.fm18, 350, '|');
        
        focus = 0;
        state = STATE_NOTIFICATION;
        newPinString = TextUtil.EMPTY_STRING;
        repeatPinString = TextUtil.EMPTY_STRING;

        String title = getPopupText(KeyNames.TITLE_CREATE_ADMIN_PIN);
        renderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
        drawRedText = false;
        setVisible(true);
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            if (state == STATE_CREATION) {
                if (focus == 0 && newPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(newPinString);
                    sf.append(code - KeyEvent.VK_0);
                    newPinString = sf.toString();

                    if (newPinString.length() == PIN_MAX_LENGTH) {
                        User family = UserManager.getInstance().loadUser(User.ID_FAMILY);
                        if (family.hasPin() && family.isValidate(newPinString)) {
                            drawRedText = true;
                            explain = differFromFamily;
                            newPinString = TextUtil.EMPTY_STRING;
                            repeatPinString = TextUtil.EMPTY_STRING;
                            focus = 0;
                        } else {
                            focus = 1;
                        }
                        
                    }
                    repaint();
                } else if (focus == 1 && repeatPinString.length() < PIN_MAX_LENGTH) {
                    StringBuffer sf = new StringBuffer(repeatPinString);
                    sf.append(code - KeyEvent.VK_0);
                    repeatPinString = sf.toString();

                    if (repeatPinString.length() == PIN_MAX_LENGTH) {
                        if (newPinString.equals(repeatPinString)) {
                            focus = 3;
                        } else {
                            drawRedText = true;
                            explain = notMatchedExplain;
                            newPinString = TextUtil.EMPTY_STRING;
                            repeatPinString = TextUtil.EMPTY_STRING;
                            focus = 0;
                        }
                    }
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_LEFT:
            if (state == STATE_NOTIFICATION) {
                if (focus == 1) {
                    focus = 0;
                    repaint();
                }
            } else {
                if (focus == 3 && newPinString.length() != 0) {
                    focus = 2;
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (state == STATE_NOTIFICATION) {
                if (focus == 0) {
                    focus = 1;
                    repaint();
                }
            } else {
                if (focus == 2) {
                    focus = 3;
                    repaint();
                }
            }
            return true;
        case OCRcEvent.VK_UP:
            if (state == STATE_CREATION) {
                if (focus == 2 && newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH) {
                    focus = 0;
                    newPinString = TextUtil.EMPTY_STRING;
                    repeatPinString = TextUtil.EMPTY_STRING;
                } else if (focus == 2) {
                    if (newPinString.length() == PIN_MAX_LENGTH) {
                        focus = 1;
                    } else {
                        focus = 0;
                    }
                } else {
                    if (newPinString.length() != PIN_MAX_LENGTH) {
                        focus = 0;
                    } else if (newPinString.length() != PIN_MAX_LENGTH) {
                        focus = 1;
                    }
                }
                repaint();
            }
            return true;
        case OCRcEvent.VK_DOWN:
            if (state == STATE_CREATION) {
                if (newPinString.length() == 0) {
                    focus = 3;
                } else {
                    focus = 2;
                }
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (state == STATE_NOTIFICATION) {
                if (focus == 0) {
                    clickingAnimation();
                    state = STATE_CREATION;
                    String title = getPopupText(KeyNames.TITLE_CREATE_ADMIN_PIN);
                    renderer.setTitle(title, PopBaseRenderer.ICON_TYPE_NONE);
                    explain = createExplain;
                    repaint();
                } else if (focus == 1) {
                    clickingAnimation();
                    PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
                }
            } else if (state == STATE_CREATION) {
                if (focus == 3 && newPinString.length() == PIN_MAX_LENGTH && repeatPinString.length() == PIN_MAX_LENGTH) {
                    clickingAnimation();
                    if (newPinString.equals(repeatPinString)) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("CreateAdminUI: save a new PIN" + newPinString);
                        }
                        User user = UserManager.getInstance().loadUser(User.ID_ADMIN);
                        user.changePin(newPinString);
                        PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_SUCCESS,
                                user.getUserId());
                    } else {
                        newPinString = TextUtil.EMPTY_STRING;
                        repeatPinString = TextUtil.EMPTY_STRING;
                        focus = 0;
                        repaint();
                    }
                } else if (focus == 2) {
                    clickingAnimation();
                    newPinString = TextUtil.EMPTY_STRING;
                    repeatPinString = TextUtil.EMPTY_STRING;
                    focus = 0;
                    repaint();
                } else if (repeatPinString.length() < PIN_MAX_LENGTH && focus == 3) {
                    clickingAnimation();
                    PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
                }
            }
            return true;
        case OCRcEvent.VK_EXIT:
            PinEnablerController.getInstance().notifyPinEnablerResult(PreferenceService.RESPONSE_CANCEL, null);
            return true;
        default:
            return true;
        }
    }

    /**
     * Animate a buttons when select.
     */
    private void clickingAnimation() {
        if (state == STATE_NOTIFICATION) {
            if (focus == 0) {
                clickEffect.start(321, 353, 162, 40);
            } else {
                clickEffect.start(485, 353, 162, 40);
            }
        } else if (state == STATE_CREATION) {
            if (focus == 2) {
                clickEffect.start(321, 353, 162, 40);
            } else if (focus == 3) {
                clickEffect.start(485, 353, 162, 40);
            }
        }
    }

    /**
     * This class render a input to create a admin PIN.
     * @author Woojung Kim
     * @version 1.1
     */
    class CreateAdminRenderer extends PopBaseRenderer {
        /** The gap of each pin box. */
        private static final int PIN_GAP = 38;

        /** The image of pin box. */
        private Image pinBoxImage = DataCenter.getInstance().getImage("05_pin_box.png");
        /** The image of pin focus. */
        private Image pinFocusImage = DataCenter.getInstance().getImage("05_pin_focus.png");
        /** The image of button when not selected. */
        private Image btnDimImage = DataCenter.getInstance().getImage("05_focus_dim.png");
        /** The image of button when can't select. */
        private Image btnOffImage = dataCenter.getImage("05_focus_off.png");
        /** The image of button when selected. */
        private Image btnFocusImage = DataCenter.getInstance().getImage("05_focus.png");
        /** The image of background. */
//        private Image bgImg = dataCenter.getImage("pop_d_glow_461.png");
        /** The image of shadow. */
//        private Image shadowImg = dataCenter.getImage("pop_sha.png");
        /** The image of high. */
//        private Image highImg = dataCenter.getImage("pop_high_402.png");
        /** The image of gap. */
        private Image gapImg = dataCenter.getImage("pop_gap_379.png");
        /** The image of green icon. */
        private Image iconGreenCheckImg = dataCenter.getImage("icon_g_check.png");
        /** The image of rend icon. */
        private Image iconRedXImg = dataCenter.getImage("icon_r_x.png");

        /** The color of button strings. */
        private Color btnColor = new Color(3, 3, 3);
        /** The color of sub title (182, 182, 182). */
        private Color subColor = new Color(182, 182, 182);
        /** The color of red text (248, 63, 63). */
        private Color redColor = new Color(248, 63, 63);
        /** The font of size 20. */
        private Font f20 = FontResource.BLENDER.getFont(20);
        /** The font of size 24. */
        private Font f24 = FontResource.BLENDER.getFont(24);
        /** The PIN to be inputed. */
        private String pinCode;
        /** The PIN to be inputed repeatly. */
        private String confirmPinCode;

        /** The button names. */
        private String btnNow, btnLater, btnCancel, btnClear, btnConfirm;

        /**
         * Prepare a UI.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {

            pinCode = getPopupText(KeyNames.SUBTITLE_PIN_CODE);
            confirmPinCode = getPopupText(KeyNames.SUBTITLE_CONFIRM_PIN_CODE);

            btnNow = getPopupText(KeyNames.BTN_NOW);
            btnLater = getPopupText(KeyNames.BTN_LATER);
            btnCancel = getPopupText(KeyNames.BTN_CANCEL);
            btnClear = getPopupText(KeyNames.BTN_CLEAR);
            btnConfirm = getPopupText(KeyNames.BTN_CONFIRM);
        }

        /**
         * Paint a items.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            // background
//            g.drawImage(bgImg, 250, 113, c);
//            g.drawImage(shadowImg, 280, 399, 402, 79, c);
            g.setColor(fillRectColor);
            g.fillRect(280, 143, 402, 259);
            g.drawImage(gapImg, 292, 181, c);
//            g.drawImage(highImg, 280, 143, c);

            g.setColor(titleColor);
            g.setFont(f24);
            GraphicUtil.drawStringCenter(g, title, 480, 170);

            g.setColor(Color.WHITE);
            g.setFont(f18);
            int posY;
            if (state == STATE_NOTIFICATION) {
                if (explain.length == 1) {
                    posY = 227;
                } else {
                    posY = 218;
                }

                for (int i = 0; i < explain.length; i++) {
                    GraphicUtil.drawStringCenter(g, explain[i], 482, posY + i * 20);
                }

                if (getFocus() == 0) {
                    g.drawImage(btnFocusImage, 321, 353, c);
                    g.drawImage(btnDimImage, 485, 353, c);
                } else if (getFocus() == 1) {
                    g.drawImage(btnDimImage, 321, 353, c);
                    g.drawImage(btnFocusImage, 485, 353, c);
                }
                g.setFont(f18);
                g.setColor(btnColor);
                GraphicUtil.drawStringCenter(g, btnNow, 400, 375);
                GraphicUtil.drawStringCenter(g, btnLater, 564, 375);
            } else if (state == STATE_CREATION) {
                if (drawRedText) {
                    g.setColor(redColor);
                } else {
                    g.setColor(Color.WHITE);
                }
                if (explain.length == 1) {
                    posY = 227;
                } else {
                    posY = 218;
                }

                for (int i = 0; i < explain.length; i++) {
                    int x = GraphicUtil.drawStringCenter(g, explain[i], 482, posY + i * 20);
                    if (i == 0 && drawRedText) {
                        g.drawImage(iconRedXImg, x - 28, posY - 16, c);
                    }
                }

                g.setFont(f17);
                g.setColor(subColor);
                g.drawString(pinCode, 326, 281);
                g.drawString(confirmPinCode, 326, 320);

                // PIN Box
                g.setFont(f24);
                for (int i = 0; i < 4; i++) {
                    int step = i * PIN_GAP;
                    if (focus == 0 && newPinString.length() == i) {
                        g.drawImage(pinFocusImage, 458 + step, 261, c);
                    } else {
                        g.drawImage(pinBoxImage, 458 + step, 261, c);
                    }

                    if (focus == 1 && repeatPinString.length() == i) {
                        g.drawImage(pinFocusImage, 458 + step, 299, c);
                    } else {
                        g.drawImage(pinBoxImage, 458 + step, 299, c);
                    }

                    if (newPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 475 + step, 288);
                    }

                    if (repeatPinString.length() > i) {
                        GraphicUtil.drawStringCenter(g, "*", 475 + step, 326);
                    }
                }

                if (newPinString.length() == PIN_MAX_LENGTH) {
                    g.drawImage(iconGreenCheckImg, 614, 264, c);
                }

                if (repeatPinString.length() == PIN_MAX_LENGTH && newPinString.equals(repeatPinString)) {
                    g.drawImage(iconGreenCheckImg, 614, 302, c);
                } else if (repeatPinString.length() == PIN_MAX_LENGTH && !newPinString.equals(repeatPinString)) {
                    g.drawImage(iconRedXImg, 614, 302, c);
                }

                if (focus == 2) {
                    if (newPinString.length() > 0) {
                        g.drawImage(btnFocusImage, 321, 353, c);
                    } else {
                        g.drawImage(btnOffImage, 321, 353, c);
                    }
                    g.drawImage(btnDimImage, 485, 353, c);
                } else if (focus == 3) {
                    if (newPinString.length() > 0) {
                        g.drawImage(btnDimImage, 321, 353, c);
                    } else {
                        g.drawImage(btnOffImage, 321, 353, c);
                    }
                    g.drawImage(btnFocusImage, 485, 353, c);
                } else {
                    if (newPinString.length() > 0) {
                        g.drawImage(btnDimImage, 321, 353, c);
                    } else {
                        g.drawImage(btnOffImage, 321, 353, c);
                    }
                    g.drawImage(btnDimImage, 485, 353, c);
                }
                g.setColor(btnColor);
                g.setFont(f20);

                if (newPinString.length() > 0) {
                    GraphicUtil.drawStringCenter(g, btnClear, 400, 375);
                } else {
                    g.setColor(btnOffShColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 401, 376);
                    g.setColor(btnOffColor);
                    GraphicUtil.drawStringCenter(g, btnClear, 400, 375);
                }

                if (newPinString.length() == 4 && repeatPinString.length() == 4 && newPinString.equals(repeatPinString)) {
                    GraphicUtil.drawStringCenter(g, btnConfirm, 564, 375);
                } else {
                    GraphicUtil.drawStringCenter(g, btnCancel, 564, 375);
                }
            }
        }
    }
}

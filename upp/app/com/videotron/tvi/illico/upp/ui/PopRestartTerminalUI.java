/**
 * 
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * @author zestyman
 *
 */
public class PopRestartTerminalUI extends BaseUI implements TVTimerWentOffListener {

	private PopRestartTerminalRenderer renderer = new PopRestartTerminalRenderer();
	private TVTimerSpec tvTimerSpec;
	private BaseUI parent;
	
	private Effect effect;
	
	private int countDown = 30;

	/**
	 * 
	 */
	public PopRestartTerminalUI() {
		setRenderer(renderer);
		
		tvTimerSpec = new TVTimerSpec();
		tvTimerSpec.setDelayTime(1000L);
		tvTimerSpec.setRepeat(true);
		tvTimerSpec.addTVTimerWentOffListener(this);
	}

	public void start(BaseUI p) {
		renderer.setTitle(getPopupText(KeyNames.TITLE_RESTART_TERMINAL), PopBaseRenderer.ICON_TYPE_ORANGE);
		parent = p;
		focus = 0;
		
		countDown = DataCenter.getInstance().getInt("restart.time");
		
		if (Log.DEBUG_ON) {
			Log.printDebug("PopRestartTerminalUI: start(): countDown=" + countDown);
		}
		
		prepare();
		setVisible(true);
		repaint();
		startTimer();
	}

	public void stop() {
		stopTimer();
		super.stop();
	}
	
	public boolean handleKey(int code) {
		
		switch (code) {
		case OCRcEvent.VK_LEFT:
			if (focus != 0) {
				focus = 0;
				repaint();
			}
			return true;
		case OCRcEvent.VK_RIGHT:
			if (focus != 1) {
				focus = 1;
				repaint();
			}
			return true;
		case OCRcEvent.VK_ENTER:
			if (focus == 0) {
				if (Log.DEBUG_ON) {
					Log.printDebug("PopRestartTerminal, STB will be reboot forcely.");
				}
				
				Host.getInstance().reboot();
			} else {
				stop();
				parent.popupClosed(this, new Integer(ACTION_CANCEL));
			}
			return true;
		case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
		}
		
		return true;
	}
	
	private void startTimer() {
		stopTimer();
		try {
			TVTimer.getTimer().scheduleTimerSpec(tvTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}
	
	private void stopTimer() {
		TVTimer.getTimer().deschedule(tvTimerSpec);
	}
	
	public void timerWentOff(TVTimerWentOffEvent e) {
		
		if (e.getTimerSpec().equals(tvTimerSpec)) {
			if (Log.DEBUG_ON) {
				Log.printDebug("PopRestartTerminal, countDown=" + countDown);
			}
			
			countDown--;
			repaint();
			
			if (countDown == 0) {
				stopTimer();
				
				if (Log.DEBUG_ON) {
					Log.printDebug("PopRestartTerminal, STB will be reboot");
				}
				
				Host.getInstance().reboot();
			}
		}
	}

	class PopRestartTerminalRenderer extends PopBaseRenderer {

		private Color explainColor = new Color(229, 229, 229);
		private Color countDownColor = new Color(254, 234, 160);
		
        /** Font. */
        private Font f18 = FontResource.BLENDER.getFont(18);
        /** Font. */
        private Font f36 = FontResource.BLENDER.getFont(36);
		
		private String explainStr;
		private String restartNowStr;
		private String cancelStr;
		
		private Image focusButton;
		private Image dimmedButton;
		
		
		public void prepare(UIComponent c) {
			
			explainStr = getPopupText(KeyNames.EXPLAIN_RESTART_TERMINAL);
			restartNowStr = getPopupText(KeyNames.BTN_RESTART_NOW);
			cancelStr = getPopupText(KeyNames.BTN_CANCEL);
			
			focusButton = getImage("05_focus.png");
			dimmedButton = getImage("05_focus_dim.png");
			
			super.prepare(c);
		}
		
		protected void paint(Graphics g, UIComponent c) {
			PopRestartTerminalUI ui = (PopRestartTerminalUI) c;
			
			super.paint(g, c);

			// explain
			g.setColor(explainColor);
			g.setFont(f18);
			GraphicUtil.drawStringCenter(g, explainStr, 482, 232);
			
			// countDown
			g.setColor(countDownColor);
			g.setFont(f36);
			int min = countDown / 60;
			int sec = countDown % 60;
			g.drawString(df2.format(min) + ":" + df2.format(sec), 409, 278);
			g.drawString ("sec.", 497, 278);
			
			int focus = c.getFocus();
            if (focus == 0) {
                g.drawImage(focusButton, 321, 318, c);
                g.drawImage(dimmedButton, 485, 318, c);
            } else {
                g.drawImage(dimmedButton, 321, 318, c);
                g.drawImage(focusButton, 485, 318, c);
            }
            g.setColor(btnColor);
            g.setFont(f18);
            GraphicUtil.drawStringCenter(g, restartNowStr, 399, 339);
            GraphicUtil.drawStringCenter(g, cancelStr, 564, 339);
		}
	}
}

/**
 * @(#)PopSelectValueUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class represent a values which can select.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectValueUI extends BaseUI {

    /** Renderer. */
    private PopSelectChannelRenderer renderer = new PopSelectChannelRenderer();
    /** Parent UI. */
    private BaseUI parent;
    /** Values which can select. */
    private String[] values;
    /** X coordinate to move. */
    private int moveX = 0;
    /** Y coordinate to move. */
    private int moveY = 0;
    /** Indicate how many line is displayed. */
    private int line = 3;
    /** Indicate a half index. */
    private int halfLine = 1;
    /** Indicate a start index. */
    private int startIdx = 0;
    /** Indicate a Y coordinate of focus. */
    private int focusY = 0;
    /** Title. */
    private String title;

    /**
     * Set a renderer.
     */
    public PopSelectValueUI() {
        setRenderer(renderer);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     * @param data data array which can be selected.
     * @param current current value.
     * @param title title
     * @param x x coordinate to move.
     * @param y y coordinate to move.
     * @param line How many line is displayed.
     */
    public void show(BaseUI com, String[] data, String current, String title, int x, int y, int line) {
        prepare();
        this.moveX = x;
        this.moveY = y;
        this.title = title.replace('|', ' ');
        this.line = line;

        this.parent = com;
        values = data;
        focus = findFocus(values, current);
        calculateIndex();
        focusY = 42 + 16 + (halfLine - 2) * 27;
        setVisible(true);
    }

    /**
     * Calculate a index.
     */
    private void calculateIndex() {
        halfLine = line / 2;
        startIdx = (values.length - halfLine + focus) % values.length;
        if (line % 2 == 1)
            halfLine += 1;
    }

    /**
     * Find a focus.
     * @param values values which can be selected.
     * @param current current value.
     * @return index of current value.
     */
    private int findFocus(String[] values, String current) {
        if (values != null && values.length > 0) {
            for (int i = 0; i < values.length; i++) {
                if (values[i].equals(current)) {
                    return i;
                }
            }
        }

        return 0;
    }

    /**
     * Get a selected value.
     * @return selected value.
     */
    public String getSelectedValue() {
        return values[focus];
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = values.length - 1;
            }
            calculateIndex();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (++focus == values.length) {
                focus = 0;
            }
            calculateIndex();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            parent.popupClosed(this, new Integer(ACTION_OK));
            return true;
            // case KeyCodes.LAST:
            // parent.popupClosed(this, new Integer(ACTION_CANCEL));
            // return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * This class render a values.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopSelectChannelRenderer extends BaseRenderer {
        /** Image for bottom part of background. */
        Image bgBottomImg;
        /** Image for top part of background. */
        Image bgTopImg;
        /** Image for middle part of background. */
        Image bgMidImg;
        /** Image for top part of shadow. */
//        Image shadowTopImg;
        /** Image for bottom part of shadow. */
//        Image shadowBottomImg;
        /** Image for up arrow. */
        Image arrowUpImg;
        /** Image for down arrow. */
        Image arrowDownImg;
        /** Image for focus. */
        Image focusImg;
        /** Color for focused content (1, 1, 1). */
        Color focusColor = new Color(1, 1, 1);
        /** Color for dimmed content (130, 130, 130) . */
        Color dimmedColor = new Color(130, 130, 130);
        /** Color for background of title (78, 78, 78). */
        Color titleBgColor = new Color(78, 78, 78);
        /** Color for foreground of title (214, 182, 55). */
        Color titleFgColor = new Color(251, 217, 89);
        /** Color for title shadow (27, 24, 12). */
        Color titleShadowColor = new Color(27, 24, 12);
        /** Color for dimmed background (0, 0, 0, 153). */
        DVBColor dimmedBackColor = new DVBColor(0, 0, 0, 153);

        public void prepare(UIComponent c) {
            bgBottomImg = BaseUI.getImage("08_op_bg_b.png");
            bgTopImg = BaseUI.getImage("08_op_bg_t.png");
            bgMidImg = BaseUI.getImage("08_op_bg_m.png");
//            shadowTopImg = BaseUI.getImage("08_op_sh_t.png");
//            shadowBottomImg = BaseUI.getImage("08_op_sh_b.png");
            arrowUpImg = BaseUI.getImage("02_ars_t.png");
            arrowDownImg = BaseUI.getImage("02_ars_b.png");
            focusImg = BaseUI.getImage("08_op_foc.png");
            super.prepare(c);
        }
        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            g.setColor(dimmedBackColor);
            g.fillRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            parent.paintSelectedContent(g, parent);

            g.translate(moveX, moveY);

            g.drawImage(bgTopImg, 0, 42, c);
            int step = 61;
            for (int i = 1; i < line - 1; i++) {
                g.drawImage(bgMidImg, 0, step, c);
                step += 27;
            }
            g.drawImage(bgBottomImg, 0, step, c);

            Rectangle before = g.getClipBounds();
            g.clipRect(3, 44, 230, (line - 1) * 27 + 6);

            g.setFont(f18);
            g.setColor(dimmedColor);
            int dataPosY = 52;
            int idx = startIdx;
            for (int i = 0; i < line; i++) {
                String value = BaseUI.getOptionText(values[idx]);
                if (value != null && value != TextUtil.EMPTY_STRING) {
                    g.drawString(value, 19, dataPosY);
                } else {
                    g.drawString(values[idx], 19, dataPosY);
                }
                dataPosY += 27;
                if (++idx == values.length) {
                    idx = 0;
                }
            }
//            g.drawImage(shadowTopImg, 4, 44, c);
//            g.drawImage(shadowBottomImg, 4, step - 11, c);

            g.setClip(before);
            
            g.setColor(focusColor);
            g.drawImage(focusImg, 2, focusY, c);
            String value = BaseUI.getOptionText(values[focus]);
            if (value != null && value != TextUtil.EMPTY_STRING) {
                g.drawString(value, 19, focusY + 21);
            } else {
                g.drawString(values[focus], 19, focusY + 21);
            }

            // title
            g.setColor(titleBgColor);
            g.fillRect(2, 15, 230, 29);

            if (title != null) {
                title = TextUtil.shorten(title, fm18, 200);
//                g.setColor(titleShadowColor);
//                g.drawString(title, 16, 36);
                g.setColor(titleFgColor);
                g.drawString(title, 15, 35);
            }

            g.drawImage(arrowUpImg, 106, 0, c);
            g.drawImage(arrowDownImg, 106, step + 15, c);
            g.translate(-moveX, -moveY);
        }
    }
}

/**
 * @(#)MySettingsUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.gui.MySettingsBackgroundRenderer;
import com.videotron.tvi.illico.util.Constants;

/**
 * This class handle a {@link MenuUI} and {@link MenuInfoUI} and display a background for Settings' menu.
 * @author Woojung Kim
 * @version 1.1
 */
public class MySettingsUI extends BaseUI {

    /** Renderer. */
    private MySettingsBackgroundRenderer renderer = new MySettingsBackgroundRenderer();
    /** MenuUI which display a menu tree at left. */
    private MenuUI menuUI = new MenuUI();
    /** MenuInfoUI which display a menu information at right. */
    private MenuInfoUI menuInfoUI = new MenuInfoUI();
    /** Effect to animate a background as fade-in. */
    private AlphaEffect fadeInStart;

    /**
     * Set a bounds and renderer.
     */
    public MySettingsUI() {
        setBounds(Constants.SCREEN_BOUNDS);
        setRenderer(renderer);
    }

    /**
     * Start a UI.
     * @param back true if come from higher depth.
     */
    public void start(boolean back) {
        
        prepare();
        
        if (!back) {

            setVisible(false);

            if (fadeInStart == null) {
                fadeInStart = new AlphaEffect(this, 15, MovingEffect.FADE_IN);
            }
            menuUI.setVisible(false);
            menuInfoUI.setVisible(false);
            add(menuUI, 0);
            add(menuInfoUI, 0);
            fadeInStart.start();
        } else {

            add(menuUI, 0);
            add(menuInfoUI, 0);
            super.start();

            menuUI.setMenuInfoUI(menuInfoUI);
            menuInfoUI.setMenuUI(menuUI);
            menuInfoUI.start(true);

            menuUI.start(back);
            // startFadeInEffect(this);
        }
    }
    
    public void init() {
        menuUI.setMenuInfoUI(menuInfoUI);
        menuInfoUI.setMenuUI(menuUI);
        menuUI.init();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        remove(menuUI);
        remove(menuInfoUI);
        clearPopup();
        menuUI.stop();
        menuInfoUI.stop();
        super.stop();
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popup = getPopup();
            if (popup.handleKey(code)) {
                return true;
            }
        } else if (menuUI.handleKey(code)) {
            return true;
        }

        return false;
    }

    /**
     * Handle a action after effect ended.
     * @param effect effect to end.
     */
    public void animationEnded(Effect effect) {

        if (effect.equals(fadeInStart)) {
            Log.printDebug("MySettingsUI: animationEnded: fadeInStart");
            super.start();
            menuUI.setMenuInfoUI(menuInfoUI);
            menuInfoUI.setMenuUI(menuUI);
            menuUI.start(false);
            menuUI.startFadeInEffect();
        } else if (effect.equals(fadeInEffect)) {
            setVisible(true);
        }
    }

    /**
     * Get a menu UI.
     * @return Menu UI.
     */
    public final MenuUI getMenuUI() {
        return menuUI;
    }
}

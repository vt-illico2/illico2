/**
 * @(#)PopSelectSmallUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.text.DecimalFormat;

import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class represent a small UI to select a values. currently values is for time.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopSelectSmallUI extends BaseUI {
    /** Indicate that value is for AM. */
    public static final int TYPE_HOURS_AM = 0x0;
    /** Indicate that value is for PM. */
    public static final int TYPE_HOURS_PM = 0x1;
    /** Indicate that value is for minute. */
    public static final int TYPE_MINS = 0x2;
    /** Renderer. */
    private PopSelectTimeRenderer renderer = new PopSelectTimeRenderer();
    /** Input buffer. */
    private StringBuffer inputBuffer = new StringBuffer();
    /** Parent UI. */
    private BaseUI parent;
    /** Values to be displayed. */
    private String[] values;
    /** Top index. */
    private int topIdx = 0;
    /** Bottom index. */
    private int bottomIdx = 0;
    /** X coordinate to move. */
    private int moveX = 0;
    /** Y coordinate to move. */
    private int moveY = 0;
    /** Indicate whether data is numeric. */
    private boolean isNumeric;
    /** Indicate whether current language is english. */
    private boolean isEnglish;

    /**
     * Set a renderer.
     */
    public PopSelectSmallUI() {
        setRenderer(renderer);
    }

    /**
     * Show a popup.
     * @param com parent UI.
     * @param lastNumber Last number
     * @param type one of {@link #TYPE_HOURS_AM}, {@link #TYPE_HOURS_PM} or {@link #TYPE_MINS}
     * @param current current value
     * @param x X coordinate to move.
     * @param y Y coordinate to move.
     */
    public void show(BaseUI com, int lastNumber, int type, String current, int x, int y) {
        prepare();
        this.moveX = x;
        this.moveY = y;

        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.LANGUAGE);
        if (pre.getCurrentValue().equals(Definitions.LANGUAGE_ENGLISH)) {
            isEnglish = true;
        } else {
            isEnglish = false;
        }

        this.parent = com;
        inputBuffer.setLength(0);
        values = makeValues(lastNumber, type);
        isNumeric = true;
        int tempFocus = findFocus(values, current);
        if (tempFocus > -1) {
            focus = tempFocus;
        } else {
            focus = 0;
        }
        calculateIndex();
        setVisible(true);
    }

    /**
     * Show a selectable Pop-up for small size values. ex) AM or PM.
     * @param com The parental component of Pop-up.
     * @param selectableValue The values of selectable.
     * @param current The current value.
     * @param x The x coordinate of Pop-up.
     * @param y The y coordinate of Pop-up.
     */
    public void show(BaseUI com, String[] selectableValue, String current, int x, int y) {
        prepare();
        this.moveX = x;
        this.moveY = y;

        this.parent = com;
        isNumeric = false;
        values = selectableValue;

        int tempFocus = findFocus(values, current);
        if (tempFocus > -1) {
            focus = tempFocus;
        } else {
            focus = 0;
        }
        calculateIndex();
        setVisible(true);
    }

    /**
     * Make a values to display.
     * @param lastNumber last number.
     * @param type one of {@link #TYPE_HOURS_AM}, {@link #TYPE_HOURS_PM} or {@link #TYPE_MINS}
     * @return values to be made.
     */
    private String[] makeValues(int lastNumber, int type) {
        String[] retVal = null;
        DecimalFormat df = new DecimalFormat("00");

        if (type == TYPE_HOURS_AM || type == TYPE_HOURS_PM) {
            retVal = new String[lastNumber];
            for (int i = 0; i < lastNumber; i++) {
                if (i == 0 && isEnglish) {
                    retVal[i] = df.format(12);
                } else {
                    retVal[i] = df.format(i);
                }
            }

        } else if (type == TYPE_MINS) {
            int length = lastNumber / 5;
            retVal = new String[length];
            int step = 0;
            for (int i = 0; i < length; i++) {
                retVal[i] = df.format(step);
                step += 5;
            }
        }

        return retVal;
    }

    /**
     * Calculate a index of top and bottom.
     */
    private void calculateIndex() {
        topIdx = (values.length - 1 + focus) % values.length;
        bottomIdx = (focus + 1) % values.length;
    }

    /**
     * Find a focus for current value.
     * @param values values to display.
     * @param current current value.
     * @return focus index.
     */
    private int findFocus(String[] values, String current) {
        for (int i = 0; i < values.length; i++) {
            if (values[i].equals(current)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Get a selected value.
     * @return selected value.
     */
    public String getSelectedValue() {
        if (isEnglish && values[focus].equals("12")) {
            return "00";
        }
        return values[focus];
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {
        switch (code) {
        case OCRcEvent.VK_0:
        case OCRcEvent.VK_1:
        case OCRcEvent.VK_2:
        case OCRcEvent.VK_3:
        case OCRcEvent.VK_4:
        case OCRcEvent.VK_5:
        case OCRcEvent.VK_6:
        case OCRcEvent.VK_7:
        case OCRcEvent.VK_8:
        case OCRcEvent.VK_9:
            // if (isNumeric) {
            // inputBuffer.append(code - OCRcEvent.VK_0);
            // if (inputBuffer.length() == 2) {
            // Log.printDebug("number = [" + inputBuffer.toString() + "]");
            // int tempFocus = findFocus(values, inputBuffer.toString());
            // if (tempFocus > -1) {
            // focus = tempFocus;
            // }
            // calulateIndex();
            // inputBuffer.setLength(0);
            // }
            // repaint();
            // }
            return true;
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = values.length - 1;
            }
            calculateIndex();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (++focus == values.length) {
                focus = 0;
            }
            calculateIndex();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            parent.popupClosed(this, new Integer(ACTION_OK));
            return true;
            // case KeyCodes.LAST:
            // parent.popupClosed(this, new Integer(ACTION_CANCEL));
            // return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }

    /**
     * This class render a small popup to select a values.
     * @author Woojung Kim
     * @version 1.1
     */
    class PopSelectTimeRenderer extends BaseRenderer {
        /** Image for background. */
        Image bgImg;
        /** Image for up arrow. */
        Image arrowUpImg;
        /** Image for down arrow. */
        Image arrowDownImg;
        /** Image for shadow. */
//        Image shadowTopImg;
        /** Image for shadow on bottom. */
//        Image shadowBottomImg;
        /** Color for focus (245, 245, 245). */
        Color focusColor = new Color(245, 245, 245);
        /** Color for dimmed (130, 130, 130). */
        Color dimmedColor = new Color(130, 130, 130);
        /** Color for dimmed background (0, 0, 0, 153). */
        DVBColor dimmedBackColor = new DVBColor(0, 0, 0, 153);

        public void prepare(UIComponent c) {
            bgImg = BaseUI.getImage("number_p_01.png");
            arrowUpImg = BaseUI.getImage("02_ars_t.png");
            arrowDownImg = BaseUI.getImage("02_ars_b.png");
//            shadowTopImg = BaseUI.getImage("11_op_sh_t.png");
//            shadowBottomImg = BaseUI.getImage("11_op_sh_b.png");
            super.prepare(c);
        }
        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        protected void paint(Graphics g, UIComponent c) {
            parent.paintSelectedContent(g, parent);

            g.translate(moveX, moveY);
            // 224, 176
            g.drawImage(bgImg, 0, 15, c); // 224, 191, c);
            g.drawImage(arrowUpImg, 8, 0, c); // 232, 176, c);
            g.drawImage(arrowDownImg, 8, 67, c); // 232, 243, c);

            g.setFont(f18);

            if (isNumeric && inputBuffer.length() > 0) {
                g.setColor(focusColor);
                GraphicUtil.drawStringCenter(g, inputBuffer.toString(), 19, 47); // 243, 223);
            } else {
                g.setColor(dimmedColor);
                Rectangle before = g.getClipBounds();
                g.clipRect(2, 17, 33, 49); // 226, 193, 33, 49);
                GraphicUtil.drawStringCenter(g, values[topIdx], 19, 24); // 243, 200);
                GraphicUtil.drawStringCenter(g, values[bottomIdx], 19, 70); // 243, 246);
                g.setColor(focusColor);
                GraphicUtil.drawStringCenter(g, values[focus], 19, 47); // 243, 223);
                g.setClip(before);

//                g.drawImage(shadowTopImg, 2, 17, c); // 226, 193, c);
//                g.drawImage(shadowBottomImg, 2, 42, c); // 226, 218, c);

                g.translate(-(moveX), -(moveY));
            }
        }
    }
}

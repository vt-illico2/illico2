/**
 * @(#)CallerIDUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.CallerIDRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a preferences for Caller ID.
 * @author Woojung Kim
 * @version 1.1
 */
public class CallerIDUI extends BaseUI {

    /** A renderer. */
    private CallerIDRenderer renderer = new CallerIDRenderer();
    /** A explain UI. */
    private ExplainUI explainUI = new ExplainUI();
    /** A popup to select a values. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** A popup to select a phone lines. */
    private PopSelectPhoneLineUI selectLineUI = new PopSelectPhoneLineUI();

    /** A string array to have a preference ids for Caller ID. */
    public final String[] preferenceIds = {PreferenceNames.CALLER_ID_DISPLAY,
            PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION, PreferenceNames.CALLER_ID_NOTIFICATION_DURATION};// ,
    // PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION};
    /** A popup to confirm if user deselect a all phone lines. */
    private PopCallerIDUI confirmUI = new PopCallerIDUI();
    /** a Hashtable to include a objects of preferences. */
    private Hashtable preTable;
    /** indicate whether enter from other app or not. */
    private boolean isFromApp;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Constructor. Set a renderer and add a explain UI.
     */
    public CallerIDUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {

        String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
        String cId;
        if (params != null && params.length > 1 && params[0].equals("Callerid")) {
            DataCenter.getInstance().remove("PARAMS");
            isFromApp = true;
            SubCategory sub = new SubCategory();
            sub.setCategoryId("caller.id");
            SubCategory sub2 = new SubCategory();
            sub2.setCategoryId("preference");
            addHistory(sub);
            addHistory(sub2);
            cId = "caller.id";
        } else {
            isFromApp = false;
            cId = ((Category) peekHistory()).getCategoryId();
        }
        preTable = DataManager.getInstance().getPreferences(cId);
        focus = 0;
        addButton(BTN_BACK);
        super.prepare();
    }

    /**
     * Start a UI.
     * @param back true if from lower UI, false otherwise.
     */
    public void start(boolean back) {
        prepare();

        setExplainText();

        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        super.stop();
    }

    /**
     * Get a Hashtable to includes a objects of preferences.
     * @return Hashtable
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Handle a key event.
     * @param code key code.
     * @return true if use a key, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            
            if (getLines() == null && focus == 1) {
                focus = 0;
            }
            
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:
            if (!canSelectMenu()) {
                return true;
            }
            if (++focus == preferenceIds.length) {
                focus = preferenceIds.length - 1;
            }
            
            if (getLines() == null && focus == 1) {
                focus = 2;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            int clickY = 0;
            if (focus == 0) {
                clickY = focus * 37;
            } else {
                clickY = focus * 37 + 18;
            }

            if (focus != 1) {
                clickEffect.start(BaseRenderer.INPUT_X, 141 + clickY, 211, 33);
                setPopup(selectUI);
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                int posY = 83 + focus * 37;
                if (focus != 0) {
                    posY += 18;
                }
                selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519, posY, 3);
            } else {
                String[] lines = getLines();
                if (lines != null && lines.length > 1) {
                    clickEffect.start(BaseRenderer.INPUT_X, 141 + clickY, 211, 33);
                    Preference pre = DataManager.getInstance().getPreference(preferenceIds[focus]);
                    setPopup(selectLineUI);
                    selectLineUI.show(this, pre.getCurrentValue(), 463, 25);
                }
            }
            repaint();
            return true;
        case KeyCodes.LAST:
            if (isFromApp) {
                popHistory();
                MonitorService mService = CommunicationManager.getInstance().getMonitorService();
                String[] parameters = new String[1];
                parameters[0] = MonitorService.REQUEST_APPLICATION_LAST_KEY;

                try {
                    mService.startUnboundApplication("Callerid", parameters);
                } catch (RemoteException e) {
                    Log.print(e);
                }
            } else {
                clickButtons(BTN_BACK);
                // startFadeOutEffect(this);
                popHistory();
                PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
            }
            return true;
        default:
            return false;
        }
    }

    /**
     * Set a Explain Text for focus.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Determines whether can select or not a some preferences according to Caller id display.
     * @return true if can select a menu, false otherwise.
     */
    public boolean canSelectMenu() {
        Preference pre = (Preference) preTable.get(PreferenceNames.CALLER_ID_DISPLAY);

        if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE)) {
            return true;
        }

        return false;
    }

    /**
     * Determines whether selected phone is single or multiple.
     * @return true if selected phone is multiple, false otherwise.
     */
    public boolean isMultiLine() {
        Preference pre = (Preference) preTable.get(PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION);

        if (pre != null) {
            String[] selectedLines = TextUtil.tokenize(pre.getCurrentValue(), '|');

            if (selectedLines.length > 1) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get a lines.
     * @return lines.
     */
    public String[] getLines() {
        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.CALLER_ID_PHONE_LINE_LIST);
        if (pre != null && pre.getCurrentValue() != null && !pre.getCurrentValue().equals(TextUtil.EMPTY_STRING)) {
            String[] lines = TextUtil.tokenize(pre.getCurrentValue(), '|');
            return lines;
        }
        return null;
//        return new String[] {"111-1111"};
//        return new String[] {"111-1111", "222-2222", "333-3333", "444-4444", "555-5555", "666-6666", "777-7777",
//                "888-8888"};
    }

    /**
     * Handle after close popup.
     * @param pop closed popup.
     * @param msg message from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {
        int action = ((Integer) msg).intValue();
        removePopup(pop);

        if (Log.DEBUG_ON) {
            Log.printDebug("CallerIDUI: popupClosed: pop = " + pop);
            Log.printDebug("CallerIDUI: popupClosed: action = " + action);
        }

        if (pop.equals(selectUI)) {

            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectUI.getSelectedValue();

                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);
            }
        } else if (pop.equals(selectLineUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectLineUI.getSelectedValue();
                if (selectedValue.length() > 0) {
                    Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                    pre.setCurrentValue(selectedValue);
                    preTable.put(pre.getID(), pre);
                    DataManager.getInstance().setPreference(pre.getID(), selectedValue);
                } else {
                    setPopup(confirmUI);
                    String title = getPopupText(KeyNames.TITLE_NO_PHONE_LINE_SELECTED);
                    String temp = getPopupText(KeyNames.EXPLAIN_NO_PHONE_LINE_SELECTED);
                    String[] explain = TextUtil.split(temp, BaseRenderer.fm18, 320, '|');

                    confirmUI.start(this, title, explain);
                }
            }
        } else if (pop.equals(confirmUI)) {
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectLineUI.getSelectedValue();
                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);
                
                Preference displayPre = (Preference) preTable.get(PreferenceNames.CALLER_ID_DISPLAY);
                displayPre.setCurrentValue(Definitions.OPTION_VALUE_DISABLE);
                DataManager.getInstance().setPreference(displayPre.getID(), Definitions.OPTION_VALUE_DISABLE);
                focus = 0;
            }
        }
        repaint();
    }
}

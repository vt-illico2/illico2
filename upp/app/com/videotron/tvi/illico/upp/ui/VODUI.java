/**
 * @(#)VODUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.VODDetailRenderer;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class represent a UI to modify a preference for VOD.
 * @author Woojung Kim
 * @version 1.1
 */
public class VODUI extends BaseUI {
    /** Renderer. */
    private VODDetailRenderer renderer = new VODDetailRenderer();
    /** Explain UI at bottom on screen. */
    private ExplainUI explainUI = new ExplainUI();
    /** Popup to select a value. */
    private PopSelectValueUI selectUI = new PopSelectValueUI();
    /** ID array for VOD. */
    public final String[] preferenceIds = {PreferenceNames.WISHLIST_DISPLAY, PreferenceNames.PREFERENCES_ON_FORMAT,
            PreferenceNames.PREFERENCES_ON_LANGUAGE};
    /** Hashtable to include a preference for VOD. */
    private Hashtable preTable;
    /** Indicate whether come from VOD. */
    private boolean isFromVOD;

    /** Clicking Effect. */
    private ClickingEffect clickEffect;

    /**
     * Set a renderer and add a Explain UI and create a ClickEffect.
     */
    public VODUI() {
        setRenderer(renderer);
        add(explainUI);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Prepare a UI.
     */
    public void prepare() {

        String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
        String cId;
        if (params != null && params.length > 1 && params[0].equals("VOD")) {
            DataCenter.getInstance().remove("PARAMS");
            isFromVOD = true;
            SubCategory sub = new SubCategory();
            sub.setCategoryId("vod");
            SubCategory sub2 = new SubCategory();
            sub2.setCategoryId("preference");
            addHistory(sub);
            addHistory(sub2);
            cId = "vod";
        } else {
            isFromVOD = false;
            cId = ((Category) peekHistory()).getCategoryId();
        }
        preTable = DataManager.getInstance().getPreferences(cId);
        focus = 0;
        super.prepare();
    }

    /**
     * Start a UI.
     */
    public void start(boolean back) {
        prepare();
        addButton(BTN_BACK);
        setExplainText();

        super.start();
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        clearPopup();
        clearButtons();
        explainUI.stop();
        super.stop();
    }

    /**
     * Get a Preference of VOD.
     * @return Preference of VOD.
     */
    public Hashtable getPreferences() {
        return preTable;
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {

        if (getPopup() != null) {
            BaseUI popUp = getPopup();
            if (popUp.handleKey(code)) {
                return true;
            }
        }

        switch (code) {
        case OCRcEvent.VK_UP:
            if (--focus < 0) {
                focus = 0;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_DOWN:

            if (++focus == preferenceIds.length) {
                focus = preferenceIds.length - 1;
            }
            setExplainText();
            repaint();
            return true;
        case OCRcEvent.VK_ENTER:
            clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37, 211, 33);
            setPopup(selectUI);
            Preference pre = (Preference) preTable.get(preferenceIds[focus]);
            selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519, 83 + focus * 37,
                    3);
            repaint();
            return true;
        case KeyCodes.LAST:
            if (isFromVOD) {
                popHistory();
                MonitorService mService = CommunicationManager.getInstance().getMonitorService();
                String[] parameters = new String[1];
                parameters[0] = MonitorService.REQUEST_APPLICATION_LAST_KEY;

                try {
                    mService.startUnboundApplication("VOD", parameters);
                } catch (RemoteException e) {
                    Log.print(e);
                }
                return true;
            } else {
                clickButtons(BTN_BACK);
                //startFadeOutEffect(this);
                popHistory();
                PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
                return true;
            }
        default:
            return false;
        }
    }

    /**
     * Set a explain text for focused preference.
     */
    private void setExplainText() {
        String text = getExplainText(preferenceIds[focus]);
        explainUI.setText(text);
        explainUI.start(false);
    }

    /**
     * Handle a action after popup closed.
     * @param pop Popup is closed.
     * @param msg messge from popup.
     */
    public void popupClosed(BaseUI pop, Object msg) {

        if (pop.equals(selectUI)) {
            int action = ((Integer) msg).intValue();
            removePopup(selectUI);
            repaint();
            if (action == BaseUI.ACTION_OK) {
                String selectedValue = selectUI.getSelectedValue();

                Preference pre = (Preference) preTable.get(preferenceIds[focus]);
                pre.setCurrentValue(selectedValue);
                preTable.put(pre.getID(), pre);
                DataManager.getInstance().setPreference(pre.getID(), selectedValue);

            }
        }
    }
}

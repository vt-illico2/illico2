/**
 * @(#)TVPictureFormatUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Dimension;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.ocap.hardware.Host;
import org.ocap.hardware.VideoOutputPort;
import org.ocap.hardware.device.FixedVideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputSettings;
import org.ocap.hardware.device.VideoResolution;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.gui.TVPictureFormatRenderer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class represent a UI to modify a TV picture format.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class TVPictureFormatUI extends BaseUI implements TVTimerWentOffListener {
	/** Constant to indicate a mode that user can select a format. */
	public static final int STATE_SELECT = 0;
	/** Constant to indicate a preview mode. */
	public static final int STATE_PREVIEW = 1;

	/** Delay time for preview mode. */
	private final int DELAY_TIME = 15; // 15 * 1000;
	/** Renderer. */
	private TVPictureFormatRenderer renderer = new TVPictureFormatRenderer();
	/** Popup to select a value. */
	private PopSelectValueUI selectUI = new PopSelectValueUI();
	/** Preference for TV Picture format. */
	private Preference pre;
	/** Indicate whether user can select a Apply button. */
	private boolean canApply;
	/** Indicate whether user selected a Apply butto. */
	private boolean isApply;
	/** Timer for preview mode. */
	private TVTimerSpec previewTvSpec;
	/** String have a value to be selected. */
	private String selectValue;
	/** Focus index of preview mode. */
	private int previewFocus;
	/** count down to zero from DELAY_TIME in preview mode. */
	private int count;

	/** Clicking Effect. */
	private ClickingEffect clickEffect;

	private boolean isAutoDetected;

	private boolean[] supportList = new boolean[7];

	private boolean isConnectedHDMI = false;

	/**
	 * Set a renderer and create a TimerSpec and ClickEffect.
	 */
	public TVPictureFormatUI() {
		renderer.setHasExpalin(false);
		setRenderer(renderer);
		previewTvSpec = new TVTimerSpec();
		clickEffect = new ClickingEffect(this, 5);
	}

	/**
	 * Prepare a UI.
	 */
	public void prepare() {
		pre = (Preference) BaseUI.peekHistory();

		if (Log.DEBUG_ON) {
			Log.printDebug("TVPictureFormatDetailUI: prepare(): current value : " + pre.getCurrentValue());
		}

		focus = 0;
		previewFocus = 0;
		count = 0;
		state = STATE_SELECT;

		isAutoDetected = false;
		Preference pre = (Preference) DataManager.getInstance().getPreference(
				PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT);

		if (pre != null && pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE)) {
			isAutoDetected = true;
		}
				
		super.prepare();
	}

	/**
	 * Start a UI.
	 */
	public void start(boolean back) {
		prepare();
		addButton(BTN_BACK);
		canApply = false;
		isApply = false;		
		selectValue = pre.getCurrentValue(); 

		PreferenceActionController.getInstance().checkScreenConfiguration();

		super.start();
	}

	/**
	 * Stop a UI.
	 */
	public void stop() {
		stopTimer();
		clearPopup();
		clearButtons();
		if (!isApply) {
			backPreviousFormat();
		}
		super.stop();
	}

	/**
	 * Get a Preference of TV Picture format.
	 * 
	 * @return Preference of TV Picture format.
	 */
	public Preference getPreference() {
		return pre;
	}

	/**
	 * Handle a key code.
	 * 
	 * @param code
	 *            key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {

		if (getPopup() != null) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_UP:
			if (--focus < 0) {
				focus = 0;
			}
			repaint();
			return true;
		case OCRcEvent.VK_DOWN:
			if (focus == 1 && canSelect()) {
				focus = 2;
				repaint();
			} else if (focus == 0) {
				focus = 1;
				repaint();
			}
			return true;
		case OCRcEvent.VK_LEFT:
			if (state == STATE_PREVIEW) {
				previewFocus = 0;
				repaint();
			}
			return true;
		case OCRcEvent.VK_RIGHT:
			if (state == STATE_PREVIEW) {
				previewFocus = 1;
				repaint();
			}
			return true;
		case OCRcEvent.VK_ENTER:
			if (state == STATE_SELECT) {
				if (focus == 0) { // choose foramt
					clickEffect.start(81, 178, 211, 33);
					setPopup(selectUI);

					if (Environment.SUPPORT_UHD) {
						supportList = new boolean[7];
						updateSupportedTVPictureFormat();
						Vector newValueVec = new Vector();
						String[] values = pre.getSelectableValues();
						for (int i = 0; i < supportList.length; i++) {
							if (supportList[i]) {
								newValueVec.add(values[i]);
							}
						}
						
						// VDTRMASTER-5464
						if (newValueVec.size() == 0) {
							// it means HDMI disconnected
							for (int i = 0; i < 5; i++) {
								newValueVec.add(values[i]);
							}
						}

						String[] newValues = new String[newValueVec.size()];
						newValues = (String[]) newValueVec.toArray(newValues);
						selectUI.show(this, newValues, pre.getCurrentValue(), pre.getName(), 261, 122, 5);
					} else {
						selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 261, 122,
								5);
					}
					repaint();
				} else if (focus == 1) { // generate preview
					clickEffect.start(80, 310, 218, 40);
					previewFocus = 1;
					count = 0;
					state = STATE_PREVIEW;
					repaint();
					PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
							selectValue);
					startChannel();
					repaint();
					startTimer();
				} else if (focus == 2) { // apply
					clickEffect.start(80, 407, 218, 40);
					// apply & go back
					isApply = true;
					PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
							selectValue);
					storeData();
					popHistory();
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, true);
				}
			} else if (state == STATE_PREVIEW) {
				if (previewFocus == 0) { // Yes = apply
					stopTimer();
					clickEffect.start(497, 452, 162, 40);
					// apply & go back
					isApply = true;
					PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
							selectValue);
					storeData();
					stopChannel();
					popHistory();
					PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, true);
				} else if (previewFocus == 1) { // No = cancel
					clickEffect.start(661, 452, 162, 40);
					stopTimer();
					stopChannel();
					backPreviousFormat();
					focus = 0;
					state = STATE_SELECT;
					canApply = false;
					repaint();
				}
			}
			return true;
		case KeyCodes.LAST:
			clickButtons(BTN_BACK);
			// startFadeOutEffect(this);
			popHistory();
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_EQUIPMENT, true);
			return true;
		default:
			return false;
		}
	}

	/**
	 * Make a format to last format.
	 */
	private void backPreviousFormat() {
		if (Log.DEBUG_ON) {
			Log.printDebug("TVPictrueFormatDetailUI: backPreviousFormat()");
		}
		PreferenceActionController.getInstance().doAction(PreferenceNames.VIDEO_TV_PICTURE_FORMAT,
				pre.getCurrentValue());
	}

	/**
	 * Store a data.
	 */
	private void storeData() {
		pre.setCurrentValue(selectValue);

		DataManager.getInstance().setPreference(PreferenceNames.VIDEO_TV_PICTURE_FORMAT, pre.getCurrentValue());

		if (isAutoDetected) {
			DataManager.getInstance().setPreference(PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT,
					Definitions.OPTION_VALUE_ENABLE);
		} else {
			DataManager.getInstance().setPreference(PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT,
					Definitions.OPTION_VALUE_DISABLE);
		}

	}

	/**
	 * Determine whether User can select a apply button or not.
	 * 
	 * @return true if user can select a apply button, false otherwise.
	 */
	public boolean canSelect() {
		return canApply;
	}

	/**
	 * Get a current value which is selected by user.
	 * 
	 * @return selected value.
	 */
	public String getCurrentValue() {
		if (Environment.SUPPORT_UHD && isAutoDetected) {

			String autoStr = BaseUI.getOptionText("Auto");
			// VDTRMASTER-5821
			String detectedStr = BaseUI.getOptionText("detect");

			StringBuffer sf = new StringBuffer();
			sf.append(autoStr + " - ");
			sf.append(selectValue);
			sf.append(" " + detectedStr);

			return sf.toString();
		} else {
			return selectValue;
		}
	}
	
	//R7.4 freelife VDTRMASTER-6166
	public String getCurrentValueMsg(String selectedValue) {
		if (Environment.SUPPORT_UHD && isAutoDetected) {

			String autoStr = BaseUI.getOptionText("Auto");
			// VDTRMASTER-5821
			String detectedStr = BaseUI.getOptionText("detect");

			StringBuffer sf = new StringBuffer();
			sf.append(autoStr + " - ");
			sf.append(selectedValue);
			sf.append(" " + detectedStr);

			return sf.toString();
		} else {
			return selectedValue;
		}
	}

	/**
	 * Get a index of focus for preview mode.
	 * 
	 * @return index of focus.
	 */
	public int getPreviewFocus() {
		return previewFocus;
	}

	/**
	 * Get a remain time for preview mode.
	 * 
	 * @return remain time
	 */
	public int getRemainTime() {
		return DELAY_TIME - count;
	}

	/**
	 * Start a {@link #previewTvSpec}, {@link #previewTvSpec} is called every
	 * seconds.
	 */
	private void startTimer() {
		stopTimer();

		previewTvSpec.addTVTimerWentOffListener(this);
		try {
			previewTvSpec.setDelayTime(1000L);
			previewTvSpec.setRepeat(true);
			TVTimer.getTimer().scheduleTimerSpec(previewTvSpec);
		} catch (TVTimerScheduleFailedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Stop a {@link #previewTvSpec}.
	 */
	private void stopTimer() {
		previewTvSpec.removeTVTimerWentOffListener(this);
		TVTimer.getTimer().deschedule(previewTvSpec);
	}

	/**
	 * Handle a action after popup closed.
	 * 
	 * @param pop
	 *            popup is closed.
	 * @param msg
	 *            message from popup.
	 */
	public void popupClosed(BaseUI pop, Object msg) {

		int action = ((Integer) msg).intValue();
		if (pop.equals(selectUI)) {
			if (action == BaseUI.ACTION_OK) {
				String selectedValue = selectUI.getSelectedValue();

				if (selectedValue.equals(Definitions.VIDEO_TV_PICTURE_FORMAT_AUTO)) {
					startAutoDetect();
				} else {
					isAutoDetected = false;
					removePopup(pop);
					selectValue = selectedValue;
				}
			} else {
				removePopup(pop);
			}

			repaint();
		}
	}

	/**
	 * Handle a Timer event. In only preview mode, count decrease from
	 * {@link #DELAY_TIME} to zero.
	 * 
	 * @param event
	 *            {@link #previewTvSpec}
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug("TVPictrueFormatDetailUI: timerWentOff: event : " + event);
		}
		if (event.getTimerSpec().equals(previewTvSpec)) {
			if (getRemainTime() <= 0) {
				stopTimer();
				stopChannel();
				backPreviousFormat();
				state = STATE_SELECT;
				// VDTRMASTER-5647
				focus = 1;
				canApply = true;
			} else {
				count++;
				if (Log.DEBUG_ON) {
					Log.printDebug("TVPictrueFormatDetailUI: timerWentOff: hasFocus : " + this.hasFocus());
				}
				if (!this.hasFocus()) {
					this.requestFocus();
				}
			}
			repaint();
		}
	}

	/**
	 * Start a channel AV to check a selected format.
	 */
	public void startChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("TVPictrueFormatDetailUI: startAV()");
		}

		EpgService eService = CommunicationManager.getInstance().getEpgService();

		if (eService != null) {
			try {
				eService.getChannelContext(0).changeChannel(-1);
			} catch (RemoteException e) {
				Log.printWarning(e);
			}
		}
	}

	/**
	 * Stop a channel AV.
	 */
	public void stopChannel() {
		if (Log.DEBUG_ON) {
			Log.printDebug("TVPictrueFormatDetailUI: stopAV()");
		}

		EpgService eService = CommunicationManager.getInstance().getEpgService();

		if (eService != null) {
			try {
				eService.getChannelContext(0).stopChannel();
			} catch (RemoteException e) {
				Log.printWarning(e);
			}
		}
	}

	private void startAutoDetect() {
		new Thread(new Runnable() {
			public void run() {
				PreferenceController.getInstance().showLoading();

				isAutoDetected = true;
				selectValue = PreferenceController.getInstance().findTVPictureFormat();
				removePopup(selectUI);
				PreferenceController.getInstance().hideLoading();
				repaint();
			}
		}).start();
	}

	public void updateSupportedTVPictureFormat() {
		String foundValue = null;

		// step 1 - check HDMI
		// 2160P / 1080P / 1080i / 720P / 480P / 480i

		Enumeration e = Host.getInstance().getVideoOutputPorts();
		int idx = 0;
		isConnectedHDMI = false;
		while (e.hasMoreElements()) {
			VideoOutputPort port = (VideoOutputPort) e.nextElement();
			VideoOutputSettings setting = (VideoOutputSettings) port;
			if (port.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
				if (port.status() == false) {
					Log.printDebug("PreferenceController, findTVPictureFormat(), HDMI port is disabled");
				} else if (setting.isDisplayConnected() == false) {
					Log.printDebug("PreferenceController, findTVPictureFormat(), HDMI port is disconnected");
				} else {
					isConnectedHDMI = true;
					VideoOutputConfiguration[] config = setting.getSupportedConfigurations();

					if (Log.EXTRA_ON) {
						for (int i = 0; i < config.length; i++) {
							FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
							VideoResolution res = fixed.getVideoResolution();
							Dimension dim = res.getPixelResolution();
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i + "] = " + fixed);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], resolution = " + res);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], dimension = " + dim);
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i
									+ "], scanmode = " + res.getScanMode());
							Log.printDebug("PreferenceController, findTVPictureFormat(), config[" + i + "], rate = "
									+ res.getRate() + ", rounded = " + Math.round(res.getRate()));
						}
					}

					final int[][] SEARCH_LIST = { { VideoResolution.SCANMODE_INTERLACED, 480 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 480 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 720 },
							{ VideoResolution.SCANMODE_INTERLACED, 1080 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 1080 },
							{ VideoResolution.SCANMODE_PROGRESSIVE, 2160 },
					};

					int foundIndex = -1;

					// for Auto
					supportList[0] = true;
					for (int i = 0; i < SEARCH_LIST.length; i++) {
						foundIndex = findResolution(config, SEARCH_LIST[i][0], SEARCH_LIST[i][1]);
						if (foundIndex != -1) {
							supportList[i + 1] = true;
							Log.printDebug("PreferenceController, findTVPictureFormat(), found=" + foundValue);
						} else {
							supportList[i + 1] = false;
						}
					}
				}
			} else {
				Log.printDebug("PreferenceController, findTVPictureFormat(), port[" + idx + "] is not HDMI, type = "
						+ port.getType() + ", state = " + port.status() + ", connected = "
						+ setting.isDisplayConnected());
			}
			idx++;
		}
	}

	private int findResolution(VideoOutputConfiguration[] config, int scanMode, int height) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PreferenceController, findResolution(), scanMode=" + scanMode + ", height=" + height);
		}
		for (int i = 0; i < config.length; i++) {
			FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
			VideoResolution res = fixed.getVideoResolution();
			Dimension dim = res.getPixelResolution();
			if (Log.DEBUG_ON) {
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "] = " + fixed);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], resolution = " + res);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], dimension = " + dim);
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], scanmode = "
						+ res.getScanMode());
				Log.printDebug("PreferenceController, findResolution(), config[" + i + "], rate = " + res.getRate()
						+ ", rounded = " + Math.round(res.getRate()));
			}

			if (res.getScanMode() == scanMode && dim.height == height) {
				return i;
			}
		}

		return -1;
	}
}

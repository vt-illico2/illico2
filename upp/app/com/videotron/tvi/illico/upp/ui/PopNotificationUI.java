/**
 * @(#)PopNotificationUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * This class represent a popup for notification.
 * @author Woojung Kim
 * @version 1.1
 */
public class PopNotificationUI extends BaseUI {

    /** Renderer. */
    private NotificationRenderer renderer = new NotificationRenderer();
    /** Parent UI. */
    private BaseUI parent;
    /** Texts to explain about notification. */
    private String[] texts;
    /** Indicate whether this can cancel or not. */
    private boolean canCancel;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;

    public PopNotificationUI() {
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Show a popup that type is notification sytile.
     * @param ui The parent UI
     * @param title The title of popup
     * @param explains The explain text of popup
     * @param icon The icon type. {@link PopBaseRenderer#ICON_TYPE_NONE},{@link PopBaseRenderer#ICON_TYPE_RED},
     *            {@link PopBaseRenderer#ICON_TYPE_YELLOW}
     */
    public void show(BaseUI ui, String title, String[] explains, int icon) {
        canCancel = false;
        prepare();
        parent = ui;
        renderer.setTitle(title, icon);
        texts = explains;
        setVisible(true);
    }

    /**
     * Show a popup with parameter.
     * @param ui parent UI.
     * @param title popup title.
     * @param explains why user see this.
     * @param icon wanted icon. one of {@link PopBaseRenderer#ICON_TYPE_NONE}, {@link PopBaseRenderer#ICON_TYPE_ORANGE},
     *            {@link PopBaseRenderer#ICON_TYPE_RED} or {@link PopBaseRenderer#ICON_TYPE_YELLOW}.
     * @param canCancel true if can cancel, false otherwise.
     */
    public void show(BaseUI ui, String title, String[] explains, int icon, boolean canCancel) {
        this.canCancel = canCancel;
        prepare();
        parent = ui;
        renderer.setTitle(title, icon);
        texts = explains;
        focus = 0;
        setVisible(true);
    }

    /**
     * Handle a key code.
     * @param code key code.
     * @return true if key code is used, false otherwise.
     */
    public boolean handleKey(int code) {
        switch (code) {
        case OCRcEvent.VK_LEFT:
            if (canCancel) {
                focus = 0;
                repaint();
            }
            return true;
        case OCRcEvent.VK_RIGHT:
            if (canCancel) {
                focus = 1;
                repaint();
            }
            return true;
        case OCRcEvent.VK_ENTER:
            if (canCancel) {
                if (focus == 0) {
                    clickEffect.start(321, 318, 162, 40);
                    parent.popupClosed(this, new Integer(ACTION_OK));
                } else {
                    clickEffect.start(485, 318, 162, 40);
                    parent.popupClosed(this, new Integer(ACTION_CANCEL));
                }
            } else {
                clickEffect.start(403, 318, 162, 40);
                parent.popupClosed(this, new Integer(ACTION_OK));
            }
            return true;
        case OCRcEvent.VK_EXIT:
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
            return true;
        default:
            return true;
        }
    }
    
    /**
     * This class render a notification items.
     * @author Woojung Kim
     * @version 1.1
     */
    class NotificationRenderer extends PopBaseRenderer {

        /** The image of button when not selected. */
        private Image btnDimImg;
        /** The image of button when selected. */
        private Image btnFocusImg;

        /** The color of button strings. */
        private Color btnColor = new Color(3, 3, 3);
        /** Name of OK button . */
        private String btnOK;
        /** Name of Yes button. */
        private String btnYes;
        /** Name of No button. */
        private String btnNo;

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
            btnDimImg = BaseUI.getImage("05_focus_dim.png");
            btnFocusImg = BaseUI.getImage("05_focus.png");
            
            btnOK = getPopupText(KeyNames.BTN_OK);
            btnYes = getPopupText(KeyNames.BTN_YES);
            btnNo = getPopupText(KeyNames.BTN_NO);
        }

        /**
         * Paint a renderer.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        public void paint(Graphics g, UIComponent c) {
            super.paint(g, c);

            g.setFont(f18);
            g.setColor(Color.WHITE);

            if (texts != null) {
                if (texts.length == 1) {
                    GraphicUtil.drawStringCenter(g, texts[0], 482, 255);
                } else {
                    int startX = 255 - texts.length / 2 * 19;
                    for (int i = 0; i < texts.length; i++) {
                        GraphicUtil.drawStringCenter(g, texts[i], 482, startX + i * 19);
                    }
                }
            }

            if (canCancel) {
                if (focus == 0) {
                    g.drawImage(btnFocusImg, 321, 318, c);
                    g.drawImage(btnDimImg, 485, 318, c);
                } else if (getFocus() == 1) {
                    g.drawImage(btnDimImg, 321, 318, c);
                    g.drawImage(btnFocusImg, 485, 318, c);
                }
                g.setColor(btnColor);
                GraphicUtil.drawStringCenter(g, btnYes, 400, 340);
                GraphicUtil.drawStringCenter(g, btnNo, 564, 340);
            } else {
                g.drawImage(btnFocusImg, 403, 318, c);
                g.setColor(btnColor);
                GraphicUtil.drawStringCenter(g, btnOK, 482, 340);
            }
        }
    }

}

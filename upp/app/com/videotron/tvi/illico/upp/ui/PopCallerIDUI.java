/**
 * @(#)PopCallerIDUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * PopCallerIDUI.
 * @author Woojung Kim
 */
public class PopCallerIDUI extends BaseUI {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 2877161940694298009L;
    /** parent UI. */
    private BaseUI parent;
    /** Title. */
    private String title;
    /** PopCallerIDUI contents. */
    private String[] contents;

    /** Effect to animate when select a menu. */
    private ClickingEffect clickEffect;

    /** Constructor. */
    public PopCallerIDUI() {
        setRenderer(new RendererPopup());
        clickEffect = new ClickingEffect(this, 5);
    }

    /**
     * Starts PopCallerIDUI.
     * @param ui CallLog
     * @param t title
     * @param con contents
     */
    public void start(BaseUI ui, String t, String[] con) {
        this.title = t;
        parent = ui;
        contents = con;
        focus = 0;
        prepare();
        setVisible(true);
        repaint();
    }

    /**
     * Stops this popup.
     * @param confirm confirmation
     */
    public void stop(boolean confirm) {
        if (confirm) {
            parent.popupClosed(this, new Integer(ACTION_OK));
        } else {
            parent.popupClosed(this, new Integer(ACTION_CANCEL));
        }
        this.setVisible(false);
        title = null;
        contents = null;
    }

    /**
     * Handles key event.
     * @param code key code
     * @return If key have to be used other application, return false or true
     */
    public boolean handleKey(int code) {
        if (code == HRcEvent.VK_LEFT || code == HRcEvent.VK_RIGHT) {
            focus = (focus + 1) % 2;
            repaint();
        } else if (code == OCRcEvent.VK_ENTER) {
            if (focus == 0) {
                clickEffect.start(321, 353, 162, 40);
                stop(true);
            } else {
                clickEffect.start(485, 353, 162, 40);
                stop(false);
            }
        } else if (code == OCRcEvent.VK_EXIT) {
            stop(false);
        }
        return true;
    }

    /**
     * Renderer for PopCallerIDUI.
     */
    class RendererPopup extends Renderer {
        /** Font. */
        private Font f18 = FontResource.BLENDER.getFont(18);
        /** Font. */
        private Font f24 = FontResource.BLENDER.getFont(24);
        /** Full screen background color. */
        private Color cBg = new DVBColor(12, 12, 12, 204);
        /** PopCallerIDUI background color. */
        private Color cPop = new Color(35, 35, 35);
        /** Button color. */
        private Color cButton = new Color(3, 3, 3);
        /** Title color. */
        private Color cTitle = new Color(251, 217, 89);
        /** Background image. */
//        private Image iBg;
        /** Gap image. */
        private Image iGap;
        /** Shadow image. */
//        private Image iShadow;
        /** Highlight image. */
//        private Image iHigh;
        /** Dimmed button image. */
        private Image iButtonDim;
        /** Button image. */
        private Image iButton;
        /** a string for ok button. */
        private String okStr;
        /** a string for cancel button. */
        private String cancelStr;

        /**
         * Paint a contents.
         * @param g Graphics to paint.
         * @param c parent UI.
         */
        public void paint(Graphics g, UIComponent c) {
            g.setColor(cBg);
            g.fillRect(0, 0, 960, 540);
//            g.drawImage(iBg, 250, 113, c);
//            g.drawImage(iShadow, 280, 399, 402, 78, c);
            g.setColor(cPop);
            g.fillRect(280, 143, 402, 259);
//            g.drawImage(iHigh, 280, 143, c);
            g.drawImage(iGap, 292, 181, c);
            g.setFont(f24);
            g.setColor(cTitle);
            GraphicUtil.drawStringCenter(g, title, 482, 169);
            g.setFont(f18);
            g.setColor(Color.white);
            int start = 270 - contents.length / 2 * 20;
            for (int i = 0; i < contents.length; i++) {
                GraphicUtil.drawStringCenter(g, contents[i], 483, start + i * 20);
            }
            int focus = c.getFocus();
            if (focus == 0) {
                g.drawImage(iButton, 321, 353, c);
                g.drawImage(iButtonDim, 485, 353, c);
            } else {
                g.drawImage(iButtonDim, 321, 353, c);
                g.drawImage(iButton, 485, 353, c);
            }
            g.setColor(cButton);
            GraphicUtil.drawStringCenter(g, okStr, 399, 374);
            GraphicUtil.drawStringCenter(g, cancelStr, 564, 374);

        }

        /**
         * Get a bounds.
         * @param c parent UI.
         * @return bounds.
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        /**
         * Prepare a renderer.
         * @param c parent UI.
         */
        public void prepare(UIComponent c) {
            okStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_YES);
            cancelStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_NO);

//            iBg = BaseUI.getImage("pop_d_glow_461.png");
            iGap = BaseUI.getImage("pop_gap_379.png");
//            iShadow = BaseUI.getImage("pop_sha_wide.png");
//            iHigh = BaseUI.getImage("pop_high_402.png");
            iButtonDim = BaseUI.getImage("05_focus_dim.png");
            iButton = BaseUI.getImage("05_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

}

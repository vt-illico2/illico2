/**
 * @(#)GeneralSettingsDetailUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Image;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.effect.ListEffect;
import com.videotron.tvi.illico.upp.gui.FavouriteChannelRenderer;
import com.videotron.tvi.illico.upp.gui.PopBaseRenderer;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This Class handle a mode and data for Favourite channels.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class FavouriteChannelUI extends BaseUI {

	/** Constant to indicate that left list has a focus. */
	public final int STATE_LEFT = 0;
	/** Constant to indicate that add button has a focus. */
	public final int STATE_ADD = 1;
	/** Constant to indicate that remove button has a focus. */
	public final int STATE_REMOVE = 2;
	/** Constant to indicate that right list has a focus. */
	public final int STATE_RIGHT = 3;
	/** Constant to indicate that User is editing in right list. */
	public final int STATE_ORDER = 4;
	/** Renderer. */
	private FavouriteChannelRenderer renderer = new FavouriteChannelRenderer();
	/** UI to display a data as list. */
	private FavouriteChannelListUI listUI;

	/** The data array of Favourite channel. */
	private Object[][] channelData;
	/** A Vector to have a data for right list. */
	private Vector rightListVector = new Vector();
	/** A Vector to have a data for left list. */
	private Vector leftListVector = new Vector();
	/** A vector to have a sister channel list */
	private Hashtable sisterTable = new Hashtable();
	/** Inidcate whether user select edit mode. */
	private boolean isEditOrderMode;
	/** a start index for left list. */
	private int leftListStartIdx;
	/** a start index for right list. */
	private int rightListStartIdx;
	/** a index of left focus. */
	private int leftFocus;
	/** a index of right focus. */
	private int rightFocus;
	/** a default logo for channel. */
	private Image defaultChannelLogo;

	private boolean isLoading;
	private Thread channelThread;

	/**
	 * Set a renderer.
	 */
	public FavouriteChannelUI() {
		renderer.setHasExpalin(false);
		setRenderer(renderer);
	}

	/**
	 * Prepare a UI.
	 */
	public void prepare() {
		super.prepare();
	}

	/**
	 * Start a UI.
	 * 
	 * @param back
	 *            ture if come from higher depth.
	 */
	public void start(boolean back) {
		if (!back) {
			prepare();
			addButton(BTN_BACK);
			addButton(BTN_PAGE);
		}

		if (listUI == null) {
			listUI = new FavouriteChannelListUI(this);
		}

		if (PreferenceController.getInstance().isChannelGrouped()) {
			channelThread = new Thread(new Runnable() {
				public void run() {
					try {
						isLoading = true;
						PreferenceController.getInstance().showLoading();
		
						defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
						channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);
						// defaultChannelLogo = (Image)
						// SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
						Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);
		
						if (Log.DEBUG_ON) {
							Log.printDebug("ProgramGuideUI: caTable size is " + caTable.size());
						}
		
						Integer chType;
						for (int i = 0; i < channelData.length; i++) {
		
							chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
							
							if (chType.intValue() == TvChannel.TYPE_TECH)
								continue;
							
							boolean isSubscribed = true;
							if (caTable != null) {
		
								Integer sourceId = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];
		
								Boolean subscribed = (Boolean) caTable.get(sourceId);
		
								if (subscribed != null) {
									isSubscribed = subscribed.booleanValue();
								}
							}
		
							String callLetterStr = ((String) channelData[i][BaseUI.INDEX_CHANNEL_NAME]);
							String sisterCalletterStr = (String) channelData[i][BaseUI.INDEX_CHANNEL_SISTER_NAME];
							if (chType.intValue() != TvChannel.TYPE_VIRTUAL
									&& isSubscribed
									&& (sisterCalletterStr == null || !sisterTable.containsKey(sisterCalletterStr))) {
								leftListVector.add(channelData[i]);
							}
	
							// if sister channel exist, put in sisterTable
							if (sisterCalletterStr != null && !sisterTable.containsKey(sisterCalletterStr)) {
								for (int sisterIdx = 0; sisterIdx < channelData.length; sisterIdx++) {
									if (sisterCalletterStr.equals(channelData[sisterIdx][BaseUI.INDEX_CHANNEL_NAME])) {
										sisterTable.put(callLetterStr, channelData[sisterIdx]);
										break;
									}
								}
							}
						}
		
						if (Log.DEBUG_ON) {
							if (channelData != null) {
								Log.printDebug("FavouriteChannelUI: prepare(): channelData : " + channelData.length);
								// for (int i = 0; i < channelData.length; i++) {
								// Log.printDebug("FavouriteChannelUI: prepare(): channelData : chNo :"
								// + channelData[i][INDEX_CHANNEL_NUMBER] + ", chName :"
								// + channelData[i][INDEX_CHANNEL_NAME]);
								// }
							} else {
								Log.printDebug("FavouriteChannelUI: prepare(): channelData is null");
							}
						}
		
						Preference pre = DataManager.getInstance().getPreference(PreferenceNames.FAVOURITE_CHANNEL);
		
						if (pre != null) {
							String storedData = pre.getCurrentValue();
							if (Log.DEBUG_ON) {
								Log.printDebug("storedData = " + storedData);
							}
							if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
								String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);
		
	//							for (int i = 0; i < parsedData.length; i++) {
	//								for (int j = 0; j < channelData.length; j++) {
	//									if (parsedData[i].equals(channelData[j][INDEX_CHANNEL_NAME])) {
	//        	                        	String calleter = (String) channelData[j][INDEX_CHANNEL_NAME];
	//        	                        	
	//        	                        	if (channelData[j][INDEX_CHANNEL_SISTER_NAME] == null 
	//        	                        			|| sisterTable.containsKey(calleter)) {
	//        		                            rightListVector.add(channelData[j]);
	//        		                            leftListVector.remove(channelData[j]);
	//        	                        	}
	//										break;
	//									}
	//								}
	//							}
								
								for (int i = 0; i < channelData.length; i++) {
									for (int j = 0; j < parsedData.length; j++) {
										if (parsedData[j].equals(channelData[i][INDEX_CHANNEL_NAME])) {
	        	                        	String calleter = (String) channelData[i][INDEX_CHANNEL_NAME];
	        	                        	
	        	                        	if (channelData[i][INDEX_CHANNEL_SISTER_NAME] == null 
	        	                        			|| sisterTable.containsKey(calleter)) {
	        		                            rightListVector.add(channelData[i]);
	        		                            leftListVector.remove(channelData[i]);
	        	                        	}
											break;
										}
									}
								}
							}
						}
		
						listUI.start(true);
						add(listUI);
		
						isEditOrderMode = false;
		
						if (leftListVector.size() == 0) {
							state = STATE_RIGHT;
							addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
							// addButton(BTN_EDIT_ORDER);
						} else {
							state = STATE_LEFT;
							if (rightListVector.size() > 0) {
								addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
							}
						}
						leftListStartIdx = 0;
						rightListStartIdx = 0;
						leftFocus = 0;
						rightFocus = 0;
		
						isLoading = false;
						repaint();
					} catch (Exception e) {
						e.printStackTrace();
					}
					PreferenceController.getInstance().hideLoading();
				}
			});
			
			channelThread.start();
		} else {
			sisterTable.clear();
			
			defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);
	        channelData = (Object[][]) SharedMemory.getInstance().get(EPG_DATA_KEY);
	        // defaultChannelLogo = (Image) SharedMemory.getInstance().get(DEFAULT_CHANNLE_LOGO);

	        Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(EpgService.DATA_KEY_CA_MAP);

	        if (Log.DEBUG_ON) {
	            Log.printDebug("ProgramGuideUI: caTable size is " + caTable.size());
	        }

	        Integer chType;
	        for (int i = 0; i < channelData.length; i++) {
	        	
	        	chType = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_TYPE];
				
				if (chType.intValue() == TvChannel.TYPE_TECH)
					continue;

	            boolean isSubscribed = true;
	            if (caTable != null) {

	                Integer sourceId = (Integer) channelData[i][BaseUI.INDEX_CHANNEL_SOURCE_ID];

	                Boolean subscribed = (Boolean) caTable.get(sourceId);

	                if (subscribed != null) {
	                    isSubscribed = subscribed.booleanValue();
	                }
	            }

	            if (chType.intValue() != TvChannel.TYPE_VIRTUAL && isSubscribed) {
	                leftListVector.add(channelData[i]);
	            }
	        }

	        if (Log.DEBUG_ON) {
	            if (channelData != null) {
	                Log.printDebug("FavouriteChannelUI: start(): channelData : " + channelData.length);
//					for (int i = 0; i < channelData.length; i++) {
//						Log.printDebug("FavouriteChannelUI: prepare(): channelData : chNo :" + channelData[i][INDEX_CHANNEL_NUMBER] + ", chName :" + channelData[i][INDEX_CHANNEL_NAME]);
//					}
	            } else {
	                Log.printDebug("FavouriteChannelUI: start(): channelData is null");
	            }
	        }

	        Preference pre = DataManager.getInstance().getPreference(PreferenceNames.FAVOURITE_CHANNEL);

	        if (pre != null) {
	            String storedData = pre.getCurrentValue();
	            if (Log.DEBUG_ON) {
	                Log.printDebug("storedData = " + storedData);
	            }
	            if (storedData != null && !storedData.equals(TextUtil.EMPTY_STRING)) {
	                String[] parsedData = TextUtil.tokenize(storedData, PreferenceService.PREFERENCE_DELIMETER);

	                for (int i = 0; i < parsedData.length; i++) {
	                    for (int j = 0; j < channelData.length; j++) {
	                        if (parsedData[i].equals(channelData[j][INDEX_CHANNEL_NAME])) {
	                        	if (rightListVector.size() > 0) {
		                        	for (int rightIdx = 0; rightIdx < rightListVector.size(); rightIdx++) {
		                        		Object[] item = (Object[]) rightListVector.get(rightIdx);
		                        		int itemNumber = ((Integer) item[INDEX_CHANNEL_NUMBER]).intValue();
		                        		int selectNumber = ((Integer) channelData[j][INDEX_CHANNEL_NUMBER]).intValue();
		                        		if (itemNumber > selectNumber) {
		                        			rightListVector.add(rightIdx, channelData[j]);
		                        			break;
		                        		}
		                        	}
		                        	
		                        	if (!rightListVector.contains(channelData[j])) {
		                        		rightListVector.add(channelData[j]);
		                        	}
	                        	} else {
	                        		rightListVector.add(channelData[j]);
	                        	}
	                            leftListVector.remove(channelData[j]);
	                            break;
	                        }
	                    }
	                }
	            }
	        }
	        
	        if (leftListVector.size() == 0) {
                state = STATE_RIGHT;
                addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
                // addButton(BTN_EDIT_ORDER);
            } else {
                state = STATE_LEFT;
                if (rightListVector.size() > 0) {
                    addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
                }
            }
            leftListStartIdx = 0;
            rightListStartIdx = 0;
            leftFocus = 0;
            rightFocus = 0;
            
            listUI.start(back);
            add(listUI);
		}

		super.start();
	}

	/**
	 * Stop a UI.
	 */
	public void stop() {
		remove(listUI);
		storeChannelData();
		leftListVector.clear();
		rightListVector.clear();
		clearButtons();
		clearPopup();
		super.stop();
		PreferenceController.getInstance().hideLoading();
		if (channelThread != null) {
			channelThread.interrupt();
		}
		channelThread = null;
		sisterTable.clear();
	}

	/**
	 * Handle a key code.
	 * 
	 * @param code
	 *            key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {
		if (getPopup() != null) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}
		switch (code) {
		case OCRcEvent.VK_LEFT:
			keyLeft();
			return true;
		case OCRcEvent.VK_RIGHT:
			keyRight();
			return true;
		case OCRcEvent.VK_UP:
			keyUp();
			return true;
		case OCRcEvent.VK_DOWN:
			keyDown();
			return true;
		case KeyEvent.VK_PAGE_UP:
			clickButtons(BTN_PAGE);
			keyPageUp();
			return true;
		case KeyEvent.VK_PAGE_DOWN:
			clickButtons(BTN_PAGE);
			keyPageDown();
			return true;
		case OCRcEvent.VK_ENTER:
			if (isEditOrderMode && state == STATE_RIGHT) {
				isEditOrderMode = false;
				repaint();
			} else {
				keyOK();
			}
			return true;
		case KeyCodes.LAST:
			clickButtons(BTN_BACK);
			// startFadeOutEffect(this);
			popHistory();
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_PROGRAM_GUIDE, true);
			return true;
			// case KeyCodes.COLOR_A:
			// if (state == STATE_RIGHT) {
			// isEditOrderMode = true;
			// repaint();
			// }
			// return true;
		case KeyCodes.COLOR_B:
			clickButtons(BTN_CLEAR_FAVOURITE_CHANNELS);
			if (rightListVector.size() > 0) {
				setPopup(notiUI);
				String explain = BaseUI.getPopupText(KeyNames.EXPLAIN_CLEAR_FAVOURITE_CHANNELS);
				notiUI.show(this, BaseUI.getPopupText(KeyNames.TITLE_CONFIRM_DELETION),
						TextUtil.split(explain, renderer.fm18, 310), PopBaseRenderer.ICON_TYPE_NONE, true);
				repaint();
			}
			return true;
		default:
			return false;
		}
	}

	/**
	 * Handle a left key.
	 */
	private void keyLeft() {
		if ((state == STATE_ADD || state == STATE_REMOVE) && leftListVector.size() > 0) {
			state = STATE_LEFT;
		} else if (state == STATE_RIGHT) {
			state = STATE_REMOVE;
			isEditOrderMode = false;
			clearButtons();
			addButton(BTN_BACK);
			addButton(BTN_PAGE);
			addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
		}
		repaint();
	}

	/**
	 * Handle a right key.
	 */
	private void keyRight() {
		if (state == STATE_LEFT) {
			state = STATE_ADD;
		} else if ((state == STATE_ADD || state == STATE_REMOVE) && rightListVector.size() > 0) {
			state = STATE_RIGHT;
			clearButtons();
			addButton(BTN_BACK);
			addButton(BTN_PAGE);
			// addButton(BTN_EDIT_ORDER);
			addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
		}
		repaint();
	}

	/**
	 * Handle a up key.
	 */
	private void keyUp() {
		if (state == STATE_LEFT) {
			if (leftListStartIdx + leftFocus > 0) {
				if (leftFocus > 4) {
					leftFocus--;
				} else if (leftListStartIdx > 0) {
					if (leftFocus < 5) {
						leftListStartIdx--;
					} else {
						leftFocus--;
					}
				} else {
					leftFocus--;
				}
			}
		} else if (state == STATE_RIGHT) {
			int currentIdx = rightListStartIdx + rightFocus;
			if (currentIdx > 0) {
				if (rightFocus > 4) {
					rightFocus--;
				} else if (rightListStartIdx > 0) {
					if (rightFocus < 5) {
						rightListStartIdx--;
					} else {
						rightFocus--;
					}
				} else {
					rightFocus--;
				}
			}
			int newIdx = rightListStartIdx + rightFocus;
			if (isEditOrderMode) {
				swapItem(currentIdx, newIdx);
			}
		}

		if (Log.DEBUG_ON) {
			Log.printDebug("FavouriteChannelUI: keyUp(): leftListStartIdx = " + leftListStartIdx);
			Log.printDebug("FavouriteChannelUI: keyUp(): leftFocus = " + leftFocus);
			Log.printDebug("FavouriteChannelUI: keyUp(): leftListVector = " + leftListVector.size());
			Log.printDebug("FavouriteChannelUI: keyUp(): rightListStartIdx = " + rightListStartIdx);
			Log.printDebug("FavouriteChannelUI: keyUp(): rightFocus = " + rightFocus);
			Log.printDebug("FavouriteChannelUI: keyUp(): rightListVector = " + rightListVector.size());
		}
		repaint();
	}

	/**
	 * Handle a down key.
	 */
	private void keyDown() {
		if (state == STATE_LEFT) {
			if (leftListStartIdx + leftFocus < leftListVector.size() - 1) {
				if (leftFocus < 4) {
					leftFocus++;
				} else if (leftListStartIdx + 9 < leftListVector.size()) {
					if (leftFocus > 3) {
						leftListStartIdx++;
					} else {
						leftFocus++;
					}
				} else {
					leftFocus++;
				}
			}
		} else if (state == STATE_RIGHT) {
			int currentIdx = rightListStartIdx + rightFocus;
			if (currentIdx < rightListVector.size() - 1) {
				if (rightFocus < 4) {
					rightFocus++;
				} else if (rightListStartIdx + 9 < rightListVector.size()) {
					if (rightFocus > 3) {
						rightListStartIdx++;
					} else {
						rightFocus++;
					}
				} else {
					rightFocus++;
				}
			}
			int newIdx = rightListStartIdx + rightFocus;
			if (isEditOrderMode) {
				swapItem(currentIdx, newIdx);
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("FavouriteChannelUI: keyDown(): leftListStartIdx = " + leftListStartIdx);
			Log.printDebug("FavouriteChannelUI: keyDown(): leftFocus = " + leftFocus);
			Log.printDebug("FavouriteChannelUI: keyDown(): leftListVector = " + leftListVector.size());
			Log.printDebug("FavouriteChannelUI: keyDown(): rightListStartIdx = " + rightListStartIdx);
			Log.printDebug("FavouriteChannelUI: keyDown(): rightFocus = " + rightFocus);
			Log.printDebug("FavouriteChannelUI: keyDown(): rightListVector = " + rightListVector.size());
		}
		repaint();
	}

	/**
	 * Handle a ok key.
	 */
	private void keyOK() {
		boolean down = false;
		if ((state == STATE_LEFT || state == STATE_ADD) && leftListVector.size() > 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("FavouriteChannelUI: keyOK(): leftListStartIdx = " + leftListStartIdx);
				Log.printDebug("FavouriteChannelUI: keyOK(): leftFocus = " + leftFocus);
				Log.printDebug("FavouriteChannelUI: keyOK(): leftListVector = " + leftListVector.size());
			}
			int addedIdx = moveLeftToRight(leftListVector.get(leftListStartIdx + leftFocus));

			if (leftListStartIdx + leftFocus >= leftListVector.size()) {
				if (leftListStartIdx + leftFocus > 0) {
					if (leftFocus > 4) {
						leftFocus--;
					} else if (leftListStartIdx > 0) {
						if (leftFocus <= 4) {
							leftListStartIdx--;
						} else {
							leftFocus--;
						}
					} else {
						leftFocus--;
					}
					down = true;
				}
			}

			if (leftListVector.size() > 0) {
				addedIdx = addedIdx - rightListStartIdx;
				listUI.startEffect(ListEffect.MODE_FROM_LEFT, leftFocus, addedIdx, down);
			}

			if (leftListVector.size() == 0) {
				clearButtons();
				addButton(BTN_BACK);
				addButton(BTN_PAGE);
				// addButton(BTN_EDIT_ORDER);
				addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
				state = STATE_RIGHT;
			} else if (rightListVector.size() > 0) {
				clearButtons();
				addButton(BTN_BACK);
				addButton(BTN_PAGE);
				addButton(BTN_CLEAR_FAVOURITE_CHANNELS);
			}
		} else if ((state == STATE_RIGHT || state == STATE_REMOVE) && rightListVector.size() > 0) {
			if (Log.DEBUG_ON) {
				Log.printDebug("FavouriteChannelUI: keyOK(): rightListStartIdx = " + rightListStartIdx);
				Log.printDebug("FavouriteChannelUI: keyOK(): rightFocus = " + rightFocus);
				Log.printDebug("FavouriteChannelUI: keyOK(): rightListVector = " + rightListVector.size());
			}
			int addedIdx = moveRightToLeft(rightListVector.get(rightListStartIdx + rightFocus));
			if (rightListStartIdx + rightFocus >= rightListVector.size()) {
				if (rightListStartIdx + rightFocus > 0) {
					if (rightFocus > 4) {
						rightFocus--;
					} else if (rightListStartIdx > 0) {
						if (rightFocus <= 4) {
							rightListStartIdx--;
						} else {
							rightFocus--;
						}
					} else {
						rightFocus--;
					}
					down = true;
				}
			}

			if (rightListVector.size() > 0) {
				addedIdx = addedIdx - leftListStartIdx;
				listUI.startEffect(ListEffect.MODE_FROM_RIGHT, addedIdx, rightFocus, down);
			}

			if (rightListVector.size() == 0) {
				clearButtons();
				addButton(BTN_BACK);
				addButton(BTN_PAGE);
				state = STATE_LEFT;
			}
		}
		repaint();
	}

	/**
	 * Handle a page up key.
	 */
	private void keyPageUp() {
		if (state == STATE_LEFT) {
			leftListStartIdx -= 9;
			if (leftListStartIdx < 0) {
				leftListStartIdx = 0;
			}
		} else if (state == STATE_RIGHT) {
			rightListStartIdx -= 9;
			if (rightListStartIdx < 0) {
				rightListStartIdx = 0;
			}
		}
		repaint();
	}

	/**
	 * Handle a page down key.
	 */
	private void keyPageDown() {
		if (Log.DEBUG_ON) {
			Log.printDebug("FavouriteChannelUI: keyPageDown(): state = " + state);

		}
		if (state == STATE_LEFT) {

			if (leftListStartIdx + 9 < leftListVector.size()) {
				leftListStartIdx += 9;
			}

			if (leftListStartIdx + 9 >= leftListVector.size()) {
				leftFocus = leftListVector.size() - leftListStartIdx - 1;

				if (leftFocus < 0) {
					leftFocus = 0;
				}
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("FavouriteChannelUI: keyPageDown(): leftListStartIdx = " + leftListStartIdx);
				Log.printDebug("FavouriteChannelUI: keyPageDown(): leftFocus = " + leftFocus);
				Log.printDebug("FavouriteChannelUI: keyPageDown(): leftListVector = " + leftListVector.size());
			}
		} else if (state == STATE_RIGHT) {

			if (rightListStartIdx + 9 < rightListVector.size()) {
				rightListStartIdx += 9;
			}

			if (rightListStartIdx + 9 >= rightListVector.size()) {
				rightFocus = rightListVector.size() - rightListStartIdx - 1;

				if (rightFocus < 0) {
					rightFocus = 0;
				}
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("FavouriteChannelUI: keyPageDown(): rightListStartIdx = " + rightListStartIdx);
				Log.printDebug("FavouriteChannelUI: keyPageDown(): rightFocus = " + rightFocus);
				Log.printDebug("FavouriteChannelUI: keyPageDown(): rightListVector = " + rightListVector.size());
			}
		}
		repaint();
	}

	/**
	 * Change a position between current item and new item in right list.
	 * 
	 * @param curIdx
	 *            The index of current Item
	 * @param newIdx
	 *            The index of new Item.
	 */
	private void swapItem(int curIdx, int newIdx) {
		if (curIdx < newIdx) {
			Object curObj = rightListVector.remove(curIdx);
			rightListVector.add(newIdx, curObj);
		} else {
			Object newObj = rightListVector.remove(newIdx);
			rightListVector.add(curIdx, newObj);
		}
	}

	/**
	 * Store a channel data for favourite channel.
	 */
	private void storeChannelData() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < rightListVector.size(); i++) {
			Object[] data = (Object[]) rightListVector.get(i);
			if (i != 0) {
				sb.append(PreferenceService.PREFERENCE_DELIMETER);
			}
			sb.append((String) data[INDEX_CHANNEL_NAME]);
			
			Object[] sisterChannelData = (Object[]) sisterTable.get((String) data[INDEX_CHANNEL_NAME]);
            
            if (sisterChannelData != null) {
            	String callLetter = (String) sisterChannelData[INDEX_CHANNEL_NAME];
            	sb.append(PreferenceService.PREFERENCE_DELIMETER);
				sb.append(callLetter);
            }
		}
		
//		// add a sister channel
//        for (int i = 0; i < rightListVector.size(); i++) {
//            Object[] data = (Object[]) rightListVector.get(i);
//            
//            Object[] sisterChannelData = (Object[]) sisterTable.get((String) data[INDEX_CHANNEL_NAME]);
//            
//            if (sisterChannelData != null) {
//            	String callLetter = (String) sisterChannelData[INDEX_CHANNEL_NAME];
//            	sb.append(PreferenceService.PREFERENCE_DELIMETER);
//				sb.append(callLetter);
//            }
//        }
        
		DataManager.getInstance().setPreference(PreferenceNames.FAVOURITE_CHANNEL, sb.toString());
	}

	/**
	 * Determine whether mode is edit mode or not.
	 * 
	 * @return true if mode is edit mode, false otherwise.
	 */
	public boolean isEditOrderMode() {
		return isEditOrderMode;
	}

	/**
	 * Get a current State.
	 * 
	 * @return one of {@link #STATE_ADD} or {@link #STATE_LEFT} or
	 *         {@link #STATE_ORDER} or {@link #STATE_REMOVE} or
	 *         {@link #STATE_RIGHT}
	 */
	public int getCurrentState() {
		return state;
	}

	/**
	 * Get a start index of left list.
	 * 
	 * @return start index.
	 */
	public int getLeftListStartIdx() {
		return leftListStartIdx;
	}

	/**
	 * Get a start index of right list.
	 * 
	 * @return start index
	 */
	public int getRightListStartIdx() {
		return rightListStartIdx;
	}

	/**
	 * Move a data from left to right.
	 * 
	 * @param o
	 *            channel data
	 * @return index to be added in right list.
	 */
	public int moveLeftToRight(Object o) {
		if (Log.DEBUG_ON) {
			Object[] channel = (Object[]) o;
			Log.printDebug("FavouriteChannelUI: moveLeftToRight(): channel no = "
					+ channel[BaseUI.INDEX_CHANNEL_NUMBER]);
			Log.printDebug("FavouriteChannelUI: moveLeftToRight(): channel name = "
					+ channel[BaseUI.INDEX_CHANNEL_NAME]);
		}

		leftListVector.remove(o);
		return addRightList(o);
	}

	/**
	 * Move a data from right to left.
	 * 
	 * @param o
	 *            channel data
	 * @return index to be added in left list.
	 */
	public int moveRightToLeft(Object o) {
		if (Log.DEBUG_ON) {
			Object[] channel = (Object[]) o;
			Log.printDebug("FavouriteChannelUI: moveRightToLeft(): channel no = "
					+ channel[BaseUI.INDEX_CHANNEL_NUMBER]);
			Log.printDebug("FavouriteChannelUI: moveRightToLeft(): channel name = "
					+ channel[BaseUI.INDEX_CHANNEL_NAME]);
		}

		rightListVector.remove(o);
		return addLeftList(o);
	}

	/**
	 * Add a channel data into left list.
	 * 
	 * @param o
	 *            channel data
	 * @return index to be added.
	 */
	public int addLeftList(Object o) {
		Object[] source = (Object[]) o;
		int sourceChNo = ((Integer) source[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
		int size = leftListVector.size();
		for (int i = 0; i < size; i++) {
			Object[] data = (Object[]) leftListVector.get(i);
			int dataChNo = ((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
			if (dataChNo > sourceChNo) {
				leftListVector.add(i, o);
				return i;
			}
		}
		leftListVector.add(o);
		return leftListVector.size() - 1;
	}

	/**
	 * Add a channel data into right list.
	 * 
	 * @param o
	 *            channel data
	 * @return index to be added.
	 */
	public int addRightList(Object o) {
		Object[] source = (Object[]) o;
		int sourceChNo = ((Integer) source[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
		int size = rightListVector.size();
		for (int i = 0; i < size; i++) {
			Object[] data = (Object[]) rightListVector.get(i);
			int dataChNo = ((Integer) data[BaseUI.INDEX_CHANNEL_NUMBER]).intValue();
			if (dataChNo > sourceChNo) {
				rightListVector.add(i, o);
				return i;
			}
		}
		rightListVector.add(o);
		return rightListVector.size() - 1;
	}

	/**
	 * Get a Vector to include a channel data for left list.
	 * 
	 * @return Vector to include a channel data.
	 */
	public Vector getLeftListData() {
		return leftListVector;
	}

	/**
	 * Get a Vector to include a channel data for right list.
	 * 
	 * @return Vector to include a channel data.
	 */
	public Vector getRightListData() {
		return rightListVector;
	}
	
	public Hashtable getSisterTable() {
    	return sisterTable;
    }

	/**
	 * Get a index of focus for left list.
	 * 
	 * @return index of focus for left list.
	 */
	public int getLeftListFocus() {
		return leftFocus;
	}

	/**
	 * Get a focus of right list.
	 * 
	 * @return index of focus for right list.
	 */
	public int getRightListFocus() {
		return rightFocus;
	}

	/**
	 * Get a subtitle for left list.
	 * 
	 * @param prefix
	 *            string to be added at head.
	 * @return subtitle for left list.
	 */
	public String getLeftListSubTitle(String prefix) {
		int countSister = 0;
    	for (int i = 0; i < leftListVector.size(); i++) {
    		Object[] data = (Object[]) leftListVector.get(i);
            String callLetter = (String) data[BaseUI.INDEX_CHANNEL_NAME];
            
            if (sisterTable.containsKey(callLetter)) {
            	countSister++;
            }
    	}
    	
        StringBuffer sf = new StringBuffer();
        sf.append(prefix);
        sf.append(" (");
        if (isLoading) {
        	sf.append(0);
        } else {
        	sf.append(leftListVector.size() + countSister);
        }
        sf.append(")");

		return sf.toString();
	}

	/**
	 * Get a subtitle for right list.
	 * 
	 * @param prefix
	 *            string to be added at head.
	 * @return subtitle for right list.
	 */
	public String getRightListSubTitle(String prefix) {
		int countSister = 0;
    	for (int i = 0; i < rightListVector.size(); i++) {
    		Object[] data = (Object[]) rightListVector.get(i);
            String callLetter = (String) data[BaseUI.INDEX_CHANNEL_NAME];
            
            if (sisterTable.containsKey(callLetter)) {
            	countSister++;
            }
    	}
    	
        StringBuffer sf = new StringBuffer();
        sf.append(prefix);
        sf.append(" (");
        if (isLoading) {
        	sf.append(0);
        } else {
        	sf.append(rightListVector.size() + countSister);
        }
        sf.append(")");

		return sf.toString();
	}

	/**
	 * Get a logo image for channel. if can't find a image, return a default
	 * logo image.
	 * 
	 * @param name
	 *            channel name.
	 * @return logo image.
	 */
	public Image getCurrentChannelLogo(String name) {
		Image logo;
		logo = (Image) SharedMemory.getInstance().get("logo." + name);

		if (logo == null) {
			return defaultChannelLogo;
		} else {
			return logo;
		}
	}

	/**
	 * Handle a action after popup is closed.
	 */
	public void popupClosed(BaseUI pop, Object msg) {
		removePopup(pop);
		int action = ((Integer) msg).intValue();
		repaint();
		if (pop.equals(notiUI)) {
			if (action == BaseUI.ACTION_OK) {
				for (; rightListVector.size() > 0;) {
					moveRightToLeft(rightListVector.get(0));
				}
				rightListStartIdx = 0;
				rightFocus = 0;
				leftListStartIdx = 0;
				leftFocus = 0;
				clearButtons();
				addButton(BTN_BACK);
				addButton(BTN_PAGE);
				state = STATE_LEFT;
				repaint();
			}
		}
	}
}

/**
 * @(#)EquipmentSetupUI.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.util.Hashtable;

import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.controller.PreferenceActionController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.DataManager;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.upp.gui.EquipmentSetupRenderer;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handle a preferences of Equipment setup.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class EquipmentSetupUI extends BaseUI {

	/** Renderer. */
	private EquipmentSetupRenderer renderer = new EquipmentSetupRenderer();
	/** Explain UI on botton. */
	private ExplainUI explainUI = new ExplainUI();
	/** Popup to select a values. */
	private PopSelectValueUI selectUI = new PopSelectValueUI();
	/** Popup to select a image values. */
	private PopSelectValueByImageUI selectImgUI = new PopSelectValueByImageUI();

	/** Preferences ids for Equipment setup. */
	public final String[] PREFERENCE_ID_LIST = { PreferenceNames.AUTOMATIC_POWER_DOWN, PreferenceNames.STB_SLEEP_TIME,
			PreferenceNames.STB_WAKE_UP_TIME, PreferenceNames.REMOTE_CONTROL_TYPE,
			PreferenceNames.VIDEO_TV_PICTURE_FORMAT, PreferenceNames.VOD_1080P_SUPPORT,
			PreferenceNames.AUDIO_VOLUME_CONTROL, 
			// VDTRMASTER-6143 R7.3 SAMSUNG UHD에서 기본값을 AC3로 설정하는 경우 HDMI통한 오디오가나오지않는 이슈가 있어서,
			//AUDIOOUTPUT메뉴를 주석처리한다. 기존 FW의 기본값은 'DISPLAY'였음
			// R7.4 sykim VTPERISCOPE-380 and VDTRMASTER-5277
			PreferenceNames.AUDIO_OUTPUT,
			PreferenceNames.DISPLAY_POWER_OFF}; 

	// VDTRMASTER-5756 - add VOD_1080P_SUPPORT
	public final String[] PREFERENCE_ID_LIST_4K = { PreferenceNames.AUTOMATIC_POWER_DOWN,
			PreferenceNames.STB_SLEEP_TIME, PreferenceNames.STB_WAKE_UP_TIME, PreferenceNames.REMOTE_CONTROL_TYPE,
			PreferenceNames.VIDEO_TV_PICTURE_FORMAT, PreferenceNames.VOD_1080P_SUPPORT,
			PreferenceNames.AUDIO_VOLUME_CONTROL, 
			// VDTRMASTER-6143 R7.3 SAMSUNG UHD에서 기본값을 AC3로 설정하는 경우 HDMI통한 오디오가나오지않는 이슈가 있어서,
			//AUDIOOUTPUT메뉴를 주석처리한다. 기존 FW의 기본값은 'DISPLAY'였음
			// R7.4 sykim VTPERISCOPE-380 and VDTRMASTER-5277
			PreferenceNames.AUDIO_OUTPUT,
			PreferenceNames.DISPLAY_POWER_OFF}; 

	/** Hashtable to include a preferences. */
	private Hashtable preTable;
	/** Start Index. */
	private int startIdx = 0;

	/** Clicking Effect. */
	private ClickingEffect clickEffect;

	private int focusAPD = 0;

	private String[] preferenceIds;
	private boolean fromOutSide = false;
	
	/**
	 * Set a renderer and add ExplainUI.
	 */
	public EquipmentSetupUI() {
		setRenderer(renderer);
		add(explainUI);
		clickEffect = new ClickingEffect(this, 5);
	}

	/**
	 * Prepare a UI.
	 */
	public void prepare() {
		if (Log.DEBUG_ON) {
			Log.printDebug("EquipmentSetup, prepare(), support_uhd=" + Environment.SUPPORT_UHD);
		}
		if (Environment.SUPPORT_UHD) {
			preferenceIds = PREFERENCE_ID_LIST_4K;
		} else {
			preferenceIds = PREFERENCE_ID_LIST;
		}

		String[] params = (String[]) DataCenter.getInstance().get("PARAMS");
		if (Log.DEBUG_ON) {
			Log.printDebug("EquipmentSetup, prepare(), params=" + params );
		}
		String cId;
		if (params != null && params.length > 1) {
			fromOutSide = true;
			DataCenter.getInstance().remove("PARAMS");
			SubCategory sub = new SubCategory();
			sub.setCategoryId("settings");
			addHistory(sub);
			cId = "equipment.setup";
		} else {
			fromOutSide = false;
			cId = ((Category) peekHistory()).getCategoryId();
		}
		startIdx = 0;
		focusAPD = 0;
		preTable = DataManager.getInstance().getPreferences(cId);

		super.prepare();
	}

	/**
	 * Start a UI.
	 * 
	 * @param back
	 *            true if come from lower depth, false otherwise.
	 */
	public void start(boolean back) {

		if (!back) {
			prepare();
			focus = 0;

			setExplainText();
		}
		addButton(BTN_BACK);
		super.start();
	}

	/**
	 * Stop a UI.
	 */
	public void stop() {
		clearPopup();
		clearButtons();
		explainUI.stop();
		super.stop();
	}

	/**
	 * Get a preferences.
	 * 
	 * @return Hashtable to include a preferences.
	 */
	public Hashtable getPreferences() {
		return preTable;
	}

	/**
	 * Handle a key code.
	 * 
	 * @param code
	 *            key code.
	 * @return true if key code is used, false otherwise.
	 */
	public boolean handleKey(int code) {

		if (getPopup() != null) {
			BaseUI popUp = getPopup();
			if (popUp.handleKey(code)) {
				return true;
			}
		}

		switch (code) {
		case OCRcEvent.VK_UP:
			if (focus == 3) {
				if (startIdx > 0) {
					startIdx--;
				} else {
					focus--;
				}
			} else {
				focus--;
			}
			if (focus < 0) {
				focus = 0;
			}

			if (startIdx + focus == 0) {
				focusAPD = 0;
			}
			setExplainText();
			repaint();
			return true;
		case OCRcEvent.VK_DOWN:
			if (focus == 3) {
				if (startIdx + 7 < preferenceIds.length) {
					startIdx++;
				} else {
					focus++;
				}
			} else if (startIdx + focus < preferenceIds.length - 1) {
				focus++;
			}
			if (focus > 6) {
				focus = 6;
			}
			setExplainText();
			repaint();
			return true;
		case OCRcEvent.VK_LEFT:
			if ((startIdx + focus) == 0 && focusAPD == 1) {
				focusAPD = 0;
				repaint();
			}
			return true;
		case OCRcEvent.VK_RIGHT:
			// VDTRMASTER-6009
			if ((startIdx + focus) == 0 && focusAPD == 0 && canDisplayAPDTime()) {
				focusAPD = 1;
				repaint();
			}
			return true;
		case OCRcEvent.VK_ENTER:
			int idx = startIdx + focus;
			if (idx == 0 && focusAPD == 1) {
				clickEffect.start(BaseRenderer.INPUT_X + 229, 141 + focus * 37, 211, 33);
			} else {
				clickEffect.start(BaseRenderer.INPUT_X, 141 + focus * 37, 211, 33);
			}

			if (idx == 0) {
				// Automatic power down
				setPopup(selectUI);
				if (focusAPD == 0) {
					Preference pre = (Preference) preTable.get(preferenceIds[idx]);
					selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
							83 + focus * 37, 3);
				} else {
					Preference pre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME);

					selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519 + 170,
							83 + focus * 37, 3);
				}

				repaint();
			} else if (idx == 1) {
				// sleep time
				Preference pre = (Preference) preTable.get(PreferenceNames.STB_SLEEP_TIME);
				addHistory(pre);
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_SLEEP_TIME, false);
			} else if (idx == 2) {
				// wake up time
				Preference pre = (Preference) preTable.get(PreferenceNames.STB_WAKE_UP_TIME);
				addHistory(pre);
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_WAKE_UP_TIME, false);
			} else if (idx == 3) {
				// Remote control type
				setPopup(selectImgUI);
				Preference pre = (Preference) preTable.get(preferenceIds[idx]);
				selectImgUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(),
						selectImgUI.STATE_REMOTE_CONTROL_TYPE, 519, 83 + focus * 37);
				repaint();
			} else if (idx == 4) {
				// TV picture format
				Preference pre = (Preference) preTable.get(PreferenceNames.VIDEO_TV_PICTURE_FORMAT);
				addHistory(pre);
				PreferenceController.getInstance().showUIComponent(BaseUI.UI_TV_PICTURE_FORMAT, false);
			} else if (preferenceIds[idx].equals(PreferenceNames.VOD_1080P_SUPPORT)
					|| preferenceIds[idx].equals(PreferenceNames.BYPASS_TERMINAL_CONVERSION)) {
				setPopup(selectUI);
				Preference pre = (Preference) preTable.get(preferenceIds[idx]);

				String enableStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_ENABLE + "d");
				String disableStr = BaseUI.getOptionText(Definitions.OPTION_VALUE_DISABLE + "d");
				String focusValue = pre.getCurrentValue();
				focusValue = BaseUI.getOptionText(focusValue + "d");

				selectUI.show(this, new String[] { enableStr, disableStr }, focusValue, pre.getName(), 519,
						83 + focus * 37, 3);
				repaint();
			} else {
				setPopup(selectUI);
				Preference pre = (Preference) preTable.get(preferenceIds[idx]);
				selectUI.show(this, pre.getSelectableValues(), pre.getCurrentValue(), pre.getName(), 519,
						83 + focus * 37, 3);
				repaint();
			}
			return true;
		case KeyCodes.LAST:
			clickButtons(BTN_BACK);
			// startFadeOutEffect(this);
			popHistory();
			PreferenceController.getInstance().showUIComponent(BaseUI.UI_MY_SETTINGS, true);
			return true;
		default:
			return false;
		}
	}

	/**
	 * Get a start index.
	 * 
	 * @return start index.
	 */
	public int getStartIdx() {
		return startIdx;
	}

	public int getFocusAPD() {
		return focusAPD;
	}

	/**
	 * Set a explain text.
	 */
	private void setExplainText() {
		String text = "";

		if (preferenceIds[startIdx + focus].equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT) && Environment.SUPPORT_UHD) {
			text = getExplainText(preferenceIds[startIdx + focus] + ".4k");
		} else {
			text = getExplainText(preferenceIds[startIdx + focus]);
		}
		explainUI.setText(text);
		explainUI.start(false);
	}

	/**
	 * Get a sleep time information.
	 * 
	 * @return a string for Enable or Disable.
	 */
	public String getSleepTimeInfo() {
		Preference pre = (Preference) preTable.get(PreferenceNames.STB_SLEEP_TIME);
		String[] data = TextUtil.tokenize(pre.getCurrentValue(), PreferenceService.PREFERENCE_DELIMETER);
		if (data != null && data.length > 0) {
			String value = BaseUI.getOptionText(data[0] + "d");
			return value;
		} else {
			return TextUtil.EMPTY_STRING;
		}
	}

	/**
	 * Get a wake up time information.
	 * 
	 * @return a string for Enable or Disable.
	 */
	public String getWakeUpTimeInfo() {
		Preference pre = (Preference) preTable.get(PreferenceNames.STB_WAKE_UP_TIME);
		String[] data = TextUtil.tokenize(pre.getCurrentValue(), PreferenceService.PREFERENCE_DELIMETER);
		if (data != null && data.length > 0) {
			String value = BaseUI.getOptionText(data[0] + "d");
			return value;
		} else {
			return TextUtil.EMPTY_STRING;
		}
	}

	public boolean canDisplayAPDTime() {
		Preference pre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN);

		if (pre != null) {
			if (pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE)) {
				return true;
			}
		}

		return false;
	}

	public String[] getPreferenceIds() {
		return preferenceIds;
	}

	public boolean isAutoDetected() {
		Preference pre = (Preference) DataManager.getInstance().getPreference(
				PreferenceNames.AUTO_DETECTED_TV_PICTURE_FORMAT);

		if (pre != null && pre.getCurrentValue().equals(Definitions.OPTION_VALUE_ENABLE)) {
			return true;
		}

		return false;
	}

	public String getTVPictureFormatText() {

		Preference pre = (Preference) preTable.get(PreferenceNames.VIDEO_TV_PICTURE_FORMAT);
		boolean isAutoDetected = isAutoDetected();

		if (Environment.SUPPORT_UHD && isAutoDetected) {
			String autoStr = BaseUI.getOptionText("Auto");
			// VDTRMASTER-5821
			String detectedStr = BaseUI.getOptionText("detect");

			StringBuffer sf = new StringBuffer();
			sf.append(autoStr + " - ");
			sf.append(pre.getCurrentValue());
			sf.append(" " + detectedStr);

			return sf.toString();
		} else {
			return pre.getCurrentValue();
		}
	}

	/**
	 * Handle a action after popup closed.
	 * 
	 * @param pop
	 *            popup to be closed.
	 * @param msg
	 *            message from popup.
	 */
	public void popupClosed(BaseUI pop, Object msg) {

		if (pop.equals(selectUI)) {
			int action = ((Integer) msg).intValue();
			removePopup(selectUI);
			repaint();
			if (action == BaseUI.ACTION_OK) {

				String selectedValue = selectUI.getSelectedValue();
				int idx = startIdx + focus;
				Preference pre = (Preference) preTable.get(preferenceIds[idx]);
				if (idx == 0 && focusAPD == 1) {
					pre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME);
				} else if (preferenceIds[idx].equals(PreferenceNames.VOD_1080P_SUPPORT)
						|| preferenceIds[idx].equals(PreferenceNames.BYPASS_TERMINAL_CONVERSION)) {
					if (selectUI.getFocus() == 0) {
						selectedValue = Definitions.OPTION_VALUE_ENABLE;
					} else {
						selectedValue = Definitions.OPTION_VALUE_DISABLE;
					}
				}

				pre.setCurrentValue(selectedValue);
				preTable.put(pre.getID(), pre);
				DataManager.getInstance().setPreference(pre.getID(), selectedValue);

				if (idx == 0 && focusAPD == 0 && selectedValue.equals(Definitions.OPTION_VALUE_ENABLE)) {
					pre = (Preference) preTable.get(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME);
					pre.setCurrentValue(Definitions.FOUR_HOURS);
					//R7.3 sangyong kim VDTRMASTER-6099
					DataManager.getInstance().setPreference(PreferenceNames.AUTOMATIC_POWER_DOWN_TIME, Definitions.FOUR_HOURS);
					focusAPD = 1;
				}

				if (preferenceIds[idx].equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT)
						|| preferenceIds[idx].equals(PreferenceNames.VIDEO_ZOOM_MODE)
						|| preferenceIds[idx].equals(PreferenceNames.AUDIO_OUTPUT)) {

					PreferenceActionController.getInstance().doAction(pre.getID(), selectedValue);
				} else if (preferenceIds[idx].equals(PreferenceNames.TERMINAL_POWER_OUTLET)) {
					if (selectedValue.equals(Definitions.OPTION_VALUE_ON)) {
						Host.getInstance().setACOutlet(true);
					} else {
						Host.getInstance().setACOutlet(false);
					}
				}
			}
		} else if (pop.equals(selectImgUI)) {
			int action = ((Integer) msg).intValue();
			removePopup(selectImgUI);
			repaint();
			if (action == BaseUI.ACTION_OK) {
				String selectedValue = selectImgUI.getSelectedValue();
				int idx = startIdx + focus;
				Preference pre = (Preference) preTable.get(preferenceIds[idx]);
				pre.setCurrentValue(selectedValue);
				preTable.put(pre.getID(), pre);
				DataManager.getInstance().setPreference(pre.getID(), selectedValue);

				if (pre.getID().equals(PreferenceNames.REMOTE_CONTROL_TYPE)) {
					PreferenceActionController.getInstance().doAction(pre.getID(), selectedValue);
				}
			}
		}
	}
}

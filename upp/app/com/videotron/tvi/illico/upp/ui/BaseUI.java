/**
 * @(#)Base.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.upp.KeyNames;
import com.videotron.tvi.illico.upp.data.Category;
import com.videotron.tvi.illico.upp.data.MainCategory;
import com.videotron.tvi.illico.upp.data.Preference;
import com.videotron.tvi.illico.upp.data.SubCategory;
import com.videotron.tvi.illico.upp.data.User;
import com.videotron.tvi.illico.upp.gui.BaseRenderer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The BaseUI component is a parent class of All UI class.
 * @author Woojung Kim
 * @version 1.1
 */
public class BaseUI extends UIComponent implements ClockListener {

    /** GUI Renderer of this UI. */
    protected BaseRenderer renderer;
    /** A instance of DataCenter. */
    protected static DataCenter dataCenter = DataCenter.getInstance();
    /** Constant for Settings Main. */
    public static final int UI_MY_SETTINGS = 0;
    /** Constant for General settings. */
    public static final int UI_GENERAL_SETTINGS = 1;
    /** Constant for Parental controls. */
    public static final int UI_PARENTAL_CONTROLS = 2;
    /** Constant for Time based restrictions. */
    public static final int UI_TIME_BASED_RESTRICTIONS = 3;
    /** Constant for Shortcut menu. */
    public static final int UI_SHORTCUT_MENU = 4;
    /** Constant for Shortcut. */
//    public static final int UI_SHORTCUT = 5;
    /** Constant for Channel Restriction. */
    public static final int UI_CHANNEL_RESTRICTION = 6;
    /** Constant for Program guide. */
    public static final int UI_PROGRAM_GUIDE = 7;
    /** Constant for Favourite channel. */
    public static final int UI_FAVOURITE_CHANNEL = 8;
    /** Constant for PVR. */
    public static final int UI_PVR = 9;
    /** Constant for VOD. */
    public static final int UI_VOD = 10;
    /** Constant for Equipment. */
    public static final int UI_EQUIPMENT = 11;
    /** Constant for Sleep time. */
    public static final int UI_SLEEP_TIME = 12;
    /** Constant for Wake up time. */
    public static final int UI_WAKE_UP_TIME = 13;
    /** Constant for Closed captionning. */
    public static final int UI_CC = 14;
    /** Constant for Caller ID. */
    public static final int UI_CALLER_ID = 15;
    /** Constant for TV picture format. */
    public static final int UI_TV_PICTURE_FORMAT = 16;
    /** Constant for Audio accessibility. */
    public static final int UI_AUDIO_ACCESSIBILITY = 17;
    /** Constant for PinEnabler. */
    public static final int UI_PINENABLER = 0xF0;
    /** Constant for Create Admin. */
    public static final int UI_CREATE_ADMIN = 0xF1;
    /** Constant for Parent Information. */
    public static final int UI_PARENTAL_INFO = 0xF2;

    /** Constant means Action is OK. */
    public static final int ACTION_OK = 0x01;
    /** Constant means Action is canceled. */
    public static final int ACTION_CANCEL = 0x02;

    /** A key name of back button. */
    public static final String BTN_BACK = KeyNames.BTN_BACK;
    /** A key name of Add period button. */
    public static final String BTN_ADD_PERIOD = KeyNames.BTN_ADD_PERIOD;
    /** A key name of Page button. */
    public static final String BTN_PAGE = KeyNames.BTN_PAGE;
    /** A key name of Edit order button. */
    public static final String BTN_EDIT_ORDER = KeyNames.BTN_EDIT_ORDER;
    /** A key name of Clear Shortcuts button. */
    public static final String BTN_CLEAR_SHORTCUTS = KeyNames.BTN_CLEAR_SHORTCUTS;
    /** A key name of Clear Channel Restrictions button. */
    public static final String BTN_CLEAR_CHANNEL_RESTRICTIONS = KeyNames.BTN_CLEAR_CHANNEL_RESTRICTIONS;
    /** A key name of Clear Channel Fliters button. */
    public static final String BTN_CLEAR_CHANNEL_FILTERS = KeyNames.BTN_CLEAR_CHANNEL_FILTERS;
    /** A key name of Clear Favourite Channels button. */
    public static final String BTN_CLEAR_FAVOURITE_CHANNELS = KeyNames.BTN_CLEAR_FAVOURITE_CHANNELS;
    /** A key name of Channel data from EPG via SharedMemory. */
    public static final String EPG_DATA_KEY = "Epg.Channels";
    /** A key name of default channel logo. */
    public static final String DEFAULT_CHANNLE_LOGO = "logo.default";

    /** A index of channel number in channel data. */
    public static final int INDEX_CHANNEL_NUMBER = 0;
    /** A index of channel name in channel data. */
    public static final int INDEX_CHANNEL_NAME = 1;
    /** A index of channel type in channel data. */
    public static final int INDEX_CHANNEL_TYPE = 4;
    /** A index of channel source id in channel data. */
    public static final int INDEX_CHANNEL_SOURCE_ID = 5;
    /** A index of channel full name in channel data. */
    public static final int INDEX_CHANNEL_FULL_NAME = 6;
    
    public static final int INDEX_CHANNEL_SISTER_NAME = 7;
    public static final int INDEX_CHANNEL_DEFINITION = 8;
    
    
    /** A Vector to have a name of images temporary in Settings session. */
    private static Vector imageKeys = new Vector();
    /** A stack for breadcrumbs. */
    private static Stack history = new Stack();
    /** A instance for Notificastion popup. */
    protected static PopNotificationUI notiUI = new PopNotificationUI();
    /** A footer. */
    protected static Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
    /** A current popup. */
    protected BaseUI popup;
    /** A clock string. */
    private static String clockString;
    /** Indicate to add or not ClockListener. */
    private static boolean isAddedClockListener = false;
    /** A fade-in Effect. */
    protected AlphaEffect fadeInEffect;
    /** A fade-out Effect. */
    protected AlphaEffect fadeOutEffect;
    /** Indicate to run or not a animation. */
    protected static boolean isAnimating;
    
    protected Effect showingEffect;

    /**
     * Start a UI.
     * @param back true if back from lower UI, false otherwise.
     */
    public void start(boolean back) {
    }
    
    public void init() {}

    /**
     * Start a UI. Add a ClockListener and Footer. Visible makes true.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: start().");
        }

        addClockListener();
        add(footer);
        setVisible(true);
    }

    /**
     * Stop a UI.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: stop().");
        }

        if (isAddedClockListener) {
            Clock.getInstance().removeClockListener(this);
            isAddedClockListener = false;
        }
        remove(footer);
        setVisible(false);
    }

    /**
     * Sets the renderer of UI.
     */
    public void setRenderer(BaseRenderer r) {
        this.renderer = r;
        super.setRenderer(r);
    }

    /**
     * Add a ClockListener.
     */
    public void addClockListener() {
        Formatter formatter = Formatter.getCurrent();
        clockString = formatter.getLongDate();
        Log.printDebug("BaseUI: clockString = " + clockString);

        if (!isAddedClockListener) {
            isAddedClockListener = true;
            Clock.getInstance().addClockListener(this);
        }
    }

    /**
     * Receive a event of Clock.
     */
    public void clockUpdated(Date date) {
        Formatter formatter = Formatter.getCurrent();
        clockString = formatter.getLongDate();
        repaint();
    }

    /**
     * Get a clock string.
     * @return current clock string.
     */
    public String getClockString() {
        return clockString;
    }

    /**
     * Start a fade-in effect.
     * @param ui target ui.
     */
    public void startFadeInEffect(BaseUI ui) {
        if (Log.INFO_ON) {
            Log.printInfo("BaseUI: startFadeInEffect");
        }
        if (fadeInEffect == null) {
            fadeInEffect = new AlphaEffect(ui, 12, MovingEffect.FADE_IN);
        }
        setVisible(false);
        fadeInEffect.start();
    }

    /**
     * Start a fade-out effect.
     * @param ui target ui.
     */
    public void startFadeOutEffect(BaseUI ui) {
        if (Log.INFO_ON) {
            Log.printInfo("BaseUI: startFadeOutEffect");
        }
        if (fadeOutEffect == null) {
            fadeOutEffect = new AlphaEffect(ui, 12, MovingEffect.FADE_OUT);
        }
        setVisible(false);
        fadeOutEffect.start();
    }
    
    public void startEffect() {
        if (showingEffect == null) {
            showingEffect = new MovingEffect(this, 8,
                            new Point(0, 40), new Point(0, 0),
                            MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        }
        showingEffect.start();
    }

    /**
     * Receive a notification that animation is started.
     * @param effect current effect.
     */
    public void animationStarted(Effect effect) {
        if (Log.INFO_ON) {
            Log.printInfo("BaseUI: animationStarted: " + effect.toString());
        }
        if (!(effect instanceof ClickingEffect)) {
            isAnimating = true;
        }
    }

    /**
     * Receive a notification that animation is ended.
     * @param effect current effect.
     */
    public void animationEnded(Effect effect) {
        if (Log.INFO_ON) {
            Log.printInfo("BaseUI: animationEntded: " + effect.toString());
        }
        if (!(effect instanceof ClickingEffect)) {
            isAnimating = false;
        }

        if (effect.equals(fadeInEffect)) {
            setVisible(true);
            requestFocus();
        }
    }

    /**
     * Get a isAnimationg.
     * @return true if UI is animating, false otherwise.
     */
    public boolean isAnimating() {
        return isAnimating;
    }

    /**
     * Adds a button.
     * @param btn a key name of button.
     */
    public void addButton(String btn) {
        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        Image btnImg = null;

        String btnName = BaseUI.getMenuText(btn);

        if (btn == BaseUI.BTN_BACK) {
            btnImg = (Image) imgTable.get(PreferenceService.BTN_BACK);
        } else if (btn == BaseUI.BTN_PAGE) {
            btnImg = (Image) imgTable.get(PreferenceService.BTN_PAGE);
        } else if (btn == BaseUI.BTN_EDIT_ORDER) {
            btnImg = (Image) imgTable.get(PreferenceService.BTN_A);
        } else if (btn == BaseUI.BTN_CLEAR_FAVOURITE_CHANNELS || btn == BaseUI.BTN_CLEAR_CHANNEL_RESTRICTIONS
                || btn == BaseUI.BTN_CLEAR_CHANNEL_FILTERS || btn == BaseUI.BTN_CLEAR_SHORTCUTS
                || btn == BaseUI.BTN_ADD_PERIOD) {
            btnImg = (Image) imgTable.get(PreferenceService.BTN_B);
        }

        footer.addButtonWithLabel(btnImg, btnName);
    }

    /**
     * Remove a button.
     * @param btn a key name of button.
     */
    public void removeButton(String btn) {
        footer.removeButton(btn);
    }

    /**
     * Remove a all buttons.
     */
    public void clearButtons() {
        footer.reset();
    }

    /**
     * Start a clicking animation.
     * @param btn a key name of button.
     */
    public void clickButtons(String btn) {
        String btnName = BaseUI.getMenuText(btn);
        footer.clickAnimation(btnName);
    }

    /**
     * Get a current popup.
     * @return the popup
     */
    public BaseUI getPopup() {
        return popup;
    }

    /**
     * Set a new popup.
     * @param p the popup to set
     */
    public void setPopup(BaseUI p) {
        popup = p;
        if (popup != null) {
            add(popup, 0);
        }
    }

    /**
     * Remove a popup.
     * @param p popup.
     */
    public void removePopup(BaseUI p) {
        remove(p);
        popup = null;
    }

    /**
     * Clear a all popup.
     */
    public void clearPopup() {
        if (popup != null) {
            remove(popup);
            popup = null;
        }
    }

    /**
     * Handle after closed popup.
     * @param pop
     * @param msg
     */
    public void popupClosed(BaseUI pop, Object msg) {

    }

    /**
     * Paint a selection content. call when display a popup to select a values.
     * @param g Graphics to paint.
     * @param ui
     */
    public void paintSelectedContent(Graphics g, BaseUI ui) {
        if (renderer != null) {
            renderer.paintSelectedContent(g, ui);
        }
    }

    /**
     * Paint a decoration items (arrows, shadows etc) when is animating.
     * @param g Graphics to paint.
     * @param ui parent ui.
     */
    public void paintDecoOfList(Graphics g, BaseUI ui) {
        if (renderer != null) {
            renderer.paintDecoOfList(g, ui);
        }
    }

    /**
     * Get a image by name.
     * @param name image name.
     * @return image.
     */
    public static Image getImage(String name) {
        Image img = dataCenter.getImage(name);

        if (img != null && !imageKeys.contains(name)) {
            imageKeys.add(name);
        }

        return img;
    }

    /**
     * Clear a temporary all images.
     */
    public static void clearImages() {
        if (imageKeys.size() > 0) {
            Enumeration enums = imageKeys.elements();

            while (enums.hasMoreElements()) {
                String key = (String) enums.nextElement();
                if (Log.DEBUG_ON) {
                    Log.printDebug("BaseUI: clearImages: name = " + key);
                }
                dataCenter.removeImage(key);
            }

            imageKeys.clear();
        }
    }

    /**
     * Add a history.
     * @param o history information.
     */
    public static void addHistory(Object o) {
        history.push(o);
    }

    /**
     * Pop a last history.
     */
    public static void popHistory() {
        if (history.size() > 0) {
            history.pop();
        }
    }

    /**
     * Peek a last history.
     * @return last history.
     */
    public static Object peekHistory() {
        return history.peek();
    }

    /**
     * Clear a all histories.
     */
    public static void clearHistory() {
        history.clear();
    }

    /**
     * Get a string array for history.
     * @return string array to have a names for history.
     */
    public static String[] getHistory() {
        String settingName = getMenuText(KeyNames.MY_SETTINGS);
        if (history.size() == 0) {
            return new String[] {settingName};
        } else {
            
            Object[] objs = new Object[history.size()];
            history.copyInto(objs);

            if (objs[history.size() -1] instanceof SubCategory) {
                String id = ((Category)objs[history.size() -1]).getCategoryId();
                if (id.equals("preference")) {
                    return getHistoryWithoutSettings();
                }
            }
            
            String[] retVal = new String[history.size() + 1];
            retVal[0] = settingName;
            for (int i = 0; i < objs.length; i++) {
                if (objs[i] instanceof MainCategory || objs[i] instanceof SubCategory) {
                    String title = getMenuText(((Category) objs[i]).getCategoryId());
                    if (title != null && !title.equals("")) {
                        retVal[i + 1] = title;
                    } else {
                        retVal[i + 1] = ((Category) objs[i]).getName();
                    }
                } else if (objs[i] instanceof Preference) {
                    retVal[i + 1] = ((Preference) objs[i]).getName();
                } else if (objs[i] instanceof User) {
                    retVal[i + 1] = ((User) objs[i]).getUserName();
                } else if (objs[i] instanceof String) {
                    retVal[i + 1] = (String) objs[i];
                }
            }
            return retVal;
        }
    }
    
    public static String[] getHistoryWithoutSettings() {
        String[] retVal = new String[history.size()];
        Object[] objs = new Object[history.size()];
        history.copyInto(objs);

        for (int i = 0; i < objs.length; i++) {
            if (objs[i] instanceof MainCategory || objs[i] instanceof SubCategory) {
                String title = getMenuText(((Category) objs[i]).getCategoryId());
                if (title != null && !title.equals("")) {
                    retVal[i] = title;
                } else {
                    retVal[i] = ((Category) objs[i]).getName();
                }
            } else if (objs[i] instanceof Preference) {
                retVal[i] = ((Preference) objs[i]).getName();
            } else if (objs[i] instanceof User) {
                retVal[i] = ((User) objs[i]).getUserName();
            } else if (objs[i] instanceof String) {
                retVal[i] = (String) objs[i];
            }
        }
        return retVal;
    }

    /**
     * Get a explain text of name(or id) in explain.txt.
     * @param name key name in explain.txt.
     * @return explain
     */
    public static String getExplainText(String name) {
        String text = dataCenter.getString("explain." + name);
        if (Log.DEBUG_ON) {
            Log.printDebug("BaseUI: getExplainText: text : " + text);
        }
        if (text == null) {
            text = TextUtil.EMPTY_STRING;
        }

        return text;
    }

    /**
     * Get a name of menu according to name(or id) in menu.txt.
     * @param name key name
     * @return menu text
     */
    public static String getMenuText(String name) {
        return dataCenter.getString("menu." + name);
    }

    /**
     * Get a short name of menu according to name(or id) in short.txt.
     * @param name key name
     * @return short text
     */
    public static String getMenuShortText(String name) {
        return dataCenter.getString("short." + name);
    }

    /**
     * Get a text of Popup according to name(or id) in popup.txt.
     * @param name key name
     * @return string
     */
    public static String getPopupText(String name) {
        return dataCenter.getString("popup." + name);
    }

    /**
     * Get a text of Popup according to name(or id) in option.txt.
     * @param name key name
     * @return string
     */
    public static String getOptionText(String name) {
        String value = dataCenter.getString("option." + name);
        if (value != null && !value.equals("")) {
            return value;
        } else {
            return name;
        }
    }
}

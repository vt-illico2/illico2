/**
 * @(#)DataManager.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import java.io.File;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.communication.CommunicationManager;
import com.videotron.tvi.illico.upp.controller.ParentalController;
import com.videotron.tvi.illico.upp.controller.PreferenceController;
import com.videotron.tvi.illico.upp.controller.ProxyController;
import com.videotron.tvi.illico.upp.util.PropertiesUtil;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * this class implements function of data management.
 * @author Woojung Kim
 * @version 1.1
 */
public final class DataManager {

    /** The instance. */
    private static DataManager instance = new DataManager();

    /** The main category list. */
    private MainCategory[] mainCategoryList;

    /** The Hashtable for Settings's preference. */
    private Hashtable settingsPreTable;
    /** The Hashtable for Settings's default preference R7.3 Tech Channel*/
    private Hashtable settingsDefaultPreTable;

    /** The root path of DVB persistent. */
    private static final String DVB_PERSISTENT_ROOT = "dvb.persistent.root";

    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty(DVB_PERSISTENT_ROOT);

    /** the path for default preference. */
    private static final String DEFAULT_PREFERENCE = "resource/properties/defaultPreference.prop";

    /** The path for common preference file that was saved in fresh memory. */
    private static final String PREFERENCE_PREFIX = "/upp/";
    /** The suffix of file name to save a preferences. */
    private static final String PREFERENCE_SUFFIX = "Preferences.prop";

    /** This field has a current postal code. */
    private String postalCode;

    /** The name of postal code data in DataCenter. */
    public static final String POSTAL_CODE = "postal.code";

    /** The name of default Postal code data in application properties. */
    public static final String DEFAULT_POSTAL_CODE = "default.postal.code";

    public String lastPreferenceName = TextUtil.EMPTY_STRING;

    /**
     * private Constructor for Singleton.
     */
    private DataManager() {
        settingsPreTable = new Hashtable();
        settingsDefaultPreTable = new Hashtable();
    }

    /**
     * Gets the single instance of DataManager.
     * @return single instance of DataManager
     */
    public static DataManager getInstance() {
        return instance;
    }

    /**
     * Initialize a manager.
     */
    public void init() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: called init()");
        }

        initRepository();
        initPreferenceData();
    }

    /**
     * Start DataManager.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: called start()");
        }

        String defaultPostalCode = DataCenter.getInstance().getString(DataManager.DEFAULT_POSTAL_CODE);

        if (Log.DEBUG_ON) {
            Log.printDebug("DataManager: loaded default postal code : " + defaultPostalCode);
        }

        if (defaultPostalCode != null && !defaultPostalCode.equals("")) {
            postalCode = defaultPostalCode;
        }
    }

    /**
     * Stop DataManager.
     */
    public void stop() {

    }

    /**
     * Dispose DataManager. Clean up a resources.
     */
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: called dispose()");
        }
        if (mainCategoryList != null) {
            for (int i = 0; i < mainCategoryList.length; i++) {
                mainCategoryList[i] = null;
            }
            mainCategoryList = null;
        }
        settingsPreTable.clear();
        settingsDefaultPreTable.clear();
        settingsPreTable = null;
        settingsDefaultPreTable = null;
    }

    /**
     * Gets the main category list.
     * @return the main category list
     */
    public MainCategory[] getMainCategoryList() {
    	
    	if (settingsPreTable.size() == 0) {
	    	Preference pre = getPreference(MonitorService.SUPPORT_UHD);
	        
	        DataCenter.getInstance().put(pre.getID(), pre.getCurrentValue());
	
	        loadSettingsData();
	        loadDefaultSettingsData();
    	}
    	
        makeMenuData();
        return mainCategoryList;
    }

    /**
     * Inits the preference data.
     */
    public void initPreferenceData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: called initPreferenceData()");
        }

        Hashtable propertiesTable = new Hashtable();
        Hashtable preferenceTable = null;
        // add a default properties into preferenceTable
        PropertiesUtil.loadAllProperties(DEFAULT_PREFERENCE, propertiesTable);
        preferenceTable = makePreferences(propertiesTable);
        settingsDefaultPreTable = (Hashtable)preferenceTable.clone();
        
        propertiesTable.clear();
        // update properties from stored user's preferences in preferenceTable .
        PropertiesUtil.loadAllProperties(REPOSITORY_ROOT + PREFERENCE_PREFIX + User.ID_ANONYMOUS + PREFERENCE_SUFFIX,
                propertiesTable);

        Hashtable storedTable = makePreferences(propertiesTable);
        updateTable(storedTable, preferenceTable);
        propertiesTable.clear();

        User user = UserManager.getInstance().loadUser(User.ID_ANONYMOUS);
        user.setPreferences(preferenceTable);

        if (Log.DEBUG_ON) {
            Log.printDebug("DataManager: initPreferenceData: preferenceTable size = "
                    + preferenceTable.size());
        }

        // different between GeneralPrefence and UPP preference.
        syncUserLanguage();
    }

    /**
     * Make a Preference Object from table include a key and String value for preference, Return a new Hashtable
     * includes a Preference objects.
     * @param table The Hashtable includes a key and String value for preference
     * @return The Hashtable includes a Preference objects.
     */
    public Hashtable makePreferences(Hashtable table) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: makePreferences()");
        }
        Hashtable retTable = new Hashtable();

        Enumeration keys = table.keys();

        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String value = (String) table.get(key);

            Preference pre = new Preference();
            pre.setID(key);
            if (value != null) {
                pre.setCurrentValue(value);
            }

            retTable.put(key, pre);
        }

        return retTable;
    }

    /**
     * Update a target Hashtable by using src Hashtable.
     * @param src The Hashtable incldues a new data.
     * @param target The Hashtable to update a new data.
     */
    public void updateTable(Hashtable src, Hashtable target) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: updateTable()");
        }
        Enumeration keys = src.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            Object obj = src.get(key);

            target.put(key, obj);
        }
    }

    /**
     * Sync a language between UPP Preferences and DataCenter in Framework.
     */
    private void syncUserLanguage() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: syncUserLanguage()");
        }
        String langValue = getPreference(PreferenceNames.LANGUAGE).getCurrentValue();
        DataCenter.getInstance().put(PreferenceNames.LANGUAGE, langValue);
    }

    /**
     * Gets the preference.
     * @param preferenceName the preference name
     * @return the preference
     */
    public Preference getPreference(String preferenceName) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getPreference: prefrenceName = " + preferenceName);
        }

        Preference retVal = null;
        Hashtable preTable = UserManager.getInstance().getCurrentUser().getPreferences();
        retVal = (Preference) preTable.get(preferenceName);

        if (retVal != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: getPreference: preference value = " + retVal.getCurrentValue());
            }
        } else {
            retVal = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters().get(preferenceName);

            if (retVal != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: getPreference: Filter value = " + retVal.getCurrentValue());
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: getPreference: can't find a Preference");
                }
            }
        }

        return retVal;
    }

    /**
     * Sets the preference.
     * @param preferenceName the preference name
     * @param value the value
     * @return true, if successful
     */
    public boolean setPreference(String preferenceName, String value) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: setPreference: preferenceName = " + preferenceName + "\t value = "
                    + value);
        }
        
        if (value == null) {
        	value = TextUtil.EMPTY_STRING;
        }
        
        boolean isFilter = false;
        Hashtable preTable = UserManager.getInstance().getCurrentUser().getPreferences();
        Preference pre = (Preference) preTable.get(preferenceName);
        if (pre != null) {
            pre.setCurrentValue(value);
            preTable.put(preferenceName, pre);
        } else {
            // case of Filter
            pre = (Preference) UserManager.getInstance().getCurrentUser().getAccessFilters().get(preferenceName);

            if (pre != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: setPreference: " + preferenceName + " is Filter.");
                }
                isFilter = true;
                pre.setCurrentValue(value);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: setPreference: preference is not existed. so make and add a"
                            + " Preference");
                }
                Preference newPre = new Preference();
                newPre.setID(preferenceName);
                newPre.setCurrentValue(value);
                UserManager.getInstance().getCurrentUser().getPreferences().put(preferenceName, newPre);
            }
        }

        if (!isFilter) {
            lastPreferenceName = preferenceName;
            String id = UserManager.getInstance().getCurrentUser().getUserId();
            if (PropertiesUtil.setProperty(REPOSITORY_ROOT + PREFERENCE_PREFIX + id + PREFERENCE_SUFFIX,
                    preferenceName, value)) {

                // notify a Preference to associated application.
                ProxyController.getInstance().notifyUpdatedPreference(preferenceName, value);

                // update language in DataCenter
                if (preferenceName.equals(PreferenceNames.LANGUAGE)) {
                    Preference sapPre = (Preference) UserManager.getInstance().getCurrentUser().getPreferences()
                            .get(PreferenceNames.SAP_LANGUAGE_AUDIO);
                    sapPre.setCurrentValue(value);
                    syncUserLanguage();
                } else if (preferenceName.equals(MonitorService.SUPPORT_UHD)) { // VTPERISCOPE-152, VDTRMASTER-5559
                	DataCenter.getInstance().put(preferenceName, value);
                }
                return true;
            }
        } else {
            // in case of Filter
            String id = UserManager.getInstance().getCurrentUser().getUserId();
            UserManager.getInstance().storeUserProperty(id, preferenceName, value);

            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: setPreference: check and saved Parental control time");
            }
            if (preferenceName.equals(RightFilter.PARENTAL_CONTROL)) {
                setPreference(RightFilter.PARENTAL_CONTROL_TIME, String.valueOf(System.currentTimeMillis()));

                MonitorService mService = CommunicationManager.getInstance().getMonitorService();
                String appName = null;
                if (mService != null) {
                    try {
                        appName = mService.getCurrentActivatedApplicationName();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }

                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: setPreference: appName : " + appName);
                    Log.printDebug("DataManager: setPreference: lastPreferenceName : " + lastPreferenceName);
                }

                boolean isStarted = PreferenceController.getInstance().isStarted();
                
                if (isStarted) {
                    // show a popup of Parental control state.
                    // don't display in case of block or unblock a channel.
                    if (!lastPreferenceName.equals(RightFilter.CHANNEL_RESTRICTION)) {
                        if (!FrameworkMain.getInstance().getHScene().isShowing()
                                && (appName == null || !appName.equals("Wizard"))) {
                            ParentalController.getInstance().showParentalInfo(value);
                        }
                    }
                }
            }
            if (!preferenceName.equals(RightFilter.PARENTAL_CONTROL_TIME)) {
                lastPreferenceName = preferenceName;
            }
            // notify language to associated application.
            // In case of Block Periods and Limits, only notify when Settings will hide.
            // refer to PreferenceController#hideUIComponent().
            if (!preferenceName.equals(RightFilter.TIME_BASED_RESTRICTIONS)) {
                ProxyController.getInstance().notifyUpdatedPreference(preferenceName, value);
            }
            return true;
        }
        return false;
    }

    /**
     * Make menu data.
     */
    public void makeMenuData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: makeMenuData()");
        }
        Vector mainVector = new Vector();
        Vector allSubVector = new Vector();
        File menuFile = (File) DataCenter.getInstance().get("MENU_DATA");

        String[] menu = TextReader.read(menuFile);
        for (int i = 0; i < menu.length; i++) {
            if (Log.DEBUG_ON) {
                Log.printDebug("menu : " + menu[i]);
            }

            String[] menuInfo = TextUtil.tokenize(menu[i], "|");

            // if don't support dvr, PVR menu don't add.
            if (!Environment.SUPPORT_DVR && menuInfo[0].equals("pvr")) {
                continue;
            }

            if (menuInfo[2].equals("main")) { // main
                MainCategory mCategory = new MainCategory();

                mCategory.setCategoryId(menuInfo[0]);
                mCategory.setName(menuInfo[1]);
                mCategory.setParentCategoryId(null);
                mainVector.add(mCategory);
            } else {
                SubCategory sCategory = new SubCategory();
                sCategory.setCategoryId(menuInfo[0]);
                sCategory.setName(menuInfo[1]);
                sCategory.setParentCategoryId(menuInfo[2]);
                allSubVector.add(sCategory);
            }
        }

        int countMainMenu = mainVector.size();

        mainCategoryList = new MainCategory[countMainMenu];
        mainCategoryList = (MainCategory[]) mainVector.toArray(mainCategoryList);

        Vector subVector = new Vector();
        for (int i = 0; i < mainCategoryList.length; i++) {
            Log.printDebug("MainCategory : " + mainCategoryList[i].getCategoryId());

            int size = allSubVector.size();

            for (int j = 0; j < size; j++) {
                SubCategory sub = (SubCategory) allSubVector.get(j);

                if (mainCategoryList[i].getCategoryId().equals(sub.getParentCategoryId())) {
                    Log.printDebug("SubCategory : " + sub.getName());
                    subVector.add(sub);
                }
            }

            SubCategory[] subs = new SubCategory[subVector.size()];
            subs = (SubCategory[]) subVector.toArray(subs);
            mainCategoryList[i].setSubCategories(subs);
            subVector.clear();
        }

        mainVector.clear();
        allSubVector.clear();
    }

    /**
     * Gets the access right.
     * @param userId the user id who have a rights.
     * @return the Hashtable includes a access rights
     */
    public Hashtable getAccessRight(String userId) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getAccessRight()");
        }
        Hashtable rights = null;

        rights = UserManager.getInstance().loadUser(userId).getAccessRights();

        return rights;
    }

    /**
     * Gets the filter list.
     * @param userId the user id who have a filters.
     * @return the filter list
     */
    public Hashtable getFilterList(String userId) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getFilterList()");
        }
        Hashtable filters = null;

        filters = UserManager.getInstance().loadUser(userId).getAccessFilters();

        return filters;
    }

    /**
     * Gets the test mode config data.
     * @return the test mode config data
     */
    public String getTestModeConfigData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getTestModeConfigData()");
        }
        return null;
    }

    /**
     * Loads config data.
     */
    public void loadConfigData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: loadConfigData()");
        }
    }

    /**
     * Inits the repository.
     */
    public void initRepository() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: initRepository()");
        }
        File file = new File(REPOSITORY_ROOT + "/upp");

        if (!file.exists()) {
            if (Log.DEBUG_ON) {
                Log.printDebug(REPOSITORY_ROOT + "/upp" + "is not exist, so mkdirs");
            }
            file.mkdirs();
        }
    }

    /**
     * Loads settings data.
     */
    public void loadSettingsData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: loadSettingsData()");
        }
        File file = (File) DataCenter.getInstance().get("PREFERENCE_INFO");

        String[] preInfos = TextReader.read(file);
        String[] parsedStr = null;
        for (int i = 0; i < preInfos.length; i++) {
            parsedStr = TextUtil.tokenize(preInfos[i], "|");

            Preference pre = null;
            if (parsedStr[0].equals(PreferenceNames.VIDEO_TV_PICTURE_FORMAT + ".4k") && Environment.SUPPORT_UHD) {
            	pre = (Preference) settingsPreTable.get(PreferenceNames.VIDEO_TV_PICTURE_FORMAT);
            } else {
            	pre = new Preference();
            	pre.setID(parsedStr[0]);
            }
            
            pre.setName(TextUtil.EMPTY_STRING);
            pre.setCategoryId(parsedStr[1]);
            if (!parsedStr[2].equals("NULL") && !parsedStr[2].equals("")) {
                pre.setDefaultValue(parsedStr[2]);
            }
            if (!parsedStr[3].equals("NULL") && !parsedStr[3].equals("")) {
                pre.setSelectableValues(TextUtil.tokenize(parsedStr[3], "^"));
            }

            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: loadSettingsData: id = " + pre.getID());
                Log.printDebug("DataManager: loadSettingsData: name = " + pre.getName());
                Log.printDebug("DataManager: loadSettingsData: default = " + pre.getDefaultValue());
                if (pre.getSelectableValues() != null) {
                    Log.printDebug("DataManager: loadSettingsData: selectableValues size = "
                            + pre.getSelectableValues().length);
                    Log.printDebug("DataManager: loadSettingsData: selectableValue[0] = "
                            + pre.getSelectableValues()[0]);
                } else {
                    Log.printDebug("DataManager: loadSettingsData: defaultValue and selectableValue is"
                            + " null");
                }
            }

            settingsPreTable.put(pre.getID(), pre);
        }
    }

    /**
     * Loads a default settings data.
     */
    public void loadDefaultSettingsData() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: loadDefaultSettingsData()");
        }
        Hashtable propertiesTable = new Hashtable();
        // add a default properties into preferenceTable
        PropertiesUtil.loadAllProperties(DEFAULT_PREFERENCE, propertiesTable);

        updateSettingsData(propertiesTable);
    }
    
    /**
     * Update a settings data by properties. This will be used to load a saved properties for each user.
     * @param propertiesTable includes a key and value for Preference.
     */
    public void updateSettingsData(Hashtable propertiesTable) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: updateSettingsData()");
        }
        Enumeration keys = settingsPreTable.keys();

        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            String value = (String) propertiesTable.get(key);

            if (value != null) {
                Preference pre = (Preference) settingsPreTable.get(key);
                pre.setCurrentValue(value);
                settingsPreTable.put(key, pre);
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: loadDefaultSettingsData: name = " + key + "\t value = "
                            + value);
                }
            }
        }
    }

    /**
     * Return a preferences of Id. The id is a Category id.
     * @param id the category id
     * @return The Hashtable to include a Preference for category id
     */
    public Hashtable getPreferences(String categoryId) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getPreferences(): categoryId = " + categoryId);
        }
        Hashtable table = new Hashtable();

        Hashtable preTable = UserManager.getInstance().getCurrentUser().getPreferences();
        
        if (settingsPreTable.size() == 0) {
	    	Preference pre = getPreference(MonitorService.SUPPORT_UHD);
	        
	        DataCenter.getInstance().put(pre.getID(), pre.getCurrentValue());
	
	        loadSettingsData();
	        loadDefaultSettingsData();
    	}
        
        Enumeration keys = settingsPreTable.keys();

        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();

            Preference pre = (Preference) settingsPreTable.get(key);
            Preference currentPre = (Preference) preTable.get(key);

            if (pre != null) {
                if (currentPre != null) {
                    pre.setCurrentValue(currentPre.getCurrentValue());
                }

                if (pre.getCategoryId().equals(categoryId)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("DataManager: getPreferences: get a preference id : " + pre.getID());
                        Log.printDebug("DataManager: getPreferences: get a preference value : "
                                + pre.getCurrentValue());
                    }
                    table.put(pre.getID(), pre);
                } else {
                    // if (Log.DEBUG_ON) {
                    // Log.printDebug("DataManager: getPreferences: preference id : " + pre.getID());
                    // Log.printDebug("DataManager: getPreferences: preference category : "
                    // + pre.getCategoryId());
                    // }
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DataManager: getPreferences: key's value not exist : " + key);
                }
            }
        }
        return table;
    }

    /**
     * Get a Preference for Settings.
     * @param pre wanted preference.
     * @return found preference.
     */
    public Preference getPeferenceForSettings(Preference pre) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getPeferenceForSettings(): pre = " + pre.getID());
        }
        Preference setPre = (Preference) settingsPreTable.get(pre.getID());

        if (setPre != null) {
            setPre.setCurrentValue(pre.getCurrentValue());
        }

        return setPre;
    }
    
    public Preference getDefaultPreferenceForSettings(Preference pre) {
    	if (Log.INFO_ON) {
            Log.printInfo("DataManager: getDefaultPreferenceForSettings(): pre = " + pre.getID());
        }
        Preference setPre = (Preference) settingsDefaultPreTable.get(pre.getID());
        return setPre;
    }

    /**
     * Get a selectable values for id.
     * @param id wanted id.
     * @return a value array.
     */
    public String[] getSelectableValues(String id) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getSelectableValues(): id = " + id);
        }
        Preference pre = (Preference) settingsPreTable.get(id);

        if (pre != null) {
            return pre.getSelectableValues();
        }
        return null;
    }

    /**
     * Save a postal code in Anonymous user.
     * @param pcode postal code.
     */
    public void setPostalCode(String pcode) {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: setPostalCode: postalCode = " + postalCode);
        }
        Hashtable preTable = UserManager.getInstance().loadUser(User.ID_ANONYMOUS).getPreferences();
        Preference pre = (Preference) preTable.get(DataManager.POSTAL_CODE);
        if (pre != null) {
            pre.setCurrentValue(pcode);
            preTable.put(DataManager.POSTAL_CODE, pre);
        } else {

            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: setPostalCode: preference is not existed. so make and add a"
                        + " Preference");
            }
            Preference newPre = new Preference();
            newPre.setID(DataManager.POSTAL_CODE);
            newPre.setCurrentValue(pcode);
            preTable.put(DataManager.POSTAL_CODE, newPre);
        }

        UserManager.getInstance().storeUserProperty(User.ID_ANONYMOUS, DataManager.POSTAL_CODE, postalCode);
    }

    /**
     * Return a postal code to be save in anonymous.
     * @return The postal code that length is 6.
     */
    public String getPostalCode() {
        if (Log.INFO_ON) {
            Log.printInfo("DataManager: getPostalCode()");
        }
        Hashtable preHash = UserManager.getInstance().loadUser(User.ID_ANONYMOUS).getPreferences();
        Preference pre = (Preference) preHash.get(DataManager.POSTAL_CODE);

        if (pre != null && pre.getCurrentValue() != null && !pre.getCurrentValue().equals(TextUtil.EMPTY_STRING)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: getPostalCode: id = " + pre.getID());
                Log.printDebug("DataManager: getPostalCode: value = " + pre.getCurrentValue());
            }
            return pre.getCurrentValue();
        } else {
            if (Log.DEBUG_ON) {
                Log.printDebug("DataManager: getPostalCode: Postal code preference is null, so return"
                        + " default = " + postalCode);
            }
            return postalCode;
        }
    }

    /**
     * Set a default postal code.
     * @param code The new postal code.
     */
    public void setDefaultPostalCode(String code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("DataManager: setDefaultPostalCode: new postal code = " + code);
        }
        postalCode = code;
    }
}

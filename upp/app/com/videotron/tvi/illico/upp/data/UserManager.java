/**
 * @(#)UserManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.util.PropertiesUtil;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * UserManager class implements User Management function (create, modify, delete User).
 * @author Woojung Kim
 * @version 1.1
 */
public final class UserManager {

    /** The instance of User Manager for Singleton. */
    private static UserManager instance = new UserManager();

    /** The root path of DVB Persistent. */
    private static final String DVB_PERSISTENT_ROOT = "dvb.persistent.root";

    /** The root path of repository. */
    private static final String REPOSITORY_ROOT = System.getProperty(DVB_PERSISTENT_ROOT);

    /** The path of User Profile. */
    private static final String PROFILE_PATH = REPOSITORY_ROOT + "/upp/";

    /** The extension of User Profile. */
    private static final String PROFILE_EXTENTION = ".prop";

    /** The file path of default Administrator. */
    private static final String ADMIN_DEFAULT_FILE = "resource/properties/defaultAdmin.prop";

    /** The file path fo default Privileged. */
    private static final String PRIVILEGED_DEFAULT_FILE = "resource/properties/defaultPrivileged.prop";

    /** The file path of Family PIN. */
    private static final String FAMILY_DEFAULT_FILE = "resource/properties/defaultFamily.prop";

    /** The file path fo default Anonoymous. */
    private static final String ANONYMOUS_DEFAULT_FILE = "resource/properties/defaultAnonymous.prop";

    /** The array includes a name of access filters. */
    public static final String[] ACCESS_FILTER_LIST = {RightFilter.PARENTAL_CONTROL, RightFilter.PARENTAL_CONTROL_TIME,
            RightFilter.SUSPEND_DEFAULT_TIME, RightFilter.CHANNEL_RESTRICTION, RightFilter.BLOCK_BY_RATINGS,
            RightFilter.BLOCK_BY_RATINGS_DISPLAY, RightFilter.HIDE_ADULT_CONTENT, RightFilter.TIME_BASED_RESTRICTIONS,
            RightFilter.PIN_CODE_UNBLOCKS_CHANNEL, RightFilter.ALLOW_PURCHASE, RightFilter.PROTECT_PVR_RECORDINGS,
            RightFilter.PIN_CODE_UNBLOCKS_CHANNEL_TIME, RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
            RightFilter.ALLOW_ADULT_CATEGORY, RightFilter.ALLOW_BYPASS_BLOCKED_TIME_PERIOD,
            RightFilter.ALLOW_BYPASS_BLOCKED_CHANNEL};

    /** The array includes a name of access rights. */
    private static final String[] ACCESS_RIGHT_LIST = {RightFilter.GAMES, RightFilter.PC_TO_TV,
            RightFilter.STB_SETTINGS};

    /** The Hashtable to keep a loaded user. */
    private Hashtable userTable;

    /** The current login user. */
    private User loginUser;

    /**
     * Instantiates a new user manager.
     */
    private UserManager() {
        userTable = new Hashtable();
    }

    /**
     * Gets the single instance of UserManager.
     * @return single instance of UserManager
     */
    public static UserManager getInstance() {
        return instance;
    }

    /**
     * Dispose a resources to use.
     */
    public void dispose() {

        Enumeration keys = userTable.keys();

        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            User user = (User) userTable.get(key);
            user.setPin(null);
            user = null;
        }
        userTable.clear();
    }

    /**
     * Adds the user.
     * @param user the user
     */
    public void addUser(User user) {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: addUser: user: " + user.getUserId());
        }
    }

    /**
     * Removes the user.
     * @param user the user
     */
    public void removeUser(User user) {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: removeUser: user: " + user.getUserId());
        }
        userTable.remove(user.getUserId());
    }

    /**
     * Modify user.
     * @param user the user
     */
    public void modifyUser(User user) {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: modifyUser: user: " + user.getUserId());
        }
        userTable.put(user.getUserId(), user);
    }

    /**
     * Load user.
     * @param userId the user id
     * @return the user
     */
    public User loadUser(String userId) {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: loadUser: user id : " + userId);
        }

        User user = (User) userTable.get(userId);

        if (user != null) {
            return user;
        }

        StringBuffer sf = new StringBuffer();

        sf.append(PROFILE_PATH);
        sf.append(userId);
        sf.append(PROFILE_EXTENTION);

        Properties defaultProps = null;

        if (userId.equals(User.ID_ADMIN)) {
            defaultProps = PropertiesUtil.read(ADMIN_DEFAULT_FILE);
        } else if (userId.equals(User.ID_ANONYMOUS)) {
            defaultProps = PropertiesUtil.read(ANONYMOUS_DEFAULT_FILE);
        } else if (userId.equals(User.ID_FAMILY)) {
            defaultProps = PropertiesUtil.read(FAMILY_DEFAULT_FILE);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("UserManager: loadUser: default preferences size : " + defaultProps.size());
        }

        Properties props = PropertiesUtil.read(defaultProps, sf.toString());

        user = new User();

        user.setUserId(props.getProperty(User.USER_PROFILE_ID));
        user.setUserName(props.getProperty(User.USER_PROFILE_NAME));

        String pinString = props.getProperty(User.USER_PROFILE_PIN);
        if (pinString != null) {
            Pin pin = new Pin();
            pin.setEncryptedPinString(pinString);
            user.setPin(pin);
        }

        Hashtable rightTable = new Hashtable();

        for (int i = 0; i < ACCESS_RIGHT_LIST.length; i++) {
            AccessRight accessRight = new AccessRight();
            accessRight.setName(ACCESS_RIGHT_LIST[i]);
            accessRight.setAccessRight((Boolean.valueOf(props.getProperty(ACCESS_RIGHT_LIST[i]))).booleanValue());
            rightTable.put(ACCESS_RIGHT_LIST[i], accessRight);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("UserManager: loadUser: right size : " + rightTable.size());
        }
        user.setAccessRights(rightTable);

        Hashtable filterTable = new Hashtable();

        for (int i = 0; i < ACCESS_FILTER_LIST.length; i++) {
            Preference filter = new Preference();
            filter.setID(ACCESS_FILTER_LIST[i]);
            String value = props.getProperty(ACCESS_FILTER_LIST[i]);
            if (value != null) {
                filter.setCurrentValue(value);
            } else {
                filter.setCurrentValue(TextUtil.EMPTY_STRING);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("UserManager: loadUser: filterName: " + ACCESS_FILTER_LIST[i]);
                Log.printDebug("UserManager: loadUser: value: " + value);
            }

            filterTable.put(ACCESS_FILTER_LIST[i], filter);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("UserManager: loadUser: filter size : " + filterTable.size());
        }
        user.setAccessFilters(filterTable);

        userTable.put(user.getUserId(), user);

        return user;
    }

    /**
     * Return a User who is found with PIN.
     * @param pin The PIN that use to find a User.
     * @return The User who has a PIN.
     */
    public User validatePin(String pin) {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: validatePin: pin : " + pin);
        }
        // step 1: admin
        User admin = loadUser(User.ID_ADMIN);

        if (admin.isValidate(pin)) {
            return admin;
        }

        // step 2: family
        User family = loadUser(User.ID_FAMILY);

        if (family != null && family.isValidate(pin)) {
            return family;
        }
        return null;
    }

    /**
     * Get a current User.
     * @return current User.
     */
    public User getCurrentUser() {
        if (Log.INFO_ON) {
            Log.printInfo("UserManager: getCurrentUser()");
        }
        if (loginUser != null) {
            return loginUser;
        } else {
            return loadUser(User.ID_ANONYMOUS);
        }
    }

    /**
     * Store a property for User by using key and value.
     * @param userId The id of User.
     * @param key The key of Property.
     * @param value The value of Property.
     */
    public void storeUserProperty(String userId, String key, String value) {
        if (Log.INFO_ON) {
            Log.printDebug("UserManager: storeUserProperty: useId : " + userId + "\t key : " + key
                    + "\t value : " + value);
        }
        StringBuffer sb = new StringBuffer();

        sb.append(PROFILE_PATH);
        sb.append(userId);
        sb.append(PROFILE_EXTENTION);
        PropertiesUtil.setProperty(sb.toString(), key, value);
    }
}

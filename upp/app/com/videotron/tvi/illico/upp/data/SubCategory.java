/**
 * @(#)SubCategory.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

/**
 * The SubCategory class represent a sub category to display in N depth. not 1depth.
 * @author Woojung Kim
 * @version 1.1
 */
public class SubCategory extends Category {
    /**
     * Constructor. set a type for SubCategory.
     */
    public SubCategory() {
        setType(CategoryType.TYPE_SUB);
    }
}

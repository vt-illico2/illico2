/**
 * @(#)MainCategory.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

/**
 * The MainCategory class represent a category to display in 1 depth.<p>
 * if SubCategory[] is null, MainCategory has a Preferences.
 * 
 * @author Woojung Kim
 * @version 1.1
 */
public class MainCategory extends Category {

    /** The array of sub category. */
    private SubCategory[] subCategories;

    public MainCategory() {
        setType(CategoryType.TYPE_MAIN);
    }
    
    /**
     * Return a array of sub category.
     * 
     * @return The array of sub category.
     */
    public SubCategory[] getSubcategories() {
        return subCategories;
    }

    /**
     * Set a array of sub category in MainCategory.
     * 
     * @param subs
     *            The array of sub category.
     */
    public void setSubCategories(SubCategory[] subs) {
        this.subCategories = subs;
    }

}

/**
 * @(#)Time.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class has a information about time for Sleep time and Wake up time.
 * @author Woojung Kim
 * @version 1.1
 */
public class Time {

    /** indicate that time is enaber or not. */
    String active = "Enable";
    /** indicate a start hour. */
    String startHour = "10";
    /** indicate a start minute. */
    String startMin = "00";
    /** indicate a am_pm for start time. */
    String startAmPm = "am";
    /** indicate a end hour. */
    String endHour = "10";
    /** indicate a end minute. */
    String endMin = "00";
    /** indicate a am_pm for end time. */
    String endAmPm = "pm";
    /** indicate a enable from SUN to SAT. */
    String days = new String("No|No|No|No|No|No|No");
    /** indicate a repeat. */
    String repeat = "No";
    /** indicate a save time. */
    long saveTime = 0;

    /**
     * Get a Active.
     * @return Enable if time is active, Disable otherwise.
     */
    public String getActive() {
        return active;
    }

    /**
     * Set a Active. one of {@link Definitions#OPTION_VALUE_ENABLE} or {@link Definitions#OPTION_VALUE_DISABLE}.
     * @param active {@link Definitions#OPTION_VALUE_ENABLE} or {@link Definitions#OPTION_VALUE_DISABLE}.
     */
    public void setActive(String active) {
        this.active = active;
    }

    /**
     * Get a start hour. a format is 00.
     * @return start hour.
     */
    public String getStartHour() {
        return startHour;
    }

    /**
     * Set a start hour. a format is 00.
     * @param startHour start hour.
     */
    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    /**
     * Get a start minute. a format is 00.
     * @return start minute.
     */
    public String getStartMin() {
        return startMin;
    }

    /**
     * Set a start minute. a format is 00.
     * @param startMin start minute.
     */
    public void setStartMin(String startMin) {
        this.startMin = startMin;
    }

    /**
     * Get a am_pm for start time.
     * @return {@link Definitions#TIME_AM} or {@link Definitions#TIME_PM}.
     */
    public String getStartAmPm() {
        return startAmPm;
    }

    /**
     * Set a am_pm for start time.
     * @param startAmPm {@link Definitions#TIME_AM} or {@link Definitions#TIME_PM}
     */
    public void setStartAmPm(String startAmPm) {
        this.startAmPm = startAmPm;
    }

    /**
     * Get a end hour. a format is 00.
     * @return end hour
     */
    public String getEndHour() {
        return endHour;
    }

    /**
     * Set a end hour. a format is 00.
     * @param endHour
     */
    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    /**
     * Get a end minute. a format is 00.
     * @return end hour.
     */
    public String getEndMin() {
        return endMin;
    }

    /**
     * Set a end minute. a format is 00.
     * @param endMin end minute.
     */
    public void setEndMin(String endMin) {
        this.endMin = endMin;
    }

    /**
     * Get a am_pm of end time.
     * @return {@link Definitions#TIME_AM} or {@link Definitions#TIME_PM}.
     */
    public String getEndAmPm() {
        return endAmPm;
    }

    /**
     * Set a am_pm of end time.
     * @param endAmPm {@link Definitions#TIME_AM} or {@link Definitions#TIME_PM}.
     */
    public void setEndAmPm(String ampm) {
        this.endAmPm = ampm;
    }

    /**
     * Get a days. indicate a enable from SUN to SAT.
     * @return days.
     */
    public String getDays() {
        return days;
    }

    /**
     * Set a days.
     * @param d days.
     */
    public void setDays(String d) {
        this.days = d;
    }

    /**
     * Get a {@link Definitions#OPTION_VALUE_YES} or {@link Definitions#OPTION_VALUE_NO} of repeat.
     * @return one of {@link Definitions#OPTION_VALUE_YES} and {@link Definitions#OPTION_VALUE_NO}
     */
    public String getRepeat() {
        return repeat;
    }

    /**
     * Set a repeat.
     * @param r {@link Definitions#OPTION_VALUE_YES} or {@link Definitions#OPTION_VALUE_NO}
     */
    public void setRepeat(String r) {
        this.repeat = r;
    }

    /**
     * Get a save time.
     * @return time when save a value.
     */
    public long getSaveTime() {
        return saveTime;
    }

    /**
     * Set a save time.
     * @param time time to save a data.
     */
    public void setSaveTime(long time) {
        saveTime = time;
    }

    /**
     * Check a day whether value is {@link Definitions#OPTION_VALUE_YES}.
     * @param day wanted day to check.
     * @return true if day marks {@link Definitions#OPTION_VALUE_YES}, false otherwise.
     */
    public boolean checkDay(int day) {
        String[] parsedDay = TextUtil.tokenize(days, PreferenceService.PREFERENCE_DELIMETER);
        if (parsedDay != null && parsedDay[day] != null) {
            if (parsedDay[day].equals(Definitions.OPTION_VALUE_YES)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get a string that make with this time data.
     * @return data string.
     */
    public String getStringData() {
        StringBuffer sf = new StringBuffer();

        sf.append(active);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(startHour);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(startMin);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(startAmPm);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(endHour);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(endMin);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(endAmPm);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(days);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(repeat);
        sf.append(PreferenceService.PREFERENCE_DELIMETER);
        sf.append(saveTime);

        return sf.toString();
    }

    /**
     * Set a data String.
     * @param data data string.
     */
    public void setData(String data) {
        if (data != null) {
            String[] parsedData = TextUtil.tokenize(data, PreferenceService.PREFERENCE_DELIMETER);

            if (parsedData.length == 16) {
                setActive(parsedData[0]);
                setStartHour(parsedData[1]);
                setStartMin(parsedData[2]);
                setStartAmPm(parsedData[3]);
                setEndHour(parsedData[4]);
                setEndMin(parsedData[5]);
                setEndAmPm(parsedData[6]);
                StringBuffer sf = new StringBuffer();
                for (int i = 7; i < 14; i++) {
                    sf.append(parsedData[i]);
                    if (i < 13) {
                        sf.append(PreferenceService.PREFERENCE_DELIMETER);
                    }
                }
                setDays(sf.toString());
                setRepeat(parsedData[14]);
                setSaveTime(Long.parseLong(parsedData[15]));
            }
        }
    }
}

/**
 * @(#)Preference.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import java.util.Iterator;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * The Preference class represent a preference.
 * @author Woojung Kim
 * @version 1.1
 */
public class Preference {

    /** The name of preference. */
    private String name;
    /** The id of preference. */
    private String id;
    /** The description of preference. */
    private String description;
    /** The category id to include a this Preference. */
    private String categoryId;
    /** The default value of preference. */
    private String defaultValue;
    /** The current value of preference. */
    private String currentValue = TextUtil.EMPTY_STRING;
    /** The value list that select by user. */
    private String[] selectableValues;
    /** The application list that relate this preference. */
    private Vector relatedAppList;

    /**
     * The constructor of Preference. Initialize a {@link #relatedApplist}.
     */
    public Preference() {
        relatedAppList = new Vector();
    }

    /**
     * Get the name of preference.
     * @return the name of preference.
     */
    public String getName() {
        String n = DataCenter.getInstance().getString("menu." + id);
        if (!n.equals(TextUtil.EMPTY_STRING)) {
            return n;
        }
        return name;
    }

    /**
     * Set the name of preference.
     * @param n The name that present a preference.
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * Get a id of preference.
     * @return id
     */
    public String getID() {
        return id;
    }

    /**
     * Set a id of Preference.
     * @param preferenceId a unique id of preference to save and load.
     */
    public void setID(String preferenceId) {
        this.id = preferenceId;
    }

    /**
     * Get a description of preference.
     * @return a description of preference.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set a description of preference.
     * @param dscript the description of preference.
     */
    public void setDescription(String dscript) {
        this.description = dscript;
    }

    /**
     * Return a category id to include this Preference.
     * @return a cateogry id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Set a category id to include this Preference.
     * @param cid a category Id to include this Preference.
     */
    public void setCategoryId(String cid) {
        categoryId = cid;
    }

    /**
     * Get a default value of preference.
     * @return the default value of preference.
     */
    public String getDefaultValue() {
        return defaultValue;
    }

    /**
     * Set a default value of preference.
     * @param value the value of default for preference.
     */
    public void setDefaultValue(String value) {
        this.defaultValue = value;
    }

    /**
     * Get a current value of preference.
     * @return a current value
     */
    public String getCurrentValue() {
        if (currentValue != TextUtil.EMPTY_STRING) {
            return currentValue;
        } else if (defaultValue != null && defaultValue != TextUtil.EMPTY_STRING) {
            return defaultValue;
        }
        return TextUtil.EMPTY_STRING;
    }

    /**
     * Set a current value of preference.
     * @param value a value to set a current value.
     */
    public void setCurrentValue(String value) {
        this.currentValue = value;
    }

    /**
     * Get a selectable value list of preference.
     * @return a list of selectable value.
     */
    public String[] getSelectableValues() {
        return selectableValues;
    }

    /**
     * Set a list of selectable value of preference.
     * @param values a list of selectable value.
     */
    public void setSelectableValues(String[] values) {
        this.selectableValues = values;
    }

    /**
     * Decide that application to have a appId can use a preference.
     * @param appId the appId that want to access a preference.
     * @return true if a application can access, false otherwise.
     */
    public boolean isRelatedApp(String appId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("Preference: isRelatedApp: appId=" + appId);
            Log.printDebug("Preference: isRelatedApp: relatedAppList size=" + relatedAppList.size());
        }
        if (relatedAppList != null) {
            Iterator iterator = relatedAppList.iterator();
            while (iterator.hasNext()) {
                String temp = (String) iterator.next();
                if (appId.equals(temp)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Add a application id to access a preference.
     * @param appId the id that application want to access a preference.
     */
    public void addRelatedApp(String appId) {
        if (!relatedAppList.contains(appId)) {
            relatedAppList.add(appId);
            if (Log.DEBUG_ON) {
                Log.printDebug("Preference: addRelatedApp: added " + appId);
            }
        } else if (Log.DEBUG_ON) {
            Log.printDebug("Preference: addRelatedApp: already added.");
        }
    }
}

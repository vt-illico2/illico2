/**
 * @(#)User.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.controller.ProxyController;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * this class implements User Management function (create, modify, delete User).
 * @author Woojung Kim
 * @version 1.1
 */
public class User {

    /** The unique id for admin user. */
    public static final String ID_ADMIN = "admin";
    /** The display name for admin user. */
    public static final String NAME_ADMIN = "Administrator";
    /** The unique id for family user. */
    public static final String ID_FAMILY = "family";
    /** The display name for family user. */
    public static final String NAME_FAMILY_USER = "Family PIN";
    /** The unique id for anonymous user. */
    public static final String ID_ANONYMOUS = "anonymous";
    /** The display name for anonymous user. */
    public static final String NAME_ANONYMOUS = "Default User";

    /** The property name of user profile id. */
    public static final String USER_PROFILE_ID = "user.profile.id";
    /** The property name of user profile name. */
    public static final String USER_PROFILE_NAME = "user.profile.name";
    /** The property name of user profile pin. */
    public static final String USER_PROFILE_PIN = "user.profile.pin";

    /** Unique identifier within Videotron profile environment. */
    private String userId;

    /** The user name. */
    private String userName;

    /** User Pin. */
    private Pin pin;

    /** The preferences of User. */
    private Hashtable preferences;

    /** The access rights of User. */
    private Hashtable accessRights;

    /** The Access filters of User. */
    private Hashtable accessFilters;

    /**
     * Gets the user id.
     * @return the user id
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the user id.
     * @param id the new user id
     */
    public void setUserId(String id) {
        this.userId = id;
    }

    /**
     * Gets the user name.
     * @return the name of user.
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the user name.
     * @param name the name.
     */
    public void setUserName(String name) {
        this.userName = name;
    }

    /**
     * Return true if User has a Pin, false otherwise.
     * @return true if User has a Pin, false otherwise
     */
    public boolean hasPin() {
        if (pin != null) {
            return true;
        }
        return false;
    }

    /**
     * Sets a Pin for User.
     * @param p The Pin of User.
     */
    public void setPin(Pin p) {
        this.pin = p;
    }

    /**
     * Change a Pin to a new pin by new pin string.
     * @param newPin The new pin string to change a Pin.
     */
    public void changePin(String newPin) {
        if (pin == null) {
            pin = new Pin();
        }
        String pinString = pin.setPin(newPin);
        UserManager.getInstance().storeUserProperty(getUserId(), USER_PROFILE_PIN, pinString);
    }

    /**
     * Check a validation for Pin. Compare a targetPin with user pin.
     * @param targetPin The pin string to compare with user pin.
     * @return true if target pin string equals user pin. false otherwise.
     */
    public boolean isValidate(String targetPin) {
        if (this.pin == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("User: isValidate: " + userName + "'s pin is null");
            }
            return false;
        }
        return this.pin.isValidate(targetPin);
    }

    /**
     * Return a Hashtable includes a preferences that User has.
     * @return the preferens
     */
    public Hashtable getPreferences() {
        return preferences;
    }

    /**
     * Set a preferences that User has.
     * @param p the Hashtable includes a preferences that user has.
     */
    public void setPreferences(Hashtable p) {
        this.preferences = p;
    }

    /**
     * Get a User's access rights.
     * @return the Hashtable includes a access rights.
     */
    public Hashtable getAccessRights() {
        return accessRights;
    }

    /**
     * Set a User's access rights.
     * @param rights The Hashtable includes a User's access rights.
     */
    public void setAccessRights(Hashtable rights) {
        this.accessRights = rights;
    }

    /**
     * Get a Hashtable includes a User's access filters.
     * @return The Hashtable includes a User's access filters.
     */
    public Hashtable getAccessFilters() {
        return accessFilters;
    }

    /**
     * Set a Hashtable includes a User's access filters.
     * @param filters The Hashtable includes a User's access filters.
     */
    public void setAccessFilters(Hashtable filters) {
        this.accessFilters = filters;
    }

    /**
     * Update a access right.
     * @param name right name
     * @param value right value.
     */
    public void updateAccessRight(String name, boolean value) {
        if (Log.INFO_ON) {
            Log.printInfo("User: updateAccessRight: name = " + name + "\t value = " + value);
        }
        AccessRight right = (AccessRight) accessRights.get(name);

        right.setAccessRight(value);
        accessRights.put(name, right);

        UserManager.getInstance().storeUserProperty(getUserId(), name, value + TextUtil.EMPTY_STRING);
    }

    /**
     * update a access filter.
     * @param name filter name.
     * @param value filter value.
     */
    public void updateAccessFilter(String name, String value) {
        if (Log.INFO_ON) {
            Log.printInfo("User: updateAccessFilter: name = " + name + "\t value = " + value);
        }
        Preference filter = (Preference) accessFilters.get(name);

        filter.setCurrentValue(value);
        accessFilters.put(name, filter);

        UserManager.getInstance().storeUserProperty(getUserId(), name, value);

        // update to other application in case of anonymous.
        if (getUserId().equals(User.ID_ANONYMOUS)) {
            ProxyController.getInstance().notifyUpdatedPreference(name, value);
        }
    }

    /**
     * Clear a resources to use for User.
     */
    public void clear() {
        preferences.clear();
        accessRights.clear();
        accessFilters.clear();
    }
}

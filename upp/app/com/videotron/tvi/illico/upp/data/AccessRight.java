/**
 * @(#)AccessRight.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

/**
 * The AccessRight class represent a access right for features.
 * @author Woojung Kim
 * @version 1.1
 */
public class AccessRight {

    /** The name of Access Right. */
    private String name;

    /** The access right. */
    private boolean hasAccessRight;

    /**
     * Set a name of Access Right.
     * @param n The name of AccessRight
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * Return a name of Access Right.
     * @return The name of AccessRight.
     */
    public String getName() {
        return name;
    }

    /**
     * Return true if has access right. false otherwise.
     * @return true or false.
     */
    public boolean hasAccessRight() {
        return hasAccessRight;
    }

    /**
     * Set a true if has a access right.
     * @param aRight a value to have a access right.
     */
    public void setAccessRight(boolean aRight) {
        this.hasAccessRight = aRight;
    }
}

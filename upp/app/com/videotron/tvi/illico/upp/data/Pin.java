/**
 * @(#)Pin.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.videotron.tvi.illico.log.Log;

/**
 * The Pin class implements PIN Code.
 * @author Woojung Kim
 * @version 1.1
 */
public class Pin {

    /** The pin code to encrypt. */
    private String pin;

    /**
     * Sets the pin.
     * @param p the encrypted pin.
     * @return The encrypted pin String.
     */
    public String setPin(String p) {

        this.pin = encryptPin(p);

        return pin;
    }

    /**
     * Sets the encrypted pin string.
     * @param p the encrypted pin.
     */
    public void setEncryptedPinString(String p) {
        this.pin = p;
    }

    /**
     * Validate between own pin and target pin.
     * @param p the pin that will be validate.
     * @return result of validation
     */
    public boolean isValidate(String p) {
        if (Log.INFO_ON) {
            Log.printInfo("Pin: targetPin : " + p);
        }

        String encryptedTagetPin = encryptPin(p);

        if (Log.DEBUG_ON) {
            Log.printDebug("Pin: encrypt targetPin : " + encryptedTagetPin);
            Log.printDebug("Pin: encrypt srcPin : " + pin);
        }

        if (encryptedTagetPin != null && pin.equals(encryptedTagetPin)) {
            return true;
        }

        return false;
    }

    /**
     * Encrypt a pin by MD5.
     * @param p the pin code that will be encrypted
     * @return encrypted string
     */
    private String encryptPin(String p) {
        if (Log.INFO_ON) {
            Log.printInfo("Pin: encryptPin : " + p);
        }
        MessageDigest digester = null;
        try {
            digester = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if (digester != null) {
            digester.update(p.getBytes());
            byte[] hash = digester.digest();
            StringBuffer sf = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                if ((0xFF & hash[i]) < 0x10) {
                    sf.append("0" + Integer.toHexString(0xFF & hash[i]));
                } else {
                    sf.append(Integer.toHexString(0xFF & hash[i]));
                }
            }

            return sf.toString();
        }

        return null;
    }
}

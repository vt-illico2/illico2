/**
 * @(#)Category.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

/**
 * The Category class represent a category to make a setting menu.
 * @author Woojung Kim
 * @version 1.1
 */
public class Category {

    /** The name of category. */
    private String name;
    /** The description of category. */
    private String description;
    /** The type of category. see {@link CategoryType}. */
    private int type;
    /** The id of category. */
    private String categoryId;
    /** The id of parent category. */
    private String parentCategoryId;

    /**
     * Get a name.
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Set a name.
     * @param name name
     */
    public void setName(String n) {
        this.name = n;
    }

    /**
     * Get a description.
     * @return description.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set a descriptions.
     * @param description description.
     */
    public void setDescription(String d) {
        this.description = d;
    }

    /**
     * Get a type. see {@link CategoryType}
     * @return type
     */
    public int getType() {
        return type;
    }

    /**
     * Set a type. see {@link CategoryType}.
     * @param type type.
     */
    public void setType(int t) {
        this.type = t;
    }

    /**
     * Get a category id.
     * @return category id.
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Set a category id.
     * @param categoryId category id.
     */
    public void setCategoryId(String cId) {
        this.categoryId = cId;
    }

    /**
     * Get a parent id.
     * @return parent id.
     */
    public String getParentCategoryId() {
        return parentCategoryId;
    }

    /**
     * Set a prarent id.
     * @param parentCategoryId
     */
    public void setParentCategoryId(String pId) {
        this.parentCategoryId = pId;
    }
}

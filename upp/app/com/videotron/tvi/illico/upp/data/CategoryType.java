/**
 * @(#)CategoryType.java
 *
 * Copyright 2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.data;

/**
 * Define a category type for Category.
 * @author Woojung Kim
 * @version 1.1
 */
public class CategoryType {

    /** Indicate that Category is 1 depth. */
	public static final int TYPE_MAIN = 0x00;
	/** Indicate that Category is sub. */
	public static final int TYPE_SUB = 0x01;
	
	/** The constructor of Util class. */
	private CategoryType() {
	}
}

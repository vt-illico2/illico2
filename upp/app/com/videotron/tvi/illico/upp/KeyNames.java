/**
 * @(#)KeyNames.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp;

/**
 * This class has a key to get a name of title or preference etc according to Language.
 * @author Woojung Kim
 * @version 1.1
 */
public class KeyNames {

    // Main Menu
    public static final String MY_SETTINGS = "mysettings";
    public static final String TO_EXIT_SETTINGS = "to.exit.settings";
    public static final String WELCOME_ADMINISTRATOR = "welcom.admin";

    // General
    public static final String SETUP_WIZARD = "setup.wizard";
    public static final String START_SETUP_WIZARD = "start.setup.wizard";
    public static final String ENTER_POSTAL_CODE = "enter.postal.code";
    public static final String TITLE_INVALID_POSTAL_CODE = "title.invalid.postal.code";
    public static final String EXPLAIN_INVALID_POSTAL_CODE = "explain.invalid.postal.code";
    public static final String RESTART_TERMINAL = "restart.terminal";
    public static final String RESTART_TERMINAL_BUTTON = "restart.terminal.button";

    // PVR
    public static final String PVR_TITLE = "pvr.title";

    // parental control
    public static final String PERIODS = "periods";
    public static final String PERIOD = "period";
    public static final String RESTRICTED_PERIOD = "restricted.period";
    public static final String PROTECTED_CHANNELS = "protected.channels";
    public static final String PROTECTED_CHANNEL = "protected.channel";
    public static final String NONE = "None";

    // Time based
    public static final String ADD_PERIOD = "add.period";

    // channel restriction
    public static final String CHANNEL_RESTRICTION_TITLE = "channel.restriction.title";
    public static final String CHANNEL_RESTRICTION_TITLE_SUB = "channel.restriction.title.sub";
    public static final String CHANNEL_RESTRICTION_LEFT_SUBTITLE = "channel.restriction.left.subtitle";
    public static final String CHANNEL_RESTRICTION_RIGHT_SUBTITLE = "channel.restriction.right.subtitle";
    public static final String CHANNEL_RESTRICTION_INIT_MSG = "channel.restriction.init.msg";
    public static final String SUB_FIX_FAVOURITE_CHANNEL = "subfix.favourite.channel";
    public static final String SUB_FIX_FAVOURITE_CHANNELS = "subfix.favourite.channels";

    // Favourite channel
    public static final String FAVOURITE_CHANNEL_TITLE = "favourite.channel.title";
    // 	VDTRMASTER-5767
    public static final String FAVOURITE_CHANNEL_TITLE_SUB = "favourite.channel.title.sub";
    public static final String FAVOURITE_CHANNEL_LEFT_SUBTITLE = "favourite.channel.left.subtitle";
    public static final String FAVOURITE_CHANNEL_RIGHT_SUBTITLE = "favourite.channel.right.subtitle";
    public static final String FAVOURITE_CHANNEL_INIT_MSG = "favourite.channel.init.msg";

    // favourite filter
    public static final String FAVOURITE_FILTER_LEFT_SUBTITLE = "favourite.filter.left.subtitle";
    public static final String FAVOURITE_FILTER_RIGHT_SUBTITLE = "favourite.filter.right.subtitle";
    public static final String FAVOURITE_FILTER_INIT_MSG = "favourite.filter.init.msg";
    public static final String FAVOURITE_FILTER_SELECT_MSG = "favourite.filter.select.msg";

    // shortcut menu
    public static final String SHORTCUT_MENU_LEFT_SUBTITLE = "shortcut.menu.left.subtitle";
    public static final String SHORTCUT_MENU_RIGHT_SUBTITLE = "shortcut.menu.right.subtitle";
    public static final String SHORTCUT_MENU_INIT_MSG = "shortcut.menu.init.msg";
    public static final String SHORTCUT_MENU_SELECT_MSG = "shortcut.menu.select.msg";

    public static final String BTN_ADD = "btn.add";
    public static final String BTN_REMOVE = "btn.remove";

    public static final String BTN_BACK = "btn.back";
    public static final String BTN_ADD_PERIOD = "btn.add.peroid";
    public static final String BTN_PAGE = "btn.page";
    public static final String BTN_EDIT_ORDER = "btn.edit.order";
    public static final String BTN_CLEAR_SHORTCUTS = "btn.clear.shortcuts";
    public static final String BTN_CLEAR_CHANNEL_RESTRICTIONS = "btn.clear.channel.restrictions";
    public static final String BTN_CLEAR_CHANNEL_FILTERS = "btn.clear.channel.filter";
    public static final String BTN_CLEAR_FAVOURITE_CHANNELS = "btn.clear.favourite.channels";

    // TV Picture format
    public static final String SUBTITLE_CHOOSE_FORMAT = "sustitle.choose.format";
    public static final String SUBTITLE_GENERATE_PREVIEW = "subtitle.generate.preview";
    public static final String SUBTITLE_APPLY_FORMAT = "subtitle.apply.format";
    public static final String CURRENT_FORMAT = "current.format";
    public static final String EXPLAIN_GENERATE_PREVIEW = "explain.generate.preview";
    public static final String EXPLAIN_PREVIEW = "explain.preview";
    public static final String BTN_PREVIEW = "btn.preview";
    public static final String BTN_APPLY = "btn.apply";

    // Time
    public static final String SLEEP_TIME = "sleep.time";
    public static final String WAKE_UP_TIME = "wake.up.time";
    public static final String START_TIME = "start.time";
    public static final String END_TIME = "end.time";
    public static final String DAY = "day";
    public static final String REPEAT = "repeat";
    public static final String UPPER_PERIOD = "Period";

    // caller id
    public static final String MULTI_LINE = "multi.line";

    // popup
    public static final String BTN_CANCEL = "btn.cancel";
    public static final String BTN_DELETE = "btn.delete";
    public static final String BTN_CLEAR = "btn.clear";
    public static final String BTN_FORGOT = "btn.forgot";
    public static final String BTN_LATER = "btn.later";
    public static final String BTN_NOW = "btn.now";
    public static final String BTN_OK = "btn.ok";
    public static final String BTN_CONFIRM = "btn.confirm";
    public static final String BTN_YES = "btn.yes";
    public static final String BTN_NO = "btn.no";
    public static final String BTN_RESTART_NOW = "btn.restart.now";

    public static final String EXPLAIN_ACCESS_MENU = "explain.access.menu";
    public static final String EXPLAIN_ACCESS_GENERAL = "explain.access.general";
    public static final String EXPLAIN_ACCESS_APPLICATION = "explain.access.application";
    public static final String EXPLAIN_ACCESS_EQUIPMENT = "explain.access.equipment";
    public static final String EXPLAIN_ACCESS_ACCESSIBILITY = "explain.access.accessibility";
    public static final String EXPLAIN_CREATE_ADMIN = "explain.create.admin";
    public static final String EXPLAIN_MODIFY_ADMIN_PIN = "explain.modify.admin.pin";
    public static final String EXPLAIN_CREATE_ADMIN_PIN = "explain.create.admin.pin";
    public static final String EXPLAIN_MODIFY_FAMILY_PIN = "explain.modify.family.pin";
    public static final String EXPLAIN_CREATE_FAMILY_PIN = "explain.create.family.pin";
    public static final String EXPLAIN_CREATE_NEW_PIN = "explain.create.new.pin";
    public static final String EXPLAIN_MODIFY_NEW_PIN = "explain.modify.new.pin";
    public static final String EXPLAIN_NOT_MATCHED = "explain.not.matched";
    public static final String EXPLAIN_MATCHED_CURRENT_PIN = "explain.matched.current.pin";
    public static final String EXPLAIN_NOT_MATCHED_ADMIN = "explain.not.matched.admin";
    public static final String EXPLAIN_NOT_MATCHED_ADMIN_NEW = "explain.not.matched.admin.new";
    public static final String EXPLAIN_NOT_MATCHED_FAMILY_NEW = "explain.not.matched.family.new";
    public static final String EXPLAIN_ADMIN_DIFFER_FROM_FAMILY = "explain.admin.differ.from.family";
    public static final String EXPLAIN_FAMILY_DIFFER_FROM_ADMIN = "explain.family.differ.from.admin";
    public static final String EXPLAIN_FORGOT_PIN = "explain.forgot.pin";
    public static final String EXPLAIN_ADMIN_PIN_ACCEPTED = "explain.admin.pin.accepted";
    public static final String EXPLAIN_FAMILY_PIN_ACCEPTED = "explain.family.pin.accepted";
    public static final String EXPLAIN_FAMILY_NEW_PIN_ACCEPTED = "explain.family.new.pin.accepted";
    public static final String EXPLAIN_NEW_PIN_ACCEPTED = "explain.new.pin.accepted";
    public static final String EXPLAIN_PIN_ERROR = "explain.pin.error";
    //R7.4 Blocked Admin PIN
    public static final String EXPLAIN_PIN_FORGET = "explain.pin.forget";
    //public static final String EXPLAIN_HOW_TO_CHANGE = "explain.how.to.change";
    public static final String EXPLAIN_RETRY = "explain.retry";
    public static final String EXPLAIN_CONTENT_NEED_ADMIN_PIN = "explain.content.need.admin.pin";
    public static final String EXPLAIN_REMOTE_CONTROL = "explain.remote.control";
    public static final String EXPLAIN_SELECT_DAYS_SLEEP_TIME = "explain.select.days.sleep.time";
    public static final String EXPLAIN_SELECT_DAYS_WAKE_UP_TIME = "explain.select.days.wake.up.time";
    public static final String EXPLAIN_SELECT_DAYS_TIME_BASED_RESTRICTIONS = "explain.select.days.time.based.restrictions";
    public static final String EXPLAIN_REQUIRE_ADMIN_PIN = "explain.require.admin.pin";
    public static final String EXPLAIN_CLEAR_RESTRICTED_CHANNELS = "explain.clear.restricted.channels";
    public static final String EXPLAIN_CLEAR_FAVOURITE_CHANNELS = "explain.clear.favourite.channels";
    public static final String EXPLAIN_CLEAR_SHORTCUTS = "explain.clear.shortcuts";
    public static final String EXPLAIN_PARENTAL_CONTROL_ON = "explain.parental.control.on";
    public static final String EXPLAIN_PARENTAL_CONTROL_SUSPEND_4 = "explain.parental.control.suspend.4";
    public static final String EXPLAIN_PARENTAL_CONTROL_SUSPEND_8 = "explain.parental.control.suspend.8";
    public static final String EXPLAIN_SELECT_LINE = "explain.phone.line.selection";
    public static final String EXPLAIN_NO_PHONE_LINE_SELECTED = "explain.no.phone.line.selected";
    public static final String EXPLAIN_NEED_PIN = "explain.need.pin";
    public static final String EXPLAIN_PIN_REMAIN = "explain.pin.remain";
    public static final String EXPLAIN_PIN_REMAINS = "explain.pin.remains";
    public static final String EXPLAIN_BUFFER_CHANGE = "explain.buffer.change";
    public static final String EXPLAIN_RESTART_TERMINAL = "explain.restart.terminal";

    public static final String SUBTITLE_NEW_PIN = "subtitle.new.pin";
    public static final String SUBTITLE_CURRENT_PIN = "subtitle.current.pin";
    public static final String SUBTITLE_REPEAT = "subtitle.repeat";
    public static final String SUBTITLE_PIN_CODE = "subtitle.pin.code";
    public static final String SUBTITLE_CONFIRM_PIN_CODE = "subtitle.confirm.pin.code";
    public static final String SUBTITLE_BLOCK_BY_RATING = "subtitle.block.by.rating";

    public static final String TITLE_MODIFY_ADMIN_PIN = "title.modify.admin.pin";
    public static final String TITLE_CREATE_ADMIN_PIN = "title.create.admin.pin";
    public static final String TITLE_MODIFY_FAMILY_PIN = "title.modify.family.pin";
    public static final String TITLE_CREATE_FAMILY_PIN = "title.create.family.pin";
    public static final String TITLE_CREATE_PIN = "title.create.pin";
    public static final String TITLE_MODIFY_PIN = "title.modify.pin";
    public static final String TITLE_ENTER_PIN = "title.enter.pin";
    public static final String TITLE_INVALID_PIN = "title.invalid.pin";
    public static final String TITLE_BLOCKED_PIN = "title.blocked.pin";
    public static final String TITLE_ADMIN_PIN_ACCEPTED = "title.admin.pin.accepted";
    public static final String TITLE_FAMILY_PIN_ACCEPTED = "title.family.pin.accepted";
    public static final String TITLE_NEW_PIN_ACCEPTED = "title.new.pin.accepted";
    public static final String TITLE_PIN_ERROR = "title.pin.error";
    public static final String TITLE_WRONG_PIN = "title.wrong.pin";
    public static final String TITLE_PARENTAL_CONTROL_WIZARD = "title.parental.control.wizard";
    public static final String TITLE_SELECT_DAYS = "title.select.days";
    public static final String TITLE_CONFIRM_DELETION = "title.confirm.deletion";
    public static final String TITLE_ENTER_ADMIN_PIN = "title.enter.admin.pin";
    public static final String TITLE_PARENTAL_CONTROL_ON = "title.parental.control.on";
    public static final String TITLE_PARENTAL_CONTROL_SUSPEND = "title.parental.control.suspend";
    public static final String TITLE_SELECT_LINE = "title.select.line";
    public static final String TITLE_NO_PHONE_LINE_SELECTED = "title.no.phone.line.selected";
    public static final String TITLE_BUFFER_CHANGE = "title.buffer.change";
    public static final String TITLE_RESTART_TERMINAL = "title.restart.terminal";

    // normal text
    public static final String EVERY_WEEK = "every.week";
    public static final String EVERY_DAY = "every.day";
    public static final String WEEKDAYS = "weekdays";
    public static final String WEEKENDS = "weekends";
    public static final String SELECT_DAYS = "select.days";
    public static final String MONDAY = "monday";
    public static final String TUESDAY = "tuesday";
    public static final String WEDNESDAY = "wednesday";
    public static final String THURSDAY = "thursday";
    public static final String FRIDAY = "friday";
    public static final String SATURDAY = "saturday";
    public static final String SUNDAY = "sunday";
    public static final String MON = "mon";
    public static final String TUE = "tue";
    public static final String WED = "wed";
    public static final String THU = "thu";
    public static final String FRI = "fri";
    public static final String SAT = "sat";
    public static final String SUN = "sun";

    public static final String SCOPE_8 = "scope.8";
    public static final String SCOPE_13 = "scope.13";
    public static final String SCOPE_16 = "scope.16";
    public static final String SCOPE_18 = "scope.18";

}

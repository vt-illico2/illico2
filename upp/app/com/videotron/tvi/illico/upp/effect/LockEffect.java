package com.videotron.tvi.illico.upp.effect;

import java.awt.Component;
import java.awt.Graphics;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.effect.ScaleEffect;
import com.videotron.tvi.illico.framework.effect.ScaleFactor;

/**
 * This class animate a lock or unlock according to Parental Control.
 * @author Woojung Kim
 * @version 1.1
 */
public class LockEffect extends ScaleEffect {

    /** A height of top. */
    private int topHeight;

    /** A x coordinate of destination for top. */
    private int[] tx1;
    /** A y coordinate of destination for top. */
    private int[] ty1;
    /** A x coordinate of source for top. */
    private int[] tx2;
    /** A Y coordinate of source for top. */
    private int[] ty2;
    /** A x coordinate of destination for bottom . */
    private int[] bx1;
    /** A y coordinate of destination for bottom. */
    private int[] by1;
    /** A x coordinate of source for bottom. */
    private int[] bx2;
    /** A y coordinate of source for bottom. */
    private int[] by2;

    /** a scaled value. */
    private static ScaleFactor LOCK_FACTOR = new ScaleFactor(1d, 1.1d, 1d, 0.2d, 0.5d, 0.7d, 0.8d, 0d, 1d);

    /**
     * Make a coordinate and values for animation.
     * @param c a component to be animated.
     * @param frameCount a total frame.
     * @param topHeight a height to separate a component.
     * @param from x coordinate to start.
     * @param to x coordinate to end.
     */
    public LockEffect(Component c, int frameCount, int topHeight, int from, int to) {
        super(c, frameCount, LOCK_FACTOR);
        this.topHeight = topHeight;

        tx1 = new int[frameCount];
        ty1 = new int[frameCount];
        tx2 = new int[frameCount];
        ty2 = new int[frameCount];
        bx1 = new int[frameCount];
        by1 = new int[frameCount];
        bx2 = new int[frameCount];
        by2 = new int[frameCount];

        int moveLen = to - from;

        int maxMoveLeft = 0;
        int maxMoveRight = 0;

        double[] offset = new double[frameCount];

        for (int i = 0; i < frameCount; i++) {
            double step = (double) i / (frameCount - 1);
            offset[i] = ((getTopOffsetFactor(step) * moveLen) + from) * scale[i];
            maxMoveLeft = Math.max(maxMoveLeft, -(int) (pointX[i] + offset[i]));
            maxMoveRight = Math.max(maxMoveRight, (int) (pointX[i] + offset[i] + widths[i]) - maxWidth);
        }
        for (int i = 0; i < frameCount; i++) {
            tx1[i] = (int) (pointX[i] + offset[i] + maxMoveLeft);
            tx2[i] = tx1[i] + widths[i];
            ty1[i] = pointY[i];
            ty2[i] = by1[i] = ty1[i] + (int) (scale[i] * topHeight);

            bx1[i] = pointX[i] + maxMoveLeft;
            bx2[i] = bx1[i] + widths[i];
            by2[i] = pointY[i] + heights[i];
        }

        clipBoundsXOffset = (imageWidth - maxWidth) / 2 - maxMoveLeft;
        maxWidth = maxWidth + maxMoveLeft + maxMoveRight;
        setClipBounds(clipBoundsXOffset, clipBoundsYOffset, maxWidth, maxHeight);
    }

    /**
     * Get a offset value for Top.
     * @param x scale step.
     * @return offset.
     */
    private double getTopOffsetFactor(double x) {
        if (x < factor.scaleUpFrom) {
            return 0;
        }
        if (x <= factor.scaleUpUntil) {
            return (x - factor.scaleUpFrom) / (factor.scaleUpUntil - factor.scaleUpFrom);
        }
        return 1;
    }

    /**
     * Display a animation.
     * @param g Graphics.
     * @param image DVBBufferedImage to display.
     * @param frame a current frame.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        // top
        g.drawImage(image, tx1[frame], ty1[frame], tx2[frame], ty2[frame], 0, 0, imageWidth, topHeight, null);
        // bottom
        g.drawImage(image, bx1[frame], by1[frame], bx2[frame], by2[frame], 0, topHeight, imageWidth, imageHeight, null);

        if (frame == frameCount - 1) {
            try {
                Thread.sleep(3000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.videotron.tvi.illico.upp.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.upp.ui.BaseUI;

/**
 * ListEffect implements that item move from left to right or from right to left.
 * @author Woojung Kim
 * @version 1.1
 */
public class ListEffect extends Effect {
    /** Indicate that items move from left to right. */
    public static final int MODE_FROM_LEFT = 0;
    /** Indicate that items move from right to left. */
    public static final int MODE_FROM_RIGHT = 1;

    /** Indicate a current mode. */
    private int currentMode = MODE_FROM_LEFT;
    /** Indicate a left index. */
    private int leftIdx;
    /** indicate a right index. */
    private int rightIdx;
    /** A y coordinate of left top. */
    private int leftTopY;
    /** A y coordinate of left bottom. */
    private int leftBotY;
    /** A height of left bottom. */
    private int leftBotHeight;
    /** A y coordinate of right top. */
    private int rightTopY;
    /** A y coordinate of right bottom. */
    private int rightBotY;
    /** A height of right bottom. */
    private int rightBotHeight;

    /** parent ui. */
    private BaseUI ui;
    /** true if need to scroll down for this effect. */
    private boolean isDown;

    /** A array for left animation to have a y coordinate. */
    private int[] leftPosY = new int[8];
    /** A array for right animation to have a y coordinate. */
    private int[] rightPosY = new int[8];

    /**
     * Constructor. Set a Parent UI to display a decoration of List.
     * @param c parent UI
     */
    public ListEffect(Component c) {
        super(c, 8);
        ui = (BaseUI) c;
        updateBackgroundBeforeStart(false);
    }

    /**
     * Start a animation.
     * @param mode a mode to want, one of {@link #MODE_FROM_LEFT}, {@link #MODE_FROM_RIGHT}
     * @param lIdx a index of left list.
     * @param rIdx a index of right list.
     * @param down a true if no items is over current index, false other.
     */
    public void start(int mode, int lIdx, int rIdx, boolean down) {
        if (Log.INFO_ON) {
            Log.printInfo("ListEffect: start()");
        }

        if (Log.DEBUG_ON) {
            Log.printInfo("ListEffect: mode = " + mode + "\t lIdx = " + lIdx + "\t rIdx = " + rIdx
                    + "\t down = " + down);
        }

        currentMode = mode;
        leftIdx = lIdx;
        rightIdx = rIdx;
        isDown = down;
        if (mode == MODE_FROM_LEFT) {
            for (int i = 0; i < 8; i++) {
                leftPosY[i] = 32 - i * 4;
                rightPosY[i] = i * 4;
            }

            leftTopY = lIdx * 32;
            leftBotY = leftTopY + 32;
            leftBotHeight = (9 - lIdx) * 32;
            rightTopY = rIdx * 32;
            rightBotY = rightTopY + 32;
            rightBotHeight = (9 - rIdx) * 32;
        } else {
            for (int i = 0; i < 8; i++) {
                leftPosY[i] = i * 4;
                rightPosY[i] = 32 - i * 4;
            }

            leftTopY = lIdx * 32;
            leftBotY = leftTopY + 32;
            leftBotHeight = (9 - lIdx) * 32;
            rightTopY = rIdx * 32;
            rightBotY = rightTopY + 32;
            rightBotHeight = (9 - rIdx) * 32;
        }
        this.start();
    }

    /**
     * Display a animation.
     * @param g Graphics.
     * @param image DVBBufferedImage to display.
     * @param frame a current frame.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {

        Rectangle before = g.getClipBounds();

        g.clipRect(53, 173, 850, 292);

        if (currentMode == MODE_FROM_LEFT) {
            // left list 53, 173, 353, 378
            if (isDown) {
                g.drawImage(image, 53, 173, 406, 173 + leftTopY + 32, 53, 173, 406, 173 + leftTopY + 32, null);
            } else {
                g.drawImage(image, 53, 173, 406, 173 + leftTopY, 53, 173, 406, 173 + leftTopY, null);
                g.drawImage(image, 53, 173 + leftTopY + leftPosY[frame], 406, 173 + leftTopY + leftPosY[frame]
                        + leftBotHeight, 53, 173 + leftTopY, 406, 173 + leftTopY + leftBotHeight, null);
            }

            // right list 555, 173, 353, 378
            if (rightIdx > -1 && rightIdx < 9) {
                g.drawImage(image, 555, 173, 908, 173 + rightTopY, 555, 173, 908, 173 + rightTopY, null);
                g.drawImage(image, 555, 173 + rightTopY + rightPosY[frame], 908, 173 + rightTopY + rightPosY[frame]
                        + rightBotHeight, 555, 173 + rightBotY, 908, 173 + rightBotY + rightBotHeight, null);
            } else {
                g.drawImage(image, 555, 173, 908, 173 + 293, 555, 173, 908, 173 + 293, null);
            }
        } else {
            // left list 53, 173, 353, 378
            if (leftIdx > -1 && leftIdx < 9) {
                g.drawImage(image, 53, 173, 406, 173 + leftTopY, 53, 173, 406, 173 + leftTopY, null);
                g.drawImage(image, 53, 173 + leftTopY + leftPosY[frame], 406, 173 + leftTopY + leftPosY[frame]
                        + leftBotHeight, 53, 173 + leftBotY, 406, 173 + leftBotY + leftBotHeight, null);
            } else {
                g.drawImage(image, 53, 173, 406, 173 + 293, 53, 173, 406, 173 + 293, null);
            }

            // right list 555, 173, 353, 378
            if (isDown) {
                g.drawImage(image, 555, 173, 908, 173 + rightTopY + 32, 555, 173, 908, 173 + rightTopY + 32, null);
            } else {
                g.drawImage(image, 555, 173, 908, 173 + rightTopY, 555, 173, 908, 173 + rightTopY, null);
                g.drawImage(image, 555, 173 + rightTopY + rightPosY[frame], 908, 173 + rightTopY + rightPosY[frame]
                        + rightBotHeight, 555, 173 + rightTopY, 908, 173 + rightTopY + rightBotHeight, null);
            }
        }

        g.setClip(before);
        ui.paintDecoOfList(g, ui);
    }
}

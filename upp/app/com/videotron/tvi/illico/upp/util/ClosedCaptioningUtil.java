/**
 * @(#)ClosedCaptioningUtil.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.util;

import java.awt.Color;

import org.ocap.media.ClosedCaptioningAttribute;

import com.videotron.tvi.illico.log.Log;

/**
 * This class provide a API to set a attributes for ClosedCaptioning.
 * @author Woojung Kim
 * @version 1.1
 */
public class ClosedCaptioningUtil {
    /** Singleton instance. */
    private static ClosedCaptioningUtil instance;
    /** Color array. */
    private Color[] colors = {Color.black, Color.yellow, Color.black, Color.white, Color.red, Color.green, Color.blue,
            Color.cyan, Color.magenta};
    /** Font Color array. */
    private Color[] fontColors = {Color.white, Color.yellow, Color.black, Color.white, Color.red, Color.green,
            Color.blue, Color.cyan, Color.magenta};

    // private int[] shadings = {ClosedCaptioningAttribute.CC_OPACITY_TRANSPARENT,
    // ClosedCaptioningAttribute.CC_OPACITY_TRANSPARENT, ClosedCaptioningAttribute.CC_OPACITY_SOLID,
    // ClosedCaptioningAttribute.CC_OPACITY_TRANSLUCENT, ClosedCaptioningAttribute.CC_OPACITY_FLASH};

    /** Shading value array. */
    private int[] shadings = {ClosedCaptioningAttribute.CC_OPACITY_SOLID,
            ClosedCaptioningAttribute.CC_OPACITY_TRANSPARENT, ClosedCaptioningAttribute.CC_OPACITY_TRANSLUCENT};
    /** Font size array. */
    private int[] fontSizes = {ClosedCaptioningAttribute.CC_PEN_SIZE_STANDARD,
            ClosedCaptioningAttribute.CC_PEN_SIZE_SMALL, ClosedCaptioningAttribute.CC_PEN_SIZE_LARGE};

    /**
     * private constructor for singleton.
     */
    private ClosedCaptioningUtil() {
    }

    /**
     * Get a singleton object of this class.
     * @return singleton object.
     */
    public static ClosedCaptioningUtil getInstance() {
        if (instance == null) {
            instance = new ClosedCaptioningUtil();
        }

        return instance;
    }

    /**
     * Updates the individual CC attribute.
     * @param attribute One of {@link ClosedCaptioningAttribute#CC_ATTRIBUTE_PEN_FG_COLOR},
     *            {@link ClosedCaptioningAttribute#CC_ATTRIBUTE_WINDOW_FILL_COLOR},
     *            {@link ClosedCaptioningAttribute#CC_ATTRIBUTE_PEN_FG_OPACITY},
     *            {@link ClosedCaptioningAttribute#CC_ATTRIBUTE_WINDOW_FILL_OPACITY},
     *            {@link ClosedCaptioningAttribute#CC_ATTRIBUTE_PEN_SIZE}
     * @param attributeStr Attribute name
     * @param value index of attribute value.
     */
    public void updateCCAttribute(int attribute, String attributeStr, int value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ClosedCaptioningUtil:: updateCCAttribute: attribute = " + attribute + " attributeStr = "
                    + attributeStr + " value = " + value);
        }
        // Convert the supplied value into an object
        Object valueObj = null;

        if (attribute == ClosedCaptioningAttribute.CC_ATTRIBUTE_PEN_FG_COLOR) {
            if (value < colors.length) {
                valueObj = fontColors[value];
            }
        } else if (attribute == ClosedCaptioningAttribute.CC_ATTRIBUTE_WINDOW_FILL_COLOR) {
            if (value < colors.length) {
                valueObj = colors[value];
            }
        } else if ((attribute == ClosedCaptioningAttribute.CC_ATTRIBUTE_PEN_FG_OPACITY)
                || (attribute == ClosedCaptioningAttribute.CC_ATTRIBUTE_WINDOW_FILL_OPACITY)) {
            if (value < shadings.length) {
                valueObj = new Integer(shadings[value]);
            }
        } else if ((attribute == ClosedCaptioningAttribute.CC_ATTRIBUTE_PEN_SIZE)) {
            if (value < fontSizes.length) {
                valueObj = new Integer(fontSizes[value]);
            }
        }

        if (valueObj == null) {
            valueObj = new Integer(value);
        }

        // Set the attribute
        try {
            ClosedCaptioningAttribute.getInstance().setCCAttribute(new int[] {attribute}, new Object[] {valueObj},
                    ClosedCaptioningAttribute.CC_TYPE_DIGITAL);
        } catch (IllegalArgumentException e) {
            StringBuffer sb = new StringBuffer();
            Object[] values = ClosedCaptioningAttribute.getInstance().getCCCapability(attribute,
                    ClosedCaptioningAttribute.CC_TYPE_DIGITAL);
            if (values != null) {
                for (int i = 0; i < values.length; i++) {
                    sb.append(values[i]);
                    sb.append(", ");
                }
            }

            Log.printError("ClosedCaptioningUtil:: updateCCAttribute: Illegal attribute value of: " + value
                    + " for attribute: " + attributeStr + " id: " + attribute + ", Valid values are: " + sb.toString());

            // Try setting attribute to 1st valid value
            setCCAttr1stValue(attribute, attributeStr);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug("ClosedCaptioningUtil:: updateCCAttribute: updating attribute: " + attributeStr
                    + " to value: " + valueObj);
        }
    }

    /**
     * Sets the value of this CC attribute to first valid value.
     * @param attribute id of attribute to set
     * @param attributeStr attribute name.
     */
    private void setCCAttr1stValue(int attribute, String attributeStr) {
        // Get the valid values for this attribute
        Object values[] = ClosedCaptioningAttribute.getInstance().getCCCapability(attribute,
                ClosedCaptioningAttribute.CC_TYPE_DIGITAL);
        if (values != null) {
            // Set attribute to first valid value
            if ((values.length > 0) && (values[0] != null)) {
                try {
                    ClosedCaptioningAttribute.getInstance().setCCAttribute(new int[] {attribute},
                            new Object[] {values[0]}, ClosedCaptioningAttribute.CC_TYPE_DIGITAL);
                } catch (Exception e) {
                    Log.printError("ClosedCaptioningUtil:: updateCCAttribute: Problems setting attribute value to"
                            + " first returned valid value: " + values[0] + " for attribute: " + attributeStr + " id: "
                            + attribute + "Exception: " + e);
                }
            }
        }
    }
}

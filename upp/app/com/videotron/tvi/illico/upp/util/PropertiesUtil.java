/**
 * @(#)PropertiesUtil.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.upp.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.PropertiesReader;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.log.Log;

/**
 * The PropertiesUtil class has a functions to support that a preferences load and save in Repository area.
 * @author Woojung Kim
 * @version 1.1
 */
public final class PropertiesUtil extends PropertiesReader {

    /**
     * Loads all preference from file and adds into Hashtable.
     * @param fileName the file name
     * @param table the table
     */
    public static void loadAllProperties(final String fileName, Hashtable table) {
        if (Log.DEBUG_ON) {
            Log.printDebug("loadAllProperties");
            Log.printDebug("fileName : " + fileName);
        }

        FileInputStream fis = null;
        try {
            Properties props = new Properties();
            fis = new FileInputStream(fileName);
            props.load(fis);

            Log.printDebug("load size : " + props.size());

            Enumeration enu = props.propertyNames();

            while (enu.hasMoreElements()) {
                String key = (String) enu.nextElement();
                String value = props.getProperty(key);

                Log.printDebug("key : " + key);
                Log.printDebug("value : " + value);
                table.put(key, value);
            }

        } catch (Exception e) {
            Log.print(e);
//            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
//                    ErrorMessageService.IXC_NAME);
//            if (eService != null) {
//                try {
//                    eService.showErrorMessage("PRF503", null);
//                } catch (RemoteException e1) {
//                    Log.print(e1);
//                }
//            }
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: loadAllProperties" + e);
                    }
                    fis = null;
                }
            }
        }
    }

    /**
     * Returns property value.
     * @param fileName the file name that include property.
     * @param keyName the key name of property
     * @return the value of property
     */
    public static String getProperty(final String fileName, final String keyName) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PropertiesUtil: getProperty: fileName = " + fileName + "\t keyName = " + keyName);
        }

        String value = null;

        FileInputStream fis = null;
        try {
            Properties props = new Properties();
            fis = new FileInputStream(fileName);
            props.load(fis);
            value = props.getProperty(keyName);
            if (Log.DEBUG_ON) {
                Log.printDebug("PropertiesUtil: getProperty: value = " + value);
            }
            if (value != null && value.equals("null") || value.equals("")) {
                value = null;
            }
        } catch (Exception e) {
            Log.print(e);
//            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
//                    ErrorMessageService.IXC_NAME);
//            if (eService != null) {
//                try {
//                    eService.showErrorMessage("PRF503", null);
//                } catch (RemoteException e1) {
//                    Log.print(e1);
//                }
//            }
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: getProperty():" + e);
                    }
                    fis = null;
                }
            }
        }

        return value.trim();
    }

    /**
     * stores property value.
     * @param fileName the file name that property will be stored.
     * @param keyName the key name of property
     * @param value the value of property
     * @return true, if successful
     */
    public static synchronized boolean setProperty(final String fileName, final String keyName, final String value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PropertiesUtil: setProperty: fileName = " + fileName + "\t keyName = " + keyName
                    + "\t value = " + value);
        }

        boolean retVal = false;
        FileInputStream fis = null;
        FileOutputStream fos = null;

        Properties props = new Properties();

        try {
            File file = new File(fileName);
            if (!file.exists()) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("file not exists");
                    Log.printDebug("call to createNewFile");
                }
                file.createNewFile();
            }
            fis = new FileInputStream(file);
            props.load(fis);
        } catch (Exception e) {
        	 Log.printError("PropertiesUtil: setProperty(): error while set a property: fileName = " + fileName + ", key = " + keyName + ", value = " + value);
             Log.printError("PropertiesUtil: setProperty(): " + e.getMessage());
             Log.print(e);
            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
                    ErrorMessageService.IXC_NAME);
            if (eService != null) {
                try {
                    eService.showErrorMessage("PRF503", null);
                } catch (RemoteException e1) {
                    Log.print(e1);
                }
            }
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                    fis = null;
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: setProperty():" + e);
                    }
                    fis = null;
                }
            }
        }

        try {
            if (props.containsKey(keyName)) {
                String storedValue = props.getProperty(keyName);
                if (storedValue != null && storedValue.equals(value)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("value is already saved. so don't restore.");
                    }
                    return true;
                }
            }

            props.setProperty(keyName, value);
            fos = new FileOutputStream(fileName);
            props.store(fos, "");
            retVal = true;
        } catch (Exception e) {
        	Log.printError("PropertiesUtil: setProperty(): error while set a property: fileName = " + fileName + ", key = " + keyName + ", value = " + value);
            Log.printError("PropertiesUtil: setProperty(): " + e.getMessage());
            Log.print(e);
            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
                    ErrorMessageService.IXC_NAME);
            if (eService != null) {
                try {
                    eService.showErrorMessage("PRF503", null);
                } catch (RemoteException e1) {
                    Log.print(e1);
                }
            }
        } finally {
            if (fos != null) {
                try {
                    fos.close();
                    fos = null;
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: setProperty():" + e);
                    }
                    fos = null;
                }
            }
        }
        return retVal;
    }

    /**
     * Read a properties from file.
     * @param base based properties.
     * @param path file full path + name.
     * @return Properties which is loaded.
     */
    public static Properties read(Properties base, String path) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PropertiesUtil: read(Properties, String): base = " + base + "\t path = " + path);
        }

        Properties p = new Properties(base);
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
            fis = new FileInputStream(path);
            bis = new BufferedInputStream(fis);
            p.load(bis);
        } catch (IOException e) {
            if (Log.ERROR_ON) {
                Log.printError("PropertiesUtil: read(Properties, String): " + e);
            }
//            ErrorMessageService eService = (ErrorMessageService) DataCenter.getInstance().get(
//                    ErrorMessageService.IXC_NAME);
//            if (eService != null) {
//                try {
//                    eService.showErrorMessage("PRF503", null);
//                } catch (RemoteException e1) {
//                    Log.print(e1);
//                }
//            }
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: setProperty():" + e);
                    }
                    bis = null;
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    if (Log.ERROR_ON) {
                        Log.printError("PropertiesUtil: read(Properties, String): " + e);
                    }
                    fis = null;
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("PropertiesUtil: read(Properties, String): " + p.size() + " properties.");
            Log.printDebug("PropertiesUtil: read(Properties, String): " + p);
        }

        return p;
    }

    /** The constructor for Utils class. */
    private PropertiesUtil() {
    }
}

This is Profile App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 1.3.0.0
    - Release App file Name
      : Profile.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2013.12.20

***
version 1.3.0.0 - 2013.12.20
    - New Feature
        + First release after merged with PO1
    - Resolved Issues
        N/A
***
version 1.2.3.0_PO1 - 2013.11.21
    - New Feature
        N/A
    - Resolved Issues
        + update a TOC for VOD and 1080p support.
***
version 1.2.2.1_PO1 - 2013.10.29
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4911 [PO1] 1080P value is displayed in TV Picture Format of Equipment Settings.
***
version 1.2.1.0_PO1 - 2013.10.22
    - New Feature
        + Support DDC 76.1 which has the feature of 1080p on VOD
***
version 1.2.0.0_PO1 - 2013.10.03
    - New Feature
        + First release including the followings
		+ Support(DDC42-2) In the List of recorded programs, sorted by date, the episodes of a series (same name or series id) should be grouped.
		(VDTRMASTER2876) ==> A new property added.
    - Resolved Issues
        + N/A
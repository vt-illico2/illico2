﻿This is Dashboard App Release Notes.

GENERAL RELEASE INFORMATION
 - Release Details
    - Release version
      : version (R7.4).1.1
    - Release file name
      : Dashboard.zip
    - Release type (official, debug, test,...)
      : official       
    - Release date
      : 2017.09.14

***
version (R7.4).1.1
    - New Feature
       N/A
    - Resolved Issues
       VDTRMASTER-6197 [CQ][R7.3][R7.4][VBM][Profile]: VBM measurements for Dashboard are not displayed when selecting Settings from Widgets, and the option "Protect access to Settings" is setted to "YES"
***
version (R7.2+CYO).1.0 - 2016.09.19
    - New Feature
       + consumed a zoom key (R7.2).
    - Resolved Issues
       N/A
***
version R7.1.1 - 2016.07.29
    - New Feature   
        N/A
    - Resolved Issues
       + VDTRMASTER-5846 [prod][R7.1] Weather service sends multiple request simultaneously
***
version R5.3.0 - 2015.08.28
    - New Feature   
        + DDC-113 Visually Impaired : ?쏶TAR??key is blocked.
    - Resolved Issues
        N/A
***
version R5.2.0 - 2015.07.31
    - New Feature   
        + Applying Flat design modification.
    - Resolved Issues
        N/A
***
version R5.1.0 - 2015.07.20
    - New Feature   
        + Impelements R5-TANK
    - Resolved Issues
        N/A
***
version 4K.2.0 - 2015.06.12
    - New Feature   
        + remove shared/00_banner_shadow.png.
        + update a 02_d_shadow.png
        + change a coordinates for background.
    - Resolved Issues
        N/A
***
version 4K.1.0 - 2015.04.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
    - Resolved Issues
        N/A

***      
version 1.4.0.1 - 2014.06.16
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-5142	[Stability] STB is out of memory : Blacksreen and/or can't launch applications

***      
version 1.0.31.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-4771 [R3][PROD][DASHBOARD] DAS500-503 dashboard errors should log only on bootup.        
***      
version 1.0.30.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-4771 [R3][PROD][DASHBOARD] DAS500-503 dashboard errors should log only on bootup.        
***      
version 1.0.29.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-4771 [R3][PROD][DASHBOARD] DAS500-503 dashboard errors should log only on bootup.       
***      
version 1.0.28.1
    - New Feature 
        N/A 
    - Resolved Issues 
        + VDTRMASTER-4689 [R3] DAS502 + DAS503 errors (can't contact MS HTTP server)       
***      
version 1.0.27.0
    - New Feature 
        + When info key was pressed, close Widgets and display VOD Flip Bar.
    - Resolved Issues 
        N/A      
***      
version 1.0.26.0
    - New Feature 
        + Applied changed ApplicationConfig module.
    - Resolved Issues 
        N/A
***      
version 1.0.25.1
    - New Feature 
        + modified PreferenceProxy class for receiving preference event properly.
    - Resolved Issues 
        + VDTRSUPPORT-155 The weather widget does not follow the user unit of measure preference. Ex: Preference is Farenheit and the widget continue to display Celcius            
***      
version 1.0.24.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRSUPPORT-155 The weather widget does not follow the user unit of measure preference. Ex: Preference is Farenheit and the widget continue to display Celcius            
***      
version 1.0.23.0
    - New Feature
        + removed tag about measurement id of widget selection.
    - Resolved Issues 
        N/A
***      
version 1.0.22.0
    - New Feature
        + changed VbmController module.(synchronized VBM ID set).
    - Resolved Issues 
        N/A      
***      
version 1.0.21.0
    - New Feature
        + added VBM-group id module.
        + wrote version info to application.prop        
    - Resolved Issues 
        N/A
***      
version 1.0.20.0
    - New Feature
        + modified VBM module.(modified MID-1019000001 and added MID-1019000002)
    - Resolved Issues 
        N/A   
***      
version 1.0.19.0
    - New Feature
        + fixed parsing module of IB data. (StringTokenizer to TextUtil.tokenize)
    - Resolved Issues 
        N/A   
***      
version 1.0.18.0
    - New Feature
        + If vbm parameters are null, then write as blank to vbm.
    - Resolved Issues 
        N/A    
***      
version 1.0.17.0
    - New Feature
        + modified to add "Dashboard" head when widget item selected. (VMB Log) 
    - Resolved Issues 
        N/A      
***      
version 1.0.16.0
    - New Feature 
        + DAS501 is added newly
        + DAS502 is added newly
        + DAS503 is added newly
    - Resolved Issues 
        N/A
***      
version 1.0.15.0
    - New Feature
        + Merged from R2 source.
        + integrated with VBM log.
    - Resolved Issues
        N/A      
*** 
version 1.0.14.0
    - New Feature 
        + Fixed the server information of widget background image.
    - Resolved Issues 
        N/A         
*** 
version 1.0.13.0
    - New Feature 
        + Fixed Fullmenu focus bug.
    - Resolved Issues 
        N/A      
*** 
version 1.0.12.0
    - New Feature 
        + added variable of active animation.
    - Resolved Issues 
        N/A  
*** 
version 1.0.11.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2888 [UI/GUI] iTV - Weather - Favorite city in widget and in application is not keep in memory; it's always "Outremont"      
*** 
version 1.0.10.0
    - New Feature 
        + Requests to flush some images when dashboard hide.
    - Resolved Issues 
        N/A      
*** 
version 1.0.9.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2658 [UI/GUI] [Settings] Wrong pop-up is displayed when we suspend / restore C & L
*** 
version 1.0.8.0
    - New Feature 
        + Fixed dashboard module.
    - Resolved Issues 
        N/A      
*** 
version 1.0.7.0
    - New Feature 
        + Widget images is available in both french & english.
    - Resolved Issues 
        N/A
*** 
version 1.0.6.0
    - New Feature 
        + Fixed this issue (WEATHER Country name is not displayed).
    - Resolved Issues 
        N/A
*** 
version 1.0.5.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2373 DDC 6451 - Weather Widget not functional.
*** 
version 1.0.4.0
    - New Feature 
        + chaged retry look-up millis (100 -> 2000)
    - Resolved Issues 
        N/A
*** 
version 1.0.3.0
    - New Feature 
        + fixed cache module of Weather widget.
        + added background image of D-Option
    - Resolved Issues 
        N/A
*** 
version 1.0.2.0
    - New Feature 
        + Widget images is available in both french & english.
    - Resolved Issues 
        N/A
*** 
version 1.0.1.0
    - New Feature 
        + Rebuild for Framework-Log.
    - Resolved Issues 
        N/A
*** 
version 0.3.36.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3692 [Regression] [Menu] [VT-Menu-302] Missing "Go to Help" in D-Options of the dashboard (widgets)
*** 
version 0.3.35.2
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3602 The Weather widget is not updated if a user changes the "hometown weather" field in the Weather application
        + VDTRMASTER-3599 [REGRESSION] [WIDGET] Buttons are not easily visible when Widget is opened in PPV Channels        
*** 
version 0.3.34.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-3228 Widget update mechanism does not work properly       
*** 
version 0.3.33.0
    - New Feature 
        + Fixed Dashboard-related issue.
    - Resolved Issues 
        N/A     
*** 
version 0.3.32.0
    - New Feature 
        + added variable of active animation.
    - Resolved Issues 
        N/A          
*** 
version 0.3.31.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2888 [UI/GUI] iTV - Weather - Favorite city in widget and in application is not keep in memory; it's always "Outremont"      
*** 
version 0.3.30.0
    - New Feature 
        + Requests to flush some images when dashboard hide.
    - Resolved Issues 
        N/A      
*** 
version 0.3.29.0
    - New Feature 
        + Requests to flush some images when dashboard hide.
    - Resolved Issues 
        N/A
*** 
version 0.3.28.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2658 [UI/GUI] [Settings] Wrong pop-up is displayed when we suspend / restore C & L
*** 
version 0.3.27.0
    - New Feature 
        + Fixed dashboard module.
    - Resolved Issues 
        N/A      
*** 
version 0.3.26.0
    - New Feature 
        + Widget images is available in both french & english.
    - Resolved Issues 
        N/A
*** 
version 0.3.25.0
    - New Feature 
        + Fixed this issue (WEATHER Country name is not displayed).
    - Resolved Issues 
        N/A
*** 
version 0.3.24.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2373 DDC 6451 - Weather Widget not functional.
*** 
version 0.3.23.0
    - New Feature 
        + changed retry look-up millis (100 -> 2000)
    - Resolved Issues 
        N/A
*** 
version 0.3.22.0
    - New Feature 
        + fixed cache module of Weather widget.
    - Resolved Issues 
        N/A
*** 
version 0.3.21.0
    - New Feature 
        + fixed cache module of Weather widget.
    - Resolved Issues 
        N/A
*** 
version 0.3.20.0
    - New Feature 
        + added background image of D-Option
        + fixed cache module of Weather widget.
    - Resolved Issues 
        N/A
*** 
version 0.3.19.1
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-2252 [UI/GUI] [Main Menu] [Video Shooting] Main/mini menu TOC should be updated
*** 
version 0.3.18.0
    - New Feature 
        + Changed app parameter value when dashboard move to settings using by d-option menu. (Roll-back)
    - Resolved Issues 
        N/A
*** 
version 0.3.17.0
    - New Feature 
        + Changed app parameter value when dashboard move to settings using by d-option menu.
    - Resolved Issues 
        N/A
*** 
version 0.3.16.0
    - New Feature 
        + Fixed D-Option bug. (Did not check PIN success)
    - Resolved Issues 
        N/A
*** 
version 0.3.15.1
    - New Feature 
        + Clicking animation(Widget, D-Option)
    - Resolved Issues 
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
*** 
version 0.3.14.0
    - New Feature 
        + Applied modified Framework(OC unload)
    - Resolved Issues 
        N/A
*** 
version 0.3.13.0
    - New Feature 
        + Removed unnecessary configuration fields of dashboard.
    - Resolved Issues 
        N/A
*** 
version 0.3.12.0
    - New Feature 
        + Test version application about weather widget.
    - Resolved Issues 
        N/A
*** 
version 0.3.11
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1733  Unable to scroll while in Main Menu
*** 
version 0.3.10
    - New Feature 
        + chaged IB module. (synchronousLoad -> asynchronousLoad)
    - Resolved Issues 
        N/A 
*** 
version 0.3.9
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-550 [VT] [Menu] There not displayed temperature number 
*** 
version 0.3.8
    - New Feature 
        N/A
    - Resolved Issues 
        + VDTRMASTER-1593  [VT] The 'rewind' function does not execute when launching the dashboard at the 'watchingTV'.
*** 
version 0.3.7
    - New Feature 
        + Applied latest TOC.
    - Resolved Issues 
        N/A        
*** 
version 0.3.6
    - New Feature 
        + Test version (VDTRMASTER-550 [VT] [Menu] There not displayed temperature number)
        + Fixed some gui issues.
    - Resolved Issues 
        + VDTRMASTER-1386  DDC 6451 - Main menu When option Suspend parental control is selected there is screen displayed for 0.5 second.        
*** 
version 0.3.5
    - New Feature 
        + Fixed Stc service module. 
    - Resolved Issues 
        N/A 
*** 
version 0.3.4
    - New Feature 
        + Changed dashboard update modue (IB, OOB). 
    - Resolved Issues 
        N/A  
*** 
version 0.3.3
    - New Feature 
        + Changed footer component in dashboard. 
        + Changed shadow image of widget. 
    - Resolved Issues 
        N/A  
*** 
version 0.3.2
    - New Feature 
        + Added footer component in dashboard. 
    - Resolved Issues 
        N/A  
*** 

-----------------------------------------------------------------------------------
- Release details
  + New feature : applied STC service module.
  + New feature : modified as ms data format(IB, OOB).
- Release name : version 0.3.1
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.23
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Nothing
- Release name : version 0.3.00
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.18
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : VDTRMASTER-899 [VT] Weather Widget item images are displayed later when launch the Fullmenu. (Test version)
- Release name : version 0.2.48
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.17
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : VDTRMASTER-651 [UI/GUI] [MainMenu] - Dashboard: layout has to be reviewed
  + Fixed : fixed some GUI issues.
- Release name : version 0.2.47
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.03.17
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : applied the footer module.
- Release name : version 0.2.46
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.26
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Test version.
- Release name : version 0.2.45
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.26
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Weather widget test version.
- Release name : version 0.2.44
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.22
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed key module.
- Release name : version 0.2.43
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.18
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed weather widget module.
- Release name : version 0.2.42
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.14
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : animation test version.
- Release name : version 0.2.41
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.14
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : animation test version.
- Release name : version 0.2.40
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.14
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed animation module.
- Release name : version 0.2.39
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.14
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed proxy info. (Lab test).
- Release name : version 0.2.38
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.11
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed postal code module.
- Release name : version 0.2.37
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.10
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : added weather widget proxy module logs.
- Release name : version 0.2.36
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.09
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : added weather widget proxy module.
- Release name : version 0.2.35
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.02.07
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Animation test version
- Release name : version 0.2.34
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Fixed D-option module (parental control)
- Release name : version 0.2.33
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version
- Release name : version 0.2.32
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version
- Release name : version 0.2.31
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version
- Release name : version 0.2.30
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Test version
- Release name : version 0.2.29
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Dashboard占쎌쥙猷욑옙?용쐻占쎌늿??Widget占쎌쥙?쏉옙怨쀬굲占쎌쥜?숋옙醫롫솋占쎌늿?뺧옙硫⑤쐻占쎌늿??占쎌쥙猷욑옙?용쐻占쎌늿?뺧옙醫롫짗占쏙옙占쎌쥙?쀯옙癒?굲 占쎌쥙?ι뇡?뱀굲 占쎌쥙猷욑옙?용쐻占쎌늿??- Release name : version 0.2.28
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.19
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Changed channel event module to channel context module.
  + Fixed : Fixed FontResource module.
- Release name : version 0.2.27
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.17
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed server information of widget background image.
- Release name : version 0.2.26
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.07
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed bugs of parental control complete popup.
  + Fixed : added Error code. (Invalid dashboard data)
- Release name : version 0.2.25
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.06
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed GUI (Parental control result popup of D-Option)
- Release name : version 0.2.24
- Release type (official, debug, test,...) : 2nd Pre SAT
- Release date : 2011.01.05
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed GUI (Parental control result popup of D-Option)
- Release name : version 0.2.23
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Dashboard Timer bugs.
  + Fixed : fixed GUI (Parental control result popup of D-Option)
  + Fixed : If you select Activate Parental control of D-option, move to Settings>Parental control.
- Release name : version 0.2.22
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed ADF file.
- Release name : version 0.2.21
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : test D-Option (applied to complete popup)
- Release name : version 0.2.20
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : fixed Registered API Error.
- Release name : version 0.2.19
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : test D-Option
- Release name : version 0.2.18
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.30
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : this problem - Option panel is applied to Dashboard Duration time.
- Release name : version 0.2.17
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.29
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : added Option - Suspend / Disable Parental Control.
- Release name : version 0.2.16
- Release type (official, debug, test,...) : pre-SAT
- Release date : 2010.12.28
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : If a widget is promotion widget, tune to channel (or execute application) for each image.
- Release name : version 0.2.15
- Release type (official, debug, test,...) : test
- Release date : 2010.12.24
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : TOC was applied.
- Release name : version 0.2.14
- Release type (official, debug, test,...) : test
- Release date : 2010.12.24
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : Disable/Enable dashboard display at Power on
  + Fixed : changed square shadow of dashboard to rounded shadow of dashboard.
- Release name : version 0.2.13
- Release type (official, debug, test,...) : test
- Release date : 2010.12.24
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + Fixed : removed the residual image during animation activating.
  + Fixed : added option menu.
- Release name : version 0.2.12
- Release type (official, debug, test,...) : test
- Release date : 2010.12.21
-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
- Release details
  + New feature : added cache manager module.
  + New feature : added error message module.
  + New feature : added dashboard option module.
  + New feature : added mini menu title (Shortcuts Menu).
  + New feature : modified up, down arrow position of dashboard.
- Release name : version 0.2.11
- Release type (official, debug, test,...) : test
- Release date : 2010.12.16
-----------------------------------------------------------------------------------

- Resolved Issues 
version 0.2.10
  + added icon of option and exit.
  + implement D-option.
  + remove videotron logo, added icon of plus.
  + added arrows (up and down) (if promotion data is larger than 1.)


version 0.2.09
  + Correction: fixed to prevent focus moving down in Full Menu

version 0.2.08
  + Correction: fixed slow displaying of Dashboard Widget due to HTTP communication 
  + Correction: fixed miscellaneous bugs 

version 0.2.07
  + fixed Background Image of Dashboard not drawn due to timing issue caused when OOB and OC are combined 
  + Correction: set Max Visible Widget Count of Full Menu Dashboard to 3
  + Correction: fixed position of Dashboard
  + Miscellaneous ? changed OC Data Format 

version 0.2.06
  + Correction: fixed missing of Widget when OOB file is received too late
  + Correction: applied Registered API

version 0.2.05
  + Correction: fixed miscellaneous bugs 

version 0.2.04
  + fixed OOB-OC, Inband-OC update module

version 0.2.03
  + fixed language module
  + fixed OOB-OC, Inband-OC module

version 0.2.02
  + fixed Dashboard GUI

version 0.2.01
  + Initial Release


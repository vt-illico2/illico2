package com.videotron.tvi.illico.dashboard.controller;

import java.io.File;
import java.util.Properties;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.dashboard.Util;
import com.videotron.tvi.illico.log.Log;

public class DashboardDataConverter {
    /** The Constant TEST_PROPERTIES_BYTE_BUFFER. */
    public static final int TEST_PROPERTIES_BYTE_BUFFER = 9000;

    /*****************************************************************
     * methods - Convert-related
     *****************************************************************/
    public static byte[] convertDashboardConfig() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardConfig]start.");
        }
        File fileDashboardListIB = new File("resource/test_data/prop_data/dashboard_config.prop");
        Properties prop = Util.loadProperties(fileDashboardListIB);
        byte[] src = new byte[TEST_PROPERTIES_BYTE_BUFFER];
        int index = 0;
        // Version
        String tempVersion = prop.getProperty("Version");
        byte version = Byte.parseByte(tempVersion);
        src[index++] = version;
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardConfig]version : "+version);
        }
        String tempDashboardInactiveTimeoutSecs = prop.getProperty("DashboardInactiveTimeoutSecs");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardInactiveTimeoutSecs : "+tempDashboardInactiveTimeoutSecs);
        }
        int dashboardInactiveTimeoutSec = 0;
        if (tempDashboardInactiveTimeoutSecs != null) {
            try{
                dashboardInactiveTimeoutSec = Integer.parseInt(tempDashboardInactiveTimeoutSecs);
            }catch(Exception ignore) {
            }
        }
        byte[] dashboardInactiveTimeoutSecs = Util.intTo2Byte(dashboardInactiveTimeoutSec);
        System.arraycopy(dashboardInactiveTimeoutSecs, 0, src, index, 2);
//        // Type Size - Dashboard, Main Menu
//        String tempDashboardIDs = prop.getProperty("DashboardIDs");
//        StringTokenizer st=new StringTokenizer(tempDashboardIDs, ",");
//        int stCount = st.countTokens();
//        byte dashboardTypeSize = (byte)stCount;
//        src[index++] = dashboardTypeSize;
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DashboardDataMgr.convertDashboardConfig]stCount : "+stCount);
//        }
//        for (int i=0; i<stCount; i++) {
//            // Type
//            String tempDashboardID = st.nextToken();
//            byte dashboardID = Byte.parseByte(tempDashboardID);
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]dashboardID-"+i+" : "+dashboardID);
//            }
//            String tempDashboardType = prop.getProperty("DashboardType_"+dashboardID);
//            src[index++] = tempDashboardType.getBytes()[0];
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardType-"+i+" : "+tempDashboardType);
//            }
//            // MaxVisibleWidget
//            String tempDashboardMaxVisibleWidgets = prop.getProperty("DashboardMaxVisibleWidgets_"+dashboardID);
//            src[index++] = Byte.parseByte(tempDashboardMaxVisibleWidgets);
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardMaxVisibleWidgets-"+i+" : "+tempDashboardMaxVisibleWidgets);
//            }
//            String tempDashboardLocationX = prop.getProperty("DashboardLocationX_"+dashboardID);
//            int dashboardLocationXI = 0;
//            if (tempDashboardLocationX != null) {
//                try{
//                    dashboardLocationXI = Integer.parseInt(tempDashboardLocationX);
//                }catch(Exception ignore) {
//                }
//            }
//            byte[] dashboardLocationX = Util.intTo2Byte(dashboardLocationXI);
//            System.arraycopy(dashboardLocationX, 0, src, index, 2);
//            index += 2;
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardLocationX-"+i+" : "+tempDashboardLocationX);
//            }
//            String tempDashboardLocationY = prop.getProperty("DashboardLocationY_"+dashboardID);
//            int dashboardLocationYI = 0;
//            if (tempDashboardLocationY != null) {
//                try{
//                    dashboardLocationYI = Integer.parseInt(tempDashboardLocationY);
//                }catch(Exception ignore) {
//                }
//            }
//            byte[] dashboardLocationY = Util.intTo2Byte(dashboardLocationYI);
//            System.arraycopy(dashboardLocationY, 0, src, index, 2);
//            index += 2;
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardLocationY-"+i+" : "+tempDashboardLocationY);
//            }
//            String tempDashboardLocationH = prop.getProperty("DashboardSizeH_"+dashboardID);
//            int dashboardLocationHI = 0;
//            if (tempDashboardLocationH != null) {
//                try{
//                    dashboardLocationHI = Integer.parseInt(tempDashboardLocationH);
//                }catch(Exception ignore) {
//                }
//            }
//            byte[] dashboardLocationH = Util.intTo2Byte(dashboardLocationHI);
//            System.arraycopy(dashboardLocationH, 0, src, index, 2);
//            index += 2;
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardLocationH-"+i+" : "+tempDashboardLocationH);
//            }
//            String tempDashboardLocationW = prop.getProperty("DashboardSizeW_"+dashboardID);
//            int dashboardLocationWI = 0;
//            if (tempDashboardLocationW != null) {
//                try{
//                    dashboardLocationWI = Integer.parseInt(tempDashboardLocationW);
//                }catch(Exception ignore) {
//                }
//            }
//            byte[] dashboardLocationW = Util.intTo2Byte(dashboardLocationWI);
//            System.arraycopy(dashboardLocationW, 0, src, index, 2);
//            index += 2;
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardLocationW-"+i+" : "+tempDashboardLocationW);
//            }
//            String tempDashboardInactiveTimeoutSecs = prop.getProperty("DashboardInactiveTimeoutSecs_"+dashboardID);
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardInactiveTimeoutSecs-"+i+" : "+tempDashboardInactiveTimeoutSecs);
//            }
//            int dashboardInactiveTimeoutSec = 0;
//            if (tempDashboardInactiveTimeoutSecs != null) {
//                try{
//                    dashboardInactiveTimeoutSec = Integer.parseInt(tempDashboardInactiveTimeoutSecs);
//                }catch(Exception ignore) {
//                }
//            }
//            byte[] dashboardInactiveTimeoutSecs = Util.intTo2Byte(dashboardInactiveTimeoutSec);
//            System.arraycopy(dashboardInactiveTimeoutSecs, 0, src, index, 2);
//            index += 2;
//            String tempDashboardMaxWidgetTypes = prop.getProperty("DashboardMaxWidgetTypes_"+dashboardID);
//            src[index++] = Byte.parseByte(tempDashboardMaxWidgetTypes);
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.convertDashboardConfig]tempDashboardMaxWidgetTypes-"+i+" : "+tempDashboardMaxWidgetTypes);
//            }
//        }
        return src;
    }
    public static byte[] convertDashboardOrder() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardOrder]start.");
        }
        File fileDashboardListOOB = new File("resource/test_data/prop_data/dashboard_ordering.prop");
        Properties prop = Util.loadProperties(fileDashboardListOOB);

        byte[] src = new byte[TEST_PROPERTIES_BYTE_BUFFER];
        int index = 0;
        // Version
        String tempVersion = prop.getProperty("Version");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardOrder]Version : "+tempVersion);
        }
        byte version = 0;
        if (tempVersion != null) {
            version = Byte.parseByte(tempVersion);
        }
        src[index++] = version;

        String tempDashboardTypes = prop.getProperty("DashboardTypes");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertDashboardOrder]tempDashboardTypes : "+tempDashboardTypes);
        }
        StringTokenizer st=new StringTokenizer(tempDashboardTypes, ",");
        int stCount = st.countTokens();
        byte dashboardTypeSize = (byte)stCount;
        src[index++] = dashboardTypeSize;

        for (int i=0; i<stCount; i++) {
            String dashboardType = st.nextToken();
            src[index++] = dashboardType.getBytes()[0];
            String tempDashboardWidgetOrder = prop.getProperty("DashboardWidgetOrder_"+dashboardType);
            StringTokenizer dWOrderST = new StringTokenizer(tempDashboardWidgetOrder, ",");
            int dWOrderSTCount = dWOrderST.countTokens();
            src[index++] = (byte)dWOrderSTCount;
            for (int j=0; j<dWOrderSTCount; j++) {
                String tempDashboardWidgetID = dWOrderST.nextToken();
                int wId = Integer.parseInt(tempDashboardWidgetID);
                byte[] dashboardWidgetID = Util.intTo2Byte(wId);
                System.arraycopy(dashboardWidgetID, 0, src, index, 2);
                index += 2;
            }
        }
        return src;
    }
    public static byte[] convertWidgetList() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertWidgetList]start.");
        }
        File fileWidgetListOOB = new File("resource/test_data/prop_data/widget_list.prop");
        Properties prop = Util.loadProperties(fileWidgetListOOB);
        byte[] src = new byte[TEST_PROPERTIES_BYTE_BUFFER];
        int index = 0;
        // Version
        String tempVersion = prop.getProperty("version");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertWidgetList]Version : "+tempVersion);
        }
        byte version = 0;
        if (tempVersion != null) {
            version = Byte.parseByte(tempVersion);
        }
        src[index++] = version;
        src[index++] = 0;
        //Widget List
        String widgetList = prop.getProperty("widget_list");
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.convertWidgetList]List : "+widgetList);
        }
        StringTokenizer st=new StringTokenizer(widgetList, ",");
        int stCount = st.countTokens();
        byte widgetListSize = (byte)stCount;
        src[index++] = widgetListSize;
        for (int i=0; i<stCount; i++) {
            //Widget ID
            String widgetID = st.nextToken();
            int wId = Integer.parseInt(widgetID);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]ID-"+widgetID+" : "+widgetID);
            }
            byte[] dashboardWidgetID = Util.intTo2Byte(wId);
            System.arraycopy(dashboardWidgetID, 0, src, index, 2);
            index += 2;
            //Widget Name
            String tempWidgetName = prop.getProperty("widget_name_"+widgetID);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Name-"+widgetID+" : "+tempWidgetName);
            }
            byte[] widgetName = tempWidgetName.getBytes();
            int widgetNameLth = widgetName.length;
            src[index++] = (byte) widgetNameLth;
            System.arraycopy(widgetName, 0, src, index, widgetNameLth);
            index += widgetNameLth;
            //Widget Status
            String tempWidgetStatus = prop.getProperty("widget_status_"+widgetID);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Status-"+widgetID+" : "+tempWidgetStatus);
            }
            src[index++] = tempWidgetStatus.getBytes()[0];
            // Possible Contextual State
            String tempPossibleContextualState = prop.getProperty("possible_contextual_state_"+widgetID);
            src[index++] = tempPossibleContextualState.getBytes()[0];
            // Managed Type
            String tempWidgetManagedType = prop.getProperty("managed_type_"+widgetID);
            src[index++] = tempWidgetManagedType.getBytes()[0];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Managed Type-"+i+" : "+tempWidgetManagedType);
            }
            //Widget Cycling Status
            String tempWidgetCyclingStatus = prop.getProperty("cycling_status_"+widgetID);
            byte widgetCyclingStatus = -1;
            if (tempWidgetCyclingStatus != null) {
                try {
                    widgetCyclingStatus = Byte.parseByte(tempWidgetCyclingStatus.trim());
                }catch(Exception e) {
                }
            }
            src[index++] = widgetCyclingStatus;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Cycling Status-"+widgetID+" : "+tempWidgetCyclingStatus);
            }
            //Widget Cycling Time
            String tempWidgetCycleTimeSec = prop.getProperty("cycle_time_"+widgetID);
            src[index++] = Byte.parseByte(tempWidgetCycleTimeSec);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Cycle Sec-"+widgetID+" : "+tempWidgetCycleTimeSec);
            }
            //Server Info
            String tempServerInfo = prop.getProperty("server_info_"+widgetID);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Server Inofo-"+widgetID+" : "+tempServerInfo);
            }
            byte[] serverInfo = tempServerInfo.getBytes();
            int serverInfoLth = serverInfo.length;
            src[index++] = (byte) serverInfoLth;
            System.arraycopy(serverInfo, 0, src, index, serverInfoLth);
            index += serverInfoLth;
            //Proxy list
            String tempProxyListCount = prop.getProperty("proxy_list_count_"+widgetID);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]tempProxyListCount : "+tempProxyListCount);
            }
            int proxyListCount = Integer.parseInt(tempProxyListCount);
            src[index++] = (byte) proxyListCount;
            for (int j=0; j<proxyListCount; j++) {
                String tempProxyHost = prop.getProperty("proxy_host_"+widgetID+"_"+j);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]tempProxyHost-"+j+" : "+tempProxyHost);
                }
                byte[] proxyHost = tempProxyHost.getBytes();
                int proxyHostLth = proxyHost.length;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]proxyHostLth-"+j+" : "+proxyHostLth);
                }
                src[index++] = (byte) proxyHostLth;
                System.arraycopy(proxyHost, 0, src, index, proxyHostLth);
                index += proxyHostLth;
                String tempProxyPort = prop.getProperty("proxy_port_"+widgetID+"_"+j);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]tempProxyPort-"+j+" : "+tempProxyPort);
                }
                int proxyPort = -1;
                if (tempProxyPort != null) {
                    try{
                        proxyPort = Integer.parseInt(tempProxyPort);
                    }catch(Exception ignore) {
                    }
                }
                byte[] proxyPorts = Util.intTo2Byte(proxyPort);
                System.arraycopy(proxyPorts, 0, src, index, 2);
                index += 2;
            }
            //Refresh rate
            String tempWidgetRefreshRateMin = prop.getProperty("refresh_rate_"+widgetID);
            int refreshRateMin = -1;
            if (tempWidgetRefreshRateMin != null) {
                try{
                    refreshRateMin = Integer.parseInt(tempWidgetRefreshRateMin);
                }catch(Exception ignore) {
                }
            }
            byte[] refreshRateMins = Util.intTo2Byte(refreshRateMin);
            System.arraycopy(refreshRateMins, 0, src, index, 2);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Refresh Rate Min-"+widgetID+" : "+tempWidgetRefreshRateMin);
            }
            src[index++] = (byte)2;
            //Widget Image Count
            String tempWidgetImageCount = prop.getProperty("img_list_size_"+widgetID);
            byte widgetImageListCount = Byte.parseByte(tempWidgetImageCount);
            src[index++] = widgetImageListCount;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.convertWidgetList]Image Count-"+widgetID+" : "+tempWidgetImageCount);
            }
            //Widget Image Name
            for (int j=0; j<widgetImageListCount; j++) {
                byte[] en = "en".getBytes();
                System.arraycopy(en, 0, src, index, 2);
                index += 2;
                String tempWidgetImgNameEn = prop.getProperty("img_name_"+widgetID+"_"+j +"_en");
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]Image Name En-"+widgetID+"-"+j+" : "+tempWidgetImgNameEn);
                }
                byte[] widgetImageNameEn = tempWidgetImgNameEn.getBytes();
                int widgetImageNameEnLth = widgetImageNameEn.length;
                src[index++] = (byte) widgetImageNameEnLth;
                System.arraycopy(widgetImageNameEn, 0, src, index, widgetImageNameEnLth);
                index += widgetImageNameEnLth;
                
                byte[] fr = "fr".getBytes();
                System.arraycopy(fr, 0, src, index, 2);
                index += 2;
                String tempWidgetImgNameFr = prop.getProperty("img_name_"+widgetID+"_"+j+"_fr");
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]Image Name Fr-"+widgetID+"-"+j+" : "+tempWidgetImgNameFr);
                }
                byte[] widgetImageNameFr = tempWidgetImgNameFr.getBytes();
                int widgetImageNameFrLth = widgetImageNameFr.length;
                src[index++] = (byte) widgetImageNameFrLth;
                System.arraycopy(widgetImageNameFr, 0, src, index, widgetImageNameFrLth);
                index += widgetImageNameFrLth;
                
                //Widget Selectable Flag
                String tempWidgetSelectableFlag = prop.getProperty("selectable_flag_"+widgetID+"_"+j);
                src[index++] = tempWidgetSelectableFlag.getBytes()[0];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.convertWidgetList]Selectable Flag-"+widgetID+" : "+tempWidgetSelectableFlag);
                }
                if (tempWidgetSelectableFlag.equals("T")) {
                    String tempWidgetTemplateType = prop.getProperty("template_type_"+widgetID+"_"+j);
                    byte widgetTemplateType = Byte.parseByte(tempWidgetTemplateType);
                    src[index++] = widgetTemplateType;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.convertWidgetList]Template Type-"+widgetID+" : "+tempWidgetTemplateType);
                    }
                    switch(widgetTemplateType) {
                        case 2:
                        case 3:
                            String tempWidgetSelectionTargetAction = prop.getProperty("selection_target_action_"+widgetID+"_"+j);
                            src[index++] = tempWidgetSelectionTargetAction.getBytes()[0];
                            if (tempWidgetSelectionTargetAction.equals("C")) {
                                //Call Letter
                                String tempWidgetCallLetter = prop.getProperty("call_letter_"+widgetID+"_"+j);
                                if (Log.DEBUG_ON) {
                                    Log.printDebug("[DashboardDataMgr.convertWidgetList]Call Letter-"+widgetID+" : "+tempWidgetCallLetter);
                                }
                                byte[] widgetCallLetter = tempWidgetCallLetter.getBytes();
                                int widgetCallLetterLth = widgetCallLetter.length;
                                src[index++] = (byte) widgetCallLetterLth;
                                System.arraycopy(widgetCallLetter, 0, src, index, widgetCallLetterLth);
                                index += widgetCallLetterLth;
                            } else if (tempWidgetSelectionTargetAction.equals("A")) {
                                //App Name
                                String tempWidgetAppName = prop.getProperty("app_name_"+widgetID+"_"+j);
                                if (Log.DEBUG_ON) {
                                    Log.printDebug("[DashboardDataMgr.convertWidgetList]App Name-"+widgetID+" : "+tempWidgetAppName);
                                }
                                byte[] widgetAppName = tempWidgetAppName.getBytes();
                                int widgetAppNameLth = widgetAppName.length;
                                src[index++] = (byte) widgetAppNameLth;
                                System.arraycopy(widgetAppName, 0, src, index, widgetAppNameLth);
                                index += widgetAppNameLth;
                                //App Parameter
                                String tempWidgetAppParam = prop.getProperty("app_param_"+widgetID+"_"+j);
                                if (Log.DEBUG_ON) {
                                    Log.printDebug("[DashboardDataMgr.convertWidgetList]App Param-"+widgetID+" : "+tempWidgetAppParam);
                                }
                                byte[] widgetAppParam = tempWidgetAppParam.getBytes();
                                int widgetAppParamLth = widgetAppParam.length;
                                src[index++] = (byte) widgetAppParamLth;
                                System.arraycopy(widgetAppParam, 0, src, index, widgetAppParamLth);
                                index += widgetAppParamLth;
                            }
                            break;
                    }
                }
            }
        }
        return src;
    }
    public static byte[] getBytesITvServer() {
        File fileITvServer = new File("resource/test_data/prop_data/itv_server.prop");
        Properties propITvServer = Util.loadProperties(fileITvServer);
        byte[] bytesITvServer = new byte[TEST_PROPERTIES_BYTE_BUFFER];

        int index = 0;
        //Server type
        String tempVersion = propITvServer.getProperty("version");
        byte version = Byte.parseByte(tempVersion);
        bytesITvServer[index++] = version;
        //Server list size
        String tempServerListSize = propITvServer.getProperty("server_list_size");
        byte serverListSize = Byte.parseByte(tempServerListSize);
        bytesITvServer[index++] = serverListSize;
        for (int i=0; i<serverListSize; i++) {
            //Server type
            String tempServerType = propITvServer.getProperty("server_type_"+i);
            byte serverType = Byte.parseByte(tempServerType);
            bytesITvServer[index++] = serverType;
            //Ip DNS
            String tempIpDns = propITvServer.getProperty("ip_dns_"+i);
            byte[] ipDns = tempIpDns.getBytes();
            int ipDnsLth = ipDns.length;
            bytesITvServer[index++] = (byte) ipDnsLth;
            System.arraycopy(ipDns, 0, bytesITvServer, index, ipDnsLth);
            index += ipDnsLth;
            //Port
            String tempPort = propITvServer.getProperty("port_"+i);
            byte[] port = Util.intTo2Byte(Integer.parseInt(tempPort));
            System.arraycopy(port, 0, bytesITvServer, index, 2);
            index += 2;
            //Context root
            String tempContextRoot = propITvServer.getProperty("context_root_"+i);
            byte[] contextRoot = tempContextRoot.getBytes();
            int contextRootLth = contextRoot.length;
            bytesITvServer[index++] = (byte) contextRootLth;
            System.arraycopy(contextRoot, 0, bytesITvServer, index, contextRootLth);
            index += contextRootLth;
            //Proxy list
            String tempProxyListSize = propITvServer.getProperty("proxy_list_size_"+i);
            byte proxyListSize = Byte.parseByte(tempProxyListSize);
            bytesITvServer[index++] = proxyListSize;
            for (int j=0; j<proxyListSize; j++) {
                //Proxy Ip
                String tempProxyIp = propITvServer.getProperty("proxy_ip_"+i+"_"+j);
                byte[] proxyIp = tempProxyIp.getBytes();
                int proxyIpLth = proxyIp.length;
                bytesITvServer[index++] = (byte) proxyIpLth;
                System.arraycopy(proxyIp, 0, bytesITvServer, index, proxyIpLth);
                index += proxyIpLth;
                //Proxy Port
                String tempProxyPort = propITvServer.getProperty("proxy_port_"+i+"_"+j);
                byte[] proxyPort = Util.intTo2Byte(Integer.parseInt(tempProxyPort));
                System.arraycopy(proxyPort, 0, bytesITvServer, index, 2);
                index += 2;
            }
        }
        return bytesITvServer;
    }
}

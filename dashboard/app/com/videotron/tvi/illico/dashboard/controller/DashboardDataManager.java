package com.videotron.tvi.illico.dashboard.controller;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.Util;
import com.videotron.tvi.illico.dashboard.comp.dashboard.Dashboard;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.comp.widget.WidgetUnit;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

public class DashboardDataManager implements DataUpdateListener {
    /** The instance. */
    private static DashboardDataManager instance;
    private SharedMemory sm = SharedMemory.getInstance();
    private DataCenter dCenter = DataCenter.getInstance();
    /*****************************************************************
     * File Name-related
     *****************************************************************/
    public static final String PROP_KEY_IB_DASHBOARD_CONFIG = "IB_DASHBOARD_LIST";
    public static final String PROP_KEY_IB_WIDGET_IMAGE = "IB_WIDGET_IMAGE";
    public static final String PROP_KEY_IB_ITV_SERVER_INFO = "IB_WIDGET_ITV_SERVER_INFO";
    public static final String PROP_KEY_OOB_WIDGET_LIST = "OOB_WIDGET_LIST";
    public static final String PROP_KEY_OOB_DASHBOARD_ORDER = "OOB_DASHBOARD_LIST";
    /*****************************************************************
     * Parcing Data-related
     *****************************************************************/
    public static final String LANGUAGE_TYPE_ENGLISH = "en";
    public static final String LANGUAGE_TYPE_FRENCH = "fr";

    private static final String WIDGET_STATUS_ALLOW = "Y";
    private static final String WIDGET_STATUS_NOT_ALLOW = "N";

    private static final int WIDGET_CYCLING_YES = 0;
    private static final int WIDGET_CYCLING_NO = 1;

    private static final String WIDGET_TARGET_TYPE_CHANNEL = "C";
    private static final String WIDGET_TARGET_TYPE_APPLICATION = "A";

    private static final String WIDGET_SELECTABLE_FLAG_TRUE = "T";
    private static final String WIDGET_SELECTABLE_FLAG_FALSE = "F";

    private static final String WIDGET_MANAGED_TYPE_ADMIN = "V";
    private static final String WIDGET_MANAGED_TYPE_USER = "U";

    private static final String WIDGET_BINDED_APP_PARAM_SEPARATOR = "|";

    private static final String DASHBOARD_TYPE_FULL_MENU = "F";
    private static final String DASHBOARD_TYPE_DASHBOARD = "D";

    private static final int SERVER_TYPE_LOTTERY = 1;
    private static final int SERVER_TYPE_VOTING = 2;
    private static final int SERVER_TYPE_WEATHER = 3;
    private static final int SERVER_TYPE_CINEMA = 4;

    private ZipFile fileWidgetImageZip;
    /*****************************************************************
     * Parcing-related
     *****************************************************************/
    /** The Constant BYTE_NUMBER_4. */
    public static final int BYTE_NUMBER_4 = 4;
    /** The Constant ZIP_FULE_BYTE_BUFFER. */
    public static final int ZIP_FILE_BYTE_BUFFER = 1024;
    /*****************************************************************
     * Footer Image-related
     *****************************************************************/
    private Hashtable footerHash;

    private byte verDashboardConfig;
    private byte verITvServerInfo;
    private byte verDashboardOrder;
    private byte verWidgetList;
    private boolean isUpdatedWidgetData;

    /**
     * Instantiates a new main data manager.
     */
    private DashboardDataManager() {
    }
    public static synchronized DashboardDataManager getInstance() {
        if (instance == null) {
            instance = new DashboardDataManager();
        }
        return instance;
    }

    /**
     * init MainDataManager.
     */
    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.init]start.");
        }
        isUpdatedWidgetData = false;
        verDashboardConfig = -1;
        verDashboardOrder = -1;
        verWidgetList = -1;
        if (!Rs.INSTEAD_OF_IB_OOB) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.init]use real data.(Inband, OOB)");
            }
            //PROP_KEY_OOB_WIDGET_LIST
            File firstFileWidgetListOOB = (File)dCenter.get(PROP_KEY_OOB_WIDGET_LIST);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.init]firstFileWidgetListOOB : "+firstFileWidgetListOOB);
            }
            if (firstFileWidgetListOOB != null) {
                dataUpdated(PROP_KEY_OOB_WIDGET_LIST, null, firstFileWidgetListOOB);
            }
            dCenter.addDataUpdateListener(PROP_KEY_OOB_WIDGET_LIST, this);
            //PROP_KEY_OOB_DASHBOARD_LIST
            File firstFileDashboardOOB = (File)dCenter.get(PROP_KEY_OOB_DASHBOARD_ORDER);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.init]firstFileDashboardOOB : "+firstFileDashboardOOB);
            }
            if (firstFileDashboardOOB != null) {
                dataUpdated(PROP_KEY_OOB_DASHBOARD_ORDER, null, firstFileDashboardOOB);
            }
            dCenter.addDataUpdateListener(PROP_KEY_OOB_DASHBOARD_ORDER, this);
            dCenter.addDataUpdateListener(PROP_KEY_IB_DASHBOARD_CONFIG, this);
            dCenter.addDataUpdateListener(PROP_KEY_IB_ITV_SERVER_INFO, this);
            dCenter.addDataUpdateListener(PROP_KEY_IB_WIDGET_IMAGE, this);
        } else {
            if (Rs.USE_PROP_DATA_INSTEAD_OF_IB_OOB) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.init]Emulator - Properties.");
                }
                byte[] bytesDashboardListIB = DashboardDataConverter.convertDashboardConfig();
                dataUpdatedByByteArray(PROP_KEY_IB_DASHBOARD_CONFIG, bytesDashboardListIB);

                byte[] bytesITvServerIB = DashboardDataConverter.getBytesITvServer();
                dataUpdatedByByteArray(PROP_KEY_IB_ITV_SERVER_INFO, bytesITvServerIB);

                byte[] bytesDashboardListOOB = DashboardDataConverter.convertDashboardOrder();
                dataUpdatedByByteArray(PROP_KEY_OOB_DASHBOARD_ORDER, bytesDashboardListOOB);

                byte[] bytesWidgetListOOB = DashboardDataConverter.convertWidgetList();
                dataUpdatedByByteArray(PROP_KEY_OOB_WIDGET_LIST, bytesWidgetListOOB);

                File fileWidgetImageZip = new File("resource/test_data/prop_data/test_widget_images_ib.zip");
                dataUpdated(PROP_KEY_IB_WIDGET_IMAGE, null, fileWidgetImageZip);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.init]Emulator - MS Src.");
                }
                File fileDashboardListIB = new File("resource/test_data/ms_data/dashboard_config_ib.txt");
                dataUpdated(PROP_KEY_IB_DASHBOARD_CONFIG, null, fileDashboardListIB);

                File fileITvServer = new File("resource/test_data/ms_data/itv_server.txt");
                dataUpdated(PROP_KEY_IB_ITV_SERVER_INFO, null, fileITvServer);

                File fileDashboardListOOB = new File("resource/test_data/ms_data/dashboard_config_oob.txt");
                dataUpdated(PROP_KEY_OOB_DASHBOARD_ORDER, null, fileDashboardListOOB);

                File fileWidgetListOOB = new File("resource/test_data/ms_data/widget_list.txt");
                dataUpdated(PROP_KEY_OOB_WIDGET_LIST, null, fileWidgetListOOB);

                File fileWidgetImageZip = new File("resource/test_data/ms_data/widget_images.zip");
                dataUpdated(PROP_KEY_IB_WIDGET_IMAGE, null, fileWidgetImageZip);
                
//                new Thread() {
//                    public void run() {
//                        try{
//                            Thread.sleep(60000);
//                        }catch(InterruptedException ie) {
//                            ie.printStackTrace();
//                        }
//                        byte[] bytesDashboardListIB = DashboardDataConverter.convertDashboardConfig();
//                        dataUpdatedByByteArray(PROP_KEY_IB_DASHBOARD_CONFIG, bytesDashboardListIB);
//
//                        byte[] bytesITvServerIB = DashboardDataConverter.getBytesITvServer();
//                        dataUpdatedByByteArray(PROP_KEY_IB_ITV_SERVER_INFO, bytesITvServerIB);
//
//                        byte[] bytesDashboardListOOB = DashboardDataConverter.convertDashboardOrder();
//                        dataUpdatedByByteArray(PROP_KEY_OOB_DASHBOARD_ORDER, bytesDashboardListOOB);
//
//                        byte[] bytesWidgetListOOB = DashboardDataConverter.convertWidgetList();
//                        dataUpdatedByByteArray(PROP_KEY_OOB_WIDGET_LIST, bytesWidgetListOOB);
//                    }
//                }.start();
            }
        }
    }

    /**
     * Dispose MainDataManager.
     */
    public void dispose() {
        instance = null;
        fileWidgetImageZip = null;
        if (fileWidgetImageZip != null) {
            try{
                fileWidgetImageZip.close();
            }catch(Exception ignore) {
            }
            fileWidgetImageZip = null;
        }
    }
    /**
     * Start MainDataManager.
     */
    public void start() {
    }
    /**
     * Stop MainDataManager.
     */
    public void stop() {
    }
    /*****************************************************************
     * methods - Parse-related
     *****************************************************************/
    private boolean parseDashboardConfig(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.parseDashboardConfig]Start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            return false;
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseDashboardConfig]Version : " + version);
            }
            // Dashboard Type Size
//            int typeSize = src[index++];
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardDataMgr.parseDashboardConfig]Type size : " + typeSize);
//            }
            Hashtable dashboardConfigListHash  = new Hashtable();
            Hashtable dConfHashFm = new Hashtable();
            dConfHashFm.put(Dashboard.SMKEY_DASHBOARD_TYPE, new Integer(Dashboard.DASHBOARD_TYPE_FULL_MENU));
            dashboardConfigListHash.put(Dashboard.SMKEY_DASHBOARD_TAG + Dashboard.DASHBOARD_TYPE_FULL_MENU, dConfHashFm);

            Hashtable dConfHashDashboard = new Hashtable();
            dConfHashDashboard.put(Dashboard.SMKEY_DASHBOARD_TYPE, new Integer(Dashboard.DASHBOARD_TYPE_DASHBOARD));
            int inactivityTimeoutSecs = Util.twoBytesToInt(src, index, true);
            index += 2;
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseDashboardConfig]Inactivity timeout secs : "
                        + inactivityTimeoutSecs);
            }
            dConfHashDashboard
                    .put(Dashboard.SMKEY_DASHBOARD_INACTIVE_TIMEOUT_SECS, new Integer(inactivityTimeoutSecs));
            dashboardConfigListHash.put(Dashboard.SMKEY_DASHBOARD_TAG + Dashboard.DASHBOARD_TYPE_DASHBOARD, dConfHashDashboard);

//            for (int i = 0; i < typeSize; i++) {
//                // Dashboard Type
//                String tempType = new String(src, index++, 1);
//                int type = -1;
//                if (tempType.equals(DASHBOARD_TYPE_FULL_MENU)) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Type-Full menu dashboard.");
//                    }
//                    type = Dashboard.DASHBOARD_TYPE_FULL_MENU;
//                } else if (tempType.equals(DASHBOARD_TYPE_DASHBOARD)) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Type-General dashboard.");
//                    }
//                    type = Dashboard.DASHBOARD_TYPE_DASHBOARD;
//                }
//                // Dashboard Max Visible Widget Count
//                int maxVisibleWidgetCount = src[index++];
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Max visible widget count-" + i + " : " + maxVisibleWidgetCount);
//                }
//                // Dashboard Location - X
//                int locationX = Util.twoBytesToInt(src, index, true);
//                index += 2;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Location X-" + i + " : " + locationX);
//                }
//                // Dashboard Location - Y
//                int locationY = Util.twoBytesToInt(src, index, true);
//                index += 2;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Location Y-" + i + " : " + locationY);
//                }
//                // Dashboard Location - H
//                int sizeH = Util.twoBytesToInt(src, index, true);
//                index += 2;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Size H-" + i + " : " + sizeH);
//                }
//                // Dashboard Location - W
//                int sizeW = Util.twoBytesToInt(src, index, true);
//                index += 2;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Size W-" + i + " : " + sizeW);
//                }
//                // Dashboard Inactivity Timeout Secs
//                int inactivityTimeoutSecs = Util.twoBytesToInt(src, index, true);
//                index += 2;
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Inactivity timeout secs-" + i + " : " + inactivityTimeoutSecs);
//                }
//                // Dashboard Max Widget Types
//                int maxWidgetTypes = src[index++];
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[DashboardDataMgr.parseDashboardConfig]["+tempType+"]Max widget types-" + i + " : " + maxWidgetTypes);
//                }
//                Hashtable dashboardConfigHash = new Hashtable();
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_TYPE, new Integer(type));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_MAX_VISIBLE_WIDGET_COUNT, new Integer(
//                        maxVisibleWidgetCount));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_LOCATION_X, new Integer(locationX));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_LOCATION_Y, new Integer(locationY));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_SIZE_H, new Integer(sizeH));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_SIZE_W, new Integer(sizeW));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_INACTIVE_TIMEOUT_SECS, new Integer(
//                        inactivityTimeoutSecs));
//                dashboardConfigHash.put(Dashboard.SMKEY_DASHBOARD_MAX_WIDGET_TYPES, new Integer(maxWidgetTypes));
//                dashboardConfigListHash.put(Dashboard.SMKEY_DASHBOARD_TAG + type, dashboardConfigHash);
//            }
            if (sm.get(Dashboard.SMKEY_DASHBOARD_IB_CONFIG_LIST) != null) {
                sm.remove(Dashboard.SMKEY_DASHBOARD_IB_CONFIG_LIST);
            }
            sm.put(Dashboard.SMKEY_DASHBOARD_IB_CONFIG_LIST, dashboardConfigListHash);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            return false;
        }
        return true;
    }
    private boolean parseDashboardOrder(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.parseDashboardOrder]Start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            return false;
        }
        try {
            int index = 0;
            // Version
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseDashboardOrder]Version : " + version);
            }
            // Dashboard Type Size
            int typeSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseDashboardOrder]Type size : " + typeSize);
            }
            Hashtable tempDashboardOrderListHash = new Hashtable();
            for (int i = 0; i < typeSize; i++) {
                // Dashboard Type
                String tempType = new String(src, index++, 1);
                int type = -1;
                if (tempType.equals(DASHBOARD_TYPE_FULL_MENU)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseDashboardOrder]["+tempType+"]Type-Full menu dashboard.");
                    }
                    type = Dashboard.DASHBOARD_TYPE_FULL_MENU;
                } else if (tempType.equals(DASHBOARD_TYPE_DASHBOARD)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseDashboardOrder]["+tempType+"]Type-General dashboard.");
                    }
                    type = Dashboard.DASHBOARD_TYPE_DASHBOARD;
                }
                int widgetSize = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseDashboardOrder]["+tempType+"]Widget size-" + i + " : " + widgetSize);
                }
                String[] widgetIds = new String[widgetSize];
                for (int j=0; j<widgetSize; j++) {
                    int widgetId = Util.twoBytesToInt(src, index, true);
                    index += 2;
                    widgetIds[j]=String.valueOf(widgetId);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseDashboardOrder]["+tempType+"]Widget Id-" + i + " - "+j+" : " + widgetIds[j]);
                    }
                }
                tempDashboardOrderListHash.put(Dashboard.SMKEY_WIDGET_ORDERS + type, widgetIds);
            }
            if (sm.get(Dashboard.SMKEY_DASHBOARD_OOB_ORDER_LIST) != null) {
                sm.remove(Dashboard.SMKEY_DASHBOARD_OOB_ORDER_LIST);
            }
            sm.put(Dashboard.SMKEY_DASHBOARD_OOB_ORDER_LIST, tempDashboardOrderListHash);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            return false;
        }
        return true;
    }
    private boolean parseWidgetList(byte[] src) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.parseWidgetList]start.");
        }
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            return false;
        }
        try {
            int index = 0;
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseWidgetList]Version : " + version);
            }
            // URL
            int urlLth = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseWidgetList]URL Length : " + urlLth);
            }
            String url = new String(src, index, urlLth);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseWidgetList]URL : " + url);
            }
            index += urlLth;
            // Widget List Size
            int listSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseWidgetList]Widget list size : " + listSize);
            }
            Hashtable tempWidgetListHash = new Hashtable();
            for (int i = 0; i < listSize; i++) {
                // Widget ID
                int id = Util.twoBytesToInt(src, index, true);
                index += 2;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Widget ID-" + i + " : " + id);
                }
                // Widget Name
                int nameLth = src[index++];
                String name = new String(src, index, nameLth);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Name-" + i + " : " + name);
                }
                index += nameLth;
                // Widget Status
                String status = new String(src, index++, 1);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Status-" + i + " : " + status);
                }
                boolean isAllowRunning = false;
                if (status != null) {
                    if (status.equals(WIDGET_STATUS_ALLOW)) {
                        isAllowRunning = true;
                    } else if (status.equals(WIDGET_STATUS_NOT_ALLOW)) {
                        isAllowRunning = false;
                    }
                }
                // Possible Contextual State
                String possibleContextualState = new String(src, index++, 1);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Possible Contextual State-" + i + " : " + possibleContextualState);
                }
                // Managed Type
                String tempManagedType = new String(src, index++, 1);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Managed Type-" + i + " : " + tempManagedType);
                }
                int managedType = -1;
                if (tempManagedType != null) {
                    if (tempManagedType.equals(WIDGET_MANAGED_TYPE_ADMIN)) {
                        managedType = Widget.WIDGET_MANAGED_TYPE_ADMIN;
                    } else if (tempManagedType.equals(WIDGET_MANAGED_TYPE_USER)) {
                        managedType = Widget.WIDGET_MANAGED_TYPE_USER;
                    }
                }
                // Widget Cycling Status
                int cyclingStatus = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]CyclingStatus-" + i + " : " + cyclingStatus);
                }
                boolean isCyclable = false;
                if (cyclingStatus == WIDGET_CYCLING_YES) {
                    isCyclable = true;
                } else if (cyclingStatus == WIDGET_CYCLING_NO) {
                    isCyclable = false;
                }
                // Widget Cycle Time Sec
                int cycleTimeSecs = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Cycle time secs-" + i + " : " + cycleTimeSecs);
                }
                //Servivce Ip - Do not user (2011.11.18)
                int serverInfoLth = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]ServerInfoLth-" + i + " : " + serverInfoLth);
                }
                String serverInfo = new String(src, index, serverInfoLth);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Server Info-" + i + " : " + serverInfo);
                }
                index += serverInfoLth;
                //Proxy Ip, Port - Do not user (2011.11.18)
                int proxyCount = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Proxy count-" + i + " : " + proxyCount);
                }
                String[] proxyIPs = new String[proxyCount];
                int[] proxyPorts = new int[proxyCount];
                for (int j=0; j<proxyCount; j++) {
                    int proxyIPLth = src[index++];
                    proxyIPs[j] = new String(src, index, proxyIPLth);
                    index += proxyIPLth;
                    proxyPorts[j] = Util.twoBytesToInt(src, index, true);
                    index += 2;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Proxy host length-" + j + " : " + proxyIPLth);
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Proxy host-" + j + " : " + proxyIPs[j]);
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Proxy port-" + j + " : " + proxyPorts[j]);
                    }
                }
                // Widget Refresh Rate
                int refreshRateMins = Util.twoBytesToInt(src, index, true);
                index += 2;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Refresh rate mins-" + i + " : " + refreshRateMins);
                }
                // Languge type size
                int langTypeCount = src[index++];
                // Widget Image List Size
                int imageCount = src[index++];
                WidgetUnit [] widgetUnits = new WidgetUnit[imageCount];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Image count-" + i + " : " + imageCount);
                }
                for (int j=0; j<widgetUnits.length; j++) {
                    String imgNameEn = null;
                    String imgNameFr = null;
                    for (int k = 0; k<langTypeCount; k++) {
                        String langType = new String(src, index, 2);
                        if (langType != null) {
                            langType = langType.trim();
                        }
                        index += 2;
                        int imageNameLth = src[index++];
                        String imgName = new String(src, index, imageNameLth);
                        index += imageNameLth;
                        if (langType.equals(LANGUAGE_TYPE_ENGLISH)) {
                            imgNameEn = imgName;
                        } else if (langType.equals(LANGUAGE_TYPE_FRENCH)) {
                            imgNameFr = imgName;
                        }
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Image name en[" + i + "-"+j+"] : " + imgNameEn);
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Image name fr[" + i + "-"+j+"] : " + imgNameFr);
                    }

                    // Widget Selectable Flag
                    String tempSelectableFlag = new String(src, index++, 1);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Selectable flag[" + i + "-"+j+"] : " + tempSelectableFlag);
                    }
                    boolean isSelectable = false;
                    if (tempSelectableFlag != null) {
                        if (tempSelectableFlag.equals(WIDGET_SELECTABLE_FLAG_TRUE)) {
                            isSelectable = true;
                        } else if (tempSelectableFlag.equals(WIDGET_SELECTABLE_FLAG_FALSE)) {
                            isSelectable = false;
                        }
                    }
                    int templateType = -1;
                    int targetType = -1;
                    String callLetter = null;
                    String appName = null;
                    String[] appParams = null;
                    if (isSelectable) {
                        // Widget Template Type
                        templateType = src[index++];
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Template type[" + i + "-"+j+"] : " + templateType);
                        }
                        switch(templateType) {
                            case Widget.WIDGET_TEMPLATE_TYPE_PROMOTION:
                            case Widget.WIDGET_TEMPLATE_TYPE_INTERACTIVE:
                                String tempTargetType = new String(src, index++, 1);
                                if (Log.DEBUG_ON) {
                                    Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Temp target type[" + i + "-"+j+"] : " + tempTargetType);
                                }
                                if (tempTargetType != null) {
                                    if (tempTargetType.equals(WIDGET_TARGET_TYPE_CHANNEL)) {
                                        targetType = Widget.WIDGET_TARGET_TYPE_CHANNEL;
                                        int callLetterLth = src[index++];
                                        callLetter = new String(src, index, callLetterLth);
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]Call letter[" + i + "-"+j+"] : " + callLetter);
                                        }
                                        index += callLetterLth;
                                    } else if (tempTargetType.equals(WIDGET_TARGET_TYPE_APPLICATION)) {
                                        targetType = Widget.WIDGET_TARGET_TYPE_APPLICATION;
                                        int appNameLth = src[index++];
                                        appName = new String(src, index, appNameLth);
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]App name[" + i + "-"+j+"] : " + appName);
                                        }
                                        index += appNameLth;
                                        int appParamLth = src[index++];
                                        String appParam = new String(src, index, appParamLth);
                                        if (Log.DEBUG_ON) {
                                            Log.printDebug("[DashboardDataMgr.parseWidgetList]["+id+"]App param[" + i + "-"+j+"] : " + appParam);
                                        }
                                        index += appParamLth;
                                        appParams = TextUtil.tokenize(appParam, WIDGET_BINDED_APP_PARAM_SEPARATOR);
                                    }
                                }
                                break;
                        }
                    }
                    widgetUnits[j] = new WidgetUnit();
                    widgetUnits[j].setImageNameEnglish(imgNameEn);
                    widgetUnits[j].setImageNameFrench(imgNameFr);
                    widgetUnits[j].setSelectable(isSelectable);
                    widgetUnits[j].setTemplateType(templateType);
                    widgetUnits[j].setTargetAction(targetType);
                    widgetUnits[j].setTargetCallLetter(callLetter);
                    widgetUnits[j].setTargetAppName(appName);
                    widgetUnits[j].setTargetAppParamter(appParams);
                }
                if (isAllowRunning) {
                    Hashtable widgetHash = new Hashtable();
                    widgetHash.put(Widget.SMKEY_WIDGET_ID, String.valueOf(id));
                    //Possible contextual state
                    if (possibleContextualState != null) {
                        widgetHash.put(Widget.SMKEY_WIDGET_POSSIBLE_CONTEXTUAL_STATE, possibleContextualState);
                    }
                    //Name
                    if (name != null) {
                        widgetHash.put(Widget.SMKEY_WIDGET_NAME, name);
                    }
                    //Managed type
                    widgetHash.put(Widget.SMKEY_WIDGET_MANAGED_TYPE, new Integer(managedType));
                    //Image count
                    widgetHash.put(Widget.SMKEY_WIDGET_IMAGE_COUNT, new Integer(imageCount));
                    //Refersh rate secs
                    widgetHash.put(Widget.SMKEY_WIDGET_REFRESH_RATE_MINS, new Integer(refreshRateMins));
                    //Cyclable
                    widgetHash.put(Widget.SMKEY_WIDGET_IS_CYCLABLE, new Boolean(isCyclable));
                    //Cycle time secs
                    widgetHash.put(Widget.SMKEY_WIDGET_CYCLE_TIME_SECS, new Integer(cycleTimeSecs));
                    //Server info - Do not user anymore (2011.11.18)
//                    if (serverInfo != null) {
//                        widgetHash.put(Widget.SMKEY_WIDGET_BG_SERVER_INFO, serverInfo);
//                    }
//                    if (proxyIPs != null) {
//                        widgetHash.put(Widget.SMKEY_WIDGET_BG_PROXY_IP_LIST, proxyIPs);
//                    }
//                    if (proxyPorts != null) {
//                        widgetHash.put(Widget.SMKEY_WIDGET_BG_PROXY_PORT_LIST, proxyPorts);
//                    }
                    if (url != null) {
                        widgetHash.put(Widget.SMKEY_WIDGET_BG_SERVER_INFO, url);
                    }
                    //Image names
                    Vector unitPkVec = new Vector();
                    for (int j=0; j<widgetUnits.length; j++) {
                        WidgetUnit wUnit = widgetUnits[j];
                        if (wUnit == null) {
                            continue;
                        }
                        String imgNameEn = wUnit.getImageNameEnglish();
                        if (imgNameEn == null) {
                            continue;
                        }
                        widgetHash.put(Widget.SMKEY_WIDGET_IMAGE_NAME_ENGLISH+imgNameEn, imgNameEn);
                        unitPkVec.addElement(imgNameEn);
                        String imgNameFr = wUnit.getImageNameFrench();
                        if (imgNameFr == null) {
                            imgNameFr = "";
                        }
                        widgetHash.put(Widget.SMKEY_WIDGET_IMAGE_NAME_FRENCH+imgNameEn, imgNameFr);
                        boolean isSelectable = wUnit.isSelectable();
                        //Selectable
                        widgetHash.put(Widget.SMKEY_WIDGET_IS_SELECTABLE+imgNameEn, new Boolean(isSelectable));
                        //Template type
                        int templateType = wUnit.getTemplateType();
                        widgetHash.put(Widget.SMKEY_WIDGET_TEMPLATE_TYPE+imgNameEn, new Integer(templateType));
                        //Target action
                        int targetAction = wUnit.getTargetAction();
                        widgetHash.put(Widget.SMKEY_WIDGET_TARGET_TYPE+imgNameEn, new Integer(targetAction));
                        //Target call letter
                        String targetCallLetter = wUnit.getTargetCallLetter();
                        if (targetCallLetter != null) {
                            widgetHash.put(Widget.SMKEY_WIDGET_CALL_LETTER+imgNameEn, targetCallLetter);
                        }
                        //Target app name
                        String targetAppName = wUnit.getTargetAppName();
                        if (targetAppName != null) {
                            widgetHash.put(Widget.SMKEY_WIDGET_BINDED_APPLICATION_NAME+imgNameEn, targetAppName);
                        }
                        //Target app parameter
                        String[] targetAppParamter = wUnit.getTargetAppParamter();
                        if (targetAppParamter != null) {
                            widgetHash.put(Widget.SMKEY_WIDGET_BINDED_APPLICATION_PARAMS+imgNameEn, targetAppParamter);
                        }
                    }
                    int unitPkVecSize = unitPkVec.size();
                    String[] unitPkList = new String[unitPkVecSize];
                    unitPkList = (String[])unitPkVec.toArray(unitPkList);
                    widgetHash.put(Widget.SMKEY_WIDGET_UNIT_PRIMARY_KEY_LIST, unitPkList);
                    tempWidgetListHash.put(Widget.SMKEY_WIDGET_TAG_DATA+id, widgetHash);
                }
            }
            if (sm.get(Widget.SMKEY_WIDGET_LIST) != null) {
                sm.remove(Widget.SMKEY_WIDGET_LIST);
            }
            sm.put(Widget.SMKEY_WIDGET_LIST, tempWidgetListHash);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            return false;
        }
        return true;
    }
    private boolean parseITvServerData(byte[] src) {
        if (src == null || src.length == 0) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_RECEIPT_PROBLEM+"] Cannot get the required configuration files from the Management System.");
            }
            return false;
        }
        try {
            int index = 0;
            //Version
            int version = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseITVServerData]version : " + version);
            }
            //Server list size
            int serverListSize = src[index++];
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.parseITVServerData]Server list size : " + serverListSize);
            }
            Hashtable widgetServerListHash = new Hashtable();
            for (int i=0; i<serverListSize; i++) {
                Hashtable widgetServerHash = new Hashtable();
                //Server type
                int serverType = src[index++];
                //IpDns
                int ipDnsLth = src[index++];
                String ipDns = new String(src, index, ipDnsLth, "ISO8859_1");
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Host : "+ipDns);
                }
                index += ipDnsLth;
                widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_HOST, ipDns);
                //Port
                int port = Util.twoBytesToInt(src, index, true);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Port : "+port);
                }
                index += 2;
                widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_PORT, new Integer(port));
                //Context root
                int contextRootLth = src[index++];
                String contextRoot = new String(src, index, contextRootLth, "ISO8859_1");
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Context root : "+contextRoot);
                }
                index += contextRootLth;
                widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_CONTEXT_ROOT, contextRoot);
                //Proxy list size
                int proxyListSize = src[index++];
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Proxy count : "+proxyListSize);
                }
                widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_PROXY_COUNT, new Integer(proxyListSize));
                for (int j=0; j<proxyListSize; j++) {
                    //Proxy ip
                    int proxyIpLth = src[index++];
                    String proxyIp = new String(src, index, proxyIpLth, "ISO8859_1");
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Proxy host - "+j+" : "+proxyIp);
                    }
                    index += proxyIpLth;
                    widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_PROXY_HOST + j, proxyIp);
                    //Proxy port
                    int proxyPort = Util.twoBytesToInt(src, index, true);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[DashboardDataMgr.parseITVServerData]["+serverType+"]Proxy port - "+j+" : "+proxyPort);
                    }
                    index += 2;
                    widgetServerHash.put(Widget.SMKEY_WIDGET_SERVER_PROXY_PORT + j, new Integer(proxyPort));
                }
                String widgetTypeSMKey = null;
                switch(serverType) {
                    case SERVER_TYPE_LOTTERY:
                        widgetTypeSMKey = Widget.WIDGET_TYPE_LOTTERY;
                        break;
                    case SERVER_TYPE_VOTING:
                        break;
                    case SERVER_TYPE_WEATHER:
                        widgetTypeSMKey = Widget.WIDGET_TYPE_WEATHER;
                        break;
                    case SERVER_TYPE_CINEMA:
                        break;
                }
                if (widgetTypeSMKey != null) {
                    widgetServerListHash.put(widgetTypeSMKey, widgetServerHash);
                }
            }
            if (sm.get(Widget.SMKEY_WIDGET_SERVER_LIST) != null) {
                sm.remove(Widget.SMKEY_WIDGET_SERVER_LIST);
            }
            sm.put(Widget.SMKEY_WIDGET_SERVER_LIST, widgetServerListHash);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            return false;
        }
        return true;
    }
    private boolean createWidgetImages(final ZipFile zipFileWidgetImage) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.createWidgetImages]Start.");
        }
        if (zipFileWidgetImage == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.createWidgetImages]Widget images are not ready.");
            }
            return false;
        }
        //flush Before Images
        Hashtable hashBeforeWidgetBgSrc = (Hashtable)sm.get(Widget.SMKEY_BG_IMAGE_SRC_LIST);
        if (hashBeforeWidgetBgSrc != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.createWidgetImages]Flush before image source.");
            }
            sm.remove(Widget.SMKEY_BG_IMAGE_SRC_LIST);
        }
        
        //craete Zip File Images
        Enumeration enu = zipFileWidgetImage.entries();
        Hashtable hashWidgetBgSrc = new Hashtable();
        while(enu.hasMoreElements()) {
            ZipEntry entry = (ZipEntry)enu.nextElement();
            if (entry == null) {
                continue;
            }
            String name = entry.getName();
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.createWidgetImages]Request put image source. name : "+name);
            }
            if (name == null) {
                continue;
            }
            byte[] srcWidgetImg = Util.getBytesFromZip(zipFileWidgetImage, name);
            if (srcWidgetImg == null) {
                continue;
            }
            hashWidgetBgSrc.put(name, srcWidgetImg);
        }
        sm.put(Widget.SMKEY_BG_IMAGE_SRC_LIST, hashWidgetBgSrc);
        return true;
    }
    /*****************************************************************
     * Methods - Footer Image-related
     *****************************************************************/
    public Image getFooterImage(String btKey) {
        if (btKey == null) {
            return null;
        }
        if (footerHash == null) {
            footerHash = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        }
        Image imgFooter = null;
        if (footerHash != null) {
            imgFooter = (Image) footerHash.get(btKey);
        }
        return imgFooter;
    }
    /*****************************************************************
     * methods - DataUpdateListener-implemented
     *****************************************************************/
    public void dataRemoved(String key) {
    }
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardDataMgr.dataUpdated]Key : " + key);
            Log.printDebug("[DashboardDataMgr.dataUpdated]Old : " + old);
            Log.printDebug("[DashboardDataMgr.dataUpdated]Value : " + value);
        }
        if (key == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Updated key is null. return.");
            }
            return;
        }
        if (value == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Updated value is null. return.");
            }
            return;
        }
        if (key.equals(PROP_KEY_IB_DASHBOARD_CONFIG)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Received dashboard config data.");
            }
            File fileDashboardIB = (File) value;
            dataUpdatedByByteArray(PROP_KEY_IB_DASHBOARD_CONFIG, getByteArrayFromFile(fileDashboardIB));
        } else if (key.equals(PROP_KEY_IB_ITV_SERVER_INFO)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Received itv server info data.");
            }
            File fileIBItvServerInfo = (File) value;
            dataUpdatedByByteArray(PROP_KEY_IB_ITV_SERVER_INFO, getByteArrayFromFile(fileIBItvServerInfo));
        } else if (key.equals(PROP_KEY_OOB_DASHBOARD_ORDER)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Received dashboard order.");
            }
            File fileDashboardOOB = (File) value;
            dataUpdatedByByteArray(PROP_KEY_OOB_DASHBOARD_ORDER, getByteArrayFromFile(fileDashboardOOB));
        } else if (key.equals(PROP_KEY_OOB_WIDGET_LIST)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Received widget list.");
            }
            File fileWidgetListOOB = (File) value;
            dataUpdatedByByteArray(PROP_KEY_OOB_WIDGET_LIST, getByteArrayFromFile(fileWidgetListOOB));
        } else if (key.equals(PROP_KEY_IB_WIDGET_IMAGE)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Received widget image.");
            }
            File tempFileWidgetListImage = (File) value;
            // create Normal icon zip file.
            try {
                fileWidgetImageZip = new ZipFile(tempFileWidgetListImage);
            } catch (Exception e) {
                Log.print(e);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]is updated widget data : "+isUpdatedWidgetData);
            }
            if (isUpdatedWidgetData) {
                boolean createdWidgetImageList = createWidgetImages(fileWidgetImageZip);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Is created widget image : "+createdWidgetImageList);
                }
                if (createdWidgetImageList) {
                    isUpdatedWidgetData = false;
                    fileWidgetImageZip = null;
                }
            }
        }
    }
    private void dataUpdatedByByteArray(String key, byte[] src) {
        if (key.equals(PROP_KEY_IB_DASHBOARD_CONFIG)) {
            if (src == null || src.length <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Dashboard config byte array is null.");
                }
                return;
            }
            byte ver = src[0];
            if (ver == verDashboardConfig) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Current dashboard config is latest data.");
                }
                return;
            }
            boolean isParsedDashboardConfig = parseDashboardConfig(src);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Is parsed dashboard config : "+isParsedDashboardConfig);
            }
            if (isParsedDashboardConfig) {
                verDashboardConfig = ver;
            }
        } else if (key.equals(PROP_KEY_IB_ITV_SERVER_INFO)) {
            if (src == null || src.length <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]ITv server info byte array is null.");
                }
                return;
            }
            byte ver = src[0];
            if (ver == verITvServerInfo) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Current ITv server info is latest data.");
                }
                return;
            }
            boolean isParsedITvServerData = parseITvServerData(src);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Is parsed ITv server info : "+isParsedITvServerData);
            }
            if (isParsedITvServerData) {
                verITvServerInfo = ver;
            }
        } else if (key.equals(PROP_KEY_OOB_DASHBOARD_ORDER)) {
            if (src == null || src.length <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Dashboard order byte array is null.");
                }
                return;
            }
            byte ver = src[0];
            if (ver == verDashboardOrder) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Current dashboard order is latest data.");
                }
                return;
            }
            boolean isParsedDashboardOrder = parseDashboardOrder(src);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Is parsed dashboard order : "+isParsedDashboardOrder);
            }
            if (isParsedDashboardOrder) {
                verDashboardOrder = ver;
            }
        } else if (key.equals(PROP_KEY_OOB_WIDGET_LIST)) {
            if (src == null || src.length <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Widget list byte array is null.");
                }
                return;
            }
            byte ver = src[0];
            if (ver == verWidgetList) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Current widget list is latest data.");
                }
                return;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Updated widget list.");
            }
            boolean isParsedWidgetList = parseWidgetList(src);
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardDataMgr.dataUpdated]Is parsed widget list : "+isParsedWidgetList);
            }
            if (isParsedWidgetList) {
                verWidgetList = ver;
                isUpdatedWidgetData = true;
                //All widget image flush and create widget image again.
                boolean createdWidgetImageList = createWidgetImages(fileWidgetImageZip);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardDataMgr.dataUpdated]Is created widget image : "+createdWidgetImageList);
                }
                if (createdWidgetImageList) {
                    isUpdatedWidgetData = false;
                    fileWidgetImageZip = null;
                }
            }
        }
    }
    /*****************************************************************
     * methods - DataManager Util-related
     *****************************************************************/
    private byte[] getByteArrayFromFile(File file) {
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException e) {
            Log.print(e);
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}

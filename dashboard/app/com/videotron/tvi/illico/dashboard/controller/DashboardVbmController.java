package com.videotron.tvi.illico.dashboard.controller;

import com.videotron.tvi.illico.log.Log;

public class DashboardVbmController extends VbmController {
    public static final long WIDGET_SELECTION = 1019000001L;
    public static final long WIDGET_SELECTION_DATA = 1019000002L;
    
    protected static DashboardVbmController instance = new DashboardVbmController();

    public static DashboardVbmController getInstance() {
        return instance;
    }
    
    //R7.4 freelife VDTRMASTER-6917
    public String savedWidgetId;
    public String savedAppName;
    public String[] savedParams;
       
    protected DashboardVbmController() {
    }
    
    public void widgetSelectionChannel(String widgetId, String callLetter) {
        Log.printInfo("[DashboardVbmController.widgetSelectionChannel] start.");
        Log.printInfo("[DashboardVbmController.widgetSelectionChannel] Param - Widget ID : " +widgetId );
        Log.printInfo("[DashboardVbmController.widgetSelectionChannel] Param - Call letter : " +callLetter );
        write(WIDGET_SELECTION, DEF_MEASUREMENT_GROUP, widgetId);
        write(WIDGET_SELECTION_DATA, DEF_MEASUREMENT_GROUP, callLetter);
        Log.printInfo("[DashboardVbmController.widgetSelectionChannel] end.");
    }
    
    public void widgetSelectionApplication(String widgetId, String appName, String[] params) {
        Log.printInfo("[DashboardVbmController.widgetSelectionApplication] start.");
        Log.printInfo("[DashboardVbmController.widgetSelectionApplication] Param - Widget ID : " +widgetId );
        Log.printInfo("[DashboardVbmController.widgetSelectionApplication] Param - App name : " +appName );
        write(WIDGET_SELECTION, DEF_MEASUREMENT_GROUP, widgetId);
        write(WIDGET_SELECTION_DATA, DEF_MEASUREMENT_GROUP, appName);
        Log.printInfo("[DashboardVbmController.widgetSelectionApplication] end.");
    }
}

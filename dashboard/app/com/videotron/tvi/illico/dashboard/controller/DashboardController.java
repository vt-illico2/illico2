package com.videotron.tvi.illico.dashboard.controller;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.dvb.io.ixc.IxcRegistry;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.communication.PreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.SharedMemoryManager;
import com.videotron.tvi.illico.dashboard.comp.dashboard.Dashboard;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.ui.DashboardUI;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.WindowProperty;

public class DashboardController implements InbandDataListener, LayeredKeyHandler {
    /** The instance. */
    private static DashboardController instance;
    /** The xlet context. */
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    public static LayeredUI layeredDialog;
    public static LayeredWindow lwDashboard;
    /*****************************************************************
     * variables - UI-related
     *****************************************************************/
    private DashboardUI dashboardUI;
    private boolean isActivated;

    /** The monitor service. */
    private MonitorService mService;
    private MainMenuService menuService;
    private ErrorMessageService emSvc;

    public static synchronized DashboardController getInstance() {
        if (instance == null) {
            instance = new DashboardController();
        }
        return instance;
    }

    private DashboardController() {
        lookupMonitorService();
        lookupMainMenuService();
    }

    public void init() {
        dashboardUI = new DashboardUI();
        lwDashboard = new LayeredWindowDashboard();
        lwDashboard.add(dashboardUI);
        lwDashboard.setVisible(true);
        dashboardUI.setVisible(true);
        layeredDialog = WindowProperty.DASHBOARD.createLayeredDialog(lwDashboard, lwDashboard.getBounds(), this);
        layeredDialog.deactivate();
    }

    public void dispose() {
        if (lwDashboard != null) {
            lwDashboard.removeAll();
            lwDashboard = null;
        }
        if (dashboardUI != null) {
            dashboardUI.dispose();
            dashboardUI = null;
        }
        if (layeredDialog != null) {
            layeredDialog.deactivate();
            layeredDialog = null;
        }
    }

    public void start() {
    }

    public void stop() {
    }

    public boolean isValidDashboard() {
        boolean isValidDashboard = false;
        if (dashboardUI != null) {
            dashboardUI.ready();
            Dashboard dashboard = dashboardUI.getDashboard();
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardController.isValidDashboard]dashboard : " + dashboard);
            }
            if (dashboard != null) {
                int addedWidgetCount = dashboard.getAddedWidgetCount();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardController.isValidDashboard]addedWidgetCount : " + addedWidgetCount);
                }
                if (addedWidgetCount > 0) {
                    isValidDashboard = true;
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DashboardUI.start]Added Widget is 0");
//                    }
//                    DashboardController.getInstance().hideDashboard();
                }
            }
        }
        return isValidDashboard;
    }

    public void showDashboard() {
        if (isActivated) {
            return;
        }
        boolean isValidDashboard = isValidDashboard();
        if (isValidDashboard) {
//            EventQueue.invokeLater(new Runnable() {
//                public void run() {
//                    if (layeredDialog != null) {
//                        layeredDialog.activate();
//                    }
//                    if (dashboardUI != null) {
//                        dashboardUI.start();
//                    }
//                }
//            });
            if (layeredDialog != null) {
                layeredDialog.activate();
            }
            if (dashboardUI != null) {
                dashboardUI.start();
            }
            isActivated = true;
        } else {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
            }
            hideDashboard();
        }
    }

    public void hideDashboard() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.hideDashboard]start - Activate Dashboard : " + isActivated);
        }
        FrameworkMain.getInstance().getImagePool().clear();
        if (!isActivated) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DashboardController.hideDashboard]Dashboard did not activate. end hideDashboard.");
            }
            return;
        }
//        EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                if (layeredDialog != null) {
//                    layeredDialog.deactivate();
//                }
//                if (dashboardUI != null) {
//                    dashboardUI.stop();
//                }
//            }
//        });
        if (layeredDialog != null) {
            layeredDialog.deactivate();
        }
        if (dashboardUI != null) {
            dashboardUI.stop();
        }
        isActivated = false;
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.hideDashboard]end.");
        }
        if (menuService != null) {
            try {
                menuService.hideMenu();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public UIComponent getUIComponent() {
        return dashboardUI;
    }

    /*****************************************************************
     * methods - MonitorService-related
     *****************************************************************/
    /**
     * Lookup monitor service.
     */
    private void lookupMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("MainController.lookupMinitorService: called");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (mService == null) {
                            try {
                                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                                mService = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[DashboardController.lookupMonitorService]not bound - " + MonitorService.IXC_NAME);
                                }
                            }
                        }
                        if (mService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("MainController.lookupMinitorService: looked up.");
                            }
                            actionAfterLookedUpMonitorService();
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("MainController.lookupMinitorService: retry MonitorService-lookup.");
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_MONITOR);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupMainMenuService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainController.lookupMainMenuService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (menuService == null) {
                            try {
                                String lookupName = "/1/2090/" + MainMenuService.IXC_NAME;
                                menuService = (MainMenuService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[DashboardController.lookupMainMenuService]not bound - " + MainMenuService.IXC_NAME);
                                }
                            }
                        }
                        if (menuService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[MainController.lookupMainMenuService]looked up.");
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[MainController.lookupMainMenuService]retry MenuService-lookup.");
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_MONITOR);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void actionAfterLookedUpMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.actionAfterLookedUpMonitorService]called");
        }
        if (!Rs.IS_EMULATOR) {
            if (mService != null) {
                try {
                    mService.addInbandDataListener(this, Rs.APP_NAME);
                } catch (Exception ignore) {
                }
            }
        }
    }
    public void receiveInbandData(String locator) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.receiveInbandData]locator : " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.receiveInbandData]called InbandAdapter.load(locator).");
        }
        if (mService != null) {
            mService.completeReceivingData(Rs.APP_NAME);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardController.receiveInbandData]called completeReceivingData.");
        }
    }
    public void startApplication(String appName, String[] appParamList) {
        if (Log.INFO_ON) {
            Log.printInfo("[DashboardCtrl.startApplication]Request app name : "+appName);
            Log.printDebug("[DashboardCtrl.startApplication]Request app parameter list : "+appParamList);
            if (appParamList != null) {
                for (int i=0; i<appParamList.length; i++) {
                    Log.printDebug("[DashboardCtrl.startApplication]Request app parameter["+i+"] : "+appParamList[i]);
                }
            }
        }
        //Special case - Settings
        boolean isProtectSettingsModifications = PreferenceProxy.getInstance().isProtectSettingsModifications();
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardCtrl.startApplication]isProtectSettingsModifications : "+isProtectSettingsModifications);
        }
        if (isProtectSettingsModifications) {
            PreferenceProxy.getInstance().requestCheckRightFilter(appName, appParamList);
        } else {
            requestStartUnboundApplication(appName, appParamList);
        }
    }
    public void requestStartUnboundApplication(String appName, String[] appParamList) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardCtrl.requestStartUnboundApp]Request app name : "+appName);
            Log.printDebug("[DashboardCtrl.requestStartUnboundApp]Request app parameter list : "+appParamList);
            if (appParamList != null) {
                for (int i=0; i<appParamList.length; i++) {
                    Log.printDebug("[DashboardCtrl.requestStartUnboundApp]Request app parameter["+i+"] : "+appParamList[i]);
                }
            }
            Log.printDebug("[DashboardCtrl.requestStartUnboundApp]Monitor service : "+mService);
        }
        if (mService == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[DashboardCtrl.requestStartUnboundApp]Monitor service is null. return.");
            }
            return;
        }
        try {
        	//R7.4 freelife VDTRMASTER-6917
        	if (appName.equals(Widget.APP_NAME_PROFILE)) {
        		DashboardVbmController.getInstance().widgetSelectionApplication(
        				DashboardVbmController.getInstance().savedWidgetId,
        				DashboardVbmController.getInstance().savedAppName,
        				DashboardVbmController.getInstance().savedParams
        				);
        	}
        }catch(Exception e) {
            e.printStackTrace();
        }
        try{
            mService.startUnboundApplication(appName, appParamList);
            if (Log.INFO_ON) {
                Log.printInfo("[DashboardCtrl.requestStartUnboundApp]Request start unbound application using by Monitor service.");
            }            
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[DashboardController.getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(String errorCode, ErrorMessageListener l) {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.showErrorMessage(errorCode, l);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void requestHideErrorMessage() {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.hideErrorMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public boolean handleKeyEvent(UserEvent userEvent) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyDashboard.handleKeyEvent]start.");
        }
        if (userEvent == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LayeredKeyDashboard.handleKeyEvent]User Event is null.");
            }
            return false;
        }
        if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int keyCode = userEvent.getCode();
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyDashboard.handleKeyEvent]Key code : " + keyCode);
        }
        boolean result = dashboardUI.handleKey(keyCode);
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyDashboard.handleKeyEvent]Result handled key : " + result);
        }
        return result;
    }
    class LayeredWindowDashboard extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowDashboard() {
            setBounds(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
        }
        public void notifyShadowed() {
            LayeredUIInfo[] layeredUIInfos = LayeredUIManager.getInstance().getAllLayeredUIInfos();
            if (layeredUIInfos != null) {
                int dsPriority = WindowProperty.DASHBOARD.getPriority();
                String dsName = WindowProperty.DASHBOARD.getName();
                int layeredUIInfosLth = layeredUIInfos.length;
                for (int i = 0; i < layeredUIInfosLth; i++) {
                    LayeredUIInfo layeredUIInfo = layeredUIInfos[i];
                    if (layeredUIInfo == null) {
                        continue;
                    }
                    String layeredUIName = layeredUIInfo.getName();
                    if (layeredUIName == null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LayeredWindowDashboard.notifyShadowed]Layered UI Name is null. continue.");
                        }
                        continue;
                    }
                    if (!dsName.equals(layeredUIName) && layeredUIInfo.getPriority() == dsPriority) {
                        boolean isActive = layeredUIInfo.isActive();
                        if (isActive) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[LayeredWindowDashboard.notifyShadowed]Dashboard stopped for activating "
                                                + layeredUIName);
                            }
                            hideDashboard();
                            break;
                        }
                    }
                }
            }
        }
    }
}

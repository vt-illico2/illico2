package com.videotron.tvi.illico.dashboard.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.ui.DashboardUI;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;

public class RendererDashboard extends Renderer {
    private static final Rectangle bounds = new Rectangle(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
    private Image imgBG;
    private Image imgTitleShadow;
    private Image imgIconPlus;
    
    public void prepare(UIComponent c) {
        imgBG = DataCenter.getInstance().getImage("00_promobg.png");
        imgTitleShadow = DataCenter.getInstance().getImage("00_title_shadow.png");
        imgIconPlus = DataCenter.getInstance().getImage("00_pluskey.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }
    protected void paint(Graphics g, UIComponent c) {
        //Background shadow
        if (imgBG != null) {
            g.drawImage(imgBG, DashboardUI.DASHBOARD_X - 80, Rs.SCREEN_SIZE_HEIGHT - imgBG.getHeight(c), c);
        }
        //Widget title
        if (imgIconPlus != null) {
            g.drawImage(imgIconPlus, DashboardUI.DASHBOARD_X - 16, DashboardUI.DASHBOARD_Y - 15, c);
        }
        if (imgTitleShadow != null) {
            g.drawImage(imgTitleShadow, DashboardUI.DASHBOARD_X - 22, DashboardUI.DASHBOARD_Y - 39, c);
        }
        String txtWidget = DataCenter.getInstance().getString("TxtDashboard.dashboard.title");
        if (txtWidget != null) {
            g.setFont(Rs.BLENDER24);
            g.setColor(Rs.C241241241);
            g.drawString(txtWidget, DashboardUI.DASHBOARD_X + 16, DashboardUI.DASHBOARD_Y);
        }
        //Version
        if (Log.EXTRA_ON) {
        	g.setFont(Rs.BLENDER18);
            g.drawString(Rs.APP_VERSION, DashboardUI.DASHBOARD_X + 300, DashboardUI.DASHBOARD_Y);
        }
    }
}

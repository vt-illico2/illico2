package com.videotron.tvi.illico.dashboard;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * @version $Revision: 1.71.6.1 $ $Date: 2017/09/13 12:49:52 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */
    public static final String APP_VERSION = "(R7.4).1.1";//"1.4.0.1";
    /** Application Name. */
    public static final String APP_NAME = "Dashboard";
    /** registered api name. */
    public static final String REGISTERED_API_NAME = "reg_api_dashboard";
    /** registered api storage priority. */
    public static final short REGISTERED_API_STORAGE_PRIOR = 123;

    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    public static final boolean USE_LOCAL_SERVER_FILE = IS_EMULATOR && true;
    public static final boolean INSTEAD_OF_IB_OOB = IS_EMULATOR && true;
    public static final boolean USE_PROP_DATA_INSTEAD_OF_IB_OOB = true; //MS_DATA or USER_PROP_DATA
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    /** The Constant Blender Pro Font size17. */
    public static final Font BLENDER17 = FontResource.BLENDER.getFont(17, true);
    /** The Constant Blender Pro Font size18. */
    public static final Font BLENDER18 = FontResource.BLENDER.getFont(18, true);
    /** The Constant Blender Pro Font size23. */
    public static final Font BLENDER23 = FontResource.BLENDER.getFont(23, true);
    /** The Constant Blender Pro Font size23. */
    public static final Font BLENDER24 = FontResource.BLENDER.getFont(24, true);
    /** The Constant Blender Pro Font size32. */
    public static final FontMetrics FM17 = FontResource.getFontMetrics(BLENDER17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(BLENDER18);
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    public static final Color C002002100 = new Color(2, 2, 100);
    public static final Color C003003003 = new Color(3, 3, 3);
    public static final Color C079129189 = new Color(79, 129, 189);
    public static final Color C193185154 = new Color(193, 185, 154);
    public static final Color C199159052 = new Color(199, 159, 52);
    public static final Color C219219219 = new Color(219, 219, 219);
    public static final Color C222016005 = new Color(222, 16, 5);
    public static final Color C230230230 = new Color(230, 230, 230);
    public static final Color C241241241 = new Color(241, 241, 241);
    public static final Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;

    /*********************************************************************************
     * Data Center Key-related
     *********************************************************************************/
    /** Data Center Key - Mini Menu Fixed Item Data. */
    public static final String DCKEY_MINI_MENU_FIXED_ITEM = "DCKEY_MINI_MENU_FIXED_ITEM";
    /** Data Center Key - Full Menu Promotion Data. */
    public static final String DCKEY_FULL_MENU_PROMOTION = "DCKEY_FULL_MENU_PROMOTION";
    /** Data Center Key - Display Menu. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_MENU = "DCKEY_MENU_CONFIG_DISPLAY_MENU";
    /** Data Center Key - dashboard display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_DASHBOARD = "DCKEY_MENU_CONFIG_DISPLAY_SEC_DASHBOARD";
    /** Data Center Key - full menu display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_FULL_MENU = "DCKEY_MENU_CONFIG_DISPLAY_SEC_FULL_MENU";
    /** Data Center Key - mini menu display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_MINI_MENU = "DCKEY_MENU_CONFIG_DISPLAY_SEC_MINI_MENU";

    /*********************************************************************************
     * Look up Milis-related
     *********************************************************************************/
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS_MONITOR = 1000;
    /** Retry Look up Millis - EPG. */
    public static final int RETRY_LOOKUP_MILLIS_EPG = 1000;
    /** Retry Look up Millis - User Profile and Preference. */
    public static final int RETRY_LOOKUP_MILLIS_UPP = 1000;

    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960;
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;

    /*********************************************************************************
     * etc Constant-related
     *********************************************************************************/
    /** The Constant MILLIS_TO_SECOND. */
    public static final int MILLIS_TO_SECOND = 1000;
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM = "DAS500";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "DAS501";
    public static final String ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM = "DAS502";
    public static final String ERROR_CODE_SERVER_ACCESS_PROBLEM = "DAS503";
}

package com.videotron.tvi.illico.dashboard;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.videotron.tvi.illico.log.Log;

public class Util {
    private static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    
    /** The Constant ZIP_FULE_BYTE_BUFFER.*/
    public static final int ZIP_FILE_BYTE_BUFFER = 1024;

    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative){
        return(((((int)data[offset])&(byNegative ? 0x7F : 0xFF))<<8)+(((int)data[offset+1])&0xFF));
    }

    public static byte[] intTo2Byte(int data) {
        byte[] byteData = new byte[2];
        byteData[0] = (byte) ((data >> 8) & 0xff);
        byteData[1] = (byte) (data & 0xff);
        return byteData;
    }

    public static byte[] stringTo2Byte(String data) {
        if (data == null) {
            return null;
        }
        data += " ";
        byte[] dataBytes = data.getBytes();
        byte[] byteData = new byte[2];
        System.arraycopy(dataBytes, 0, byteData, 0, 2);
        return byteData;
    }
    /**
     * String to2 byte.
     *
     * @param data the data
     * @return the byte[]
     */
    public static byte[] stringToNByte(String data, int targetByte) {
        if (data == null) {
            data = "";
        }
        int dataLth = data.length();
        if (targetByte>dataLth) {
            for (int i=0; i<targetByte-dataLth; i++) {
                data += " ";
            }
        }
        byte[] dataBytes = data.getBytes();
        byte[] byteData = new byte[targetByte];
        System.arraycopy(dataBytes, 0, byteData, 0, targetByte);
        return byteData;
    }
    public static byte[] getBytesFromZip(ZipFile zipFile, String entryName) {
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }

        if (entryName == null) {
            entryName = "";
        } else {
            entryName = entryName.trim();
        }
        if (entryName.length()<=0) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Entry Name is invalid.");
            }
            return null;
        }

        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            ZipEntry entry = zipFile.getEntry(entryName);
            in = new BufferedInputStream(zipFile.getInputStream(entry));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        } finally {
            if (bout != null) {
                bout.reset();
                try {
                    bout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bout = null;
            }
            if (in != null) {
                try{
                    in.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                in = null;
            }
        }
        return result;
    }


    /**
     * Gets the bytes from zip.
     *
     * @param zipFile
     *            the zip file
     * @param entryName
     *            the entry name
     * @return the bytes from zip
     * @throws ZipException
     *             the zip exception
     */

    public static byte[] getByteArrayFromFile(File file) {
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (bout != null) {
                bout.reset();
                try {
                    bout.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bout = null;
            }
            if (in != null) {
                try{
                    in.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                in = null;
            }
        }
        return result;
    }
    /**
     * Load properties using by file name.
     * @param reqFileName file name
     * @return the properties
     */
    public static Properties loadProperties(String reqFileName) {
        if (reqFileName == null) {
            return null;
        }
        File file = new File(reqFileName);
        return loadProperties(file);
    }

    /**
     * Load properties using by file.
     * @param reqFile file
     * @return the properties
     */
    public static Properties loadProperties(File reqFile) {
        if (reqFile == null) {
            return null;
        }
        Properties prop = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(reqFile);
            prop.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally{
            if (fis != null) {
                try{
                    fis.close();
                }catch(Exception ignore) {
                }
                fis = null;
            }
        }
        return prop;
    }
    
    public static Date convertXmlDateTimeToDate(final String xmlDateTime) {
        if (xmlDateTime.length() != 25) {
            return null;
        }
        StringBuilder sb = new StringBuilder(xmlDateTime);
        sb.deleteCharAt(22);
        Date d = null;
        try {
            d = SDF.parse(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
}

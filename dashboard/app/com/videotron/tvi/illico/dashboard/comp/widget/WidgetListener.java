package com.videotron.tvi.illico.dashboard.comp.widget;

public interface WidgetListener {
   void startedUnboundApplication(String widgetId, String reqAppName, String[] reqExtedParams);
   void selectedChannel(String widgetId, String reqCallLetter);
}

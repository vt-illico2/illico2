package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

public class WeatherWidgetPreferenceProxy implements PreferenceListener{
    private static WeatherWidgetPreferenceProxy instance;
    private PreferenceService prefSvc;

    public static final String UPP_KEY_HOME_TOWN_LOCATION_ID = "WEATHER_HOME_TOWN_LOCATION_ID";
    public static final String UPP_KEY_TEMPERATURE_UNIT = "WEATHER_TEMPERATURE_UNIT";

    private static final int UPP_INDEX_COUNT = 3;
    private static final int UPP_INDEX_LANGUAGE = 0;
    private static final int UPP_INDEX_HOME_TOWN_LOCATION_ID = 1;
    private static final int UPP_INDEX_TEMPERATURE_UNIT = 2;
    private String[] prefNames;
    private String[] prefValues;

    public static synchronized WeatherWidgetPreferenceProxy getInstance() {
        if (instance == null) {
            instance = new WeatherWidgetPreferenceProxy();
        }
        return instance;
    }
    private WeatherWidgetPreferenceProxy() {
        init();
    }
    public void init() {
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
        prefNames[UPP_INDEX_HOME_TOWN_LOCATION_ID] = UPP_KEY_HOME_TOWN_LOCATION_ID;
        prefNames[UPP_INDEX_TEMPERATURE_UNIT] = UPP_KEY_TEMPERATURE_UNIT;
        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_FRENCH;
        prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID] = "";
        prefValues[UPP_INDEX_TEMPERATURE_UNIT] = WeatherWidgetDataManager.TEMPERATURE_UNIT_C;
    }
    public void dispose() {
        prefNames = null;
        prefValues = null;
        instance = null;
    }
    public void start() {
        checkPreferenceService();
    }
    public void stop() {
        if (prefSvc != null) {
            try{
                prefSvc.removePreferenceListener("WEATHER_WIDGET", this);
            }catch(Exception ignore) {
            }
            prefSvc = null;
        }
    }
    private void checkPreferenceService() {
        if (prefSvc == null) {
            setPreferenceService();
        }
    }
    private void setPreferenceService() {
        try {
            String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
            prefSvc = (PreferenceService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
        } catch (Exception e) {
            if (Log.INFO_ON) {
                Log.printInfo("[WeatherWidgetPreferenceProxy.setPreferenceService]not bound - " + PreferenceService.IXC_NAME);
            }
        }
        if (prefSvc != null) {
            String[] uppData = null;
            try {
                uppData = prefSvc.addPreferenceListener(this, "WEATHER_WIDGET"+System.currentTimeMillis(), prefNames, prefValues) ;
                if (uppData == null || uppData.length < UPP_INDEX_COUNT) {
                    if (Log.WARNING_ON) {
                        Log.printWarning("[WeatherWidgetPreferenceProxy.setPreferenceService]Upp data is invalid. return.");
                    }
                    return;
                }
                receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
                receiveUpdatedPreference(UPP_KEY_HOME_TOWN_LOCATION_ID, uppData[UPP_INDEX_HOME_TOWN_LOCATION_ID]);
                receiveUpdatedPreference(UPP_KEY_TEMPERATURE_UNIT, uppData[UPP_INDEX_TEMPERATURE_UNIT]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private PreferenceService getPreferenceService() {
        checkPreferenceService();
        return prefSvc;
    }
    /**
     * invoked when the value of the preference has changed.
     * @param name
     *            Updated Preference Name
     * @param value
     *            Updated value of Preference
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetPreferenceProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            prefValues[UPP_INDEX_LANGUAGE] = value;
            WeatherWidgetDataManager.getInstance().updatedPreferenceLanguage(value);
        } else if (name.equals(UPP_KEY_HOME_TOWN_LOCATION_ID)) {
            prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID] = value;
        } else if (name.equals(UPP_KEY_TEMPERATURE_UNIT)) {
            prefValues[UPP_INDEX_TEMPERATURE_UNIT] = value;
        }
    }
    /*********************************************************************************
     * Return Data-related
     *********************************************************************************/
    public String getPostalCode() {
        PreferenceService pSvc = getPreferenceService();
        String postalCode = null;
        if (pSvc != null) {
            try{
                postalCode = pSvc.getPostalCode();
            }catch(Exception e) {
                e.printStackTrace();
            }
        }
        return postalCode;
    }
    public String getHomeTownLocationId() {
        checkPreferenceService();
        if (prefValues == null) {
            return null;
        }
        return prefValues[UPP_INDEX_HOME_TOWN_LOCATION_ID];
    }
    public boolean setHomeTownLocationId(String reqValue) {
        if (reqValue == null) {
            return false;
        }
        PreferenceService pSvc = getPreferenceService();
        boolean result = false;
        if (pSvc != null) {
            try{
                result = pSvc.setPreferenceValue(UPP_KEY_HOME_TOWN_LOCATION_ID, reqValue);
            } catch(Exception e) {
               e.printStackTrace();
            }
        }
        return result;
    }
}

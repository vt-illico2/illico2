package com.videotron.tvi.illico.dashboard.comp;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.dashboard.communication.CommunicationManager;
import com.videotron.tvi.illico.dashboard.comp.dashboard.Dashboard;
import com.videotron.tvi.illico.dashboard.comp.widget.ProxyInfo;
import com.videotron.tvi.illico.dashboard.comp.widget.ServerInfo;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.comp.widget.WidgetUnit;
import com.videotron.tvi.illico.dashboard.comp.widget.promotion.PromotionWidget;
import com.videotron.tvi.illico.dashboard.comp.widget.weather.WeatherWidget;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;

public class SharedMemoryManager {
    private static SharedMemoryManager instance;
    private SharedMemory sm = SharedMemory.getInstance();

    private SharedMemoryManager() {
    }

    public static synchronized SharedMemoryManager getInstance() {
        if (instance == null) {
            instance = new SharedMemoryManager();
        }
        return instance;
    }

    /*****************************************************************
     * methods - SharedMemory-related
     *****************************************************************/
//    private boolean putWidget(Widget widget) {
//        if (widget == null) {
//            return false;
//        }
//
//        Hashtable widgetListHash = (Hashtable)sm.get(Widget.SMKEY_WIDGET_LIST);
//        if (widgetListHash == null) {
//            widgetListHash = new Hashtable();
//        }
//        String id = widget.getId();
//        String name = widget.getName();
//        boolean isAllowRunning = widget.isAllowRunning();
//        int refreshRateSecs = widget.getRefreshRateSecs();
////        Image[] images = widget.getId();
//        boolean isCyclable = widget.isCyclable();
//        int cycleTimeSecs = widget.getCycleTimeSecs();
//        int templateType = widget.getTemplateType();
//        boolean isSelectable = widget.isSelectable();
//        String callLetter = widget.getCallLetter();
//        String bindedApplicationName = widget.getBindedApplicationName();
//        String[] bindedApplicationNameParams = widget.getBindedApplicationParams();
//        int managedType = widget.getManagedType();
//
//        Hashtable widgetHash = new Hashtable();
//        widgetHash.put(Widget.SMKEY_WIDGET_NAME, name);
//        widgetHash.put(Widget.SMKEY_WIDGET_IS_ALLOW_RUN, new Boolean(isAllowRunning));
//        widgetHash.put(Widget.SMKEY_WIDGET_REFRESH_RATE_SECS, new Integer(refreshRateSecs));
//        widgetHash.put(Widget.SMKEY_WIDGET_IS_CYCLABLE, new Boolean(isCyclable));
//        widgetHash.put(Widget.SMKEY_WIDGET_CYCLE_TIME_SECS, new Integer(cycleTimeSecs));
//        widgetHash.put(Widget.SMKEY_WIDGET_TEMPLATE_TYPE, new Integer(templateType));
//        widgetHash.put(Widget.SMKEY_WIDGET_IS_SELECTABLE, new Boolean(isSelectable));
//        widgetHash.put(Widget.SMKEY_WIDGET_CALL_LETTER, callLetter);
//        widgetHash.put(Widget.SMKEY_WIDGET_BINDED_APPLICATION_NAME, bindedApplicationName);
//        widgetHash.put(Widget.SMKEY_WIDGET_BINDED_APPLICATION_PARAMS, bindedApplicationNameParams);
//        widgetHash.put(Widget.SMKEY_WIDGET_MANAGED_TYPE, new Integer(managedType));
//
//        widgetListHash.put(Widget.SMKEY_WIDGET_TAG_DATA+id, widgetHash);
//        return true;
//    }
    public Widget getWidget(String id, String reqContState) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]start.");
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Param - Request content state : "+reqContState);
        }
        Hashtable widgetListHash = (Hashtable)sm.get(Widget.SMKEY_WIDGET_LIST);
        if (widgetListHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Widget List Hash is null.");
            }
            return null;
        }
        Hashtable widgetHash = (Hashtable) widgetListHash.get(Widget.SMKEY_WIDGET_TAG_DATA + id);
        if (widgetHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory][" + id + "]Widget Hash is null.");
            }
            return null;
        }
        //Possible contextual state
        String pContState = (String)widgetHash.get(Widget.SMKEY_WIDGET_POSSIBLE_CONTEXTUAL_STATE);
        if (pContState == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Possible Contextual State is null.");
            }
            return null;
        }
        pContState = pContState.trim();
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Possible Contextual State : "+pContState);
        }
        if (!pContState.equals(Widget.WIDGET_CONTEXTUAL_STATE_BOTH) && !pContState.equals(reqContState)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Possible Contextual State is not match.");
            }
            return null;
        }
        //Managed type
        Integer managedTypeI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_MANAGED_TYPE);
        int managedType = -1;
        if (managedTypeI != null) {
            managedType = managedTypeI.intValue();
        }
        //Image count
        Integer imageCountI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_IMAGE_COUNT);
        int imageCount = -1;
        if (imageCountI != null) {
            imageCount = imageCountI.intValue();
        }
        //Refersh rate secs
        Integer refreshRateMinsI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_REFRESH_RATE_MINS);
        int refreshRateMins = -1;
        if (refreshRateMinsI != null) {
            refreshRateMins = refreshRateMinsI.intValue();
        }
        //Cyclable
        Boolean isCyclableB = (Boolean)widgetHash.get(Widget.SMKEY_WIDGET_IS_CYCLABLE);
        boolean isCyclable = false;
        if (isCyclableB != null) {
            isCyclable = isCyclableB.booleanValue();
        }
        //Cycle time secs
        Integer cycleTimeSecsI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_CYCLE_TIME_SECS);
        int cycleTimeSecs = -1;
        if (cycleTimeSecsI != null) {
            cycleTimeSecs = cycleTimeSecsI.intValue();
        }
        //Server info
        String serverInfo = (String)widgetHash.get(Widget.SMKEY_WIDGET_BG_SERVER_INFO);
        String[] proxyIPs = (String[])widgetHash.get(Widget.SMKEY_WIDGET_BG_PROXY_IP_LIST);
        int[] proxyPorts = (int[])widgetHash.get(Widget.SMKEY_WIDGET_BG_PROXY_PORT_LIST);
        //Image names
        String[] unitPkList = (String[])widgetHash.get(Widget.SMKEY_WIDGET_UNIT_PRIMARY_KEY_LIST);
        WidgetUnit[] wUnits = null;
        if (unitPkList != null) {
            wUnits = new WidgetUnit[unitPkList.length];
            for (int i=0; i<unitPkList.length; i++) {
                String unitPk = unitPkList[i];
                if (unitPk == null) {
                    continue;
                }
                //Widget unit
                wUnits[i] = new WidgetUnit();
                wUnits[i].setImageNameEnglish(unitPk);
                String imgNameFr = (String) widgetHash.get(Widget.SMKEY_WIDGET_IMAGE_NAME_FRENCH + unitPk);
                wUnits[i].setImageNameFrench(imgNameFr);
                //Selectable
                Boolean isSelectableB = (Boolean)widgetHash.get(Widget.SMKEY_WIDGET_IS_SELECTABLE + unitPk);
                boolean isSelectable = false;
                if (isSelectableB != null) {
                    isSelectable = isSelectableB.booleanValue();
                }
                wUnits[i].setSelectable(isSelectable);
                //Template type
                Integer templateTypeI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_TEMPLATE_TYPE+unitPk);
                int templateType = -1;
                if (templateTypeI != null) {
                    templateType = templateTypeI.intValue();
                }
                wUnits[i].setTemplateType(templateType);
                //Target action
                Integer targetActionI = (Integer)widgetHash.get(Widget.SMKEY_WIDGET_TARGET_TYPE+unitPk);
                int targetAction = -1;
                if (targetActionI != null) {
                    targetAction = targetActionI.intValue();
                }
                wUnits[i].setTargetAction(targetAction);
                //Target call letter
                String targetCallLetter = (String)widgetHash.get(Widget.SMKEY_WIDGET_CALL_LETTER+unitPk);
                wUnits[i].setTargetCallLetter(targetCallLetter);
                //Target app name
                String targetAppName = (String)widgetHash.get(Widget.SMKEY_WIDGET_BINDED_APPLICATION_NAME+unitPk);
                wUnits[i].setTargetAppName(targetAppName);
                //Target app parameter
                String[] targetAppParams = (String[])widgetHash.get(Widget.SMKEY_WIDGET_BINDED_APPLICATION_PARAMS+unitPk);
                wUnits[i].setTargetAppParamter(targetAppParams);
            }
        }
        
        // R5 - TANK
        // check application rights
        Vector retUnitVector = new Vector();
        for (int i = 0; i < wUnits.length; i++) {
        	
        	if (wUnits[i].getTargetAction() == Widget.WIDGET_TARGET_TYPE_APPLICATION 
        			&& wUnits[i].getTargetAppName() != null) {
        		try {
        			MonitorService mService = CommunicationManager.getInstance().getMonitorService();
        			if (Log.DEBUG_ON) {
                        Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]mService=" + mService);
                    }
					int result = mService.checkAppAuthorization(wUnits[i].getTargetAppName());
					
					if (Log.DEBUG_ON) {
						Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"] check app authorization,"
								+ " app name : " + wUnits[i].getTargetAppName());
						Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"] check app authorization,"
								+ " result : " + result);
					}
					
					if (result == MonitorService.AUTHORIZED || result == MonitorService.NOT_FOUND_ENTITLEMENT_ID) {
						retUnitVector.add(wUnits[i]);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
        		
        	} else {
        		retUnitVector.add(wUnits[i]);
        	}
        }
        
        if (Log.DEBUG_ON) {
			Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"] new Unit size : "
					+ retUnitVector.size());
        }
        if (retUnitVector.size() == 0) {
        	return null;
        }
        
        wUnits = new WidgetUnit[retUnitVector.size()];
        retUnitVector.toArray(wUnits);
        
        //Name
        String name = (String) widgetHash.get(Widget.SMKEY_WIDGET_NAME);
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]id : "+id);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]name : "+name);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]imageCount : "+imageCount);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]refreshRateMins : "+refreshRateMins);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]isCyclable : "+isCyclable);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]cycleTimeSecs : "+cycleTimeSecs);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]managedType : "+managedType);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]serverInfo : "+serverInfo);
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Widget units : "+wUnits);
            if (wUnits != null) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Widget units count : "+wUnits.length);
            }
        }
        Widget widget = null;
        if (name == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Widget name is null.");
            }
            return null;
        }
        if (name.equals(Widget.WIDGET_TYPE_PROMOTION)) {
            widget = new PromotionWidget();
        } else if (name.equals(Widget.WIDGET_TYPE_WEATHER)) {
            widget = new WeatherWidget();
        } else {
            if (Log.WARNING_ON) {
                Log.printWarning("[SharedMemoryMgr.getWidgetFromSharedMemory]["+id+"]Widget name is invalid.");
            }
        }
        if (widget == null) {
            return null;
        }
        widget.setId(id);
        widget.setName(name);
        widget.setImageCount(imageCount);
        widget.setRefreshRateMins(refreshRateMins);
        widget.setCyclable(isCyclable);
        widget.setCycleTimeSecs(cycleTimeSecs);
        widget.setWidgetUnits(wUnits);
        widget.setManagedType(managedType);
        widget.setProxyIpList(proxyIPs);
        widget.setProxyPortList(proxyPorts);
        widget.setServerInfo(serverInfo);
        return widget;
    }
    public Dashboard getDashboard(int dashboardType) {
        Hashtable dashboardListHash = (Hashtable)sm.get(Dashboard.SMKEY_DASHBOARD_IB_CONFIG_LIST);
        if (dashboardListHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]Dashboard List Hash is null.");
            }
            return null;
        }
        Hashtable dashboardHash = (Hashtable)dashboardListHash.get(Dashboard.SMKEY_DASHBOARD_TAG+dashboardType);
        if (dashboardHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]Dashboard Hash is null.");
            }
            return null;
        }

        Integer typeI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_TYPE);
        int type = -1;
        if (typeI != null) {
            type = typeI.intValue();
        }

//        Integer maxWidgetTypesI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_MAX_WIDGET_TYPES);
//        int maxWidgetTypes = -1;
//        if (maxWidgetTypesI != null) {
//            maxWidgetTypes = maxWidgetTypesI.intValue();
//        }

        Integer maxVisibleWidgetCountI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_MAX_VISIBLE_WIDGET_COUNT);
        int maxVisibleWidgetCount = -1;
        if (maxVisibleWidgetCountI != null) {
            maxVisibleWidgetCount = maxVisibleWidgetCountI.intValue();
        }

        Integer inactiveTimeoutSecsI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_INACTIVE_TIMEOUT_SECS);
        int inactiveTimeoutSecs = -1;
        if (inactiveTimeoutSecsI != null) {
            inactiveTimeoutSecs = inactiveTimeoutSecsI.intValue();
        }

        Integer locationXI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_LOCATION_X);
        int locationX = -1;
        if (locationXI != null) {
            locationX = locationXI.intValue();
        }

        Integer locationYI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_LOCATION_Y);
        int locationY = -1;
        if (locationYI != null) {
            locationY = locationYI.intValue();
        }

        Integer sizeWI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_SIZE_W);
        int sizeW = -1;
        if (sizeWI != null) {
            sizeW = sizeWI.intValue();
        }

        Integer sizeHI = (Integer)dashboardHash.get(Dashboard.SMKEY_DASHBOARD_SIZE_H);
        int sizeH = -1;
        if (sizeHI != null) {
            sizeH = sizeHI.intValue();
        }

        Dashboard dashboard = new Dashboard();
//        dashboard.setId(id);
        dashboard.setType(type);
//        dashboard.setMaxWidgetTypes(maxWidgetTypes);
        dashboard.setMaxVisibleWidgetCount(maxVisibleWidgetCount);
        dashboard.setInactivityTimeoutSecs(inactiveTimeoutSecs);
        dashboard.setLocationX(locationX);
        dashboard.setLocationY(locationY);
        dashboard.setSizeW(sizeW);
        dashboard.setSizeH(sizeH);

        return dashboard;
    }
    public String[] getWidgetOrder(int id) {
        Hashtable widgetOrdersHash = (Hashtable)sm.get(Dashboard.SMKEY_DASHBOARD_OOB_ORDER_LIST);
        if (widgetOrdersHash == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]Widget Order Hash is null.");
            }
            return null;
        }
        String[] widgetOrder = (String[])widgetOrdersHash.get(Dashboard.SMKEY_WIDGET_ORDERS+id);
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getWidgetFromSharedMemory]widget Order : "+widgetOrder);
        }
        return widgetOrder;
    }
    public ServerInfo getServerInfo(String reqWidgetType){
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getServerInfo]Start");
            Log.printDebug("[SharedMemoryMgr.getServerInfo]Param - Request widget type : "+reqWidgetType);
        }
        if (reqWidgetType == null) {
            Log.printError("[SharedMemoryMgr.getServerInfo]Request widget type is null. return null.");
            return null;
        }
        Hashtable widgetServerListHash = (Hashtable)sm.get(Widget.SMKEY_WIDGET_SERVER_LIST);
        if (widgetServerListHash == null) {
            Log.printError("[SharedMemoryMgr.getServerInfo]Widget server list hash is null.");
            return null;
        }
        Hashtable widgetServerHash = (Hashtable)widgetServerListHash.get(reqWidgetType);
        if (widgetServerHash == null) {
            Log.printError("[SharedMemoryMgr.getServerInfo]Widget Hash is null.");
            return null;
        }
        //Possible contextual state
        String serverHost = (String)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_HOST);
        Integer tempSrverPort = (Integer)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_PORT);
        int serverPort = 0;
        if (tempSrverPort != null) {
            serverPort = tempSrverPort.intValue();
        }
        String contextRoot = (String)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_CONTEXT_ROOT);
        Integer tempProxyCount = (Integer)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_PROXY_COUNT);
        int proxyCount = 0;
        if (tempProxyCount != null) {
            proxyCount = tempProxyCount.intValue();
        }
        ProxyInfo[] pInfoList = null;
        if (proxyCount > 0) {
            pInfoList = new ProxyInfo[proxyCount];
            for (int i=0; i<proxyCount; i++) {
                pInfoList[i] = new ProxyInfo();
                String proxyHost = (String)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_PROXY_HOST + i);
                Integer tempProxyPort = (Integer)widgetServerHash.get(Widget.SMKEY_WIDGET_SERVER_PROXY_PORT + i);
                int proxyPort = 0;
                if (tempProxyPort != null) {
                    proxyPort = tempProxyPort.intValue();
                }
                pInfoList[i].setProxyHost(proxyHost);
                pInfoList[i].setProxyPort(proxyPort);
            }
        }
        ServerInfo sInfo = new ServerInfo();
        sInfo.setHost(serverHost);
        sInfo.setPort(serverPort);
        sInfo.setContextRoot(contextRoot);
        sInfo.setProxyInfos(pInfoList);
        if (Log.DEBUG_ON) {
            Log.printDebug("[SharedMemoryMgr.getServerInfo]Server host ["+reqWidgetType+"] : "+serverHost);
            Log.printDebug("[SharedMemoryMgr.getServerInfo]Server port ["+reqWidgetType+"] : "+serverPort);
            Log.printDebug("[SharedMemoryMgr.getServerInfo]Context root ["+reqWidgetType+"] : "+contextRoot);
            ProxyInfo[] pInfos = sInfo.getProxyInfos();
            if (pInfos != null) {
                int pInfosLth = pInfos.length;
                Log.printDebug("[SharedMemoryMgr.getServerInfo]Proxy count ["+reqWidgetType+"] : "+pInfosLth);
                for (int i = 0; i<pInfosLth; i++) {
                    if (pInfos[i] == null) {
                        continue;
                    }
                    Log.printDebug("[SharedMemoryMgr.getServerInfo]Proxy host ["+reqWidgetType+"]["+i+"] : "+pInfos[i].getProxyHost());
                    Log.printDebug("[SharedMemoryMgr.getServerInfo]Proxy port ["+reqWidgetType+"]["+i+"] : "+pInfos[i].getProxyPort());
                }
            }
        }
        return sInfo;
    }
}

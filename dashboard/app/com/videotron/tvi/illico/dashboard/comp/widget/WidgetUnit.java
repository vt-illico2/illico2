package com.videotron.tvi.illico.dashboard.comp.widget;


public class WidgetUnit {
    private String imageNameEn;
    private String imageNameFr;
    private boolean isSelectable;
    private int templateType;
    private int targetAction;
    private String targetCallLetter;
    private String targetAppName;
    private String[] targetAppParamter;

    public String getImageNameEnglish() {
        return imageNameEn;
    }
    public void setImageNameEnglish(String imageNameEn) {
        this.imageNameEn = imageNameEn;
    }
    public String getImageNameFrench() {
        return imageNameFr;
    }
    public void setImageNameFrench(String imageNameFr) {
        this.imageNameFr = imageNameFr;
    }
    public boolean isSelectable() {
        return isSelectable;
    }
    public void setSelectable(boolean isSelectable) {
        this.isSelectable = isSelectable;
    }
    public int getTemplateType() {
        return templateType;
    }
    public void setTemplateType(int templateType) {
        this.templateType = templateType;
    }
    public int getTargetAction() {
        return targetAction;
    }
    public void setTargetAction(int targetAction) {
        this.targetAction = targetAction;
    }
    public String getTargetCallLetter() {
        return targetCallLetter;
    }
    public void setTargetCallLetter(String targetCallLetter) {
        this.targetCallLetter = targetCallLetter;
    }
    public String getTargetAppName() {
        return targetAppName;
    }
    public void setTargetAppName(String targetAppName) {
        this.targetAppName = targetAppName;
    }
    public String[] getTargetAppParamter() {
        return targetAppParamter;
    }
    public void setTargetAppParamter(String[] targetAppParamter) {
        this.targetAppParamter = targetAppParamter;
    }
}

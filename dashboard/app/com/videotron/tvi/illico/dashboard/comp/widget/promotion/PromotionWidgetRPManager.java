package com.videotron.tvi.illico.dashboard.comp.widget.promotion;

import java.io.IOException;
import java.net.URL;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.comp.IOUtil;
import com.videotron.tvi.illico.dashboard.comp.widget.ServerInfo;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.log.Log;

public class PromotionWidgetRPManager {
    private static PromotionWidgetRPManager instance;
    
    private ErrorMessageService emSvc;

    public static synchronized PromotionWidgetRPManager getInstance() {
        if (instance == null) {
            instance = new PromotionWidgetRPManager();
        }
        return instance;
    }

    private PromotionWidgetRPManager() {
    }

    public byte[] getWidgetBackgroundImage(ServerInfo bgServerInfo, String reqWidgetBgImgName) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromoWidgetRPMgr.getWidgetBackgroundImage]start");
            Log.printDebug("[PromoWidgetRPMgr.getWidgetBackgroundImage]Param - Request server info : ["
                    + bgServerInfo + "]");
            Log.printDebug("[PromoWidgetRPMgr.getWidgetBackgroundImage]Param - Request widget background image name : ["
                    + reqWidgetBgImgName + "]");
        }
        if (bgServerInfo == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][PromotionBG]Server info is invalid.");
            }
            return null;
        }
        if (reqWidgetBgImgName == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][PromotionBG]Background image name is invalid.");
            }
            return null;
        }
        URL reqURL = null;
        try {
            reqURL = bgServerInfo.getURL(reqWidgetBgImgName);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][PromotionBG]Cannot create URL object by following exception ["+e.getMessage()+"].");
            }
            return null;
        }
        if (reqURL == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][PromotionBG]URL object is null.");
            }
            return null;
        }
        byte[] srcWidgetBgImg = null;
        try {
            srcWidgetBgImg = IOUtil.getBytesFromUrl(reqURL);
        }catch(IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][PromotionBG]Cannot retrieve Widget images from the MS HTTP Server by IOException["+e.getMessage()+"].");
            }
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][PromotionBG]Cannot retrieve Widget images from the MS HTTP Server by Exception["+e.getMessage()+"].");
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromoWidgetRPMgr.getWidgetBackgroundImage][PromotionBG]Response widget background image src : "
                    + srcWidgetBgImg);
        }
        return srcWidgetBgImg;
    }
    
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(final String errorCode) {
        new Thread(){
            public void run() {
                ErrorMessageService svc = getErrorMessageService();
                if (svc != null) {
                    try {
                        svc.showErrorMessage(errorCode, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}

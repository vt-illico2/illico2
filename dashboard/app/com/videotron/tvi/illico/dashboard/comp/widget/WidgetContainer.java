package com.videotron.tvi.illico.dashboard.comp.widget;

import java.awt.Container;

abstract public class WidgetContainer extends Container {
    private static final long serialVersionUID = -8963401142621421317L;
    public void init(){}
    abstract public void dispose();
    abstract public void start();
    abstract public void stop();
    abstract public boolean keyAction(int keyCode);
}

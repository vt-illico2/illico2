package com.videotron.tvi.illico.dashboard.comp;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;

public class IOUtil {
    public static Object getObejctFromUrlByParser(URL reqURL, DefaultHandler reqHandler) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getObejctFromURLByParser]Start.");
            Log.printDebug("[RPManager.getObejctFromURLByParser]Param - request URL : [" + reqURL + "]");
            Log.printDebug("[RPManager.getObejctFromURLByParser]Param - request Handler : [" + reqHandler + "]");
        }
        if (reqURL == null || reqHandler == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getObejctFromURLByParser]Request parameter is invalid. return null.");
            }
            return null;
        }
        Object result = null;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) reqURL.openConnection();
            
            // VDTRMASTER-5846
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes] reponseCode=" + code);
            }
            result = read(con.getInputStream(), reqHandler);
        } catch (Exception ioe) {
            ioe.printStackTrace();
            throw ioe;
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    public static Object getObejctFromFileByParser(File reqFile, DefaultHandler reqHandler) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqFile + "]");
        }
        if (reqFile == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        Object result = null;
        try {
            result = read(new FileInputStream(reqFile), reqHandler);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    public static byte[] getBytesFromUrl(URL reqUrl) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqUrl + "]");
        }
        if (reqUrl == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        byte[] result = null;
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) reqUrl.openConnection();
            con.getResponseCode();
            result = read(reqUrl.openStream());
        } catch (Exception ioe) {
            ioe.printStackTrace();
            throw ioe;
        } finally {
            if (con != null) {
                try {
                    con.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                con = null;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    public static byte[] getBytesFromFile(File reqFile) throws Exception{
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]Start.");
            Log.printDebug("[RPManager.getBytes]Param - request URL : [" + reqFile + "]");
        }
        if (reqFile == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[RPManager.getBytes]Request parameter is invalid. return null.");
            }
            return null;
        }
        byte[] result = null;
        try {
            result = read(new FileInputStream(reqFile));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
    private static Object read(InputStream in, DefaultHandler handler) throws Exception{
        if (in == null || handler == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("RPMgr.getLotteryList]URL is null. return null.");
            }
            return null;
        }
        SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
        try {
            parser.parse(in, handler);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                in = null;
            }
        }
        SAXHandler saxHandler = (SAXHandler) handler;
        Object obj = null;
        if (saxHandler != null) {
            obj = saxHandler.getResultObject();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("RPMgr.getLotteryList]Response lottery list : "+obj);
        }
        return obj;
    }
    private static byte[] read(InputStream in) throws Exception{
        byte[] result = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(in);
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
            throw ioe;
        } finally {
            try {
                if (bout != null) {
                    bout.reset();
                    bout.close();
                    bout = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (in != null) {
                    in.close();
                    in = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[RPManager.getBytes]End : " + result);
        }
        return result;
    }
}

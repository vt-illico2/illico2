package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.awt.Image;
import java.util.Date;

public class Weather {
    public static final String TAG_NAME_LOCATION_ID = "LocationId";
    public static final String TAG_NAME_ICON_CODE = "IconCode";
    public static final String TAG_NAME_WEATHER_ICON = "WeatherIcon";
    public static final String TAG_NAME_CITY = "City";
    public static final String TAG_NAME_LATEST_REFRESH_DATE = "LatestRefreshDate";
    public static final String TAG_NAME_TEMPERATURE_C = "TemperatureC";
    public static final String TAG_NAME_TEMPERATURE_F = "TemperatureF";
    
    public static final String IMAGE_KEY_WEATHER_ICON = "WeatherIcon_";
    
    private String locationId;
    private String iconCode;
    private Image weatherIcon;
    private String cityName;
    private Date latestRefreshDate;
    private String temperatureC;
    private String temperatureF;
    
    public String getLocationId() {
        return locationId;
    }
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
    
    public String getIconCode() {
        return iconCode;
    }
    public void setIconCode(String iconCode) {
        this.iconCode = iconCode;
    }
    
    public Image getWeatherIcon() {
        return weatherIcon;
    }
    public void setWeatherIcon(Image weatherIcon) {
        this.weatherIcon = weatherIcon;
    }
    
    public String getCityName() {
        return cityName;
    }
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
    
    public Date getLatestRefreshDate() {
        return latestRefreshDate;
    }
    public void setLatestRefreshDate(Date latestRefreshDate) {
        this.latestRefreshDate = latestRefreshDate;
    }
    
    public String getTemperatureC() {
        return temperatureC;
    }
    public void setTemperatureC(String temperatureC) {
        this.temperatureC = temperatureC;
    }
    
    public String getTemperatureF() {
        return temperatureF;
    }
    public void setTemperatureF(String temperatureF) {
        this.temperatureF = temperatureF;
    }
}

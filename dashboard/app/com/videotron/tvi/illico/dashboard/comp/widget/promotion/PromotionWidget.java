package com.videotron.tvi.illico.dashboard.comp.widget.promotion;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.comp.dashboard.DashboardPreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;

public class PromotionWidget extends Widget {
    private static final long serialVersionUID = 1L;
    public static final int PROMOTION_TYPE_DEFAULT_PLACEHOLDER = 1;
    public static final int PROMOTION_TYPE_NO_INTERACTION = 2;
    public static final int PROMOTION_TYPE_TV_CHANNEL_LINK = 3;
    public static final int PROMOTION_TYPE_TV_SUBSCRIPTION = 4;
    public static final int PROMOTION_TYPE_PPV_LINK = 5;
    public static final int PROMOTION_TYPE_UNBOUND = 6;
    public static final int PROMOTION_TYPE_BOUND = 7;
    public static final int PROMOTION_TYPE_VOD_CATEGORY = 8;
    public static final int PROMOTION_TYPE_VOD_BUNDLE = 9;
    public static final int PROMOTION_TYPE_VOD_TITLE = 10;

    private static Image imgFocus;
    private static Image imgUp;
    private static Image imgDown;
    private static Image imgLeft;
    private static Image imgRight;
//    private static Image imgShadow;
//    private static Image[] imgDefaultBgs = {
//        dataCenter.getImage("shared/11_banner_01.png"),
//        dataCenter.getImage("shared/11_banner_02.png"),
//        dataCenter.getImage("shared/11_banner_03.png"),
//        dataCenter.getImage("shared/11_banner_04.png"),
//    };

    private Font f16 = FontResource.BLENDER.getFont(16, true);
    private Font f18 = FontResource.BLENDER.getFont(18, true);
    private Font f22 = FontResource.BLENDER.getFont(22, true);
    private FontMetrics fm16 = FontResource.getFontMetrics(f16);
    private FontMetrics fm18 = FontResource.getFontMetrics(f18);
    private FontMetrics fm22 = FontResource.getFontMetrics(f22);
    private Color c050050050 = new Color(50, 50, 50);
    private Color c086086086 = new Color(86, 86, 86);
    private Color c110110110 = new Color(110, 110, 110);
    private Image[] imgBgList;
    private int imgBgListCount;
    private int currentIndex;
    /*****************************************************************
     * variables - Timer-related
     *****************************************************************/
    private CycleTimer cycleTimer;

    private boolean started = false;

    private DataCenter dataCenter = DataCenter.getInstance();


    public PromotionWidget() {
        Log.printDebug("[PromotionWidget] created : " + Integer.toHexString(hashCode()));
        init();
    }

    public void init() {
    }

    public void dispose() {
    }

    protected synchronized void startWidget() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromotionWidget.startWidget] " + Integer.toHexString(hashCode()));
        }
        imgFocus = dataCenter.getImage("shared/00_banner_focus.png");
        imgUp = dataCenter.getImage("shared/00_widget_t.png");
        imgDown = dataCenter.getImage("shared/00_widget_b.png");
        imgLeft = dataCenter.getImage("shared/02_ars_l.png");
        imgRight = dataCenter.getImage("shared/02_ars_r.png");
//        imgShadow = dataCenter.getImage("shared/00_banner_shadow.png");
//        imgDefaultBgs = new Image[] {
//            dataCenter.getImage("shared/11_banner_01.png"),
//            dataCenter.getImage("shared/11_banner_02.png"),
//            dataCenter.getImage("shared/11_banner_03.png"),
//            dataCenter.getImage("shared/11_banner_04.png"),
//        };

        FrameworkMain.getInstance().getImagePool().waitForAll();
        PromotionWidgetDataManager.getInstance().init();
        PromotionWidgetDataManager.getInstance().start();
        curReqStatus = REQUEST_STATUS_LOADING;
        imgBgListCount = 0;
        this.started = true;

        new Thread() {
            public void run() {
                try{
                    byte[][] widgetBgImgSrcList = PromotionWidgetDataManager.getInstance().getWidgetBackgroundImageSourceList(bgServerInfo, wUnits);
                    if (widgetBgImgSrcList != null) {
                        String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
                        imgBgListCount = widgetBgImgSrcList.length;
                        imgBgList = new Image[imgBgListCount];
                        for (int i=0; i<imgBgListCount; i++) {
                            if (widgetBgImgSrcList[i] == null) {
                                continue;
                            }
                            if (wUnits[i] == null) {
                                continue;
                            }
                            String imgName = null;
                            if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                                imgName = wUnits[i].getImageNameFrench();
                            } else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                                imgName = wUnits[i].getImageNameEnglish();
                            }
                            if (imgName == null) {
                                continue;
                            }
                            imgBgList[i] = FrameworkMain.getInstance().getImagePool().createImage(widgetBgImgSrcList[i], imgName);
                        }
                        FrameworkMain.getInstance().getImagePool().waitForAll();
                    }

                    curReqStatus = REQUEST_STATUS_VALID;

                    synchronized (PromotionWidget.this) {
                        if (!started) {
                            stopTimerAndflushImage();
                            return;
                        }
                    }

                    if (cycleTimeSecs > 0) {
                        if (cycleTimer == null) {
                            cycleTimer = new CycleTimer(cycleTimeSecs * Rs.MILLIS_TO_SECOND);
                        }
                        cycleTimer.startTimer();
                    }
                    currentIndex = 0;
                    repaint();
                    return;
                }catch(Exception ex) {
                    imgBgListCount = 0;
                    Log.print(ex);
                }
                curReqStatus = REQUEST_STATUS_INVALID;
            }
        }.start();
    }

    private void stopTimerAndflushImage() {
        if (cycleTimer != null) {
            cycleTimer.stopTimer();
        }
        Image[] oldList = imgBgList;
        if (oldList != null) {
            for (int i = 0; i < oldList.length; i++) {
                if (oldList[i] != null) {
                    oldList[i].flush();
                }
            }
        }
        imgBgList = null;
    }

    protected synchronized void stopWidget() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromotionWidget.stopWidget] " + Integer.toHexString(hashCode()));
        }
        stopTimerAndflushImage();

        if (wUnits != null) {
            String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
            for (int i=0; i<wUnits.length; i++) {
                if (wUnits[i] == null) {
                    continue;
                }
                String imgName = null;
                if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                    imgName = wUnits[i].getImageNameFrench();
                } else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                    imgName = wUnits[i].getImageNameEnglish();
                }
                if (imgName == null) {
                    continue;
                }
                dataCenter.removeImage(imgName);
            }
        }
        dataCenter.removeImage("shared/00_banner_focus.png");
        imgFocus = null;
        dataCenter.removeImage("shared/00_widget_t.png");
        imgUp = null;
        dataCenter.removeImage("shared/00_widget_b.png");
        imgDown = null;
        dataCenter.removeImage("shared/02_ars_l.png");
        imgLeft = null;
        dataCenter.removeImage("shared/02_ars_r.png");
        imgRight = null;
//        dataCenter.removeImage("shared/00_banner_shadow.png");
//        imgShadow = null;
//        dataCenter.removeImage("shared/11_banner_01.png");
//        dataCenter.removeImage("shared/11_banner_02.png");
//        dataCenter.removeImage("shared/11_banner_03.png");
//        dataCenter.removeImage("shared/11_banner_04.png");
//        imgDefaultBgs = null;
        PromotionWidgetDataManager.getInstance().stop();
        PromotionWidgetDataManager.getInstance().dispose();
        this.started = false;
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_OK:
                executeWidgetUnit(currentIndex);
                return true;
            case Rs.KEY_UP:
                if (imgBgListCount <= 1 || currentIndex == 0) {
                    return true;
                }
                currentIndex--;
                repaint();
                return true;
            case Rs.KEY_DOWN:
                if (imgBgListCount <= 1 || currentIndex == imgBgListCount - 1) {
                    return true;
                }
                currentIndex++;
                repaint();
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
//        if (imgShadow != null) {
//            g.drawImage(imgShadow, 0, 103, this);
//        }
        if (isSelected && imgFocus != null) {
            g.drawImage(imgFocus, 13, 15, this);
        }
        switch(curReqStatus) {
            case REQUEST_STATUS_LOADING:
                g.setColor(c086086086);
                g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                g.setColor(c050050050);
                g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                String txtLoading = dataCenter.getString("TxtCommon_Loading");
                if (txtLoading != null) {
                    g.setFont(f22);
                    g.setColor(c110110110);
                    g.drawString(txtLoading, 110 - fm22.stringWidth(txtLoading) / 2, 70);
                    g.setColor(c050050050);
                    g.drawString(txtLoading, 110 - fm22.stringWidth(txtLoading) / 2 - 1, 69);
                }
                return;
            case REQUEST_STATUS_INVALID:
                g.setColor(c086086086);
                g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                g.setColor(c050050050);
                g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                String txtInvalidData = dataCenter.getString("TxtCommon_Invalid_Data");
                if (txtInvalidData != null) {
                    g.setFont(f18);
                    g.setColor(c110110110);
                    g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2, 58);
                    g.setColor(c050050050);
                    g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2 - 1, 57);
                    g.setFont(f16);
                    String invalidData = "[Name-Id] : ["+name + "-" +id+"]";
                    g.setColor(c110110110);
                    g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2, 76);
                    g.setColor(c050050050);
                    g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2 - 1, 75);
                }
                return;
        }
        if (isSelected) {
            if (imgBgListCount > 1) {
                if (currentIndex != 0) {
                    g.drawImage(imgUp, 96, 3, this);
                }
                if (currentIndex != imgBgListCount - 1) {
                    g.drawImage(imgDown, 96, 94, this);
                }
            }
        }
        if (imgBgListCount > currentIndex && imgBgList[currentIndex] != null) {
            g.drawImage(imgBgList[currentIndex], 21, 9, IMAGE_WIDTH, IMAGE_HEIGHT, this);
        } else {
            g.setColor(c086086086);
            g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
            g.setColor(c050050050);
            g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
            String txtInvalidData = dataCenter.getString("TxtCommon_Invalid_Data");
            if (txtInvalidData != null) {
                g.setFont(f18);
                g.setColor(c110110110);
                g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2, 58);
                g.setColor(c050050050);
                g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2 - 1, 57);
                g.setFont(f16);
                String invalidData = "[Name-Id] : ["+name + "-" +id+"]";
                g.setColor(c110110110);
                g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2, 76);
                g.setColor(c050050050);
                g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2 - 1, 75);
            }
        }
        if (isSelected) {
            if (!isFirstIndex() && imgLeft!= null) {
                g.drawImage(imgLeft, 0, 48, this);
            }
            if (!isLastIndex() && imgRight!= null) {
                g.drawImage(imgRight, 201, 48, this);
            }
        }
    }
    public void focusChanged(boolean isSelected) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WidgetPromotion.focusChanged]isSelected : "+isSelected);
        }
        if (cycleTimer != null) {
            if (isSelected) {
                cycleTimer.stopTimer();
            } else {
                cycleTimer.startTimer();
            }
        }
    }
//    public boolean isValidWidget() {
//        if (imgWidgetBGs == null) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[WidgetPromotion.isValidWidget]imgWidgetBGs is invalid.");
//            }
//            return false;
//        }
//        if (imgWidgetBGs.length != imageCount) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[WidgetPromotion.isValidWidget]Image count is invalid.");
//            }
//            return false;
//        }
//        for (int i=0; i<imageCount; i++) {
//            if (imgWidgetBGs[i] == null) {
//                if (Log.DEBUG_ON) {
//                    Log.printDebug("[WidgetPromotion.isValidWidget]Image BG["+i+"] is invalid.");
//                }
//                return false;
//            }
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[WidgetPromotion.isValidWidget]This widget is valid.");
//        }
//        return true;
//    }
    /*****************************************************************
     * class - TVTimerSpec-related
     *****************************************************************/
    class CycleTimer implements TVTimerWentOffListener{
        private TVTimerSpec timer;

        public CycleTimer(int timerMins) {
            setTimer(timerMins);
        }

        private void setTimer(int timerMillis) {
            //Condition
            if (!isCyclable) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WidgetPromotion.setTimer]This widget don't allow cycling. [isCyclable variable is false]");
                }
                return;
            }
            if (cycleTimeSecs <= 0) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WidgetPromotion.setTimer]This widget don't allow cycling. [Cycling millis is 0 or less.]");
                }
                return;
            }
            if (imgBgListCount <= 1) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WidgetPromotion.setTimer]This widget don't allow cycling. [Background image count is 1 or less.]");
                }
                return;
            }
            Log.printDebug("june - [WidgetPromotion.setTimer] scheduling");

            if (timer == null) {
                timer = new TVTimerSpec();
            }
            timer.setDelayTime(timerMillis);
            timer.setRepeat(false);
        }
        public synchronized void startTimer() {
            stopTimer();
            if (timer != null) {
                timer.addTVTimerWentOffListener(this);
                try {
                    TVTimer.getTimer().scheduleTimerSpec(timer);
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }
        private synchronized void stopTimer() {
            Log.printDebug("[WidgetPromotion.stopTimer]");
            if (timer != null) {
                timer.removeTVTimerWentOffListener(this);
                TVTimer.getTimer().deschedule(timer);
            }
        }
        public void timerWentOff(TVTimerWentOffEvent event) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WidgetPromotion.timerWentOff]Start.");
            }
            currentIndex = (currentIndex+1) % imgBgListCount;
            startTimer();
            repaint();
        }
    }
}

package com.videotron.tvi.illico.dashboard.comp.dashboard;

import java.awt.Container;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.log.Log;

public class Dashboard extends Container implements AnimationRequestor, TVTimerWentOffListener {
    private static final long serialVersionUID = 4798846576693875740L;
    /*****************************************************************
     * methods - SharedMemory-related
     *****************************************************************/
    public static final String SMKEY_DASHBOARD_IB_CONFIG_LIST = "SMK_DASHBOARD_IB";
    public static final String SMKEY_DASHBOARD_OOB_ORDER_LIST = "SMK_DASHBOARD_OOB";
    public static final String SMKEY_DASHBOARD_TAG = "SMK_DASHBOARD_";
    public static final String SMKEY_DASHBOARD_ID = "SMK_DASHBOARD_ID";
    public static final String SMKEY_DASHBOARD_TYPE = "SMK_DASHBOARD_TYPE";
    public static final String SMKEY_DASHBOARD_MAX_WIDGET_TYPES = "SMK_DASHBOARD_MAX_WIDGET_TYPES";
    public static final String SMKEY_DASHBOARD_MAX_VISIBLE_WIDGET_COUNT = "SMK_DASHBOARD_MAX_VISIBLE_WIDGET_COUNT";
    public static final String SMKEY_DASHBOARD_INACTIVE_TIMEOUT_SECS = "SMK_DASHBOARD_INACTIVE_TIMEOUT_SECS";
    public static final String SMKEY_DASHBOARD_LOCATION_X = "SMK_DASHBOARD_LOCATION_X";
    public static final String SMKEY_DASHBOARD_LOCATION_Y = "SMK_DASHBOARD_LOCATION_Y";
    public static final String SMKEY_DASHBOARD_SIZE_W = "SMK_DASHBOARD_SIZE_W";
    public static final String SMKEY_DASHBOARD_SIZE_H = "SMK_DASHBOARD_SIZE_H";
    public static final String SMKEY_WIDGET_ORDERS = "SMK_WIDGET_ORDERS_";

    public static final int DASHBOARD_TYPE_FULL_MENU = 0;
    public static final int DASHBOARD_TYPE_DASHBOARD = 1;
    public static final int DASHBOARD_TYPE_CUSTOM = 2;

    public static final String[] SMK_DASHBOARD_TYPES = {
        "SMK_TYPE_FULL_MENU",
        "SMK_TYPE_DASHBOARD",
        "SMK_TYPE_CUSTOM"
    };
    /*****************************************************************
     * constants - Anmation-related
     *****************************************************************/
    public static final int WIDGET_WIDTH = 218;
    public static final int WIDGET_HEIGHT = 130;
    public static final int WIDGET_GAP = 21;
    public static final int WIDGET_IMAGE_WIDTH = 176;
    private int overlappedWth;
    private int widgetGap;
    public static final int FULL_MEUN_WIDGET_MARGIN = 5;
    public static final int EFFECT_FRAME_COUNT = 7;
    private Effect[] showingEffects;
    /*****************************************************************
     * variables - Widget Data
     *****************************************************************/
    private int type;
//    private int maxWidgetTypes;
    private int maxVisibleWidgetCount;
    private int inactivityTimeoutSecs;
    private int locationX;
    private int locationY;
    private int sizeW;
    private int sizeH;
    /*****************************************************************
     * variables - TVTimer
     *****************************************************************/
    /** The timer full menu. */
    private TVTimerSpec timerDashboard;

    private Vector widgetVector;
    private int totalWidgetCount;
    private int widgetVectorStartIdx;
    private int widgetVectorDisplayCount;
    private int currentIndex;

    private boolean isDashboardArea;
    private boolean isExitFirstIndex;
    private boolean isStaticDashboard;
    private boolean isInitFocusDashboard;
    private boolean isInactiveAnimation;
    /*****************************************************************
     * variables - TVTimer
     *****************************************************************/
    private DashboardListener listener;

    public Dashboard() {
        widgetVector = new Vector();
        DashboardPreferenceProxy.getInstance().init();
    }
    public void dispose() {
        DashboardPreferenceProxy.getInstance().dispose();
        stop();
        if (widgetVector != null) {
            widgetVector.clear();
            widgetVector = null;
        }
    }
    public void start(boolean isInitFocusDashboard) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.start]");
        }
        DashboardPreferenceProxy.getInstance().start();
        this.isInitFocusDashboard = isInitFocusDashboard;
        this.setVisible(false);
        DataCenter.getInstance().put("TxtCommon_Loading_fr", "Chargement");
        DataCenter.getInstance().put("TxtCommon_Loading_en", "Loading");
        DataCenter.getInstance().put("TxtCommon_Invalid_Data_fr", "Des données non valides.");
        DataCenter.getInstance().put("TxtCommon_Invalid_Data_en", "Invalid data.");
        currentIndex = 0;
        int dashboardW = getWidth();
        overlappedWth = ((WIDGET_WIDTH * maxVisibleWidgetCount) - dashboardW) / (maxVisibleWidgetCount - 1);
        widgetGap = WIDGET_WIDTH - overlappedWth;
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.start]Dashboard width : " + dashboardW);
            Log.printDebug("[Dashboard.start]Overlapped width : " + overlappedWth);
            Log.printDebug("[Dashboard.start]Max visible widget count  : " + maxVisibleWidgetCount);
            Log.printDebug("[Dashboard.start]Gap between widget (Widget width) : " + widgetGap + " ("
                    + WIDGET_WIDTH + ")");
        }
        startWidgets();
        setWidget();
        createEffects();
        // start effect from index 0 sequencely.
        if (showingEffects != null && showingEffects.length > 0 && !isInactiveAnimation) {
            try {
                showingEffects[0].start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            animationEndedShowEffect();
        }
        // Create Timer
        if (!isStaticDashboard) {
            if (inactivityTimeoutSecs > 0) {
                setTimer(inactivityTimeoutSecs);
                startTimer();
            }
        }
    }
    public void stop() {
        DashboardPreferenceProxy.getInstance().stop();
        stopWidgets();
        stopTimer();
    }
    private void startWidgets() {
        if (widgetVector != null) {
            int widgetVectorSz = widgetVector.size();
            for (int i=0; i<widgetVectorSz; i++) {
                Widget widget = (Widget)widgetVector.get(i);
                if (widget == null) {
                    continue;
                }
                widget.start();
                widget.setSelected(false);
            }
        }
    }
    private void stopWidgets() {
        Log.printDebug("[Dashboard.stopWidgets]");
        if (widgetVector != null) {
            int widgetVectorSz = widgetVector.size();
            for (int i=0; i<widgetVectorSz; i++) {
                Widget widget = (Widget)widgetVector.get(i);
                if (widget == null) {
                    continue;
                }
                widget.setSelected(false);
                widget.stop();
            }
        }
        removeAll();
        widgetVectorStartIdx = 0;
        widgetVectorDisplayCount = 0;
    }
    /*****************************************************************
     * methods - Key-related
     *****************************************************************/
    public boolean keyAction (int keyCode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.keyAction]keyCode : "+keyCode);
        }
        startTimer();
        int tempValue = -1;
        switch(keyCode) {
            case HRcEvent.VK_UP:
                Widget widgetU = (Widget)widgetVector.get(currentIndex);
                if (widgetU == null) {
                    return true;
                }
                widgetU.keyAction(keyCode);
                repaint();
                return true;
            case HRcEvent.VK_DOWN:
                Widget widgetD = (Widget)widgetVector.get(currentIndex);
                if (widgetD == null) {
                    return true;
                }
                widgetD.keyAction(keyCode);
                repaint();
                return true;
            case HRcEvent.VK_LEFT:
                if (currentIndex == 0) {
                    if (isExitFirstIndex) {
                        Widget widgetL = (Widget)widgetVector.get(currentIndex);
                        if (widgetL != null) {
                            widgetL.setSelected(false);
                        }
                        if (listener != null) {
                            listener.focusLost();
                        }
                    }
                    return true;
                }
                tempValue = currentIndex-1;
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Dashboard.keyAction]tempValue : "+tempValue);
                }
                if (tempValue == currentIndex) {
                    return true;
                }
                Widget widgetL = (Widget)widgetVector.get(currentIndex);
                if (widgetL != null) {
                    widgetL.setSelected(false);
                }
                if (tempValue == -1) {
                    if (isExitFirstIndex) {
                        if (listener != null) {
                            listener.focusLost();
                        }
                    }
                    return true;
                }
                setCurrentIndex(tempValue);
                setWidget();
                repaint();
                return true;
            case HRcEvent.VK_RIGHT:
                if (currentIndex == totalWidgetCount-1) {
                    return true;
                }
                tempValue = currentIndex + 1;
                if (tempValue == currentIndex || tempValue == -1) {
                    return true;
                }
                Widget widgetR = (Widget)widgetVector.get(currentIndex);
                if (widgetR != null) {
                    widgetR.setSelected(false);
                }
                setCurrentIndex(tempValue);
                setWidget();
                repaint();
                return true;
            case HRcEvent.VK_ENTER:
                Widget widgetOk = (Widget)widgetVector.get(currentIndex);
                if (widgetOk != null) {
                    Rectangle r = widgetOk.getBounds();
                    ClickingEffect effect = new ClickingEffect(this, 5);
                    effect.updateBackgroundBeforeStart(false);
                    effect.start(r.x + 13, r.y + 15, r.width - 26, r.height - 30);
                    widgetOk.keyAction(keyCode);
                }
                return true;
        }
        return false;
    }
    /*****************************************************************
     * methods - DashboardListener-related
     *****************************************************************/
    public void setDashboardListener(DashboardListener listener) {
        this.listener = listener;
    }
    public void removeDashboardListener() {
        listener = null;
    }
    /*****************************************************************
     * methods - GetSet-related
     *****************************************************************/
    public void setBounds(int x, int y, int w, int h) {
        super.setBounds(x, y, w, h);
        locationX = x;
        locationY = y;
        sizeW = w;
        sizeH = h;
    }
    private void createEffects() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.createEffects]Max visible widget count : "+maxVisibleWidgetCount);
        }
        if (maxVisibleWidgetCount <= 0) {
            showingEffects = null;
            return;
        }
        int widgetVectorDisplayCount = 0;
        if (totalWidgetCount <= maxVisibleWidgetCount) {
            widgetVectorDisplayCount = totalWidgetCount;
        } else {
            widgetVectorDisplayCount = maxVisibleWidgetCount;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.createEffects]Dashboard display count : "+widgetVectorDisplayCount);
        }
        showingEffects = new MovingEffect[widgetVectorDisplayCount];
        for (int i = 0; i < widgetVectorDisplayCount; i++) {
            int effectX = (widgetGap * i) + WIDGET_GAP;
            int fromX = locationX + effectX;
            int toX = locationX + effectX;
            int fromY = locationY + (sizeH / 2);
            int toY = locationY;
            showingEffects[i] = new MovingEffect(this, EFFECT_FRAME_COUNT, new Point(0, (sizeH / 2)),
                    new Point(0, 0), MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
            showingEffects[i].setTargetBounds(effectX, 0, WIDGET_IMAGE_WIDTH, WIDGET_HEIGHT);
            showingEffects[i].updateBackgroundBeforeStart(i == 0);
            if (Log.DEBUG_ON) {
                Log.printDebug("[Dashboard.createEffects]Effect["+i+"] target bound : ("+effectX+", 0, "+WIDGET_IMAGE_WIDTH+", "+WIDGET_HEIGHT+") ");
                Log.printDebug("[Dashboard.createEffects]Effect["+i+"] move : X ("+fromX+" -> "+toX+"), Y ("+fromY+" -> "+toY+") ");
            }
        }
    }
    public void setWidget() {
        int tempWidgetVectorStartIdx = 0;
        int tempWidgetVectorDisplayCount = 0;
        if (widgetVector != null) {
            totalWidgetCount = widgetVector.size();
        }
        if (totalWidgetCount <= maxVisibleWidgetCount) {
            tempWidgetVectorStartIdx = 0;
            tempWidgetVectorDisplayCount = totalWidgetCount;
        } else {
            if (widgetVectorDisplayCount == 0) {
                tempWidgetVectorStartIdx = 0;
            } else if (widgetVectorStartIdx>currentIndex) {
                tempWidgetVectorStartIdx = widgetVectorStartIdx-1;
            } else if (widgetVectorStartIdx+maxVisibleWidgetCount<=currentIndex) {
                tempWidgetVectorStartIdx = widgetVectorStartIdx+1;
            } else {
                tempWidgetVectorStartIdx = widgetVectorStartIdx;
            }
            tempWidgetVectorDisplayCount = maxVisibleWidgetCount;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.setWidget]tempWidgetVectorStartIdx : "+tempWidgetVectorStartIdx);
            Log.printDebug("[Dashboard.setWidget]tempWidgetVectorDisplayCount : "+tempWidgetVectorDisplayCount);
            Log.printDebug("[Dashboard.setWidget]totalwidgetCount : "+totalWidgetCount);
            Log.printDebug("[Dashboard.setWidget]locationX : "+locationX);
            Log.printDebug("[Dashboard.setWidget]locationY : "+locationY);
            Log.printDebug("[Dashboard.setWidget]sizeH : "+sizeH);
            Log.printDebug("[Dashboard.setWidget]sizeW : "+sizeW);
        }
        //remove Before widget
        for (int i=0; i<widgetVectorDisplayCount; i++) {
            int idx = widgetVectorStartIdx + i;
            Widget widget = (Widget)widgetVector.get(idx);
            if (widget == null) {
                continue;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[Dashboard.setWidget]Widget["+idx+"] is removed from dashboard.");
            }
            remove(widget);
        }
        widgetVectorStartIdx = tempWidgetVectorStartIdx;
        widgetVectorDisplayCount = tempWidgetVectorDisplayCount;
        //add Current widget
        for (int i=widgetVectorStartIdx; i<widgetVectorStartIdx+widgetVectorDisplayCount; i++) {
            int widgetX = widgetGap*(i-widgetVectorStartIdx);
            Widget widget = (Widget)widgetVector.get(i);
            if (widget == null) {
                continue;
            }
            if (i == 0) {
                widget.setFirstIndex(true);
            }
            if (i == totalWidgetCount-1) {
                widget.setLastIndex(true);
            }
            widget.setBounds(widgetX, 0, WIDGET_WIDTH, WIDGET_HEIGHT);
            if (Log.DEBUG_ON) {
                Log.printDebug("[Dashboard.setWidget]Widget["+i+"]'s bound ("+widgetX+", 0, "+WIDGET_WIDTH+", "+WIDGET_HEIGHT+")");
            }
            if (i != currentIndex) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Dashboard.setWidget]Widget["+i+"] is added to dashboard.");
                }
                add(widget);
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.setWidget]Current index of dashboard : "+currentIndex);
        }
        if (totalWidgetCount > 0) {
            Widget widget = (Widget)widgetVector.get(currentIndex);
            if (widget != null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Dashboard.setWidget]current index Widget["+currentIndex+"] is added to dashboard.");
                }
                add(widget, 0);
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[Dashboard.setWidget]end");
        }
    }
    public void setDashboardArea(boolean isDashboardArea) {
        this.isDashboardArea = isDashboardArea;
        if (isDashboardArea) {
            setCurrentIndex(0);
            setWidget();
        } else {
            ((Widget)widgetVector.get(currentIndex)).setSelected(false);
        }
        repaint();
    }
    public boolean isDashboardArea() {
        return isDashboardArea;
    }
    public void setExitFirstIndex(boolean isExitFirstIndex) {
        this.isExitFirstIndex = isExitFirstIndex;
    }
    public void setCurrentIndex(int currentIndex) {
        this.currentIndex = currentIndex;
        try{
            ((Widget)widgetVector.get(currentIndex)).setSelected(true);
        }catch(Exception ignore) {
        }
    }
    public void addWidget(Widget widget) {
        if (widget == null) {
            return;
        }
        widgetVector.add(widget);
    }
    public void removeWidget(Widget widget) {
        if (widget == null) {
            return;
        }
        widgetVector.remove(widget);
    }
    public void flushAllWidget() {
        Log.printDebug("[Dashboard.flushAllWidget]");
        if (widgetVector != null) {
            int widgetVectorSize = widgetVector.size();
            for (int i=0; i<widgetVectorSize; i++) {
                Widget widget = (Widget)widgetVector.get(i);
                if (widget == null) {
                    continue;
                }
                widget.stop();
                widget = null;
            }
            widgetVector.clear();
            widgetVector = null;
        }
    }
    public void setStaticDashboard(boolean isStaticDashboard) {
        this.isStaticDashboard=isStaticDashboard;
    }

    /*****************************************************************
     * methods - GetSet-related
     *****************************************************************/
    public Widget getSelectedWidget() {
        if (widgetVector == null) {
            return null;
        }
        Widget widget = (Widget)widgetVector.get(currentIndex);
        return widget;
    }
//    public int getMaxVisibleWidgetCount() {
//        return maxVisibleWidgetCount;
//    }
    public void setMaxVisibleWidgetCount(int maxVisibleWidgetCount) {
        this.maxVisibleWidgetCount = maxVisibleWidgetCount;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public int getLocationX() {
        return locationX;
    }
    public void setLocationX(int locationX) {
        this.locationX = locationX;
    }
    public int getLocationY() {
        return locationY;
    }
    public void setLocationY(int locationY) {
        this.locationY = locationY;
    }
    public int getSizeW() {
        return sizeW;
    }
    public void setSizeW(int sizeW) {
        this.sizeW = sizeW;
    }
    public int getSizeH() {
        return sizeH;
    }
    public void setSizeH(int sizeH) {
        this.sizeH = sizeH;
    }
    public int getInactivityTimeoutSecs() {
        return inactivityTimeoutSecs;
    }
    public void setInactivityTimeoutSecs(int inactivityTimeoutSecs) {
        this.inactivityTimeoutSecs = inactivityTimeoutSecs;
    }
//    public int getMaxWidgetTypes() {
//        return maxWidgetTypes;
//    }
//    public void setMaxWidgetTypes(int maxWidgetTypes) {
//        this.maxWidgetTypes = maxWidgetTypes;
//    }
    public int getAddedWidgetCount() {
        int addedWidgetCount = 0;
        if (widgetVector != null) {
            addedWidgetCount = widgetVector.size();
        }
        return addedWidgetCount;
    }
    public void setInactiveAnimation(boolean isInactiveAnimation) {
        this.isInactiveAnimation=isInactiveAnimation;
    }
    /**
     * Called when animation ended.
     * @param effect
     *            ended effect
     */
    public void animationEnded(Effect effect) {
        if (effect == null) {
            return;
        }
        if (showingEffects == null) {
            return;
        }
        int showingEffectsLth = showingEffects.length;
        if (effect.equals(showingEffects[showingEffectsLth-1])) {
            animationEndedShowEffect();
        }else {
            for (int i=0; i<showingEffectsLth; i++) {
                if (effect.equals(showingEffects[i])) {
                    if (i != showingEffects.length-1) {
                        showingEffects[i+1].start();
                    }
                    break;
                }
            }
        }
    }
    private void animationEndedShowEffect() {
        if (isInitFocusDashboard) {
            isDashboardArea = true;
            setCurrentIndex(0);
        }
        this.setVisible(true);
        listener.animationEnded();
    }
    public void animationStarted(Effect effect) {
    }
    public boolean skipAnimation(Effect effect) {
        return false;
    }
    /*****************************************************************
     * TVTimerSpec-related
     *****************************************************************/
    public void timerWentOff(TVTimerWentOffEvent event) {
        Widget widget = (Widget)widgetVector.get(currentIndex);
        if (widget != null) {
            widget.setSelected(false);
        }
        listener.timeout();
    }
    public void setTimer(int timerSecs) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[TimerDashboard.setTimer]Dashboard inactive sec is "+timerSecs+" secs.");
        }
        if (timerDashboard == null) {
            timerDashboard = new TVTimerSpec();
        }
        timerDashboard.setDelayTime(timerSecs * Rs.MILLIS_TO_SECOND);
        timerDashboard.setRepeat(false);
        timerDashboard.addTVTimerWentOffListener(this);
    }
    public void startTimer() {
        stopTimer();
        if (timerDashboard != null) {
            try {
                TVTimer.getTimer().scheduleTimerSpec(timerDashboard);
            } catch (Exception ignore) {
                ignore.printStackTrace();
            }
        }
    }
    public void stopTimer() {
        if (timerDashboard != null) {
            TVTimer.getTimer().deschedule(timerDashboard);
        }
    }
}

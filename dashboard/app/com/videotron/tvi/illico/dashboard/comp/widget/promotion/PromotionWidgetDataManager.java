package com.videotron.tvi.illico.dashboard.comp.widget.promotion;

import java.util.Hashtable;

import com.videotron.tvi.illico.dashboard.comp.dashboard.DashboardPreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.widget.ServerInfo;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.comp.widget.WidgetUnit;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;


public class PromotionWidgetDataManager {
    private static PromotionWidgetDataManager instance;

    public static synchronized PromotionWidgetDataManager getInstance() {
        if (instance == null) {
            instance = new PromotionWidgetDataManager();
        }
        return instance;
    }
    private PromotionWidgetDataManager() {
    }
    public void init() {
    }
    public void dispose() {
    }
    public void start() {
    }
    public void stop() {
    }
    public byte[][] getWidgetBackgroundImageSourceList(ServerInfo bgServerInfo, WidgetUnit[] wUnits) {
        if (wUnits == null) {
            return null;
        }
        int wUnitsLth = wUnits.length;
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromoWidgetDataMgr.getWidgetBackgroundImage]Widget image name count : "+wUnitsLth);
        }
        byte[][] widgetBgImgSrcList = new byte[wUnitsLth][];
        Hashtable widgetBgImgHash = (Hashtable)SharedMemory.getInstance().get(Widget.SMKEY_BG_IMAGE_SRC_LIST);
        String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
        if (Log.DEBUG_ON) {
            Log.printDebug("[PromoWidgetDataMgr.getWidgetBackgroundImage]Current language : "+curLang);
        }
        for (int i=0; i<wUnitsLth; i++) {
            if (wUnits[i] == null) {
                continue;
            }
            String widgetImgName = null;
            if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                widgetImgName = wUnits[i].getImageNameFrench();
            } else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                widgetImgName = wUnits[i].getImageNameEnglish();
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[PromoWidgetDataMgr.getWidgetBackgroundImage]Widget image name-"+i+" : "+widgetImgName);
            }
            if (widgetImgName == null) {
                continue;
            }
            byte[] wBgImgSrc = null;
            if (widgetBgImgHash != null) {
                byte[] tempWBgImgSrc = (byte[])widgetBgImgHash.get(widgetImgName);
                if (tempWBgImgSrc != null) {
                    wBgImgSrc = (byte[])tempWBgImgSrc.clone();
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[PromoWidgetDataMgr.getWidgetBackgroundImage]Widget background image from shared memory-"+i+" : "+wBgImgSrc);
            }
            if (wBgImgSrc == null) {
                wBgImgSrc = PromotionWidgetRPManager.getInstance().getWidgetBackgroundImage(bgServerInfo, widgetImgName);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[PromoWidgetDataMgr.getWidgetBackgroundImage]Widget background image from server-"+i+" : "+wBgImgSrc);
                }
                if (wBgImgSrc != null) {
                    widgetBgImgHash.put(widgetImgName, wBgImgSrc);
                    SharedMemory.getInstance().remove(Widget.SMKEY_BG_IMAGE_SRC_LIST);
                    SharedMemory.getInstance().put(Widget.SMKEY_BG_IMAGE_SRC_LIST, widgetBgImgHash);
                }
            }
            widgetBgImgSrcList[i] = wBgImgSrc;
        }
        return widgetBgImgSrcList;
    }
}

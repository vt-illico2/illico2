package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.comp.dashboard.DashboardPreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class WeatherWidget extends Widget {
    private static final long serialVersionUID = 1L;
    /*****************************************************************
     * variables - Resources-related
     *****************************************************************/
    private Font f16 = FontResource.BLENDER.getFont(16, true);
    private Font f18 = FontResource.BLENDER.getFont(18, true);
    private Font f19 = FontResource.BLENDER.getFont(19, true);
    private Font f22 = FontResource.BLENDER.getFont(22, true);
    private Font f30 = FontResource.BLENDER.getFont(30, true);
    private FontMetrics fm16 = FontResource.getFontMetrics(f16);
    private FontMetrics fm18 = FontResource.getFontMetrics(f18);
    private FontMetrics fm19 = FontResource.getFontMetrics(f19);
    private FontMetrics fm22 = FontResource.getFontMetrics(f22);
    private FontMetrics fm30 = FontResource.getFontMetrics(f30);
    private Color c000019022 = new Color(0, 19, 22);
    private Color c050050050 = new Color(50, 50, 50);
    private Color c086086086 = new Color(86, 86, 86);
    private Color c110110110 = new Color(110, 110, 110);
    private Color c255255255 = new Color(255, 255, 255);

    private Image imgTempC;
    private Image imgTempF;
    private static Image imgFocus;
    private static Image imgLeft;
    private static Image imgRight;
//    private static Image imgShadow;

    private boolean started = false;
    /*****************************************************************
     * variables - Timer-related
     *****************************************************************/
    private RefreshTimer refreshTimer;
    /*****************************************************************
     * variables - Data-related
     *****************************************************************/
    private Image[] imgBgList;
    private String displayName;
    private String weatherIconCode;
    private Image imgWeatherIcon;
    private String curTemperature;
    private Image imgTemperatureUnit;

    protected synchronized void startWidget() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidget.startWidget]Start.");
        }
        imgFocus = DataCenter.getInstance().getImage("shared/00_banner_focus.png");
        imgLeft = DataCenter.getInstance().getImage("shared/02_ars_l.png");
        imgRight = DataCenter.getInstance().getImage("shared/02_ars_r.png");
//        imgShadow = DataCenter.getInstance().getImage("shared/00_banner_shadow.png");
        imgTempC = DataCenter.getInstance().getImage("shared/11_c.png");
        imgTempF = DataCenter.getInstance().getImage("shared/11_f.png");
        WeatherWidgetDataManager.getInstance().init();
        WeatherWidgetDataManager.getInstance().start();
        curReqStatus = REQUEST_STATUS_LOADING;
        this.started = true;

        new Thread() {
            public void run() {
                try {
                    if (wUnits != null) {
                        byte[][] wBgImgSrcList = WeatherWidgetDataManager.getInstance().getWidgetBackgroundImageSourceList(bgServerInfo, wUnits);
                        if (wBgImgSrcList != null) {
                            int wBgImgSrcListCount = wBgImgSrcList.length;
                            imgBgList = new Image[wBgImgSrcListCount];
                            String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
                            for (int i=0; i<wBgImgSrcListCount; i++) {
                                if (wUnits[i] == null || wBgImgSrcList[i] == null) {
                                    continue;
                                }
                                String imgName = null;
                                if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                                    imgName = wUnits[i].getImageNameFrench();
                                }else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                                    imgName = wUnits[i].getImageNameEnglish();
                                }
                                if (imgName == null) {
                                    continue;
                                }
                                imgBgList[i] = FrameworkMain.getInstance().getImagePool().createImage(wBgImgSrcList[i], imgName);
                            }
                            FrameworkMain.getInstance().getImagePool().waitForAll();
                        }
                    }
                    synchronized (WeatherWidget.this) {
                        if (!started) {
                            stopTimerAndflushImage();
                            return;
                        }
                    }
                    String hometownLocId = WeatherWidgetDataManager.getInstance().getHomeTownLocationId();
                    LatestForecast latestForecast = WeatherWidgetDataManager.getInstance().getLatestForecast(
                            hometownLocId);
                    setWeatherData(latestForecast);
                    curReqStatus = REQUEST_STATUS_VALID;
                    if (refreshRateMins > 0) {
                        if (refreshTimer == null) {
                            refreshTimer = new RefreshTimer(refreshRateMins);
                        }
                        refreshTimer.startTimer();
                    }
                    repaint();
                    return;
                }catch(Exception ex) {
                    Log.print(ex);
                }
                curReqStatus = REQUEST_STATUS_INVALID;
                repaint();
            }
        }.start();
    }

    private void stopTimerAndflushImage() {
        if (refreshTimer != null) {
            refreshTimer.stopTimer();
        }
        Image[] oldList = imgBgList;
        if (oldList != null) {
            for (int i = 0; i < oldList.length; i++) {
                if (oldList[i] != null) {
                    oldList[i].flush();
                }
            }
        }
        imgBgList = null;
    }

    protected synchronized void stopWidget() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidget.stopWidget]Start.");
        }
        if (refreshTimer != null) {
            refreshTimer.stopTimer();
        }
        imgBgList = null;

        displayName = null;
        curTemperature = null;

        if (wUnits!=null) {
            String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
            for (int i=0; i<wUnits.length; i++) {
                String imgName = null;
                if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                    imgName = wUnits[i].getImageNameFrench();
                }else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                    imgName = wUnits[i].getImageNameEnglish();
                }
                if (imgName!=null) {
                    DataCenter.getInstance().removeImage(imgName);
                }
            }
        }
        if (wUnits != null) {
            String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
            for (int i=0; i<wUnits.length; i++) {
                if (wUnits[i] == null) {
                    continue;
                }
                String imgName = null;
                if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                    imgName = wUnits[i].getImageNameFrench();
                } else if (curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                    imgName = wUnits[i].getImageNameEnglish();
                }
                if (imgName != null) {
                    DataCenter.getInstance().removeImage(imgName);
                }
            }
        }
        //remove background image
        imgWeatherIcon = null;
        if (weatherIconCode != null) {
            DataCenter.getInstance().removeImage(weatherIconCode);
            weatherIconCode = null;
        }
        DataCenter.getInstance().removeImage("shared/00_banner_focus.png");
        imgFocus = null;
        DataCenter.getInstance().removeImage("shared/02_ars_l.png");
        imgLeft = null;
        DataCenter.getInstance().removeImage("shared/02_ars_r.png");
        imgRight = null;
        // 4K - remove
//        DataCenter.getInstance().removeImage("shared/00_banner_shadow.png");
//        imgShadow = null;
        imgTemperatureUnit = null;
        DataCenter.getInstance().removeImage("shared/11_f.png");
        imgTempF = null;
        DataCenter.getInstance().removeImage("shared/11_c.png");
        imgTempC = null;

        curReqStatus = REQUEST_STATUS_LOADING;
        WeatherWidgetDataManager.getInstance().stop();
        WeatherWidgetDataManager.getInstance().dispose();
        this.started = false;
    }
    public void focusChanged(boolean isSelected) {
    }
    public boolean keyAction(int keyCode) {
        switch(keyCode) {
            case Rs.KEY_LEFT:
            case Rs.KEY_RIGHT:
                return true;
            case Rs.KEY_OK:
                executeWidgetUnit(0);
                return true;
        }
        return false;
    }
    public void paint(Graphics g) {
    	// 4K - remove
//        if (imgShadow != null) {
//            g.drawImage(imgShadow, 0, 103, this);
//        }
        if (isSelected && imgFocus != null) {
            g.drawImage(imgFocus, 13, 15, this);
        }
        switch(curReqStatus) {
            case REQUEST_STATUS_LOADING:
                g.setColor(c086086086);
                g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                g.setColor(c050050050);
                g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                String txtLoading = DataCenter.getInstance().getString("TxtCommon_Loading");
                if (txtLoading != null) {
                    g.setFont(f22);
                    g.setColor(c110110110);
                    g.drawString(txtLoading, 110 - fm22.stringWidth(txtLoading) / 2, 70);
                    g.setColor(c050050050);
                    g.drawString(txtLoading, 110 - fm22.stringWidth(txtLoading) / 2 - 1, 69);
                }
                return;
            case REQUEST_STATUS_INVALID:
                g.setColor(c086086086);
                g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                g.setColor(c050050050);
                g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
                String txtInvalidData = DataCenter.getInstance().getString("TxtCommon_Invalid_Data");
                if (txtInvalidData != null) {
                    g.setFont(f18);
                    g.setColor(c110110110);
                    g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2, 58);
                    g.setColor(c050050050);
                    g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2 - 1, 57);
                    g.setFont(f16);
                    String invalidData = "[Name-Id] : ["+name + "-" +id+"]";
                    g.setColor(c110110110);
                    g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2, 76);
                    g.setColor(c050050050);
                    g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2 - 1, 75);
                }
                return;
        }
        if (imgBgList == null || imgBgList.length <= 0 || imgBgList[0] == null) {
            g.setColor(c086086086);
            g.fillRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
            g.setColor(c050050050);
            g.drawRect(21, 21, IMAGE_WIDTH, IMAGE_HEIGHT - 24);
            String txtInvalidData = DataCenter.getInstance().getString("TxtCommon_Invalid_Data");
            if (txtInvalidData != null) {
                g.setFont(f18);
                g.setColor(c110110110);
                g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2, 58);
                g.setColor(c050050050);
                g.drawString(txtInvalidData, 110 - fm18.stringWidth(txtInvalidData) / 2 - 1, 57);
                g.setFont(f16);
                String invalidData = "[Name-Id] : ["+name + "-" +id+"]";
                g.setColor(c110110110);
                g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2, 76);
                g.setColor(c050050050);
                g.drawString(invalidData, 110 - fm16.stringWidth(invalidData) / 2 - 1, 75);
            }
        } else {
            g.drawImage(imgBgList[0], 21, 9, IMAGE_WIDTH, IMAGE_HEIGHT, this);
        }
        if (isSelected) {
            if (!isFirstIndex() && imgLeft!= null) {
                g.drawImage(imgLeft, 0, 48, this);
            }
            if (!isLastIndex() && imgRight!= null) {
                g.drawImage(imgRight, 201, 48, this);
            }
        }
        g.setFont(f19);
        g.setColor(c000019022);
        if (displayName != null) {
            String shortenedDisplayName = TextUtil.shorten(displayName, fm19, 160);
            g.drawString(shortenedDisplayName, 32, 42);
        } else {
            g.drawString("N/A", 32, 42);
        }

        if (imgWeatherIcon != null) {
            g.drawImage(imgWeatherIcon, 32, 45, 90, 54, this);
        }
        g.setFont(f30);
        g.setColor(c255255255);
        if (curTemperature != null && curTemperature.trim().length()>0) {
            if (imgTemperatureUnit != null) {
                g.drawImage(imgTemperatureUnit, 157, 67, this);
            }
        } else {
            curTemperature = "N/A";
        }
        g.drawString(curTemperature, 154 - fm30.stringWidth(curTemperature), 82);
    }
    private void setWeatherData(LatestForecast latestForecast) {
        if (latestForecast == null) {
            return;
        }
        CityInfo cInfo = latestForecast.getCityInfo();
        if (cInfo != null) {
            displayName = cInfo.getDisplayName();
        }
        CurrentCondition curCond = latestForecast.getCurrentCondition();
        if (curCond != null) {
            weatherIconCode = curCond.getIconCode();
            imgWeatherIcon = WeatherWidgetDataManager.getInstance().getWeatherIconImage(weatherIconCode);
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidget.setWeatherData]weatherIconCode : "+weatherIconCode);
                Log.printDebug("[WeatherWidget.setWeatherData]imgWeatherIcon : "+imgWeatherIcon);
            }
            int temperatureUnit = WeatherWidgetDataManager.getInstance().getTemperatureUnit();
            switch(temperatureUnit) {
                case WeatherWidgetDataManager.TEMPERATURE_UNIT_ID_C:
                    curTemperature = curCond.getTemperatureC();
                    imgTemperatureUnit = imgTempC;
                    break;
                case WeatherWidgetDataManager.TEMPERATURE_UNIT_ID_F:
                    curTemperature = curCond.getTemperatureF();
                    imgTemperatureUnit = imgTempF;
                    break;
            }
        }
    }
    /*****************************************************************
     * class - TVTimerSpec-related
     *****************************************************************/
    class RefreshTimer implements TVTimerWentOffListener{
        private TVTimerSpec timerRefresh;
        public RefreshTimer(int timerMins) {
            setTimer(timerMins);
        }
        private void setTimer(int timerMins) {
            if (timerRefresh == null) {
                timerRefresh = new TVTimerSpec();
            }
            timerRefresh.setDelayTime(timerMins * Rs.MILLIS_TO_SECOND * 60);
            timerRefresh.setRepeat(false);
        }
        public synchronized void startTimer() {
            stopTimer();
            if (timerRefresh != null) {
                timerRefresh.addTVTimerWentOffListener(this);
                try {
                    TVTimer.getTimer().scheduleTimerSpec(timerRefresh);
                } catch (Exception ignore) {
                    ignore.printStackTrace();
                }
            }
        }
        public synchronized void stopTimer() {
            if (timerRefresh != null) {
                timerRefresh.removeTVTimerWentOffListener(this);
                TVTimer.getTimer().deschedule(timerRefresh);
            }
        }
        public void timerWentOff(TVTimerWentOffEvent event) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidget.timerWentOff]Refresh Data");
            }
            curReqStatus = REQUEST_STATUS_LOADING;
            new Thread() {
                public void run() {
                    try{
                        String hometownLocId = WeatherWidgetDataManager.getInstance().getHomeTownLocationId();
                        LatestForecast latestForecast = WeatherWidgetDataManager.getInstance().getLatestForecast(hometownLocId);
                        setWeatherData(latestForecast);
                        curReqStatus = REQUEST_STATUS_VALID;
                        synchronized (WeatherWidget.this) {
                            if (!started) {
                                return;
                            }
                        }
                        startTimer();
                        repaint();
                        return;
                    }catch(Exception ex) {
                        Log.print(ex);
                    }
                    curReqStatus = REQUEST_STATUS_INVALID;
                    repaint();
                }
            }.start();
        }
    }
}

package com.videotron.tvi.illico.dashboard.comp.widget;

import java.awt.Container;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.dashboard.communication.PreferenceProxy;
import com.videotron.tvi.illico.dashboard.controller.DashboardVbmController;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;

abstract public class Widget extends Container implements AnimationRequestor {
    private static final long serialVersionUID = 1L;
    protected static final int IMAGE_WIDTH=176;
    protected static final int IMAGE_HEIGHT=105;
    /*********************************************************************************
     * Request-related
     *********************************************************************************/
    protected static final int REQUEST_STATUS_LOADING = 0;
    protected static final int REQUEST_STATUS_VALID = 1;
    protected static final int REQUEST_STATUS_INVALID = 2;
    protected int curReqStatus;
    /*****************************************************************
     * constants - SharedMemory-related 
     *****************************************************************/
    public static final String SMKEY_WIDGET_LIST = "SMK_WIDGET_LIST";
    public static final String SMKEY_WIDGET_TAG_DATA = "SMK_WIDGET_";
    public static final String SMKEY_WIDGET_TAG_IMAGES = "SMK_WIDGET_BACK_IMAGES_";
    public static final String SMKEY_WIDGET_ID = "SMK_WIDGET_ID";
    public static final String SMKEY_WIDGET_NAME = "SMK_WIDGET_NAME";
    public static final String SMKEY_WIDGET_IMAGE_COUNT = "SMK_WIDGET_IMAGE_COUNT";
    public static final String SMKEY_WIDGET_REFRESH_RATE_MINS = "SMK_WIDGET_REFRESH_RATE_SECS";
    public static final String SMKEY_WIDGET_IS_CYCLABLE = "SMK_WIDGET_IS_CYCLABLE";
    public static final String SMKEY_WIDGET_CYCLE_TIME_SECS = "SMK_WIDGET_CYCLE_TIME_SECS";
    public static final String SMKEY_WIDGET_TEMPLATE_TYPE = "SMK_WIDGET_TEMPLATE_TYPE_";
    public static final String SMKEY_WIDGET_TARGET_TYPE = "SMKEY_WIDGET_TARGET_TYPE_";
    public static final String SMKEY_WIDGET_IMAGE_NAME_ENGLISH = "SMK_WIDGET_IMAGE_NAME_EN_";
    public static final String SMKEY_WIDGET_IMAGE_NAME_FRENCH = "SMK_WIDGET_IMAGE_NAME_FR_";
    public static final String SMKEY_WIDGET_IS_SELECTABLE = "SMK_WIDGET_IS_SELECTABLE_";
    public static final String SMKEY_WIDGET_CALL_LETTER = "SMK_WIDGET_CALL_LETTER_";
    public static final String SMKEY_WIDGET_BINDED_APPLICATION_NAME = "SMK_WIDGET_BINDED_APPLICATION_NAME_";
    public static final String SMKEY_WIDGET_BINDED_APPLICATION_PARAMS = "SMK_WIDGET_BINDED_APPLICATION_PARAMS_";
    public static final String SMKEY_WIDGET_MANAGED_TYPE = "SMK_WIDGET_MANAGED_TYPE";
    public static final String SMKEY_WIDGET_UNIT_PRIMARY_KEY_LIST = "SMKEY_WIDGET_UNIT_PRIMARY_KEY_LIST";
    public static final String SMKEY_WIDGET_BG_SERVER_INFO = "SMKEY_WIDGET_BG_SERVER_INFO";
    public static final String SMKEY_WIDGET_BG_PROXY_IP_LIST = "SMKEY_WIDGET_BG_PROXY_IP_LIST";
    public static final String SMKEY_WIDGET_BG_PROXY_PORT_LIST = "SMKEY_WIDGET_BG_ROXY_PORT_LIST";
    public static final String SMKEY_WIDGET_POSSIBLE_CONTEXTUAL_STATE = "SMKEY_WIDGET_POSSIBLE_CONTEXTUAL_STATE";
    public static final String SMKEY_BG_IMAGE_SRC_LIST = "SMKEY_BG_IMAGE_SRC_LIST";
    public static final String SMKEY_BG_IMAGE_SRC_LIST_EN = "SMKEY_BG_IMAGE_SRC_LIST_EN";
    public static final String SMKEY_BG_IMAGE_SRC_LIST_FR = "SMKEY_BG_IMAGE_SRC_LIST_FR";
    /*****************************************************************
     * constants - WIDGET_TYPE-related
     *****************************************************************/
    public static final String SMKEY_WIDGET_DETAIL = "SMKEY_WIDGET_DETAIL";
    public static final String WIDGET_TYPE_PROMOTION = "Promotion";
    public static final String WIDGET_TYPE_LOTTERY = "Lottery";
    public static final String WIDGET_TYPE_WEATHER = "Weather";
    public static final String WIDGET_TYPE_OPTION = "Option";
    /*****************************************************************
     * constants - WIDGET_SERVER-related
     *****************************************************************/
    public static final String SMKEY_WIDGET_SERVER_LIST = "SMKEY_WIDGET_SERVER_LIST";
    public static final String SMKEY_WIDGET_SERVER_HOST = "SMKEY_WIDGET_SERVER_HOST";
    public static final String SMKEY_WIDGET_SERVER_PORT = "SMKEY_WIDGET_SERVER_PORT";
    public static final String SMKEY_WIDGET_SERVER_CONTEXT_ROOT = "SMKEY_WIDGET_SERVER_CONTEXT_ROOT";
    public static final String SMKEY_WIDGET_SERVER_PROXY_COUNT = "SMKEY_WIDGET_SERVER_PROXY_COUNT"; 
    public static final String SMKEY_WIDGET_SERVER_PROXY_HOST = "SMKEY_WIDGET_SERVER_PROXY_HOST_";
    public static final String SMKEY_WIDGET_SERVER_PROXY_PORT = "SMKEY_WIDGET_SERVER_PROXY_PORT_";
    
    /*****************************************************************
     * constants - WIDGET_CONTEXTUAL_TYPE-related
     *****************************************************************/
    public static final String WIDGET_CONTEXTUAL_STATE_MENU = "F";
    public static final String WIDGET_CONTEXTUAL_STATE_DASHBOARD = "D";
    public static final String WIDGET_CONTEXTUAL_STATE_BOTH = "B";
    /*****************************************************************
     * constants - WIDGET-related
     *****************************************************************/
    public static final int WIDGET_MANAGED_TYPE_ADMIN = 0;
    public static final int WIDGET_MANAGED_TYPE_USER = 1;
    
    public static final int WIDGET_TEMPLATE_TYPE_OPTION = 0;
    public static final int WIDGET_TEMPLATE_TYPE_INFORMATION = 1;
    public static final int WIDGET_TEMPLATE_TYPE_PROMOTION = 2;
    public static final int WIDGET_TEMPLATE_TYPE_INTERACTIVE = 3;
    
    public static final int WIDGET_TARGET_TYPE_CHANNEL = 0;
    public static final int WIDGET_TARGET_TYPE_APPLICATION = 1;
    
    // R5
    public static final String APP_NAME_PROFILE = "Profile";
    
    private MonitorService mSvc;
    private EpgService eSvc;
    /*****************************************************************
     * Listener-related
     *****************************************************************/
    private WidgetListener listener;
    
    protected String id;
    protected String name;
    protected int imageCount;
    protected int refreshRateMins;
    protected boolean isCyclable;
    protected int cycleTimeSecs;
    protected int managedType;
    protected boolean isSelected;
    protected WidgetUnit[] wUnits;
    /*****************************************************************
     * Server info-related
     *****************************************************************/
    protected ServerInfo bgServerInfo;
    protected String serverInfo;
    protected String[] proxyIPs;
    protected int[] proxyPorts;
    private boolean isFirstIndex;
    private boolean isLastIndex;
    
    //R7.4 freelife VDTRMASTER-6197
    private String savedAppName;
    private String[] savedParams;
    
    public Widget() {
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getImageCount() {
        return imageCount;
    }
    public void setImageCount(int imageCount) {
        this.imageCount = imageCount;
    }
    public int getRefreshRateMins() {
        return refreshRateMins;
    }
    public void setRefreshRateMins(int refreshRateMins) {
        this.refreshRateMins = refreshRateMins;
    }
    public boolean isCyclable() {
        return isCyclable;
    }
    public void setCyclable(boolean isCyclable) {
        this.isCyclable = isCyclable;
    }
    public int getCycleTimeSecs() {
        return cycleTimeSecs;
    }
    public void setCycleTimeSecs(int cycleTimeSecs) {
        this.cycleTimeSecs = cycleTimeSecs;
    }
    public int getManagedType() {
        return managedType;
    }
    public void setManagedType(int managedType) {
        this.managedType = managedType;
    }
    public WidgetUnit[] getWidgetUnits() {
        return wUnits;
    }
    public void setWidgetUnits(WidgetUnit[] wUnits){
        this.wUnits = wUnits;
    }
    public String getServerInfo() {
        return serverInfo;
    }
    public void setServerInfo(String serverInfo) {
        this.serverInfo = serverInfo;
    }
    public String[] getProxyIpList() {
        return proxyIPs;
    }
    public void setProxyIpList(String[] proxyIPs) {
        this.proxyIPs = proxyIPs;
    }
    public int[] getProxyPortList() {
        return proxyPorts;
    }
    public void setProxyPortList(int[] proxyPorts) {
        this.proxyPorts = proxyPorts;
    }
    public boolean isSelected() {
        return isSelected;
    }
    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
        focusChanged(isSelected);
    }
    public boolean isFirstIndex() {
        return isFirstIndex;
    }
    public void setFirstIndex(boolean isFirstIndex) {
        this.isFirstIndex = isFirstIndex;
    }
    public boolean isLastIndex() {
        return isLastIndex;
    }
    public void setLastIndex(boolean isLastIndex) {
        this.isLastIndex = isLastIndex;
    }
    public void start() {
        setBackgroundServerInfo();
        new Thread() {
            public void run(){
                startWidget();
                repaint();
            }
        }.start();
    }
    public void stop() {
        bgServerInfo = null;
        isFirstIndex = false;
        isLastIndex = false;
        stopWidget();
    }
    
    public boolean skipAnimation(Effect effect) {
        return false;
    }

    public void animationStarted(Effect effect) {
    }

    public void animationEnded(Effect effect) {
    }
    
    abstract public boolean keyAction(int keyCode);
    abstract public void focusChanged(boolean isSelected);
    abstract protected void startWidget();
    abstract protected void stopWidget();
    
    public static byte[] getURLConnect(String data) {
        BufferedInputStream bis = null;
        byte[] b = null;
        try {
            URL url = new URL(data);
            // VDTRMASTER-5846
//            ((HttpURLConnection) url.openConnection()).getResponseCode();
            bis = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int c;
            while ((c = bis.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }
            b = baos.toByteArray();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return b;
    }
    /*****************************************************************
     * protected-related
     *****************************************************************/
    protected void setBackgroundServerInfo() {
        bgServerInfo = new ServerInfo();
        bgServerInfo.setUrlData(serverInfo);
        if (proxyIPs != null && proxyPorts != null) {
            int proxyIPsLth = proxyIPs.length;
            int proxyPortsLth = proxyPorts.length;
            ProxyInfo[] pInfoList = new ProxyInfo[proxyIPsLth]; 
            if (proxyIPsLth == proxyPortsLth) {
                for (int i=0; i<proxyIPsLth; i++) {
                    pInfoList[i] = new ProxyInfo();
                    pInfoList[i].setProxyHost(proxyIPs[i]);
                    pInfoList[i].setProxyPort(proxyPorts[i]);
                }
            }
            bgServerInfo.setProxyInfos(pInfoList);
        }
    }
    /*****************************************************************
     * methods - TargetAction-related
     *****************************************************************/
    protected void executeWidgetUnit(int curIdx) {
        if (wUnits == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[Widget.executeWidgetUnit]Widget unit is null. return.");
            }
            return;
        }
        if (wUnits.length <= curIdx) {
            if (Log.WARNING_ON) {
                Log.printWarning("[Widget.executeWidgetUnit]Widget unit length is invalid. return.");
            }
            return;
        }
        if (wUnits[curIdx] == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[Widget.executeWidgetUnit]Current widget unit is null. return.");
            }
            return;
        }
        boolean isSelectable = wUnits[curIdx].isSelectable();
        if (Log.DEBUG_ON) {
            Log.printDebug("[Widget.executeWidgetUnit]isSelectable : "+isSelectable);
        }
        if (isSelectable == false) {
            return;
        }
        int templateType = wUnits[curIdx].getTemplateType();
        if (Log.DEBUG_ON) {
            Log.printDebug("[Widget.executeWidgetUnit]templateType : "+templateType);
        }
        switch(templateType) {
            case WIDGET_TEMPLATE_TYPE_PROMOTION:
            case WIDGET_TEMPLATE_TYPE_INTERACTIVE:
                int targetType = wUnits[curIdx].getTargetAction();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[Widget.executeWidgetUnit]targetType : "+targetType);
                }
                if (targetType == WIDGET_TARGET_TYPE_CHANNEL) {
                    String targetCallLetter = wUnits[curIdx].getTargetCallLetter();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[Widget.executeWidgetUnit]Target call letter : "+targetCallLetter);
                    }
                    if (targetCallLetter != null) {
                        requestChangeChannel(targetCallLetter);
                    }
                }else if (targetType == WIDGET_TARGET_TYPE_APPLICATION) {
                    String targetAppName = wUnits[curIdx].getTargetAppName();
                    String[] targetAppParams = wUnits[curIdx].getTargetAppParamter();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[Widget.executeWidgetUnit]Target app name : "+targetAppName);
                        Log.printDebug("[Widget.executeWidgetUnit]Target app params : "+targetAppParams);
                    }
                    if (targetAppName != null) {
                        requestStartUnboundApplication(targetAppName, targetAppParams);
                    }
                }
        }
    }
    protected EpgService getEpgService() {
        if (eSvc == null) {
            try {
                String lookupName = "/1/2026/" + EpgService.IXC_NAME;
                eSvc = (EpgService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[PromotionWidget.getEpgService]not bound - " + EpgService.IXC_NAME);
                }
            }
        }
        return eSvc;
    }
    protected boolean requestChangeChannel(String callLetter) {
        try{
            EpgService svc = getEpgService();
            if (svc != null) {
                TvChannel tvChannel = svc.getChannel(callLetter);
                svc.getChannelContext(0).changeChannel(tvChannel);
                if (listener!=null) {
                    listener.selectedChannel(id, callLetter);
                }
            }
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    protected MonitorService getMonitorService() {
        if (mSvc == null) {
            try {
                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                mSvc = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[PromotionWidget.getMonitorService]not bound - " + MonitorService.IXC_NAME);
                }
            }
        }
        return mSvc;
    }
    protected boolean requestStartUnboundApplication(String appName, String[] extendedParams) {
        try {
            MonitorService svc = getMonitorService();
            if (svc != null) {
            	
            	if (checkSettingsRelated(appName, extendedParams)) {
            		//R7.4 freelife VDTRMASTER-6197
            		DashboardVbmController.getInstance().savedWidgetId = id;
            		DashboardVbmController.getInstance().savedAppName = appName;            		
            		DashboardVbmController.getInstance().savedParams = extendedParams;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[MainCtrl.startApplication]This menu item checked by settings confirmation module.");
                    }
                    return true;
                }
            	
                svc.startUnboundApplication(appName, extendedParams);
                if (listener!=null) {
                    listener.startedUnboundApplication(id, appName, extendedParams);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    
    public void setWidgetListener(WidgetListener listener) {
        this.listener=listener;
    }
    public void removeWidgetListener() {
        this.listener=null;
    }    
    
    // R5
    public boolean checkSettingsRelated(String appName, String[] extParams) {
        if (appName.equalsIgnoreCase(APP_NAME_PROFILE)) {
            boolean isProtectSettingsModifications = PreferenceProxy.getInstance().isProtectSettingsModifications();
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainCtrl.startApplication]isProtectSettingsModifications : "+isProtectSettingsModifications);
            }
            if (isProtectSettingsModifications) {
                PreferenceProxy.getInstance().requestCheckRightFilter(appName, extParams);
                return true;
            }
        }
        return false;
    }
    
}

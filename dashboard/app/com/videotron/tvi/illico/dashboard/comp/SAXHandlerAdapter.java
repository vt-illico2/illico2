package com.videotron.tvi.illico.dashboard.comp;

import org.xml.sax.Attributes;

public class SAXHandlerAdapter extends SAXHandler {

    public void startDocument() {
    }
    
    public void endDocument() {
    }
    
    public void startElement(String valueXMLTag, Attributes valueAttributes) {
    }

    public void endElement(String valueXMLTag) {
    }

    public void parseCDData(String valueCDData) {
    }
}

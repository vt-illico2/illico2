package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.dashboard.comp.SAXHandlerAdapter;

public class LocationIdHandler extends SAXHandlerAdapter {
    private String locationId;
    public void startDocument() {
    }
    public void endDocument() {
        setResultObject(locationId);
    }
    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("SITE")) {
            if (attr != null) {
                int attrLth = attr.getLength();
                for (int i=0; i<attrLth; i++) {
                    String lName = attr.getLocalName(i);
                    if (lName == null) {
                        continue;
                    }
                    String lValue = attr.getValue(i);
                    try{
                        lValue = new String(lValue.getBytes(), "UTF-8");
                    }catch(Exception e) {
                        e.printStackTrace();
                    }
                    if (lName.equalsIgnoreCase("LOCATIONID")) {
                        locationId = lValue;
                    } 
                }
            }
        }
    }
}

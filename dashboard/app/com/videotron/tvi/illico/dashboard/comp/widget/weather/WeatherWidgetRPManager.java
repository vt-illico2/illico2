package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.io.IOException;
import java.net.URL;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.comp.IOUtil;
import com.videotron.tvi.illico.dashboard.comp.widget.ServerInfo;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.log.Log;

public class WeatherWidgetRPManager {
    private ErrorMessageService emSvc;
    
    private static WeatherWidgetRPManager instance;
    public static synchronized WeatherWidgetRPManager getInstance() {
        if (instance == null) {
            instance = new WeatherWidgetRPManager();
        }
        return instance;
    }
    private WeatherWidgetRPManager() {
    }
    /*********************************************************************************
     * Service-related
     *********************************************************************************/
    public CityInfo getCityInfoByPostalCode(String reqLangTag, String reqPostalCode){
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]start");
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]Param - request postal code : ["+reqPostalCode+"]");
        }
        if (reqLangTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]Language tag is invalid.");
            }
            return null;
        }
        if (reqPostalCode == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]Postal code is invalid.");
            }
            return null;
        }
        if (reqPostalCode.trim().length() < 3) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]Postal code's length is invalid.");
            }
            return null;
        }
        String postalCode = reqPostalCode.trim().toUpperCase().substring(0, 3);
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]Request postal code : ["+postalCode+"]");
        }
        CityInfoHandler handler = new CityInfoHandler();
        String filePath = "postal/" + reqLangTag + "/" + postalCode + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]Request file path : ["+filePath+"]");
        }
        URL reqURL = null;
        ServerInfo serverInfo = WeatherWidgetDataManager.getInstance().getServerInfo();
        try {
            if (serverInfo != null) {
                reqURL = serverInfo.getURL(filePath);
            }
        } catch (Exception e) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]Cannot create URL object by following exception ["+e.getMessage()+"].");
            }
            e.printStackTrace();
        }
        if (reqURL == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]URL object is null.");
            }
            return null;
        }
        CityInfo cityInfo = null;
        try{
            cityInfo = (CityInfo)IOUtil.getObejctFromUrlByParser(reqURL, handler);
        }catch(IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][CityInfo]Cannot retrieve city info from the Weather Server by IOException["+e.getMessage()+"].");
            }
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][CityInfo]Cannot retrieve city info from the Weather Server by Exception["+e.getMessage()+"].");
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getCityInfoByPostalCode]Response city info : "+cityInfo);
        }
        return cityInfo;
    }
    public LatestForecast getLatestForecast(String reqLangTag, String reqLocId) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]start");
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]Param - request language type : ["+reqLangTag+"]");
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]Param - request location id : ["+reqLocId+"]");
        }
        if (reqLangTag == null) {
            if (Log.ERROR_ON) {
                Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Language tag is invalid.");
            }
            return null;
        }
        if (reqLocId == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Location id is invalid.");
            }
            return null;
        }
        if (reqLocId.trim().length() == 0) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Location id's length is invalid.");
            }
            return null;
        }
        LatestForecastHandler handler = new LatestForecastHandler();
        String filePath = "forecast/" + reqLangTag + "/" + reqLocId + ".xml";
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]Request file path : ["+filePath+"]");
        }
        URL reqURL = null;
        ServerInfo serverInfo = WeatherWidgetDataManager.getInstance().getServerInfo();
        try {
            if (serverInfo != null) {
                reqURL = serverInfo.getURL(filePath);
            }
        } catch (Exception e) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Cannot create URL object by following exception ["+e.getMessage()+"].");
            }
            e.printStackTrace();
        }
        if (reqURL == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]URL object is null.");
            }
            return null;
        }
        LatestForecast latestForecast = null;
        try{
            latestForecast = (LatestForecast)IOUtil.getObejctFromUrlByParser(reqURL, handler);
        }catch(IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][LatestForecast]Cannot retrieve latest forecast data from the Weather Server by IOException["+e.getMessage()+"].");
            }
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][LatestForecast]Cannot retrieve latest forecast data from the Weather Server by Exception["+e.getMessage()+"].");
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]Response latest forecast : "+latestForecast);
        }
        return latestForecast;
    }
    public byte[] getWidgetBackgroundImage(ServerInfo bgServerInfo, String reqWidgetBgImgName) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getWidgetBackgroundImage]start");
            Log.printDebug("[WeatherWidgetRPMgr.getWidgetBackgroundImage]Param - Request server info : ["+bgServerInfo+"]");
            Log.printDebug("[WeatherWidgetRPMgr.getWidgetBackgroundImage]Param - Request widget background image name : ["+reqWidgetBgImgName+"]");
        }
		if (bgServerInfo==null){
			if (Log.ERROR_ON){
				Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][WeatherBG]Server info is invalid.");
			}
            return null;
        }
        if (reqWidgetBgImgName == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][WeatherBG]Background image name is invalid.");
            }
            return null;
        }
        URL reqURL = null;
        try {
            reqURL = bgServerInfo.getURL(reqWidgetBgImgName);
        } catch (Exception e) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][WeatherBG]Cannot create URL object by following exception ["+e.getMessage()+"].");
            }
            e.printStackTrace();
            return null;
        }
        if (reqURL == null) {
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][WeatherBG]URL object is null.");
            }
            return null;
        }

        byte[] srcWidgetBgImg = null;
        try{
            srcWidgetBgImg = IOUtil.getBytesFromUrl(reqURL);
        }catch(IOException e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_ACCESS_PROBLEM+"][WeatherBG]Cannot retrieve Widget images from the MS HTTP Server by IOException["+e.getMessage()+"].");
            }
        }catch(Exception e) {
            e.printStackTrace();
            if (Log.ERROR_ON) {
            	Log.printError("["+Rs.ERROR_CODE_SERVER_DATA_RECEIPT_PROBLEM+"][WeatherBG]Cannot retrieve Widget images from the MS HTTP Server by Exception["+e.getMessage()+"].");
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetRPMgr.getLatestForecast]Response widget background image src : "+srcWidgetBgImg);
        }
        return srcWidgetBgImg;
    }
    
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(final String errorCode) {
        new Thread(){
            public void run() {
                ErrorMessageService svc = getErrorMessageService();
                if (svc != null) {
                    try {
                        svc.showErrorMessage(errorCode, null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}

package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.awt.Image;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.dashboard.comp.SharedMemoryManager;
import com.videotron.tvi.illico.dashboard.comp.dashboard.DashboardPreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.widget.ServerInfo;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.comp.widget.WidgetUnit;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;

public class WeatherWidgetDataManager {
    private static WeatherWidgetDataManager instance;
    private SharedMemory sm = SharedMemory.getInstance();

    private static final String SMKEY_WEATHER_DATA_LIST = "SMKEY_WEATHER_DATA_LIST";
    private static final String SMKEY_WEATHER_DATA_LOCATION_ID = "SMKEY_WEATHER_DATA_LOCATION_ID";
    private static final String SMKEY_WEATHER_DATA_CITY_NAME = "SMKEY_WEATHER_DATA_CITY_NAME";
    private static final String SMKEY_WEATHER_DATA_COUNTRY_NAME = "SMKEY_WEATHER_DATA_COUNTRY_NAME";
    private static final String SMKEY_WEATHER_DATA_PROVINCE_NAME = "SMKEY_WEATHER_DATA_PROVINCE_NAME";
    private static final String SMKEY_WEATHER_DATA_CITY_TYPE = "SMKEY_WEATHER_DATA_CITY_TYPE";
    private static final String SMKEY_WEATHER_DATA_ICON_CODE = "SMKEY_WEATHER_DATA_ICON_CODE";
    private static final String SMKEY_WEATHER_DATA_FORECAST_TEXT = "SMKEY_WEATHER_DATA_FORECAST_TEXT";
    private static final String SMKEY_WEATHER_DATA_TEMPERATURE_C = "SMKEY_WEATHER_DATA_TEMPERATURE_C";
    private static final String SMKEY_WEATHER_DATA_TEMPERATURE_F = "SMKEY_WEATHER_DATA_TEMPERATURE_F";
    private static final String SMKEY_WEATHER_DATA_NEXT_UPDATE_DATE = "SMKEY_WEATHER_DATA_NEXT_UPDATE_DATE";

    public static final int TEMPERATURE_UNIT_ID_C = 0;
    public static final int TEMPERATURE_UNIT_ID_F = 1;
    public static final String TEMPERATURE_UNIT_C = "C";
    public static final String TEMPERATURE_UNIT_F = "F";
    private int temperatureUnit = -1;

    public static final String LANGUAGE_TAG_EN = "en";
    public static final String LANGUAGE_TAG_FR = "fr";
    private String curLanguageTag;

    private ServerInfo sInfo;

    /*********************************************************************************
     * CurrentCondition-related
     *********************************************************************************/
    public static final String SEPARATOR = "|";
    private Hashtable latestForecastHash;
    
    private WeatherWidgetPreferenceProxy wwPrefProxy;

    public static synchronized WeatherWidgetDataManager getInstance() {
        if (instance == null) {
            instance = new WeatherWidgetDataManager();
        }
        return instance;
    }
    private WeatherWidgetDataManager() {
    }
    public void init() {
    }
    public void dispose() {
    }
    public void start() {
        sInfo = SharedMemoryManager.getInstance().getServerInfo(Widget.WIDGET_TYPE_WEATHER);
        wwPrefProxy=WeatherWidgetPreferenceProxy.getInstance();
        wwPrefProxy.start();
        initLatestForecastData();
    }
    public void stop() {
        if (latestForecastHash != null) {
            latestForecastHash.clear();
            latestForecastHash = null;
        }
        if (wwPrefProxy!=null) {
            wwPrefProxy.stop();
            wwPrefProxy=null;
        }
        disposeLatestForecastData();
        sInfo = null;
    }
    public byte[][] getWidgetBackgroundImageSourceList(ServerInfo bgServerInfo, WidgetUnit[] wUnitList) {
        if (wUnitList == null) {
            return null;
        }
        int wUnitListLth = wUnitList.length;
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getWidgetBackgroundImage]Widget unit count : " + wUnitListLth);
        }
        byte[][] wBgImgSrcList = new byte[wUnitListLth][];
        Hashtable wBgImgSrcHash = (Hashtable) SharedMemory.getInstance().get(Widget.SMKEY_BG_IMAGE_SRC_LIST);
        String curLang = DashboardPreferenceProxy.getInstance().getCurrentLanguage();
        for (int i = 0; i < wUnitListLth; i++) {
            if (wUnitList[i] == null) {
                continue;
            }
            String imgName = null;
            if (curLang.equals(Definitions.LANGUAGE_FRENCH)) {
                imgName = wUnitList[i].getImageNameFrench();
            } else if(curLang.equals(Definitions.LANGUAGE_ENGLISH)) {
                imgName = wUnitList[i].getImageNameEnglish();
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getWidgetBackgroundImage]Widget image name-" + i + " : " + imgName);
            }
            if (imgName == null) {
                continue;
            }
            byte[] wBgImgSrc = null;
            if (wBgImgSrcHash != null) {
                byte[] wBgImgSrcTemp = (byte[])wBgImgSrcHash.get(imgName);
                if (wBgImgSrcTemp != null) {
                    wBgImgSrc = (byte[])wBgImgSrcTemp.clone();
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getWidgetBackgroundImage]Widget background image from shared memory-"
                                + i + " : " + wBgImgSrc);
            }
            if (wBgImgSrc == null) {
                wBgImgSrc = WeatherWidgetRPManager.getInstance().getWidgetBackgroundImage(bgServerInfo, imgName);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[WeatherWidgetDataMgr.getWidgetBackgroundImage]Widget background image from server-"
                                    + i + " : " + wBgImgSrc);
                }
                if (wBgImgSrc != null) {
                    wBgImgSrcHash.put(imgName, wBgImgSrc);
                    SharedMemory.getInstance().remove(Widget.SMKEY_BG_IMAGE_SRC_LIST);
                    SharedMemory.getInstance().put(Widget.SMKEY_BG_IMAGE_SRC_LIST, wBgImgSrcHash);
                }
            }
            wBgImgSrcList[i] = wBgImgSrc;
        }
        return wBgImgSrcList;
    }
    public String getCurrentLanguageTag() {
        return curLanguageTag;
    }
    public LatestForecast getLatestForecast(String regLocId) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]start.");
            Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Param  - Request location id : [" + regLocId + "]");
        }
        if (regLocId == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Request location id is null. return null.");
            }
            return null;
        }
        String lType = getCurrentLanguageTag();
        if (lType == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Language type is null. return null.");
            }
            return null;
        }
        String latestForecastHashKey = regLocId + SEPARATOR + lType;
        LatestForecast lForecast = (LatestForecast) latestForecastHash.get(latestForecastHashKey);
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Cached latest forecast hash key : "+latestForecastHashKey);
            Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Cached latest forecast : "+lForecast);
        }
        if (lForecast != null) {
            boolean isValidLatestForecastAboutUpdateDate = isValidLatestForecastAboutUpdateDate(lForecast);
            if (isValidLatestForecastAboutUpdateDate) {
                return lForecast;
            }
            latestForecastHash.remove(latestForecastHashKey);
            lForecast = null;
        }
        // Invalid latest forecast. so gets latest forecast from server.
        lForecast = WeatherWidgetRPManager.getInstance().getLatestForecast(lType, regLocId);
        if (lForecast != null) {
            latestForecastHash.put(latestForecastHashKey, lForecast);
        }
        return lForecast;
    }
    public Image getWeatherIconImage(String reqIconCode) {
        if (reqIconCode == null) {
            return null;
        }
        return DataCenter.getInstance().getImage("shared/icons/"+reqIconCode.trim()+".png");
    }
    public int getTemperatureUnit() {
        String uppTemperatureUnit = DashboardPreferenceProxy.getInstance().getTemperatureUnit();
        if (uppTemperatureUnit != null && uppTemperatureUnit.equals(TEMPERATURE_UNIT_F)) {
            temperatureUnit = TEMPERATURE_UNIT_ID_F;
        } else {
            temperatureUnit = TEMPERATURE_UNIT_ID_C;
        }
        return temperatureUnit;
    }
    public String getHomeTownLocationId() throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]start.");
        }
        String homeTownLocId = wwPrefProxy.getHomeTownLocationId();
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]return cached (UPP) home town location id : " + homeTownLocId);
        }
        if (homeTownLocId != null && homeTownLocId.length() > 0) {
            return homeTownLocId;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]Get home town location id by postal code.");
        }
        String adminPostalCode = wwPrefProxy.getPostalCode();
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]UPP admin postal code : "
                    + adminPostalCode);
        }
        String lTag = getCurrentLanguageTag();
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]Language tag : " + lTag);
        }
        CityInfo cInfo = WeatherWidgetRPManager.getInstance().getCityInfoByPostalCode(lTag, adminPostalCode);
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]Response city info : " + cInfo);
        }
        if (cInfo != null) {
            homeTownLocId = cInfo.getLocationId();
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]Response home town location id : " + homeTownLocId);
            }
            if (homeTownLocId != null && homeTownLocId.length() > 0) {
                wwPrefProxy.setHomeTownLocationId(homeTownLocId);
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[WeatherWidgetDataMgr.getHomeTownLocationId]end : " + homeTownLocId);
        }
        return homeTownLocId;
    }
    /*****************************************************************
     * variables - SharedMemory-related
     *****************************************************************/
    private void initLatestForecastData() {
        latestForecastHash = new Hashtable();
        Hashtable weatherDataListHash = (Hashtable)sm.get(SMKEY_WEATHER_DATA_LIST);
        if (weatherDataListHash == null) {
            return ;
        }
        Enumeration enu = weatherDataListHash.keys();
        while(enu.hasMoreElements()) {
            String key = (String)enu.nextElement();
            Hashtable weatherData = (Hashtable) weatherDataListHash.get(key);
            LatestForecast lf = convertLatestForecastFromHashtable(weatherData);
            if (lf != null && isValidLatestForecastAboutUpdateDate(lf)) {
                latestForecastHash.put(key, lf);
            }
        }
    }
    private void disposeLatestForecastData() {
        if (latestForecastHash == null) {
            return;
        }
        Hashtable weatherDataListHash = null;
        Enumeration enu = latestForecastHash.keys();
        while(enu.hasMoreElements()) {
            String key = (String)enu.nextElement();
            LatestForecast lf = (LatestForecast) latestForecastHash.get(key);
            if (isValidLatestForecastAboutUpdateDate(lf)) {
                Hashtable hash = convertHashtableFromLatestForecast(lf);
                if (hash != null) {
                    if (weatherDataListHash == null) {
                        weatherDataListHash = new Hashtable();
                    }
                    weatherDataListHash.put(key, hash);
                }
            }
        }
        if (sm.get(SMKEY_WEATHER_DATA_LIST) != null) {
            sm.remove(SMKEY_WEATHER_DATA_LIST);
        }
        if (weatherDataListHash != null) {
            sm.put(SMKEY_WEATHER_DATA_LIST, weatherDataListHash);
        }
    }
    private boolean isValidLatestForecastAboutUpdateDate(LatestForecast reqLatestForecast) {
        if (reqLatestForecast == null) {
            return false;
        }
        Date nextUpdateDate = reqLatestForecast.getNextUpdateDate();
        Date curDate = new Date();
        if (nextUpdateDate != null && curDate.before(nextUpdateDate)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[WeatherWidgetDataMgr.getLatestForecast]Latest forecast is valid.");
            }
            return true;
        }
        return false;
    }
    private LatestForecast convertLatestForecastFromHashtable(Hashtable reqHash) {
        if (reqHash == null) {
            return null;
        }
        String locationId = (String)reqHash.get(SMKEY_WEATHER_DATA_LOCATION_ID);
        String cityName = (String)reqHash.get(SMKEY_WEATHER_DATA_CITY_NAME);
        String countryName = (String)reqHash.get(SMKEY_WEATHER_DATA_COUNTRY_NAME);
        String provinceName = (String) reqHash.get(SMKEY_WEATHER_DATA_PROVINCE_NAME);
        Integer tempCityType = (Integer)reqHash.get(SMKEY_WEATHER_DATA_CITY_TYPE);
        int cityType = CityInfo.CITY_TYPE_CANADA;
        if (tempCityType != null) {
            cityType = tempCityType.intValue();
        }
        String iconCode = (String)reqHash.get(SMKEY_WEATHER_DATA_ICON_CODE);
        String forecastText = (String)reqHash.get(SMKEY_WEATHER_DATA_FORECAST_TEXT);
        String temperatureC = (String)reqHash.get(SMKEY_WEATHER_DATA_TEMPERATURE_C);
        String temperatureF = (String)reqHash.get(SMKEY_WEATHER_DATA_TEMPERATURE_F);
        String nextUpdateDate = (String)reqHash.get(SMKEY_WEATHER_DATA_NEXT_UPDATE_DATE);

        CityInfo cInfo = new CityInfo();
        cInfo.setLocationId(locationId);
        cInfo.setCityName(cityName);
        cInfo.setCountryName(countryName);
        cInfo.setProvinceName(provinceName);
        cInfo.setCityType(cityType);

        CurrentCondition cc = new CurrentCondition();
        cc.setIconCode(iconCode);
        cc.setForecastText(forecastText);
        cc.setTemperatureC(temperatureC);
        cc.setTemperatureF(temperatureF);

        LatestForecast lf = new LatestForecast();
        lf.setCityInfo(cInfo);
        lf.setCurrentCondition(cc);
        lf.setNextUpdateDate(nextUpdateDate);
        return lf;
    }
    private Hashtable convertHashtableFromLatestForecast(LatestForecast reqLf) {
        if (reqLf == null) {
            return null;
        }
        Hashtable hash = new Hashtable();

        CityInfo cInfo = reqLf.getCityInfo();
        if (cInfo != null) {
            String locId = cInfo.getLocationId();
            if (locId != null) {
                hash.put(SMKEY_WEATHER_DATA_LOCATION_ID, locId);
            }
            String cName = cInfo.getCityName();
            if (cName != null) {
                hash.put(SMKEY_WEATHER_DATA_CITY_NAME, cName);
            }
            String countryName = cInfo.getCountryName();
            if (countryName != null) {
                hash.put(SMKEY_WEATHER_DATA_COUNTRY_NAME, countryName);
            }
            String provinceName = cInfo.getProvinceName();
            if (provinceName != null) {
                hash.put(SMKEY_WEATHER_DATA_PROVINCE_NAME, provinceName);
            }
            hash.put(SMKEY_WEATHER_DATA_CITY_TYPE, new Integer(cInfo.getCityType()));
        }
        CurrentCondition cc = reqLf.getCurrentCondition();
        if (cc != null) {
            String iconCode = cc.getIconCode();
            if (iconCode != null) {
                hash.put(SMKEY_WEATHER_DATA_ICON_CODE, iconCode);
            }
            String forecastText = cc.getForecastText();
            if (forecastText != null) {
                hash.put(SMKEY_WEATHER_DATA_FORECAST_TEXT, forecastText);
            }
            String temperatureC = cc.getTemperatureC();
            if (temperatureC != null) {
                hash.put(SMKEY_WEATHER_DATA_TEMPERATURE_C, temperatureC);
            }
            String temperatureF = cc.getTemperatureF();
            if (temperatureF != null) {
                hash.put(SMKEY_WEATHER_DATA_TEMPERATURE_F, temperatureF);
            }
        }
        String nextUpdateDate = reqLf.getNextUpdateDateStr();
        if (nextUpdateDate != null) {
            hash.put(SMKEY_WEATHER_DATA_NEXT_UPDATE_DATE, nextUpdateDate);
        }
        return hash;
    }
    public ServerInfo getServerInfo() {
        return sInfo;
    }
    /*********************************************************************************
     * Preference proxy-related
     *********************************************************************************/
    public void updatedPreferenceLanguage(String reqLang) {
        if (reqLang != null && reqLang.equals(Definitions.LANGUAGE_FRENCH)) {
            curLanguageTag = LANGUAGE_TAG_FR;
        } else {
            curLanguageTag = LANGUAGE_TAG_EN;
        }
    }

}

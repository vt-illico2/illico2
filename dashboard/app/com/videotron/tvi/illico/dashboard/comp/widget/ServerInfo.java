package com.videotron.tvi.illico.dashboard.comp.widget;

import java.net.URL;

import com.videotron.tvi.illico.log.Log;

public class ServerInfo {
    private String host; //IP or DNS
    private int port; //Port
    private String contextRoot; //Context Root
    private String urlData;
    private ProxyInfo[] proxyInfos; //Proxy Info

    public String getHost() {
        return host;
    }
    public void setHost(String host) {
        this.host = host;
        urlData = null;
    }
    public int getPort() {
        return port;
    }
    public void setPort(int port) {
        this.port = port;
        urlData = null;
    }
    public String getContextRoot() {
        return contextRoot;
    }
    public void setContextRoot(String contextRoot) {
        this.contextRoot = contextRoot;
        urlData = null;
    }
    public ProxyInfo[] getProxyInfos() {
        return proxyInfos;
    }
    public void setProxyInfos(ProxyInfo[] proxyInfos) {
        this.proxyInfos = proxyInfos;
    }
    public void setUrlData(String urlData) {
        this.urlData = urlData;
    }
    public String getUrlInfo() {
        if (urlData == null) {
            if (host == null) {
                if (Log.DEBUG_ON) {
                    Log.printError("[ServerInfo.getUrlInfo]Host infomation is null. return null.");
                }
                return null;
            }
            urlData = "http://" + host + ":" + port;
            String tempCtxRoot = contextRoot;
            if (tempCtxRoot != null && tempCtxRoot.trim().length() > 0) {
                if (!tempCtxRoot.startsWith("/")) {
                    tempCtxRoot = "/" + tempCtxRoot;
                }
                urlData += tempCtxRoot;
            }
        }
        if (urlData != null && urlData.endsWith("/")) {
            urlData = urlData.substring(0, urlData.length()-1);
        }
        return urlData;
    }
    public URL getURL(String fileName) throws Exception {
        if (Log.DEBUG_ON) {
            Log.printDebug("[ServerInfo.getURL]Start.");
        }
        if (fileName == null || fileName.trim().length() == 0) {
            if (Log.DEBUG_ON) {
                Log.printError("[ServerInfo.getURL]File name is null. return null.");
            }
            return null;
        }
        if (fileName.startsWith("/")) {
            fileName = fileName.substring(1);
        }
        String urlInfo = getUrlInfo();
        if (urlInfo == null) {
            if (Log.DEBUG_ON) {
                Log.printError("[ServerInfo.getURL]URL info is null. return null.");
            }
            return null;
        }
        urlInfo += "/" + fileName;
        if (Log.DEBUG_ON) {
            Log.printDebug("[ServerInfo.getURL]URL info : " + urlInfo);
        }
        URL url = null;
        if (proxyInfos == null || proxyInfos.length == 0) {
            url = new URL(urlInfo);
            if (Log.DEBUG_ON) {
                Log.printDebug("[ServerInfo.getURL]non-Proxy URL : " + url);
            }
        } else {
            for (int i = 0; i < proxyInfos.length; i++) {
                if (proxyInfos[i] == null) {
                    continue;
                }
                String proxyHost = proxyInfos[i].getProxyHost();
                int proxyPort = proxyInfos[i].getProxyPort();
                url = new URL("HTTP", proxyHost, proxyPort, urlInfo);
                if (url != null) {
                    break;
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("[ServerInfo.getURL]Proxy URL : " + url);
            }
        }
        return url;
    }
}

package com.videotron.tvi.illico.dashboard.comp.dashboard;


public interface DashboardListener {
    void focusLost();
    void foucsGained();
    void focusReject();
    void timeout();
    void animationEnded();
}

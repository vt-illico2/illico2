package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.dashboard.comp.SAXHandlerAdapter;
import com.videotron.tvi.illico.log.Log;

public class LatestForecastHandler extends SAXHandlerAdapter {
    private LatestForecast latestForecast;
    private String valueXMLTag;

    public void startDocument() {
        latestForecast = new LatestForecast();
    }
    public void endDocument() {
        setResultObject(latestForecast);
    }
    public void startElement(String valueXMLTag, Attributes attr) {
        this.valueXMLTag = valueXMLTag;
        if (valueXMLTag == null) {
            return;
        }
        if (valueXMLTag.equalsIgnoreCase("SITE")) {
            latestForecast.setCityInfo(getCityInfo(attr));
        } else if (valueXMLTag.equalsIgnoreCase("OBS")) {
            latestForecast.setCurrentCondition(getCurrentCondition(attr));
        }
    }
    public void parseCDData(String valueCDData) {
        if (valueXMLTag!= null && valueXMLTag.equalsIgnoreCase("NextUpdate")) {
            latestForecast.setNextUpdateDate(valueCDData);
        } 
    }

    private CityInfo getCityInfo(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CityInfo ci = new CityInfo();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("NAME")) {
                ci.setCityName(lValue);
            } else if (lName.equalsIgnoreCase("PROV")) {
                ci.setProvinceName(lValue);
            } else if (lName.equalsIgnoreCase("COUNTRY")) {
                ci.setCountryName(lValue);
                if (lValue != null) {
                    if (lValue.equalsIgnoreCase("Canada")) {
                        ci.setCityType(CityInfo.CITY_TYPE_CANADA);
                    } else {
                        ci.setCityType(CityInfo.CITY_TYPE_OTHER_CITY);
                    }
                }
            } else if (lName.equalsIgnoreCase("LOCATIONID")) {
                ci.setLocationId(lValue);
            }
        }
        return ci;
    }
    private CurrentCondition getCurrentCondition(Attributes attr) {
        if (attr == null) {
            return null;
        }
        CurrentCondition cc = new CurrentCondition();
        int attrLth = attr.getLength();
        for (int i=0; i<attrLth; i++) {
            String lName = attr.getLocalName(i);
            if (lName == null) {
                continue;
            }
            String lValue = attr.getValue(i);
            if (Log.DEBUG_ON) {
                Log.printDebug("[LatestForecastHandler.getCurrentCondition]lName : "+lName);
                Log.printDebug("[LatestForecastHandler.getCurrentCondition]lValue : "+lValue);
            }
            try{
                lValue = new String(lValue.getBytes(), "UTF-8");
            }catch(Exception e) {
                e.printStackTrace();
            }
            if (lName.equalsIgnoreCase("DATE")) {
                cc.setWeatherDate(lValue);
            } else if (lName.equalsIgnoreCase("STATION")) {
                cc.setWeatherStation(lValue);
            } else if (lName.equalsIgnoreCase("ICON")) {
                cc.setIconCode(lValue);
            } else if (lName.equalsIgnoreCase("FORECAST_TEXT")) {
                cc.setForecastText(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_C")) {
                cc.setTemperatureC(lValue);
            } else if (lName.equalsIgnoreCase("TEMPERATURE_F")) {
                cc.setTemperatureF(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_C")) {
                cc.setFeelsLikeC(lValue);
            } else if (lName.equalsIgnoreCase("FEELS_LIKE_F")) {
                cc.setFeelsLikeF(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_KM")) {
                cc.setWindSpeedKm(lValue);
            } else if (lName.equalsIgnoreCase("WIND_SPEED_MI")) {
                cc.setWindSpeedMi(lValue);
            } else if (lName.equalsIgnoreCase("WIND_DIR")) {
                cc.setWindDirection(lValue);
            } else if (lName.equalsIgnoreCase("HUMIDITY")) {
                cc.setHumidity(lValue);
            }
        }
        return cc;
    }
}

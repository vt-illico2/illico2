package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import org.xml.sax.Attributes;

import com.videotron.tvi.illico.dashboard.comp.SAXHandlerAdapter;
import com.videotron.tvi.illico.log.Log;

public class WeatherDataHandler extends SAXHandlerAdapter{
    private Weather weather;

    public void startDocument() {
        weather = new Weather();
    }

    public void endDocument() {
        setResultObject(weather);
    }

    public void startElement(String valueXMLTag, Attributes attr) {
        if (valueXMLTag == null) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CityWeatherHandler.startElement]valueXMLTag:" + valueXMLTag);
        }
        if (valueXMLTag.equalsIgnoreCase("OBS")) {
            if (attr == null) {
                return;
            }
            int attrLth = attr.getLength();
            for (int i=0; i<attrLth; i++) {
                String lName = attr.getLocalName(i);
                if (lName == null) {
                    continue;
                }
                String lValue = attr.getValue(i);
                try{
                    lValue = new String(lValue.getBytes(), "UTF-8");
                }catch(Exception e) {
                    e.printStackTrace();
                }
                if (lName.equalsIgnoreCase("ICON")) {
                    weather.setIconCode(lValue);
                } else if (lName.equalsIgnoreCase("TEMPERATURE_C")) {
                    weather.setTemperatureC(lValue);
                } else if (lName.equalsIgnoreCase("TEMPERATURE_F")) {
                    weather.setTemperatureF(lValue);
                }
            }
        } else if (valueXMLTag.equalsIgnoreCase("SITE")) {
            if (attr == null) {
                return;
            }
            int attrLth = attr.getLength();
            for (int i=0; i<attrLth; i++) {
                String lName = attr.getLocalName(i);
                if (lName == null) {
                    continue;
                }
                String lValue = attr.getValue(i);
                try{
                    lValue = new String(lValue.getBytes(), "UTF-8");
                }catch(Exception e) {
                    e.printStackTrace();
                }
                if (lName.equalsIgnoreCase("NAME")) {
                    weather.setCityName(lValue);
                }
            }
        }
    }
}

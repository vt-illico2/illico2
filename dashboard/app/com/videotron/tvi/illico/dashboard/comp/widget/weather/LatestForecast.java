package com.videotron.tvi.illico.dashboard.comp.widget.weather;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.videotron.tvi.illico.log.Log;

public class LatestForecast {
    private final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    
    private CityInfo ci;
    private CurrentCondition cc;
    private Date nextUpdateDate;
    private String nextUpdateDateStr;

    public CityInfo getCityInfo() {
        return ci;
    }
    public void setCityInfo(CityInfo ci) {
        this.ci = ci;
    }

    public CurrentCondition getCurrentCondition() {
        return cc;
    }
    public void setCurrentCondition(CurrentCondition cc) {
        this.cc = cc;
    }

    public void setNextUpdateDate(String nextUpdateDateStr) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LatestForecast.setNextUpdateDate]Param - next update date string : " + nextUpdateDateStr);
        }
        this.nextUpdateDateStr = nextUpdateDateStr;
        if (nextUpdateDateStr == null) {
            return;
        }
        try {
            nextUpdateDate = convertXmlDateTimeToDate(nextUpdateDateStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private Date convertXmlDateTimeToDate(final String xmlDateTime) {
        if (xmlDateTime.length() != 25) {
            return null;
        }
        StringBuffer sb = new StringBuffer(xmlDateTime);
        sb.deleteCharAt(22);
        Date d = null;
        try {
            d = SDF.parse(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return d;
    }
    public Date getNextUpdateDate() {
        return nextUpdateDate;
    }
    public String getNextUpdateDateStr() {
        return nextUpdateDateStr;
    }
}

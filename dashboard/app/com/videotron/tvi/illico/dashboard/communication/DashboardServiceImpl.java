package com.videotron.tvi.illico.dashboard.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.dashboard.controller.DashboardController;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.dashboard.DashboardService;
import com.videotron.tvi.illico.log.Log;

public class DashboardServiceImpl implements DashboardService {
    /** DashboardServiceImpl instance - Singleton. */
    private static DashboardServiceImpl instance;
    /** XletContext instance. use binding dashboard and unbinding dashboard. */

    public static synchronized DashboardServiceImpl getInstance() {
        if (instance == null) {
            instance = new DashboardServiceImpl();
        }
        return instance;
    }

    private DashboardServiceImpl() {
    }

    public void init() {
        bindToIxc();
    }

    public void dispose() {
        try {
            IxcRegistry.unbind(FrameworkMain.getInstance().getXletContext(), DashboardService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Bind to IxcRegistry.
     */
    private void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("[DashboardServiceImpl.bindToIxc]bind to IxcRegistry");
            }
            IxcRegistry.bind(FrameworkMain.getInstance().getXletContext(), DashboardService.IXC_NAME, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDashboard() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardServiceImpl.showDashboard]start");
        }
        DashboardController.getInstance().showDashboard();
    }

    public void hideDashboard() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DashboardServiceImpl.hideDashboard]start");
        }
        DashboardController.getInstance().hideDashboard();
    }
}

package com.videotron.tvi.illico.dashboard.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.controller.DashboardVbmController;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;

public class CommunicationManager {
    private static CommunicationManager instance;
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS = 2000;
    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    /** The monitor service. */
    private MonitorService mSvc;
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private StcService stcSvc;
    private LogLevelChangeListenerImpl llclImpl;
    
    /*********************************************************************************
     * VBM Service-related
     *********************************************************************************/
    private VbmService vSvc;

    private CommunicationManager() {
    }

    public static synchronized CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }

    public void init() {
        llclImpl = new LogLevelChangeListenerImpl();
        if (!Rs.IS_EMULATOR) {
            lookupStcService();
            lookupVbmService();
        }
        lookupMonitorService();
    }
    public void dispose() {
        if (stcSvc != null) {
            try{
                stcSvc.removeLogLevelChangeListener(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
            stcSvc = null;
        }
        llclImpl = null;
        if (vSvc!=null) {
        	DashboardVbmController.getInstance().dispose();
            vSvc=null;
        }
        mSvc=null;
    }
    public void start() {
    }
    public void stop() {
    }
    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    public MonitorService getMonitorService() {
        return mSvc;
    }
    private void lookupMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupMonitorService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (mSvc == null) {
                            try {
                                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                                mSvc = (MonitorService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupMonitorService]not bound - " + MonitorService.IXC_NAME);
                                }
                            }
                        }
                        if (mSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupMonitorService]looked up.");
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupMonitorService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public boolean requestIsTvViewingState() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestIsTvViewingState]Start.");
        }
        if (Rs.IS_EMULATOR) {
            return false;
        }
        boolean result = false;
        try {
            if (mSvc != null && mSvc.isTvViewingState()) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestIsTvViewingState]Result : " + result);
        }
        return result;
    } 
    public String requestGetVideoContextName() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestGetVideoContextName]start");
        }
        if (Rs.IS_EMULATOR) {
            return null;
        }
        String result = null;
        try {
            if (mSvc != null) {
                result = mSvc.getVideoContextName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestGetVideoContextName]Result : " + result);
        }
        return result;
    } 
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private void lookupStcService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupStcService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (stcSvc == null) {
                            try {
                                String lookupName = "/1/2100/" + StcService.IXC_NAME;
                                stcSvc = (StcService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupStcService]not bound - " + StcService.IXC_NAME);
                                }
                            }
                        }
                        if (stcSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupStcService]looked up.");
                            }
                            try {
                                int currentLevel = stcSvc.registerApp(Rs.APP_NAME);
                                Log.printDebug("stc log level = " + currentLevel);
                                Log.setStcLevel(currentLevel);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                            if (o != null) {
                                DataCenter.getInstance().put("LogCache", o);
                            }
                            try {
                                stcSvc.addLogLevelChangeListener(Rs.APP_NAME, llclImpl);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupStcService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupVbmService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupVbmService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (vSvc == null) {
                            try {
                                String lookupName = "/1/2085/" + VbmService.IXC_NAME;
                                vSvc = (VbmService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupVbmService]not bound - " + VbmService.IXC_NAME);
                                }
                            }
                        }
                        if (vSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupVbmService]looked up.");
                            }
                            DashboardVbmController.getInstance().init(vSvc);
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupVbmService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

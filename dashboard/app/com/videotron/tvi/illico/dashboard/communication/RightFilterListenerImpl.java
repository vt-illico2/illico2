package com.videotron.tvi.illico.dashboard.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.dashboard.controller.DashboardController;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;

public class RightFilterListenerImpl implements RightFilterListener {
    private String reqAppName;
    private String[] reqParamList;

    public void setRequestApplicationName(String reqAppName) {
        this.reqAppName = reqAppName;
    }
    public void setRequestExtendedParams(String[] reqExtParams) {
        this.reqParamList = reqExtParams;
    }
    public void receiveCheckRightFilter(int response) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[receiveCheckRightFilter]response : "+response);
        }
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[receiveCheckRightFilter]reqAppName : "+reqAppName);
                Log.printDebug("[receiveCheckRightFilter]reqExtParams : "+reqParamList);
            }
            if (reqAppName == null) {
                return;
            }
            DashboardController.getInstance().requestStartUnboundApplication(reqAppName, reqParamList);
        }
    }
    public String[] getPinEnablerExplain() throws RemoteException {
        return new String[]{
                DataCenter.getInstance().getString("TxtDashboard.pin_enabler_explain_0"),
                DataCenter.getInstance().getString("TxtDashboard.pin_enabler_explain_1")
        };
    }
}

package com.videotron.tvi.illico.dashboard.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 * 
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    /** The instance - PreferenceProxy. */
    private static PreferenceProxy instance;
    /** The preference service. */
    private PreferenceService uppSvc;
    
    private static final int UPP_INDEX_COUNT = 6;
    private static final int UPP_INDEX_LANGUAGE = 0;
    private static final int UPP_INDEX_DASHBOARD_MENU_DISPLAY = 1;
    private static final int UPP_INDEX_SUSPEND_DEFAULT_TIME = 2;
    private static final int UPP_INDEX_BLOCK_BY_RATING = 3;
    private static final int UPP_INDEX_PARENTAL_CONTROL = 4;
    private static final int UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS = 5;
    
    private String[] prefNames;
    private String[] prefValues;
    
//    private RightFilterListenerImpl rImpl;

    /** The Constant LANGUAGE_TYPE_ENGLISH. */
    public static final int LANGUAGE_TYPE_ENGLISH = 0;
    /** The Constant LANGUAGE_TYPE_FRENCH. */
    public static final int LANGUAGE_TYPE_FRENCH = 1;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }
    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
        prefNames[UPP_INDEX_DASHBOARD_MENU_DISPLAY] = PreferenceNames.DASHBOARD_MENU_DISPLAY;
        prefNames[UPP_INDEX_SUSPEND_DEFAULT_TIME] = RightFilter.SUSPEND_DEFAULT_TIME;
        prefNames[UPP_INDEX_BLOCK_BY_RATING] = RightFilter.BLOCK_BY_RATINGS;
        prefNames[UPP_INDEX_PARENTAL_CONTROL] = RightFilter.PARENTAL_CONTROL;
        prefNames[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = RightFilter.PROTECT_SETTINGS_MODIFICATIONS;
        
        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_FRENCH;
        prefValues[UPP_INDEX_DASHBOARD_MENU_DISPLAY] = Definitions.OPTION_VALUE_YES;
        prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME] = Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS;
        prefValues[UPP_INDEX_BLOCK_BY_RATING] = Definitions.RATING_G;
        prefValues[UPP_INDEX_PARENTAL_CONTROL] = Definitions.OPTION_VALUE_OFF;
        prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = Definitions.OPTION_VALUE_NO;
    }

    /**
     * Inits PreferenceProxy.
     * 
     * @param xCtx
     *            the xlet context
     */
    public void init() {
//        rImpl = new RightFilterListenerImpl();
        lookupPreferenceService();
    }
    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (uppSvc != null) {
            try{
                uppSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception e) {
                e.printStackTrace();
            }
            uppSvc = null;
        }
//        rImpl = null;
    }
    /**
     * Start PreferenceProxy.
     */
    public void start() {
    }
    /**
     * Stop PreferenceProxy.
     */
    public void stop() {
    }
    /**
     * Look up User Profile and Preference.
     */
    private void lookupPreferenceService() {
        /** IXC Binding */
        try {
            new Thread() {
                public void run() {
                    String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                    while (true) {
                        if (uppSvc == null) {
                            try {
                                uppSvc = (PreferenceService) IxcRegistry.lookup(FrameworkMain.getInstance().getXletContext(), lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[DashboardController.lookupPreferenceService]not bound - " + PreferenceService.IXC_NAME);
                                }
                            }
                        }
                        if (uppSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("PreferenceProxy.lookUpService: preferenceService ");
                            }
                            String[] uppData = null;
                            try {
                                uppData = uppSvc.addPreferenceListener(PreferenceProxy.this, Rs.APP_NAME, prefNames, 
                                        prefValues);
                            } catch (Exception ignore) {
                                ignore.printStackTrace();
                            }
                            if (uppData != null) {
                                try {
                                    receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    receiveUpdatedPreference(PreferenceNames.DASHBOARD_MENU_DISPLAY, uppData[UPP_INDEX_DASHBOARD_MENU_DISPLAY]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    receiveUpdatedPreference(RightFilter.SUSPEND_DEFAULT_TIME, uppData[UPP_INDEX_SUSPEND_DEFAULT_TIME]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    receiveUpdatedPreference(RightFilter.BLOCK_BY_RATINGS, uppData[UPP_INDEX_BLOCK_BY_RATING]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    receiveUpdatedPreference(RightFilter.PARENTAL_CONTROL, uppData[UPP_INDEX_PARENTAL_CONTROL]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    receiveUpdatedPreference(RightFilter.PROTECT_SETTINGS_MODIFICATIONS, uppData[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS]);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            break;
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_UPP);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * invoked when UPP application has been finished to check Rights and Filters.
     * @param result
     *            result checks ring filter
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveCheckRightFilter(String result) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy.receiveCheckRightFilter: result-" + result);
        }
    }
    /**
     * invoked when the value of the preference has changed.
     * @param name
     *            Updated Preference Name
     * @param value
     *            Updated value of Preference
     * @throws RemoteException
     *             the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PreferenceProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            prefValues[UPP_INDEX_LANGUAGE] = value;
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
        } else if (name.equals(PreferenceNames.DASHBOARD_MENU_DISPLAY)) {
            prefValues[UPP_INDEX_DASHBOARD_MENU_DISPLAY] = value;
        } else if (name.equals(RightFilter.SUSPEND_DEFAULT_TIME)) {
            prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME] = value;
        } else if (name.equals(RightFilter.BLOCK_BY_RATINGS)) {
            prefValues[UPP_INDEX_BLOCK_BY_RATING] = value;
        } else if (name.equals(RightFilter.PARENTAL_CONTROL)) {
            prefValues[UPP_INDEX_PARENTAL_CONTROL] = value;
        } else if (name.equals(RightFilter.PROTECT_SETTINGS_MODIFICATIONS)) {
            prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = value;
        }
    }
    /*********************************************************************************
     * Language-related
     *********************************************************************************/
    public String getCurrentLanguage() {
        return prefValues[UPP_INDEX_LANGUAGE];
    }
    /*********************************************************************************
     * Parental control-related
     *********************************************************************************/
//    public boolean isActivatedParentalControl() {
//        if (Log.DEBUG_ON) {
//            Log.developer.printDebug("[PrefProxy.isActivatedParentalControl]Block by rating value : "
//                    + prefValues[UPP_INDEX_BLOCK_BY_RATING]);
//        }
//        if (prefValues[UPP_INDEX_BLOCK_BY_RATING] == null) {
//            if (Log.DEBUG_ON) {
//                Log.developer.printDebug("[PrefProxy.isActivatedParentalControl]Block by rating value is invalid.");
//            }
//            return false;
//        }
//        boolean result = false;
//        if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_8)) {
//            result = true;
//        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_13)) {
//            result = true;
//        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_16)) {
//            result = true;
//        } else if (prefValues[UPP_INDEX_BLOCK_BY_RATING].equals(Definitions.RATING_18)) {
//            result = true;
//        }
//        if (Log.DEBUG_ON) {
//            Log.developer.printDebug("[PrefProxy.isActivatedParentalControl]Result : " + result);
//        }
//        return result;
//    }
//    public boolean isEnabledParentalControl() {
//        if (Log.DEBUG_ON) {
//            Log.developer.printDebug("[PrefProxy.isEnabledParentalControl]Parental control value : "
//                    + prefValues[UPP_INDEX_PARENTAL_CONTROL]);
//        }
//        if (prefValues[UPP_INDEX_PARENTAL_CONTROL] == null) {
//            if (Log.DEBUG_ON) {
//                Log.developer.printDebug("[PrefProxy.isEnabledParentalControl]Parental control value is invalid.");
//            }
//            return false;
//        }
//        boolean result = false;
//        if (prefValues[UPP_INDEX_PARENTAL_CONTROL].equals(Definitions.OPTION_VALUE_ON)) {
//            result = true;
//        }
//        if (Log.DEBUG_ON) {
//            Log.developer.printDebug("[PrefProxy.isActivatedParentalControl]Result : " + result);
//        }
//        return result;
//    }
    public boolean setParentalControl(String reqValue) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.activateParentalControl]Request value : " + reqValue);
            Log.printDebug("[PrefProxy.activateParentalControl]Current parental control : " + prefValues[UPP_INDEX_PARENTAL_CONTROL]);
        }
        if (reqValue == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.activateParentalControl]Request value is invalid.");
            }
            return false;
        }
        boolean result = false;
        if (reqValue.equals(prefValues[UPP_INDEX_PARENTAL_CONTROL])) {
            result = true;
        } else {
            if (uppSvc != null) {
                try{
                    result = uppSvc.setPreferenceValue(RightFilter.PARENTAL_CONTROL, reqValue);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.activateParentalControl]result : " + result);
        }
        return result; 
    }
    public String getParentalControl() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.getParentalControl]Parental control : " + prefValues[UPP_INDEX_PARENTAL_CONTROL]);
        }
        return prefValues[UPP_INDEX_PARENTAL_CONTROL];
    }
    public String getCurrentSuspendDefaultTime() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.getCurrentSuspendDefaultTime]Suspend default time : " + prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME]);
        }
        return prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME];
    }
    /*********************************************************************************
     * Dashboard-related
     *********************************************************************************/
    public boolean isDisplayDashboardMenu() {
        if (prefValues == null || prefValues[UPP_INDEX_DASHBOARD_MENU_DISPLAY] == null) {
            return false;
        }
        if (prefValues[UPP_INDEX_DASHBOARD_MENU_DISPLAY].equals(Definitions.OPTION_VALUE_YES)) {
            return true;
        }
        return false;
    }
    public boolean setDisplayDashboardMenu(String reqValue) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setDisplayDashboardMenu]Parameter reqValue : " + reqValue);
        }
        if (reqValue == null) {
            return false;
        }
        if (reqValue.equals(prefValues[UPP_INDEX_DASHBOARD_MENU_DISPLAY])) {
            return true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setCurrentLanguage]PreferenceService : " + uppSvc);
        }
        if (uppSvc != null) {
            try{
                uppSvc.setPreferenceValue(PreferenceNames.DASHBOARD_MENU_DISPLAY, reqValue);
            } catch(Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }
    /*********************************************************************************
     * PIN Enabler-related
     *********************************************************************************/
    public boolean showPinEnabler(PinEnablerListener l, String[] msg) {
        if (uppSvc != null) {
            try {
                uppSvc.showPinEnabler(l, msg);
                return true;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    } 
    public boolean isProtectSettingsModifications() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isProtectSettingsModifications]Called.");
        }
        if (prefValues == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Preference value list is null return false.");
            }
            return false;
        }
        if (prefValues[UPP_INDEX_PARENTAL_CONTROL] == null
                || !prefValues[UPP_INDEX_PARENTAL_CONTROL].equals(Definitions.OPTION_VALUE_ON)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Parental control value is not on. return false.");
            }
            return false;
        }
        if (prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] == null
                || prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS].equals(Definitions.OPTION_VALUE_NO)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Protect settings modification value is not yes. return false.");
            }
            return false;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isProtectSettingsModifications]return true.");
        }
        return true;
    }
    /*********************************************************************************
     * CheckRight-related
     *********************************************************************************/
    public boolean requestCheckRightFilter(String appName, String[] extParams) {
        RightFilterListenerImpl rImpl = new RightFilterListenerImpl();
        rImpl.setRequestApplicationName(appName);
        rImpl.setRequestExtendedParams(extParams);
        try {
            uppSvc.checkRightFilter(rImpl, appName,
                    new String[] { RightFilter.PROTECT_SETTINGS_MODIFICATIONS }, null, null);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}

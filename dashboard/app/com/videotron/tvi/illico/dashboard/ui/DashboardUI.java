package com.videotron.tvi.illico.dashboard.ui;

import java.awt.Image;
import java.rmi.RemoteException;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.dashboard.Rs;
import com.videotron.tvi.illico.dashboard.communication.CommunicationManager;
import com.videotron.tvi.illico.dashboard.communication.PreferenceProxy;
import com.videotron.tvi.illico.dashboard.comp.SharedMemoryManager;
import com.videotron.tvi.illico.dashboard.comp.dashboard.Dashboard;
import com.videotron.tvi.illico.dashboard.comp.dashboard.DashboardListener;
import com.videotron.tvi.illico.dashboard.comp.widget.Widget;
import com.videotron.tvi.illico.dashboard.comp.widget.WidgetListener;
import com.videotron.tvi.illico.dashboard.controller.DashboardController;
import com.videotron.tvi.illico.dashboard.controller.DashboardVbmController;
import com.videotron.tvi.illico.dashboard.gui.RendererDashboard;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;

public class DashboardUI extends UIComponent implements DashboardListener, MenuListener, WidgetListener {
    private static final long serialVersionUID = -5947200732204758199L;
    private SharedMemoryManager smMgr = SharedMemoryManager.getInstance();
    private Dashboard dashboard;
    public static final int DASHBOARD_X = 80;
    public static final int DASHBOARD_Y = 370;
    public static final int DASHBOARD_W = 800;
    public static final int DASHBOARD_H = 200;
    public static final int DASHBOARD_MAX_VISIBLE_WIDGET_COUNT = 4;
    /*********************************************************************************
     * Option-related
     *********************************************************************************/
    private OptionScreen optScr;
    private static final MenuItem OPTION_SUSPEND_PARENTAL_CONTROL = new MenuItem(
            "TxtDashboard.option.suspend_parental_control");
    private static final MenuItem OPTION_ENABLE_PARENTAL_CONTROL = new MenuItem("TxtDashboard.option.enable_parental_control");
    private static final MenuItem OPTION_ACTIVATE_PARENTAL_CONTROL = new MenuItem(
            "TxtDashboard.option.activate_parental_control");
    private static final MenuItem OPTION_HELP = new MenuItem("TxtDashboard.option.help");
    
    public static final String APP_NAME_PROFILE = "Profile";
    /*********************************************************************************
     * Footer-related
     *********************************************************************************/
    private Hashtable footerHash;
    private Footer footer;
    private static final int FOOTER_COUNT = 3;
    private static final int FOOTER_MENU = 0;
    private static final int FOOTER_HIDE_WIDGETS = 1;
    private static final int FOOTER_OPTION = 2;
    private String[] footerButtonNameList = {PreferenceService.BTN_MENU, PreferenceService.BTN_EXIT,
    		PreferenceService.BTN_D};
    private String[] footerTxtKeyList = {"TxtDashboard.footer.access.menu", "TxtDashboard.footer.exit", 
    		"TxtDashboard.footer.option"};

    public DashboardUI() {
        if (renderer == null) {
            renderer = new RendererDashboard();
        }
        setRenderer(renderer);
    }
    public void ready() {
        if (optScr == null) {
            optScr = new OptionScreen();
        }
        dashboard = smMgr.getDashboard(Dashboard.DASHBOARD_TYPE_DASHBOARD);
        String[] widgetOrders = smMgr.getWidgetOrder(Dashboard.DASHBOARD_TYPE_DASHBOARD);
        if (dashboard != null && widgetOrders != null) {
            // add Normal Widget
            for (int i = 0; i < widgetOrders.length; i++) {
                String widgetID = widgetOrders[i];
                Widget widget = smMgr.getWidget(widgetID, Widget.WIDGET_CONTEXTUAL_STATE_DASHBOARD);
                if (Log.DEBUG_ON) {
                    Log.printDebug("[DashboardUI.start]widgetID-widget : " + widgetID+" - "+widget);
                }
                if (widget == null) {
                    continue;
                }
                widget.setWidgetListener(this);
                dashboard.addWidget(widget);
            }
//            int x = dashboard.getLocationX();
//            int y = dashboard.getLocationY();
//            int w = dashboard.getSizeW();
//            int h = dashboard.getSizeH();
//            if (Log.DEBUG_ON) {
//                Log.printDebug("[DashboardUI.start]x : " + x);
//                Log.printDebug("[DashboardUI.start]y : " + y);
//                Log.printDebug("[DashboardUI.start]w : " + w);
//                Log.printDebug("[DashboardUI.start]h : " + h);
//            }
//            dashboard.setBounds(x, y, w, h);
            dashboard.setBounds(DASHBOARD_X, DASHBOARD_Y, DASHBOARD_W, DASHBOARD_H);
            dashboard.setMaxVisibleWidgetCount(DASHBOARD_MAX_VISIBLE_WIDGET_COUNT);
            dashboard.setDashboardListener(this);
        }
    }
    public void dispose() {
    }
    public void start() {
        prepare();
        repaint();
        //Create footer
        if (footer == null) {
            footer = new Footer();
            for (int i=0; i<FOOTER_COUNT; i++) {
                Image imgIcon = getFooterImage(footerButtonNameList[i]);
                if (imgIcon != null) {
                    footer.addButton(imgIcon, footerTxtKeyList[i]);
                }
            }
            if (dashboard != null) {
//                int x = dashboard.getLocationX();
//                int y = dashboard.getLocationY();
//                int w = dashboard.getSizeW();
                footer.setBounds(DASHBOARD_X + 20, DASHBOARD_Y + 118, DASHBOARD_W, 24);
            }
            add(footer);
        }
        if (dashboard != null) {
            add(dashboard);
            dashboard.start(true);
//            dashboard.setDashboardArea(true);
        }
    }
    public void stop() {
        if (dashboard != null) {
            dashboard.stop();
            dashboard.flushAllWidget();
            remove(dashboard);
            dashboard = null;
        }
        //Dispose footer
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (optScr != null) {
            optScr.stop();
        }
    }

    public boolean handleKey(int code) {
        boolean handleKeyResult = false;
        if (dashboard != null && dashboard.isDashboardArea()) {
            switch (code) {
	            case HRcEvent.VK_INFO:
	            	if (CommunicationManager.getInstance().requestIsTvViewingState()) {
	            		String vCtxName = CommunicationManager.getInstance().requestGetVideoContextName();     		
	            		if (vCtxName != null && !vCtxName.equalsIgnoreCase(MonitorService.DEFAULT_VIDEO_NAME)) {
	            			DashboardController.getInstance().hideDashboard();         			
	            		}
	            	}
	            	return false;
                case Rs.KEY_EXIT:
                    if (footer != null) {
                        footer.clickAnimation(FOOTER_HIDE_WIDGETS);
                    }
                    DashboardController.getInstance().hideDashboard();
                    return true;
                case KeyCodes.LAST:
                    DashboardController.getInstance().hideDashboard();
                    return true;
                case KeyCodes.COLOR_A:
                	DashboardController.getInstance().hideDashboard();
                    return true;
                case KeyCodes.COLOR_D:
                    if (footer != null) {
                        footer.clickAnimation(FOOTER_OPTION);
                    }
                    MenuItem root = new MenuItem("Dashboard");
                    String parentalControlValue = PreferenceProxy.getInstance().getParentalControl();
                    if (parentalControlValue.equals(Definitions.OPTION_VALUE_ON)) {
                        root.add(OPTION_SUSPEND_PARENTAL_CONTROL);
                    } else if (parentalControlValue.equals(Definitions.OPTION_VALUE_OFF)) {
                        root.add(OPTION_ACTIVATE_PARENTAL_CONTROL);
                    } else if (parentalControlValue.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS) || parentalControlValue.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS)) {
                        root.add(OPTION_ENABLE_PARENTAL_CONTROL);
                    }
                    root.add(OPTION_HELP);
                    optScr.start(root, this);
                    if (dashboard != null) {
                        dashboard.stopTimer();
                    }
                    return true;
                case KeyCodes.LITTLE_BOY:
                	// consumed forcely
                // DDC-113 Visually Impaired : “STAR” key is blocked.
                case KeyCodes.STAR:
                    // R7.2
                case KeyCodes.ASPECT:
                	return true;
            }
            handleKeyResult = dashboard.keyAction(code);
        }
        return handleKeyResult;
    }

    /*****************************************************************
     * methods - Renderer-related
     *****************************************************************/
    public Dashboard getDashboard() {
        return dashboard;
    }

    /*****************************************************************
     * methods - DashboardListener-implemented
     *****************************************************************/
    public void focusReject() {
    }

    public void focusLost() {
    }

    public void foucsGained() {
    }

    public void timeout() {
        DashboardController.getInstance().hideDashboard();
    }
    public void animationEnded() {
    }

    /*********************************************************************************
     * MenuListener-implemented
     *********************************************************************************/
    public void canceled() {
        if (dashboard != null) {
            dashboard.startTimer();
        }
        if (optScr != null) {
            optScr.stop();
        }
    }

    public void selected(MenuItem item) {
        if (optScr != null) {
            optScr.stop();
        }
        if (item == null) {
            return;
        }
        if (item.equals(OPTION_SUSPEND_PARENTAL_CONTROL)) {
            String[] msg = new String[]{
                DataCenter.getInstance().getString("TxtDashboard.msgSuspendPIN1"),
                DataCenter.getInstance().getString("TxtDashboard.msgSuspendPIN2")
            };
            PreferenceProxy.getInstance().showPinEnabler(new PinEnablerListener() {
                public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                    if (response == PreferenceService.RESPONSE_SUCCESS) {
                        String curSusDefTime = PreferenceProxy.getInstance().getCurrentSuspendDefaultTime();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[PinEnablerListener]curSusDefTime : " + curSusDefTime);
                        }
                        if (curSusDefTime != null
                                && (curSusDefTime.equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS) || curSusDefTime
                                        .equals(Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS))) {
                            PreferenceProxy.getInstance().setParentalControl(curSusDefTime);
                        }
                    }
                    if (dashboard != null) {
                        dashboard.startTimer();
                    }
                }
            }, msg);
        } else if (item.equals(OPTION_ENABLE_PARENTAL_CONTROL)) {
            String[] msg = new String[]{
                DataCenter.getInstance().getString("TxtDashboard.msgRestorePIN1"),
                DataCenter.getInstance().getString("TxtDashboard.msgRestorePIN2")
            };
            PreferenceProxy.getInstance().showPinEnabler(new PinEnablerListener() {
                public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                    if (response == PreferenceService.RESPONSE_SUCCESS) {
                        PreferenceProxy.getInstance().setParentalControl(Definitions.OPTION_VALUE_ON);
                    }
                    if (dashboard != null) {
                        dashboard.startTimer();
                    }
                }
            }, msg);
        } else if (item.equals(OPTION_ACTIVATE_PARENTAL_CONTROL)) {
            String[] msg = new String[]{
                DataCenter.getInstance().getString("TxtDashboard.msgProtectSetting1"),
                DataCenter.getInstance().getString("TxtDashboard.msgProtectSetting2")
            };
            PreferenceProxy.getInstance().showPinEnabler(new PinEnablerListener() {
                public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                    if (response == PreferenceService.RESPONSE_SUCCESS) {
                        DashboardController.getInstance().startApplication(APP_NAME_PROFILE,
                                new String[] { Rs.APP_NAME, PreferenceService.PARENTAL_CONTROLS });
                    }
                    if (dashboard != null) {
                        dashboard.startTimer();
                    }
                }
            }, msg);
        } else if (item.equals(OPTION_HELP)) {
            DashboardController.getInstance().startApplication("Help", new String[]{null, Rs.APP_NAME});
        }
    }
    /*****************************************************************
     * Methods - Footer Image-related
     *****************************************************************/
    private Image getFooterImage(String btKey) {
        if (btKey == null) {
            return null;
        }
        if (footerHash == null) {
            footerHash = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        }
        Image imgFooter = null;
        if (footerHash != null) {
            imgFooter = (Image) footerHash.get(btKey);
        }
        return imgFooter;
    }
    
    /*****************************************************************
     * WidgetListener-implemented
     *****************************************************************/
    public void startedUnboundApplication(String widgetId, String reqAppName, String[] reqExtedParams) {
    	DashboardVbmController.getInstance().widgetSelectionApplication(widgetId, reqAppName, reqExtedParams);
    }
    public void selectedChannel(String widgetId, String reqCallLetter) {
    	DashboardVbmController.getInstance().widgetSelectionChannel(widgetId, reqCallLetter);
    }
}

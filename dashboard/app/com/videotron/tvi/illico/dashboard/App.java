package com.videotron.tvi.illico.dashboard;

import java.io.File;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import org.ocap.system.RegisteredApiManager;

import com.videotron.tvi.illico.dashboard.communication.CommunicationManager;
import com.videotron.tvi.illico.dashboard.communication.DashboardServiceImpl;
import com.videotron.tvi.illico.dashboard.communication.PreferenceProxy;
import com.videotron.tvi.illico.dashboard.controller.DashboardController;
import com.videotron.tvi.illico.dashboard.controller.DashboardDataManager;
import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

public class  App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    private XletContext xletContext;
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        System.out.println("[Dashboard][App.initXlet]start");
        xletContext = ctx;
        Log.setApplication(this);
        registerAPI();
        FrameworkMain.getInstance().init(this);
    }
    public void startXlet() throws XletStateChangeException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.startXlet]start");
        }
        FrameworkMain.getInstance().start();
        PreferenceProxy.getInstance().start();
        DashboardController.getInstance().start();
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.startXlet]end");
        }
    }
    public void pauseXlet() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.pauseXlet]start");
        }
        FrameworkMain.getInstance().pause();
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.pauseXlet]end");
        }
    }

    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.destroyXlet]start");
        }
        CommunicationManager.getInstance().dispose();
        PreferenceProxy.getInstance().dispose();
        DashboardDataManager.getInstance().dispose();
        DashboardController.getInstance().dispose();
        DashboardServiceImpl.getInstance().dispose();
        FrameworkMain.getInstance().destroy();
        if (Log.DEBUG_ON) {
            Log.printDebug("[App.destroyXlet]end");
        }
    }
    
    private void registerAPI() {
        System.out.println("[Dashboard-App.registerAPI]start");
        try {
            RegisteredApiManager rm = RegisteredApiManager.getInstance();
            rm.register(Rs.REGISTERED_API_NAME, Rs.APP_VERSION, new File("./scdf.xml").getAbsoluteFile(),
                    Rs.REGISTERED_API_STORAGE_PRIOR);
            String[] rAPINames = rm.getNames();
            if (rAPINames != null) {
                System.out.println("[Dashboard-App.registerAPI]Registered API Name Count : " + rAPINames.length);
                for (int i = 0; i < rAPINames.length; i++) {
                    String rAPIName = rAPINames[i];
                    if (rAPIName == null) {
                        continue;
                    }
                    String rAPIVersion = rm.getVersion(rAPIName);
                    System.out.println("[Dashboard-App.registerAPI]" + i + "-Name[" + rAPIName + "], Version["
                            + rAPIVersion + "]");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("[Dashboard-App.registerAPI]Exception occurred");
            return;
        }
        System.out.println("[Dashboard-App.registerAPI]end");
    }
    
    /*********************************************************************************
     * ApplicationConfig-implemented
     *********************************************************************************/
	public void init() {
        DashboardServiceImpl.getInstance().init();
        DashboardController.getInstance().init();
        DashboardDataManager.getInstance().init();
        PreferenceProxy.getInstance().init();
        CommunicationManager.getInstance().init();
	}

    /**
     * Gets the version of this application.
     *
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }

    /**
     * Gets the name of this application.
     *
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }

    /**
     * Gets the XletContext instance of this application.
     *
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

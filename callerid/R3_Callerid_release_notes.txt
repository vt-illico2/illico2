This is CallerID App Release Notes.

GENERAL RELEASE INFORMATION.
- Release Details
    - Release name
      : version 4K.3.0
    - Release App file Name
      : Callerid.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2015.06.12

***
version 4K.3.0 - 2015.06.12
    - New Feature   
        + update a 07_wizardbg.png
    - Resolved Issues
        N/A
***
version 4K.2.0 - 2015.5.13
    - New Feature   
        + update a 01_acbg_1st.png
        + update a 01_acbg_2nd.png
        + update a 01_acline.png
        + update a bg.jpg
    - Resolved Issues
        N/A
***
version 4K.1.0 - 2015.4.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
    - Resolved Issues
        N/A
***
version 1.0.11.1
    - New Feature
        N/A
    - Resolved Issues
    	+ VDTRMASTER-4792 CLD500 - At boot time the application is detecting a change in profile setting and then sending an http request to CallerID backend

***
version 1.0.10.1
    - New Feature
        N/A
    - Resolved Issues
    	+ VDTRMASTER-4792 CLD500 - At boot time the application is detecting a change in profile setting and then sending an http request to CallerID backend
***
version 1.0.9.1
    - New Feature
        N/A
    - Resolved Issues
    	+ VDTRMASTER-4792 CLD500 - At boot time the application is detecting a change in profile setting and then sending an http request to CallerID backend
***
version 1.0.8.1
    - New Feature
        N/A
    - Resolved Issues
    	+ VDTRMASTER-4792 CLD500 - At boot time the application is detecting a change in profile setting and then sending an http request to CallerID backend
***
version 1.0.7.0
    - New Feature
        + While the booting process is on-going, don't call setControlStatus API to the Server as getting the update event from Profile application
          not to generate CID500 error. When the booting finishes and there is a new update by a user, call the API.
    - Resolved Issues
    	+ N/A
***
version 1.0.6.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRSUPPORT-160 : [R2+][Caller ID][INTGR] shouldn't be able to scroll down when no calls are available.
***
version 1.0.5.0
- New Feature
        + N/A
    - Resolved Issues
        + Do not use a recursive call when the Callerid gets a  MAC address.
***
version 1.0.4.0
- New Feature
        + N/A
    - Resolved Issues
        + call the registerToServer() in new thread when event  received from EIDMappingTableUpdateListener
***
version 1.0.3.0
- New Feature
        + N/A
    - Resolved Issues
        + Arrange log print.
        + Edit code when create socket.
***
version 1.0.2.0
- New Feature
        + N/A
    - Resolved Issues
        + Error codes are edited.
            1. CID506 is added. 
            2. CLDxxx is replaced with CIDxxx.
            3. CID505 behavior is changed to 'log only'           
***
version 1.0.1.0
- New Feature
        + N/A
    - Resolved Issues
        + Error codes are edited.
            1. CID004 is added. 
            2. CID001 is replaced with CID002.
            3. CID002 is replaced with CID003.
            4. CID003 is replaced with CID004.
***
version 1.0.0.0
- New Feature
        + set VBM parameters
    - Resolved Issues
     	+ N/A
***
version 0.3.77.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRSUPPORT-160 : [R2+][Caller ID][INTGR] shouldn't be able to scroll down when no calls are available.
***
version 0.3.76.0
    - New Feature
        + N/A
    - Resolved Issues
        + Do not use a recursive call when the Callerid gets a MAC address.
***
version 0.3.75.0
    - New Feature
        + N/A
    - Resolved Issues
        + call the registerToServer() in new thread when event  received from EIDMappingTableUpdateListener
***
version 0.3.74.0
    - New Feature
        + N/A
    - Resolved Issues
        + Arrange log print.    
        + Edit code when create socket.
***
version 0.3.73.0
    - New Feature
        + N/A
    - Resolved Issues
        + Arrange log print.
        + Add defense code in 
***
version 0.3.72.0
    - New Feature
        + N/A
    - Resolved Issues
        + Error codes are edited.
            1. CID004 is added. 
            2. CID001 is replaced with CID002.
            3. CID002 is replaced with CID003.
            4. CID003 is replaced with CID004.
***
version 0.3.71.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-3520 :[Regression] [Message] [CallerID] Top banner displays in english on teaser screen of CallerID if you don't have the CallerID package and language set to french
***
version 0.3.70.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-3598 : [REGRESSION] [CALLERID] Caller Name is not aligned with other lines
***
version 0.3.69.1
    - New Feature
        + N/A
    - Resolved Issues
        + Bug fixed no update of clock in Teaser.(Related to VDTRMASTER-3568)
***
version 0.3.68.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-3475 : [Regression ATP Message] [Caller ID] Unsubscribed from Caller ID - Back/Last button on remote should go back to Main Menu but goes to last watched channel instead
***
version 0.3.67.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-3148 : [CallerID] Wrong display of time in French
***
version 0.3.66.0
    - New Feature
        + N/A
    - Resolved Issues
        + Bug fixed no encoding for the name of call log
***
version 0.3.65.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2715: [UI/GUI] Dynamic creation of the Call History is reversed
***
version 0.3.64.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2715: [UI/GUI] Dynamic creation of the Call History is reversed
***
version 0.3.63.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2715: [UI/GUI] Dynamic creation of the Call History is reversed
***
version 0.3.62.1
    - New Feature
        + N/A 
    - Resolved Issues
        + VDTRMASTER-2606: [UI/GUI] Caller ID - Number of characters for name display 
 *** 
version 0.3.61.3
    - New Feature
        + N/A 
    - Resolved Issues
        + VDTRMASTER-2654: CALLER ID No missed calls is displayed a few seconds before showing call log
        + VDTRMASTER-2670: Caller Id Breadcrumbs displayed in english
        + VDTRMASTER-2671: Caller Id Call log empty, viewer can select Delete all calls button
*** 
version 0.3.60.1
    - New Feature
        + N/A 
    - Resolved Issues
        + VDTRMASTER-2641:[UI/GUI] Caller ID - Remove "Line x:" in the caller ID banner
*** 
version 0.3.59.0
    - New Feature
        + Roll back - AV resizing after ending of the fade-in animation.
        + Sort call log by phone id.
    - Resolved Issues
        + Bug fixed no display of notification banner when it receives 'Unknown' call.
*** 
version 0.3.58.0
    - New Feature
        + AV resizing after ending of the fade-in animation.
    - Resolved Issues
        + N/A
*** 
version  0.3.57.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2525 : [UI/GUI] [Caller-ID] - Missing Page + Page - behaviour in call log
*** 
version  0.3.56.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2525 : [UI/GUI] [Caller-ID] - Missing Page + Page - behaviour in call log
***          
version  0.3.55.2
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2497 : [UI/GUI] [Caller-ID] - Wrong phone number in caller display banner
        + VDTRMASTER-2499 : [UI/GUI] [Caller-ID] - Exit icon required in Caller history.
***          
version  0.3.54.1
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-2479 : DDC 6451 - Caller Id Log date displayed in english.
***          
version  0.3.53.1
    - New Feature
        + Gets MS URL from IB
    - Resolved Issues
        + VDTRMASTER-2099 : [Caller ID] Can not remove a number that is no longer assigned to a STB
***          
version  0.3.52.0
    - New Feature
        + Applied the changed Framework-Log
    - Resolved Issues
        + N/A.
***          
version  0.3.51.0
    - New Feature
        + Clicking animation(exit button, popup button..)
    - Resolved Issues
        + VDTRMASTER-2097 -[UI/GUI] [Animation / General] - all selectable items should show a "bump" effect.
        + Print log simply about SocketTimeoutException
***          
version  0.3.50.0
    - New Feature
        + Applied modified Framework(OC unload)
    - Resolved Issues
        + Comparison of Mac address string
***          
version  0.3.49.0
    - New Feature
        + N/A
    - Resolved Issues
        + Change background image of call log
        + timer setting for retry stage when the registration is failed.
***          
version  0.3.48.0
    - New Feature
        + N/A
    - Resolved Issues
        + Bug fixed receiving channel event
***          
version  0.3.47.0
    - New Feature
        + N/A
    - Resolved Issues
        + Change the time of initial registration
***          
version  0.3.46.0
    - New Feature
        + Rebuild new TextUtil("...")
    - Resolved Issues
        + Bug fixed NPE of UdpServer
***          
version  0.3.45.0
    - New Feature
        + Get MS URL from SharedMemory
    - Resolved Issues
        + Bug fixed 'Protect access to settings'
        + Change the time of socket timeout
***          
version  0.3.44.0
    - New Feature
        + Protect access to settings
    - Resolved Issues
        + N/A
***          
version  0.3.43.2
    - New Feature
        + N/A
    - Resolved Issues
        + VDTRMASTER-1972 - Caller ID - The Back button in the teaser screen does not work
        + VDTRMASTER-1959 - [VT][Callerid] Caller number is displayed instead of called umber(phone line) in bottom of the call notification
***          
version  0.3.42.0
    - New Feature
        + Teaser image depending on Language setting. (english, french)
    - Resolved Issues
        + Bug fixed updating of the preference value on PVR box.(always listening the values.)
        + Bug fixed wrong TOC
***          
version  0.3.41.0
    - New Feature
        + N/A
    - Resolved Issues
        + Disconnecting/closing connections in a try/finally block 
***          
version  0.3.40.0
    - New Feature
        + N/A
    - Resolved Issues
       	+ Clock repaint
***          
version  0.3.39.0
    - New Feature
        + N/A
    - Resolved Issues
       	+ Clock repaint 
***          
version  0.3.38.1
    - New Feature
        + Add TOC
    - Resolved Issues
       + VDTRMASTER-1798 : [UI/GUI] [Caller ID] - TOC for Caller ID 
***
version  0.3.37.1
    - New Feature
        + Add TOC
    - Resolved Issues
       + VDTRMASTER-1786 : [UI/GUI] [Caller ID] - Missing screen in Caller ID TOC
***
version  0.3.36
    - New Feature
        + New Teaser GUI
    - Resolved Issues
       + N/A
***
version  0.3.35
    - New Feature
        + N/A
    - Resolved Issues
       + Unconditionally connect to primary server at the initial connection of first stage of retry.
***
version  0.3.34
    - New Feature
        + New TOC
    - Resolved Issues
       + N/A
***
version  0.3.33
    - New Feature
        + N/A
    - Resolved Issues
       + Do not use Log.developer.print(e) when it catches NotBoundException
***
version  0.3.32
    - New Feature
        + Change URL path ( updsettings -> updsetting)
    - Resolved Issues
       + Bug fixed exit key
       + Bug fixed  confirm popup displayed behind the scaled video (fixed by using LayeredUI)
***
version  0.3.31
    - New Feature
        + N/A
    - Resolved Issues
       + Bug fixed in deleting call log
***
version  0.3.30
    - New Feature
        + N/A
    - Resolved Issues
       + Bug fixed the previous call log is not initialized.
***
version  0.3.29
    - New Feature
        + N/A
    - Resolved Issues
       + Bug fixed updating of status notification(enable-disable)
       + Bug fixed OK key action on tab
***
version  0.3.28
    - New Feature
        + N/A
    - Resolved Issues
       + Bug fixed updating of status notification(enable-disable)
***
version  0.3.27
    - New Feature
        + N/A
    - Resolved Issues
       + Remove Back key action
       + Remove e.printStackTrace()
       + Bug fixed fade-in animation
***
version  0.3.26
    - New Feature
        + Display call notification unconditionally if the phone number is the first call
    - Resolved Issues
       + Remove footer
       + Remove test code
       + Bug fixed fade-in animation
       + Add catching IOexception in registration
***
version  0.3.25
    - New Feature
        + Fade-in animation
        + Do not rotation focus (list -> left key -> option, option -> right key -> list)
    - Resolved Issues
       + Do not display list focus when tab has a focus
       + Bug fixed tab focusing
***
version  0.3.24
    - New Feature
        + Change value VideoController.SHOW_AND_HIDE to VideoController.SHOW
    - Resolved Issues
       + N/A
***
version  0.3.23
    - New Feature
        + N/A
    - Resolved Issues
       + GUI tuning for hybrid focusing
***
version  0.3.22
    - New Feature
        + Show call notification with no condition for test
    - Resolved Issues
       + N/A
  ***
version  0.3.21
    - New Feature
        + Footer clickAnimation
        + Add image of tab
        + Tab has a focus
        + Changed time of socket timeout 60 seconds to 5 second
    - Resolved Issues
       + Bug fixed NPE of UdpServer
  ***
version  0.3.20
    - New Feature
        + Private call ^50
        + Add TOC of private call and unknown  call
    - Resolved Issues

  ***
version  0.3.19
    - New Feature
        +
    - Resolved Issues
        + Bug fixed UDP server
        + Bug fixed synchronize  notification control status of UPP  and value of server
***
version  0.3.18
    - New Feature
        + Add local preference value for storing phone line number
    - Resolved Issues
        + Changed duration time of Notification to millisecond
***
version  0.3.17
    - New Feature
        + Do not check phone line for test temporary
    - Resolved Issues
        + Bug fixed focus cannot move to delete button when multi-line tab
        + Bug fixed updating notification status
***
version  0.3.16
    - New Feature
        + Modified saving phone line
        + Prevent duplicated register to server
    - Resolved Issues
        + bug fixed focusing error when there is no call log
***
            New features -NA
                JIRA # fixed - NA
                Other             - bug fixed NPE when login

        - Release name
          : version 0.3.15
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.30
---------------------------------------------------------------------------------------------------------
               New features -NA
                JIRA # fixed - NA
                Other             - set socket timeout to 0

        - Release name
          : version 0.3.14
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.30
---------------------------------------------------------------------------------------------------------
               New features - add UDP server for call notification
                JIRA # fixed - NA
                Other             -NA

        - Release name
          : version 0.3.13
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.30
---------------------------------------------------------------------------------------------------------
               New features - NA
                JIRA # fixed - NA
                Other             - modify indicator of caller renderer
                                        - bug fixed  getting control status
                                       - bug fixed   deleting call log

        - Release name
          : version 0.3.12
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29
---------------------------------------------------------------------------------------------------------
               New features - NA
                JIRA # fixed - NA
                Other             - bug fixed of setting data(call log)


        - Release name
          : version 0.3.11
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29
---------------------------------------------------------------------------------------------------------
               New features - do not  login and receive ack message through Daemon
                JIRA # fixed - NA
                Other             - divide log(original , hexa)


        - Release name
          : version 0.3.10
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29

---------------------------------------------------------------------------------------------------------
                New features - login and receive ack message through Daemon
                JIRA # fixed - NA
                Other             - NA


        - Release name
          : version 0.3.9
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29

---------------------------------------------------------------------------------------------------------
                New features -rollback to v0.3.7
                JIRA # fixed - NA
                Other             - NA


        - Release name
          : version 0.3.8
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29

---------------------------------------------------------------------------------------------------------
                New features - login and receive ack message through Daemon
                JIRA # fixed - NA
                Other             - NA


        - Release name
          : version 0.3.7
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29

---------------------------------------------------------------------------------------------------------
                JIRA # fixed - NA
                Other             - set port when login to server


        - Release name
          : version 0.3.6
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.29

---------------------------------------------------------------------------------------------------------
                New features - add "called party number" to Call class
                JIRA # fixed - NA
                Other             - change in parsing code when request call log
                                       - do not change message of call notification to hexa string

        - Release name
          : version 0.3.5
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.25

---------------------------------------------------------------------------------------------------------
                New features -  NA
                JIRA # fixed - NA
                Other             - change in parsing code when request call log

        - Release name
          : version 0.3.4
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.25

---------------------------------------------------------------------------------------------------------
                New features -  add error code(when caller cannot login to server yet(CLD505).)
                JIRA # fixed - NA
                Other             - change code in registering to server

        - Release name
          : version 0.3.3
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.24

---------------------------------------------------------------------------------------------------------
         - Release Details
                New features -  add error code(when caller cannot communicated with Loading Animation)
                JIRA # fixed - NA
                Other             - bug fixed of starting exception
                                        - add logs
                                        - if the result of application authorization is not  "NOT_AUTHORIZED", it's same with "AUTHORIZED".
                                                                                        (including no file to authorize)
        - Release name
          : version 0.3.2
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.24

---------------------------------------------------------------------------------------------------------
         - Release Details
                New features -   load image when applications starts, release image when application stops.
                JIRA # fixed - NA
                Other             - request AV through EPG when call log scene

        - Release name
          : version 0.3.1
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.23

---------------------------------------------------------------------------------------------------------
         - Release Details
                New features -  save phone line number through UPP
                JIRA # fixed - NA
                Other             - build with new Monitor IXC

        - Release name
          : version 0.3.0
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.18

---------------------------------------------------------------------------------------------------------
         - Release Details
                New features -  set priority of notification
                                               remove test code
                JIRA # fixed - NA
                Other             - NA

        - Release name
          : version 0.2.03
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.14

---------------------------------------------------------------------------------------------------------
         - Release Details
                New features -  GUI,
                                               integrated with STC
                                               receive IB data
                JIRA # fixed - NA
                Other             - NA

        - Release name
          : version 0.2.02
         - Release type (official, debug, test,...)
          :   test
        - Release date
          : 2011.03.11

---------------------------------------------------------------------------------------------------------
version 0.2.01
    initial release
package com.videotron.tvi.illico.callerid;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.callerid.communication.CommunicationManager;
import com.videotron.tvi.illico.callerid.communication.PreferenceProxy;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * Caller ID main class.
 * @author Jinjoo Ok
 *
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext.*/
    private XletContext context;
    /** Application name. */
    public static final String APP_NAME = "Callerid";
    /**
     * called when application destroyed.
     * @param arg0 XletContext
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *                 state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean arg0) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
    }
    /**
     * called by Monitor when STB booted.
     * @param ctx XletContext
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        this.context = ctx;
        FrameworkMain.getInstance().init(this);
    }
    
    public void init() {
    	CallerIDController.getInstance().init();
    	CommunicationManager.getInstance().init(context);
    	PreferenceProxy.getInstance().init(context);
    }
    /**
     * called when application paused.
     */
    public final void pauseXlet() {
        if (Log.DEBUG_ON) {
            Log.printDebug("pauseXlet");
        }
        FrameworkMain.getInstance().pause();
        CallerIDController.getInstance().stop();
        FrameworkMain.getInstance().getImagePool().clear();
    }
    /**
     * called by Monitor when STB booted.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
        if (Log.DEBUG_ON) {
            Log.printDebug("startXlet!!!");
        }
        CallerIDController.getInstance().start();
    }
    /**
     * Returns name of application. The name can be used as a unique key to find <code>AppProxy</code> from
     * <code>AppsDatabase</code>.
     *
     * @return name of application
     */
    public String getApplicationName() {
        return APP_NAME;
    }

    /**
     * Returns version of application.
     *
     * @return version of application
     */
    public final String getVersion() {
        return Rs.VERSION;
    }
    /**
     * Returns the XletContext of application.
     *
     * @return XletContext
     */
    public final XletContext getXletContext() {
        return context;
    }

}

package com.videotron.tvi.illico.callerid;

import java.awt.Font;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.FontResource;
/**
 * Resource class.
 * @author Jinjoo Ok
 *
 */
public final class Rs {
    /**
     * Constructor.
     */
    private Rs() {

    }
    /** Version. */
    public static final String VERSION = "4K.3.0";//"1.0.11.1";
    /** Data Center Key - inband data path. */
    public static final String DCKEY_INBAND = "INBAND";
    /** Data Center Key - number of retries. */
    public static final String DCKEY_RETRIES = "NUMBER_OF_RETRIES";
    /** Data Center Key - retry waiting time. */
    public static final String DCKEY_RETRY_WAITING_TIME = "RETRY_WAITNG_TIME";
    /** Data Center Key - max response waiting time. */
    public static final String DCKEY_MAX_RESPONSE_WAITING_TIME = "REGISTER_MAX_RESPONSE_WAITING_TIME";
    /** Data Center Key - main server ip. */
    public static final String DCKEY_SERVER_IP = "SERVER_IP";
    /** Data Center Key - main server port for TCP. */
    public static final String DCKEY_SERVER_TCP_PORT = "SERVER_TCP_PORT";
    /** Data Center Key - main server port for UDP. */
    public static final String DCKEY_SERVER_UDP_PORT = "SERVER_UDP_PORT";
    /** Data Center Key - backup server ip. */
    public static final String DCKEY_BACKUP_SERVER_IP = "BACKUP_SERVER_IP";
    /** Data Center Key - backup server port for UDP. */
    public static final String DCKEY_BACKUP_SERVER_TCP_PORT = "BACKUP_SERVER_TCP_PORT";
    /** Data Center Key - backup server port for TCP. */
    public static final String DCKEY_BACKUP_SERVER_UDP_PORT = "BACKUP_SERVER_UDP_PORT";
    /** Data Center Key - server context root. */
    public static final String DCKEY_SERVER_CONTEXT = "SERVER_CONTEXT";
    /** Data Center Key - backup server context root. */
    public static final String DCKEY_BACKUP_SERVER_CONTEXT = "BACKUP_SERVER_CONTEXT";
    public static final String DCKEY_PROXY_USE = "PROXY_USE";
    public static final String DCKEY_PROXY_HOST = "PROXY_HOST";
    public static final String DCKEY_PROXY_PORT = "PROXY_PORT";
    /** Data Center Key - banner display time. */
    public static final String DCKEY_BANNER_DISPLAY_TIME = "BANNER_DISPLAY_TIME";
    /** Data Center Key - banner activation. */
    public static final String DCKEY_BANNER_ACTIVATION = "BANNER_ACTIVATION";
    /** Data Center Key - total time of first retry stage(minute). */
    public static final String DCKEY_1ST_REQUEST_TOTAL_TIME = "1ST_REQUEST_TOTAL_TIME";
    /** Data Center Key - repeated time of first retry stage(minute). */
    public static final String DCKEY_1ST_REQUEST_REPEAT_TIME = "1ST_REQUEST_REPEAT_TIME";
    /** Data Center Key - total time of second retry stage(minute). */
    public static final String DCKEY_2ND_REQUEST_TOTAL_TIME = "2ND_REQUEST_TOTAL_TIME";
    /** Data Center Key - repeated time of second retry stage(minute). */
    public static final String DCKEY_2ND_REQUEST_REPEAT_TIME = "2ND_REQUEST_REPEAT_TIME";
    /** Data Center Key - total time of  third retry stage(hour). */
    public static final String DCKEY_3RD_REQUEST_TOTAL_TIME = "3RD_REQUEST_TOTAL_TIME";
    /** Data Center Key - repeated time of third retry stage(hour). */
    public static final String DCKEY_3RD_REQUEST_REPEAT_TIME = "3RD_REQUEST_REPEAT_TIME";
    /** Image download url. */
    public static final String DCKEY_IMG_PATH = "IMG_PATH";
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** Font. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    /** Error code - Impossible to retrieve the notification status. */
    public static final String ERROR_CODE_001 = "CID001";
    /** Error code - Impossible to set the notification status. */
    public static final String ERROR_CODE_002 = "CID002";
    /** Error code - Impossible to retrieve the call log. */
    public static final String ERROR_CODE_003 = "CID003";
    /** Error code - Impossible to delete the call log. */
    public static final String ERROR_CODE_004 = "CID004";
    /** Error code - Cannot communicate with the MediaFriends HTTP Server. */
    public static final String ERROR_CODE_500 = "CLD500";
    /** Error code - Cannot get the server information from the Management System. */
    public static final String ERROR_CODE_501 = "CLD501";
    /** Error code - Cannot parse the server information from the Management System. */
    public static final String ERROR_CODE_502 = "CLD502";
    /** Error code - Cannot retrieve teaser image from the MS HTTP Server. */
    public static final String ERROR_CODE_503 = "CLD503";
    /** Error code - Cannot communicate with the Loading Animation.*/
    public static final String ERROR_CODE_504 = "CLD504";
    /** Error code - Cannot login to server yet. */
    public static final String ERROR_CODE_505 = "CLD505";
    /**Error code - Cannot parse call notification message. */
    public static final String ERROR_CODE_506 = "CLD506";
}


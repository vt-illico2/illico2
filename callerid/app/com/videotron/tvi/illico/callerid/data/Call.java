package com.videotron.tvi.illico.callerid.data;

import com.videotron.tvi.illico.util.TextUtil;

/**
 *
 * @author Jinjoo Ok
 *
 */
public class Call {

    private String entryId;
    private String phoneId;
    /** the name who called. */
    private String name;
    /** phone number. */
    private String callingPhoneNumber;
    /** information. */
    private String information;
    /** the time of the calls.*/
    private String time;
    /** the date of the calls.*/
    private String date;
    /** Indicates whether if this call log is seen or not. */
    private boolean seen;
    /** Received phone number.*/
    private String calledPhoneNumber = TextUtil.EMPTY_STRING;
    public String getCalledPhoneNumber() {
        return calledPhoneNumber;
    }
    public void setCalledPhoneNumber(String calledPhoneNumber) {
        this.calledPhoneNumber = calledPhoneNumber;
    }
    /**
     * Gets name.
     * @return name
     */
    public String getName() {
        return name;
    }
    /**
     * Sets name.
     * @param caller name
     */
    public void setName(String caller) {
        this.name = caller;
    }
    /**
     * Gets phone number.
     * @return phone number
     */
    public String getCallingPhoneNumber() {
        return callingPhoneNumber;
    }
    /**
     * Sets phone number.
     * @param number phone number
     */
    public void setCallingPhoneNumber(String number) {
        this.callingPhoneNumber = number;
    }
    public String getInformation() {
        return information;
    }
    public void setInformation(String text) {
        this.information = text;
    }
    public String getTime() {
        return time;
    }
    /**
     * Sets the time.
     * @param t time
     */
    public void setTime(String t) {
        this.time = t;
    }
    /**
     * Gets entry-id.
     * @return entry id
     */
    public String getEntryId() {
        return entryId;
    }
    /**
     * Sets entry id.
     * @param id entry id
     */
    public void setEntryId(String id) {
        this.entryId = id;
    }
    /**
     * Gets phone-id.
     * @return phone id
     */
    public String getPhoneId() {
        return phoneId;
    }
    /**
     * Sets phone id.
     * @param id phone id
     */
    public void setPhoneId(String id) {
        this.phoneId = id;
    }

    public boolean isSeen() {
        return seen;
    }
    /**
     * Sets seen/unseen.
     * @param seen whether if call log is seen or not.
     */
    public void setSeen(boolean seen) {
        this.seen = seen;
    }
    public String getDate() {
        return date;
    }
    /**
     * Sets the date.
     * @param d date
     */
    public void setDate(String d) {
        this.date = d;
    }
}

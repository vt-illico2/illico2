package com.videotron.tvi.illico.callerid.data;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Locale;

import com.videotron.tvi.illico.callerid.communication.PreferenceProxy;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * DataManager class manages Call Log received from back-end and Configuration Data packaged with Application.
 * @author Jinjoo Ok
 *
 */
public final class DataManager {
    /** Instance of DataManager. */
    private static DataManager instance = new DataManager();
    /** list of call log. */
    private Call[][] callList;
    /** Constructor. */
    private DataManager() {
    }
    /**
     * Gets instance of DataManager.
     * @return DataManager
     */
    public static DataManager getInstance() {
        return instance;
    }
    /**
     * Initialize call list.
     */
    public void init() {
        if (callList != null) {
            for (int i = 0; i < callList.length; i++) {
                callList[i] = null;
            }
            callList = null;
        }
    }
    /**
     * To classify by phone id, arranges call log.
     * @param call list of call
     */
    public void setCallLog(Call[] call) {
        if (call != null) {
            HashMap callMap = new HashMap();
            String id = null;
            ArrayList ar = null;
            for (int i = 0; i < call.length; i++) {
                id = call[i].getPhoneId();
                if (callMap.get(id) == null) {
                    ar = new ArrayList();
                    ar.add(call[i]);
                    callMap.put(id, ar);
                } else {
                    ar = (ArrayList) callMap.get(id);
                    ar.add(call[i]);
                }
            }
            this.callList = new Call[callMap.size()][];
            Object[] keys = callMap.keySet().toArray();
            ArrayList keyList = new ArrayList();
            for (int i = 0; i < keys.length; i++) {
                keyList.add(keys[i]);
            }
            ArrayList list = null;
            boolean removed = false;
            String line = DataCenter.getInstance().getString(PreferenceProxy.PHONE_ID_LINE);
            String[] token = TextUtil.tokenize(line, "|");
            String[] temp = null;
            for (int i = 0; i < token.length; i++) {
                temp = TextUtil.tokenize(token[i], ":");
                boolean exist = false;
                for (int j = 0; j < callList.length; j++) {
                    if (temp[0].equals((String) keyList.get(j))) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    removed = true;
                    PreferenceProxy.getInstance().updataPhoneLineTab(temp[0], "-1");
                }
            }
//            for (int i = 0; i < callList.length; i++) {
//                if (line.indexOf((String) keyList.get(i)) != -1) {
//                    isInStbExist = false;
//                    break;
//                }
//            }
            if (line.equals(TextUtil.EMPTY_STRING) || removed) {
                Collections.sort(keyList, new Sort());
                StringBuffer sb = new StringBuffer();
                for (int i = 0; i < callList.length; i++) {
                    list = (ArrayList) callMap.get((String) keyList.get(i));
                    Log.printDebug("setCallLog() key " + keyList.get(i));
                    callList[i] = new Call[list.size()];
                    for (int j = 0; j < callList[i].length; j++) {
                        callList[i][j] = (Call) list.get(j);
                    }
                    sb.append(keyList.get(i));
                    sb.append(":");
                    sb.append(String.valueOf(i));
                    if (i + 1 < callList.length) {
                        sb.append("|");
                    }
                }
                PreferenceProxy.getInstance().setPreferenceValue(PreferenceProxy.PHONE_ID_LINE, sb.toString());
            } else {
                for (int i = 0, idx = -1; i < callList.length; i++) {
                    idx = PreferenceProxy.getInstance().getPhoneLineTab((String) keyList.get(i));
                    list = (ArrayList) callMap.get((String) keyList.get(i));
                    Log.printDebug("setCallLog() key " + keyList.get(i) +" line No." + idx);
                    callList[idx] = new Call[list.size()];
                    for (int j = 0; j < callList[idx].length; j++) {
                        callList[idx][j] = (Call) list.get(j);
                    }
                }
            }
            list.clear();
            callMap.clear();
        } else {
            callList = null;
        }
    }
    /**
     * Gets call list.
     * @return call list
     **/
    public Call[][] getCallList() {
        return callList;
    }
    class Sort implements Comparator {
        Collator c = Collator.getInstance(Locale.CANADA_FRENCH);
        public Sort() {
        }

         public int compare(Object arg0, Object arg1) {
            return Integer.parseInt((String)arg0)- Integer.parseInt((String)arg1);
         }
    }
}

package com.videotron.tvi.illico.callerid.controller;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.callerid.App;
import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.Util;
import com.videotron.tvi.illico.callerid.communication.PreferenceProxy;
import com.videotron.tvi.illico.callerid.data.Call;
import com.videotron.tvi.illico.callerid.data.DataManager;
import com.videotron.tvi.illico.callerid.ui.CallLog;
import com.videotron.tvi.illico.callerid.ui.Popup;
import com.videotron.tvi.illico.callerid.ui.Teaser;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class processes call notification received from General Daemon.
 * @author Jinjoo Ok
 */
public final class CallerIDController extends LayeredWindow implements LayeredKeyHandler,
        ScreenSaverConfirmationListener {

    private static final long serialVersionUID = 2675452830268584756L;
    /** LayeredUI. */
    public LayeredUI root;
    /** LayeredUI for Popup. */
    public LayeredUI popupUI;
    /** LayeredWindow for Popup. */
    private LayeredWindowPopup popupWindow;
    /** CallerIDController instance. */
    private static CallerIDController instance = new CallerIDController();
    /** MAC address. This is a unique client identifier. */
    private String macAddress;
    /** Teaser UI. */
    private Teaser teaser;
    /** Call Log UI. */
    private CallLog callLog;
    /** Notification. */
    private NotificationImpl notificationOption;
    /** Call Data for notification. */
    private Call callNotification;
    /** point of Loading animation. */
    private static Point lodingPoint = new Point(480, 240);

    /** Constructor. */
    private CallerIDController() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        root = WindowProperty.CALLERID.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        popupWindow = new LayeredWindowPopup();
        popupUI = WindowProperty.ITV_POPUP.createLayeredDialog(popupWindow, Constants.SCREEN_BOUNDS, this);
    }

    /**
     * Gets CallerIDController instance.
     * @return CallerIDController
     */
    public static CallerIDController getInstance() {
        return instance;
    }

    /**
     * Initialize.
     */
    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CallerIDController init()");
        }
        teaser = new Teaser();
        callLog = new CallLog();
        // damonListener = new DaemonListener();
        notificationOption = new NotificationImpl();
    }

    /**
     * Start.
     */
    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CallerIDController start()");
        }
        
        PreferenceProxy.getInstance().addPreferenceListener();
        
        addScreenSaverListener();
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        String[] param = null;
        if (monitor != null) {
            try {
                param = monitor.getParameter(App.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        boolean back = (param != null && param.length > 0 && param[0]
                .equals(MonitorService.REQUEST_APPLICATION_LAST_KEY));
        if (Log.DEBUG_ON) {
            Log.printDebug("back : " + back);
        }
        if (back || isSubscribeService()) {
            showCallLog(!back);
        } else {
            showTeaser();
        }
    }

    /**
     * Stops UI.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("CallerIDController.stop");
        }
        if (callLog.isVisible()) {
            callLog.stop();
            this.remove(callLog);
        } else if (teaser.isVisible()) {
            teaser.stop();
            this.remove(teaser);
        }
        hideLoadingAnimation();
        root.deactivate();
        removeScreenSaverListener();
    }

    /**
     * Remove ScreenSaverConfirmListener.
     */
    private void removeScreenSaverListener() {
        MonitorService ms = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
        if (ms != null) {
            try {
                ms.removeScreenSaverConfirmListener(this, App.APP_NAME);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    /**
     * Adds ScreenSaverListener.
     */
    private void addScreenSaverListener() {
        MonitorService ms = ((MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME));
        try {
            if (ms != null) {
                ms.addScreenSaverConfirmListener(this, App.APP_NAME);
            } else {
                if (Log.WARNING_ON) {
                    Log.printWarning("CallerID can't get MonitorService.");
                }
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    // /**
    // * Registers this class to Daemon.
    // */
    // public void addDaemonListener() {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("addDaemonListener()");
    // }
    // DaemonService daemon = (DaemonService) DataCenter.getInstance().get(DaemonService.IXC_NAME);
    // try {
    // if (daemon != null) {
    // daemon.addListener(App.APP_NAME, damonListener);
    // }
    // } catch (Exception e) {
    // Log.print(e);
    // }
    // }
    /**
     * Adds listener to call notification.
     */
    public void addNotificationListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("addNotificationListener()");
        }
        NotificationService noti = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
        try {
            if (noti != null) {
                noti.setRegisterNotificaiton(App.APP_NAME, null);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Response Monitor's confirm.
     * @return if it returns true, ScreenSaver will be started.
     * @throws RemoteException remote exception
     * @see com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener#confirmScreenSaver()
     */
    public boolean confirmScreenSaver() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("confirmScreenSaver=====");
        }
        if (root.isActive()) {
            return true;
        }
        return false;
    }

    /**
     * Key event.
     * @param event UserEvent
     * @return if true return then key event is not passed other application.
     */
    public boolean handleKeyEvent(UserEvent event) {

        // handleKey
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        // if (Environment.EMULATOR) {
        // if (event.getCode() == KeyCodes.SEARCH) {
        // callNotification = new Call();
        // callNotification.setCalledPhoneNumber("(514) 902-2200");
        // callNotification.setCallingPhoneNumber("private number");
        // callNotification.setName("Private name");
        // NotificationService noti = (NotificationService) DataCenter.getInstance().get(
        // NotificationService.IXC_NAME);
        // if (noti != null) {
        // try {
        // noti.setPriority(0);
        // noti.setNotifyAndTime(notificationOption, 10 * Constants.MS_PER_SECOND);
        // } catch (Exception e) {
        // Log.print(e);
        // }
        // }
        // return true;
        // }
        // }
        boolean isUsed = false;
        if (callLog.isVisible()) {
            isUsed = callLog.handleKey(event.getCode());
        } else {
            isUsed = teaser.handleKey(event.getCode());
        }
        return isUsed;
    }

    /**
     * Exits to channel.
     */
    public void exit() {
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (monitor != null) {
            try {
                monitor.exitToChannel();
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Parses message received from Daemon.
     * @param message message received from Daemon.
     */
    public void parseMessage(String message) {
        if (Log.DEBUG_ON) {
            Log.printDebug("parseMessage : " + message);
        }
        if (message.indexOf("Ack") != -1) {
            ServerConnectionController.getInstance().parseAck(message);
        } else {

            if (!isSubscribeService()) {
                return;
            }
            StringBuffer sb = new StringBuffer(message);
            int index = sb.indexOf(">");
            if (index != -1) {
                sb.delete(0, index + 1);
                index = sb.indexOf("<");
                sb.delete(index, sb.length());
            }
            String[] token = TextUtil.tokenize(sb.toString(), "|");
            if (Log.DEBUG_ON) {
                Log.printDebug("parseMessage token length : " + token.length);
                for (int i = 0; i < token.length; i++) {
                    Log.printDebug("parseMessage parsed value : " + token[i]);
                }
            }

            if (token != null) {
                for (int i = 0; i < token.length; i++) {
                    if (token[i] != null) {
                        token[i] = token[i].trim();
                    }
                }
                if (token[0].equalsIgnoreCase(macAddress)) {
                    ServerConnectionController.getInstance().checkRegister();
                    try {
                        sb.setLength(0);
                        sb.append(token[2]);
                        callNotification = null;
                        callNotification = new Call();
                        callNotification.setCallingPhoneNumber(token[2]);
                        boolean check = false;
                        callNotification.setCalledPhoneNumber(token[4]);
                        if (token[3].equals("^48")) {
                            callNotification.setName(DataCenter.getInstance().getString("Label.lblUnknown"));
                        } else if (token[3].equals("^50") || token[3].equals("^49")) {
                            callNotification.setName(DataCenter.getInstance().getString("Label.lblPrivate"));
                            callNotification.setCallingPhoneNumber(DataCenter.getInstance().getString(
                                    "Label.lblPrivateNum"));
                        } else {
                            callNotification.setName(token[3]);
                        }
                        String selection = DataCenter.getInstance().getString(
                                PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION);
                        String[] lines = TextUtil.tokenize(selection, "|");
                        for (int i = 0; i < lines.length; i++) {
                            Log.printDebug("selection line num in upp : " + lines[i]);
                            if (token[4].equals(lines[i])) {
                                check = true;
                                break;
                            }
                        }
                        boolean save = PreferenceProxy.getInstance().savePhoneLine(
                                PreferenceNames.CALLER_ID_PHONE_LINE_LIST, token[4]);
                        if (save) {
                            check = true;
                        }
                        if (DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_DISPLAY)
                                .equals(Definitions.OPTION_VALUE_DISABLE)) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("CALLER_ID_DISPLAY: "
                                        + DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_DISPLAY));
                            }
                            if (!save) {
                                return;
                            }
                        }
                        if (!check) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("phone number is not included in selection list of UPP so return "
                                        + token[4]);
                            }
                            return;
                        }
                    } catch (Exception e) {
                        CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_506);
                        Log.print(e);
                    }
                    NotificationService noti = (NotificationService) DataCenter.getInstance().get(
                            NotificationService.IXC_NAME);
                    if (noti != null && callNotification != null) {
                        try {
                            String duration = DataCenter.getInstance().getString(
                                    PreferenceNames.CALLER_ID_NOTIFICATION_DURATION);
                            if (Log.DEBUG_ON) {
                                Log.printDebug("pass call to Notification duration " + duration);
                            }
                            noti.setPriority(0);
                            noti.setNotifyAndTime(notificationOption, Long.parseLong(duration.substring(0, 2))
                                    * Constants.MS_PER_SECOND);
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("mac adddress is not same with client identifier " + token[0]);
                    }
                }
            } else {
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_506);
            }
        }
    }

    /**
     * Shows loading animation.
     */
    public void showLoadingAnimation() {
        if (Log.DEBUG_ON) {
            Log.printDebug("showLoadingAnimation");
        }
        try {
            LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(
                    LoadingAnimationService.IXC_NAME);
            if (loading != null) {
                loading.showLoadingAnimation(lodingPoint);
            } else {
                showErrorPopup(Rs.ERROR_CODE_504);
            }
        } catch (Exception e) {
            showErrorPopup(Rs.ERROR_CODE_504);
            Log.print(e);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("after call showLoadingAnimation");
        }
    }

    /**
     * Hides loading animation.
     */
    public void hideLoadingAnimation() {
        try {
            LoadingAnimationService loading = (LoadingAnimationService) DataCenter.getInstance().get(
                    LoadingAnimationService.IXC_NAME);
            if (loading != null) {
                loading.hideLoadingAnimation();
            } else {
                showErrorPopup(Rs.ERROR_CODE_504);
            }
        } catch (Exception e) {
            showErrorPopup(Rs.ERROR_CODE_504);
            Log.print(e);
        }
    }

    /**
     * Checks authorization of Caller(PK_AFFICHUR).
     * @return if it has a authority, returns true
     */
    public boolean isSubscribeService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("isSubscribeService ");
        }
        if (Environment.EMULATOR) {
            return true;
        }
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);

        try {
            if (monitor != null) {
                int i = monitor.checkAppAuthorization(App.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("isSubscribeService i " + i);
                }

                if (i != MonitorService.NOT_AUTHORIZED) {
                    return true;
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Can't get MonitorService");
                }
            }
        } catch (Exception e) {
            if (Log.ERROR_ON) {
                Log.print(e);
            }
        }
        return false;
    }

    /**
     * set MacAddress.
     */
    public void setMacAddress() {
        while (macAddress == null || macAddress.length() == 0) {
            if (Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO
                    || Environment.HW_VENDOR == Environment.HW_VENDOR_EMULATOR) {
                macAddress = Host.getInstance().getReverseChannelMAC();
                String[] token = TextUtil.tokenize(macAddress, ":");
                StringBuffer address = new StringBuffer();
                for (int i = 0; i < token.length; i++) {
                    address.append(token[i]);
                }
                macAddress = address.toString();
            } else {
                try {
                    MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
                    if (monitor != null) {
                        byte[] addr = monitor.getCableCardMacAddress();
                        macAddress = Util.getStringBYTEArray(addr);
                    }
                } catch (Exception e) {
                    if (Log.WARNING_ON) {
                        Log.print(e);
                    }
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("CallIDController, macAddress :: " + macAddress);
            }

            if (macAddress == null || macAddress.length() == 0) {
                try {
                    Thread.sleep(Constants.MS_PER_SECOND * 5);
                } catch (Exception e) {
                    Log.print(e);
                }
            } else {
                macAddress = macAddress.toUpperCase();
                DataCenter.getInstance().put("MAC", macAddress);
                break;
            }
        }
    }

    /**
     * Registers to server.
     */
    public void registerToServer() {
        if (isSubscribeService()) {
            ServerConnectionController.getInstance().initRetryStageIndex();
            ServerConnectionController.getInstance().registerToServer();
        } else {
            if (ServerConnectionController.getInstance().isRegistered()) {
                ServerConnectionController.getInstance().stop();
            }
        }
    }

    /**
     * shows error Pop-up through ErrorMessageService.
     * @param code error code.
     */
    public void showErrorPopup(String code) {
        ErrorMessageService errorService = (ErrorMessageService) DataCenter.getInstance().get(
                ErrorMessageService.IXC_NAME);
        if (errorService != null) {
            try {
                errorService.showErrorMessage(code, null);
            } catch (Exception e) {
                Log.print(e);
            }
        } else {
            if (Log.ERROR_ON) {
                Log.printError("Can't get ErrorMessageService");
            }
        }
    }

    /**
     * Gets error message from the Error Message.
     * @param code error code
     * @return error message
     */
    public String getErrorMsg(String code) {
        if (Log.DEBUG_ON) {
            Log.printDebug("getErrorMsg " + code);
        }
        ErrorMessageService errorService = (ErrorMessageService) DataCenter.getInstance().get(
                ErrorMessageService.IXC_NAME);
        String text = null;
        if (errorService != null) {
            try {
                ErrorMessage msg = errorService.getErrorMessage(code);
                if (msg != null) {
                    if (DataCenter.getInstance().getString(PreferenceNames.LANGUAGE)
                            .equals(Definitions.LANGUAGE_ENGLISH)) {
                        text = msg.getEnglishContent();
                    } else {
                        text = msg.getFrenchContent();
                    }
                    if (text == null || text.length() == 0) {
                        text = "Unknown error";
                        if (Log.WARNING_ON) {
                            Log.printWarning("Error message is not defined in Error Message Service, code :" + code);
                        }
                    } else if (Log.INFO_ON) {
                        Log.printInfo("getErrorMessage() code : " + code + ", msg : " + msg);
                    }
                } else if (Log.ERROR_ON) {
                    Log.printError("Search cannot get ErrorMessage form ErrorMessageService");
                }

            } catch (Exception e) {
                Log.print(e);
            }
        }
        return text;
    }

    /**
     * Shows teaser.
     */
    public void showTeaser() {
        if (Log.DEBUG_ON) {
            Log.printDebug("showTeaser");
        }
        this.add(teaser);
        teaser.start();
    }

    /**
     * Shows call log.
     * @param reset whether if it reset or not.
     */
    private void showCallLog(boolean reset) {
        this.add(callLog);
        if (!reset) {
            callLog.start(false);
        } else {
            callLog.start(true);
            // if (ServerConnectionController.getInstance().isRegistered()) {
            // showLoadingAnimation();
            // int result = ServerConnectionController.getInstance().retrieveCallLog();
            // hideLoadingAnimation();
            // if (result == -1) {
            // showErrorPopup(Rs.ERROR_CODE_002);
            // }
            // }
            // callLog.showCallLog();
        }
    }

    /**
     * Deletes all log.
     */
    public void deleteAllLog() {
        showLoadingAnimation();
        if (ServerConnectionController.getInstance().deleteCallLog() == 0) {
            callLog.init();
            DataManager.getInstance().init();
            int result = ServerConnectionController.getInstance().retrieveCallLog();
            hideLoadingAnimation();
            if (result == -1) {
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_003);
            }
            callLog.showCallLog();
        } else {
            hideLoadingAnimation();
            showErrorPopup(Rs.ERROR_CODE_004);
        }
    }

    /**
     * Requests starting unbound application to Monitor.
     * @param appName application name.
     * @param param parameters
     */
    public void startUnboundApplication(String appName, String[] param) {
        MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (monitor != null) {
            try {
                monitor.startUnboundApplication(appName, param);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Opens Popup.
     * @param pop Popup
     */
    public void openPopup(Popup pop) {
        popupWindow.add(pop, 0);
        popupUI.activate();
    }

    /**
     * Closes Popup.
     * @param pop Popup
     */
    public void closePopup(Popup pop) {
        popupWindow.remove(pop);
        popupUI.deactivate();
    }

    // /**
    // * Listener for receiving call notification from Daemon.
    // * @author okwing
    // */
    // class DaemonListener implements RemoteRequestListener {
    // /**
    // * Receive call notification from Daemon.
    // * @param path
    // * @param body data
    // * @exception RemoteException any RemoteException
    // * @return response
    // */
    // public byte[] request(String path, byte[] body) throws RemoteException {
    //
    // if (body == null || body.length == 0) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("request body length 0, so return");
    // }
    // return null;
    // }
    // parseMessage(new String(body));
    // return null;
    // }
    // }

    class NotificationImpl implements NotificationOption {

        public String getApplicationName() throws RemoteException {
            return App.APP_NAME;
        }

        public String[][] getButtonNameAction() throws RemoteException {
            return null;
        }

        public int getCategories() throws RemoteException {
            return 0;
        }

        public long getCreateDate() throws RemoteException {
            return 0;
        }

        public int getDisplayCount() throws RemoteException {
            return 0;
        }

        /**
         * Gets time of start display.
         */
        public long getDisplayTime() throws RemoteException {
            return System.currentTimeMillis();
        }

        public String getLargePopupMessage() throws RemoteException {
            return null;
        }

        public String getLargePopupSubMessage() throws RemoteException {
            return null;
        }

        public String getMessage() throws RemoteException {
            return callNotification.getCallingPhoneNumber();
        }

        public String getMessageID() throws RemoteException {
            return null;
        }

        public int getNotificationAction() throws RemoteException {
            return ACTION_NONE;
        }

        public String[] getNotificationActionData() throws RemoteException {
            return null;
        }

        public int getNotificationPopupType() throws RemoteException {
            return CALLER_ID_POPUP;
        }

        public long getRemainderDelay() throws RemoteException {
            return 0;
        }

        /**
         * Gets count of how much notification will be displayed.
         * @return 1 only once displayed
         */
        public int getRemainderTime() throws RemoteException {
            return 1;
        }

        public String getSubMessage() throws RemoteException {
            return callNotification.getName();
        }

        public boolean isCountDown() throws RemoteException {
            return false;
        }

        public boolean isViewed() throws RemoteException {
            return false;
        }

    }

    class LayeredWindowPopup extends LayeredWindow {

        private static final long serialVersionUID = -198437599068226594L;

        public LayeredWindowPopup() {
            setBounds(Constants.SCREEN_BOUNDS);
        }

        public void notifyShadowed() {
        }
    }

}

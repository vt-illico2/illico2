package com.videotron.tvi.illico.callerid.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.Util;
import com.videotron.tvi.illico.callerid.communication.PreferenceProxy;
import com.videotron.tvi.illico.callerid.data.Call;
import com.videotron.tvi.illico.callerid.data.DataManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * ServerConnectionController implements connection with back-end. Registers STB to back-end and retrieves call log from
 * back-end.
 * @author Jinjoo Ok
 */
public class ServerConnectionController implements TVTimerWentOffListener {
    /** ServerConnectionController instance. */
    public static ServerConnectionController instance = new ServerConnectionController();
    /** Unique client identifier(lower case). It's STB MacAddress. */
    private String clientIdentifier;
    /** Indicates the registration was completed. */
    private boolean registered;
    /** Indicates currently try to connect to main server. */
    private boolean primaryServerConnect;
    /** Number of retry. */
    private int retryCount;
    /** Server URL. */
    private static String urlPath;
    /** Offset start of entry id at EBIF data. */
    private static final int START_ENTRY_ID = 81;
    /** Offset start of phone id at EBIF data. */
    private static final int START_PHONE_ID = 83;
    /** Offset start of calling name at EBIF data. */
    private static final int START_CALLING_NAME = 85;
    /** Offset start of phone number at EBIF data. */
    private static final int START_PHONE_NUM = 87;
    /** Offset start of time at EBIF data. */
    private static final int START_TIME = 89;
    /** Offset start of seen/unseen at EBIF data. */
    private static final int START_SEEN = 91;
    /** The number of the hour of the day. */
    private static final int DAY_HOURS = 24;
    /** The time to register to server again after registration success. It's made to avoid concentrated requests. */
    private int oneDayRegisterTime;
    /** Number of retry when registration. */
    private int registerAgain;
    /** The array of retry stage. */
    private int[] retryStage = new int[4];
    /** Indicates current index of retry stage. */
    private int retryStageIndex;
    /** Index of proxy server. */
    private int currentProxyIndex;
    /** UDP socket. */
    private DatagramSocket udpSocket;
    /** UDP server waiting for call notification. */
    private UdpServer udpServer;
    /** Indicates the registration is under way. */
    private boolean requstingLogin;
    /** TimerSpec. */
    private TVTimerSpec timerSpec = new TVTimerSpec();
    /** TimerSpec. */
    private TVTimerSpec timerSpec5Min = new TVTimerSpec();
    /** TimerSpec. */
    private TVTimerSpec timerSpec1Hour = new TVTimerSpec();

    /** TimerSpec. */
    private TVTimerSpec timerSpec1Day = new TVTimerSpec();
    /** The day of month that registered to server. */
    private int loginDay;
    /** Texts for English days. */
    public static final String[] MONTHS = {"Jan.", "Feb.", "Mar.", "Apr.", "May", "Jun.", "Jul.", "Aug.", "Sept.",
            "Oct.", "Nov.", "Dec."};
    /** Texts for English days. */
    public static final String[] MONTHS_FRENCH = {"janv.", "févr.", "mars", "avr.", "mai", "juin", "juil.", "août",
            "sept.", "oct.", "nov.", "déc."};
    private boolean initial;

    /**
     * Gets ServerConnectionController instance.
     * @return ServerConnectionController
     */
    public static ServerConnectionController getInstance() {
        return instance;
    }

    /**
     * Start.
     */
    public void start() {
        initial = true;
        clientIdentifier = DataCenter.getInstance().getString("MAC").toLowerCase();
        primaryServerConnect = true;
        StringBuffer sb = new StringBuffer("http://");
        sb.append(DataCenter.getInstance().getString(Rs.DCKEY_SERVER_IP));
        sb.append(":");
        sb.append(DataCenter.getInstance().getInt(Rs.DCKEY_SERVER_TCP_PORT));
        String context = DataCenter.getInstance().getString(Rs.DCKEY_SERVER_CONTEXT);
        if (!context.startsWith("/")) {
            sb.append("/");
        }
        sb.append(context);
        if (!context.endsWith("/")) {
            sb.append("/");
        }
        urlPath = sb.toString();
        if (Log.INFO_ON) {
            Log.printInfo("ServerConnectionController start() clientIdentifier :" + clientIdentifier);
            Log.printInfo("ServerConnectionController start(): urlPath :" + urlPath);
            Log.printInfo("ServerConnectionControllerstart() : the time to register again is " + oneDayRegisterTime);
        }
        oneDayRegisterTime = new Random().nextInt(DAY_HOURS);
        retryStage[0] = DataCenter.getInstance().getInt(Rs.DCKEY_1ST_REQUEST_TOTAL_TIME)
                / DataCenter.getInstance().getInt(Rs.DCKEY_1ST_REQUEST_REPEAT_TIME) - 1;
        retryStage[1] = DataCenter.getInstance().getInt(Rs.DCKEY_2ND_REQUEST_TOTAL_TIME)
                / DataCenter.getInstance().getInt(Rs.DCKEY_2ND_REQUEST_REPEAT_TIME) - 1;
        retryStage[2] = DataCenter.getInstance().getInt(Rs.DCKEY_3RD_REQUEST_TOTAL_TIME)
                / DataCenter.getInstance().getInt(Rs.DCKEY_3RD_REQUEST_REPEAT_TIME) - 1;
        retryStage[3] = 0;
        registered = false;
        registerToServer();

    }

    /**
     * Start timer.
     * @param delay delay time
     * @param timer TVTimverSpec
     */
    private void startTimer(long delay, TVTimerSpec timer) {
        TVTimerSpec tvTimer = timer;
        synchronized (tvTimer) {
            stopTimer(tvTimer);
            if (Log.DEBUG_ON) {
                Log.printDebug("startTimer delay " + delay);
            }
            try {
                tvTimer.setDelayTime(delay);
                tvTimer.setRepeat(true);
                tvTimer.addTVTimerWentOffListener(this);
                TVTimer.getTimer().scheduleTimerSpec(tvTimer);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }

    }

    /**
     * Stop timer.
     * @param timer TVTimverSpec
     **/
    private void stopTimer(TVTimerSpec timer) {
        if (Log.INFO_ON) {
            Log.printInfo("ServerConnectionController: stopTimer");
        }
        TVTimerSpec tvTimer = timer;
        synchronized (tvTimer) {
            tvTimer.removeTVTimerWentOffListener(this);
            TVTimer.getTimer().deschedule(tvTimer);
        }
    }

    // public void parseAck(String message) {
    // StringBuffer ack = new StringBuffer(message);
    // if (Log.DEBUG_ON) {
    // Log.printDebug("ACK msg: " + ack.toString());
    // }
    // if (message == null || message.length() == 0) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("Registration fail!, data is null");
    // }
    // startRetryStage();
    // } else {
    // int index = ack.indexOf(">");
    // ack.delete(0, index + 1);
    // index = ack.indexOf("<");
    // ack.delete(index, ack.length());
    // if (ack.toString().equals(clientIdentifier.toUpperCase())) {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("Registration success!");
    // }
    // registered = true;
    // registerAgain = 0;
    // retryStageIndex = 0;
    // //for registration refresh
    // startTimer(Constants.MS_PER_DAY);
    // PreferenceProxy.getInstance().setSyncToServer();
    // } else {
    // startRetryStage();
    // if (Log.DEBUG_ON) {
    // Log.printDebug("Registration fail!");
    // }
    // }
    // }
    // }
    /**
     * Stops.
     */
    public void stop() {
        registered = false;
        stopTimer(timerSpec);
        stopTimer(timerSpec5Min);
        stopTimer(timerSpec1Hour);
        stopTimer(timerSpec1Day);
        PreferenceProxy.getInstance().initPhoneLine();
    }

    public boolean isRegistered() {
        if (Environment.EMULATOR) {
            return true;
        }
        return registered;
    }

    public void parseAck(String message) {
        if (Log.DEBUG_ON) {
            Log.printDebug("parseAck");
        }
        if (message == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("parseAck Registration fail!, data is null");
            }
            startRetryStage();
            return;
        }
        StringBuffer ack = new StringBuffer(message.trim());
        if (Log.DEBUG_ON) {
            Log.printDebug("parseAck() ACK msg: " + ack.toString());
        }
        int index = ack.indexOf(">");
        ack.delete(0, index + 1);
        index = ack.indexOf("<");
        ack.delete(index, ack.length());
        if (ack.toString().equalsIgnoreCase(clientIdentifier)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("Registration success!");
            }
            registered = true;
            registerAgain = 0;
            initRetryStageIndex();
            // for registration refresh
            startTimer(Constants.MS_PER_HOUR, timerSpec);
            PreferenceProxy.getInstance().setSyncToServer();
        } else {
            startRetryStage();
            if (Log.DEBUG_ON) {
                Log.printDebug("Registration fail!");
            }
        }
        requstingLogin = false;
    }

    /**
     * Rester to server.
     */
    public void registerToServer() {
        if (Log.DEBUG_ON) {
            Log.printDebug("registerToServer primaryServerConnect " + primaryServerConnect);
        }
        if (!CallerIDController.getInstance().isSubscribeService()) {
            if (Log.DEBUG_ON) {
                Log.printDebug("registerToServer(), not a subscriber, so return! ");
            }
            return;
        }
        if (requstingLogin) {
            if (Log.DEBUG_ON) {
                Log.printDebug("registerToServer progress in login, so return ");
            }
            return;
        }
        if (registered) {
            if (Log.DEBUG_ON) {
                Log.printDebug("registration aleady done, so return.");
            }
            return;
        }
        if (clientIdentifier == null || clientIdentifier.length() == 0) {
            if (Log.DEBUG_ON) {
                Log.printDebug("MAC address not ready, so return.");
            }
            return;
        }
        requstingLogin = true;
        DatagramPacket packet = null;
        DatagramPacket in = null;
        InetAddress serverInet = null;
        StringBuffer reg = new StringBuffer("<Reg>");
        reg.append(clientIdentifier.toUpperCase());
        reg.append("</>");

        String regHex = Util.stringToHexString(reg.toString());
        String ip = null;
        int port;
        try {
            if (primaryServerConnect) {
                ip = DataCenter.getInstance().getString(Rs.DCKEY_SERVER_IP);
                port = DataCenter.getInstance().getInt(Rs.DCKEY_SERVER_UDP_PORT);
            } else {
                ip = DataCenter.getInstance().getString(Rs.DCKEY_BACKUP_SERVER_IP);
                port = DataCenter.getInstance().getInt(Rs.DCKEY_BACKUP_SERVER_UDP_PORT);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("ip: " + ip + ",port: " + port + ", msg : " + reg.toString());
            }
            if (Environment.EMULATOR) {
                ip = "10.10.1.159";// test
                port = 9275;
            }
            if (ip == null) {
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_501);
            } else if (ip.equals("")) {
                startRetryStage();
                if (Log.DEBUG_ON) {
                    Log.printDebug("there is no IB data yet.");
                }
                requstingLogin = false;
                return;
            }

            // try {
            serverInet = InetAddress.getByName(ip);
            if (udpSocket != null) {
                if (!udpSocket.isClosed()) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("socket close first ");
                    }
                    udpServer.setRunning(false);
                    udpServer = null;
                    udpSocket.close();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("socket is closed ? " + udpSocket.isClosed());
                    }
                    udpSocket = null;
                }
            }
            if (udpSocket == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("create Datagram socket");
                }
                udpSocket = new DatagramSocket(port);
            }
            udpSocket.setSoTimeout(DataCenter.getInstance().getInt(Rs.DCKEY_MAX_RESPONSE_WAITING_TIME) * 1000);
            if (Log.DEBUG_ON) {
                Log.printDebug("timeout: " + udpSocket.getSoTimeout());
            }
            byte[] out = Util.hexStringToByteArray(regHex);
            packet = new DatagramPacket(out, out.length, serverInet, port);
            Log.printDebug("Datagram socket " + udpSocket);
            udpSocket.send(packet);

            if (Log.DEBUG_ON) {
                Log.printDebug("send data");
            }
            byte[] buffer = new byte[1024];
            in = new DatagramPacket(buffer, 1024);
            udpSocket.receive(in);

            byte[] data = in.getData();
            if (data != null) {
                StringBuffer ack = new StringBuffer(new String(data).trim());
                if (Log.DEBUG_ON) {
                    Log.printDebug("ACK msg: " + ack.toString());
                }
                if (ack.indexOf("<NewCall>") != -1) {
                    CallerIDController.getInstance().parseMessage(ack.toString());
                } else {
                    int index = ack.indexOf(">");
                    ack.delete(0, index + 1);
                    index = ack.indexOf("<");
                    ack.delete(index, ack.length());
                    if (ack.toString().equalsIgnoreCase(clientIdentifier)) {
                        if (Log.INFO_ON) {
                            Log.printInfo("Registration success! time is " + getDateTime());
                        }
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(new Date());
                        loginDay = cal.get(Calendar.DAY_OF_MONTH);
                        if (Log.INFO_ON) {
                            Log.printInfo("Registration loginDay " + loginDay);
                        }
                        registered = true;
                        registerAgain = 0;
                        initRetryStageIndex();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("create UdpServer!");
                        }
                        if (!Environment.EMULATOR) {
                            startTimer(Constants.MS_PER_HOUR, timerSpec);
                        } else {
                            startTimer(Constants.MS_PER_MINUTE, timerSpec);
                        }
                        PreferenceProxy.getInstance().setSyncToServer();
                    } else {
                        startRetryStage();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("Registration fail!");
                        }
                    }
                }
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Registration fail!, data is null");
                }
                startRetryStage();
            }
        } catch (Exception e) {
            registered = false;
            startRetryStage();
            if (Log.DEBUG_ON) {
                Log.printDebug("catch exception ; " + e.getMessage());
            }
        } finally {
            requstingLogin = false;
            udpServer = new UdpServer();
            udpServer.setRunning(true);
            try {
                udpSocket.setSoTimeout(0);
            } catch (Exception e) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("exception setSoTimeout(0)");
                }
            }
            udpServer.start();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("Registration end! requstingLogin: " + requstingLogin + " registered: " + registered);
        }
    }

    /**
     * If no acknowledgement is received for the initial registration, the retry mechanism is as follows. 1. for the
     * first 3 minutes after the initial registration, retry once every 60 seconds alternating between primary and
     * secondary TransmissionServer IP addresses. 2. for the first 30 minutes after the initial registration, retry once
     * every 5 minutes. 3. for the fist 24 hours after the initial registration. Retry once per hour. 4. Once every 24
     * hours, alternating between primary and secondary TransmissionServer IP addresses until an acknowledgement is
     * received.
     */
    private void startRetryStage() {
        registered = false;
        if (Log.DEBUG_ON) {
            Log.printDebug("startRetryStage " + retryStageIndex);
        }
        if (retryStageIndex == 0) {
            if (registerAgain == 0) {
                primaryServerConnect = false;
                startTimer(Constants.MS_PER_MINUTE * DataCenter.getInstance().getInt(Rs.DCKEY_1ST_REQUEST_REPEAT_TIME),
                        timerSpec);
                startTimer(Constants.MS_PER_MINUTE * DataCenter.getInstance().getInt(Rs.DCKEY_2ND_REQUEST_REPEAT_TIME),
                        timerSpec5Min);
                startTimer(Constants.MS_PER_HOUR * DataCenter.getInstance().getInt(Rs.DCKEY_3RD_REQUEST_REPEAT_TIME),
                        timerSpec1Hour);
                startTimer(Constants.MS_PER_DAY, timerSpec1Day);
            }
        } else if (retryStageIndex == 1) {
            stopTimer(timerSpec);
        } else if (retryStageIndex == 2) {
            stopTimer(timerSpec5Min);
        } else {
            stopTimer(timerSpec1Hour);
        }
    }

    public void initRetryStageIndex() {
        retryStageIndex = 0;
        stopTimer(timerSpec5Min);
        stopTimer(timerSpec1Hour);
        stopTimer(timerSpec1Day);
    }

    /**
     * Gets HTTP connection.
     * @param param parameter
     * @return HttpURLConnection
     */
    private HttpURLConnection connect(String param) {
        URL url;
        HttpURLConnection con = null;
        StringBuffer query = new StringBuffer(urlPath);
        query.append(param);
        if (Log.DEBUG_ON) {
            Log.printDebug("connect url : " + query.toString());
        }
        String[] proxy = (String[]) DataCenter.getInstance().get(Rs.DCKEY_PROXY_HOST);
        int[] port = (int[]) DataCenter.getInstance().get(Rs.DCKEY_PROXY_PORT);
        try {
            if (DataCenter.getInstance().getString(Rs.DCKEY_PROXY_USE).equals("true")) {
                url = new URL("HTTP", proxy[currentProxyIndex], port[currentProxyIndex], query.toString());
            } else {
                url = new URL(query.toString());
            }
            con = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            if (proxy != null) {
                currentProxyIndex = (currentProxyIndex + 1) % proxy.length;
            }
            connect(param);
        }
        return con;
    }

    /**
     * Gets Notification Control Status.
     * @return status
     */
    public int getControlStatus() {
        int status = -1;
        if (!registered) {
            return status;
        }
        InputStream in = null;
        ByteArrayOutputStream baos = null;
        HttpURLConnection con = null;
        OutputStream out = null;
        try {
            con = connect("Getters");
            StringBuffer query = new StringBuffer();
            query.append("type=stb-settings&client-id=");
            query.append(clientIdentifier);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", query.toString().getBytes().length + "");
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            out = con.getOutputStream();
            out.write(query.toString().getBytes());
            out.flush();
            if (Log.DEBUG_ON) {
                Log.printDebug("url : " + query.toString());
            }
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("ServerConnectionController, getControlStatus: response code : " + code);
            }
            if (code == HttpURLConnection.HTTP_OK) {

                in = con.getInputStream();
                byte[] data = null;
                baos = new ByteArrayOutputStream();
                int length = 0;
                while ((length = in.read()) != -1) {
                    baos.write(length);
                }
                data = baos.toByteArray();
                status = Util.oneByteToInt(data, 50);
                retryCount = 0;
                if (Log.DEBUG_ON) {
                    Log.printDebug("getControlStatus original msg " + new String(data));
                    Log.printDebug("ServerConnectionController, getControlStatus: response status : " + status);
                }
            }
        } catch (Exception e) {
            Log.print(e);
            try {
                Thread.sleep(DataCenter.getInstance().getInt(Rs.DCKEY_RETRY_WAITING_TIME));
            } catch (Exception ee) {
                Log.print(ee);
            }
            retryCount++;
            if (retryCount < DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
                status = getControlStatus();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("call error Popup in getControllStatus");
                }
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_500);
            }
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        if (status != -1 || retryCount == DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
            retryCount = 0;
        }
        return status;
    }

    /**
     * Sets notification control status at server side.
     * @param status , 1 means 'On'and 0 means 'Off'
     * @return true means update was successful
     */
    public boolean setControlStatus(int status) {
        if (Log.DEBUG_ON) {
            Log.printDebug("setControlStatus status " + status + " registered " + registered);
        }

        boolean success = false;
        OutputStream out = null;
        HttpURLConnection con = null;
        try {
            con = connect("Setters");
            StringBuffer query = new StringBuffer();
            con.setDoOutput(true);
            query.append("type=updsetting&client-id=");
            query.append(clientIdentifier);
            query.append("&state=");
            query.append(status);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", query.toString().getBytes().length + "");
            con.setRequestMethod("POST");

            out = con.getOutputStream();
            out.write(query.toString().getBytes());
            out.flush();
            if (Log.DEBUG_ON) {
                Log.printDebug("Setters)body msg : " + query.toString());
            }
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("Setters)getResponseCode : " + code);
            }
            if (code == HttpURLConnection.HTTP_OK) {
                success = true;
                retryCount = 0;
            }
        } catch (Exception e) {
            Log.print(e);
            try {
                Thread.sleep(DataCenter.getInstance().getInt(Rs.DCKEY_RETRY_WAITING_TIME));
            } catch (Exception ee) {
                Log.print(e);
            }
            retryCount++;
            if (retryCount < DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
                success = setControlStatus(status);
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("call error Popup in setControllStatus");
                }
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_500);
            }
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        if (status != -1 || retryCount == DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
            retryCount = 0;
        }
        return success;
    }

    /**
     * Retrieve call log from back-end.
     */
    public int retrieveCallLog() {
        if (Log.DEBUG_ON) {
            Log.printDebug("ServerConnectionController, retrieveCallLog() is called");
        }

        int result = -1;
        Call[] call = null;
        HttpURLConnection con = null;
        OutputStream out = null;
        InputStream in = null;
        ByteArrayOutputStream baos = null;
        try {
            con = connect("Getters");
            StringBuffer query = new StringBuffer();
            query.append("type=call-log&client-id=");
            query.append(clientIdentifier);
            query.append("&phone-id=-1");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", String.valueOf(query.toString().getBytes().length));
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            out = con.getOutputStream();
            out.write(query.toString().getBytes());
            out.flush();

            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("retrieveCallLog path " + urlPath + "Getters?" + query.toString());
                Log.printDebug("ServerConnectionController, retrieveCallLog: response code : " + code);
            }
            if (code == HttpURLConnection.HTTP_OK) {
                in = con.getInputStream();
                byte[] data = null;
                baos = new ByteArrayOutputStream();
                int length = 0;
                while ((length = in.read()) != -1) {
                    baos.write(length);
                }
                data = baos.toByteArray();
                length = data.length;
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog: ByteArrayOutputStream size : "
                            + length);
                }
                int entryCnt = Util.oneByteToInt(data, START_ENTRY_ID - 1);
                if (Log.DEBUG_ON) {
                    Log.printDebug("retrieveCallLog: total entryCnt : " + entryCnt);
                }
                int startPos = 14 + Util.twoBytesToInt(data, 18);
                int cidPos = startPos + Util.twoBytesToInt(data, 75);
                int cidLen = Util.twoBytesToInt(data, cidPos);
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog: cidLen : " + cidLen);
                    Log.printDebug("ServerConnectionController, retrieveCallLog: client-id : "
                            + new String(data, cidPos + 2, cidLen));
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog: startPos : " + startPos);
                }
                // entry id
                int entryPos = Util.twoBytesToInt(data, START_ENTRY_ID) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog: entryPos : " + entryPos);
                }
                entryPos += 6; // skip 6 byte.
                call = new Call[entryCnt];
                for (int i = 0; i < entryCnt; i++) {
                    call[i] = new Call();
                    call[i].setEntryId(String.valueOf(Util.fourBytesToInt(data, entryPos)));
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ServerConnectionController, retrieveCallLog:setEntryId " + call[i].getEntryId());
                    }
                    entryPos += 4;
                }
                // phone id
                entryPos = Util.twoBytesToInt(data, START_PHONE_ID) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog:phone id start " + entryPos);
                }
                entryPos += 6; // skip 6 byte.
                for (int i = 0; i < entryCnt; i++) {
                    call[i].setPhoneId(String.valueOf(Util.fourBytesToInt(data, entryPos)));
                    entryPos += 4;
                    if (Log.DEBUG_ON) {
                        Log.printDebug("ServerConnectionController, retrieveCallLog:phone id " + call[i].getPhoneId());
                    }
                }
                // calling name
                entryPos = Util.twoBytesToInt(data, START_CALLING_NAME) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("ServerConnectionController, retrieveCallLog: name start: " + entryPos);
                }
                int len = Util.twoBytesToInt(data, entryPos);
                entryPos += 2;
                String text = null;
                try {
                    text = new String(data, entryPos, len, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    text = new String(data, entryPos, len);
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("retrieveCallLog: names(original) : " + text);
                }

                String[] tokens = TextUtil.tokenize(text, "|");
                int[] indexOfPrivate = new int[entryCnt];
                for (int i = 0; i < entryCnt; i++) {
                    indexOfPrivate[i] = -1;
                    tokens[i] = tokens[i].trim();
                    if (tokens[i].equals("^48")) {
                        call[i].setName(DataCenter.getInstance().getString("Label.lblUnknown"));
                    } else if (tokens[i].equals("^49") || tokens[i].equals("^50")) {
                        call[i].setName(DataCenter.getInstance().getString("Label.lblPrivate"));
                        indexOfPrivate[i] = 0;
                    } else {
                        call[i].setName(tokens[i]);
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("retrieveCallLog: name : " + call[i].getName());
                    }
                }
                // phone number
                entryPos = Util.twoBytesToInt(data, START_PHONE_NUM) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("phone number) entryPos: " + entryPos);
                }
                len = Util.twoBytesToInt(data, entryPos);
                entryPos += 2;
                text = new String(data, entryPos, len);
                if (Log.DEBUG_ON) {
                    Log.printDebug("phone num)original string: " + text);
                }
                tokens = TextUtil.tokenize(text, "|");
                for (int i = 0; i < entryCnt; i++) {
                    tokens[i] = tokens[i].trim();
                    if (indexOfPrivate[i] == 0) {
                        call[i].setCallingPhoneNumber(DataCenter.getInstance().getString("Label.lblPrivateNum"));
                    } else {
                        call[i].setCallingPhoneNumber(tokens[i]);
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("phone num) " + call[i].getCallingPhoneNumber());
                    }
                }
                // time
                entryPos = Util.twoBytesToInt(data, START_TIME) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("time) entryPos: " + entryPos);
                }
                len = Util.twoBytesToInt(data, entryPos);
                entryPos += 2;
                text = new String(data, entryPos, len);
                if (Log.DEBUG_ON) {
                    Log.printDebug("time) original string: " + text);
                }

                tokens = TextUtil.tokenize(text, "|");
                StringBuffer sb = new StringBuffer();
                int index;
                String[] time;
                String lang = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);
                boolean french = false;
                if (lang.equals(Definitions.LANGUAGE_FRENCH)) {
                    french = true;
                }
                String lowerCase;
                for (int i = 0, idx = -1; i < entryCnt; i++) {
                    tokens[i] = tokens[i].trim();
                    sb.setLength(0);
                    sb.append(tokens[i]);
                    index = sb.indexOf(":");
                    if (index != -1) {
                        call[i].setTime(sb.substring(index - 2));
                        call[i].setDate(sb.substring(0, index - 3));
                    } else {
                        call[i].setDate(tokens[i]);
                        call[i].setTime("");
                    }
                    call[i].setDate(getDate(call[i].getDate(), french));
                    lowerCase = call[i].getTime().toLowerCase();
                    idx = lowerCase.indexOf("m");
                    if (idx != -1 && french) {
                        sb.setLength(0);
                        time = TextUtil.tokenize(lowerCase, ":");
                        int hour = Integer.parseInt(time[0]);
                        if (lowerCase.indexOf("p") != -1 && hour > 0 && hour < 12) {
                            sb.append(String.valueOf(hour + 12));
                        } else {
                            sb.append(time[0]);
                        }
                        sb.append(":");
                        sb.append(time[1].substring(0, 2));
                        call[i].setTime(sb.toString());
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("date/time) " + call[i].getDate() + " " + call[i].getTime());
                    }
                }
                // seen, unseen
                entryPos = Util.twoBytesToInt(data, START_SEEN) + startPos;
                if (Log.DEBUG_ON) {
                    Log.printDebug("seen/unssen) entryPos: " + entryPos);
                }
                len = Util.twoBytesToInt(data, entryPos);
                entryPos += 2;
                text = new String(data, entryPos, len);
                if (Log.DEBUG_ON) {
                    Log.printDebug("seen/unssen) text: " + text);
                }
                tokens = TextUtil.tokenize(text, "|");
                for (int i = 0; i < entryCnt; i++) {
                    if (tokens[i].trim().equals("1")) {
                        call[i].setSeen(true);
                    } else {
                        call[i].setSeen(false);
                    }
                }
                if (entryCnt > 0) {
                    DataManager.getInstance().setCallLog(call);
                }
                result = 0;
                retryCount = 0;
                baos.close();
            }
        } catch (Exception e) {
            Log.print(e);
            if (Environment.EMULATOR) {
                Call[] c = new Call[30];
                for (int i = 0; i < c.length; i++) {
                    c[i] = new Call();
                    c[i].setDate("2011 Aug. 08");
                    if (i > 5) {
                        c[i].setTime("12:24PM");
                    } else {
                        c[i].setTime("01:24PM");
                    }
                    c[i].setName("MMMMMMMMMMMMMMMMAAAB");
                    c[i].setCallingPhoneNumber("010-333-2222");
                    c[i].setPhoneId("1091");
                    // if (i > 10) {
                    // c[i].setPhoneId("1091");
                    // c[i].setCallingPhoneNumber("010-000-3333");
                    // }
                    if (i > 15) {
                        c[i].setPhoneId("100");
                        c[i].setCallingPhoneNumber("010-000-0000");
                    }
                    // if (i > 17) {
                    // c[i].setPhoneId("1098");
                    // c[i].setCallingPhoneNumber("010-000-xxx");
                    // }
                    // if (i > 20) {
                    // c[i].setPhoneId("1092");
                    // c[i].setCallingPhoneNumber("010-000-111");
                    // }
                }
                String lang = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);
                boolean french = false;
                if (lang.equals(Definitions.LANGUAGE_FRENCH)) {
                    french = true;
                }
                String[] time;
                StringBuffer sb = new StringBuffer();
                String lowerCase;
                for (int i = 0, idx = -1; i < c.length; i++) {
                    c[i].setDate(getDate(c[i].getDate(), french));
                    lowerCase = c[i].getTime().toLowerCase();
                    idx = lowerCase.indexOf("m");
                    if (idx != -1 && french) {
                        sb.setLength(0);
                        time = TextUtil.tokenize(lowerCase, ":");
                        int hour = Integer.parseInt(time[0]);
                        if (lowerCase.indexOf("p") != -1 && hour > 0 && hour < 12) {
                            sb.append(String.valueOf(hour + 12));
                        } else {
                            sb.append(time[0]);
                        }
                        sb.append(":");
                        sb.append(time[1].substring(0, 2));
                        c[i].setTime(sb.toString());
                    }
                }
                DataManager.getInstance().setCallLog(c);
                return 0;
            }
            DataManager.getInstance().setCallLog(null);
            try {
                Thread.sleep(DataCenter.getInstance().getInt(Rs.DCKEY_RETRY_WAITING_TIME));
            } catch (Exception ee) {
                Log.print(ee);
            }
            retryCount++;
            if (Log.DEBUG_ON) {
                Log.printDebug("Exception retryCount " + retryCount + " "
                        + DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES));
            }
            if (retryCount < DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
                retrieveCallLog();
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("call error Popup in retrieveCallLog");
                }
                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_500);
            }
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        if (retryCount == DataCenter.getInstance().getInt(Rs.DCKEY_RETRIES)) {
            retryCount = 0;
        }

        return result;
    }

    /**
     * Delete CallLog.
     * @return returns 0 if the request is success.
     */
    public int deleteCallLog() {
        if (Log.DEBUG_ON) {
            Log.printDebug("deleteCallLog() called");
        }
        // if (Environment.EMULATOR) {
        // return 0;
        // }
        HttpURLConnection con = null;
        OutputStream out = null;
        int result = -1;
        try {
            con = connect("Setters");
            StringBuffer query = new StringBuffer();
            con.setDoOutput(true);

            query.append("type=del-call-log&client-id=");
            query.append(clientIdentifier);
            query.append("&channel-number=1");
            query.append("&phone-id=-1&entry-id=-1");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            con.setRequestProperty("Content-Length", query.toString().getBytes().length + "");
            con.setRequestMethod("POST");
            out = con.getOutputStream();
            out.write(query.toString().getBytes());
            out.flush();
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
                Log.printDebug("url : " + query.toString());
                Log.printDebug("deleteCallLog() response code " + code);
            }
            if (code == HttpURLConnection.HTTP_NO_CONTENT) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("The call logs were successfully deleted.");
                }
                result = 0;
                // PreferenceProxy.getInstance().initPhoneLineTab();
            }
        } catch (Exception e) {
            Log.print(e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        return result;
    }

    /**
     * if no acknowledgement is received when the registration sent to server and receives a Caller ID notification with
     * the correct Unique Client Identifier, it consider the registration is successful.
     */
    public void checkRegister() {
        if (Log.DEBUG_ON) {
            Log.printDebug("checkRegister registered " + registered);
        }
        if (!registered) {
            if (Log.INFO_ON) {
                Log.printDebug("Ack msg is not arrived, but It's considered that registration is success!");
            }
            registered = true;
            registerAgain = 0;
            initRetryStageIndex();
            // for registration refresh
            if (!Environment.EMULATOR) {
                startTimer(Constants.MS_PER_HOUR, timerSpec);
            } else {
                startTimer(Constants.MS_PER_MINUTE, timerSpec);
            }
            new Thread() {
                public void run() {
                    PreferenceProxy.getInstance().setSyncToServer();
                }
            } .start();
        }
    }

    /**
     * Get date and time in form of yyyy/MM/dd/ HH:mm:ss.
     * @return yyyy/MM/dd/ HH:mm:ss
     */
    public static String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss");
        return sdf.format(new Date());
    }

    private static int getDateIndex(String mon) {
        for (int i = 0; i < MONTHS.length; i++) {
            if (MONTHS[i].toLowerCase().indexOf(mon.toLowerCase()) != -1) {
                return i;
            }
        }
        return Integer.parseInt(mon) - 1;
    }

    /**
     * Gets date depending on the language.
     * @param date date string
     * @param french whether if the language is French or not.
     * @return changed date
     */
    public static String getDate(String date, boolean french) {
        StringBuffer sb = new StringBuffer();
        String[] day = TextUtil.tokenize(date, " ");
        if (french) {
            sb.append(day[2]);
            sb.append(" ");
            sb.append(MONTHS_FRENCH[getDateIndex(day[1])]);
            sb.append(" ");
            sb.append(day[0]);
        } else {
            sb.append(day[0]);
            sb.append(" ");
            sb.append(MONTHS[getDateIndex(day[1])]);
            sb.append(" ");
            sb.append(day[2]);
        }
        return sb.toString();
    }

    /**
     * try again registration when it failed.
     * @param event TVTimerWentOffEvent
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug("timerWentOff registered " + registered + " registerAgain " + registerAgain);
            Log.printDebug("timerWentOff getDateTime() " + getDateTime());
            if (event.getTimerSpec() == timerSpec5Min) {
                Log.printDebug("timerSpec5Min  timerWentOff");
            } else if (event.getTimerSpec() == timerSpec) {
                Log.printDebug("timerSpec  timerWentOff");
            } else if (event.getTimerSpec() == timerSpec1Hour) {
                Log.printDebug("timerSpec1Hour  timerWentOff");
            }
        }
        if (!registered) {
            // stopTimer();
            if (registerAgain == retryStage[retryStageIndex]) {
                if (retryStageIndex < 3) {
                    retryStageIndex++;
                }
                registerAgain = 0;
            } else {
                if (retryStageIndex < 3) {
                    registerAgain++;
                }
            }
            if (event.getTimerSpec() == timerSpec1Day) {
                PreferenceProxy.getInstance().updatePhoneExpire(null, true);
            }
            primaryServerConnect = !primaryServerConnect;
            if (!requstingLogin) {
                new Thread() {
                    public void run() {
                        registerToServer();
                    }
                } .start();
            }
        } else {
            // per 1hour.
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            // Registration Refresh per a day
            if ((oneDayRegisterTime == hour && (!initial || day != loginDay)) || Environment.EMULATOR) {
                if (day != loginDay) {
                    initial = false;
                }
                PreferenceProxy.getInstance().updatePhoneExpire(null, true);
                if (!requstingLogin) {
                    new Thread() {
                        public void run() {
                            registered = false;
                            registerToServer();
                        }
                    } .start();
                }
            }

        }
    }

    /**
     * Waits for call notification.
     * @author Jinjoo Ok
     */
    class UdpServer extends Thread {
        boolean running;

        public void setRunning(boolean run) {
            running = run;
        }

        /**
         * run() of Thread class.
         */
        public void run() {

            byte[] buffer = new byte[1024];
            DatagramPacket in = null;
            InetAddress inet = null;
            if (Log.INFO_ON) {
                Log.printInfo("Caller UDP server run start : registered " + registered);
            }
            if (udpSocket == null) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("udpSocket is null");
                }
                return;
            }
            if (Log.INFO_ON) {
                try {
                    Log.printInfo("Caller UDP server soTimeout " + udpSocket.getSoTimeout());
                } catch (Exception e) {
                    Log.print(e);
                }
            }
            while (running && udpServer == UdpServer.this) {
                try {
                    if (!requstingLogin) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("Udp Server while run start : " + this.hashCode());
                        }
                        in = new DatagramPacket(buffer, 1024);
                        udpSocket.receive(in);
                        inet = in.getAddress();
                        String ip = inet.getHostAddress();
                        if (Log.INFO_ON) {
                            Log.printInfo("HostAddress : " + inet.getHostAddress() + " " + in.getPort());
                            Log.printInfo("ip : " + ip);
                        }

                        byte[] buf = in.getData();
                        byte[] actualBuf = new byte[in.getLength()];
                        if (Log.DEBUG_ON) {
                            Log.printDebug("in.getLength() " + in.getLength());
                        }
                        System.arraycopy(buf, 0, actualBuf, 0, actualBuf.length);
                        if (actualBuf != null && !requstingLogin) {
                            String msg = null;
                            try {
                                msg = new String(actualBuf, 0, actualBuf.length, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                msg = new String(actualBuf);
                            }
                            CallerIDController.getInstance().parseMessage(msg);
                        } else {
                            if (Log.DEBUG_ON) {
                                Log.printInfo("msg null");
                            }
                        }
                        try {
                            Thread.sleep(Constants.MS_PER_SECOND);
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }
                } catch (SocketTimeoutException ste) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Socket receive time out - Waiting..");
                    }
                } catch (SocketException se) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Udp Server catch SocketException.. ");
                    }
                } catch (Exception e) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("Udp Server catch exception " + e.getMessage());
                    }
                }
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("Udp Server end.." + this.hashCode());
            }
        }
    }

    // class Timer extends Thread {
    // public void run() {
    // while(scheduled) {
    //
    // }
    // }
    // }

}

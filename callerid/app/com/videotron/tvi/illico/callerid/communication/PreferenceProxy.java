package com.videotron.tvi.illico.callerid.communication;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Date;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.callerid.App;
import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.Util;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.callerid.controller.ServerConnectionController;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values of CallerID Application.
 * @author Jinjoo Ok
 */
public class PreferenceProxy implements PreferenceListener, RightFilterListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();
    /** XletContext. */
//    private XletContext xletContext;
    /** Whether if listener is added to UPP. */
    private boolean addedListener;
    /**
     * The day of received phone call. If this date over the SAVED_PERIOD of property file, the phone line will be
     * removed.
     */
    private static final String LAST_PHONE_CALL = "LAST_PHONE_CALL";
    /** The property name of upp. PHONE_ID_LINE=phone_id:line_no|phone_id1:line_no1.... */
    public static final String PHONE_ID_LINE = "PHONE_ID_LINE";

    /**
     * Gets instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }

    /**
     * Initialize.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
    }

    /**
     * Registers listener to UPP.
     */
    public void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        
        if (addedListener) {
        	if (Log.DEBUG_ON) {
                Log.printDebug("PreferenceProxy, addPreferenceListener addedListener = " + addedListener);
            }
        	return;
        }
        
        try {
            PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                    PreferenceService.IXC_NAME);
            String[] keys = new String[]{PreferenceNames.LANGUAGE, PreferenceNames.CALLER_ID_DISPLAY,
                    PreferenceNames.CALLER_ID_NOTIFICATION_DURATION, PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION,
                    PreferenceNames.CALLER_ID_PHONE_LINE_LIST, RightFilter.PROTECT_SETTINGS_MODIFICATIONS,
                    RightFilter.PARENTAL_CONTROL, LAST_PHONE_CALL, PHONE_ID_LINE};
            String[] values = preferenceService.addPreferenceListener(this, App.APP_NAME, keys, new String[]{
                    Definitions.LANGUAGE_ENGLISH, null, Definitions.CALLER_ID_DURATION_10SEC, null, null, null, null,
                    null, null});

            if (Log.DEBUG_ON) {
                Log.printDebug("PreferenceProxy, values " + values.length);
            }
            for (int i = 0; i < values.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("PreferenceProxy, put exist ? " + keys[i] + " " + values[i]);
                }
                DataCenter.getInstance().put(keys[i], values[i]);
            }
            addedListener = true;
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Synchronize between server and UPP.
     */
    public void setSyncToServer() {
    	boolean bootTime = DataCenter.getInstance().getBoolean("BOOT_TIME");
    	
    	if (bootTime) {
    		DataCenter.getInstance().remove("BOOT_TIME");
    		 if (Log.DEBUG_ON) {
                 Log.printDebug("this request is on boot time, so return");
             }
             return;
    	}
    	
    	
        if (DataCenter.getInstance().get("controlStatus") != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("setSyncToServer() already synchronized so return");
            }
            return;
        }
        int status = ServerConnectionController.getInstance().getControlStatus();
        if (Log.DEBUG_ON) {
            Log.printDebug("setSyncToServer controlStatus " + status);
        }
        if (status == -1) {
            CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_001);
            return;
        }
        DataCenter.getInstance().put("controlStatus", new Integer(status));
        String onoff = null;
        if (status == 1) {
            onoff = Definitions.OPTION_VALUE_ENABLE;
        } else {
            onoff = Definitions.OPTION_VALUE_DISABLE;
        }
        String value = DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_DISPLAY);
        if (status != -1 && !onoff.equals(value)) {
            setPreferenceValue(PreferenceNames.CALLER_ID_DISPLAY, onoff);
        }
    }

    /**
     * Receive preferences from Profile.
     * @param name preference name
     * @param value value of preference
     * @exception RemoteException RemoteException
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("receiveUpdatedPreference, name : " + name + " value: " + value);
        }
        if (name.equals(PreferenceNames.CALLER_ID_DISPLAY)) {
            int option = -1;
            if (value.equals(Definitions.OPTION_VALUE_DISABLE)) {
                option = 0;
            } else if (value.equals(Definitions.OPTION_VALUE_ENABLE)) {
                option = 1;
            }
            if (!addedListener) {
                return;
            }
            if (DataCenter.getInstance().get("controlStatus") != null) {
                int serverStatus = DataCenter.getInstance().getInt("controlStatus");
                if (option == -1) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("setControlStatus, value : " + option + " so return");
                    }
                    return;
                }
                if (serverStatus != option) {
                    boolean success = ServerConnectionController.getInstance().setControlStatus(option);
                    DataCenter.getInstance().put("controlStatus", new Integer(option));
                    if (!success) {
                        CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_002);
                    }
                }
            } else {
            	boolean success = ServerConnectionController.getInstance().setControlStatus(option);
                DataCenter.getInstance().put("controlStatus", new Integer(option));
                if (!success) {
                    CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_002);
                }
            }
        }
        DataCenter.getInstance().put(name, value);
    }

    /**
     * Saves phone line to memory through UPP.
     * @param name preference name
     * @param value preference value
     * @return if new line is saved, returns true
     */
    public boolean savePhoneLine(String name, String value) {
        updatePhoneExpire(value, false);
        String saved = DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_PHONE_LINE_LIST);
        if (Log.DEBUG_ON) {
            Log.printDebug("CALLER_ID_PHONE_LINE_LIST in upp : " + saved);
        }
        if (saved.indexOf(value) == -1) {
            String list = DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION);
            if (Log.DEBUG_ON) {
                Log.printDebug("CALLER_ID_PHONE_LINE_SELECTION in upp : " + list);
            }
            Object[] tokens = TextUtil.tokenize(list, "|");
            Object[] tokenNew = new Object[tokens.length + 1];
            // if (tokenNew.length > 5) {
            // if (Log.INFO_ON) {
            // Log.printDebug("phone line number is over 5, so do not save.");
            // }
            // } else {
            System.arraycopy(tokens, 0, tokenNew, 0, tokens.length);
            tokenNew[tokens.length] = value;
            Arrays.sort(tokenNew);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < tokenNew.length; i++) {
                sb.append(tokenNew[i]);
                if (i + 1 < tokenNew.length) {
                    sb.append("|");
                }
            }
            String valueNew = sb.toString();
            setPreferenceValue(PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION, valueNew);
            // }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("savePhoneLine preference name " + name);
            Log.printDebug("saved value " + saved + " new value " + value);
        }
        if (!saved.equals(TextUtil.EMPTY_STRING) && saved.indexOf(value) != -1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("savePhoneLine already saved " + name + ", so return");
            }
            return false;
        }
        if (!saved.equals(TextUtil.EMPTY_STRING)) {
            Object[] tokens = TextUtil.tokenize(saved, "|");
            Object[] tokenNew = new Object[tokens.length + 1];
            // if (tokenNew.length > 5) {
            // if (Log.INFO_ON) {
            // Log.printDebug("phone line number is over 5, so do not save.");
            // }
            // return false;
            // }
            System.arraycopy(tokens, 0, tokenNew, 0, tokens.length);
            tokenNew[tokens.length] = value;
            Arrays.sort(tokenNew);
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < tokenNew.length; i++) {
                sb.append(tokenNew[i]);
                if (i + 1 < tokenNew.length) {
                    sb.append("|");
                }
            }

            value = sb.toString();
            if (Log.DEBUG_ON) {
                Log.printDebug("saved value after sorting : " + value);
            }
        }
        setPreferenceValue(name, value);
        return true;
    }

    public void checkRightFilter() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.checkRightFilter(this, App.APP_NAME,
                        new String[]{RightFilter.PROTECT_SETTINGS_MODIFICATIONS}, null, null);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Destroy.
     */
    public void destroy() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                addedListener = false;
                preferenceService.removePreferenceListener(App.APP_NAME, this);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        DataCenter.getInstance().remove(PreferenceService.IXC_NAME);
    }

    public void receiveCheckRightFilter(int response) throws RemoteException {
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            CallerIDController.getInstance().startUnboundApplication("SETTINGS",
                    new String[]{App.APP_NAME, PreferenceService.CALLER_ID});
        }
    }

    public String[] getPinEnablerExplain() throws RemoteException {
        return null;
    }

    /**
     * Updates the day(yyyyMMdd) of received phone call.
     * @param phone phone number
     * @param all whether if all numbers are updated or not.
     */
    public void updatePhoneExpire(String phone, boolean all) {
        String count = DataCenter.getInstance().getString(LAST_PHONE_CALL);
        if (Log.DEBUG_ON) {
            Log.printDebug("updatePhoneExpire(), LAST_PHONE_CALL : " + count);
            Log.printDebug("updatePhoneExpire() phone: " + phone);
        }
        StringBuffer sb = new StringBuffer();
        String saved = DataCenter.getInstance().getString(PreferenceNames.CALLER_ID_PHONE_LINE_LIST);
        Log.printDebug("updatePhoneExpire() saved: " + saved);
        if (!all && ((count.equals(TextUtil.EMPTY_STRING) || count.indexOf(phone) == -1))) {
            if (saved.equals(TextUtil.EMPTY_STRING)) {
                saved = phone;
            } else if (saved.indexOf(phone) == -1) {
                saved = new StringBuffer(saved).append("|").append(phone).toString();
            }
            String[] tokens = TextUtil.tokenize(saved, "|");
            for (int i = 0; i < tokens.length; i++) {
                sb.append(tokens[i]);
                sb.append(":");
                sb.append(Util.getDateString(new Date()));
                if (i + 1 < tokens.length) {
                    sb.append("|");
                }
            }
            FrameworkMain.getInstance().printSimpleLog("Caller last call: " + sb.toString());
            setPreferenceValue(LAST_PHONE_CALL, sb.toString());
        } else {
            String[] list = TextUtil.tokenize(count, "|");
            boolean change = false;
            long today = Util.getDateString(new Date());
            Log.printDebug("today " + today);
            long expireDay = 0;
            StringBuffer dateNew = new StringBuffer();
            for (int i = 0; i < list.length; i++) {
                if (all || list[i].indexOf(phone) != -1) {
                    String[] cnt = TextUtil.tokenize(list[i], ":");
                    expireDay = Util.getDatString(cnt[1], DataCenter.getInstance().getInt("SAVED_PERIOD"));
                    Log.printDebug("expireDay " + expireDay);
                    if (today > expireDay) {
                        removePhoneLine(cnt[0]);
                        list[i] = null;
                    } else {
                        dateNew.append(cnt[0]);
                        dateNew.append(":");
                        dateNew.append(today);
                        list[i] = dateNew.toString();
                    }
                    change = true;
                }
                if (list[i] != null) {
                    sb.append(list[i]);
                    sb.append("|");
                }
            }
            if (sb.length() > 0 && sb.charAt(sb.length() - 1) == '|') {
                sb.deleteCharAt(sb.length() - 1);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("updatePhoneExpire() change " + change + ", new value " + sb.toString());
            }
            if (change) {
                setPreferenceValue(LAST_PHONE_CALL, sb.toString());
                FrameworkMain.getInstance().printSimpleLog("Caller last call: " + sb.toString());
            }
        }
    }

    /**
     * Removes the phone number of the preference from upp.
     * @param phone phone number
     * @param uppValue preference name of upp
     */
    private void removePhoneLine(String phone, String uppValue) {
        String saved = DataCenter.getInstance().getString(uppValue);
        StringBuffer sb = new StringBuffer(saved);
        if (saved.indexOf(phone) != -1) {
            int index = saved.indexOf(phone);

            int end = index + phone.length();
            if (index + phone.length() < saved.length() && saved.charAt(index + phone.length()) == '|') {
                end = index + phone.length() + 1;
            }
            sb.delete(index, end);
            if (sb.length() > 0 && sb.lastIndexOf("|") == sb.length() - 1) {
                sb.deleteCharAt(sb.lastIndexOf("|"));
            }
            setPreferenceValue(uppValue, sb.toString());
        }
    }

    /**
     * Removes the phone line from upp.
     * @param phone phone number
     */
    private void removePhoneLine(String phone) {
        if (Log.DEBUG_ON) {
            Log.printDebug("removePhoneLine(),  phone : " + phone);
        }
        removePhoneLine(phone, PreferenceNames.CALLER_ID_PHONE_LINE_LIST);
        removePhoneLine(phone, PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION);
    }

    /**
     * Sets preference value.
     * @param key preference name
     * @param value preference value
     */
    public void setPreferenceValue(String key, String value) {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.setPreferenceValue(key, value);
            } catch (Exception e) {
                Log.print(e);
            }
        } else {
            Log.printDebug("setPreferenceValue() PreferenceService is null");
        }
    }

    /**
     * Initialize all phone line.
     */
    public void initPhoneLine() {
        if (Log.DEBUG_ON) {
            Log.printDebug("initPhoneLine()");
        }
        setPreferenceValue(PreferenceNames.CALLER_ID_PHONE_LINE_LIST, TextUtil.EMPTY_STRING);
        setPreferenceValue(PreferenceNames.CALLER_ID_PHONE_LINE_SELECTION, TextUtil.EMPTY_STRING);
        setPreferenceValue(LAST_PHONE_CALL, TextUtil.EMPTY_STRING);
    }

    // public void initPhoneLineTab() {
    // if (Log.DEBUG_ON) {
    // Log.printDebug("initPhoneLineTab()");
    // }
    // setPreferenceValue(PHONE_ID_LINE, TextUtil.EMPTY_STRING);
    // }
    /**
     * Returns phone line number.
     * @param phoneId phone id
     * @return index of line
     */
    public int getPhoneLineTab(String phoneId) {
        int index = -1;
        String lineList = DataCenter.getInstance().getString(PHONE_ID_LINE);
        String[] list = TextUtil.tokenize(lineList, "|");
        String[] temp = null;
        for (int i = 0; i < list.length; i++) {
            temp = TextUtil.tokenize(list[i], ":");
            if (temp[0].equals(phoneId)) {
                index = Integer.parseInt(temp[1]);
            }
        }
        if (index != -1) {
            return index;
        } else {
            updataPhoneLineTab(phoneId, String.valueOf(list.length));
            return list.length;
        }
        // return index;
    }

    /**
     * Updates the line number of the phone id.
     * @param phoneId phone id
     * @param line line number. '-1' is a initial value.
     */
    public void updataPhoneLineTab(String phoneId, String line) {
        StringBuffer sb = new StringBuffer();
        String saved = DataCenter.getInstance().getString(PHONE_ID_LINE);
        String[] tokens = TextUtil.tokenize(saved, "|");
        String[] temp = null;
        if (saved.indexOf(phoneId) != -1) {
            for (int i = 0; i < tokens.length; i++) {
                temp = TextUtil.tokenize(tokens[i], ":");
                if (temp[0].equals(phoneId) && !line.equals("-1")) {
                    sb.append(temp[0]);
                    sb.append(":");
                    sb.append(line);
                    if (i + 1 < tokens.length) {
                        sb.append("|");
                    }
                }
            }
        } else {
            if (tokens.length > 0) {
                sb.append(saved);
                sb.append("|");
            }
            sb.append(phoneId);
            sb.append(":");
            sb.append(line);
        }
        setPreferenceValue(PHONE_ID_LINE, sb.toString());
    }
}

package com.videotron.tvi.illico.callerid.communication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.callerid.App;
import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.callerid.controller.ServerConnectionController;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.epg.VideoResizeListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.EIDMappingTableUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * CommunicationManager class looks up communication interface provided by other applications. And it receives In-Band
 * data from MS, parses data.
 * @author Jinjoo Ok
 */
public final class CommunicationManager implements DataUpdateListener, ChannelEventListener{
    /** XletContext. */
    private XletContext xletCtx;
    /** class instance. */
    private static CommunicationManager instance = new CommunicationManager();
    /** Listener for Receiving inband data. */
    private InbandListener inbandListener = new InbandListener();
    /** listener for AV resize. */
    private ResizeListener vImpl;
    /** listener for check of subscribe. */
    private EIDListener eidListener;
    /** The version of configuration file. */
    private int versionCallerIDConfig;
    /** Listener for log level change. */
    private LogLevelAdapter logListener = new LogLevelAdapter();

    private IxcLookupWorker ixcWorker;

    /**
     * Get Communication instance.
     * @return CommunicationController
     */
    public static CommunicationManager getInstance() {
        return instance;
    }

    /**
     * Constructor.
     */
    private CommunicationManager() {
    }

    /**
     * lookup Services. bind CallerIDService to IXC.
     * @param xCtx XletContext
     */
    public void init(XletContext xCtx) {
        this.xletCtx = xCtx;
        ixcWorker = new IxcLookupWorker(xCtx);
        vImpl = new ResizeListener();
        eidListener = new EIDListener();
        DataCenter.getInstance().addDataUpdateListener(Rs.DCKEY_INBAND, this);

        // ixcWorker.lookup("/1/2040/", DaemonService.IXC_NAME);
        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, this);
        ixcWorker.lookup("/1/2026/", EpgService.IXC_NAME, this);
        ixcWorker.lookup("/1/2110/", NotificationService.IXC_NAME, this);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME);
        ixcWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME);
        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME);
        if (!Environment.EMULATOR) {
            ixcWorker.lookup("/1/2100/", StcService.IXC_NAME, this);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("init, addDataUpdateListener()");
        }

    }

    /**
     * Called when application destroy.
     */
    public void destroy() {
        DataCenter dc = DataCenter.getInstance();
        dc.remove(DaemonService.IXC_NAME);
        dc.remove(EpgService.IXC_NAME);
        dc.remove(NotificationService.IXC_NAME);
        dc.remove(MonitorService.IXC_NAME);
        dc.remove(LoadingAnimationService.IXC_NAME);
        dc.remove(ErrorMessageService.IXC_NAME);
    }

    /**
     * Requests resizing screen to Video Controller.
     * @param x x position
     * @param y y position
     * @param w width
     * @param h weight
     */
    public void requestResizeScreen(int x, int y, int w, int h) {
        VideoController ctl = getVideoController();
        if (ctl != null) {
            try {
                ctl.resize(x, y, w, h, VideoController.SHOW);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Gets Video Controller.
     * @return VideoController
     */
    private VideoController getVideoController() {
        VideoController vCnt = (VideoController) DataCenter.getInstance().get("VideoController");
        if (vCnt != null) {
            return vCnt;
        }
        EpgService svc = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

        if (svc != null) {
            try {
                vCnt = svc.getVideoController();
                if (vCnt != null) {
                    vCnt.addVideoResizeListener(vImpl);
                    DataCenter.getInstance().put("VideoController", vCnt);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return vCnt;
    }

    /**
     * As received events, parse IB data.
     * @param file IB data
     */
    private void parseData(File file) {
        // if (Log.DEBUG_ON) {
        // Log.printDebug("parseData!! ");
        // }
        FileInputStream fis = null;
        ByteArrayOutputStream baos = null;
        try {
            // File
            fis = new FileInputStream(file);
            baos = new ByteArrayOutputStream();
            int len = 0;
            while ((len = fis.read()) != -1) {
                baos.write(len);
            }
            byte[] arr = baos.toByteArray();
            len = arr.length;

            int index = 0;
            int version = oneByteToInt(arr, index++);
            if (Log.INFO_ON) {
                Log.printInfo("CallerID parseData() version = " + version);
            }
            if (version != versionCallerIDConfig) {
                versionCallerIDConfig = version;
                len = oneByteToInt(arr, index++);
                /** main server information */

                DataCenter.getInstance().put(Rs.DCKEY_SERVER_IP, new String(arr, index, len)); // IP or DNS
                index += len;
                DataCenter.getInstance().put(Rs.DCKEY_SERVER_TCP_PORT, String.valueOf(twoBytesToInt(arr, index))); // port
                index += 2;
                DataCenter.getInstance().put(Rs.DCKEY_SERVER_UDP_PORT, String.valueOf(twoBytesToInt(arr, index))); // port
                index += 2;
                len = oneByteToInt(arr, index++);
                DataCenter.getInstance().put(Rs.DCKEY_SERVER_CONTEXT, new String(arr, index, len)); // context
                index += len;

                len = oneByteToInt(arr, index++);
                /** secondary server information */
                DataCenter.getInstance().put(Rs.DCKEY_BACKUP_SERVER_IP, new String(arr, index, len)); // IP or DNS
                index += len;
                DataCenter.getInstance()
                        .put(Rs.DCKEY_BACKUP_SERVER_TCP_PORT, String.valueOf(twoBytesToInt(arr, index))); // port
                index += 2;
                DataCenter.getInstance()
                        .put(Rs.DCKEY_BACKUP_SERVER_UDP_PORT, String.valueOf(twoBytesToInt(arr, index))); // port
                index += 2;
                len = oneByteToInt(arr, index++);
                DataCenter.getInstance().put(Rs.DCKEY_BACKUP_SERVER_CONTEXT, new String(arr, index, len)); // context
                index += len;
                // main proxy
                len = oneByteToInt(arr, index++);
                if (Log.DEBUG_ON) {
                    Log.printDebug("len " + len);
                }
                if (len > 0) {
                    String[] proxyIP = new String[len];
                    int[] proxyPort = new int[len];
                    for (int i = 0, ipLen = 0; i < len; i++) {
                        ipLen = oneByteToInt(arr, index++);
                        proxyIP[i] = new String(arr, index, ipLen);
                        index += ipLen;
                        proxyPort[i] = twoBytesToInt(arr, index);
                        index += 2;
                        if (Log.INFO_ON) {
                            Log.printInfo("CallerID parseData() proxyIP : " + proxyIP[i] + ", port : "
                                    + proxyPort[i]);
                        }
                    }
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_USE, "true");
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_HOST, proxyIP);
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_PORT, proxyPort);
                } else {
                    DataCenter.getInstance().put(Rs.DCKEY_PROXY_USE, "false");
                    DataCenter.getInstance().remove(Rs.DCKEY_PROXY_HOST);
                    DataCenter.getInstance().remove(Rs.DCKEY_PROXY_PORT);
                    if (Log.INFO_ON) {
                        Log.printInfo("CallerID parseData() proxy server not used!!");
                    }
                }
                if (Log.INFO_ON) {
                    Log.printInfo("CallerID parseData() serverInfo main ip : "
                            + DataCenter.getInstance().getString(Rs.DCKEY_SERVER_IP));
                    Log.printInfo("CallerID parseData() serverInfo main port for TCP: "
                            + DataCenter.getInstance().getString(Rs.DCKEY_SERVER_TCP_PORT));
                    Log.printInfo("CallerID parseData() serverInfo main port for UDP: "
                            + DataCenter.getInstance().getString(Rs.DCKEY_SERVER_UDP_PORT));
                    Log.printInfo("CallerID parseData() serverInfo bakcup ip : "
                            + DataCenter.getInstance().getString(Rs.DCKEY_BACKUP_SERVER_IP));
                    Log.printInfo("CallerID parseData() serverInfo bakcup port for TCP: "
                            + DataCenter.getInstance().getString(Rs.DCKEY_BACKUP_SERVER_TCP_PORT));
                    Log.printInfo("CallerID parseData() serverInfo bakcup port for UDP: "
                            + DataCenter.getInstance().getString(Rs.DCKEY_BACKUP_SERVER_UDP_PORT));
                    Log.printInfo("CallerID parseData() serverInfo main context root "
                            + DataCenter.getInstance().getString(Rs.DCKEY_SERVER_CONTEXT));
                    Log.printInfo("CallerID parseData() serverInfo bakcup context root "
                            + DataCenter.getInstance().getString(Rs.DCKEY_BACKUP_SERVER_CONTEXT));
                }
                len = oneByteToInt(arr, index++);
                DataCenter.getInstance().put(Rs.DCKEY_IMG_PATH, new String(arr, index, len));
                if (Log.INFO_ON) {
                    Log.printInfo("Server url : " + DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
            CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_502);
        } finally {
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception ex) {
                   Log.print(ex);
                }
            }
        }
    }

    /**
     * Requests channel change.
     * @param id channel id
     */
    public void requestChangeChannel(int id) {
        try {
            EpgService epg = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            if (epg != null) {
                epg.getChannelContext(0).changeChannel(id);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Changes one byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Changes two byte to integer.
     * @param data byte array
     * @param offset start index
     * @return integer
     */
    public static int twoBytesToInt(byte[] data, int offset) {
        return (((((int) data[offset]) & 0xFF) << 8) + (((int) data[offset + 1]) & 0xFF));
    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        if (value instanceof File) {
            if (key.equals(Rs.DCKEY_INBAND) && value != null) {
                parseData((File) value);
            }
        } else if (key.equals(MonitorService.IXC_NAME)) {
            MonitorService service = (MonitorService) value;
            try {
                service.addInbandDataListener(inbandListener, App.APP_NAME);
                service.addEIDMappingTableUpdateListener(eidListener, App.APP_NAME);
            } catch (Exception e) {
                Log.print(e);
            }
            new Thread() {
                public void run() {
                    CallerIDController.getInstance().setMacAddress();
                }
            } .start();
        } else if (key.equals(NotificationService.IXC_NAME)) {
            CallerIDController.getInstance().addNotificationListener();
        } else if (key.equals(StcService.IXC_NAME)) {
            try {
                int currentLevel = ((StcService) value).registerApp(App.APP_NAME);
                if (Log.DEBUG_ON) {
                    Log.printDebug("stc log level : " + currentLevel);
                }
                Log.setStcLevel(currentLevel);
                Object o = SharedMemory.getInstance().get("stc-" + App.APP_NAME);
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    ((StcService) value).addLogLevelChangeListener(App.APP_NAME, logListener);
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        } else if (key.equals(EpgService.IXC_NAME)) {
            EpgService service = (EpgService) value;
            try {
                service.getChannelContext(0).addChannelEventListener(this);
            } catch (Exception e) {
                Log.print(e);
            }
        }

    }

    public void dataRemoved(String key) {

    }

    class ResizeListener implements VideoResizeListener {
        public void mainScreenResized(int x, int y, int w, int h) throws RemoteException {
        }
    }

    /**
     * Listener for change of EID table. If there are any changes in EID table, Callerid application checks the
     * registration.
     * @author Jinjoo Ok
     */
    class EIDListener implements EIDMappingTableUpdateListener {
        public void tableUpdated() {
            if (Log.DEBUG_ON) {
                Log.printDebug("EID table is updated, call registerToServer");
            }
            Thread th = new Thread() {
                public void run() {
                    CallerIDController.getInstance().registerToServer();
                }
            };
            th.start();
        }
    }

    /**
     * Log level change listener.
     * @author Jinjoo Ok
     */
    class LogLevelAdapter implements LogLevelChangeListener {
        /**
         * It is called when log level changed.
         * @param logLevel level of log
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    /**
     * Listener for receiving Inband data.
     */
    class InbandListener implements InbandDataListener {
        /**
         * called when Monitor tuned Inband channel.
         * @param locator string Locator
         */
        public void receiveInbandData(String locator) {

            if (Log.DEBUG_ON) {
                Log.printDebug("receiveInbandData " + locator);
            }
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            int monitorFileVersion = -1;
            try {
                monitorFileVersion = monitor.getInbandDataVersion(MonitorService.callerid_server_config);
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(e);
                }
            }
            if (monitorFileVersion != versionCallerIDConfig) {
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }
            try {
                if (monitor != null) {
                    monitor.completeReceivingData(App.APP_NAME);
                }
            } catch (Exception e) {
                if (Log.WARNING_ON) {
                    Log.printWarning(e);
                }
            }
        }
    }

    public void selectionRequested(int id, short type) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("CommunicationManager.selectionRequested() called ");
        }
        EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (epgService != null) {
            epgService.getChannelContext(0).removeChannelEventListener(this);
            new Thread() {
                public void run() {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("call ServerConnectionController.getInstance().start() ");
                    }
                    
                    DataCenter.getInstance().put("BOOT_TIME", new Boolean(true));
                    
                    ServerConnectionController.getInstance().start();

                    PreferenceProxy.getInstance().addPreferenceListener();
                }
            } .start();
        }
    }

    public void selectionBlocked(int id, short type) throws RemoteException {
    }

}

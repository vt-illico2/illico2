package com.videotron.tvi.illico.callerid.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.callerid.data.Call;
import com.videotron.tvi.illico.callerid.ui.CallLog;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * Renderer for Call log ui.
 * @author Jinjoo Ok
 */
public class CallLogRenderer extends Renderer {
    /** list separation image. */
    private Image iLine;
    /** shadow image of bottom. */
//    private Image iLineShadowB;
    /** shadow image of top. */
//    private Image iLineShadowT;
    /** background image. */
    private Image iBg;
    /** Background image of list part. */
    private Image iListBg;
    /** Top of pane arrow image. */
    private Image iArrowTop;
    /** Bottom of pane arrow image. */
    private Image iArrowBottom;
    /** image of option part. */
    private Image iOptionM;
    /** image of option part. */
    private Image iOption1st;
    /** image of option part. */
    private Image iOption2nd;
    /** list separation image of option part. */
    private Image iOptionLine;
    /** shadow image of option part. */
//    private Image iOptionShadow;
    /** shadow image of option part. */
//    private Image iOptionB;
    /** option arrow image. */
    private Image iOptionArr;
    /** arrow image. */
    private Image iHistory;
    /** Option button focus image. */
    private Image iOptFocus;
    /** focus of option part. */
    // private Image iListFocusDim;
    /** list focus. */
    // private Image iListFocus;
    /** the Clock icon image. */
    private Image iClock;
    /** image of Av area. */
    private Image iAvOut;
    /** Tab focus image. */
    private Image iTabFocus;
    /** Tab image has not a focus. */
    private Image iTabDim;
    /** Tab image. */
    private Image iTab;
    /** Empty tab image. */
    private Image iTabInac;
    /** background image of bottom part. */
    private Image iHotkeybg;

    /** Color of list title. */
    private Color cTitle = new Color(204, 204, 204);
    /** Color. */
    private Color c236 = new Color(236, 236, 237);
    /** Color. */
    private Color c230 = new Color(230, 230, 230);
    /** Color. */
    private Color c189 = new Color(189, 189, 189);
    /** Color. */
    private Color c3 = new Color(3, 3, 3);
    /** Color. */
    // private Color c23 = new Color(23, 23, 23);
    /** Color. */
    private Color c110 = new Color(110, 110, 110);
    /** Font of size 17. */
    private Font f17 = FontResource.BLENDER.getFont(17);
    /** Font of size 18. */
    private Font f18 = FontResource.BLENDER.getFont(18);
    /** Font of size 19. */
    private Font f19 = FontResource.BLENDER.getFont(19);
    /** Font of size 21. */
    private Font f21 = FontResource.BLENDER.getFont(21);
    /** Font of size 26. */
    private Font f26 = FontResource.BLENDER.getFont(26);

    public static final int MAX_LIST_NUM = 9;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    private void paintBg(Graphics g, UIComponent c) {
        g.drawImage(iBg, 0, 0, c);
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        int start = 53;
        if (imgLogo != null) {
            g.drawImage(imgLogo, start, 21, c);
            start += imgLogo.getWidth(c) + 14;
            g.drawImage(iHistory, start, 45, c);
            start += 14;
        }
        g.setColor(c236);
        g.setFont(f18);
        g.drawString(DataCenter.getInstance().getString("Label.lblCallerid"), start, 57);
        g.setFont(f26);
        g.setColor(Color.white);
        g.drawString(DataCenter.getInstance().getString("Label.lblCallLog"), 60, 98);
        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(f17);
                g.setColor(Color.white);
                longDateWth = FontResource.getFontMetrics(f17).stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
            }
        }
        int imgClockWth = iClock.getWidth(c);
        g.drawImage(iClock, 910 - longDateWth - 8 - imgClockWth, 43, c);
        g.drawImage(iHotkeybg, 367, 466, c);
    }

    protected void paint(Graphics g, UIComponent c) {
        CallLog ui = ((CallLog) c);
        paintBg(g, c);

        g.drawImage(iListBg, 7, 142, c);
        g.setColor(cTitle);
        g.setFont(f17);
        g.drawString("Date", 70, 165);
        g.drawString(DataCenter.getInstance().getString("Label.lblTime"), 185, 165);
        g.drawString(DataCenter.getInstance().getString("Label.lblName"), 270, 165);
        g.drawString(DataCenter.getInstance().getString("Label.lblNumber"), 495, 165);

        g.drawImage(iOptionM, 628, 389, c);
        g.drawImage(iOption1st, 628, 388, c);
        g.drawImage(iOption2nd, 628, 431, c);
        g.drawImage(iOptionLine, 643, 426, c);
//        g.drawImage(iOptionB, 627, 446, c);
//        g.drawImage(iOptionShadow, 581, 458, c);

        Call[][] list = ui.getCallList();
        // boolean focusList = ui.isFocusOnList();
        boolean focusTab = ui.isTabFocus();
        if (list == null) {
            if (ui.isNoCall()) {
                g.setColor(c110);
                g.setFont(f17);
                GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblNoCalls"), 308, 199);
                g.drawImage(iLine, 68, 208, c);
            }
        } else {
            int tab = ui.getFocus();
            if (list.length > 1) {
                drawTabs(g, ui, list.length);
            }
            int curPage = ui.getCurPage();
            int totalPage = ui.getTotalPage();
            for (int i = 0; i < list[tab].length; i++) {
                if (i > MAX_LIST_NUM - 1 || curPage * MAX_LIST_NUM + i > list[tab].length - 1) {
                    break;
                }
                g.setFont(f17);
                g.setColor(c189);
                g.drawString(list[tab][curPage * MAX_LIST_NUM + i].getDate(), 71, 199 + i * 32);
                g.drawString(list[tab][curPage * MAX_LIST_NUM + i].getTime(), 185, 199 + i * 32);
                GraphicUtil.drawStringRight(g, list[tab][curPage * MAX_LIST_NUM + i].getCallingPhoneNumber(), 584,
                        199 + i * 32);
                g.setFont(f18);
                g.setColor(c230);
                g.drawString(
                        TextUtil.shorten(list[tab][curPage * MAX_LIST_NUM + i].getName(), g.getFontMetrics(), 215),
                        270, 199 + i * 32);
                if (i < MAX_LIST_NUM - 1) {
                    g.drawImage(iLine, 68, 208 + i * 32, c);
                }
            }
            if (list[tab].length > MAX_LIST_NUM) {
                if (curPage > 0) {
//                    g.drawImage(iLineShadowT, 55, 175, c);
                    g.drawImage(iArrowTop, 325, 168, c);
                }
                if (curPage < totalPage - 1) {
//                    g.drawImage(iLineShadowB, 55, 408, c);
                    g.drawImage(iArrowBottom, 325, 457, c);
                }
            }
        }
        int optFoc = ui.getOptionFocus();
        if (!focusTab) {
            g.drawImage(iOptFocus, 626, 387 + optFoc * 39, c);
        }
        int startX = 673;
        if (optFoc == 1 && !focusTab) {
            g.setFont(f21);
            g.setColor(c3);
            startX = 668;
            g.drawImage(iOptionArr, 651, 405, c);
        } else {
            g.setColor(c230);
            g.setFont(f18);
            g.drawImage(iOptionArr, 651, 444, c);
        }

        g.drawString(DataCenter.getInstance().getString("Label.lblPreferences"), startX, 453);
        startX = 673;
        if (optFoc == 0 && !focusTab) {
            g.setFont(f21);
            g.setColor(c3);
            startX = 668;
            g.drawImage(iOptionArr, 651, 444, c);
        } else {
            g.setColor(c230);
            g.setFont(f18);
            g.drawImage(iOptionArr, 651, 405, c);

        }
        if (list == null) {
            g.setColor(c110);
            g.setFont(f18);
        }
        g.drawString(DataCenter.getInstance().getString("Label.lblDelete"), startX, 414);

        g.drawImage(iAvOut, 622, 84, c);
    }

    /**
     * Draws Tab.
     * @param g Graphics
     * @param ui CallLog
     * @param tabSize size of tabs
     */
    private void drawTabs(Graphics g, CallLog ui, int tabSize) {
        int tFoc = ui.getFocus();
        g.setFont(f18);
        g.setColor(c230);
        String line = DataCenter.getInstance().getString("Label.lblLine");
        for (int i = 0; i < 5; i++) {
            if (i != tFoc) {
                if (i < tabSize) {
                    g.drawImage(iTab, 53 + i * 88, 111, ui);
                    GraphicUtil.drawStringCenter(g, line + " " + (i + 1), 98 + i * 88, 133);
                } else {
                    g.drawImage(iTabInac, 53 + i * 88, 111, ui);
                }
            }
        }
        if (ui.isTabFocus()) {
            g.drawImage(iTabFocus, 53 + tFoc * 88, 111, ui);
        } else {
            g.drawImage(iTabDim, 53 + tFoc * 88, 111, ui);
        }
        g.setFont(f19);
        g.setColor(Color.black);
        GraphicUtil.drawStringCenter(g, line + " " + (tFoc + 1), 98 + tFoc * 88, 133);
    }

    public void prepare(UIComponent c) {
        iLine = DataCenter.getInstance().getImage("07_wizardline.png");
//        iLineShadowB = DataCenter.getInstance().getImage("07_wizard_sha_bottom.png");
//        iLineShadowT = DataCenter.getInstance().getImage("07_wizard_sha_top.png");
        iBg = DataCenter.getInstance().getImage("bg.jpg");
        iListBg = DataCenter.getInstance().getImage("07_wizardbg.png");
        iArrowTop = DataCenter.getInstance().getImage("02_ars_t.png");
        iArrowBottom = DataCenter.getInstance().getImage("02_ars_b.png");
        iOptionM = DataCenter.getInstance().getImage("01_acbg_m.png");
        iOption1st = DataCenter.getInstance().getImage("01_acbg_1st.png");
        iOption2nd = DataCenter.getInstance().getImage("01_acbg_2nd.png");
        iOptionLine = DataCenter.getInstance().getImage("01_acline.png");
//        iOptionShadow = DataCenter.getInstance().getImage("01_ac_shadow.png");
//        iOptionB = DataCenter.getInstance().getImage("01_acglow.png");
        iOptionArr = DataCenter.getInstance().getImage("02_detail_ar.png");
        iHistory = DataCenter.getInstance().getImage("his_over.png");
        iOptFocus = DataCenter.getInstance().getImage("02_detail_bt_foc.png");
        // iListFocusDim = DataCenter.getInstance().getImage("07_wizardfocus_dim.png");
        // iListFocus = DataCenter.getInstance().getImage("07_wizardfocus.png");
        iClock = DataCenter.getInstance().getImage("clock.png");
        iAvOut = DataCenter.getInstance().getImage("02_av_out.png");
        iTabFocus = DataCenter.getInstance().getImage("16_tab_foc.png");
        iTab = DataCenter.getInstance().getImage("16_tab.png");
        iTabInac = DataCenter.getInstance().getImage("16_tab_inac.png");
        iTabDim = DataCenter.getInstance().getImage("16_tab_foc_dim.png");
        iHotkeybg = DataCenter.getInstance().getImage("01_hotkeybg.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Releases images.
     */
    public void releaseImage() {
        DataCenter.getInstance().removeImage("01_hotkeybg.png");
        DataCenter.getInstance().removeImage("07_wizardline.png");
//        DataCenter.getInstance().removeImage("07_wizard_sha_bottom.png");
//        DataCenter.getInstance().removeImage("07_wizard_sha_top.png");
        DataCenter.getInstance().removeImage("bg.jpg");
        DataCenter.getInstance().removeImage("07_wizardbg.png");
        DataCenter.getInstance().removeImage("02_ars_t.png");
        DataCenter.getInstance().removeImage("02_ars_b.png");
        DataCenter.getInstance().removeImage("01_acbg_m.png");
        DataCenter.getInstance().removeImage("01_acbg_1st.png");
        DataCenter.getInstance().removeImage("01_acbg_2nd.png");
        DataCenter.getInstance().removeImage("01_acline.png");
//        DataCenter.getInstance().removeImage("01_ac_shadow.png");
//        DataCenter.getInstance().removeImage("01_acglow.png");
        DataCenter.getInstance().removeImage("02_detail_ar.png");
        DataCenter.getInstance().removeImage("his_over.png");
        DataCenter.getInstance().removeImage("02_detail_bt_foc.png");
        // DataCenter.getInstance().removeImage("07_wizardfocus_dim.png");
        // DataCenter.getInstance().removeImage("07_wizardfocus.png");
        DataCenter.getInstance().removeImage("clock.png");
        DataCenter.getInstance().removeImage("02_av_out.png");
        DataCenter.getInstance().removeImage("16_tab_foc.png");
        DataCenter.getInstance().removeImage("16_tab.png");
        DataCenter.getInstance().removeImage("16_tab_inac.png");
        DataCenter.getInstance().removeImage("16_tab_foc_dim.png");
    }
}

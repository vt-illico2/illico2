package com.videotron.tvi.illico.callerid.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.callerid.ui.Teaser;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * Renderer of Teaser.
 * @author Jinjoo Ok
 */
public class TeaserRenderer extends Renderer {
    /** background image. */
    private Image bg;
    /** arrow image. */
    private Image iHistory;
    /** the Clock icon image. */
    private Image iClock;
    /** Button image. */
    private Image iFocus;
    /** Color. */
    private Color c236 = new Color(236, 236, 237);
    /** Color. */
    private Color c3 = new Color(3, 3, 3);
    /** Font of size 18. */
    private Font f18 = FontResource.BLENDER.getFont(18);
    /** Font of size 17. */
    private Font f17 = FontResource.BLENDER.getFont(17);

    public void prepare(UIComponent c) {
        bg = DataCenter.getInstance().getImage("bg.jpg");
        iHistory = DataCenter.getInstance().getImage("his_over.png");
        iClock = DataCenter.getInstance().getImage("clock.png");
        iFocus = DataCenter.getInstance().getImage("05_focus.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Releases images.
     */
    public void releaseImage() {
        DataCenter.getInstance().removeImage("bg.jpg");
        DataCenter.getInstance().removeImage("clock.png");
        DataCenter.getInstance().removeImage("his_over.png");
        DataCenter.getInstance().removeImage("05_focus.png");
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    /**
     * Paint.
     * @param g Graphics
     * @param c UIComponent
     */
    public void paint(Graphics g, UIComponent c) {
        g.drawImage(bg, 0, 0, c);
        Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        int start = 53;
        if (imgLogo != null) {
            g.drawImage(imgLogo, start, 21, c);
            start += imgLogo.getWidth(c) + 14;
            g.drawImage(iHistory, start, 45, c);
            start += 14;
        }
        g.setColor(c236);
        g.setFont(f18);
        g.drawString(DataCenter.getInstance().getString("Label.lblCallerid"), start, 57);

        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(f17);
                g.setColor(Color.white);
                longDateWth = FontResource.getFontMetrics(f17).stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
            }
        }
        int imgClockWth = iClock.getWidth(c);
        g.drawImage(iClock, 910 - longDateWth - 8 - imgClockWth, 43, c);

        Image teaser = ((Teaser) c).getTeaserImage();
        if (teaser != null) {
            g.drawImage(teaser, 47, 129, c);
            g.drawImage(iFocus, 483, 388, c);
            g.setColor(c3);
            g.setFont(f18);
            GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblBack"), 562, 410);
        }
    }
}

package com.videotron.tvi.illico.callerid.ui;

import java.awt.Image;
import java.io.IOException;
import java.util.Date;
import java.util.Hashtable;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.callerid.gui.TeaserRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.callerid.App;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * if the user's package does not include Caller ID, show teaser instead of call notification.
 * @author Jinjoo Ok
 */
public class Teaser extends UIComponent implements ClockListener {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = -5588784291635809538L;
    /** Teaser Image. */
    private Image teaser;
    /** Effect. */
    private ClickingEffect clickEffect;
    /** Footer. */
    private Footer footer;

    /**
     * constructor.
     */
    public Teaser() {
        setRenderer(new TeaserRenderer());
        setVisible(false);
        clickEffect = new ClickingEffect(this, 5);
        footer = new Footer();
        footer.setBounds(0, 488, 906, 25);
        add(footer);
    }

    /**
     * Prepare.
     */
    public void prepare() {
        footer.reset();
        Hashtable imgButtonTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        footer.addButton((Image) imgButtonTable.get(PreferenceService.BTN_EXIT), "Label.lblExit");
        renderer.prepare(this);
    }

    public void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("start!!!");
        }
        Clock.getInstance().addClockListener(this);
        prepare();
        setTeaserImage();
        if (!CallerIDController.getInstance().root.isActive()) {
            CallerIDController.getInstance().root.activate();
        }
        setVisible(true);
    }

    /**
     * Sets teaser image from Web.
     */
    public void setTeaserImage() {
        // if (Environment.EMULATOR) {
        // teaser = DataCenter.getInstance().getImage("instruction_image.png");
        // return;
        // }
        new Thread() {
            public void run() {
                StringBuffer imgName = new StringBuffer(DataCenter.getInstance().getString(Rs.DCKEY_IMG_PATH));
                imgName.append("teaser_");
                String lang = DataCenter.getInstance().getString(PreferenceNames.LANGUAGE);
                if (lang.equals(Definitions.LANGUAGE_ENGLISH)) {
                    imgName.append("en.png");
                } else {
                    imgName.append("fr.png");
                }
                try {
                    Thread.sleep(500);
                } catch (Exception e) {
                    Log.print(e);
                }
                CallerIDController.getInstance().showLoadingAnimation();
                try {
                    byte[] imgByte = URLRequestor.getBytes(imgName.toString(), null);
                    if (!(imgByte == null || imgByte.length == 0)) {
                        FrameworkMain.getInstance().getImagePool().remove("TEASER");
                        teaser = FrameworkMain.getInstance().getImagePool().createImage(imgByte, "TEASER");
                        if (Log.DEBUG_ON) {
                            Log.printDebug("setTeaserImage end!!");
                        }
                    }
                    repaint();
                } catch (IOException e) {
                    CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_503);
                    Log.print(e);
                } finally {
                    CallerIDController.getInstance().hideLoadingAnimation();
                }
            }
        }.start();
    }

    /**
     * Gets teaser image.
     * @return image
     */
    public Image getTeaserImage() {
        return teaser;
    }

    public boolean handleKey(int code) {
        boolean used = false;
        switch (code) {
        case KeyCodes.LAST:
        case OCRcEvent.VK_EXIT:
        case Rs.KEY_OK:
            used = true;
            if (code == KeyCodes.LAST || code == Rs.KEY_OK) {
                clickEffect.start(483, 388, 156, 33);
            } else {
                footer.clickAnimation(0);
            }
            CallerIDController.getInstance().startUnboundApplication("Menu",   new String[]{App.APP_NAME});
            break;
        default:
            break;
        }
        return used;
    }

    /**
     * Stops scene.
     */
    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("stop");
        }
        Clock.getInstance().removeClockListener(this);
        if (teaser != null) {
            FrameworkMain.getInstance().getImagePool().remove("TEASER");
            teaser = null;
        }
        setVisible(false);
        ((TeaserRenderer) renderer).releaseImage();
    }

    public void clockUpdated(Date date) {
        repaint();
    }
}

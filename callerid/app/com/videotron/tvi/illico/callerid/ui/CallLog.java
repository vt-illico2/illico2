package com.videotron.tvi.illico.callerid.ui;

import java.awt.EventQueue;
import java.awt.Image;
import java.util.Date;
import java.util.Hashtable;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.callerid.App;
import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.communication.CommunicationManager;
import com.videotron.tvi.illico.callerid.communication.PreferenceProxy;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.callerid.controller.ServerConnectionController;
import com.videotron.tvi.illico.callerid.data.Call;
import com.videotron.tvi.illico.callerid.data.DataManager;
import com.videotron.tvi.illico.callerid.gui.CallLogRenderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * Displays call log received from back-end.
 * @author Jinjoo Ok
 */
public class CallLog extends UIComponent implements PopupListener, ClockListener {

    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 3261471400802449572L;
    /** Call log. */
    private Call[][] callList;
    /** Confirm popup for deletion log. */
    private Popup confirmPop;
    /** focus of deletion and preference. */
    private int optionFocus;
    /** Indicates whether if focus is on the tab. */
    private boolean isTabFocus;
    /** AlphaEffect. */
    private AlphaEffect fadeIn;
    /** Effect. */
    private ClickingEffect clickEffect;
    private boolean back;
    /** Footer. */
    private Footer footer;
    private boolean noCall;
    private int curPage;
    private int totalPage;

    /**
     * Constructor.
     */
    public CallLog() {
        setRenderer(new CallLogRenderer());
        fadeIn = new AlphaEffect(this, 15, AlphaEffect.FADE_IN);
        setVisible(false);
        clickEffect = new ClickingEffect(this, 5);
        footer = new Footer();
        footer.setBounds(0, 488, 906, 25);
        add(footer);
    }

    public void prepare() {
        footer.reset();
        Hashtable imgButtonTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        footer.addButton((Image) imgButtonTable.get(PreferenceService.BTN_PAGE), "Label.lblPage");
        footer.addButton((Image) imgButtonTable.get(PreferenceService.BTN_EXIT), "Label.lblExit");
        renderer.prepare(this);
    }

    public void init() {
        focus = 0;
        optionFocus = 0;
        callList = null;
        isTabFocus = true;
        noCall = false;
    }

    /**
     * Starts.
     * @param reset whether if data reset or not.
     */
    public void start(boolean reset) {
        Clock.getInstance().addClockListener(this);
        setVisible(false);
        prepare();
        back = !reset;
        if (reset) {
            init();
        }

        if (!CallerIDController.getInstance().root.isActive()) {
            CallerIDController.getInstance().root.activate();
        }
        fadeIn.start();
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                CommunicationManager.getInstance().requestResizeScreen(628, 84, 284, 160);
                CommunicationManager.getInstance().requestChangeChannel(-1);
            }
        });
    }

    /**
     * Stops this scene.
     */
    public void stop() {
        Clock.getInstance().removeClockListener(this);
        CommunicationManager.getInstance().requestResizeScreen(0, 0, 960, 540);
        setVisible(false);
        ((CallLogRenderer) renderer).releaseImage();
        if (confirmPop != null) {
            confirmPop.stop(false);
        }
    }

    public void animationEnded(Effect effect) {
        if (effect == fadeIn) {
            setVisible(true);
            if (!back) {
                new Thread() {
                    public void run() {
                        DataManager.getInstance().init();
                        if (ServerConnectionController.getInstance().isRegistered()) {
                            CallerIDController.getInstance().showLoadingAnimation();
                            int result = ServerConnectionController.getInstance().retrieveCallLog();
                            CallerIDController.getInstance().hideLoadingAnimation();
                            if (result == -1) {
                                CallerIDController.getInstance().showErrorPopup(Rs.ERROR_CODE_003);
                            }

                        } else if (!Environment.EMULATOR) {
                            String msg = CallerIDController.getInstance().getErrorMsg(Rs.ERROR_CODE_505);
                            if (Log.ERROR_ON) {
                                Log.printError(Rs.ERROR_CODE_505 + ":" + msg);
                            }
                        }
                        showCallLog();
                    }
                } .start();
            }
        }
    }

    /**
     * Shows call log.
     */
    public void showCallLog() {
        if (Log.DEBUG_ON) {
            Log.printDebug("showCallLog");
        }
        callList = DataManager.getInstance().getCallList();
        if (callList != null) {
            if (callList.length == 1) {
                isTabFocus = false;
                focus = 0;
            }
            curPage = 0;
            totalPage = (callList[0].length - 1) / CallLogRenderer.MAX_LIST_NUM + 1;
            if (Log.DEBUG_ON) {
                Log.printDebug("showCallLog callList len " + callList.length + " focus " + focus);
            }
        } else {
            callList = null;
            optionFocus = 1;
            isTabFocus = false;
            noCall = true;
            if (Log.DEBUG_ON) {
                Log.printDebug("showCallLog callList null");
            }
        }
        repaint();
    }

    /**
     * Gets call list.
     * @return call list
     */
    public Call[][] getCallList() {
        return callList;
    }

    /**
     * Handles key event.
     * @param code key code
     * @return if return true is lower other application can't receive the key.
     */
    public boolean handleKey(int code) {
        boolean used = false;
        if (confirmPop != null) {
            used = confirmPop.handleKey(code);
            return used;
        }
        switch (code) {
        case HRcEvent.VK_PAGE_DOWN:
            if (callList == null) {
                return true;
            }
            if (curPage < totalPage - 1) {
                curPage++;
            }
            footer.clickAnimation(0);
            used = true;
            repaint();
            break;
        case HRcEvent.VK_PAGE_UP:
            if (callList == null) {
                return true;
            }
            if (curPage > 0) {
                curPage--;
            }
            footer.clickAnimation(0);
            used = true;
            repaint();
            break;
        case Rs.KEY_DOWN:
            if (callList == null) {
                return true;
            }
            if (!isTabFocus) {
                optionFocus = (optionFocus + 1) % 2;
            }
            used = true;
            repaint();
            break;
        case Rs.KEY_UP:
            if (callList == null) {
                return true;
            }
            if (!isTabFocus) {
                optionFocus = (optionFocus + 1) % 2;
            }
            used = true;
            repaint();
            break;
        case Rs.KEY_OK:
            used = true;
            if (!isTabFocus) {
                clickEffect.start(628, optionFocus * 38 + 388, 284, 43);
                if (optionFocus == 0) {
                    // delete all log
                    if (confirmPop == null) {
                        confirmPop = new Popup();
                    }
                    confirmPop.start(
                            this,
                            DataCenter.getInstance().getString("Label.lblConfirmDelete"),
                            TextUtil.split(DataCenter.getInstance().getString("Label.lblDeleteDesc"),
                                    FontResource.getFontMetrics(Rs.F18), 200));
                    CallerIDController.getInstance().openPopup(confirmPop);
                } else {
                    if (Definitions.OPTION_VALUE_ON.equals(DataCenter.getInstance().getString(
                            RightFilter.PARENTAL_CONTROL))
                            && Definitions.OPTION_VALUE_YES.equals(DataCenter.getInstance().getString(
                                    RightFilter.PROTECT_SETTINGS_MODIFICATIONS))) {
                        // check right filter
                        PreferenceProxy.getInstance().checkRightFilter();
                        return true;
                    }
                    CallerIDController.getInstance().startUnboundApplication("SETTINGS",
                            new String[]{App.APP_NAME, PreferenceService.CALLER_ID});
                }
            } else if (!isTabFocus) {
                optionFocus = 0;
                repaint();
            }

            break;
        case HRcEvent.VK_RIGHT:
            if (isTabFocus && callList != null && callList.length > 1) {
                if (focus < callList.length - 1) {
                    focus++;
                    curPage = 0;
                    totalPage = (callList[focus].length - 1) / CallLogRenderer.MAX_LIST_NUM + 1;
                } else {
                    isTabFocus = false;
                }
            }
            repaint();
            used = true;
            break;
        case HRcEvent.VK_LEFT:
            if (callList == null) {
                return true;
            }
            if (isTabFocus && callList.length > 1) {
                if (focus > 0) {
                    focus--;
                    curPage = 0;
                    totalPage = (callList[focus].length - 1)/ CallLogRenderer.MAX_LIST_NUM + 1;
                }
            } else if (!isTabFocus && callList.length > 1) {
                isTabFocus = true;
            }
            repaint();
            used = true;
            break;
        case KeyCodes.LAST:
            used = true;
            break;
        case OCRcEvent.VK_EXIT:
            footer.clickAnimation(1);
            CallerIDController.getInstance().exit();
            used = true;
            break;
        default:
            break;
        }
        return used;
    }

    /**
     * Gets focus of option part.
     * @return option index
     */
    public int getOptionFocus() {
        return optionFocus;
    }

    /**
     * Returns true if the focus is on the tab.
     * @return true if the focus is on the tab.
     */
    public boolean isTabFocus() {
        return isTabFocus;
    }

    public int getCurPage() {
        return curPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public boolean isNoCall() {
        return noCall;
    }

    /**
     * When popup closed, this is called.
     * @param src component
     * @param param parameter
     */
    public void popClosed(UIComponent src, Object param) {
        if (src.equals(confirmPop)) {
            if (param != null) {
                new Thread() {
                    public void run() {
                        CallerIDController.getInstance().deleteAllLog();
                    }
                }.start();
            }
            confirmPop = null;
        }
    }

    public void clockUpdated(Date date) {
        repaint();
    }

}

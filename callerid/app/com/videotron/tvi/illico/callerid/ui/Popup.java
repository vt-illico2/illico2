package com.videotron.tvi.illico.callerid.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.callerid.Rs;
import com.videotron.tvi.illico.callerid.controller.CallerIDController;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
/**
 * Popup.
 * @author Jinjoo Ok
 *
 */
public class Popup extends UIComponent {
    /** serialVersionUID for serialization. */
    private static final long serialVersionUID = 2877161940694298009L;
    /** PopupListener. */
    private PopupListener listener;
    /** Title.*/
    private String title;
    /** Popup contents. */
    private String[] contents;
    /** Effect. */
    private ClickingEffect clickEffect;
    /** Constructor. */
    public Popup() {
        setRenderer(new RendererPopup());
        clickEffect = new ClickingEffect(this, 5);
    }
    /**
     * Starts Popup.
     * @param ui CallLog
     * @param t title
     * @param con contents
     */
    public void start(CallLog ui, String t, String[] con) {
        this.title = t;
        listener = ui;
        contents = con;
        focus = 0;
        prepare();
        setVisible(true);
        repaint();
    }
    /**
     * Stops this popup.
     * @param confirm confirmation
     */
    public void stop(boolean confirm) {
        if (confirm) {
            listener.popClosed(this, new Boolean(confirm));
        } else {
            listener.popClosed(this, null);
        }
        this.setVisible(false);
        ((RendererPopup) renderer).releaseImage();
        title = null;
        contents = null;
        CallerIDController.getInstance().closePopup(this);
    }
    /**
     * Handles key event.
     * @param code key code
     * @return If key have to be used other application, return false or true
     */
    public boolean handleKey(int code) {
        if (code == HRcEvent.VK_LEFT || code == HRcEvent.VK_RIGHT) {
            focus = (focus + 1) % 2;
            repaint();
        } else if (code == Rs.KEY_OK) {
            clickEffect.start(321 + focus * 164, 353, 156, 33);
            if (focus == 0) {
                stop(true);
            } else {
                stop(false);
            }
        } else if (code == Rs.KEY_EXIT) {
            stop(false);
        }
        return true;
    }
    /**
     * Renderer for Popup.
     */
    class RendererPopup extends Renderer {
        /** Font. */
        private Font f18 = FontResource.BLENDER.getFont(18);
        /** Font. */
        private Font f24 = FontResource.BLENDER.getFont(24);
        /** Full screen background color. */
        private Color cBg = new DVBColor(12, 12, 12, 204);
        /** Popup background color. */
        private Color cPop = new Color(35, 35, 35);
        /** Button color. */
        private Color cButton = new Color(3, 3, 3);
        /** Title color. */
        private Color cTitle = new Color(252, 202, 4);
        /** Background image. */
//        private Image iBg;
        /** Gap image. */
        private Image iGap;
        /** Shadow image. */
//        private Image iShadow;
        /** Highlight image. */
//        private Image iHigh;
        /** Dimmed button image. */
        private Image iButtonDim;
        /** Button image. */
        private Image iButton;

        public void paint(Graphics g, UIComponent c) {
            g.setColor(cBg);
            g.fillRect(0, 0, 960, 540);
//            g.drawImage(iBg, 250, 113, c);
//            g.drawImage(iShadow, 280, 399, 402, 78, c);
            g.setColor(cPop);
            g.fillRect(280, 143, 402, 259);
//            g.drawImage(iHigh, 280, 143, c);
            g.drawImage(iGap, 292, 181, c);
            g.setFont(f24);
            g.setColor(cTitle);
            GraphicUtil.drawStringCenter(g, title, 482, 169);
            g.setFont(f18);
            g.setColor(Color.white);
            int start = 270 - contents.length / 2 * 20;
            for (int i = 0; i < contents.length; i++) {
                GraphicUtil.drawStringCenter(g, contents[i], 483, start + i * 20);
            }
            int focus = c.getFocus();
            if (focus == 0) {
                g.drawImage(iButton, 321, 353, c);
                g.drawImage(iButtonDim, 485, 353, c);
            } else {
                g.drawImage(iButtonDim, 321, 353, c);
                g.drawImage(iButton, 485, 353, c);
            }
            g.setColor(cButton);
            GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblYes"), 399, 374);
            GraphicUtil.drawStringCenter(g, DataCenter.getInstance().getString("Label.lblNo"), 564, 374);

        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void prepare(UIComponent c) {
//            iBg = DataCenter.getInstance().getImage("pop_d_glow_461.png");
            iGap = DataCenter.getInstance().getImage("pop_gap_379.png");
//            iShadow = DataCenter.getInstance().getImage("pop_sha_wide.png");
//            iHigh = DataCenter.getInstance().getImage("pop_high_402.png");
            iButtonDim = DataCenter.getInstance().getImage("05_focus_dim.png");
            iButton = DataCenter.getInstance().getImage("05_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public void releaseImage() {
//            DataCenter.getInstance().removeImage("pop_d_glow_461.png");
            DataCenter.getInstance().removeImage("pop_gap_379.png");
//            DataCenter.getInstance().removeImage("pop_high_402.png");
            DataCenter.getInstance().removeImage("05_focus_dim.png");
//            DataCenter.getInstance().removeImage("pop_sha_wide.png");
            DataCenter.getInstance().removeImage("05_focus.png");
        }
    }

}

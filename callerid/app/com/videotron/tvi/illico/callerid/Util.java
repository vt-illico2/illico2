package com.videotron.tvi.illico.callerid;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Utility class.
 * @author Jinjoo Ok
 *
 */
public final class Util {
    /** Constructor. */
    private Util() {

    }
    /**
     * Gets date string.
     * @param d Date
     * @return date string
     */
    public static long getDateString(Date d) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return Long.parseLong(sdf.format(d));
    }

    /**
     * Get millisecond time.
     * @param date yyyyMMdd
     * @param add the amount of date to be added to the field
     * @return date string yyyyMMdd.
     */
    public static long getDatString(String date, int add) {
        Calendar cal = Calendar.getInstance();
        if (date != null) {
            cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
            cal.set(Calendar.MONTH, Integer.parseInt(date.substring(4, 6)) - 1);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6, 8)));
            cal.add(Calendar.DAY_OF_MONTH, add);
        }
        return getDateString(cal.getTime());
    }
    /**
     * Returns a String from the byte array.
     * @param data byte array
     * @return String
     */
    public static String getStringBYTEArray(byte[] data) {
        if (data == null) {
            return "";
        }
        return getStringBYTEArrayOffset(data, 0);
    }

    /**
     * Returns a String from the byte array.
     * @param data byte array
     * @param offset offset
     * @return String
     */
    public static String getStringBYTEArrayOffset(byte[] data, int offset) {
        if (data == null) {
            return "";
        }

        StringBuffer buff = new StringBuffer(4096);
        for (int i = offset; i < offset + data.length; i++) {
            buff.append(getHexStringBYTE(data[i]));
            if (i != data.length - 1) {
                buff.append("");
            }
        }
        return buff.toString();
    }

    /**
     * Returns a String from the byte array.
     * @param b byte
     * @return String
     */
    public static String getHexStringBYTE(byte b) {
        String str = new String();

        // handle the case of -1
        if (b == -1) {
            str = "FF";
            return str;
        }

        int value = (((int) b) & 0xFF);

        if (value < 16) {
            // pad out string to make it look nice
            str = "0";
        }
        str += (Integer.toHexString(value)).toUpperCase();
        return str;
    }

    /**
     * changes string to hexadecimal string.
     * @param inputText normal string
     * @return hexa string
     */
    public static String stringToHexString(String inputText) {
        StringBuffer outputText = new StringBuffer();
        if (inputText == null || inputText.length() == 0) {
            return "";
        }

        byte[] byteArray = inputText.getBytes();
        for (int i = 0; i < byteArray.length; i++) {
            int byteData = ((int) byteArray[i]) & 0xFF;
            String addedText = Integer.toHexString(byteData);
            while (addedText.length() < 2) {
                addedText = "0" + addedText;
            }
            outputText.append(addedText);
        }
        return outputText.toString();
    }

    /**
     * Changes one byte to integer.
     * @param data byte array
     * @param offset offset
     * @return integer
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]) & 0xFF);
    }

    /**
     * Changes two bytes to integer.
     * @param data byte array
     * @param offset offset
     * @return integer
     */
    public static int twoBytesToInt(byte[] data, int offset) {
        return (((((int) data[offset]) & 0xFF) << 8) + (((int) data[offset + 1]) & 0xFF));
    }

    public static int fourBytesToInt(byte[] data, int offset) {
        int total = ((((int) data[offset]) & 0xFF) << 32) + ((((int) data[offset + 1]) & 0xFF) << 16);
        total += ((((int) data[offset + 2]) & 0xFF) << 8) + (((int) data[offset + 3]) & 0xFF);
        return total;
    }

    public static byte[] hexStringToByteArray(String input) {
        if (input.length() % 2 != 0) {
            return null;
        }
        byte[] result = new byte[input.length() / 2];
        for (int i = 0; i < input.length(); i += 2) {
            result[i / 2] = (byte) Integer.parseInt(input.substring(i, i + 2), 16);
        }
        return result;
    }
}

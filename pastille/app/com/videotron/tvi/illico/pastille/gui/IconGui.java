/**
 * @(#)IconGui.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.data.ConfigManager;
import com.videotron.tvi.illico.pastille.data.PastilleData;
import com.videotron.tvi.illico.pastille.ixc.IXCManager;
import com.videotron.tvi.illico.pastille.ui.IconUI;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * IconGui Graphics.
 * @author pellos
 */
public class IconGui extends Renderer {
    /** Renderer size. */
    private Rectangle bounds = new Rectangle(0, 0, 960, 540);

    /** pastille Data. */
    private PastilleData pastille;

    /** icon Image. */
    private Image iconImg;

    private Image bg1;
    private Image bg2;
    private Image btnOk;
    private Image btnExit;

    /** image X-coordinate. */
    private int imageX;
    /** image Y-coordinate. */
    private int imageY;

    /** event name. */
    private String eventName;

    /** image bilnd. */
    private boolean imageBlind;
    private String corner = null;

    private String title = "";
    private String subTitle = "";
    private String ok = "";
    private String exit = "";

    public static final Color C_241_241_241 = new Color(241, 241, 241);
    public static final Color C_255_255_255 = new Color(255, 255, 255);
    public static final Color C_255_203_0 = new Color(255, 203, 0);
    public static final Font BLENDER17 = FontResource.BLENDER.getFont(17);
    public static final Font BLENDER19 = FontResource.BLENDER.getFont(19);
    public static final Font BLENDER24 = FontResource.BLENDER.getFont(24);
    String imgName = null;
    /**
     * Constructor.
     */
    public IconGui() {

    }

    /**
     * This will be called before the related UIComponent is shown.
     * @param c UIComponent to be painted by this renderer.
     * @see UIComponent#prepare
     */
    public void prepare(UIComponent c) {
        stop();
        IconUI iconUi = (IconUI) c;

        pastille = iconUi.getPastilleData();
        eventName = null;
        if (pastille == null) {
            Log.printDebug("pastille = " + pastille);
            return;
        }
        eventName = pastille.getName();
        Log.printDebug("imageX = " + imageX + "imageY = " + imageY);

        corner = pastille.getCorner();

        if (corner == null) {
            imageX = pastille.getXCoordinate();
            imageY = pastille.getYCoordinate();
        } else if (corner.equals("topLeft")) {
            imageX = 41;
            imageY = 30;
        } else if (corner.equals("topRight")) {
            imageX = 482;
            imageY = 30;
        } else if (corner.equals("botLeft")) {
            imageX = 41;
            imageY = 392;
        } else if (corner.equals("botRight")) {
            imageX = 482;
            imageY = 392;
        }
        Log.printDebug("imageX = " + imageX + " imageY = " + imageY);
        String language = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
        Log.printDebug("language = " + language);

        if (language == null || language.equals(Definitions.LANGUAGE_FRENCH)) {
            title = pastille.getTitle(0);
            subTitle = pastille.getSubTitle(0);
            ok = pastille.getOkStatus(0);
        } else {
            title = pastille.getTitle(1);
            subTitle = pastille.getSubTitle(1);
            ok = pastille.getOkStatus(1);
        }
      
        if (MainManager.getInstance().imageTable != null) {
//            btnOk = (Image) MainManager.getInstance().imageTable.get(PreferenceService.BTN_OK);
            btnOk = DataCenter.getInstance().getImage("b_btn_ok.png");
            btnExit = (Image) MainManager.getInstance().imageTable.get(PreferenceService.BTN_EXIT);
        } else {
            btnOk = DataCenter.getInstance().getImage("b_btn_ok.png");
            btnExit = DataCenter.getInstance().getImage("b_btn_exit.png");
        }

        exit = DataCenter.getInstance().getString("pastille.exit");
        bg1 = DataCenter.getInstance().getImage("pastille_bg.png");
        bg2 = DataCenter.getInstance().getImage("pastille_bg_l.png");

        String fullURL = pastille.getPastilleType();
        Log.printDebug("fullURL = " + fullURL);
        int index2 = fullURL.indexOf("fr:");
        int index3 = fullURL.indexOf("en:");
        String frImageName = null;
        String enImageName = null;

        if (index3 > index2) {
            frImageName = fullURL.substring(index2 + 3, index3 - 1);
            enImageName = fullURL.substring(index3 + 3, fullURL.length());
        } else {
            enImageName = fullURL.substring(index3 + 3, index2 - 1);
            frImageName = fullURL.substring(index2 + 3, fullURL.length());
        }
        Log.printDebug("frImageName = " + frImageName);
        Log.printDebug("enImageName = " + enImageName);

        if (language == null || language.equals(Definitions.LANGUAGE_FRENCH)) {
            imgName = frImageName;
        } else {
            imgName = enImageName;
        }

        Log.printDebug("imgName = " + imgName);
        iconImg = FrameworkMain.getInstance().getImagePool().getImage(imgName);
        Log.printDebug("iconImage = " + iconImg);
        if (iconImg == null) {
            try {
//                byte[] imgByte = URLRequestor.getBytes(imgName, null);
                byte[] imgByte = ConfigManager.getInstance().getImageByte(imgName);
                Log.printDebug("imgByte = " + imgByte);
                if(imgByte != null) {
                    Log.printDebug("createImage");
                    iconImg = FrameworkMain.getInstance().getImagePool().createImage(imgByte, imgName);
                    Log.printDebug("iconImg " + iconImg);
                } else {
                    iconImg = DataCenter.getInstance().getImage("pastille_img.png");
                }
            } catch (Exception e) {
                IXCManager.getInstance().showErrorMessage("PST500");
                Log.print(e);
                iconImg = DataCenter.getInstance().getImage("pastille_img.png");
            }
        }



        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * Icon is Stop.
     */
    public void stop() {
        setImageBlind(false);
        if (iconImg != null) {
            DataCenter.getInstance().removeImage(imgName);
            iconImg = null;
        }
        eventName = null;
    }

    /**
     * Icon is blind.
     * @param blind is boolean.
     */
    public void setImageBlind(boolean blind) {
        imageBlind = blind;
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return bounds;
    }

    /**
     * Paints the Container.
     * @param g The graphics context to use for painting.
     */
    public void paint(Graphics g, UIComponent c) {

        Log.printDebug("IconRenderer " + imageBlind);

        if (imageBlind && iconImg == null) {
            return;
        }

        if(corner != null){
            if (corner.equals("topRight") || corner.equals("botRight")) {
                g.drawImage(bg1, imageX + 29, imageY - 14, c);
            } else {
                g.drawImage(bg2, imageX + 29, imageY - 14, c);
            }
        }

        g.setFont(BLENDER24);
        g.setColor(C_255_255_255);
        String shortenText = TextUtil.shorten(title, g.getFontMetrics(), 320);
        g.drawString(shortenText, imageX + 117, imageY + 50);

        g.setFont(BLENDER19);
        g.setColor(C_255_203_0);
        String shortenSubTitle = TextUtil.shorten(subTitle, g.getFontMetrics(), 320);
        g.drawString(shortenSubTitle, imageX + 117, imageY + 74);

        if (btnOk != null) {
            g.drawImage(btnOk, imageX + 111, imageY + 95, c);
            int okw = btnOk.getWidth(c);
            g.setFont(BLENDER17);
            g.setColor(C_241_241_241);
            
            String shortenOk = TextUtil.shorten(ok, g.getFontMetrics(), 200);
            g.drawString(shortenOk, imageX + 111 + okw + 5, imageY + 110);
        }
        if (btnExit != null) {
            g.drawImage(btnExit, imageX + 356, imageY + 95, c);
            int exitw = btnExit.getWidth(c);
            g.setFont(BLENDER17);
            g.setColor(C_241_241_241);
            String shortenExit = TextUtil.shorten(exit, g.getFontMetrics(), 45);
            g.drawString(shortenExit, imageX + 356 + exitw + 5, imageY + 110);
        }

        g.drawImage(iconImg, imageX, imageY, c);
    }
}

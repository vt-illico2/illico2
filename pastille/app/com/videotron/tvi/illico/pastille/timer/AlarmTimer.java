/**
 * @(#)AlarmTimer.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.timer;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.timer.AlarmTimer;

import java.util.Calendar;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

/**
 * This class is that If day passes, inputs event with current channel into queue.
 * @author pellos
 */
public final class AlarmTimer implements TVTimerWentOffListener {

    /** Instance of TVTimerSpec. */
    private TVTimerSpec timer;

    /** Instance of AlarmTimer. */
    private static AlarmTimer alarmTimer;

    /**
     * get instance of AlarmTimer.
     * @return alarmTimer
     */
    public static AlarmTimer getInstance() {
        if (alarmTimer == null) {
            alarmTimer = new AlarmTimer();
        }

        return alarmTimer;
    }

    /**
     * Construct. set TVTimerSpec.
     */
    private AlarmTimer() {
        timer = new TVTimerSpec();
        timer.setRepeat(false);
        timer.setAbsolute(true);
        timer.addTVTimerWentOffListener(this);
    }

    /**
     * start TVTimer.
     */
    public void startTimer() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.add(Calendar.DAY_OF_MONTH, 1);

        timer.setAbsoluteTime(calendar.getTimeInMillis());

        if (Log.DEBUG_ON) {
            Log.printDebug(calendar);
        }

        stopTimer();

        try {
            timer = TVTimer.getTimer().scheduleTimerSpec(timer);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * stop TVTimer.
     */
    public void stopTimer() {
        TVTimer.getTimer().deschedule(timer);
    }

    /**
     * Is Listener. If event is received, puts event with channel into queue.
     * @param e TVTimerWentOffEvent
     */
    public void timerWentOff(TVTimerWentOffEvent e) {
        Log.printDebug("AlarmTimer = " + Calendar.getInstance());
        MainManager.getInstance().putEvent(MainManager.getCurrentChannelName());
        startTimer();
    }
}

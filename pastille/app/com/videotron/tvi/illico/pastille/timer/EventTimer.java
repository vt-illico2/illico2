/**
 * @(#)EventTimer.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.timer;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.data.ConvertUtil;
import com.videotron.tvi.illico.pastille.timer.EventTimer;

import java.util.Calendar;
import java.util.Date;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

/**
 * This class notify start of event on event time.
 * @author pellos
 */
public final class EventTimer implements TVTimerWentOffListener {

    /** Instance of AlarmTimer. */
    private static EventTimer eventTimer;

    /** Instance of TVTimerSpec. */
    private TVTimerSpec timer;

    /**
     * get instance of EventTimer.
     * @return eventTimer
     */
    public static EventTimer getInstance() {
        if (eventTimer == null) {
            eventTimer = new EventTimer();
        }

        return eventTimer;
    }

    /**
     * Construct. set TVTimerSpec.
     */
    private EventTimer() {
        timer = new TVTimerSpec();
        timer.setRepeat(false);
        timer.setAbsolute(true);
        timer.addTVTimerWentOffListener(this);
    }

    /**
     * start TVTimer.
     * @param time millisecond
     */
    public void startTimer(long time) {
        Calendar calendar = ConvertUtil.changeSecondToTime(time);
        timer.setAbsoluteTime(calendar.getTimeInMillis());
        stopTimer();
        try {
            timer = TVTimer.getTimer().scheduleTimerSpec(timer);
        } catch (Exception e) {
            Log.print(e);
        }
        Log.printDebug("startTimer = " + new Date(calendar.getTimeInMillis()));
    }

    /**
     * stop TVTimer.
     */
    public void stopTimer() {
        TVTimer.getTimer().deschedule(timer);
    }

    /**
     * Is Listener. If event is received, starts Pastille Event.
     * @param e TVTimerWentOffEvent
     */
    public void timerWentOff(TVTimerWentOffEvent e) {
        Log.printDebug("timerWentOff " + System.currentTimeMillis());
        MainManager.getInstance().startIcon();
    }
}

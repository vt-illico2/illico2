/**
 * @(#)MonitorServiceListener.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.ixc;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;

import java.rmi.RemoteException;

/**
 * This class is Listener that send to Monitor IXC.
 * @author pellos
 */
public class MonitorServiceListener implements ServiceStateListener {

    /** Instance of MonitorService. */
    private MonitorService monitorService;

    /** before TVstate. */
    private static int beforeTvState;

    /**
     * Constructor.
     * @param service is MonitorService interface.
     */
    public MonitorServiceListener(final MonitorService service) {
        monitorService = service;
    }

    /**
     * receive tvState.
     * @param newTVState is tv state.
     * @throws RemoteException the exception.
     */
    public void stateChanged(int newTVState) throws RemoteException {
        Log.printInfo("newTVState = " + newTVState);
        MainManager.setTvState(newTVState);
        MainManager.getInstance().setIconState(MainManager.ICON_DEACTIVE);

        if (newTVState == 0) {
            if (beforeTvState != newTVState) {
                MainManager.getInstance().putEvent(MainManager.getCurrentChannelName());
            }
        } else {
            MainManager.getInstance().resetData();
        }

        beforeTvState = newTVState;
    }

}

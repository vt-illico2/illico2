/**
 * @(#)IXCManager.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.ixc;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.App;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * This class connects another application by IXC communication.
 * @author pellos
 */
public final class IXCManager implements STBModeChangeListener, DataUpdateListener {

    /** Instance of IXCManager. */
    private static IXCManager ixcManager;

    /** Instance of XletContext. */
    private XletContext xletContext;

    /** Instance of EpgService. */
    private EpgService epgService;

    /** EpgServiceListener of EpgService. */
    private EpgServiceListener epgServiceListener;

    /** Instance of MonitorService. */
    private MonitorService monitorService;

    private StcService stcService;
    private ErrorMessageService errorMgsService;
    /** MonitorServiceListener of MonitorService. */
    private MonitorServiceListener monitorServiceListener;

    private IxcLookupWorker ixcWorker;

    /** STB mode. */
    public static int stbMode = MonitorService.STB_MODE_NORMAL;

    /**
     * get instance of IXCManager.
     * @return IXCManager
     */
    public static IXCManager getInstance() {
        if (ixcManager == null) {
            ixcManager = new IXCManager();
        }
        return ixcManager;
    }

    /**
     * Constructor.
     */
    private IXCManager() {
    }

    /**
     * In the case this class starts, calls init method.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
        xletContext = xlet;
        ixcWorker = new IxcLookupWorker(xlet);
        PreferenceProxy.getInstance().init(xletContext);

        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, this);
        ixcWorker.lookup("/1/2026/", EpgService.IXC_NAME, this);
        ixcWorker.lookup("/1/2100/", StcService.IXC_NAME, this);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, this);
        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, this);
    }

    /**
     * Called when data is removed at DataCenter.
     * @param key data's key
     */
    public void dataRemoved(String key) {
    }

    /**
     * Called when data has been updated.
     * @param name key of data.
     * @param old old value of data.
     * @param remote new value of data.
     */
    public void dataUpdated(String name, Object old, Object remote) {
        if (name.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) remote;
            try {
                monitorServiceListener = new MonitorServiceListener(monitorService);
                monitorService.addStateListener(monitorServiceListener, "Pastille");
                monitorService.addSTBModeChangeListener(ixcManager, App.APP_NAME);
                stbMode = monitorService.getSTBMode();
            } catch (Exception e) {
                Log.print(e);
            }

        } else if (name.equals(EpgService.IXC_NAME)) {
            epgService = (EpgService) remote;
            try {
                epgServiceListener = new EpgServiceListener(epgService);
                epgService.getChannelContext(0).addChannelEventListener(epgServiceListener);
            } catch (Exception e) {
                Log.print(e);
            }

        } else if (name.equals(StcService.IXC_NAME)) {
            stcService = (StcService) remote;
            try {
                int currentLevel = stcService.registerApp(App.APP_NAME);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);

                Object o = SharedMemory.getInstance().get("stc-" + App.APP_NAME);
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    stcService.addLogLevelChangeListener(App.APP_NAME, new LogLevelAdapter());
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            } catch (Exception e) {
                Log.print(e);
            }

        } else if (name.equals(ErrorMessageService.IXC_NAME)) {
            errorMgsService = (ErrorMessageService) remote;

        } else if (name.equals(PreferenceService.IXC_NAME)) {
            PreferenceProxy.getInstance().setPreferenceService((PreferenceService) remote);
        }
    }

//
//    /**
//     * connect to EPGService.
//     */
//    public void lookupEpgService() {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("IXCManager.lookupEpgService: called");
//        }
//        Thread th = new Thread("PastilleEPGIXC") {
//            public void run() {
//                while (true) {
//                    try {
//                        String lookupName = "/1/2026/EpgService";
//                        epgService = (EpgService) IxcRegistry.lookup(xletContext, lookupName);
//                    } catch (Exception e) {
//                        // Log.print(e);
//                    }
//
//                    if (epgService != null) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("IXCManager.lookupEpgService: EpgService is looked up.");
//                        }
//
//                        try {
//                            epgServiceListener = new EpgServiceListener(epgService);
//                            epgService.getChannelContext(0).addChannelEventListener(epgServiceListener);
//                        } catch (Exception e) {
//                            Log.print(e);
//                        }
//                        break;
//                    }
//
//                    try {
//                        Thread.sleep(RETRY_LOOKUP_MILLIS);
//                    } catch (Exception e) {
//                        Log.print(e);
//                    }
//                }
//            }
//
//        };
//        th.start();
//    }
//
//    /**
//     * connect to MoniterService.
//     */
//    public void lookupMonitorService() {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("lookupMonitorService: called");
//        }
//
//        Thread th = new Thread("PastilleMoniterIXC") {
//            public void run() {
//                int count = 0;
//                while (true) {
//                    try {
//                        String lookupName = "/1/1/MonitorService";
//                        monitorService = (MonitorService) IxcRegistry.lookup(xletContext, lookupName);
//                    } catch (Exception e) {
//                        // Log.print(e);
//                    }
//
//                    if (monitorService != null) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("IXCManager.lookupmonitorService: monitorService is looked up.");
//                        }
//
//                        try {
//                            monitorServiceListener = new MonitorServiceListener(monitorService);
//                            monitorService.addStateListener(monitorServiceListener, "Pastille");
//                            monitorService.addSTBModeChangeListener(ixcManager, App.APP_NAME);
//                            stbMode = monitorService.getSTBMode();
//                            if (Log.INFO_ON) {
//                                Log.printInfo("monitorService Lookup completed");
//                            }
//                            break;
//                        } catch (Exception e) {
//                            Log.print(e);
//                        }
//                    }
//
//                    try {
//                        Thread.sleep(RETRY_LOOKUP_MILLIS);
//                    } catch (Exception e) {
//                        Log.print(e);
//                    }
//                    count++;
//                }
//            }
//        };
//        th.start();
//    }
//
//    public void lookupSTCService() {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("lookupSTCervice: called");
//        }
//
//        Thread th = new Thread("PastilleSTCIXC") {
//            public void run() {
//                int count = 0;
//                while (true) {
//                    try {
//                        String lookupName = "/1/2100/" + StcService.IXC_NAME;
//                        stcService = (StcService) IxcRegistry.lookup(xletContext, lookupName);
//                    } catch (Exception e) {
//                        // Log.print(e);
//                    }
//
//                    if (stcService != null) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("IXCManager.lookupStcService: stcService is looked up.");
//                        }
//
//                        try {
//                            int currentLevel = stcService.registerApp(App.APP_NAME);
//                            Log.printDebug("stc log level = " + currentLevel);
//                            Log.setStcLevel(currentLevel);
//
//                            Object o = SharedMemory.getInstance().get("stc-" + App.APP_NAME);
//                            if (o != null) {
//                                DataCenter.getInstance().put("LogCache", o);
//                            }
//                            try {
//                                stcService.addLogLevelChangeListener(App.APP_NAME, new LogLevelAdapter());
//                            } catch (RemoteException ex) {
//                                Log.print(ex);
//                            }
//
//                            break;
//                        } catch (Exception e) {
//                            Log.print(e);
//                        }
//                    }
//                    try {
//                        Thread.sleep(RETRY_LOOKUP_MILLIS);
//                    } catch (Exception e) {
//                        Log.print(e);
//                    }
//                    count++;
//                }
//            }
//
//        };
//        th.start();
//    }
//
//    public void lookupErrorMessageService() {
//        if (Log.DEBUG_ON) {
//            Log.printDebug("lookupErrorervice: called");
//        }
//
//        Thread th = new Thread("PastilleSTCIXC") {
//            public void run() {
//                int count = 0;
//                while (true) {
//                    try {
//                        String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
//                        errorMgsService = (ErrorMessageService) IxcRegistry.lookup(xletContext, lookupName);
//                    } catch (Exception e) {
//                        // Log.print(e);
//                    }
//
//                    if (errorMgsService != null) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("IXCManager.lookupErrorService: errorService is looked up.");
//                        }
//                        break;
//                    }
//
//                    try {
//                        Thread.sleep(RETRY_LOOKUP_MILLIS);
//                    } catch (Exception e) {
//                        Log.print(e);
//                    }
//                    count++;
//                }
//            }
//
//        };
//        th.start();
//    }

    /**
     * Start unbound Applicastion.
     * @param appName is name
     * @param parameters is parameter
     * @return result is boolean
     */
    public boolean unboundApplicastionStart(String appName, String[] parameters) {
        boolean result = false;
        Log.printDebug("unboundApplicastionStart " + appName);
        try {
            result = monitorService.startUnboundApplication(appName, parameters);
        } catch (RemoteException e) {
            showErrorMessage("PST501");
            Log.print(e);
        }
        return result;
    }

    public boolean getEidAtPackageNameEidMappingFile(String packageID) {
        boolean result = false;
        try {
            int i = monitorService.checkResourceAuthorization(packageID);
            Log.printDebug("isSubscribeService i " + i);

            if (i == MonitorService.AUTHORIZED) {
                return true;
            } else {
                return false;
            }
        } catch (RemoteException e) {
            Log.print(e);
        }
        return result;
    }

    /**
     * select Channel.
     * @param number channel number.
     */
    public void channelSelect(int number) {
        Log.printInfo("channel number = " + number);
        try {
            epgService.changeChannel(number);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * select Channel.
     * @param number channel name
     */
    public void channelSelect(String number) {
        Log.printInfo("channel number = " + number);
        try {
            TvChannel tv = epgService.getChannel(number);
            epgService.changeChannel(tv);
        } catch (RemoteException e) {
            showErrorMessage("PST502");
            Log.print(e);
        }
    }

    public void setCompleteReceivingData() {
        try {
            monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
        } catch (RemoteException e) {
        	// only log as per specs.
            //IXCManager.getInstance().showErrorMessage("SDM500");
            Log.print(e);
        }
    }

    public void showErrorMessage(String code) {
        if (errorMgsService != null) {
            try {
                errorMgsService.showErrorMessage(code, null);
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    public void modeChanged(int newMode) throws RemoteException {
        stbMode = newMode;
    }
}

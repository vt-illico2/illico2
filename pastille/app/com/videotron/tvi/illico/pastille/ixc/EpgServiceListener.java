/**
 * @(#)EpgServiceListener.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.ixc;

import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;

import java.rmi.RemoteException;

/**
 * This class is Listener that send to EPG IXC.
 * @author pellos
 */
public class EpgServiceListener implements ChannelEventListener {

    /** Instance of EpgService. */
    private EpgService epgService;

    /**
     * Constructor.
     * @param service is EpgService interface.
     */
    public EpgServiceListener(final EpgService service) {
        epgService = service;
    }

    /**
     * deosn't used.
     * @param id is channel source id.
     * @param type channel type.
     * @throws RemoteException is RemoteException
     */
    public void selectionBlocked(int id, short type) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("selectionBlocked: id = " + id + " type = " + type);
        }
    	
    	TvChannel tvChannel = epgService.getChannel(id);
        MainManager.getInstance().cancellationStateChange();
        MainManager.setCurrentChannelName(tvChannel.getCallLetter());
        if (Log.DEBUG_ON) {
            Log.printDebug("channel name = " + tvChannel.getCallLetter());
        }
    	MainManager.getInstance().resetData();
    }

    /**
     * If channel is changed, received event.
     * @param id is channel source id.
     * @param type channel type.
     * @throws RemoteException is RemoteException
     */
    public void selectionRequested(int id, short type) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo("selectionRequested: id = " + id + " type = " + type);
        }
        process(id);
    }

    private void process(int id) throws RemoteException {
        TvChannel tvChannel = epgService.getChannel(id);
        MainManager.getInstance().cancellationStateChange();
        MainManager.setCurrentChannelName(tvChannel.getCallLetter());
        if (Log.DEBUG_ON) {
            Log.printDebug("channel name = " + tvChannel.getCallLetter());
        }
        MainManager.getInstance().putEvent(MainManager.getCurrentChannelName());
    }
}

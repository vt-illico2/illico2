package com.videotron.tvi.illico.pastille.ixc;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values of Search Application.
 * @author
 */
public class PreferenceProxy implements PreferenceListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();
    /** XletContext. */
    private XletContext xletContext;
    /** PreferenceService instance. */
    private PreferenceService preferenceService;
    /** Key set. */
    public static String[] preferenceKeys;

    /**
     * Get instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }

    /**
     * Initialize.
     * @param xlet XletContext
     */
    public void init(XletContext xlet) {
        this.xletContext = xlet;
        preferenceKeys = new String[]{PreferenceNames.LANGUAGE};
    }

    public void setPreferenceService(PreferenceService ps) {
        this.preferenceService = ps;
        addPreferenceListener();
    }

    /**
     * register listener to UPP.
     */
    private void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        try {
            String[] values = preferenceService.addPreferenceListener(this, FrameworkMain.getInstance().getApplicationName(),
                    preferenceKeys, new String[]{""});
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
            String language = (String)DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
            Log.printDebug("language = " + language);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    /**
     * values are received update Listener from UPP.
     */
    public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
        Log.printDebug("preferenceName = " + preferenceName + " value = " + value);

        if (preferenceName.equals(PreferenceNames.LANGUAGE)) {
            DataCenter.getInstance().put(preferenceName, value);
        }
    }
    
    // add for VDTRMASTER-5552
    public boolean showPinEnabler(PinEnablerListener l, String[] msg) {
        if (preferenceService != null) {
            try {
            	preferenceService.showPinEnabler(l, msg);
                return true;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}

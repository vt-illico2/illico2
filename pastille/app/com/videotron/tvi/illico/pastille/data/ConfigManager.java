package com.videotron.tvi.illico.pastille.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;

import org.xml.sax.InputSource;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.data.ConfigManager;
import com.videotron.tvi.illico.pastille.data.XMLParser;
import com.videotron.tvi.illico.pastille.ixc.IXCManager;
import com.videotron.tvi.illico.util.Environment;

public class ConfigManager implements InbandDataListener, DataUpdateListener {

    private static final ConfigManager instance = new ConfigManager();
    public XMLParser xml;// = new XMLParser(1);
    private String xmlFileName = "OOBBAND";
    private String zipFileName = "OOBBANDIMG";

    private Hashtable imgHash;

    public static ConfigManager getInstance() {
        return instance;
    }

    private ConfigManager() {
    }

    int number = 0;
    public void init() {
    	if (Log.DEBUG_ON) {
            Log.printDebug("ConfigManager: init()");
        }
        if (!Environment.EMULATOR) {
            xml = new XMLParser();
        } else {
            File f = new File("resource/data/pastille_images.zip");
            imgHash = ZipFileReader.read(f);
            xml = new XMLParser(number++);
        }

        DataCenter.getInstance().addDataUpdateListener(zipFileName, this);
        DataCenter.getInstance().addDataUpdateListener(xmlFileName, this);
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigManager: init() end");
        }
    }
   
    public void receiveInbandData(String locator) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigManager: receiveInbandData = " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        IXCManager.getInstance().setCompleteReceivingData();
    }

    public void dataUpdated(String key, Object old, Object value) {
        Log.printInfo("key = " + key + " value = " + value);

        if (key != null) {
            if (xmlFileName.equals(key)) {
                try {
                    xml.setParserXml((File) value);
                } catch (Exception e) {
                	// only log as per specs
                    //IXCManager.getInstance().showErrorMessage("SDM502");
                    Log.printError("dataUpdated Exception");
                }
            } else if (zipFileName.equals(key)) {
                if (imgHash != null) {
                    imgHash.clear();
                    imgHash = null;
                }
                imgHash = ZipFileReader.read((File) value);
            }
        }
    }

    public void dataRemoved(String key) {
    }

    public byte[] getImageByte(String key) {
        Log.printDebug("getImageByte Key = " + key);

        if (imgHash != null) {
            Log.printDebug("getImageByte imgHash = " + imgHash.size());
            Object ob = imgHash.get(key);
            if(ob != null) {
                return (byte[]) ob;
            }
        }
        return null;
    }
}

/**
 * @(#)PastilleData.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.Rs;
import com.videotron.tvi.illico.pastille.data.ConvertUtil;
import com.videotron.tvi.illico.pastille.data.Period;

/**
 * This class is a PastilleData.
 * @author pellos
 */

public class PastilleData {

    public static final String PASTILLE_NAME = "pastille";

    /** Event name. */
    private String name;

    /** Channels of event. */
    private Vector channels;

    /** StartDate of event. */
    private Calendar startDate;

    /** EndDate of event. */
    private Calendar endDate;

    /** Default week. */
    private byte[] defaultWeek;

    /** Default interval time between period to period. */
    private String defaultInterval;

    /** Default duration of event. */
    private long defaultDuration;

    /** Default Cancel value. */
    private int defaultCancellationMethod;

    /** Package ID. */
    private Vector eid;

    /** Is a default period or selected period after channel change. */
    private boolean onChannelChange;

    /** In case default period, Icon viewing delay time after channel change. */
    private static int tuneDelay = 30;

    /** Is saved a periodData. */
    private Vector periodData;

    /** Relative coordinate. */
    private String corner;

    /** X-coordinate. */
    private int xCoordinate;
    /** Y-coordinate. */
    private int yCoordinate;

    /** Pastille Application ID. */
    private String pastilleApplicationID;

    /** Pastille type. */
    private String pastilleType;

    /** byte of image. */
    private byte[] imageByte;

    /** Event start time. */
    private long iconViewedTime;

    /** onSuspendCancellation value. */
    private int onSuspendCancellationMethod;
    /** onExitCancellation value. */
    private int onExitCancellationMethod;
    /** onSelectCancellation value. */
    private int onSelectCancellationMethod;
    /** onTimeoutCancellation value. */
    private int onTimeoutCancellationMethod;

    /** GroupID of period. */
    private String groupID;

    /** Feedback OnExit Hashtable. */
    private Hashtable feedbakcOnExitHash;
    /** Feedback OnTimeout Hashtable. */
    private Hashtable feedbakcOnTimeoutHash;
    /** Feedback OnSuspend Hashtable. */
    private Hashtable feedbakcOnSuspendHash;
    /** Feedback OnSelect Hashtable. */
    private Hashtable feedbakcOnSelectHash;

    /** Is saved a groupID. */
    private Hashtable groupsOfPeriod;

    /** Default end time. */
    private static final String END_TIME = "23:59:59";

    /** EVENT_TARGET_TYPE key. */
    public static final String EVENT_TARGET_TYPE = "eventTargetType";
    /** TARGET_LOCATOR key. */
    public static final String TARGET_LOCATOR = "targetLocator";
    /** TARGET_PAMAMETERS key. */
    public static final String TARGET_PAMAMETERS = "targetPamameters";

    /** ONEXIT value define. */
    public static final int ONEXIT = 0;
    /** ONTIMEOUT value define. */
    public static final int ONTIMEOUT = 1;
    /** ONSUSPEND value define. */
    public static final int ONSUSPEND = 2;
    /** ONSELECT value define. */
    public static final int ONSELECT = 3;

    /** TARGET_TYPE_APPLICATION value define. */
    public static final String TARGET_TYPE_APPLICATION = "Application";
    /** TARGET_TYPE_TUNER value define. */
    public static final String TARGET_TYPE_TUNER = "Tune";

    /** 0 array is the French, 1 array is the English. **/
    private String[] title = new String[2];
    private String[] subTitle = new String[2];
    private String[] okStatus = new String[2];

    /**
     * Constructor. Set default value.
     */
    public PastilleData() {
        channels = new Vector();
        startDate = Calendar.getInstance();
        endDate = Calendar.getInstance();
        defaultWeek = new byte[7];
        defaultInterval = "00:05:00";
        defaultDuration = 60L;
        defaultCancellationMethod = 0;
        onChannelChange = false;
        periodData = new Vector();
        eid = new Vector();
        onSuspendCancellationMethod = 0;
        onExitCancellationMethod = 0;
        onSelectCancellationMethod = 0;
        onTimeoutCancellationMethod = 0;
        feedbakcOnExitHash = new Hashtable();
        feedbakcOnTimeoutHash = new Hashtable();
        feedbakcOnSuspendHash = new Hashtable();
        feedbakcOnSelectHash = new Hashtable();
        groupsOfPeriod = new Hashtable();
        startDate.add(Calendar.DAY_OF_MONTH, -1);
        endDate.set(Calendar.YEAR, 9999);
    }

    /**
     * Get event Name.
     * @return name
     */
    public final String getName() {
        return name;
    }

    /**
     * Set event Name.
     * @param eventName is an eventName
     */
    public final void setName(String eventName) {
        name = eventName;
    }

    /**
     * Get Channels.
     * @return channels
     */
    public final Vector getChannels() {
        return channels;
    }

    /**
     * Save channels.
     * @param str is a channel.
     */
    public final void setChannels(String str) {
        String temp;
        StringTokenizer parser = new StringTokenizer(str, ",");

        while (parser.hasMoreTokens()) {
            temp = parser.nextToken();
            channels.add(temp.trim());
            //
            Rs.getChannelDatas().put(temp.trim(), getName());
            // Rs.getPastilleDatas().put(temp, getName());
        }
    }

    /**
     * Get start date.
     * @return startDate
     */
    public final Calendar getStartDate() {
        return startDate;
    }

    /**
     * Converts String to Calendar and sets end date.
     * @param paramDate is a date(YYYY:MM:DD)
     */
    public final void setStartDate(String paramDate) {
        int yy = Integer.parseInt(paramDate.substring(0, 4));
        int mm = Integer.parseInt(paramDate.substring(5, 7));
        int dd = Integer.parseInt(paramDate.substring(8, 10));
        startDate.set(Calendar.YEAR, yy);
        startDate.set(Calendar.MONTH, mm - 1);
        startDate.set(Calendar.DAY_OF_MONTH, dd);
        startDate.set(Calendar.HOUR_OF_DAY, 0);
        startDate.set(Calendar.MINUTE, 0);
        startDate.set(Calendar.SECOND, 0);
        setEndDate("9999-12-31");
    }

    /**
     * Set default Interval.
     * @param interval is a default interval.
     */
    public final void setDefaultInterval(String interval) {
        defaultInterval = interval;
    }

    /**
     * Get default Interval.
     * @return defaultInterval
     */
    public final String getDefaultInterval() {
        return defaultInterval;
    }

    /**
     * Get default Week.
     * @return defaultWeek.
     */
    public final byte[] getDefaultWeek() {
        return defaultWeek;
    }

    /**
     * Set default Week.
     * @param week 1bit is a day of week. Monday ~ Sunday(0000000)
     */
    public final void setDefaultWeek(String week) {
        for (int a = 0; a < week.length(); a++) {
            defaultWeek[a] = Byte.parseByte(week.substring(a, a + 1));
        }
    }

    /**
     * Get EID.
     * @return eid
     */
    public final Vector getEid() {
        return eid;
    }

    /**
     * Set EID. EID is a package name.
     * @param paramEid is a Package name.
     */
    public final void setEid(String paramEid) {
        String temp;
        StringTokenizer parser = new StringTokenizer(paramEid, ",");

        while (parser.hasMoreTokens()) {
            temp = parser.nextToken();
            eid.add(temp.trim());
        }
    }

    /**
     * is a default period or not.
     * @return onChannelChange
     */
    public final boolean isOnChannelChange() {
        return onChannelChange;
    }

    /**
     * Set OnChannelChange.
     * @param flag is a true or false.
     */
    public final void setOnChannelChange(String flag) {
        if (flag.equalsIgnoreCase("true")) {
            onChannelChange = true;
            setGroupID(getName());
            setAvailableGroupID(getGroupID(), "true");
        } else {
            onChannelChange = false;
        }
    }

    /**
     * Get tune delay time.
     * @return tuneDelay
     */
    public static int getTuneDelay() {
        return tuneDelay;
    }

    /**
     * Set tune delay.
     * @param delay is a delay time.
     */
    public static void setTuneDelay(String delay) {
        tuneDelay = Integer.parseInt(delay);
    }

    /**
     * Get end date.
     * @return endDate
     */
    public final Calendar getEndDate() {
        return endDate;
    }

    /**
     * Set end date.
     * @param date is String (YYYY:MM:DD)
     */
    public final void setEndDate(String date) {
        int yy = Integer.parseInt(date.substring(0, 4));
        int mm = Integer.parseInt(date.substring(5, 7));
        int dd = Integer.parseInt(date.substring(8, 10));
        endDate.set(Calendar.YEAR, yy);
        endDate.set(Calendar.MONTH, mm - 1);
        endDate.set(Calendar.DAY_OF_MONTH, dd);
        endDate.set(Calendar.HOUR_OF_DAY, 23);
        endDate.set(Calendar.MINUTE, 59);
        endDate.set(Calendar.SECOND, 59);
    }

    /**
     * Get default duration.
     * @return defaultDuration
     */
    public final long getDefaultDuration() {
        return defaultDuration;
    }

    /**
     * Set default duration.
     * @param str is a duration (HH:MM:SS)
     */
    public final void setDefaultDuration(String str) {
        defaultDuration = ConvertUtil.changeDateToSecond(str);
    }

    /**
     * set default duration.
     * @param duration is a millisecond.
     */
    public final void setDefaultDuration(long duration) {
        defaultDuration = duration;
    }

    /**
     * Get default cancellationMethod.
     * @return defaultCancellationMethod
     */
    public final int getDefaultCancellationMethod() {
        return defaultCancellationMethod;
    }

    /**
     * Set default cancellationMethod.
     * @param cancellationMethod is a cancellation
     */
    public final void setDefaultCancellationMethod(int cancellationMethod) {
        defaultCancellationMethod = cancellationMethod;
    }

    /**
     * Get group ID.
     * @return groupID
     */
    public final String getGroupID() {
        return groupID;
    }

    /**
     * Set group ID.
     * @param id is a group ID.
     */
    public final void setGroupID(String id) {
        groupID = id;
    }

    /**
     * Makes period from XML data.
     * @param id gropuId of period
     * @param start startTime of period
     * @param end endTime of period
     * @param interval interval of period
     * @param onSuspendCancellation onSuspendCancellationMethod of period
     * @param onExitCancellation onExitCancellationMethod of period
     * @param onSelectCancellation onSelectCancellationMethod of period
     * @param onTimeoutCancellation onTimeoutCancellationMethod of period
     * @param days days of period
     * @param duration duration of period
     */
    public final void setPeriod(String id, String start, String end, String interval, String onSuspendCancellation,
            String onExitCancellation, String onSelectCancellation, String onTimeoutCancellation, String days,
            String duration) {
        Log.printDebug("id = " + id + "start = " + start +"end = " + end +"interval = " + interval);
        Log.printDebug("days = " + days + "duration = " + duration);
        if (end == null) {
            end = END_TIME;
        }

        if (interval == null || interval.equals("00:00:00") ) {
            interval = getDefaultInterval();
        }

        int tempOnSuspendCancellationMethod = 0;
        if (onSuspendCancellation == null) {
            tempOnSuspendCancellationMethod = getDefaultCancellationMethod();
        } else {
            tempOnSuspendCancellationMethod = Integer.parseInt(onSuspendCancellation);
        }

        int tempOnExitCancellationMethod = 0;
        if (onExitCancellation == null) {
            tempOnExitCancellationMethod = getDefaultCancellationMethod();
        } else {
            tempOnExitCancellationMethod = Integer.parseInt(onExitCancellation);
        }

        int tempOnSelectCancellationMethod = 0;
        if (onSelectCancellation == null) {
            tempOnSelectCancellationMethod = getDefaultCancellationMethod();
        } else {
            tempOnSelectCancellationMethod = Integer.parseInt(onSelectCancellation);
        }

        int tempOnTimeoutCancellationMethod = 0;
        if (onTimeoutCancellation == null) {
            tempOnTimeoutCancellationMethod = getDefaultCancellationMethod();
        } else {
            tempOnTimeoutCancellationMethod = Integer.parseInt(onTimeoutCancellation);
        }

        byte[] tempDays = null;
        if (days == null) {
            tempDays = getDefaultWeek();
        } else {
            for (int a = 0; a < days.length(); a++) {
                defaultWeek[a] = Byte.parseByte(days.substring(a, a + 1));
            }
            tempDays = defaultWeek;
        }

        long tempDuration = 0L;
        if (duration == null) {
            tempDuration = getDefaultDuration();
        } else {
            tempDuration = ConvertUtil.changeDateToSecond(duration);
        }

        Period period = new Period(getName() + id, ConvertUtil.getTime(start), ConvertUtil.getTime(end), ConvertUtil
                .getTime(interval), tempOnSuspendCancellationMethod, tempOnExitCancellationMethod,
                tempOnSelectCancellationMethod, tempOnTimeoutCancellationMethod, tempDays, tempDuration);

        periodData.add(period);

        setAvailableGroupID(getName() + id, "true");
    }

    /**
     * Set available GroupID.
     * @param id is a groupID of period
     * @param value is a available true or false.
     */
    public final void setAvailableGroupID(String id, String value) {
        Log.printDebug("Group key = " + id + " value = " + value);
        groupsOfPeriod.put(id, value);
    }

    /**
     * Set all groups.
     * @param available is a available true or false.
     */
    public final void setAvailableAllGroup(String available) {
        Enumeration enumeration = groupsOfPeriod.keys();
        String key = null;

        while (enumeration.hasMoreElements()) {
            key = (String) enumeration.nextElement();
            groupsOfPeriod.put(key, available);
        }
    }

    /**
     * Get Available Group.
     * @param id is a group
     * @return boolean
     */
    public final boolean getAvailableGroup(String id) {
        String avail = (String) groupsOfPeriod.get(id);
        Log.printDebug("getAvailableGroup = " + avail);
        return avail != null && avail.equals("true");
    }

    /**
     * Get period date.
     * @return periodData is a vector
     */
    public final Vector getPeriodData() {
        return periodData;
    }

    /**
     * Get PastilleApplicationID.
     * @return pastilleApplicationID
     */
    public final String getPastilleApplicationID() {
        return pastilleApplicationID;
    }

    /**
     * Set PastilleApplicationID.
     * @param pastilleID is a PastilleApplicationID
     */
    public final void setPastilleApplicationID(String pastilleID) {
        pastilleApplicationID = pastilleID;
    }

    /**
     * Get corner. Relative position.
     * @return corner
     */
    public final String getCorner() {
        return corner;
    }

    /**
     * Set corner.
     * @param location is a relative position
     */
    public final void setCorner(String location) {
        corner = location;
    }

    /**
     * Get X-Coordinate.
     * @return xCoordinate
     */
    public final int getXCoordinate() {
        return xCoordinate;
    }

    /**
     * Set X-Coordinate.
     * @param location is an absolute coordinate
     */
    public final void setXCoordinate(int location) {
        xCoordinate = location;
    }

    /**
     * Get Y-Coordinate.
     * @return yCoordinate
     */
    public final int getYCoordinate() {
        return yCoordinate;
    }

    /**
     * Set Y-Coordinate.
     * @param location is an absolute coordinate.
     */
    public final void setYCoordinate(int location) {
        yCoordinate = location;
    }

    /**
     * Get PastilleType.
     * @return pastilleType
     */
    public final String getPastilleType() {
        return pastilleType;
    }

    /**
     * Set PastilleType.
     * @param type is a pastilleType.
     */
    public final void setPastilleType(String type) {
        pastilleType = type;
    }

    /**
     * Set FeedbakcOnExitHash.
     * @param key key
     * @param value value
     */
    public final void setFeedbakcOnExitHash(String key, String value) {
        feedbakcOnExitHash.put(key, value);
    }

    /**
     * Get FeedbakcOnExitHash.
     * @return feedbakcOnExitHash
     */
    public final Hashtable getFeedbakcOnExitHash() {
        return feedbakcOnExitHash;
    }

    /**
     * Set FeedbakcOnTimeoutHash.
     * @param key key
     * @param value value
     */
    public final void setFeedbakcOnTimeoutHash(String key, String value) {
        feedbakcOnTimeoutHash.put(key, value);
    }

    /**
     * Get FeedbakcOnTimeoutHash.
     * @return feedbakcOnTimeoutHash
     */
    public final Hashtable getFeedbakcOnTimeoutHash() {
        return feedbakcOnTimeoutHash;
    }

    /**
     * Set FeedbakcOnSuspendHash.
     * @param key key
     * @param value value
     */
    public final void setFeedbakcOnSuspendHash(String key, String value) {
        feedbakcOnSuspendHash.put(key, value);
    }

    /**
     * Get FeedbakcOnSuspendHash.
     * @return feedbakcOnSuspendHash
     */
    public final Hashtable getFeedbakcOnSuspendHash() {
        return feedbakcOnSuspendHash;
    }

    /**
     * Set FeedbakcOnSelectHash.
     * @param key key
     * @param value value
     */
    public final void setFeedbakcOnSelectHash(String key, String value) {
        feedbakcOnSelectHash.put(key, value);
    }

    /**
     * Get FeedbakcOnSelectHash.
     * @return feedbakcOnSelectHash
     */
    public final Hashtable getFeedbakcOnSelectHash() {
        return feedbakcOnSelectHash;
    }

    /**
     * Get imageByte.
     * @return imageByte
     */
    public final byte[] getImageByte() {
        return imageByte;
    }

    /**
     * Set imageByte.
     * @param imageData is received from HTTP.
     */
    public final void setImageByte(byte[] imageData) {
        imageByte = imageData;
    }

    /**
     * Get IconViewedTime.
     * @return iconViewedTime
     */
    public final long getIconViewedTime() {
        return iconViewedTime;
    }

    /**
     * Set IconViewedTime.
     * @param time is a start time of period of event
     */
    public final void setIconViewedTime(long time) {
        iconViewedTime = time;
    }

    /**
     * Get OnSuspendCancellationMethod.
     * @return onSuspendCancellationMethod
     */
    public final int getOnSuspendCancellationMethod() {
        return onSuspendCancellationMethod;
    }

    /**
     * Set OnSuspendCancellationMethod.
     * @param onSuspendCancellation is a value.
     */
    public final void setOnSuspendCancellationMethod(int onSuspendCancellation) {
        this.onSuspendCancellationMethod = onSuspendCancellation;
    }

    /**
     * Get OnExitCancellationMethod.
     * @return onExitCancellationMethod
     */
    public final int getOnExitCancellationMethod() {
        return onExitCancellationMethod;
    }

    /**
     * Set OnExitCancellationMethod.
     * @param onExitCancellation is a value
     */
    public final void setOnExitCancellationMethod(int onExitCancellation) {
        this.onExitCancellationMethod = onExitCancellation;
    }

    /**
     * Get OnSelectCancellationMethod.
     * @return onSelectCancellationMethod
     */
    public final int getOnSelectCancellationMethod() {
        return onSelectCancellationMethod;
    }

    /**
     * Set OnSelectCancellationMethod.
     * @param onSelectCancellation is a value.
     */
    public final void setOnSelectCancellationMethod(int onSelectCancellation) {
        this.onSelectCancellationMethod = onSelectCancellation;
    }

    /**
     * Get OnTimeoutCancellationMethod.
     * @return onTimeoutCancellationMethod
     */
    public final int getOnTimeoutCancellationMethod() {
        return onTimeoutCancellationMethod;
    }

    /**
     * Set OnTimeoutCancellationMethod.
     * @param onTimeoutCancellation is a value
     */
    public final void setOnTimeoutCancellationMethod(int onTimeoutCancellation) {
        this.onTimeoutCancellationMethod = onTimeoutCancellation;
    }

    public String getOkStatus(int num) {
        return okStatus[num];
    }

    public void setOkStatus(int type, String str) {
        this.okStatus[type] = str;
    }

    public String getTitle(int num) {
        return title[num];
    }

    public void setTitle(int type, String str) {
        this.title[type] = str;
    }


    public String getSubTitle(int num) {
        return subTitle[num];
    }

    public void setSubTitle(int type, String str) {
        this.subTitle[type] = str;
    }

    /**
     * toString.
     * @return String
     */
//    public String toString() {
//        return "PastilleData [channels=" + channels + ", corner=" + corner + ", defaultCancellationMethod="
//                + defaultCancellationMethod + ", defaultDuration=" + defaultDuration + ", defaultInterval="
//                + defaultInterval + ", defaultWeek="
//                + (defaultWeek != null ? arrayToString(defaultWeek, defaultWeek.length) : null) + ", eid=" + eid
//                + ", endDate=" + endDate + ", feedbakcOnExitHash=" + feedbakcOnExitHash + ", feedbakcOnSelectHash="
//                + feedbakcOnSelectHash + ", feedbakcOnSuspendHash=" + feedbakcOnSuspendHash
//                + ", feedbakcOnTimeoutHash=" + feedbakcOnTimeoutHash + ", groupID=" + groupID + ", groupsOfPeriod="
//                + groupsOfPeriod + ", iconViewedTime=" + iconViewedTime + ", imageByte="
//                + (imageByte != null ? arrayToString(imageByte, imageByte.length) : null) + ", name=" + name
//                + ", onChannelChange=" + onChannelChange + ", onExitCancellationMethod=" + onExitCancellationMethod
//                + ", onSelectCancellationMethod=" + onSelectCancellationMethod + ", onSuspendCancellationMethod="
//                + onSuspendCancellationMethod + ", onTimeoutCancellationMethod=" + onTimeoutCancellationMethod
//                + ", pastilleApplicationID=" + pastilleApplicationID + ", pastilleType=" + pastilleType
//                + ", periodData=" + periodData + ", startDate=" + startDate + ", xCoordinate=" + xCoordinate
//                + ", yCoordinate=" + yCoordinate + "]";
//    }

    /**
     * convert array to string.
     * @param array array
     * @param len length
     * @return string
     */
    private String arrayToString(Object array, int len) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        for (int i = 0; i < len; i++) {
            if (i > 0) {
                buffer.append(", ");
            }
            if (array instanceof byte[]) {
                buffer.append(((byte[]) array)[i]);
            }
        }
        buffer.append("]");
        return buffer.toString();
    }
}

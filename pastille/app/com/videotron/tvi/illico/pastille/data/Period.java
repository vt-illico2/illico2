/**
 * @(#)Period.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.util.Calendar;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;

/**
 * This class is a period data.
 *
 * @author pellos
 *
 */
public class Period {

    /** Strart time of period. */
    private Vector startTimes;

    /** Day of week. */
    private byte[] week;

    /** Value of onSuspendCancellation. */
    private int onSuspendCancellationMethod;
    /** Value of onExitCancellation. */
    private int onExitCancellationMethod;
    /** Value of onSelectCancellation. */
    private int onSelectCancellationMethod;
    /** Value of onTimeoutCancellation. */
    private int onTimeoutCancellationMethod;

    /** Duration of period. */
    private long duration;

    /** Name of period. */
    private String groupID;

    /**
     * Constructor. Set default value.
     *
     * @param groupID
     *            is a groupID
     * @param startTime
     *            is a startTime
     * @param endTime
     *            is an endTime
     * @param interval
     *            is an interval
     * @param onSuspendCancellationMethod
     *            is a value
     * @param onExitCancellationMethod
     *            is a value
     * @param onSelectCancellationMethod
     *            is a value
     * @param onTimeoutCancellationMethod
     *            is a value
     * @param days
     *            is a value
     * @param duration
     *            is a value
     */
    public Period(String groupID, Calendar startTime, Calendar endTime, Calendar interval,
            int onSuspendCancellationMethod, int onExitCancellationMethod, int onSelectCancellationMethod,
            int onTimeoutCancellationMethod, byte[] days, long duration) {
        startTimes = new Vector();
        week = new byte[7];
        this.onSuspendCancellationMethod = -1;
        this.onExitCancellationMethod = -1;
        this.onSelectCancellationMethod = -1;
        this.onTimeoutCancellationMethod = -1;
        this.duration = -1L;
        setGroupID(groupID);
        setStartTime(startTime, endTime, interval);
        this.onSuspendCancellationMethod = onSuspendCancellationMethod;
        this.onExitCancellationMethod = onExitCancellationMethod;
        this.onSelectCancellationMethod = onSelectCancellationMethod;
        this.onTimeoutCancellationMethod = onTimeoutCancellationMethod;
        setWeek(days);
        setDuration(duration);
    }

    /**
     * Get startTime.
     *
     * @return startTimes
     */
    public Vector getStartTime() {
        return startTimes;
    }

    /**
     * Set startTime.
     *
     * @param startTime
     *            is a Calendar
     * @param endTime
     *            is a Calendar
     * @param interval
     *            is a Calendar
     */
    public void setStartTime(Calendar startTime, Calendar endTime, Calendar interval) {
        if (endTime != null && interval != null) {
            while (startTime.before(endTime)) {
                Calendar tempCalendarTime = (Calendar) startTime.clone();
                startTimes.add(tempCalendarTime);
                startTime.add(Calendar.HOUR_OF_DAY, interval.get(Calendar.HOUR_OF_DAY));
                startTime.add(Calendar.MINUTE, interval.get(Calendar.MINUTE));
                startTime.add(Calendar.SECOND, interval.get(Calendar.SECOND));
            }
        } else {
            startTimes.add(startTime);
        }
    }

    /**
     * Get groupID.
     *
     * @return groupID
     */
    public final String getGroupID() {
        return groupID;
    }

    /**
     * Set GroupID.
     *
     * @param id
     *            is a groupID
     */
    public void setGroupID(String id) {
        groupID = id;
    }

    /**
     * get Week.
     *
     * @return week is byte
     */
    public final byte[] getWeek() {
        return week;
    }

    /**
     * set Week.
     *
     * @param weekData
     *            is a byte and 7bit. 1bit is day of week.
     *
     */
    public void setWeek(byte[] weekData) {
        week = weekData;
    }

    /**
     * Get OnSuspendCancellationMethod.
     *
     * @return onSuspendCancellationMethod
     */
    public final int getOnSuspendCancellationMethod() {
        return onSuspendCancellationMethod;
    }

    /**
     * Set OnSuspendCancellationMethod.
     *
     * @param onSuspendCancellation
     *            is value
     */
    public void setOnSuspendCancellationMethod(String onSuspendCancellation) {
        this.onSuspendCancellationMethod = Integer.parseInt(onSuspendCancellation);
    }

    /**
     * Get OnExitCancellationMethod.
     *
     * @return onExitCancellationMethod
     */
    public final int getOnExitCancellationMethod() {
        return onExitCancellationMethod;
    }

    /**
     * Set OnExitCancellationMethod.
     *
     * @param onExitCancellationValue
     *            is value
     */
    public void setOnExitCancellationMethod(String onExitCancellationValue) {
        onExitCancellationMethod = Integer.parseInt(onExitCancellationValue);
    }

    /**
     * Get OnSelectCancellationMethod.
     *
     * @return onSelectCancellationMethod
     */
    public final int getOnSelectCancellationMethod() {
        return onSelectCancellationMethod;
    }

    /**
     * Set OnSelectCancellationMethod.
     *
     * @param onSelectCancellationValue
     *            is value
     */
    public void setOnSelectCancellationMethod(String onSelectCancellationValue) {
        onSelectCancellationMethod = Integer.parseInt(onSelectCancellationValue);
    }

    /**
     * Get OnTimeoutCancellationMethod.
     *
     * @return onTimeoutCancellationMethod
     */
    public int getOnTimeoutCancellationMethod() {
        return onTimeoutCancellationMethod;
    }

    /**
     * Set OnTimeoutCancellationMethod.
     *
     * @param onTimeoutCancellationValue
     *            is value
     */
    public void setOnTimeoutCancellationMethod(String onTimeoutCancellationValue) {
        onTimeoutCancellationMethod = Integer.parseInt(onTimeoutCancellationValue);
    }

    /**
     * Get duration.
     *
     * @return duration
     */
    public long getDuration() {
        return duration;
    }

    /**
     * Set duration.
     *
     * @param time
     *            millisecond
     */
    public void setDuration(long time) {
        duration = time;
    }

    /**
     * teString.
     * @return String
     */
//    public String toString() {
//        return "Period [duration=" + duration + ", groupID=" + groupID + ", onExitCancellationMethod="
//                + onExitCancellationMethod + ", onSelectCancellationMethod=" + onSelectCancellationMethod
//                + ", onSuspendCancellationMethod=" + onSuspendCancellationMethod + ", onTimeoutCancellationMethod="
//                + onTimeoutCancellationMethod + ", startTimes=" + startTimes + ", week="
//                + (week != null ? arrayToString(week, week.length) : null) + "]";
//    }

    /**
     * convert array to string.
     * @param array array
     * @param len length
     * @return string
     */
    private String arrayToString(Object array, int len) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("[");
        for (int i = 0; i < len; i++) {
            if (i > 0) {
                buffer.append(", ");
            }
            if (array instanceof byte[]) {
                buffer.append(((byte[]) array)[i]);
            }
        }
        buffer.append("]");
        return buffer.toString();
    }

    /**
     * toString.
     *
     * public String toString(){ Log.printInfo("duration = " + duration + '\n'+ '\t' + '\t' + "startTimes = "
     * +startTimes.size() + '\n'+ '\t' + '\t' + "groupID = " + groupID + '\n'+ '\t' + '\t' +
     * "onSuspendCancellationMethod = " + onSuspendCancellationMethod + '\n'+ '\t' + '\t' +
     * "onExitCancellationMethod = " + onExitCancellationMethod + '\n'+ '\t' + '\t' + "onSelectCancellationMethod = " +
     * onSelectCancellationMethod + '\n'+ '\t' + '\t' + "onTimeoutCancellationMethod = " + onTimeoutCancellationMethod);
     *
     * return null; }
     */

}

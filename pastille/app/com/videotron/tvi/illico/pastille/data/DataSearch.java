/**
 * @(#)DataSearch.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.util.Calendar;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.Rs;
import com.videotron.tvi.illico.pastille.data.ConvertUtil;
import com.videotron.tvi.illico.pastille.data.PastilleData;
import com.videotron.tvi.illico.pastille.data.Period;
import com.videotron.tvi.illico.pastille.ixc.IXCManager;

/**
 * This class search Pastille Data.
 * @author pellos
 */
public class DataSearch {

    /** save to started channel. */
    private String saveChannel;
    /**
     * Constructor.
     */
    public DataSearch() {
    }

    /**
     * searches to event from pastilleDatas of Rs.java.
     * @param channel is current Channel name
     * @return pastilleData
     */
    public PastilleData searchData(String channel) throws Exception {
        Log.printDebug("searchData Start = " + channel);
        Calendar currentCalendar = Calendar.getInstance();
        boolean isSelectedData = false;
        long selectedTime = 0L;
        long selectedDuration = 0L;
        PastilleData pastilleData = null;

        String value = (String) Rs.getChannelDatas().get(channel);
        Log.printDebug("channelDatas =  " + value);

        if (value != null && IXCManager.stbMode == MonitorService.STB_MODE_NORMAL) {
            pastilleData = (PastilleData) Rs.getPastilleDatas().get(value);
        } else {
            saveChannel = channel;
            return null;
        }

        Log.printDebug("currentCalendar = " + currentCalendar.getTime());
        Log.printDebug("EndDate = " + pastilleData.getEndDate().getTime());
        Log.printDebug("StartDate = " + pastilleData.getStartDate().getTime());

        if (currentCalendar.before(pastilleData.getEndDate()) &&
                currentCalendar.after(pastilleData.getStartDate())) {

            // byte[] monday ~ sunday and Calendar sunday(1) ~ saturday(7) so -2
            int dayOfWeek = currentCalendar.get(Calendar.DAY_OF_WEEK) - 2;

            if (dayOfWeek < 0) {
                dayOfWeek = 6;
            }

            long currendSecond = ConvertUtil.changeCalendarToSecond(currentCalendar);
            selectedDuration = pastilleData.getDefaultDuration();
            byte[] weeks = pastilleData.getDefaultWeek();

            if (pastilleData.isOnChannelChange()) {
                if (weeks[dayOfWeek] == 1 && pastilleData.getAvailableGroup(pastilleData.getGroupID())) {

                    selectedDuration = pastilleData.getDefaultDuration();
                    Log.printDebug("saveChannel = " + saveChannel);
                    Log.printDebug("channel = " + channel);

                    if(saveChannel == null || !saveChannel.equals(channel)){
                        selectedTime = ConvertUtil.changeCalendarToSecond(Calendar.getInstance())
                            + (long) PastilleData.getTuneDelay();
                        Log.printDebug("3selectedTime = " + PastilleData.getTuneDelay());
                    } else {
                        selectedTime = ConvertUtil.changeCalendarToSecond(Calendar.getInstance())
                        + ConvertUtil.changeDateToSecond(pastilleData.getDefaultInterval());

                        Log.printDebug("4selectedTime = " + pastilleData.getDefaultInterval());
                    }
                    pastilleData.setOnExitCancellationMethod(pastilleData.getDefaultCancellationMethod());
                    pastilleData.setOnSelectCancellationMethod(pastilleData.getDefaultCancellationMethod());
                    pastilleData.setOnSuspendCancellationMethod(pastilleData.getDefaultCancellationMethod());
                    pastilleData.setOnTimeoutCancellationMethod(pastilleData.getDefaultCancellationMethod());
                    isSelectedData = true;
                }
            } else {
                Vector periods = pastilleData.getPeriodData();

                if (periods == null) {
                    saveChannel = channel;
                    return null;
                }

                for (int b = 0; b < periods.size(); b++) {
                    Period period = (Period) periods.elementAt(b);

                    if (period.getWeek() != null) {
                        weeks = period.getWeek();
                    }

                    String groutID = period.getGroupID();
                    Log.printDebug("pastilleData groutID = " + groutID);

                    if (weeks[dayOfWeek] != 1 || !pastilleData.getAvailableGroup(groutID)) {
                        continue;
                    }

                    Vector startTimes = period.getStartTime();

                    for (int i = 0; i < startTimes.size(); i++) {
                        Calendar startOfPeriod = (Calendar) startTimes.elementAt(i);
                        long startSecondOfPeriod = ConvertUtil.changeCalendarToSecond(startOfPeriod);

                        if (currendSecond >= startSecondOfPeriod || selectedTime != 0L
                                && selectedTime <= startSecondOfPeriod) {
                            continue;
                        }

                        selectedTime = startSecondOfPeriod;

                        if (period.getDuration() != 0L) {
                            selectedDuration = period.getDuration();
                        }
                        pastilleData.setGroupID(period.getGroupID());
                        pastilleData.setOnExitCancellationMethod(period.getOnExitCancellationMethod());
                        pastilleData.setOnSelectCancellationMethod(period.getOnSelectCancellationMethod());
                        pastilleData.setOnSuspendCancellationMethod(period.getOnSuspendCancellationMethod());
                        pastilleData.setOnTimeoutCancellationMethod(period.getOnTimeoutCancellationMethod());
                        Log.printDebug("startOfPeriod =  " + startOfPeriod.getTime());
                        isSelectedData = true;
                    }
                }
            }
        }

        Log.printDebug("selectedTime = " + selectedTime);
        Log.printDebug("isSelectedData = " + isSelectedData);
        saveChannel = channel;

        if(isSelectedData && pastilleData.getEid().size() > 0) {
            Log.printDebug("pastilleData.getEid() = " + pastilleData.getEid());

            for(int a = 0; a < pastilleData.getEid().size(); a++){
                String key = (String)pastilleData.getEid().elementAt(a);
                isSelectedData = IXCManager.getInstance().getEidAtPackageNameEidMappingFile(key);
                if(!isSelectedData) {
                    break;
                }
            }
            Log.printDebug("isSelectedData() = " + isSelectedData);
        }

        if (isSelectedData) {
            Log.printDebug("PastilleApplicationID = " + pastilleData.getPastilleApplicationID());
            pastilleData.setIconViewedTime(selectedTime);
            pastilleData.setDefaultDuration(selectedDuration);
            return pastilleData;
        } else {
            pastilleData = null;
            Log.printDebug("pastilleData = null");
            return pastilleData;
        }
    }
}

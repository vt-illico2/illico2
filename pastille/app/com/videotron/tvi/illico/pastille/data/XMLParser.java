/**
 * @(#)XMLParser.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.Rs;
import com.videotron.tvi.illico.pastille.ixc.IXCManager;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class makes Pastille Data from XML Data.
 * @author pellos
 */
public class XMLParser extends DefaultHandler {

    /** instance data of PastilleData. */
    private PastilleData pastilleData;

    /** parameter data value. */
    private int paramValue;

    /** parameter state. */
    private static final int ACTIVE_PASTILLE_TYPE = 0;

    /** is saved pastille data. */
    private static Vector pastilleDatas = new Vector();

    /** ONEXIT is state value of cancel. */
    private static final int ONEXIT = 0;
    /** ONTIMEOUT is state value of cancel. */
    private static final int ONTIMEOUT = 1;
    /** ONSUSPEND is state value of cancel. */
    private static final int ONSUSPEND = 2;
    /** ONSELECT is state value of cancel. */
    private static final int ONSELECT = 3;

    /** state value of cancellation. */
    private static int triggerType = 0;

    /** groupID of period. */
    private int groupID;

    /**
     * Constructor.
     */
    public XMLParser() {
    }

    public XMLParser(int num) {
        pastilleData = null;
        groupID = 0;
        MainManager.getInstance().setIconState(MainManager.SCHEDULE_UPDATE);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            Log.printInfo("XMLParser start");
            SAXParser parser = factory.newSAXParser();
            File f = new File("resource/data/pastille_schedule" + num + ".xml");
            Rs.getChannelDatas().clear();
            InputSource is = new InputSource(new FileInputStream(f));
            parser.parse(is, this);
        } catch (ParserConfigurationException e) {
            Log.print(e);
        } catch (SAXException e) {
            Log.print(e);
        } catch (FileNotFoundException e) {
            Log.print(e);
        } catch (IOException e) {
            Log.print(e);
        }
    }

    public void setParserXml(File file) {
        pastilleDatas.clear();
        pastilleData = null;
        groupID = 0;
        MainManager.getInstance().setIconState(MainManager.SCHEDULE_UPDATE);
        SAXParserFactory factory = SAXParserFactory.newInstance();
        try {
            Log.printInfo("XMLParser start");
            SAXParser parser = factory.newSAXParser();
            InputSource is = new InputSource(new FileInputStream(file));
            Rs.getChannelDatas().clear();
            parser.parse(is, this);
        } catch (ParserConfigurationException e) {
        	// only log as per specs
            //IXCManager.getInstance().showErrorMessage("SDM502");
        	Log.printError("XMLParser: setParserXml: error code = SDM502");
            Log.print(e);
        } catch (SAXException e) {
        	// only log as per specs
            //IXCManager.getInstance().showErrorMessage("SDM502");
        	Log.printError("XMLParser: setParserXml: error code = SDM502");
            Log.print(e);
        } catch (FileNotFoundException e) {
        	// only log as per specs
            //IXCManager.getInstance().showErrorMessage("SDM502");
        	Log.printError("XMLParser: setParserXml: error code = SDM502");
            Log.print(e);
        } catch (IOException e) {
        	// only log as per specs
            //IXCManager.getInstance().showErrorMessage("SDM502");
        	Log.printError("XMLParser: setParserXml: error code = SDM502");
            Log.print(e);
        } catch (Exception e){
        	// only log as per specs
            //IXCManager.getInstance().showErrorMessage("SDM502");
        	Log.printError("XMLParser: setParserXml: error code = SDM502");
            Log.print(e);
        }
    }

    /**
     * get PastilleDatas.
     * @return pastilleDatas
     */
    public Vector getPastilleDatas() {
        return pastilleDatas;
    }

    /**
     * doesn't used.
     * @throws SAXException
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     */
    public void startDocument() throws SAXException {
    }

    /**
     * doesn't used.
     * @throws SAXException
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     */
    public void endDocument() throws SAXException {
    }

    /**
     * doesn't used.
     * @param name The name of the skipped entity.
     * @throws SAXException
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     */
    public void skippedEntity(String name) throws SAXException {
    }

    /**
     * Receive notification of the start of an element. Is received values of xml. This method makes pastille data.
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @param attributes The attributes attached to the element. If there are no attributes, it shall be an empty
     *            Attributes object.
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.ContentHandler#startElement
     */
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
//        Log.printDebug("qName " + qName + " attributes = " + attributes);
        if (qName.equals("schedule")) {
            String qNameSub = attributes.getQName(0);
            String value = attributes.getValue(0);

            if (qNameSub.equals("tuneDelay")) {
                PastilleData.setTuneDelay(value);
            }
        } else if (qName.equals("event")) {
            pastilleData = null;
            pastilleData = new PastilleData();

            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);
                if (qNameSub.equals("name")) {
                    pastilleData.setName(value);
                } else if (qNameSub.equals("channels")) {
                    pastilleData.setChannels(value);
                } else if (qNameSub.equals("startDate")) {
                    pastilleData.setStartDate(value);
                } else if (qNameSub.equals("endDate")) {
                    pastilleData.setEndDate(value);
                } else if (qNameSub.equals("days")) {
                    pastilleData.setDefaultWeek(value);
                } else if (qNameSub.equals("defaultDuration")) {
                    pastilleData.setDefaultDuration(value);
                } else if (qNameSub.equals("defaultCancellationMethod")) {
                    pastilleData.setDefaultCancellationMethod(Integer.parseInt(value));
                } else if (qNameSub.equals("defaultInterval")) {
                    pastilleData.setDefaultInterval(value);
                } else if (qNameSub.equals("package")) {
                    pastilleData.setEid(value);
                } else if (qNameSub.equals("onChannelChange")) {
                    pastilleData.setOnChannelChange(value);
                }
            }
        } else if (qName.equals("period")) {
            String startTime = null;
            String endTime = null;
            String interval = null;
            String onSuspendCancellationMethod = null;
            String onExitCancellationMethod = null;
            String onSelectCancellationMethod = null;
            String onTimeoutCancellationMethod = null;
            String days = null;
            String duration = null;
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);


//                Log.printDebug("qNameSub " + qNameSub + " value = " + value);

                if (qNameSub.equals("startTime")) {
                    startTime = value;
                } else if (qNameSub.equals("endTime")) {
                    endTime = value;
                } else if (qNameSub.equals("interval")) {
                    interval = value;
                } else if (qNameSub.equals("duration")) {
                    duration = value;
                } else if (qNameSub.equals("days")) {
                    days = value;
                } else if (qNameSub.equals("onSuspendCancellationMethod")) {
                    onSuspendCancellationMethod = value;
                } else if (qNameSub.equals("onExitCancellationMethod")) {
                    onExitCancellationMethod = value;
                } else if (qNameSub.equals("onSelectCancellationMethod")) {
                    onSelectCancellationMethod = value;
                } else if (qNameSub.equals("onTimeoutCancellationMethod")) {
                    onTimeoutCancellationMethod = value;
                }
            }

            pastilleData.setPeriod(String.valueOf(groupID), startTime, endTime, interval, onSuspendCancellationMethod,
                    onExitCancellationMethod, onSelectCancellationMethod, onTimeoutCancellationMethod, days, duration);
            groupID++;
        } else if (qName.equals("pastilleDefinition")) {
            String qNameSub = attributes.getQName(0);
            String value = attributes.getValue(0);

            if (qNameSub.equals("pastilleApplicationID")) {
                pastilleData.setPastilleApplicationID(value);
            }
        } else if (qName.equals("relativeLocation")) {
            String value = attributes.getValue(0);
            pastilleData.setCorner(value);
        } else if (qName.equals("absoluteLocation")) {
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);

                if (qNameSub.equals("xLocation")) {
                    pastilleData.setXCoordinate(Integer.parseInt(value));
                } else if (qNameSub.equals("yLocation")) {
                    pastilleData.setYCoordinate(Integer.parseInt(value));
                }
            }
        } else if (qName.equals("activePastilleType")) {
            paramValue = ACTIVE_PASTILLE_TYPE;
        } else if (qName.equals("feedback")) {
            String value = attributes.getValue(0);

            if (value.equalsIgnoreCase("onExit")) {
                triggerType = ONEXIT;
            } else if (value.equalsIgnoreCase("onTimeout")) {
                triggerType = ONTIMEOUT;
            } else if (value.equalsIgnoreCase("onSuspend")) {
                triggerType = ONSUSPEND;
            } else if (value.equalsIgnoreCase("onSelect")) {
                triggerType = ONSELECT;
            }
        } else if (qName.equals("actionEvent")) {
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);

                switch (triggerType) {
                case ONEXIT:
                    pastilleData.setFeedbakcOnExitHash(qNameSub, value);
                    break;

                case ONTIMEOUT:
                    pastilleData.setFeedbakcOnTimeoutHash(qNameSub, value);
                    break;

                case ONSUSPEND:
                    pastilleData.setFeedbakcOnSuspendHash(qNameSub, value);
                    break;

                case ONSELECT:
                    pastilleData.setFeedbakcOnSelectHash(qNameSub, value);
                    break;
                default:
                    break;
                }
            }
        } else if (qName.equals("title")) {
            int languageType = 0;
            String title = null;
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);
                if (qNameSub.equals("description")) {
                    title = value;
                } else if (qNameSub.equals("language")) {
                    if (value.equals("fr")) {
                        languageType = 0;
                    } else {
                        languageType = 1;
                    }
                }
            }
            pastilleData.setTitle(languageType, title);
        } else if (qName.equals("subTitle")) {
            int languageType = 0;
            String subTitle = null;
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);
                if (qNameSub.equals("description")) {
                    subTitle = value;
                } else if (qNameSub.equals("language")) {
                    if (value.equals("fr")) {
                        languageType = 0;
                    } else {
                        languageType = 1;
                    }
                }
            }
            pastilleData.setSubTitle(languageType, subTitle);
        } else if (qName.equals("okStatus")) {
            int languageType = 0;
            String okstatus = null;
            for (int a = 0; a < attributes.getLength(); a++) {
                String qNameSub = attributes.getQName(a);
                String value = attributes.getValue(a);
                if (qNameSub.equals("description")) {
                    okstatus = value;
                } else if (qNameSub.equals("language")) {
                    if (value.equals("fr")) {
                        languageType = 0;
                    } else {
                        languageType = 1;
                    }
                }
            }
            pastilleData.setOkStatus(languageType, okstatus);
        }
    }

    /**
     * Receive notification of the end of an element. Saves pastille data.
     * <p>
     * By default, do nothing. Application writers may override this method in a subclass to take specific actions at
     * the end of each element (such as finalising a tree node or writing output to a file).
     * </p>
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.ContentHandler#endElement
     */
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals("event") && pastilleDatas != null) {
            pastilleDatas.add(pastilleData);
            Rs.setPastilleData(pastilleDatas);
        }
    }

    /**
     * doesn't used.
     * @param s The Namespace prefix being declared.
     * @param s1 The Namespace URI mapped to the prefix.
     * @throws SAXException
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     */
    public void startPrefixMapping(String s, String s1) throws SAXException {
    }

    /**
     * doesn't used.
     * @param prefix The Namespace prefix being declared.
     * @throws SAXException
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     */
    public void endPrefixMapping(String prefix) throws SAXException {
    }

    /**
     * Receive notification of character data inside an element.
     * <p>
     * By default, do nothing. Application writers may override this method to take specific actions for each chunk of
     * character data (such as adding the data to a node or buffer, or printing it to a file).
     * </p>
     * @param ch The characters.
     * @param start The start position in the character array.
     * @param length The number of characters to use from the character array.
     * @exception org.xml.sax.SAXException Any SAX exception, possibly wrapping another exception.
     * @see org.xml.sax.ContentHandler#characters
     */
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = (new String(ch, start, length)).trim();
        if (value.length() > 0) {
            switch (paramValue) {
            case ACTIVE_PASTILLE_TYPE:
                pastilleData.setPastilleType(value);
                break;
            default:
                break;
            }
        }
    }
}

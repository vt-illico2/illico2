/**
 * @(#)ConvertUtil.java
 * 
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.util.Calendar;

import com.videotron.tvi.illico.log.Log;

/**
 * Utility class.
 * @author pellos
 */
public final class ConvertUtil {

    /**
     * Constructor.
     */
    private ConvertUtil() {
    }

    /**
     * Convert from String to Calendar.
     * @param time is String 00:00:00
     * @return calendar
     */
    public static Calendar getTime(String time) {
        Calendar calendar = null;
        if (time != null) {
            calendar = Calendar.getInstance();
            int tt = Integer.parseInt(time.substring(0, 2));
            int mm = Integer.parseInt(time.substring(3, 5));
            int ss = Integer.parseInt(time.substring(6, 8));

            calendar.set(11, tt);
            calendar.set(12, mm);
            calendar.set(13, ss);
        }
        return calendar;
    }

    /**
     * Convert from String to millisecond.
     * @param data is String.
     * @return second
     */
    public static long changeDateToSecond(String data) {
        long second = 0L;
        long tt = Integer.parseInt(data.substring(0, 2));
        long mm = Integer.parseInt(data.substring(3, 5));
        long ss = Integer.parseInt(data.substring(6, 8));
        second = tt * 3600L + mm * 60L + ss;
        return second;
    }

    /**
     * Convert from Calendar to millisecond.
     * @param cal is Calendar.
     * @return second
     */
    public static long changeCalendarToSecond(Calendar cal) {
        long second = cal.get(Calendar.HOUR_OF_DAY) * 3600 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND);
        return second;
    }

    /**
     * Convert from millisecond to Calendar.
     * @param sumSecond is millisecond
     * @return calendar
     */
    public static Calendar changeSecondToTime(long sumSecond) {
        Calendar calendar = Calendar.getInstance();
        int time = (int) (sumSecond / 3600L);
        int minute = (int) ((sumSecond - (long) (time * 3600)) / 60L);
        int second = (int) (sumSecond - (long) (time * 3600 + minute * 60));
        calendar.set(11, time);
        calendar.set(12, minute);
        calendar.set(13, second);
        return calendar;
    }
}

/**
 * @(#)HttpConnection.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.data;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.data.HttpConnection;

/**
 * This class is with HTTP Connection. Pastille icon datas get to server.
 *
 * @author pellos
 *
 */
public final class HttpConnection {

    /** The singleton instance of HttpConnection. */
    private static HttpConnection httpConnection;

    /** ServerIP. */
    private static String serverIP = "";

    /** IP Header. */
    private static final String URL_PRE = "http://";

    /** IP middle. */
    private static final String URL_MID = "/vt/test/";

    /**
     * Constructor.
     */
    private HttpConnection() {
        String ip = (String) DataCenter.getInstance().get("serverIP");
        String port = (String) DataCenter.getInstance().get("port");
        StringBuffer sb = new StringBuffer();
        sb.append(URL_PRE);
        sb.append(ip);
        sb.append(":");
        sb.append(port);
        sb.append(URL_MID);
        serverIP = sb.toString();
        Log.printDebug("URL = " + serverIP);
    }

    /**
     * Gets the singleton instance of HttpConnection.
     *
     * @return instance
     */
    public static HttpConnection getInstance() {
        if (httpConnection == null) {
            httpConnection = new HttpConnection();
        }
        return httpConnection;
    }

    /**
     * get image data from server.
     *
     * @param name
     *            is Image name.
     * @return b
     */
    public byte[] getURLConnect(String name) {
        BufferedInputStream bis = null;
        byte[] b = null;
        Log.printDebug("getURLImage = " + name);

        try {
            URL url = new URL(serverIP + name);

            int code = ((HttpURLConnection) url.openConnection()).getResponseCode();
            Log.printDebug("URLImage code = " + code);

            bis = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            byte[] buf = new byte[1024];
            int c;

            while ((c = bis.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }

            b = baos.toByteArray();
        } catch (MalformedURLException e) {
            Log.print(e);
        } catch (IOException e) {
            Log.print(e);
        } finally {
            try {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (IOException e) {
                        Log.print(e);
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }

        return b;
    }
}

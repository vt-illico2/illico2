/**
 * @(#)Rs.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.data.PastilleData;
import com.videotron.tvi.illico.util.Environment;

import java.util.Hashtable;
import java.util.Vector;

/**
 * This class saves pastille datas, and defines config date.
 *
 * @author pellos
 *
 */
public final class Rs {

    /**
     * Constructor.
     */
    private Rs() {

    }

    /** is emul. */
    public static final boolean ISEMUL = Environment.EMULATOR;

    /** This table saves pastille data. */
    private static Hashtable pastilleDatas = new Hashtable();

    /** This table saves channel names. */
    private static Hashtable channelDatas = new Hashtable();

    /**
     * saves pastille data from xml.
     *
     * @param data
     *            is pastille datas
     */
    public static void setPastilleData(Vector data) {
        for (int a = 0; a < data.size(); a++) {
            PastilleData pastilleData = (PastilleData) data.elementAt(a);
            pastilleDatas.put(pastilleData.getName(), pastilleData);
//            Log.printInfo(pastilleData.toString());
        }

        if (data.size() > 0) {
            MainManager.getInstance().setIconState(MainManager.ICON_DEACTIVE);
            Log.printDebug("setPastilleData END");
            MainManager.setTvState(MainManager.TV_VIEWING_STATE);

//            MainManager.setCurrentChannelName("EMUL3");
            MainManager.getInstance().putEvent(MainManager.getCurrentChannelName());
        } else {
            MainManager.getInstance().resetData();
        }
    }

    /**
     * get pastille data.
     *
     * @return pastilleDatas
     */
    public static Hashtable getPastilleDatas() {
        return pastilleDatas;
    }

    /**
     * get channel data.
     *
     * @return channelDatas
     */
    public static Hashtable getChannelDatas() {
        return channelDatas;
    }
}

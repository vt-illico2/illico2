/**
 * @(#)IconUI.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille.ui;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.MainManager;
import com.videotron.tvi.illico.pastille.data.PastilleData;
import com.videotron.tvi.illico.pastille.gui.IconGui;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class is IconUI.
 * @author pellos
 */
public class IconUI extends UIComponent implements LayeredKeyHandler, TVTimerWentOffListener {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** BOUNDS. */
    private static final Rectangle BOUNDS_MINI_MENU = new Rectangle(0, 0, 960, 540);

    /** Instance of IconGui. */
    private IconGui iconGui;

    LayeredWindowPastille layeredWindow;

    /** pastille data. */
    private PastilleData pastilleData;

    /** duration of event. */
    private long duration;

    /** Instance of TVTimerSpec. */
    private TVTimerSpec timer;

    /** second time. */
    private static final long SECOND_TIME = 1000L;
    /** Instance of LayeredUI. */
    private LayeredUI layerui;

    /** If another application stared, this value is true. */
    private boolean isOverlapped = false;

    private AlphaEffect showingEffect;

    private boolean keyLock = false;

    /**
     * Get PastilleData.
     * @return pastilleData
     */
    public PastilleData getPastilleData() {
        return pastilleData;
    }

    /**
     * Constructor. Set TVTimerSpec. make instance of IconGUI.
     */
    public IconUI() {
        duration = 0L;
        timer = new TVTimerSpec();
        timer.setRepeat(false);
        timer.setAbsolute(true);
        timer.addTVTimerWentOffListener(this);
    }

    /**
     * set IconUI.
     */
    public void init() {
        setBounds(BOUNDS_MINI_MENU);
        setVisible(false);
        if (layeredWindow == null) {
            layeredWindow = new LayeredWindowPastille();
            layeredWindow.setBounds(BOUNDS_MINI_MENU);
            layeredWindow.setVisible(false);
            layeredWindow.add(this);
        }

        iconGui = new IconGui();
        setRenderer(iconGui);
        prepare();
        layerui = WindowProperty.PASTILLE.createLayeredDialog(layeredWindow, BOUNDS_MINI_MENU, this);
        Log.printDebug("layerui = " + layerui);
    }

    /**
     * sets Pastille Data. starts event.
     * @param pastille is event data
     */
    public void start(PastilleData pastille) {
        pastilleData = null;
        layeredWindow.setVisible(true);
        layerui.activate();

        if (isOverlapped) {
            MainManager.getInstance().cancellation(MainManager.NO_CANCEL);
            return;
        }

        pastilleData = pastille;
        duration = pastilleData.getDefaultDuration();
        Log.printDebug("duration.getDuration = " + duration);
        prepare();
        stopTimer();
        timer.setDelayTime(duration * SECOND_TIME);

        try {
            timer = TVTimer.getTimer().scheduleTimerSpec(timer);
        } catch (Exception e) {
            Log.print(e);
        }

        iconImageAlphaEffect();
    }

    /**
     * stops event.
     */
    public void stop() {
        keyLock = false;
        if(layerui == null) {
            return;
        }
        layeredWindow.setVisible(false);
        this.setVisible(false);
        layerui.deactivate();
        iconGui.stop();
        stopTimer();
        iconGui.setImageBlind(true);
        repaint();
        MainManager.getInstance().setIconState(MainManager.ICON_DEACTIVE);
        pastilleData = null;
    }

    public void iconImageAlphaEffect() {
        Log.printDebug("iconImageAlphaEffect");

        keyLock = true;
        if (showingEffect == null) {
            showingEffect = new AlphaEffect(this, 35, AlphaEffect.FADE_IN);
        }
        showingEffect.start();
    }

    /**
     * stops TVTimer.
     */
    public void stopTimer() {
        TVTimer.getTimer().deschedule(timer);
    }

    /**
     * As events end, this method receive TvTimer Event.
     * @param tv events.
     */
    public void timerWentOff(TVTimerWentOffEvent tv) {
        Log.printDebug("timerWentOff");
        if (pastilleData != null) {
            int onTimeoutCancel = pastilleData.getOnTimeoutCancellationMethod();
            stop();
            MainManager.getInstance().cancellation(onTimeoutCancel);
            MainManager.getInstance().nextAction(PastilleData.ONTIMEOUT);
        }
    }

    /**
     * is received romoteControl events.
     * @param e UserEvent.
     * @return boolean
     */
    public boolean handleKeyEvent(UserEvent e) {

        Log.printDebug("iconGui.isKeyLock() " + keyLock);
        if(pastilleData == null || keyLock){
            return false;
        }
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }

        int keycode = e.getCode();
        Log.printDebug("handleKeyEvent = " + keycode);
        if (keycode == OCRcEvent.VK_ENTER) {
            int onSelectCancel = pastilleData.getOnSelectCancellationMethod();
            stop();
            MainManager.getInstance().cancellation(onSelectCancel);
            MainManager.getInstance().nextAction(PastilleData.ONSELECT);
            return true;
        } else if (keycode == OCRcEvent.VK_EXIT) {
            int onexitCancel = pastilleData.getOnExitCancellationMethod();
            stop();
            MainManager.getInstance().cancellation(onexitCancel);
            MainManager.getInstance().nextAction(PastilleData.ONEXIT);
            return true;
        } else if (keycode == OCRcEvent.VK_CHANNEL_UP || keycode == OCRcEvent.VK_CHANNEL_DOWN) {
            stop();
        } else {
            int onSuspendCancel = pastilleData.getOnSuspendCancellationMethod();
            stop();
            MainManager.getInstance().cancellation(onSuspendCancel);
            MainManager.getInstance().nextAction(PastilleData.ONSUSPEND);
        }
        return false;
    }


    public void animationEnded(Effect effect) {
        this.setVisible(true);
        repaint();
        keyLock = false;
    }


    class LayeredWindowPastille extends LayeredWindow {
        private static final long serialVersionUID = 1L;

        /**
         * Constructor. Sets LayersWindow.
         */
        public LayeredWindowPastille() {
            setBounds(BOUNDS_MINI_MENU);
        }

        /**
         * Another application isn't start.
         */
        public void notifyClean() {
            isOverlapped = false;
            Log.printInfo("notifyClean isOverlapped = " + isOverlapped);
        }

        /**
         * Another application is started.
         */
        public void notifyShadowed() {
            isOverlapped = true;
            Log.printInfo("notifyShadowed isOverlapped = " + isOverlapped);

            if (iconGui != null && this.isVisible()  && pastilleData != null) {
                int onSuspendCancel = pastilleData.getOnSuspendCancellationMethod();
                MainManager.getInstance().cancellation(onSuspendCancel);
                MainManager.getInstance().nextAction(PastilleData.ONSUSPEND);
            }
        }
    }
}

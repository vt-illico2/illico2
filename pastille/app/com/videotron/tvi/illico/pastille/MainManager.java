/**
 * @(#)DataManager.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.pastille;

import java.rmi.RemoteException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pastille.data.ConfigManager;
import com.videotron.tvi.illico.pastille.data.DataSearch;
import com.videotron.tvi.illico.pastille.data.PastilleData;
import com.videotron.tvi.illico.pastille.ixc.IXCManager;
import com.videotron.tvi.illico.pastille.ixc.PreferenceProxy;
import com.videotron.tvi.illico.pastille.timer.AlarmTimer;
import com.videotron.tvi.illico.pastille.timer.EventTimer;
import com.videotron.tvi.illico.pastille.ui.IconUI;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class controls Pastille. channel events put into Queue and veiws Icon on screen
 * @author pellos
 */
public final class MainManager implements Runnable {

    /** PastilleData of current. */
    private PastilleData pastilleData;

    /** Original data of pastilleData before changed PastilleData. */
    private PastilleData originalPastilleData;

    /** The singleton instance of PastilleData. */
    private static MainManager instance;

    /** The singleton instance of DataSearch. */
    private DataSearch dataSearch;

    /** The singleton instance of IconUI. */
    private static IconUI iconUI;

    /** Event Queue controller. */
    private Thread thread;

    /** Runner of thread event queue. */
    private boolean running;

    /** Before inbandData Load. */
    public static final int REDAY = 0;
    /** Pastille Icon is not visibly. */
    public static final int ICON_DEACTIVE = 1;
    /** Pastille Icon is visibly. */
    public static final int ICON_ACTIVE = 2;
    /** Pastille data is updated state. */
    public static final int SCHEDULE_UPDATE = 3;

    /** Current state of Patille. */
    private int iconState;

    /** Queue to store the channel. */
    private Vector eventQueue;

    /** Current channel name. */
    private static String currentChannelName = "";

    /** is state of TV received from monitor. */
    private static int tvState = -1;

    /** unknow state of TV. */
    public static final int UNKNOWN_STATE = -1;
    /** viewing state of TV. */
    public static final int TV_VIEWING_STATE = 0;
    /** full screen another Application state of TV. */
    public static final int FULL_SCREEN_APP_STATE = 1;

    /** cancellation method of no cancellation. */
    public static final int NO_CANCEL = 0;
    /** cancellation method of cancel only period. */
    private static final int CANCEL_ONLY_PERIOD = 1;
    /** cancellation method of cancel whole event. */
    private static final int CANCEL_WHOLE_EVENT = 2;
    /** cancellation method of cancel period permanently. */
    private static final int CANCEL_PERIOD_PERMANENTLY = 3;
    /** cancellation method of cancel whole event permanently. */
    private static final int CANCEL_WHOLEEVENT_PERMANENTLY = 4;

    /** cancellation value. */
    private int cancellation;

    /** wait time with fast channel change. */
    public static final long WAIT_TIME_FOR_ICON_VIEWABLE = 1000L;

    public Hashtable imageTable = new Hashtable();

    /**
     * Constructor.
     */
    private MainManager() {
        running = false;
        iconState = 0;
        eventQueue = new Vector();
        cancellation = 0;
    }

    /**
     * Gets the singleton instance of MainManager.
     * @return instance is instance of MainManager.
     */
    public static MainManager getInstance() {
        if (instance == null) {
            instance = new MainManager();
        }
        return instance;
    }

    /**
     * initial method after called Constructor.
     * @param xletContext is XletContext.
     */
    public void init(XletContext xletContext) {
        Log.printInfo("init");
        iconUI = new IconUI();
        ConfigManager.getInstance().init();
        IXCManager.getInstance().init(xletContext);

//        IXCManager.getInstance().lookupEpgService();
//        IXCManager.getInstance().lookupMonitorService();
//        IXCManager.getInstance().lookupSTCService();
//        IXCManager.getInstance().lookupErrorMessageService();

        dataSearch = new DataSearch();
    }

    /**
     * start Thread, AlarmTimer.
     */
    public void start() {
        Log.printInfo("start");
        if (thread == null) {
            thread = new Thread(this, "Pastille MainThread2");
        }

        if (thread != null) {
            thread.start();
        }

//        Object ob = DataCenter.getInstance().get("OOBBAND");
//
//        if (ob != null) {
//            try {
//                ConfigManager.getInstance().xml.setParserXml((File) ob);
//            } catch (Exception e) {
//                Log.printError("dataUpdated Exception");
//            }
//        }
        imageTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        iconUI.init();
        AlarmTimer.getInstance().startTimer();
    }

    /**
     * stopf
    public void stop() {
    }

    /**
     * destroy.
     */
    public void destroy() {
    }

    /**
     * run. If the channel changes, event is put into queue. This thread pulls last event from queue. search available
     * event by channel name.
     */
    public void run() {
        if (running) {
            return;
        }
        running = true;

        while (running) {
            while (eventQueue.isEmpty()) {
                synchronized (this) {
                    try {
                        wait();
                    } catch (InterruptedException e) {
                        Log.print(e);
                    }
                }
            }

            String channelName = (String) eventQueue.remove(eventQueue.size() - 1);
            Log.printDebug("eventQueue channelName = " + channelName);
            Log.printDebug("eventQueue.size() = " + eventQueue.size());
            eventQueue.clear();

            try {
                Thread.sleep(WAIT_TIME_FOR_ICON_VIEWABLE);
            } catch (InterruptedException e) {
                Log.print(e);
            }

            if (!eventQueue.isEmpty()) {
                Log.printDebug("eventQueue.size() = " + eventQueue.size());
            } else {
                pastilleData = null;
                try {
                    pastilleData = dataSearch.searchData(channelName);
                } catch (Exception e) {
                    IXCManager.getInstance().showErrorMessage("SDM501");
                    eventQueue.clear();
                }
                if (pastilleData != null) {
                    String appId = pastilleData.getPastilleApplicationID();
                    Log.printDebug("appId = " + appId);
                    if (appId == null) {
                    } else {// if (appId.equals(PastilleData.PASTILLE_NAME)) {
                        long startTime = pastilleData.getIconViewedTime();
                        EventTimer.getInstance().startTimer(startTime);
                    }
                }
            }
        }

        eventQueue.clear();
        running = false;
        if (thread == null) {
            thread = new Thread(this, "Pastille MainThread");
        }

        if (thread != null) {
            thread.start();
        }
    }

    /**
     * put channel name into queue. notify thread.
     * @param channelName is channel name.
     */
    public synchronized void putEvent(String channelName) {
        Log.printDebug("New PutChannelEvent()" + channelName);
        resetData();
        eventQueue.add(channelName);
        notifyAll();
    }

    /**
     * start event or start next event.
     */
    public void startIcon() {
        Log.printDebug("New getTvState() = " + getTvState());

        if (pastilleData != null && getTvState() == TV_VIEWING_STATE) {
            setIconState(ICON_ACTIVE);
            if (pastilleData != null) {
                originalPastilleData = pastilleData;
            }
            iconUI.start(pastilleData);
        } else if (pastilleData != null && getTvState() != TV_VIEWING_STATE) {
            cancellation(NO_CANCEL);
        }
    }

    /**
     * stop event.
     */
    public void resetData() {
        EventTimer.getInstance().stopTimer();
        if(iconUI != null){
            iconUI.stop();
        }
    }

    /**
     * If cancel was dose, cancel event reverts without CANCEL_PERIOD_PERMANENTLY, CANCEL_WHOLEEVENT_PERMANENTLY after
     * channel change.
     */
    public void cancellationStateChange() {
        if (originalPastilleData != null && cancellation != CANCEL_PERIOD_PERMANENTLY
                && cancellation != CANCEL_WHOLEEVENT_PERMANENTLY) {
            Log.printDebug("AvailableAllGroup");
            originalPastilleData.setAvailableAllGroup("true");
        }
        originalPastilleData = null;
        cancellation = 0;
    }

    /**
     * set cancellation state.
     * @param state kinds of Cancel.
     */
    public void cancellation(int state) {
        Log.printDebug("cancellation state = " + state);
        cancellation = state;
        String groupID = pastilleData.getGroupID();
        switch (state) {
        case NO_CANCEL:
            getInstance().putEvent(getCurrentChannelName());
            break;

        case CANCEL_ONLY_PERIOD:
            resetData();
            pastilleData.setAvailableGroupID(groupID, "false");
            getInstance().putEvent(getCurrentChannelName());
            break;

        case CANCEL_WHOLE_EVENT:
            resetData();
            pastilleData.setAvailableAllGroup("false");
            break;

        case CANCEL_PERIOD_PERMANENTLY:
            resetData();
            pastilleData.setAvailableGroupID(groupID, "false");
            break;

        case CANCEL_WHOLEEVENT_PERMANENTLY:
            resetData();
            pastilleData.setAvailableAllGroup("false");
            break;
        default:
            resetData();
            getInstance().putEvent(getCurrentChannelName());
            break;
        }
    }

    /**
     * next action after pastille is ended.
     * @param state is Pastille cancel value.
     */
    public void nextAction(int state) {
        Log.printDebug("state = " + state);
        Hashtable hashtable = null;

        switch (state) {
        case PastilleData.ONEXIT:
            hashtable = originalPastilleData.getFeedbakcOnExitHash();
            break;
        case PastilleData.ONTIMEOUT:
            hashtable = originalPastilleData.getFeedbakcOnTimeoutHash();
            break;
        case PastilleData.ONSUSPEND:
            hashtable = originalPastilleData.getFeedbakcOnSuspendHash();
            break;
        case PastilleData.ONSELECT:
            hashtable = originalPastilleData.getFeedbakcOnSelectHash();
            break;
        default:
            break;
        }

        if (hashtable != null) {
            Log.printDebug("hashtable = " +  hashtable.size());
//            Enumeration em = hashtable.elements();
            Enumeration em = hashtable.keys();
            while(em.hasMoreElements()){
                Object ob = em.nextElement();
                Log.printDebug("ob = " +  ob);
                Object val = hashtable.get(ob);
                Log.printDebug("val = " +  val);
            }

            String eventTargetType = (String) hashtable.get(PastilleData.EVENT_TARGET_TYPE);
            Log.printDebug("eventTargetType " + eventTargetType);
            if (eventTargetType == null) {
                return;
            } else if (eventTargetType.equalsIgnoreCase(PastilleData.TARGET_TYPE_APPLICATION)) {
                String targetLocator = (String) hashtable.get(PastilleData.TARGET_LOCATOR);
                String targetPamameters = (String) hashtable.get(PastilleData.TARGET_PAMAMETERS);
                String[] param = null;
                if (targetPamameters != null) {
                    StringTokenizer parser = new StringTokenizer(targetPamameters, "|");
                    param = new String[parser.countTokens() + 1];
                    int count = 0;
//                    param[count++] = targetLocator;
                    param[count++] = App.APP_NAME;
                    while (parser.hasMoreTokens()) {
                        String tempToken = parser.nextToken();
                        Log.printDebug("tempToken = " + tempToken);
                        if(tempToken != null && tempToken.length() > 0) {
                            param[count++] = tempToken;
                        }
                    }
                } else {
//                    param = new String[]{targetLocator};
                    param = new String[]{App.APP_NAME};
                }
                
                // VDTRMASTER-5552
                if (targetLocator.equals("Options")) {
                	PinEnablerListenerImpl pel = new PinEnablerListenerImpl(targetLocator, param);
                    String[] msg = new String[]{
                        DataCenter.getInstance().getString("pastille.unblockPIN1"),
                        DataCenter.getInstance().getString("pastille.unblockPIN2")
                    };
                    
                    PreferenceProxy.getInstance().showPinEnabler(pel, msg);
                } else {
                	IXCManager.getInstance().unboundApplicastionStart(targetLocator, param);
                }
            } else if (eventTargetType.equalsIgnoreCase(PastilleData.TARGET_TYPE_TUNER)) {
                String targetLocator = (String) hashtable.get(PastilleData.TARGET_LOCATOR);
                IXCManager.getInstance().channelSelect(targetLocator);
            } else {
                return;
            }
        }
    }

    /**
     * get icon state.
     * @return iconState is current icon state.
     */
    public int getIconState() {
        return iconState;
    }

    /**
     * set icon state.
     * @param state is icon state
     */
    public void setIconState(int state) {
        Log.printDebug("IconState = " + state);
        if (SCHEDULE_UPDATE != iconState || ICON_ACTIVE != state) {
            iconState = state;
        }
    }

    /**
     * get current channel name.
     * @return currentChannelName
     */
    public static String getCurrentChannelName() {
        return currentChannelName;
    }

    /**
     * If channel is changed, set current channel name.
     * @param channelName is channel name.
     */
    public static void setCurrentChannelName(String channelName) {
        currentChannelName = channelName;
    }

    /**
     * get TV state.
     * @return tvState
     */
    public static int getTvState() {
        return tvState;
    }

    /**
     * set tv state.
     * @param state is tv state.
     */
    public static void setTvState(int state) {
        tvState = state;
    }

    public PastilleData getOriginalPastilleData() {
        return originalPastilleData;
    }
    
    // add for VDTRMASTER-5552
    public class PinEnablerListenerImpl implements PinEnablerListener {
        private String targetAppName;
        private String[] targetAppParams;
        
        public PinEnablerListenerImpl(String targetAppName, String[] targetAppParams) {
            this.targetAppName = targetAppName;
            this.targetAppParams = targetAppParams;
        }
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            if (response == PreferenceService.RESPONSE_SUCCESS) {
            	IXCManager.getInstance().unboundApplicastionStart(targetAppName, targetAppParams);
            }
        }
    }
}

// $Header: /home/cvs/illico2/aod_test/app/com/alticast/aod/main/AodTestMain.java,v 1.1 2013/01/31 22:16:45 wslee Exp $

/*
 *  AodTestMain.java	$Revision: 1.1 $ $Date: 2013/01/31 22:16:45 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.alticast.aod.main;

import java.awt.event.KeyEvent;
import java.rmi.NotBoundException;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.xlet.XletContext;

import org.dvb.event.UserEvent;
import org.dvb.io.ixc.IxcRegistry;
import org.havi.ui.event.HRcEvent;

import com.alticast.aod.data.DataManager;
import com.alticast.aod.ui.TestScene;
import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;

public class AodTestMain implements LayeredKeyHandler {

	private TestScene scene;

	private String[] displayName = null;
	private String[] applicationName = null;

	private int size = 0;
	private int currentFocus = 0;

	private MonitorService monitorService = null;

	private XletContext xletContext = null;

	private static final String LOG_HEADER = "[AodTestMain] ";

	public AodTestMain(XletContext xlet) {

		this.xletContext = xlet;

		lookupMonitor();

		Hashtable applications = DataManager.getInstance().getApplicationInfo();

		size = applications.size();
		displayName = new String[size];
		applicationName = new String[size];

		int count = 0;
		Enumeration keys = applications.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) applications.get(key);

			displayName[count] = key;
			applicationName[count] = value;
			Log.printInfo(LOG_HEADER + " TEST KEY=" + displayName[count] + ", v=" + applicationName[count]);
			count++;
		}

		scene = new TestScene(this, displayName);
		scene.showScene();
	}
	
	public void dispose() {
		Log.printInfo(LOG_HEADER + "dispose()");
		scene.dispose();
	}

	/**
	 * Implements the Layered UI's key event handler.
	 * 
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "handleKeyEvent() event=" + event + ", currentFocus=" + currentFocus);
		}

		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = event.getCode();

		switch (keyCode) {
		case HRcEvent.VK_ENTER:
			try {
				Log.printInfo(LOG_HEADER + " startUnboundApplication() " + applicationName[currentFocus]);
				monitorService.startUnboundApplication(applicationName[currentFocus], null);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		case HRcEvent.VK_DOWN:
			if (currentFocus >= (size - 1)) {
				currentFocus = 0;
			} else {
				currentFocus++;
			}
			scene.setFocus(currentFocus);
			return true;
		case HRcEvent.VK_UP:
			if (currentFocus == 0) {
				currentFocus = size - 1;
			} else {
				currentFocus--;
			}
			scene.setFocus(currentFocus);
			return true;
		}

		return false;
	}

	/**
	 * It finds the Monitor application's IXC interface.
	 */
	private void lookupMonitor() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "lookupMonitor");
		}
		Thread monitorThread = new Thread("LookupMonitor") {
			public void run() {

				String name = "/1/1/" + MonitorService.IXC_NAME;

				while (true) {
					try {
						monitorService = (MonitorService) IxcRegistry.lookup(xletContext, name);
					} catch (NotBoundException notbound) {
						Log.printInfo(LOG_HEADER + "not bound - Monitor IXC");
						try {
							Thread.sleep(5 * 1000L);
						} catch (Exception e) {
							Log.print(e);
						}
					} catch (Exception e) {
						Log.print(e);
						break;
					}

					if (monitorService != null) {
						if (Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "Found Monitor IXC");
						}
						break;
					}
				}
			}
		};
		monitorThread.start();
	}

}

/*
 * $Log: AodTestMain.java,v $
 * Revision 1.1  2013/01/31 22:16:45  wslee
 * [vt/aod_test] Test app
 *
 */
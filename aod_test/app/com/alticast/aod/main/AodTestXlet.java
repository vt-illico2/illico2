// $Header: /home/cvs/illico2/aod_test/app/com/alticast/aod/main/AodTestXlet.java,v 1.2 2013/03/25 07:59:38 wslee Exp $

/*
 *  AodTestXlet.java	$Revision: 1.2 $ $Date: 2013/03/25 07:59:38 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.alticast.aod.main;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

public class AodTestXlet implements Xlet, ApplicationConfig {
	
	
	private AodTestMain aodTest; 

	/** The XletContext. */
    private XletContext xletContext;

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);        
    }
    
    public void init() {
        aodTest = new AodTestMain(xletContext);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("AodTest: startXlet");
        }
        FrameworkMain.getInstance().start();
        //MonitorMain.getInstance().start();
        
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("AodTest: pauseXlet");
        }
        FrameworkMain.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("AodTest: destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        aodTest.dispose();
    }

    // //////////////////////////////////////////////////////////////////////////
    // ApplicationConfig implementation
    // //////////////////////////////////////////////////////////////////////////

    /**
     * Returns the version of Application.
     * @return version string.
     */
    public String getVersion() {
        return "1.0.0.0";
    }

    /**
     * Returns the name of Application.
     * @return application name.
     */
    public String getApplicationName() {
        return "AodTest";
    }

    /**
     * Returns the Application's XletContext.
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

/*
 * $Log: AodTestXlet.java,v $
 * Revision 1.2  2013/03/25 07:59:38  wslee
 * [vt/aod_test] ApplicationConfig#init() ����
 *
 * Revision 1.1  2013/01/31 22:16:45  wslee
 * [vt/aod_test] Test app
 *
 */
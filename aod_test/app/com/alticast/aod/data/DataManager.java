// $Header: /home/cvs/illico2/aod_test/app/com/alticast/aod/data/DataManager.java,v 1.1 2013/01/31 22:16:45 wslee Exp $

/*
 *  DataManager.java	$Revision: 1.1 $ $Date: 2013/01/31 22:16:45 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.alticast.aod.data;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.videotron.tvi.illico.log.Log;

public class DataManager {

	private static DataManager instance = null;
	
	private Hashtable hash;
	
	private static final String LOG_HEADER = "[DataManager] ";
	
	public synchronized static DataManager getInstance() {
		if(instance == null) {
			instance = new DataManager();
		}
		return instance;
	}
	
	private DataManager() {
		Log.printInfo(LOG_HEADER + "DataManager()");
		hash = new Hashtable();
		
		Properties props = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(".resource/application.prop");
            props.load(fis);
            
            if (props != null) {
                Enumeration en = props.propertyNames();
                while (en.hasMoreElements()) {
                    Object key = en.nextElement();
                    hash.put(key.toString(), props.get(key));
                    Log.printInfo(LOG_HEADER + "App : key=" + key + ", value=" + props.get(key));
                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            props = null;
        }
        finally {
            try {
                fis.close();
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
	}
	
	public Hashtable getApplicationInfo() {
		return hash;
	}
}

/*
 * $Log: DataManager.java,v $
 * Revision 1.1  2013/01/31 22:16:45  wslee
 * [vt/aod_test] Test app
 *
 */
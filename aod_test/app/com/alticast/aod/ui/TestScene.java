// $Header: /home/cvs/illico2/aod_test/app/com/alticast/aod/ui/TestScene.java,v 1.1 2013/01/31 22:16:45 wslee Exp $

/*
 *  TestScene.java	$Revision: 1.1 $ $Date: 2013/01/31 22:16:45 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.alticast.aod.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.WindowProperty;

public class TestScene extends LayeredWindow {

	public static final long serialVersionUID = 2012122600L;

	private LayeredUI rootWindow;
	
	private String[] menuNames;
	
	private int currentFocus = 0;

	public TestScene(LayeredKeyHandler keyHandler, String[] menus) {
		
		this.menuNames = menus;
		
		setBounds(Constants.SCREEN_BOUNDS);
		rootWindow = LayeredUIManager.getInstance().createLayeredDialog(WindowProperty.TERMINAL_BLOCKED_TIME.getName(),
				WindowProperty.TERMINAL_BLOCKED_TIME.getPriority(), false, this, Constants.SCREEN_BOUNDS, keyHandler);

		setVisible(true);
	}

	public void showScene() {
		rootWindow.activate();
	}
	
	/**
	 * Disposes the popup.
	 */
	public void dispose() {
		LayeredUIManager.getInstance().disposeLayeredUI(rootWindow);
	}
	
	private static final int START_X = 960/2;
	private static final int START_Y = 100;
	
	private static final int MENU_HEIGHT = 50;
	private static final int MENU_WIDTH = 200;
	
	private static final Color NORMAL_COLOR = Color.black;
	
	private static final Color FOCUS_COLOR = Color.yellow;
	
	private static final Color OUT_LINE = Color.white;
	
	private static final Color FONT_FOCUS_COLOR = Color.black;
	
	private static final Font BLENDER18 = FontResource.BLENDER.getFont(18);
	
	public void paint(Graphics g) {
		
		for(int i=0; i<menuNames.length; i++) {
			Color backColor = null;
			Color fontColor = null;
			if(currentFocus == i) {
				backColor = FOCUS_COLOR;
				fontColor = FONT_FOCUS_COLOR;
			}
			else {
				backColor = NORMAL_COLOR;
				fontColor = OUT_LINE;
			}
			g.setColor(backColor);
			g.fillRect(START_X, START_Y + (i*MENU_HEIGHT), MENU_WIDTH, MENU_HEIGHT);
			g.setColor(OUT_LINE);
			g.drawRect(START_X, START_Y + (i*MENU_HEIGHT), MENU_WIDTH, MENU_HEIGHT);
			
			g.setFont(BLENDER18);
			g.setColor(fontColor);
			
			g.drawString(menuNames[i], START_X + 15, (START_Y + (i*MENU_HEIGHT) + 30));
		}
		
	}
	
	public void setFocus(int focus) {
		currentFocus = focus;
		repaint();
	}

}

/*
 * $Log: TestScene.java,v $
 * Revision 1.1  2013/01/31 22:16:45  wslee
 * [vt/aod_test] Test app
 *
 */
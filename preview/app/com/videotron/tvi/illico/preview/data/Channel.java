/*
 *  @(#)Channel.java 1.0 2011.03.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.data;

import java.awt.Image;

import com.videotron.tvi.illico.ixc.epg.TvChannel;

/**
 * This class is Channel structure.
 *
 * @since 2011.03.18
 * @version $Revision: 1.8 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public class Channel {
    /** The Constant VISUAL_SD. */
    public static final String VISUAL_SD = "SD";
    /** The Constant VISUAL_HD. */
    public static final String VISUAL_HD = "HD";
    /** The Constant VISUAL_3D. */
    public static final String VISUAL_3D = "3D";
    /** The Constant VISUAL_UHD. */
    public static final String VISUAL_UHD = "UHD";

    /** The Constant STATUS_NEW. */
    public static final String STATUS_NEW = "new";
    /** The Constant STATUS_OLD. */
    public static final String STATUS_OLD = "old";
    
    /** The Constant SUBSCRIBE_COMPLETED. */
    public static final int SUBSCRIBE_COMPLETED = 0;
    /** The Constant SUBSCRIBE_INSTANT. */
    public static final int SUBSCRIBE_INSTANT = 1;
    /** The Constant SUBSCRIBED_CYO. */
    public static final int SUBSCRIBE_CYO = 2;

    /** The id. */
    private String id = null;
    /** The description english. */
    private String descEng = null;
    /** The description french. */
    private String descFr = null;
    /** The name. */
    private String name = null;
    /** The preview image url. */
    private String previewImgUrl = null;
    /** The brand image url. */
    private String brandImgUrl = null;
    /** The start date. */
    private String startDate = null;
    /** The end date. */
    private String endDate = null;
    /** The visual type. */
    private String visualType = null;
    /** The sub description english. */
    private String subDescEng = null;
    /** The sub description french. */
    private String subDescFr = null;
    /** The status. */
    private String status = null;
    /** The preview img. */
    private Image previewImg = null;
    /** The brand img. */
    private Image brandImg = null;
    /** The TvChannel gained from EPGService. */
    private TvChannel tvCh = null;
    /** The shortcut desc. */
    private String shortcutDescEn = null;
    /** The shortcut desc. */
    private String shortcutDescFr = null;
    /** whether try to get preview image from server. */
    private boolean isTryPreviewImg = false;
    /** whether try to get brand image from server. */
    private boolean isTryBrandImg = false;    
    /** The subscription state. */
    private int subscriptionState = SUBSCRIBE_COMPLETED;

    /**
     * @return the isTryPreviewImg
     */
    public boolean isTryPreviewImg() {
        return isTryPreviewImg;
    }
    /**
     * @return the isTryBrandImg
     */
    public boolean isTryBrandImg() {
        return isTryBrandImg;
    }
    /**
     * @return the shortcutDescFr
     */
    public String getShortcutDescFr() {
        return shortcutDescFr;
    }
    /**
     * @param desc the shortcutDescFr to set
     */
    public void setShortcutDescFr(String desc) {
        this.shortcutDescFr = desc;
    }
    /**
     * @return the shortcutDesc
     */
    public String getShortcutDescEn() {
        return shortcutDescEn;
    }
    /**
     * @param desc the shortcutDesc to set
     */
    public void setShortcutDescEn(String desc) {
        this.shortcutDescEn = desc;
    }
    /**
     * @return the tvCh
     */
    public TvChannel getTvCh() {
        return tvCh;
    }
    /**
     * @param ch the tvCh to set
     */
    public void setTvCh(TvChannel ch) {
        this.tvCh = ch;
    }
    /**
     * @return the previewImg
     */
    public Image getPreviewImg() {
        return previewImg;
    }
    /**
     * @param img the previewImg to set
     */
    public void setPreviewImg(Image img) {
        this.previewImg = img;
        isTryPreviewImg = true;
    }
    /**
     * @return the brandImg
     */
    public Image getBrandImg() {
        return brandImg;
    }
    /**
     * @param img the brandImg to set
     */
    public void setBrandImg(Image img) {
        this.brandImg = img;
        isTryBrandImg = true;
    }
    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }
    /**
     * @param chStatus the status to set
     */
    public void setStatus(String chStatus) {
        this.status = chStatus;
    }
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }
    /**
     * @param chId the id to set
     */
    public void setId(String chId) {
        this.id = chId;
    }
    /**
     * @return the descEng
     */
    public String getDescEng() {
        return descEng;
    }
    /**
     * @param desc the descEng to set
     */
    public void setDescEng(String desc) {
        this.descEng = desc;
    }
    /**
     * @return the descFr
     */
    public String getDescFr() {
        return descFr;
    }
    /**
     * @param desc the descFr to set
     */
    public void setDescFr(String desc) {
        this.descFr = desc;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param chName the name to set
     */
    public void setName(String chName) {
        this.name = chName;
    }
    /**
     * @return the previewImgUrl
     */
    public String getPreviewImgUrl() {
        return previewImgUrl;
    }
    /**
     * @param imgUrl the previewImgUrl to set
     */
    public void setPreviewImgUrl(String imgUrl) {
        this.previewImgUrl = imgUrl;
    }
    /**
     * @return the brandImgUrl
     */
    public String getBrandImgUrl() {
        return brandImgUrl;
    }
    /**
     * @param imgUrl the brandImgUrl to set
     */
    public void setBrandImgUrl(String imgUrl) {
        this.brandImgUrl = imgUrl;
    }
    /**
     * @return the startDate
     */
    public String getStartDate() {
        return startDate;
    }
    /**
     * @param date the startDate to set
     */
    public void setStartDate(String date) {
        this.startDate = date;
    }
    /**
     * @return the endDate
     */
    public String getEndDate() {
        return endDate;
    }
    /**
     * @param date the endDate to set
     */
    public void setEndDate(String date) {
        this.endDate = date;
    }
    /**
     * @return the visualType
     */
    public String getVisualType() {
        return visualType;
    }
    /**
     * @param type the visualType to set
     */
    public void setVisualType(String type) {
        this.visualType = type;
    }
    /**
     * @return the subDescEng
     */
    public String getSubDescEng() {
        return subDescEng;
    }
    /**
     * @param desc the subDescEng to set
     */
    public void setSubDescEng(String desc) {
        this.subDescEng = desc;
    }
    /**
     * @return the subDescFr
     */
    public String getSubDescFr() {
        return subDescFr;
    }
    /**
     * @param desc the subDescFr to set
     */
    public void setSubDescFr(String desc) {
        this.subDescFr = desc;
    }
    /**
     * @return the subscriptionState
     */
    public int getSubscriptionState() {
        return subscriptionState;
    }
    /**
     * @param subscriptionState the subscriptionState to set
     */
    public void setSubscriptionState(int subscriptionState) {
        this.subscriptionState = subscriptionState;
    }
}

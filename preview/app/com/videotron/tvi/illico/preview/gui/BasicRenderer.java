/*
 *  @(#)BasicRenderer.java 1.0 2011.03.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.ui.ChListUI;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>BasicRenderer</code> This class the basic renderer class for each renderer.
 * Each renderder has to inherit this class as super class.
 * @since   2011.03.19
 * @version $Revision: 1.11 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class BasicRenderer extends Renderer {
    /** The clock bg img. */
//    private static Image clockBgImg = null;
    /** The clock img. */
    private static Image clockImg = null;
    /** The logo img. */
    private static Image logoImg = null;
    /** The logo part img. */
    private static Image logoPartImg = null;
    /** The btn back img. */
    public static Image btnBackImg = null;
    /** The btn page img. */
    public static Image btnPageImg = null;
    /** The btn exit img. */
    public static Image btnExitImg = null;
    /** The btn slash img. */
    //private static Image btnSlashImg = null;
    /** The hotkeybg. */
    public static Image hotkeybgImg = null;
    /** The menu1 bg. */
    protected static Image menu1BgImg = null;
    /** The menu2 bg. */
    protected static Image menu2BgImg = null;
    /** The menu shadow. */
//    protected static Image menuShadowImg = null;
    /** The menu grow. */
//    protected static Image menuGrowImg = null;
    /** The menu line. */
    protected static Image menuLineImg = null;
    /** The menu arrow icon. */
    protected static Image menuArrIconImg = null;
    /** The menu focus. */
    protected static Image menuFocusImg = null;
    /** The menu middle bg img. */
    protected static Image menuMiddleBgImg = null;
    /** The scroll shadow. */
//    protected static Image scrollShadowImg = null;
    /** The icon new english img. */
    public static Image iconNewEnImg = null;
    /** The icon new french img. */
    public static Image iconNewFrImg = null;
    /** The icon favor img. */
    protected static Image iconFavorImg = null;
    /** The icon hd img. */
    protected static Image iconHDImg = null;    
    /** The icon is img. */
    protected static Image iconISAImg = null;    
    /** The icon is focus img. */
    protected static Image iconISAFocusImg = null;
    /** The icon for UHD. */
    protected static Image iconUHDImg = null;

    /** The location title txt. */
    private static String locTitleTxt = null;
    /** The location title txt for ch list. */
    private static String locTitleChListTxt = null;
    /** The btn back txt. */
    public static String btnBackTxt = null;
    /** The btn scroll txt. */
    public static String btnScrollTxt = null;
    /** The btn exit txt. */
    public static String btnExitTxt = null;
    /** The 3D description txt. */
    protected static String desc3DTxt = null;
    
    /** The menu tune txt. */
    protected static String menuTuneTxt = null;    
    /** The menu subscribe channel txt. */
    protected static String menuSubscribeChannelTxt = null;    
    /** The menu change package txt. */
    protected static String menuChangePackageTxt = null;

    /**
     * Instantiates a new basic renderer.
     */
    public BasicRenderer() { }

    /**
     * Gets the string value of UI from DataCenter. If the value is null, return a empty string as "".
     *
     * @param key the key
     * @return the string
     */
    public String getString(String key) {
        if (key == null) { return ""; }
        String value = DataCenter.getInstance().getString("renderer." + key);
        if (value != null) { return value; }
        return "";
    }

    /**
     * Gets the image from DataCenter.
     *
     * @param imageName the image name
     * @return the image
     */
    public Image getImage(String imageName) {
        return DataCenter.getInstance().getImage(imageName);
    }

    /**
     * remove the image from DataCenter.
     *
     * @param imageName the image name
     */
    public void removeImage(String imageName) {
        DataCenter.getInstance().removeImage(imageName);
    }
    
    /**
     * Gets the subscription txt.
     *
     * @param subscriptionState the subscription state
     * @return the subscription txt
     */
    protected String getSubscriptionTxt(int subscriptionState) {
        if (subscriptionState == Channel.SUBSCRIBE_INSTANT) {
            return menuSubscribeChannelTxt;
        } else if (subscriptionState == Channel.SUBSCRIBE_CYO) {
            return menuChangePackageTxt;
        } else { return menuTuneTxt; }
    }

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the UIComponent
     */
    public void prepare(UIComponent c) {
        hotkeybgImg = getImage("01_hotkeybg.png");
//        clockBgImg = getImage("clock_bg.png");
        clockImg = getImage("clock.png");
        logoImg = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        if (logoImg == null) { logoImg = getImage("logo_en.png"); }
        logoPartImg = getImage("his_over.png");
        Hashtable imgTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        if (imgTable != null && !imgTable.isEmpty()) {
            btnBackImg = (Image) imgTable.get(PreferenceService.BTN_BACK);
            btnPageImg = (Image) imgTable.get(PreferenceService.BTN_PAGE);
            btnExitImg = (Image) imgTable.get(PreferenceService.BTN_EXIT);
        } else {
            btnBackImg = getImage("b_btn_back.png");
            btnPageImg = getImage("b_btn_page.png");
            btnExitImg = getImage("b_btn_exit.png");
        }
        //btnSlashImg = getImage("btn_slash.png");

        menu1BgImg = getImage("01_acbg_1st.png");
        menu2BgImg = getImage("01_acbg_2nd.png");
//        menuShadowImg = getImage("01_ac_shadow.png");
//        menuGrowImg = getImage("01_acglow.png");
        menuLineImg = getImage("01_acline.png");
        menuArrIconImg = getImage("02_detail_ar.png");
        menuFocusImg = getImage("02_detail_bt_foc.png");
        menuMiddleBgImg = getImage("01_acbg_m.png");
//        scrollShadowImg = getImage("18_de_sha.png");
        iconNewEnImg = getImage("18_fpv_new_en.png");
        iconNewFrImg = getImage("18_fpv_new_fr.png");
        iconFavorImg = getImage("02_icon_fav.png");
        iconHDImg = getImage("icon_hd.png");
        iconISAImg = getImage("02_icon_isa_ac.png");
        iconISAFocusImg = getImage("02_icon_isa_bl.png");
        iconUHDImg = getImage("icon_uhd.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();

        locTitleTxt = getString("locTitle");
        locTitleChListTxt = getString("locTitleChList");
        btnBackTxt = getString("back");
        btnScrollTxt = getString("scrollDesc");
        desc3DTxt = getString("desc3D");
        btnExitTxt = getString("exit");
        
        menuTuneTxt = getString("menuTune");
        menuSubscribeChannelTxt = getString("menuSubscribeChannel");
        menuChangePackageTxt = getString("menuChangePackage");
    }

    /**
     * Get the renderer bound of UI .
     *
     * @param c the UIComponent
     * @return the preferred bounds
     */
    public Rectangle getPreferredBounds(UIComponent c) {
        return Rs.SCENE_BOUND;
    }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(logoImg, 53, 21, c);
        int pos = 53 + logoImg.getWidth(c) + 15;
        g.drawImage(logoPartImg, pos, 45, c);
        pos += logoPartImg.getWidth(c) + 11;
        g.setFont(Rs.F18);
        g.setColor(Rs.C236);
        String locTitle = locTitleTxt;
        if (this instanceof ChListRenderer) { locTitle = locTitleChListTxt; }
        g.drawString(locTitle, pos, 57);
//        g.drawImage(clockBgImg, 749, 37, c);
        //g.drawImage(clockImg, 742, 44, c);

        Formatter formatter = Formatter.getCurrent();
        int longDateWth = 0;
        if (formatter != null) {
            String longDate = formatter.getLongDate();
            if (longDate != null) {
                g.setFont(Rs.F17);
                g.setColor(Rs.C255);
                longDateWth = g.getFontMetrics().stringWidth(longDate);
                g.drawString(longDate, 910 - longDateWth, 57);
                //GraphicUtil.drawStringRight(g, longDate, 910, 57);
            }
        }
        if (clockImg != null) {
            int imgClockWth = clockImg.getWidth(c);
            g.drawImage(clockImg, 910 - longDateWth - 4 - imgClockWth, 44, c);
        }

        /*int btnGap = 6;
        g.setFont(Rs.F17);
        g.setColor(Rs.C241);
        FontMetrics fm = g.getFontMetrics();
        Image[] btnImgs = {btnBackImg, btnPageImg};
        String[] btnTxt = {btnBackTxt, btnScrollTxt};
        pos = 910;
        for (int i = btnImgs.length - 1; i > -1; i--) {
            if (btnTxt[i] != null && btnTxt[i].length() > 0) {
                pos -= fm.stringWidth(btnTxt[i]);
                g.drawString(btnTxt[i], pos, 503);
                pos -= btnGap;
            }
            if (btnImgs[i] != null) {
                pos -= btnImgs[i].getWidth(c);
                g.drawImage(btnImgs[i], pos, 488, c);
                pos -= btnGap;
            }
            if (i > 0) {
                pos -= btnSlashImg.getWidth(c);
                g.drawImage(btnSlashImg, pos, 492, c);
                pos -= btnGap;
            }
        }*/
    }
    
    /**
     * Paint menu icon.
     *
     * @param g the Graphics
     * @param c the UIComponent
     * @param subscriptionState the subscription state
     * @param isFocus the is focus
     * @param isISAMenu the is isa menu
     * @param addedPos the added pos
     */
    protected void paintMenuIcon(Graphics g, UIComponent c, int subscriptionState, boolean isFocus, boolean isISAMenu, int addedPos) {
        if (isISAMenu && subscriptionState == Channel.SUBSCRIBE_INSTANT) {
            if (isFocus) {
                g.drawImage(iconISAFocusImg, 638, 367 + addedPos, c);
            } else { g.drawImage(iconISAImg, 638, 366 + addedPos, c); }
        } else if (!isFocus) {
            g.drawImage(menuArrIconImg, 649, 373 + addedPos, c);
        }
    }
}

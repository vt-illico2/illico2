/*
 *  @(#)OneChRenderer.java 1.0 2011.03.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.ui.ChListUI;
import com.videotron.tvi.illico.preview.ui.OneChUI;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>OneChRenderer</code> The class to paint a GUI for one channel information.
 *
 * @since   2011.03.19
 * @version $Revision: 1.9 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class OneChRenderer extends BasicRenderer {
    /** Background image. */
    private Image bgImg = null;
    /** The middle bg img. */
    private Image middleBgImg = null;
    /** The scroll shadow. */
//    public Image scrollShadowImg = null;
    /** The scroll shadow top img. */
//    public Image scrollShadowTopImg = null;
    /** The default poster img. */
    private Image defaultPosterImg = null;

    /** The menu txt. */
    public String[] menuTxt = null;

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgImg = getImage("bg.jpg");
        middleBgImg = getImage("01_vod_bgline.png");
//        scrollShadowImg = getImage("18_de_sha.png");
//        scrollShadowTopImg = getImage("18_de_sha_top.png");
        defaultPosterImg = getImage("portrait.jpg");
        super.prepare(c);

        if (menuTxt == null) {
            if (Config.showSubscribeMenu) {
                menuTxt = new String[3];
            } else { menuTxt = new String[2]; }
        }
        if (Config.showSubscribeMenu) {
            menuTxt[0] = getString("menuSubscribe");
            menuTxt[1] = getString("menuTune");
            menuTxt[2] = getString("menuViewAll");
        } else {
            menuTxt[0] = getString("menuTune");
            menuTxt[1] = getString("menuViewAll");
        }
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        OneChUI ui = (OneChUI) c;
        if (ui.ch == null) { return; }
        super.paint(g, c);

        g.drawImage(middleBgImg, 0, 250, c);
        //menu
        if (!Config.showSubscribeMenu) { g.translate(0, 42); }
        g.drawImage(menu1BgImg, 626, 355, c);
        g.drawImage(menu2BgImg, 626, 398, c);
        if (Config.showSubscribeMenu) { g.drawImage(menuMiddleBgImg, 626, 434, 284, 40, c); }
        g.drawImage(menuLineImg, 641, 395, c);
        if (Config.showSubscribeMenu) { g.drawImage(menuLineImg, 641, 433, c); }
        if (!Config.showSubscribeMenu) { g.translate(-0, -42); }
//        g.drawImage(menuGrowImg, 625, 453, c);
//        g.drawImage(menuShadowImg, 579, 465, c);
        for (int i = 0; i < menuTxt.length; i++) {
            int subscriptionState = ui.ch.getSubscriptionState();
            if (i == OneChUI.MENU_TUEN) {
                menuTxt[i] = getSubscriptionTxt(subscriptionState);
            }
            
            int addedPos = i * 39;
            if (!Config.showSubscribeMenu) { addedPos += 42; }
            //g.drawImage(menuArrIconImg, 649, 373 + addedPos, c);
            if (i == ui.menuFocus) {
                g.drawImage(menuFocusImg, 624, 355 + addedPos, c);
                g.setFont(Rs.F21);
                g.setColor(Rs.C3);
                g.drawString(menuTxt[i], 666, 382 + addedPos);
            } else {
                g.setFont(Rs.F18);
                g.setColor(Rs.C230);
                g.drawString(menuTxt[i], 671, 381 + addedPos);
            }
            paintMenuIcon(g, c, subscriptionState, (i == ui.menuFocus),
                    i == ChListUI.MENU_TUEN, addedPos);
        }

        Image img = ui.ch.getBrandImg();
        if (img == null) { img = defaultPosterImg; }
        if (img != null) { g.drawImage(img, 0, 111, 960, 208, c); }

        String str = ui.ch.getName();
        g.setFont(Rs.F24);
        g.setColor(Rs.C255203000);
        g.drawString(str, 55, 346);
        int pos = 55 + g.getFontMetrics().stringWidth(str) + 9;

        boolean isFr = Config.language.equals(Definitions.LANGUAGE_FRENCH);
        if (Channel.STATUS_NEW.equalsIgnoreCase(ui.ch.getStatus())) {
            img = iconNewEnImg;
            if (isFr) { img = iconNewFrImg; }
            g.drawImage(img, pos, 332, c);
        }

        g.drawImage(iconFavorImg, 52, 356, c);
        str = "";
        pos = 83;
        TvChannel tvCh = ui.ch.getTvCh();
        if (tvCh != null) {
            try {
                str = String.valueOf(tvCh.getNumber());
                g.setFont(Rs.F23);
                g.setColor(Rs.C254);
                if (str != null && str.length() > 0) {
                    g.drawString(str, pos, 374);
                    pos += g.getFontMetrics().stringWidth(str) + 10;
                }

                String chName = tvCh.getCallLetter();
                img = (Image) SharedMemory.getInstance().get("logo." + chName);
                if (img == null) { img = (Image) SharedMemory.getInstance().get("logo.default"); }
                if (img != null) {
                    g.drawImage(img, pos, 354, c);
                    pos += img.getWidth(c) + 13;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        g.setFont(Rs.F22);
        g.setColor(Rs.C210);
        str = ui.ch.getId();
        if (str != null && str.length() > 0) {
            g.drawString(str, pos, 374);
            pos += g.getFontMetrics().stringWidth(str) + 10;
        }
        if (Channel.VISUAL_HD.equalsIgnoreCase(ui.ch.getVisualType())) { g.drawImage(iconHDImg, pos, 361, c); }
        else if (Channel.VISUAL_UHD.equalsIgnoreCase(ui.ch.getVisualType())) {
        	g.drawImage(iconUHDImg, pos, 361, c);
        }
        
        str = ui.ch.getShortcutDescEn();
        if (isFr) { str = ui.ch.getShortcutDescFr(); }
        if (str != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C224);
            g.drawString(TextUtil.shorten(str, g.getFontMetrics(), 588 - 56), 56, 398);
        }
    }
}

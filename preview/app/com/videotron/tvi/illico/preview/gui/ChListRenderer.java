/*
 *  @(#)ChListRenderer.java 1.0 2011.03.20
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.gui;

import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.ui.ChListUI;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ChListRenderer</code> The class to paint a GUI for channel list information.
 *
 * @since   2011.03.20
 * @version $Revision: 1.10 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class ChListRenderer extends BasicRenderer {
    /** Background image. */
    private Image bgImg = null;
    /** The scroll shadow. */
//    public Image scrollShadowImg = null;
    /** The scroll shadow top img. */
//    public Image scrollShadowTopImg = null;
    /** The list line img. */
    public Image listLineImg = null;
    /** The list bullet img. */
    public Image listBulletImg = null;
    /** The list focus img. */
    public Image listFocusImg = null;
    /** The poster shadow img. */
//    private Image posterShadowImg = null;
    /** The text new english img. */
    private Image txtNewEnImg = null;
    /** The text new french img. */
    private Image txtNewFrImg = null;
    /** The list arrow top img. */
    public Image listArrowTopImg = null;
    /** The list arrow bottom img. */
    public Image listArrowBottomImg = null;
    /** The icon new english img. */
    public Image iconNewEnFocusImg = null;
    /** The icon new french img. */
    public Image iconNewFrFocusImg = null;
    /** The default poster img. */
    private Image defaultPosterImg = null;

    /** The menu txt. */
    public String[] menuTxt = null;
    /** The list title txt. */
    public String listTitleTxt = null;    

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c the c
     */
    public void prepare(UIComponent c) {
        bgImg = getImage("07_bg_main.jpg");
//        scrollShadowImg = getImage("18_maint_sha.png");
//        scrollShadowTopImg = getImage("18_maint_sha_top.png");
        listLineImg = getImage("00_mini_line.png");
        listBulletImg = getImage("01_bullet01.png");
        listFocusImg = getImage("00_menufocus.png");
//        posterShadowImg = getImage("18_main_img_sha.png");
        txtNewEnImg = getImage("icon_new_en.png");
        txtNewFrImg = getImage("icon_new_fr.png");
        listArrowTopImg = getImage("02_ars_t.png");
        listArrowBottomImg = getImage("02_ars_b.png");
        iconNewEnFocusImg = getImage("18_fpv_new_en_f.png");
        iconNewFrFocusImg = getImage("18_fpv_new_fr_f.png");
        defaultPosterImg = getImage("FPV_default.png");
        super.prepare(c);

        if (menuTxt == null) {
            if (Config.showSubscribeMenu) {
                menuTxt = new String[3];
            } else { menuTxt = new String[2]; }
        }
        
        if (Config.showSubscribeMenu) {
            menuTxt[0] = getString("menuSubscribe");
            menuTxt[1] = menuTuneTxt;
            menuTxt[2] = getString("menuAboutCh");
        } else {
            menuTxt[0] = menuTuneTxt;
            menuTxt[1] = getString("menuAboutCh");
        }
        
        listTitleTxt = getString("chList");
    }

    /**
     * This will be called when scene is stopped to fulsh images.
     */
    public void stop() { }

    /**
     * Graphics paint.
     *
     * @param g the Graphics
     * @param c the UIComponent
     */
    protected void paint(Graphics g, UIComponent c) {
        g.drawImage(bgImg, 0, 0, c);
        super.paint(g, c);
        ChListUI ui = (ChListUI) c;
        if (ui.ch == null) { return; }
        g.drawImage(hotkeybgImg, 236, 466, c);

        int stateFocus = ui.stateFocus;
        //menu
        if (!Config.showSubscribeMenu) { g.translate(0, 42); }
        g.drawImage(menu1BgImg, 626, 355, c);
        g.drawImage(menu2BgImg, 626, 398, c);
        if (Config.showSubscribeMenu) { g.drawImage(menuMiddleBgImg, 626, 434, 284, 40, c); }
        g.drawImage(menuLineImg, 641, 395, c);
        if (Config.showSubscribeMenu) { g.drawImage(menuLineImg, 641, 433, c); }
        if (!Config.showSubscribeMenu) { g.translate(-0, -42); }
//        g.drawImage(menuGrowImg, 625, 453, c);
//        g.drawImage(menuShadowImg, 579, 465, c);
        for (int i = 0; i < menuTxt.length; i++) {
            int subscriptionState = ui.ch.getSubscriptionState();
            if (i == ChListUI.MENU_TUEN) {
                menuTxt[i] = getSubscriptionTxt(subscriptionState);
            }
            
            int addedPos = i * 39;
            if (!Config.showSubscribeMenu) { addedPos += 42; }
            //g.drawImage(menuArrIconImg, 649, 373 + addedPos, c);
            if (i == ui.menuFocus && stateFocus == ChListUI.STATE_MENU) {
                g.drawImage(menuFocusImg, 624, 355 + addedPos, c);
                g.setFont(Rs.F21);
                g.setColor(Rs.C3);
                g.drawString(menuTxt[i], 666, 382 + addedPos);
            } else {
                g.setFont(Rs.F18);
                g.setColor(Rs.C230);
                g.drawString(menuTxt[i], 671, 381 + addedPos);
            }
            
            paintMenuIcon(g, c, subscriptionState, (i == ui.menuFocus && stateFocus == ChListUI.STATE_MENU),
                    i == ChListUI.MENU_TUEN, addedPos);
        }

//        g.drawImage(posterShadowImg, 349, 306, c);
        Image img = ui.ch.getPreviewImg();
        if (img == null) { img = defaultPosterImg; }
        if (img != null) { g.drawImage(img, 371, 101, 162, 216, c); }
        boolean isFr = Config.language.equals(Definitions.LANGUAGE_FRENCH);
        if (Channel.STATUS_NEW.equalsIgnoreCase(ui.ch.getStatus())) {
            img = txtNewEnImg;
            if (isFr) { img = txtNewFrImg; }
            g.drawImage(img, 478, 101, c);
        }

        String str = ui.ch.getName();
        g.setFont(Rs.F24);
        g.setColor(Rs.C255203000);
        g.drawString(TextUtil.shorten(str, g.getFontMetrics(), 908 - 554), 554, 117);

        g.drawImage(iconFavorImg, 551, 128, c);
        str = "";
        int pos = 582;
        TvChannel tvCh = ui.ch.getTvCh();
        if (tvCh != null) {
            try {
                str = String.valueOf(tvCh.getNumber());
                g.setFont(Rs.F23);
                g.setColor(Rs.C254);
                if (str != null && str.length() > 0) {
                    g.drawString(str, pos, 146);
                    pos += g.getFontMetrics().stringWidth(str) + 10;
                }

                String chName = tvCh.getCallLetter();
                img = (Image) SharedMemory.getInstance().get("logo." + chName);
                if (img == null) { img = (Image) SharedMemory.getInstance().get("logo.default"); }
                if (img != null) {
                    g.drawImage(img, pos, 125, c);
                    pos += img.getWidth(c) + 13;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        g.setFont(Rs.F22);
        g.setColor(Rs.C254);
        str = ui.ch.getId();
        if (str != null && str.length() > 0) {
            g.drawString(str, pos, 146);
            pos += g.getFontMetrics().stringWidth(str) + 10;
        }
        //if (!Channel.VISUAL_SD.equalsIgnoreCase(ui.ch.getVisualType())) {
        if (Channel.VISUAL_HD.equalsIgnoreCase(ui.ch.getVisualType())) { g.drawImage(iconHDImg, pos, 133, c); }
        else if (Channel.VISUAL_UHD.equalsIgnoreCase(ui.ch.getVisualType())) {
        	g.drawImage(iconUHDImg, pos, 133, c);
        }
        
        str = ui.ch.getShortcutDescEn();
        if (isFr) { str = ui.ch.getShortcutDescFr(); }
        if (str != null) {
            g.setFont(Rs.F18);
            g.setColor(Rs.C224);
            g.drawString(TextUtil.shorten(str, g.getFontMetrics(), 900 - 554), 554, 183);
        }

        //list
        g.setFont(Rs.F20);
        g.setColor(Rs.C46);
        g.drawString(listTitleTxt, 66, 95);
        g.setColor(Rs.C214182061);
        g.drawString(listTitleTxt, 65, 94);
    }
}

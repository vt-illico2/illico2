/*
 *  @(#)SceneManager.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.ui;

import java.awt.Container;
import java.awt.event.KeyEvent;
import java.util.Vector;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.preview.controller.communication.ISAProxy;
import com.videotron.tvi.illico.preview.controller.vbm.VbmController;
import com.videotron.tvi.illico.preview.ui.BasicUI;
import com.videotron.tvi.illico.preview.ui.ChListUI;
import com.videotron.tvi.illico.preview.ui.OneChUI;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class is the SceneManager class.
 *
 * @version $Revision: 1.9 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public final class SceneManager implements LayeredKeyHandler {
    /** The Constant NUM_SCENE. */
    private static final int NUM_SCENE = 2;
    /** The Constant SC_INVALID. */
    public static final int SC_INVALID = -1;
    /** The Constant SC_ONE_CH. */
    public static final int SC_ONE_CH = 0;
    /** The Constant SC_CH_LIST. */
    public static final int SC_CH_LIST = 1;

    /** Instance of SceneManager. */
    private static SceneManager instance = null;
    /** The LayeredUIHandler. */
    private LayeredUIHandler layeredHandler = null;
    /** The root container. */
    private Container rootContainer = null;

    /** scene list. */
    public BasicUI[] scenes = null;
    /** is a current Scene ID. */
    public int curSceneId = SC_INVALID;

    /**
     * Save scene historys.
     * included previous scene id, datas.
     */
    private Vector history = new Vector();

    /**
     * Gets the singleton instance of SceneManager.
     * @return SceneManager
     */
    public static synchronized SceneManager getInstance() {
        if (instance == null) { instance = new SceneManager(); }
        return instance;
    }

    /**
     * Constructor.
     */
    private SceneManager() {
    }

    /**
     * Initialize a SceneManager.
     */
    public void init() {
        layeredHandler = new LayeredUIHandler(this);
        rootContainer = layeredHandler.getLayeredContainer();
        scenes = new BasicUI[NUM_SCENE];
        curSceneId = SC_INVALID;
    }

    /**
     * start the SceneManage.
     */
    public void start() {
        Logger.info(this, "called start()");
        ISAProxy.getInstance().printList();
        history.clear();
        layeredHandler.activate();
        //printAllWindows();
        final Object[] historyData = {String.valueOf(SC_INVALID), null, null};
        String[] param = CommunicationManager.getInstance().getParameter();
        VbmController.getInstance().writeStartLog(param);
        if (param == null || param.length < 2 || param[1] == null) {
            if (Environment.EMULATOR) {
                new Thread("start()") {
                    public void run() {
                        try { Thread.sleep(15000); } catch (Exception e) { e.printStackTrace(); }
                        goToScene(SC_CH_LIST, true, historyData);
                    }
                } .start();
            } else { goToScene(SC_CH_LIST, true, historyData); }
        } else {
            historyData[1] = param[1];
            goToScene(SC_ONE_CH, true, historyData);
        }
        //layeredHandler.activate();
    }

    /*private void printAllWindows() {
        Logger.debug(this, "printAllWindow()");
       Container root = rootContainer;
        Container temp2 = null;
        while(true) {
              temp2 = root.getParent();
              if(temp2 == null) {
                     break;
              }
              root = temp2;
        }
        Logger.debug(this, "Root=" + root.toString());
        printAllVisibleContainer(root, 0);
    }
    private void printAllVisibleContainer(Container cont, int depth) {
        int count = cont.getComponentCount();
        String tab = "";
        for(int i=0; i<depth; i++) {
              tab += "++++";
        }
        for(int i=0; i<count; i++) {
              Component c = cont.getComponent(i);
              if(c.isVisible()) {
                  Logger.debug(this, tab + "[Visible] " + c);
                  if (c instanceof Container) { printAllVisibleContainer((Container)c, depth + 1); }
              }
              else {
                  Logger.debug(this, tab + "[Invisible] " + c);
              }
        }
    }*/

    /**
     * pause the SceneManage.
     */
    public void pause() {
        Logger.info(this, "called pause()");
        layeredHandler.deactivate();
        history.clear();
        if (curSceneId != SC_INVALID) {
            scenes[curSceneId].stop();
            rootContainer.removeAll();
        }
        for (int i = 0; scenes != null && i < scenes.length; i++) {
            if (scenes[i] != null) {
                scenes[i].dispose();
                scenes[i] = null;
            }
        }
        curSceneId = SC_INVALID;
        FrameworkMain.getInstance().getImagePool().clear();
        VbmController.getInstance().writeEndLog();
    }

    /**
     * dispose the SceneManage.
     */
    public void dispose() {
        pause();
        layeredHandler.destroy();
        rootContainer = null;
        history = null;
        scenes = null;
    }

    /**
     * go to NextScene.
     *
     * @param reqSceneId Scene id.
     * @param isRest whether reset or not
     * @param data  the data of scene(sceneId, needed data, focus index);
     */
    public void goToScene(int reqSceneId, boolean isRest, Object[] data) {
        Logger.info(this, "goToScene()-reqSceneId = " + reqSceneId + " curSceneId = " + curSceneId);
        if (reqSceneId == SC_INVALID) {
            //CommunicationManager.getInstance().exitToChannel();
            return;
        }
        if (curSceneId == reqSceneId) { return; }

        if (curSceneId != SC_INVALID) {
            scenes[curSceneId].stop();
            rootContainer.remove(scenes[curSceneId]);
        }

        if (data != null) {
            history.add(data);
        } else if (data == null && history.size() > 0) { history.remove(history.size() - 1); }

        if (scenes[reqSceneId] == null) { scenes[reqSceneId] = createScene(reqSceneId); }
        curSceneId = reqSceneId;
        rootContainer.add(scenes[reqSceneId]);
        scenes[reqSceneId].start(isRest);
        //rootContainer.repaint();
    }

    /**
     * Creates and Initialize the Scene.
     *
     * @param reqSceneId    scene id for request
     * @return scene    requested scene
     */
    private BasicUI createScene(int reqSceneId) {
        BasicUI scene = null;
        switch (reqSceneId) {
            case SC_ONE_CH:
                scene = new OneChUI();
                break;
            case SC_CH_LIST:
                scene = new ChListUI();
                break;
            default:
                break;
        }
        scene.setVisible(false);
        return scene;
    }

    /**
     * Gets the previous data on the next Scene.
     * @return data is a previous scene data.
     */
    public Object[] getSceneHistory() {
        if (history.size() > 0) {
            Object[] data = (Object[]) history.elementAt(history.size() - 1);
            return data;
        }
        return null;
    }

    /**
     * implement LayeredKeyHandler method. handle KeyEvent.
     *
     * @param event the UserEvent
     * @return true, if handle key
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (event == null) {
            Logger.debug(this, "handleKeyEvent()-User Event is null.");
            return false;
        }
        if (event.getType() != KeyEvent.KEY_PRESSED) { return false; }
        if (curSceneId == SC_INVALID) { return false; }
        boolean isHandling = scenes[curSceneId].handleKey(event.getCode());
        if (event.getCode() == KeyCodes.COLOR_D) { return true; }
        return isHandling;
    }
}

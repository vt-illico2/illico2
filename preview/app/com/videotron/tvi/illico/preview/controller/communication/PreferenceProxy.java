/*
 *  @(#)PreferenceProxy.java 1.0 2011.03.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @since 2011.03.19
 * @version $Revision: 1.8 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public final class PreferenceProxy implements PreferenceListener {
    /** Is a instance of PreferenceProxy. */
    private static PreferenceProxy instance  = null;

    /** a instance of PreferenceService. */
    private PreferenceService prefSvc = null;
    /** The pin enabler proxy. */
    private PinEnablerProxy pinEnablerProxy = null;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) { instance = new PreferenceProxy(); }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
        pinEnablerProxy = new PinEnablerProxy();
    }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        if (prefSvc != null) {
            try {
                prefSvc.removePreferenceListener(Config.APP_NAME, this);
            } catch (Exception e) {
                e.printStackTrace();
            }
            prefSvc = null;
        }
        pinEnablerProxy = null;
        instance = null;
    }

    /**
     * Look up User Profile and Preference.
     *
     * @param prefService PreferenceService gained from IXC
     */
    public void addPreferenceListener(PreferenceService prefService) {
        Logger.info(this, "called addPreferenceListener()");
        
        String[] preferenceList = new String[]{PreferenceNames.LANGUAGE, MonitorService.SUPPORT_UHD};
        
        try {
            prefSvc = prefService;
            Logger.debug(this, "addPreferenceListener()-prefSvc : " + prefSvc);
            if (prefSvc != null) {
                String[] uppData = prefSvc.addPreferenceListener(this, Config.APP_NAME,
                		preferenceList, new String[]{Definitions.LANGUAGE_FRENCH});
                for (int i = 0; uppData != null && i < uppData.length; i++) {
                    Logger.debug(this, "addPreferenceListener()-uppData[] : " + uppData[i]);
                    if (preferenceList[i].equals(PreferenceNames.LANGUAGE)) {
                        Config.language = uppData[i];
                    }
                    DataCenter.getInstance().put(preferenceList[i], uppData[i]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * invoked when the value of the preference has changed.
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        Log.printDebug("called receiveUpdatedPreference()");
        if (name == null || value == null) { return; }
        Log.printInfo("name = " + name + ", value =" + value);
        if (PreferenceNames.LANGUAGE.equals(name)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, value);
            Config.language = value;
        }
    }

    /**
     * Show pin enabler.
     */
    public void showPinEnabler() {
        if (prefSvc == null) { return; }
        try {
            String[] explain = new String[]{DataCenter.getInstance().getString("renderer.pinTitle1"),
                    DataCenter.getInstance().getString("renderer.pinTitle2")};
            prefSvc.showPinEnabler(pinEnablerProxy, explain);
        } catch (Exception e) {
            Logger.debug(this, "showPinEnabler()-e : " + e.getMessage());
        }
    }

    /**
     * Hide pin enabler.
     */
    public void hidePinEnabler() {
        if (prefSvc == null) { return; }
        try {
            prefSvc.hidePinEnabler();
        } catch (Exception e) {
            Logger.debug(this, "hidePinEnabler()-e : " + e.getMessage());
        }
    }

    /**
     * The Class PinEnablerProxy implement PinEnablerListener.
     */
    class PinEnablerProxy implements PinEnablerListener {
        /**
         * implements abstract method.
         *
         * @param response the response
         * @param detail the detail
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.upp.PinEnablerListener#receivePinEnablerResult(int, java.lang.String)
         */
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            if (response == PreferenceService.RESPONSE_SUCCESS) {
                hidePinEnabler();
                CommunicationManager.getInstance().requestStartUnboundApplication("Options",
                        new String[]{Config.APP_NAME});
            }
        }
    }
}

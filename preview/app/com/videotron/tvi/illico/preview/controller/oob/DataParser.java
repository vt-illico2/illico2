/*
 *  @(#)DataParser.java 1.0 2011.03.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.oob;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Vector;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.videotron.tvi.illico.preview.data.Channel;

/**
 * This class parse OOB XML datas.
 *
 * @since 2011.03.18
 * @version $Revision: 1.6 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public class DataParser extends DefaultHandler {
    /** The SAXParserFactory instance. */
    private SAXParserFactory factory = null;

    /** The ch list. */
    private Vector chList = new Vector();
    /** The channel list of Hashtable type. */
    private Hashtable hChList = new Hashtable();

    /**
     * Instantiates a new SAXParserFactory.
     */
    public DataParser() {
        factory = SAXParserFactory.newInstance();
    }

    /**
     * parse xml data.
     *
     * @param input the iInputStream
     * @throws Exception the exception
     */
    private void parseXml(InputStream input) throws Exception {
        SAXParser parser = factory.newSAXParser();
        InputSource inputSource = new InputSource(input);
        //inputSource.setEncoding("UTF-8");
        parser.parse(inputSource, this);
        //parser.parse(new InputSource(new InputStreamReader(input, "UTF-8")), this);
    }

    /**
     * parse channel list.
     *
     * @param input the iInputStream
     * @return the vector of channel list
     * @throws Exception the exception
     */
    public Object[] parseChList(InputStream input) throws Exception {
        chList.clear();
        hChList.clear();
        parseXml(input);
        return new Object[]{chList, hChList};
    }

    /**
     * Receive notification of the start of an element. Is received values of xml. This method makes pastille data.
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @param attr The attributes attached to the element. If there are no attributes, it shall be an empty
     *            Attributes object.
     * @throws SAXException the sAX exception
     * @see org.xml.sax.ContentHandler#startElement
     */
    public void startElement(String uri, String localName, String qName, Attributes attr) throws SAXException {
        if ("channel".equalsIgnoreCase(localName)) {
            Channel ch = new Channel();
            ch.setId(attr.getValue("id"));
            ch.setDescEng(attr.getValue("description_en"));
            ch.setDescFr(attr.getValue("description_fr"));
            ch.setName(attr.getValue("channel_name"));
            ch.setPreviewImgUrl(attr.getValue("preview_image"));
            ch.setBrandImgUrl(attr.getValue("branded_image"));
            ch.setStartDate(attr.getValue("startDate"));
            ch.setEndDate(attr.getValue("endDate"));
            ch.setVisualType(attr.getValue("visual_type"));
            ch.setSubDescEng(attr.getValue("subDescription_en"));
            ch.setSubDescFr(attr.getValue("subDescription_fr"));
            ch.setStatus(attr.getValue("status"));
            ch.setShortcutDescEn(attr.getValue("shortcutDesc_en"));
            ch.setShortcutDescFr(attr.getValue("shortcutDesc_fr"));
            chList.addElement(ch);
            hChList.put(ch.getId(), ch);
        }
    }

    /**
     * Receive notification of the end of an element. Saves pastille data.
     * <p>
     * By default, do nothing. Application writers may override this method in a subclass to take specific actions at
     * the end of each element (such as finalising a tree node or writing output to a file).
     * </p>
     * @param uri The Namespace URI, or the empty string if the element has no Namespace URI or if Namespace processing
     *            is not being performed.
     * @param localName The local name (without prefix), or the empty string if Namespace processing is not being
     *            performed.
     * @param qName The qualified name (with prefix), or the empty string if qualified names are not available.
     * @throws SAXException the sAX exception
     * @see org.xml.sax.ContentHandler#endElement
     */
    public void endElement(String uri, String localName, String qName) throws SAXException { }
    
    /**
     * Gets the encode string.
     *
     * @param str the str
     * @return the encode string
     */
    private String getEncodeString2(String str) {
        try {
            return new String(str.getBytes("iso8859-1"), "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}

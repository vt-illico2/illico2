/*
 *  @(#)ISAProxy.java 1.0 2013.10.10
 *  Copyright (c) 2013 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.communication;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;

import java.rmi.RemoteException;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.
 * <p>
 * Try to look up UPP Service at the time of initializing this class. If successful, upp data get from the
 * PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @since 2013.10.10
 * @version $Revision: 1.5 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public final class ISAProxy implements ISAServiceDataListener {    
    /** The instance. */
    private static ISAProxy instance  = null;

    /** The isa svc. */
    private ISAService isaService = null;
    
    /** The instant services. */
    public String[] instantServices = null;
    private String[] excludedChannels;
    private int[] excludedChannelTypes;

    /**
     * Gets the single instance of ISAProxy.
     * @return single instance of ISAProxy
     */
    public static synchronized ISAProxy getInstance() {
        if (instance == null) { instance = new ISAProxy(); }
        return instance;
    }

    /**
     * Instantiates a new ISAProxy.
     */
    private ISAProxy() {
        
    }

    /**
     * Dispose ISAProxy.
     */
    public void dispose() {
        if (isaService != null) {
            try {
                isaService.removeISAServiceDataListener(Config.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
            isaService = null;
        }
        instance = null;
    }

    /**
     * Adds the isa listener.
     *
     * @param service the service
     */
    public void addISAListener(ISAService service) {
        Logger.info(this, "called addISAListener()");
        try {
            isaService = service;
            Logger.debug(this, "addISAListener()-isaService : " + isaService);
            if (isaService != null) {
                instantServices = isaService.getISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW);
                printList();
                isaService.addISAServiceDataListener(this, Config.APP_NAME);
                if (instantServices == null || instantServices.length == 0) {
                    instantServices = isaService.getISAServiceList(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW);
                    printList();
                }
                Logger.debug(this, "addISAListener()-instantServices : " + instantServices);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Notify to be updated a ISAService list.
     * 
     * @param isaServiceType
     *            one of {@link ISAService#ISA_SERVICE_TYPE_CHANNEL},
     *            {@link ISAService#ISA_SERVICE_TYPE_FREE_PREVIEW},
     *            {@link ISAService#ISA_SERVICE_TYPE_ILLICO_SERVICE}
     * @param serviceList a String array consist of serviceId for each ISA service type.
     * @see com.videotron.tvi.illico.ixc.isa.ISAServiceDataListener#updateISAServiceList(java.lang.String, java.lang.String[])
     */
    public void updateISAServiceList(String isaServiceType, String[] serviceList) {
        Logger.info(this, "called updateISAServiceList() - isaServiceType : " + isaServiceType);
        Logger.info(this, "called updateISAServiceList() - serviceList : " + serviceList);
        if (isaServiceType != null && isaServiceType.equals(ISAService.ISA_SERVICE_TYPE_FREE_PREVIEW)) {
            instantServices = serviceList;
        }        
    }

    // Bundling
    public void updateISAExcludedChannelsAndTypes(String[] excludedChannels, int[] excludedTypes)
            throws RemoteException {
        Logger.info(this, "called updateISAServiceList() - excludedChannels : " + excludedChannels);
        Logger.info(this, "called updateISAServiceList() - excludedTypes : " + excludedTypes);
        this.excludedChannels = excludedChannels;
        this.excludedChannelTypes = excludedTypes;
    }

    // Bundling
    public boolean isIncludedInExcludedChannels(String serviceId) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ISAProxy: isIncludedInExcludedChannels: serviceId=" + serviceId);
        }
        if (excludedChannels != null) {
            for (int i = 0; i < excludedChannels.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(
                            "ISAProxy: setExcludedChannels: excludedChannels[" + i + "]=" + excludedChannels[i]);
                }
                if (excludedChannels[i].equals(serviceId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isIncludedInExcludedChannelTypes(String serviceId) {
        EpgService eService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

        if (Log.DEBUG_ON) {
            Log.printDebug("ISAProxy: isIncludedInExcludedChannelTypes: serviceId=" + serviceId + ", epgService="
                    + eService);
        }

        if (excludedChannelTypes != null && eService != null) {
            TvChannel ch = null;
            try {
                ch = eService.getChannel(serviceId);

                if (ch != null) {
                    for (int i = 0; i < excludedChannelTypes.length; i++) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(
                                    "ISAProxy: isIncludedInExcludedChannelTypes: ch.getType=" + ch.getType());
                        }
                        if (excludedChannelTypes[i] == ch.getType()) {
                            return true;
                        }
                    }

                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

        return false;
    }



    /**
     * Show a Instance subscription popup.
     * 
     * @param callLetter Call Letter in the case of channel or free preview channel.
     * @param entryPoint one of {@link #ENTRY_POINT_FRP_E01, {@link #ENTRY_POINT_FRP_E02}
     */
    public void showSubscriptionPopup(String callLetter, String entryPoint) {
        if (isaService == null) { return; }
        try {
            isaService.showSubscriptionPopup(callLetter, entryPoint);
        } catch (Exception e) {
            Logger.debug(this, "showSubscriptionPopup()-e : " + e.getMessage());
        }
    }
    
    /**
     * Prints the list.
     */
    public void printList() {
        Logger.debug(this, "printList()-instantServices : " + instantServices);        
        for (int i = 0; instantServices != null && i < instantServices.length; i++) {
            Logger.debug(this, "printList()-instantServices[" + i + "]" + instantServices[i]);
        }
    }
}

/*
 *  @(#)MainManager.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller;

import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.preview.controller.oob.OobManager;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;

/**
 * This class is the main class.
 *
 * @since 2011.03.17
 * @version $Revision: 1.6 $ $Date: 2017/01/09 20:35:40 $
 * @author tklee
 */
public final class MainManager {
    /** Instance of MainManager. */
    private static MainManager instance = new MainManager();

    /**
     * Gets the singleton instance of MainManager.
     * @return MainManager
     */
    public static MainManager getInstance() {
        return instance;
    }

    /**
     * initial method after called Constructor.
     */
    public void init() {
        Logger.info(this, "called init()");
        SceneManager.getInstance().init();
        OobManager.getInstance().start();
        CommunicationManager.getInstance().start();
    }

    /**
     * This method starts a SceneManager.
     */
    public void start() {
        Logger.info(this, "called start()");
        SceneManager.getInstance().start();
        CommunicationManager.getInstance().addScreenSaverListener();
        CommunicationManager.getInstance().addCaUpdateListener();
    }

    /**
     * This method pause processes.
     */
    public void pause() {
        CommunicationManager.getInstance().removeCaUpdateListener();
        SceneManager.getInstance().pause();
        CommunicationManager.getInstance().removeScreenSaverListener();        
        CommunicationManager.getInstance().hideLoadingAnimation();
    }

    /**
     * This method dispose a SceneManager.
     */
    public void destroy() {
        SceneManager.getInstance().dispose();
        CommunicationManager.getInstance().removeScreenSaverListener();
        CommunicationManager.getInstance().removeCaUpdateListener();
    }
}

/*
 *  @(#)OobManager.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.oob;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.util.Environment;

/**
 * This class process OOB data.
 * @author tklee
 * @version $Revision: 1.5 $ $Date: 2017/01/09 20:35:39 $
 */
public class OobManager implements DataUpdateListener {
    /** The Constant NO_ERR. */
    public static final int NO_ERR = -1;
    /** The Constant ERR_XML_GET. */
    private static final int ERR_XML_GET = 0;
    /** The Constant ERR_XML_PARSING. */
    public static final int ERR_XML_PARSING = 1;
    /** The error code. */
    public final String[] errCode = {"FPR500", "FPR501"};
    /** The errpor message. */
    private final String[] errMsg = {"Cannot retrieve the XML preview file from the MS",
            "Cannot parse the XML preview file"};

    /** The Constant IB_SERVER_CONFIG. the name for inband path.*/
    private static final String OOB_CONFIG = "previewConfig";

    /** Class instance. */
    private static OobManager instance = null;

    /** The DataParser instance. */
    private DataParser dataParser = null;
    /** The xml file. */
    private File xmlFile = null;

    /** The error type. */
    public int errorType = NO_ERR;


    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static OobManager getInstance() {
        if (instance == null) { instance = new OobManager(); }
        return instance;
    }

    /**
     * lookup Services via IXC.
     */
    public void start() {
        dataParser = new DataParser();
        DataCenter.getInstance().addDataUpdateListener(OOB_CONFIG, this);
        if (Environment.EMULATOR) {
            DataCenter.getInstance().put(OOB_CONFIG, new File("data/channel_free_previews.xml"));
        }
    }

    /**
     * Gets the channel list.
     *
     * @return the channel list ([0] : Vector, [1] : Hashtable)
     */
    public Object[] getChList() {
        Logger.debug(this, "called getChList()");
        errorType = NO_ERR;
        InputStream inputStream = null;
        try {
            if (xmlFile == null) { xmlFile = (File) DataCenter.getInstance().get(OOB_CONFIG); }
            Logger.debug(this, "getChList() xmlFile : " + xmlFile);
            inputStream = new FileInputStream(xmlFile);
            try {
                Object[] newData = dataParser.parseChList(inputStream);
                Vector newList = (Vector) newData[0];
                if (newList == null || newList.size() == 0) {
                    errorType = ERR_XML_PARSING;
                    printError(null);
                } else { return newData; }
            } catch (Exception e) {
                e.printStackTrace();
                errorType = ERR_XML_PARSING;
                printError(e.getMessage());
            }
        } catch (Exception e) {
            e.printStackTrace();
            errorType = ERR_XML_GET;
            printError(e.getMessage());
        } finally {
            try {
                if (inputStream != null) { inputStream.close(); }
            } catch (Exception e) { e.printStackTrace(); }
        }
        return null;
    }

    /**
     * Prints the error.
     * @param addTxt the add text
     */
    public void printError(String addTxt) {
        printError(errorType, addTxt);
    }

    /**
     * Prints the error.
     *
     * @param errType the error type. refer to upper constant.
     * @param addTxt the add text
     */
    private void printError(int errType, String addTxt) {
        if (errType == NO_ERR) { return; }
        String errTxt = "[" + errCode[errType] + "]" + errMsg[errType];
        if (addTxt != null) { errTxt += "-" + addTxt; }
        Logger.error(this, errTxt);
    }

    /**
     * Gets the error code.
     *
     * @return the error code
     */
    public String getErrorCode() {
        return errCode[errorType];
    }

    /**
     * Called when data has been updated.
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        Logger.debug(this, "dataUpdated()-key = " + key + " value = " + value);
        xmlFile = (File) value;
    }

    /**
     * Called when data has been removed.
     *
     * @param key the key
     */
    public void dataRemoved(String key) { }
}

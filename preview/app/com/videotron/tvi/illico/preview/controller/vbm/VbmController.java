package com.videotron.tvi.illico.preview.controller.vbm;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * The Class VbmController.
 */
public class VbmController implements LogCommandTriggerListener {
    /** The Constant MID_APP_START. */
    private static final long MID_APP_START = 1017000001L;
    /** The Constant MID_APP_EXIT. */
    private static final long MID_APP_EXIT   = 1017000002L;
    /** The Constant MID_PARENT_APP. */
    private static final long MID_PARENT_APP   = 1017000003L;

    /** The Constant DEF_MEASUREMENT_GROUP. */
    public static final String DEF_MEASUREMENT_GROUP = "-1";
    /** The Constant VALUE_SEPARATOR. */
    public static final String VALUE_SEPARATOR = "|";

    /** The instance. */
    private static VbmController instance = new VbmController();

    /** The enabled to put vbm log. */
    private static boolean enabled = false;

    /** The vbm service. */
    private VbmService vbmService;
    /** The id set. */
    private HashSet idSet = new HashSet();
    /** The vbm log buffer. */
    private Vector vbmLogBuffer;
    /** The session id. */
    private String sessionId = null;

    /**
     * Gets the single instance of VbmController.
     *
     * @return single instance of VbmController
     */
    public static synchronized VbmController getInstance() {
        return instance;
    }

    /**
     * Inits the.
     *
     * @param service the service
     */
    public void init(VbmService service) {
        Log.printInfo("VbmController.init");
        vbmService = service;
        long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(Config.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        try {
            vbmService.addLogCommandChangeListener(Config.APP_NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

        if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmController: ids[" + i + "] = " + ids[i]);
                }
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (vbmLogBuffer == null) {
            Log.printDebug("VbmController: found vbmLogBuffer");
        } else {
            Log.printDebug("VbmController: not found vbmLogBuffer.");
        }
    }

    /**
     * Dispose.
     */
    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(Config.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    /**
     * Write start log.
     *
     * @param startParam the start param
     */
    public void writeStartLog(String[] startParam) {
        sessionId = Long.toString(System.currentTimeMillis());
        String parentAppName = "";
        if (startParam != null && startParam.length > 0 && startParam[0] != null) { parentAppName = startParam[0]; }
        write(MID_PARENT_APP, parentAppName);
        write(MID_APP_START, sessionId);
    }

    /**
     * Write end log.
     */
    public void writeEndLog() {
        write(MID_APP_EXIT, sessionId);
        sessionId = null;
    }

    /**
     * Check empty.
     */
    private void checkEmpty() {
        enabled = vbmLogBuffer != null && !idSet.isEmpty();
    }

    /**
     * Write.
     *
     * @param id the id
     * @param str the str
     * @return true, if successful
     */
    private boolean write(long id, String str) {
        return write(id, null, new String[]{str});
    }

    /**
     * Write.
     *
     * @param id the id
     * @param group the group
     * @param values the values
     * @return true, if successful
     */
    private boolean write(long id, String group, String[] values) {
        try {
            if (!enabled) {
                return false;
            }
            if (!idSet.contains(new Long(id))) {
                return false;
            }
            if (group == null) {
                group = DEF_MEASUREMENT_GROUP;
            }
            StringBuffer sb = new StringBuffer(80);
            sb.append(id);
            sb.append(VbmService.SEPARATOR);
            sb.append(group);
            if (values != null && values.length > 0) {
                sb.append(VbmService.SEPARATOR);
                sb.append(values[0]);
                for (int i = 1; i < values.length; i++) {
                    sb.append(VALUE_SEPARATOR);
                    sb.append(values[i]);
                }
            }
            String str = sb.toString();
            if (Log.DEBUG_ON) {
                Log.printDebug("VbmController.write: " + str);
            }
            vbmLogBuffer.addElement(str);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.print(e);
        }
        return false;
    }

    /**
     * event invoked when logLevel data updated.
     * it is only applied to action measurements
     *
     * @param measurementId the measurement id
     * @param command - true to start logging, false to stop logging
     * @throws RemoteException - occur during the execution of a remote method call
     */
    public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

    /**
     * event invoked when log of trigger measurement should be collected.
     *
     * @param measurementId trigger measurement
     */
    public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }
}

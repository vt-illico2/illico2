/*
 *  @(#)CommunicationManager.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.controller.communication;

import java.awt.EventQueue;
import java.awt.Point;
import java.rmi.Remote;
import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.CaUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.preview.App;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;
import com.videotron.tvi.illico.preview.controller.vbm.VbmController;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This class for connection the IXC.
 *
 * @since 2011.03.17
 * @author tklee
 * @version $Revision: 1.8 $ $Date: 2017/01/09 20:35:39 $
 */
public class CommunicationManager {
    /** The lookup sleep time. */
    private final long lookupSleepTime = 2000;
    /** The point of loading animation to show on screen. */
    private final Point loadingAniPoint = new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2);

    /** Class instance. */
    private static CommunicationManager instance = null;

    /** The MonitorService. */
    private MonitorService monitorService = null;
    /** The ErrorMessageService. */
    private ErrorMessageService errMsgService = null;
    /** The StcService. */
    private StcService stcService = null;
    /** The LoadingAnimationService. */
    private LoadingAnimationService loadingService = null;
    /** The EpgService. */
    private EpgService epgService = null;

    /** The ScreenSaverProxy instance. */
    private ScreenSaverProxy screensaverProxy = null;
    /** The ErrorMessageProxy instance. */
    private ErrorMessageProxy errMsgProxy = null;    
    /** The epg service proxy. */
    private EpgServiceProxy epgServiceProxy = null;

    /** The is loading animation. */
    private boolean isLoadingAnimation = false;

    /**
     * Get CommunicationManager instance.
     * @return CommunicationManager
     */
    public static CommunicationManager getInstance() {
        if (instance == null) { instance = new CommunicationManager(); }
        return instance;
    }

    /**
     * lookup Services via IXC.
     */
    public void start() {
        lookUpService("/1/2030/", PreferenceService.IXC_NAME);
        lookUpService("/1/1/", MonitorService.IXC_NAME);
        lookUpService("/1/2026/", EpgService.IXC_NAME);
        lookUpService("/1/2075/", LoadingAnimationService.IXC_NAME);
        lookUpService("/1/2100/", StcService.IXC_NAME);
        lookUpService("/1/2025/", ErrorMessageService.IXC_NAME);
        lookUpService("/1/2085/", VbmService.IXC_NAME);
        lookUpService("/1/2200/", ISAService.IXC_NAME);
    }

    /**
     * Look up service.
     * @param path path string
     * @param name IXC name
     */
    private void lookUpService(String path, final String name) {
        final String ixcName = path + name;
        Thread thread = new Thread("IXC_LookUp:" + name) {
            public void run() {
                Remote remote = null;
                while (remote == null) {
                    try {
                        remote = IxcRegistry.lookup(App.xletContext, ixcName);
                        if (remote != null) {
                            Logger.debug(this, "lookUpService()-success : " + name);
                            if (name.equals(MonitorService.IXC_NAME)) {
                                monitorService = (MonitorService) remote;
                                screensaverProxy = new ScreenSaverProxy();
                            } else if (name.equals(LoadingAnimationService.IXC_NAME)) {
                                loadingService = (LoadingAnimationService) remote;
                            } else if (name.equals(StcService.IXC_NAME)) {
                                stcService = (StcService) remote;
                                new STCServiceProxy().startService();
                            } else if (name.equals(ErrorMessageService.IXC_NAME)) {
                                errMsgService = (ErrorMessageService) remote;
                                errMsgProxy = new ErrorMessageProxy();
                            } else if (name.equals(EpgService.IXC_NAME)) {
                                epgService = (EpgService) remote;
                                epgServiceProxy = new EpgServiceProxy();
                            } else if (name.equals(PreferenceService.IXC_NAME)) {
                                PreferenceProxy.getInstance().addPreferenceListener((PreferenceService) remote);
                            } else if (name.equals(VbmService.IXC_NAME)) {
                                VbmController.getInstance().init((VbmService) remote);
                            } else if (name.equals(ISAService.IXC_NAME)) {
                                ISAProxy.getInstance().addISAListener((ISAService) remote);
                            }
                            break;
                        }
                    } catch (Exception e) {
                        Logger.debug(this, "lookUpService()-not bound : " + e.toString());
                    }
                    try {
                        Thread.sleep(lookupSleepTime);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    /**
     * Gets the parameter from Monitor.
     *
     * @return the start parameter
     */
    public String[] getParameter() {
        if (monitorService != null) {
            try {
                String[] parm = monitorService.getParameter(Config.APP_NAME);
                Logger.debug(this, "getParameter()-startParm : " + parm);
                return parm;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Start Unbound Application.
     * @param reqAppName application Name.
     * @param reqParams null.
     */
    public void requestStartUnboundApplication(String reqAppName, String[] reqParams) {
        try {
            if (monitorService != null) { monitorService.startUnboundApplication(reqAppName, reqParams); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Exit to channel.
     */
    public void exitToChannel() {
        try {
            if (monitorService != null) { monitorService.exitToChannel(); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Exit to channel.
     *
     * @param sourceId the source id
     */
    public void exitToChannel(int sourceId) {
        try {
            if (monitorService != null) { monitorService.exitToChannel(sourceId); }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Checks if is resource authorization.
     *
     * @param callLetter the call letter
     * @return true, if is resource authorization
     */
    public boolean isResourceAuthorization(String callLetter) {
        try {
            if (monitorService != null) {
                int authType = monitorService.checkResourceAuthorization(callLetter);
                Logger.debug(this, "isResourceAuthorization(" + callLetter + ")-authType : " + authType);
                return authType == MonitorService.AUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * This method start loadingAnimation.
     */
    public void showLoadingAnimation() {
        try {
            if (loadingService != null) {
                loadingService.showLoadingAnimation(loadingAniPoint);
                isLoadingAnimation = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return the isLoadingAnimation
     */
    public boolean isLoadingAnimation() {
        return isLoadingAnimation;
    }

    /**
     * This method stop loadingAnimation.
     */
    public void hideLoadingAnimation() {
        try {
            if (loadingService != null) {
                loadingService.hideLoadingAnimation();
                isLoadingAnimation = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add ScreenSaverListener.
     */
    public void addScreenSaverListener() {
        if (screensaverProxy != null) { screensaverProxy.addScreenSaverListener(); }
    }

    /**
     * Removes removeScreenSaverListener from MonitorService.
     */
    public void removeScreenSaverListener() {
        if (screensaverProxy != null) { screensaverProxy.removeScreenSaverListener(); }
    }
    
    /**
     * Add CaUpdateListener.
     */
    public void addCaUpdateListener() {
        if (epgServiceProxy != null) { epgServiceProxy.addCaUpdateListener(); }
    }

    /**
     * Removes CaUpdateListener from EpgService.
     */
    public void removeCaUpdateListener() {
        if (epgServiceProxy != null) { epgServiceProxy.removeCaUpdateListener(); }
    }

    /**
     * Show error message.
     * @param code the code or error
     */
    public void showErrorMessage(String code) {
        if (errMsgProxy != null) { errMsgProxy.showErrorMessage(code); }
    }
    
    // 4K
    public void showCommonMessage(String code) {
    	if (errMsgProxy != null) { errMsgProxy.showCommonMessage(code); }
    }

    /**
     * Hide error message.
     */
    public void hideErrorMessage() {
        if (errMsgProxy != null) { errMsgProxy.hideErrorMessage(); }
    }

    /**
     * Gets the TvChannel.
     *
     * @param callLetter the ch call letter(name)
     * @return the channel
     */
    public TvChannel getChannel(String callLetter) {
        if (epgService == null) { return null; }
        try {
            return epgService.getChannel(callLetter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Change channel.
     *
     * @param tvCh the TvChannel
     */
    public void changeChannel(TvChannel tvCh) {
        if (epgService == null) { return; }
        try {
            epgService.changeChannel(tvCh);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The Class ScreenSaverProxy implement ScreenSaverConfirmationListener.
     */
    class ScreenSaverProxy implements ScreenSaverConfirmationListener {
        /**
         * Add ScreenSaverListener.
         */
        public void addScreenSaverListener() {
            try {
                monitorService.addScreenSaverConfirmListener(this, Config.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Removes removeScreenSaverListener from MonitorService.
         */
        public void removeScreenSaverListener() {
            try {
                monitorService.removeScreenSaverConfirmListener(this, Config.APP_NAME);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * ScreenSaver is Confirmed to listeners.
         *
         * @return true
         * @throws RemoteException the remote exception
         */
        public boolean confirmScreenSaver() throws RemoteException {
            return true;
        }
    }

    /**
     * The Class ErrorMessageProxy implement ErrorMessageListener.
     */
    class ErrorMessageProxy implements ErrorMessageListener {
        /**
         * Show error message.
         *
         * @param code the code
         */
        public void showErrorMessage(String code) {
            try {
                errMsgService.showErrorMessage(code, this);
            } catch (Exception e) {
                Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
            }
        }
        
        public void showCommonMessage(String code) {
        	try {
                errMsgService.showCommonMessage(code, this);
            } catch (Exception e) {
                Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
            } 
        }

        /**
         * Hide error message.
         */
        public void hideErrorMessage() {
            try {
                errMsgService.hideErrorMessage();
            } catch (Exception e) {
                Logger.debug(this, "showErrorMessage()-e : " + e.getMessage());
            }
        }

        /**
         * called when ErrorMessageService completed the action requested.
         *
         * @param buttonType the button type
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener#actionPerformed(int)
         */
        public void actionPerformed(int buttonType) throws RemoteException {
            if (buttonType == ErrorMessageListener.BUTTON_TYPE_CLOSE) {
                SceneManager sceneManager = SceneManager.getInstance();
                sceneManager.scenes[sceneManager.curSceneId].notifyErrorMessage();
            }
        }
    }

    /**
     * The Class STCServiceProxy implement LogLevelChangeListener.
     */
    class STCServiceProxy implements LogLevelChangeListener {
        /**
         * Start service.
         */
        public void startService() {
            try {
                int currentLevel = stcService.registerApp(Config.APP_NAME);
                Logger.debug(this, "stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
            Object logCacheObj = SharedMemory.getInstance().get("stc-" + Config.APP_NAME);
            if (logCacheObj != null) { DataCenter.getInstance().put("LogCache", logCacheObj); }
            try {
                stcService.addLogLevelChangeListener(Config.APP_NAME, this);
            } catch (RemoteException ex) {
                Logger.error(this, ex.getMessage());
            }
        }

        /**
         * implements LogLevelChangeListener method.
         * when log level is changed, this method is called.
         *
         * @param logLevel the log level
         * @throws RemoteException the remote exception
         * @see com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener#logLevelChanged(int)
         */
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }
    
    /**
     * The Class EpgServiceProxy implement CaUpdateListener.
     */
    class EpgServiceProxy implements CaUpdateListener {
        /**
         * Add CaUpdateListener.
         */
        public void addCaUpdateListener() {
            Logger.debug(this, "called addCaUpdateListener()");
            try {
                epgService.addCaUpdateListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * Removes CaUpdateListener from EPGService.
         */
        public void removeCaUpdateListener() {
            Logger.debug(this, "called removeCaUpdateListener()");
            try {
                epgService.removeCaUpdateListener(this);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /**
         * @see com.videotron.tvi.illico.ixc.epg.CaUpdateListener#caUpdated(int, boolean)
         */
        public void caUpdated(final int sourceId, final boolean authorized) throws RemoteException {
            Logger.debug(this, "called caUpdated() - sourceId : " + sourceId + ", authorized : " + authorized);
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    SceneManager sceneManager = SceneManager.getInstance();
                    if (sceneManager.curSceneId == SceneManager.SC_INVALID) { return; }
                    sceneManager.scenes[sceneManager.curSceneId].notifyCaUpdated(sourceId, authorized);
                }
            });
        }
    }
}

/*
 *  @(#)BasicUI.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.preview.controller.communication.ISAProxy;
import com.videotron.tvi.illico.preview.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.preview.controller.oob.OobManager;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.gui.BasicRenderer;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Environment;

/**
 * This Class is the BasicUI class. All Scene is drawn on BasicUI.
 * @version $Revision: 1.13 $ $Date: 2017/01/09 20:35:39 $
 * @author pellos
 */
public abstract class BasicUI extends UIComponent implements ClockListener {
    /** serialVersionUID for serializable type.*/
    private static final long serialVersionUID = 1L;
    /** The id region data. */
    protected static final String ID_CH_DATA = "channelList";
    /** The Constant ID_CH_IMAGE. */
    protected static final String ID_CH_IMAGE = "channelImage";

    /** The Constant ERRMSG_NONE. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_NONE = 0;
    /** The Constant ERRMSG_PREV. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_PREV = 1;
    /** The Constant ERRMSG_EXIT. the action state after notification from ErrorMessage.*/
    protected static final int ERRMSG_EXIT = 2;

    /** ScrollTexts Instance. */
    protected static ScrollTexts scrollText = null;
    /** The scroll shadow component. */
    protected static ScrollShadowComp scrollShadowComp = null;
    /** The Footer instance. */
    protected static Footer footer = null;
    /** The data loading thread. */
    private DataLoadingThread dataLoadingThread = null;
    /** The channel list of Vector type. */
    public static Vector vChList = null;
    /** The channel list of Hashtable type. */
    protected static Hashtable hChList = null;    
    /** The Channel instance focused. */
    public Channel ch = null;

    /** Is stored the previous Scene ID. */
    protected int previousSceneID = -1;
    /** Is stored the value. */
    protected Object receivedData = null;
    /** The focus history. */
    protected Object[] focusHistory = null;
    /** The err msg action. */
    protected int errMsgAction = 0;

    /**
     * Instantiates a new BasicUI.
     */
    public BasicUI() {
        dataLoadingThread = new DataLoadingThread();
        if (scrollShadowComp == null) { scrollShadowComp = new ScrollShadowComp(); }
        if (footer == null) { footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.TEXT); }
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     */
    public void start(boolean isReset) {
        if (scrollText == null) { scrollText = new ScrollTexts(); }
        footer.reset();
        add(footer);

        Object[] history = SceneManager.getInstance().getSceneHistory();
        previousSceneID = SceneManager.SC_INVALID;
        if (history != null) {
            previousSceneID = Integer.parseInt((String) history[0]);
            receivedData = history[1];
            focusHistory = (Object[]) history[2];
        }
        Clock.getInstance().addClockListener(this);
        //setVisible(true);
    }

    /**
     * update a clock.
     *
     * @param date the date
     */
    public void clockUpdated(Date date) {
        repaint();
    }

    /**
     * Sets the focus history.
     *
     * @param focusValue the new focus history
     */
    protected void setFocusHistory(Object[] focusValue) {
        Object[] history = SceneManager.getInstance().getSceneHistory();
        if (history != null && history.length > 2) { history[2] = focusValue; }
    }

    /**
     * Sets the focus history. if need, each scene implements this method.
     */
    protected void setFocusHistory() { }

    /**
     * Gets the current scene id.
     *
     * @return the current scene id
     */
    protected String getCurrentSceneId() {
        return String.valueOf(SceneManager.getInstance().curSceneId);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        Clock.getInstance().removeClockListener(this);
        setFocusHistory();
        removeAll();
        setVisible(false);
    }

    /**
     * Is disposed the scene.
     */
    public void dispose() {
        stop();
        removeAll();
        if (dataLoadingThread != null) { dataLoadingThread.stop(); }
        scrollText = null;
        previousSceneID = -1;
        vChList = null;
        hChList = null;
    }

    /**
     * implement handleKey method.
     *
     * @param code the key code
     * @return true, if this key code is used.
     */
    public boolean handleKey(int code) {
        Logger.debug(this, "handleKey()-code : " + code);
        if (code == Rs.KEY_EXIT) {
            footer.clickAnimation(BasicRenderer.btnExitTxt);
            CommunicationManager.getInstance().exitToChannel();
            return true;
        }
        if (CommunicationManager.getInstance().isLoadingAnimation()) { return true; }
        return keyAction(code);
    }

    /**
     * Notify from ErrorMessage after action is performed.
     * perform next state by errMsgAction type.
     */
    public void notifyErrorMessage() {
        if (errMsgAction == ERRMSG_PREV) {
            SceneManager.getInstance().goToScene(previousSceneID, false, null);
        } else if (errMsgAction == ERRMSG_EXIT) { CommunicationManager.getInstance().exitToChannel(); }
    }    

    /**
     * Show error message.
     *
     * @param errAction the error action
     * @param errCode the error code
     */
    protected void showErrorMessage(int errAction, String errCode) {
        errMsgAction = errAction;
        CommunicationManager.getInstance().showErrorMessage(errCode);
    }

    /**
     * Check error message.
     *
     * @return true, whether ErrorMessage will be showed or not.
     */
    protected boolean checkErrorMessageShow() {
        if (OobManager.getInstance().errorType != OobManager.NO_ERR) { return true; }
        return false;
    }

    /**
     * Load data by using Thread.
     *
     * @param obj request data
     * @param isLoadingBar the is loading bar. if true, loading bar will be showed.
     */
    protected void loadData(Object[] obj, boolean isLoadingBar) {
        if (isLoadingBar) { CommunicationManager.getInstance().showLoadingAnimation(); }
        if (dataLoadingThread != null) { dataLoadingThread.setObj(obj); }
    }

    /**
     * return the result of action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected abstract boolean keyAction(int keyCode);

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(ID_CH_DATA)) {
            if (vChList == null) {
                OobManager oobManager = OobManager.getInstance();
                Object[] chList = oobManager.getChList();
                if (chList != null) {
                    vChList = (Vector) chList[0];
                    hChList = (Hashtable) chList[1];
                } else {
                    if (!checkErrorMessageShow()) {
                        oobManager.errorType = OobManager.ERR_XML_PARSING;
                        oobManager.printError(null);
                    }
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    showErrorMessage(ERRMSG_EXIT, oobManager.getErrorCode());
                }
            }
        }
    }

    /**
     * Gets the init and the last number.
     *
     * @param focus the focus
     * @param totalCount the total count
     * @param itemsPerPage the items per page
     * @return the init and the last number
     */
    protected int[] getInitLastNumber(int focus, int totalCount, int itemsPerPage) {
        int halfPos = itemsPerPage / 2;
        int[] number = new int[] {0, totalCount - 1};
        if (totalCount > itemsPerPage) {
            if (focus < halfPos) {
                number[0] = totalCount - (halfPos - focus);
            } else { number[0] = focus - halfPos; }
            if (totalCount - focus <= halfPos) {
                number[1] = focus + halfPos - totalCount;
            } else { number[1] = focus + halfPos; }
        }
        return number;
    }

    /**
     * Sets the preview image.
     *
     * @param channel Channel instance for the new preview image
     */
    /*protected void setPreviewImage(Channel channel) {
        if (channel == null) { return; }
        channel.setPreviewImg(getImage(channel.getPreviewImgUrl(), channel.getId()));
        channel.setBrandImg(getImage(channel.getBrandImgUrl(), channel.getId()));
    }*/

    /**
     * Gets the image.
     *
     * @param imgUrl the image url
     * @param id the id
     * @return the Image object
     */
    protected Image getPreviewImage(String imgUrl, String id) {
        if (imgUrl == null || imgUrl.length() == 0) {
            Logger.error(this, "getImage()-" + id + " : image url is empty.");
            return null;
        }
        Image img = DataCenter.getInstance().getImage(imgUrl);
        if (img == null) {
            try {
                byte[] imgByte = URLRequestor.getBytes(imgUrl, null);
                if (imgByte != null) {
                    img = FrameworkMain.getInstance().getImagePool().createImage(imgByte, imgUrl);
                } else { Logger.error(this, "getImage()-" + id + " : cannot get image from server."); }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return img;
    }

    /**
     * Tune to channel.
     *
     * @param tvCh the TvChannel
     */
    protected void tuneToChannel(TvChannel tvCh) {
        if (tvCh != null) {
            try {
                CommunicationManager.getInstance().exitToChannel(tvCh.getSourceId());
            } catch (Exception e) {
                e.printStackTrace();
                Logger.error(this, "tuneToChannel()-error : " + e.getMessage());
            }
        }
    }
    
    /**
     * Notify CaUpdated.
     *
     * @param sourceId the source id
     * @param authorized the authorized
     */
    public void notifyCaUpdated(int sourceId, boolean authorized) {        
        Logger.debug(this, "notifyCaUpdated() - vChList : " + vChList);
        for (int i = 0; vChList != null && i < vChList.size(); i++) {
            Channel channel = (Channel) vChList.elementAt(i);
            TvChannel tvChannel = channel.getTvCh();
            Logger.debug(this, "notifyCaUpdated() - vChList[" + i + "] - tvChannel : " + tvChannel);
            try {
                if (tvChannel != null && tvChannel.getSourceId() == sourceId) {                    
                    int oldState = channel.getSubscriptionState();                    
                    if (authorized) {
                        channel.setSubscriptionState(Channel.SUBSCRIBE_COMPLETED);
                    } else {
                        checkInstantChannel(channel);
                    }
                    
                    Logger.debug(this, "notifyCaUpdated() - oldState : " + oldState);
                    Logger.debug(this, "notifyCaUpdated() - ch : " + ch);
                    Logger.debug(this, "notifyCaUpdated() - channel : " + channel);
                    Logger.debug(this, "notifyCaUpdated() - channel.getSubscriptionState() : " + channel.getSubscriptionState());
                    if (channel == ch && oldState != channel.getSubscriptionState()) { repaint(); }
                    break;
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }
    
    /**
     * Check channel data.
     *
     * @param ch the Channel
     */
    protected void checkChannelData(Channel ch) {
        TvChannel tvChannel = ch.getTvCh();
        boolean isSubscribed = false;
        if (tvChannel != null) {
            try {
                isSubscribed = tvChannel.isSubscribed() && !tvChannel.isAuthorizedByFreePreview();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        Logger.debug(this, "checkChannelData()-isSubscribed : " + isSubscribed);
        if (!isSubscribed) {
            checkInstantChannel(ch);
        }
    }
    
    /**
     * Check instant channel.
     *
     * @param ch the Channel
     */
    private void checkInstantChannel(Channel ch) {
        Logger.debug(this, "checkInstantChannel()-ch.getId() : " + ch.getId());
        ISAProxy.getInstance().printList();
        String[] instantServices = ISAProxy.getInstance().instantServices;
        boolean isInstantChannel = false;
        for (int i = 0; instantServices != null && i < instantServices.length; i++) {
            if (ch.getId().equals(instantServices[i])) {
                isInstantChannel = true;
                break;
            }
        }

        // Bundling
        if (!ISAProxy.getInstance().isIncludedInExcludedChannels(ch.getId())
                && !ISAProxy.getInstance().isIncludedInExcludedChannelTypes(ch.getId())) {
            isInstantChannel = true;
        }
        
        Logger.debug(this, "checkInstantChannel()-isInstantChannel : " + isInstantChannel);
        ch.setSubscriptionState(isInstantChannel ? Channel.SUBSCRIBE_INSTANT : Channel.SUBSCRIBE_CYO);
    }
    
    /**
     * Check action button.
     *
     * @param ch the ch
     */
    protected void checkActionButton(Channel ch, String entryPoint) {
        int subscriptionState = ch.getSubscriptionState();
        Logger.debug(this, "checkActionButton()-subscriptionState : " + subscriptionState);
        if (subscriptionState == Channel.SUBSCRIBE_INSTANT) {
        	// 4K
        	try {
	        	if (ch.getTvCh().getDefinition() == TvChannel.DEFINITION_4K && !Environment.SUPPORT_UHD) {
	        		CommunicationManager.getInstance().showCommonMessage(null);
	        	} else {
	        		ISAProxy.getInstance().showSubscriptionPopup(ch.getId(), entryPoint);
	        	}
        	} catch (RemoteException e) {
        		e.printStackTrace();
        		Logger.error(this, e.getMessage());
        	}
        } else if (subscriptionState == Channel.SUBSCRIBE_CYO) {
            //tklee 20131121 - changed requirement : launch CYO app. by CYO popup of ISA.
            //PreferenceProxy.getInstance().showPinEnabler();
        	// 4K
        	try {
	        	if (ch.getTvCh().getDefinition() == TvChannel.DEFINITION_4K && !Environment.SUPPORT_UHD) {
	        		CommunicationManager.getInstance().showCommonMessage(null);
	        	} else {
	        		ISAProxy.getInstance().showSubscriptionPopup(ch.getId(), entryPoint);
	        	}
        	} catch (RemoteException e) {
        		e.printStackTrace();
        		Logger.error(this, e.getMessage());
        	}
        } else {
            TvChannel tvCh = ch.getTvCh();
            tuneToChannel(tvCh);
        }
    }

    /**
     * The Class ScrollShadowComp is for shadow when scroll component is showed.
     */
    protected class ScrollShadowComp extends Component {
        /** serialVersionUID. */
        private static final long serialVersionUID = 1L;

        /** The shadow img. */
        private Image shadowImg = null;

        /**
         * Sets the shadow img.
         *
         * @param shaImg the new shadow img
         */
        public void setShadowImg(Image shaImg) {
            shadowImg = shaImg;
        }

        /**
         * paint.
         *
         * @param        g           Graphics 객체.
         */
        public void paint(Graphics g) {
            g.drawImage(shadowImg, 0, 0, this);
        }
    }

    /**
     * <code>DataLoadingThread</code> This class process datas loaded in queue.
     *
     * @since   2011.02.25
     * @version 1.0, 2010.02.25
     * @author  tklee
     * @see     java.lang.Runnable
     */
    public class DataLoadingThread implements Runnable {
        /** The thread. */
        private Thread thread = null;
        /** The queue for process ID. */
        private ArrayList queue = null;

        /**
         * This is constructor to define variables.
         */
        public DataLoadingThread() {
            queue = new ArrayList();
        }

        /**
         * set Object for processing into queue.
         * @param obj   Object for processing.
         */
        public void setObj(Object[] obj) {
            Logger.debug(this, "called setObj()");
            if (obj == null) { return; }
            if (thread == null) {
                thread = new Thread(this, "BasicUI-DataLoadingThread");
                thread.start();
            }

            if (queue != null) {
                synchronized (queue) {
                    queue.add(obj);
                    Logger.debug(this, "run() - queue.add");
                    queue.notify();
                    Logger.debug(this, "run() - request queue notify");
                }
            }
        }

        /**
         * stop Thread.
         */
        public void stop() {
            thread = null;
            queue.clear();
            synchronized (queue) {
                queue.notify();
            }
        }

        /**
         * run Thread.
         */
        public void run() {
            while (thread != null) {
                try {
                    synchronized (queue) {
                        if (queue.isEmpty()) {
                            Logger.debug(this, "run() - queue wait");
                            queue.wait();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (thread == null) { break; }
                Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                if (!queue.isEmpty()) {
                    Object[] obj = (Object[]) queue.remove(0);
                    String procId = (String) obj[0];
                    Logger.debug(this, "run() - procId : " + procId);
                    Logger.debug(this, "run() - queue.isEmpty() : " + queue.isEmpty());
                    if (queue.isEmpty()) { notifyFromDataLoadingThread(obj, true); }
                }
            }
            queue.clear();
        }
    }
}

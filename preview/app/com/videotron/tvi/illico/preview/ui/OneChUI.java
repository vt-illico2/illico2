/*
 *  @(#)OneChUI.java 1.0 2011.03.19
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.ui;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.preview.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.preview.controller.oob.OobManager;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.gui.BasicRenderer;
import com.videotron.tvi.illico.preview.gui.OneChRenderer;
import com.videotron.tvi.illico.util.Environment;

/**
 * <code>OneChUI</code> The class show the information of one channel.
 *
 * @since   2011.03.19
 * @version $Revision: 1.17 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class OneChUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MENU_SUBSCRIBE. */
    public static int MENU_SUBSCRIBE = 0;
    /** The Constant MENU_TUEN. */
    public static int MENU_TUEN = 1;
    /** The Constant MENU_VIEW_ALL. */
    public static int MENU_VIEW_ALL = 2;

    /** Is the OneChRenderer instance. */
    private OneChRenderer renderer = null;
    /** The Channel instance. */
    //public Channel ch = null;
    /** The ClickingEffect instance. */
    private ClickingEffect clickEffect = null;

    /** The menu focus. */
    public int menuFocus = 0;

    /**
     * Initialize the Scene.
     */
    public OneChUI() {
        renderer = new OneChRenderer();
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
        if (!Config.showSubscribeMenu) {
            MENU_SUBSCRIBE = -1;
            MENU_TUEN = 0;
            MENU_VIEW_ALL = 1;
        }
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     */
    public void start(boolean isReset) {
        super.start(isReset);
        prepare();
        scrollShadowComp.setBounds(233, 466, 683, 17);
        scrollShadowComp.setShadowImg(BasicRenderer.hotkeybgImg);

        if (isReset) {
            menuFocus = 0;
        } else  if (focusHistory != null && focusHistory.length > 0) {
            menuFocus = Integer.parseInt((String) focusHistory[0]);
        }
        setVisible(true);
        loadData(new Object[]{ID_CH_DATA}, true);
    }

    /**
     * set the focus history.
     * called by super.stop()
     */
    protected void setFocusHistory() {
        Object[] objs = new Object[1];
        objs[0] = String.valueOf(menuFocus);
        setFocusHistory(objs);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        renderer.stop();
        ch = null;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (menuFocus > 0) {
                menuFocus--;
                repaint();
            }
            break;
        case Rs.KEY_DOWN:
            if (menuFocus < renderer.menuTxt.length - 1) {
                menuFocus++;
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            int addPos = 0;
            if (!Config.showSubscribeMenu) { addPos = 42; }
            clickEffect.start(626, 355 + (menuFocus * 39) + addPos, 910 - 626, 396 - 355);
            if (menuFocus == MENU_SUBSCRIBE) {
                //CommunicationManager.getInstance().requestStartUnboundApplication("Options",
                //        new String[]{MonitorService.REQUEST_APPLICATION_HOT_KEY});
            	// 4K
            	try {
                	if (ch.getTvCh().getDefinition() == TvChannel.DEFINITION_4K && !Environment.SUPPORT_UHD) {
                		CommunicationManager.getInstance().showCommonMessage(null);
                	} else {
                		PreferenceProxy.getInstance().showPinEnabler();
                	}
            	} catch (RemoteException e) {
            		e.printStackTrace();
            		Logger.error(this, e.getMessage());
            	}
            } else if (menuFocus == MENU_TUEN) {
                //TvChannel tvCh = ch.getTvCh();
                //tuneToChannel(tvCh);
                checkActionButton(ch, ISAService.ENTRY_POINT_FPR_E02);
            } else {
                //Object[] values = {getCurrentSceneId(), null, null};
                //SceneManager.getInstance().goToScene(SceneManager.SC_CH_LIST, true, values);
                SceneManager.getInstance().goToScene(SceneManager.SC_CH_LIST, true, null);
            }
            break;
        case Rs.KEY_PAGE_DOWN:
            footer.clickAnimation(BasicRenderer.btnScrollTxt);
            scrollText.showNextPage();
            break;
        case Rs.KEY_PAGE_UP:
            footer.clickAnimation(BasicRenderer.btnScrollTxt);
            scrollText.showPreviousPage();
            break;
        case Rs.KEY_BACK:
            footer.clickAnimation(BasicRenderer.btnBackTxt);
            //SceneManager.getInstance().goToScene(previousSceneID, false, null);
            SceneManager.getInstance().goToScene(SceneManager.SC_CH_LIST, true, null);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(Object[] obj, boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(ID_CH_DATA)) {
            super.notifyFromDataLoadingThread(obj, isFromThread);
            if (hChList != null) {
                Channel tempCh = (Channel) hChList.get(receivedData);
                if (tempCh == null) {
                    Logger.error(this, "cannot find this channel : " + receivedData);
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    showErrorMessage(ERRMSG_EXIT, OobManager.getInstance().errCode[OobManager.ERR_XML_PARSING]);
                } else {
                    if (!tempCh.isTryBrandImg()) {
                        tempCh.setBrandImg(getPreviewImage(tempCh.getBrandImgUrl(), tempCh.getId()));
                    }
                    if (tempCh.getTvCh() == null) {
                        tempCh.setTvCh(CommunicationManager.getInstance().getChannel(tempCh.getId()));
                    }
                    checkChannelData(tempCh);
                    FrameworkMain.getInstance().getImagePool().waitForAll();
                    CommunicationManager.getInstance().hideLoadingAnimation();

                    boolean isFr = Config.language.equals(Definitions.LANGUAGE_FRENCH);
                    String desc = tempCh.getSubDescEng();
                    if (isFr) { desc = tempCh.getSubDescFr(); }
                    //int yPos = 408;
                    int yPos = 400; //397
                    int lineCount = 4;
                    //if (!Channel.VISUAL_3D.equalsIgnoreCase(tempCh.getVisualType())) {
                    String shortCutdesc = tempCh.getShortcutDescEn();
                    if (isFr) { shortCutdesc = tempCh.getShortcutDescFr(); }
                    if (shortCutdesc == null || shortCutdesc.length() == 0) {
                        //yPos = 391;
                        yPos = 383;
                        lineCount = 5;
                    }
                    //scrollText.setBounds(55, yPos, 598 - 55, 472 - yPos);
                    scrollText.setBounds(50, yPos, 598 - 50, 473 - yPos);
                    scrollText.setFont(Rs.F14_D);
                    scrollText.setForeground(Rs.C255);
                    scrollText.setRows(lineCount);
                    scrollText.setRowHeight(16);
                    scrollText.setContents(desc);
                    //add(scrollShadowComp);
//                    scrollText.setBottomMaskImage(renderer.scrollShadowImg);
//                    scrollText.setTopMaskImage(renderer.scrollShadowTopImg);
                    scrollText.setInsets(408 - 400, 55 - 50, 473 - 471, 0);
                    add(scrollShadowComp);
                    add(scrollText);
                    addFooterBtn();
                    ch = tempCh;
                    repaint();
                }
            }
        }
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        int pageCount = scrollText.getPageCount();
        if (previousSceneID != -1) {
            footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        }
        if (pageCount > 1) { footer.addButtonWithLabel(BasicRenderer.btnPageImg, BasicRenderer.btnScrollTxt); }
    }
}

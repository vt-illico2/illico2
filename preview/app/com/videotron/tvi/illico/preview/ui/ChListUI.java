/*
 *  @(#)ChListUI.java 1.0 2011.03.20
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.ui;

import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.rmi.RemoteException;
import java.util.Vector;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.preview.common.Config;
import com.videotron.tvi.illico.preview.common.Logger;
import com.videotron.tvi.illico.preview.common.Rs;
import com.videotron.tvi.illico.preview.controller.communication.CommunicationManager;
import com.videotron.tvi.illico.preview.controller.communication.ISAProxy;
import com.videotron.tvi.illico.preview.controller.communication.PreferenceProxy;
import com.videotron.tvi.illico.preview.controller.ui.SceneManager;
import com.videotron.tvi.illico.preview.data.Channel;
import com.videotron.tvi.illico.preview.gui.BasicRenderer;
import com.videotron.tvi.illico.preview.gui.ChListRenderer;
import com.videotron.tvi.illico.preview.ui.comp.MainMenuUI;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>ChListUI</code> The class show the information of channel list.
 *
 * @since   2011.03.20
 * @version $Revision: 1.17 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class ChListUI extends BasicUI {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant MENU_SUBSCRIBE. */
    public static int MENU_SUBSCRIBE = 0;
    /** The Constant MENU_TUEN. */
    public static int MENU_TUEN = 1;
    /** The Constant MENU_VIEW_ALL. */
    public static int MENU_VIEW_ALL = 2;

    /** The Constant STATE_LIST. */
    public static final int STATE_LIST = 0;
    /** The Constant STATE_MENU. */
    public static final int STATE_MENU = 1;

    /** The Constant ROWS_PER_PAGE. */
    public static final int ROWS_PER_PAGE = 11;
    /** The Constant MIDDLE_POS. */
    public static final int MIDDLE_POS = 5;

    /** Is the OneChRenderer instance. */
    private ChListRenderer renderer = null;
    /** The Channel instance. */
    //public Channel ch = null;
    /** The ClickingEffect instance. */
    private ClickingEffect clickEffect = null;
    /** The AlphaEffect instance. */
    private AlphaEffect fadeIn = null;
    /** The main menu component. */
    private MainMenuUI mainMenu = null;

    /** The menu focus. */
    public int menuFocus = 0;
    /** The state focus. */
    public int stateFocus = 0;
    /** The list focus. */
    public int listFocus = 0;
    /** The list position. */
    public int listPosition = 0;
    /** The thread index. */
    private int threadIndex = -1;
    /** The is main menu effect. */
    private boolean isMainMenuEffect = false;

    /**
     * Initialize the Scene.
     */
    public ChListUI() {
        renderer = new ChListRenderer();
        setRenderer(renderer);
        clickEffect = new ClickingEffect(this, 5);
        fadeIn = new AlphaEffect(this, 8, AlphaEffect.FADE_IN);
        mainMenu = new MainMenuUI();
        mainMenu.setBounds(48, 101, 304, 485 - 101);
        mainMenu.setRenderer(new MainMenuRenderer());
        if (!Config.showSubscribeMenu) {
            MENU_SUBSCRIBE = -1;
            MENU_TUEN = 0;
            MENU_VIEW_ALL = 1;
        }
    }

    /**
     * Start the Scene.
     *
     * @param isReset whether resources is reset or not.
     */
    public void start(boolean isReset) {
        Logger.debug(this, "called start()");
        super.start(isReset);
        prepare();
        //scrollShadowComp.setShadowImg(renderer.scrollShadowImg);
        //scrollShadowComp.setBounds(548, 286, 346, 33);

        setVisible(false);
        isMainMenuEffect = false;
        if (isReset) {
            menuFocus = 0;
            stateFocus = 0;
            listFocus = 0;
            listPosition = 0;
        } else  if (focusHistory != null && focusHistory.length > 3) {
            menuFocus = Integer.parseInt((String) focusHistory[0]);
            stateFocus = Integer.parseInt((String) focusHistory[1]);
            listFocus = Integer.parseInt((String) focusHistory[2]);
            listPosition = Integer.parseInt((String) focusHistory[3]);
        }
        //loadData(new Object[]{ID_CH_DATA}, true);
        if (vChList == null) {
            this.notifyFromDataLoadingThread(new Object[]{ID_CH_DATA}, false);
        } else {
            setVisible(true);
            loadData(new Object[]{ID_CH_DATA}, true);
        }
    }

    /**
     * set the focus history.
     * called by super.stop()
     */
    protected void setFocusHistory() {
        Object[] objs = new Object[4];
        objs[0] = String.valueOf(menuFocus);
        objs[1] = String.valueOf(stateFocus);
        objs[2] = String.valueOf(listFocus);
        objs[3] = String.valueOf(listPosition);
        setFocusHistory(objs);
    }

    /**
     * Stop the Scene.
     */
    public void stop() {
        super.stop();
        renderer.stop();
        ch = null;
        threadIndex = -1;
    }

    /**
     * Dispose scene.
     */
    public void dispose() {
        super.dispose();
        renderer.stop();
    }

    /**
     * It does a key action.
     *
     * @param keyCode the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        switch (keyCode) {
        case Rs.KEY_UP:
            if (stateFocus == STATE_LIST) {
                if (listFocus > 0) {
                    if (listPosition != MIDDLE_POS || listFocus == listPosition) { listPosition--; }
                    listFocus--;
                    setChData();
                }
            } else {
                if (menuFocus > 0) {
                    menuFocus--;
                    repaint();
                }
            }
            break;
        case Rs.KEY_DOWN:
            if (stateFocus == STATE_LIST) {
                int totalCount = vChList.size();
                if (listFocus < totalCount - 1) {
                    if (listPosition != MIDDLE_POS || listPosition + listFocus >= totalCount - 1) { listPosition++; }
                    listFocus++;
                    setChData();
                }
            } else {
                if (menuFocus < renderer.menuTxt.length - 1) {
                    menuFocus++;
                    repaint();
                }
            }
            break;
        case Rs.KEY_LEFT:
            if (stateFocus > 0) {
                stateFocus--;
                repaint();
            }
            break;
        case Rs.KEY_RIGHT:
            if (stateFocus == 0) {
                stateFocus++;
                menuFocus = 0;
                repaint();
            }
            break;
        case Rs.KEY_ENTER:
            Channel channel = (Channel) vChList.elementAt(listFocus);
            Object[] values = {getCurrentSceneId(), channel.getId(), null};
            if (stateFocus == STATE_LIST) {
                clickEffect.start(52, 101 + (listPosition * 34), 348 - 52, 142 - 101);
                SceneManager.getInstance().goToScene(SceneManager.SC_ONE_CH, true, values);
            } else {
                int addPos = 0;
                if (!Config.showSubscribeMenu) { addPos = 42; }
                clickEffect.start(626, 355 + (menuFocus * 39) + addPos, 910 - 626, 396 - 355);
                if (menuFocus == MENU_SUBSCRIBE) {
                    //CommunicationManager.getInstance().requestStartUnboundApplication("Options",
                    //        new String[]{MonitorService.REQUEST_APPLICATION_HOT_KEY});
                	// 4K
                	try {
	                	if (ch.getTvCh().getDefinition() == TvChannel.DEFINITION_4K && !Environment.SUPPORT_UHD) {
	                		CommunicationManager.getInstance().showCommonMessage(null);
	                	} else {
	                		PreferenceProxy.getInstance().showPinEnabler();
	                	}
                	} catch (RemoteException e) {
                		e.printStackTrace();
                		Logger.error(this, e.getMessage());
                	}
                } else if (menuFocus == MENU_TUEN) {
                    //TvChannel tvCh = ch.getTvCh();
                    //tuneToChannel(tvCh);
                    checkActionButton(ch, ISAService.ENTRY_POINT_FPR_E01);
                } else { SceneManager.getInstance().goToScene(SceneManager.SC_ONE_CH, true, values); }
            }
            break;
        case Rs.KEY_PAGE_DOWN:
            footer.clickAnimation(BasicRenderer.btnScrollTxt);
            scrollText.showNextPage();
            break;
        case Rs.KEY_PAGE_UP:
            footer.clickAnimation(BasicRenderer.btnScrollTxt);
            scrollText.showPreviousPage();
            break;
        case Rs.KEY_BACK:
            //footer.clickAnimation(BasicRenderer.btnBackTxt);
            //SceneManager.getInstance().goToScene(previousSceneID, false, null);
            break;
        default:
            return false;
        }
        return true;
    }

    /**
     * Sets the ch data.
     * if ch data is already exist, only repaint().
     */
    private void setChData() {
        Channel curCh = (Channel) vChList.elementAt(listFocus);
        if (curCh.getPreviewImg() != null || curCh.getTvCh() != null || curCh.isTryPreviewImg()) {
            ch = curCh;
            setScrollText(ch);
            add(scrollText);
            repaint();
        } else {
            repaint();
            loadData(new Object[]{ID_CH_IMAGE, new Integer(listFocus), new Integer(++threadIndex)}, false);
        }
    }

    /**
     * Sets the scroll text.
     *
     * @param channel the new scroll text
     */
    private void setScrollText(Channel channel) {
        boolean isFr = Config.language.equals(Definitions.LANGUAGE_FRENCH);
        String desc = channel.getDescEng();
        if (isFr) { desc = channel.getDescFr(); }
        //int yPos = 194;
        int yPos = 186;
        int lineCount = 7;
        //if (!Channel.VISUAL_3D.equalsIgnoreCase(channel.getVisualType())) {
        String shortCutdesc = channel.getShortcutDescEn();
        if (isFr) { shortCutdesc = channel.getShortcutDescFr(); }
        if (shortCutdesc == null || shortCutdesc.length() == 0) {
            //yPos = 166;
            yPos = 158;
            lineCount = 8;
        }
        //scrollText.setBounds(553, yPos, 909 - 553, 310 - yPos); //real
        scrollText.setBounds(548, yPos, 909 - 548, 320 - yPos); //test
        scrollText.setFont(Rs.F14_D);
        scrollText.setForeground(Rs.C255);
        scrollText.setRows(lineCount);
        scrollText.setRowHeight(16);
        scrollText.setContents(desc);
//        scrollText.setBottomMaskImage(renderer.scrollShadowImg);
//        scrollText.setTopMaskImage(renderer.scrollShadowTopImg);
        scrollText.setInsets(194 - 186, 553 - 548, 320 - 310, 0);
        //add(scrollShadowComp);
        //add(scrollText);
        addFooterBtn();
    }
    
    /**
     * Notify from data loading thread.
     *
     * @param obj the data for processing
     * @param isFromThread whether is returned from thread.
     */
    protected void notifyFromDataLoadingThread(final Object[] obj, final boolean isFromThread) {
        Logger.debug(this, "called notifyFromDataLoadingThread()");
        String procId = (String) obj[0];
        if (procId.equals(ID_CH_DATA)) {
            super.notifyFromDataLoadingThread(obj, isFromThread);
            Logger.debug(this, "called notifyFromDataLoadingThread()-vChList : " + vChList);
            if (vChList != null) {
                final Channel tempCh = (Channel) vChList.elementAt(listFocus);
                if (!tempCh.isTryPreviewImg()) {
                    tempCh.setPreviewImg(getPreviewImage(tempCh.getPreviewImgUrl(), tempCh.getId()));
                }
                if (tempCh.getTvCh() == null) {
                    tempCh.setTvCh(CommunicationManager.getInstance().getChannel(tempCh.getId()));
                }
                checkChannelData(tempCh);
                FrameworkMain.getInstance().getImagePool().waitForAll();
                EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        CommunicationManager.getInstance().hideLoadingAnimation();
                        if (!isFromThread) { setVisible(false); }
                        setScrollText(tempCh);
                        if (!isFromThread) { fadeIn.start(); }
                        setVisible(true);
                        add(mainMenu);
                        mainMenu.setVisible(false);
                        if (!isFromThread) {
                            isMainMenuEffect = true;
                            mainMenu.startEffect();
                            isMainMenuEffect = false;
                        }
                        add(scrollText);
                        ch = tempCh;
                        mainMenu.setVisible(true);
                        repaint();
                    }
                });
            }
        } else if (procId.equals(ID_CH_IMAGE)) {
            new Thread("ChListUI|ID_CH_IMAGE") {
                public void run() {
                    int focus = ((Integer) obj[1]).intValue();
                    int index = ((Integer) obj[2]).intValue();
                    if (index != threadIndex || focus != listFocus
                            || CommunicationManager.getInstance().isLoadingAnimation()) { return; }
                    try {
                        Thread.sleep(600);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Channel reqCh = (Channel) vChList.elementAt(focus);
                    if (index != threadIndex || focus != listFocus
                            || CommunicationManager.getInstance().isLoadingAnimation()) { return; }
                    CommunicationManager.getInstance().showLoadingAnimation();
                    if (reqCh.getPreviewImg() == null && !reqCh.isTryPreviewImg()) {
                        reqCh.setPreviewImg(getPreviewImage(reqCh.getPreviewImgUrl(), reqCh.getId()));
                    }
                    if (reqCh.getTvCh() == null) {
                        reqCh.setTvCh(CommunicationManager.getInstance().getChannel(reqCh.getId()));
                    }
                    checkChannelData(reqCh);
                    CommunicationManager.getInstance().hideLoadingAnimation();
                    if (focus == listFocus && index == threadIndex) {
                        setScrollText(reqCh);
                        ch = reqCh;
                        repaint();
                    }
                }
            } .start();
        }
    }

    /**
     * Adds the footer button.
     */
    protected void addFooterBtn() {
        footer.reset();
        /*if (previousSceneID != -1) {
            footer.addButtonWithLabel(BasicRenderer.btnBackImg, BasicRenderer.btnBackTxt);
        }*/
        footer.addButtonWithLabel(BasicRenderer.btnExitImg, BasicRenderer.btnExitTxt);
        int pageCount = scrollText.getPageCount();
        if (pageCount > 1) { footer.addButtonWithLabel(BasicRenderer.btnPageImg, BasicRenderer.btnScrollTxt); }
    }

    /**
     * <code>ChListRenderer</code> The class to paint a GUI for channel list information.
     *
     * @since   2011.03.20
     * @version $Revision: 1.17 $ $Date: 2017/01/09 20:35:39 $
     * @author  tklee
     */
    public class MainMenuRenderer extends Renderer {
        /**
         * Get the renderer bound of UI .
         *
         * @param c the UIComponent
         * @return the preferred bounds
         */
        public Rectangle getPreferredBounds(UIComponent c) {
            return mainMenu.getBounds();
        }

        /**
         * This will be called before the related UIComponent is shown.
         *
         * @param c the c
         */
        public void prepare(UIComponent c) { }

        /**
         * Graphics paint.
         *
         * @param g the Graphics
         * @param c the UIComponent
         */
        protected void paint(Graphics g, UIComponent c) {//mainMenu.setBounds(48, 101, 304, 485 - 101);
            g.translate(-48, -101);
            Vector list = BasicUI.vChList;
            int totalCount = list.size();
            if (totalCount > 0) {
                int initNum = listFocus - listPosition;
                int lastNum = 0;
                if (totalCount < ChListUI.ROWS_PER_PAGE) { lastNum = totalCount - 1; }
                lastNum = listFocus + ChListUI.ROWS_PER_PAGE - 1 - listPosition;
                if (lastNum >= totalCount) { lastNum = totalCount - 1; }
                for (int i = initNum, j = 0; i <= lastNum; i++, j++) {
                    int addPos = (j * 34);
                    if (j < ChListUI.ROWS_PER_PAGE - 1) { g.drawImage(renderer.listLineImg, 66, 139 + addPos, c); }
                    if (i != listFocus || (stateFocus == ChListUI.STATE_MENU)) {
                        Object item = list.elementAt(i);
                        paintItem(g, c, false, item, addPos);
                    }
                }
                if (stateFocus == ChListUI.STATE_LIST) {
                    paintItem(g, c, true, list.elementAt(listFocus), (listPosition * 34));
                    if (initNum > 0) { g.drawImage(renderer.listArrowTopImg, 190, 95, c); }
                    if (lastNum < totalCount - 1) { g.drawImage(renderer.listArrowBottomImg, 190, 468, c); }
                }
            }
            g.translate(48, 101);
        }

        /**
         * Paint item.
         *
         * @param g the Graphics
         * @param c the UIComponent
         * @param isFocus the is focus
         * @param item the item
         * @param addPos the add position
         */
        private void paintItem(Graphics g, UIComponent c, boolean isFocus, Object item, int addPos) {
            Logger.debug(this, "paintItem()-mainMenu.isVisible() : " + mainMenu.isVisible());
            if (isFocus && !isMainMenuEffect) {
                g.setColor(Rs.C3);
                g.setFont(Rs.F21);
                g.drawImage(renderer.listFocusImg, 48, 101 + addPos, c);
            } else {
                g.setColor(Rs.C230);
                g.setFont(Rs.F18);
                g.drawImage(renderer.listBulletImg, 72, 119 + addPos, c);
            }
            Channel ch = (Channel) item;
            int limitPos = 332;
            if (Channel.STATUS_NEW.equalsIgnoreCase(ch.getStatus())) {
                Image iconImg = BasicRenderer.iconNewEnImg;
                if (isFocus) { iconImg = renderer.iconNewEnFocusImg; }
                if (Config.language.equals(Definitions.LANGUAGE_FRENCH)) {
                    iconImg = BasicRenderer.iconNewFrImg;
                    if (isFocus) { iconImg = renderer.iconNewFrFocusImg; }
                }
                int imgPos = 332 - iconImg.getWidth(c);
                limitPos = imgPos - 8;
                g.drawImage(iconImg, imgPos, 113 + addPos, c);
            }
            String str = TextUtil.shorten(ch.getName(), g.getFontMetrics(), limitPos - 94);
            g.drawString(str, 94, 128 + addPos);
        }
    }
}

/*
 *  @(#)MainMenuUI.java 1.0 2011.04.18
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.ui.comp;

import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;

/**
 * <code>MainMenuUI</code> the component for main menu.
 *
 * @since   2011.04.18
 * @version $Revision: 1.3 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public class MainMenuUI extends UIComponent {
    /** Is the serialVersionID. */
    private static final long serialVersionUID = 1L;

    /** The Constant ROWS_PER_PAGE. */
    public static final int ROWS_PER_PAGE = 11;
    /** The Constant MIDDLE_POS. */
    public static final int MIDDLE_POS = 5;

    /** The AlphaEffect instance. */
    private AlphaEffect fadeIn = null;

    /*public int listFocus = 0;
    public int listPosition = 0;
    private int listCount = 0;*/

    /**
     * Initialize the Component.
     */
    public MainMenuUI() { }

    /**
     * set Renderer instance and create AlphaEffect instance.
     *
     * @param renderer the Renderer object
     * @see com.videotron.tvi.illico.framework.UIComponent#setRenderer(com.videotron.tvi.illico.framework.Renderer)
     */
    public void setRenderer(Renderer renderer) {
        super.setRenderer(renderer);
        //clickEffect = new ClickingEffect(this, 5);
        fadeIn = new AlphaEffect(this, 15, AlphaEffect.FADE_IN);
    }

    /**
     * Start effect.
     */
    public void startEffect() {
        fadeIn.start();
    }

    /*public void setListCount(int totalCount) {
        this.listCount = totalCount;
    }

    /*public void reset() {
        listFocus = 0;
        listPosition = 0;
        listCount = 0;
    }
    public void keyUp() { }
    public void keyDown() { }*/
}

/*
 *  @(#)Rs.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.preview.common;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * This class is the Resource class.
 * @version $Revision: 1.3 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public final class Rs {
    /** The Constant F14. */
    public static final Font F14 = FontResource.BLENDER.getFont(14);
    /** The Constant F15. */
    public static final Font F15 = FontResource.BLENDER.getFont(15);
    /** The Constant F17. */
    public static final Font F17 = FontResource.BLENDER.getFont(17);
    /** The Constant F18. */
    public static final Font F18 = FontResource.BLENDER.getFont(18);
    /** The Constant F20. */
    public static final Font F20 = FontResource.BLENDER.getFont(20);
    /** The Constant F21. */
    public static final Font F21 = FontResource.BLENDER.getFont(21);
    /** The Constant F22. */
    public static final Font F22 = FontResource.BLENDER.getFont(22);
    /** The Constant F23. */
    public static final Font F23 = FontResource.BLENDER.getFont(23);
    /** The Constant F24. */
    public static final Font F24 = FontResource.BLENDER.getFont(24);
    /** The Constant F14_D. */
    public static final Font F14_D = FontResource.DINMED.getFont(14);

    /** The Constant C0. */
    public static final Color C0 = new Color(0, 0, 0);
    /** The Constant C3. */
    public static final Color C3 = new Color(3, 3, 3);
    /** The Constant C46. */
    public static final Color C46 = new Color(46, 46, 46);
    /** The Constant C214182061. */
    public static final Color C214182061 = new Color(214, 182, 61);
    /** The Constant C210. */
    public static final Color C210 = new Color(210, 210, 210);
    /** The Constant C224. */
    public static final Color C224 = new Color(224, 224, 224);
    /** The Constant C230. */
    public static final Color C230 = new Color(230, 230, 230);
    /** The Constant C236. */
    public static final Color C236 = new Color(236, 236, 236);
    /** The Constant C241. */
    public static final Color C241 = new Color(241, 241, 241);
    /** The Constant C254. */
    public static final Color C254 = new Color(254, 254, 254);
    /** The Constant C255. */
    public static final Color C255 = new Color(255, 255, 255);
    /** The Constant C255203000. */
    public static final Color C255203000 = new Color(255, 203, 0);

    /** The Constant DVB90C255. */
    public static final Color DVB90C255 = new DVBColor(255, 255, 255, 255 * 90 / 100);

    /** Screen Size. */
    public static final Rectangle SCENE_BOUND = new Rectangle(0, 0, 960, 540);

    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = KeyEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = KeyEvent.VK_DOWN;
    /** The Constant KEY_ENTER. */
    public static final int KEY_ENTER = HRcEvent.VK_ENTER;
    /** The Constant KEY_PAGE_UP. */
    public static final int KEY_PAGE_UP = HRcEvent.VK_PAGE_UP;
        /** The Constant KEY_PAGE_DOWN. */
    public static final int KEY_PAGE_DOWN = HRcEvent.VK_PAGE_DOWN;
    /** The Constant KEY_BACK. */
    public static final int KEY_BACK = KeyCodes.LAST;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;

    /**
     * Instantiates a contructor.
     */
    private Rs() { }
}

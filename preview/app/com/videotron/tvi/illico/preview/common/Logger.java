/*
 *  @(#)Logger.java 1.0 2011.02.23
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.preview.common;

import com.videotron.tvi.illico.log.Log;

/**
 * <code>Logger</code> this class print the log.
 *
 * @since   2011.02.23
 * @version $Revision: 1.4 $ $Date: 2017/01/09 20:35:39 $
 * @author  tklee
 */
public final class Logger {
    /**
     * Constructor.
     */
    private Logger() { }

    /**
     * print logs for info.
     *
     * @param   obj         the object of class
     * @param   message     message for printing
     */
    public static void error(Object obj, String message) {
        Log.printError(createMessage(obj, message));
    }

    /**
     * print logs for info.
     *
     * @param   obj         the object of class
     * @param   message     message for printing
     */
    public static void info(Object obj, String message) {
        Log.printInfo(createMessage(obj, message));
    }

    /**
     * print logs for debug.
     *
     * @param   obj         the object of class
     * @param   message     message for printing
     */
    public static void debug(Object obj, String message) {
        Log.printDebug(createMessage(obj, message));
    }

    /**
     * return log message.
     *
     * @param obj the obj
     * @param message the message
     * @return created message
     */
    private static String createMessage(Object obj, String message) {
        String classFullyName = obj.getClass().getName();
        String className = classFullyName.substring(classFullyName.lastIndexOf('.') + 1);
        return className + "|" + message;
    }
}

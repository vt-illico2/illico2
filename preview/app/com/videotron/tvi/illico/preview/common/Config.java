/*
 *  @(#)Config.java 1.0 2011.03.17
 *  Copyright (c) 2011 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */

package com.videotron.tvi.illico.preview.common;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;

/**
 * <code>Config</code> the class for application configuration.
 *
 * @version $Revision: 1.24 $ $Date: 2017/01/09 20:35:39 $
 * @author tklee
 */
public final class Config {
    /** Application Version. */
    public static final String APP_VERSION = "(R7.2+CYO).1.0";//"R4_1.1.0";//"1.4.0.0";
    /** Application Name. */
    public static final String APP_NAME = "Preview";

    /** The language. */
    public static String language = Definitions.LANGUAGE_FRENCH;
    /** The show subscribe menu. */
    public static boolean showSubscribeMenu = false;

    /**
     * set configuration values.
     */
    static {
        try {
            showSubscribeMenu = DataCenter.getInstance().getBoolean("showSubscribeMenu");
            Logger.debug(Config.class, "showSubscribeMenu : " + showSubscribeMenu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * constructor.
     */
    private Config() { }

    /**
     * @version 20101230_v2.0.1 - 최종 릴리즈
     */
}

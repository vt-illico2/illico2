﻿This is PVR App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version (R7.2+CYO).2.1
    - Release App file Name
      : PVR.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2016.10.4

***
version (R7.2+CYO).2.1 - 2016.10.4
    - New Feature   
        + fail.txt modified
    - Resolved Issues
        + VDTRMASTER-5865	 In the French version of the error message pop-up for PVR705, the error code mentioned is PVR700 instead of PVR705

***
version (R7.2+CYO).1.1 - 2016.9.19
    - New Feature   
        N/A
    - Resolved Issues
        + VDTRMASTER-5824	 [R7.2] Cannot play UHD content from recording list in stand alone mode (coax disconnected)

***
version R7.1.0 - 2016.3.31
    - New Feature   
        + R7
    - Resolved Issues
        N/A

***
version R5.8.2 - 2016.2.26
    - New Feature   
        N/A
    - Resolved Issues
        + VDTRMASTER-5702	 [PVR][R5][QUALIF-PROD] [R7] pvr terminal name is not saved
        + VDTRMASTER-5753	 [PVR] PVR801 message when trying to play multiple local records from R5 menu- PVR R5.7.0

***
version R5.7.0 - 2015.10.18
    - New Feature   
        + DDC-114
    - Resolved Issues
        N/A

***
version R5.6.1 - 2015.10.7
    - New Feature   
        N/A
    - Resolved Issues
        + VDTRMASTER-5658	 [R5] - Scheduled series on a PVR sometimes disappear

***
version R5.5.2 - 2015.9.10
    - New Feature   
        N/A
    - Resolved Issues
        + VDTRMASTER-5528	 [CQ][R5][PVR]: Portal is overlapped on PVR >list of recorded programs
        + VDTRMASTER-5588	 [CQ][R5][VBM][PVR]: Measurement value should be logged only when the PVR Asset page is accessed

***
version R5.4.1 - 2015.8.28
    - New Feature   
        + DDC-113 : '*'Key blocking
    - Resolved Issues
        + VDTRMASTER-5592	 [CQ][R5][VBM][PVR]: Scheduled recording: Generate only one measurement, even in case of repeated programs

***
version R5.3.0 - 2015.7.31
    - New Feature   
        + HDCP2.2
        + R5 Illico rebranding (Image changes)
        + Flat design (Image changes)
    - Resolved Issues
        N/A

***
version R5.2.4 - 2015.7.20
    - New Feature   
        + DDC-107
        + Tank
    - Resolved Issues
        + VDTRMASTER-5479	[CQ][UHD][R6][Companion] Wrong response result when scheduling a UHD recording
        + VDTRMASTER-5478	[CQ][UHD][R6][Companion] Wrong response result when playing a UHD content
        + VDTRMASTER-5474	[CQ][R5][PVR]: Definition Icon missing on Recording option pop-up
        + VDTRMASTER-5487	[PROD] Some Remote PVR calls don't work correctly when trying to record ongoing programs

***
version R5.1.2 - 2015.6.17
    - New Feature   
        + images changed, TOC changed
        + UHD_BITRATE_X in application.prop is changed.
    - Resolved Issues
        + VDTRMASTER-5441	[PVR] Modification (improvment) to display 320hr of available recording space
        + VDTRMASTER-5463	[CQ][UHD][R6][PVR] Conflict popup should not appear when recording repeated UHD program

***
version R5.1.0_beta - 2015.6.1
    - New Feature   
        + R5 : Unofficial release of R5
        + Image files and TOCs files are newly added, so please check contents when override it in Qualif or Prod.
    - Resolved Issues
        N/A

***
version 4K.2.1 - 2015.5.13
    - New Feature   
        + images changed
    - Resolved Issues
        + VDTRMASTER-5412	[CQ][UHD][R6][PVR] Check the amount of free space of UHD content in PVR list screen

***
version 4K.1.0 - 2015.4.10
    - New Feature   
        + [4K] Initial draft for periscope features.
        + [4K] Flat design applied.
        + [4K] UHD_BITRATE/UHD_BITRATE_X are added in application.prop
    - Resolved Issues
        N/A

***
version 1.5.15.2 - 2014.11.10
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5256	[CQ][ECLR][PVR]: Conflict management: unexpected recording is deleted when you exit the conflic screen
        + VDTRMASTER-5285   [CQ][ECLR][R4]: List of recorded programms is not updated correctly if record is deleted remotely	

***
version 1.5.14.1 - 2014.08.28
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5256	[CQ][ECLR][PVR]: Conflict management: unexpected recording is deleted when you exit the conflic screen

***
version 1.5.13.4 - 2014.08.11
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5246	[CQ][ECLR][R4][PVR]: Press Exit on recording conflict page, the Confirmation screen is not always displayed
        + VDTRMASTER-5256	[CQ][ECLR][R4][PVR]: Conflict management: unexpected recording is deleted when you exit the conflic screen
        + VDTRMASTER-5247	[CQ][ECLR][R4][PVR]: PVR LIST sorting option «By Terminal» is not saved after terminal reboot.
        + VDTRMASTER-5259	[CQ][ECLR][R4][PVR]: Conflict screen: The system is too slow, displays incorrect wireframe and description.

***
version 1.5.12.2 - 2014.07.21
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5215	[CQ][PO1/2/3][R4][Profile] Recording buffer popup doesn't show up
        + VDTRMASTER-5077	[VBM][CQ][R4] Measurement logs for Disk usage/capacity should not be collected/or send in case of HD STBs

***
version 1.5.11.2 - 2014.07.02
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5040	Buffer parameters not updated for scheduled recordings
        + VDTRMASTER-5171   [CQ] [R4][PVR] If on the "Remove repeat option" confirmation pop-up the user press exit, the "Modify Recording" lost the option to select "OK" or "Cancel" buttons.

***
version 1.5.10.1 - 2014.06.25
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5171   [CQ] [R4][PVR] If on the "Remove repeat option" confirmation pop-up the user press exit, the "Modify Recording" lost the option to select "OK" or "Cancel" buttons.

***
version 1.5.9.1 - 2014.06.11
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-5147	[CQ][ECLR][PVR] Conflict notification screen does not appear after scheduling 8 future recordings and adding a 9th current recording that is in conflict with the others

***
version 1.5.8.2 - 2014.05.27
    - New Feature
        + Merged from HEAD (PO1-2-3) PVR 1.4.7.1
    - Resolved Issues
        + VDTRMASTER-5095	[CQ][ECLR][R4][PVR] On press on 'Exit' key in the conflict pop-up, if the last action that triggered the conflict is a series or recurring recording is not ignored completly
        + VDTRMASTER-5099	[CQ][ECLR][R4][PVR] Channel isn`t being synthonised after conflict resolution in PIP mode

***
version 1.5.7.2 - 2014.05.14
    - New Feature
        + Merged from HEAD (PO1-2-3) PVR 1.4.6.4
    - Resolved Issues
        + VDTRMASTER-5095	[CQ][ECLR][R4][PVR] On press on 'Exit' key in the conflict pop-up, if the last action that triggered the conflict is a series or recurring recording is not ignored completly
        + VDTRMASTER-5097	[CQ][ECLR][R4][PVR] After a manual recording, the last recording confirmation popup is not launched

***
version 1.5.6.0 - 2014.05.07
    - New Feature
        N/A
    - Resolved Issues
        + Fixed bug - fastfoward is not working for the PVR playback.
		  Which is related to CISCOGTWY-58 [CQ][ECLR][R4][PVR] Fast-forward on a locale or remote recording is not working on a G8

***
version 1.5.5.3 - 2014.04.30
    - New Feature
        N/A
    - Resolved Issues
        + CISCOGTWY-47	[CQ][ECLR][R4][SDV][PVR] Starting the 8th recording on a SDV channel switches the channel to the previous recording one.
        + CISCOGTWY-38	[ING-DEV-VID][ECLR][PHASE1][PVR] PVR704 ERROR (INSUFFICIENT_RESSOURCES) HAPPENS WITH MORE THAN 8 RECORDINGS
        + VDTRMASTER-4803	[CQ][R3][MR] 'Too many occurences' popup doesn't show up

***
version 1.5.4.0 - 2014.04.16
    - New Feature
        + Merged from HEAD (PO1-2-3) PVR 1.4.4.3
    - Resolved Issues
        N/A

***
version 1.5.3.0 - 2014.04.03
    - New Feature
        + TOC and GUI modification.
        + Merged from HEAD (PO1-2-3) PVR 1.4.3.2
    - Resolved Issues
        N/A

***
version 1.5.2.0 - 2014.03.18
    - New Feature
        + TOC and GUI modification.
    - Resolved Issues
        N/A

***
version 1.5.1.0 - 2014.03.06
    - New Feature
        + Second release of Gateway PVR
    - Resolved Issues
        N/A

***
version 1.5.0.0 - 2014.02.19
    - New Feature
        + First release of Gateway PVR
    - Resolved Issues
        N/A

***
version 1.4.0.2_PO3 - 2014.02.18
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-5037   [CQ][PO1][R4][PVR] - Missing recordings on local STBs
        + VDTRMASTER-5034	[CQ][PO1][R4][PVR] - Sort by program length is not working correctly for ongoing recordings

***
version 1.3.2.1_PO3 - 2014.01.27
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-5016  [PO3 Companion][CQ][R4]PVR: The scheduled list version shall not be changed after watching a "NOT WATCHED"

***
version 1.3.1.1_PO3 - 2014.01.16
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-5009  [PO3 Companion] RPVR schedule recording returns error code even if there is no conflict.

***
version 1.3.0.0 - 2013.12.23
    - New Feature
        + Merged PO1 and 3 to R3 HEAD branch.

***
version 1.2.2.0_PO1 - 2013.11.04
    - New Feature
        + Changed shadow image of scroll text in PVR LIST.
    - Resolved Issues
        + N/A

***
version 1.2.1.0_PO1 - 2013.10.30
    - New Feature
        + Changed TOC - "Free space" in List footer.
    - Resolved Issues
        + N/A

***
version 1.2.0.0_PO1 - 2013.10.07
    - New Feature
        + First release including the followings
		+ DDC42.1 (Selection save)
		+ DDC 42.2 (Grouping/Degrouping) ==> Impact to Profile app
		+ DDC 42.4 (FF/REW with 4, 16, 64, 128X)
		+ DDC 42.7 (D option in VOD and PVR)
		+ DDC 42.9 (Stop recording when 2 busy tuners)
		+ DDC 77 Font size
    - Resolved Issues
        + N/A
		
***
version 1.2.10.1_PO3 - 2013.12.13
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  

***
version 1.2.9.2_PO3 - 2013.12.09
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4970  [CQ][R4][PVR][GUI]:Rec LIST: Long titles should not scroll over icons. 
        + VDTRMASTER-4971  [CQ][R4][PVR]: Upcoming scheduled recordings should not appear in the list of recordings

***
version 1.2.8.4_PO3 - 2013.12.05
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  
        + VDTRMASTER-4963  [CQ][PO3][Compagnon] Error code is not correct and STB goes to black screen when play a local pvr content of upcoming list.  
        + VDTRMASTER-4727  [CQ][R3][MR]:Deleting ongoing recordings on Client while the conflict notfication is on the Server  
        + VDTRMASTER-4964  PVR does not send VBM trigger logs.  

***
version 1.2.7.5_PO3 - 2013.11.27
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4786  PVR - Add the PVR801 code to the message "viewing failed" when it occured  
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  
        + VDTRMASTER-4718  [CQ][R3][PVR]: a pending scheduled recording should start once the tuner has been released  
        + VDTRMASTER-4951  [PO3 Companion] UDN should be equal to 0 for STBs that are not in multiroom.  
        + VDTRMASTER-4799  [CQ][R3][PVR]: Wrong error code «PVR706 instead of PVR 710» displays when the disk is full  

***
version 1.2.6.0_PO3 - 2013.11.15
    - New Feature
        + Merged from R3 HEAD
    - Resolve
        N/A

***
version 1.2.5.1_PO3 - 2013.11.12
    - New Feature
        + Shows PIN popup automatically when there is a limitation on the PR.
    - Resolved Issues
        + VDTRMASTER-4940  [PO3 Companion][CQ][R4] - Application should not allow to attempt recording an unsubscribed channel  

***
version 1.2.4.2_PO3 - 2013.11.08
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4925  [PO3 Companion][CQ][R4] - Can't resume viewing a PVR content 
        + VDTRMASTER-4935  [PO3 Companion] RPVR getRecordingsMetadata of a manual recording doesn't return the category parameter

***
version 1.2.3.2_PO3 - 2013.10.31
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4913  [PO3 Companion] RPVR getRecordingsList returns scheduled records even if they are skipped.  
        + VDTRMASTER-4910  [PO3 Companion] PVR getRecordingsList does not return UDN when type=1 or type=3  

***
version 1.2.2.0_PO3 - 2013.10.16
    - New Feature
        N/A
    - Resolved Issues
        + RPVR version will be updated when recording is partially viewed.

***
version 1.2.1.0_PO3 - 2013.10.02
    - New Feature
        + Applied "api" parameter to the remote PVR.

***
version 1.2.0.0_PO3 - 2013.09.27
    - New Feature
        + First release including the followings
        + UC-PVR01B - Schedule a recording
        + UC-PVR02B - Delete a scheduled recording
        + UC-PVR03B - Retrieve a list of recordings and scheduled recordings
        + UC-PVR04B - Start a PVR playback
        + UC-REM01B - Apply remote control subset to STB
        + UC-STA01B - Receive the current state of the STB
    - Resolved Issues
        + N/A 

display	Veuillez entrer le NIP admin|pour afficher le contenu adulte.	Please enter your Admin PIN|to display adult content.
stop	Veuillez entrer le NIP admin|pour arrêter cet enregistrement protégé.	Please enter your Admin PIN|to stop this protected recording.
delete	Veuillez entrer le NIP admin|pour supprimer cet enregistrement protégé.	Please enter your Admin PIN|to delete this protected recording.
modify	Veuillez entrer le NIP admin|pour modifier cet enregistrement protégé.	Please enter your Admin PIN|to modify this protected recording.
play	Veuillez entrer le NIP admin|pour lire cet enregistrement protégé.	Please enter your Admin PIN|to play this protected recording.
skip	Veuillez entrer le NIP admin|pour sauter cet enregistrement protégé.	Please enter your Admin PIN|to skip this protected recording.
block	Veuillez entrer le NIP admin|pour bloquer cet enregistrement.	Please enter your Admin PIN|to block this recording.
unblock	Veuillez entrer le NIP admin|pour débloquer cet enregistrement.	Please enter your Admin PIN|to unblock this recording.
unblock_all	Veuillez entrer le NIP admin|pour débloquer tous les enregistrements. 	Please enter your Admin PIN|to unblock all recordings. 
suspend	Veuillez entrer le NIP admin|pour suspendre les Contrôles et limites.	Please enter your Admin PIN|to suspend Controls and Limits.
restore	Veuillez entrer le NIP admin|pour rétablir les Contrôles et limites.	Please enter your Admin PIN|to restore Controls and Limits.
access_pref	Veuillez entrer le NIP admin|pour accéder aux paramètres.	Please enter your Admin PIN|to access your preferences.

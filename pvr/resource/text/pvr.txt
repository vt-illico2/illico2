Cancel	Annuler	Cancel
Close	Fermer	Close
Exit	Quitter	Exit
Search	Recherche	Search
ChangeList	Changer de liste	Change list
CreateRecordings	Enregistrement manuel	Manual Recording
Date	Date	Date
Every day	Chaque jour	Every day
Friday	Vendredi	Friday
Monday	Lundi	Monday
Saturday	Samedi	Saturday
Sunday	Dimanche	Sunday
Thursday	Jeudi	Thursday
Tuesday	Mardi	Tuesday
Wednesday	Mercredi	Wednesday
Fri.	Vendredi	Friday
GoRecordedList	Liste des enregistrements	List of recorded programs
Modify	Modifier	Modify
Mon.	Lundi	Monday
More Days	Plus de jours	More days
OK	OK	OK
Options	Options	Options
MsgRepeat1	Choisissez les jours de répétition.	Set repeat days.
PVR	ENP	PVR
Record	Enregistrer	Record
TitleConfirm	Confirmer l’enregistrement manuel	Confirm manual recording
Repeat	Répéter	Repeat
Sat.	Samedi	Saturday
Save	Sauvegarder	Save
Scroll Description	Défiler la description	Scroll description
Sorted	Tri :	Sorted :
Sun.	Dimanche	Sunday
Sunday	Dimanche	Sunday
Thu.	Jeudi	Thursday
TITLE	Titre	Title
Tue.	Mardi	Tuesday
Wed.	Mercredi	Wednesday
Weekdays	Jours de semaine	Weekdays
Weekends	Fins de semaine	Weekends
Everyday	Chaque jour	Every day
JAN	Jan	Jan
FEB	Fév	Feb
MAR	Mars	Mar
APR	Avril	April
MAY	Mai	May
JUNE	Juin	June
JULY	Juill	July
AUG	Août	Aug
SEP	Sep	Sep
OCT	Oct	Oct
NOV	Nov	Nov
DEC	Déc	Dec
MON	Lun.	Mon.
TUS	Mar.	Tus.
WEN	Mer.	Wed.
THU	Jeu.	Thu.
FRI	Ven.	Fri.
SAT	Sam.	Sat.
SUN	Dim.	Sun
More Details	Plus de détails	More Details
delete_popup_title	Supprimer l'enregistrement	Delete recording
delete_popup_msg	Voulez-vous vraiment supprimer	Do you really want to delete 
cancel_popup_title	Annuler l'enregistrement	Cancel recording
cancel_popup_msg	Voulez-vous vraiment annuler l'enregistrement de	Are you sure you want to cancel the recording of
resume_view	À partir de maintenant	Resume viewing
play_from_begin	Depuis le début	Play from beginning
stop_and_save	Arrêter et sauvegarder	Stop and save 
stop_and_delete	Arrêter et supprimer	Stop and delete
continue_recording	Poursuivre l’enregistrement	Continue recording
resume_popup_title	Reprendre le visionnement	Resume Viewing
Cancel	Annuler	Cancel
cancel_series_popup_title	Annuler la série	Cancel series
cancel_series_popup_msg	Voulez-vous vraiment annuler tous les enregistrements à venir de	Are you sure you want to cancel all upcoming recordings of
delete_series_popup_title	Supprimer la série	Delete Series
delete_series_popup_msg	Voulez-vous vraiment supprimer les épisodes enregistrés de	Do you really want to delete all recorded episodes of
is_recording_stop_msg	est en cours d’enregistrement.|Désirez-vous arrêter l’enregistrement?	is currently recording.|Do you want to stop recording this show?
ListEmpty	Aucune émission enregistrée.	The list of recorded programs is empty.
SchudededListEmpty	Aucun enregistrement à venir.	There is currently no upcoming recording.
channel_is_invalid	La chaîne est invalide.	Channel is invalid.
day	jour	day
days	jours	days
For	Durant	For
PVR_management	Gérer l'ENP	PVR Management
Start_wizard	Assistant ENP	PVR Wizard
Yes	Oui	Yes
No	Non	No
viewing	fois	time
viewings	fois	times
minute	min	min
conflictTitle	Conflit d’horaire	Scheduling conflict
conflictDesc0	Ces enregistrements entrent en conflit.	Some shows will not be recorded due to a scheduling conflict.
conflictDesc1	L'enregistrement surligné ci-dessous a occasionné un conflit d'horaire.	The highlighted recording below has caused a scheduling conflict.
conflictDesc2	Seulement 2 émissions peuvent être enregistrées à la fois.	Only 2 programs can be recorded at the same time.
conflictDesc3	Que voulez-vous faire?	What would you like to do?
conflictBtn1	Choisir émissions à enregistrer	Select shows to record
conflictBtn2	Autres heures de diffusion	See other showtimes
conflictBtn3	Résoudre plus tard	Resolve later
conflictPickTitle	Choisir les émissions à enregistrer	Select shows to record
conflictPickDesc1	Décochez les émissions à retirer	Uncheck shows you wish to remove.
conflictPickDesc2	Rouge: indique que les émissions choisies occasionnent un conflit d’horaire.	Red: indicates a scheduling conflict between selected programs.
conflictPickDesc3	Vert: indique que les émissions choisies seront enregistrées.	Green: indicates all selected programs can be recorded.
conflictPickBtn	Enreg. choix	Record selection
conflictConfirmTitle	Résumé des enregistrements	Recording Summary
conflictConfirmDesc1	Le système enregistrera :	System will record:
conflictConfirmDesc2	Le système n’enregistrera pas :	System will not record:
conflictConfirmDesc3	Le système enregistrera à un autre moment :	System will record at another time:
popup_title_resolved	Conflit résolu	Conflict Resolved
popup_title_not_resolved	Conflit non résolu	Unresolved Conflict
popup_title_not_resolved_many	Conflit non résolu	Conflict not resolved
conflictResolvedDesc	Le conflit est résolu.	Conflict is resolved.
conflictNotResolved	Certaines émissions risquent de ne pas être enregistrées comme prévu.	Some programs may not be recorded as scheduled.
cancelOptionTitle	Épisode en conflit	Conflicting episode
cancelOptionDesc	Plusieurs enregistrements de %% sont programmés.|Choisissez une option :	Several recordings have been set for %%.|Choose an option :
cancelOptionBtn1	Sauter cet épisode	Skip this episode
cancelOptionBtn2	Supprimer la série entière	Delete entire series
findOtherTitle	Autres heures de diffusion	Alternate Showtimes
findOtherDesc	Choisissez une émission pour voir les heures de diffusion disponibles	Select a program to see other available recording times.
selectOtherTitle	Choisir une heure de diffusion	Select Alternate Showtime
selectOtherDesc	Choisissez l’heure d’enregistrement de	Select a time to record
pvrIssueTitle	Problèmes d’ENP	PVR issues
pvrIssueList	problèmes à résoudre	issues to resolve
By date	par date	by date
sortByAlphabetical	Trier par ordre alphab.	Sort alphabetically
sortByDate	Trier par date	Sort by date
Back	Retour	Back
Back_to_menu	Retour : Menu ENP	Back : PVR menu
continue_view	Poursuivre le visionnement	Continue viewing
play_begin	Visionner depuis le début	Play from beginning
delete	Supprimer	Delete
Live_TV	Retour à la télé	Live TV
Return_list	Retour à la liste	Back to list
stopped_title	Émission interrompue	Program Stopped
finished_title	Émission terminée	End of program
rec_opt	Options d’enregistrement 	Recording options
rec_once	Enregistrer une fois	Record once
rec_all	Répéter l’enregistrement	Repeat recording
rec_this_ep	Enregistrer cet épisode	Record this episode
rec_all_ep	Enregistrer la série	Record series
view	Voir	View
notRecord	L'émission n'a pas été enregistrée au complet.	The program was not recorded in full.
issue_title_disk_low	Espace disque restreint	PVR disc space low
issue_title_conflict	Conflit d’horaire	Scheduling conflict
issue_title_failed	1 Problème d'ENP	1 PVR issue
issue_title_popup_failed	L'enregistrement a échoué	Recording Failed
issue_title_popup_cancelled	L’émission n’est plus disponible	Program Unavailable
issue_title_multi_1	Voir les	View your
issue_title_multi_2	problèmes	issues
issue_msg_disk	Disque plein	Disk full
issue_msg_conflict	Conflit d’horaire	Schedule conflict
issue_msg_failed	Échec	Failed
issue_msg_cancelled	Non dispo.	Unavailable
issue_msg_multi_0	Il n'y a plus de problème d'ENP.	There are no PVR issues anymore.
issue_msg_multi_1	Vous avez	You have
issue_msg_multi_2	problèmes d'ENP :	PVR issues:
issue_desc_disk	Certaines émissions risquent de ne pas être enregistrées tel que prévu en raison d’un manque d’espace sur le disque dur.	Some of your scheduled programs may not record due to a lack of disk space.
issue_desc_conflict	Trop d’enregistrements ont été programmés pour la même heure. Certaines émissions risquent de ne pas être enregistrées.	Too many programs are scheduled to record at the same time. Some programs may not be recorded.
issue_desc_failed	Raisons possibles :| |1. Changement à l'horaire|2. Terminal déconnecté|3. Résolution automatique d'un conflit|4. Problème technique|5. Chaîne non accessible	Possible reasons :| |1. Schedule change|2. Terminal disconnected|3. Automatic conflict resolution|4. Technical issue|5. Channel not accessible
issue_desc_multi	"Pour voir vos problèmes d'ENP, appuyez sur « OK »."	"To view your PVR issues, press ""OK""."
issue_desc_list_failed	"Pour voir votre problème d'ENP, appuyez sur « OK »."	"To view your PVR issue, press ""OK""."
issue_btn_disk	Nettoyer le disque	Clean disc
issue_btn_conflict	Résoudre le conflit	Resolve conflict
issue_btn_failed	Supprimer la notification	Delete notification
failed_popup_title	Cet enregistrement n'a pas pu s'effectuer :	Recording Failed
failed_popup_msg	L'enregistrement n'a pas pu s'effectuer.	This recording could not be completed.
cancelled_popup_title	Émission non disponible	Program Unavailable
cancelled_popup_msg	Cette émission a été supprimée de l’horaire.	This program was removed from the programming.
disk_popup_title_1	Espace disque :	Disc Space:
disk_popup_title_2	Espace disque :	Disc Space:
disk_popup_title_3	Disque plein	Disc Full
disk_popup_msg_1	Il ne reste que @% ou moins d'espace libre sur votre ENP.|Voulez-vous nettoyer votre disque maintenant?	Your PVR disc has less than @% free space for new recordings.|Do you want to clean your disc now?
disk_popup_msg_2	Il ne reste que @% ou moins d'espace libre sur votre ENP.|Certaines émissions risquent bientôt de ne pas s'enregistrer.|Voulez-vous nettoyer votre disque maintenant?	"Your PVR disc has less then @% of space left.|Very soon, programs will not be recorded.|Do you want to clean your disc now?"
disk_popup_msg_3	"Le disque dur de votre ENP est plein.|Pour enregistrer d’autres émissions, vous devez libérer de l’espace."	"Your PVR disc is full.|To record more programs, you need to free up some space."
disk_popup_button_clean	Nettoyer le disque	Clean disk
disk_popup_button_later_1	Plus tard	Remind me later
disk_popup_button_later_2	Annuler	Cancel
disk_popup_button_later_3	Plus tard	Remind me later
almost_popup_title	Disque dur presque plein	PVR Disc Almost Full
almost_popup_msg_1	Le disque dur de votre ENP est presque plein.|Les émissions suivantes risquent de ne pas être enregistrées :	Your PVR disc is almost full.|The following programs may not record:
almost_popup_msg_2	"Pour assurer l’enregistrement de vos émissions, vous devez libérer de l’espace."	"To insure these titles will be recorded, you need to free up some space."
conflict_popup_title	Conflit d’ENP	PVR Conflict
conflict_popup_msg	L'enregistrement de ces émissions a entraîné un conflit d'horaire :	The recording of these programs has caused a scheduling conflict:
conflict_popup_button_0	Résoudre maintenant	Resolve conflict
conflict_popup_button_1	Plus tard	Remind me later
more_programs	autres émissions	more programs 
lowPopupTitle	ESPACE RESTREINT SUR LE DISQUE DE L’ENP	LOW PVR DISK SPACE
fullPopupTitle	DISQUE DUR DE L’ENP PLEIN	PVR DISK FULL
fullbtn	Ignorer; ne pas enregistrer	Ignore; do not record
low5PopupTitle	Le disque dur de votre ENP est presque plein.	PVR disk space is almost full.
multiDeleteDesc1	Cochez les enregistrements que vous désirez supprimer et appuyez sur « Supprimer les enregistrements».	"Check the recordings you wish to delete and select ""Delete recordings"""
multiDeleteTitle	Supprimer plusieurs enregistrements	Delete Multiple Recordings 
multiDeleteBtn1	Supprimer les enregistrements	Delete Recordings
descRecorded	Enregistrements récents :	Latest recordings :
descUpcoming	Enregistrements à venir :	Upcoming recordings :
Local	Local	Local
Enter Your PIN	Entrer votre NIP	Enter Your PIN
UseHD	Enregistrer la chaîne HD	Record HD Channel
hdavailable	Canal HD disponible. Syntoniser le	HD channel available. Tune in to
stop_recording	Arrêter l’enregistrement	Stop recording
stop_recording_msg_1	est en cours d’enregistrement.	is being recorded.
stop_recording_msg_2	Voulez-vous arrêter l’enregistrement?	Do you want to stop this recording?
record_past_title	Enregistrement en décalage	Recording Timeshifted TV
record_past_msg_1	"Vous regardez ""%1"" en décalage."	"You are watching ""%1"" on timeshifted TV."
record_past_msg_2	"""%2"" est présentement diffusé selon l’horaire régulier."	"""%2"" is now showing on live TV. "
record_past_msg_3	Que voulez-vous enregistrer?	Which show do you want to record?
record_past_btn_1	Sauvegarder %% minutes de	Save %% minutes of
record_past_btn_2	Enregistrer	Record
record_past_btn_3	Annuler l’enregistrement	Cancel recording
cc_notice_title	Tous les syntoniseurs sont occupés	All tuners are busy
cc_notice_msg_1	"Pour changer de chaîne, vous devez arrêter l'enregistrement de ""%%""."	"To change channel, you must stop recording ""%%""."
cc_notice_msg_2	Que voulez-vous faire?	What do you want to do?
cc_notice_btn_1	Ne pas changer de chaîne	Do not change channel
cc_notice_btn_2	Arrêter l'enreg. et changer de chaîne	Stop recording and change channel
tuner_request_title	Tous les syntoniseurs sont occupés	All tuners are busy
tuner_request_msg_1	"Pour compléter cette action, vous devez arrêter un enregistrement en cours."	"To complete this action, you must stop one of your ongoing recordings."
tuner_request_msg_2	Choisissez l'enregistrement à arrêter:	Choose the recording to stop:
tuner_request_btn	Arrêter	Stop
sb_title	Émission en décalage	Time-Shift TV
sb_msg_1	"En changeant de chaîne, vous risquez de perdre une partie de l’émission."	"By changing the channel, you risk losing part of this program."
sb_msg_2	Désirez-vous sauvegarder la partie qui a été enregistrée automatiquement? (%% minutes)	Do you want to save what was automatically recorded? (%% minutes)
sb_btn_1	Sauvegarder et changer de chaîne	Save and change channel
sb_btn_2	Ne pas sauvegarder et changer de chaîne	Do not save and change channel
sb_msg_wait	. . . 	. . .
rec_con_title	Un enregistrement programmé est sur le point de commencer	A scheduled recording is about to start
rec_con_msg_1	"Un changement de chaîne s'effectuera pour enregistrer ""%1"" dans %2 (mm:ss)"	"Channel will change to record ""%1"" on another channel in %2 (mm:ss)"
rec_con_msg_2	Que désirez-vous faire?	What do you want to do?
rec_con_btn_1	Syntoniser et enregistrer	Tune in and record
rec_con_btn_2	Ne pas enregistrer	Do not record
dup_rec_title	Enregistrement en double	Duplicate Recording
dup_rec_msg	L’enregistrement de %1 a déjà été programmé.	%1 is already scheduled to be recorded.
cannot_past_title	Émission non disponible	Program not available
cannot_past_msg	Veuillez choisir une émission en cours ou à venir.	Please choose a current or an upcoming program.
page_up_down	Page haut/bas	Page up/down 
More Details	Plus de détails	More details
changeMonth	Changer de mois	Change month
modify_recording	Modifier l'enregistrement	Modify Recording
Start	Début	Start
End	Fin	End
Date	Date	Date
ViewEpisodes	Voir les épisodes	View episodes
RecOpt	Options d’enregistrement :	Recording options :
repeatePopTitle	Choisir jour(s)	Select Day(s)
inactive	Contenu inactif	Inactive content
standAloneTitle	Câble déconnecté	Cable disconnected
standAloneDesc	Le câble est déconnecté du terminal. Vérifiez que le câble est bien branché. Appuyez sur (LIST) pour visionner les émissions enregistrées sur ce terminal.	The cable is disconnected from the terminal. Check that your cable is properly connected. Press (LIST) to watch programs recorded on this terminal.
seletPopupBtn1	Chaque…	Every…
seletPopupBtn2	Non	No
strInActive	ne figure plus à la programmation.	is no longer on the schedule.
issue_stop_popup_msg	est en cours d’enregistrement.|Voulez-vous arrêter cet enregistrement?	is currently being recorded.|Do you want to stop this recording?
issue_stop_popup_btn_0	Arrêter et sauvegarder	Stop and save
issue_stop_popup_btn_1	Arrêter et supprimer	Stop and delete
issue_stop_popup_btn_2	Poursuivre l’enregistrement	Continue recording
time_error_popup_title	Plages incompatibles	Incompatible showtimes
time_error_popup_msg	"""%1"" %2|Les heures spécifiées (%3) ne correspondent pas avec celles de l'émission que vous souhaitez enregistrer.|Vérifiez les valeurs."	"""%1"" %2|The defined showtime (%3) does not match with the showtime of the program you wish to record.|Please select other values."
play_title_recorded	Débuter le visionnement	Play recording
play_msg_recorded	"Voulez-vous reprendre le visionnement de ""%%""?"	"Do you want to resume viewing of ""%%""?"
play_title_recording	Enregistrement en cours	Recording in Progress
play_msg_recording	"""%%"" en cours d’enregistrement.|Voulez-vous visionner cet enregistrement?"	"""%%"" is currently being recorded.|Do you want to start watching the recording?"
play_btn_current	À partir de maintenant	From current time
play_btn_resume	Reprendre vision.	Resume viewing
play_btn_begin	Voir depuis le début	From the beginning
last_x_recordings	%% derniers enregistrements	Last %% recordings
keep_all	Tout	Keep All
keep_all.short	Sauveg. tout	Keep All
All	Tout	All
every	Chaque	Every
Every...	Chaque...	Every...
14days	Durant 14 jours	For 14 days
7days	Durant 7 jours	For 7 days
until_i_erase	Jusqu'à suppression	Until I delete
until_i_erase.short	Jusqu’à suppr. 	Until I delete
last5	5 derniers enregistrements	Last 5 recordings
last3	3 derniers enregistrements	Last 3 recordings
last_x	%% derniers	Last %%
delete_in	Dans	Delete in
select_all	Sélectionner tout	Select All
unselect_all	Désélectionner tout	Unselect All
remove_repeat_title	Ne plus répéter	Remove repeat option
remove_repeat_msg	Voulez-vous vraiment désactiver la répétition?|Tous les enregistrements à venir seront annulés pour cette émission.	Are you sure you want to remove the repeat option?|All upcoming recordings will be cancelled for this program.
buffer_change_title	Période tampon :	Recording buffer :
buffer_change_msg	Cette période tampon sera appliquée au début et à la fin des enregistrements que vous programmerez à l'avenir.|Elle ne sera pas appliquée aux enregistrements déjà programmés.	This recording buffer will be added at the beginning and at the end of all recordings scheduled from this point on.|It will not be applied to recordings that are already scheduled.
many_programs_title	Trop d’occurences détectées	Too many occurences detected
many_programs_msg	Cette émission comporte un nombre trop élevé d'occurrences à enregistrer.|Veuillez choisir une fréquence d'enregistrement moins élevée.	This program has too many occurrences to record.|Please reduce recording frequency.
many_programs_btn0	Modifier la fréquence	Modify frequency
many_programs_btn1	Annuler l’enreg.	Cancel recording
inactive_desc	ne figure plus à la programmation.	is no longer on the schedule.
pressOK	Appuyez sur « OK » pour plus de détails.	"Press ""OK"" for more information."
MB	 Mo	 MB
GB	 Go	 GB
manual_block_popup_title	Titre bloqué manuellement	Manually blocked title
manual_block_popup_msg	Le NIP admin sera requis pour débloquer ce titre lors du prochain accès à la liste des émissions enregistrées.	The Admin PIN will be required to unblock this title the next time you will access to the List of Recorded Programs.
N/A	N/D	N/A
gw_conflict_popup_title	Conflit d’enregistrement	Recording conflict
gw_conflict_popup_desc0	La quantité d’enregistrements prévus pour le [DATE] dépasse la limite de votre Enregistreur.	The amount of recordings planned for [DATE] exceeds the limit for your Recorder.
gw_conflict_popup_desc1	Veuillez supprimer ou modifier au moins un enregistrement.	Please delete or reschedule at least one recording.
gw_conflict_popup_desc3	Il y a conflit pour %% enregistrements prévus pour le [DATE].	%% recordings are in conflict on [DATE].
gw_conflict_popup_desc4	Veuillez supprimer ou modifier plusieurs enregistrements.	Please remove or edit conflicting recordings.
gw_conflict_popup_title2	Limite d’enregistrements atteinte par votre Enregistreur	Your Recorder has reached maximum recording capacity
gw_conflict_popup_desc5	"Pour effectuer cette action, vous devez arrêter un des enregistrements en cours."	"To complete this action, you must stop one of your ongoing recordings."
gw_conflict_popup_desc6	Choisir l’enregistrement à arrêter :	Choose the recording to stop :
Keep	Conserver	Keep
will_be_deleted	Sera supprimé	Will be deleted
will_be_stopped_and_saved	Sera arrêté et sauvegardé	Will be stopped and saved
apply_changes	Appliquer les changements	Apply changes
conflict_resovled_many_schedules	Le conflit est résolu.|Les changements ont été appliqués afin de permettre l’enregistrement de ce qui a été programmé.	Changes applied.|System will record according to new changes.
conflict_not_resovled_schedules	L’action demandée n’a pu être effectuée.	The requested action could not be carried out.
stb_too_busy_title	Limite atteinte par votre Enregistreur	Recorder’s maximum capacity reached
stb_too_busy_btn	Choisir l’enregistrement à arrêter	Choose the recording you wish to stop
stb_too_busy_msg_channel	"Votre appareil a atteint la limite d’enregistrements simultanés.|Afin de pouvoir changer de chaîne, vous devez arrêter un des enregistrements en cours."	"Your device has reached its limit for simultaneous recordings.|To change the channel, please stop one of your recordings."
stb_too_busy_msg_vod	"Votre appareil a atteint la limite d’enregistrements et de visionnements simultanés.|Afin de pouvoir commander une vidéo sur demande, vous devez arrêter un des enregistrements en cours."	"Your device has reached its limit for simultaneous recording and viewing.|To order a video on demand, please stop one of your recordings."
conflict_resolved_tuner_msg_1	Le conflit est résolu.|L’enregistrement de %% a été arrêté.	Conflict resolved.|Recording of %% has been stopped.
conflict_resolved_tuner_msg_n	Le conflit est résolu.|Les enregistrements de %% ont été arrêtés.	Conflict resolved.|Recordings of %% have been stopped.
edit_too_many_title	Traitement de la demande	Processing your order
edit_too_many_msg	"Cette émission comporte un nombre élevé d’épisodes à enregistrer, ce qui pourrait ralentir momentanément le fonctionnement de votre Enregistreur.|Vous pouvez modifier la fréquence pour réduire le nombre d’enregistrements à venir. "	"There are a significant number of episodes to record for this show, which could slow down your Recorder momentarily.|You can change how often you record the show to lower the number of future recordings."
edit_too_many_btn1	Modifier la fréquence	Modify frequency
uhd_conflict_desc0	Vous pouvez regarder ou enregistrer un maximum de [UHD MAX] émissions en ultra-haute définition (UHD) simultanément.	You can watch or record a maximum of [UHD MAX] Ultra HD programs simultaneously.
uhd_conflict_desc1	Supprimez un enregistrement ou reprogrammez-le pour pouvoir continuer.	Delete or reschedule a recording in order to proceed.
format	Format	Format
hd	HD	HD
sd	SD	SD
resume_viewing	Reprendre le visionnement	Resume viewing
play_from_beginning	Reprendre depuis le début	Play from beginning
play_from_beginning2	Visionner depuis le début	Play from beginning
play_from_current_time	Visionner dès maintenant	Play from current time

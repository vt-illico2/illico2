recordingName	Nom de l’enregistrement	Recording Name
Channel	Chaîne	Channel
Date	Date	Date
Repeat	Répéter	Repeat
ManualRecording	Enregistrement manuel	Manual Recording
record	Enregistrer	Record
clear	Annuler	Cancel
None	Non	No
Yes	Oui	Yes
Every...	Chaque...	Every...
14days	Durant 14 jours	For 14 days
7days	Durant 7 jours	For 7 days
Last5	5 derniers enregistrements	Last 5 recordings
Last3	3 derniers enregistrements	Last 3 recordings
AllO	Tout	Keep All
Last5O	5 derniers enregistrements	Last 5 recordings
Last3O	3 derniers enregistrements	Last 3 recordings
AllS	Tout	Keep All
Last5S	5 derniers enregistrements	Last 5 recordings
Last3S	3 derniers enregistrements	Last 3 recordings
Done	OK	OK
Record more	Autre enregistrement	Record more
confMessage_R3	Un enregistrement manuel à la chaîne @ a été programmé.| |Une barre horizontale rouge indique les enregistrements manuels dans le guide horaire.	"You’ve scheduled a manual recording on channel @.| |In the program guide, manual recordings are marked with a horizontal red bar."
confMessage_R2	Un enregistrement manuel|à la chaine @ a été programmé.| |Prenez note que cet enregistrement ne sera pas visible dans le guide horaire.	A manual recording on channel @|has been scheduled.| |Please take note that this recording will not be shown in the Program Guide. 
confTitle	Enregistrement programmé	Scheduled Recording
Frequency	Fréquence	Frequency
cancel	Annuler	Cancel
All	Tout	All
Every	Chaque	Every
at	à	at
Any day at	N’importe quel jour à	Any day at
AllRecordings	Tous les enregistrements	All recordings
JAN	Janvier	January
FEB	Février	February
MAR	Mars	March
APR	Avril	April
MAY	Mai	May
JUNE	Juin	June
JULY	Juillet	July
AUG	Août	August
SEP	Septembre	September
OCT	Octobre	October
NOV	Novembre	November
DEC	Décembre	December

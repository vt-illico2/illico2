package com.alticast.illico.pvr;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.*;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;

import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.StorageInfoManager.CriticalListener;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.KeyboardListenerAction;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * MenuController
 *
 * @author June Park
 */
public class MenuController extends LayeredWindow implements LayeredKeyHandler, ClockListener, IssueChangedListener {

    boolean running = false;
    LayeredUI ui;

    FullScreenPanel[] scenes = new FullScreenPanel[7];
    FullScreenPanel current;

    private int resizeX = -1;
    private int resizeY = -1;
    private int resizeW = -1;
    private int resizeH = -1;

    public static String recorderID;

    public int currentSceneID = -1;
    public static String currentDate;

    private static MenuController instance = new MenuController();
    public Hashtable imageTable = new Hashtable();

    private Stack selectedMenu;

    static AbstractRecording targetRecording = null;
    static int targetStartPoint = VideoPlayer.PLAY_FROM_BEGINNING;

    static int lastScene = -1;

    public static MenuController getInstance() {
        return instance;
    }

    private MenuController() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);
        ui = WindowProperty.PVR_MENU.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
    }

    public void init() {
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.start()");
        imageTable = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        running = true;
        IssueManager.getInstance().addListener(this);
        selectedMenu = new Stack();
    }

    public synchronized void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.pause()");
        IssueManager.getInstance().removeListener(this);
        ui.deactivate();
        for (int a = 0; a < scenes.length; a++) {
            try {
                if (scenes[a] != null) {
                    scenes[a].stop();
                    if (scenes[a] instanceof PVRPortal) {
                        ((PVRPortal) scenes[a]).pvrPortalTree.setIndex(0);
                    }
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        if (selectedMenu != null) {
            selectedMenu.clear();
        }
        resizeX = -1;
        resizeY = -1;
        resizeW = -1;
        resizeH = -1;
        this.removeAll();
        currentSceneID = -1;
        current = null;
        running = false;
    }

    public void resizeScaledVideo(int x, int y, int w, int h) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.resizeScaledVideo("+x+", "+y+", "+w+", "+h+")");
        if (resizeX == x && resizeY == y && resizeW == w && resizeH == h) {
            return;
        } else {
            resizeX = x;
            resizeY = y;
            resizeW = w;
            resizeH = h;
        }
        try {
            EpgService epgSvc = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            VideoController vCnt = epgSvc.getVideoController();
            vCnt.resize(x, y, w, h, VideoController.SHOW);
            Log.printInfo("x=" + x + " y=" + y + " w=" + w + " h=" + h);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(ui);
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent("+e+")");
        boolean ret;
        if (current != null && current.isVisible()) {
            switch (code) {
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return !(current instanceof PVRPortal);
            }
            ret = current.handleKey(code);
        } else {
            ret = VideoPlayer.getInstance().handleKey(code);
        }
        if (ret) {
            return true;
        }
        switch (code) {
        case KeyCodes.LIST:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : LIST");
            switch (currentSceneID) {
            case PvrService.MENU_RECORDING_LIST:
                if (!Core.standAloneMode) {
                    if (Environment.SUPPORT_DVR) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call showMenu(MENU_SCHEDULED_RECORDINGS)");
                        showMenu(PvrService.MENU_SCHEDULED_RECORDINGS);
                    } else {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call exitToChannel()");
                        Core.getInstance().exitToChannel();
                    }
                }
                break;
            case PvrService.MENU_SCHEDULED_RECORDINGS:
                // Image clear 때문에 lock icon이 잠깐 남는 문제 보완.
                // ui.deactivate();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call exitToChannel() -- 2");
                Core.getInstance().exitToChannel();
                break;
            default:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call showMenu(MENU_RECORDING_LIST)");
                showMenu(PvrService.MENU_RECORDING_LIST);
                break;
            }
            return true;
        case OCRcEvent.VK_LIVE:
        case OCRcEvent.VK_EXIT:
            // Image clear 때문에 lock icon이 잠깐 남는 문제 보완.
//            if (App.R3_TARGET && "PVR".equals(Core.getInstance().getVideoContextName())) {
//                goToMenu(PvrService.MENU_PLAY_LAST_RECORDING);
//            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : EXIT or LIVE");
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call exitToChannel() -- 3");
                Core.getInstance().exitToChannel();
//            }
            return true;
        case KeyCodes.SEARCH:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : SEARCH");
            if (!Core.standAloneMode) {
                KeyboardListenerAction.getInstance().currentSceneCloseVirtualKeyboard();
                PreferenceProxy.getInstance().hidePinEnabler();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.handleKeyEvent() : Call startSearch(true)");
                startSearch(true);
            }
            return true;
//        case OCRcEvent.VK_PINP_UP:
//            if (Log.EXTRA_ON) {
//                long time = System.currentTimeMillis();
//                String path = "getRecordingsList";
//                byte[] body = "type=3&meta=1".getBytes();
//                byte[] res = null;
//                try {
//                    res = RemotePvrHandler.getInstance().request(path, body);
//                } catch (Exception ex){
//                    Log.print(ex);
//                }
//                time = System.currentTimeMillis() - time;
//                FrameworkMain.getInstance().printSimpleLog("Test getRecordingsList: " + time + " ms, "
//                                                           + (res != null ? res.length : -1) + " bytes");
//            }
//            return false;
        }
        return false;
    }

    public void startSearch(boolean flag) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.startSearch("+flag+")");
        Object ob = DataCenter.getInstance().get(SearchService.IXC_NAME);
        if (ob != null) {
            SearchService searchService = (SearchService) ob;
            try {
                searchService.launchSearchApplication(SearchService.PLATFORM_PVR, flag);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
    }

    //->Kenneth[2017.3.22] R7.3 : D-option 에 Search 가 추가됨에 따라 그에 따른 logging 을 위해서
    // param 을 던지는 함수 추가
    public void startSearch(boolean flag, String param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.startSearch("+flag+", "+param+")");
        Object ob = DataCenter.getInstance().get(SearchService.IXC_NAME);
        if (ob != null) {
            SearchService searchService = (SearchService) ob;
            try {
                searchService.launchSearchApplicationWithParam(SearchService.PLATFORM_PVR, flag, param);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        }
    }
    //<-

    public synchronized void showMainMenu() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.showMainMenu()");
        goToMenu(PvrService.MENU_PVR_PORTAL);
    }

    public synchronized void showPreviousMenu() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.showPreviousMenu()");
        int menuId;
        try {
            menuId = ((Integer) selectedMenu.pop()).intValue();
        } catch (Exception ex) {
            menuId = PvrService.MENU_PVR_PORTAL;
        }
        goToMenu(menuId);
    }

    public synchronized void showMenu(int id) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.showMenu("+id+")");
        if (currentSceneID >= 0) {
            selectedMenu.push(new Integer(currentSceneID));
        }
        goToMenu(id);
    }

    public synchronized void playLastRecording() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.playLastRecording()");
        playRecording(null, VideoPlayer.PLAY_RESUME);
    }

    public synchronized void playRecording(AbstractRecording rec, int startPoint) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.playRecording("+rec+", "+startPoint+")");

        //->Kenneth[2015.3.1] 4K : Non 4K STB 에서 원격의 UHD play 를 막는다
        //->Kenneth[2015.6.24] DDC-107
        if (Core.getInstance().needToShowIncompatibilityPopup(rec, true)) {
        //if (Core.getInstance().needToShowIncompatibilityPopup(rec)) {
            Log.printDebug(App.LOG_HEADER+"MenuController.playRecording() : Call Core.showIncompatibilityErrorMessage()");
            Core.getInstance().showIncompatibilityErrorMessage();
            return;
        }

        targetRecording = rec;
        targetStartPoint = startPoint;
        showMenu(PvrService.MENU_PLAY_RECORDING);
    }

    public synchronized void goToMenu(int id) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"START : MenuController.goToMenu("+id+") : currentSceneID = "+currentSceneID);
        boolean play;
        if (id == PvrService.MENU_PLAY_RECORDING) {
            //->Kenneth[2015.8.28] : VDTRMASTER-5528
            // 도대체 왜 여기서 list 를 보여주도록 id 를 조작하는지 모르겠네..
            String videoContext = Core.getInstance().getVideoContextName();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.goToMenu() : videoContext = "+videoContext);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.goToMenu() : current = "+current);
            if (videoContext != null && "PVR".equals(videoContext)) {
                if (current != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.goToMenu() : PVR playing so call current.stop(), null and return");
                    current.stop();
                    current.setVisible(false);
                    current = null;
                    return;
                }
            }
            //<-
            id = PvrService.MENU_RECORDING_LIST;
            play = true;
        } else {
            play = false;
        }

        if (App.R3_TARGET
                && !Environment.SUPPORT_DVR
                && id == PvrService.MENU_PVR_PORTAL
                && "PVR".equals(Core.getInstance().getVideoContextName())) {
            // 재생중 non-PVR에서 다시 PVR Main으로 진입하고자 하는 경우
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.goToMenu() : Call VideoPlayer.updateMediaTime()"); 
            VideoPlayer.getInstance().updateMediaTime();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.goToMenu() : Call VideoPlayer.exitToList()"); 
            VideoPlayer.getInstance().exitToList();
            return;
        }


        // VDTRMASTER-768 재연안되어서 체크 안해도 될 듯.
        // if (Core.standAloneMode && id != PvrService.MENU_RECORDING_LIST) {
        // Log.printWarning("MenuController: do not show menu in stand alone mode.");
        // return;
        // }
        final int menuId = id;
        if (current != null && menuId != currentSceneID) {
            current.stop();
        }
        switch (menuId) {
        case PvrService.MENU_CREATE_RECORDING:
            scenes[menuId] = new ManualRecordingPanel();
//            if (scenes[menuId] == null) {
//                scenes[menuId] = new ManualRecording();
//            }
            break;
        // case PvrSesrvice.MENU_CREATE_RECORDING:
        case PvrService.MENU_SCHEDULED_RECORDINGS:
            scenes[menuId] = UpcomingList.getInstance();
            break;
        case PvrService.MENU_RECORDING_LIST:
            scenes[menuId] = RecordedList.getInstance();
            break;
        case PvrService.MENU_PREFERENCE:
            if (scenes[menuId] == null) {
                scenes[menuId] = new PVRPortalPreference();
                // scenes[menuId].prepare();
                // ((PVRPortal) scenes[menuId]).start();
            }
            break;
        case PvrService.MENU_PVR_PORTAL:
            if (scenes[menuId] == null) {
                scenes[menuId] = new PVRPortal();
                // scenes[menuId].prepare();
                // ((PVRPortal) scenes[menuId]).start();
            }
            selectedMenu.clear();
            break;
        case PvrService.MENU_PVR_MANAGEMENT:
            if (scenes[menuId] == null) {
                scenes[menuId] = new PVRManagement();
                // scenes[menuId].prepare();
                // ((PVRManagement) scenes[menuId]).start();
            }
            break;
        case PvrService.MENU_WIZARD:
            scenes[menuId] = new ManagementWizard();
            break;
        default:
            Core.getInstance().showErrorMessage("MOT504", null);
            Thread t = new Thread("PVR.exit") {
                public void run() {
                    Core.getInstance().exitToChannel();
                }
            };
            t.start();
            return;
        }

        if (running) {
            scenes[menuId].start();
            current = scenes[menuId];

            scenes[menuId].setVisible(!play);
            if (currentSceneID != menuId) {
                this.removeAll();
                this.add(scenes[menuId]);

                if (currentSceneID == PvrService.MENU_PVR_PORTAL && !play) {
                    // Main에서 다른 곳으로 가고자 하는 경우
                    boolean stopped = false;
                    if (App.R3_TARGET && "PVR".equals(Core.getInstance().getVideoContextName())) {
                        VideoPlayer.getInstance().updateMediaTime();
                        stopped = VideoPlayer.getInstance().stopPlaying(true);
                    }
                    if (!stopped) {
                        try {
                            Core.epgService.stopChannel();
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                        if (App.R3_TARGET) {
                            try {
                                Core.monitorService.releaseVideoContext();
                            } catch (Exception ex) {
                                Log.print(ex);
                            }
                        }
                    }
                }
            }
            if (play) {
                AbstractRecording rec = targetRecording;
                ListPanel panel = (ListPanel) scenes[menuId];
                panel.stopOptions();
                if (rec == null) {
                    boolean result = VideoPlayer.getInstance().playLastRecording();
                    panel.focusChanged();
                    if (!result) {
                        panel.setVisible(true);
                    }
                } else {
                    panel.focusTo(rec);
                    panel.focusChanged();
                    int blockLevel = ParentalControl.getLevel(rec);
                    boolean result = blockLevel == ParentalControl.OK;
                    if (result) {
                        VideoPlayer.getInstance().playRecording(rec, targetStartPoint);
                    } else {
                        panel.setVisible(true);
                        // 2013-11-12 다시 보여주기로 변경
                        panel.processUnblockAndPlay(rec, blockLevel);
                    }
                }
            } else {
                VideoPlayer.getInstance().lastRecording = null;
            }
            ui.activate();
            currentSceneID = menuId;
            lastScene = menuId;
        } else {
            Log.printWarning("MenuController: already paused !");
        }
        targetRecording = null;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"END : MenuController.goToMenu("+id+") : currentSceneID = "+currentSceneID);
    }

    public void clockUpdated(Date date) {
        currentDate = Formatter.getCurrent().getLongDate();
        if (current != null) {
            current.repaint();
        }
    }

    public FullScreenPanel getCurrentScene() {
        return current;
    }

    public synchronized void issueChanged(Issue old, Issue issue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.issueChanged("+old+", "+issue+")");
        if (current == null) {
            return;
        }
        if (current instanceof PVRPortal) {
            ((PVRPortal) current).issueUpdated(issue);
        }
    }

    public synchronized void issueResolved() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"MenuController.issueResolved()");
        if (current == null) {
            return;
        }
        if (current instanceof ManualRecordingPanel) {
            ((ManualRecordingPanel) current).issueResolved();
        }
    }

}

package com.alticast.illico.pvr;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;

/**
 * This class is the initial class of PVR Xlet.
 *
 * @version $Revision: 1.276.2.5.2.4 $ $Date: 2017/10/02 12:49:56 $
 * @author  June Park
 */
public class App implements Xlet, ApplicationConfig {
    public static final String LOG_HEADER = "T_PVR : ";

    public static final boolean R3_TARGET = true;
    public static final boolean SUPPORT_HN = true;
    public static final boolean SHOW_MANUAL_RECORINGS_IN_GUIDE = R3_TARGET;
    public static final boolean MANY_TUNER = Environment.TUNER_COUNT > 2;

    public static final String NAME = "PVR";
    // Kenneth[2014.11.10] 버전 수정
    //public static final String VERSION = "1.5.15.2";
    // Kenneth[2015.1.6] 4K 용으로 버전 수정
    //public static final String VERSION = "4K.1.x_test8";//VDTRMASTER-5351
    //public static final String VERSION = "R5.1.0_beta_01"; // 6.1 초기 릴리즈 
    //public static final String VERSION = "R5.1.0_beta_02"; // [2015.6.3] DDC-007 305H : Rafik 메일에 의거 kbps -> bps 할 때 1024 아닌 1000 사용(VDTRMATER-5441) 
    //public static final String VERSION = "R5.1.0_beta_03"; // [2015.6.5] GUI 반영
    //public static final String VERSION = "R5.1.0_beta_04"; // [2015.6.6] 주환 이슈 반영
    //public static final String VERSION = "R5.1.0_beta_05"; // [2015.6.9] J-F 요청에 따라 UHD bitrate 을 35000 으로
    //public static final String VERSION = "R5.1.0_beta_06"; // [2015.6.12] VBM 수정
    //public static final String VERSION = "R5.1.0_beta_10"; // [2015.6.15] 주환 이슈 수정
    //public static final String VERSION = "R5.1.2"; // [2015.6.17] R5 첫 공식 릴리즈:VDTRMASTER-5441, 5463
    //public static final String VERSION = "R5.2.x_01"; // [2015.6.18] VDTRMASTER-5479
    //public static final String VERSION = "R5.2.x_02"; // [2015.6.24] DDC-107
    //public static final String VERSION = "R5.2.x_03"; // [2015.6.25] DDC-107(Companion 관련), VDTRMASTER-5478
    //public static final String VERSION = "R5.2.x_04"; // [2015.6.29] Tank, pvr 링크
    //public static final String VERSION = "R5.2.x_05"; // [2015.7.4] Tank : flipbar 안보이게
    //public static final String VERSION = "R5.2.x_06"; // [2015.7.8] VDTRMASTER-5474 SD/HD channel 정보로 판단
    //public static final String VERSION = "R5.2.x_07"; // [2015.7.15] VDTRMASTER-5487 SDV 인경우 scheduleRecording 에서 delete 하는 부분 늦춤
    //public static final String VERSION = "R5.2.x_08"; // [2015.7.17] Tank : PopupController 에서 menu 키 consume
    //public static final String VERSION = "R5.2.x_09"; // [2015.7.20] Tank : PopupController 에서 menu 키 consume(PopupController 에서만 처리하도록)
    //public static final String VERSION = "R5.2.4"; // [2015.7.20] VT 공식 Release
    //public static final String VERSION = "R5.3.x_01"; // [2015.7.23] D option BG 바뀜(flat design)
    //public static final String VERSION = "R5.3.x_03"; // [2015.7.28] D option BG + HDCP2.2
    //public static final String VERSION = "R5.3.x_04"; // [2015.7.29] RemotePlay 의 경우도 HDCP2.2 처리.(PvrServiceImpl)
    //public static final String VERSION = "R5.3.x_05"; // [2015.7.30] PVR play 중 아니면 hot plugged event 처리 안함
    //public static final String VERSION = "R5.3.0"; // [2015.7.31] VT 공식 Release
    //public static final String VERSION = "R5.4.x_01"; // [2015.8.24] DDC-113 *key blocking in popup, VDTRMASTER-5592
    //public static final String VERSION = "R5.4.1"; // [2015.8.28] VT 공식 릴리즈
    //public static final String VERSION = "R5.5.x_01"; // [2015.8.28] VDTRMASTER-5528
    //public static final String VERSION = "R5.5.x_02"; // [2015.9.2] VDTRMASTER-5588
    //public static final String VERSION = "R5.5.2"; // [2015.9.10] VT 공식 릴리즈
    //public static final String VERSION = "R5.6.x_01"; // [2015.10.1] VDTRMASTER-5658(lost recording)
    //public static final String VERSION = "R5.6.x_TEST_IN_HOUR"; // [2015.10.2] 테스트 릴리즈
    //public static final String VERSION = "R5.6.1"; // [2015.10.7] VT 공식 릴리즈
    //public static final String VERSION = "R5.6.1_TEST_IN_HOUR"; // [2015.10.13] Tuanh 요청으로 릴리즈 함.
    //public static final String VERSION = "R5.6.1"; // [2015.10.13] 테스트 릴리즈 후 원복
    //public static final String VERSION = "R5.7.x_01"; // [2015.10.14] checkLogOnOff 추가
    //public static final String VERSION = "R5.7.x_02"; // [2015.10.15] DDC-114
    //public static final String VERSION = "R5.7.0"; // [2015.10.18] VT 공식 릴리즈
    //public static final String VERSION = "R5.8.x_01"; // [2015.10.23] VDTRMASTER-5702
    //public static final String VERSION = "R5.8.x_02"; // [2016.1.18] VDTRMASTER-5753
    //public static final String VERSION = "R5.8.2"; // [2016.2.25] VT 공식 릴리즈
    //public static final String VERSION = "R7.1.x_01"; // [2016.3.8] R7 개발/테스트
    //public static final String VERSION = "R7.1.0"; // [2016.3.31] R7 VT 공식 릴리즈
    //public static final String VERSION = "R7.2.x"; // [2016.6.1] VDTRMASTER-5824 improvement(standalone 시 UHD 플레이시키기), LOG_CAPTURE_APP_NAME 기능추가
    //public static final String VERSION = "(R7.2+CYO).1.1";//2016.9.16 : R7.2/CYO Bundling 공식 첫 릴리즈 (VDTRMASTER-5824)
    //public static final String VERSION = "(R7.2+CYO).2.x";//2016.9.22 : VDTRMASTER-5865(fail.txt TOC만 수정)
    //public static final String VERSION = "(R7.2+CYO).2.1";//2016.10.4 : 2번째 릴리즈
    //public static final String VERSION = "(R7.3).1.x";//2017.1.25 : VDTRMASTER-6029 테스트 -> illicoWeb 의 이슈였음
    //public static final String VERSION = "(R7.3).1.x_01";//2017.3.16 : R7.3 구현
    //public static final String VERSION = "(R7.3).1.x_02";//2017.3.22 : D-option 에 search 추가
    //public static final String VERSION = "(R7.3).1.x_03";//2017.3.22 : TOC 수정
    //public static final String VERSION = "(R7.3).1.x_04";//2017.3.29 : VDTRMASTER-5703 힌트를 찾기 위해 FDR 로그 찍음 + GUI 수정
    //public static final String VERSION = "(R7.3).1.0";//2017.3.30 : R7.3 1st 릴리즈
    //public static final String VERSION = "(R7.3).2.x";//2017.4.10 : VDTRMASTER-6094
    //public static final String VERSION = "(R7.3).2.1";//2017.4.11 : 2번째 공식 릴리즈
    //public static final String VERSION = "(R7.3).3.x";//2017.4.18 : VDTRMASTER-6109
    //public static final String VERSION = "(R7.3).3.x";//2017.4.19 : VDTRMASTER-6110
    //public static final String VERSION = "(R7.3).3.2";//2017.4.21 : 3번째 릴리즈
    //public static final String VERSION = "(R7.4).1.x_01";//2017.8.23 : AppData 에 EnglishTitle 추가
    //public static final String VERSION = "(R7.4).1.x_02";//2017.8.24 : ListPanel.start() 에서 sortingOption 다시 적용
    //public static final String VERSION = "(R7.4).1.x_03";//2017.9.1 : removeAllUpcomingRecordings 추가
    //public static final String VERSION = "(R7.4).1.x_04";//2017.9.5 : PVR701 인 경우 PVR715 로 바꿔서 보냄
    //public static final String VERSION = "(R7.4).1.3";//2017.9.7 : 첫번째 R7.4 릴리즈 (6054, 6128, 6147)
    //public static final String VERSION = "(R7.4).2.x";//2017.9.28 : VDTRMASTER-6214
    //public static final String VERSION = "(R7.4).2.1";//2017.10.2 : 2번째 릴리즈 : VDTRMASTER-6214
    public static final String VERSION = "(R7.5).0.5";

    /** XletContext. */
    protected XletContext xletContext;

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     *
     * @param ctx The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public synchronized void initXlet(XletContext ctx) throws XletStateChangeException {
        if (!SUPPORT_HN && !Environment.SUPPORT_DVR) {
            System.out.println("<PVR> skip to launch PVR app.");
            return;
        }
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
    }

    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("Call App.initXlet()");
        }
        ConfigManager.getInstance();   // listener 를 빨리 걸기 위해
        Core.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     *
     * @throws XletStateChangeException is thrown if the Xlet cannot start
     *                                  providing service.
     */
    public synchronized void startXlet() throws XletStateChangeException {
        if (Log.DEBUG_ON) {
            Log.printDebug("Call App.startXlet()");
        }
        if (!SUPPORT_HN && !Environment.SUPPORT_DVR) {
            return;
        }
        FrameworkMain.getInstance().start();
        try {
            Core.getInstance().start();
        } catch (Throwable t) {
            Log.print(t);
        }
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public synchronized void pauseXlet() {
        if (!SUPPORT_HN && !Environment.SUPPORT_DVR) {
            return;
        }
        FrameworkMain.getInstance().pause();
        try {
            Core.getInstance().pause();
        } catch (Throwable t) {
            Log.print(t);
        }
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     *
     * @param unconditional If unconditional is true when this method is called,
     *    requests by the Xlet to not enter the destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue
     *    to execute (Not enter the Destroyed state). This exception is ignored
     *    if unconditional is equal to true.
     */
    public synchronized void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (!SUPPORT_HN && !Environment.SUPPORT_DVR) {
            return;
        }
        FrameworkMain.getInstance().destroy();
    }

    ////////////////////////////////////////////////////////////////////////////
    // ApplicationConfig implementation
    ////////////////////////////////////////////////////////////////////////////

    /**
     * Returns the version of Application.
     *
     * @return version string.
     */
    public String getVersion() {
        return VERSION;
    }

    /**
     * Returns the name of Application.
     *
     * @return application name.
     */
    public String getApplicationName() {
        return NAME;
    }

    /**
     * Returns the Application's XletContext.
     *
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

}

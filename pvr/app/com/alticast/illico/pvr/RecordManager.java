package com.alticast.illico.pvr;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Collections;

import javax.tv.locator.InvalidLocatorException;
import javax.tv.locator.Locator;
import javax.tv.locator.LocatorFactory;
import javax.media.Time;
import javax.tv.service.Service;

import org.ocap.dvr.*;
import org.ocap.net.*;
import org.ocap.resource.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.RecordingList;
import org.ocap.storage.ExtendedFileAccessPermissions;
import org.ocap.hn.content.IOStatus;

import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.data.AppDataEntries;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;

/**
 * RecordManager
 *
 * @author  June Park
 */
public class RecordManager implements RecordingChangedListener, RecordingAlertListener, DataUpdateListener {
    /** 40 years. 시간 못받아 1970년으로 부팅해도 여유가 좀 있음. */
    public static final long MAX_PERIOD = Constants.MS_PER_DAY * 365 * 40;
    /** 7 days. */
    public static final long FAILED_RECORDING_KEEP_DURATION = 7 * Constants.MS_PER_DAY;

    private static final String ORGANIZATION = "Videotron";
    /** Reason */
    public static final int CANCELLED = 14;

    private static final boolean STOP_WHEN_DISK_FULL = false;
    private static final boolean TRANSFORM_LOCATOR = false;

    private long sdvDefaultFreq = 0;

    private static Locator DUMMY_SDV_LOCATOR;
    static {
        try {
            DUMMY_SDV_LOCATOR = new OcapLocator(0);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private static final String[] FAILED_REASONS = {
        "OK",
        "CA_REFUSAL",
        "CONTENT_NOT_FOUND",
        "TUNING_FAILURE",
        "INSUFFICIENT_RESOURCES",
        "ACCESS_WITHDRAWN",
        "RESOURCES_REMOVED",
        "SERVICE_VANISHED",
        "TUNED_AWAY",
        "USER_STOP",
        "SPACE_FULL",
        "OUT_OF_BANDWIDTH",
        "RESOLUTION_ERROR",
        "POWER_INTERRUPTION",
        "CANCELLED",
    };

    private int sdvDefaultFrequency = 0;
    private long recordingBuffer = 0;

    private OcapRecordingManager manager;

    private static Worker deleteWorker = new Worker("RecordManager.delete_worker", true);

    private static RecordManager instance = new RecordManager();

//    /** 녹화 종료 후 즉시 녹화할 program list */
//    private ArrayList nextPrograms = new ArrayList();

    public static RecordManager getInstance() {
        return instance;
    }

    private RecordManager() {
        RecordingManager rm = RecordingManager.getInstance();
        if (rm instanceof OcapRecordingManager) {
            manager = (OcapRecordingManager) rm;
        } else {
            if (Log.ERROR_ON) {
                Log.printError(rm + " is not OcapRecordingManager !");
            }
            return;
        }
        manager.addRecordingChangedListener(this);
        manager.addRecordingAlertListener(this, 20 * Constants.MS_PER_SECOND);

        DataCenter.getInstance().addDataUpdateListener(PreferenceNames.RECORD_BUFFER, this);
    }

    public void init() {
    }

    public void dispose() {
        manager.removeRecordingChangedListener(this);
//        stopAllRecordings(RecordingListManager.getInstance().getInProgressList());
    }

    public int getSdvDefaultFrequency() {
        if (sdvDefaultFrequency == 0) {
            try {
                int freq = ((Integer) SharedMemory.getInstance().get(SharedDataKeys.EPG_MC_FREQ)).intValue();
                sdvDefaultFrequency = freq;
            } catch (Exception ex) {
            }
        }
        return sdvDefaultFrequency;
    }

    public long getRecordingBuffer() {
        return recordingBuffer;
    }

    private static OcapRecordingProperties createRecordingProperties(long saveUntil) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.createRecordingProperties("+saveUntil+")");
        if (saveUntil >= MAX_PERIOD) {
            saveUntil = MAX_PERIOD;
        }
        return new OcapRecordingProperties(
                OcapRecordingProperties.HIGH_BIT_RATE,
                saveUntil / Constants.MS_PER_SECOND,
                OcapRecordingProperties.DELETE_AT_EXPIRATION,
                OcapRecordingProperties.RECORD_WITH_CONFLICTS,
                new ExtendedFileAccessPermissions(true, true,
                        true, true,
                        true, true,
                        null, null),
                ORGANIZATION,
                null);
    }

    public static long getExpirationPeriod(RecordingRequest r) {
        try {
            RecordingSpec rs = r.getRecordingSpec();
            OcapRecordingProperties rp = (OcapRecordingProperties) rs.getProperties();
            long exp = rp.getExpirationPeriod() * Constants.MS_PER_SECOND;
            if (exp >= MAX_PERIOD) {
                return Long.MAX_VALUE;
            } else {
                return exp;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return Long.MAX_VALUE;
    }

    public static long getExpirationTime(RecordingRequest r) {
        long exp = getExpirationPeriod(r);
        if (exp >= MAX_PERIOD) {
            return Long.MAX_VALUE;
        } else {
            return AppData.getScheduledStartTime(r) + exp;
        }
    }

    public static void setExpirationPeriod(RecordingRequest r, long duration) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.setExpirationPeriod("+r+", "+duration+")");
        if (duration == Long.MAX_VALUE) {
            duration = MAX_PERIOD;
        }
        try {
            RecordingSpec rs = r.getRecordingSpec();
            OcapRecordingProperties rp = (OcapRecordingProperties) rs.getProperties();
            OcapRecordingProperties np = new OcapRecordingProperties(
                rp.getBitRate(),
                duration / Constants.MS_PER_SECOND,
                rp.getRetentionPriority(),
                rp.getPriorityFlag(),
                rp.getAccessPermissions(),
                rp.getOrganization(),
                rp.getDestination()
            );
            r.setRecordingProperties(np);
            RemotePvrHandler.updateVersion(r);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static void setExpirationTime(RecordingRequest r, long time) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.setExpirationTime("+r+", "+new Date(time)+")");
        if (time == Long.MAX_VALUE) {
            setExpirationPeriod(r, Long.MAX_VALUE);
            return;
        }
        long newDur = time - AppData.getScheduledStartTime(r);
        if (newDur > 0) {
            setExpirationPeriod(r, newDur);
        }
    }

    public void setSkipped(RecordingRequest r, boolean skip) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.setSkipped("+r+", "+skip+")");
        int oldState = LeafRecordingRequest.PENDING_NO_CONFLICT_STATE;
        try {
            oldState = r.getState();
        } catch (Exception ex) {
            Log.print(ex);
        }
        AppData.set(r, AppData.UNSET, skip ? Boolean.TRUE : Boolean.FALSE);
        if (Environment.TUNER_COUNT <= 2) {
            // gateway에서는 다시 재구성하지 않고 최근에 조작한 recording을 최하위 순으로 만든다.
            resetPrioritized(r);
        }
        if (skip) {
            IssueManager.getInstance().updateIssue();
        } else {
            try {
                if (oldState == LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE
                        && r.getState() == LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                    IssueManager.getInstance().processNewConfilct(r);
                    IssueManager.getInstance().updateIssue();
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void resetPrioritized(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.resetPrioritized("+r+")");
        if (r == null) {
            return;
        }
        ResourceUsage[] usages = manager.getPrioritizedResourceUsages(r);
        if (usages == null || usages.length <= 1) {
            return;
        }
        ArrayList list = new ArrayList();
        for (int i = 0; i < usages.length; i++) {
            Log.printDebug("RecordManager.setPrioritized: [" + i + "] = " + usages[i]);
            list.add(new TunerUsage(usages[i]));
        }
        Collections.sort(list);

        ResourceUsage[] ret = new ResourceUsage[list.size()];
        for (int i = 0; i < ret.length; i++) {
            TunerUsage tu = (TunerUsage) list.get(i);
            ret[i] = tu.source;
        }
        manager.setPrioritization(ret);
    }

    /** 주어진 채널이 녹화중이면 RecordingRequest를 return */
    public RecordingRequest getRecordingRequest(int id) {
        return manager.getRecordingRequest(id);
    }

    /** 주어진 채널이 녹화중이면 RecordingRequest를 return */
    public LeafRecordingRequest getRequest(TvChannel ch) {
        if (ch == null) {
            return null;
        }
        try {
            return getRequest(ch.getSourceId());
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    /** 주어진 채널이 녹화중이면 RecordingRequest를 return */
    public LeafRecordingRequest getRequest(int sourceId) {
        RecordingListManager lm = RecordingListManager.getInstance();
        RecordingList list = RecordingListManager.filter(lm.getInProgressWithoutErrorList(), AppData.SOURCE_ID, new Integer(sourceId));
        if (list == null || list.size() < 1) {
            if (Log.DEBUG_ON) {
                Log.printDebug("getRecordingRequest.no list for sid = " + sourceId);
            }
            return null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("getRecordingRequest.size = " + list.size() + " for sid = " + sourceId);
        }
        return (LeafRecordingRequest) list.getRecordingRequest(0);
    }

    /** 프로그램에 해당하는 RecordingRequest를 return */
    public OcapRecordingRequest getRequest(TvProgram p) {
        RecordingListManager lm = RecordingListManager.getInstance();
        try {
            RecordingList list = RecordingListManager.filter(lm.getCancelableList(), AppData.CHANNEL_NAME, p.getCallLetter());
            list = RecordingListManager.filter(list, AppData.PROGRAM_START_TIME, new Long(p.getStartTime()));
            if (list == null || list.size() < 1) {
                return null;
            }
            return (OcapRecordingRequest) list.getRecordingRequest(0);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }


    public TvProgram getProgram(RecordingRequest req) {
        Object pid = AppData.get(req, AppData.PROGRAM_ID);
        if (pid == null) {
            Log.printInfo("RecordManager.getProgram: manual recording.");
            return null;
        }
        String call = AppData.getString(req, AppData.CHANNEL_NAME);
        long start = AppData.getLong(req, AppData.PROGRAM_START_TIME);

        EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (epgService == null) {
            return null;
        }
        try {
            TvProgram p = epgService.getProgram(call, start);
            if (p == null) {
                Log.printWarning("RecordManager.getProgram: not found.");
                return null;
            }
            String pid2 = p.getId();
            if (pid.equals(pid2)) {
                return p;
            }
            Log.printWarning("RecordManager.getProgram: program id is not match. " + pid + ", " + pid2);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    //->Kenneth[2015.10.28] 안쓰는 놈이라 명시적으로 지워 버림. replace 를 쓸 것.
    /* 
    public RecordingRequest _reschedule(RecordingRequest r, long start, long dur) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"RecordManager._reschedule: from = " + new Date(start) + ", dur = " + dur);
        }
        RecordingSpec spec = r.getRecordingSpec();
        if (spec instanceof LocatorRecordingSpec) {
            LocatorRecordingSpec lrs = (LocatorRecordingSpec) spec;
            try {
                LocatorRecordingSpec newSpec = new LocatorRecordingSpec(lrs.getSource(),
                    new Date(start), dur, lrs.getProperties());
                r.reschedule(newSpec);
                try {
                    TvProgram high = Core.epgService.getHighestRatingProgram(
                                AppData.getString(r, AppData.CHANNEL_NAME), start, start + dur);
                    if (high != null) {
                        AppData.set(r, AppData.PARENTAL_RATING, high.getRating());
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
                UpcomingList.getInstance().updateRecording(r);
                RecordedList.getInstance().updateRecording(r);
                RemotePvrHandler.updateScheduledVersion();
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("RecordManager._reschedule: not LocatorRecordingSpec = " + spec);
        }
        return r;
    }
    */

    public RecordingRequest replace(RecordingRequest r, long start, long dur) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"RecordManager.replace() : from = " + new Date(start) + ", dur = " + new Date(start+dur));
        }
        RecordingSpec spec = r.getRecordingSpec();
        if (spec instanceof LocatorRecordingSpec) {
            LocatorRecordingSpec lrs = (LocatorRecordingSpec) spec;
            try {
                LocatorRecordingSpec newSpec = new LocatorRecordingSpec(lrs.getSource(),
                    new Date(start), dur, lrs.getProperties());

                String[] keys = r.getKeys();
                Serializable[] values;
                if (keys == null) {
                    keys = new String[0];
                    values = new Serializable[0];
                } else {
                    values = new Serializable[keys.length];
                    for (int i = 0; i < keys.length; i++) {
                        values[i] = r.getAppData(keys[i]);
                    }
                }
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.replace() : Call oldRecording.delete()");
                    r.delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.replace() : Call record(newSpec)");
                RecordingRequest newRecording = manager.record(newSpec, keys, values);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.replace() : newRecording created : "+newRecording);
                AppData.set(newRecording, AppData.CREATION_TIME, new Long(System.currentTimeMillis()));
                AppData.set(newRecording, AppData.YEAR_SHIFT, new Integer(Core.getInstance().getYearShift()));

                try {
                    TvProgram high = Core.epgService.getHighestRatingProgram(
                                AppData.getString(r, AppData.CHANNEL_NAME), start, start + dur);
                    if (high != null) {
                        AppData.set(newRecording, AppData.PARENTAL_RATING, high.getRating());
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.replace() : Call RemotePvrHandler.updateScheduledVersion()");
                RemotePvrHandler.updateScheduledVersion();
                return newRecording;
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("RecordManager.replace() : not LocatorRecordingSpec = " + spec);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.replace() : returns null");
        return null;
    }

    /** 지우고 새로 만든다. */
    public boolean replace(RecordingRequest r, TvProgram p) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"RecordManager.replace: new program = " + p);
        }
        try {
            TvChannel ch = p.getChannel();
            AppData old = AppData.create(r);
            old.update(ch, p);

            long startTime = p.getStartTime() - recordingBuffer;
            long endTime = p.getEndTime() + recordingBuffer;

            record(ch, startTime, endTime - startTime, old);

            r.delete();
            return true;
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
    }

    public void saveBuffer(int tunerIndex, TvChannel ch, long from, long to) {
        RecordingOption option = new RecordingOption(ch, from, to);
        option.creationMethod = AppData.BY_USER;
        try {
            option.title = DataCenter.getInstance().getString("TSB_SAVE_PREFIX") + ch.getCallLetter();
        } catch (Exception ex) {
            Log.print(ex);
            option.title = DataCenter.getInstance().getString("TSB_SAVE_PREFIX");
        }
        saveBufferImpl(tunerIndex, from, to - from, option);
    }

    public void saveBuffer(int tunerIndex, TvChannel ch, TvProgram p) {
        try {
            long start = p.getStartTime();
            long dur = p.getEndTime() - start;
            RecordingOption option = new RecordingOption(ch, p, 0L);
            option.creationMethod = AppData.BY_USER;
            saveBufferImpl(tunerIndex, start, dur, option);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /** 버퍼에 있는걸 copy 만 한다. */
    private void saveBufferImpl(int tunerIndex, long start, long dur, RecordingOption option) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.saveBufferImpl("+tunerIndex+", "+new Date(start)+", "+dur+")");
        try {
            option.type = RecordingOption.TYPE_SINGLE;
            option.all = false;
            AppData appData = AppData.create(option);

            String[] keys;
            Serializable[] values;
            if (appData != null) {
                AppDataEntries entries = appData.getEntries();
                keys = entries.getKeys();
                values = entries.getValues();
            } else {
                // TODO - fix
                keys = new String[0];
                values = new Serializable[0];
            }

            OcapRecordingRequest request = null;
            try {
                ServiceContextRecordingSpec spec = new ServiceContextRecordingSpec(
                    Environment.getServiceContext(tunerIndex),
                    new Date(start), dur, createRecordingProperties(appData.saveUntil));
                request = (OcapRecordingRequest) manager.record(spec, keys, values);
                if (Log.DEBUG_ON) {
                    Log.printDebug(request + " " + request.getState());
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /** RecordingScheduler에 의해 다른 episode 녹화를 요청할 때. */
    public RecordingRequest seriesRecord(TvProgram p, long startOffset, long endOffset, String groupKey, int type, boolean suppressConflict, int creationMethod) {
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord()");
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : TvProgram = "+p);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : startOffset = "+startOffset);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : endOffset = "+endOffset);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : groupKey = "+groupKey);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : type = "+type);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : suppressConflict = "+suppressConflict);
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : creationMethod = "+creationMethod);
        RecordingRequest req = getRequest(p);
        if (req != null) {
            Log.printWarning("RecordManager.seriesRecord: already scheduled = " + p);
            return null;
        }
        TvChannel ch = null;
        try {
            ch = p.getChannel();
        } catch (Exception ex) {
        }
        if (ch == null) {
            Log.printWarning("RecordManager.seriesRecord: channel not found = " + p);
            return null;
        }
        RecordingOption option = new RecordingOption(ch, p, startOffset, endOffset);
        option.creationMethod = creationMethod;
        option.groupKey = groupKey;
        option.type = type;
        option.all = true;
        option.suppressConflict = suppressConflict;
        if (Log.DEBUG_ON)Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord() : Call record(option, false)");
        return record(option, false);
    }

    /** RecordingScheduler에 의해 다른 Manual repeat 녹화를 요청할 때. */
    public RecordingRequest seriesRecord(String callLetter, long from, long to, String title, String groupKey, int creationMethod) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.seriesRecord("+callLetter+", "+title+", "+groupKey+")");
        EpgService es = Core.epgService;
        if (es == null) {
            return null;
        }
        TvChannel ch = null;
        try {
            ch = es.getChannel(callLetter);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (ch == null) {
            return null;
        }
        RecordingOption option = new RecordingOption(ch, from, to);
        option.creationMethod = creationMethod;
        option.groupKey = groupKey;
        option.type = RecordingOption.TYPE_REPEAT;
        option.title = title;
        option.all = true;
        return record(option, false);
    }

    /** Record with recording option. */
    public RecordingRequest record(RecordingOption option) {
        return record(option, true);
    }

    /**
     * Record with recording option.
     *
     * newSeries가 false인 경우는 RecordingScheduler에 의해서 eposide 추가 되는 경우.
     */
    public RecordingRequest record(RecordingOption option, boolean newSeries) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record("+option+", "+newSeries+")");
        long dur = option.endTime - option.startTime;
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"RecordManager.record: option = " + option + ", all = " + option.all + ", dur = " + dur);
        }
        if (option.channel == null) {
            Log.printWarning("RecordManager.record: channel not found.");
            return null;
        }
//        if (option.all && newSeries && option.program != null) {
//            option.suppressConflict = true;
//        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record() : Call AppData.create(option)");
        AppData appData = AppData.create(option);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record() : Call record("+option.channel+", "+new Date(option.startTime)+", "+dur+", "+appData);
        RecordingRequest ret = record(option.channel, option.startTime, dur, appData);
        // 아래 코드는 새로만드는 newSeries 인 경우만 동작한다. EPG 업데이트에 의한 경우는 무시.
        if (option.all && newSeries) {
            String groupKey = null;
            RecordingScheduler sch = RecordingScheduler.getInstance();
            if (option.type == RecordingOption.TYPE_SERIES) {
                try {
                    TvProgram p = option.program;
                    groupKey = sch.setSeries(p, option);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"record() : TYPE_SERIES : groupKey = "+groupKey);
            } else if (option.type == RecordingOption.TYPE_REPEAT) {
                try {
                    TvProgram p = option.program;
                    TvChannel ch = option.channel;
                    if (p != null) {
                        groupKey = sch.setForcedRepeat(p, option, false);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"record() : FORCED_REPEAT : groupKey = "+groupKey);
                    } else {
                        groupKey = sch.setManualRepeat(ret, option.getRepeatString(), option.keepOption, option.onlyFromEpgCache, false, false);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"record() : MANUAL_REPEAT : groupKey = "+groupKey);
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (groupKey != null) {
                AppData.set(ret, AppData.GROUP_KEY, groupKey);
                UpcomingList.getInstance().updateRecording(ret);
                RecordedList.getInstance().updateRecording(ret);
                //->Kenneth[2015.8.24] VDTRMASTER-5592
                // Series 인 놈은 여기서 VBM 남긴다.
                // (테스트 후에 보니)위에서 record() 를 미리 불러 버리므로 series 의 경우도 일단 로그 아래서 남김
                // 따라서 여기서 뭔가 처리하지 않고 아래에서 group key 가 있는 경우는 로그 안남기면 됨.
                //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"record() : write VBM for Series : "+groupKey);
                //PvrVbmController.getInstance().writeNewRecordingScheduled();
                //<-
            }
        }
        return ret;
    }

    private RecordingRequest record(TvChannel ch, long start, long dur, AppData appData) {
        try {
            long saveUntil;
            String[] keys;
            Serializable[] values;
            if (appData != null) {
                AppDataEntries entries = appData.getEntries();
                keys = entries.getKeys();
                values = entries.getValues();
                saveUntil = appData.saveUntil;
                Long eid = (Long) appData.get(AppData.PPV_EID);
                if (eid != null && eid.longValue() != 0) {
                    Long st = (Long) appData.get(AppData.PROGRAM_START_TIME);
                    Long et = (Long) appData.get(AppData.PROGRAM_END_TIME);
                    if (st != null && et != null) {
                        long newEnd = Math.min(start + dur, et.longValue());
                        start = Math.max(start, st.longValue());
                        dur = newEnd - start;
                    }
                }
            } else {
                keys = new String[0];
                values = new Serializable[0];
                saveUntil = Long.MAX_VALUE;
            }
            return record(ch, start, dur, keys, values, saveUntil);
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }
    }

    public RecordingRequest record(TvChannel ch, long start, long dur,
                        String[] keys, Serializable[] values, long saveUntil) {
        try {
            Locator locator;
            if (ch.getType() == TvChannel.TYPE_SDV) {
                locator = new OcapLocator(ch.getSourceId());
//                locator = new OcapLocator(getSdvDefaultFrequency(), 0xFFFF, 0x10);
//                locator = new OcapLocator(Core.epgService.getDataChannel().getSourceId());
            } else {
                String locatorString = ch.getLocatorString();
                if (Log.DEBUG_ON) {
                    Log.printDebug("RecordManager.record: locatorString = " + locatorString);
                }
                locator = LocatorFactory.getInstance().createLocator(locatorString);
            }
            return record(locator, start, dur, keys, values, saveUntil);
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }

    }

    private RecordingRequest record(Locator locator, long start, long dur,
                        String[] keys, Serializable[] values, long saveUntil) {
        Locator[] locators = null;
        if (TRANSFORM_LOCATOR) {
            try {
                locators = LocatorFactory.getInstance().transformLocator(locator);
            } catch (InvalidLocatorException ex) {
            }
            if (locators == null) {
                locators = new Locator[] { locator };
            }
        } else {
            locators = new Locator[] { locator };
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"RecordManager.record : dump locators from : " + locator + " ; " + locator.getClass());
            for (int i = 0; i < locators.length; i++) {
                Log.printDebug(App.LOG_HEADER+" locator[" + i + "] = " + locators[i] + " ; " + locators[i].getClass());
                if (locators[i] instanceof OcapLocator) {
                    Log.printDebug(App.LOG_HEADER+"  ocap source id = " + ((OcapLocator) locators[i]).getSourceID() );
                }
            }
        }

        //->Kenneth[2015.8.24] R5 : VDTRMASTER-5592
        // Series 가 아닌 놈만 여기서 VBM 로그 남겨야함.
        // Series 인 놈도 group key 생성 전에 record 가 불리우게 되므로 하나는 남기게 된다.
        // 이후 scheduler 가 추가적으로 생성하는 recording 들만 group key 로 구분해서 막으면 됨.
        try {
            int groupKeyIndex = -1;
            if (keys != null) {
                for (int i = 0 ; i < keys.length ; i++) {
                    if (AppData.GROUP_KEY.equals(keys[i])) {
                        groupKeyIndex = i;
                        break;
                    }
                }
            }
            Log.printDebug(App.LOG_HEADER+"RecordManager.record() : groupKeyIndex = "+groupKeyIndex);
            String groupKey = null;
            if (groupKeyIndex > -1) {
                groupKey = (String)values[groupKeyIndex];
                Log.printDebug(App.LOG_HEADER+"RecordManager.record() : groupKey = "+groupKey);
            }

            if (groupKey == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"record() : write VBM");
                PvrVbmController.getInstance().writeNewRecordingScheduled();
            }
        } catch (Exception e) {
            Log.print(e);
        }
        //<-

        RecordingRequest request = null;
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record() : CREATE Real Recording");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record() : Start Time : "+new Date(start));
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.record() : Duration : "+dur);
            LocatorRecordingSpec spec = new LocatorRecordingSpec(locators,
                new Date(start), dur, createRecordingProperties(saveUntil));
            return manager.record(spec, keys, values);
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }
    }

    public RecordingRequest[] _findOverlapped(long startA, long endA) {
    	ArrayList overlapped = new ArrayList();
    	RecordingList list = RecordingListManager.getInstance().getCancelableList();
    	RecordingRequest reqB = null;
    	long startB, endB;
    	for (int i = 0; i < list.size(); i++) {
    		reqB = list.getRecordingRequest(i);
    		startB = AppData.getScheduledStartTime(reqB) + 1000;
    		endB = startB + AppData.getScheduledDuration(reqB) - 1000;

    		if (endB < startA || endA < startB) { // B end before A || A end before B
    			continue;
    		}
   			overlapped.add(reqB); // else, it's overlapped
    	}
    	if (Log.DEBUG_ON) {
            Log.printDebug("overlapped recording found : " + overlapped.size());
        }
    	RecordingRequest[] ret = new RecordingRequest[overlapped.size()];
    	overlapped.toArray(ret);
    	return ret;
    }

    // return suggested recordings w/o conflict
    public ArrayList _getSuggested(RecordingRequest[] conflicts) {
    	ArrayList list = new ArrayList();
    	// TODO fill the logic
    	list.add(conflicts[0]);
    	list.add(conflicts[conflicts.length-1]);
    	return list;
    }

    public boolean stop(TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.stop("+p+")");
        OcapRecordingRequest req = getRequest(p);
        if (req == null) {
            return false;
        }
        try {
            if (RecordingListManager.isInProgress(req)) {
                req.stop();
            } else {
                req.delete();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }

    public void cancel(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.cancel("+req+")");
        try {
            setExpirationTime(req, System.currentTimeMillis() + FAILED_RECORDING_KEEP_DURATION);
            ((OcapRecordingRequest) req).cancel();
        } catch (Exception ex) {
            try {
                req.delete();
            } catch (Exception ex2) {
            }
        }
    }

    /** RecordingChangedListener */
    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged("+e+")");
        final RecordingRequest req = e.getRecordingRequest();
        final int change = e.getChange();
        final int oldState = e.getOldState();
        final int newState = e.getState();

//        if (Log.DEBUG_ON) {
//            try {
//                Log.printDebug("time = " + new Date());
//                Log.printDebug("RecordManager, " + req.getAppData(AppData.TITLE));
//                Log.printDebug("recordingChanged. id = " + req.getId() + ", req = " + req);
//                Log.printDebug(" event = " + e);
//                Field[] fields = e.getClass().getFields();
//                for (int i = 0; i < fields.length; i++) {
//                    String name = fields[i].getName();
//                    if (!name.startsWith("ENTRY_")) {
//                        continue;
//                    }
//                    Object o = fields[i].get(e);
//                    if (o instanceof Integer && ((Integer)o).intValue() == change) {
//                        Log.printDebug(" change = " + name);
//                        break;
//                    }
//                }
//                fields = req.getClass().getFields();
//                for (int i = 0; i < fields.length; i++) {
//                    String name = fields[i].getName();
//                    if (!name.endsWith("_STATE")) {
//                        continue;
//                    }
//                    Object o = fields[i].get(req);
//                    if (o instanceof Integer && ((Integer)o).intValue() == oldState) {
//                        Log.printDebug(" old state = " + name);
//                        break;
//                    }
//                }
//                fields = req.getClass().getFields();
//                for (int i = 0; i < fields.length; i++) {
//                    String name = fields[i].getName();
//                    if (!name.endsWith("_STATE")) {
//                        continue;
//                    }
//                    Object o = fields[i].get(req);
//                    if (o instanceof Integer && ((Integer)o).intValue() == newState) {
//                        Log.printDebug(" new state = " + name);
//                        break;
//                    }
//                }
//                AppData.print(req);
//            } catch (Exception ex) {
//                Log.print(ex);
//            }
//        }

        String changeString = "";
        switch (change) {
            case RecordingChangedEvent.ENTRY_ADDED:
                changeString = "ENTRY_ADDED : " + getStateString(newState);
                break;
            case RecordingChangedEvent.ENTRY_DELETED:
                changeString = "ENTRY_DELETED : " + getStateString(oldState);
                break;
            case RecordingChangedEvent.ENTRY_STATE_CHANGED:
                changeString = "ENTRY_STATE_CHANGED : " + getStateString(oldState) + " > " + getStateString(newState);
                break;
        }
        if (Log.INFO_ON) {
            try {
                Log.printInfo(App.LOG_HEADER+"RecordingManager.recordingChanged() : time = " + new Date());
                Log.printInfo(App.LOG_HEADER+"RecordManager, " + req.getAppData(AppData.TITLE));
                Log.printInfo(App.LOG_HEADER+"recordingChanged. id = " + req.getId() + ", req = " + req);
                Log.printInfo(App.LOG_HEADER+" event = " + e);
                Log.printInfo(App.LOG_HEADER+" change = " + changeString);
                AppData.printAppData(req);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        MetadataDiagScreen.getInstance().writeLocalLog("Local " + req.getId() + " : " + changeString);



        if (change != RecordingChangedEvent.ENTRY_DELETED) {
            final int reason = AppData.updateFailedReason(req);
            switch (newState) {
                case OcapRecordingRequest.FAILED_STATE:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : FAILED_STATE");
                    Thread t = new Thread("recording_fail") {
                        public void run() {
                            // 예기치 못한 conflict 상황으로 실패 했을 경우
                            boolean sdv = false;
                            if (reason == RecordingFailedException.CONTENT_NOT_FOUND) {
                                sdv = checkSdvFailed(req);
                            }
                            if (!sdv) {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged().recording_fail.run() : Call recordingFailed()");
                                recordingFailed(req, reason);
                                setExpirationTime(req, System.currentTimeMillis() + FAILED_RECORDING_KEEP_DURATION);
                            }
                        }
                    };
                    t.start();
                    break;
                case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : IN_PROGRESS_WITH_ERROR_STATE");
                    boolean deleted = deleteSkippedRecording(req);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : deleteSkippedRecording() returns "+deleted);
                    if (!deleted) {
                        if (change == RecordingChangedEvent.ENTRY_ADDED) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : Start recording_error_addded");
                            Thread tt = new Thread("recording_error_added") {
                                public void run() {
                                    boolean sdv = false;
                                    if (reason == RecordingFailedException.CONTENT_NOT_FOUND) {
                                        sdv = checkSdvFailed(req);
                                    }
                                    if (!sdv) {
                                        if (Log.DEBUG_ON)
                                            Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged().recording_error_added.run() : Call IssueManager.processNewConfilct()");
                                        IssueManager.getInstance().processNewConfilct(req);
                                    }
                                }
                            };
                            tt.start();
                        } else {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : Start recording_error_changed");
                            Thread tt = new Thread("recording_error_changed") {
                                public void run() {
//                                    if (oldState == OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE && reason == RecordingFailedException.INSUFFICIENT_RESOURCES) {
//                                        try {
//                                            ((LeafRecordingRequest) req).stop();
//                                        } catch (Exception ex) {
//                                        }
//                                    } else
                                    if (reason == RecordingFailedException.CONTENT_NOT_FOUND) {
                                        checkSdvFailed(req);
                                    }
                                }
                            };
                            tt.start();
                        }
                    }
                    break;
                case OcapRecordingRequest.IN_PROGRESS_STATE:
                case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : Call deleteSkippedRecording()");
                    deleteSkippedRecording(req);
                    break;
                case OcapRecordingRequest.INCOMPLETE_STATE:
                case OcapRecordingRequest.COMPLETED_STATE:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : Call RecordingScheduler.notifyRecordingCompleted()");
                    RecordingScheduler.getInstance().notifyRecordingCompleted(req);
                    break;
                case OcapRecordingRequest.DELETED_STATE:
                    Log.printInfo("RecordManager: expired");
                    try {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingChanged() : Call req.delete()");
                        req.delete();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                    break;
            }
        }
    }

    private boolean checkSdvFailed(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.checkSdvFailed("+req+")");
        RecordingSpec spec = req.getRecordingSpec();
        if (!(spec instanceof LocatorRecordingSpec)) {
            return false;
        }
        LocatorRecordingSpec ls = (LocatorRecordingSpec) spec;
        Locator[] array = ls.getSource();
        if (array.length <= 0 || !(array[0] instanceof OcapLocator)) {
            return false;
        }
        final int sid = AppData.getInteger(req, AppData.SOURCE_ID);
        OcapLocator ol = (OcapLocator) array[0];
        int osid = ol.getSourceID();
        Log.printDebug("RecordManager: checkSdvFailed sid = " + osid);
        if (osid == -1) {
            // 이미 freq가 있는 sdv 채널
            try {
                Core.epgService.notifyRequestedSdvRecordingFailed(sid);
            } catch (Exception e) {
                Log.print(e);
            }
            return false;
        }

        long start = ls.getStartTime().getTime();
        long dur = ls.getDuration();
        if (start + dur <= System.currentTimeMillis()) {
            // 이미 끝나버린 recording
            return false;
        }

        try {
            TvChannel channel = Core.epgService.getChannel(sid);
            if (channel == null) {
                return false;
            }
            if (channel.getType() != TvChannel.TYPE_SDV) {
                // SDV 채널이 아님
                return false;
            }
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }

        boolean ret = processSdvRecording(req, start, dur, sid);
        if (!ret) {
            try {
                Core.epgService.notifyRequestedSdvRecordingFailed(sid);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        return ret;
    }

    private boolean processSdvRecording(RecordingRequest req, long start, long dur, int sid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.processSdvRecording("+req+")");
        String[] keys = req.getKeys();
        Serializable[] values;
        if (keys == null) {
            keys = new String[0];
            values = new Serializable[0];
        } else {
            values = new Serializable[keys.length];
            for (int i = 0; i < keys.length; i++) {
                if (AppData.FAILED_REASON.equals(keys[i])) {
                    values[i] = new Integer(0);
                } else {
                    values[i] = req.getAppData(keys[i]);
                }
            }
        }

        Log.printInfo("RecordManager: dummy sdv recording is failed = " + sid);
        try {
            String locatorString = Core.epgService.requestSdvRecording(sid);
            int returnedReason = 0;
            try {
                returnedReason = Integer.parseInt(locatorString);
            } catch (Exception ex) {
            }
            if (returnedReason != 0 || locatorString == null || locatorString.equals(DUMMY_SDV_LOCATOR.toExternalForm())) {
                Log.printError("RecordManager: failed to get SDV locator.");
                if (returnedReason != RecordingFailedException.INSUFFICIENT_RESOURCES) {
                    try {
                        ((LeafRecordingRequest) req).stop();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
                if (returnedReason != 0) {
                    AppData.set(req, AppData.FAILED_REASON, new Integer(returnedReason));
                }
                return false;
            }
            Locator locator = LocatorFactory.getInstance().createLocator(locatorString);
            long saveUntil = getExpirationPeriod(req);

            try {
                req.delete();
            } catch (Exception e) {
                Log.print(e);
            }

            RecordingRequest r = record(locator, start, dur, keys, values, saveUntil);
            if (r == null) {
                return false;
            }
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
        return true;
    }

    private static int recordingFailed(RecordingRequest req, int reason) {
        return recordingFailed(req, reason, true);
    }

    /** 녹화 실패 처리 */
    public static int recordingFailed(RecordingRequest req, int reason, boolean show) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingFailed("+req+", "+reason+", "+show+")");
        if (!(req instanceof LeafRecordingRequest)) {
            return 0;
        }
        LeafRecordingRequest r = (LeafRecordingRequest) req;
        // 실패 원인
//        String exception = null;
//        int reason = 0;
        try {
            // RecordingFailedException
//            RecordingFailedException ex = (RecordingFailedException) r.getFailedException();
//            Log.print(ex);
//            reason = ex.getReason();
            if (reason == RecordingFailedException.USER_STOP) {
                Log.printDebug("RecordingFailed: reason is USER_STOP");
                int oldReason = AppData.getInteger(r, AppData.FAILED_REASON);
                if (oldReason != reason) {
                    reason = oldReason;
                }
            }

            String reasonString = getFailedReasonString(reason);
            String call = AppData.getString(r, AppData.CHANNEL_NAME);
            String title = AppData.getString(r, AppData.TITLE);
            String sid = AppData.getString(r, AppData.SOURCE_ID);
            int id = r.getId();
            String logString = "RecordingFailed. reason = " + reason + ", " + getFailedReasonString(reason)
                             + ", call = " + call + ", sid = " + sid + ", id = " + id + ", title = " + title;

            if (reason == RecordingFailedException.USER_STOP) {
                Log.printInfo(logString);
            } else {
                Log.printError(logString);
                AppData.set(r, AppData.FAILED_REASON, new Integer(reason));
            }
            if (show && reason != RecordingFailedException.USER_STOP
                       && reason != RecordingFailedException.INSUFFICIENT_RESOURCES
                       && reason != RecordingFailedException.SPACE_FULL) {
                String code = "PVR" + (700 + reason);
                Hashtable dynamic = new Hashtable();
                dynamic.put("<ERROR_CODE>", reasonString);
                dynamic.put("<NUMBER>", AppData.getString(r, AppData.CHANNEL_NUMBER));
                dynamic.put("<CHANNEL>", call);
                dynamic.put("<TITLE>", title);
                dynamic.put("<DATE_TIME>", Formatter.getCurrent().getLongDate(AppData.getScheduledStartTime(r)));
                //->Kenneth[2017.9.5] R7.4 : Error Message app 에 보낼때 PVR701 은 PVR715 로 바꿔서 보냄.
                if ("PVR701".equals(code)) {
                    if (Log.DEBUG_ON) Log.printDebug("RecordManager.showErrorMessage() : Replace PVR701 -> PVR715");
                    code = "PVR715";
                }
                //<-
                Core.getInstance().showErrorMessage(code, null, dynamic);
//                AppData.set(req, AppData.NI_INDEX, new Integer(-1));
            }
        } catch (Exception exc) {
            Log.print(exc);
        }
        return reason;
    }

    /** 녹화를 추가하고 나서 체크. */
    public boolean checkDiskSpace(RecordingRequest req) {
        return checkDiskSpace(req, true);
    }

    /** 녹화를 추가하고 나서 체크. */
    public boolean checkDiskSpace(RecordingRequest req, boolean showPopup) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.checkDiskSpace("+req+", "+showPopup+")");
        try {
            switch (req.getState()) {
                case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                    int reason = AppData.updateFailedReason(req);
                    if (reason == RecordingFailedException.SPACE_FULL) {
                        if (showPopup) {
                            showDiskFullMessage(req);
                        }
                        return false;
                    }
                    return true;
                case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                    if (showPopup) {
                        showDiskFullMessage(req);
                        return false;
                    }
                    return true;
                case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
                case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                    TvChannel ch = Core.epgService.getChannel(AppData.getInteger(req, AppData.SOURCE_ID));
                    //->Kenneth[2015.2.3] 4K
                    //if (!StorageInfoManager.getInstance().isCapable(AppData.getScheduledDuration(req), ch.isHd())) {
                    if (!StorageInfoManager.getInstance().isCapable(AppData.getScheduledDuration(req), ch.getDefinition())) {
                        if (showPopup) {
                            showDiskFullMessage(null);
                        }
                        return false;
                    }
                    return true;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }

    private void showDiskFullMessage(RecordingRequest toStop) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.showDiskFullMessage("+toStop+")");
        Core.getInstance().showErrorMessage("PVR601", null, null);
        if (STOP_WHEN_DISK_FULL) {
            if (toStop != null) {
                try {
                    ((LeafRecordingRequest) toStop).stop();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    public static String getFailedReasonString(int reason) {
        if (reason > 0 && reason < FAILED_REASONS.length) {
            return FAILED_REASONS[reason];
        } else {
            return "UNKNOWN(" + reason + ")";
        }
    }

    /** RecordingAlertListener. */
    public void recordingAlert(RecordingAlertEvent e) {
        RecordingRequest req = e.getRecordingRequest();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.recordingAlert("+req.getId()+")");
        deleteSkippedRecording(req);
    }

    private boolean deleteSkippedRecording(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.deleteSkippedRecording()");
        if (AppData.getBoolean(req, AppData.UNSET)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("RecordManager: skipped = " + AppData.getString(req, AppData.TITLE));
            }
            try {
                req.delete();
            } catch (Exception ex) {
                Log.print(ex);
            }
            return true;
        }
        return false;
    }

    public BufferingRequest getBufferingRequest(TvChannel channel) {
        BufferingRequest[] br = manager.getBufferingRequests();
        try {
            String chLocatorString = channel.getLocatorString();
            for (int i = 0; i < br.length; i++) {
                Locator loc = br[i].getService().getLocator();
                if (loc != null) {
                    if (loc.toExternalForm().equals(chLocatorString)) {
                        return br[i];
                    }
                }
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public void cancelAllBufferingRequests() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.cancelAllBufferingRequests()");
        BufferingRequest[] br = manager.getBufferingRequests();
        if (br != null) {
            for (int i = 0; i < br.length; i++) {
                Log.printDebug("RecordManager: cancelBufferingRequest = " + br[i]);
                try {
                    manager.cancelBufferingRequest(br[i]);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }


    public static boolean isInUseLocalOrRemote(RecordingRequest rr) {
        try {
            if (((IOStatus) rr).isInUse()) {
                return true;
            }
        } catch (Exception ex) {
        }
        try {
            Service s = VideoPlayer.getInstance().getService();
            if (s != null && s.equals(((LeafRecordingRequest) rr).getService())) {
                return true;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public static boolean deleteIfNotUsing(RecordingRequest rr) {
        if (isInUseLocalOrRemote(rr)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.deleteIfNotUsing() returns false");
            return false;
        }
        delete(rr);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.deleteIfNotUsing() returns true");
        return true;
    }

    public static boolean deleteIfPossible(RecordingRequest rr) {
        try {
            if (((IOStatus) rr).isInUse()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.deleteIfPossible() returns false");
                return false;
            }
        } catch (Exception ex) {
        }
        delete(rr);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.deleteIfPossible() returns true");
        return true;
    }

    public static void delete(final RecordingRequest rr) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.delete("+rr+")");
        RecordingListManager.markToBeDeleted(rr);
        deleteWorker.push(new Runnable() {
            public void run() {
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.delete().run() : rr.delete()");
                    rr.delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        });
    }

    public void dataUpdated(String key, Object old, Object value) {
        recordingBuffer = Math.max(PvrUtil.convertBufferMinute(value) * Constants.MS_PER_MINUTE, 0);
        Log.printInfo("RecordManager: buffer = " + recordingBuffer);
    }

    public void dataRemoved(String key)  {
    }

    public static String getStateString(int state) {
        switch (state) {
            case LeafRecordingRequest.COMPLETED_STATE:
                return "COMPLETED_STATE";
            case LeafRecordingRequest.DELETED_STATE:
                return "DELETED_STATE";
            case LeafRecordingRequest.FAILED_STATE:
                return "FAILED_STATE";
            case LeafRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                return "IN_PROGRESS_INCOMPLETE_STATE";
            case LeafRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                return "IN_PROGRESS_INSUFFICIENT_SPACE_STATE";
            case LeafRecordingRequest.IN_PROGRESS_STATE:
                return "IN_PROGRESS_STATE";
            case LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                return "IN_PROGRESS_WITH_ERROR_STATE";
            case LeafRecordingRequest.INCOMPLETE_STATE:
                return "INCOMPLETE_STATE";
            case LeafRecordingRequest.PENDING_NO_CONFLICT_STATE:
                return "PENDING_NO_CONFLICT_STATE";
            case LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                return "PENDING_WITH_CONFLICT_STATE";
            case OcapRecordingRequest.CANCELLED_STATE:
                return "CANCELLED_STATE";
            case OcapRecordingRequest.TEST_STATE:
                return "TEST_STATE";
            default:
                return "UNKNOWON_STATE (" + state + ")";

        }
    }

    //->Kenneth[2015.5.25] R5 : SD/HD Grouping 으로 인해서 Sister program 이 Recording 중인지 알 필요 생김
    // 아래의 두 API 를 새롭게 정의한다.
    // 얘는 Sister Program 을 찾아서 돌려준다.
    public TvProgram getSisterProgram(TvProgram program) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram("+program+")");
        if (program == null) return null;

        //->Kenneth[2016.3.8] R7 : Ungrouped 에서는 sister program 을 null 로 리턴한다.
        if (!Core.isChannelGrouped) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() : Ungrouped, so return null");
            return null;
        }
        //<-

        TvProgram sisterProgram = null;
        try {
            TvChannel ch = program.getChannel();
            if (ch == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() : Channel is null, Strange!");
                return null;
            }
            TvChannel sisterCh = ch.getSisterChannel();
            if (sisterCh == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() : SisterChannel is null");
                return null;
            }
            if (!sisterCh.isSubscribed()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() : SisterChannel is not subscribed");
                return null;
            }
            EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            if (es == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() : EpgService is null");
                return null;
            }
            long pst = program.getStartTime();
            sisterProgram = es.getProgram(sisterCh, pst);
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgram() returns "+sisterProgram);
        return sisterProgram;
    }

    // Sister Program 을 찾고 걔가 Recording schedule 되어 있는지 돌려준다.
    public RecordingRequest getSisterProgramRecordingRequest(TvProgram program) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgramRecordingRequest("+program+")");
        if (program == null) return null;
        //->Kenneth[2016.3.8] R7 : Ungrouped 에서는 getSisterProgram() 이 null 을 돌려주므로 얘도 자동적으로 null 리턴하게
        // 되므로 별다른 처리하지 않는다.
        TvProgram sisterProgram = getSisterProgram(program);
        try {
            if (sisterProgram == null) return null;
            RecordingRequest req = getRequest(sisterProgram);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.getSisterProgramRecordingRequest() : sister's RecordingRequest = "+req);
            return req;
        } catch (Exception e) {
            Log.print(e);
        }
        return null;
    }

    //->Kenneth[2017.9.1] R7.4 : PVR715 팝업의 경우 Error Message 로부터 호출될 수 있다
    // callLetter 에 포함된 모든 upcoming recordings 을 지운다
    public void removeAllUpcomingRecordings(String callLetter) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings("+callLetter+")");
        try {
            // 1. Upcoming list 를 가져온다.
            RecordingList pendingList = RecordingListManager.getInstance().getPendingList();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : pendingList = "+pendingList);
            if (pendingList == null) {
                return;
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : size = "+pendingList.size());
            if (pendingList.size() == 0) {
                return;
            }
            // 2. EPG 를 통해서 src_id 를 찾는다
            EpgService es = Core.epgService;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : es = "+es);
            if (es == null) {
                return ;
            }
            TvChannel ch = null;
            try {
                ch = es.getChannel(callLetter);
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : ch = "+ch);
            if (ch == null) {
                return ;
            }
            int sourceId = ch.getSourceId();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : sourceId = "+sourceId);
            // 3. Upcoming list 중에서 source ID 에 해당하는 놈들만 골라낸다.
            RecordingList chRecordings = RecordingListManager.filter(pendingList, AppData.SOURCE_ID, new Integer(sourceId));
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : chRecordings = "+chRecordings);
            if (chRecordings == null) {
                return;
            }
            int size = chRecordings.size();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : chRecordings.size() = "+size);
            if (size == 0) {
                return;
            }
            // 4. Recording 을 지운다. 
            Vector vector = new Vector();
            for (int i = 0 ; i < size ; i++) {
                int id = chRecordings.getRecordingRequest(i).getId();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : chRecordings["+i+"].id = "+id);
                vector.addElement(AbstractRecording.find(id+""));
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordManager.removeAllUpcomingRecordings() : Call UpcomingList.deleteRecordings(vector, false)");
            UpcomingList.getInstance().deleteRecordings(vector, false);
        } catch (Exception e) {
            Log.print(e);
        }
    }
    //<-
}

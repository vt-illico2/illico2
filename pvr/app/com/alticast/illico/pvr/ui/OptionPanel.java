package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.*;
import java.util.Date;
import java.awt.*;

public abstract class OptionPanel extends FullScreenPanel implements MenuListener, PopupContainer {

    protected ClickingEffect clickEffect;

    public MenuItem buttons = new MenuItem("Buttons");

    public Popup currentPopup = null;

    public OptionPanel() {
       Component filler = new Component() {
            public void paint(Graphics g) {
                if (isDimmed()) {
                    g.setColor(GraphicsResource.cDimmedBG);
                    g.fillRect(0, 0, 960, 540);
                }
            }
        };
        filler.setBounds(0, 0, 960, 540);
        this.add(filler);

        clickEffect = new ClickingEffect(this);
    }

    public void showPopup(Popup popup) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OptionPanel.showPopup("+popup+")");
        synchronized (stack) {
            if (currentPopup != null) {
                hidePopup(currentPopup);
            }
            popup.prepare();
            popup.start(this);
            popup.setVisible(false);
            currentPopup = popup;
            this.add(popup, 0);
            popup.startEffect();
        }
    }

    protected void hidePopup() {
        synchronized (stack) {
            hidePopup(currentPopup);
        }
    }

    public void hidePopup(Popup popup) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"OptionPanel.hidePopup("+popup+")");
        synchronized (stack) {
            if (currentPopup == popup) {
                if (popup != null) {
                    popup.stop();
                    this.remove(popup);
                }
                currentPopup = null;
            }
        }
        repaint();
    }

    public synchronized boolean focusUp() {
        if (focus > 0) {
            int nf = findSelectableBefore(focus);
            if (nf >= 0) {
                focus = nf;
                return true;
            }
        }
        return false;
    }

    public synchronized boolean focusDown() {
        if (focus < buttons.size() - 1) {
            int nf = findSelectableAfter(focus);
            if (nf >= 0) {
                focus = nf;
                return true;
            }
        }
        return false;
    }

    private int findSelectableAfter(int from) {
        int size = buttons.size();
        int i = Math.max(0, from + 1);
        while (i < size) {
            MenuItem menu = buttons.getItemAt(i);
            if (menu.isEnabled()) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private int findSelectableBefore(int from) {
        int size = buttons.size();
        int i = Math.min(size - 1, from - 1);
        while (i >= 0) {
            MenuItem menu = buttons.getItemAt(i);
            if (menu.isEnabled()) {
                return i;
            }
            i--;
        }
        return -1;
    }

    public boolean isDimmed() {
        return currentPopup != null;
    }

}

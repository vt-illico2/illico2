package com.alticast.illico.pvr.ui;

import java.awt.*;
import javax.tv.util.*;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.*;
import com.alticast.ui.*;

/**
 * RecordingIcon
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/09 20:41:16 $
 * @author  June Park
 */
public class RecordingIcon extends Component implements TVTimerWentOffListener {

    private static final long DEFAULT_AUTO_CLOSE_DELAY = 5000L;
    private static final Rectangle BOUNDS = new Rectangle(61, 50, 50, 28);

    private Image image = DataCenter.getInstance().getImage("REC_notice.png");

    private LayeredUI ui;

    private TVTimerSpec autoCloseTimer;

    private static RecordingIcon instance = new RecordingIcon();

    public static RecordingIcon getInstance() {
        return instance;
    }

    private RecordingIcon() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setTime(DEFAULT_AUTO_CLOSE_DELAY);
        autoCloseTimer.setRepeat(false);
        autoCloseTimer.setAbsolute(false);
        autoCloseTimer.addTVTimerWentOffListener(this);

        LayeredWindow window = new LayeredWindow();
        window.setSize(BOUNDS.width, BOUNDS.height);
        window.add(this);
        window.setVisible(true);
        this.setSize(BOUNDS.width, BOUNDS.height);
        this.setVisible(true);

        ui = WindowProperty.PVR_RECORDING_ICON.createLayeredWindow(window, BOUNDS);
        ui.deactivate();
    }

    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

    public void start() {
        ui.activate();
        startAutoCloseTimer();
    }

    public void stop() {
        ui.deactivate();
    }

    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(ui);
    }

    /** 자동으로 닫는 timer를 구동 */
    public synchronized void startAutoCloseTimer() {
        try {
            // 기존 타이머 제거 후 add
            TVTimer.getTimer().deschedule(autoCloseTimer);
            autoCloseTimer = TVTimer.getTimer().scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        stop();
    }

}

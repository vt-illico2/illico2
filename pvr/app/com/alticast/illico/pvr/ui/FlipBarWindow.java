package com.alticast.illico.pvr.ui;

import java.awt.event.KeyEvent;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.Player; import javax.media.Time;
import java.util.Date;
import java.util.Vector;
import java.rmi.RemoteException;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.Service;
import javax.tv.util.*;
import org.ocap.dvr.OcapRecordingManager;
import org.ocap.dvr.TimeShiftEvent;
import org.ocap.dvr.TimeShiftListener;
import org.ocap.dvr.TimeShiftProperties;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.RecordingManager;
import org.ocap.shared.dvr.RecordedService;
import org.ocap.shared.dvr.SegmentedRecordedService;
import org.ocap.shared.media.EnteringLiveModeEvent;
import org.ocap.shared.media.LeavingLiveModeEvent;
import org.ocap.shared.media.TimeShiftControl;
import org.dvb.event.UserEventRepository;
import org.dvb.event.UserEventListener;
import org.dvb.event.EventManager;
import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.flipbar.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.data.*;

import com.alticast.ui.LayeredWindow;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredKeyHandler;

/**
 * FlipBarWindow.
 *
 * 1) started - active,   has child             (보이고 있는 상태)
 * 2) hidden  - deactive, has child             (다른 app 때문에 숨겨진 상태)
 * 3) ready   - deactive,  no child, currentBar (보일 준비하는 상태.child 있고)
 * 3) stopped - deactive,  no child, no bar     (보이지 않는 상태)
 *
 * @author  June Park
 */
public class FlipBarWindow extends LayeredWindow
                        implements LayeredKeyHandler, TVTimerWentOffListener,
                                   FlipBarListener, ClockListener {

    private static long FLIPBAR_HIDE_DELAY_PLAY = 5000L;
    private static long FLIPBAR_HIDE_DELAY_PAUSE = 60000L;
    private static long FLIPBAR_HIDE_DELAY_REWIND = 0L;

    private FlipBar timeBar;
    private FlipBar playBar;

    private FlipBar currentBar;

    private int currentId;
    private int channelType;
    private TvChannel currentChannel;

//    private RecordedService currentRecordedService;
    private AbstractRecording currentRecording;
    private LayeredUI ui;

    private TVTimerSpec refreshTimer;
    private TVTimerSpec autoCloseTimer;

    private TimeShiftControl control;
    private Player player;

    private boolean enabled = true;

    private static FlipBarWindow instance = new FlipBarWindow();

    public static FlipBarWindow getInstance() {
        return instance;
    }

    private FlipBarWindow() {
        DataCenter dc = DataCenter.getInstance();
        FLIPBAR_HIDE_DELAY_PLAY = dc.getLong("FLIPBAR_HIDE_DELAY_PLAY");
        FLIPBAR_HIDE_DELAY_PAUSE = dc.getLong("FLIPBAR_HIDE_DELAY_PAUSE");
        FLIPBAR_HIDE_DELAY_REWIND = dc.getLong("FLIPBAR_HIDE_DELAY_REWIND");

        ui = WindowProperty.PVR_FLIP_BAR.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();
    }

    public void init() {
        refreshTimer = new TVTimerSpec();
        refreshTimer.setTime(500);
        refreshTimer.setRepeat(true);
        refreshTimer.setAbsolute(false);
        refreshTimer.setRegular(false);
        refreshTimer.addTVTimerWentOffListener(new RefreshTimerListener());

        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setTime(FLIPBAR_HIDE_DELAY_PLAY);
        autoCloseTimer.setRepeat(false);
        autoCloseTimer.setAbsolute(false);
        autoCloseTimer.addTVTimerWentOffListener(this);

        Clock.getInstance().addClockListener(this);

        timeBar = FlipBarFactory.getInstance().createTimeshiftFlipBar();
        playBar = FlipBarFactory.getInstance().createPvrFlipBar();

        timeBar.addListener(this);
        playBar.addListener(this);

        player = VideoPlayer.getInstance().getPlayer();

        setBounds(Constants.SCREEN_BOUNDS);

        FrameworkMain.getInstance().getImagePool().waitForAll();
        timeBar.setVisible(true);
        playBar.setVisible(true);
    }

//    public void controllerUpdate(ControllerEvent e) {
//        start();
//    }

    public FlipBar getPvrFlipBar() {
        return playBar;
    }

    public FlipBar getTimeshiftFlipBar() {
        return timeBar;
    }

    /** 준비된 flipbar 띄우기. started or hidden state. */
    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.start() : "+currentBar);
        if (currentBar == null) {
            return;
        }
        this.add(currentBar);
        if (!enabled) {
            return;
        }
        updateProgress();
        currentBar.prepare();
        ui.activate();

        TVTimer.getTimer().deschedule(refreshTimer);
        try {
            refreshTimer = TVTimer.getTimer().scheduleTimerSpec(refreshTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
        startAutoCloseTimer();
    }

    /** Timeshift flipbar 준비. ready state. */
    public synchronized void ready(int cid, TvChannel ch, TvProgram program) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.ready() : "+program);
        currentChannel = ch;
        currentId = cid;
        control = VideoPlayer.getInstance().getControl();
        player = VideoPlayer.getInstance().getPlayer();

        FlipBarContent fc = null;
        if (program != null) {
            try {
                fc = createFlipBarContent(ch, program);
                channelType = ch.getType();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        if (fc == null) {
            long now = System.currentTimeMillis();
            fc = new FlipBarContent("", now - Constants.MS_PER_HOUR, now + Constants.MS_PER_HOUR);
        }
        timeBar.setContent(fc);

        refreshTimeBar();
        updatePrograms();

        this.removeAll();
        currentBar = timeBar;
    }

    /** PVR flipbar 준비. ready state. */
    public synchronized void ready(AbstractRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.ready() : "+r);
        control = VideoPlayer.getInstance().getControl();
        player = VideoPlayer.getInstance().getPlayer();

        FlipBarContent fc = new FlipBarContent(r.getTitle(), 0, r.getRecordedDuration());
        fc.setRating(r.getRating());
        fc.setResolution(r.getDefinition() == 1);

        playBar.setContent(fc);
        playBar.setIndicationTimes(r.getSegmentTimes());

        if (currentBar != playBar) {
            currentBar = playBar;
            this.removeAll();
            this.add(playBar);
        }
        currentRecording = r;
//        currentRecordedService = rs;
    }

    public synchronized void update(RemoteRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.update() : "+r);
        if (currentBar == playBar) {
            currentRecording = r;
        }
    }

    /** Flipbar가 필요 없는 상태. stopped state. */
    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.stop()");
        TVTimer.getTimer().deschedule(autoCloseTimer);
        hideAll();
        currentBar = null;
    }

    /** Flipbar를 잠시 숨기기. hidden state. (FULL_APP_STATE 등) */
    private synchronized void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.pause()");
        ui.deactivate();
        TVTimer.getTimer().deschedule(refreshTimer);
    }

    /** FlipBar가 사라진 상태. ready sate. auto hide. */
    public synchronized void hideAll() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.hideAll()");
        pause();
        removeAll();
        if (currentBar != null) {
            currentBar.setFocus(FlipBarAction.PAUSE);
        }
    }

    /** 잠시 숨겼던걸 보여주기. started state. */
    private synchronized void resume() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.resume()");
        if (this.getComponentCount() > 0) {
            start();
        }
    }

    public synchronized boolean isStarted() {
        return this.getComponentCount() > 0;
    }

    //->Kenneth[2015.7.4] Tank
    public static boolean isPortalVisible = false;
    public synchronized void enableFlipBar(boolean e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.enableFlipBar("+e+")");
        // TODO - 같은 경우 skip 해도 될듯 하나, 그냥 두자.
        this.enabled = e;
        if (enabled && !isPortalVisible) {
        //if (enabled) {
            resume();
        } else {
            pause();
        }
    }
    //<-

    public void setFocus(FlipBarAction action) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.setFocus("+action+")");
        FlipBar bar = currentBar;
        if (bar != null) {
            bar.setFocus(action);
        }
    }

    public void dispose() {
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.handleKeyEvent("+e+")");
        int code = e.getCode();
        if (code == OCRcEvent.VK_EXIT) {
            hideAll();
            return true;
        }
        startAutoCloseTimer();
        if (currentBar != null) {
            return currentBar.handleKey(code);
        }
        return false;
    }

    /** 자동으로 닫는 timer를 구동 */
    public void startAutoCloseTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.startAutoCloseTimer()");
        synchronized (autoCloseTimer) {
            // 기존 타이머 제거 후 add
            TVTimer.getTimer().deschedule(autoCloseTimer);
            try {
                float rate = player.getRate();
                long time;
                if (rate == 0.0f) {
                    // pasue
                    time = FLIPBAR_HIDE_DELAY_PAUSE;
                } else if (rate >= VideoPlayer.MIN_FAST_RATE || rate <= -VideoPlayer.MIN_FAST_RATE) {
                    time = FLIPBAR_HIDE_DELAY_REWIND;
                } else {
                    time = FLIPBAR_HIDE_DELAY_PLAY;
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug("FlipBarWindow.startAutoCloseTimer = " + time + ", rate=" + rate);
                }
                if (time > 0) {
                    autoCloseTimer.setTime(time);
                    TVTimer.getTimer().scheduleTimerSpec(autoCloseTimer);
                }
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

    public synchronized void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.timerWentOff()");
        // close
        hideAll();
    }

    private void updateProgress() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.updateProgress()");
        if (currentBar == timeBar) {
            refreshTimeBar();
        } else if (currentBar == playBar) {
            refreshPlayBar();
        }
    }

    private FlipBarContent createFlipBarContent(TvChannel ch, TvProgram p) throws RemoteException {
        long start = p.getStartTime();
        long end = p.getEndTime();
        if (end <= start) {
            end = start + Constants.MS_PER_HOUR;
        }
        FlipBarContent fc = new FlipBarContent(p.getTitle(), start, end);
        fc.setRating(p.getRating());
        fc.setResolution(p.isHd() && (ch == null || ch.isHd()));
        fc.setData(p);
        return fc;
    }

    private void updatePrograms() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.updatePrograms()");
        if (control == null) {
            Log.printWarning("FlipBarWindow.updatePrograms: control is null.");
            return;
        }
        long updateTime = System.currentTimeMillis();
        FlipBarContent[] con = timeBar.getContents();
        FlipBarContent cur = timeBar.getCurrentContent();
        int cid = currentId;

        long bufferStart = PvrUtil.getTime(control.getBeginningOfBuffer());
        long bufferEnd = PvrUtil.getTime(control.getEndOfBuffer());
        EpgService epg = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        TvProgram p;
        try {
            int type = TvProgram.TYPE_UNKNOWN;
            if (cur != null) {
                TvProgram currentProgram = (TvProgram) cur.getData();
                if (currentProgram != null) {
                    type = currentProgram.getType();
                }
            }
            if (type == TvProgram.TYPE_UNKNOWN) {
                // current program has unknown type. update All
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.updatePrograms() : update all");
                Vector vector = new Vector();
                long time = bufferStart;
                TvProgram lastProgram = null;
                do {
                    p = epg.getProgram(cid, time);
                    if (p != null) {
                        FlipBarContent fc = createFlipBarContent(currentChannel, p);
                        time = fc.getEndTime();
                        vector.addElement(fc);
                        // TODO - 무한루프 걸릴 수 있다.
                        lastProgram = p;
                    }
                } while (time <= bufferEnd && p != null && lastProgram != p);
                FlipBarContent[] array = new FlipBarContent[vector.size()];
                vector.copyInto(array);
                timeBar.setContents(array);
            } else {
                // update head or tail
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.updatePrograms() : partial update");
                Vector vector = null;
                if (bufferStart < con[0].getStartTime()) {
                    vector = new Vector();
                    long time = con[0].getStartTime() - 1;
                    do {
                        p = epg.getProgram(cid, time);
                        if (p != null) {
                            FlipBarContent fc = createFlipBarContent(currentChannel, p);
                            time = fc.getStartTime() - 1;
                            vector.add(0, fc);
                        }
                    } while (bufferStart < time && p != null);
                    for (int i = 0; i < con.length; i++) {
                        vector.addElement(con[i]);
                    }
                }
                if (con[con.length-1].getEndTime() <= bufferEnd) {
                    if (vector == null) {
                        vector = new Vector();
                        for (int i = 0; i < con.length; i++) {
                            vector.addElement(con[i]);
                        }
                    }
                    long time = con[con.length-1].getEndTime();
                    do {
                        p = epg.getProgram(cid, time);
                        if (p != null) {
                            FlipBarContent fc = createFlipBarContent(currentChannel, p);
                            time = fc.getEndTime();
                            vector.addElement(fc);
                        }
                    } while (time <= bufferEnd && p != null);
                }
                if (vector != null) {
                    FlipBarContent[] array = new FlipBarContent[vector.size()];
                    vector.copyInto(array);
                    timeBar.setContents(array);
                }
            }
        } catch (Exception ex) {
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.updatePrograms() : time = "+ (System.currentTimeMillis() - updateTime));
    }

    // to update programs
    public void clockUpdated(Date date) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.clockUpdated("+date+")");
        if (currentBar != timeBar) {
            return;
        }
        updatePrograms();
    }


    private void refreshTimeBar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar()");
        if (control == null) {
            return;
        }
        // 현재 시간
        long nowTime = System.currentTimeMillis();
        long bufferBeginTime;
        long bufferEndTime;
        long playingTime;

        // buffer 시작, 끝
        if (control != null) {
            long bb = PvrUtil.getTime(control.getBeginningOfBuffer());
            long be = PvrUtil.getTime(control.getEndOfBuffer());
            bufferBeginTime = bb > 0 ? bb : nowTime;
            bufferEndTime = be > 0 ? be : nowTime;
        } else {
            bufferBeginTime = nowTime;
            bufferEndTime = nowTime;
        }
        if (player != null) {
            playingTime = PvrUtil.getTime(player.getMediaTime());
            if (playingTime < nowTime - Constants.MS_PER_DAY) {
                // 터무니 없이 작은 경우. 채널 전환 도중에 발생.
                playingTime = nowTime;
            }
        } else {
            playingTime = nowTime;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : BufferBeginTime = " + bufferBeginTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : BufferEndTime = " + bufferEndTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : Call FlipBar.setBufferTime(bufferBeginTime, BufferEndTime)");
        timeBar.setBufferTime(bufferBeginTime, bufferEndTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : NowTime = " + nowTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : PlayingTime = " + playingTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshTimeBar() : Call FlipBar.setMediaTime(playingTime)");
        timeBar.setMediaTime(playingTime);
        if (channelType == TvChannel.TYPE_PPV) {
            timeBar.setMode(FlipBar.MODE_PPV);
        } else if (VideoPlayer.getInstance().isLiveMode()) {
            timeBar.setMode(FlipBar.MODE_TV);
        } else {
            timeBar.setMode(FlipBar.MODE_TIMESHIFT);
        }
    }

    private void refreshPlayBar() {
        long dur = currentRecording.getRecordedDuration();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshPlayBar() : dur = " + dur + ", player = " + player);
        dur = Math.max(dur, 1);
        long playingTime;
        if (player != null) {
            playingTime = PvrUtil.getTime(player.getMediaTime());
            if (dur - playingTime < 200L) {
                playingTime = dur;
            }
        } else {
            playingTime = 0;
        }

        playBar.getCurrentContent().setEndTime(dur);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshPlayBar() : PlayingTime = " + playingTime);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.refreshPlayBar() : Call FlipBar.setMediaTime(playingTime)");
        playBar.setMediaTime(playingTime);
        playBar.setSkippable(dur >= 10 * Constants.MS_PER_MINUTE);
    }

    public boolean isSkippable() {
        FlipBar bar = currentBar;
        return ui.isActive() && bar != null && bar.isSkippable();
    }

    class RefreshTimerListener implements TVTimerWentOffListener {
        public void timerWentOff(TVTimerWentOffEvent event) {
            updateProgress();
        }
    }

    // FlipBar Listener
    public void actionPerformed(FlipBarAction action) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed("+action+")");
        VideoPlayer vc = VideoPlayer.getInstance();
        if (action == FlipBarAction.PLAY) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.togglePlaySlow()");
            vc.togglePlaySlow();
        } else if (action == FlipBarAction.PAUSE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.togglePause()");
            vc.togglePause();
        } else if (action == FlipBarAction.FAST_FORWARD) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.fastForward()");
            vc.fastForward();
        } else if (action == FlipBarAction.REWIND) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.rewind()");
            vc.rewind();
        } else if (action == FlipBarAction.SKIP_FORWARD) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.skipForward()");
            vc.skipForward();
        } else if (action == FlipBarAction.SKIP_BACKWARD) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.skipBackward()");
            vc.skipBackward();
        } else if (action == FlipBarAction.STOP) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.stop()");
            vc.stop();
        } else if (action == FlipBarAction.RECORD) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"FlipBarWindow.actionPerformed() : Call VideoPlayer.record()");
            vc.record();
        }
    }

    public void notifyShown(long time) {
    }

    public void notifyHidden(boolean selection) {
    }
}

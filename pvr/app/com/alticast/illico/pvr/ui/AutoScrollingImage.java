package com.alticast.illico.pvr.ui;

import java.awt.*;
import javax.tv.util.*;
import org.dvb.ui.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.pvr.list.*;


public class AutoScrollingImage implements TVTimerWentOffListener {

    private static final long START_DELAY = 3 * Constants.MS_PER_SECOND;
    private static final long MOVE_DELAY = 140L;
    private static final int MOVE_GAP = 7;

    private final Byte moveTimerLock = new Byte((byte) 9);

    private Entry currentEntry;
    public int offset = 0; // negative integer = paint offset, 0 = ready, 1 = to be painted

    public DVBBufferedImage image;

    protected TVTimerSpec updateTimer = new TVTimerSpec();
    protected TVTimerSpec moveTimer = new TVTimerSpec();

    private int width;
    private int height;

    private int currentWidth;

    private int targetX;
    private int targetWidth;
    private int middleGap;

    private AutoScrollingImageListener listener;

    public AutoScrollingImage(int w, int h, AutoScrollingImageListener l) {
        this.width = w;
        this.height = h;
        this.listener = l;
        prepare();

        updateTimer.setDelayTime(START_DELAY);
        updateTimer.setRepeat(false);
        updateTimer.setRegular(false);
        updateTimer.addTVTimerWentOffListener(new UpdateTimerListener());

        moveTimer.setDelayTime(MOVE_DELAY);
        moveTimer.setRepeat(true);
        moveTimer.setRegular(false);
        moveTimer.addTVTimerWentOffListener(this);
    }

    public synchronized void flush() {
        if (image != null) {
            image.dispose();
            image = null;
        }
        offset = 0;
        currentEntry = null;
        stopMoveTimer();
        stopUpdateTimer();
    }

    public synchronized void prepare() {
        if (image == null) {
            image = new DVBBufferedImage(width, height);
        }
        offset = 0;
        currentEntry = null;
    }

    public synchronized void setFocus(Entry entry) {
        Log.printDebug("AutoScrollingImage.setFocus = " + entry);
        if (entry == null) {
            currentEntry = null;
            offset = 0;
            stopMoveTimer();
            stopUpdateTimer();
            return;
        }
        if (!isChanged(entry)) {
            Log.printDebug("AutoScrollingImage.setFocus: same = " + currentEntry);
            return;
        }
        this.currentEntry = entry;
        offset = 0;
        startUpdateTimer();
    }

    private boolean isChanged(Entry entry) {
        if (currentEntry == null) {
            return true;
        }
        boolean isRecording1 = currentEntry instanceof AbstractRecording;
        boolean isRecording2 = entry instanceof AbstractRecording;

        if (isRecording1 && isRecording2) {
            AbstractRecording a1 = (AbstractRecording) currentEntry;
            AbstractRecording a2 = (AbstractRecording) entry;
            if (!a1.getIdObject().equals(a2.getIdObject())) {
                return true;
            }
            if (!a1.getTitle().equals(a2.getTitle())) {
                return true;
            }
            return !a1.getEpisodeTitle().equals(a2.getEpisodeTitle());
        } else if (!isRecording1 && !isRecording2) {
            return !entry.getTitle().equals(currentEntry.getTitle());
        } else {
            return true;
        }
    }

    public DVBGraphics getGraphics() {
        if (image != null) {
            return (DVBGraphics) image.getGraphics();
        }
        return null;
    }

    private synchronized void clear() {
        if (image == null) {
            return;
        }
        DVBGraphics g = (DVBGraphics) image.getGraphics();
        try {
            g.setDVBComposite(DVBAlphaComposite.Src);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }
        g.clearRect(0, 0, width, height);
        try {
            g.setDVBComposite(DVBAlphaComposite.SrcOver);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }
        g.dispose();
    }

    private void startUpdateTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(moveTimer);
        timer.deschedule(updateTimer);
        try {
            updateTimer = timer.scheduleTimerSpec(updateTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void stopUpdateTimer() {
        TVTimer.getTimer().deschedule(updateTimer);
    }

    class UpdateTimerListener implements TVTimerWentOffListener {
        public void timerWentOff(TVTimerWentOffEvent e) {
            Log.printDebug("AutoScrollingImage: ready animation");
            clear();
            offset = 1;
            listener.repaintImage();
        }
    }

    public synchronized void startAnimation(int tx, int tw, int space) {
        offset = 0;
        if (space <= tw) {
            return;
        }
        this.targetX = tx;
        this.targetWidth = tw;
        this.currentWidth = space;

        this.middleGap = (MOVE_GAP * 10) - (space % MOVE_GAP);

        startMoveTimer();
    }

    private void startMoveTimer() {
        synchronized (moveTimerLock) {
            TVTimer timer = TVTimer.getTimer();
            timer.deschedule(moveTimer);
            try {
                moveTimer = timer.scheduleTimerSpec(moveTimer);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void stopMoveTimer() {
        synchronized (moveTimerLock) {
            TVTimer.getTimer().deschedule(moveTimer);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        Log.printDebug("ticker - move timer");
        offset = offset - MOVE_GAP;
        if (currentWidth + middleGap + offset <= 0) {
            stopMoveTimer();
            offset = 0;
            listener.repaintImage();
            startUpdateTimer();
        }
        listener.repaintImage();
    }

    public boolean paint(Graphics g, Component c) {
        if (image == null || offset >= 0) {
            return false;
        }
        g.translate(targetX, 0);
        int sx1, sx2;
        int dx1, dx2;
        if (offset + currentWidth > 0) {
//            g.fillRect(targetX, targetWidth, 0, height);
            dx1 = 0;
            sx1 = targetX - offset;

            if (offset + currentWidth > targetWidth) {
                sx2 = targetX + targetWidth - offset;
                dx2 = targetWidth;
            } else {
                sx2 = targetX + currentWidth;
                dx2 = currentWidth + offset;
            }
            g.drawImage(image, dx1, 0, dx2, height, sx1, 0, sx2, height, c);
        }

        if (currentWidth + middleGap + offset < targetWidth) {
            sx1 = targetX;
            dx1 = currentWidth + middleGap + offset;
            sx2 = targetX + targetWidth - dx1;
            dx2 = targetWidth;

            g.drawImage(image, dx1, 0, dx2, height, sx1, 0, sx2, height, c);
        }
        g.translate(-targetX, 0);
        return true;
    }

}

package com.alticast.illico.pvr.ui;

import java.awt.event.KeyEvent;
import java.awt.*;
import javax.media.ControllerEvent;
import javax.media.ControllerListener;
import javax.media.Player;
import javax.media.Time;
import java.util.Date;
import java.util.Vector;
import java.rmi.RemoteException;
import javax.tv.util.*;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.RecordedService;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.flipbar.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.Popup;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.config.PvrConfig;
import com.alticast.illico.pvr.data.*;

import com.alticast.ui.LayeredWindow;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredKeyHandler;

public class PlayInfo extends Popup implements LayeredKeyHandler, TVTimerWentOffListener {

    static DataCenter dc = DataCenter.getInstance();

    private static PlayInfo instance = new PlayInfo();

    public static PlayInfo getInstance() {
        return instance;
    }

    LayeredUI ui;

    ScrollTexts scroll = new ScrollTexts();
    Footer footer = new Footer();

    String title;
    boolean isHd;
    //->Kenneth[2015.2.16] 4k
    boolean isUhd;
    String startTime;
    String channel;

    private TVTimerSpec autoCloseTimer;

    private PlayInfo() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setTime(5000L);
        autoCloseTimer.setRepeat(false);
        autoCloseTimer.setAbsolute(false);
        autoCloseTimer.addTVTimerWentOffListener(this);

        setRenderer(new PlayInfoRenderer());
        DataCenter dc = DataCenter.getInstance();

        LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.add(this);

        ui = WindowProperty.PVR_PLAY_INFO.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        ui.deactivate();

        scroll.setBounds(0, 128, 960, 65);
        scroll.setFont(FontResource.DINMED.getFont(15));
        scroll.setForeground(new Color(235, 235, 235));
        scroll.setRows(3);
        scroll.setRowHeight(17);
        scroll.setInsets(0, 78, 15, 27);
        // TODO - top mask
        scroll.setBottomMaskImage(dc.getImage("07_record_sha.png"));
        scroll.setBottomMaskImagePosition(0, 145-128);
        scroll.setVisible(true);
        this.add(scroll);

        Image iconScroll = footer.addButton(PreferenceService.BTN_PAGE, "pvr.Scroll Description");
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        footer.linkWithScrollTexts(iconScroll, scroll);
        footer.setBounds(500, 198, 906-500, 25);
        this.add(footer);
    }

    /** 준비된 flipbar 띄우기. started or hidden state. */
    public synchronized void start(AbstractRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PlayInfo.start("+r+")");
        if (r == null) {
            return;
        }
        Formatter fm = Formatter.getCurrent();
        title = r.getTitle();
        isHd = r.getInteger(AppData.DEFINITION) == 1;
        isUhd = r.getInteger(AppData.DEFINITION) == 2;
        long st = r.getLong(AppData.PROGRAM_START_TIME);
        startTime = fm.getLongDayText(st) + " " + fm.getTime(st);
        channel = r.getString(AppData.CHANNEL_NUMBER) + "  " + r.getString(AppData.CHANNEL_NAME);
        scroll.setContents(r.getString(AppData.FULL_DESCRIPTION));
        prepare();
        this.setVisible(false);
        ui.activate();
        startAutoCloseTimer();
        startEffect();
    }

    /** Flipbar가 필요 없는 상태. stopped state. */
    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PlayInfo.stop()");
        ui.deactivate();
        TVTimer.getTimer().deschedule(autoCloseTimer);
    }

    public synchronized void startAutoCloseTimer() {
        TVTimer.getTimer().deschedule(autoCloseTimer);
        PvrConfig config = (PvrConfig) DataCenter.getInstance().get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config == null) {
            return;
        }
        autoCloseTimer.setTime(config.infoDuration * Constants.MS_PER_SECOND);
        try {
            TVTimer.getTimer().scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        stop();
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PlayInfo.handleKeyEvent("+e+")");
        startAutoCloseTimer();
        switch (e.getCode()) {
            case OCRcEvent.VK_PAGE_UP:
                scroll.showPreviousPage();
                break;
            case OCRcEvent.VK_PAGE_DOWN:
                scroll.showNextPage();
                break;
            case OCRcEvent.VK_EXIT:
            case OCRcEvent.VK_INFO:
            case KeyCodes.LAST:
                stop();
                break;
            case OCRcEvent.VK_GUIDE:
            case KeyCodes.VOD:
            case KeyCodes.MENU:
            case KeyCodes.WIDGET:
            case KeyCodes.LIST:
                stop();
                return false;
            default:
                return false;
        }
        return true;
    }

    public void animationEnded(Effect effect) {
        super.animationEnded(effect);
        startAutoCloseTimer();
    }

    public void startClickingEffect() {
    }

    class PlayInfoRenderer extends Renderer {

        private final Rectangle BOUNDS = new Rectangle(0, 290, 960, 250);

        Image i_07_pop_sha;
        Image i_large_noti_b;
        Image i_large_noti_m;
        Image i_large_noti_t;
        Image icon_hd;
        //->Kenneth[2015.2.16] 4k
        Image icon_uhd;

        Color cTitle = new Color(255, 203, 0);
        Font fTitle = FontResource.BLENDER.getFont(24);

        Color cTime = new Color(224, 224, 224);
        Font fTime = FontResource.BLENDER.getFont(19);

        Color cChannel = Color.white;
        Font fChannel = FontResource.BLENDER.getFont(22);

        public Rectangle getPreferredBounds(UIComponent c) {
            return BOUNDS;
        }

        public void prepare(UIComponent c) {
            i_07_pop_sha = dc.getImage("07_pop_sha.png");
            i_large_noti_b = dc.getImage("large_noti_b.png");
            i_large_noti_m = dc.getImage("large_noti_m.png");
            i_large_noti_t = dc.getImage("large_noti_t.png");
            icon_hd = dc.getImage("icon_hd.png");
            //->Kenneth[2015.2.16] 4k
            icon_uhd = dc.getImage("icon_uhd.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public void paint(Graphics g, UIComponent c) {
            g.drawImage(i_07_pop_sha, 0, 79, 960, 171, c);
            g.drawImage(i_large_noti_b, 0, 120, c);
            g.drawImage(i_large_noti_m, 0, 70, 960, 50, c);
            g.drawImage(i_large_noti_t, 0, 0, c);

            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(title, 79, 85);

            //->Kenneth[2015.2.16] 4k
            if (isUhd) {
                g.drawImage(icon_uhd, g.getFontMetrics().stringWidth(title) + 79 + 5, 72, c);
            } else if (isHd) {
                g.drawImage(icon_hd, g.getFontMetrics().stringWidth(title) + 79 + 5, 72, c);
            }

            g.setColor(cTime);
            g.setFont(fTime);
            g.drawString(startTime, 79, 109);

            g.setColor(cChannel);
            g.setFont(fChannel);
            GraphicUtil.drawStringRight(g, channel, 910, 85);
        }
    }
}

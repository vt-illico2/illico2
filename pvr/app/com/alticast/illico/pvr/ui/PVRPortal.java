package com.alticast.illico.pvr.ui;

import java.rmi.RemoteException;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.MenuController;
import com.alticast.illico.pvr.gui.PVRPortalRenderer;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoResizeListener;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.MenuListener;
import com.videotron.tvi.illico.ui.OptionScreen;
import com.videotron.tvi.illico.util.*;

public class PVRPortal extends FullScreenPanel implements VideoResizeListener, MenuListener {
    private static final long serialVersionUID = 8410559940186295592L;

    /** D-Option instance. */
    private OptionScreen optScr;

    /** D-Opton MenuItem. */
    private static final SceneChangeMenuItem OPTION_MENU_RECORDING_LIST = new SceneChangeMenuItem("pvrportal.recording_list", PvrService.MENU_RECORDING_LIST);
    private static final SceneChangeMenuItem OPTION_SCHEDULED_RECORDINGS = new SceneChangeMenuItem("pvrportal.scheduled_recording_list", PvrService.MENU_SCHEDULED_RECORDINGS);
    private static final SceneChangeMenuItem OPTION_CREATE_RECORDING = new SceneChangeMenuItem("pvrportal.create_recording", PvrService.MENU_CREATE_RECORDING);
    private static final SceneChangeMenuItem OPTION_PVR_MANAGEMENT = new SceneChangeMenuItem("pvrportal.pvr_management", PvrService.MENU_PVR_MANAGEMENT);
    private static final SceneChangeMenuItem OPTION_PREFERENCE = new SceneChangeMenuItem("pvrportal.preference", PvrService.MENU_PREFERENCE);

    /*****************************************************************
     * values - PVRPortalTree-related
     *****************************************************************/
//    public static final int[] PORTAL_LIST_ORDER = {PvrService.MENU_RECORDING_LIST,
//            PvrService.MENU_SCHEDULED_RECORDINGS, PvrService.MENU_CREATE_RECORDING, PvrService.MENU_PVR_MANAGEMENT,
//            PvrService.MENU_PREFERENCE};
//    public static final String[] PORTAL_LIST_TXT_KEYS_1 = new String[] {"pvrportal.recording_list",
//            "pvrportal.scheduled_recording_list", "pvrportal.create_recording", "pvrportal.pvr_management",
//            "pvrportal.preference"};
//    public static final String[] PORTAL_LIST_TXT_KEYS_2 = new String[] {"pvrportal.recording_list", "pvrportal.preference"};

    public static final SceneChangeMenuItem[] MENU_PVR = {
        OPTION_MENU_RECORDING_LIST,
        OPTION_SCHEDULED_RECORDINGS,
        OPTION_CREATE_RECORDING,
        OPTION_PVR_MANAGEMENT,
        OPTION_PREFERENCE,
    };

    public static final SceneChangeMenuItem[] MENU_NON_PVR = {
        OPTION_MENU_RECORDING_LIST,
//        OPTION_PREFERENCE,
    };

    public static SceneChangeMenuItem[] menuKeys = Environment.SUPPORT_DVR ? MENU_PVR : MENU_NON_PVR;

    public PVRPortalTree pvrPortalTree;

//    public MenuItem dOptionKey;

    public PvrFooter footer;

    AlphaEffect initStart;

    public PVRPortal() {
        stack.push("pvr.PVR");
        if (pvrPortalTree == null) {
            pvrPortalTree = new PVRPortalTree(this);
        }

        if (optScr == null) {
            optScr = new OptionScreen();
        }
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortal.start()");
        if (!App.R3_TARGET || "EPG".equals(Core.getInstance().getVideoContextName())) {
            try {
                Object ob = DataCenter.getInstance().get(EpgService.IXC_NAME);
                if (ob != null) {
                    EpgService epgService = (EpgService) ob;
                    epgService.changeChannel(-1);
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        setRenderer(new PVRPortalRenderer());

        if (footer == null) {
            footer = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
            add(footer);
            footer.setBounds(0, 488, 906, 25);
        }

        prepare();

        if (initStart == null) {
            initStart = new AlphaEffect(this, 15, MovingEffect.FADE_IN);
            Log.printDebug("first = " + initStart);
        }
        issueUpdated(IssueManager.getInstance().getIssue());

        if (pvrPortalTree != null) {
            add(pvrPortalTree);
            pvrPortalTree.start();
        }
//        if (MenuController.getInstance().comeToAnother) {
//            initStart.start();
//        } else {
            MenuController.getInstance().resizeScaledVideo(373, 72, 537, 301);

            this.setVisible(true);
            pvrPortalTree.effectScene(true);
//        }
    }

    public void issueUpdated(Issue issue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortal.issueUpdated("+issue+")");
        pvrPortalTree.setIssue(issue);
        repaint();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortal.stop()");
        if (pvrPortalTree != null) {
            pvrPortalTree.stop();
            remove(pvrPortalTree);
        }
        if (footer != null) {
            remove(footer);
            footer = null;
        }

        if (optScr != null) {
            optScr.stop();
        }
//        try {
//            Object ob = DataCenter.getInstance().get(EpgService.IXC_NAME);
//            if (ob != null) {
//                EpgService epgService = (EpgService) ob;
//                epgService.stopChannel();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        MenuController.getInstance().resizeScaledVideo(0, 0, 960, 540);
    }

    public boolean handleKey(int keyCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortal.handleKey("+keyCode+")");
        boolean keyCodeResult = false;
        if (pvrPortalTree != null) {
            switch (keyCode) {
                case KeyCodes.COLOR_D:
                    footer.clickAnimationByKeyCode(keyCode);
//                    footer.clickAnimation(0);
                    MenuItem root = new MenuItem("D-Option", menuKeys);
                    root.add(MenuItems.getParentalControlMenu());
                    root.add(MenuItems.GO_HELP);
                    optScr.start(root, this);
                    keyCodeResult = true;
                    break;
                case KeyCodes.SEARCH:
                    optScr.stop();
                    break;
                default:
                    keyCodeResult = pvrPortalTree.handleKey(keyCode);
                    repaint();
                    break;
            }
        }
        return keyCodeResult;
    }

//    public int getType() {
//        return type;
//    }

    /*****************************************************************
     * methods - VideoResizeListener-implemented
     *****************************************************************/
    public void mainScreenResized(int x, int y, int w, int h) throws RemoteException {

    }

    /*****************************************************************
     * methods - PVRPortal-related
     *****************************************************************/

    /**
     * D-Option canceled.
     */
    public void canceled() {
    }

    /**
     * D-Option Selected.
     */
    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortal.selected("+item+")");
        Core.getInstance().selected(item);
    }

    public void animationEnded(Effect effect) {
        Log.printDebug("effect = " + effect);
        if (effect instanceof AlphaEffect) {
            MenuController.getInstance().resizeScaledVideo(373, 72, 537, 301);
            this.setVisible(true);
            pvrPortalTree.effectStart();
        }
    }
}

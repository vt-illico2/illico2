package com.alticast.illico.pvr.ui;

import java.awt.*;
import javax.tv.util.*;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.*;
import com.alticast.ui.*;

/**
 * VideoFeedback
 *
 * @version $Revision: 1.9 $ $Date: 2017/01/09 20:41:16 $
 * @author  June Park
 */
public class VideoFeedback extends Component implements TVTimerWentOffListener {

    private static final long DEFAULT_AUTO_CLOSE_DELAY = 2000L;
    private static final Rectangle BOUNDS = new Rectangle(429, 197, 104, 104);

    private LayeredUI ui;

    private TVTimerSpec autoCloseTimer;

    private static VideoFeedback instance = new VideoFeedback();

    public static final int NO_ICON = 0;

    public static final int SKIP_BACKWARD = 1;
    public static final int REWIND        = 2;
    public static final int STOP          = 3;
    public static final int PAUSE         = 4;
    public static final int PLAY          = 5;
    public static final int FAST_FORWARD  = 6;
    public static final int SKIP_FORWARD  = 7;
    public static final int SLOW          = 8;

    public static final int RECORD        = 9;
    public static final int STOP_RECORD   = 10;

    public static final int FF2            = 11;
    public static final int FF4            = 12;
    public static final int FF8            = 13;
    public static final int FF16           = 14;
    public static final int FF32           = 15;
    public static final int FF64           = 16;
    public static final int FF128          = 17;
    public static final int FF256          = 18;

    public static final int REW2           = 19;
    public static final int REW4           = 20;
    public static final int REW8           = 21;
    public static final int REW16          = 22;
    public static final int REW32          = 23;
    public static final int REW64          = 24;
    public static final int REW128         = 25;
    public static final int REW256         = 26;

    private int mode = NO_ICON;
    private boolean enabled = true;

    private Image[] icons       = new Image[27];
    private Image[] backgrounds = new Image[27];
    {
        DataCenter dc = DataCenter.getInstance();
        Image bg1 = dc.getImage("04_flip_st_foc_b01.png");
        Image bg2 = dc.getImage("04_flip_st_foc_b02.png");

        for (int i = SKIP_BACKWARD; i <= SKIP_FORWARD; i++) {
            icons[i] = dc.getImage("04_flip_st_0" + i + "_foc_b.png");
            backgrounds[i] = bg1;
        }
        for (int i = FF2; i <= REW256; i++) {
            backgrounds[i] = bg1;
        }
        icons[SLOW] = dc.getImage("04_flip_st_slow.png");
        backgrounds[SLOW] = bg1;

        icons[FF2] = dc.getImage("04_feed_ff_2.png");
        icons[FF4] = dc.getImage("04_feed_ff_4.png");
        icons[FF8] = dc.getImage("04_feed_ff_8.png");
        icons[FF16] = dc.getImage("04_feed_ff_16.png");
        icons[FF32] = dc.getImage("04_feed_ff_32.png");
        icons[FF64] = dc.getImage("04_feed_ff_64.png");
        icons[FF128] = dc.getImage("04_feed_ff_128.png");
        icons[FF256] = dc.getImage("04_feed_ff_256.png");

        icons[REW2] = dc.getImage("04_feed_rew_2.png");
        icons[REW4] = dc.getImage("04_feed_rew_4.png");
        icons[REW8] = dc.getImage("04_feed_rew_8.png");
        icons[REW16] = dc.getImage("04_feed_rew_16.png");
        icons[REW32] = dc.getImage("04_feed_rew_32.png");
        icons[REW64] = dc.getImage("04_feed_rew_64.png");
        icons[REW128] = dc.getImage("04_feed_rew_128.png");
        icons[REW256] = dc.getImage("04_feed_rew_256.png");

        icons[RECORD] = dc.getImage("04_flip_st_08_foc_b.png");
        backgrounds[RECORD] = bg2;
        icons[STOP_RECORD] = dc.getImage("04_flip_st_08_stop_b.png");
        backgrounds[STOP_RECORD] = bg2;
    }

    public static VideoFeedback getInstance() {
        return instance;
    }

    private VideoFeedback() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setTime(DEFAULT_AUTO_CLOSE_DELAY);
        autoCloseTimer.setRepeat(false);
        autoCloseTimer.setAbsolute(false);
        autoCloseTimer.addTVTimerWentOffListener(this);

        LayeredWindow window = new LayeredWindow();
        window.setSize(BOUNDS.width, BOUNDS.height);
        window.add(this);
        window.setVisible(true);
        this.setSize(BOUNDS.width, BOUNDS.height);
        this.setVisible(true);

        ui = WindowProperty.PVR_VIDEO_FEEDBACK.createLayeredWindow(window, BOUNDS);
        ui.deactivate();
    }

    public synchronized void start(int newMode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoFeedback.start("+newMode+")");
        if (newMode == PAUSE && this.mode == STOP) {
            // mode 변경 말자
        } else {
            this.mode = newMode;
        }
        if (enabled) {
            ui.activate();
        }
        startAutoCloseTimer();
    }

    public void rateChanged(float rate) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoFeedback.rateChanged("+rate+")");
        if (rate == 0.0f) {
            start(PAUSE);
        } else if (rate == 1.0f) {
            start(PLAY);
        } else if (rate >= VideoPlayer.MIN_FAST_RATE) {
            int index = (int) Math.round(Math.log(rate)/Math.log(2.0d));
            index = FF2 - 1 + index;
            if (index > FF256) {
                index = FAST_FORWARD;
            }
            start(index);
        } else if (rate <= -VideoPlayer.MIN_FAST_RATE) {
            int index = (int) Math.round(Math.log(-rate)/Math.log(2.0d));
            index = REW2 - 1 + index;
            if (index > REW256) {
                index = REWIND;
            }
            start(index);
        } else if (rate == TunerController.SLOW_PLAY_RATE) {
            start(SLOW);
        }
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoFeedback.stop()");
        ui.deactivate();
        mode = NO_ICON;
    }

    public synchronized void enableFeedback(boolean e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoFeedback.enableFeedback("+e+")");
        this.enabled = e;
        if (enabled) {
            if (mode != NO_ICON) {
                ui.activate();
            }
        } else {
            ui.deactivate();
        }
    }

    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(ui);
    }

    /** 자동으로 닫는 timer를 구동 */
    public void startAutoCloseTimer() {
        synchronized (autoCloseTimer) {
            // 기존 타이머 제거 후 add
            TVTimer.getTimer().deschedule(autoCloseTimer);
            try {
                TVTimer.getTimer().scheduleTimerSpec(autoCloseTimer);
            } catch (TVTimerScheduleFailedException ex) {
                Log.print(ex);
            }
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        stop();
    }

    public void paint(Graphics g) {
        int index = this.mode;
        if (index != NO_ICON) {
            g.drawImage(backgrounds[index], 0, 0, this);
            if (index >= FF2) {
                g.drawImage(icons[index], 451-429, 223-197, this);
            } else {
                g.drawImage(icons[index], 22, 35, this);
            }
        }
    }
}

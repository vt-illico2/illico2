package com.alticast.illico.pvr.ui;

import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.*;

import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.hn.content.IOStatus;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;

// PVR Disk space management
public class ManagementWizard extends ListOptionPanel {

    WizardRenderer renderer = new WizardRenderer();

    RecordingList list;

    public PvrFooter footer = new PvrFooter();

    public int step = 1;
    public static final int FINAL_STEP = 6;

    MenuItem menuNext = new MenuItem("wizard.Next step");
    MenuItem menuPrev = new MenuItem("wizard.Previous step");
    MenuItem menuFinal = new MenuItem("wizard.Final step");
    MenuItem menuJumpToFinal = new MenuItem("wizard.Jump to final step");
    MenuItem menuDeleteAll = new MenuItem("wizard.Delete all selected titles");
    MenuItem menuCancel = new MenuItem("wizard.Cancel");

    MenuItem menuSelectAll = new MenuItem("pvr.select_all");
    MenuItem menuUnselectAll = new MenuItem("pvr.unselect_all");

    MenuItem[][] buttonArray = {
        null,
        { menuSelectAll, menuNext, menuJumpToFinal, menuCancel },  // 1
        { menuSelectAll, menuNext, menuPrev, menuJumpToFinal, menuCancel },    // 2
        { menuSelectAll, menuNext, menuPrev, menuJumpToFinal, menuCancel },    // 3
        { menuSelectAll, menuNext, menuPrev, menuJumpToFinal, menuCancel },    // 4
        { menuSelectAll, menuFinal, menuPrev, menuCancel },    // 5
        { menuSelectAll, menuDeleteAll, menuPrev, menuCancel },    // 6
    };

    SortingOption[] sortingOptions = {
        null,
        new SizeSortingOption(),
        new LargestSeriesSortingOption(),
        new DateSortingOption(SortingOption.FLAT, true),
        new ExpirationSortingOption(true),
        new CompletedViewdSortingOption(true),
        new SizeSortingOption(),
    };

    public String subTitle;
    public String desc2;

    Stack stepCodes = new Stack();

    HashSet selectedRecordings;
    HashSet selectedFolder;

    private long selectedSize = 0;
    public int listSize = 0;
    public int checkedCount = 0;

    public ManagementWizard() {
        stack.push("pvr.PVR");
        stack.push("pvr.PVR_management");
        setRenderer(renderer);

        add(footer);
        footer.setBounds(0, 488, 906, 25);
    }

    public void start() {
        Log.printDebug(App.LOG_HEADER+"ManagementWizard.start()");
        footer.reset();
        footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");

        selectedSize = 0;
        selectedRecordings = new HashSet();
        selectedFolder = new HashSet();

        list = RecordingListManager.getInstance().getRecordedList();
        listSize = (list != null) ? list.size() : 0;

        setStep(1);
        focus = -1;
        super.start();
    }

    public void stop() {
        list = null;
        listData = null;
        hidePopup();
        super.stop();
    }

    public boolean handleKey(int code) {
        Log.printDebug(App.LOG_HEADER+"ManagementWizard.handleKey("+code+")");
        boolean toBeConsumed = (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code) || toBeConsumed;
        }
        switch (code) {
            case OCRcEvent.VK_LEFT:
                if (focus >= 0) {
                    focus = -1;
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (focus < 0 && buttons.size() > 0) {
                    focusDown();
                }
                break;
            case OCRcEvent.VK_UP:
                if (focus < 0) {
                    listData.keyUp();
                } else {
                    focusUp();
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (focus < 0) {
                    listData.keyDown();
                } else {
                    focusDown();
                }
                break;
            case OCRcEvent.VK_ENTER:
                if (focus < 0) {
                    clickEffect.start(((ListOptionPanelRenderer)renderer).getListFocusBounds(this));
                    selected(listData.getCurrent());
                } else {
                    clickEffect.start(((OptionPanelRenderer)renderer).getButtonFocusBounds(this));
                    selected(buttons.getItemAt(focus));
                }
                break;
            case KeyCodes.LAST:
                footer.clickAnimationByKeyCode(code);
                if (step == 1) {
                    MenuController.getInstance().showPreviousMenu();
                } else {
                    goToPrev();
                }
                break;
            case KeyCodes.COLOR_C:
                if (Log.EXTRA_ON) {
                    MetadataDiagScreen.getInstance().startRecording((AbstractRecording) listData.getCurrent(), false);
                }
                break;
            case KeyCodes.COLOR_B:
                if (Log.EXTRA_ON) {
                    test();
                }
                break;
            default:
                return toBeConsumed;
        }
        repaint();
        return true;
    }

    public void selected(MenuItem item) {
        if (item == menuNext) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : goToNext()");
            goToNext();
        } else if (item == menuPrev) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : goToPrev()");
            goToPrev();
        } else if (item == menuFinal || item == menuJumpToFinal) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : goToFinal()");
            goToFinal();
        } else if (item == menuDeleteAll) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : confirmDelete()");
            confirmDelete();
        } else if (item == menuCancel) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : confirmCancel()");
            confirmCancel();
        } else if (item == menuSelectAll) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : checkAll(true)");
            checkAll(true);
        } else if (item == menuUnselectAll) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.selected() : checkAll(false)");
            checkAll(false);
        }
    }

    public void selected(Entry entry) {
        setChecked(entry, !isChecked(entry));
    }

    public void canceled() {
    }

    private void goToNext() {
        stepCodes.push(new Integer(step));
        setStep(step + 1);
    }

    private void goToPrev() {
        int prevCode;
        try {
            prevCode = ((Integer) stepCodes.pop()).intValue();
        } catch (Exception ex) {
            prevCode = Math.max(step - 1, 1);
        }
        setStep(prevCode);
    }

    private void goToFinal() {
        stepCodes.push(new Integer(step));
        setStep(FINAL_STEP);
    }

    private synchronized void setStep(int newStep) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.setStep("+newStep+")");
        while (newStep <= FINAL_STEP) {
            listData = getListData(newStep);
            if (listData != null && listData.getCurrentList().size() > 0) {
                break;
            }
            newStep++;
        }
        step = Math.min(newStep, FINAL_STEP);
        buttons = new MenuItem("", buttonArray[step]);
        subTitle = dataCenter.getString("wizard.step" + step + "_title");
        desc2 = dataCenter.getString("wizard.step" + step + "_desc_line2");
        this.checkedCount = 0;
        Vector list = listData.getCurrentList();
        this.listSize = list.size();
        if (step == 2) {
            selectedFolder = new HashSet();
            for (int i = 0; i < listSize; i++) {
                RecordingFolder folder = (RecordingFolder) list.elementAt(i);
                if (processChecked(folder)) {
                    checkedCount++;
                    selectedFolder.add(folder.getTitle());
                }
            }
            if (listData.getCurrentList().size() > 0) {
                focus = -1;
            } else {
                focus = 0;
            }
        } else if (step == FINAL_STEP) {
            checkedCount = getSelectedCount();
            if (checkedCount > 0) {
                menuDeleteAll.setEnabled(true);
                focus = 0;
            } else {
                menuDeleteAll.setEnabled(false);
                focus = 2;
            }
        } else {
            for (int i = 0; i < listSize; i++) {
                AbstractRecording r = (AbstractRecording) list.elementAt(i);
                if (isChecked(r)) {
                    checkedCount++;
                }
            }
            if (listData.getCurrentList().size() > 0) {
                focus = -1;
            } else {
                focus = 0;
            }
        }
        if (step == FINAL_STEP && listSize <= 0) {
            buttons.removeItemAt(0);
        } else {
            buttons.setItemAt(checkedCount > 0 ? menuUnselectAll : menuSelectAll, 0);
            buttons.getItemAt(0).setEnabled(listSize > 0);
        }
    }

    private ListData getListData(int step) {
        RecordingList targetList = list;
        if (step == FINAL_STEP) {
            if (targetList != null) {
                targetList = targetList.filterRecordingList(new SelectedRecordingListFilter());
            }
        }
        SortingOption option = sortingOptions[step];
        if (option == null) {
            return null;
        }
        return new ListData(targetList, option);
    }

    private boolean processChecked(RecordingFolder folder) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.processChecked("+folder+")");
        Vector entries = folder.getEntries();
        int size = entries.size();
        for (int j = 0; j < size; j++) {
            if (!isChecked((Entry) entries.elementAt(j))) {
                return false;
            }
        }
        return true;
    }

    public boolean isChecked(Entry entry) {
        if (entry instanceof AbstractRecording) {
            return selectedRecordings.contains(((AbstractRecording) entry).getIdObject());
        } else if (entry instanceof RecordingFolder) {
            return selectedFolder.contains(entry.getTitle());
        }
        return false;
    }

    private void setChecked(Entry entry, boolean checked) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.setChecked("+entry+", "+checked+")");
        boolean ret;
        if (entry instanceof AbstractRecording) {
            ret = setChecked((AbstractRecording) entry, checked);
        } else if (entry instanceof RecordingFolder) {
            RecordingFolder folder = (RecordingFolder) entry;
            Vector entries = folder.getEntries();
            int size = entries.size();
            for (int j = 0; j < size; j++) {
                setChecked((AbstractRecording) entries.elementAt(j), checked);
            }
            if (checked) {
                ret = selectedFolder.add(folder.getTitle());
            } else {
                ret = selectedFolder.remove(folder.getTitle());
            }
        } else {
            return;
        }
        if (ret) {
            if (checked) {
                checkedCount++;
            } else {
                checkedCount--;
            }
            buttons.setItemAt(checkedCount > 0 ? menuUnselectAll : menuSelectAll, 0);
        }
    }

    private boolean setChecked(AbstractRecording r, boolean checked) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.setChecked("+r+", "+checked+")");
        boolean ret;
        if (checked) {
            ret = selectedRecordings.add(r.getIdObject());
            if (ret) {
                selectedSize += r.getRecordedSize();
            }
        } else {
            ret = selectedRecordings.remove(r.getIdObject());
            if (ret) {
                selectedSize -= r.getRecordedSize();
            }
        }
        if (ret && step == FINAL_STEP) {
            menuDeleteAll.setEnabled(getSelectedCount() > 0);
        }
        return ret;
    }

    private void checkAll(boolean check) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.checkAll("+check+")");
        Vector vector = listData.getCurrentList();
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            setChecked((Entry) vector.elementAt(i), check);
        }
        buttons.setItemAt(check ? menuUnselectAll : menuSelectAll, 0);
    }

    public int getSelectedCount() {
        return selectedRecordings.size();
    }

    public long getSelectedSize() {
        return Math.min(selectedSize, StorageInfoManager.getInstance().getAllocatedSpace());
    }

    private void confirmCancel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.confirmCancel()");
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString("wizard.quitWizard"));
        popup.setDescription(dataCenter.getString("wizard.cancelConfirm"));
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    MenuController.getInstance().showPreviousMenu();
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    public void test() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.test()");
        RecordingList targetList = list;
        if (targetList == null) {
            return;
        }
        targetList = targetList.filterRecordingList(new SelectedRecordingListFilter());
        if (targetList == null) {
            return;
        }
//        PVRManagement.freedBytes = selectedSize;
        int size = targetList.size();
        int failCount = 0;

        RecordingRequest notDeleted = null;
        long total = 0;
        for (int i = 0; i < size; i++) {
            RecordingRequest req = targetList.getRecordingRequest(i);
            long recSize = LocalRecording.getRecordedSize(req);
            Log.printDebug("size= " + recSize);
            total += recSize;
        }
        Log.printDebug("total= " + total);
    }

    private void confirmDelete() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.confirmDelete()");
        Vector vector = new Vector();
        RecordingList targetList = list;
        if (targetList != null) {
            targetList = targetList.filterRecordingList(new SelectedRecordingListFilter());
            if (targetList != null) {
                int size = targetList.size();
                for (int i = 0; i < size; i++) {
                    vector.addElement(targetList.getRecordingRequest(i));
                }
            }
        }
        final RecordingList list = targetList;

        if (ParentalControl.isProtected() || ParentalControl.isProtected(vector)) {
            PinEnablerListener pl = new PinEnablerListener() {
                public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                    if (response != PreferenceService.RESPONSE_SUCCESS) {
                        return;
                    }
                    ParentalControl.pinChecked = true;
                    showDeletePopup(list);
                }
            };
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.delete"), pl);
        } else {
            showDeletePopup(list);
        }
    }

    private void showDeletePopup(final RecordingList list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.showDeletePopup()");
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString("wizard.CONFIRM DELETION"));

        StringBuffer buf = new StringBuffer(200);

        int count = getSelectedCount();
        String str = dataCenter.getString("wizard.delConfirm1");
        str = TextUtil.replace(str, "%1", String.valueOf(count));
        str = TextUtil.replace(str, "%2", count > 1 ? "s" : "");
        buf.append(str);
        buf.append('\n');
        buf.append(TextUtil.replace(dataCenter.getString("wizard.delConfirm2"), "%%", StorageInfoManager.getSizeString2(getSelectedSize())));

        popup.setDescription(buf.toString());
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    new DeleteThread(list).start();
                    MenuController.getInstance().showPreviousMenu();
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    class DeleteThread extends Thread {
        RecordingList targetList;
        public DeleteThread(RecordingList list) {
            super("PVRWizard.delete");
            targetList = list;
        }

        public void run() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.DeleteThread.run()");
            if (targetList == null) {
                return;
            }
            int size = targetList.size();
            int failCount = 0;

            RecordingRequest notDeleted = null;
            for (int i = 0; i < size; i++) {
                try {
                    RecordingRequest req = targetList.getRecordingRequest(i);
                    boolean using = false;
                    if (App.SUPPORT_HN) {
                        try {
                            using = ((IOStatus) req).isInUse();
                        } catch (Exception ex) {
                        }
                    }
                    if (using) {
                        failCount++;
                        notDeleted = req;
                    } else {
                        PVRManagement.freedBytes += LocalRecording.getRecordedSize(req);
                        try {
                            req.delete();
                            PvrVbmController.getInstance().writeRecordingDeleted(req, PvrVbmController.MID_DELETE_FROM_FREE_UP);
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                    FullScreenPanel comp = MenuController.getInstance().getCurrentScene();
                    if (comp != null && comp instanceof PVRManagement) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManagementWizard.DeleteThread.run() : FullScreenPanel.refresh()");
                        comp.refresh();
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (failCount == 1) {
                Log.printDebug(App.LOG_HEADER+"ManagementWizard.DeleteThread.run() : Call showCannotDeletePopup()--1");
                PopupController.showCannotDeletePopup(notDeleted, PopupController.getInstance());
            } else if (failCount > 0) {
                Log.printDebug(App.LOG_HEADER+"ManagementWizard.DeleteThread.run() : Call showCannotDeletePopup()--2");
                PopupController.showCannotDeletePopup(failCount, PopupController.getInstance());
            }
        }
    }

    class SelectedRecordingListFilter extends RecordingListFilter {
        public boolean accept(RecordingRequest req) {
            try {
                return selectedRecordings.contains(new Integer(req.getId()));
            } catch (Exception ex) {
                return false;
            }
        }
    }

}

package com.alticast.illico.pvr.ui;

import java.awt.event.KeyEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;

// PVR Disk space management
public class PVRManagement extends OptionPanel {
    private static final long serialVersionUID = 1L;

    PVRManagementRenderer renderer = new PVRManagementRenderer();
    /** D-Option instance. */
    private OptionScreen optScr;

    public PvrFooter footer;

    public static long freedBytes = 0;
    public int recordingCount = 0;

    MenuItem menuWizard = new MenuItem("management.btnWIZARD");
    MenuItem menuList = new MenuItem("management.GoRecordedList");

    public PVRManagement() {
        stack.push("pvr.PVR");
        buttons = new MenuItem("", new MenuItem[] { menuWizard, menuList });
        setRenderer(renderer);
        if (optScr == null) {
            optScr = new OptionScreen();
        }
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRManagement.start()");
        if (footer == null) {
            footer = new PvrFooter();
            footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
            footer.addButton(PreferenceService.BTN_D, "pvr.Options");
            add(footer);
            footer.setBounds(0, 488, 906, 25);
        }
        recordingCount = RecordingListManager.getInstance().getRecordedList().size();
        if (recordingCount > 0) {
            menuWizard.setEnabled(true);
            setFocus(0);
        } else {
            menuWizard.setEnabled(false);
            setFocus(1);
        }
        super.start();
        this.setVisible(true);
        repaint();
    }

    public void refresh() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRManagement.refresh()");
        recordingCount = RecordingListManager.getInstance().getRecordedList().size();
        if (recordingCount > 0) {
            menuWizard.setEnabled(true);
        } else {
            menuWizard.setEnabled(false);
            setFocus(1);
        }
        super.refresh();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRManagement.stop()");
        if (footer != null) {
            remove(footer);
            footer = null;
        }
        if (optScr != null) {
            optScr.stop();
        }
        setVisible(false);
        freedBytes = 0;
    }

    public void setFocus(int f) {
        if (f != 0 || menuWizard.isEnabled()) {
            super.setFocus(f);
        }
    }

    public boolean handleKey(int type) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRManagement.handleKey("+type+")");
        boolean toBeConsumed = (type >= KeyEvent.VK_0 && type <= KeyEvent.VK_9);
        switch (type) {
        case KeyEvent.VK_UP:
            setFocus(0);
            repaint();
            break;
        case KeyEvent.VK_DOWN:
            setFocus(1);
            repaint();
            break;
        case KeyEvent.VK_ENTER:
            clickEffect.start(((OptionPanelRenderer)renderer).getButtonFocusBounds(this));
            switch (focus) {
            case 0: // start to wizard
                MenuController.getInstance().showMenu(PvrService.MENU_WIZARD);
                break;
            case 1: // goto recorded list
                MenuController.getInstance().showMenu(PvrService.MENU_RECORDING_LIST);
                break;
            case 2: // help
                break;
            }
            break;
        case KeyCodes.COLOR_D:
            footer.clickAnimationByKeyCode(type);
            MenuItem root = new MenuItem("D-Option");
            if (RecordingListManager.getInstance().getRecordedList().size() > 0) {
                root.add(MenuItems.GO_WIZARD);
            }
            root.add(MenuItems.GO_RECORDED);
            if (!Core.standAloneMode) {
                root.add(MenuItems.GO_HELP);
            }
            optScr.start(root, this);
            break;
        case KeyCodes.LAST:
            footer.clickAnimationByKeyCode(type);
            MenuController.getInstance().showPreviousMenu();
            break;
        case KeyCodes.SEARCH:
            optScr.stop();
            return toBeConsumed;
        default:
            return toBeConsumed;
        }
        return true;
    }

    public void canceled() {
    }

    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRManagement.selected("+item+")");
        if (optScr != null) {
            optScr.stop();
        }
        Core.getInstance().selected(item);
    }
}

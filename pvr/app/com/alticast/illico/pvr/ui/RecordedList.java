package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.*;
import java.util.*;
import javax.tv.util.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;

/**
 * Recorded List.
 *
 * @author  June Park
 */
public class RecordedList extends ListPanel implements DeviceListListener {

    MenuItem buttonEmpty = new MenuItem("");

    MenuItem buttonUnblock = new MenuItem("", new MenuItem[] {
        MenuItems.UNBLOCK_RECORDING
    });

    //->Kenneth[2017.3.14] R7.3
    // Recording 진행중인 NOT_YET_VIEWED 
    MenuItem buttonPlayStopOptions = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.STOP, MenuItems.MORE_OPTIONS
    });
    // Recording 진행중인 COMPLETELY_VIEWED
    MenuItem buttonReplayStopOptions = new MenuItem("", new MenuItem[] {
        MenuItems.REPLAY, MenuItems.STOP, MenuItems.MORE_OPTIONS
    });
    // Recording 진행중인 PARTIALLY_VIEWED
    MenuItem buttonResumeStopOptions = new MenuItem("", new MenuItem[] {
        MenuItems.RESUME_VIEWING, MenuItems.STOP, MenuItems.MORE_OPTIONS
    });
    // Recording 완료된 NOT_YET_VIEWED
    MenuItem buttonPlayDeleteOptions = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.DELETE, MenuItems.MORE_OPTIONS
    });
    // Recording 완료된 COMPLETELY_VIEWED
    MenuItem buttonReplayDeleteOptions = new MenuItem("", new MenuItem[] {
        MenuItems.REPLAY, MenuItems.DELETE, MenuItems.MORE_OPTIONS
    });
    // Recording 완료된 PARTIALLY_VIEWED 
    MenuItem buttonResumeDeleteOptions = new MenuItem("", new MenuItem[] {
        MenuItems.RESUME_VIEWING, MenuItems.DELETE, MenuItems.MORE_OPTIONS
    });
    /*
    MenuItem buttonPlayStopEditMore = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.EDIT, MenuItems.STOP, MenuItems.MORE_DETAILS
    });
    MenuItem buttonPlayStopMore = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.STOP, MenuItems.MORE_DETAILS
    });
    MenuItem buttonPlayEditDeleteMore = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.EDIT, MenuItems.DELETE, MenuItems.MORE_DETAILS
    });
    MenuItem buttonPlayDeleteMore = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.DELETE, MenuItems.MORE_DETAILS
    });
    */
    //<-
    MenuItem buttonViewDelete = new MenuItem("", new MenuItem[] {
        MenuItems.VIEW_EPISODES, MenuItems.DELETE
    });
    MenuItem buttonBackDelete = new MenuItem("", new MenuItem[] {
        MenuItems.BACK, MenuItems.DELETE
    });

    public static SortingOption SORT_TITLE = new TitleSortingOption(false);
    public static SortingOption SORT_DATE = new DateSortingOption(SortingOption.TREE, false);

    public static SortingOption SORT_TERMINAL = new TerminalSortingOption(SORT_TITLE);

    public static SortingOption SORT_GENRE = new GenreSortingOption();
    public static SortingOption SORT_PROGRAM_LENGTH = new DurationSortingOption();
    public static SortingOption SORT_SIZE = new SizeSortingOption();
    public static SortingOption SORT_SAVETIME = new ExpirationSortingOption(true);

    public static SortingOption SORT_NOT_YET = new NotYetViewedSortingOption();
    public static SortingOption SORT_PARTIALLY = new PartiallyViewedSortingOption();
    public static SortingOption SORT_COMPLETELY = new CompletedViewdSortingOption();

    public static final MenuItem SORT_VIEWED_NUMBER = new MenuItem("sort.By viewed number", new MenuItem[] {
        SORT_NOT_YET, SORT_PARTIALLY, SORT_COMPLETELY
    });

    private MenuItem menuSortLocal = new MenuItem("option.Sort", new MenuItem[] {
        SORT_TITLE, SORT_DATE, SORT_GENRE, SORT_PROGRAM_LENGTH, SORT_SIZE, SORT_SAVETIME, SORT_VIEWED_NUMBER
    });

    private MenuItem menuSortRemote = new MenuItem("option.Sort", new MenuItem[] {
        SORT_TERMINAL, SORT_TITLE, SORT_DATE, SORT_GENRE, SORT_PROGRAM_LENGTH, SORT_SIZE, SORT_SAVETIME, SORT_VIEWED_NUMBER
    });

    private static RecordedList instance = new RecordedList();
    public static RecordedList getInstance() {
        return instance;
    }

    private RecordedList() {
        super(RECORDED_LIST, new ListUIFilter(RecordingListManager.playableFilter));
        if (App.SUPPORT_HN) {
            HomeNetManager.getInstance().addDeviceListListener(this);
        }
    }

    protected Vector getDeviceList() {
        return HomeNetManager.getInstance().getDeviceList();
    }

    public void deviceListChanged(Vector deviceList) {
        updateWorker.pushRemoteUpdateJob();
    }

    protected SortingOption getDefaultSortingOption() {
        return SORT_TITLE;
    }

    protected ListPanelRenderer createRenderer() {
        return new RecordedListRenderer();
    }

    protected MenuItem getSortingMenu() {
        if (listData == null || HomeNetManager.getInstance().getDeviceCount() <= 0) {
            return menuSortLocal;
        } else {
            return menuSortRemote;
        }
    }

    public MenuItem createButtons(Entry entry) {
        MenuItem newButtons = createButtonsImpl(entry);
        if (Core.standAloneMode) {
            int index = newButtons.indexOf(MenuItems.EDIT);
            if (index >= 0) {
                newButtons.removeItemAt(index);
            }
        }
        return newButtons;
    }

    //->Kenneth[2017.3.22] R7.3 : repeated recordings 의 경우는 More Details 타이틀이 보여야 하고
    // single recording 의 경우는 More Options 타이틀이 보여야 함
    private MenuItem changeMoreOptionsTitle(MenuItem buttons, String optionsTitle) {
        int index = buttons.indexOf(MenuItems.MORE_OPTIONS);
        if (index >= 0) {
            buttons.getItemAt(index).setDisplayableText(optionsTitle);
        }
        return buttons;
    }
    //<-

    private MenuItem createButtonsImpl(Entry entry) {
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            if (ParentalControl.isAccessBlocked(rec)) {
                return buttonUnblock;
            }
            short stateGroup = rec.getStateGroup();
            //->Kenneth[2017.3.14] R7.3 : Viewing Status 따라 button 이 세분화 
            // 먼저 MenuItem 에 icon 을 추가한다.
            MenuItems.PLAY.focusedIcon = DataCenter.getInstance().getImage("icon_rec_play_f.png");
            MenuItems.PLAY.unfocusedIcon = DataCenter.getInstance().getImage("icon_rec_play.png");
            MenuItems.REPLAY.focusedIcon = DataCenter.getInstance().getImage("icon_rec_replay_f.png");
            MenuItems.REPLAY.unfocusedIcon = DataCenter.getInstance().getImage("icon_rec_replay.png");
            MenuItems.RESUME_VIEWING.focusedIcon = DataCenter.getInstance().getImage("icon_rec_resume_f.png");
            MenuItems.RESUME_VIEWING.unfocusedIcon = DataCenter.getInstance().getImage("icon_rec_resume.png");

            int viewingState = rec.getViewingState();
            boolean single = rec.getGroupKey() == null;

            String moreDetails = DataCenter.getInstance().getString("List.More Details");
            if (stateGroup == RecordingListManager.IN_PROGRESS) {
                // 녹화 진행중
                if (single) {
                    // 싱글 레코딩
                    if (viewingState == AbstractRecording.NOT_YET_VIEWED) {
                        return changeMoreOptionsTitle(buttonPlayStopOptions, null);
                    } else if (viewingState == AbstractRecording.PARTIALLY_VIEWED) {
                        return changeMoreOptionsTitle(buttonResumeStopOptions, null);
                    } else if (viewingState == AbstractRecording.COMPLETELY_VIEWED) {
                        return changeMoreOptionsTitle(buttonReplayStopOptions, null);
                    }
                } else {
                    // Series 나 Manual 레코딩
                    if (viewingState == AbstractRecording.NOT_YET_VIEWED) {
                        return changeMoreOptionsTitle(buttonPlayStopOptions, moreDetails);
                    } else if (viewingState == AbstractRecording.PARTIALLY_VIEWED) {
                        return changeMoreOptionsTitle(buttonResumeStopOptions, moreDetails);
                    } else if (viewingState == AbstractRecording.COMPLETELY_VIEWED) {
                        return changeMoreOptionsTitle(buttonReplayStopOptions, moreDetails);
                    }
                }
            } else {
                // 녹화 완료됨
                if (single) {
                    // 싱글 레코딩
                    if (viewingState == AbstractRecording.NOT_YET_VIEWED) {
                        return changeMoreOptionsTitle(buttonPlayDeleteOptions, null);
                    } else if (viewingState == AbstractRecording.PARTIALLY_VIEWED) {
                        return changeMoreOptionsTitle(buttonResumeDeleteOptions, null);
                    } else if (viewingState == AbstractRecording.COMPLETELY_VIEWED) {
                        return changeMoreOptionsTitle(buttonReplayDeleteOptions, null);
                    }
                } else {
                    // Series 나 Manual 레코딩
                    if (viewingState == AbstractRecording.NOT_YET_VIEWED) {
                        return changeMoreOptionsTitle(buttonPlayDeleteOptions, moreDetails);
                    } else if (viewingState == AbstractRecording.PARTIALLY_VIEWED) {
                        return changeMoreOptionsTitle(buttonResumeDeleteOptions, moreDetails);
                    } else if (viewingState == AbstractRecording.COMPLETELY_VIEWED) {
                        return changeMoreOptionsTitle(buttonReplayDeleteOptions, moreDetails);
                    }
                }
            }
            /*
            boolean single = rec.getGroupKey() == null;
            if (stateGroup == RecordingListManager.IN_PROGRESS) {
                return single && rec instanceof LocalRecording ? buttonPlayStopEditMore : buttonPlayStopMore;
            } else {
                return single && rec instanceof LocalRecording ? buttonPlayEditDeleteMore : buttonPlayDeleteMore;
            }
            */
            //<-
        } else if (entry instanceof RecordingFolder) {
            RecordingFolder folder = (RecordingFolder) entry;
            if (ParentalControl.isAccessBlocked(folder)) {
                return buttonUnblock;
            }
            return listData.getCurrentFolder() == null ? buttonViewDelete : buttonBackDelete;
        } else {
            return buttonEmpty;
        }
        return buttonEmpty;
    }

    public MenuItem createOptions(Entry entry) {
        MenuItem root = new MenuItem("ListOptions");

        if (listData != null && listData.getCompletedRecordingCount() > 0) {
            root.add(MenuItems.MULTI_DELETE);
        }

        if (Core.standAloneMode) {
            root.add(MenuItems.GO_MANAGEMENT);
        } else {
            if (Environment.SUPPORT_DVR) {
                root.add(MenuItems.GO_UPCOMING);
                root.add(MenuItems.GO_MANUAL);
                root.add(MenuItems.GO_MANAGEMENT);
            }
            root.add(getSortingMenu());
            //->Kenneth[2017.3.16] R7.3 : Search 를 D-option 에 추가
            root.add(MenuItems.SEARCH);
            //<-
            if (Environment.SUPPORT_DVR) {
                root.add(MenuItems.GO_PREFERENCES);
            }
            if (entry instanceof LocalRecording) {
                if (((AbstractRecording) entry).isBlocked()) {
                    root.add(MenuItems.UNBLOCK_MANUALLY);
                } else {
                    root.add(MenuItems.BLOCK_MANUALLY);
                }
            } else if (entry instanceof RecordingFolder) {
                RecordingFolder rf = (RecordingFolder) entry;
                if (rf.type == RecordingFolder.LOCAL_ONLY) {
                    if (rf.isBlocked()) {
                        root.add(MenuItems.UNBLOCK_MANUALLY);
                    } else {
                        root.add(MenuItems.BLOCK_MANUALLY);
                    }
                }
            }
            root.add(MenuItems.GO_HELP);
            root.add(MenuItems.getParentalControlMenu());
        }
        return root;
    }

    // ListPanel 에 있는 놈을 여기서 override 한다.
    // 필요한 버튼만 처리하고 나머지 동일한 것들은 super.selected() 를 호출하게 한다.
    public void selected(MenuItem item) {
        Log.printDebug(App.LOG_HEADER+"RecordedList.selected("+item+")");
        Entry entry = listData.getCurrent();
        if (item == MenuItems.MORE_OPTIONS) {
            if (entry instanceof AbstractRecording) {
                Log.printDebug(App.LOG_HEADER+"RecordedList.selected() : MORE_OPTIONS : Call showMoreOptionsPopup()");
                showMoreOptionsPopup((AbstractRecording)entry);
            }
            return;
        } else if (item == MenuItems.REPLAY) {
            Log.printDebug(App.LOG_HEADER+"RecordedList.selected() : REPLAY");
            if (entry instanceof AbstractRecording) {
                processPlay((AbstractRecording) entry);
            }
            return;
        } else if (item == MenuItems.RESUME_VIEWING) {
            Log.printDebug(App.LOG_HEADER+"RecordedList.selected() : RESUME_VIEWING");
            if (entry instanceof AbstractRecording) {
                processPlay((AbstractRecording) entry);
            }
            return;
        }
        super.selected(item);
    }
}

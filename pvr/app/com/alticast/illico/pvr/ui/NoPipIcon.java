package com.alticast.illico.pvr.ui;

import java.awt.*;
import javax.tv.util.*;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.*;
import com.alticast.ui.*;

/**
 * RecordingIcon
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/09 20:41:16 $
 * @author  June Park
 */
public class NoPipIcon extends Component implements TVTimerWentOffListener {

    private static final long DEFAULT_AUTO_CLOSE_DELAY = 5000L;
    private static final Rectangle BOUNDS = new Rectangle(60, 48, 50, 50);

    private Image image = DataCenter.getInstance().getImage("no_action.png");

    private LayeredUI ui;

    private TVTimerSpec autoCloseTimer;

    private static NoPipIcon instance = new NoPipIcon();

    public static NoPipIcon getInstance() {
        return instance;
    }

    private NoPipIcon() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setTime(DEFAULT_AUTO_CLOSE_DELAY);
        autoCloseTimer.setRepeat(false);
        autoCloseTimer.setAbsolute(false);
        autoCloseTimer.addTVTimerWentOffListener(this);

    }

    public void paint(Graphics g) {
        g.drawImage(image, 0, 0, this);
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NoPipIcon.start()");
        if (ui != null) {
            return;
        }
        LayeredWindow window = new LayeredWindow();
        window.setSize(BOUNDS.width, BOUNDS.height);
        window.add(this);
        window.setVisible(true);
        this.setSize(BOUNDS.width, BOUNDS.height);
        this.setVisible(true);

        ui = WindowProperty.PVR_POPUP.createLayeredWindow(window, BOUNDS);
        ui.activate();
        startAutoCloseTimer();
    }

    public synchronized void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"NoPipIcon.stop()");
        if (ui != null) {
            ui.deactivate();
            WindowProperty.dispose(ui);
            ui = null;
        }
    }

    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(ui);
    }

    /** 자동으로 닫는 timer를 구동 */
    public synchronized void startAutoCloseTimer() {
        try {
            // 기존 타이머 제거 후 add
            TVTimer.getTimer().deschedule(autoCloseTimer);
            autoCloseTimer = TVTimer.getTimer().scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        stop();
    }

}

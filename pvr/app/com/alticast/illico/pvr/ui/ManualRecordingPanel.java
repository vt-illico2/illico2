package com.alticast.illico.pvr.ui;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.ui.DVBColor;
import org.ocap.shared.dvr.RecordingChangedEvent;
import org.ocap.shared.dvr.navigation.RecordingList;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.comp.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.issue.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.MenuItem;
import org.ocap.shared.dvr.*;
import com.videotron.tvi.illico.pvr.communication.*;

public class ManualRecordingPanel extends OptionPanel implements OptionSelectionListener {

    MenuItem menuRecord = new MenuItem("pvr.Record");
    MenuItem menuCancel = new MenuItem("pvr.Cancel");

    long start;
    long end;

    ChannelEditField channelField;
    DateEditField dateField;
    TimeEditField startField;
    TimeEditField endField;
    RepeatEditField repeatField;
    OptionEditField saveField;
    StringInputField nameField;

    EditField[] fields;

    int editFocus;

    PvrFooter footer;

    RecordingOption currentOption;

    ManualRecordingPanelRenderer panelRenderer = new ManualRecordingPanelRenderer();

    RecordingRequest toBeChecked;

    public ManualRecordingPanel() {
        stack.push("pvr.PVR");
        buttons = new MenuItem("", new MenuItem[] { menuRecord, menuCancel });
        footer = new PvrFooter();
        footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
        add(footer);
        footer.setBounds(0, 488, 906, 25);

        EditingComponent comp = new EditingComponent();
        this.add(comp, 0);

        setRenderer(panelRenderer);
        setFont(GraphicsResource.BLENDER17);
        setForeground(GraphicsResource.C_182_182_182);
    }

    public void resetSaveOption(boolean repeat) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.resetSaveOption("+repeat+")");
        saveField.setOptions(repeat ? PvrUtil.SAVE_SERIES : PvrUtil.SAVE_RECORDING);
        if (repeat) {
            int count = PvrUtil.getDefaultKeepCount();
            if (count == 3) {
                saveField.setSelectedKey(PvrUtil.LAST_3);
            } else if (count == 5) {
                saveField.setSelectedKey(PvrUtil.LAST_5);
            } else {
                saveField.setSelectedKey(PvrUtil.LAST_MAX);
            }
        } else {
            long exp = PvrUtil.getDefaultExpirationPeriod();
            if (exp == 7 * Constants.MS_PER_DAY) {
                saveField.setSelectedKey(PvrUtil.SAVE_7);
            } else if (exp == 14 * Constants.MS_PER_DAY) {
                saveField.setSelectedKey(PvrUtil.SAVE_14);
            } else {
                saveField.setSelectedKey(PvrUtil.SAVE_MAX);
            }
        }
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.start()");
        toBeChecked = null;
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int min = c.get(Calendar.MINUTE);
        if (min < 30) {
            c.set(Calendar.MINUTE, 30);
        } else {
            c.set(Calendar.MINUTE, 0);
            c.add(Calendar.HOUR_OF_DAY, 1);
        }
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        start = c.getTimeInMillis();
        end = start + Constants.MS_PER_HOUR;

        boolean hour24 = Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage());

        //->Kenneth[2015.5.23] : timer 를 사용함에 따라서 릭을 방지하기 위해 지워주는 코드 넣음.
        if (channelField != null) {
            channelField.removeInputTimerListener();
        }
        //->Kenneth[2015.5.23] : repaint 를 하기 위해서 this 를 넘겨줌
        channelField = new ChannelEditField(dataCenter.getString("manual.Channel"), this);
        dateField = new DateEditField(dataCenter.getString("pvr.Date"), start);
        startField = new TimeEditField(dataCenter.getString("pvr.Start"), true, hour24);
        endField = new TimeEditField(dataCenter.getString("pvr.End"), true, hour24);
        repeatField = new RepeatEditField(dataCenter.getString("pvr.Repeat"), true, false, true, null);
        saveField = new OptionEditField(dataCenter.getString("pvr.Save"));
        nameField = new StringInputField(dataCenter.getString("manual.recordingName"));

        nameField.setCurrentValue(dataCenter.getString("manual.ManualRecording"));

        dateField.setListener(this);
        repeatField.setListener(this);

        resetSaveOption(false);
//        saveField.setInitialValue(r.getSaveText());

        Vector list = new Vector();
        list.add(channelField);
        list.add(dateField);
        list.add(startField);
        list.add(endField);
        list.add(repeatField);
        list.add(saveField);
        list.add(nameField);

        fields = new EditField[list.size()];
        list.copyInto(fields);

        startField.setTime(start);
        endField.setTime(end);

        editFocus = -1;
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].isEditable) {
                editFocus = i;
                break;
            }
        }
        focus = -1;
        super.start();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.stop()");
        toBeChecked = null;
        nameField.closeVirtualKeyboard();
        super.stop();
    }

    public void showCalendar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.showCalendar()");
        final ITVCalendar popup = new ITVCalendar();
        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int editFocus) {
                dateField.setDay(popup.getSelectedDay());
            }
        };
        popup.setListener(popupListener);
        this.add(popup);
        showPopup(popup);
    }

    public void showDaysPopup() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.showDaysPopup()");
        final SelectDaysPopup popup = new SelectDaysPopup(repeatField.getCurrentDays());
        popup.setPosition(428, 104);
        this.add(popup);
        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int editFocus) {
                repeatField.setDays(popup.getDays());
            }
        };
        popup.setListener(popupListener);
        showPopup(popup);
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.handleKey("+code+")");
        boolean toBeConsumed = (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code) || toBeConsumed;
        }
        if (editFocus >= 0) {
            boolean ret = fields[editFocus].handleKey(code);
            if (ret) {
                repaint();
                return true;
            }
            if (code == OCRcEvent.VK_RIGHT) {
                focus = 0;
                editFocus = -editFocus - 1;
                repaint();
                return true;
            }
        }
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (focus >= 0) {
                    clickEffect.start(((OptionPanelRenderer)renderer).getButtonFocusBounds(this));
                    selected(buttons.getItemAt(focus));
                } else if (editFocus >= 0) {
                    if (fields[editFocus] == repeatField && repeatField.focus == 1) {
                        showDaysPopup();
                    }
                }
                break;
            case OCRcEvent.VK_UP:
                if (focus >= 0) {
                    if (focus > 0) {
                        focus--;
                    }
                } else {
                    keyUp();
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (focus >= 0) {
                    if (focus < buttons.size() - 1) {
                        focus++;
                    }
                } else {
                    keyDown();
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (editFocus < 0) {
                    editFocus = -editFocus - 1;
                    focus = -1;
                }
                break;
            case OCRcEvent.VK_LAST:
                footer.clickAnimationByKeyCode(code);
                MenuController.getInstance().showPreviousMenu();
//                close();
                break;
            case KeyCodes.SEARCH:
                if (editFocus >= 0 && fields[editFocus] == nameField) {
                    nameField.closeVirtualKeyboard();
                }
                return false;
            default:
                return toBeConsumed;
        }
        repaint();
        return true;
    }

    private void keyUp() {
        EditField last = editFocus >= 0 ? fields[editFocus] : null;
        int i = (editFocus < 0) ? fields.length - 1 : editFocus - 1;
        for (; i >= 0; i--) {
            if (fields[i].isEditable) {
                editFocus = i;
                if (last != null && last instanceof TimeEditField && fields[i] instanceof TimeEditField) {
                    fields[i].resetFocus(last.focus);
                } else {
                    fields[i].resetFocus(0);
                }
                return;
            }
        }
    }

    private void keyDown() {
        if (editFocus < 0 && editFocus >= fields.length - 1) {
            return;
        }
        EditField last = fields[editFocus];
        int i = editFocus + 1;
        for (; i < fields.length; i++) {
            if (fields[i].isEditable) {
                editFocus = i;
                if (last != null && last instanceof TimeEditField && fields[i] instanceof TimeEditField) {
                    fields[i].resetFocus(last.focus);
                } else {
                    fields[i].resetFocus(0);
                }
                return;
            }
        }
    }

    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.selected()");
        if (item == menuCancel) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.selected() : showMenu(MENU_PVR_PORTAL)");
            MenuController.getInstance().showMenu(PvrService.MENU_PVR_PORTAL);
        } else if (item == menuRecord) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.selected() : processRecord()");
            processRecord();
        }
    }

    public void canceled() {
    }


    public void selectionChanged(Object key) {
        if (key.equals(PvrUtil.MORE_DAYS)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.selectionChanged() : showCalendar()");
            showCalendar();
        } else {
            boolean repeat = !RepeatEditField.KEY_NO.equals(key);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.selectionChanged() : resetSaveOption()");
            resetSaveOption(repeat);
        }
    }

    private void processRecord() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord()");
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(startField.getTime()));
        int sh = cal.get(Calendar.HOUR_OF_DAY);
        int sm = cal.get(Calendar.MINUTE);
        cal.setTime(new Date(endField.getTime()));
        int eh = cal.get(Calendar.HOUR_OF_DAY);
        int em = cal.get(Calendar.MINUTE);

        cal.setTime(new Date(dateField.getDay()));
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.HOUR_OF_DAY, sh);
        cal.set(Calendar.MINUTE, sm);
        long newStart = cal.getTimeInMillis();
        cal.set(Calendar.HOUR_OF_DAY, eh);
        cal.set(Calendar.MINUTE, em);
        long newEnd = cal.getTimeInMillis();
        if (newEnd <= newStart || newEnd <= System.currentTimeMillis()) {
            newEnd = newEnd + Constants.MS_PER_DAY;
        }

        String repeatString = repeatField.getRepeatString();
        String saveValue = (String) saveField.getSelectedValue();
        String titleValue = (String) nameField.getChangedValue();
        final String title;
        if (titleValue == null || titleValue.trim().length() == 0) {
            title = dataCenter.getString("manual.ManualRecording");
        } else {
            title = titleValue;
        }

        int number = 0;
        TvChannel ch = null;
        try {
            number = ((Integer) channelField.getChangedValue()).intValue();
            ch = Core.epgService.getChannelByNumber(number);
        } catch (Exception ex) {
            Log.print(ex);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: channel = " + ch);
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: start = " + new Date(newStart));
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: end   = " + new Date(newEnd));
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: repeatString = " + repeatString);
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: saveValue    = " + saveValue);
            Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: title   = " + title);
            try {
                if (ch != null) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord: definition = " + ch.getDefinition());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (ch == null) {
            InfoOneButtonPopup opp = new InfoOneButtonPopup(dataCenter.getString("pvr.channel_is_invalid"));
            opp.setDescription(dataCenter.getString("pvr.channel_is_invalid"));
            showPopup(opp);
            return;
        }

        //->Kenneth[2015.2.7] 4K
        //->Kenneth[2015.6.24] DDC-107
        if (Core.needToShowIncompatibilityPopup(ch, false)) {
        //if (Core.needToShowIncompatibilityPopup(ch)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord() : Call Core.showIncompatibilityErrorMessage()");
            Core.showIncompatibilityErrorMessage();
            return;
        }

        final RecordingOption option = new RecordingOption(ch, newStart, newEnd);
        option.creationMethod = AppData.BY_USER;
        this.currentOption = option;
        option.title = title;
        if (repeatString == null) {
            option.all = false;
            option.type = RecordingOption.TYPE_SINGLE;
            long exp = Long.MAX_VALUE;;
            if (PvrUtil.SAVE_14.equals(saveValue)) {
                exp = Constants.MS_PER_DAY * 14;
            } else if (PvrUtil.SAVE_7.equals(saveValue)) {
                exp = Constants.MS_PER_DAY * 7;
            }
            option.saveUntil = exp;
        } else {
            option.all = true;
            option.type = RecordingOption.TYPE_REPEAT;
            option.setRepeatString(repeatString);
            int keepOption = 0;
            if (PvrUtil.LAST_3.equals(saveValue)) {
                keepOption = 3;
            } else if (PvrUtil.LAST_5.equals(saveValue)) {
                keepOption = 5;
            }
            option.keepOption = keepOption;
        }

        final int chNumber = number;
        ManualConfirmPopup popup = new ManualConfirmPopup(option, repeatString, saveValue);

        class ManualPopupAdapter extends PopupAdapter implements Runnable {

            public void selected(int focus) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord().ManualPopupAdapter.selected("+focus+")");
                if (focus == 0) {
                    new Thread(ManualPopupAdapter.this, "ManualRecording").start();
                }
            }

            public void run() {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.processRecord().ManualPopupAdapter.run()");
                RecordingRequest req = RecordManager.getInstance().record(option);
                int state = LeafRecordingRequest.DELETED_STATE;
                try {
                    if (req != null) {
                        state = req.getState();
                    }
                } catch (Exception ex) {
                }
                boolean ok = true;
                switch (state) {
                    case LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                    case LeafRecordingRequest.FAILED_STATE:
                    case LeafRecordingRequest.DELETED_STATE:
                        ok = false;
                        break;
                    case LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                        int reason = AppData.updateFailedReason(req);
                        if (reason == RecordingFailedException.INSUFFICIENT_RESOURCES) {
                            ok = false;
                        }
                        break;

                }
                if (ok) {
                    RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(req);
                    int size = list == null ? -1 : list.size();
                    Log.printInfo(App.LOG_HEADER+"ManualRecordingPanel.processRecord: ov size = " + size);
                    if (Log.DEBUG_ON) {
                        if (list != null) {
                            for (int i = 0; i < size; i++) {
                                AppData.printAppData(list.getRecordingRequest(i));
                            }
                        }
                    }
                    ok = size < Environment.TUNER_COUNT;
                }
                //->Kenneth[2015.3.19] 4K : 아래의 showFinishPopup 이 불려서 팝업이 뜨고 그 위에
                //ManyConflictCautionPopup 이 뜨는 현상 발생. 그래서 ok 에 대한 체크를 더해준다.
                if (ok) {
                    boolean uhdConflict = IssueManager.getInstance().hasMaxUhdConflict(req);
                    if (uhdConflict) ok = false;
                }
                Log.printInfo(App.LOG_HEADER+"ManualRecordingPanel.processRecord: ok = " + ok);
                if (ok) {
                    showFinishPopup(req, title, chNumber);
                } else {
                    toBeChecked = req;
                }
                Log.printInfo(App.LOG_HEADER+"ManualRecordingPanel.processRecord: state = " + state);
            }
        };
        popup.setListener(new ManualPopupAdapter());
        showPopup(popup);
    }

    private void showFinishPopup(final RecordingRequest req, String title, int number) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.showFinishPopup()");
        toBeChecked = null;
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString("manual.confTitle"));
        popup.setButton("manual.Done", "manual.Record more");

        String msgKey = App.SHOW_MANUAL_RECORINGS_IN_GUIDE ? "manual.confMessage_R3" : "manual.confMessage_R2";
        String msg = TextUtil.replace(dataCenter.getString(msgKey), "@", String.valueOf(number));
        msg = TextUtil.replace(msg, "|", "\n");
        popup.setDescription(msg);
        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus != 1) {
                    MenuController.getInstance().showMainMenu();
                    RecordManager.getInstance().checkDiskSpace(req);
                } else {
                    start();
                }
            }
        };
        popup.setListener(popupListener);
        showPopup(popup);
        PvrVbmController.getInstance().writeUpcomingRecording(req, currentOption);
    }

    private boolean isVisibleEditingPopup() {
        return (currentPopup != null) && (currentPopup instanceof ITVCalendar || currentPopup instanceof SelectDaysPopup);
    }

    public boolean isDimmed() {
        return super.isDimmed() && !isVisibleEditingPopup() || panelRenderer.editing != null;
    }

    public void issueResolved() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManualRecordingPanel.issueResolved()");
        RecordingRequest r = toBeChecked;
        if (r == null) {
            return;
        }
        try {
            if (r.getState() == LeafRecordingRequest.PENDING_NO_CONFLICT_STATE) {
                showFinishPopup(r, AppData.getString(r, AppData.TITLE),
                                AppData.getInteger(r, AppData.CHANNEL_NUMBER));
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    class ManualRecordingPanelRenderer extends OptionPanelRenderer {
        Color cDim = new DVBColor(0, 0, 0, 204);

        Image bg = dataCenter.getImage("bg.jpg");
        Image hotkeybg = dataCenter.getImage("01_hotkeybg.png");
        Image sep = dataCenter.getImage("07_sep_pvr.png");

        String strManualRecording = dataCenter.getString("manual.ManualRecording");

        EditField editing = null;
        boolean editPopup = false;

        public ManualRecordingPanelRenderer() {
            SHADOW_Y = 460;
        }

        public void paintBackground(Graphics g, FullScreenPanel c) {
            g.drawImage(bg, 0, 0, c);
            g.drawImage(hotkeybg, 242, 466, c);
            g.drawImage(sep, 0, 122, c);

            g.setFont(GraphicsResource.BLENDER32);
            g.setColor(GraphicsResource.C_255_255_255);
            g.drawString(strManualRecording, 55, 106);
        }

        protected void paint(Graphics g, UIComponent c) {
            super.paint(g, c);
            editing = null;
            editPopup = isVisibleEditingPopup();
            if (editFocus >= 0) {
                if (fields[editFocus].isEditing() || editPopup) {
                    editing = fields[editFocus];
                }
            }

            g.translate(233, 161);
            for (int i = 0; i < fields.length; i++) {
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(fields[i].fieldName, 56-233, 20);
                fields[i].paint(g, editFocus == i && editing == null, c);
                g.translate(0, 40);
            }
            g.translate(-233, -161 - 40 * fields.length);

            paintEditingComponent(g, c);
        }

        public void paintEditingComponent(Graphics g, UIComponent c) {
            if (editing != null) {
                g.translate(233, 161 + 40 * editFocus);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(editing.fieldName, 56-233, 20);

                editing.paint(g, !editPopup, c);
                g.translate(-233, -161 - 40 * editFocus);
            }
        }
    }

    class EditingComponent extends UIComponent {
        public EditingComponent() {
            setRenderer(new EditingRenderer());
        }
    }

    class EditingRenderer extends Renderer {
        protected void paint(Graphics g, UIComponent c) {
            panelRenderer.paintEditingComponent(g, c);
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void prepare(UIComponent c) {
        }
    }

}

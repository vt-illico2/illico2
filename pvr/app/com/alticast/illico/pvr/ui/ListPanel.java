package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.*;
import java.util.*;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.rmi.RemoteException;
import javax.tv.service.Service;
import javax.tv.util.*;
import org.ocap.ui.event.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;

/**
 * Panel for Recorded or Upcoming List.
 *
 * @author  June Park
 */
public abstract class ListPanel extends ListOptionPanel implements TVTimerWentOffListener,
                                    RecordingChangedListener, IssueChangedListener,
                                    MenuOpenListener, AutoScrollingImageListener, DataUpdateListener {

    public static final int RECORDED_LIST = 0;
    public static final int UPCOMING_LIST = 1;
    protected int type;
    protected static final String[] TITLE_KEY = { "List.title_recorded", "List.title_upcoming" };
    protected static final String[] LOG_PREFIX = { "RecordedList", "UpcomingList" };
    public static final String[] SORTING_OPTION_NAME = { "RecordedList.sorting", "UpcomingList.sorting" };
    protected String logPrefix;

    protected static int freeSpaceIndex = 0;
    protected static String freeSpaceString = "";

    protected TVTimerSpec freeSpaceUpdateTimer = new TVTimerSpec();
    //->Kenneth[2017.3.15] R7.3 : resume viewing button 의 토글을 위한 timer 
    protected TVTimerSpec resumeViewingButtonTimer;
    //<-

    public ScrollTexts scrollText = new ScrollTexts();
    protected PvrFooter footer = new PvrFooter();
    protected OptionScreen optionScreen = new OptionScreen();
    protected OptionScreen sortScreen = new OptionScreen(KeyCodes.COLOR_A, PreferenceService.BTN_A, "pvr.Close");

    protected Byte buildLock = new Byte((byte) 3);

    protected int recordingCount = 0;

    public SortingOption sortingOption;

    protected MultipleStateFilter listFilter;

    public static String requestId = null;

    protected UpdateWorker updateWorker;

    private boolean listenerEnabled = false;

    private ListUpdateJob fullUpdateJob = new ListUpdateJob(ListData.FULL_UPDATE, null);

    private AutoScrollingImage ticker = new AutoScrollingImage(1300, ListPanelRenderer.LIST_Y_GAP, this);

    protected boolean started = false;

    protected String preferredSortingKey;

    public TextBox noRecordingBox = new TextBox(TextBox.ALIGN_CENTER);

    //->Kenneth[2015.6.12] R5
    private long sessionId;

    public ListPanel(int type, MultipleStateFilter filter) {
        stack.push("pvr.PVR");
        this.type = type;
        this.listFilter = filter;
        sortingOption = getDefaultSortingOption();
        preferredSortingKey = getDefaultSortingOption().getKey();

        logPrefix = LOG_PREFIX[type];
        scrollText.setBounds(520, 197, 394, 90);
        scrollText.setFont(FontResource.DINMED.getFont(18));
        scrollText.setForeground(GraphicsResource.C_255_255_255);
        scrollText.setRows(4);
        scrollText.setRowHeight(19);
        scrollText.setTopMaskImagePosition(0, 0);
        scrollText.setBottomMaskImagePosition(0, 59);
        scrollText.setInsets(0, 2, 0, 0);
        this.add(scrollText);

        noRecordingBox.setFont(FontResource.BLENDER.getFont(18));
        noRecordingBox.setForeground(GraphicsResource.C_227_227_228);
        noRecordingBox.setRowHeight(22);
        noRecordingBox.setBounds(60, 260, 420, 145);
        this.add(noRecordingBox);

//        noRecording.setFont(FontResource.BLENDER.getFont(24));
//        noRecording.setForeground(Color.white);
//        noRecording.setBounds(100, 312, 760, 30);
//        this.add(message);

        footer.setBounds(0, 488, 906, 25);
        this.add(footer);

        freeSpaceUpdateTimer.setDelayTime(
            DataCenter.getInstance().getLong("FREE_SPACE_UPDATE_TIME", 5 * Constants.MS_PER_SECOND));
        freeSpaceUpdateTimer.setRepeat(true);
        freeSpaceUpdateTimer.setRegular(false);
        freeSpaceUpdateTimer.addTVTimerWentOffListener(this);

        //->Kenneth[2017.3.15] R7.3 : resume viewing button 의 토글을 위한 timer 
        if (this instanceof RecordedList) {
            resumeViewingButtonTimer = new TVTimerSpec();
            resumeViewingButtonTimer.setDelayTime(3000L);
            resumeViewingButtonTimer.setRepeat(true);
            resumeViewingButtonTimer.setRegular(false);
            resumeViewingButtonTimer.addTVTimerWentOffListener(this);
        }
        //<-

//        updateWorker = new Worker(TITLE_KEY[type], true);
        updateWorker = new UpdateWorker();
    }

    public synchronized void init() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.init()");
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".init");
        }
        IssueManager.getInstance().addListener(this);
        enableListener(true);

        DataCenter dataCenter = DataCenter.getInstance();
        String spn = getSortingPreferenceName();
        dataCenter.addDataUpdateListener(spn, this);
        Object spv = dataCenter.get(spn);
        if (spv != null) {
            setPreferredSortingKey(spv.toString());
            sortingOption = getSortingOption(preferredSortingKey);
//            dataUpdated(spn, null, spv);
        }
        updateWorker.pushFullUpdateJob();
    }

    public void enableListener(boolean enable) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.enableListener("+enable+")");
        RecordingManager rm = RecordingManager.getInstance();
        synchronized (rm) {
            if (listenerEnabled == enable) {
                return;
            }
            if (enable) {
                rm.addRecordingChangedListener(this);
            } else {
                rm.removeRecordingChangedListener(this);
            }
            listenerEnabled = enable;
        }
    }

    public synchronized void start() {
        Log.printDebug(App.LOG_HEADER+"Start ListPanel.start()");
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".start : " + requestId);
        }
        this.focus = -1;
        int index = -1;
        String searchId = requestId;
        boolean showRemoteSearchPopup = false;
        if (searchId != null) {
            AbstractRecording rec = null;
            try {
                rec = AbstractRecording.find(searchId);
                if (rec != null) {
                    index = focusTo(rec);
                    if (index < 0) {
                        SortingOption dso = getDefaultSortingOption();
                        if (sortingOption != dso) {
                            buildList(dso);
                            index = focusTo(rec);
                        } else {
                            RecordingFolder folder = listData.getCurrentFolder();
                            if (folder != null) {
                                listData.selected(folder);
                                index = focusTo(rec);
                            }
                        }
                    }
                } else {
                    // not found
                    try {
                        Integer.parseInt(searchId);
                    } catch (NumberFormatException nfe) {
                        // from remote
                        boolean found = findRemoteDevice(searchId);
                        showRemoteSearchPopup = !found;
                    }

                }
            } catch (Exception ex) {
            }
        }
        if (index >= 0) {
            listData.focus = index;
        } else {
            //->Kenneth[2017.8.24] R7.4 : sorting 을 다시 한다.
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.start() : Call listData.setTotalEntries("+sortingOption+")");
            listData.setTotalEntries(sortingOption);
            //<-
            listData.setFocus(null);
        }
        ListPanelRenderer lpr = createRenderer();
        lpr.setAutoScrollingImage(ticker);
        setRenderer(lpr);

        scrollText.setBottomMaskImage(dataCenter.getImage("07_des_sh02.png"));
        if (type == RECORDED_LIST) {
            scrollText.setTopMaskImage(dataCenter.getImage("07_des_sh02_t2.png"));
        } else {
            scrollText.setTopMaskImage(dataCenter.getImage("07_des_sh02_t.png"));
        }
        updateFreeSpace();
        focusChanged();
        requestId = searchId;
        updateFooter();
        updateMessageBoxText(listData);
        super.start();
//        setVisible(true);
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(freeSpaceUpdateTimer);
        try {
            freeSpaceUpdateTimer = timer.scheduleTimerSpec(freeSpaceUpdateTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (showRemoteSearchPopup) {
            showRemoteSearchPopup();
        }
        ticker.prepare();
        started = true;
        //->Kenneth[2015.9.1] VDTRMASTER-5588
        boolean isListVisible = isVisible();
        Log.printDebug(App.LOG_HEADER+"ListPanel.start() : isVisible() = "+isListVisible);
        //<-
        //->Kenneth[2015.6.12] R5
        if (type == RECORDED_LIST && isListVisible) {//->VDTRMASTER-5588
        //if (type == RECORDED_LIST) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.start() : RECORDED_LIST : Leave VBM log");
            sessionId = System.currentTimeMillis();
            PvrVbmController.getInstance().writeAccessListPage(sessionId);
        } else {
            Log.printDebug(App.LOG_HEADER+"ListPanel.start() : UPCOMING_LIST : Do not leave VBM log");
        }
        //<-
        Log.printDebug(App.LOG_HEADER+"End ListPanel.start()");
    }

    public synchronized void stop() {
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".stop");
        }
        ticker.flush();
        TVTimer.getTimer().deschedule(freeSpaceUpdateTimer);
        //->Kenneth[2017.3.15] R7.3
        if (resumeViewingButtonTimer != null) {
            TVTimer.getTimer().deschedule(resumeViewingButtonTimer);
        }
        //<-
        optionScreen.stop();
        sortScreen.stop();
        hidePopup();
        super.stop();
        setRenderer(null);
        requestId = null;
        MetadataDiagScreen.getInstance().stop();
        started = false;
    }

    public void stopOptions() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.stopOptions()");
        optionScreen.stop();
        sortScreen.stop();
    }

    /** PauseXlet. Parental, Session 등을 정리. */
    public void pause() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.pause()");
        restoreSorting();
    }

    private void restoreSorting() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.restoreSorting()");
        Thread t = new Thread("ListPanel.restoreSorting") {
            public void run() {
                synchronized (ListPanel.this) {
                    SortingOption so = getSortingOption(preferredSortingKey);
                    if (sortingOption != so) {
                        buildList(so);
                    }
                }
            }
        };
        t.start();
    }

    public int focusTo(AbstractRecording rec) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.focusTo()");
        int index = listData.getCurrentList().indexOf(rec);
        if (index >= 0) {
            listData.focus = index;
        } else {
            RecordingFolder folder = new RecordingFolder(rec.getTitle(), new Vector());
            index = listData.getCurrentList().indexOf(folder);
        }
        return index;
    }

    private boolean findRemoteDevice(String searchId) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.findRemoveDevice()");
        Vector deviceList = HomeNetManager.getInstance().getDeviceList();
        if (deviceList == null) {
            return false;
        }
        Enumeration en = deviceList.elements();
        while (en.hasMoreElements()) {
            RemoteDevice rd = (RemoteDevice) en.nextElement();
            if (searchId.endsWith(rd.getUdn())) {
                return true;
            }
        }
        return false;
    }

    protected abstract SortingOption getDefaultSortingOption();

    protected abstract ListPanelRenderer createRenderer();

    protected abstract MenuItem getSortingMenu();

    private void setPreferredSortingKey(String key) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.setPreferredSortingKey("+key+")");
        this.preferredSortingKey = key;
//        SortingOption so = (SortingOption) getSortingMenu().find(key);
//        if (so == null) {
//            so = getDefaultSortingOption();
//        }
//        this.preferredSortingOption = so;
    }

    private SortingOption getSortingOption(String key) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.getSortingOption("+key+")");
        SortingOption so = (SortingOption) getSortingMenu().find(key);
        if (so == null) {
            so = getDefaultSortingOption();
        }
        return so;
    }

    public String getSortingPreferenceName() {
        return SORTING_OPTION_NAME[type];
    }

    protected void changeSortingOption(SortingOption so) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.changeSortingOption("+so+")");
        sort(so);
        PreferenceProxy.getInstance().setPreferenceData(getSortingPreferenceName(), so.getKey());
    }

    protected void sort(SortingOption so) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.sort("+so+")");
        buildList(so, ListData.SORT, null);
    }

    private void buildList() {
        buildList(sortingOption);
    }

    private void buildList(SortingOption so) {
        buildList(so, ListData.FULL_UPDATE);
    }

    protected void buildList(SortingOption so, byte type) {
        buildList(so, type, null);
    }

    // TODO - 이 작업 수행 중 sort를 바꾸면 sort 가 되돌아가는 문제가 있다.
    protected void buildList(SortingOption so, byte type, Object obj) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.buildList("+so+", "+type+", "+obj+")");
        if (Log.INFO_ON) {
            Log.printInfo(logPrefix + ".buildList : " + ListData.getUpdateTypeString(type) + " : " + so);
        }
        SortingOption oldSorting = sortingOption;
        long time = System.currentTimeMillis();
        ListData data;
        if (type == ListData.FULL_UPDATE || listData == null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.buildList() : Call createListData()");
            data = createListData(so);
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.buildList() : Call listData.createNewData()");
            data = listData.createNewData(so, type, obj);
        }
        Log.printDebug(App.LOG_HEADER+"ListPanel.buildList() data = "+data);
        if (data == null) {
            return;
        }
        appendAdditionalData(data);
        data.resetFocus(listData);
        synchronized (this) {
            if (oldSorting != sortingOption) {
                Log.printWarning(logPrefix + ".buildList : sorting option has been changed !");
                updateFull();
                return;
            }
            Entry old = null;
            if (listData != null) {
                old = listData.getCurrent();
            }
            recordingCount = data.getRecordingCount();
            if (sortingOption != so) {
                data.setFocus(null);
                sortingOption = so;
            }
            updateMessageBoxText(data);
            listData = data;
            if (renderer != null) {
                if (old == null || !old.equals(data.getCurrent())) {
                    focus = -1;
                    updateFooter();
                }
                focusChanged();
                repaint();
            }
        }
        time = System.currentTimeMillis() - time;
        String result = logPrefix + ".buildList : " + ListData.getUpdateTypeString(type) + " : takes " + time + " ms";
        if (Log.DEBUG_ON) {
            Log.printDebug(result);
        }
        FrameworkMain.getInstance().printSimpleLog(result);
    }

    private RecordingList getFilteredList() {
        RecordingList list = RecordingManager.getInstance().getEntries(listFilter);
        return list;
//        return RecordingListManager.filterOutToBeDeleted(list);
//        return RecordingListManager.reverseFilter(list, AppData.PROGRAM_TYPE, AppData.TO_BE_DELETED, true);
    }

    protected ListData createListData(SortingOption so) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.createListData("+so+")");
        return new ListData(getFilteredList(), getDeviceList(), IssueManager.getInstance().getIssue(), so);
    }

    protected void appendAdditionalData(ListData newData) {
    }

    protected abstract Vector getDeviceList();

    private Vector getAllRecordings() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.getAllRecordings()");
        Vector vector = new Vector();
        RecordingList list = RecordingListManager.getInstance().getRecordedList();
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                vector.addElement(list.getRecordingRequest(i));
            }
        }
        if (App.SUPPORT_HN) {
            HomeNetManager.getInstance().getAllRecordingItems(vector);
        }
        return vector;
    }

    private void updateMessageBoxText(ListData data) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.updateMessageText("+data+")");
        int recordingCount = data.getRecordingCount();
        if (recordingCount > 0 && !data.hasFilteredRecording()) {
            String msg;
            if (recordingCount == 1) {
                msg = DataCenter.getInstance().getString("List.not_match_1");
            } else {
                msg = DataCenter.getInstance().getString("List.not_match_2");
                msg = TextUtil.replace(msg, "%%", String.valueOf(recordingCount));
            }
            noRecordingBox.setContents(msg);
            noRecordingBox.setVisible(true);
        } else {
            noRecordingBox.setVisible(false);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    // Navigation & Focus
    //////////////////////////////////////////////////////////////////////////

    public boolean handleKey(int code) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.handleKey("+code+")");
        //->Kenneth[2015.10.14] log util 개발 추가 -> 이번 릴리즈에서는 막기로 함.
        //Log.checkLogOnOff(code, "PVR");
        //<-
        boolean toBeConsumed = (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code) || toBeConsumed;
        }
        boolean changed = false;
        switch (code) {
            case KeyCodes.COLOR_A:
                footer.clickAnimationByKeyCode(code);
                optionScreen.stop();
                showSort();
                break;
            case KeyCodes.COLOR_D:
                footer.clickAnimationByKeyCode(code);
                sortScreen.stop();
                showOptions(listData.getCurrent());
                break;
            case OCRcEvent.VK_LEFT:
                if (focus >= 0) {
                    focus = -1;
                    ticker.setFocus(listData.getCurrent());
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (focus < 0 && buttons.size() > 0) {
                    focus = 0;
                    ticker.setFocus(null);
                }
                break;
            case OCRcEvent.VK_UP:
                if (focus < 0) {
                    changed = listData.keyUp();
                } else if (focus > 0) {
                    focus--;
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (focus < 0) {
                    changed = listData.keyDown();
                } else if (focus < buttons.size() - 1) {
                    focus++;
                }
                break;
            case OCRcEvent.VK_PAGE_UP:
                scrollText.showPreviousPage();
                repaint();
                break;
            case OCRcEvent.VK_PAGE_DOWN:
                scrollText.showNextPage();
                repaint();
                break;
            case OCRcEvent.VK_ENTER:
                Log.printDebug(App.LOG_HEADER+"ListPanel.handlKey(VK_ENTER)");
                if (focus < 0) {
                    clickEffect.start(((ListOptionPanelRenderer)renderer).getListFocusBounds(this));
                    selected(listData.getCurrent());
                } else {
                    clickEffect.start(((OptionPanelRenderer)renderer).getButtonFocusBounds(this));
                    selected(buttons.getItemAt(focus));
                }
                break;
            case OCRcEvent.VK_LAST:
                footer.clickAnimationByKeyCode(code);
                RecordingFolder folder = listData.getCurrentFolder();
                if (folder != null) {
                    // move to parent
                    listData.selected(folder);
                    updateFooter();
                    changed = true;
                } else {
                    // exit
                    if (Core.standAloneMode) {
                        Core.getInstance().exitToChannel();
                    } else {
                        if (requestId != null) {
                            MenuController.getInstance().startSearch(false);
                            //->Kenneth[2015.6.12] R5
                            PvrVbmController.getInstance().writeExitByBack(sessionId);
                            //<-
                        } else if (Environment.SUPPORT_DVR) {
                            MenuController.getInstance().showMenu(PvrService.MENU_PVR_PORTAL);
                        }
                    }
                }
                break;
            case OCRcEvent.VK_LIST:
            case OCRcEvent.VK_EXIT:
            case KeyCodes.SEARCH:
                optionScreen.stop();
                sortScreen.stop();
                footer.clickAnimationByKeyCode(code);
                return false;
            case KeyCodes.FAV:
                if (Log.EXTRA_ON) {
                    if (type == RECORDED_LIST) {
                        MetadataDiagScreen.getInstance().startHomeNetLogs();
                    } else {
                        MetadataDiagScreen.getInstance().startLocalLogs();
                    }
                }
                return false;
            case OCRcEvent.VK_DISPLAY_SWAP:
                if (Log.EXTRA_ON) {
                    buildList();
//                    updateWorker.pushRemoteUpdateJob();
                }
                return false;
            case KeyCodes.COLOR_C:
            case KeyCodes.COLOR_B:
                if (Log.EXTRA_ON) {
                    Entry entry = listData.getCurrent();
                    if (entry != null && entry instanceof AbstractRecording) {
                        MetadataDiagScreen.getInstance().startRecording((AbstractRecording) entry, code == KeyCodes.COLOR_B);
                    } else {
                        MetadataDiagScreen.getInstance().startRoot();
                    }
                }
                return false;
            default:
                return toBeConsumed;
        }
        if (changed) {
            focusChanged();
        }
        repaint();
        return true;
    }

    public void focusChanged() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.focusChanged()");
        Entry current = listData.getCurrent();
        buttons = createButtons(current);
        int buttonSize = buttons.size();
        if (focus >= buttonSize) {
            focus = buttonSize - 1;
        }
        //->Kenneth[2017.3.15] R7.3
        Log.printDebug(App.LOG_HEADER+"ListPanel.focusChanged() : Call resetResumeViewingButtonTimer()");
        resetResumeViewingButtonTimer();
        //<-
        String desc = "";
        if (current != null) {
            if (current instanceof AbstractRecording) {
                AbstractRecording r = (AbstractRecording) current;
                if (ParentalControl.getLevel(r) != ParentalControl.ADULT_CONTENT) {
                    desc = r.getString(AppData.FULL_DESCRIPTION);
                }
            } else if (current instanceof InactiveSeries) {
                InactiveSeries is = (InactiveSeries) current;
                desc = is.getDescription();
            }
        }
        scrollText.setContents(desc);
        if (requestId != null) {
            requestId = null;
            updateFooter();
        }
        if (focus == -1) {
            ticker.setFocus(current);
        } else {
            ticker.setFocus(null);
        }
    }

    protected void updateFooter() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.updateFooter()");
        synchronized (footer) {
            footer.reset();
            if (!Core.standAloneMode) {
                if (listData.getCurrentFolder() == null) {
                    if (requestId != null) {
                        footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
                    } else if (Environment.SUPPORT_DVR) {
                        footer.addButton(PreferenceService.BTN_BACK, "pvr.Back_to_menu");
                    }
                } else {
                    footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
                }
                if (type == RECORDED_LIST && Environment.SUPPORT_DVR) {
                    footer.addButton(PreferenceService.BTN_LIST, "pvr.ChangeList");
                } else {
                    footer.addButton(PreferenceService.BTN_LIST, "pvr.Exit");
                }
                footer.addButton(PreferenceService.BTN_SEARCH, "pvr.Search");
            } else {
                footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
            }
            footer.addButton(PreferenceService.BTN_A, "option.Sort");
            footer.addButton(PreferenceService.BTN_D, "pvr.Options");
        }
    }

    public static String getFreeSpaceString(long free, long total) {
        //->Kenneth[2015.2.3] 4K
        //return getFreeSpaceString(free, total, freeSpaceIndex % 3);

        //->Kenneth[2015.5.11] 4K : 4K 박스에서만 UHD 시간이 보여야 됨.
        //return getFreeSpaceString(free, total, freeSpaceIndex % 4);
        String result = null;
        if (Environment.SUPPORT_UHD) {
            result = getFreeSpaceString(free, total, freeSpaceIndex % 4);
        } else {
            result = getFreeSpaceString(free, total, freeSpaceIndex % 3);
        }
        return result;
    }

    private static String getFreeSpaceString(long free, long total, int type) {
        //Log.printDebug(App.LOG_HEADER+"ListPanel.getFreeSpaceString()");
        StringBuffer sb = new StringBuffer(40);
        switch (type) {
            case 0:
                sb.append((int) (StorageInfoManager.getRatio(free, total) * 100));
                sb.append("% ");
                sb.append(dataCenter.getString("List.FreeSpace"));
                break;
            case 1:
                sb.append(dataCenter.getString("List.FreeSpace_short"));
                sb.append(" : ");
                //->Kenneth[2015.2.3] 4K
                int mSD = StorageInfoManager.getAvailableDuration(free, Constants.DEFINITION_SD);
                sb.append(mSD / 60);
                sb.append("h ");
                sb.append(mSD % 60);
                sb.append("min SD");
                break;
            case 2:
                sb.append(dataCenter.getString("List.FreeSpace_short"));
                sb.append(" : ");
                //->Kenneth[2015.2.3] 4K
                int mHD = StorageInfoManager.getAvailableDuration(free, Constants.DEFINITION_HD);
                sb.append(mHD / 60);
                sb.append("h ");
                sb.append(mHD % 60);
                sb.append("min HD");
                break;
            case 3:
                //->Kenneth[2015.2.3] 4K
                sb.append(dataCenter.getString("List.FreeSpace_short"));
                sb.append(" : ");
                int mUHD = StorageInfoManager.getAvailableDuration(free, Constants.DEFINITION_4K);
                sb.append(mUHD / 60);
                sb.append("h ");
                sb.append(mUHD % 60);
                sb.append("min UHD");
                break;
        }
        return sb.toString();
    }


    private void updateFreeSpace() {
        //Log.printDebug(App.LOG_HEADER+"ListPanel.updateFreeSpace()");
        long total = 1;
        long free = 0;
        String string = null;
        if (Environment.SUPPORT_DVR) {
            total = StorageInfoManager.getInstance().getAllocatedSpace();
            free = StorageInfoManager.getInstance().getFreeSpace();
            string = getFreeSpaceString(free, total);
        } else if (App.SUPPORT_HN) {
            Vector deviceList = HomeNetManager.getInstance().getDeviceList();
            if (deviceList != null) {
                int size = deviceList.size();
                if (size == 0) {
//                    return;
                } else if (size == 1) {
                    RemoteDevice dev = (RemoteDevice) deviceList.elementAt(0);
                    total = dev.totalSpace;
                    free = dev.freeSpace;
                    string = getFreeSpaceString(free, total);
                } else {
                    //->Kenneth : 아랫라인에서 freeSpaceIndex 를 2 나눠주고 size 로 % 하는 이유는 머지?
                    // 일단은 그대로 놔둠
                    RemoteDevice dev = (RemoteDevice) deviceList.elementAt((freeSpaceIndex / 2) % size);
                    total = dev.totalSpace;
                    free = dev.freeSpace;
                    //->Kenneth[2015.2.3] 4K
                    //if (freeSpaceIndex % 2 == 0) {
                    if (freeSpaceIndex % 3 == 0) {
                        string = "" + dev.getFriendlyName();
                    } else {
                        string = getFreeSpaceString(free, total, 0);
                    }
                }
            }
        }
        if (total == 1) {
            string = null;
        }
        ListPanelRenderer.totalSpace = total;
        ListPanelRenderer.freeSpace = free;
        ListPanelRenderer.freeSpaceString = string;
    }

    public String getTitle() {
        return dataCenter.getString(TITLE_KEY[type]) + " (" + recordingCount + ")";
    }

    /////////////////////////////////////////////////////////////////////////
    // Menu & Option
    /////////////////////////////////////////////////////////////////////////

    protected void showOptions(Entry entry) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showOptions()");
        optionScreen.start(createOptions(entry), null, this, this);
    }

    protected void showSort() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showSort()");
        sortScreen.start(getSortingMenu(), null, this, this);
    }

    protected void selected(Entry entry) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.selected("+entry+")");
        if (entry instanceof AbstractRecording) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : AbstractRecording");
            AbstractRecording rec = (AbstractRecording) entry;
            int level = ParentalControl.getLevel(rec);
            if (level == ParentalControl.OK) {
                if (type == RECORDED_LIST) {
                    processPlay(rec);
                } else {
                    processEdit(rec);
                }
            } else {
                processUnblock(rec, level);
            }
        } else if (entry instanceof RecordingFolder) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : RecordingFolder");
            RecordingFolder folder = (RecordingFolder) entry;
            if (ParentalControl.isAccessBlocked(folder) && listData.getCurrentFolder() == null) {
                processUnblock(folder);
            } else {
                listData.selected(folder);
                updateFooter();
                focusChanged();
            }
        } else if (entry instanceof Issue) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : Issue");
            IssueManager.getInstance().showIssue();
        } else if (entry instanceof InactiveSeries) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : InactiveSeries");
            RecordingScheduler.getInstance().unset(((InactiveSeries) entry).getGroupKey());
            updateFull();
        }
        repaint();
    }

    public void selected(MenuItem item) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.selected("+item+")");
        Entry entry = listData.getCurrent();
        if (item instanceof SortingOption) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : SortingOption");
            changeSortingOption((SortingOption) item);
        } else if (item == MenuItems.DELETE || item == MenuItems.CANCEL || item == MenuItems.CANCEL_SERIES) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : DELETE || CANCEL || CANCEL_SERIES");
            if (entry instanceof RecordingFolder) {
                // delete folder
                processDelete((RecordingFolder) entry);
            } else if (entry instanceof AbstractRecording) {
                processDelete((AbstractRecording) entry);
            } else if (entry instanceof InactiveSeries) {
                RecordingScheduler.getInstance().unset(((InactiveSeries) entry).getGroupKey());
                updateFull();
            }
        } else if (item == MenuItems.PLAY) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : PLAY");
            if (entry instanceof AbstractRecording) {
                processPlay((AbstractRecording) entry);
            }
        } else if (item == MenuItems.STOP) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : STOP");
            if (entry instanceof AbstractRecording) {
                processStop((AbstractRecording) entry);
            }
        } else if (item == MenuItems.VIEW_EPISODES || item == MenuItems.BACK) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : VIEW_EPISODES || BACK");
            if (entry instanceof RecordingFolder) {
                listData.selected((RecordingFolder) entry);
                updateFooter();
                focusChanged();
            }
        } else if (item == MenuItems.MORE_DETAILS) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : MORE_DETAILS");
            if (entry instanceof AbstractRecording) {
                processDetail((AbstractRecording) entry);
            }
        } else if (item == MenuItems.EDIT || item == MenuItems.EDIT_SERIES) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : EDIT || EDIT_SERIES");
            if (entry instanceof AbstractRecording) {
                processEdit((AbstractRecording) entry);
            }
        } else if (item == MenuItems.BLOCK_MANUALLY) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : BLOCK_MANUALLY");
            if (entry instanceof AbstractRecording) {
                processBlockManually((AbstractRecording) entry);
            } else if (entry instanceof RecordingFolder) {
                processBlockManually((RecordingFolder) entry);
            }
        } else if (item == MenuItems.UNBLOCK_MANUALLY) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : UNBLOCK_MANUALLY");
            if (entry instanceof AbstractRecording) {
                processUnblockManually((AbstractRecording) entry);
            } else if (entry instanceof RecordingFolder) {
                processUnblockManually((RecordingFolder) entry);
            }
        } else if (item == MenuItems.UNBLOCK_RECORDING) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : UNBLOCK_RECORDING");
            if (entry instanceof AbstractRecording) {
                AbstractRecording rec = (AbstractRecording) entry;
                processUnblock(rec, ParentalControl.getLevel(rec));
            } else if (entry instanceof RecordingFolder) {
                processUnblock((RecordingFolder) entry);
            }
        } else if (item == MenuItems.SKIP) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : SKIP");
            if (entry instanceof AbstractRecording) {
                processSkip((AbstractRecording) entry);
            }
        } else if (item == MenuItems.RESET) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : RESET");
            if (entry instanceof AbstractRecording) {
                setSkipped((AbstractRecording) entry, false);
                focusChanged();
            }
        } else if (item == MenuItems.MULTI_DELETE) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : MULTI_DELETE");
            Vector vector = getAllRecordings();
            if (vector.size() > 0) {
                showPopup(new MultipleDeletePopup(vector));
            }
        //->Kenneth[2017.3.16] R7.3 : Search 추가
        } else if (item == MenuItems.SEARCH) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : SEARCH");
            MenuController.getInstance().startSearch(true, "D_PVR");
        //<-
        } else {
            Log.printDebug(App.LOG_HEADER+"ListPanel.selected() : else");
            Core.getInstance().selected(item);
        }
        repaint();
    }

    private void delete(Entry entry) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.delete("+entry+")");
        if (entry instanceof RecordingFolder) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.delete() : RecordingFolder");
            // delete folder
            processDelete((RecordingFolder) entry);
        } else if (entry instanceof AbstractRecording) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.delete() : AbstractRecording");
            processDelete((AbstractRecording) entry);
        }
    }

    protected void playRecording(AbstractRecording r, int playPosition) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.playRecording("+r+")");

        //->Kenneth[2015.3.1] 4K : Non 4K STB 에서 원격의 UHD play 를 막는다
        //->Kenneth[2015.6.24] DDC-107
        if (Core.getInstance().needToShowIncompatibilityPopup(r, true)) {
        //if (Core.getInstance().needToShowIncompatibilityPopup(r)) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.playRecording() : Call Core.showIncompatibilityErrorMessage()");
            Core.getInstance().showIncompatibilityErrorMessage();
            return;
        }

        Service s = r.getService();
        if (s == null) {
            Log.printWarning("ListPanel.playRecording: service is null");
        }
        setVisible(false);
        focus = -1;
        if (requestId != null) {
            requestId = null;
            updateFooter();
        }
        VideoPlayer.getInstance().playRecording(r, playPosition);
    }

    public void deleteRecording(final AbstractRecording r, final long mid) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording("+r+")");
        if (r == null) {
            return;
        }
        Thread t = new Thread("PVR.delete:" + r) {
            public void run() {
                boolean remoteDeleted = false;
                boolean result = r.deleteIfPossible();
                Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : deleteIfPossible returns "+result);
                if (result) {
                    synchronized (this) {
                        if (listData != null) {
                            Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call listData.delete()");
                            listData.delete(r);
                            focusChanged();
                            repaint();
                        }
                    }
                    if (r instanceof RemoteRecording) {
                        Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call HomeNetManager.deleteRecording()");
                        HomeNetManager.getInstance().deleteRecording((RemoteRecording) r);
                        remoteDeleted = true;
                    }
                    if (type == UPCOMING_LIST) {
                        String groupKey = r.getGroupKey();
                        if (groupKey != null) {
                            Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call RecordingScheduler.unset("+groupKey+")");
                            RecordingScheduler.getInstance().unset(groupKey);
                        }
                    } else {
                        if (r instanceof LocalRecording) {
                            Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call PvrVbmController.writeRecordingDeleted()");
                            PvrVbmController.getInstance().writeRecordingDeleted(((LocalRecording) r).getRecordingRequest(), mid);
                        }
                    }
                } else {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call showCannotDeletePopup()");
                    showCannotDeletePopup(r);
                }
                if (remoteDeleted) {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecording.Thread.run() : Call updateWorker.pushRemoteUpdateJob()");
                    updateWorker.pushRemoteUpdateJob();
                }
            }
        };
        t.start();
    }

    /** from list */
    private void deleteRecording(RecordingFolder folder) {
        deleteRecordings(folder.getEntries(), true);
    }

    /** from multiple delete popup. */
    public void deleteRecordings(final Vector vector, final boolean inList) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings()");
        if (vector == null) {
            return;
        }
        Thread t = new Thread("PVR.deleteRecordings:" + vector.size()) {
            public void run() {
//                enableListener(false);
                try {
                    process();
                } catch (Throwable t) {
                    Log.print(t);
                }
//                enableListener(true);
//                // TODO - june - 개선
//                updateFull();
            }

            private void process() {
                Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process()");
                if (Log.DEBUG_ON) {
                    Log.printDebug(logPrefix + ".deleteRecordings");
                }
                Enumeration en = vector.elements();
                Vector toBeDeleted = new Vector();
                Vector target = new Vector();
                while (en.hasMoreElements()) {
                    Entry entry = (Entry) en.nextElement();
                    if (entry instanceof AbstractRecording) {
                        AbstractRecording r = (AbstractRecording) entry;
                        if (r.getStateGroup() != RecordingListManager.IN_PROGRESS) {
                            if (!App.SUPPORT_HN || (r instanceof LocalRecording) && !r.isInUse()) {
                                Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call markToBeDeleted()");
                                RecordingListManager.markToBeDeleted(r);
//                                r.setAppData(AppData.PROGRAM_TYPE, AppData.TO_BE_DELETED);
                                target.addElement(r);
                            }
                            toBeDeleted.addElement(r);
                        }
                    }
                }
                ListData data = listData;
                if (data != null) {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call ListData.delete()");
                    data.delete(target);
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call sort()");
                    sort(sortingOption);
                }
                int size = toBeDeleted.size();
                int failedCount = 0;
                AbstractRecording notDeleted = null;
                boolean remoteDeleted = false;
                for (int i = 0; i < size; i++) {
                    AbstractRecording r = (AbstractRecording) toBeDeleted.elementAt(i);
                    boolean result = r.deleteIfPossible();
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : deleteIfPossible() returns "+result);
                    if (!result) {
                        failedCount++;
                        notDeleted = r;
                    } else if (r instanceof RemoteRecording) {
                        Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call HomeNetManager.deleteRecording()");
                        HomeNetManager.getInstance().deleteRecording((RemoteRecording) r);
                        remoteDeleted = true;
                    }
                    if (type == UPCOMING_LIST) {
                        String groupKey = r.getGroupKey();
                        if (groupKey != null) {
                            Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call RecordingScheduler.unset("+groupKey+")");
                            RecordingScheduler.getInstance().unset(groupKey);
                        }
                    } else {
                        if (r instanceof LocalRecording) {
                            PvrVbmController.getInstance().writeRecordingDeleted(((LocalRecording) r).getRecordingRequest(),
                                inList ? PvrVbmController.MID_DELETE_FROM_LIST : PvrVbmController.MID_DELETE_FROM_MULTI);
                        }
                    }
                }
                if (failedCount == 1) {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call showCannotDeletePopup("+notDeleted+")");
                    showCannotDeletePopup(notDeleted);
                } else if (failedCount > 1) {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call showCannotDeletePopup("+failedCount+")");
                    showCannotDeletePopup(failedCount);
                }
                if (remoteDeleted) {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.deleteRecordings().Thread.process() : Call updateWorker.pushRemoteUpdateJob()");
                    updateWorker.pushRemoteUpdateJob();
                }
            }
        };
        t.start();
    }

    /////////////////////////////////////////////////////////////////////////
    // MenuOpenListener
    /////////////////////////////////////////////////////////////////////////

    public void opened(MenuItem item) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.opened");
        int size = item.size();
        for (int i = 0; i < size; i++) {
            MenuItem menu = item.getItemAt(i);
            menu.setChecked(menu == sortingOption);
        }
    }

    public void canceled() {
    }

    /////////////////////////////////////////////////////////////////////////
    // process
    /////////////////////////////////////////////////////////////////////////

    protected void processPlay(final AbstractRecording r) {
        //->Kenneth[2017.3.15] R7.3 : ButtonListPopup 을 사용하도록 전반적으로 바뀌었음.
        Log.printDebug(App.LOG_HEADER+"ListPanel.processPlay("+r+")");
        if (r.getService() == null) {
            Log.printWarning("ListPanel.processPlay: service is null");
            int state = r.getRealState();
            Log.printDebug(" state = " + r.getRealState());
            if (state != 0 && state != LeafRecordingRequest.DELETED_STATE) {
                VideoPlayer.getInstance().showViewFailedPopup(r, VideoPlayer.VIEWING_FAILED_NO_SERVICE, true);
            }
            return;
        }
        short stateGroup = r.getStateGroup();
        long mediaTime = r.getMediaTime();

        final int[] option = new int[2];
        if (stateGroup != RecordingListManager.IN_PROGRESS) {
            if (mediaTime == 0) {
                playRecording(r, VideoPlayer.PLAY_FROM_BEGINNING);
                return;
            } else {
                option[0] = VideoPlayer.PLAY_RESUME;
                option[1] = VideoPlayer.PLAY_FROM_BEGINNING;
            }
        } else {
            if (mediaTime == 0 || mediaTime > Constants.MS_PER_DAY) {
                option[0] = VideoPlayer.PLAY_FROM_BEGINNING;
                option[1] = VideoPlayer.PLAY_CURRENT;
            } else {
                option[0] = VideoPlayer.PLAY_RESUME;
                option[1] = VideoPlayer.PLAY_FROM_BEGINNING;
            }
        }

        ButtonListPopup p = new ButtonListPopup(2);
        PvrFooter right = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
        right.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        right.setBounds(488, 378, 161, 30);
        p.add(right);

        p.setTitle(r.getTitle());
        if (stateGroup == RecordingListManager.IN_PROGRESS) {
            if (mediaTime == 0 || mediaTime > Constants.MS_PER_DAY) {
                p.setButtons(new String[] {"pvr.play_from_beginning2", "pvr.play_from_current_time"});
            } else {
                p.setButtons(new String[] {"pvr.resume_viewing", "pvr.play_from_beginning"});
            }
        } else {
            p.setButtons(new String[] {"pvr.resume_viewing", "pvr.play_from_beginning"});
            if (r.getViewingState() == AbstractRecording.COMPLETELY_VIEWED && mediaTime > 0L) {
                // COMPLETELY_VIEWED 이지만 아직 완전히 content 끝까지 도달하지 않은 상태 
                p.setFocus(1);
            }
        }
        PopupAdapter l = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0 || focus == 1) {
                    playRecording(r, option[focus]);
                }
            }
        };
        p.setListener(l);
        p.consumeExit();
        showPopup(p);
    }

    protected void processSkip(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processSkip("+r+")");
        if (!ParentalControl.isProtected(r)) {
            setSkipped(r, true);
            focusChanged();
        } else {
            ListPinListener pl = new ListPinListener(true) {
                public void processPinAction() {
                    setSkipped(r, true);
                }
            };
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.skip"), pl);
        }
    }

    protected void processStop(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processStop("+r+")");
        if (!ParentalControl.isProtected(r)) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.processStop() : Call showStopPopup()");
            showStopPopup(r);
        } else {
            ListPinListener pl = new ListPinListener(true) {
                public void processPinAction() {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.processStop().ListPinListener.processPinAction() : Call showStopPopup()");
                    showStopPopup(r);
                }
            };
            Log.printDebug(App.LOG_HEADER+"ListPanel.processStop() : Call PreferenceProxy.showPinEnabler()");
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.stop"), pl);
        }
    }

    protected void processEdit(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processEdit("+r+")");
        if (!ParentalControl.isProtected(r)) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.processEdit() : Call showEditPopup()");
            showEditPopup(r);
        } else {
            ListPinListener pl = new ListPinListener(true) {
                public void processPinAction() {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.processEdit().ListPinListener.processPinAction() : Call showEditPopup()");
                    showEditPopup(r);
                }
            };
            Log.printDebug(App.LOG_HEADER+"ListPanel.processEdit() : Call PreferenceProxy.showPinEnabler()");
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.modify"), pl);
        }
    }

    protected void processDetail(AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processDetail("+r+") : Call showPopup(ListDetailsPopup)");
        ListDetailsPopup popup = new ListDetailsPopup(r, type);
        showPopup(popup);
    }

    protected void processDelete(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete("+r+")");
        if (!ParentalControl.isProtected(r)) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete() : Call showDeletePopup()");
            showDeletePopup(r);
        } else {
            ListPinListener pl = new ListPinListener(true) {
                public void processPinAction() {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete().ListPinListener.processPinAction() : Call showDeletePopup()");
                    showDeletePopup(r);
                }
            };
            Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete() : Call PreferenceProxy.showPinEnabler()");
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.delete"), pl);
        }
    }

    protected void processDelete(final RecordingFolder folder) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete("+folder+")");
        if (!ParentalControl.isProtected(folder.getEntries())) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete() : Call showDeletePopup()");
            showDeletePopup(folder);
        } else {
            ListPinListener pl = new ListPinListener(true) {
                public void processPinAction() {
                    Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete().ListPinListener.processPinAction() : Call showDeletePopup()");
                    showDeletePopup(folder);
                }
            };
            Log.printDebug(App.LOG_HEADER+"ListPanel.processDelete() : Call PreferenceProxy.showPinEnabler()");
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.delete"), pl);
        }
    }

    protected void processUnblock(final AbstractRecording r, int level) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processUnblock("+r+")");
        final boolean adult = level == ParentalControl.ADULT_CONTENT;
        String key = adult ? "pin.display" : "pin.unblock_all";
        ListPinListener pl = new ListPinListener(true) {
            public void processPinAction() {
                ParentalControl.unblock(r);
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString(key), pl);
    }

    public void processUnblockAndPlay(final AbstractRecording r, int level) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processUnblockAndPlay("+r+")");
        final boolean adult = level == ParentalControl.ADULT_CONTENT;
        String key = adult ? "pin.display" : "pin.unblock_all";
        ListPinListener pl = new ListPinListener(true) {
            public void processPinAction() {
                ParentalControl.unblock(r);
                playRecording(r, VideoPlayer.PLAY_FROM_BEGINNING);
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString(key), pl);
    }

    protected void processUnblock(final RecordingFolder folder) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processUnblock("+folder+")");
        ListPinListener pl = new ListPinListener(true) {
            public void processPinAction() {
                ParentalControl.unblock(folder);
                Vector vector = folder.getEntries();
                Enumeration en = vector.elements();
                while (en.hasMoreElements()) {
                    Entry entry = (Entry) en.nextElement();
                    if (entry instanceof AbstractRecording) {
                        ParentalControl.unblock((AbstractRecording) entry);
                    }
                }
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.unblock_all"), pl);
    }

    protected void processBlockManually(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processBlockManually("+r+")");
        ListPinListener pl = new ListPinListener(false) {
            public void processPinAction() {
                String title = r.getTitle();
                blockFolderManually(title, true);
                ParentalControl.clear(title);
                updateFull();
                if (ParentalControl.pinChecked) {
                    showManualBlockPopup();
                }
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.block"), pl);
    }

    protected void processBlockManually(final RecordingFolder folder) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processBlockManually("+folder+")");
        ListPinListener pl = new ListPinListener(false) {
            public void processPinAction() {
                blockFolderManually(folder.getTitle(), true);
                folder.block(true);
                if (ParentalControl.pinChecked) {
                    showManualBlockPopup();
                }
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.block"), pl);
    }

    protected void processUnblockManually(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processUnblockManually("+r+")");
        ListPinListener pl = new ListPinListener(false) {
            public void processPinAction() {
                String title = r.getTitle();
                blockFolderManually(title, false);
                ParentalControl.unblock(title);
                updateFull();
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.unblock_all"), pl);
    }

    protected void processUnblockManually(final RecordingFolder folder) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.processUnblockManually("+folder+")");
        ListPinListener pl = new ListPinListener(false) {
            public void processPinAction() {
                blockFolderManually(folder.getTitle(), false);
                folder.block(false);
            }
        };
        PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.unblock_all"), pl);
    }

    private void blockFolderManually(String title, boolean block) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.blockFolderManually("+title+")");
        Log.printInfo("ListPanel.blockAll: " + title + ", " + block);
        Boolean value = block ? Boolean.TRUE : Boolean.FALSE;
        RecordingList list = getFilteredList();
        list = RecordingListManager.filter(list, AppData.TITLE, title);
        int size = (list != null) ? list.size() : 0;
        for (int i = 0; i < size; i++) {
            RecordingRequest req = list.getRecordingRequest(i);
            AppData.set(req, AppData.BLOCK, value);
            if (block) {
                ParentalControl.clear(req);
            } else {
                ParentalControl.unblock(req);
            }
        }
    }

    private void setSkipped(AbstractRecording r, boolean skip) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.setSkipped("+skip+")");
        if (r instanceof LocalRecording) {
            RecordManager.getInstance().setSkipped(((LocalRecording) r).getRecordingRequest(), skip);
        } else {
            r.setAppData(AppData.UNSET, skip ? Boolean.TRUE : Boolean.FALSE);
        }
    }


    /////////////////////////////////////////////////////////////////////////
    // Popup
    /////////////////////////////////////////////////////////////////////////

    protected void showStopPopup(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showStopPopup("+r+")");
        ListThreeButtonPopup popup = new ListThreeButtonPopup(dataCenter.getString("pvr.stop_recording"));
        popup.setButtonMessage(dataCenter.getString("pvr.stop_and_save"), dataCenter.getString("pvr.stop_and_delete"),
                               dataCenter.getString("pvr.continue_recording"));
        String msg = '\"' + r.getTitle() + "\" " + TextUtil.replace(dataCenter.getString("pvr.is_recording_stop_msg"), "|", "\n");
        popup.setMessage(msg);

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                switch (focus) {
                    case 0:
                        r.stop();
                        break;
                    case 1:
                        showDeletePopup(r);
                        break;
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    protected void showDeletePopup(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showDeletePopup("+r+")");
        String title, msg;
        if (type == RECORDED_LIST) {
            title = "pvr.delete_popup_title";
            msg = "pvr.delete_popup_msg";
        } else {
            String groupKey = r.getGroupKey();
            if (groupKey != null) {
                title = "pvr.cancel_series_popup_title";
                msg = "pvr.cancel_series_popup_msg";
            } else {
                title = "pvr.cancel_popup_title";
                msg = "pvr.cancel_popup_msg";
            }
        }
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString(title));
        popup.setDescription(dataCenter.getString(msg) + "\n'" + r.getTitle() + "' ?");
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    deleteRecording(r, PvrVbmController.MID_DELETE_FROM_LIST);
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    protected void showDeletePopup(final RecordingFolder folder) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showDeletePopup("+folder+")");
        ListSelectPopup popup = new ListSelectPopup();
        String title, msg;
        if (type == RECORDED_LIST) {
            title = "pvr.delete_series_popup_title";
            msg = "pvr.delete_series_popup_msg";
        } else {
            title = "pvr.cancel_series_popup_title";
            msg = "pvr.cancel_series_popup_msg";
        }

        popup.setPopupTitle(dataCenter.getString(title));
        popup.setDescription(dataCenter.getString(msg) + "\n'"
                + TextUtil.shorten(folder.getTitle(), GraphicsResource.FM18, 250) + "' ?");
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    deleteRecording(folder);
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    //->Kenneth[2017.3.14] R7.3
    protected void showMoreOptionsPopup(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showMoreOptionsPopup("+r+")");
        final boolean isLocal = r instanceof LocalRecording;
        Log.printDebug(App.LOG_HEADER+"ListPanel.showMoreOptionsPopup() : isLocal = "+isLocal);
        final boolean isSingle = r.getGroupKey() == null;
        Log.printDebug(App.LOG_HEADER+"ListPanel.showMoreOptionsPopup() : isSingle = "+isSingle);
        ListMoreOptionsPopup popup = null;
        if (isLocal && isSingle) {
            popup = new ListMoreOptionsPopup(r, ListMoreOptionsPopup.TYPE_EDIT);
        } else {
            popup = new ListMoreOptionsPopup(r, ListMoreOptionsPopup.TYPE_STATIC);
        }
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    if (isSingle && isLocal) {
                        Log.printDebug(App.LOG_HEADER+"ListPanel.showMoreOptionsPopup().listener : Call updateFull()");
                        updateFull();
                    }
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }
    //<-

    protected void showEditPopup(final AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showEditPopup("+r+")");
        ListEditPopup popup = new ListEditPopup(r);
        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    updateFull();
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    protected void showCannotDeletePopup(AbstractRecording r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showCannotDeletePopup("+r+")");
        PopupController.showCannotDeletePopup(r, this);
    }

    protected void showCannotDeletePopup(int failedCount) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showCannotDeletePopup("+failedCount+")");
        PopupController.showCannotDeletePopup(failedCount, this);
    }

    protected void showManualBlockPopup() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showManualBlockPopup()");
        InfoOneButtonPopup popup = new InfoOneButtonPopup(dataCenter.getString("pvr.manual_block_popup_title"));
        popup.fillBg = false;
        popup.setDescription(dataCenter.getString("pvr.manual_block_popup_msg"));
        showPopup(popup);
    }

    protected void showRemoteSearchPopup() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.showRemoteSearchPopup()");
        InfoOneButtonPopup popup = new InfoOneButtonPopup(dataCenter.getString("multiroom.cannot_find_title"));
        popup.fillBg = false;
        popup.setDescription(dataCenter.getString("multiroom.cannot_find_msg"));
        showPopup(popup);
    }

    public void updateFolder() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.updateFolder()");
        ListData data = listData;
        if (data != null) {
            RecordingFolder folder = listData.getCurrentFolder();
            if (folder != null) {
                folder.update();
            }
        }
    }

    ////////////////////////////////////////////////////////////////////
    // DataUpdateListener
    ////////////////////////////////////////////////////////////////////

    public void dataUpdated(String key, Object old, Object value) {
        Log.printDebug(logPrefix + ".dataUpdated : " + key + ", " + value);
        if (value != null) {
            String newSortingKey = value.toString();
            setPreferredSortingKey(newSortingKey);
            if (!started) {
                restoreSorting();
            }
        }
    }

    public void dataRemoved(String key)  {
    }


    /////////////////////////////////////////////////////////////////////////
    // Listeners
    /////////////////////////////////////////////////////////////////////////

    public void timerWentOff(TVTimerWentOffEvent e) {
        TVTimerSpec spec = e.getTimerSpec();
        if (spec == freeSpaceUpdateTimer) {
            freeSpaceIndex = Math.max(freeSpaceIndex + 1, 0);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.timerWentOff() : Call updateFreeSpace()");
            updateFreeSpace();
            repaint(43, 487, 438, 25);
        //->Kenneth[2017.3.15] R7.3
        } else if (spec == resumeViewingButtonTimer) {
            int index = buttons.indexOf(MenuItems.RESUME_VIEWING);
            if (index >= 0) {
                String displayableText = buttons.getItemAt(index).getDisplayableText();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListPanel.timerWentOff() : displayableText = "+displayableText);
                if (buttons.getItemAt(index).getDisplayableText() == null) {
                    Entry current = listData.getCurrent();
                    if (current instanceof AbstractRecording) {
                        AbstractRecording r = (AbstractRecording) current;
                        String remained = Formatter.getCurrent().getDurationText4(r.getRecordedDuration()-r.getMediaTime());
                        String text = DataCenter.getInstance().getString("List.remaining");
                        text = TextUtil.replace(text, "%%", remained);
                        buttons.getItemAt(index).setDisplayableText(text);
                    }
                } else {
                    buttons.getItemAt(index).setDisplayableText(null);
                }
                repaint();
            }
        //<-
        }
    }
    //->Kenneth[2017.3.15] R7.3
    private void resetResumeViewingButtonTimer() {
        if (resumeViewingButtonTimer == null) return;
        TVTimer.getTimer().deschedule(resumeViewingButtonTimer);
        try {
            TVTimer.getTimer().scheduleTimerSpec(resumeViewingButtonTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
        int index = buttons.indexOf(MenuItems.RESUME_VIEWING);
        if (index >= 0) {
            buttons.getItemAt(index).setDisplayableText(null);
        }
        repaint();
    }
    //<-

    public void repaintImage() {
        try {
            Rectangle r = ((ListOptionPanelRenderer)renderer).getListFocusBounds(this);
            repaint(r.x, r.y, r.width, r.height);
        } catch (Exception ex) {
        }
    }

    /** Issue 변경. */
    public void issueChanged(Issue old, Issue issue) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.issueChanged()");
        updateWorker.pushIssueUpdateJob();
    }

    /** Recording 추가 삭제를 감지하여 list update 한다. */
    public void recordingChanged(RecordingChangedEvent evt) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.recordingChanged("+evt+")");
        int oldState = evt.getOldState();
        int newState = evt.getState();
        int change = evt.getChange();
        boolean oldAccept = false;
        boolean newAccept = false;
        RecordingRequest rr = evt.getRecordingRequest();
        switch (change) {
            case RecordingChangedEvent.ENTRY_ADDED:
                newAccept = listFilter.accept(rr);
                break;
            case RecordingChangedEvent.ENTRY_DELETED:
                oldAccept = listFilter.accept(oldState);
                break;
            case RecordingChangedEvent.ENTRY_STATE_CHANGED:
                newAccept = listFilter.accept(rr);
                oldAccept = listFilter.accept(oldState);
                break;
        }
        if (oldAccept != newAccept) {
            // WITH_ERROR_STATE는 recorded list 에서 acceptable 하지만 service가 없으면 보이지 않는다.
            // 따라서 oldAccept == false, newAccept == true 인 경우에는 old recording이 없게 된다
            if (newAccept) {
                if (Log.INFO_ON) {
                    Log.printInfo(logPrefix + ".recordingChanged: added");
                }
                updateWorker.pushAddJob(rr);
            } else {
                if (Log.INFO_ON) {
                    Log.printInfo(logPrefix + ".recordingChanged: deleted");
                }
                updateWorker.pushRemoveJob(rr);
            }
        } else if (newAccept) {
            // state 바뀐 경우
            if (Log.INFO_ON) {
                Log.printInfo(logPrefix + ".recordingChanged: changed");
            }
            updateWorker.pushChangeJob(rr);
        }
    }

    public abstract MenuItem createButtons(Entry entry);

    public abstract MenuItem createOptions(Entry entry);

    abstract class ListPinListener implements PinEnablerListener {
        boolean releasePin;

        public ListPinListener(boolean release) {
            releasePin = release;
        }

        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug(App.LOG_HEADER+"ListPanel.receivePinEnablerResult: result = " + response + " detail = " + detail);
            if (response != PreferenceService.RESPONSE_SUCCESS) {
                return;
            }
            if (releasePin) {
                ParentalControl.pinChecked = true;
            }
            processPinAction();
            focusChanged();
            repaint();
        }

        public abstract void processPinAction();

    }

    public void updateFull() {
        Log.printDebug(App.LOG_HEADER+"ListPanel.updateFull()");
        updateWorker.pushFullUpdateJob();
    }

    public void updateRecording(RecordingRequest r) {
        Log.printDebug(App.LOG_HEADER+"ListPanel.updateRecording()");
        if (listFilter.accept(r)) {
            updateWorker.pushChangeJob(r);
        }
    }

    class UpdateWorker extends Worker {

        public UpdateWorker() {
            super(logPrefix + ".Worker", true);
        }

        public synchronized void pushFullUpdateJob() {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushFullUpdateJob()");
            boolean removed = remove(fullUpdateJob);
            if (removed) {
                Log.printInfo(threadName + " : full update : replacing");
            }
            push(fullUpdateJob);
        }

        public void pushAddJob(RecordingRequest r) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushAddJob()");
            pushJob(ListData.RECORDING_ADDED, r);
        }

        public void pushChangeJob(RecordingRequest r) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushChangeJob()");
            pushJob(ListData.RECORDING_CHANGED, r);
        }

        public void pushRemoveJob(RecordingRequest r) {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushRemoveJob()");
            pushJob(ListData.RECORDING_REMOVED, r);
        }

        public void pushRemoteUpdateJob() {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushRemoveUpdateJob()");
            pushJob(ListData.REMOTE_CHANGED, getDeviceList());
        }

        public void pushIssueUpdateJob() {
            Log.printDebug(App.LOG_HEADER+"ListPanel.UpdateWorker.pushIssueUpdateJob()");
            pushJob(ListData.ISSUE_CHANGED, null);
        }

        public synchronized void pushJob(byte type, Object obj) {
            push(new ListUpdateJob(type, obj));
        }
    }

    class ListUpdateJob implements Runnable {
        byte type;
        Object obj;

        public ListUpdateJob(byte type, Object obj) {
            this.type = type;
            this.obj = obj;
        }

        public void run() {
            synchronized (buildLock) {
                Log.printDebug(App.LOG_HEADER+"ListPanel.ListUpdateJob.run()");
                SortingOption targetOption = sortingOption;
                if (type == ListData.REMOTE_CHANGED) {
                    // Device가 추가되면 terminal 대신 title 로 sorting 된 놈을
                    // 다시 terminal 로 돌려줘야 한다.
                    if (!sortingOption.getKey().equals(preferredSortingKey)) {
                        Log.printInfo(logPrefix + ": different sorting option !");
                        if (sortingOption == getDefaultSortingOption()) {
                            SortingOption so = (SortingOption) getSortingMenu().find(preferredSortingKey);
                            if (so != null) {
                                Log.printInfo(logPrefix + ": will use sorting option : " + so);
                                targetOption = so;
                            }
                        }
                    } else {
                        SortingOption so = (SortingOption) getSortingMenu().find(preferredSortingKey);
                        Log.printDebug(logPrefix + ": check = " + so);
                        if (so == null) {
                            Log.printInfo(logPrefix + ": will use default sorting option");
                            targetOption = getDefaultSortingOption();
                        }
                    }
                }
                Log.printDebug(App.LOG_HEADER+"ListPanel.ListUpdateJob.run() : Call buildList()");
                buildList(targetOption, type, obj);
            }
        }

        public String toString() {
            return "ListUpdate[" + ListData.getUpdateTypeString(type) + "]";
        }

    }

}

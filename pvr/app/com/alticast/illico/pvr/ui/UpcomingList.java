package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.*;
import java.util.*;
import javax.tv.util.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;

/**
 * Upcoming List.
 *
 * @author  June Park
 */
public class UpcomingList extends ListPanel {

    MenuItem buttonEmpty = new MenuItem("");

    MenuItem buttonUnblock = new MenuItem("", new MenuItem[] {
        MenuItems.UNBLOCK_RECORDING
    });

    MenuItem buttonCancel = new MenuItem("", new MenuItem[] {
        MenuItems.CANCEL
    });

    MenuItem buttonViewCancel = new MenuItem("", new MenuItem[] {
        MenuItems.VIEW_EPISODES, MenuItems.CANCEL_SERIES
    });

    MenuItem buttonBackCancel = new MenuItem("", new MenuItem[] {
        MenuItems.BACK, MenuItems.CANCEL_SERIES
    });

    MenuItem buttonPlayStopEditMore = new MenuItem("", new MenuItem[] {
        MenuItems.PLAY, MenuItems.STOP, MenuItems.EDIT, MenuItems.MORE_DETAILS
    });

    MenuItem buttonSkipEditCancelMore = new MenuItem("", new MenuItem[] {
        MenuItems.SKIP, MenuItems.EDIT, MenuItems.CANCEL, MenuItems.MORE_DETAILS
    });

    MenuItem buttonSeriesSkipEditCancelMore = new MenuItem("", new MenuItem[] {
//        MenuItems.SKIP, MenuItems.EDIT_SERIES, MenuItems.CANCEL_SERIES, MenuItems.MORE_DETAILS
        MenuItems.SKIP, MenuItems.EDIT, MenuItems.CANCEL_SERIES, MenuItems.MORE_DETAILS
    });

    MenuItem buttonSeriesResetEditCancelMore = new MenuItem("", new MenuItem[] {
//        MenuItems.RESET, MenuItems.EDIT_SERIES, MenuItems.CANCEL_SERIES, MenuItems.MORE_DETAILS
        MenuItems.RESET, MenuItems.EDIT, MenuItems.CANCEL_SERIES, MenuItems.MORE_DETAILS
    });

    MenuItem buttonResetEditCancelMore = new MenuItem("", new MenuItem[] {
        MenuItems.RESET, MenuItems.EDIT, MenuItems.CANCEL, MenuItems.MORE_DETAILS
    });

    MenuItem menuSort = new MenuItem("option.Sort", new MenuItem[] {
        SORT_TITLE, SORT_DATE
    });

    private static SortingOption SORT_TITLE = new TitleSortingOption(true);
    private static SortingOption SORT_DATE = new DateSortingOption(SortingOption.GROUP, true);


    private static UpcomingList instance = new UpcomingList();
    public static UpcomingList getInstance() {
        return instance;
    }

    private UpcomingList() {
        super(UPCOMING_LIST, new ListUIFilter(RecordingListManager.cancelableFilter));
    }

    protected SortingOption getDefaultSortingOption() {
        return SORT_DATE;
    }

    protected ListPanelRenderer createRenderer() {
        return new UpcomingListRenderer();
    }

    protected MenuItem getSortingMenu() {
        return menuSort;
    }

    protected Vector getDeviceList() {
        return null;
    }

    protected void appendAdditionalData(ListData newData) {
        Log.printDebug(App.LOG_HEADER+"UpcomingList.appendAdditionalData("+newData+")");
        RecordingScheduler rs = RecordingScheduler.getInstance();
        Vector vector = rs.getInactiveList();
        int size = vector.size();
        Log.printDebug(App.LOG_HEADER+"UpcomingList.appendAdditionalData() : inactive size = "+size);
        if (size == 0) {
            return;
        }
        Vector entries = newData.getTotalList();
        Vector children = new Vector(size);
        entries.addElement(new InactiveSeparator(children));

        for (int i = 0; i < size; i++) {
            InactiveSeries is = new InactiveSeries(rs.get((String) vector.elementAt(i)));
            children.addElement(is);
            entries.addElement(is);
        }
    }

    public MenuItem createButtons(Entry entry) {
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            if (ParentalControl.isAccessBlocked(rec)) {
                return buttonUnblock;
            }
            short stateGroup = rec.getStateGroup();
            boolean single = rec.getGroupKey() == null;
            if (stateGroup == RecordingListManager.IN_PROGRESS) {
                return buttonPlayStopEditMore;
            } else {
                boolean skipped = rec.getBoolean(AppData.UNSET);
                if (skipped) {
                    return single ? buttonResetEditCancelMore : buttonSeriesResetEditCancelMore;
                } else {
                    return single ? buttonSkipEditCancelMore : buttonSeriesSkipEditCancelMore;
                }
            }
        } else if (entry instanceof RecordingFolder) {
            return listData.getCurrentFolder() == null ? buttonViewCancel : buttonBackCancel;
        } else if (entry instanceof InactiveSeries) {
            return buttonCancel;
        } else {
            return buttonEmpty;
        }
    }

    public MenuItem createOptions(Entry entry) {
        MenuItem root = new MenuItem("ListOptions");
        if (Environment.SUPPORT_DVR) {
            root.add(MenuItems.GO_RECORDED);
            root.add(MenuItems.GO_MANUAL);
            root.add(MenuItems.GO_MANAGEMENT);
        }
        root.add(menuSort);
        //->Kenneth[2017.3.16] R7.3 : Search 를 D-option 에 추가
        root.add(MenuItems.SEARCH);
        //<-
        if (Environment.SUPPORT_DVR) {
            root.add(MenuItems.GO_PREFERENCES);
        }
        if (entry instanceof AbstractRecording) {
            if (((AbstractRecording) entry).isBlocked()) {
                root.add(MenuItems.UNBLOCK_MANUALLY);
//                } else {
//                    root.add(MenuItems.BLOCK_RECORDING);
            }
        }
        root.add(MenuItems.GO_HELP);
        root.add(MenuItems.getParentalControlMenu());
        return root;
    }

}

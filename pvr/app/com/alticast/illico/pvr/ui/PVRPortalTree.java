package com.alticast.illico.pvr.ui;

import java.awt.Point;
import java.awt.Rectangle;

import org.havi.ui.event.HRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.MenuController;
import com.alticast.illico.pvr.gui.PVRPortalTreeRenderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;

public class PVRPortalTree extends UIComponent {
    private static final long serialVersionUID = 1112941331020827045L;

    private static final int TREE_X = 0;
    private static final int TREE_Y = 58;
    private static final int TREE_WIDTH = 412;
    private static final int TREE_HEIGHT = 482;
    public static final Rectangle BOUNDS_TREE = new Rectangle(TREE_X, TREE_Y, TREE_WIDTH, TREE_HEIGHT);

    private ClickingEffect clickEffect;

    private int currentIndex;
    public Issue issue;

    AlphaEffect initStart;
    MovingEffect backStart;

    public PVRPortalTreeRenderer treeRenderer;

    public PVRPortalTree(final UIComponent parentUIComponent) {
        if (treeRenderer == null) {
            treeRenderer = new PVRPortalTreeRenderer();
        }

        setRenderer(treeRenderer);
    }

    public void start() {
        Log.printDebug("portal tree start");

        prepare();

        if (initStart == null) {
            initStart = new AlphaEffect(treeRenderer.treeText, 15, MovingEffect.FADE_IN);
            Log.printDebug("first = " + initStart);
        }

        if (backStart == null) {
            backStart = new MovingEffect(treeRenderer.treeText, 15, new Point(-15, 0), new Point(0, 0),
                    MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
            Log.printDebug("back = " + backStart);
        }
        setVisible(true);
    }

    public void effectScene(boolean s) {
        treeRenderer.focusViewing = true;
        treeRenderer.treeText.setVisible(true);

    }

    public void effectStart() {
        Log.printDebug("PVRPortalTree initStart start");
        initStart.start();
    }

    public void stop() {
        if(treeRenderer.treeText != null) {
            treeRenderer.treeText.setVisible(false);
        }
        issue = null;
    }

    public boolean handleKey(int keyCode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PVRPortalTree.handleKey]keyCode:" + keyCode);
        }
        switch (keyCode) {
        case HRcEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"PVRPortalTree.handlKey(VK_ENTER)");
            // warning
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
                clickEffect.setClipBounds(0, 0, 412, 248);
            }
            clickEffect.start(48, 44 + (currentIndex * 34), 304, 42);

            if (currentIndex == PVRPortal.menuKeys.length) {
                IssueManager.getInstance().showIssue();
            } else {
                int pvrSvcId = PVRPortal.menuKeys[currentIndex].getSceneCode();
                MenuController.getInstance().showMenu(pvrSvcId);
                PvrVbmController.getInstance().writePvrButtonSelection(pvrSvcId);
            }
            return true;
        case HRcEvent.VK_UP:
            if (currentIndex == 0) {
                return true;
            }
            currentIndex--;
            repaint();
            return true;
        case HRcEvent.VK_DOWN:
            if (currentIndex >= PVRPortal.menuKeys.length - (issue != null ? 0 : 1)) {
                return true;
            }
            currentIndex++;
            Log.printDebug("currentIndex =  " + currentIndex);
            repaint();
            return true;
        case KeyCodes.LAST:
            return true;
        }
        return false;
    }

    /*****************************************************************
     * methods - PVRPortal-related
     *****************************************************************/

    public void setIndex(int index) {
        currentIndex = index;
    }

    public int getCurrentIndex() {
        return currentIndex;
    }

    public void setIssue(Issue issue) {
        this.issue = issue;
        if (issue == null && currentIndex >= PVRPortal.menuKeys.length) {
            currentIndex = 0;
        }
        repaint();
    }

    public void animationEnded(Effect effect) {
    }
}

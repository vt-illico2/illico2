package com.alticast.illico.pvr.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Calendar;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.Constants;

public class ITVCalendar extends Popup {

    public DataCenter dataCenter = DataCenter.getInstance();

    private String cur_month[][] = new String[6][7];

    private int focusYear = 2010;
    private int focusMonth = 5;
    private int focusDay = 0;
    private int focusWeeks = 0;

    private int calendarIndexX = 0;
    private int calendarIndexY = 0;

    private int currentMonth;
    private int currentYear;
    private int currentDay;

    private boolean active = false;

    private String stringWeek[] = {dataCenter.getString("pvr.SUN"), dataCenter.getString("pvr.MON"), dataCenter.getString("pvr.TUS"),
            dataCenter.getString("pvr.WEN"), dataCenter.getString("pvr.THU"), dataCenter.getString("pvr.FRI"),
            dataCenter.getString("pvr.SAT")};

    String[] month = {dataCenter.getString("manual.JAN"), dataCenter.getString("manual.FEB"),
            dataCenter.getString("manual.MAR"), dataCenter.getString("manual.APR"), dataCenter.getString("manual.MAY"),
            dataCenter.getString("manual.JUNE"), dataCenter.getString("manual.JULY"), dataCenter.getString("manual.AUG"),
            dataCenter.getString("manual.SEP"), dataCenter.getString("manual.OCT"), dataCenter.getString("manual.NOV"),
            dataCenter.getString("manual.DEC")};

    String changeMonth = dataCenter.getString("pvr.changeMonth");

    public ITVCalendar() {
        setRenderer(new CalendarGraphis());
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ITVCalendar.start()");
        this.setVisible(true);
        Calendar cal = Calendar.getInstance();
        focusYear = cal.get(Calendar.YEAR);
        focusMonth = cal.get(Calendar.MONTH) + 1;
        focusDay = cal.get(Calendar.DAY_OF_MONTH);
        focusWeeks = cal.get(Calendar.DAY_OF_WEEK) - 1;

        currentYear = focusYear;
        currentMonth = focusMonth;
        currentDay = focusDay;

        setDate(focusYear, focusMonth, focusDay);
        active = true;
        super.start();
    }

    public boolean isActive() {
        return active;
    }

    private String getDate() {
        String date = "";
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, focusYear);
        ca.set(Calendar.MONTH, focusMonth - 1);
        ca.set(Calendar.DAY_OF_MONTH, getDay(calendarIndexX, calendarIndexY));
        ca.set(Calendar.SECOND, 0);
        ca.set(Calendar.MILLISECOND, 0);

        Log.printInfo("Calendar = " + ca.getTime());
        if (calendarIndexY == 0 && getDay(calendarIndexX, calendarIndexY) > 20) {
            ca.add(Calendar.MONTH, -1);
        } else if (calendarIndexY == (cur_month.length - 1) && getDay(calendarIndexX, calendarIndexY) < 8) {
            ca.add(Calendar.MONTH, +1);
        }
        Log.printInfo("Calendar = " + ca.getTime());

        int num = ca.get(Calendar.DAY_OF_WEEK) - 1;
        if (num == 0) {
            num = 6;
        }
        // date = stringWeek[num - 1] + ". " + month[focusMonth - 1].substring(0, 3) + ". " +
        // ca.get(Calendar.DAY_OF_MONTH);
        date = stringWeek[num - 1] + ". " + month[ca.get(Calendar.MONTH)].substring(0, 3) + ". "
                + ca.get(Calendar.DAY_OF_MONTH);
        Log.printInfo("date = " + date);
        return date;
    }

    public long getSelectedDay() {
        return getCalendar().getTimeInMillis();
    }

    public Calendar getCalendar() {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, focusYear);
        ca.set(Calendar.MONTH, focusMonth - 1);
        ca.set(Calendar.DAY_OF_MONTH, getDay(calendarIndexX, calendarIndexY));
        ca.set(Calendar.HOUR_OF_DAY, 23);
        ca.set(Calendar.MINUTE, 59);
        ca.set(Calendar.SECOND, 59);
        ca.set(Calendar.MILLISECOND, 0);

        if (calendarIndexY == 0 && getDay(calendarIndexX, calendarIndexY) > 20) {
            ca.add(Calendar.MONTH, -1);
        } else if (calendarIndexY == (cur_month.length - 1) && getDay(calendarIndexX, calendarIndexY) < 8) {
            ca.add(Calendar.MONTH, +1);
        }
        return ca;
    }

    public void showITVCalendar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ITVCalendar.showITVCalendar()");
        Calendar calendar = Calendar.getInstance();
        setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        prepare();
    }

    public void hideITVCalendar() {
        setVisible(false);
    }

    public void movePreWeek() {
        calendarIndexY--;
        int nextDay = getDay(calendarIndexX, calendarIndexY);

        if (calendarIndexY < 0 || (nextDay > 20 && calendarIndexY == 0) || nextDay == 0) {
            focusMonth--;

            if (focusMonth < 1) {
                focusMonth = 12;
                focusYear--;
            }

            Calendar c1 = Calendar.getInstance();
            c1.set(Calendar.YEAR, focusYear);
            c1.set(Calendar.MONTH, focusMonth - 1);

            setDate(focusYear, focusMonth, c1.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
    }

    private void moveNextWeek() {
        calendarIndexY++;
        int nextDay = getDay(calendarIndexX, calendarIndexY);

        if (calendarIndexY > cur_month.length || (nextDay < 8 && calendarIndexY >= 4) || nextDay == 0) {
            calendarIndexY = 0;
            focusMonth++;

            if (focusMonth > 12) {
                focusMonth = 1;
                focusYear++;
            }
            setDate(focusYear, focusMonth, 1);
        }
    }

    private void moveNextDay() {
        calendarIndexX++;

        if (calendarIndexX > 6) {
            calendarIndexX = 0;
            calendarIndexY++;
        }

        int nextDay = getDay(calendarIndexX, calendarIndexY);

        if (nextDay == 0 || (nextDay < 8 && calendarIndexY >= 4)) {
            focusMonth++;

            if (focusMonth > 12) {
                focusMonth = 1;
            }
            setDate(focusYear, focusMonth, 1);
        }
        repaint();
    }

    private void movePreDay() {
        calendarIndexX--;

        if (calendarIndexX < 0) {
            calendarIndexX = 6;
            calendarIndexY--;
        }
        int nextDay = getDay(calendarIndexX, calendarIndexY);
        if (nextDay == 0 || (nextDay > 20 && calendarIndexY == 0)) {
            focusMonth--;

            if (focusMonth < 1) {
                focusMonth = 12;
                focusYear--;
            }

            Calendar c1 = Calendar.getInstance();
            c1.set(Calendar.YEAR, focusYear);
            c1.set(Calendar.MONTH, focusMonth - 1);

            setDate(focusYear, focusMonth, c1.getActualMaximum(Calendar.DAY_OF_MONTH));
            // setDate(focusYear, focusMonth, 0);
        }
    }

    private int getDay(int X, int Y) {
        int day = 0;
        try {
            day = Integer.parseInt(cur_month[Y][X]);
        } catch (Exception e) {
            day = 0;
        }
        Log.printInfo("day = " + day);
        return day;
    }

    private int getMemoIndex() {
        return ((focusYear * 1000) + (focusMonth * 100) + focusDay);
    }

    private void movePrevMonth(int curM) {
        curM--;
        if (curM < 1) {
            curM = 12;
            focusYear--;
        }
        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.YEAR, focusYear);
        c1.set(Calendar.MONTH, curM -1);

        setDate(focusYear, curM, c1.getActualMaximum(Calendar.DAY_OF_MONTH));
    }

    private void moveNextMonth(int curM) {
        curM++;
        if (curM > 12) {
            curM = 1;
            focusYear++;
        }

        setDate(focusYear, curM, 1);
    }

    private void movepreYear(int curY) {
        curY--;
        if (curY < 1) {
            curY = Calendar.getInstance().get(Calendar.YEAR);
        }

        setDate(curY, focusMonth, 0);
    }

    private void moveNextYear(int curY) {
        curY++;
        setDate(curY, focusMonth, 0);
    }

    private void setDate(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        if (day != 0) {
            calendar.set(Calendar.DAY_OF_MONTH, day);
        }
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        focusYear = calendar.get(Calendar.YEAR);
        focusMonth = calendar.get(Calendar.MONTH) + 1;
        focusDay = calendar.get(Calendar.DAY_OF_MONTH);

        cur_month = null;
        cur_month = new String[6][7];

        for (int a = calendar.getMinimum(Calendar.DAY_OF_MONTH); a <= calendar.getActualMaximum(Calendar.DAY_OF_MONTH); a++) {
            calendar.set(Calendar.DATE, a);

            int weekOfMonth = calendar.get(Calendar.WEEK_OF_MONTH);
            int dayOfweek = calendar.get(Calendar.DAY_OF_WEEK);

            cur_month[weekOfMonth - 1][dayOfweek - 1] = String.valueOf(a);

            if (cur_month[weekOfMonth - 1][dayOfweek - 1] != null
                    && focusDay == Integer.parseInt(cur_month[weekOfMonth - 1][dayOfweek - 1])) {
                calendarIndexX = dayOfweek - 1;
                calendarIndexY = weekOfMonth - 1;
            }
        }

        int m1 = calendar.get(Calendar.MONTH) - 1;
        Log.printDebug("m1 " + m1);

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.MONTH, m1);
        int lastDayOfPreMonth = c1.getActualMaximum(Calendar.DAY_OF_MONTH);

        for (int a = 6; a > -1; a--) {
            if (cur_month[0][a] == null || cur_month[0][a].length() == 0) {
                cur_month[0][a] = String.valueOf(lastDayOfPreMonth);
                lastDayOfPreMonth--;
            }
        }

        int firstDayOfNextMonth = 1;
        boolean pushDay = true;
        for (int a = 4; a < 6; a++) {
            // if (!pushDay)
            // break;
            for (int b = 0; b < 7; b++) {
                if (cur_month[a][b] == null) {
                    cur_month[a][b] = String.valueOf(firstDayOfNextMonth);
                    firstDayOfNextMonth++;
                    pushDay = false;
                }
            }
        }
    }

    private boolean checkMonth(int unit) {
        Calendar cal = getCalendar();
        cal.add(unit, -1);
        int year = cal.get(Calendar.YEAR);
        if (year < currentYear) {
            return false;
        } else if (year > currentYear) {
            return true;
        }
        return cal.get(Calendar.MONTH) >= (currentMonth - 1);
    }

    public boolean handleKey(int type) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ITVCalendar.handleKey("+type+")");
        switch (type) {
        case KeyCodes.FORWARD:
        case HRcEvent.VK_PAGE_UP:
            moveNextMonth(focusMonth);
            break;
        case HRcEvent.VK_PAGE_DOWN:
        case KeyCodes.BACK:
            if (checkMonth(Calendar.MONTH)) {
                movePrevMonth(focusMonth);
            }
            break;
        case KeyEvent.VK_RIGHT:
            moveNextDay();
            break;
        case KeyEvent.VK_LEFT:
            if (checkMonth(Calendar.DAY_OF_MONTH)) {
                movePreDay();
            }
            break;
        case KeyEvent.VK_UP:
            if (checkMonth(Calendar.WEEK_OF_MONTH)) {
                movePreWeek();
            }
            break;
        case KeyEvent.VK_DOWN:
            moveNextWeek();
            break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
            if (Calendar.getInstance().before(getCalendar())) {
                active = false;
                if (listener != null) {
                    listener.selected(0);
                }
                close();
            }
            break;
//        case KeyCodes.LAST:
        case OCRcEvent.VK_EXIT:
            close();
            break;
        default:
            return false;
        }

        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(360 + 32 * calendarIndexX, 160 + 27 * calendarIndexY, 34, 31);
    }

    public class CalendarGraphis extends Renderer {

        Image calendar_bg;
        Image line_240;
        Image focus_m_240;
        Image line_m_240;
        Image btn_skip;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        private boolean checkAvailable(int a, int b) {
            if (a == 0 && Integer.parseInt(cur_month[0][b]) > 8) {
                return false;
            }
            int d = Integer.parseInt(cur_month[a][b]);
            if ((a == 4 || a == 5) && d < 15) {
                return false;
            }
            if (currentYear == focusYear && currentMonth == focusMonth && d < currentDay) {
                return false;
            }
            return true;
        }

        protected void paint(Graphics g, UIComponent c) {
//            g.setColor(GraphicsResource.cDimmedBG);
//            g.fillRect(0, 0, 960, 540);
            g.drawImage(calendar_bg, 349, 111, c);

            g.drawImage(line_240, 360, 188, c);
            g.drawImage(line_240, 360, 216, c);
            g.drawImage(line_240, 360, 242, c);
            g.drawImage(line_240, 360, 269, c);
            g.drawImage(line_240, 360, 297, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_200_200_200);
            GraphicUtil.drawStringCenter(g, month[focusMonth - 1], 349 + 124, 133);

            g.drawImage(line_m_240, 353, 140, c);

            for (int a = 0; a < cur_month.length; a++) {
                for (int b = 0; b < cur_month[a].length; b++) {
                    if (cur_month[a][b] != null) {
                        if (!"0".equals(cur_month[a][b])) {
                            if (checkAvailable(a, b)) {
                                g.setColor(GraphicsResource.C_193_191_191);
                                g.setFont(GraphicsResource.BLENDER18);
                            } else {
                                g.setColor(GraphicsResource.C_124_124_124);
                                g.setFont(GraphicsResource.BLENDER18);
                            }
                            if (calendarIndexX == b && calendarIndexY == a) {
                                g.drawImage(focus_m_240, 360 + b * 32, 160 + a * 27, c);
                                g.setColor(GraphicsResource.C_4_4_4);
                                g.setFont(GraphicsResource.BLENDER20);
                                GraphicUtil.drawStringCenter(g, cur_month[a][b], 377 + (b * 32), 183 + (a * 27));
                            }

                            if (!(calendarIndexX == b && calendarIndexY == a)) {
                                GraphicUtil.drawStringCenter(g, cur_month[a][b], 377 + (b * 32), 182 + (a * 27));
                            }
                        }
                    }

                    g.setFont(GraphicsResource.BLENDER14);
//                    g.setColor(focusWeeks == b ? GraphicsResource.C_255_255_255 : GraphicsResource.C_140_139_139);
                    g.setColor(calendarIndexX == b ? GraphicsResource.C_255_255_255 : GraphicsResource.C_140_139_139);
                    if (a == 0) {
                        g.drawString(stringWeek[b], 365 + b * 32, 155);
                    }
                }
            }
            if (btn_skip != null) {
                g.drawImage(btn_skip, 448, 330, c);
            }

            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_241_241_241);
            g.drawString(changeMonth, 504, 345);
        }

        public void prepare(UIComponent c) {
            calendar_bg = DataCenter.getInstance().getImage("calendar_bg.png");

            line_240 = DataCenter.getInstance().getImage("line_240.png");
            focus_m_240 = DataCenter.getInstance().getImage("focus_s_240.png");

            line_m_240 = DataCenter.getInstance().getImage("line_m_240.png");
            if (MenuController.getInstance().imageTable != null) {
                btn_skip = (Image) MenuController.getInstance().imageTable.get(PreferenceService.BTN_SKIP);
            } else {
                btn_skip = DataCenter.getInstance().getImage("b_btn_skip.png");
            }
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

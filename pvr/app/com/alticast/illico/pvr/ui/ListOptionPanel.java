package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.*;
import java.util.Date;
import java.awt.*;

public abstract class ListOptionPanel extends OptionPanel {

    public ListData listData = null;

    public ListOptionPanel() {
    }

}

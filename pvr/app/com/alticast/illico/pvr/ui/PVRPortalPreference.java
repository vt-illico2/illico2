package com.alticast.illico.pvr.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.rmi.*;

import org.havi.ui.event.HRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.gui.PVRPortalPreferenceUIComponent;
import com.alticast.illico.pvr.gui.ScaleUPList;
import com.alticast.illico.pvr.gui.PVRPortalPreferenceUIComponent.PVRPortalPreferenceRenderer;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.keyboard.*;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.*;
import com.videotron.tvi.illico.util.*;

public class PVRPortalPreference extends FullScreenPanel implements KeyboardListener {

//    public static final int PREFERENCE_RECORD_BUFFER = 0;
//    public static final int PREFERENCE_DEFAULT_SAVE_TIME = 1;
//    public static final int PREFERENCE_MULTIROOM_SUPPORT = 2;
//    public static final int PREFERENCE_TERMINAL_NAME = 3;

    BaseUIRenderer baseUIRenderer = new BaseUIRenderer();
    private PVRPortalPreferenceUIComponent pvrPortalPreferenceUIComponent;
    private PVRPortalPreferenceRenderer pvrPortalPreferenceRenderer;

    public ScaleUPList scaleUPList = new ScaleUPList();

    public String[] list0;
    public String[] list1;
    public String[] list2;
    public String[] list3;
    public String[] list4;

//    private final int[] preferenceListOrder = {PREFERENCE_RECORD_BUFFER, PREFERENCE_DEFAULT_SAVE_TIME,
//            PREFERENCE_TERMINAL_NAME, PREFERENCE_MULTIROOM_SUPPORT};
//
//    private final String[] txtPreferenceListKey = new String[] {"pvrportal.record_buffer",
//            "pvrportal.default_save_time", "pvrportal.terminal_name", "pvrportal.multiroom_support"};
//    private String preferenceTitleKey;

    public PvrFooter footer;

    private int optionSize;
    public boolean editing = false;

    public PVRPortalPreference() {
        setRenderer(baseUIRenderer);
        this.setBounds(0, 0, 960, 540);
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortalReference.start()");
        optionSize = App.SUPPORT_HN ? 4 : 3;
        if(footer == null) {
            footer = new PvrFooter();
            add(footer);
            footer.setBounds(0, 488, 906, 25);
        }

        list0 = new String[] {
            Definitions.RECORD_BUFFER_0MIN,
            Definitions.RECORD_BUFFER_1MIN,
            Definitions.RECORD_BUFFER_2MIN,
            Definitions.RECORD_BUFFER_5MIN,
            Definitions.RECORD_BUFFER_10MIN,
            Definitions.RECORD_BUFFER_15MIN,
        };
        list1 = new String[] {dataCenter.getString("pvrportal.Until"),
                dataCenter.getString("pvrportal.7days"),
                dataCenter.getString("pvrportal.14days")};
        list2 = new String[] {dataCenter.getString("pvrportal.All"),
                dataCenter.getString("pvrportal.Last3"),
                dataCenter.getString("pvrportal.Last5")};

        resetTerminalNames(dataCenter.getString(PreferenceNames.TERMINAL_OTHER_NAME));
        list4 = new String[] {dataCenter.getString("pvrportal.enabled"),
                dataCenter.getString("pvrportal.disabled") };

        if (scaleUPList == null) {
            scaleUPList = new ScaleUPList();
        }

        if (pvrPortalPreferenceUIComponent == null) {
            pvrPortalPreferenceUIComponent = new PVRPortalPreferenceUIComponent(this);
            pvrPortalPreferenceRenderer = pvrPortalPreferenceUIComponent.rendar;
            add(pvrPortalPreferenceUIComponent, 0);
            pvrPortalPreferenceUIComponent.setBounds(0, 0, 960, 540);
        }

        prepare();

        footer.setVisible(true);
        pvrPortalPreferenceUIComponent.setVisible(true);
        scaleUPList.setVisible(false);
        this.setVisible(true);
        repaint();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortalReference.stop()");
        if (footer != null) {
            remove(footer);
            footer = null;
        }

        if (scaleUPList != null) {
            remove(scaleUPList);
            scaleUPList = null;
        }

        if (pvrPortalPreferenceUIComponent != null) {
            remove(pvrPortalPreferenceUIComponent);
            pvrPortalPreferenceUIComponent = null;
        }

        focus = 0;
        this.removeAll();
        closeVirtualKeyboard();
        this.setVisible(false);
    }

    private void resetTerminalNames(String other) {
        String[] list;
        if (other.length() == 0) {
            list = new String[6];
        } else {
            list = new String[7];
        }
        for (int i = 0; i < list.length; i++) {
            if (i == 6) {
                list[i] = other;
            } else {
                list[i] = dataCenter.getString("multiroom.name" + i);
            }
        }
        this.list3 = list;
    }

    public void scaleUp() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortalReference.scaleUp()");
        this.remove(scaleUPList);
        switch (focus) {
        case 0:
            this.add(scaleUPList, 0);
            scaleUPList.setBounds(new Rectangle(519, 83 - 25, 234, 264));
            scaleUPList.setItem(list0);
            scaleUPList.setSelectedIndex(dataCenter.getString(PreferenceNames.RECORD_BUFFER));
            scaleUPList.setTitle(pvrPortalPreferenceRenderer.getFocusListString());
            scaleUPList.setVisible(true);
            break;
        case 1:
            {
            this.add(scaleUPList, 0);
            scaleUPList.setBounds(new Rectangle(519, 120 + 11, 234, 120));
            scaleUPList.setItem(list1);
            Object value = dataCenter.getString(PreferenceNames.DEFAULT_SAVE_TIME);
            int focusIndex;
            if (Definitions.DEFAULT_SAVE_TIME_AFTER_7_DAYS.equals(value)) {
                focusIndex = 1;
            } else if (Definitions.DEFAULT_SAVE_TIME_AFTER_14_DAYS.equals(value)) {
                focusIndex = 2;
            } else {
                focusIndex = 0;
            }
            scaleUPList.setSelectedIndex(focusIndex);
            scaleUPList.setTitle(dataCenter.getString("pvrportal.savetime_title"));
            scaleUPList.setVisible(true);
            }
            break;
        case 2:
            {
            this.add(scaleUPList, 0);
            scaleUPList.setBounds(new Rectangle(519, 157 + 22, 234, 120));
            scaleUPList.setItem(list2);
            Object value = dataCenter.getString(PreferenceNames.DEFAULT_SAVE_TIME_REPEAT);
            int focusIndex;
            if (Definitions.DEFAULT_SAVE_TIME_LAST_3.equals(value)) {
                focusIndex = 1;
            } else if (Definitions.DEFAULT_SAVE_TIME_LAST_5.equals(value)) {
                focusIndex = 2;
            } else {
                focusIndex = 0;
            }
            scaleUPList.setSelectedIndex(focusIndex);
            scaleUPList.setTitle(dataCenter.getString("pvrportal.savetime_title"));
            scaleUPList.setVisible(true);
            }
            break;
        case 3:
            {
            this.add(scaleUPList, 0);
            scaleUPList.setBounds(new Rectangle(519, 157 + 22 + 48, 234, 264));
            scaleUPList.setItem(list3);
            String value = pvrPortalPreferenceRenderer.getTerminalValue();
            int focusIndex = 0;
            if (value != null) {
                for (int i = 1; i < list3.length; i++) {
                    if (value.equals(list3[i])) {
                        focusIndex = i;
                        break;
                    }
                }
            }
            scaleUPList.setSelectedIndex(focusIndex);
            scaleUPList.setTitle(pvrPortalPreferenceRenderer.getFocusListString());
            scaleUPList.setVisible(true);
            }
            break;
//        case 4:
//            {
//            this.add(scaleUPList, 0);
//            scaleUPList.setBounds(new Rectangle(519, 157 + 22 + 48 * 2, 234, 120));
//            scaleUPList.setItem(list4);
//            Object value = dataCenter.getString(PreferenceNames.MULTIROOM_SUPPORT);
//            int focusIndex;
//            if (Definitions.OPTION_VALUE_ENABLE.equals(value)) {
//                focusIndex = 0;
//            } else {
//                focusIndex = 1;
//            }
//            scaleUPList.setSelectedIndex(focusIndex);
//            scaleUPList.setTitle(pvrPortalPreferenceRenderer.getFocusListString());
//            scaleUPList.setVisible(true);
//            }
//            break;
        }
        repaint();
    }

    public boolean handleKey(int keyCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortalReference.handleKey("+keyCode+")");
        boolean toBeConsumed = (keyCode >= HRcEvent.VK_0 && keyCode <= HRcEvent.VK_9);
        if (scaleUPList.isVisible()) {
            boolean result = scaleUPList.handleKey(keyCode);
            if (keyCode == HRcEvent.VK_ENTER) {
                Log.printDebug(App.LOG_HEADER+"PVRPortalPreference.handlKey(VK_ENTER) -- 1");
                remove(scaleUPList);
                setUppData();
            }
            repaint();
            return result || toBeConsumed;
        }
        switch (keyCode) {
        case HRcEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"PVRPortalPreference.handlKey(VK_ENTER) -- 2");
            scaleUp();
            return true;
        case HRcEvent.VK_UP:
            if (focus == 0) {
                return true;
            }
            focus--;
            repaint();
            return true;
        case HRcEvent.VK_DOWN:
            if (focus < optionSize - 1) {
                focus++;
                repaint();
            }
            return true;
        case KeyCodes.LAST:
            footer.clickAnimationByKeyCode(keyCode);
            MenuController.getInstance().showPreviousMenu();
            return true;
        }
        return toBeConsumed;
    }

    public void setUppData() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PVRPortalReference.setUppData()");
        final int focusIndex = scaleUPList.getFocusIndex();
        switch (focus) {
        case 0:
            {
            String value = null;
            if (focusIndex == 0) {
                value = Definitions.RECORD_BUFFER_0MIN;
            } else if (focusIndex == 1) {
                value = Definitions.RECORD_BUFFER_1MIN;
            } else if (focusIndex == 2) {
                value = Definitions.RECORD_BUFFER_2MIN;
            } else if (focusIndex == 3) {
                value = Definitions.RECORD_BUFFER_5MIN;
            } else if (focusIndex == 4) {
                value = Definitions.RECORD_BUFFER_10MIN;
            } else if (focusIndex == 5) {
                value = Definitions.RECORD_BUFFER_15MIN;
            }
            if (value.equals(dataCenter.getString(PreferenceNames.RECORD_BUFFER))) {
                Log.printWarning("PVRPortalPreference: buffer option is not changed");
                return;
            }
            int min = PvrUtil.convertBufferMinute(value);
            String title = dataCenter.getString("pvr.buffer_change_title") + " " + min + " minute";
            if (min > 1) {
                title = title + "s";
            }
            InfoOneButtonPopup bufferChangePopup = new InfoOneButtonPopup(title);
            String desc = TextUtil.replace(dataCenter.getString("pvr.buffer_change_msg"), "|", "\n");
            bufferChangePopup.setDescription(desc);
            final String upValue = value;
            PopupAdapter popupListener = new PopupAdapter() {
                public void selected(int focus) {
                    PreferenceProxy.getInstance().setPreferenceData(PreferenceNames.RECORD_BUFFER, upValue);
                    pvrPortalPreferenceRenderer.setRecordingbufferValue(list0[focusIndex]);
                }
            };
            bufferChangePopup.setListener(popupListener);
            PopupController.getInstance().showPopup(bufferChangePopup);
            }
            break;
        case 1:
            {
            String value = null;
            if (focusIndex == 0) {
                value = Definitions.DEFAULT_SAVE_TIME_UNTIL_I_ERASE;
            } else if (focusIndex == 1) {
                value = Definitions.DEFAULT_SAVE_TIME_AFTER_7_DAYS;
            } else if (focusIndex == 2) {
                value = Definitions.DEFAULT_SAVE_TIME_AFTER_14_DAYS;
            }
            PreferenceProxy.getInstance().setPreferenceData(PreferenceNames.DEFAULT_SAVE_TIME, value);
            pvrPortalPreferenceRenderer.setSaveTime1Value(list1[focusIndex]);
            }
            break;
        case 2:
            {
            String value = null;
            if (focusIndex == 0) {
                value = Definitions.DEFAULT_SAVE_TIME_KEEP_ALL;
            } else if (focusIndex == 1) {
                value = Definitions.DEFAULT_SAVE_TIME_LAST_3;
            } else if (focusIndex == 2) {
                value = Definitions.DEFAULT_SAVE_TIME_LAST_5;
            }

            Log.printDebug("setUppData value = " + value);
            PreferenceProxy.getInstance().setPreferenceData(PreferenceNames.DEFAULT_SAVE_TIME_REPEAT, value);
            pvrPortalPreferenceRenderer.setSaveTime2Value(list2[focusIndex]);
            }
            break;
        case 3:
            {
            String other = dataCenter.getString(PreferenceNames.TERMINAL_OTHER_NAME);
            if (focusIndex == 0 || focusIndex == 6 && other.equals(pvrPortalPreferenceRenderer.getTerminalValue())) {
                KeyboardListenerAction.getInstance().openVirtualKeyboard(
                        KeyboardOption.TYPE_ALPHANUMERIC, 404, 151, other, true, this);
                editing = true;
            } else {
                HomeNetManager.getInstance().setDeviceName(list3[focusIndex]);
                pvrPortalPreferenceRenderer.setTerminalValue(list3[focusIndex]);
            }
            }
            break;
//        case 4:
//            {
//            String value = null;
//            if (focusIndex == 0) {
//                value = Definitions.OPTION_VALUE_ENABLE;
//            } else {
//                value = Definitions.OPTION_VALUE_DISABLE;
//            }
//            Log.printDebug("setUppData value = " + value);
//            PreferenceProxy.getInstance().setPreferenceData(PreferenceNames.MULTIROOM_SUPPORT, value);
//            pvrPortalPreferenceRenderer.setMultiroomValue(list4[focusIndex]);
//            }
//            break;
        }
    }

    /*****************************************************************
     * methods - PVRPortalPreference-related
     *****************************************************************/

    public class BaseUIRenderer extends Renderer {
        Image bg;
        Image hotkeybg;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.drawImage(bg, 0, 0, c);
            g.drawImage(hotkeybg, 242, 466, c);
        }

        public void prepare(UIComponent c) {
            bg = dataCenter.getImage("bg.jpg");
            hotkeybg = dataCenter.getImage("01_hotkeybg.png");
            footer.addButton(PreferenceService.BTN_BACK, "pvr.Back");
        }
    }


    public synchronized void closeVirtualKeyboard() {
        KeyboardListenerAction.getInstance().closeVirtualKeyboard(this);
        editing = false;
    }

    /////////////////////////////////////////////////////////////////////
    // KeyboardListener
    /////////////////////////////////////////////////////////////////////

    public void cursorMoved(int position) throws RemoteException {
    }

    public void focusOut(int direction) throws RemoteException {
    }

    public void inputCanceled(String text) throws RemoteException {
        closeVirtualKeyboard();
    }

    public void inputEnded(String text) throws RemoteException {
        String name;
        text = text.trim();
        if (text.length() == 0) {
            name = dataCenter.getString("multiroom.default_terminal_name");
        } else {
            name = text;
        }
        PreferenceProxy.getInstance().setPreferenceData(PreferenceNames.TERMINAL_OTHER_NAME, text);
        resetTerminalNames(text);
        HomeNetManager.getInstance().setDeviceName(name);
        pvrPortalPreferenceRenderer.setTerminalValue(name);
        closeVirtualKeyboard();
    }

    public void modeChanged(int mode) throws RemoteException {
    }

    public void textChanged(String ext) throws RemoteException {
    }

    public void inputCleared() throws RemoteException {
    }
}

package com.alticast.illico.pvr.ui;

public interface AutoScrollingImageListener {

    public void repaintImage();

}
package com.alticast.illico.pvr.ui;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import java.util.Date;
import java.util.Stack;

public abstract class FullScreenPanel extends UIComponent implements ClockListener {

    protected static DataCenter dataCenter = DataCenter.getInstance();

    public Stack stack = new Stack();

    public FullScreenPanel() {
        setBounds(Constants.SCREEN_BOUNDS);
    }

    public void refresh() {
        repaint();
    }

    public void clockUpdated(Date date) {
        repaint();
    }

}

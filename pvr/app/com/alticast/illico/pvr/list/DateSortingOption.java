package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import java.text.SimpleDateFormat;
import com.videotron.tvi.illico.log.Log;

public class DateSortingOption extends SortingOption {

    private static final Date DEFAULT_DATE = new Date(0);

    boolean ascending;

    public DateSortingOption(byte type, boolean ascending) {
        super("sort.By date recorded", type);
        this.ascending = ascending;
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        Object groupKey = null;
        long time = 0;
        if (entry instanceof AbstractRecording) {
            if (Log.INFO_ON) Log.printInfo("entry : AbstractRecording");
            AbstractRecording rec = (AbstractRecording) entry;
            time = rec.getStartTime();
            //DDC-001 : add yearShift
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(time));
            int yearShift = rec.getInteger(AppData.YEAR_SHIFT);
            if (Log.INFO_ON) Log.printInfo("yearShift in DateSortingOption is "+yearShift+" : "+rec.getIdObject());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ssS");
            if (yearShift > 0) {
                if (Log.INFO_ON) Log.printInfo("old time  = "+sdf.format(c.getTime()));
                c.add(Calendar.YEAR, yearShift);
                time = c.getTime().getTime();
                if (Log.INFO_ON) Log.printInfo("new time  = "+sdf.format(c.getTime()));
            } else {
                if (Log.INFO_ON) Log.printInfo("time  = "+sdf.format(c.getTime()));
            }
            if (type == GROUP) {
                c.set(Calendar.MILLISECOND, 0);
                c.set(Calendar.SECOND, 0);
                c.set(Calendar.MINUTE, 0);
                c.set(Calendar.HOUR_OF_DAY, 0);
                groupKey = c.getTime();
                if (Log.INFO_ON) Log.printInfo("GROUP groupKey : "+groupKey);
            } else if (type == TREE) {
                groupKey = entry.getTitle();
                if (Log.INFO_ON) Log.printInfo("TREE groupKey : "+groupKey);
            }
        } else if (entry instanceof DateSeparator) {
            time = (((DateSeparator) entry).getDate()).getTime();
            if (Log.INFO_ON) Log.printInfo("entry : DateSeperator : "+time);
        } else if (entry instanceof RecordingFolder) {
            if (Log.INFO_ON) Log.printInfo("entry : RecordingFolder");
            time = ((RecordingFolder) entry).getTime();
        }
        if (!ascending) {
            time = -time;
        }
        return new SortingKey(entry, new StringBasedComparable(entry.getTitle(), new Long(time)), groupKey);
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        if (groupKey instanceof Date) {
            return new DateSeparator((Date) groupKey, vector);
        } else if (groupKey instanceof String) {
            return new RecordingFolder((String) groupKey, vector);
        } else {
            Log.printWarning("DateSortingOption: invalid groupKey : " + groupKey);
            return new RecordingFolder(groupKey.toString(), vector);
        }
    }
}

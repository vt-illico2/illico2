package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import java.text.CollationKey;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class LargestSeriesSortingOption extends SortingOption {

    public LargestSeriesSortingOption() {
        super("", TREE);
    }

    public boolean accept(SortingKey sortingKey) {
        return sortingKey.groupKey != null;
    }

    public SortingKey getSortingKey(Entry entry) {
        if (entry instanceof AbstractRecording) {
            return new SortingKey(entry, new Integer(0), entry.getTitle());
        } else if (entry instanceof RecordingFolder) {
            RecordingFolder folder = (RecordingFolder) entry;
            return new SortingKey(entry, new Long(-folder.getRecordedSize()), null);
        } else {
            return null;
        }
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        String gk = groupKey.toString();
        return new RecordingFolder(gk, vector);
    }

    public Vector sort(Vector items) {
        Vector vector = super.sort(items);
        int size = vector.size();
        Vector newVector = new Vector(size);
        for (int i = 0; i < size; i++) {
            Object o = vector.elementAt(i);
            if (o instanceof RecordingFolder) {
                newVector.addElement(o);
            }
        }
        return newVector;
    }
}

package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;

/**
 * Compare의 결과가 같으면 시간을 우선하여 sorting
 */
public class TimeBasedComparable implements Comparable {

    protected long time;
    protected Comparable comparable;

    public TimeBasedComparable(long time, Comparable comparable) {
        this.time = time;
        this.comparable = comparable;
    }

    public int compareTo(Object o) {
        if (o instanceof TimeBasedComparable) {
            TimeBasedComparable c = (TimeBasedComparable) o;
            int ret = comparable.compareTo(c.comparable);
            if (ret == 0) {
                long diff = this.time - c.time;
                if (diff == 0) {
                    return 0;
                } else if (diff > 0) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                return ret;
            }
        } else {
            return 0;
        }
    }

}


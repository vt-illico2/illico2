package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class SizeSortingOption extends SortingOption {

    private static final Long DEFAULT_SIZE = new Long(0);

    public SizeSortingOption() {
        super("sort.By size of recording", FLAT);
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        Long size;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            size = new Long(-rec.getRecordedSize());
        } else {
            size = DEFAULT_SIZE;
        }
        return new SortingKey(entry, size, null);
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return null;
    }

}

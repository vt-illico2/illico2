package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.util.Formatter;

public class DateSeparator extends Separator {

    private Date date;

    public DateSeparator(Date date, Vector children) {
        super(children);
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public String getTitle() {
        return Formatter.getCurrent().getLongDayText(date);
    }

    public boolean equals(Object o) {
        if (o instanceof DateSeparator) {
            return date.equals(((DateSeparator) o).date);
        }
        return false;
    }

}


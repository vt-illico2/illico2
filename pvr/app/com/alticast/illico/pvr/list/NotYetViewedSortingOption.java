package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class NotYetViewedSortingOption extends SortingOption implements Comparable {

    public NotYetViewedSortingOption() {
        super("sort.By Not Yet Viewed", FLAT);
    }

    public boolean accept(SortingKey sortingKey) {
        return sortingKey.groupKey != null;
    }

    public SortingKey getSortingKey(Entry entry) {
        int count = 0;
        long mediaTime = 0;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            count = rec.getViewedCount();
            mediaTime = rec.getMediaTime();
            CollationKey ck = frCollator.getCollationKey(entry.getTitle());
            Comparable c = new TimeBasedComparable(rec.getStartTime(), ck);
            return new SortingKey(entry, c, (mediaTime == 0 && count == 0) ? this : null);
        } else {
            return new SortingKey(entry, this, null);
        }
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return new FilterTitle("List.Not_Yet_Viewed", vector);
    }

    public int compareTo(Object o) {
        return 0;
    }

}

package com.alticast.illico.pvr.list;

import java.io.*;
import java.util.*;
import javax.tv.service.*;
import javax.media.Time;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.hn.*;
import org.ocap.hn.content.*;
import org.ocap.hn.recording.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;

public class RemoteRecording extends AbstractRecording {

    private RecordingContentItem item;
    private MetadataNode node;
    private boolean isClean;

    private Device device;
    private String udn;

    private long size = -1;
    private long duration = -1;

    public RemoteRecording(RecordingContentItem item) {
        this.item = item;
        try {
            device = item.getServer().getDevice();
            udn = device.getProperty(Device.PROP_UDN);
        } catch (Exception ex) {
            Log.print(ex);
        }
        String remoteId = item.getID();
        idObject = remoteId + udn;
        node = item.getRootMetadataNode();
        state = getRealState();
        channelName = getString(AppData.CHANNEL_NAME);
        Object rating = getAppData(AppData.PARENTAL_RATING);
        if (rating != null) {
            this.rating = rating.toString();
            ratingIndex = PvrUtil.getRatingIndex(this.rating);
        }
        Object obj = getAppData(AppData.GROUP_KEY);
        if (obj instanceof String) {
            groupKey = (String) obj;
        } else if (obj != null) {
            Log.printWarning("RemoteRecording: group_key is not String = " + obj + ", id = " + remoteId);
        }
        obj = getAppData(AppData.EPISODE_TITLE);
        if (obj instanceof String) {
            episodeTitle = (String) obj;
        } else if (obj != null) {
            Log.printWarning("RemoteRecording: episode_title is not String = " + obj + ", id = " + remoteId);
        }
        if (episodeTitle != null && episodeTitle.length() <= 0) {
            episodeTitle = null;
        }
        title = getString(AppData.TITLE);

        //->Kenneth[2017.8.22] R7.4 : EnglishTitle 생김
        englishTitle = getString(AppData.ENGLISH_TITLE);
        //<-

//        programStartTime = getLong(AppData.PROGRAM_START_TIME);
//        if (programStartTime == 0) {
//            programStartTime = getScheduledStartTime();
//        }
//        programEndTime = getLong(AppData.PROGRAM_END_TIME);
//        if (programEndTime == 0) {
//            programEndTime = programStartTime + getScheduledDuration();
//        }
        startTime = getScheduledStartTime();
        endTime = startTime + getScheduledDuration();
        isClean = RecordingListManager.checkClean(this);

        short stateGroup = RecordingListManager.getStateGroup(state);
        if (stateGroup == RecordingListManager.COMPLETED) {
            size = getRecordedSize();
            duration = getRecordedDuration();
        }
    }

    public int getLocalId() {
        return getInteger(AppData.LOCAL_ID);
    }

    public int getRealState() {
        try {
            return ((Integer) getAppData(RecordingContentItem.PROP_RECORDING_STATE)).intValue();
        } catch (Exception ex) {
        }
        return 0;
    }

    public boolean isInUse() {
        try {
            boolean inUse = ((IOStatus) item).isInUse();
            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemoteRecording.isInUse() returns "+inUse);
            return inUse;
        } catch (Exception ex) {
            //Log.printDebug(App.LOG_HEADER+"RemoteRecording.isInUse() returns false (by Exception)");
            return false;
        }
    }

    public long getScheduledStartTime() {
        try {
            return getLong(AppData.SCHEDULED_START_TIME);
//            return ((Date) getAppData(RecordingContentItem.PROP_START_TIME)).getTime();
        } catch (Exception ex) {
            return 0;
        }
    }

    public long getScheduledDuration() {
        return getLong(AppData.SCHEDULED_DURATION);
//        return getInteger(RecordingContentItem.PROP_DURATION);
    }

    public long getExpirationPeriod() {
        return getLong(RecordingContentItem.PROP_EXPIRATION_PERIOD) * Constants.MS_PER_SECOND;
    }

    public void setExpirationPeriod(long time) {
        // TODO
        Log.printWarning("RemoteRecording.setExpirationPeriod: this method should not be called.");
    }

    public long getMediaTime() {
        return Math.max(0, getLong(RecordingContentItem.PROP_PRESENTATION_POINT));
    }

    public void setMediaTime(long ms) {
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.setMediaTime("+ms+")");
        if (getMediaTime() == ms) {
            return;
        }
        Time time;
        if (ms == Long.MAX_VALUE) {
            time = new Time(Double.POSITIVE_INFINITY);
        } else {
            time = new Time(ms * 1000000);
        }
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        item.requestSetMediaTime(time, handler);
        handler.waitForResponse(2000L);
    }

    public void reschedule(long start, long dur) {
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.reschedule: this method should not be called.");
    }

    public int getFailedReason() {
        // Note - removed since HNEXT I16
        return getInteger("ocap:failureReason");
    }

    public int getKeepCount() {
        return getInteger(AppData.KEEP_COUNT);
    }

    ///////////////////////////////////////////////////////////////////////
    // Service
    ///////////////////////////////////////////////////////////////////////

    public Service getService() {
        try {
            return item.getItemService();
        } catch (Exception ex) {
            return null;
        }
    }

    public long getRecordedSize() {
        if (size >= 0) {
            return size;
        }
        try {
            return item.getContentSize();
        } catch (Exception ex) {
            Log.printError(ex);
            Log.print(ex);
            return 0L;
        }
    }

    public long getRecordedDuration() {
        if (duration >= 0) {
            return duration;
        }
        if (getStateGroup() == RecordingListManager.IN_PROGRESS && state != LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
            Log.printDebug("RemoteRecording.getRecordedDuration: returns estimated duration");
            Date d = getRecordingStartTime();
            if (d != null) {
                return Math.max(0L, System.currentTimeMillis() - d.getTime());
            } else {
                return 0L;
            }
        } else {
//            Object o = node.getMetadata(RecordingContentItem.PROP_DURATION);
//            Log.printDebug("RemoteRecording.getRecordedDuration: duration from meta = " + o);
//            if (o != null) {
//                int dur = 0;
//                try {
//                    dur = ((Integer) o).intValue();
//                } catch (ClassCastException ex) {
//                    Log.print(ex);
//                    try {
//                        dur = Integer.parseInt(o.toString());
//                    } catch (Exception e) {
//                        Log.print(e);
//                    }
//                } catch (Exception ex) {
//                    Log.print(ex);
//                }
//                return dur;
//            } else {
//                return 0;
//            }

            long dur = getInteger(RecordingContentItem.PROP_DURATION);
//            Log.printDebug("RemoteRecording.getRecordedDuration: duration from meta = " + dur);
            return dur;

//            long dur = 0;
//            int num = item.getResourceCount();
//            for (int i = 0; i < num; i++) {
//                ContentResource cr = item.getResource(i);
//                if (cr != null && cr instanceof StreamableContentResource) {
//                    dur += PvrUtil.getTime(((StreamableContentResource) cr).getDuration());
//                }
//            }
//            return dur;
        }
    }

    public Date getRecordingStartTime() {
        return (Date) getAppData(RecordingContentItem.PROP_START_TIME);
    }

    public boolean isClean() {
        return isClean;
    }

    public void stop() {
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.stop()");
        short stateGroup = RecordingListManager.getStateGroup(state);
        if (stateGroup != RecordingListManager.IN_PROGRESS) {
            return;
        }
        RecordingNetModule rnm = (RecordingNetModule) device.getNetModule(NetModule.CONTENT_RECORDER);
        NetActionHandlerImpl action = new NetActionHandlerImpl();
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.stop() : Call RecordingNemModule.requestDisable()");
        rnm.requestDisable(item, action);
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.stop() : Call NetActionHandlerImpl.waitForResponse(1000L)");
        action.waitForResponse(1000L);
    }

    public boolean delete() {
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.delete()");
        RecordingNetModule rnm = (RecordingNetModule) device.getNetModule(NetModule.CONTENT_RECORDER);
        NetActionHandlerImpl action = new NetActionHandlerImpl();
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.delete() : Call RecordingNetModule.requestDelete()");
        rnm.requestDelete(item, action);
        //->Kenneth[2015.1.9] : VDTRMASTER-5351 : 1초 내에 requestDelete 의 응답이 오지 않는 경우 발생, Cisco 에서.
        // 따라서 일단 5초까지 기다리도록 한다.
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.delete() : Call NetActionHandlerImpl.waitForResponse(5000L) : Notice 1s -> 5s");
        action.waitForResponse(5000L);
        //action.waitForResponse(1000L);
        Log.printDebug(App.LOG_HEADER+"RemoteRecording.delete() : returns NetActionHandlerImpl.isCompleted()");
        return action.isCompleted();
    }

    public long getSpaceRequired() {
        // TODO - 구현가능?
        return -1;
    }

    public long[] getSegmentTimes() {
        Object o = getAppData(AppData.SEGMENT_TIMES);
        if (o != null && o instanceof Long[]) {
            Long[] array = (Long[]) o;
            long[] times = new long[array.length];
            for (int i = 0; i < times.length; i++) {
                times[i] = array[i].longValue();
            }
            return times;
        }
        return null;
    }

    public String getUdn() {
        return udn;
    }

    public boolean isProtected() {
        return ParentalControl.isProtected() || RemoteDevice.protectedSet.contains(udn);
    }

    ///////////////////////////////////////////////////////////////////////
    // AppData
    ///////////////////////////////////////////////////////////////////////

    public Object getAppData(String key) {
        return node.getMetadata(key);
    }

    public void setAppData(String key, Serializable value) {
// Note - local 에서만 되어야 한다고 하는데...
//        node.addMetadata(key, value);
    }

    public String[] getKeys() {
        return node.getKeys();
    }

    public String getLocation() {
        String s = null;
        if (device != null) {
            s = device.getProperty(Device.PROP_FRIENDLY_NAME);
        }
        if (s == null) {
            s = "";
        }
        return s;
    }

    public Device getDevice() {
        return device;
    }

    public RecordingContentItem getItem() {
        return item;
    }

    public boolean equals(Object o) {
        if (o != null && o instanceof RemoteRecording) {
            return ((RemoteRecording) o).idObject.equals(idObject);
        }
        return false;
    }

    public void dump() {
        Log.printDebug("RemoteRecording ########################");
        Log.printDebug(" idObject = " + idObject);
        Log.printDebug(" state = " + RecordManager.getStateString(state));
        Log.printDebug(" title = " + getTitle());
        Log.printDebug(" size = " + getRecordedSize() + " : " + item.getContentSize());
    }

}

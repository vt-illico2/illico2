package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.framework.DataCenter;

public class FilterTitle extends Separator {

    String key;

    public FilterTitle(String key, Vector children) {
        super(children);
        this.key = key;
    }

    public String getTitle() {
        return DataCenter.getInstance().getString(key) + " (" + children.size() + ")";
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }
}

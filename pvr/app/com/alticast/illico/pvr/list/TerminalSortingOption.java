package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import java.text.CollationKey;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class TerminalSortingOption extends SortingOption {

    SortingOption subOption;

    public TerminalSortingOption(SortingOption subOption) {
        super("sort.By Terminal", GROUP, false);
        this.subOption = subOption;
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        if (entry instanceof TerminalSeparator) {
            TerminalSeparator ts = (TerminalSeparator) entry;
            return new SortingKey(entry, new TerminalBasedComparable(ts.isLocal, frCollator.getCollationKey(entry.getTitle())), null);
        }
        return subOption.getSortingKey(entry);
    }

    /** RecordingVector로 통합. */
    public Vector prepareRecordings(Vector deviceVector) {
        Vector vector = new Vector();
        Enumeration en = deviceVector.elements();
        while (en.hasMoreElements()) {
            RecordingVector v = (RecordingVector) en.nextElement();
            if (v.size() > 0) {
                vector.add(new TerminalSeparator(v.name, v.isLocal, subOption.sort(v)));
            }
        }
        return vector;
    }

//    public Vector sort(Vector items) {
//        if (items.size() == 1) {
//            return ((TerminalSeparator) items.elementAt(0)).getEntries();
//        } else {
//            return super.sort(items);
//        }
//    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return subOption.createFolder(groupKey, vector);
    }

    // local 우선, remote 끼리는 이름 순
    class TerminalBasedComparable implements Comparable {

        protected boolean isLocal;
        protected Comparable comparable;

        public TerminalBasedComparable(boolean local, Comparable comparable) {
            this.isLocal = local;
            this.comparable = comparable;
        }

        public int compareTo(Object o) {
            if (o instanceof TerminalBasedComparable) {
                TerminalBasedComparable c = (TerminalBasedComparable) o;
                if (isLocal == c.isLocal) {
                    return comparable.compareTo(c.comparable);
                } else {
                    return isLocal ? -1 : 1;
                }
            } else {
                return 0;
            }
        }

    }
}

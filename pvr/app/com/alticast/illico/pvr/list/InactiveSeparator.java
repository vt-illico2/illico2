package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.framework.DataCenter;

public class InactiveSeparator extends Separator {

    public InactiveSeparator(Vector children) {
        super(children);
    }

    public String getTitle() {
        return DataCenter.getInstance().getString("pvr.inactive");
    }

}


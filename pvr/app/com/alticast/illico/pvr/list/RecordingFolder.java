package com.alticast.illico.pvr.list;

import java.util.*;
import java.awt.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.log.Log;
import java.text.SimpleDateFormat;

public class RecordingFolder extends Folder {

    protected String title;
    private String countString;
    protected boolean blocked;
    protected int ratingIndex;
    protected long recordedSize;
    public boolean notViewed;

    public static final byte EMPTY       = 0;
    public static final byte LOCAL_ONLY  = 1;
    public static final byte REMOTE_ONLY = 2;
    public static final byte MIXED       = 3;

    public final byte type;

    public Folder parent = null;

    private static String getTitle(Vector vector) {
        if (vector.size() > 0) {
            return ((Entry) vector.elementAt(0)).getTitle();
        } else {
            Log.printWarning("RecordingFolder: can't find title");
            return "";
        }
    }

    public RecordingFolder(Vector children) {
        this(getTitle(children), children);
    }

    public RecordingFolder(String title, Vector children) {
        super(children);
        this.title = title;
        this.countString = " (" + children.size() + ")";

        blocked = false;
        notViewed = false;
        int size = children != null ? children.size() : 0;
        recordedSize = 0;
        boolean hasLocal = false;
        boolean hasRemote = false;
        for (int i = 0; i < size; i++) {
            AbstractRecording r = (AbstractRecording) children.elementAt(i);
            ratingIndex = Math.max(ratingIndex, r.getRatingIndex());
            if (r.isBlocked()) {
                blocked = true;
            }
            if (r.getViewedCount() <= 0) {
                notViewed = true;
            }
            recordedSize += r.getRecordedSize();
            if (r instanceof LocalRecording) {
                hasLocal = true;
            } else if (r instanceof RemoteRecording) {
                hasRemote = true;
            }
        }
        if (hasLocal) {
            type = hasRemote ? MIXED : LOCAL_ONLY;
        } else {
            type = hasRemote ? REMOTE_ONLY : EMPTY;
        }
        update();
    }

    public void update() {
        boolean blocked = false;
        boolean notViewed = false;
        int size = children != null ? children.size() : 0;
        recordedSize = 0;
        for (int i = 0; i < size; i++) {
            AbstractRecording r = (AbstractRecording) children.elementAt(i);
            ratingIndex = Math.max(ratingIndex, r.getRatingIndex());
            if (r.isBlocked()) {
                blocked = true;
            }
            if (r.getViewedCount() <= 0) {
                notViewed = true;
            }
            recordedSize += r.getRecordedSize();
        }
        this.blocked = blocked;
        this.notViewed = notViewed;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return title + countString;
    }

    public long getRecordedSize() {
        return recordedSize;
    }

    public String getDisplayTitle(String s, FontMetrics fm, int width) {
        int cw = fm.stringWidth(countString);
        return TextUtil.shorten(s, fm, width - cw) + countString;
    }

    public String getFullDisplayTitle(String s) {
        return s + countString;
    }

    public int getRatingIndex() {
        return ratingIndex;
    }

    public boolean equals(Object o) {
        if (o instanceof RecordingFolder) {
            RecordingFolder f = (RecordingFolder) o;
            if (parent == null) {
                return f.title.equals(title);
            } else {
                return parent.equals(f.parent) && f.title.equals(title);
            }
        } else {
            return false;
        }
    }

    public boolean isBlocked() {
        return blocked;
    }

    public void block(boolean block) {
        blocked = block;
    }

    public long getTime() {
        if (children.size() > 0) {
            AbstractRecording rec = (AbstractRecording) children.elementAt(0);
            long time = rec.getStartTime();
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(time));
            int yearShift = rec.getInteger(AppData.YEAR_SHIFT);
            if (Log.INFO_ON) Log.printInfo("Folder : "+yearShift+" : "+rec.getIdObject());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ssS");
            if (yearShift > 0) {
                if (Log.INFO_ON) Log.printInfo("old time = "+sdf.format(c.getTime()));
                c.add(Calendar.YEAR, yearShift);
                time = c.getTime().getTime();
                if (Log.INFO_ON) Log.printInfo("new time = "+sdf.format(c.getTime()));
            } else {
                if (Log.INFO_ON) Log.printInfo("time  = "+sdf.format(c.getTime()));
            }
            return time;
        }
        return 0L;
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }

}


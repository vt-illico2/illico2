package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class DurationSortingOption extends SortingOption {

    private static final Long DEFAULT_DURATION = new Long(0);

    public DurationSortingOption() {
        super("sort.By program length", FLAT);
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        Long dur;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            dur = new Long(rec.getRecordedDuration());
        } else {
            dur = DEFAULT_DURATION;
        }
        return new SortingKey(entry, dur, null);
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return null;
    }

}

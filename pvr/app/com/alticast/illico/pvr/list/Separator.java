package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;

public abstract class Separator extends Folder {

    public Separator(Vector children) {
        super(children);
    }

    public boolean isSelectable() {
        return false;
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }

}


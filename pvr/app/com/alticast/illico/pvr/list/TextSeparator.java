package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.util.Formatter;

public class TextSeparator extends Separator {

    private String text;

    public TextSeparator(String text, Vector children) {
        super(children);
        this.text = text;
    }

    public String getTitle() {
        return text;
    }

    public boolean equals(Object o) {
        if (o instanceof TextSeparator) {
            return text.equals(((TextSeparator) o).text);
        } else {
            return super.equals(o);
        }
    }
}


package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.util.Formatter;

public class TerminalSeparator extends TextSeparator {

    boolean isLocal;

    public TerminalSeparator(String text, boolean local, Vector children) {
        super(text, children);
        this.isLocal = local;
        for (int i = 0; i < children.size(); i++) {
            Object entry = children.elementAt(i);
            if (entry instanceof RecordingFolder) {
                ((RecordingFolder) entry).parent = this;
            }
        }
    }

}


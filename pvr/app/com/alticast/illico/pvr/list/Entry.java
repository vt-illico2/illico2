package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;

public abstract class Entry {

    public abstract String getTitle();

    /** class 별 paint 할 때 비교를 줄이기 위해 한번 돌아간다. */
    public abstract void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r);

    public boolean isSelectable() {
        return true;
    }

    public String toString() {
        return getTitle();
    }

}


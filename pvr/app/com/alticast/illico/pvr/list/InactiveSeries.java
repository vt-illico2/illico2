package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.framework.*;

public class InactiveSeries extends ProgramDetail {

    protected int convertedCategory;

    public InactiveSeries(Series s) {
        this.channelName = s.channel;
        try {
            TvChannel ch = Core.epgService.getChannel(channelName);
            if (ch != null) {
                channelNumber = ch.getNumber();
            }
        } catch (Exception ex) {
        }

        this.groupKey = s.groupKey;
        this.title = s.title;
        this.convertedCategory = PvrUtil.getConvertedCategory(s.category, s.subCategory);
        this.definition = s.definition;
        this.language = s.language;
        this.rating = s.rating;
        this.ratingIndex = PvrUtil.getRatingIndex(s.rating);

        this.idObject = groupKey;
    }

    public InactiveSeries(String groupKey) {
        this.groupKey = groupKey;
        this.title = RecordingScheduler.getInstance().getTitle(groupKey);
    }

    public int getConvertedCategory() {
        return convertedCategory;
    }

    public boolean isBlocked() {
        return false;
    }

    public String getDescription() {
        return ParentalControl.getTitle(this) + " " + DataCenter.getInstance().getString("pvr.inactive_desc");
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }

}


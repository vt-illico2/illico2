package com.alticast.illico.pvr.list;

import java.util.Vector;
import com.alticast.illico.pvr.hn.*;

class RecordingVector extends Vector {

    String name;
    boolean isLocal;

    /** Local */
    public RecordingVector() {
        this(HomeNetManager.getInstance().getLocalDeviceName(), true);
    }

    /** Remote */
    public RecordingVector(RemoteDevice rd) {
        this(rd.getFriendlyName(), false);
    }

    private RecordingVector(String name, boolean local) {
        this.name = name;
        this.isLocal = local;
    }

    public int findIndexById(Object id) {
        int size = size();
        for (int i = 0; i < elementCount; i++) {
            Object o = elementData[i];
            if (o instanceof AbstractRecording) {
                if (((AbstractRecording) o).getIdObject().equals(id)) {
                    return i;
                }
            }
        }
        return -1;
    }
}

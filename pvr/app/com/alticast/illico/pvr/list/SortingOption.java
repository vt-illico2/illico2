package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.*;
import com.videotron.tvi.illico.ui.MenuItem;

public abstract class SortingOption extends MenuItem {

    public static Collator frCollator = Collator.getInstance(Locale.CANADA_FRENCH);

    public static final byte FLAT  = 0;
    public static final byte TREE  = 1;
    public static final byte GROUP = 2;

    protected byte type;
    protected boolean currentOnTop;

    public SortingOption(String name, byte type) {
        this(name, type, true);
    }

    public SortingOption(String name, byte type, boolean currentOnTop) {
        super(name);
        this.type = type;
        this.currentOnTop = currentOnTop;
    }

    public abstract SortingKey getSortingKey(Entry entry);

    public abstract boolean accept(SortingKey sortingKey);

    public abstract Folder createFolder(Object groupKey, Vector vector);

    /** 모든 device별 recording을 한개의 vector로 통합. */
    public Vector prepareRecordings(Vector deviceVector) {
        Vector vector = new Vector();
        Enumeration en = deviceVector.elements();
        while (en.hasMoreElements()) {
            Vector v = (Vector) en.nextElement();
            vector.addAll(v);
        }
        return vector;
    }

    public Vector sort(Vector items) {
        // sorting key로 sort
        int size = items.size();
        Vector keys = new Vector(size + 4); // list of SortingKey
        for (int i = 0; i < size; i++) {
            Entry entry = (Entry) items.elementAt(i);
            SortingKey sk = getSortingKey(entry);
            if (accept(sk)) {
                keys.addElement(sk);
            }
        }
        Collections.sort(keys);

        size = keys.size();
        Vector ret = new Vector(size);
        Vector top = new Vector(4);
        Hashtable groups = new Hashtable();

        for (int i = 0; i < size; i++) {
            SortingKey sk = (SortingKey) keys.elementAt(i);
            if (currentOnTop && sk.entry instanceof AbstractRecording && ((AbstractRecording) sk.entry).isInProgress()) {
                // 진행중인 recoridng을 상단으로 보내기위해
                top.addElement(sk.entry);
            } else {
                // 진행중이지 않은 recording은 folder나 group에 추가하지 않는다
                if (type == FLAT) {
                    ret.addElement(sk.entry);      // list of Entry
                } else if (type == GROUP) {
                    Object gk = sk.groupKey;
                    if (gk == null) {
                        ret.addElement(sk);    // list of SortingKey group
                    } else {
                        // group별로 list를 만들어 저장
                        Vector vector = (Vector) groups.get(gk);
                        if (vector == null) {
                            vector = new Vector();
                            groups.put(gk, vector);
                        }
                        vector.addElement(sk.entry);
                    }
                } else if (type == TREE) {
                    // tree 형태는 title이 group key가 된다
                    String title = sk.entry.getTitle();
                    Vector vector = (Vector) groups.get(title);
                    if (vector == null) {
                        vector = new Vector();
                        groups.put(title, vector);
                    }
                    vector.addElement(sk.entry);
                }
            }
        }
        if (type == FLAT) {
            if (currentOnTop) {
                moveToTop(ret, top);
            }
            return ret;
        }

        Map.Entry group;
        Set set = groups.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            // group을 folder로 만들고 sorting key를 생성해서 group 밖의 recording과 같게 취급
            group = (Map.Entry) it.next();
            ret.addElement(getSortingKey(createFolder(group.getKey(), (Vector) group.getValue())));
        }

        // sorting key로 다시 sort
        Collections.sort(ret);
        size = ret.size();
        Vector entries = new Vector(size + 2);
        for (int i = 0; i < size; i++) {
            entries.addElement(((SortingKey) ret.elementAt(i)).entry);
        }
        Vector flatVector = new Vector(size * 2 + 4);
        int eSize = entries.size();
        if (type == GROUP) {
            // group안의 내용을 풀어서 최종 flatVector에 추가
            for (int i = 0; i < eSize; i++) {
                Entry en = (Entry) entries.elementAt(i);
                flatVector.addElement(en);
                if (en instanceof Folder) {
                    Vector child = ((Folder) en).getEntries();
                    int cSize = child.size();
                    for (int j = 0; j < cSize; j++) {
                        flatVector.addElement(child.elementAt(j));
                    }
                }
            }
        } else if (type == TREE) {
            // tree child가 1이면 flatVector에 추가 아니면 folder 자체를 그대로 추가
            for (int i = 0; i < eSize; i++) {
                Entry en = (Entry) entries.elementAt(i);
                if (en instanceof Folder) {
                    Vector child = ((Folder) en).getEntries();
                    int cSize = child.size();
                    if (cSize > 1) {
                        flatVector.addElement(en);
                    } else if (cSize == 1) {
                        flatVector.addElement(child.elementAt(0));
                    }
                }
            }
        }
        if (currentOnTop) {
            moveToTop(flatVector, top);
        }
        return flatVector;
    }

    private void moveToTop(Vector data, Vector top) {
        int size = top.size();
        for (int i = size - 1; i >= 0; i--) {
            Object o = top.elementAt(i);
            data.remove(o);
            data.add(0, o);
        }
    }
}


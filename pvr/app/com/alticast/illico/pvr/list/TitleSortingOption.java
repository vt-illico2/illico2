package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import java.text.CollationKey;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;
import java.text.SimpleDateFormat;

public class TitleSortingOption extends SortingOption {

    private boolean timeAscending = false;

    public TitleSortingOption(boolean timeAscending) {
        super("sort.By Alphabetically", TREE, true);
        this.timeAscending = timeAscending;
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        String title = entry.getTitle();
        long time = 0;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            time = rec.getStartTime();

            //DDC-001 : add yearShift
            Calendar c = Calendar.getInstance();
            c.setTime(new Date(time));
            int yearShift = rec.getInteger(AppData.YEAR_SHIFT);
            if (Log.INFO_ON) Log.printInfo("YS in TitleSortingOption is "+yearShift+" : "+rec.getIdObject());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ssS");
            if (yearShift > 0) {
                if (Log.INFO_ON) Log.printInfo("old time  = "+sdf.format(c.getTime()));
                c.add(Calendar.YEAR, yearShift);
                time = c.getTime().getTime();
                if (Log.INFO_ON) Log.printInfo("new time  = "+sdf.format(c.getTime()));
            } else {
                if (Log.INFO_ON) Log.printInfo("time  = "+sdf.format(c.getTime()));
            }

            if (timeAscending) {
                time = -time;
            }
        }
        CollationKey ck = frCollator.getCollationKey(title);
        return new SortingKey(entry, new TimeBasedComparable(time, ck), title);
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return new RecordingFolder(groupKey.toString(), vector);
    }
}

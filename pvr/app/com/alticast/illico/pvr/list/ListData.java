package com.alticast.illico.pvr.list;

import org.ocap.shared.dvr.navigation.*;
import org.ocap.shared.dvr.*;
import org.ocap.hn.recording.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;
import java.util.*;
import org.ocap.ui.event.*;

public class ListData {

    public static final byte FULL_UPDATE = 0;
    public static final byte SORT = 1;
    public static final byte REMOTE_CHANGED = 2;
    public static final byte RECORDING_ADDED = 3;
    public static final byte RECORDING_CHANGED = 4;
    public static final byte RECORDING_REMOVED = 5;
    public static final byte ISSUE_CHANGED = 6;

    private static final String[] UPDATE_TYPE_STRING = {
        "full", "sort", "remote", "added", "changed", "removed", "issue"
    };

    public static String getUpdateTypeString(byte type) {
        return UPDATE_TYPE_STRING[(int) type];
    }

    Vector totalEntries;
    Issue issue;
    SortingOption option;

    RecordingFolder currentFolder;
    Vector currentEntries;

    int recordingCount = 0;
    int completedRecordingCount = 0;
    int activeDeviceCount = 0;
    int totalDeviceCount = 0;

    boolean hasFilteredRecording = false;

    public int focus = 0;

    Vector currentDeviceVector;

    public ListData(RecordingList list, SortingOption option) {
        this(list, null, null, option);
    }

    public ListData(RecordingList list, Vector deviceList, Issue issue, SortingOption option) {
        this.issue = issue;
        this.recordingCount = 0;

        // build
        Vector deviceVector = new Vector();
        if (list != null) {
            RecordingVector vector = new RecordingVector();
            int size = list.size();
            for (int i = 0; i < size; i++) {
                LocalRecording r = new LocalRecording((LeafRecordingRequest) list.getRecordingRequest(i));
                int stateGroup = r.getStateGroup();
                if (stateGroup != RecordingListManager.INVALID) {
                    vector.addElement(r);
                } else {
                    Log.printWarning("invalid LocalRecording : state = " + r.getState() + ", id = " + r.getIdObject());
                }
            }
            deviceVector.add(vector);
        }
        addRemoteDevices(deviceVector, deviceList);

        this.currentDeviceVector = deviceVector;
        updateDeviceVector();
        setTotalEntries(option);
    }

    private static void addRemoteDevices(Vector deviceVector, Vector deviceList) {
        if (deviceList != null) {
            Enumeration en = deviceList.elements();
            while (en.hasMoreElements()) {
                RemoteDevice rd = (RemoteDevice) en.nextElement();
                RecordingVector vector = new RecordingVector(rd);
                RecordingContentItem[] array = rd.getRecordings();
                Log.printDebug("ListData: remote recordings = " + array.length);
                for (int i = 0; i < array.length; i++) {
                    RemoteRecording r = new RemoteRecording(array[i]);
                    int stateGroup = r.getStateGroup();
                    if (stateGroup != RecordingListManager.INVALID) {
                        vector.addElement(r);
                    } else {
                        Log.printWarning("invalid RemoteRecording : state = " + r.getState() + ", id = " + r.getIdObject());
                    }
                }
                deviceVector.add(vector);
            }
        }
    }

    private void updateDeviceVector() {
        recordingCount = 0;
        completedRecordingCount = 0;
        totalDeviceCount = 0;
        activeDeviceCount = 0;
        Enumeration en = currentDeviceVector.elements();
        while (en.hasMoreElements()) {
            RecordingVector vector = (RecordingVector) en.nextElement();
            int size = vector.size();
            for (int i = 0; i < size; i++) {
                AbstractRecording r = (AbstractRecording) vector.elementAt(i);
                if (r.getStateGroup() == RecordingListManager.COMPLETED) {
                    completedRecordingCount++;
                }
            }
            recordingCount += size;
            if (!vector.isLocal) {
                totalDeviceCount++;
                if (size > 0) {
                    activeDeviceCount++;
                }
            }
        }
    }

    //->Kenneth[2017.8.24] R7.4 : 외부에서도 부를 수 있게 바꿈
    public void setTotalEntries(SortingOption option) {
    //private void setTotalEntries(SortingOption option) {
    //<-
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.setTotalEntries() : Call sort()");
        this.totalEntries = sort(option);
        hasFilteredRecording = totalEntries.size() > 0;
        if (issue != null) {
            totalEntries.insertElementAt(issue, 0);
        }
    }

    private ListData(ListData data) {
        this.issue = IssueManager.getInstance().getIssue();
        this.recordingCount = data.recordingCount;
        this.completedRecordingCount = data.completedRecordingCount;
        this.currentDeviceVector = data.currentDeviceVector;
        this.activeDeviceCount = data.activeDeviceCount;
        this.totalDeviceCount = data.totalDeviceCount;
    }

    private ListData(ListData data, SortingOption option) {
        this(data);
        setTotalEntries(option);
    }

    public ListData createNewData(SortingOption option, byte type, Object obj) {
        try {
            if (type == ISSUE_CHANGED || type == SORT) {
                return new ListData(this, option);
            }
            ListData newData = new ListData(this);
            if (type == RECORDING_REMOVED) {
                removeRecording(newData, obj);
            } else if (type == RECORDING_ADDED) {
                addRecording(newData, obj, true);
            } else if (type == RECORDING_CHANGED) {
                addRecording(newData, obj, false);
            } else if (type == REMOTE_CHANGED) {
                remoteChanged(newData, obj);
            }
            newData.setTotalEntries(option);
            return newData;
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }
    }

    private static boolean removeRecording(ListData data, Object obj) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.removeRecording("+data+", "+obj+")");
        RecordingVector localVector = null;
        try {
            localVector = (RecordingVector) data.currentDeviceVector.elementAt(0);
        } catch (Exception ex) {
        }
        if (localVector == null || !localVector.isLocal) {
            Log.printWarning("ListData.removeRecording : can't find local device");
            return false;
        }
        int index = findIndexByRecording(localVector, ((RecordingRequest) obj));
        if (index == -1) {
            Log.printWarning("ListData.removeRecording : can't find recording");
            return false;
        }
        localVector.removeElementAt(index);
        data.updateDeviceVector();
        return true;
    }

    private static int findIndexByRecording(RecordingVector v, RecordingRequest r) {
        try {
            return v.findIndexById(new Integer(r.getId()));
        } catch (Exception ex) {
        }
        return -1;
    }

    private static boolean addRecording(ListData data, Object obj, boolean add) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.addRecording("+obj+", "+add+")");
        RecordingVector localVector = null;
        try {
            localVector = (RecordingVector) data.currentDeviceVector.elementAt(0);
        } catch (Exception ex) {
        }
        LeafRecordingRequest rr = (LeafRecordingRequest) obj;
        if (localVector == null || !localVector.isLocal) {
            if (add) {
                Log.printInfo("ListData.addRecording : creating new list for local recording");
            } else {
                Log.printWarning("ListData.changeRecording : creating new list for local recording");
            }
            localVector = new RecordingVector();
            data.currentDeviceVector.add(0, localVector);
        } else {
            int index = findIndexByRecording(localVector, rr);
            if (index != -1) {
                if (add) {
                    Log.printWarning("ListData.addRecording : old recording is exist");
                }
                LocalRecording r = (LocalRecording) localVector.elementAt(index);
                localVector.removeElementAt(index);
            } else {
                if (!add) {
                    Log.printWarning("ListData.changeRecording : old recording is not exist");
                    // list에 with error recording의 경우 added 임에도 changed로 올 수 있다.
                    // 이 경우 return 하면 recording이 추가되지 않기 때문에 changed도 무조건 추가한다.
//                    return false;
                }
            }
        }
        LocalRecording r = new LocalRecording(rr);
        Log.printWarning("ListData: created new LocalRecording");
        int stateGroup = r.getStateGroup();
        if (stateGroup != RecordingListManager.INVALID) {
            localVector.addElement(r);
            data.updateDeviceVector();
            return true;
        } else {
            Log.printWarning("ListData.addRecording : invalid : state = " + r.getState() + ", id = " + r.getIdObject());
            return false;
        }
    }

    private static boolean remoteChanged(ListData data, Object obj) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.remoteChanged("+data+", "+obj+")");
        RecordingVector localVector = null;
        try {
            localVector = (RecordingVector) data.currentDeviceVector.elementAt(0);
        } catch (Exception ex) {
        }
        Vector deviceVector = new Vector();
        if (localVector != null && localVector.isLocal) {
            deviceVector.add(localVector);
        }
        addRemoteDevices(deviceVector, (Vector) obj);
        data.currentDeviceVector = deviceVector;
        data.updateDeviceVector();
        return true;
    }

    private Vector sort(SortingOption option) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.sort("+option+")");
        this.option = option;
        Vector vector = option.prepareRecordings(currentDeviceVector);
        Vector newEntries = option.sort(vector);

        this.currentFolder = null;
        this.currentEntries = newEntries;
        return newEntries;
    }

    public int getRecordingCount() {
        return recordingCount;
    }

    public int getCompletedRecordingCount() {
        return completedRecordingCount;
    }

    public int getActiveDeviceCount() {
        return activeDeviceCount;
    }

    public int getTotalDeviceCount() {
        return totalDeviceCount;
    }

    public RecordingFolder getCurrentFolder() {
        return currentFolder;
    }

    public Vector getCurrentList() {
        return currentEntries;
    }

    public Vector getTotalList() {
        return totalEntries;
    }

    public boolean hasFilteredRecording() {
        return hasFilteredRecording;
    }

    public Entry getCurrent() {
        return getEntry(focus);
    }

    private Entry getEntry(int index) {
        try {
            return (Entry) currentEntries.elementAt(index);
        } catch (Exception ex) {
            return null;
        }
    }

    private synchronized void setIssue(Issue newIssue) {
        if (newIssue != null) {
            totalEntries.insertElementAt(newIssue, 0);
        }
    }

    /** Issue 변경. */
    public synchronized boolean issueChanged(Issue oldIssue, Issue newIssue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.issueChanged("+oldIssue+", "+newIssue+")");
        Entry oldFocus = getCurrent();
        Entry entry = null;
        if (totalEntries.size() > 0) {
            entry = (Entry) totalEntries.elementAt(0);
        }

        if (newIssue != null) {
            if (entry != null && entry instanceof Issue) {
                totalEntries.setElementAt(newIssue, 0);
            } else {
                totalEntries.insertElementAt(newIssue, 0);
            }
        } else {
            if (entry != null && entry instanceof Issue) {
                totalEntries.removeElementAt(0);
            }
        }
        resetFocus(oldFocus, focus);
        return oldFocus == null || !oldFocus.equals(getCurrent());
    }

    public void resetFocus(ListData data) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.resetFocus() -- 1");
        if (data == null) {
            return;
        }
        resetFocus(data.currentFolder, data.focus, data.getCurrent());
    }

    public synchronized void resetFocus(RecordingFolder dataFolder, int dataFocus, Entry dataEntry) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.resetFocus() -- 2");
        if (dataFolder != null) {
            int index = totalEntries.indexOf(dataFolder);
            if (Log.DEBUG_ON) {
                Log.printDebug("ListData.resetFocus: index of folder = " + index);
            }
            if (index >= 0) {
                focus = index;
                selected((RecordingFolder) totalEntries.elementAt(index));
                resetFocus(dataEntry, dataFocus);
            } else {
                resetFocus(dataEntry, 0);
            }
        } else {
            resetFocus(dataEntry, dataFocus);
        }
    }

    private void resetFocus(Entry entry, int start) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.resetFocus() -- 3");
        if (currentEntries.size() <= 0) {
            focus = -1;
            return;
        }
        int index = entry != null ? currentEntries.indexOf(entry) : -1;
        if (Log.DEBUG_ON) {
            Log.printDebug("ListData.resetFocus: index of entry = " + index + " " + entry);
        }
        if (index >= 0) {
            focus = index;
        } else {
            int newFocus = findSelectableBefore(start + 1);
            if (newFocus < 0) {
                focus = Math.max(findSelectableAfter(-1), 0);
            } else {
                focus = newFocus;
            }
        }
    }

    private int findSelectableAfter(int from) {
        int size = currentEntries.size();
        int i = Math.max(0, from + 1);
        while (i < size) {
            Entry entry = (Entry) currentEntries.elementAt(i);
            if (entry.isSelectable()) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private int findSelectableBefore(int from) {
        int size = currentEntries.size();
        int i = Math.min(size - 1, from - 1);
        while (i >= 0) {
            Entry entry = (Entry) currentEntries.elementAt(i);
            if (entry != null && entry.isSelectable()) {
                return i;
            }
            i--;
        }
        return -1;
    }

    public synchronized void setFocus(Entry toBeFocused) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.setFocus("+toBeFocused+")");
        if (toBeFocused == null) {
            currentFolder = null;
            currentEntries = totalEntries;
            if (currentEntries.size() > 0) {
                focus = Math.max(findSelectableAfter(-1), 0);
            } else {
                focus = -1;
            }
            return;
        }
        int size = currentEntries.size();
        int target = -1;
        for (int i = 0; i < size; i++) {
            if (toBeFocused == currentEntries.elementAt(i)) {
                focus = i;
                return;
            } else if (toBeFocused.equals(currentEntries.elementAt(i))) {
                target = i;
            }
        }
        if (target >= 0) {
            focus = target;
        } else {
            focus = 0;
        }
    }

    public synchronized boolean keyUp() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.keyUp()");
        if (focus > 0) {
            int nf = findSelectableBefore(focus);
            if (nf >= 0) {
                focus = nf;
                return true;
            }
        }
        return false;
    }

    public synchronized boolean keyDown() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.keyDown()");
        if (focus < currentEntries.size() - 1) {
            int nf = findSelectableAfter(focus);
            if (nf >= 0) {
                focus = nf;
                return true;
            }
        }
        return false;
    }

    public synchronized void selected(RecordingFolder folder) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.selected()");
        if (folder == currentFolder) {
            currentFolder = null;
            currentEntries = totalEntries;
            setFocus(folder);
            return;
        }
        if (folder != getCurrent()) {
            return;
        }
        Vector childEntries = folder.getEntries();
        int size = childEntries.size();
        Vector subEntries = new Vector(childEntries.size() + 2);
        subEntries.addElement(folder);
        subEntries.addAll(childEntries);
        currentFolder = folder;
        currentEntries = subEntries;
        if (size > 0) {
            focus = 1;
        } else {
            focus = 0;
        }
    }

    public synchronized void delete(Entry entry) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.delete("+entry+")");
        try {
            currentEntries.removeElement(entry);
            int size = currentEntries.size();
            if (focus >= size && size > 0) {
                focus = size - 1;
            }
            Entry en = getEntry(focus);
            if (en == null || !en.isSelectable()) {
                int newFocus = findSelectableBefore(focus);
                if (newFocus < 0) {
                    newFocus = Math.max(findSelectableAfter(-1), 0);
                }
                focus = newFocus;
            }
        } catch (Exception ex) {
        }
    }

    // list 에서 지웠을 때 전체 update 대신
    // recording을 data에서 빨리 지우고 sorting 만 다시 한다.
    public synchronized void delete(Vector target) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.delete("+target+")");
        long time = System.currentTimeMillis();

        try {
            Vector deviceVector = currentDeviceVector;
            if (deviceVector == null) {
                return;
            }
            Enumeration en = deviceVector.elements();
            while (en.hasMoreElements()) {
                if (target == null || target.size() == 0) {
                    return;
                }
                delete((RecordingVector) en.nextElement(), target);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void delete(RecordingVector rv, Vector target) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ListData.delete("+rv+", "+target+")");
        int size = target.size();
        for (int i = size - 1; i >= 0; i--) {
            Object recording = target.get(i);
            if (rv.remove(recording)) {
                target.remove(i);
                recordingCount--;
            }
        }
    }

}


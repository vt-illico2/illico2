package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.text.*;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;

/**
 * Compare의 결과가 같으면 String으로 우선하여 sorting
 */
public class StringBasedComparable implements Comparable {

    static Collator frCollator = Collator.getInstance(Locale.CANADA_FRENCH);

    protected CollationKey collationKey;
    protected Comparable comparable;

    public StringBasedComparable(String s, Comparable comparable) {
        this.collationKey = frCollator.getCollationKey(s);
        this.comparable = comparable;
    }

    public int compareTo(Object o) {
        if (o instanceof StringBasedComparable) {
            StringBasedComparable c = (StringBasedComparable) o;
            int ret = comparable.compareTo(c.comparable);
            if (ret == 0) {
                return collationKey.compareTo(c.collationKey);
            } else {
                return ret;
            }
        } else {
            return 0;
        }
    }

}


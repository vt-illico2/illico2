package com.alticast.illico.pvr.list;

import java.io.*;
import java.util.*;
import javax.tv.service.*;
import java.awt.Graphics;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.hn.*;
import org.ocap.shared.dvr.*;
import org.ocap.hn.recording.*;

public abstract class AbstractRecording extends ProgramDetail {
    //->Kenneth[2017.3.13] R7.3 : Viewing status 가 구체화 되었음
    public static final int NOT_YET_VIEWED = 0;
    public static final int PARTIALLY_VIEWED = 1;
    public static final int COMPLETELY_VIEWED = 2;

    private float threshold_30;
    private float threshold_60;
    private float threshold_over_60;
    private float min30 = (float)(30*60*1000);
    private float min60 = (float)(60*60*1000);
    //<-
    protected String episodeTitle;
    protected long startTime;
    protected long endTime;

    protected int state;
    //->Kenneth[2017.8.22] R7.4 : EnglishTitle 생김
    protected String englishTitle;
    //<-

    //->Kenneth[2017.3.13] R7.3 : Viewing status 가 구체화 되었음
    public int getViewingState() {
        //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : "+getTitle());
        int viewingState = NOT_YET_VIEWED;
        try {
            float duration = (float)getRecordedDuration();//ms
            //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : duration = "+duration);
            float mediaTime = (float)getMediaTime();//ms
            //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : mediaTime = "+mediaTime);

            if (threshold_30 == 0f) {
                String threshold_30_str = DataCenter.getInstance().getString("THRESHOLD_30");
                String threshold_60_str = DataCenter.getInstance().getString("THRESHOLD_60");
                String threshold_over_60_str = DataCenter.getInstance().getString("THRESHOLD_OVER_60");
                threshold_30 = Float.parseFloat(threshold_30_str);
                threshold_60 = Float.parseFloat(threshold_60_str);
                threshold_over_60 = Float.parseFloat(threshold_over_60_str);
            }

            boolean viewed = getViewedCount() > 0;
            //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : viewed = "+viewed);
            if (mediaTime == 0.0f) {
                if (viewed) {
                    viewingState = COMPLETELY_VIEWED;
                    //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : viewingState = COMPLETELY_VIEWED");
                } else {
                    viewingState = NOT_YET_VIEWED;
                    //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : viewingState = NOT_YET_VIEWED");
                }
            } else {
                float thresholdRate = 0.0f;
                if (duration < min30) {
                    thresholdRate = threshold_30;//1
                } else if (duration < min60) {
                    thresholdRate = threshold_60;//0.995
                } else {
                    thresholdRate = threshold_over_60;//0.975
                }
                float threshold = duration*thresholdRate;
                //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : threshold = "+threshold);
                if (mediaTime >= threshold) {
                    viewingState = COMPLETELY_VIEWED;
                    //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : viewingState = COMPLETELY_VIEWED");
                } else {
                    viewingState = PARTIALLY_VIEWED;
                    //if (Log.EXTRA_ON) Log.printDebug("AbstractRecording.getViewingState() : viewingState = PARTIALLY_VIEWED");
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return viewingState;
    }
    //<-

    public long getStartTime() {
        return startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public final int getState() {
        return state;
    }

    //->Kenneth[2017.8.22] R7.4 : EnglishTitle 생김
    public String getEnglishTitle(){
        return englishTitle;
    }
    //<-

    public abstract long getScheduledStartTime();

    public abstract long getScheduledDuration();

    public abstract Service getService();

    public abstract long getRecordedSize();

    public abstract long getRecordedDuration();

    public abstract Date getRecordingStartTime();

    public abstract long getMediaTime();

    public abstract void setMediaTime(long ms);

    public abstract long getExpirationPeriod();

    public abstract long getSpaceRequired();

    public abstract long[] getSegmentTimes();

    public abstract boolean isInUse();

    public abstract boolean isProtected();

    public abstract int getKeepCount();

    public abstract int getLocalId();

    public long getExpirationTime() {
        long exp = getExpirationPeriod();
        if (exp >= RecordManager.MAX_PERIOD) {
            return Long.MAX_VALUE;
        } else {
            return getScheduledStartTime() + exp;
        }
    }

    public abstract void setExpirationPeriod(long time);

    public void setExpirationTime(long time) {
        if (Log.DEBUG_ON) {
            Log.printDebug("AbstractRecording.setExpirationTime = " + new Date(time));
        }
        if (time == Long.MAX_VALUE) {
            setExpirationPeriod(Long.MAX_VALUE);
            return;
        }
        long newDur = time - getScheduledStartTime();
        if (newDur > 0) {
            setExpirationPeriod(newDur);
        }
    }

//    public abstract void reschedule(long start, long dur);

    // AppData

    public abstract Object getAppData(String key);

    public String getString(String key) {
        try {
            return getAppData(key).toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public int getInteger(String key) {
        Object o = getAppData(key);
        try {
            return ((Integer) o).intValue();
        } catch (ClassCastException ex) {
            try {
                return Integer.parseInt(o.toString());
            } catch (Exception e) {
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public boolean getBoolean(String key) {
        Object o = getAppData(key);
        try {
            return ((Boolean) o).booleanValue();
        } catch (Exception ex) {
            return o != null && ("true".equals(o) || "1".equals(o));
        }
    }

    public long getLong(String key) {
        Object o = getAppData(key);
        try {
            return ((Long) o).longValue();
        } catch (ClassCastException ex) {
            try {
                return Long.parseLong(o.toString());
            } catch (Exception e) {
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public abstract void setAppData(String key, Serializable value);

    public abstract String[] getKeys();

    public abstract String getLocation();

    public String getTitle() {
        return title;
//        return getString(AppData.TITLE);
    }

    public int getChannelNumber() {
        return getInteger(AppData.CHANNEL_NUMBER);
    }

    public String getGroupKey() {
        return groupKey;
    }

    public int getDefinition() {
        return getInteger(AppData.DEFINITION);
    }

    public String getLanguage() {
        return getString(AppData.LANGUAGE);
    }

    public String getEpisodeTitle() {
        if (episodeTitle != null) {
            return episodeTitle;
        } else {
            return getTitle();
        }
    }

    public boolean isBlocked() {
        return getBoolean(AppData.BLOCK);
    }

    public int getViewedCount() {
        return getInteger(AppData.VIEW_COUNT);
    }

    public abstract int getRealState();

    public abstract boolean isClean();

    public abstract void stop();

    public abstract boolean delete();

    public boolean deleteIfPossible() {
        Log.printDebug(App.LOG_HEADER+"AbstractRecording.deleteIfPossible()");
        boolean inUse = isInUse();
        Log.printDebug(App.LOG_HEADER+"AbstractRecording.deleteIfPossible() : isInUse() returns "+inUse);
        if (inUse) {
            Log.printDebug(App.LOG_HEADER+"AbstractRecording.deleteIfPossible() returns false");
            return false;
        } else {
            boolean deleteResult = delete();
            Log.printDebug(App.LOG_HEADER+"AbstractRecording.deleteIfPossible() : delete() returns "+deleteResult);
            Log.printDebug(App.LOG_HEADER+"AbstractRecording.deleteIfPossible() returns "+deleteResult);
            return deleteResult;
        }
    }

    public short getStateGroup() {
        return RecordingListManager.getStateGroup(getState());
    }

    public boolean isInProgress() {
        return getStateGroup() == RecordingListManager.IN_PROGRESS;
    }

    public int getConvertedCategory() {
        int c = getInteger(AppData.CATEGORY);
        if (c == 15) {
            // movie
            int sub = getInteger(AppData.SUBCATEGORY);
            if (sub > 0) {
                return sub;
            } else {
                return c;
            }
        } else {
            return c;
        }
    }

    public String getRepeatText() {
        DataCenter dataCenter = DataCenter.getInstance();
        if (groupKey != null) {
            return PvrUtil.getRepeatText(RecordingScheduler.getInstance().getRepeatString(groupKey));
        } else {
            return dataCenter.getString("pvr.No");
        }
    }

    // save until ...
    public String getSaveText() {
        DataCenter dataCenter = DataCenter.getInstance();
        if (groupKey != null) {
            int keepCount = getKeepCount();
            if (keepCount > 0) {
                return TextUtil.replace(dataCenter.getString("pvr.last_x_recordings"), "%%", String.valueOf(keepCount));
            } else {
                return dataCenter.getString("pvr.keep_all");
            }
        } else {
            long expTime = getExpirationTime();
            if (expTime == Long.MAX_VALUE) {
                return dataCenter.getString("pvr.until_i_erase");
            } else {
                if (expTime - System.currentTimeMillis() > RecordManager.MAX_PERIOD) {
                    return dataCenter.getString("pvr.until_i_erase");
                } else {
                    long dur = Math.max(0, expTime - Math.max(System.currentTimeMillis(), getScheduledStartTime()));
                    return dataCenter.getString("pvr.For") + ' ' + Formatter.getCurrent().getDurationText2(dur);
                }
            }
        }
    }

    // delete in ...
    public String getDeletionText() {
        DataCenter dataCenter = DataCenter.getInstance();
        String groupKey = this.getGroupKey();
        if (groupKey == null) {
            long time = this.getExpirationTime();
            if (time == Long.MAX_VALUE) {
                return dataCenter.getString("pvr.until_i_erase.short");
            } else {
                long dur = time - System.currentTimeMillis();
                if (dur > RecordManager.MAX_PERIOD) {
                    return dataCenter.getString("pvr.until_i_erase.short");
                } else {
                    return dataCenter.getString("pvr.delete_in") + " " + Formatter.getCurrent().getDurationText3(dur);
                }
            }
        } else {
            int keepCount = getKeepCount();
            if (keepCount > 0) {
                return TextUtil.replace(dataCenter.getString("pvr.last_x"), "%%", String.valueOf(keepCount));
            } else {
                return dataCenter.getString("pvr.keep_all.short");
            }
        }
    }

    public abstract String getUdn();

    public static AbstractRecording find(String id) {
        Log.printDebug(App.LOG_HEADER+"AbstractRecording.find("+id+")");
        try {
            int localId = Integer.parseInt(id);
            RecordingRequest req = RecordingManager.getInstance().getRecordingRequest(localId);
            if (req != null && req instanceof LeafRecordingRequest) {
                return new LocalRecording((LeafRecordingRequest) req);
            }
            return null;
        } catch (Exception ex) {
            RecordingContentItem item = HomeNetManager.getInstance().find(id);
            if (item != null) {
                return new RemoteRecording(item);
            }
            return null;
        }
    }

    public static AbstractRecording find(int localId, String udn) throws IllegalArgumentException {
        Log.printDebug(App.LOG_HEADER+"AbstractRecording.find("+localId+", "+udn+")");
        if (udn == null || udn.equals(HomeNetManager.localUdn)) {
            RecordingRequest req = null;
            try {
                req = RecordingManager.getInstance().getRecordingRequest(localId);
            } catch (Exception ex) {
            }
            if (req != null && req instanceof LeafRecordingRequest) {
                return new LocalRecording((LeafRecordingRequest) req);
            }
            return null;
        } else {
            RecordingContentItem item = HomeNetManager.getInstance().find(localId, udn);
            if (item != null) {
                return new RemoteRecording(item);
            }
            return null;
        }
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }

}

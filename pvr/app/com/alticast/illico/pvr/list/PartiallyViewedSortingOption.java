package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class PartiallyViewedSortingOption extends SortingOption implements Comparable {

    public PartiallyViewedSortingOption() {
        super("sort.By Partially Viewed", FLAT);
    }

    public boolean accept(SortingKey sortingKey) {
        return sortingKey.groupKey != null;
    }

    public SortingKey getSortingKey(Entry entry) {
        int count = 0;
        long mediaTime = 0;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            count = rec.getViewedCount();
            mediaTime = rec.getMediaTime();
            CollationKey ck = frCollator.getCollationKey(entry.getTitle());
            Comparable c = new TimeBasedComparable(rec.getStartTime(), ck);
            //->Kenneth[2017.4.18] R7.3/VDTRMASTER-6109 : viewing state 에 관한 R7.3 수정 반영
            int viewingState = rec.getViewingState();
            return new SortingKey(entry, c, viewingState == AbstractRecording.PARTIALLY_VIEWED? this : null);
            //return new SortingKey(entry, c, (mediaTime != 0 && count == 0) ? this : null);
            //<-
        } else {
            return new SortingKey(entry, this, null);
        }
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return new FilterTitle("List.Partially_Viewed", vector);
    }

    public int compareTo(Object o) {
        return 0;
    }

}

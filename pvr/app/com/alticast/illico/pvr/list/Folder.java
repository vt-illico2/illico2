package com.alticast.illico.pvr.list;

import java.util.*;
import java.awt.*;

public abstract class Folder extends Entry {

    protected Vector children;

    public Folder(Vector children) {
        this.children = children;
    }

    public Vector getEntries() {
        return children;
    }

}


package com.alticast.illico.pvr.list;

import java.util.*;

/**
 * List 구조에 의해 compare가 빈번하기 때문에 별도로 sorting key를 관리하고,
 * Sorting이 끝나고 나서 entry를 추출한다.
 */
public final class SortingKey implements Comparable {
    Entry entry;
    Comparable compareKey;
    Object groupKey;

    public SortingKey(Entry entry, Comparable compareKey, Object groupKey) {
        this.entry = entry;
        this.compareKey = compareKey;
        this.groupKey = groupKey;
    }

    public int compareTo(Object c) {
        if (c instanceof SortingKey) {
            return compareKey.compareTo(((SortingKey) c).compareKey);
        } else {
            return compareTo(c);
        }
    }
}

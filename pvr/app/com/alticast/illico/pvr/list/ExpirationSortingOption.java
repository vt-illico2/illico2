package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class ExpirationSortingOption extends SortingOption {

    boolean ascending;

    public ExpirationSortingOption(boolean ascending) {
        super("sort.By save time", FLAT);
        this.ascending = ascending;
    }

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        long time = 0;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            String groupKey = rec.getGroupKey();
            if (groupKey == null) {
                time = Math.min(rec.getExpirationTime(), Long.MAX_VALUE - 1000);
            } else {
                int count = rec.getKeepCount();
                if (count == 0) {
                    time = Long.MAX_VALUE;
                } else {
                    time = Long.MAX_VALUE - 1000 - count;
                }
            }
        }
        if (!ascending) {
            time = -time;
        }
        return new SortingKey(entry, new StringBasedComparable(entry.getTitle(), new Long(time)), null);
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return null;
    }
}

package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class CompletedViewdSortingOption extends SortingOption implements Comparable {

    private boolean byViewCount;

    public CompletedViewdSortingOption() {
        this(false);
    }

    public CompletedViewdSortingOption(boolean byViewCount) {
        super("sort.By Completely Viewed", FLAT);
        this.byViewCount = byViewCount;
    }

    public boolean accept(SortingKey sortingKey) {
        return sortingKey.groupKey != null;
    }

    public SortingKey getSortingKey(Entry entry) {
        int count = 0;
        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            count = rec.getViewedCount();
            Comparable c;
            if (byViewCount) {
                c = new Integer(-count);
            } else {
                CollationKey ck = frCollator.getCollationKey(entry.getTitle());
                c = new TimeBasedComparable(rec.getStartTime(), ck);
            }
            //->Kenneth[2017.4.18] R7.3/VDTRMASTER-6109 : viewing state 에 관한 R7.3 수정 반영
            int viewingState = rec.getViewingState();
            return new SortingKey(entry, c, viewingState == AbstractRecording.COMPLETELY_VIEWED ? this : null);
            //return new SortingKey(entry, c, count > 0 ? this : null);
            //<-
        } else {
            return new SortingKey(entry, this, null);
        }
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return new FilterTitle("List.Completely_Viewed", vector);
    }

    public int compareTo(Object o) {
        return 0;
    }

}

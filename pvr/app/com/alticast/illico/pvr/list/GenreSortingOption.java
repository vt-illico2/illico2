package com.alticast.illico.pvr.list;

import java.util.*;
import java.text.Collator;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.log.Log;

public class GenreSortingOption extends SortingOption {

    private GenreSortingOptionByGenre mainOption;
    private DateSortingOption subOption = new DateSortingOption(TREE, false);

    public GenreSortingOption() {
        super("sort.By genre", GROUP, true);
        mainOption = new GenreSortingOptionByGenre(this.currentOnTop);
    }

    private ProgressComparable progressComparable = new ProgressComparable();

    public boolean accept(SortingKey sortingKey) {
        return true;
    }

    public SortingKey getSortingKey(Entry entry) {
        if (entry instanceof GenreSeparator) {
            GenreSeparator gs = (GenreSeparator) entry;
            return new SortingKey(entry, gs, null);
        } else {
            return new SortingKey(entry, progressComparable, null);
        }
    }

    /** RecordingVector로 통합. */
    public Vector prepareRecordings(Vector deviceVector) {
        Vector vector = super.prepareRecordings(deviceVector);
        Vector folders = mainOption.sort(vector);

        Vector newVector = new Vector();
        Enumeration en = folders.elements();
        while (en.hasMoreElements()) {
            Object o = en.nextElement();
            if (o instanceof GenreSeparator) {
                GenreSeparator f = (GenreSeparator) o;
                Vector sortedChildren = subOption.sort(f.getEntries());
                newVector.add(new GenreSeparator(f.genre, sortedChildren));
            } else if (currentOnTop && o instanceof AbstractRecording && ((AbstractRecording) o).isInProgress()) {
                newVector.add(o);
            }
        }
        return newVector;
    }

    public Folder createFolder(Object groupKey, Vector vector) {
        return subOption.createFolder(groupKey, vector);
    }

    class ProgressComparable implements Comparable {
        public int compareTo(Object o) {
            return -1;
        }
    }

    class GenreSortingOptionByGenre extends SortingOption {
        public GenreSortingOptionByGenre(boolean currentOnTop) {
            super("sort.By genre", GROUP, currentOnTop);
        }

        public boolean accept(SortingKey sortingKey) {
            return true;
        }

        public SortingKey getSortingKey(Entry entry) {
            Comparable compareKey;
            Comparable groupKey;
            if (entry instanceof GenreSeparator) {
                compareKey = (GenreSeparator) entry;
                groupKey = null;
            } else {
                int genre = 0;
                if (entry instanceof AbstractRecording) {
                    AbstractRecording rec = (AbstractRecording) entry;
                    genre = rec.getConvertedCategory();
                    if (genre == 0) {
                        genre = Integer.MAX_VALUE;
                    }
                    groupKey = new Integer(genre);
                    compareKey = new StringBasedComparable(entry.getTitle(),
                                        new TimeBasedComparable(rec.getStartTime(), groupKey));
                } else {
                    groupKey = new Integer(genre);
                    compareKey = new StringBasedComparable(entry.getTitle(), groupKey);
                }
            }
            return new SortingKey(entry, compareKey, groupKey);
        }

        public Folder createFolder(Object groupKey, Vector vector) {
            if (groupKey instanceof Integer) {
                return new GenreSeparator(((Integer) groupKey).intValue(), vector);
            } else {
                Log.printWarning("GenreSortingOption: groupKey is not Integer: " + groupKey);
                return new GenreSeparator(0, vector);
            }
        }


    }

}

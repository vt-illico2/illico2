package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.framework.DataCenter;

public class GenreSeparator extends Separator implements Comparable {

    int genre;
    int originalKey;

    public GenreSeparator(int genre, Vector children) {
        super(children);
        this.genre = genre;
        if (genre == Integer.MAX_VALUE) {
            originalKey = 0;
        } else {
            originalKey = genre;
        }
    }

    public String getTitle() {
        return DataCenter.getInstance().getString("Categories." + originalKey);
    }

    public int compareTo(Object c) {
        if (c instanceof GenreSeparator) {
            GenreSeparator gs = (GenreSeparator) c;
            return genre - gs.genre;
        } else {
            return 0;
        }
    }

    public boolean equals(Object o) {
        if (o instanceof GenreSeparator) {
            return compareTo(o) == 0;
        } else {
            return false;
        }
    }

}


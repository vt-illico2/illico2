package com.alticast.illico.pvr.list;

import java.io.*;
import javax.tv.service.*;
import javax.media.Time;
import java.util.Date;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.hn.content.IOStatus;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.HomeNetManager;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.framework.*;

public class LocalRecording extends AbstractRecording {

    private LeafRecordingRequest request;
    private boolean isClean;

    private long size = -1;
    private long duration = -1;
    private int id;

    public LocalRecording(LeafRecordingRequest req) {
        this.request = req;
        try {
            id = req.getId();
        } catch (Exception ex) {
        }
        idObject = new Integer(id);
        state = getRealState();
        channelName = AppData.getString(req, AppData.CHANNEL_NAME);
        Object rating = getAppData(AppData.PARENTAL_RATING);
        if (rating != null) {
            this.rating = rating.toString();
            ratingIndex = PvrUtil.getRatingIndex(this.rating);
        }
        groupKey = (String) getAppData(AppData.GROUP_KEY);
        episodeTitle = (String) getAppData(AppData.EPISODE_TITLE);
        title = getString(AppData.TITLE);
        //->Kenneth[2017.8.22] R7.4 : EnglishTitle 생김
        englishTitle = getString(AppData.ENGLISH_TITLE);
        //<-
        if (episodeTitle != null && episodeTitle.length() <= 0) {
            episodeTitle = null;
        }

//        programStartTime = getLong(AppData.PROGRAM_START_TIME);
//        if (programStartTime == 0) {
//            programStartTime = getScheduledStartTime();
//        }
//        programEndTime = getLong(AppData.PROGRAM_END_TIME);
//        if (programEndTime == 0) {
//            programEndTime = programStartTime + getScheduledDuration();
//        }
        startTime = getScheduledStartTime();
        endTime = startTime + getScheduledDuration();
        isClean = RecordingListManager.checkClean(this);

        short stateGroup = RecordingListManager.getStateGroup(state);
        if (stateGroup == RecordingListManager.COMPLETED) {
            size = getRecordedSize();
            duration = getRecordedDuration();
        }
    }

    public LeafRecordingRequest getRecordingRequest() {
        return request;
    }

    public int getId() {
        return id;
    }

    public int getLocalId() {
        return id;
    }

    public int getRealState() {
        try {
            return request.getState();
        } catch (Exception ex) {
        }
        return 0;
    }

    public boolean isInUse() {
        try {
            return ((IOStatus) request).isInUse();
        } catch (Exception ex) {
            return false;
        }
    }

    public boolean isProtected() {
        return ParentalControl.isProtected();
    }

    public long getScheduledStartTime() {
        return AppData.getScheduledStartTime(request);
    }

    public long getScheduledDuration() {
        return AppData.getScheduledDuration(request);
    }

    public long getExpirationPeriod() {
        return RecordManager.getExpirationPeriod(request);
    }

    public void setExpirationPeriod(long duration) {
        RecordManager.setExpirationPeriod(request, duration);
    }

    public static long getMediaTime(RecordingRequest rr) {
        try {
            return PvrUtil.getTime(((OcapRecordedService) ((LeafRecordingRequest) rr).getService()).getMediaTime());
        } catch (Exception ex) {
            return 0L;
        }
    }

    public long getMediaTime() {
        OcapRecordedService service = (OcapRecordedService) getService();
        if (service != null) {
            return PvrUtil.getTime(service.getMediaTime());
        } else {
            return 0;
        }
    }

    public void setMediaTime(long ms) {
        try {
            Time time;
            if (ms == Long.MAX_VALUE) {
                time = new Time(Double.POSITIVE_INFINITY);
            } else {
                time = new Time(ms * 1000000);
            }
            ((RecordedService) getService()).setMediaTime(time);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public RecordingRequest reschedule(long start, long dur) {
//        RecordManager.getInstance().reschedule(request, start, dur);
        return RecordManager.getInstance().replace(request, start, dur);
    }

    public int getKeepCount() {
        if (groupKey != null) {
            return RecordingScheduler.getInstance().getKeepCount(groupKey);
        }
        return 0;
    }

    ///////////////////////////////////////////////////////////////////////
    // Service
    ///////////////////////////////////////////////////////////////////////

    public Service getService() {
        try {
            return request.getService();
        } catch (Exception ex) {
            return null;
        }
    }

    public static long getRecordedSize(RecordingRequest req) {
        try {
            return ((OcapRecordedService) ((LeafRecordingRequest) req).getService()).getRecordedSize();
        } catch (Exception ex) {
        }
        return 0;
    }

    public long getRecordedSize() {
        if (size >= 0) {
            return size;
        }
//        if (com.videotron.tvi.illico.util.Environment.EMULATOR) {
//            return ((long) id) * 1024 * 1024;
//        }
        Service service = getService();
        if (service != null && service instanceof OcapRecordedService) {
            return ((OcapRecordedService) service).getRecordedSize();
        }
        return 0;
    }

    public long getRecordedDuration() {
        if (duration >= 0) {
            return duration;
        }
        try {
            Service service = getService();
            if (service != null) {
                return ((RecordedService) service).getRecordedDuration();
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public Date getRecordingStartTime() {
        try {
            Service service = getService();
            if (service != null) {
                return ((RecordedService) service).getRecordingStartTime();
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public boolean isClean() {
        return isClean;
    }

    public void stop() {
        try {
            request.stop();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public boolean delete() {
        RecordManager.delete(request);
        return true;
    }

    public long getSpaceRequired() {
        if (request instanceof OcapRecordingRequest) {
            return ((OcapRecordingRequest) request).getSpaceRequired();
        } else {
            return -1;
        }
    }

    public long[] getSegmentTimes() {
        return getSegmentTimes(getService());
    }

    public static long[] getSegmentTimes(Service rs) {
        if (rs != null && rs instanceof SegmentedRecordedService) {
            SegmentedRecordedService ss = (SegmentedRecordedService) rs;
            RecordedService[] segments = ss.getSegments();
            if (segments != null) {
                if (segments.length >= 2) {
                    long[] indicationTimes = new long[segments.length - 1];
                    long time = 0;
                    for (int i = 0; i < indicationTimes.length; i++) {
                        time += segments[i].getRecordedDuration();
                        indicationTimes[i] = time;
                        if (Log.DEBUG_ON) {
                            Log.printDebug("LocalRecording: segmentTimes[" + i + "] = " + time);
                        }
                    }
                    return indicationTimes;
                }
            }
        }
        return null;
    }

    public String getUdn() {
        return HomeNetManager.localUdn;
    }

    ///////////////////////////////////////////////////////////////////////
    // AppData
    ///////////////////////////////////////////////////////////////////////

    public Object getAppData(String key) {
        return AppData.get(request, key);
    }

    public void setAppData(String key, Serializable value) {
        AppData.set(request, key, value);
    }

    public void removeAppData(String key) {
        AppData.remove(request, key);
    }

    public String[] getKeys() {
        return request.getKeys();
    }

    public String getLocation() {
        return DataCenter.getInstance().getString("pvr.Local");
    }

    public boolean equals(Object o) {
        if (o != null && o instanceof LocalRecording) {
            return ((LocalRecording) o).id == id;
        }
        return false;
    }

}

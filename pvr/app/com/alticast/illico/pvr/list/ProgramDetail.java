package com.alticast.illico.pvr.list;

import java.awt.Graphics;
import java.util.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.*;

public abstract class ProgramDetail extends Entry {

    protected Object idObject;

    protected int channelNumber;
    protected String channelName;
    protected String groupKey;
    protected String title;

    protected int definition;
    protected String language;
    protected int ratingIndex;
    protected String rating;

    public int getChannelNumber() {
        return channelNumber;
    }

    public String getChannelName() {
        return channelName;
    }

    public String getGroupKey() {
        return groupKey;
    }

    public String getTitle() {
        return title;
    }

    public abstract int getConvertedCategory();

    public abstract boolean isBlocked();

    public String getGenreName() {
        return DataCenter.getInstance().getString("Categories." + getConvertedCategory());
    }

    public int getDefinition() {
        return definition;
    }

    public String getLanguage() {
        return language;
    }

    public String getRating() {
        return rating;
    }

    public int getRatingIndex() {
        return ratingIndex;
    }

    public Object getIdObject() {
        return idObject;
    }

}


package com.alticast.illico.pvr.hn;

import org.ocap.hn.NetworkInterface;
import org.ocap.hn.security.NetSecurityManager;
import org.ocap.hn.security.NetAuthorizationHandler;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.*;

public class NetSecurityManagerImpl {
    NetSecurityManager manager = NetSecurityManager.getInstance();

    private static NetSecurityManagerImpl instance = new NetSecurityManagerImpl();

    NetworkInterface moCa = null;

    private NetSecurityManagerImpl() {
        NetworkInterface[] net = NetworkInterface.getNetworkInterfaces();
        Log.printDebug("net = " + net);
        for (int a = 0; net != null && a < net.length; a++) {
            if (net[a].getType() == NetworkInterface.MOCA) {
                moCa = net[a];
                Log.printDebug("NetSecurityManagerImpl: Found MOCA Interface = " + moCa);
                break;
            }
        }
    }

    public static NetSecurityManagerImpl getInstance() {
        return instance;
    }

    public void disableMocaPrivacy() {
        Log.printDebug(App.LOG_HEADER+"NetSecurityManagerImpl.disableMocaPrivacy()");
        manager.disableMocaPrivacy(moCa);
    }

    public void enableMocaPrivacy() {
        Log.printDebug(App.LOG_HEADER+"NetSecurityManagerImpl.enableMocaPrivacy()");
        try {
            manager.enableMocaPrivacy(moCa);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public String getNetworkPassword() {
        Log.printDebug(App.LOG_HEADER+"NetSecurityManagerImpl.getNetworkPassword() returns "+manager.getNetworkPassword(moCa));
        return manager.getNetworkPassword(moCa);
    }

    public void setNetworkPassword(String pw) {
        Log.printDebug(App.LOG_HEADER+"NetSecurityManagerImpl.setNetworkPassword("+pw+")");
        try {
            manager.setNetworkPassword(moCa, pw);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void setAuthorizationHandler(NetAuthorizationHandler nah) {
        Log.printDebug(App.LOG_HEADER+"NetSecurityManagerImpl.setAuthorizationHandler("+nah+")");
        manager.setAuthorizationHandler(nah);
    }


}

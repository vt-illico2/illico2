package com.alticast.illico.pvr.hn;

import org.ocap.hn.NetActionEvent;
import org.ocap.hn.NetActionHandler;
import org.ocap.hn.NetActionRequest;
import org.ocap.hn.content.ContentContainer;
import org.ocap.hn.content.navigation.ContentList;
import org.ocap.hn.recording.RecordingContentItem;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.log.Log;

public class NetActionHandlerImpl implements NetActionHandler {
    public NetActionRequest request;
    public Object response;
    public int status;
    public ContentList contentList;
    public ContentContainer contentContainer;
    public String searchCb[];

    public NetActionHandlerImpl() {
        //->Kenneth[2015.1.9] : VDTRMASTER-5351 : 1초 내에 requestDelete 의 응답이 오지 않는 경우 발생, Cisco 에서.
        // 그리고 문제가 ACTION_COMPLETED 가 0 으로 정의되어 있어서 notify 를 못 받은 상황임에도 받은 것으로 인식하는
        // 오류 있음. 따라서 초기값을 -1 로 세팅
        status = -1;
    }

    public synchronized Object waitForResponse() {
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.waitForResponse()");
        if (response == null) {
            try {
                this.wait();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.waitForResponse() returns "+response);
        return response;
    }

    public synchronized Object waitForResponse(long ms) {
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.waitForResponse("+ms+")");
        if (response == null) {
            try {
                this.wait(ms);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.waitForResponse() returns "+response);
        return response;
    }

    public int getStatus() {
        return status;
    }

    public boolean isCompleted() {
        return status == NetActionEvent.ACTION_COMPLETED;
    }

    public synchronized void notify(NetActionEvent event) {
        status = event.getActionStatus();
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.notify : state is " + getActionStatusString(status));
        request = event.getActionRequest();
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.notify : request is " + request);
        response = event.getResponse();
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.notify : response is " + response);

        if (status == NetActionEvent.ACTION_COMPLETED) {
            Object obj = response;
            Log.printInfo("NetActionHandlerImpl.notify: response = " + obj);
            if (obj instanceof ContentList) {
                contentList = (ContentList) obj;
            } else if (obj instanceof ContentContainer) {
                contentContainer = (ContentContainer) obj;
            } else if (obj instanceof String[]) {
                Log.printInfo("NetActionHandlerImpl.notify: Receive response on requesting search capabilities");
                searchCb = (String[]) obj;
            } else if (obj instanceof RecordingContentItem) {
            }
        }
        Log.printDebug(App.LOG_HEADER+"NetActionHandlerImpl.notify : Call this.notifyAll()");
        this.notifyAll();
    }

    public static String getActionStatusString(int status) {
        switch (status) {
        case NetActionEvent.ACTION_CANCELED:
            return "NetActionEvent.ACTION_CANCELED";
        case NetActionEvent.ACTION_FAILED:
            return "NetActionEvent.ACTION_FAILED";
        case NetActionEvent.ACTION_IN_PROGRESS:
            return "NetActionEvent.ACTION_IN_PROGRESS";
        case NetActionEvent.ACTION_COMPLETED:
            return "NetActionEvent.ACTION_COMPLETED";
        case NetActionEvent.ACTION_STATUS_NOT_AVAILABLE:
            return "NetActionEvent.ACTION_STATUS_NOT_AVAILABLE";
        default:
            return "NetActionEvent.UNKNOWN";
        }
    }
}

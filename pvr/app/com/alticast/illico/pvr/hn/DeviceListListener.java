package com.alticast.illico.pvr.hn;

import java.util.*;

public interface DeviceListListener {

    public void deviceListChanged(Vector deviceList);

}
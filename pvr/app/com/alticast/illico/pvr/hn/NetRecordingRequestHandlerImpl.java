package com.alticast.illico.pvr.hn;

import java.net.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.hn.*;
import org.ocap.hn.content.*;
import org.ocap.hn.recording.*;
import org.ocap.hn.content.IOStatus;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.log.Log;

public class NetRecordingRequestHandlerImpl implements NetRecordingRequestHandler {

    public boolean notifyDelete(InetAddress address, ContentEntry recording) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyDelete = " + recording);
        }
        if (recording != null && recording instanceof RecordingRequest) {
            final RecordingRequest rr = (RecordingRequest) recording;
            if (VideoPlayer.getInstance().isPlaying(rr)) {
                Log.printWarning("NetRecordingRequestHandler.notifyDelete : recording is being played");
                return false;
            } else if (recording instanceof IOStatus && ((IOStatus) recording).isInUse()) {
                Log.printWarning("NetRecordingRequestHandler.notifyDelete : recording is in use");
                return false;
            } else {
                Thread t = new Thread("Remote.delete") {
                    public void run() {
                        try {
                            rr.delete();
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                };
                t.start();
                return true;
            }
        } else {
            return false;
        }
    }

    public boolean notifyDeleteService(InetAddress address, ContentEntry recording) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyDeleteService = " + recording);
        }
        // TODO
        return false;
    }

    public boolean notifyDisable(InetAddress address, ContentEntry recording) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyDisable = " + recording);
        }
        if (recording != null && recording instanceof LeafRecordingRequest) {
            LeafRecordingRequest rr = (LeafRecordingRequest) recording;
            try {
                rr.stop();
            } catch (Exception ex) {
                Log.print(ex);
            }
            return true;
        }
        return false;
    }

    public boolean notifyPrioritization(InetAddress address, NetRecordingEntry[] recordings) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyPrioritization");
        }
        // TODO
        return false;
    }

    public boolean notifyPrioritization(InetAddress address, RecordingContentItem[] recordings) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyPrioritization");
        }
        // TODO
        return false;
    }

    public boolean notifyReschedule(InetAddress address, ContentEntry recording, NetRecordingEntry spec) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifyReschedule = " + recording);
        }
        // TODO
        return false;
    }

    public boolean notifySchedule(InetAddress address, NetRecordingEntry spec) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"NetRecordingRequestHandler.notifySchedule = " + spec);
        }
        // TODO
        return false;
    }

}

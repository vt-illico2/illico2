package com.alticast.illico.pvr.hn;

import org.ocap.hn.*;
import org.ocap.hn.content.*;
import org.ocap.hn.content.navigation.*;
import org.ocap.hn.recording.*;
import org.ocap.hn.security.*;
import org.ocap.hn.service.*;
import org.ocap.hn.profiles.upnp.*;
import java.util.*;
import com.alticast.illico.pvr.VideoPlayer;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.DataCenter;

public class RemoteDevice implements ContentServerListener {

    private static final boolean ALWAYS_BROWSE_ALL = false;

    public static final byte LOADING_STATE = 0;
    public static final byte COMPLETED_STATE = 1;

    private static int MAX_NUMBER_IDS_FOR_SEARCH = 30;

    public static HashSet protectedSet = new HashSet();

    private byte state = LOADING_STATE;
    private boolean disposed = false;

    Device device;
    ContentServerNetModule csnm;
    Hashtable recordings = new Hashtable();
    NetActionRequest request;

    ContentContainer root;
    MetadataNode node;

    String udn;

    public long totalSpace = 1;
    public long freeSpace = 0;
    public boolean protection = false;

    UpdateWorker updateWorker;

    RequestRootHandler requestRootHandler = new RequestRootHandler();

    static {
        MAX_NUMBER_IDS_FOR_SEARCH = Math.max(5, DataCenter.getInstance().getInt("MAX_NUMBER_IDS_FOR_SEARCH", 30));
    }

    public RemoteDevice(Device dev) {
        this(dev, (ContentServerNetModule) dev.getNetModule(NetModule.CONTENT_SERVER));
    }

    public RemoteDevice(Device dev, ContentServerNetModule csnm) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.Constructor");
        this.device = dev;
        this.csnm = csnm;
        if (csnm != null) {
            csnm.addContentServerListener(this);
        }
        this.udn = dev.getProperty(Device.PROP_UDN);

        updateWorker = new UpdateWorker("UpdateWorker." + udn);
        boolean success = reset(true);
        if (!success) {
            state = COMPLETED_STATE;
        }
    }

    public void updateRootContainer(boolean sync) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.updateRootContainer("+sync+")");
        if (disposed) {
            return;
        }
        if (Log.INFO_ON) {
            Log.printInfo("RemoteDevice.updateRootContainer : sync = " + sync);
        }
        if (csnm == null) {
            Log.printWarning("RemoteDevice.updateRootContainer : ContentServerNetModule is null");
            return;
        }
//        if (sync) {
//            NetActionHandlerImpl handler = new NetActionHandlerImpl();
//            csnm.requestRootContainer(handler);
//            requestRootHandler.setRoot(handler.waitForResponse());
//        } else {
            csnm.requestRootContainer(requestRootHandler);
//        }
    }

    public int size() {
        return recordings.size();
    }

    public synchronized RecordingContentItem[] getRecordings() {
        RecordingContentItem[] items = new RecordingContentItem[recordings.size()];
        return (RecordingContentItem[]) recordings.values().toArray(items);
    }

    public RecordingContentItem find(String id) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.find("+id+")");
        if (!id.endsWith(udn)) {
            return null;
        }
        RecordingContentItem[] array = getRecordings();
        for (int i = 0; i < array.length; i++) {
            if (id.equals(array[i].getID() + udn)) {
                return array[i];
            }
        }
        return null;
    }

    public RecordingContentItem find(int localId) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.find("+localId+") -- 2");
        RecordingContentItem[] array = getRecordings();
        for (int i = 0; i < array.length; i++) {
            MetadataNode node = array[i].getRootMetadataNode();
            if (node != null) {
                Object o = node.getMetadata(AppData.LOCAL_ID);
                if (o != null) {
                    if (o instanceof Integer) {
                        if (((Integer) o).intValue() == localId) {
                            return array[i];
                        }
                    } else {
                        try {
                            if (Integer.parseInt(o.toString()) == localId) {
                                return array[i];
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }
        return null;
    }

    public boolean reset() {
        return reset(false);
    }

    private synchronized boolean reset(boolean sync) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.reset("+sync+")");
        if (disposed) {
            return false;
        }
        if (Log.INFO_ON) {
            Log.printInfo("RemoteDevice.reset: UDN = " + udn);
        }
        updateRootContainer(sync);
        if (csnm != null) {
            updateWorker.push(new BrowseJob());
            return true;
        } else {
            Log.printWarning("RemoteDevice.reset: ContentServerNetModule is null");
            return false;
        }
    }

    public synchronized void dispose() {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.dispose()");
        disposed = true;
        protectedSet.remove(udn);
        if (csnm != null) {
            csnm.removeContentServerListener(this);
        }
        recordings.clear();
        if (request != null) {
            request.cancel();
            request = null;
        }
    }

//    public byte getState() {
//        return state;
//    }

    public synchronized void deleteRecording(RecordingContentItem item) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.deleteRecording("+item+")");
        recordings.remove(item.getID());
    }

    private ContentList requestSearchEntries(HashSet set) {
        boolean containsRoot = set.remove("0");
        Object[] array = set.toArray();
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.requestSearchEntries: size = " + array.length);
        if (containsRoot) {
            updateRootContainer(false);
        }
        if (array.length <= 0) {
            return null;
        }

        StringBuffer sb = new StringBuffer();
        sb.append("@id = \"");
        sb.append(array[0]);
        sb.append('"');

        for (int i = 1; i < array.length; i++) {
            sb.append(" or @id = \"");
            sb.append(array[i]);
            sb.append('"');
        }
        String sc = sb.toString();

        Log.printDebug("RemoteDevice.requestSearchEntries: " + sc);
        if (csnm != null) {
            NetActionHandlerImpl handler = new NetActionHandlerImpl();
            csnm.requestSearchEntries("0", "*", 0, 0, sc, "", handler);

            long timeout = array.length * 20 * Constants.MS_PER_SECOND;
            timeout = Math.min(timeout, 5 * Constants.MS_PER_MINUTE);
            timeout = Math.max(timeout, 30 * Constants.MS_PER_SECOND);
            handler.waitForResponse(timeout);
            return handler.contentList;
        } else {
            return null;
        }
    }

    private ContentList requestBrowseEntries(String start) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.requestBrowseEntries: start = " + start);
        if (csnm != null) {
            NetActionHandlerImpl handler = new NetActionHandlerImpl();
            csnm.requestBrowseEntries(start, "*", true, 0, 0, "", handler);
            handler.waitForResponse();
            return handler.contentList;
        } else {
            return null;
        }
    }

    /*
    public void notify(NetActionEvent event) {
        int status = event.getActionStatus();
        if (Log.INFO_ON) {
            Log.printInfo("RemoteDevice.notify = " + NetActionHandlerImpl.getActionStatusString(status));
            Log.printInfo("RemoteDevice.notify: UDN = " + udn);
        }
        NetActionRequest naq = event.getActionRequest();
        Log.printInfo("RemoteDevice: request = " + naq + ", old = " + request);
        switch (status) {
            case NetActionEvent.ACTION_CANCELED:
                break;
            case NetActionEvent.ACTION_FAILED:
                state = COMPLETED_STATE;
                synchronized (this) {
                    if (naq != null && naq.equals(request)) {
                        HomeNetManager.getInstance().deviceStateChanged(this);
                        request = null;
                    }
                }
                break;
            case NetActionEvent.ACTION_COMPLETED:
                Object o = event.getResponse();
                if (o != null && o instanceof ContentList) {
                    HashSet set = new HashSet();
                    processList(set, (ContentList) o);

                    Log.printInfo("RemoteDevice: found " + set.size() + " recordings");
                    MetadataDiagScreen.getInstance().writeLog("RemoteDevice: found " + set.size() + " recordings");
                    synchronized (this) {
                        this.recordings = set;
                    }
                }
                state = COMPLETED_STATE;
                synchronized (this) {
                    if (naq != null && naq.equals(request)) {
                        Log.printDebug("RemoteDevice: update");
                        HomeNetManager.getInstance().deviceStateChanged(this);
                        request = null;
                    } else {
                        Log.printDebug("RemoteDevice: update failed");
                    }
                }
                break;
            case NetActionEvent.ACTION_STATUS_NOT_AVAILABLE:
            case NetActionEvent.ACTION_IN_PROGRESS:
            default:
                break;
        }
    }
    */

    public Device getDevice() {
        return device;
    }

    public String getUdn() {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.getUdn returns "+udn+"");
        return udn;
    }

    public String getFriendlyName() {
        return device.getProperty(Device.PROP_FRIENDLY_NAME);
    }

    public Object getAppData(String key) {
        return node.getMetadata(key);
    }

    public long getLong(String key) {
        Object o = getAppData(key);
        try {
            return ((Long) o).longValue();
        } catch (ClassCastException ex) {
            try {
                return Long.parseLong(o.toString());
            } catch (Exception e) {
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public boolean getBoolean(String key) {
        Object o = getAppData(key);
        try {
            return ((Boolean) o).booleanValue();
        } catch (Exception ex) {
            return o != null && ("true".equals(o) || "1".equals(o));
        }
    }

    class RequestRootHandler implements NetActionHandler {
        public void notify(NetActionEvent event) {
            int status = event.getActionStatus();
            if (Log.INFO_ON) {
                Log.printInfo(App.LOG_HEADER+"RemoteDevice.RequestRootHandler.notify = " + NetActionHandlerImpl.getActionStatusString(status));
                Log.printInfo(App.LOG_HEADER+"RemoteDevice.RequestRootHandler.notify: UDN = " + udn);
            }
            NetActionRequest naq = event.getActionRequest();
            if (status == NetActionEvent.ACTION_COMPLETED) {
                setRoot(event.getResponse());
            }
        }

        private void setRoot(Object o) {
        Log.printDebug(App.LOG_HEADER+"RemoteDevice.RequestRootHandler.setRoot()");
            if (o != null && o instanceof ContentContainer) {
                ContentContainer con = (ContentContainer) o;
                root = con;
                node = con.getRootMetadataNode();
                try {
                    totalSpace = Math.max(getLong(HomeNetManager.KEY_TOTAL_SPACE), 1);
                    freeSpace = getLong(HomeNetManager.KEY_FREE_SPACE);
                    protection = getBoolean(HomeNetManager.KEY_PROTECTED);
                    if (protection) {
                        protectedSet.add(udn);
                    } else {
                        protectedSet.remove(udn);
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("RemoteDevice.updateRootContainer: totalSpace = " + totalSpace);
                        Log.printDebug("RemoteDevice.updateRootContainer: freeSpace = " + freeSpace);
                        Log.printDebug("RemoteDevice.updateRootContainer: protected = " + protection + " : " + getAppData(HomeNetManager.KEY_PROTECTED));
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            } else {
                Log.printWarning("RemoteDevice.RequestRootHandler: response is not a ContentContainer = " + o);
            }
        }
    }


    ////////////////////////////////////////////////////////////////////
    // ContentServerListener
    ////////////////////////////////////////////////////////////////////

    public void contentUpdated(final ContentServerEvent evt) {
        Log.printInfo(App.LOG_HEADER+"ContentServerListener.contentUpdated = " + evt);
        int event = evt.getEventID();
        String[] ids = evt.getContent();
        if (Log.DEBUG_ON) {
            String s = HomeNetManager.getContentServerEventString(event) + " " + PvrUtil.toString(ids);
            MetadataDiagScreen.getInstance().writeLog(s);
            Log.printDebug("ContentServerListener: " + s);
        }
        if (ALWAYS_BROWSE_ALL) {
            Thread t = new Thread("RemoteDevice.contentUpdated") {
                public void run() {
                    reset();
                }
            };
            t.start();
        } else {
            if (ids == null || ids.length <= 0) {
                Log.printWarning("RemoteDevice.contentUpdated: with null ID");
                return;
            }
            switch (event) {
                case ContentServerEvent.CONTENT_CHANGED:
                    updateWorker.changeContents(ids);
                    break;
                case ContentServerEvent.CONTENT_ADDED:
                    updateWorker.addContents(ids);
                    break;
                case ContentServerEvent.CONTENT_REMOVED:
                    updateWorker.removeContents(ids);
                    break;
            }
        }
    }

    class UpdateWorker extends Worker {

        public UpdateWorker(String name) {
            super(name, false);
        }

        public void addContents(String[] ids) {
            newContentJob(ids, true);
        }

        public void changeContents(String[] ids) {
            newContentJob(ids, false);
        }

        private synchronized void newContentJob(String[] ids, boolean add) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.UpdateWorker.newContentJob("+ids+")");
            if (ids == null || ids.length == 0) {
                return;
            }
            AddContentJob last = null;
            try {
                last = (AddContentJob) lastElement();
            } catch (Exception ex) {
            }
            if (last != null && last.size() + ids.length <= MAX_NUMBER_IDS_FOR_SEARCH) {
                last.merge(ids, add);
            } else if (ids.length <= MAX_NUMBER_IDS_FOR_SEARCH) {
                push(new AddContentJob(ids, add));
            } else {
                int l = 0;
                int r = l + MAX_NUMBER_IDS_FOR_SEARCH;
                do {
                    String[] newIds = new String[r - l];
                    System.arraycopy(ids, l, newIds, 0, r - l);
                    push(new AddContentJob(newIds, add));
                    l = r;
                    r = Math.min(l + MAX_NUMBER_IDS_FOR_SEARCH, ids.length);
                } while (l < ids.length);
            }
        }

        public synchronized void removeContents(String[] ids) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.UpdateWorker.removeContents("+ids+")");
            if (ids == null || ids.length == 0) {
                return;
            }
            RemoveContentJob last = null;
            try {
                last = (RemoveContentJob) lastElement();
            } catch (Exception ex) {
            }
            if (last != null) {
                last.merge(ids);
            } else {
                push(new RemoveContentJob(ids));
            }
        }
    }

    class BrowseJob implements Runnable, NetActionHandler {
        public synchronized void run() {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.BrowseJob.run()");
            if (request != null) {
                Log.printDebug("RemoteDevice.reset: cancel old request = " + request);
                request.cancel();
            }
            if (disposed) {
                return;
            }
            updateWorker.clear();
            MetadataDiagScreen.getInstance().writeLog("RemoteDevice: requestBrowseEntries");
            request = csnm.requestBrowseEntries("0", "*", true, 0, 0, "", BrowseJob.this);
            try {
                BrowseJob.this.wait();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        public synchronized void notify(NetActionEvent event) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.BrowseJob.notify("+event+")");
            int status = event.getActionStatus();
            NetActionRequest naq = event.getActionRequest();
            if (Log.INFO_ON) {
                Log.printInfo("RemoteDevice.notify = " + NetActionHandlerImpl.getActionStatusString(status));
                Log.printInfo("RemoteDevice.notify: UDN = " + udn);
                Log.printInfo("RemoteDevice: request = " + naq + ", old = " + request);
            }
            switch (status) {
                case NetActionEvent.ACTION_CANCELED:
                    break;
                case NetActionEvent.ACTION_FAILED:
                    state = COMPLETED_STATE;
                    MetadataDiagScreen.getInstance().writeLog("RemoteDevice: browse failed. retry...");
                    synchronized (RemoteDevice.this) {
                        if (naq != null && naq.equals(request)) {
                            HomeNetManager.getInstance().deviceStateChanged(RemoteDevice.this);
                            request = null;
                        }
                    }
                    Thread t = new Thread() {
                        public void run() {
                            try {
                                sleep(3000L);
                            } catch (Exception ex) {
                            }
                            reset();
                        }
                    };
                    t.start();
                    break;
                case NetActionEvent.ACTION_COMPLETED:
                    Object o = event.getResponse();
                    if (o != null && o instanceof ContentList) {
                        Hashtable table = new Hashtable();
                        processList(table, (ContentList) o);
                        String s = "RemoteDevice: found " + table.size() + " recordings";
                        Log.printInfo(s);
                        MetadataDiagScreen.getInstance().writeLog(s);
                        synchronized (RemoteDevice.this) {
                            recordings = table;
                        }
                    }
                    state = COMPLETED_STATE;
                    synchronized (RemoteDevice.this) {
                        HomeNetManager.getInstance().deviceStateChanged(RemoteDevice.this);
                        request = null;
                    }
                    break;
                case NetActionEvent.ACTION_STATUS_NOT_AVAILABLE:
                case NetActionEvent.ACTION_IN_PROGRESS:
                default:
                    break;
            }
            BrowseJob.this.notifyAll();
        }

        private void processList(Hashtable table, ContentList list) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.BrowseJob.processList("+list+")");
            if (list == null) {
                return;
            }
            while (list.hasMoreElements()) {
                ContentEntry ce = (ContentEntry) list.nextElement();
                Log.printDebug("RemoteDevice: entry = " + ce);
                if (ce instanceof RecordingContentItem) {
                    Object old = table.put(ce.getID(), ce);
                    if (old != null) {
                        Log.printWarning(" already contained !");
                    }
                } else if (ce instanceof ContentContainer) {
                    processList(table, requestBrowseEntries(ce.getID()));
                }
            }
        }
    }

    abstract class ContentJob implements Runnable {
        HashSet idSet = new HashSet();

        public int size() {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.ContentJob.size() returns "+idSet.size());
            return idSet.size();
        }
    }

    class AddContentJob extends ContentJob {
        boolean add;

        HashSet addSet = new HashSet();

        public AddContentJob(String[] ids, boolean add) {
            this.add = add;
            merge(ids, add);
        }

        public void merge(String[] ids, boolean add) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.AddContentJob.merge()");
            for (int i = 0; i < ids.length; i++) {
                idSet.add(ids[i]);
                if (add) {
                    addSet.add(ids[i]);
                }
            }
        }

        public void run() {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.AddContentJob.run()");
            if (disposed) {
                return;
            }
            if (add) {
                Log.printDebug("running AddContentJob");
            } else {
                Log.printDebug("running ChangeContentJob");
            }
            ContentList list = requestSearchEntries(idSet);
            int size = 0;
            if (list != null) {
                size = list.size();
                while (list.hasMoreElements()) {
                    ContentEntry ce = (ContentEntry) list.nextElement();
                    if (ce instanceof RecordingContentItem) {
                        synchronized (RemoteDevice.this) {
                            String id = ce.getID();
                            Object removed = recordings.remove(id);
                            if (removed != null || addSet.contains(id)) {
                                recordings.put(id, ce);
                            }
                        }
                        VideoPlayer.getInstance().contentChanged((RecordingContentItem) ce);
                    }
                }
                HomeNetManager.getInstance().deviceStateChanged(RemoteDevice.this);
            } else if (idSet.size() > 0) {
                try {
                    Thread.sleep(3000L);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                updateWorker.addElement(AddContentJob.this);
            } else {
                return;
            }
            String s = "RemoteDevice.search : result size = " + size;
            Log.printInfo(s);
            MetadataDiagScreen.getInstance().writeLog(s);
        }

        public String toString() {
            return (add) ? "AddContentJob" : "ChangeContentJob";
        }
    }

    class RemoveContentJob extends ContentJob {

        public RemoveContentJob(String[] ids) {
            merge(ids);
        }

        public void merge(String[] ids) {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.RemoveContentJob.merge()");
            for (int i = 0; i < ids.length; i++) {
                idSet.add(ids[i]);
            }
        }

        public void run() {
            Log.printDebug(App.LOG_HEADER+"RemoteDevice.RemoveContentJob.run()");
            if (disposed) {
                return;
            }
            Log.printDebug("running RemoveContentJob");
            int total = idSet.size();
            int removed = 0;
            synchronized (RemoteDevice.this) {
                RecordingContentItem[] array = getRecordings();
                for (int i = 0; i < array.length; i++) {
                    String id = array[i].getID();
                    if (idSet.contains(id)) {
                        Object o = recordings.remove(id);
                        if (o != null) {
                            removed++;
                        }
                    }
                }
            }
            if (removed > 0) {
                HomeNetManager.getInstance().deviceStateChanged(RemoteDevice.this);
            }
            String s = "RemoteDevice.contentRemoved: " + removed + "/" + total;
            Log.printInfo(s);
            MetadataDiagScreen.getInstance().writeLog(s);
        }

        public String toString() {
            return "RemoveContentJob";
        }
    }

}

package com.alticast.illico.pvr.hn;

import java.io.*;
import java.net.*;
import java.util.*;
import javax.tv.util.*;

import org.ocap.dvr.*;
import org.ocap.hn.*;
import org.ocap.hn.content.*;
import org.ocap.hn.content.navigation.*;
import org.ocap.hn.recording.*;
import org.ocap.hn.security.*;
import org.ocap.hn.service.*;
import org.ocap.hn.profiles.upnp.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.storage.*;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.*;

public class HomeNetManager implements Runnable, DeviceEventListener, NetModuleEventListener, NetAuthorizationHandler,
                                       RecordingChangedListener, TVTimerWentOffListener, DataUpdateListener {

    private static HomeNetManager instance = new HomeNetManager();

    public static HomeNetManager getInstance() {
        return instance;
    }

    private static final boolean PUBLISH_AT_ONCE = true;

    NetManager netManager;
    public ContentContainer localRootContainer = null;
    Device localDevice = null;
    NetRecordingRequestManager requestManager = null;

    Vector deviceList = new Vector();
    Vector listeners = new Vector();

    boolean enabled = true;

    boolean pcOn;
    boolean pcProtected;

    private MultipleStateFilter filter = RecordingListManager.accessibleFilter;

    public static final String KEY_TOTAL_SPACE = "illico2_total_space";
    public static final String KEY_FREE_SPACE = "illico2_free_space";
    public static final String KEY_SEGMENTS = "illico2_segments";
    public static final String KEY_PROTECTED = "illico2_protected";

    long lastPublishedFreeSpace = 0;
    long lastPublishedTotalSpace = 0;
    boolean lastPublishedProtection = false;

    private SingleThreadRunner metadataWorker;
    private Worker publishWorker;
    private TVTimerSpec metadataTimer = new TVTimerSpec();

    long metadataPublishInterval = Constants.MS_PER_MINUTE;
    public static String localUdn = "";

    private HomeNetManager() {
        if (App.SUPPORT_HN) {
            netManager = NetManager.getInstance();

            DataCenter dataCenter = DataCenter.getInstance();
            metadataPublishInterval = dataCenter.getLong("DISK_SPACE_PUBLISH_INTERVAL_MIN", 10) * Constants.MS_PER_MINUTE;
            metadataTimer.setRepeat(false);
            metadataTimer.setAbsolute(false);
            metadataTimer.setRegular(false);
            metadataTimer.addTVTimerWentOffListener(this);
        }

        Runnable runnable = new Runnable() {
            public void run() {
                Log.printDebug(App.LOG_HEADER+"HomeNetManager : Call publishMetadata()");
                publishMetadata();
                Log.printDebug(App.LOG_HEADER+"HomeNetManager : Call startMetadataTimer()");
                startMetadataTimer();
            }
        };
        metadataWorker = new SingleThreadRunner(runnable, "HomeNetManager.metaData");
        publishWorker = new Worker("HomeNetManager.worker", false);
    }

    public void init() {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.init()");
        if (App.SUPPORT_HN) {
            new Thread(this, "HomeNetManager.init").start();
        }
    }

    public void setEnabled(boolean enabled) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.setEnabled("+enabled+")");
        if (enabled) {
            NetSecurityManagerImpl.getInstance().enableMocaPrivacy();
        } else {
            NetSecurityManagerImpl.getInstance().disableMocaPrivacy();
        }
    }

    public void run() {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.run()");
        if (netManager == null) {
            Log.printWarning("HomeNetManager: NetManager is null.");
            return;
        }

        NetSecurityManagerImpl nsm = NetSecurityManagerImpl.getInstance();
        nsm.setAuthorizationHandler(this);
        setEnabled(true);
        DataCenter dataCenter = DataCenter.getInstance();
        Object pw = dataCenter.get("FIXED_MOCA_PASSWORD");
        if (pw != null) {
            nsm.setNetworkPassword(pw.toString());
        }
        netManager.addNetModuleEventListener(this);

        dataCenter.addDataUpdateListener(RightFilter.PARENTAL_CONTROL, this);
        dataCenter.addDataUpdateListener(RightFilter.PROTECT_PVR_RECORDINGS, this);
        pcOn = ParentalControl.enabled;
        pcProtected = ParentalControl.protect;

        NetList netList = netManager.getDeviceList(null);

        Log.printDebug("HomeNetManager: deviceList size = " + netList.size());
        int localCount = 0;
        Enumeration en = netList.getElements();

        ContentContainer rootCon = null;
        while (en.hasMoreElements()) {
            try {
                Device dev = (Device) en.nextElement();

                String deviceType = dev.getType();
                boolean local = dev.isLocal();
                String deviceInfo = getDeviceInfo(dev);
                if (Log.DEBUG_ON) {
                    Log.printDebug("Device = " + deviceInfo);
                    Log.printDebug("Device.local = " + local);
                    Log.printDebug("Device.InetAddress " + dev.getInetAddress());
                    Log.printDebug("Device.type " + deviceType);
                    Log.printDebug("Device.Capabilities " + dev.getCapabilities());
                }
                if (!isValidDevice(dev)) {
                    Log.printDebug("Device: invalid device");
                    continue;
                }
                ContentServerNetModule csnModule1 = (ContentServerNetModule) dev.getNetModule(NetModule.CONTENT_SERVER);
                if (local) {
                    setLocalDevice(dev);
                    if (rootCon == null) {
                        rootCon = findRootContainer(csnModule1);
                    }
                } else {
                    MetadataDiagScreen.getInstance().writeLog("Device found = " + deviceInfo);
                    addDevice(dev, csnModule1);
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

        setLocalRootContainer(rootCon);

        deviceStateChanged(null);
        Log.printDebug(App.LOG_HEADER+"HomeNetManager: init completed");
    }

    private void setLocalRootContainer(ContentContainer newCon) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.setLocalRootContainer("+newCon+")");
        synchronized (KEY_SEGMENTS) {
            if (this.localRootContainer != null || newCon == null) {
                return;
            }
            this.localRootContainer = newCon;
            if (Environment.SUPPORT_DVR && !Environment.EMULATOR) {
                try {
                    RecordingManager.getInstance().addRecordingChangedListener(this);
                } catch (Exception ex) {
                }
                metadataWorker.start();
                publishLocalRecordings();
            }
        }
    }

    private void setLocalDevice(Device dev) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.setLocalDevice("+dev+")");
        synchronized (KEY_SEGMENTS) {
            if (this.localDevice != null || dev == null || !dev.isLocal()) {
                return;
            }
            this.localDevice = dev;
            localUdn = dev.getProperty(Device.PROP_UDN);

            String name = getDeviceName();
            if (name == null || name.length() <= 0) {
                setDeviceName(DataCenter.getInstance().getString("multiroom.default_terminal_name"));
            }
            RecordingNetModule recModule = (RecordingNetModule) dev.getNetModule(NetModule.CONTENT_RECORDER);
            if (recModule != null && recModule.isLocal() && recModule instanceof NetRecordingRequestManager) {
                this.requestManager = (NetRecordingRequestManager) recModule;
                Log.printInfo("HomeNetManager: found NetRecordingRequestManager = " + requestManager);
                requestManager.setNetRecordingRequestHandler(new NetRecordingRequestHandlerImpl());
            }
        }
    }

    private synchronized RemoteDevice addDevice(Device dev, ContentServerNetModule csnm) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.addDevice("+dev+")");
        RemoteDevice rd = findDevice(dev);
        if (rd != null) {
            Log.printWarning("HomeNetManager.addDevice: already added = " + rd.device);
            MetadataDiagScreen.getInstance().writeLog("HomeNetManager.addDevice: already added = " + rd.device);
            return null;
        }
        rd = new RemoteDevice(dev, csnm);
        deviceList.addElement(rd);
        return rd;
    }

    private boolean isValidDevice(Device dev) {
        return Device.TYPE_MEDIA_SERVER.equals(dev.getType())
               && dev.getNetModule(NetModule.CONTENT_RECORDER) != null;
    }

    public String getLocalDeviceName() {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.getLocalDeviceName() returns "+DataCenter.getInstance().getString("pvr.Local"));
        return DataCenter.getInstance().getString("pvr.Local");
    }

    public Vector getDeviceList() {
        //Log.printDebug(App.LOG_HEADER+"HomeNetManager.getDeviceList() returns "+deviceList);
        return deviceList;
    }

    public int getDeviceCount() {
        return deviceList.size();
    }

    public void setDeviceName(String name) {
        if (localDevice == null) {
            return;
        }
        if (name == null || name.length() <= 0) {
            name = " ";
        }
        Log.printInfo(App.LOG_HEADER+"HomeNetManager.setDeviceName = " + name);
        localDevice.setFriendlyName(name);
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.setDeviceName: end of method");
    }

    public String getDeviceName() {
        if (localDevice != null) {
            return localDevice.getProperty(Device.PROP_FRIENDLY_NAME);
        } else {
            return null;
        }
    }

    public RemoteDevice findDevice(Device dev) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.findDevice("+dev+")");
        if (dev == null) {
            return null;
        }
        Enumeration en = deviceList.elements();
        while (en.hasMoreElements()) {
            RemoteDevice r = (RemoteDevice) en.nextElement();
            if (r.getDevice().equals(dev)) {
                return r;
            } else {
                String udn = r.getUdn();
                if (udn != null && udn.equals(dev.getProperty(Device.PROP_UDN))) {
                    return r;
                }
            }
        }
        return null;
    }

    public void deleteRecording(RemoteRecording r) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.deleteRecording("+r+")");
        RemoteDevice rd = findDevice(r.getDevice());
        if (rd != null) {
            rd.deleteRecording(r.getItem());
        }
    }

    public Vector getAllRecordingItems() {
        return getAllRecordingItems(new Vector(40));
    }

    public Vector getAllRecordingItems(Vector vector) {
        Vector deviceList = this.deviceList;
        if (deviceList != null) {
            Enumeration en = deviceList.elements();
            while (en.hasMoreElements()) {
                RemoteDevice rd = (RemoteDevice) en.nextElement();
                RecordingContentItem[] array = rd.getRecordings();
                for (int i = 0; i < array.length; i++) {
                    vector.addElement(array[i]);
                }
            }
        }
        return vector;
    }

    public void addDeviceListListener(DeviceListListener l) {
        listeners.addElement(l);
    }

    public void deviceStateChanged(RemoteDevice rd) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.deviceStateChanged("+rd+")");
        Enumeration en = listeners.elements();
        while (en.hasMoreElements()) {
            DeviceListListener l = (DeviceListListener) en.nextElement();
            l.deviceListChanged(deviceList);
        }
    }

    private void publishMetadata() {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.publishMetadata");
        ContentContainer con = localRootContainer;
        if (con == null) {
            return;
        }
        MetadataNode node = con.getRootMetadataNode();
        if (node != null) {
            long total = StorageInfoManager.getInstance().getAllocatedSpace();
            long free = StorageInfoManager.getInstance().getFreeSpace();
            boolean protection = pcOn && pcProtected;
            boolean published = false;
            if (lastPublishedTotalSpace != total) {
                node.addMetadata(KEY_TOTAL_SPACE, new Long(total));
                lastPublishedTotalSpace = total;
                published = true;
            }
            if (lastPublishedFreeSpace != free) {
                node.addMetadata(KEY_FREE_SPACE, new Long(free));
                lastPublishedFreeSpace = free;
                published = true;
            }
            if (lastPublishedProtection != protection) {
                node.addMetadata(KEY_PROTECTED, new Boolean(protection));
                lastPublishedProtection = protection;
                published = true;
            }
            if (published) {
                Log.printInfo("HomeNetManager.publishMetadata = " + free + " / " + total + " : " + protection);
            } else {
                Log.printInfo("HomeNetManager.publishMetadata : not changed = " + free + " / " + total + " : " + protection);
            }
        }
    }

    public static ContentContainer findRootContainer(ContentServerNetModule csnm) {
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        NetActionRequest naq = csnm.requestRootContainer(handler);
        ContentContainer con = (ContentContainer) handler.waitForResponse();
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.findRootContainer = " + con);
        return con;
    }

    private ContentContainer findContentContainer(ContentServerNetModule csnm, String id, boolean flag, String seekName) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.findContentContainer: " + id + ", " + flag + ", " + seekName);
        NetActionHandlerImpl handler = new NetActionHandlerImpl();
        NetActionRequest naq = csnm.requestBrowseEntries(id, "*", flag, 0, 0, "", handler);
        Object response = handler.waitForResponse();
        if (response == null || !(response instanceof ContentList)) {
            Log.printWarning("HomeNetManager.findContentContainer: invalid response = " + response);
            return null;
        }
        ContentList list = (ContentList) response;
        while (list.hasMoreElements()) {
            ContentEntry ce = (ContentEntry) list.nextElement();
//            String ceID = ce.getID();
            String ceName = castToString(ce.getRootMetadataNode().getMetadata(UPnPConstants.TITLE));
            if (ce instanceof ContentContainer) {
                if (seekName.equals(ceName)) {
                    return (ContentContainer) ce;
                }
            }
        }
        return null;
    }

    private void publishLocalRecordings() {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.publishLocalRecordings");
        if (localRootContainer == null) {
            Log.printWarning("HomeNetManager: current root container is not exist.");
            return;
        }
        RecordingList recList = RecordingManager.getInstance().getEntries(filter);
        if (recList == null) {
            return;
        }
        int size = recList.size();
        int count = 0;
        if (PUBLISH_AT_ONCE) {
            Vector vector = new Vector(Math.max(size, 10));
            for (int i = 0; i < size; i++) {
                RecordingContentItem rci = getRecordingItem(recList.getRecordingRequest(i));
                if (rci != null) {
                    vector.addElement(rci);
                    count++;
                }
            }
            RecordingContentItem[] items = new RecordingContentItem[vector.size()];
            vector.copyInto(items);
            localRootContainer.addContentEntries(items);
        } else {
            for (int i = 0; i < size; i++) {
                RecordingContentItem rci = getRecordingItem(recList.getRecordingRequest(i));
                if (rci != null) {
                    localRootContainer.addContentEntry(rci);
                    count++;
                }
            }
        }
        String s = "publishLocalRecordings: " + count + "/" + size;
        Log.printInfo(s);
        MetadataDiagScreen.getInstance().writeLog(s);
    }

    /**
     * Server publish/unpublish specific recording via recordingID
     * @param recID
     * @param True to publish; False to unpublish
     * @return True if publish successfully; False otherwise.
     */
    private boolean publishRecording(RecordingRequest req, boolean publish) {
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"HomeNetManager.publish : " + publish);

            long dur = 0;
            try {
                RecordedService service = ((LeafRecordingRequest) req).getService();
                if (service != null) {
                    dur = (service).getRecordedDuration();
                }
            } catch (Exception ex) {
                dur = -1;
            }
            String s = "publish '" + AppData.getString(req, AppData.TITLE) + "' = " + (publish ? "add" : "remove") + " : dur = " + dur;
            MetadataDiagScreen.getInstance().writeLog(s);
        }
        if (localRootContainer == null) {
            Log.printWarning("HomeNetManager.publish : current root container is not exist.");
            return false;
        }
        if (req == null || !(req instanceof RecordingContentItem)) {
            Log.printWarning("HomeNetManager.publish : invalid RecordingRequest = " + req);
            return false;
        }

        boolean result = false;
        try {
            if (publish) {
                RecordingContentItem rci = (RecordingContentItem) req;
                if (localRootContainer.contains(rci)) {
                    Log.printInfo("HomeNetManager.publish : already added");
                    MetadataNode node = rci.getRootMetadataNode();
                    updateSegmentTimes(req, node);
                    updateKeepCount(req, node);
                } else {
                    Log.printDebug("HomeNetManager.publish : new recording");
                    result = localRootContainer.addContentEntry(getRecordingItem(req));
                }
            } else {
                result = localRootContainer.removeContentEntry((RecordingContentItem) req);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (Log.INFO_ON) {
            Log.printInfo("HomeNetManager.publish : result = " + result);
        }
        return result;
    }

    private RecordingContentItem getRecordingItem(RecordingRequest req) {
        if (req == null || !(req instanceof RecordingContentItem)) {
            Log.printWarning("HomeNetManager: invalid RecordingRequest = " + req);
            return null;
        }
        RecordingContentItem rci = (RecordingContentItem) req;
        MetadataNode node = rci.getRootMetadataNode();

        String title = AppData.getString(req, AppData.TITLE);
        String channel = AppData.getString(req, AppData.CHANNEL_NAME);

        Hashtable table = new Hashtable();

        String[] keys = req.getKeys();
        if (keys == null || keys.length <= 0) {
            Log.printWarning("AppData: no appdata keys");
        } else {
            node.addNameSpace("ocapApp", "urn chemas-opencalbe-com cap-application_Already_PreLoaded");
            for (int i = 0; i < keys.length; i++) {
                if (!keys[i].equals(AppData.NI_INDEX)) {
                    node.addMetadata(keys[i], AppData.get(req, keys[i]));
                }
            }
        }
        updateSegmentTimes(req, node);
        updateKeepCount(req, node);
        node.addMetadata(AppData.SCHEDULED_START_TIME, new Long(AppData.getScheduledStartTime(req)));
        node.addMetadata(AppData.SCHEDULED_DURATION, new Long(AppData.getScheduledDuration(req)));
        node.addMetadata(UPnPConstants.CREATOR, channel);
        node.addMetadata(UPnPConstants.TITLE, title);
        node.addMetadata(AppData.LOCAL_ID, new Integer(req.getId()));
        return rci;
    }

    public RecordingContentItem find(String id) {
        Enumeration en = deviceList.elements();
        while (en.hasMoreElements()) {
            RemoteDevice r = (RemoteDevice) en.nextElement();
            RecordingContentItem item = r.find(id);
            if (item != null) {
                return item;
            }
        }
        return null;
    }

    public RecordingContentItem find(int localId, String udn) throws IllegalArgumentException {
        Enumeration en = deviceList.elements();
        while (en.hasMoreElements()) {
            RemoteDevice rd = (RemoteDevice) en.nextElement();
            String deviceUdn = rd.getUdn();
            Log.printDebug("HomeNetManager.find : udn = " + deviceUdn);
            if (udn.equals(deviceUdn)) {
                RecordingContentItem item = rd.find(localId);
                if (item != null) {
                    return item;
                } else {
                    return null;
                }
            }
        }
        throw new IllegalArgumentException("invalid UDN");
    }

    private void updateSegmentTimes(RecordingRequest req, MetadataNode node) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.updateSegmentTimes()");
        try {
            long[] times = LocalRecording.getSegmentTimes(((LeafRecordingRequest) req).getService());
            if (times != null) {
                Long[] array = new Long[times.length];
                for (int i = 0; i < times.length; i++) {
                    array[i] = new Long(times[i]);
                }
                node.addMetadata(AppData.SEGMENT_TIMES, array);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void updateKeepCount(RecordingRequest req, MetadataNode node) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.updateKeepCount()");
        Object groupKey = AppData.get(req, AppData.GROUP_KEY);
        if (groupKey != null) {
            int kc = RecordingScheduler.getInstance().getKeepCount(groupKey.toString());
            node.addMetadata(AppData.KEEP_COUNT, new Integer(kc));
        }
    }

    public static boolean checkPlaySlow(AbstractRecording ar) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.checkPlaySlow()");
//        if (ar != null && ar instanceof RemoteRecording && Environment.HW_VENDOR == Environment.HW_VENDOR_CISCO) {
//            Device device = ((RemoteRecording) ar).getDevice();
//            if (device != null) {
//                String prop = device.getProperty(Device.PROP_MANUFACTURER_URL);
//                if (prop != null && prop.toUpperCase().indexOf("SAMSUNG") >= 0) {
//                    return false;
//                }
//            }
//        }
        if (ar != null && ar instanceof RemoteRecording) {
            return false;
        }
        return true;
    }


    public void updateKeepCount(final String groupKey, final int keepCount) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.updateKeepCount("+keepCount+")");
        if (Log.INFO_ON) {
            Log.printInfo("HomeNetManager.updateKeepCount: " + groupKey + ", " + keepCount);
        }
        if (groupKey == null) {
            return;
        }
        Thread t = new Thread("HomeNetManager.updateKeepCount") {
            public void run() {
                RecordingList list = RecordingManager.getInstance().getEntries(filter);
                list = RecordingListManager.filter(list, AppData.GROUP_KEY, groupKey);
                if (list != null) {
                    int size = list.size();
                    for (int i = 0; i < size; i++) {
                        RecordingRequest r = list.getRecordingRequest(i);
                        publishRecording(r, true);
                    }
                }
            }
        };
        t.start();
    }


    //////////////////////////////////////////////////////////////////////////
    // NetAuthorizationHandler
    //////////////////////////////////////////////////////////////////////////

    public boolean notifyAction(String actionName, InetAddress inetAddress, String macAddress, int activityId) {
        Log.printInfo(App.LOG_HEADER+"HomeNetManager.NetAuthorizationHandler.notifyAction = " + actionName);
        return false;
    }

    public void notifyActivityEnd(int activityId) {
        Log.printInfo(App.LOG_HEADER+"HomeNetManager.NetAuthorizationHandler.notifyActivityEnd = " + activityId);

    }

    public boolean notifyActivityStart(InetAddress inetAddress, String macAddress, URL url, int activityId) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.NetAuthorizationHandler.notifyActivityStart = " + activityId);
        return true;
    }

    ////////////////////////////////////////////////////////////////////
    // DeviceEventListener
    ////////////////////////////////////////////////////////////////////

    public void notify(DeviceEvent event) {
        Log.printInfo(App.LOG_HEADER+"HomeNetManager.DeviceEventListener.notify = " + event);
        int type = event.getType();
        Device dev = (Device) event.getSource();
        boolean local = dev.isLocal();
        String s = getDeviceEventString(type) + " = " + getDeviceInfo(dev);
        MetadataDiagScreen.getInstance().writeLog(s);
        if (Log.DEBUG_ON) {
            Log.printDebug("DeviceEventListener: " + s);
        }
        if (dev.isLocal()) {
            Log.printWarning("DeviceEventListener: device is local");
            return;
        }
        RemoteDevice rd;
        switch (type) {
            case DeviceEvent.DEVICE_ADDED:
                if (isValidDevice(dev)) {
                    ContentServerNetModule csnm = (ContentServerNetModule) dev.getNetModule(NetModule.CONTENT_SERVER);
                    if (csnm != null) {
                        rd = addDevice(dev, csnm);
                        if (rd != null) {
                            deviceStateChanged(rd);
                        }
                    } else {
                        MetadataDiagScreen.getInstance().writeLog("DeviceEventListener : not a valid device : " + dev.getType());
                        Log.printWarning("DeviceEventListener : not a valid device : " + dev.getType());
                    }
                }
                return;
            case DeviceEvent.DEVICE_REMOVED:
                VideoPlayer.getInstance().notifyDeviceRemoved(dev);
                synchronized (this) {
                    rd = findDevice(dev);
                    Log.printDebug("DeviceEventListener : RemoteDevice = " + rd);
                    if (rd != null) {
                        rd.dispose();
                        deviceList.remove(rd);
                        deviceStateChanged(rd);
                    }
                }
                return;
            case DeviceEvent.DEVICE_UPDATED:
            case DeviceEvent.STATE_CHANGE:
                rd = findDevice(dev);
                Log.printDebug("DeviceEventListener : RemoteDevice = " + rd);
                if (rd != null) {
                    rd.reset();
                }
                return;
        }
    }

    ////////////////////////////////////////////////////////////////////
    // NetModuleEventListener
    ////////////////////////////////////////////////////////////////////

    public void notify(NetModuleEvent event) {
        Log.printInfo(App.LOG_HEADER+"HomeNetManager.NetModuleEventListener.notify = " + event);
        NetModule nm = (NetModule) event.getSource();
        if (nm == null) {
            return;
        }
        int type = event.getType();
        String s = getNetModuleEventString(type) + " = " + nm + " : " + nm.getDevice();
        MetadataDiagScreen.getInstance().writeLog(s);
        if (Log.DEBUG_ON) {
            Log.printDebug("NetModuleEventListener: " + s);
        }
        Device dev = nm.getDevice();

        RemoteDevice rd;
        switch (type) {
            case NetModuleEvent.MODULE_ADDED:
                if (Device.TYPE_MEDIA_SERVER.equals(dev.getType())) {
                    ContentServerNetModule csnm = null;
                    RecordingNetModule rnm = null;
                    if (nm instanceof ContentServerNetModule) {
                        csnm = (ContentServerNetModule) nm;
                        rnm = (RecordingNetModule) dev.getNetModule(NetModule.CONTENT_RECORDER);
                    } else if (nm instanceof RecordingNetModule) {
                        rnm = (RecordingNetModule) nm;
                        csnm = (ContentServerNetModule) dev.getNetModule(NetModule.CONTENT_SERVER);
                    }
                    if (csnm != null && rnm != null) {
                        if (nm.isLocal()) {
                            setLocalDevice(dev);
                            if (localRootContainer == null) {
                                ContentContainer con = findRootContainer(csnm);
                                setLocalRootContainer(con);
                            }
                        } else {
                            rd = addDevice(dev, csnm);
                            if (rd != null) {
                                deviceStateChanged(rd);
                            }
                        }
                    } else {
                        MetadataDiagScreen.getInstance().writeLog("invalid module");
                        Log.printInfo("NetModuleEventListener : invalid module");
                    }
                } else {
                    MetadataDiagScreen.getInstance().writeLog("not a media server");
                }
                break;
            case NetModuleEvent.MODULE_REMOVED:
                VideoPlayer.getInstance().notifyDeviceRemoved(dev);
                synchronized (this) {
                    rd = findDevice(dev);
                    Log.printDebug("NetModuleEventListener : RemoteDevice = " + rd);
                    if (rd != null) {
                        rd.dispose();
                        deviceList.remove(rd);
                        deviceStateChanged(rd);
                    }
                }
                break;
            case NetModuleEvent.MODULE_UPDATED:
            case NetModuleEvent.STATE_CHANGE:
                rd = findDevice(dev);
                Log.printDebug("NetModuleEventListener : RemoteDevice = " + rd);
                if (rd != null) {
                    rd.reset();
                }
                break;
        }
    }

    ////////////////////////////////////////////////////////////////////
    // RecordingChangedListener
    ////////////////////////////////////////////////////////////////////

    public void recordingChanged(final RecordingChangedEvent evt) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.recordingChanged("+evt+")");
        int change = evt.getChange();
        final RecordingRequest req = evt.getRecordingRequest();

        final boolean toAdd;
        switch (change) {
            case RecordingChangedEvent.ENTRY_ADDED:
                if (filter.accept(req)) {
                    toAdd = true;
                } else {
                    return;
                }
                break;
            case RecordingChangedEvent.ENTRY_DELETED:
                toAdd = false;
                break;
            case RecordingChangedEvent.ENTRY_STATE_CHANGED:
                boolean beforeAccept = filter.accept(evt.getOldState());
                toAdd = filter.accept(req);
                if (!beforeAccept && !toAdd) {
                    Log.printDebug("HomeNetManager: skip to publish");
                    return;
                }
                break;
            default:
                return;
        }
        // event dispatch가 느리니까 Thread로 분리
        publishWorker.push(new Runnable() {
            public void run() {
                publishRecording(req, toAdd);
            }
        });
        startMetadataTimer(10 * Constants.MS_PER_SECOND);
    }

    ////////////////////////////////////////////////////////////////////
    // TVTimerWentOffListener
    ////////////////////////////////////////////////////////////////////

    private void startMetadataTimer() {
        startMetadataTimer(metadataPublishInterval);
    }

    private void startMetadataTimer(long delayTime) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.startMetadataTimer("+delayTime+")");
        TVTimer.getTimer().deschedule(metadataTimer);
        try {
            metadataTimer.setTime(delayTime);
            metadataTimer = TVTimer.getTimer().scheduleTimerSpec(metadataTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.timerWentOff() : Call metadataWorker.start()");
        metadataWorker.start();
    }

    ////////////////////////////////////////////////////////////////////
    // DataUpdateListener
    ////////////////////////////////////////////////////////////////////

    public void dataUpdated(String key, Object old, Object value) {
        Log.printDebug(App.LOG_HEADER+"HomeNetManager.dataUpdated("+key+")");
        if (RightFilter.PARENTAL_CONTROL.equals(key)) {
            pcOn = Definitions.OPTION_VALUE_ON.equals(value);
        } else if (RightFilter.PROTECT_PVR_RECORDINGS.equals(key)) {
            pcProtected = Definitions.OPTION_VALUE_YES.equals(value);
        }
        metadataWorker.start();
    }

    public void dataRemoved(String key)  {
    }

    ////////////////////////////////////////////////////////////////////
    // Utilities
    ////////////////////////////////////////////////////////////////////

    public static String getDeviceInfo(Device dev) {
        if (dev != null) {
            return dev + " ; " + (dev.isLocal() ? "local " : "remote ") + dev.getName() + " ; " + dev.getProperty(Device.PROP_UDN);
        } else {
            return "Device is null";
        }
    }

    private static String castToString(Object o) {
        if (o == null) {
            return "";
        } else if (o instanceof String) {
            return (String) o;
        } else if (o instanceof String[]) {
            String[] array = (String[]) o;
            if (array.length == 0) {
                return "";
            }
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                if (array[i] != null) {
                    sb.append(array[i]);
                    sb.append(' ');
                }
            }
            return sb.toString();
        } else {
            return o.toString();
        }
    }

    public static String getDeviceEventString(int type) {
        switch (type) {
            case DeviceEvent.DEVICE_ADDED:
                return "DEVICE_ADDED";
            case DeviceEvent.DEVICE_REMOVED:
                return "DEVICE_REMOVED";
            case DeviceEvent.DEVICE_UPDATED:
                return "DEVICE_UPDATED";
            case DeviceEvent.STATE_CHANGE:
                return "STATE_CHANGE";
            default:
                return "UNKNOWN";
        }
    }

    public static String getNetModuleEventString(int type) {
        switch (type) {
            case NetModuleEvent.MODULE_ADDED:
                return "MODULE_ADDED";
            case NetModuleEvent.MODULE_BUSY:
                return "MODULE_BUSY";
            case NetModuleEvent.MODULE_REMOVED:
                return "MODULE_REMOVED";
            case NetModuleEvent.MODULE_UPDATED:
                return "MODULE_UPDATED";
            case NetModuleEvent.STATE_CHANGE:
                return "STATE_CHANGE";
            default:
                return "UNKNOWN";
        }
    }

    public static String getContentServerEventString(int type) {
        switch (type) {
            case ContentServerEvent.CONTENT_ADDED:
                return "CONTENT_ADDED";
            case ContentServerEvent.CONTENT_REMOVED:
                return "CONTENT_REMOVED";
            case ContentServerEvent.CONTENT_CHANGED:
                return "CONTENT_CHANGED";
            default:
                return "UNKNOWN";
        }
    }
}


package com.alticast.illico.pvr;

import java.io.*;
import java.util.*;
import javax.tv.service.*;
import javax.tv.locator.*;
import org.ocap.net.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.ocap.hn.recording.*;

import com.alticast.illico.pvr.filter.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.data.PvrContentImpl;
import com.alticast.illico.pvr.data.RecordedContentImpl;
import com.alticast.illico.pvr.util.SingleThreadRunner;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.pvr.*;

/**
 * 녹화된 결과물을 관리.
 *
 * @author  June Park
 */
public class RecordingListManager implements RecordingChangedListener, DeviceListListener, Runnable {

    private static final String RECORDING_CHANNELS = "RECORDING_CHANNELS";

    public static final short INVALID     = -1;
    public static final short IN_PROGRESS = PvrContent.STATE_RECORDING;
    public static final short PENDING     = PvrContent.STATE_SCHEDULED;
    public static final short COMPLETED   = PvrContent.STATE_RECORDED;

    /** 진행중 states */
    public static final int[] IN_PROGRESS_STATES = {
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE
    };

    public static final int[] IN_PROGRESS_WITHOUT_ERROR_STATES = {
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE
    };

    public static final int[] IN_PROGRESS_WITH_FAILED_REASON = {
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE
    };

    /** 완료 states */
    public static final int[] COMPLETED_STATES = {
        OcapRecordingRequest.INCOMPLETE_STATE,
        OcapRecordingRequest.COMPLETED_STATE
    };

    /** 재생 가능한 states */
    public static final int[] PLAYABLE_STATES = {
        // IN_PROGRESS_STATES
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE,
        // COMPLETED_STATES
        OcapRecordingRequest.INCOMPLETE_STATE,
        OcapRecordingRequest.COMPLETED_STATE
    };

    /** PENDING states */
    public static final int[] PENDING_STATES = {
        OcapRecordingRequest.PENDING_NO_CONFLICT_STATE,
        OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE,
    };

    public static final int[] CANCELABLE_STATES = {
        // IN_PROGRESS_STATES
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE,
        // PENDING_STATES
        OcapRecordingRequest.PENDING_NO_CONFLICT_STATE,
        OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE,
    };

    public static final int[] VALID_STATES = {
        // IN_PROGRESS_STATES
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE,
        // PENDING_STATES
        OcapRecordingRequest.PENDING_NO_CONFLICT_STATE,
        OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE,
        // COMPLETED_STATES
        OcapRecordingRequest.INCOMPLETE_STATE,
        OcapRecordingRequest.COMPLETED_STATE,
    };

    public static final int[] CANCELLED_FAILED_STATES = {
        OcapRecordingRequest.CANCELLED_STATE,
        OcapRecordingRequest.FAILED_STATE,
    };

    public static final int[] ACCESSIBLE_STATES = {
        // IN_PROGRESS_STATES
        OcapRecordingRequest.IN_PROGRESS_STATE,
        OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
        OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE,
        OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE,
        // COMPLETED_STATES
        OcapRecordingRequest.INCOMPLETE_STATE,
        OcapRecordingRequest.COMPLETED_STATE,
        // INVALID
        OcapRecordingRequest.CANCELLED_STATE,
        OcapRecordingRequest.FAILED_STATE,
    };

    //////////////////////////////////////////////////////////
    // singleton implementation
    //////////////////////////////////////////////////////////

    private static RecordingListManager instance = new RecordingListManager();

    public static RecordingListManager getInstance() {
        return instance;
    }

    public static MultipleStateFilter validFilter = new MultipleStateFilter(VALID_STATES);
    public static ValidMultipleStateFilter playableFilter = new ValidMultipleStateFilter(PLAYABLE_STATES);
    public static MultipleStateFilter recordedFilter = new MultipleStateFilter(COMPLETED_STATES);
    public static MultipleStateFilter inProgressFilter = new MultipleStateFilter(IN_PROGRESS_STATES);
    public static ValidMultipleStateFilter inProgressValidFilter = new ValidMultipleStateFilter(IN_PROGRESS_STATES);
    public static MultipleStateFilter inProgressWithoutErrorFilter = new MultipleStateFilter(IN_PROGRESS_WITHOUT_ERROR_STATES);
    public static MultipleStateFilter inProgressWithFailedFilter = new MultipleStateFilter(IN_PROGRESS_WITH_FAILED_REASON);
    public static MultipleStateFilter pendingFilter = new MultipleStateFilter(PENDING_STATES);
    public static MultipleStateFilter cancelableFilter = new MultipleStateFilter(CANCELABLE_STATES);
    public static MultipleStateFilter cancelledFailedFilter = new MultipleStateFilter(CANCELLED_FAILED_STATES);
    public static ValidMultipleStateFilter accessibleFilter = new ValidMultipleStateFilter(ACCESSIBLE_STATES);

    public static RecordingStateFilter failedFilter = new RecordingStateFilter(OcapRecordingRequest.FAILED_STATE);
    public static RecordingStateFilter pendingNoConflictFilter = new RecordingStateFilter(OcapRecordingRequest.PENDING_NO_CONFLICT_STATE);
    public static RecordingStateFilter pendingWithConflictFilter = new RecordingStateFilter(OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE);
    public static RecordingListFilter activeFilter = new ReverseAppDataFilter(AppData.UNSET, Boolean.TRUE, true);

//    public static RecordingListFilter notToBeDeletedFilter = new NotToBeDeletedFilter();

//    public static RecordingListComparator orderByTime = new RecordedTimeComparator();
//    public static RecordingListComparator deleteOrder = new DeleteOrder();

    private String[] allContents = new String[0];
    private String[] recordedContents = new String[0];
    private Hashtable contentsMap;

    private Vector listeners = new Vector();

    private SingleThreadRunner runner;

    public static HashSet toBeDeleted = new HashSet();

    public static long maxEndTime = 0;

    public RecordingListManager() {
//        deleteInvalidLists();
        runner = new SingleThreadRunner(this, "RecordingListManager.recordingChanged");

        Vector recordingChannels = new Vector();
        SharedMemory.getInstance().put(RECORDING_CHANNELS, recordingChannels);
    }

    public static void markToBeDeleted(AbstractRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.markToBeDeleted("+r+")");
        if (r instanceof LocalRecording) {
            toBeDeleted.add(r.getIdObject());
        }
    }

    public static void markToBeDeleted(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.markToBeDeleted("+r+")");
        toBeDeleted.add(new Integer(r.getId()));
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"START : RecordingListManager.init()");
        RecordingManager.getInstance().addRecordingChangedListener(this);
        if (App.SUPPORT_HN) {
            HomeNetManager.getInstance().addDeviceListListener(this);
        }
        runner.start();
        if (Log.EXTRA_ON && false) {
            printList();
        }

        boolean recover = false;
        int date = DataCenter.getInstance().getInt("EXPIRING_DATE_FOR_RECOVERING_SCHEDULE");
        if (date > 0) {
            Calendar c = Calendar.getInstance();
            c.setTime(new Date());
            int y = c.get(Calendar.YEAR);
            int m = c.get(Calendar.MONTH) + 1;
            int d = c.get(Calendar.DAY_OF_MONTH);
            int today = d + m * 100 + y * 10000;
            recover = today <= date;
            if (!recover) {
                Log.printWarning("RecordingListManager: today = " + today + ", exp_date = " + date + ", recover = " + recover);
            } else {
                Log.printInfo("RecordingListManager: today = " + today + ", exp_date = " + date + ", recover = " + recover);
            }
        }


        RecordingList list = RecordingManager.getInstance().getEntries();
        if (list != null) {
            int size = list.size();
            Log.printInfo("RecordingListManager: list size = " + size);
            for (int i = 0; i < size; i++) {
                RecordingRequest req = list.getRecordingRequest(i);
                int state = 0;
                try {
                    state = req.getState();
                    if (Log.DEBUG_ON) {
                        int id = req.getId();
                        Log.printDebug("RecordingListManager: request[" + i + "/" + size + "] id = " + id + ", state = " + req.getState());
                    }
                    if (state == LeafRecordingRequest.DELETED_STATE) {
                        req.delete();
                        continue;
                    }
                } catch (Exception ex) {
                }

                int reason = AppData.updateFailedReason(req);
                if (state == LeafRecordingRequest.FAILED_STATE && reason == RecordingFailedException.RESOURCES_REMOVED) {
                    if (AppData.getInteger(req, AppData.NI_INDEX) != -1) {
                        RecordManager.recordingFailed(req, reason, false);
                        AppData.set(req, AppData.NI_INDEX, new Integer(-1));
                    }
                }
            }
        }

        list = RecordingManager.getInstance().getEntries(inProgressWithFailedFilter);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                RecordingRequest req = list.getRecordingRequest(i);

                Object obj = AppData.get(req, AppData.FAILED_REASON);
                if (obj == null) {
                    try {
                        LeafRecordingRequest r = (LeafRecordingRequest) req;
                        RecordingFailedException ex = (RecordingFailedException) r.getFailedException();
                        int reason = ex.getReason();
                        AppData.set(req, AppData.FAILED_REASON, new Integer(reason));
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            }
        }
        list = getCancelledFailedList();
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                RecordingRequest req = list.getRecordingRequest(i);
                long expTime = RecordManager.getExpirationTime(req);
                if (expTime < System.currentTimeMillis()) {
                    // FAILED state에서 지워지지 않는 문제가 있어서처리
                    try {
                        req.delete();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                } else {
                    long deleteTime = System.currentTimeMillis() + RecordManager.FAILED_RECORDING_KEEP_DURATION;
                    if (expTime > deleteTime) {
                        RecordManager.setExpirationTime(req, deleteTime);
                    }
                }
            }
        }

        if (recover) {
            list = getCancelableList();
            if (list != null) {
                for (int i = 0; i < list.size(); i++) {
                    recover(list.getRecordingRequest(i));
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"END : RecordingListManager.init()");
    }

    private void recover(RecordingRequest r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.recover("+r+")");
        try {
            RecordingSpec spec = r.getRecordingSpec();
            if (spec instanceof LocatorRecordingSpec) {
                LocatorRecordingSpec lrs = (LocatorRecordingSpec) spec;
                Locator[] locator = lrs.getSource();
                if (locator != null && locator.length == 1) {
                    Locator loc = locator[0];
                    if (loc instanceof OcapLocator) {
                        OcapLocator ol = (OcapLocator) loc;
                        int sid = ol.getSourceID();
                        Log.printInfo("RecordingListManager.recover : OcapLocator = " + ol + ", sourceID = " + sid);
                        if (sid == 0) {
                            Log.printInfo("RecordingListManager.recover : SDV channel");
                        } else if (sid > 0) {
                            Log.printInfo("RecordingListManager.recover : no need to be recovered");
                        } else {
                            int newSid = AppData.getInteger(r, AppData.SOURCE_ID);
                            if (newSid > 0) {
                                Locator[] nLoc = new Locator[] { new OcapLocator(newSid) };
                                LocatorRecordingSpec newSpec = new LocatorRecordingSpec(nLoc,
                                    lrs.getStartTime(), lrs.getDuration(), lrs.getProperties());
                                r.reschedule(newSpec);
                                Log.printInfo("RecordingListManager.recover : successfully recovered");
                            } else {
                                Log.printError("RecordingListManager.recover : can't recover = " + loc);
                            }
                        }
                    } else {
                        Log.printWarning("RecordingListManager.recover : not an OcapLocator = " + loc);
                    }
                } else {
                    Log.printWarning("RecordingListManager.recover : invalid locator");
                }
            } else {
                Log.printWarning("RecordingListManager.recover : not a LocatorRecordingSpec = " + spec);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void dispose() {
        RecordingManager.getInstance().removeRecordingChangedListener(this);
    }

    private void deleteInvalidLists() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.deleteInvalidLists()");
        RecordingList list = RecordingManager.getInstance().getEntries();
        for (int i = 0; i < list.size(); i++) {
            RecordingRequest req = list.getRecordingRequest(i);
            int state = req.getState();
//            if (state == LeafRecordingRequest.FAILED_STATE || state == OcapRecordingRequest.CANCELLED_STATE) {
            if (state == OcapRecordingRequest.CANCELLED_STATE) {
                try {
                    req.delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    private void printList() {
        RecordingList list = RecordingManager.getInstance().getEntries();
        Log.printDebug("RecordingListManager: list size = " + list.size());
        for (int i = 0; i < list.size(); i++) {
            RecordingRequest req = list.getRecordingRequest(i);
            try {
                Log.printDebug(i + "] id=" + req.getId() + ", st=" + req.getState() +
                    ", chn=" + AppData.get(req, AppData.CHANNEL_NUMBER) + ", sid=" + AppData.get(req, AppData.SOURCE_ID) +
                    ", sc_start=" + AppData.getScheduledStartTime(req) + ", sc_dur=" + AppData.getScheduledDuration(req) +
                    ", exp=" + new Date(RecordManager.getExpirationTime(req)) +
                    ", title=" + AppData.get(req, AppData.TITLE)
                    );
                RecordingSpec rs = req.getRecordingSpec();
                if (rs != null && rs instanceof ServiceRecordingSpec) {
                    ServiceRecordingSpec srs = (ServiceRecordingSpec) rs;
                    Service svc = srs.getSource();
                    int sid = -1;
                    if (svc != null) {
                        Locator loc = svc.getLocator();
                        if (loc != null & loc instanceof OcapLocator) {
                            sid = ((OcapLocator) loc).getSourceID();
                        }
                    }
                    Log.printDebug(" .. " + srs.getStartTime() +  ", " + srs.getDuration() + ", sid=" + sid);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
        list = getPlayableList();
        Log.printDebug("RecordingListManager: getPlayableList = " + list);
        if (list != null) {
            Log.printDebug("getPlayableList.size = " + list.size());
            for (int i = 0; i < list.size(); i++) {
                try {
                    LeafRecordingRequest req = (LeafRecordingRequest) list.getRecordingRequest(i);
                    RecordedService rs = req.getService();
                    if (rs != null) {
                        Log.printDebug(i + "] time=" + rs.getRecordingStartTime() + ", dur=" + rs.getRecordedDuration() + ", id=" + req.getId());
                    }
                    AppData.printAppData(req);
                } catch (Exception ex) {
                    Log.print(ex);
                }

            }
        }
    }

    public static short getStateGroup(int state) {
        switch (state) {
            // IN_PROGRESS_STATES
            case OcapRecordingRequest.IN_PROGRESS_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
            case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
            case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                //->Kenneth : 너무 빈번하게 호출되므로 extra 에서만 보이게 함
                //if (Log.EXTRA_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.getStateGroup("+state+") returns IN_PROGRESS");
                return IN_PROGRESS;
            // PENDING_STATES
            case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
            case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                //if (Log.EXTRA_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.getStateGroup("+state+") returns PENDING");
                return PENDING;
            // COMPLETED_STATES
            case OcapRecordingRequest.INCOMPLETE_STATE:
            case OcapRecordingRequest.COMPLETED_STATE:
                //if (Log.EXTRA_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.getStateGroup("+state+") returns COMPLETED");
                return COMPLETED;
            default:
                //if (Log.EXTRA_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.getStateGroup("+state+") returns INVALID");
                return INVALID;
        }
    }

    /**
     * MyChannel에서 재생할수 있는 list를 return.
     * 정렬되지 않은 list를 return 함에 주의
     * INCOMPLETE_STATE, COMPLETED_STATE
     */
    public RecordingList getPlayableList() {
        return RecordingManager.getInstance().getEntries(playableFilter);
    }

    /**
     * MyChannel에서 재생할수 있는 list를 return.
     * INCOMPLETE_STATE, COMPLETED_STATE
     */
    public boolean isPlayable(RecordingRequest req) {
        boolean result = playableFilter.accept(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.isPlayable("+req+") returns "+result);
        return result;
    }

    public RecordingList getInProgressList() {
        return RecordingManager.getInstance().getEntries(inProgressFilter);
    }

    public RecordingList getInProgressValidList() {
        return RecordingManager.getInstance().getEntries(inProgressValidFilter);
    }

    /**
     * 녹화중인 list를 return.
     * IN_PROGRESS_STATE, IN_PROGRESS_INSUFFICIENT_SPACE_STATE,
     * (IN_PROGRESS_WITH_ERROR_STATE, IN_PROGRESS_INCOMPLETE_STATE)
     */
    public RecordingList getInProgressWithoutErrorList() {
        return RecordingManager.getInstance().getEntries(inProgressWithoutErrorFilter);
    }

    /**
     * Pending 상태의 list를 return.
     * PENDING_NO_CONFLICT_STATE, PENDING_WITH_CONFLICT_STATE,
     */
    public RecordingList getPendingList() {
        return RecordingManager.getInstance().getEntries(pendingFilter);
    }

    public RecordingList getRecordedList() {
        return RecordingManager.getInstance().getEntries(recordedFilter);
    }

    public RecordingList getCancelableList() {
        return RecordingManager.getInstance().getEntries(cancelableFilter);
    }

    public RecordingList getValidList() {
        return RecordingManager.getInstance().getEntries(validFilter);
    }

    /** 녹화 예정인 Request인지 check. */
    public static boolean isPending(RecordingRequest req) {
        boolean result = pendingFilter.accept(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.isPending("+req+") returns "+result);
        return result;
    }

    /** 녹화중인 Request인지 check. */
    public static boolean isInProgress(RecordingRequest req) {
        boolean result = inProgressFilter.accept(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.isInProgress("+req+") returns "+result);
        return result;
    }

    /** 녹화중인 Request인지 check. */
    public boolean isRecorded(RecordingRequest req) {
        boolean result = recordedFilter.accept(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.isRecorded("+req+") returns "+result);
        return result;
    }

    /** FAILED_STATE인 list를 return */
    public RecordingList getFailedList() {
        return RecordingManager.getInstance().getEntries(failedFilter);
    }

    /** CANCLED_STATE, FAILED_STATE인 list를 return */
    public RecordingList getCancelledFailedList() {
        return RecordingManager.getInstance().getEntries(cancelledFailedFilter);
    }

    /** Cancelable 이고 UNSET이 아닌 list를 return. */
    public RecordingList getActiveCancelableList() {
        RecordingList list = getCancelableList();
        if (list == null) {
            return null;
        }
        return list.filterRecordingList(activeFilter);
    }

    public RecordingList getCancelTargetList(long maxStartTime) {
        RecordingList list = getActiveCancelableList();
        if (list == null) {
            return null;
        }
        list = list.filterRecordingList(new ScheduledDateFilter(maxStartTime));
        return list.sortRecordingList(new ScheduledDateComparator());
    }

    /** 겹치고 UNSET이 아닌 list를 return */
    public RecordingList getActiveOverlappingList(RecordingRequest req) {
        if (req == null || !(req instanceof OcapRecordingRequest)) {
            return null;
        }
//        try {
//            if (req.getState() == OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
//                return RecordingManager.getInstance().getEntries(inProgressWithoutErrorFilter);
//            }
//        } catch (Exception ex) {
//        }
        RecordingList list = ((OcapRecordingRequest) req).getOverlappingEntries();
        if (list == null) {
            return null;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("RecordingListManager.getActiveOverlappingList: found " + list.size());
        }
        return list.filterRecordingList(activeFilter);
    }

    public RecordingList getActiveOverlappingList(long from, long to) {
        RecordingList list = getActiveCancelableList();
        if (list == null) {
            return null;
        }
        return list.filterRecordingList(new OverlappingFilter(from, to));
    }

    public RecordingList getConflictList() {
        RecordingList list = RecordingManager.getInstance().getEntries(pendingWithConflictFilter);
        if (list == null) {
            return null;
        }
        return list.filterRecordingList(activeFilter);
    }

    /** RecordingList를 주어진 AppData로 filtering */
    public static RecordingList filter(RecordingList list, String key, Object value) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.filter("+key+")");
        if (list == null) {
            return null;
        }
        return list.filterRecordingList(new AppDataFilter(key, value));
    }

    /** RecordingList를 주어진 AppData가 아닌것만 filtering */
    public static RecordingList reverseFilter(RecordingList list, String key, Object value, boolean includeNull) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.reverseFilter("+key+")");
        if (list == null) {
            return null;
        }
        return list.filterRecordingList(new ReverseAppDataFilter(key, value, includeNull));
    }

//    public static RecordingList filterOutToBeDeleted(RecordingList list) {
//        if (list == null) {
//            return null;
//        }
//        return list.filterRecordingList(new NotToBeDeletedFilter());
//    }

    /** RecordingList를 주어진 AppData로 sorting */
    public static RecordingList sort(RecordingList list, String key) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.sort("+key+")");
        if (list == null) {
            return null;
        }
        return list.sortRecordingList(new AppDataComparator(key));
    }

    public static RecordingList sortByDate(RecordingList list) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.sortByDate()");
        if (list == null) {
            return null;
        }
        return list.sortRecordingList(new ScheduledDateComparator());
    }

    /** false이면 list에서 별도 아이콘 표시. */
    public static boolean checkClean(AbstractRecording r) {
        if (r == null) {
            return true;
        }
        try {
            int state = r.getState();
            switch (state) {
                case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
//                case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                    return false;
                case OcapRecordingRequest.INCOMPLETE_STATE:
                    int reason = r.getInteger(AppData.FAILED_REASON);
                    return reason == 0 || reason == RecordingFailedException.USER_STOP;
            }
            return true;
        } catch (IllegalStateException ex) {
            return false;
        } catch (Exception ex) {
            return true;
        }
    }

    /** RecordingChangedListener */
    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.recordingChanged("+e+")");
        runner.start();
    }

    /** DeviceListListener */
    public void deviceListChanged(Vector deviceList) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingListManager.deviceListChanged()");
        runner.start();
    }

    // notify
    public synchronized void run() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"START : RecordingListManager.run()");
        RecordingList list = RecordingManager.getInstance().getEntries(validFilter);
        int size = list.size();
        Vector allIds = new Vector();
        Vector recIds = new Vector();
        Hashtable newTable = new Hashtable();

        long newMax = 0;
        RecordingRequest r;
        for (int i = 0; i < size; i++) {
            r = list.getRecordingRequest(i);
            if (r instanceof LeafRecordingRequest) {
                LeafRecordingRequest leaf = (LeafRecordingRequest) r;
                LocalRecording local = new LocalRecording(leaf);
                if (!local.getBoolean(AppData.SUPPRESS_CONFLICT)) {
                    newMax = Math.max(newMax, local.getEndTime());
                }
                PvrContentImpl content;
                if (local.getStateGroup() == PENDING) {
                    content = new PvrContentImpl(local);
                } else {
                    content = new RecordedContentImpl(local);
                    recIds.addElement(content.getId());
                }
                String id = content.getId();
                allIds.addElement(id);
                newTable.put(id, content);
            }
        }
        maxEndTime = newMax;

        Vector devices = HomeNetManager.getInstance().getDeviceList();
        if (devices != null) {
            Enumeration en = devices.elements();
            while (en.hasMoreElements()) {
                RemoteDevice rd = (RemoteDevice) en.nextElement();
                RecordingContentItem[] array = rd.getRecordings();
                for (int i = 0; i < array.length; i++) {
                    RemoteRecording rr = new RemoteRecording(array[i]);
                    if (rr.getStateGroup() != INVALID) {
                        PvrContentImpl content = new RecordedContentImpl(rr);
                        String id = content.getId();
                        recIds.addElement(id);
                        allIds.addElement(id);
                        newTable.put(id, content);
                    }
                }
            }
        }

        String[] newRecIds = new String[recIds.size()];
        recIds.copyInto(newRecIds);
        String[] newAllIds = new String[allIds.size()];
        allIds.copyInto(newAllIds);

        this.allContents = newAllIds;
        this.recordedContents = newRecIds;
        this.contentsMap = newTable;

        ContentsChangedListener[] ccl = new ContentsChangedListener[listeners.size()];
        listeners.copyInto(ccl);
        for (int i = 0; i < ccl.length; i++) {
            try {
                ccl[i].contentsChanged(allContents);
            } catch (Exception t) {
                Log.print(t);
            }
        }

        ArrayList remove = new ArrayList(4);
        Iterator it = toBeDeleted.iterator();
        while (it.hasNext()) {
            Integer in = (Integer) it.next();
            if (!newTable.containsKey(in.toString())) {
                remove.add(in);
            }
        }
        for (int i = 0; i < remove.size(); i++) {
            toBeDeleted.remove(remove.get(i));
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"END : RecordingListManager.run()");
    }

    public void addContentsChangedListener(ContentsChangedListener l) {
        synchronized (listeners) {
            if (!listeners.contains(l)) {
                listeners.addElement(l);
            }
        }
    }

    public void removeContentsChangedListener(ContentsChangedListener l) {
        synchronized (listeners) {
            listeners.removeElement(l);
        }
    }

    public String[] getAllContents() {
        return allContents;
    }

    public String[] getRecordedContents() {
        return recordedContents;
    }

    public PvrContent getContent(String id) {
        return (PvrContent) contentsMap.get(id);
    }

    public static RecordedService getService(RecordingRequest r) {
        if (r instanceof LeafRecordingRequest) {
            try {
                return ((LeafRecordingRequest) r).getService();
            } catch (Exception ex) {
            }
        }
        return null;
    }

    public static String getDeletingTargetString() {
        return toBeDeleted.toString();
    }

}


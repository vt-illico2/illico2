package com.alticast.illico.pvr;

import java.rmi.RemoteException;

import org.ocap.dvr.storage.FreeSpaceListener;
import org.ocap.dvr.storage.MediaStorageVolume;
import org.ocap.storage.LogicalStorageVolume;
import org.ocap.storage.StorageManager;
import org.ocap.storage.StorageProxy;

import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.data.ProgramRemainder;
import com.alticast.illico.pvr.config.PvrConfig;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Constants;


/**
 * provide information about pvr storage
 */
public class StorageInfoManager implements DataUpdateListener {

    public static final boolean KB_UNIT_1024 = false;

	private static StorageInfoManager instance = new StorageInfoManager();

    public static StorageInfoManager getInstance() {
        return instance;
    }

    public static final byte DISK_SUFFICIENT = 0;
    public static final byte DISK_WARNING    = 1;
    public static final byte DISK_CRITICAL   = 2;
    public static final byte DISK_FULL       = 3;

    private MediaStorageVolume msv;
    private long totalStorage;

    private FreeSpaceListener warningListener = new WarningListener();
    private FreeSpaceListener criticalListener = new CriticalListener();

    public static int WARNING_LEVEL = 20;
    public static int CRITICAL_LEVEL = 5;

    //->Kenneth[2015.2.3] 4K
    private static int uhdBitRate;
    private static int hdBitRate;
    private static int sdBitRate;

    private StorageInfoManager() {
        DataCenter dataCenter = DataCenter.getInstance();
        sdBitRate = dataCenter.getInt("SD_BITRATE");
        hdBitRate = dataCenter.getInt("HD_BITRATE");
        uhdBitRate = dataCenter.getInt("UHD_BITRATE");
    }

    //->Kenneth[2015.3.14] DDC-007 305H
    private void defineBitrate() {
        DataCenter dataCenter = DataCenter.getInstance();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate()"); 
        long xCapacity = dataCenter.getLong("BITRATE_X_DISK_CAPACITY_THRESHOLD_IN_BYTES");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate() : xCapacity = "+xCapacity); 
        long total = totalStorage;
        //long total = getAllocatedSpace();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate() : total = "+total); 
        if (xCapacity <= 0) return;
        if (xCapacity > total) return;
        int sdBitRateX = dataCenter.getInt("SD_BITRATE_X");
        int hdBitRateX = dataCenter.getInt("HD_BITRATE_X");
        int uhdBitRateX = dataCenter.getInt("UHD_BITRATE_X");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate() : sdBitRateX = "+sdBitRateX); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate() : hdBitRateX = "+hdBitRateX); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.defineBitrate() : uhdBitRateX = "+uhdBitRateX); 
        if (sdBitRateX > 0) {
            sdBitRate = sdBitRateX;
        }
        if (hdBitRateX > 0) {
            hdBitRate = hdBitRateX;
        }
        if (uhdBitRateX > 0) {
            uhdBitRate = uhdBitRateX;
        }
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.init() : msv = "+msv); 
    	if (msv != null) {
    		return;
    	}
        StorageProxy[] sp = StorageManager.getInstance().getStorageProxies();
        for (int i = 0; i < sp.length; i++) {
        	if (Log.DEBUG_ON) {
                Log.printDebug("StorageProxy = " + sp[i].getStatus() + ", " + sp[i].getFreeSpace() + "/" + sp[i].getTotalSpace());
            }
        	LogicalStorageVolume[] vol = sp[i].getVolumes();
            totalStorage = sp[i].getTotalSpace();
        	for (int j = 0; j < vol.length; j++) {
        		if (Log.DEBUG_ON) {
                    Log.printDebug("vol = " + i + " " + j + ", " + vol[j] + ", " + vol[j].getPath());
                }
        		if (vol[j] instanceof MediaStorageVolume) {
        			msv = (MediaStorageVolume) vol[j];
                    if (Log.DEBUG_ON) {
                        Log.printDebug("found MSV = " + i + " " + j + ", " + msv);
                    }
        		}
        	}
        }

        DataCenter.getInstance().addDataUpdateListener(ConfigManager.PVR_CONFIG_INSTANCE, this);
        Object config = DataCenter.getInstance().get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config != null) {
            dataUpdated(ConfigManager.PVR_CONFIG_INSTANCE, config, config);
        }

        //->Kenneth[2015.3.14] DDC-007 305H
        defineBitrate();
    }

    //->Kenneth[2015.2.3] 4K : hd -> definition
    public boolean isCapable(long duration, int definition) {
        int targetRate = sdBitRate;
        if (definition == Constants.DEFINITION_HD) {
            targetRate = hdBitRate;
        } else if (definition == Constants.DEFINITION_4K) {
            targetRate = uhdBitRate;
        }
        //->Kenneth[2015.6.3] VDTRMASTER-5441 : Rafik 메일의 의거 stream 의 경우는 1024 대신 1000 사용
    	int bytesPerSec = targetRate * 1000 / 8;
    	//int bytesPerSec = targetRate * 1024 / 8;
        return bytesPerSec * duration / Constants.MS_PER_SECOND <= getFreeSpace();
    }

    //->Kenneth[2015.2.3] 4K : hd -> definition
    // returns estimated available recording duration in minute;
    public int getAvailableDuration(int definition) {
        return getAvailableDuration(getFreeSpace(), definition);
    }

    //->Kenneth[2015.2.3] 4K : hd -> definition
    public static int getAvailableDuration(long free, int definition) {
        // Kbps -> byte per min
        int targetRate = sdBitRate;
        if (definition == Constants.DEFINITION_HD) {
            targetRate = hdBitRate;
        } else if (definition == Constants.DEFINITION_4K) {
            targetRate = uhdBitRate;
        }
        //->Kenneth[2015.6.3] VDTRMASTER-5441 : Rafik 메일의 의거 stream 의 경우는 1024 대신 1000 사용
    	int bytesPerMinute = targetRate * 1000 * 60 / 8;
    	//int bytesPerMinute = targetRate * 1024 * 60 / 8;
    	// 1048576 = Mega, 8 = bit to byte, 60 = seconds to minute
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+
                "StorageInfoManager.getAvailiableDuration("+free+", "+definition+")"); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+
                "StorageInfoManager.getAvailiableDuration() bytesPerMinute = "+bytesPerMinute); 
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+
                "StorageInfoManager.getAvailiableDuration() returns = "+(int)(free/bytesPerMinute)); 
        return (int) (free / bytesPerMinute);
    }

    public static String getSizeString(long bytes) {
        if (KB_UNIT_1024) {
            int mb = (int) (bytes >> 20);
            if (mb < 1024) {
                String strMb = DataCenter.getInstance().getString("pvr.MB");
                if (mb <= 0) {
                    return DataCenter.getInstance().getString("pvr.N/A") + strMb;
                } else {
                    return mb + strMb;
                }
            }
            if (mb >= 1024 * 100) {
                return (mb >> 10) + DataCenter.getInstance().getString("pvr.GB");
            }
            return ((double) ((mb * 10) / 1024) / 10.0D) + DataCenter.getInstance().getString("pvr.GB");
        } else {
            int mb = (int) (bytes / 1000000);
            if (mb < 1000) {
                String strMb = DataCenter.getInstance().getString("pvr.MB");
                if (mb <= 0) {
                    return DataCenter.getInstance().getString("pvr.N/A") + strMb;
                } else {
                    return mb + strMb;
                }
            }
            if (mb >= 1000 * 100) {
                return (mb / 1000) + DataCenter.getInstance().getString("pvr.GB");
            }
            return ((double) (mb / 100) / 10.0D) + DataCenter.getInstance().getString("pvr.GB");
        }
    }

    public static String getSizeString2(long bytes) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.getSizeString2("+bytes+")"); 
        if (KB_UNIT_1024) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.getSizeString2() : KB_UNIT_1024"); 
            int mb = (int) (bytes >> 20);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.getSizeString2() : mb = "+mb); 
            if (mb < 1024) {
                return mb + DataCenter.getInstance().getString("pvr.MB");
            }
            if (mb >= 1024 * 100) {
                return (mb >> 10) + DataCenter.getInstance().getString("pvr.GB");
            }
            return ((double) ((mb * 10) / 1024) / 10.0D) + DataCenter.getInstance().getString("pvr.GB");
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.getSizeString2() : NOT KB_UNIT_1024"); 
            int mb = (int) (bytes / 1000000);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StorageInfoManager.getSizeString2() : mb = "+mb); 
            if (mb < 1000) {
                return mb + DataCenter.getInstance().getString("pvr.MB");
            }
            if (mb >= 1000 * 100) {
                return (mb / 1000) + DataCenter.getInstance().getString("pvr.GB");
            }
            return ((double) (mb / 100) / 10.0D) + DataCenter.getInstance().getString("pvr.GB");
        }
    }

    public byte getDiskStatus() {
        int free = getFreePercent();
        if (free >= WARNING_LEVEL) {
            return DISK_SUFFICIENT;
        } else if (free >= CRITICAL_LEVEL) {
            return DISK_WARNING;
        } else if (isFull()) {
            return DISK_FULL;
        }
        return DISK_CRITICAL;
    }

    public long getAllocatedSpace() {
        if (msv == null) {
            return totalStorage > 0 ? totalStorage : 1;
        }
        long ret = msv.getAllocatedSpace();
        if (Environment.EMULATOR && ret == 0) { // TODO, remove
        	ret = totalStorage;
        }
        return ret;
    }

    public long getFreeSpace() {
    	long ret;
        if (msv == null) {
            return 0;
        }
        ret = msv.getFreeSpace();
        return ret;
    }

    public long getUsingSpace() {
    	return getAllocatedSpace() - getFreeSpace();
    }

    public int getFreeSpaceMB() {
    	return (int) (getFreeSpace() >> 20);
    }

    public double getFreeRatio() {
        if (msv == null) {
            return 0;
        }
        return getRatio(getFreeSpace(), getAllocatedSpace());
    }

    public static double getRatio(long free, long total) {
        if (total <= 0) {
            Log.printError("StorageInfo: allocated space is not avaliable!");
            return 0;
        }
        if (free > total) {
            Log.printError("StorageInfo: free space is larger than allocated space!");
            return 100;
        }
        if (free < 0L) {
            Log.printError("StorageInfo: free space is negative!");
            return 0;
        }
        return ((double) free / total);
    }

    public int getFreePercent() {
        return (int) (getFreeRatio() * 100);
    }

    public int getUsingPercent() {
        return 100 - getFreePercent();
    }

    public boolean isFull() {
        return getFreeRatio() < 0.001d;
    }

    public synchronized void dataUpdated(String key, Object old, Object value) {
        if (msv != null) {
            msv.removeFreeSpaceListener(warningListener);
            msv.removeFreeSpaceListener(criticalListener);

            PvrConfig config = (PvrConfig) value;
            WARNING_LEVEL = config.warningSpaceLevel;
            CRITICAL_LEVEL = config.criticalSpaceLevel;
            msv.addFreeSpaceListener(warningListener, WARNING_LEVEL);
            msv.addFreeSpaceListener(criticalListener, CRITICAL_LEVEL);

        	Log.printInfo("StorageInfoManager: new level = "
                + config.warningSpaceLevel + ", " + config.criticalSpaceLevel);
            IssueManager.getInstance().updateIssue();
        }
    }

    public void dataRemoved(String key)  {
    }

    class WarningListener implements FreeSpaceListener {
        public void notifyFreeSpace() {
            Log.printWarning("StorageInfoManager.notifyFreeSpace - Warning");
            FrameworkMain.getInstance().printSimpleLog("Disk Space - Warning");
            IssueManager.getInstance().updateIssue();
        }
    }


    class CriticalListener implements FreeSpaceListener {
        public void notifyFreeSpace() {
            Log.printWarning("StorageInfoManager.notifyFreeSpace - Critical");
            FrameworkMain.getInstance().printSimpleLog("Disk Space - Critical");
            IssueManager.getInstance().updateIssue();

            Thread th = new Thread() {
                public void run(){
                    NotificationService service = (NotificationService) DataCenter.getInstance().get(NotificationService.IXC_NAME);
                    try {
//                        // Pellos Priority is 0
//                        service.setPriority(0);
                        String messageID = service.setNotify(new ProgramRemainder());
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            };
            th.start();
        }
    }
}

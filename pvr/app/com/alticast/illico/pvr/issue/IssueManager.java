package com.alticast.illico.pvr.issue;

import java.awt.event.KeyEvent;
import java.util.*;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.ocap.dvr.OcapRecordingRequest;
import org.ocap.dvr.storage.FreeSpaceListener;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.RecordingList;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.hn.recording.*;
import org.ocap.hn.content.*;

import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.filter.MultipleStateFilter;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.RemoteIterator;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.pvr.config.*;

// Manager class for issue about PVR - conflict/low disk
public class IssueManager extends LayeredWindow
                          implements LayeredKeyHandler, RecordingChangedListener, DeviceListListener, Runnable {
    private static final long SCHEDULE_WARNING_BEFORE = 48 * Constants.MS_PER_HOUR + 30 * Constants.MS_PER_MINUTE;

    private static IssueManager instance = new IssueManager();

    public static IssueManager getInstance() {
        return instance;
    }

    private Issue issue;

    private ConflictResolveSession currentSession = null;

    private LayeredUI popupUI;
    private Popup current;

    private Vector listeners = new Vector();

    private SingleThreadRunner runner;

    private Worker conflictWorker = new Worker("IssueManager.conflict_worker", false);

    private TvProgram tooManyProgram = null;

    private MultipleStateFilter conflictFilter = RecordingListManager.cancelableFilter;

    private IssueManager() {
        runner = new SingleThreadRunner(this, "IssueManager.updateIssue");
        RecordingManager.getInstance().addRecordingChangedListener(this);
        if (App.SUPPORT_HN) {
            HomeNetManager.getInstance().addDeviceListListener(this);
        }
        updateIssue();
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.init()");
    }

    public void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.pause()");
        if (!isConflictResolving()) {
            if (current != null) {
                stopPopup(current);
            }
        }
    }

    public void destroy() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.destroy()");
        RecordingManager.getInstance().removeRecordingChangedListener(this);
        if (popupUI != null) {
            WindowProperty.dispose(popupUI);
        }
    }

    public synchronized void setTooManyProgram(TvProgram pr) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.setTooManyProgram("+pr+")");
        tooManyProgram = pr;
        ConflictResolveSession session = currentSession;
        if (pr == null || session == null) {
            return;
        }
        if (checkTooMany(currentSession.currentIssue, pr)) {
            Log.printWarning("IssueManager: hide conflict popup");
            currentSession.notifyConflictResolved();
//            hideAll();
            return;
        }
    }

    protected boolean checkTooMany(RecordingRequest[] array) {
        return checkTooMany(array, tooManyProgram);
    }

    protected boolean checkTooMany(RecordingRequest[] array, TvProgram p) {
        if (p == null || array == null || array.length == 0) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.checkTooMany() returns false");
            return false;
        }
        RecordingRequest many = RecordManager.getInstance().getRequest(p);
        Log.printWarning("IssueManager: checkTooMany = " + many.getId());
        if (many != null) {
            for (int i = 0; i < array.length; i++) {
                Log.printWarning("IssueManager: checkTooMany [" + i + "/" + array.length + "] = " + array[i].getId());
                if (many.equals(array[i])) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.checkTooMany() returns true");
                    return true;
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.checkTooMany() returns false");
        return false;
    }

    public synchronized boolean isConflictResolving() {
        return currentSession != null;
//        if (current == null) {
//            return false;
//        }
//        if (current == conflictCautionPopup || current == conflictPickPopup
//                || current == otherShowtimesPopup || current == cancelOptionPopup) {
//            return true;
//        }
//        return false;
    }

    public synchronized void startSession(ConflictResolveSession session) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.startSession("+session+")");
        currentSession = session;
        readyUI();
        session.prepare();
        this.add(session);
        session.setVisible(true);
    }

    public synchronized boolean stopSession(ConflictResolveSession session) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.stopSession("+session+")");
        if (session == currentSession) {
            currentSession = null;
            removeAndHide(session);
//            hideAll();
            return true;
        }
        return false;
    }

    private synchronized void startPopup(Popup c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.startPopup()");
        c.prepare();
        readyUI();
        this.add(c);
        c.start();
        c.setVisible(true);
        current = c;
    }

    public synchronized void stopPopup(Popup c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.stopPopup()");
        if (c != null) {
            c.stop();
            if (current == c) {
                current = null;
            }
            removeAndHide(c);
        }

    }

    private void removeAndHide(UIComponent c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.removeAndHide()");
        this.remove(c);
        if (this.getComponentCount() <= 0) {
            if (popupUI != null) {
                popupUI.deactivate();
            }
        }
    }

    public Issue getIssue() {
        return issue;
    }

    public void showIssue() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.showIssue()");
        Issue is = this.issue;
        if (is == null) {
            Log.printWarning("IssueManager: no issue !");
            return;
        }
        readyUI();
        switch (is.getType()) {
            case Issue.DISK_WARNING_ISSUE:
            case Issue.DISK_CRITICAL_ISSUE:
            case Issue.DISK_FULL_ISSUE:
                showDiskIssuePopup((DiskIssue) is);
                break;
            case Issue.CONFLICT_ISSUE:
                startPopup(new ConflictIssuePopup((ConflictIssue) is));
                break;
            case Issue.FAILED_ISSUE:
                startPopup(new RecordingFailedPopup((FailedIssue) is));
                break;
            case Issue.MULTIPLE_ISSUE:
                MultipleIssuePopup multipleIssuePopup = new MultipleIssuePopup();
                multipleIssuePopup.setIssue((MultipleIssue) is);
                multipleIssuePopup.resetFocus();
                startPopup(multipleIssuePopup);
                break;
        }
    }

    private void showDiskIssuePopup(DiskIssue diskIssue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.showDiskIssuePopup()");
        byte status = diskIssue.getType();
        if (status == DiskIssue.DISK_CRITICAL_ISSUE) {
            RecordingList list = RecordingListManager.getInstance().getCancelTargetList(
                                        System.currentTimeMillis() + SCHEDULE_WARNING_BEFORE);
            if (list != null && list.size() > 0) {
                startPopup(new DiskFullCancelPopup(list));
                return;
            }
        }
        startPopup(new DiskIssuePopup(status));
    }

    public boolean handleKeyEvent(UserEvent evt) {
        if (evt.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = evt.getCode();
        if (currentSession != null) {
            boolean consumed = currentSession.handleKey(code);
            if (consumed) {
                return true;
            }
            return blockKeys(code);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"IssueManager.handleKeyEvent: cur = " + current + ", key = " + code);
        }
        if (current != null && current.getParent() != null && current.isVisible()) {
            boolean consumed = current.handleKey(code);
            if (consumed) {
                return true;
            }
            return blockKeys(code);
        }
        return false;
    }

    private boolean blockKeys(int code) {
        switch (code) {
            case OCRcEvent.VK_GUIDE:
            case OCRcEvent.VK_INFO:
            case KeyCodes.SEARCH:
            case KeyCodes.LIST:
            case KeyCodes.SETTINGS:
            case KeyCodes.COLOR_A:
            case KeyCodes.COLOR_B:
            case KeyCodes.COLOR_C:
            case KeyCodes.COLOR_D:
            case KeyCodes.PIP:
            case KeyCodes.MENU:
            case KeyCodes.WIDGET:
            case KeyCodes.FAST_FWD:
            case KeyCodes.REWIND:
            case KeyCodes.PLAY:
            case KeyCodes.PAUSE:
            case KeyCodes.LIVE:
            case KeyCodes.STOP:
            case KeyCodes.RECORD:
            case KeyCodes.FORWARD:
            case KeyCodes.BACK:
            case OCRcEvent.VK_LEFT:
            case OCRcEvent.VK_RIGHT:
            case OCRcEvent.VK_UP:
            case OCRcEvent.VK_DOWN:
            case OCRcEvent.VK_PAGE_UP:
            case OCRcEvent.VK_PAGE_DOWN:
            case OCRcEvent.VK_ENTER:
            case OCRcEvent.VK_EXIT:
            case KeyCodes.LAST:
            case KeyCodes.FAV:
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                return true;
            default:
                return (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
        }
    }


    /** 강제 update. */
    public void updateIssue() {
        runner.start();
    }

    /** Remote Device의 failed recording을 update 하기 위해. */
    public void deviceListChanged(Vector deviceList) {
        updateIssue();
    }

    /** update. */
    public void run() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.run()");
        Vector vector = new Vector();
        byte diskStatus = StorageInfoManager.getInstance().getDiskStatus();
        if (Environment.SUPPORT_DVR) {
            switch (diskStatus) {
                case StorageInfoManager.DISK_WARNING:
                    vector.addElement(new DiskIssue(Issue.DISK_WARNING_ISSUE));
                    break;
                case StorageInfoManager.DISK_CRITICAL:
                    vector.addElement(new DiskIssue(Issue.DISK_CRITICAL_ISSUE));
                    break;
                case StorageInfoManager.DISK_FULL:
                    vector.addElement(new DiskIssue(Issue.DISK_FULL_ISSUE));
            }
        }

        RecordingList list = RecordingListManager.getInstance().getConflictList();
        Vector conflicts = getConflictIssues(list);
        if (conflicts != null) {
            vector.addAll(conflicts);
        }

        list = RecordingListManager.getInstance().getCancelledFailedList();
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                vector.addElement(new FailedIssue((LeafRecordingRequest) list.getRecordingRequest(i)));
            }
        }

        if (App.SUPPORT_HN) {
            Vector deviceList = HomeNetManager.getInstance().getDeviceList();
            if (deviceList != null) {
                Enumeration en = deviceList.elements();
                while (en.hasMoreElements()) {
                    RemoteDevice rd = (RemoteDevice) en.nextElement();
                    RecordingContentItem[] array = rd.getRecordings();
                    for (int i = 0; i < array.length; i++) {
                        try {
                            MetadataNode node = array[i].getRootMetadataNode();
                            int state = ((Integer) node.getMetadata(RecordingContentItem.PROP_RECORDING_STATE)).intValue();
                            if (RecordingListManager.cancelledFailedFilter.accept(state)) {
                                vector.addElement(new FailedIssue(array[i]));
                            }
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }

        Issue newIssue;
        int size = vector.size();
        if (size == 0) {
            newIssue = null;
        } else if (size == 1) {
            newIssue = (Issue) vector.elementAt(0);
        } else {
            newIssue = new MultipleIssue(vector);
        }

        boolean changed = false;
        if (issue == null && newIssue == null) {
            return;
        }
        if (issue != null && newIssue != null) {
            changed = !issue.equals(newIssue);
        } else {
            changed = true;
        }

        Log.printDebug("IssueManager.updateIssue: " + issue + " -> " + newIssue);
        Issue old = issue;
        issue = newIssue;

        if (changed) {
            notifyIssueChanged(old, newIssue);
        }
        Log.printInfo("IssueManager.updateIssue: changed = " + changed);
    }

    private Vector getConflictIssues(RecordingList list) {
        if (list == null) {
            return null;
        }
        int size = list.size();
        if (size == 0) {
            return null;
        }
        list = RecordingListManager.sortByDate(list);
        ConflictIssue[] array = new ConflictIssue[size];
        Vector vector = new Vector(size + 2);
        for (int i = 0; i < size; i++) {
            RecordingRequest req = list.getRecordingRequest(i);
            array[i] = new ConflictIssue(req);
        }
        ConflictIssue target = array[0];
        for (int i = 1; i < size; i++) {
            boolean merged = target.merge(array[i]);
            if (!merged) {
                vector.add(target);
                target = array[i];
            } else {
                Log.printInfo("IssueManager: merged conflict[" + i + "]");
            }
        }
        vector.add(target);
        return vector;
    }

    private void notifyIssueChanged(Issue old, Issue newIssue) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.notifyIssueChanged()");
        IssueChangedListener[] array;
        synchronized (listeners) {
            array = new IssueChangedListener[listeners.size()];
            listeners.copyInto(array);
        }
        for (int i = 0; i < array.length; i++) {
            try {
                array[i].issueChanged(old, newIssue);
            } catch (Exception t) {
                Log.print(t);
            }
        }
    }

    public void addListener(IssueChangedListener l) {
        if (!listeners.contains(l)) {
            listeners.addElement(l);
        }
    }

    public void removeListener(IssueChangedListener l) {
        try {
            listeners.removeElement(l);
        } catch (Exception ex) {
        }
    }

    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged("+e+")");
        RecordingRequest req = e.getRecordingRequest();
        int change = e.getChange();
        int oldState = e.getOldState();
        int newState = e.getState();

        switch (change) {
        case RecordingChangedEvent.ENTRY_STATE_CHANGED:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : ENTRY_STATE_CHANGED");
            if (newState == OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                // conflict가 된 경우
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : Call processNewConfilct() -- 1");
                processNewConfilct(req);
            } else {
                if (currentSession != null && conflictFilter.accept(oldState) && !conflictFilter.accept(newState)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : Call checkUpdateSession() -- 2");
                    checkUpdateSession(req);
                }
            }
            break;
        case RecordingChangedEvent.ENTRY_ADDED:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : ENTRY_ADDED");
            //->Kenneth[2015.2.13] 4K
            boolean uhdConflict = hasMaxUhdConflict(req);
            if (uhdConflict) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : ENTRY_ADDED : UHD Conflict");
                processNewUhdConfilct(req);
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : ENTRY_ADDED : Not UHD Conflict");
                switch (newState) {
                    case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
                        break;
                    case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : Call processNewConflict() -- 3");
                        processNewConfilct(req);
                        break;
                    case OcapRecordingRequest.IN_PROGRESS_STATE:
                    case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                    case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                        break;
                    case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                        // Note - RecordManager 내부로 옮겼다.
    //                    processNewConfilct(req);
                        break;
                }
            }
            AppData.remove(req, AppData.SUPPRESS_CONFLICT);
            break;
        case RecordingChangedEvent.ENTRY_DELETED:
            Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : ENTRY_DELETED : currentSession = " + currentSession);
            if (currentSession != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : Call checkUpdateSession() -- 4");
                checkUpdateSession(req);
            }
            break;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.recordingChanged() : Call updateIssue()");
        updateIssue();
    }

    //->Kenneth[2015.6.9] 디버깅을 위해서 추가한 기능
    private void printReq(RecordingRequest req) {
        try {
            String chName = AppData.getString(req, AppData.CHANNEL_NAME);
            int chNum = AppData.getInteger(req, AppData.CHANNEL_NUMBER);
            String progName = AppData.getString(req, AppData.EPISODE_TITLE);
            Date startTime = new Date(AppData.getScheduledStartTime(req));
            Date endTime = new Date(AppData.getScheduledStartTime(req)+AppData.getScheduledDuration(req));
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"** RecordingRequest LOG from  ***********************************************************");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Channel = ["+chNum+"] "+chName);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Program = "+progName);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"("+startTime+") ~ ("+endTime+")");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"*****************************************************************************************");
            if (Log.DEBUG_ON) Log.printDebug("");
        } catch (Exception e) {
        }
    }
    //<-

    //->Kenneth[2015.6.9] 두개의 RecordingRequest 가 overlap 되는지 돌려준다.
    private boolean isOverlapped(RecordingRequest req1, RecordingRequest req2) {
        boolean result = false;
        try {
            //->Kenneth[2015.6.10] PROGRAM_START_TIME,END_TIME 은 buffer 를 포함하지 않는다. 따라서 getScheduledStartTime 과
            //getScheduledDuration 을 사용하도록 한다.
            long startTime1 = AppData.getScheduledStartTime(req1);
            long endTime1 = startTime1 + AppData.getScheduledDuration(req1);
            long startTime2 = AppData.getScheduledStartTime(req2);
            long endTime2 = startTime2 + AppData.getScheduledDuration(req2);
            if (startTime1 < startTime2) {
                // 1 번이 더 먼저 시작되는 경우 : 2 번의 시작 시간이 1의 끝시간보다 앞에 있으면 오버랩임
                if (startTime2 < endTime1) {
                    result = true;
                }
            } else if (startTime1 > startTime2) {
                // 2 번이 더 먼저 시작되는 경우 : 1 번의 시작 시간이 2의 끝시간보다 앞에 있으며 오버랩임
                if (startTime1 < endTime2) {
                    result = true;
                }
            } else if (startTime1 == startTime2) {
                // 두개의 start time 이 같으면 오버랩 되는 것임.
                result = true;
            }
        } catch (Exception e) {
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.isOverlapped() returns "+result);
        return result;
    }
    //<-
    
    //->Kenneth[2015.2.13] 4K
    public boolean hasMaxUhdConflict(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict("+req+")");
        printReq(req);
        try {
            int maxUhdLimit = ConfigManager.getInstance().pvrConfig.uhdMaxTuners; 
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() : maxUhdLimit = "+maxUhdLimit);
            int definition = AppData.getInteger(req, AppData.DEFINITION);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() : definition = "+definition);
            // UhdMaxTuner 가 정상적으로 세팅되어 있고 또한 들어온 RecordingRequest 가 4K 인 경우 
            if (maxUhdLimit >= 0 && definition == 2) {
                // 동시간대에 overlap 되는 RecordingRequest 중에 4K 인 놈이 몇개인지 체크
                RecordingRequest[] uhdRecordings = getOverlappedUhdRecordingRequests(req);
                if (uhdRecordings != null) {
                    int uhdSize = uhdRecordings.length;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() : uhdSize = "+uhdSize);
                    // 아래 조건식에 '+1' 이 들어간 이유는 overlappedUhdSize 는 지금 ADD 된 RecordingRequest 를 뺀 숫자이기
                    // 때문에 지금 ADD 된 놈을 더해준 값을 가지고 max 와 비교해야 맞다
                    if ((uhdSize+1) > maxUhdLimit) {
                        //->Kenneth[2015.6.9] VDTRMASTER-5463 : 동시간대에 초과된 recording 이 있다하더라도 실제 그만한
                        // 튜너가 필요한지는 다시 확인해야 한다.
                        // 각 request 마다 overlap 되는 놈의 숫자를 모두 확인해서 하나라도 max 를 넘어서는 놈이 있으면 실제
                        // conflict 있는 것임. 만약 모두 max 를 넘어서지 않는다면 실제 tuner 가 초과되어 사용되는 경우는
                        // 아님. 즉 한 프로그램(A)이 여러 프로그램(B, C)과 오버랩 되지만 다른 프로그램(B 와 C)끼리는 오버랩 되지 않아서
                        // 실제 사용해야할 튜너는 오버랩되는 프로그램의 총합보다 작게 된다. (A,B,C 의 세개가 동시간에
                        // 겹치는 것이 아닌 A,B 혹은 A,C 가 겹치되 B,C 는 겹치지 않는 상황인 것임.)

                        // ====> 생각해 보니 이 논리에도 구멍이 있었음. 가령 A 와 오버랩 되는게 B,C 가 있을수 있지만 B
                        // 와 오버랩 되는건 A 나, C 가 아닌 D 가 될수도 있음. 이러면 서로 다른 request 를 가지고
                        // 비교하기 때문에 옳은 갯수를 구할 수 없음. 따라서 처음 가져온 오버랩 리스트 내에서만 상호
                        // 비교를 하도록 한다.
                        for (int i = 0 ; i < uhdSize ; i++) {
                            RecordingRequest target = uhdRecordings[i];
                            printReq(target);
                            int count = 0;
                            // 아래처럼 자기 자신을 배재하지 않고 for 문을 돌리면 자기 자신은 무조건 overlap 이라고
                            // count 된다. 마치 +1 을 한 효과를 주어 parameter 로 받은 req 를 더한 것과 같은 값이 된다.
                            // 즉 (uhdSize+1) > maxUhdLimit 과 동일한 계산식을 사용할 수 있게 된다.
                            for (int j = 0 ; j < uhdSize ; j++) {
                                if (isOverlapped(target, uhdRecordings[j])) {
                                    count ++;
                                }
                            }
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() : "+i+"th count = "+count);
                            if ((count+1) > maxUhdLimit) {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() returns true");
                                return true;
                            }

                            //RecordingRequest[] reqs= getOverlappedUhdRecordingRequests(target);
                            //if (reqs == null || reqs.length == 0) continue;
                            //int overlappedSize = reqs.length;
                            //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() : "+i+"th overlappedSize = "+overlappedSize);
                            //if ((overlappedSize+1) > maxUhdLimit) {
                            //    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() returns true");
                            //    return true;
                            //}
                        }
                        //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() returns true");
                        //return true;
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.hasMaxUhdConflict() returns false");
        return false;
    }

    //->Kenneth[2015.2.14] 4K
    private RecordingRequest[] getOverlappedUhdRecordingRequests(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.getOverlappedUhdRecordingRequests("+req+")");
        RecordingRequest[] result = null;
        try {
            RecordingList overlappedRecordings = RecordingListManager.getInstance().getActiveOverlappingList(req);
            if (overlappedRecordings == null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.getOverlappedUhdRecordingRequests() returns null -- 1");
                return result;
            }
            int size = overlappedRecordings.size();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.getOverlappedUhdRecordingRequests() : size = "+size);
            Vector v = new Vector();
            for (int i = 0 ; i < size ; i++) {
                RecordingRequest r = overlappedRecordings.getRecordingRequest(i);
                int tmpDef = AppData.getInteger(r, AppData.DEFINITION);
                if (tmpDef == 2) {
                    v.addElement(r);
                }
            }
            size = v.size();
            if (size == 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.getOverlappedUhdRecordingRequests() returns null -- 2");
                return result;
            }
            result = new RecordingRequest[size];
            for (int i = 0 ; i < size ; i++) {
                result[i] = (RecordingRequest)v.elementAt(i);
            }
        } catch (Exception e) {
            Log.print(e);
        }
        return result;
    }

    private void checkUpdateSession(final RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.checkUpdateSession()");
        Thread t = new Thread("IssueManager.checkUpdateSession") {
            public void run() {
                ConflictResolveSession session = currentSession;
                Log.printDebug(App.LOG_HEADER+"IssueManager.checkUpdateSession: session = " + session);
                if (session == null) {
                    return;
                }
                synchronized (session) {
                    RecordingRequest[] array = session.currentIssue;
                    if (array != null) {
                        int index = getIndex(array, req);
                        Log.printDebug(App.LOG_HEADER+"IssueManager.checkUpdateSession: array index = " + index);
                        if (index > 0) {
                            session.notifyConflictResolved();
                            processNewConfilct(array[0]);
                        } else if (index == 0 && array.length > 1) {
                            session.notifyConflictResolved();
                            processNewConfilct(array[1]);
                        }
                    } else {
                        Log.printDebug(App.LOG_HEADER+"IssueManager.checkUpdateSession: array is null");
                    }
                }
            }
        };
        t.start();
    }

    private int getIndex(RecordingRequest[] array, RecordingRequest target) {
        if (array == null || target == null) {
            return -1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == target) {
                return i;
            }
        }
        return -1;
    }

    //->Kenneth[2015.2.13] 4K
    public void processNewUhdConfilct(RecordingRequest req) {
        Log.printInfo(App.LOG_HEADER+"IssueManager.processNewUhdConfilct("+req+")");
        if (AppData.getBoolean(req, AppData.UNSET)) {
            Log.printInfo("IssueManager.processNewUhdConfilct: new recording is to be skipped.");
            return;
        }
        if (AppData.getBoolean(req, AppData.SUPPRESS_CONFLICT)) {
            Log.printInfo("IssueManager.processNewUhdConfilct: new recording is from remote.");
            return;
        }
        conflictWorker.push(ConflictResolveSession.createUhdSession(req, getOverlappedUhdRecordingRequests(req)));
    }

    public boolean processNewConfilct(RecordingRequest req) {
        Log.printInfo(App.LOG_HEADER+"IssueManager.processNewConfilct("+req+")");
        if (AppData.getBoolean(req, AppData.UNSET)) {
            Log.printInfo("IssueManager.processNewConfilct: new recording is to be skipped.");
            return false;
        }
        if (AppData.getBoolean(req, AppData.SUPPRESS_CONFLICT)) {
            Log.printInfo("IssueManager.processNewConfilct: new recording is from remote.");
//            AppData.remove(req, AppData.SUPPRESS_CONFLICT);
            return false;
        }
        conflictWorker.push(ConflictResolveSession.create(req, null));
        return true;
    }

    /** From multiple popup. */
    public boolean showConflictIssue(ConflictIssue ci, final Popup returnPopup) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.showConflictIssue()");
        conflictWorker.push(ConflictResolveSession.create(null, ci.getRecordings()));
        if (returnPopup != null) {
            conflictWorker.push(new Runnable() {
                public void run() {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.showConflictIssue().run() : Call startPopup(returnPopup)");
                    startPopup(returnPopup);
                }
            });
        }
        return true;
//        Popup p = current;
//        if (p != null && p instanceof MultipleIssuePopup) {
//            lastMultipleIssuePopup = (MultipleIssuePopup) p;
//        }
//        anotherShowtime.clear();
//        showConflictPopup(null, ci.getRecordings());
    }

    /** From EPG. */
    public void showTunerRequest(TunerRequestor tr) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.showTunerRequest()");
        ConflictResolveSession newSession = ConflictResolveSession.create(tr);
        if (newSession != null) {
            conflictWorker.push(newSession);
        }
    }

    public synchronized void _hideAll() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager._hideAll()");
        if (current != null) {
            current.stop();
        }
    }

    public synchronized void _hideAllImpl() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager._hideAllImpl()");
        removeAll();
        current = null;
        if (popupUI != null) {
            popupUI.deactivate();
        }
    }

    private synchronized void readyUI() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"IssueManager.readyUI()");
        if (popupUI == null) {
            popupUI = WindowProperty.PVR_CONFLICT_POPUP.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
            setBounds(Constants.SCREEN_BOUNDS);
            setVisible(true);
        }
        if (current != null) {
            current.stop();
            remove(current);
        }
        popupUI.activate();
    }


}


package com.alticast.illico.pvr.issue;

import java.awt.Graphics;
import com.alticast.illico.pvr.list.Entry;
import com.alticast.illico.pvr.ui.ListPanel;
import com.alticast.illico.pvr.gui.ListPanelRenderer;
import com.videotron.tvi.illico.framework.DataCenter;

public abstract class Issue extends Entry {

    public static final byte NO_ISSUE = 0;
    public static final byte DISK_WARNING_ISSUE = 1;
    public static final byte DISK_CRITICAL_ISSUE = 2;
    public static final byte DISK_FULL_ISSUE  = 3;
    public static final byte CONFLICT_ISSUE   = 4;
    public static final byte FAILED_ISSUE     = 5;
    public static final byte MULTIPLE_ISSUE   = 9;

    protected byte type;

    public byte getType() {
        return type;
    }

    public abstract String getTitle();

    public String getTitleInPopup() {
        return getTitle();
    }

    // list entry
    public abstract String getContent();

    public abstract String getDescription();

    public String getDescriptionInList() {
        return getDescription() + "\n\n" + DataCenter.getInstance().getString("pvr.pressOK");
    }

    public abstract String getButtonText();

    public boolean equals(Object o) {
        if (o instanceof Issue) {
            Issue issue = (Issue) o;
            return issue.type == this.type;
        }
        return false;
    }

    public String toString() {
        return "Issue_" + type + "[" + getTitle() + "]";
    }

    public void paint(Graphics g, int row, boolean hasFocus, ListPanel p, ListPanelRenderer r) {
        r.paintEntry(g, this, row, hasFocus, p);
    }

}

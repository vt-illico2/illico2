package com.alticast.illico.pvr.issue;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.OcapRecordingRequest;
import java.util.*;

public class ConflictIssue extends Issue implements Comparator {

    RecordingRequest request;
    long targetStartTime;
    long targetEndTime;

    long totalStartTime = Long.MAX_VALUE;
    long totalEndTime = 0;

    Vector recordings = new Vector();

    public ConflictIssue(RecordingRequest req) {
        this.type = CONFLICT_ISSUE;
        request = req;
        targetStartTime = AppData.getScheduledStartTime(request);
        targetEndTime = targetStartTime + AppData.getScheduledDuration(request);

        recordings.addElement(request);

        RecordingListManager rlm = RecordingListManager.getInstance();
        RecordingList list = rlm.getActiveOverlappingList(req);
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                RecordingRequest r = list.getRecordingRequest(i);
                RecordingList subList = rlm.getActiveOverlappingList(r);
                // NOTE - OCAP spec 문제로 state가 맞지 않아 null 이 나오는 경우가 있어서 이렇게 보완
                int subSize = subList != null ? subList.size() : 99;
                if (subSize >= 2) {
                    recordings.addElement(r);
                    try {
                        if (r.getState() == OcapRecordingRequest.PENDING_NO_CONFLICT_STATE) {
                            long start = AppData.getScheduledStartTime(r);
                            totalStartTime = Math.min(totalStartTime, start);
                            totalEndTime = Math.max(totalEndTime, start + AppData.getScheduledDuration(r));
                        }
                    } catch (Exception ex) {
                    }
                }
            }
        }
        Collections.sort(recordings, this);
    }

    public String getTitle() {
        return DataCenter.getInstance().getString("pvr.issue_title_conflict");
    }

    public String getContent() {
        DataCenter dataCenter = DataCenter.getInstance();
        return dataCenter.getString("pvr.issue_msg_conflict") + "  " + Formatter.getCurrent().getShortDate(targetStartTime);
    }

    public String getDescription() {
        return DataCenter.getInstance().getString("pvr.issue_desc_conflict");
    }

    public String getButtonText() {
        return DataCenter.getInstance().getString("pvr.issue_btn_conflict");
    }

    public RecordingRequest[] getRecordings() {
        RecordingRequest[] ret = new RecordingRequest[recordings.size()];
        recordings.copyInto(ret);
        return ret;
    }

    public boolean merge(ConflictIssue issue) {
        if (this.totalStartTime != issue.totalStartTime || this.totalEndTime != issue.totalEndTime) {
            return false;
        }
        Vector vector = issue.recordings;
        int size = vector.size();
        for (int i = 0; i < size; i++) {
            Object o = vector.elementAt(i);
            if (!recordings.contains(o)) {
                recordings.addElement(o);
            }
        }
        return true;
    }

    public int compare(Object o1, Object o2) {
        if (o1 instanceof RecordingRequest && o2 instanceof RecordingRequest) {
            int i1 = AppData.getInteger((RecordingRequest) o1, AppData.CHANNEL_NUMBER);
            int i2 = AppData.getInteger((RecordingRequest) o2, AppData.CHANNEL_NUMBER);
            return i1 - i2;
        }
        return 0;
    }

    public boolean equals(Object o) {
        if (o instanceof ConflictIssue) {
            ConflictIssue issue = (ConflictIssue) o;
            return request.equals(issue.request);
        }
        return false;
    }

}

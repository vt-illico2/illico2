package com.alticast.illico.pvr.issue;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.hn.recording.RecordingContentItem;

public class FailedIssue extends Issue {

    int reason;
    long startTime;
    String number;
    String title;
    AbstractRecording rec;

    public FailedIssue(LeafRecordingRequest req) {
        this(new LocalRecording(req));
    }

    public FailedIssue(RecordingContentItem item) {
        this(new RemoteRecording(item));
    }

    private FailedIssue(AbstractRecording rec) {
        this.rec = rec;
        this.type = FAILED_ISSUE;
        reason = rec.getInteger(AppData.FAILED_REASON);
        startTime = rec.getScheduledStartTime();
        number = rec.getString(AppData.CHANNEL_NUMBER);
        title = rec.getTitle();
    }

    public AbstractRecording getRecording() {
        return rec;
    }

    //->Kenneth[2017.3.29] R7.3 : VDTRMASTER-5703 힌트 얻기 위한 로그 추가
    // 로그를 남기기 위해서 boolean 을 return 하도록 바꿈
    public boolean deleteRequest() {
        try {
            return rec.delete();
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
    }
    /*
    public void deleteRequest() {
        try {
            rec.delete();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }
    */
    //<-

    public int getReason() {
        return reason;
    }

    public String getTitle() {
        return DataCenter.getInstance().getString("pvr.issue_title_failed");
    }

    public String getTitleInPopup() {
        return DataCenter.getInstance().getString(
                reason == RecordManager.CANCELLED ? "pvr.issue_title_popup_cancelled" : "pvr.issue_title_popup_failed");
    }

    public String getContent() {
        return title;
//        return TextUtil.shorten(title, GraphicsResource.FM18, 225) + DataCenter.getInstance().getString("pvr.issue_msg_failed");
    }

    public String getChannelAndDate() {
        return "CH " + number + "  " + Formatter.getCurrent().getShortDate(startTime);
    }

    public String getDescription() {
        DataCenter dataCenter = DataCenter.getInstance();
        String st = dataCenter.getString("fail." + (700 + reason));
        if (st.length() == 0) {
            st = dataCenter.getString("fail.700");
        }
        return st;
    }

    public String getDescriptionInList() {
        return DataCenter.getInstance().getString("pvr.issue_desc_list_failed");
    }

    public String getButtonText() {
        return DataCenter.getInstance().getString("pvr.issue_btn_failed");
    }

    public boolean equals(Object o) {
        if (o instanceof FailedIssue) {
            FailedIssue issue = (FailedIssue) o;
            return rec.equals(issue.rec);
        }
        return false;
    }

}

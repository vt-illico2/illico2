package com.alticast.illico.pvr.issue;

import java.awt.*;
import org.ocap.shared.dvr.RecordingRequest;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.*;

public class ConflictRenderingItem {
    public static final byte KEEP = 0;
    public static final byte DELETE = 1;
    public static final byte SAVE = 2;
    public static final byte RESCHEDULE = 3;

    public static final int DELETE_OPTION_THIS = 0;
    public static final int DELETE_OPTION_ALL = 1;

    public final RecordingRequest request;

    public final long from;
    public final long to;
    public final String title;
    public final String call;
    public Image logo;

    public String shortenTitle;
    public int graphLeft;
    public int graphRight;
    public String time;

    public byte action = KEEP;
    public TvProgram[] otherShowtimes;
    public TvProgram rescheduleTarget;
    public String rescheduleText = "";
    public int deleteOption = DELETE_OPTION_THIS;

    public short stateGroup;
    public String groupKey;

    public ConflictRenderingItem(RecordingRequest r) {
        request = r;
        from = AppData.getScheduledStartTime(r);
        to = from + AppData.getScheduledDuration(r);
        title = ParentalControl.getTitle(r);    //AppData.getString(r, AppData.TITLE);
        call = AppData.getString(r, AppData.CHANNEL_NAME);

        logo = (Image) SharedMemory.getInstance().get("logo." + call);
        if (logo == null) {
            logo = (Image) SharedMemory.getInstance().get("logo.default");
        }
        try {
            stateGroup = RecordingListManager.getStateGroup(r.getState());
        } catch (Exception ex) {
            stateGroup = RecordingListManager.PENDING;
        }
        if (stateGroup == RecordingListManager.PENDING) {
            groupKey = (String) AppData.get(r, AppData.GROUP_KEY);
        }
    }

    public void setRescheduleTarget(TvProgram p) {
        if (p == null) {
            action= KEEP;
            rescheduleText = "";
        } else {
            action = RESCHEDULE;
            rescheduleTarget = p;
            try {
                Formatter f = Formatter.getCurrent();
                long start = p.getStartTime();
                rescheduleText = f.getLongDayText(start) + " " + f.getTime(start) + " - " + f.getTime(p.getEndTime());
            } catch (Exception ex) {
                rescheduleText = "";
            }
        }
    }

    public void prepare(FontMetrics fm, int stringWidth, long min, long max, int x, int width) {
        shortenTitle = TextUtil.shorten(title, fm, stringWidth);
        graphLeft = (int) ((from - min) * width / (max - min)) + x;
        graphRight = (int) ((to - min) * width / (max - min)) + x;

        Formatter formatter = Formatter.getCurrent();
        time = formatter.getTime(from) + " - " + formatter.getTime(to);
    }
    
    //->Kenneth [2015.3.14] 4K
    public static String getActionStr(int actionInt) {
        if (actionInt == KEEP) return "KEEP";
        if (actionInt == DELETE) return "DELETE";
        if (actionInt == SAVE) return "SAVE";
        if (actionInt == RESCHEDULE) return "RESCHEDULE";
        return "UNKNOWN";
    }
}

package com.alticast.illico.pvr.issue;

import java.rmi.RemoteException;
import java.util.*;
import org.ocap.shared.dvr.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.popup.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;

public class DualTunerConflictResolveSession extends ConflictResolveSession {

    private ConflictCautionPopup conflictCautionPopup;
    private ConflictResolvedPopup conflictResolvedPopup;
    private ConflictPickPopup conflictPickPopup;
    private ConflictConfirmPopup conflictConfirmPopup;
    private OtherShowtimesPopup otherShowtimesPopup;
    private DualTunerCancelOptionPopup cancelOptionPopup;

    private HashMap anotherShowtime = new HashMap();

    public DualTunerConflictResolveSession(RecordingRequest request, RecordingRequest[] overlapped) {
        super(request, overlapped);
    }

    // overriden
    protected synchronized boolean showConflictPopup(RecordingRequest req, RecordingRequest[] overlapped) {
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showConflictPopup");
        Core.resetScreenSaverTimer();
        conflictCautionPopup = new ConflictCautionPopup(this);
        conflictCautionPopup.setTargetRecording(req);
        conflictCautionPopup.setConflictRecording(overlapped);
        currentIssue = conflictCautionPopup.getConflicts();

        if (IssueManager.getInstance().checkTooMany(currentIssue)) {
            Log.printWarning("DualTunerConflictResolveSession: don't show conflict popup");
            return false;
        } else {
            startPopup(conflictCautionPopup);
            return true;
        }
    }

    public synchronized void showConfirmPopup(ArrayList selected, ArrayList repeated) {
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showConfirmPopup");
        if (conflictConfirmPopup == null) {
            conflictConfirmPopup = new ConflictConfirmPopup(this);
        }
        conflictConfirmPopup.setConflictRecording(currentIssue);
        conflictConfirmPopup.setWillBeRecordedRecording(selected);
        conflictConfirmPopup.setRepeatedRecording(repeated);
//        conflictConfirmPopup.setOtherShowtime(anotherShowtime);

        startPopup(conflictConfirmPopup);
//        add(conflictConfirmPopup);
//        conflictConfirmPopup.start();
//        current = conflictConfirmPopup;
    }

    public synchronized void showCancelOptionPopup(ArrayList selected, ArrayList repeated) {
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showCancelOptionPopup");
        if (cancelOptionPopup == null) {
            cancelOptionPopup = new DualTunerCancelOptionPopup(this);
        }
        cancelOptionPopup.setWillBeRecordedRecording(selected);
        cancelOptionPopup.setRepeatedRecording(repeated);
        startPopup(cancelOptionPopup);
//        add(cancelOptionPopup);
//        cancelOptionPopup.start();
//        current = cancelOptionPopup;
    }

    public synchronized void showOtherTimesPopup(HashMap otherShowTimes) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showOtherTimesPopup: " + otherShowTimes);
        }
        if (conflictPickPopup == null) {
            conflictPickPopup = new ConflictPickPopup(this);
        }
        conflictPickPopup.setMode(true);
        if (otherShowTimes != null) {
            conflictPickPopup.setOtherShowtimes(otherShowTimes);
        }
        conflictPickPopup.setConflictRecording(currentIssue);
        startPopup(conflictPickPopup);
    }

    public synchronized void showResolvedPopup(boolean resolved) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showResolvedPopup : " + resolved);
        }
        anotherShowtime.clear();
        if (conflictResolvedPopup == null) {
            conflictResolvedPopup = new ConflictResolvedPopup(this);
        }
        conflictResolvedPopup.setMode(resolved);

        startPopup(conflictResolvedPopup);

        currentIssue = null;
    }


    public synchronized void showPickupPopup() {
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showPickupPopup");
        if (conflictPickPopup == null) {
            conflictPickPopup = new ConflictPickPopup(this);
        }
        conflictPickPopup.setMode(false);
        conflictPickPopup.setConflictRecording(currentIssue);

        startPopup(conflictPickPopup);
    }

    public synchronized void showTimetable(RecordingRequest recordingRequest, TvProgram[] programs) {
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.showTimetable");
        if (otherShowtimesPopup == null) {
            otherShowtimesPopup = new OtherShowtimesPopup(this);
        }
        otherShowtimesPopup.setRecordingRequest(recordingRequest);
        otherShowtimesPopup.setPrograms(programs);

        startPopup(otherShowtimesPopup);
    }

    public void showRescheduleConfirmPopup(RecordingRequest r, TvProgram p) {
        startPopup(new RescheduleConfirmPopup(this, r, p));
    }

    public void pickAnotherShowtime(RecordingRequest moved, TvProgram another) {
        anotherShowtime.put(moved, another);
        ArrayList selected = new ArrayList();
        for (int i = 0; i < currentIssue.length; i++) {
            RecordingRequest req = currentIssue[i];
            if (!anotherShowtime.containsKey(req)) {
                selected.add(req);
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.pickAnotherShowtime : selected = "
                                + selected + ", another = " + anotherShowtime);
        }
        int size = selected.size();
        RecordingRequest[] recordings = new RecordingRequest[size];
        if (size > 0) {
            selected.toArray(recordings);
        }
        if (hasConflict(recordings)) {
            showConflictPopup(null, recordings);
        } else {
            resolve(currentIssue, selected, null, null);
//            showConfirmPopup(selected, null, anotherShowtime);
        }
    }

    public synchronized void showConflictPopup() {
        startPopup(conflictCautionPopup);
    }

    public void resolve(RecordingRequest[] conflicts, ArrayList selected, ArrayList repeated, ArrayList toStop) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.resolve : other="
                    + anotherShowtime + ", toStop.size" + (toStop != null ? toStop.size() : -1));
        }
        currentIssue = null;

        String programId = null;
        long otherStart = -1;
        long otherDur = 0;

        int deletedCnt = 0, movedCnt = 0;
        RecordingRequest wasWithError = null;

        final HashSet selectedChannels = new HashSet();

        for (int i = 0; i < conflicts.length; i++) {
            RecordingRequest req = conflicts[i];
            if (selected.contains(req)) {
                if (req.getState() == LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                    wasWithError = req;
                }
                Object o = AppData.get(req, AppData.SOURCE_ID);
                if (o != null) {
                    selectedChannels.add(o);
                }
            } else {    // unselected
                if (anotherShowtime == null || !anotherShowtime.containsKey(req)) { // moved
                    try {
                        if (toStop != null && toStop.contains(conflicts[i])) {
                            ((LeafRecordingRequest) conflicts[i]).stop();
                        } else {
                            boolean result = RecordManager.deleteIfPossible(conflicts[i]);
                            if (!result) {
                                Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.resolve() : Call showCannotDeletePopup()");
                                PopupController.showCannotDeletePopup(conflicts[i], PopupController.getInstance());
                                Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.resolve() : Call LeafRecordingRequest.stop()()");
                                ((LeafRecordingRequest) conflicts[i]).stop();
                            }
                        }
                        deletedCnt++;
                    } catch (AccessDeniedException e) {
                        Log.print(e);
                    } catch (IllegalStateException e) {
                        try {
                            conflicts[i].delete();
                        } catch (Exception ex) {
                        }
                    }
                }
            }
        }

        Map.Entry entry;
        Set set = anotherShowtime.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            entry = (Map.Entry) it.next();
            try {
                RecordManager.getInstance().replace((RecordingRequest) entry.getKey(), (TvProgram) entry.getValue());
                movedCnt++;
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        anotherShowtime.clear();

        if (repeated != null) {
            while (repeated.isEmpty() == false) {
                RecordingRequest rr = (RecordingRequest) repeated.remove(0);
                String groupKey = AppData.getString(rr, AppData.GROUP_KEY);
                RecordingScheduler.getInstance().unset(groupKey);
            }
        }
        final RecordingRequest withError = wasWithError;
        if (withError != null) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.resolve().t.run() : Call changeChannelByRecordingChange()");
                        Core.epgService.changeChannelByRecordingChange(withError.getId(), selectedChannels);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            };
            t.start();
        }
        Log.printDebug(App.LOG_HEADER+"DualTunerConflictResolveSession.resolve() : Call showResolvedPopup(true)");
        showResolvedPopup(true);
    }

}

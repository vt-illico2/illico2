package com.alticast.illico.pvr.issue;

import com.videotron.tvi.illico.framework.*;
import com.alticast.illico.pvr.StorageInfoManager;

public class DiskIssue extends Issue {

    public DiskIssue(byte type) {
        this.type = type;
    }

    public String getTitle() {
        String title = DataCenter.getInstance().getString("pvr.disk_popup_title_" + type);
        if (type < DiskIssue.DISK_FULL_ISSUE) {
            title = title + " " + StorageInfoManager.getInstance().getFreePercent() + "%";
        }
        return title;
    }

    public String getContent() {
        return getTitle();
    }

    public String getDescription() {
        return DataCenter.getInstance().getString("pvr.issue_desc_disk");
    }

    public String getButtonText() {
        return DataCenter.getInstance().getString("pvr.issue_btn_disk");
    }
}

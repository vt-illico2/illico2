package com.alticast.illico.pvr.issue;

public interface IssueChangedListener {

    public void issueChanged(Issue old, Issue current);
}

package com.alticast.illico.pvr.issue;

import java.rmi.RemoteException;
import java.util.*;
import java.awt.EventQueue;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.popup.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;

public abstract class ConflictResolveSession extends UIComponent implements Runnable {

    protected static final Short UI_LOCK = new Short((short) 456);

    protected RecordingRequest req;
    protected RecordingRequest[] overlapped;

    protected RecordingRequest[] currentIssue;

    protected Popup currentPopup;

    protected ConflictResolveSession(RecordingRequest r, RecordingRequest[] overlapped) {
        this.req = r;
        this.overlapped = overlapped;
        setBounds(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
    }

    //->Kenneth[2015.2.14] 4K
    public boolean isUhdConflict = false;
    public static ConflictResolveSession createUhdSession(RecordingRequest r, RecordingRequest[] overlapped) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.createUhdSession("+r+", "+overlapped+")");
        return new ManyTunerConflictResolveSession(r, overlapped, true);
    }

    public static ConflictResolveSession create(RecordingRequest r, RecordingRequest[] overlapped) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.create("+r+", "+overlapped+")");
        if (App.MANY_TUNER) {
            return new ManyTunerConflictResolveSession(r, overlapped);
        } else {
            return new DualTunerConflictResolveSession(r, overlapped);
        }
    }

    public static ConflictResolveSession create(TunerRequestor tr) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.create("+tr+")");
        if (!App.MANY_TUNER) {
            return null;
        }
        RecordingList list = RecordingListManager.getInstance().getInProgressWithoutErrorList();
        if (list == null) {
            return null;
        }
        int size = list.size();
        if (size <= 0) {
            return null;
        }
        RecordingRequest[] array = new RecordingRequest[size];
        for (int i = 0; i < size; i++) {
            array[i] = list.getRecordingRequest(i);
        }
        return new ManyTunerConflictResolveSession(null, array, tr);
    }

    public final void run() {
        boolean shown = false;
        synchronized (this) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.run() : Call IssueManager.startSession(this)");
            IssueManager.getInstance().startSession(this);
            try {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.run() : Call process()");
                shown = process();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        if (shown) {
            synchronized (UI_LOCK) {
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.run() : Before wait()");
                    UI_LOCK.wait();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.run() : After wait()");
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.run() : Call IssueManager.stopSession(this)");
        IssueManager.getInstance().stopSession(this);
    }

    public void notifyConflictResolved() {
        synchronized (UI_LOCK) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.notifyConflictResolved() : Call notifyAll()");
            UI_LOCK.notifyAll();
        }
    }

    public void startPopup(final Popup c) {
        if (EventQueue.isDispatchThread()) {
            startPopupImpl(c);
        } else {
            EventQueue.invokeLater(new Runnable() {
                public void run() {
                    startPopupImpl(c);
                }
            });
        }
    }

    private synchronized void startPopupImpl(Popup c) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.startPopupImpl()");
        if (currentPopup != null) {
            this.remove(currentPopup);
            currentPopup.stop();
        }
        this.add(c);
        c.start();
        c.setVisible(true);
        currentPopup = c;
        repaint();
    }

    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.handleKey() : "+currentPopup);
        if (currentPopup != null) {
            return currentPopup.handleKey(code);
        }
        return false;
    }

    protected boolean process() {
        if (overlapped != null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.process() : Call showConflictPopup()");
            return showConflictPopup(req, overlapped);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("ConflictResolveSession.process = " + AppData.getString(req, AppData.TITLE) + ", " + req);
        }
        if (AppData.getBoolean(req, AppData.UNSET)) {
            Log.printInfo("ConflictResolveSession.process: new recording is to be skipped.");
            return false;
        }
        if (AppData.getBoolean(req, AppData.SUPPRESS_CONFLICT)) {
            Log.printInfo("ConflictResolveSession.process: new recording is from remote.");
//            AppData.remove(req, AppData.SUPPRESS_CONFLICT);
            return false;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.process() : Call showConflictPopup() -- 2");
        return showConflictPopup(req);
    }

    protected static TvProgram findAnotherEpisode(RecordingRequest req, String sid, String pid) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.findAnotherEpisode()");
        String call = (String) AppData.get(req, AppData.CHANNEL_NAME);
        if (call == null) {
            return null;
        }
        RemoteIterator ri = Core.epgService.findEpisodes(call, sid, pid);
        long oldStartTime = AppData.getLong(req, AppData.PROGRAM_START_TIME);

        long buffer = RecordManager.getInstance().getRecordingBuffer();
        ArrayList list = new ArrayList();
        while (ri.hasNext()) {
            TvProgram p = (TvProgram) ri.next();

            long startTime = p.getStartTime();
            if (startTime <= System.currentTimeMillis()) {
                Log.printDebug("ConflictResolveSession.scheduleAnotherEpisode : not a future program = " + p);
            } else if (oldStartTime == startTime) {
                Log.printDebug("ConflictResolveSession.scheduleAnotherEpisode : same program = " + p);
            } else if (checkConflict(startTime-buffer, p.getEndTime()+buffer, Environment.TUNER_COUNT-1)) {
                Log.printInfo("ConflictResolveSession.scheduleAnotherEpisode : conflict = " + p);
            } else {
                return p;
            }
        }
        return null;
    }

    /** 겹치는 시간을 제외한 Other showtimes 를 return */
    public static TvProgram[] getOtherShowtimes(RecordingRequest req, long from) throws RemoteException {
        long buffer = RecordManager.getInstance().getRecordingBuffer();
        EpgService svc = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);

        String callLetter = AppData.getString(req, AppData.CHANNEL_NAME);
        String pid = AppData.getString(req, AppData.PROGRAM_ID);

        RemoteIterator ri = svc.findPrograms(callLetter, pid, null, from, Long.MAX_VALUE);
        ArrayList list = new ArrayList();
        while (ri.hasNext()) {
            TvProgram p = (TvProgram) ri.next();
            if (Log.DEBUG_ON) {
                Log.printDebug("ConflictResolveSession.getOtherShowtimes = " + p);
            }
            long startTime = p.getStartTime();
            if (AppData.getLong(req, AppData.PROGRAM_START_TIME) == startTime) {
                Log.printDebug("ConflictResolveSession.getOtherShowtimes : same start time");
            } else if (checkConflict(startTime-buffer, p.getEndTime()+buffer, Environment.TUNER_COUNT-1)) {
                Log.printDebug("ConflictResolveSession.getOtherShowtimes : cause conflict");
            } else if (RecordManager.getInstance().getRequest(p) != null) {
                Log.printDebug("ConflictResolveSession.getOtherShowtimes : already scheduled");
            } else {
                list.add(p);
            }
        }
        int size = list.size();
        if (size <= 0) {
            return new TvProgram[0];
        }
        TvProgram[] programs = new TvProgram[size];
        list.toArray(programs);
        return programs;
    }

    protected boolean isConflictState(RecordingRequest req) {
        try {
            switch (req.getState()) {
                case LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                case LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.isConflictState() returns true");
                    return true;
            }
        } catch (Exception ex) {
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.isConflictState() returns false");
        return false;
    }

    protected final boolean showConflictPopup(RecordingRequest req) {
        RecordingListManager rlm = RecordingListManager.getInstance();
        RecordingList list = rlm.getActiveOverlappingList(req);
        int size = list == null ? -1 : list.size();
        if (Log.INFO_ON) {
            Log.printInfo(App.LOG_HEADER+"ConflictResolveSession.showConflictPopup: size = " + size);
        }

        if (size < Environment.TUNER_COUNT) {
            // 일어나서는 안되는 경우...
            Log.printWarning("ConflictResolveSession.showConflictPopup: can't find overlapping list. size = " + size);
            return false;
        }
        list = RecordingListManager.sort(list, AppData.CHANNEL_NUMBER);
        Vector vector = new Vector(size);
        boolean existsWithConfState = isConflictState(req);
        for (int i = 0; i < size; i++) {
            RecordingRequest r = list.getRecordingRequest(i);
            RecordingList subList = rlm.getActiveOverlappingList(r);
            // NOTE - OCAP spec 문제로 state가 맞지 않아 null 이 나오는 경우가 있어서 이렇게 보완
            int subSize = subList != null ? subList.size() : 99;
            if (Log.DEBUG_ON) {
                Log.printDebug("ConflictResolveSession.showConflictPopup: overlapping list of sub request[" + i + "] = " + subSize);
            }
            if (subSize >= Environment.TUNER_COUNT) {
                if (!existsWithConfState) {
                    existsWithConfState = isConflictState(r);
                }
                vector.addElement(r);
            }
        }
        if (!existsWithConfState) {
            Log.printWarning("IssueManager.showConflictPopup: no conflict recording !");
            return false;
        }
        size = vector.size();
        if (size < Environment.TUNER_COUNT) {
            // 일어나서는 안되는 경우...
            Log.printWarning("ConflictResolveSession.showConflictPopup: overlapping list is less than 2 = " + size);
            return false;
        }

        RecordingRequest[] overlapped = new RecordingRequest[size];
        vector.copyInto(overlapped);

        Vector movableEpisodes = new Vector();
        Object[] moveParam = checkMoveSeries(req);
        if (moveParam != null) {
            movableEpisodes.addElement(moveParam);
        }

        for (int i = 0; i < overlapped.length && size + 1 - movableEpisodes.size() > Environment.TUNER_COUNT; i++) {
            moveParam = checkMoveSeries(overlapped[i]);
            if (moveParam != null) {
                movableEpisodes.addElement(moveParam);
            }
        }
        int mes = movableEpisodes.size();
        if (size + 1 - mes <= Environment.TUNER_COUNT) {
            int moveCount = size + 1 - Environment.TUNER_COUNT;
            Log.printWarning("ConflictResolveSession: moving " + moveCount + " episodes.");
            for (int i = 0; i < moveCount; i++) {
                Object[] last = (Object[]) movableEpisodes.remove(movableEpisodes.size() - 1);
                try {
                    RecordingScheduler.getInstance().rescheduleSeries(
                        (RecordingRequest) last[0], (String) last[1], (TvProgram) last[2] );
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            return false;
        }
        return showConflictPopup(req, overlapped);
    }

    protected abstract boolean showConflictPopup(RecordingRequest req, RecordingRequest[] overlapped);


    /** returns [RecordingRequest, groupKey, TvProgram] if the series episode is movable. */
    protected Object[] checkMoveSeries(RecordingRequest req) {
        String groupKey = (String) AppData.get(req, AppData.GROUP_KEY);
        String seriesId = (String) AppData.get(req, AppData.SERIES_ID);
        Log.printInfo(App.LOG_HEADER+"ConflictResolveSession.checkMoveSeries: " + groupKey + ", " + seriesId);
        if (groupKey != null && seriesId != null) {
            int cm = AppData.getInteger(req, AppData.CREATION_METHOD);
            if (cm == AppData.BY_USER) {
                return null;
            }
            String programId = (String) AppData.get(req, AppData.PROGRAM_ID);
            Log.printInfo("ConflictResolveSession.checkMoveSeries: program ID = " + programId);
            TvProgram p = null;
            if (programId != null) {
                try {
                    p = findAnotherEpisode(req, seriesId, programId);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (p != null) {
                return new Object[] { req, groupKey, p };
//                try {
//                    RecordingScheduler.getInstance().rescheduleSeries(req, groupKey, p);
//                } catch (Exception ex) {
//                    Log.print(ex);
//                }
//                return true;
            }
        }
        return null;
    }

    public static boolean hasConflict(RecordingRequest[] recordings) {
        return checkConflict(recordings, Environment.TUNER_COUNT);
    }

    public static boolean checkConflict(long from, long to, int max) {
        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(from, to);
        if (list == null) {
            return false;
        }
        RecordingRequest[] array = new RecordingRequest[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.getRecordingRequest(i);
        }
        return checkConflict(array, max);
    }

    public static boolean checkConflict(RecordingRequest[] recordings, int max) {
		ArrayList sort = new ArrayList();
		for (int i = 0; i < recordings.length; i++) {
			RecordingRequest req = recordings[i];
            long t = AppData.getScheduledStartTime(req);
			sort.add(new TimeEvent(t, true));
			sort.add(new TimeEvent(t + AppData.getScheduledDuration(req), false));
		}
		Collections.sort(sort);

        int count = 0;
		for (int i = 0; i < sort.size(); i++) {
			TimeEvent e = (TimeEvent) sort.get(i);
			if (e.start) {
                count++;
                if (count > max) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.checkConflict() returns true");
                    return true;
                }
            } else {
                count--;
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolveSession.checkConflict() returns false");
        return false;
    }

    protected static class TimeEvent implements Comparable {
        long time;
        boolean start;

        public TimeEvent(long time, boolean start) {
            this.time = time;
            this.start = start;
        }

        public int compareTo(Object obj) {
            long ot = ((TimeEvent) obj).time;
            if (time < ot) {
                return -1;
            } else if (time > ot) {
                return 1;
            } else {
                return start ? 1 : -1;
            }
        }
    }

}

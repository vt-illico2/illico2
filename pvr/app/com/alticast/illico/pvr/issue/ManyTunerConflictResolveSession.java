package com.alticast.illico.pvr.issue;

import java.rmi.RemoteException;
import java.util.*;
import org.ocap.shared.dvr.*;

import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.popup.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;

public class ManyTunerConflictResolveSession extends ConflictResolveSession {

    ManyConflictCautionPopup conflictCautionPopup;
    TunerRequestor tunerRequestor;

    RecordingRequest cancelTarget;

    long sessionStartTime = System.currentTimeMillis();

    //->Kenneth[2015.2.14] 4K
    protected ManyTunerConflictResolveSession(RecordingRequest request, RecordingRequest[] overlapped, boolean isUhd) {
        this(request, overlapped, null);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession("+request+", "+overlapped+", "+isUhd+")");
        isUhdConflict = isUhd;
    }

    protected ManyTunerConflictResolveSession(RecordingRequest request, RecordingRequest[] overlapped) {
        this(request, overlapped, null);
    }

    protected ManyTunerConflictResolveSession(RecordingRequest request, RecordingRequest[] overlapped, TunerRequestor tr) {
        super(request, overlapped);
        this.tunerRequestor = tr;
        this.cancelTarget = this.req;
    }

    public TunerRequestor getTunerRequestor() {
        return tunerRequestor;
    }

    // overriden
    public void notifyConflictResolved() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved()");
        if (tunerRequestor != null) {
            synchronized (tunerRequestor) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved() : Call tunerRequestor.notifyAll()");
                tunerRequestor.notifyAll();
            }
        }
        //->Kenneth[2015.3.20] : 4K
        // UHD 인 경우 다음과 같은 문제가 발생함.
        // Contention Resolver 팝업이 뜬 상태에서 가만 놔두면 하나의 레코딩이 끝남.
        // 그러면 COMPLETE_STATE 가 발생하면서 notifyConflictResolved() 가 불리면서 팝업 사라짐.
        // 레코딩 리스트에 가면 conflict 팝업을 뜨게 만든 레코딩도 잘 레코딩 되어 있음.
        // 따라서 여기에 추가하는 코드는
        // 1. uhd conflict 인 경우
        // 2. notifyConflictResolved 가 불릴 때
        // 3. currentIssue 가 null 이 아니면 (null 인 경우는 정상적으로 사용자가 contention resolve 하면 null 이 됨)
        // 4. cancelTarget 이 null 이 아니면 얘를 delete 함.
        try {
            printRecordings();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved() : isUhdConflict = "+isUhdConflict);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved() : currentIssue = "+currentIssue);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved() : cancelTarget = "+cancelTarget);
            if (isUhdConflict && currentIssue != null && cancelTarget != null) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.notifyConflictResolved() : Call cancelTarget.delete()!!!");
                cancelTarget.delete();
            }
        } catch (Exception e) {
            Log.print(e);
        }

        super.notifyConflictResolved();
    }

    //->Kenneth[2015.3.20] 로그 추가
    private void printRecordings() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.printRecordings()");
        if (currentIssue == null) return;
        try {
            if (Log.DEBUG_ON) Log.printDebug("############################################################################");
            if (Log.DEBUG_ON) Log.printDebug("##");
            for (int i = 0 ; i < currentIssue.length ; i++) {
                int chNumber = AppData.getInteger(currentIssue[i], AppData.CHANNEL_NUMBER);
                String chName = AppData.getString(currentIssue[i], AppData.CHANNEL_NAME);
                String title = AppData.getString(currentIssue[i], AppData.TITLE);
                String state = RecordManager.getStateString(currentIssue[i].getState());
                if (Log.DEBUG_ON) Log.printDebug("## "+chNumber+" : "+chName+" : "+title+" : "+state);
            }
            if (Log.DEBUG_ON) Log.printDebug("##");
            if (Log.DEBUG_ON) Log.printDebug("############################################################################");
        } catch (Exception e) {
            Log.print(e);
        }
    }

    // overriden
    protected boolean process() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.process()");
        if (tunerRequestor == null) {
            // scheduled conflict
            return super.process();
        }
        // request tuner
        BottomPopup p = new BottomPopup(268);
        p.footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");

        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int focus) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.process() : Call showConflictPopup()");
                showConflictPopup(req, overlapped);
            }

            public void canceled() {
                tunerRequestor.result = false;
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.process() : Call notifyConflictResolved()");
                notifyConflictResolved();
            }

        };
        p.setListener(popupListener);
        DataCenter dataCenter = DataCenter.getInstance();
        String msgKey = (tunerRequestor.type == TunerRequestor.MANY_TUNER_BY_CHANNEL)
                        ? "pvr.stb_too_busy_msg_channel" : "pvr.stb_too_busy_msg_vod";
        p.texts = TextUtil.split(dataCenter.getString(msgKey), BottomPopupRenderer.fmText, BottomPopupRenderer.MAX_TEXT_WIDTH, "|");
        p.title = dataCenter.getString("pvr.stb_too_busy_title");
        p.iconName = "icon_noti_or.png";
        p.buttons = new String[] { dataCenter.getString("pvr.stb_too_busy_btn") };
        startPopup(p);
        return true;
    }

    // overriden
    protected synchronized boolean showConflictPopup(RecordingRequest r, RecordingRequest[] others) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showConflictPopup("+r+", "+others+")");
        Core.resetScreenSaverTimer();
        cancelTarget = r;

        if (r != null && others != null) {
            ArrayList list = new ArrayList(others.length + 2);
            long latestTime = 0L;
            int index = -1;
            list.add(r);
            long time = AppData.getLong(r, AppData.CREATION_TIME);
            if (time > latestTime) {
                latestTime = time;
                index = 0;
            }
            for (int i = 0; i < others.length; i++) {
                list.add(others[i]);
                time = AppData.getLong(others[i], AppData.CREATION_TIME);
                if (time > latestTime) {
                    latestTime = time;
                    index = i + 1;
                }
            }
            if (Log.INFO_ON) {
                Log.printInfo("ManyTunerConflictResolveSession: target index = " + index + ", time = " + new Date(latestTime));
            }
            if (index >= 0) {
                this.cancelTarget = (RecordingRequest) list.remove(index);
                RecordingRequest[] newOverlapped = new RecordingRequest[others.length];
                newOverlapped = (RecordingRequest[]) list.toArray(newOverlapped);
                others = newOverlapped;
            }
        }

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showConflictPopup() : create ManyConflictCautionPopup");
        conflictCautionPopup = new ManyConflictCautionPopup(this);
        currentIssue = conflictCautionPopup.setRecordings(cancelTarget, others);
        if (conflictCautionPopup.getDate() <= 0 && conflictCautionPopup.firstValid != null) {
            currentIssue = null;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showConflictPopup() : Call IssueManager.processNewConflict(firstValid)");
            IssueManager.getInstance().processNewConfilct(conflictCautionPopup.firstValid);
            return false;
        }

        if (IssueManager.getInstance().checkTooMany(currentIssue)) {
            Log.printWarning("ManyTunerConflictResolveSession: don't show conflict popup");
            return false;
        } else {
            startPopup(conflictCautionPopup);
            return true;
        }
    }

    public synchronized void showOtherShowTimesPopup(ConflictRenderingItem cri) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showOtherShowTimesPopup()");
        OtherShowtimesPopup otherShowtimesPopup = new OtherShowtimesPopup(this);
        otherShowtimesPopup.setConflictRenderingItem(cri);
        startPopup(otherShowtimesPopup);
    }

    public synchronized void showCancelOptionPopup(ConflictRenderingItem cri) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCancelOptionPopup()");
        ManyTunerCancelOptionPopup cancelPopup = new ManyTunerCancelOptionPopup(this, cri);
        startPopup(cancelPopup);
    }

    public synchronized void showCurrentConflictPopup() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCurrentConflictPopup()");
        conflictCautionPopup.updateConflictRanges();
        startPopup(conflictCautionPopup);
    }

    public synchronized void showResolvedPopup(ArrayList items) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showResolvedPopup()");
        currentIssue = null;
        if (tunerRequestor == null) {
            // scheduled recording
            ConflictResolvedPopup conflictResolvedPopup = new ConflictResolvedPopup(this);
            conflictResolvedPopup.setMode(true);
            startPopup(conflictResolvedPopup);
        } else {
            int saved = 0;
            StringBuffer sb = new StringBuffer();
            int size = items.size();
            for (int i = 0; i < size; i++) {
                ConflictRenderingItem cri = (ConflictRenderingItem) items.get(i);
                if (cri.action != ConflictRenderingItem.KEEP) {
                    if (saved > 0) {
                        sb.append(',');
                        sb.append(' ');
                    }
                    sb.append(cri.title);
                    saved++;
                }
            }
            // request tuner
            BottomPopup p = new BottomPopup(340);
            p.setTextYPosition(465);
            DataCenter dataCenter = DataCenter.getInstance();
            p.title = dataCenter.getString("pvr.popup_title_resolved");
            p.iconName = "icon_g_check.png";
            String msgKey = (saved > 1) ? "pvr.conflict_resolved_tuner_msg_n" : "pvr.conflict_resolved_tuner_msg_1";
            p.texts = TextUtil.split(TextUtil.replace(dataCenter.getString(msgKey), "%%", sb.toString()),
                                     BottomPopupRenderer.fmText, BottomPopupRenderer.MAX_TEXT_WIDTH, "|");
            PopupAdapter popupListener = new PopupAdapter() {
                public boolean consumeKey(int code) {
                    return true;
                }
                public void canceled() {
                    tunerRequestor.result = true;
                    notifyConflictResolved();
                }

            };
            p.setListener(popupListener);
            p.setAutoClose(3000L);
            startPopup(p);
        }
        ResolveJob job = new ResolveJob(items);
        job.start();
    }

    public synchronized void showCanceledPopup() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup()");
        currentIssue = null;
        if (tunerRequestor != null) {
            tunerRequestor.result = false;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Call notifyConflictResolved");
            notifyConflictResolved();
            return;
        }
        ConflictResolvedPopup conflictResolvedPopup = new ConflictResolvedPopup(this);
        conflictResolvedPopup.setMode(false);
        startPopup(conflictResolvedPopup);
        Thread t = new Thread("Conflit.cancel") {
            public void run() {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Conflict.cancel-Thread.run()");
                RecordingRequest r = cancelTarget != null ? cancelTarget : req;
                if (r != null) {
                    boolean restored = ListEditPopup.restoreIfModified(r, sessionStartTime);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Conflict.cancel-Thread.run() : restored = "+restored);
                    if (restored) {
                        return;
                    }
                    int creationMethod = AppData.getInteger(r, AppData.CREATION_METHOD);
//                    if (creationMethod == AppData.RESCHEDULED) {
//                        long creationTime = AppData.getLong(r, AppData.CREATION_TIME);
//                        if (sessionStartTime - creationTime < Constants.MS_PER_MINUTE) {
//                            Log.printInfo("ManyTunerConflictResolveSession: rescheduled recording will be restored");
//                            RecordingRequest newReq = RecordManager.getInstance().restore(r);
//                            if (newReq != null) {
//                                return;
//                            }
//                        }
//                    }
                    Object groupKey = AppData.get(r, AppData.GROUP_KEY);
                    Log.printDebug("ManyTunerConflictResolveSession.cancel: groupKey = " + groupKey);
                    if (groupKey != null) {
                        if (creationMethod == AppData.BY_USER || creationMethod == AppData.NEW_SERIES) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Conflict.cancel-Thread.run() : Call unset()");
                            RecordingScheduler.getInstance().unset(groupKey.toString());
                        } else {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Conflict.cancel-Thread.run() : Call remove()");
                            RecordingScheduler.getInstance().remove(groupKey.toString(),
                                        AppData.getString(r, AppData.CHANNEL_NAME), AppData.getScheduledStartTime(r));
                        }
                    }
                    try {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.showCanceledPopup() : Conflict.cancel-Thread.run() : Call r.delete()");
                        r.delete();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            }
        };
        t.start();
    }

    class ResolveJob extends Thread {
        ArrayList list;

        public ResolveJob(ArrayList items) {
            super("Conflit.resolve");
            this.list = items;
        }

        public void run() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run()");
            HashSet toUnset = new HashSet();
            RecordingRequest wasWithError = null;
            HashSet selectedChannels = new HashSet();

            TvChannel channelByAnotherCC = null;
            if (tunerRequestor != null) {
                int anotherVideoIndex = 1 - tunerRequestor.videoIndex; // 0 or 1
                try {
                    ChannelContext cc = Core.epgService.getChannelContext(anotherVideoIndex);
                    if (cc.getState() != ChannelContext.STOPPED) {
                        channelByAnotherCC = cc.getCurrentChannel();
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            int onlyStoppedSourceId = channelByAnotherCC == null ? -1 : 0;

            int size = list.size();
            for (int i = 0; i < size; i++) {
                try {
                    ConflictRenderingItem cri = (ConflictRenderingItem) list.get(i);
                    RecordingRequest req = cri.request;
                    switch (cri.action) {
                        case ConflictRenderingItem.KEEP:
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : KEEP");
                            if (req.getState() == LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                                wasWithError = req;
                            }
                            Object o = AppData.get(req, AppData.SOURCE_ID);
                            if (o != null) {
                                selectedChannels.add(o);
                            }
                            break;

                        case ConflictRenderingItem.RESCHEDULE:
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : RESCHEDULE");
                            RecordManager.getInstance().replace(req, cri.rescheduleTarget);
                            break;

                        case ConflictRenderingItem.SAVE:
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : SAVE");
                            if (onlyStoppedSourceId != -1) {
                                if (onlyStoppedSourceId != 0) {
                                    onlyStoppedSourceId = -1;
                                } else {
                                    onlyStoppedSourceId = AppData.getInteger(req, AppData.SOURCE_ID);
                                }
                            }
                            AppData.set(req, AppData.UNSET, Boolean.TRUE);
                            try {
                                LeafRecordingRequest l = (LeafRecordingRequest) req;
                                RecordedService s = l.getService();
                                if (s != null) {
                                    if (Log.DEBUG_ON)
                                    Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : SAVE : Call r.stop()");
                                    l.stop();
                                } else {
                                    Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : SAVE : Call r.delete()");
                                    l.delete();
                                }
                            } catch (IllegalStateException ex) {
                                try {
                                    req.delete();
                                } catch (Exception exc) {
                                }
                            }
                            break;

                        case ConflictRenderingItem.DELETE:
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : DELETE");
                            try {
                                if (Log.DEBUG_ON)
                                    Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : DELETE : Call RecordManager.deleteIfPossible(req)");
                                boolean result = RecordManager.deleteIfPossible(req);
                                if (!result) {
                                    Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call showCannotDeletePopup()");
                                    PopupController.showCannotDeletePopup(req, PopupController.getInstance());
                                    Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call req.stop()");
                                    ((LeafRecordingRequest) req).stop();
                                }
                            } catch (Exception ex) {
                                Log.print(ex);
                            }
                            if (cri.deleteOption == ConflictRenderingItem.DELETE_OPTION_ALL) {
                                if (cri.groupKey != null) {
                                    toUnset.add(cri.groupKey);
                                }
                            }
                            break;
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }

            if (tunerRequestor != null) {
                tunerRequestor.result = true;
                TvChannel channelToChangeFromAnother = null;
                try {
                    if (onlyStoppedSourceId > 0 && channelByAnotherCC != null && channelByAnotherCC.getSourceId() == onlyStoppedSourceId) {
                        ChannelContext targetCC = Core.epgService.getChannelContext(tunerRequestor.videoIndex);
                        channelToChangeFromAnother = targetCC.getCurrentChannel();
                        ChannelContext anotherCC = Core.epgService.getChannelContext(1 - tunerRequestor.videoIndex);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call anotherCC.stopChannel()");
                        anotherCC.stopChannel();
                        Environment.getServiceContext(tunerRequestor.videoIndex).stop();
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
                synchronized (tunerRequestor) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call tonerRequestor.notifyAll()");
                    tunerRequestor.notifyAll();
                }
                if (channelToChangeFromAnother != null) {
                    try {
                        ChannelContext anotherCC = Core.epgService.getChannelContext(1 - tunerRequestor.videoIndex);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call anotherCC.changeChannel()");
                        anotherCC.changeChannel(channelToChangeFromAnother);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }

            } else if (wasWithError != null) {
                // with_error 였던 inprogress recording을 선택한 경우 그 채널을 선택해서 recording이 시작하도록 함
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call changeChannelByRecordingChange()");
                    Core.epgService.changeChannelByRecordingChange(wasWithError.getId(), selectedChannels);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }

            // delete all 한 series를 모두 삭제
            Iterator it = toUnset.iterator();
            while (it.hasNext()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.ResolveJob.run() : Call RecordingScheduler.unset()");
                RecordingScheduler.getInstance().unset((String) it.next());
            }
        }
    }


    public static ArrayList getConflictRanges(ArrayList conflictRenderingItems, int max) {
        int size = conflictRenderingItems.size();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.getConflictRanges() : size = "+size+", max = "+max);
		ArrayList sort = new ArrayList();
		for (int i = 0; i < size; i++) {
			ConflictRenderingItem cri = (ConflictRenderingItem) conflictRenderingItems.get(i);
            if (cri.action == ConflictRenderingItem.KEEP) {
                sort.add(new TimeEvent(cri.from, true));
                sort.add(new TimeEvent(cri.to, false));
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ManyTunerConflictResolveSession.getConflictRanges() : "+i+" action = "+ConflictRenderingItem.getActionStr(cri.action));
		}
		Collections.sort(sort);

        ArrayList ret = new ArrayList();

        long conflictStartTime = 0;
        int count = 0;
        size = sort.size();
		for (int i = 0; i < size; i++) {
			TimeEvent e = (TimeEvent) sort.get(i);
			if (e.start) {
                count++;
                if (count > max && conflictStartTime == 0) {
                    conflictStartTime = e.time;
                }
            } else {
                count--;
                if (count == max) {
                    ret.add(new long[] { conflictStartTime, e.time });
                    conflictStartTime = 0;
                }
            }
        }
        return ret;
    }



}

package com.alticast.illico.pvr.issue;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import java.util.*;
import com.videotron.tvi.illico.log.Log;

public class MultipleIssue extends Issue {

    Vector issues;

    public MultipleIssue(Vector issues) {
        this.type = MULTIPLE_ISSUE;
        this.issues = issues;
    }

    public int getIssueCount() {
        return issues.size();
    }

    public Issue getIssue(int i) {
        try {
            return (Issue) issues.elementAt(i);
        } catch (Exception ex) {
            return null;
        }
    }

    public void remove(Issue issue) {
        try {
            issues.remove(issue);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public String getTitle() {
        DataCenter dataCenter = DataCenter.getInstance();
        return dataCenter.getString("pvr.issue_title_multi_1") + " " + getIssueCount() + " "
             + dataCenter.getString("pvr.issue_title_multi_2");
    }

    public String getContent() {
        int count = getIssueCount();
        DataCenter dataCenter = DataCenter.getInstance();
        if (count == 0) {
            return dataCenter.getString("pvr.issue_msg_multi_0");
        } else {
            return dataCenter.getString("pvr.issue_msg_multi_1") + " " + count + " "
                 + dataCenter.getString("pvr.issue_msg_multi_2");
        }
    }

    public String getDescription() {
        return DataCenter.getInstance().getString("pvr.issue_desc_multi");
    }

    public String getDescriptionInList() {
        return getDescription();
    }

    public String getButtonText() {
        return "";
    }

    public boolean equals(Object o) {
        if (o instanceof MultipleIssue) {
            MultipleIssue issue = (MultipleIssue) o;
            if (issue.getIssueCount() != this.getIssueCount()) {
                return false;
            } else {
                // TODO - 개수만 같으면 안바뀐다고 나오는데?
                return true;
            }
        }
        return super.equals(o);
    }

}

package com.alticast.illico.pvr;

import com.alticast.illico.pvr.popup.Popup;

public interface PopupContainer {

    public void showPopup(Popup p);

    public void hidePopup(Popup p);

}

package com.alticast.illico.pvr;

import java.awt.Point;
import java.rmi.RemoteException;
import java.util.Hashtable;
import javax.tv.xlet.XletContext;
import javax.tv.service.selection.ServiceContext;
import org.ocap.hardware.Host;
import com.alticast.illico.pvr.hn.HomeNetManager;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.stc.*;
import com.videotron.tvi.illico.ixc.errormessage.*;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.CommunicationManager;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;
//->Kenneth[2015.6.24] DDC-107
import com.videotron.tvi.illico.util.VideoOutputUtil;
import com.videotron.tvi.illico.util.VideoOutputListener;
//<-
//->Kenneth[2015.7.6] Tank
import com.videotron.tvi.illico.ixc.mainmenu.*;
//<-

/**
 * This class is the main class of PVR application.
 *
 * @author June Park
 */
public final class Core implements DataUpdateListener, ServiceStateListener, SearchActionEventListener, ScreenSaverConfirmationListener {

    /** XletContext. */
    private XletContext xletContext;

    public static EpgService epgService;
    public static MonitorService monitorService;
    static NotificationService notificationService;

    TunerController[] tunerControllers = new TunerController[2];
    TunerController mainController;
    TunerController pipController;

    private int monitorState;
    public static boolean standAloneMode = false;
    public static boolean hasPvrPermission = Environment.SUPPORT_DVR;

    private LogLevelAdapter logListener = new LogLevelAdapter();
    private VideoContextAdapter videoContextListener = new VideoContextAdapter();

    /** singleton instance. */
    private static Core instance = new Core();

    public static boolean started = false;

    private String videoContext = "";

    //->Kenneth[2015.7.6] Tank
    public static MainMenuService mainMenuService;
    //<-

    //->Kenneth[2016.3.8] R7
    public static boolean isChannelGrouped = true;
    //<-

    private int yearShift = 0;

    /**
     * Returns the singleton instance of EpgCore.
     * @return EpgCore instance.
     */
    public static Core getInstance() {
        return instance;
    }

    /** Constructor. */
    private Core() {
    }

    /** Called when application's initXlet. */
    public synchronized void init(XletContext context) {
        if (Log.DEBUG_ON) {
            Log.printDebug("");
            Log.printDebug("=======================");
            Log.printDebug("Start Core.init()");
            Log.printDebug("=======================");
        }
        this.xletContext = context;

        RecordManager.getInstance().init();
        ParentalControl.getInstance().init();

        TunerResourceManager.getInstance().init();
        CommunicationManager.getInstance().start(xletContext, this);

        StorageInfoManager.getInstance().init();
        RecordingScheduler.getInstance().init();

        PvrServiceImpl.getInstance().start(xletContext);

        mainController = VideoPlayer.getInstance();
        pipController = new TunerController();
        mainController.logPrefix = "TunerController[0]";
        pipController.logPrefix = "TunerController[1]";
        tunerControllers[0] = mainController;
        tunerControllers[1] = pipController;
        for (int i = 0; i < tunerControllers.length; i++) {
            tunerControllers[i].init(i);
        }

        FlipBarWindow.getInstance().init();
        MenuController.getInstance().init();
        PowerModeHandler.getInstance().init();

        IssueManager.getInstance().init();

        HomeNetManager.getInstance().init();

        Thread t = new Thread("PVR.init") {
            public void run() {
                RecordingListManager.getInstance().init();
                RecordedList.getInstance().init();
                UpcomingList.getInstance().init();
                PopupController.getInstance();
            }
        };
        t.start();

        if (Log.DEBUG_ON) {
            Log.printDebug("=======================");
            Log.printDebug("End Core.init()");
            Log.printDebug("=======================");
            Log.printDebug("");
        }
    }

    public synchronized void start() {
        if (Log.DEBUG_ON) {
            Log.printDebug("");
            Log.printDebug("=======================");
            Log.printDebug("Start Core.start()");
            Log.printDebug("=======================");
        }
        started = true;
        MenuController.getInstance().start();
        // PreferenceProxy.getInstance().addPreferenceListener();

        SearchService searchService = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
        try {
            searchService.addSearchActionEventListener(FrameworkMain.getInstance().getApplicationName(), this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        MenuController.currentDate = Formatter.getCurrent().getLongDate();

        String[] param = null;
        try {
            monitorService.addScreenSaverConfirmListener(this, FrameworkMain.getInstance().getApplicationName());
            param = monitorService.getParameter(FrameworkMain.getInstance().getApplicationName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // param[0] = appName param[1] = PVR_Page_Number param[2] = search index
        String parent = null;
        int firstMenuPage;
        if (param != null && param.length > 0) {
            if (Log.DEBUG_ON) {
                for (int i = 0; i < param.length; i++) {
                    Log.printInfo("Core.start: param[" + i + "] = " + param[i]);
                }
            }
            parent = param[0];
            if (MonitorService.REQUEST_APPLICATION_LAST_KEY.equals(parent)) {
                if (MenuController.lastScene == -1 || VideoPlayer.getInstance().lastRecording != null) {
                    firstMenuPage = PvrService.MENU_PLAY_RECORDING;
                } else {
                    firstMenuPage = MenuController.lastScene;
                }

            //->Kenneth[2015.6.29] Tank
            } else if ("Menu".equals(parent) && param.length > 2 && param[1] != null && "FROM_PORTAL".equals(param[1])) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.start() : FROM_PORTAL");
                int param2 = 0;
                try {
                    param2 = Integer.parseInt(param[2]);
                } catch (Exception e) {
                    Log.print(e);
                }
                firstMenuPage = param2;
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.start() : FROM_PORTAL : firstMenuPage = "+firstMenuPage);
            //<-
            } else if ("PLAY".equals(parent)) {
                AbstractRecording rec = null;
                String udn = null;
                try {
                    udn = param[3];
                } catch (Exception ex) {
                }
                try {
                    rec = AbstractRecording.find(Integer.parseInt(param[1]), udn);
                } catch (Exception ex) {
                }
                int startPoint = 0;
                try {
                    startPoint = Integer.parseInt(param[2]);
                } catch (Exception ex) {
                }
                if (rec == null) {
                    firstMenuPage = PvrService.MENU_RECORDING_LIST;
                } else {
                    firstMenuPage = PvrService.MENU_PLAY_RECORDING;
                    MenuController.targetRecording = rec;
                    MenuController.targetStartPoint = startPoint;
                }
            } else if (NotificationService.IXC_NAME.equals(param[0])) {
                try {
                    firstMenuPage = Integer.parseInt(param[1]);
                } catch (Exception ex) {
                    firstMenuPage = PvrService.MENU_PVR_PORTAL;
                }
            } else if (MonitorService.REQUEST_APPLICATION_HOT_KEY.equals(parent)) {
                firstMenuPage = PvrService.MENU_RECORDING_LIST;
            } else if (param.length > 1) {
                if ("Search".equals(parent) && param.length > 2) {
                    ListPanel.requestId = "" + param[2];
                }
                int page = 0;
                try {
                    page = Integer.parseInt(param[1]);
                } catch (Exception ex) {
                }
                firstMenuPage = page;
            } else {
                firstMenuPage = PvrService.MENU_PVR_PORTAL;
            }
        } else {
            int state = 0;
            try {
                state = monitorService.getSTBMode();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

            standAloneMode = state == MonitorService.STB_MODE_STANDALONE;

            if (standAloneMode) {
                firstMenuPage = PvrService.MENU_RECORDING_LIST;
            } else {
                firstMenuPage = PvrService.MENU_PVR_PORTAL;
            }
        }
        if (!Environment.SUPPORT_DVR && firstMenuPage != PvrService.MENU_PLAY_RECORDING) {
            firstMenuPage = PvrService.MENU_RECORDING_LIST;
        }
        if (App.R3_TARGET) {
            if (firstMenuPage != PvrService.MENU_PVR_PORTAL) {
                try {
                    Core.epgService.getChannelContext(0).stopChannel();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }

        MenuController.getInstance().showMenu(firstMenuPage);
        Clock.getInstance().addClockListener(MenuController.getInstance());
        if (parent != null) {
            PvrVbmController.getInstance().writeParentApp(parent);
        }
        PvrVbmController.getInstance().writeApplicationStart();
        if (Log.DEBUG_ON) {
            Log.printDebug("=======================");
            Log.printDebug("End Core.start()");
            Log.printDebug("=======================");
            Log.printDebug("");
        }
    }

    /** Called when application's pauseXlet. */
    public synchronized void pause() {
        if (Log.DEBUG_ON) {
            Log.printDebug("");
            Log.printDebug("=======================");
            Log.printDebug("Start Core.pause()");
            Log.printDebug("=======================");
        }
        started = false;
        try {
            VideoPlayer.getInstance().pauseXlet();
        } catch (Throwable e) {
            Log.print(e);
        }

        try {
            PreferenceProxy.getInstance().ratingHash.clear();
            ParentalControl.getInstance().pause();
        } catch (Throwable e) {
            Log.print(e);
        }
        removeScreenSaver();

        try {
            SearchService searchService = (SearchService) DataCenter.getInstance().get(SearchService.IXC_NAME);
            if (searchService != null) {
                searchService.removeSearchAcitonEventListener(SearchService.PLATFORM_PVR);
                searchService.stopSearchApplication();
            }
        } catch (Throwable e1) {
            Log.print(e1);
        }

//        if (!App.R3_TARGET) {
//            Object ob1 = DataCenter.getInstance().get(EpgService.IXC_NAME);
//            if (ob1 != null) {
//                EpgService epgService = (EpgService) ob1;
//                try {
//                    epgService.changeChannel(-1);
//                } catch (Throwable e) {
//                    Log.print(e);
//                }
//            }
//        }

        try {
            PopupController.getInstance().currentSceneStop();
        } catch (Throwable e) {
            Log.print(e);
        }
        try {
            MenuController.getInstance().pause();
        } catch (Throwable e) {
            Log.print(e);
        }
        try {
            RecordedList.getInstance().pause();
            UpcomingList.getInstance().pause();
            MenuController.getInstance().resizeScaledVideo(0, 0, 960, 540);
            Clock.getInstance().removeClockListener(MenuController.getInstance());
            FrameworkMain.getInstance().getImagePool().clear();
            IssueManager.getInstance().pause();
        } catch (Throwable e) {
            Log.print(e);
        }
        PvrVbmController.getInstance().writeApplicationExit();
        if (Log.DEBUG_ON) {
            Log.printDebug("=======================");
            Log.printDebug("End Core.pause()");
            Log.printDebug("=======================");
            Log.printDebug("");
        }
    }

    public int getYearShift() {
        if (Log.INFO_ON) Log.printInfo("getYearShift() : current = "+yearShift);
        if (yearShift > 0) return yearShift;
        try {
            yearShift = monitorService.getYearShift();
            if (Log.INFO_ON) Log.printInfo("monitor's yearShift = "+yearShift);
        } catch (Exception e) {
            yearShift = 0;
            Log.print(e);
        }
        return yearShift;
    }

    private void removeScreenSaver() {
        try {
            monitorService.removeScreenSaverConfirmListener(this, FrameworkMain.getInstance().getApplicationName());
        } catch (Throwable e) {
            Log.print(e);
        }
    }

    public void exitToChannel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.exitToChannel()");
        try {
            monitorService.exitToChannel();
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public MonitorService getMonitorService() {
        return monitorService;
    }

    /** Called when application's destroyXlet. */
    public void destroy() {
        FlipBarWindow.getInstance().dispose();

        IssueManager.getInstance().destroy();

        PreferenceProxy.getInstance().removePreferenceListener();
    }

    /** ServiceStateListener. */
    public void stateChanged(int newMonitorState) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"Core: (monitor)stateChanged : " + newMonitorState);
        }
        this.monitorState = newMonitorState;

        if (monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
            PopupController.getInstance().currentSceneStop();
        }
        VideoPlayer.getInstance().stateChanged(newMonitorState);
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"Core: (monitor)stateChanged __ end: ");
        }
    }

    public int getMonitorState() {
        return monitorState;
    }

    public TunerController getTunerController(int index) {
        return tunerControllers[index];
    }

    public TunerController getTunerController(TvChannel channel) {
        if (channel == null) {
            return null;
        }
        try {
            for (int i = 0; i < tunerControllers.length; i++) {
                if (channel.equals(tunerControllers[i].getChannel())) {
                    return tunerControllers[i];
                }
            }
        } catch (Exception ex) {
        }
        return null;
    }

    public void dataUpdated(String key, Object old, Object value) {

        Log.printDebug("key = " + key + " value = " + value);

        if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                monitorService.addInbandDataListener(ConfigManager.getInstance(), appName);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                monitorService.addStateListener(this, appName);
                int state = monitorService.getSTBMode();
                Log.printDebug("standAloneMode = " + state);
                standAloneMode = state == MonitorService.STB_MODE_STANDALONE ? true : false;
            } catch (RemoteException ex) {
                Log.print(ex);
            }

            try {
                this.monitorState = monitorService.getState();
            } catch (RemoteException ex) {
                Log.print(ex);
            }


            try {
                monitorService.addVideoContextListener(videoContextListener, App.NAME);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            try {
                videoContextListener.videoModeChanged(monitorService.getVideoContextName());
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        } else if (key.equals(EpgService.IXC_NAME)) {
            epgService = (EpgService) value;
            try {
                epgService.addEpgDataListener(RecordingUpdater.getInstance());
                epgService.addEpgDataListener(RecordingScheduler.getInstance());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            // try {
            // epgService.addChannelEventListener(VideoPlayer.getInstance());
            // } catch (RemoteException ex) {
            // Log.print(ex);
            // }
        } else if (key.equals(PreferenceService.IXC_NAME)) {
            PreferenceProxy.getInstance().addPreferenceListener();
        } else if (key.equals(NotificationService.IXC_NAME)) {
            notificationService = (NotificationService) value;
            String appName = FrameworkMain.getInstance().getApplicationName();
            try {
                notificationService.setRegisterNotificaiton(appName, new NotificationListenerImpl());
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        } else if (key.equals(LoadingAnimationService.IXC_NAME)) {
            LoadingAnimationService service = (LoadingAnimationService) value;

        } else if (key.equals(VbmService.IXC_NAME)) {
            PvrVbmController.getInstance().init((VbmService) value);

        } else if (key.equals(StcService.IXC_NAME)) {
            StcService stcService = (StcService) value;
            try {
                int currentLevel = stcService.registerApp(App.NAME);
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + App.NAME);
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(App.NAME, logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        } else if (key.equals(DaemonService.IXC_NAME)) {
            DaemonService daemonService = (DaemonService) value;
            try {
                daemonService.addListener(App.NAME, RemotePvrHandler.getInstance());
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        //->Kenneth[2015.7.6] Tank
        } else if (key.equals(MainMenuService.IXC_NAME)) {
            mainMenuService = (MainMenuService) value;
        //<-
        }
        // use one time
        DataCenter.getInstance().removeDataUpdateListener(key, this);
    }

    public void dataRemoved(String key) {
    }


    public void selected(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.selected()");
        if (item == MenuItems.GO_HELP) {
            startUnboundApp("Help", new String[] { MonitorService.REQUEST_APPLICATION_HOT_KEY, App.NAME, null} );
        } else if (item instanceof SceneChangeMenuItem) {
            ((SceneChangeMenuItem) item).changeScene();
        } else if (item == MenuItems.PC_SUSPEND) {
            showParentalControlSettingPopup(item, "pin.suspend");
        } else if (item == MenuItems.PC_ACTIVATE) {
            showParentalControlSettingPopup(item, "pin.access_pref");
        } else if (item == MenuItems.PC_ENABLE) {
            showParentalControlSettingPopup(item, "pin.restore");
        }
    }

    public void showParentalControlSettingPopup(final MenuItem item, final String key) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.showParentalControlSettingPopup()");
        RightFilterListener rfl = new RightFilterListener() {
            String[] explain = TextUtil.tokenize(DataCenter.getInstance().getString(key), '|');
//            String[] explain = TextUtil.split(DataCenter.getInstance().getString(key), GraphicsResource.FM17, 300);
            public void receiveCheckRightFilter(int response) {
                if (response == PreferenceService.RESPONSE_SUCCESS) {
                    changeParentalControlSetting(item);
                }
            }
            public String[] getPinEnablerExplain() {
                return explain;
            }
        };
        PreferenceService ps = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            ps.checkRightFilter(rfl, App.NAME, new String[] { RightFilter.PARENTAL_CONTROL }, null, null);
            return;
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    private void changeParentalControlSetting(MenuItem item) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.changeParentalControlSetting()");
        DataCenter dc = DataCenter.getInstance();
        if (item == MenuItems.PC_ACTIVATE) {
            try {
                monitorService.startUnboundApplication("SETTINGS", new String[] {
                                    App.NAME, PreferenceService.PARENTAL_CONTROLS } );
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else if (item == MenuItems.PC_SUSPEND) {
            String curSusDefTime = dc.getString(RightFilter.SUSPEND_DEFAULT_TIME);
            if (Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS.equals(curSusDefTime)
                    || Definitions.OPTION_VALUE_SUSPENDED_FOR_8_HOURS.equals(curSusDefTime)) {
                PreferenceProxy.getInstance().setPreferenceData(RightFilter.PARENTAL_CONTROL, curSusDefTime);
            }
        } else if (item == MenuItems.PC_ENABLE) {
            PreferenceProxy.getInstance().setPreferenceData(RightFilter.PARENTAL_CONTROL, Definitions.OPTION_VALUE_ON);
        }
    }

    public String getVideoContextName() {
        return videoContext;
    }

    public String getCurrentActivatedApplicationName() {
        try {
            return monitorService.getCurrentActivatedApplicationName();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return "";
    }

    public void stopOverlappingApplication() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.stopOverlappingApplication()");
        if (App.R3_TARGET) {
            try {
                monitorService.stopOverlappingApplication();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public void moveToMenu(int menu) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.moveToMenu()");
        try {
            String app = getCurrentActivatedApplicationName();
            if (App.NAME.equals(app)) {
                MenuController.getInstance().showMenu(menu);
            } else {
                startUnboundApp(App.NAME, new String[] {App.NAME, String.valueOf(menu)});
            }
        } catch (Exception e) {
            Log.printWarning(e);
        }
    }

    public void startUnboundApp(String appName, String[] param) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.startUnboundApp("+appName+")");
        try {
            monitorService.startUnboundApplication(appName, param);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean checkPermission() {
        return Environment.SUPPORT_DVR && checkPermission(App.NAME);
    }

    private boolean checkPermission(String appName) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.checkPermission("+appName+")");
        try {
            return monitorService.checkAppAuthorization(appName) != MonitorService.NOT_AUTHORIZED;
        } catch (Exception ex) {
        }
        return true;
    }

    //->Kenneth[2015.10.15] DDC-114 : 어떤 원인인지 저장해 놨다가 showIncompatibilityErrorMessage 에서 사용한다.
    // 이는 항상 needToShowIncompatibilityPopup 가 먼저 불리워야 한다는 가정이 있음.
    static String reasonMsg = "";
    //->Kenneth[2015.7.28] HDCP2.2 체크도 추가
    //->Kenneth[2015.6.24] DDC-107 로 인해서 하나의 함수로 모두 처리할 수 있게 한다.
    public static boolean needToShowIncompatibilityPopup(Object target, boolean checkUhdHdmiConnection) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup("+target+", "+checkUhdHdmiConnection+")");
        if (target == null) return false;
        int definition = -1;
        //->Kenneth[2016.6.1] VDTRMASTER-5824 : standalone 모드에서는 uhd 패키지 있는 것으로 간주할것
        boolean supportUhd = Environment.SUPPORT_UHD;
        if (standAloneMode) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : StandAloneMode. So supportUhd = true");
            supportUhd = true;
        }
        //<-
        try {
            if (target instanceof TvChannel) {
                definition = ((TvChannel)target).getDefinition();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : ch definition = "+definition);
                if (!Environment.SUPPORT_UHD && definition == TvChannel.DEFINITION_4K) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : returns true --- 1");
                    reasonMsg = "";//DDC-114
                    return true;
                }
            } else if (target instanceof AbstractRecording) {
                definition = ((AbstractRecording)target).getDefinition();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : ar definition = "+definition);
                //->Kenneth[2015.7.29] remote recording 인지 체크하는 코드 없앰. local recording 의 경우 여기서 필터링
                // 안되는 문제 발생
                if (!supportUhd && definition == TvChannel.DEFINITION_4K) {
                //if (!Environment.SUPPORT_UHD && definition == TvChannel.DEFINITION_4K) {
                //if (!Environment.SUPPORT_UHD && definition == TvChannel.DEFINITION_4K && (target instanceof RemoteRecording)) {
                //<-
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : returns true --- 2");
                    reasonMsg = "";//DDC-114
                    return true;
                }
            }
            if (checkUhdHdmiConnection && supportUhd && definition == TvChannel.DEFINITION_4K) {
            //if (checkUhdHdmiConnection && Environment.SUPPORT_UHD && definition == TvChannel.DEFINITION_4K) {
                boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : canPlayUhd = "+canPlayUhd);
                if (!canPlayUhd) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : returns true --- 3");
                    Log.printError("PVR850");
                    reasonMsg = "PVR850";//DDC-114
                    return true;
                //->Kenneth[2015.7.28] HDCP2.2 체크도 추가
                } else {
                    if (VideoOutputUtil.getInstance().existHdcpApi()) {
                        boolean hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : hdcp22Supported = "+hdcp22Supported);
                        if (!hdcp22Supported) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : Leave PVR851");
                            Log.printError("PVR851");
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : returns true --- 4");
                            reasonMsg = "PVR851";//DDC-114
                            return true;
                        }
                    }
                //<-
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : returns false");
        return false;
    }
    /*
    //->Kenneth[2015.2.7] 4K : incompatibilty popup 을 띄워야 하는 상황인지 체크한다.
    public static boolean needToShowIncompatibilityPopup(TvChannel ch) {
        boolean result = false;
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() ch.definition = "+ch.getDefinition());
            if (!Environment.SUPPORT_UHD && ch != null && ch.getDefinition() == TvChannel.DEFINITION_4K) {
                result = true;
            } 
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() returns "+result);
        return result;
    }

    //->Kenneth[2015.3.1] 4K : incompatibilty popup 을 띄워야 하는 상황인지 체크한다.
    public static boolean needToShowIncompatibilityPopup(AbstractRecording ar) {
        boolean result = false;
        try {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() ar.definition = "+ar.getDefinition());
            if (!Environment.SUPPORT_UHD && ar != null && ar.getDefinition() == TvChannel.DEFINITION_4K) {
                if (ar instanceof RemoteRecording) {
                    // 리모트 레코딩만 처리. 엄밀히 하면 다 해야 되지만 Non 4K STB 의 경우 로컬에 UHD recording 을
                    // 가지는 경우는 없으므로 고려치 않아도 됨
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : RemoteRecording");
                    result = true;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() : Non RemoteRecording");
                }
            } 
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.needToShowIncompatibilityPopup() returns "+result);
        return result;
    }
    */
    //<-

    //->Kenneth[2015.10.15] DDC-114 : needToShowIncompatibilityPopup 에서 저장된 reasonMsg 를 사용한다.
    //->Kenneth[2015.2.7] 4K
    public static void showIncompatibilityErrorMessage() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.showIncompatibilityErrorMessage()");
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            if (Log.ERROR_ON) {
                Log.printError("CommunicationManager: Not bound ErrorMessageService");
            }
            return;
        }
        try {
            // DDC-114
            //svc.showCommonMessage("");
            svc.showCommonMessage(reasonMsg);
            //<-
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void showErrorMessage(String errorCode, ErrorMessageListener l) {
        showErrorMessage(errorCode, l, null);
    }

    public void showErrorMessage(String errorCode, ErrorMessageListener l, Hashtable table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.showErrorMessage("+errorCode+")");
        ErrorMessageService svc = (ErrorMessageService) DataCenter.getInstance().get(ErrorMessageService.IXC_NAME);
        if (svc == null) {
            if (Log.ERROR_ON) {
                Log.printError("CommunicationManager: Not bound ErrorMessageService");
            }
            return;
        }
        try {
            if (table != null) {
                svc.showErrorMessage(errorCode, l, table);
            } else {
                svc.showErrorMessage(errorCode, l);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void actionRequested(String platform, String[] param) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.actionRequested("+platform+")");
        if (param != null) {
            if (param.length == 1) {
                return;
            }
            if (param != null && param[1] != null) {
                int page = 0;
                try {
                    page = Integer.parseInt(param[1]);
                } catch (Exception ex) {
                }
                ListPanel.requestId = "" + param[2];
                MenuController.getInstance().showMenu(page);
            }
        }
    }

    public static void resetScreenSaverTimer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.resetScreenSaverTimer()");
        try {
            monitorService.resetScreenSaverTimer();
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public boolean confirmScreenSaver() throws RemoteException {
        VideoPlayer videoPlayer = VideoPlayer.getInstance();
        if (videoPlayer.getState() == TunerController.PLAY_RECORDING) {
            boolean paused = videoPlayer.isPaused();
            if (paused) {
                FrameworkMain.getInstance().printSimpleLog("confirmScreenSaver = paused in playback");
            }
            return paused;
        }

        if (monitorState != MonitorService.TV_VIEWING_STATE) {
            FrameworkMain.getInstance().printSimpleLog("confirmScreenSaver = app state");
            return true;
        }
        removeScreenSaver();
        Log.printError("confirmScreenSaver: illegal state");
        return false;
    }


    public void showLoading() {
        showLoading(new Point(Constants.SCREEN_WIDTH / 2, Constants.SCREEN_HEIGHT / 2));
    }

    public void showLoading(Point p) {
        LoadingAnimationService service = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            service.showLoadingAnimation(p);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void hideLoading() {
        LoadingAnimationService service = (LoadingAnimationService) DataCenter.getInstance().get(LoadingAnimationService.IXC_NAME);
        try {
            service.hideLoadingAnimation();
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    class VideoContextAdapter implements VideoContextListener {
        public void videoModeChanged(String videoName) throws RemoteException {
            Log.printDebug("VideoContextListener.videoModeChanged = " + videoName);
            videoContext = videoName;
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
    	public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

    class NotificationListenerImpl implements NotificationListener {

        public void action(String id) throws RemoteException {
        }

        public void cancel(String id) throws RemoteException {
            final String messageid = id;
            Thread th = new Thread() {
                public void run() {
                    NotificationService service = (NotificationService) DataCenter.getInstance().get(
                            NotificationService.IXC_NAME);
                    try {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.NotificationListenerImpl.cancel().run() : removeNotify()");
                        service.removeNotify(messageid);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            };
            th.start();
        }

        public void directShowRequested(String platform, String[] param) throws RemoteException {
            Log.printInfo("platform = " + platform);
            if (param != null) {
                if (param.length == 1) {
                    return;
                }
                if (param != null && param[1] != null) {
                    int page = Integer.parseInt(param[1]);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Core.NotificationListenerImpl.directShowRequested() : showMenu()");
                    MenuController.getInstance().showMenu(page);
                }
            }
        }
    }

    //->Kenneth[2015.5.25] R5 : EPG 가 떠 있는 상태인지 알려준다.
    public boolean isEpgLaunched() {
        boolean epgLaunched = false;
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            if (monitor != null) {
                String activeApp = monitor.getCurrentActivatedApplicationName();
                Log.printDebug(App.LOG_HEADER+"Core.isEpgLaunched() : Active App = "+activeApp);
                // EPG 가 떠 있는 경우만 true
                if ("EPG".equals(activeApp)) {
                    epgLaunched = true;
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
        Log.printDebug(App.LOG_HEADER+"Core.isEpgLaunched() returns = "+epgLaunched);
        return epgLaunched;
    }
}

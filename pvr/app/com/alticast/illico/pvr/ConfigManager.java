package com.alticast.illico.pvr;

import java.io.File;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.alticast.illico.pvr.config.*;

public class ConfigManager implements InbandDataListener, DataUpdateListener {

    private static final String INSTANCE_SUFFIX = "_instance";

    public static final String PVR_CONFIG = "PVR_CONFIG";
    public static final String PVR_CONFIG_INSTANCE = PVR_CONFIG + INSTANCE_SUFFIX;

    private DataCenter dataCenter = DataCenter.getInstance();

    public PvrConfig pvrConfig;

    private static final ConfigManager instance = new ConfigManager();

    public static ConfigManager getInstance() {
        return instance;
    }

    private ConfigManager() {
        // inband files
        dataCenter.addDataUpdateListener(PVR_CONFIG, this);
        // by default
        SharedMemory.getInstance().put(PvrService.DATA_KEY_TSB_KEEP_ALIVE_DURATION,
                    new Long(2 * Constants.MS_PER_MINUTE) );

    }

    public void receiveInbandData(String locator) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigManager.receiveInbandData = " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        try {
            MonitorService monitor = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
            monitor.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("ConfigManager.dataUpdated : " + key);
        }
        File file = (File) value;
        if (PVR_CONFIG.equals(key)) {
            PvrConfig config = PvrConfig.create(file);
            if (Log.DEBUG_ON) {
                Log.printDebug("ConfigManager: value = " + config);
            }
            if (config == null || config.equals(pvrConfig)) {
                // skip
                return;
            }
            this.pvrConfig = config;
            DataCenter.getInstance().put(PVR_CONFIG_INSTANCE, config);
            SharedMemory.getInstance().put(PvrService.DATA_KEY_TSB_KEEP_ALIVE_DURATION,
                    new Long(config.tsbKeepDuration * Constants.MS_PER_SECOND) );
        }
    }

    public void dataRemoved(String key)  {
    }

    public static int oneByteToInt(byte[] data, int offset) {
        return ((int) data[offset]) & 0xFF;
    }

    public static int twoBytesToInt(byte[] data, int offset) {
        return twoBytesToInt(data, offset, false);
    }

    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return ((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF);
    }

    public static String byteArrayToString(byte[] data, int offset, int length) {
        return new String(data, offset, length);
    }

}

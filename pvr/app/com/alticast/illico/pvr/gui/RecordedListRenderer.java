package com.alticast.illico.pvr.gui;

import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.data.*;
import java.util.*;
import java.awt.*;

/**
 * Renderer for Recorded List.
 *
 * @author  June Park
 */
public class RecordedListRenderer extends ListPanelRenderer {

    private RecordingRenderer renderByDuration = new RecordingRendererByDuration();
    private RecordingRenderer renderByDate = new RecordingRendererByDate();
    private RecordingRenderer renderBySize = new RecordingRendererBySize();
    private RecordingRenderer renderByDeletion = new RecordingRendererDeletionDate();

    private Hashtable rendererTable = new Hashtable();

    private Font fTitle = GraphicsResource.BLENDER18;
    private Font fTitleNew = GraphicsResource.BLENDER18_BOLD;

    protected String emptyList;
    protected String emptyDeviceTitle;
    protected String[] emptyDeviceMessage;

    public RecordedListRenderer() {
        bgIcon = dataCenter.getImage("07_bgicon_02.png");
        listIcon = dataCenter.getImage("07_icon_rlist.png");
        lastest = dataCenter.getString("pvr.descRecorded");
        emptyList = dataCenter.getString("pvr.ListEmpty");
        emptyDeviceTitle = dataCenter.getString("multiroom.no_device_title");
        emptyDeviceMessage = TextUtil.split(dataCenter.getString("multiroom.no_device_msg"), GraphicsResource.FM18, 400);
        rendererTable.put(RecordedList.SORT_SIZE.getKey(), renderBySize);
        rendererTable.put(RecordedList.SORT_SAVETIME.getKey(), renderByDeletion);
        rendererTable.put(RecordedList.SORT_DATE.getKey(), renderByDate);
        rendererTable.put(RecordedList.SORT_GENRE.getKey(), renderByDate);
    }

    protected void paintBackground(Graphics g, FullScreenPanel c) {
        g.drawImage(background, 0, 0, c);
        g.drawImage(bgIcon, 784, 94, c);
    }

    protected RecordingRenderer getRecordingRenderer() {
        RecordingRenderer ren = (RecordingRenderer) rendererTable.get(sortingOption.getKey());
        if (ren != null) {
            return ren;
        } else {
            return renderByDuration;
        }
    }

    protected void paintEmptyList(Graphics g) {
        g.setColor(GraphicsResource.C_227_227_228);
        g.setFont(GraphicsResource.BLENDER18);
        if (App.SUPPORT_HN && !Environment.SUPPORT_DVR && listData.getTotalDeviceCount() <= 0) {
            GraphicUtil.drawStringCenter(g, emptyDeviceTitle, 270, 175 + 32 * 2);
            int sy = 175 + 32 * 3;
            for (int i = 0; i < emptyDeviceMessage.length; i++) {
                GraphicUtil.drawStringCenter(g, emptyDeviceMessage[i], 270, sy);
                sy += 20;
            }
        } else {
            GraphicUtil.drawStringCenter(g, emptyList, 270, 175 + 32 * 4);
        }
    }

    abstract class RecordedRenderer extends RecordingRenderer {
        protected int col2x = 326;

        public int[] paint(Graphics g, AbstractRecording r, int offset, boolean inProgress, boolean clean, boolean hasFocus, byte titleMode, Component p) {
            //->Kenneth[2017.3.13] R7.3 : 아이콘들 바뀜(Partially viewed, Completely viewed)
            if (inProgress) {
                g.drawImage(icon_rec, offset + 67, 11, p);
            //->Kenneth[2017.4.10] R7.3 : VDTRMASTER-6094 : 더이상 single episode icon 은 보여주지 않는다.
            /*
            } else if (r.getGroupKey() != null) {
                g.drawImage(hasFocus ? i_07_single_f : i_07_single, offset + 69, 5, p);
            */
            //<-
            } else {
                int viewingState = r.getViewingState();
                if (viewingState == AbstractRecording.PARTIALLY_VIEWED) {
                    g.drawImage(hasFocus ? icon_rec_resume_f : icon_rec_resume, offset + 69, 5, p);
                } else if (viewingState == AbstractRecording.COMPLETELY_VIEWED) {
                    g.drawImage(hasFocus ? icon_rec_replay_f : icon_rec_replay, offset + 69, 5, p);
                }
            }
            /*
            if (inProgress) {
                g.drawImage(icon_rec, offset + 67, 11, p);
            } else if (!clean) {
                g.drawImage(i_07_icon_info_disk_s, offset + 75, 159-151, p);
            } else if (r.getGroupKey() != null) {
                g.drawImage(hasFocus ? i_07_single_f : i_07_single, offset + 69, 5, p);
            }
            */
            //<-

            boolean viewed = r.getViewedCount() > 0;
            g.setFont(viewed ? fTitle : fTitleNew);
            if (!hasFocus && viewed) {
                g.setColor(GraphicsResource.C_178_178_178);
            }
            int sx = offset + 102;
            String title = offset == 0 ? ParentalControl.getTitle(r) : ParentalControl.getEpisodeTitle(r);
            int space = col2x - sx - 5;
            FontMetrics fm = g.getFontMetrics();

            switch (titleMode) {
                case TITLE_NORMAL:
                    String s = TextUtil.shorten(title, fm, space);
                    g.drawString(s, sx, 174-151);
                    break;
                case TITLE_READY:
                    int tw = fm.stringWidth(title);
                    g.drawString(title, sx, 174-151);
                    return new int[] { sx, space, tw };
//                case TITLE_MOVING:
//                    break;
            }
            paintInfo(g, r, offset, hasFocus, viewed, p);
            return null;
        }

        public abstract void paintInfo(Graphics g, AbstractRecording r, int offset, boolean hasFocus, boolean viewed, Component p);
    }

    class RecordingRendererByDuration extends RecordedRenderer {
        public RecordingRendererByDuration() {
            this.col2x = 316;
        }

        public void paintInfo(Graphics g, AbstractRecording r, int offset, boolean hasFocus, boolean viewed, Component p) {
//            g.setFont(viewed ? fInfo : fInfoNew);
//            g.setColor(hasFocus ? Color.black : GraphicsResource.C_189_189_189);

            String s = formatter.getDurationText4(r.getRecordedDuration());
            GraphicUtil.drawStringRight(g, s, 473, 174-151);

            s = formatter.getShortDayText(r.getStartTime());
            g.drawString(s, col2x, 174-151);
        }
    }

    class RecordingRendererByDate extends RecordingRendererByDuration {

        public int getFolderTitleWidth() {
            return 205;
        }

        public int[] paint(Graphics g, RecordingFolder folder, boolean hasFocus, byte titleMode, Component p) {
            int[] value = super.paint(g, folder, hasFocus, titleMode, p);
            if (value != null) {
                return value;
            }
            String s = formatter.getShortDayText(folder.getTime());
            g.drawString(s, col2x, 174-151);
            return null;
        }
    }

    class RecordingRendererBySize extends RecordedRenderer {
        public RecordingRendererBySize() {
            this.col2x = 395;
        }

        public void paintInfo(Graphics g, AbstractRecording r, int offset, boolean hasFocus, boolean viewed, Component p) {
//            g.setFont(viewed ? fInfo : fInfoNew);
//            g.setColor(hasFocus ? Color.black : GraphicsResource.C_189_189_189);
            GraphicUtil.drawStringRight(g, StorageInfoManager.getSizeString(r.getRecordedSize()), 473, 174-151);
        }
    }

    class RecordingRendererDeletionDate extends RecordedRenderer {
        public RecordingRendererDeletionDate() {
            this.col2x = 300;
        }

        public void paintInfo(Graphics g, AbstractRecording r, int offset, boolean hasFocus, boolean viewed, Component p) {
//            g.setFont(viewed ? fInfo : fInfoNew);
//            g.setColor(hasFocus ? Color.black : GraphicsResource.C_189_189_189);
            g.setFont(fTitle);
            GraphicUtil.drawStringRight(g, r.getDeletionText(), 473, 174-151);
            g.drawString(formatter.getShortDayText(r.getStartTime()), col2x, 174-151);
        }
    }

}

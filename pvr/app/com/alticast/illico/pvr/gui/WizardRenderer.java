package com.alticast.illico.pvr.gui;

import java.awt.*;
import java.util.*;

import com.alticast.illico.pvr.StorageInfoManager;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;

public class WizardRenderer extends ListOptionPanelRenderer {
    public static final int ROW_COUNT = 9;
    public static final int MIDDLE_POS = (ROW_COUNT - 1) / 2;
    public static final int LIST_Y_GAP = 32;

    private Image backgound = dataCenter.getImage("bg.jpg");
    private Image hotkeybg = dataCenter.getImage("01_hotkeybg.png");

    private Image i_07_wizardbg = dataCenter.getImage("07_wizardbg.png");
    private Image i_07_w_panel = dataCenter.getImage("07_w_panel.png");

    private Image i_07_infobg = dataCenter.getImage("07_infobg.png");
    private Image i_07_wizard_sha_top = dataCenter.getImage("07_wizard_sha_top.png");
    private Image i_07_wizard_sha_bottom = dataCenter.getImage("07_wizard_sha_bottom.png");
    private Image i_07_wizardfocus = dataCenter.getImage("07_wizardfocus.png");
    private Image i_07_wizardfocus_dim = dataCenter.getImage("07_wizardfocus_dim.png");
    private Image i_07_wizardline = dataCenter.getImage("07_wizardline.png");
    private Image i_07_series = dataCenter.getImage("07_series.png");
    private Image i_07_series_f = dataCenter.getImage("07_series_f.png");
    private Image i_07_wizardglow = dataCenter.getImage("07_wizard_glow.png");

    private Image check = dataCenter.getImage("check.png");
    private Image check_foc = dataCenter.getImage("check_foc.png");
    private Image check_box = dataCenter.getImage("check_box.png");
    private Image check_box_foc = dataCenter.getImage("check_box_foc.png");

    private Image i_01_acglow = dataCenter.getImage("01_acglow.png");
    private Image i_01_acbg_1st = dataCenter.getImage("01_acbg_1st.png");
    private Image i_01_acbg_2nd = dataCenter.getImage("01_acbg_2nd.png");
    private Image i_01_acbg_m = dataCenter.getImage("01_acbg_m.png");
    private Image i_01_ac_shadow = dataCenter.getImage("01_ac_shadow.png");
    private Image i_01_acline = dataCenter.getImage("01_acline.png");

    private Image i_02_detail_bt_foc = dataCenter.getImage("02_detail_bt_foc.png");
    private Image i_02_ars_b = dataCenter.getImage("02_ars_b.png");
    private Image i_02_ars_t = dataCenter.getImage("02_ars_t.png");
    private Image i_02_detail_ar = dataCenter.getImage("02_detail_ar.png");

    private String title;

    private String selected1;
    private String selected2;
    private String willBe;

    private String startWizard;
    private String nextStep;
    private String prevStep;
    private String finalStep;
    private String jumpFinal;
    private String deleteAll;
    private String cancel;

    private int[] widthOfTitle = new int[] {0, 231, 229, 225, 200, 225, 225};

    private String listTitle;
    private String listDateRecorded;
    private String listSize;
    private String listSpaceUsed;
    private String listDeletion;
    private String listTimesViewed;
    private String listLength;

    int step;
    int focus;
    Formatter formatter;

    public WizardRenderer() {
        step = 1;
        SHADOW_Y = 459;
    }

    protected void paintBackground(Graphics g, FullScreenPanel c) {
        g.drawImage(backgound, 0, 0, c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        ManagementWizard p = (ManagementWizard) c;

        this.formatter = Formatter.getCurrent();
        this.step = p.step;
        this.focus = p.getFocus();

        leftTopTitle(g, p);
        rightPanel(g, p);
        rightBottomButton(g, c);
        paintList(g, p);
    }

    public Rectangle getListFocusBounds(ListOptionPanel c) {
        ListData listData = c.listData;
        if (listData == null) {
            return null;
        }
        Vector vector = listData.getCurrentList();
        int listFocus = listData.focus;
        int listSize = vector.size();
        ///////////////////
        int over = listSize - ROW_COUNT;
        int move = listFocus - MIDDLE_POS;
        int firstIndex;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }
        // focus
        if (listFocus >= 0 && listSize > 0) {
            return new Rectangle(53 + 2, 173 + 1 + (listFocus - firstIndex) * LIST_Y_GAP, 527, 36);
        }
        return null;
    }

    /** List. */
    private void paintList(Graphics g, ManagementWizard p) {
        // bg
        g.drawImage(i_07_wizardbg, 7, 142, p);
        // title
        g.setColor(GraphicsResource.C_204_204_204);
        g.drawString(listTitle, 103, 165);

        if (step == 1) {
            g.drawString(listDateRecorded, 403, 164);
            g.drawString(listSize, 527, 164);
        } else if (step == 2) {
            g.drawString(listSize, 527, 164);
        } else if (step == 3) {
            g.drawString(listDateRecorded, 384, 164);
            g.drawString(listSize, 527, 164);
        } else if (step == 4) {
            GraphicUtil.drawStringRight(g, listDeletion, 493, 164);
            g.drawString(listSize, 527, 164);
        } else if (step == 5) {
            g.drawString(listTimesViewed, 410, 164);
            g.drawString(listSpaceUsed, 527, 164);
        } else if (step == 6) {
            g.drawString(listDateRecorded, 330, 164);
            g.drawString(listLength, 445, 164);
            g.drawString(listSize, 527, 164);
        }

        ListData listData = p.listData;
        Vector vector = listData.getCurrentList();
        int listFocus = listData.focus;
        int listSize = vector.size();
        ///////////////////

        int ty = 173;
        int over = listSize - ROW_COUNT;
        int move = listFocus - MIDDLE_POS;
        int firstIndex;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }
        g.translate(0, ty);
        //->Kenneth[2015.2.12] 4K flat design
        // 첫라인은 그리지 않는다
        //g.drawImage(i_07_wizardline, 68, 3, p);

        //->Kenneth[2015.2.16] 4K
        // 아래 라인 그리는 것보다 포커스가 먼저 그려지는 것이 flat design 튜닝중에발견
        // 그래서 line 그리는 곳 밑으로 보낸다.
        // focus
//        if (listFocus >= 0 && listSize > 0) {
//            Image im = focus < 0 ? i_07_wizardfocus : i_07_wizardfocus_dim;
//            g.drawImage(im, 53, (listFocus - firstIndex) * LIST_Y_GAP, p);
//        }

        int size = Math.min(ROW_COUNT, listSize);
        int index = firstIndex;
        // 1. line 을 먼저 그린다
        for (int i = 0; i < size; i++) {
            //->Kenneth[2015.2.12] 4K flat design
            //g.drawImage(i_07_wizardline, 68, LIST_Y_GAP + 3, p);
            if (i == (size-1)) {
                // 맨 마지막 라인은 그리지 않는다
            } else {
                g.drawImage(i_07_wizardline, 68, LIST_Y_GAP + 3, p);
            }
            index++;
            g.translate(0, LIST_Y_GAP);
        }
        g.translate(0, -LIST_Y_GAP * size);

        //->Kenneth[2015.2.16] 4K
        // 아래 라인 그리는 것보다 포커스가 먼저 그려지는 것이 flat design 튜닝중에발견
        // 그래서 line 그리도록 이리로 이동 : 아래 Y 좌표 계산 유의
        // 2. Focus 를 그린다
        if (listFocus >= 0 && listSize > 0) {
            Image im = focus < 0 ? i_07_wizardfocus : i_07_wizardfocus_dim;
            g.drawImage(im, 53, (listFocus - firstIndex) * LIST_Y_GAP, p);
        }

        // 3. 리스트의 content 를 그린다
        index = firstIndex;
        for (int i = 0; i < size; i++) {
            Entry entry = (Entry) vector.elementAt(index);
            paintEntry(g, entry, i, index == listFocus, p);
            index++;
            g.translate(0, LIST_Y_GAP);
        }
        g.translate(0, -LIST_Y_GAP*size-ty);


        if (firstIndex > 0) {
            g.drawImage(i_07_wizard_sha_top, 55, 175, p);
            g.drawImage(i_02_ars_t, 306, 168, p);
        }

        if (firstIndex + ROW_COUNT < listSize) {
            g.drawImage(i_07_wizard_sha_bottom, 55, 408, p);
            g.drawImage(i_02_ars_b, 305, 457, p);
        }

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.setFont(GraphicsResource.BLENDER16);
            g.drawString("Number of Recordings = " + p.listSize, 648, 80);
            g.drawString("Checked Entries in this step = " + p.checkedCount + " / " + listSize, 648, 100);

        }
    }

    private void paintEntry(Graphics g, Entry entry, int row, boolean hasFocus, ManagementWizard c) {
        g.drawImage(hasFocus ? check_box_foc : check_box, 75, 12, c);
        boolean checked = c.isChecked(entry);
        if (checked) {
            g.drawImage(hasFocus ? check_foc : check, 76, 10, c);
        }
        Font fText;
        Color cText;
        if (hasFocus) {
            g.setFont(GraphicsResource.BLENDER19);
            g.setColor(GraphicsResource.C_0_0_0);
            fText = GraphicsResource.BLENDER18;
            cText = GraphicsResource.C_0_0_0;
        } else {
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_230_230_230);
            fText = GraphicsResource.BLENDER17;
            cText = GraphicsResource.C_189_189_189;
        }

        String title;
        int sx;
        long size;
        AbstractRecording rec = null;
        if (entry instanceof AbstractRecording) {
            rec = (AbstractRecording) entry;
            size = rec.getRecordedSize();
            title = TextUtil.shorten(ParentalControl.getTitle(rec), g.getFontMetrics(), widthOfTitle[step]);
            sx = 103;
        } else if (entry instanceof RecordingFolder) {
            RecordingFolder folder = ((RecordingFolder) entry);
            size = folder.getRecordedSize();
            g.drawImage(hasFocus ? i_07_series_f : i_07_series, 98, 7, c);
            title = folder.getDisplayTitle(ParentalControl.getTitle(folder), g.getFontMetrics(), widthOfTitle[step]);
            sx = 127;
        } else {
            return;
        }
        g.drawString(title, 127, 26);
        g.setFont(fText);
        g.setColor(cText);


        String sizeStr = StorageInfoManager.getSizeString2(size);
        GraphicUtil.drawStringRight(g, sizeStr, 562, 26);

        switch (step) {
        case 1: // Mar.30
            g.drawString(formatter.getDayText(rec.getScheduledStartTime()), 434, 26);
            break;
        case 2: // series
            break;
        case 3: // Mar.30-12:00pm
            Date date = new Date(rec.getScheduledStartTime());
            g.drawString(formatter.getDayText(date) + " - " + formatter.getTime(date), 376, 26);
            break;
        case 4: // 3 days left, 1 day left
            GraphicUtil.drawStringRight(g, rec.getDeletionText(), 493, 26);
            break;
        case 5: // Mar.30 15 viewings
            int count = rec.getViewedCount();
            GraphicUtil.drawStringCenter(g, count + " " +
                    (count <= 1 ? dataCenter.getString("pvr.viewing") : dataCenter.getString("pvr.viewings")), 458, 26);
            break;
        case 6: // Mar.30 60min
            g.drawString(formatter.getDayText(rec.getScheduledStartTime()), 361, 26);
            GraphicUtil.drawStringRight(g, formatter.getDurationText(rec.getRecordedDuration()), 495, 26);
            break;
        }

    }

    private void rightPanel(Graphics g, ManagementWizard c) {
        int selectCnt = c.getSelectedCount();
        long selectedBytes = c.getSelectedSize();

        g.drawImage(i_07_infobg, 626, 143, c);
        g.setFont(GraphicsResource.BLENDER18);
        String str = selectCnt + " ";
        g.setColor(GraphicsResource.C_199_159_52);
        int w = g.getFontMetrics().stringWidth(str);
        g.drawString(str, 648, 177);
        g.setColor(GraphicsResource.C_227_227_228);
        g.drawString(selectCnt <= 1 ? selected1 : selected2, 648 + w, 177);

        str = StorageInfoManager.getSizeString2(selectedBytes);
        w = g.getFontMetrics().stringWidth(str);
        g.setColor(GraphicsResource.C_199_159_52);
        g.drawString(str, 648, 203);
        g.setColor(GraphicsResource.C_227_227_228);
        g.drawString(willBe, 648 + 5 + w, 203);

        g.setFont(GraphicsResource.BLENDER16);

        str = StorageInfoManager.getSizeString2(StorageInfoManager.getInstance().getUsingSpace());
        GraphicUtil.drawStringRight(g, str, 690, 227);
        str = StorageInfoManager.getSizeString2(StorageInfoManager.getInstance().getAllocatedSpace());
        g.drawString(str, 854, 227);

        g.setColor(GraphicsResource.C_91_91_91);
        g.fillRect(697, 219, 152, 8);

        long total = StorageInfoManager.getInstance().getAllocatedSpace();
        long using = total - StorageInfoManager.getInstance().getFreeSpace();

        int usingWidth = Math.max(0, Math.min(152, (int) Math.round((double) using * 150 / total)));
        int selectedWidth = (int) Math.round((double) selectedBytes * 150 / total);

        g.setColor(GraphicsResource.C_203_27_12);
        g.fillRect(698, 220, usingWidth - selectedWidth, 6);
        g.setColor(GraphicsResource.C_255_203_0);
        g.fillRect(698 + usingWidth - selectedWidth, 220, selectedWidth, 6);
    }

    public void leftTopTitle(Graphics g, ManagementWizard c) {
        g.setFont(GraphicsResource.BLENDER26);
        g.setColor(GraphicsResource.C_255_255_255);
        g.drawString(c.subTitle, 61, 98);

        g.drawImage(i_07_wizardglow, 20, 70, c);
        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_227_227_227_170);
        g.drawString(c.desc2, 61, 133);
    }

    public void rightBottomButton(Graphics g, UIComponent c) {
        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_241_241_241);
        g.drawImage(hotkeybg, 242, 466, c);
    }

    public void prepare(UIComponent c) {
        selected1 = dataCenter.getString("wizard.selected_titles_1");
        selected2 = dataCenter.getString("wizard.selected_titles_2");
        willBe = dataCenter.getString("wizard.will be available");

        startWizard = dataCenter.getString("wizard.Start Wizard");
        nextStep = dataCenter.getString("wizard.Next step");
        prevStep = dataCenter.getString("wizard.Previous step");
        finalStep = dataCenter.getString("wizard.Final step");
        jumpFinal = dataCenter.getString("wizard.Jump to final step");
        deleteAll = dataCenter.getString("wizard.Delete all selected titles");
        cancel = dataCenter.getString("wizard.Cancel");

        listTitle = dataCenter.getString("wizard.title");
        listDateRecorded = dataCenter.getString("wizard.listDateRecorded");
        listSize = dataCenter.getString("wizard.listSize");
        listSpaceUsed = dataCenter.getString("wizard.listSpaceUsed");
        listDeletion = dataCenter.getString("wizard.listDeletion");
        listTimesViewed = dataCenter.getString("wizard.listTimesViewed");
        listLength = dataCenter.getString("wizard.listLength");

        super.prepare(c);
    }

    public String getStringTime(long duration) {
        return formatter.getDurationText(duration);
    }
}

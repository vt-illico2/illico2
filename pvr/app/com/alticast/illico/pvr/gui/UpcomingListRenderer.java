package com.alticast.illico.pvr.gui;

import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.data.*;
import java.util.*;
import java.awt.*;
import org.ocap.dvr.*;

/**
 * Renderer for Upcoming List.
 *
 * @author  June Park
 */
public class UpcomingListRenderer extends ListPanelRenderer {

    private Image i_07_list_dotline = dataCenter.getImage("07_list_dotline.png");
    private Image i_07_list_dotline_foc = dataCenter.getImage("07_list_dotline_foc.png");

    protected String emptyList;

    private ScheduledRenderer sr = new ScheduledRenderer();

    public UpcomingListRenderer() {
        bgIcon = dataCenter.getImage("07_bgicon_01.png");
        listIcon = dataCenter.getImage("07_icon_upcm.png");
        lastest = dataCenter.getString("pvr.descUpcoming");
        emptyList = dataCenter.getString("pvr.SchudededListEmpty");
    }

    protected void paintBackground(Graphics g, FullScreenPanel c) {
        g.drawImage(background, 0, 0, c);
        g.drawImage(bgIcon, 784, 77, c);
    }

    protected RecordingRenderer getRecordingRenderer() {
        return sr;
    }

    protected void paintEmptyList(Graphics g) {
        g.setColor(GraphicsResource.C_227_227_228);
        g.setFont(GraphicsResource.BLENDER18);
        GraphicUtil.drawStringCenter(g, emptyList, 270, 175 + 32 * 4);
    }

    class ScheduledRenderer extends RecordingRenderer {
        /** Recording entry */
        public int[] paint(Graphics g, AbstractRecording r, int offset, boolean inProgress, boolean clean, boolean hasFocus, byte titleMode, Component p) {
            if (inProgress) {
                g.drawImage(icon_rec, offset + 67, 11, p);
            } else if (r.getGroupKey() != null) {
                g.drawImage(hasFocus ? i_07_repeat_f : i_07_repeat, offset + 69, 5, p);
            }
            Color cInfo;
            Color color = g.getColor();
            boolean isSkipped = r.getBoolean(AppData.UNSET);
            if (clean) {
                clean = r.getState() != OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE;
            }
            if (!clean) {
                cInfo = GraphicsResource.C_215_61_64;
                color = cInfo;
            } else {
                if (isSkipped) {
                    cInfo = hasFocus ? Color.black : GraphicsResource.C_124_124_124;
                    color = cInfo;
                } else {
                    cInfo = hasFocus ? Color.black : GraphicsResource.C_189_189_189;
                }
            }
            g.setColor(color);
            g.setFont(GraphicsResource.BLENDER18);
            int sx = offset + 102;
            String title = offset == 0 ? ParentalControl.getTitle(r) : ParentalControl.getEpisodeTitle(r);
            int space = 240 - offset;
            FontMetrics fm = g.getFontMetrics();

            switch (titleMode) {
                case TITLE_NORMAL:
                    String s = TextUtil.shorten(title, fm, space);
                    g.drawString(s, sx, 174-151);
                    break;
                case TITLE_READY:
                    int tw = fm.stringWidth(title);
                    g.drawString(title, sx, 174-151);
                    return new int[] { sx, space, tw };
//                case TITLE_MOVING:
//                    break;
            }


//            int transX = 0;
//            if (isLong) {
//                int tw = fm.stringWidth(title);
//                transX = tw - space + 5;
//                g.drawString(title, sx, 174-151);
//            } else {
//                String s = TextUtil.shorten(title, fm, space);
//                g.drawString(s, sx, 174-151);
//            }
//            if (transX > 0) {
//                g.translate(transX, 0);
//            }
//
            g.setFont(GraphicsResource.BLENDER16);
            g.setColor(cInfo);
            String sr = formatter.getTime(r.getStartTime()) + " - " + formatter.getTime(r.getEndTime());
            GraphicUtil.drawStringRight(g, sr, 473, 174-151);

            if (isSkipped) {
                g.drawImage(hasFocus ? i_07_list_dotline : i_07_list_dotline_foc, 71, 169-151, p);
            }
//            if (transX > 0) {
//                g.translate(-transX, 0);
//            }
            return null;
        }
    }

}

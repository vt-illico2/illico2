package com.alticast.illico.pvr.gui;

import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.hn.*;
import org.ocap.shared.dvr.RecordingFailedException;
import java.util.*;
import java.awt.*;

/**
 * Renderer for Recorded or Upcoming List.
 *
 * @author  June Park
 */
public abstract class ListPanelRenderer extends ListOptionPanelRenderer {

    public static final int ROW_COUNT = 10;
    public static final int MIDDLE_POS = (ROW_COUNT - 1) / 2;
    public static final int LIST_Y_GAP = 32;

    protected Image background = dataCenter.getImage("bg.jpg");

    protected Image i_07_roundbg_s = dataCenter.getImage("07_roundbg_s.png");
    protected Image i_07_rouudcover_s = dataCenter.getImage("07_rouudcover_s.png");

    protected Image i_07_listshadow = dataCenter.getImage("07_listshadow.png");
    protected Image i_07_list_bg = dataCenter.getImage("07_list_bg.png");
    protected Image i_07_list_sh_b = dataCenter.getImage("07_list_sh_b.png");
    protected Image i_07_list_sh_t = dataCenter.getImage("07_list_sh_t.png");
    protected Image i_07_list_line = dataCenter.getImage("07_list_line.png");
    protected Image i_02_ars_b = dataCenter.getImage("02_ars_b.png");
    protected Image i_02_ars_t = dataCenter.getImage("02_ars_t.png");

    protected Image i_07_list_foc = dataCenter.getImage("07_list_foc.png");
    protected Image i_07_list_focus01_dim = dataCenter.getImage("07_list_focus01_dim.png");

    protected Image i_07_issue_foc_2 = dataCenter.getImage("07_issue_foc_2.png");
    protected Image i_07_lowdisk_foc_2 = dataCenter.getImage("07_lowdisk_foc_2.png");
    protected Image i_07_icon_info_s = dataCenter.getImage("07_icon_info_s.png");
    protected Image i_07_icon_info_disk_s = dataCenter.getImage("07_icon_info_disk_s.png");

    protected Image i_07_series = dataCenter.getImage("07_series.png");
    protected Image i_07_series_f = dataCenter.getImage("07_series_f.png");
    protected Image i_07_single = dataCenter.getImage("07_single.png");
    protected Image i_07_single_f = dataCenter.getImage("07_single_f.png");
    protected Image i_07_repeat = dataCenter.getImage("07_icon_repeat.png");
    protected Image i_07_repeat_f = dataCenter.getImage("07_icon_repeat_foc.png");

    protected Image icon_plus = dataCenter.getImage("03_icon_plus.png");
    protected Image icon_plus_foc = dataCenter.getImage("03_icon_plus_foc.png");
    protected Image icon_minus = dataCenter.getImage("03_icon_minus.png");
    protected Image icon_minus_foc = dataCenter.getImage("03_icon_minus_foc.png");

    protected Image icon_hd = dataCenter.getImage("icon_hd.png");
    protected Image icon_en = dataCenter.getImage("icon_en.png");
    protected Image icon_fr = dataCenter.getImage("icon_fr.png");
    protected Image icon_rec = dataCenter.getImage("icon_rec.png");
    //->Kenneth[2015.2.16] 4K
    protected Image icon_uhd = dataCenter.getImage("icon_uhd.png");

    protected Image i_07_dayglow_t = dataCenter.getImage("07_dayglow_t.png");
    protected Image i_07_dayglow_b = dataCenter.getImage("07_dayglow_b.png");

    protected Image icon_system = dataCenter.getImage("icon_system.png");
    protected Image i_02_icon_con = dataCenter.getImage("02_icon_con.png");

    //->Kenneth[2017.3.14] R7.3
    protected Image icon_rec_play = dataCenter.getImage("icon_rec_play.png");
    protected Image icon_rec_play_f = dataCenter.getImage("icon_rec_play_f.png");
    protected Image icon_rec_replay = dataCenter.getImage("icon_rec_replay.png");
    protected Image icon_rec_replay_f = dataCenter.getImage("icon_rec_replay_f.png");
    protected Image icon_rec_resume = dataCenter.getImage("icon_rec_resume.png");
    protected Image icon_rec_resume_f = dataCenter.getImage("icon_rec_resume_f.png");
    //<-

    protected Image[] ratingImages = {
        null,
        dataCenter.getImage("icon_g.png"),
        dataCenter.getImage("icon_8.png"),
        dataCenter.getImage("icon_13.png"),
        dataCenter.getImage("icon_16.png"),
        dataCenter.getImage("icon_18.png")
    };

    protected Image bgIcon;
    protected Image listIcon;
    protected String lastest;

    protected String strNotRecord = dataCenter.getString("pvr.notRecord");

    protected static Color cIconNormal = new Color(0xFFCB00);
    protected static Color cIconWarning = new Color(0xEE7A10);
    protected static Color cIconCritical = new Color(0xD10010);

    //->Kenneth[2015.2.12] 4K flat design
    //protected static Color cEntryBackground = new Color(0x0a0a0a);
    protected static Color cEntryBackground = new Color(30, 30, 30);
    //protected static Color cEntryBackground = GraphicsResource.C_0_0_0_128;

    protected static Font fEpisode = FontResource.DINMED.getFont(18);

    protected SortingOption sortingOption;
    protected Formatter formatter;
    protected ListData listData;
    protected RecordingFolder currentFolder;
    protected boolean topLevel;
    protected int recordingCount;

    protected RecordingRenderer recordingRenderer;

    public static long totalSpace;
    public static long freeSpace;
    public static String freeSpaceString;

    private AutoScrollingImage ticker;

    public void setAutoScrollingImage(AutoScrollingImage ticker) {
        this.ticker = ticker;
    }

    /** Left-bottom free space. */
    private void paintFreeSpace(Graphics g, ListPanel p) {
        String fss = freeSpaceString;
        if (fss == null) {
            return;
        }
        double fsr = StorageInfoManager.getRatio(freeSpace, totalSpace);
        Color cFreeText;
        Color cFreeIcon;

        int free = (int) (fsr * 100);
        if (free >= StorageInfoManager.WARNING_LEVEL) {
            cFreeText = GraphicsResource.C_180_180_180;
            cFreeIcon = cIconNormal;
        } else if (free >= StorageInfoManager.CRITICAL_LEVEL) {
            cFreeText = GraphicsResource.C_212_126_3;
            cFreeIcon = cIconWarning;
        } else {
            cFreeText = GraphicsResource.C_212_126_3;
            cFreeIcon = cIconCritical;
        }

//        byte diskStatus = IssueManager.getInstance().getDiskStatus();
//        switch (diskStatus) {
//            case StorageInfoManager.DISK_WARNING:
//                cFreeText = GraphicsResource.C_212_126_3;
//                cFreeIcon = cIconWarning;
//                break;
//            case StorageInfoManager.DISK_FULL:
//            case StorageInfoManager.DISK_CRITICAL:
//                cFreeText = GraphicsResource.C_212_126_3;
//                cFreeIcon = cIconCritical;
//                break;
//            default:
//                cFreeText = GraphicsResource.C_180_180_180;
//                cFreeIcon = cIconNormal;
//                break;
//        }

        g.drawImage(i_07_roundbg_s, 54, 488, p);

        g.setColor(cFreeIcon);
        g.fillArc(55, 489, 20, 20, 90, - (int) ((1 - fsr) * 360));

        g.drawImage(i_07_rouudcover_s, 54, 488, p);

        g.setFont(GraphicsResource.DINMED15);
        g.setColor(cFreeText);
        g.drawString(fss, 81, 503);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.drawString(freeSpace + " / " + totalSpace, 80, 490);
//            g.drawString(RecordingListManager.getDeletingTargetString(), 300, 490);
//            g.drawString(RemoteDevice.protectedSet.toString(), 300, 490);
        }
    }

    public Rectangle getListFocusBounds(ListOptionPanel c) {
        ListData listData = c.listData;
        if (listData == null) {
            return null;
        }
        Vector vector = listData.getCurrentList();
        int listFocus = listData.focus;
        int listSize = vector.size();
        ///////////////////
        int over = listSize - ROW_COUNT;
        int move = listFocus - MIDDLE_POS;
        int firstIndex;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }
        // focus
        if (listFocus >= 0 && listSize > 0) {
            return new Rectangle(52 + 2, 151 + 1 + (listFocus - firstIndex) * LIST_Y_GAP, 433, 36);
        }
        return null;
    }

    /** List. */
    private void paintList(Graphics g, ListPanel p) {
        // bg
        g.drawImage(i_07_listshadow, 6, 465, p);
        g.drawImage(i_07_list_bg, 53, 114, p);

        // title
        g.drawImage(listIcon, 70, 125, p);
        String listTitle = p.getTitle();
        g.setFont(GraphicsResource.BLENDER20);
        //->Kenneth[2015.2.12] 4K flat design
        //g.setColor(GraphicsResource.C_46_46_45);
        //g.drawString(listTitle, 94, 141);
        //g.setColor(GraphicsResource.C_214_182_55);
        g.setColor(GraphicsResource.C_251_217_89);
        g.drawString(listTitle, 93, 140);

        this.listData = p.listData;
        this.currentFolder = listData.getCurrentFolder();
        this.topLevel = currentFolder == null;
        this.recordingCount = listData.getRecordingCount();

        if (sortingOption != null && recordingCount > 0) {
            g.setColor(GraphicsResource.C_227_227_228);
            g.setFont(GraphicsResource.BLENDER18);
            String s = dataCenter.getString(sortingOption.getKey());
            GraphicUtil.drawStringRight(g, dataCenter.getString("pvr.Sorted") + " " + s.toLowerCase(), 486, 109);
        }

        Vector vector = listData.getCurrentList();
        int listFocus = listData.focus;
        int listSize = vector.size();
        ///////////////////

        int ty = 151;
        int focus = p.getFocus();

        int over = listSize - ROW_COUNT;
        int move = listFocus - MIDDLE_POS;
        int firstIndex;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }
        g.translate(0, ty);
        //->Kenneth[2015.2.12] 4K flat design
        // 첫 라인은 그리지 않는다
        //g.drawImage(i_07_list_line, 70, 2, p);

        //->Kenneth[2015.2.16] 4K
        // 아래 라인 그리는 것보다 포커스가 먼저 그려지는 것이 flat design 튜닝중에발견
        // 그래서 line 그리는 곳 밑으로 보낸다.
        // focus
//        if (listFocus >= 0 && listSize > 0) {
//            Image im = focus < 0 ? i_07_list_foc : i_07_list_focus01_dim;
//            g.drawImage(im, 52, (listFocus - firstIndex) * LIST_Y_GAP, p);
//        }

        int size = Math.min(ROW_COUNT, listSize);
        int index = firstIndex;
        Entry entry = null;
//        try {
//            entry = (Entry) vector.elementAt(listFocus);
//        } catch (Exception ex) {
//        }
//        ticker.setFocus(entry);

        // 1. line 을 먼저 그린다
        for (int i = 0; i < size; i++) {
            //->Kenneth[2015.2.12] 4K flat design
            //g.drawImage(i_07_list_line, 70, LIST_Y_GAP + 2, p);
            if (i == (size-1)) {
                // 맨 마지막 라인은 그리지 않는다
            } else {
                g.drawImage(i_07_list_line, 70, LIST_Y_GAP + 2, p);
            }
            index++;
            g.translate(0, LIST_Y_GAP);
        }

        //->Kenneth[2015.2.16] 4K
        // 아래 라인 그리는 것보다 포커스가 먼저 그려지는 것이 flat design 튜닝중에발견
        // 그래서 line 그리도록 이리로 이동 : 아래 Y 좌표 계산 유의
        // 2. Focus 를 그린다
        g.translate(0, -LIST_Y_GAP*size);
        if (listFocus >= 0 && listSize > 0) {
            Image im = focus < 0 ? i_07_list_foc : i_07_list_focus01_dim;
            g.drawImage(im, 52, (listFocus - firstIndex) * LIST_Y_GAP, p);
        }

        // 3. 리스트의 content 를 그린다
        index = firstIndex;
        for (int i = 0; i < size; i++) {
            entry = null;
            try {
                entry = (Entry) vector.elementAt(index);
            } catch (Exception ex) {
            }
            if (entry != null) {
                entry.paint(g, i, index == listFocus, p, this);
            }
            index++;
            g.translate(0, LIST_Y_GAP);
        }
        g.translate(0, -LIST_Y_GAP*size);

        // 위의 translate 결과로 인해 아래의 translate 도 변해야 함
        //g.translate(0, -LIST_Y_GAP * size - ty);
        g.translate(0, - ty);

        if (firstIndex > 0) {
            g.drawImage(i_07_list_sh_t, 54, 153, p);
            g.drawImage(i_02_ars_t, 259, 145, p);
        }

        if (firstIndex + ROW_COUNT < listSize) {
            g.drawImage(i_07_list_sh_b, 54, 415, p);
            g.drawImage(i_02_ars_b, 259, 465, p);
        }
    }

    /** Black background. */
    private void fillEntryBackground(Graphics g, int row, boolean hasFocus, ListPanel p) {
        if (!hasFocus) {
            g.setColor(cEntryBackground);
            //g.setColor(Color.blue); //TEST Color
            g.fillRect(54, 2, 433, LIST_Y_GAP + 1);
        }
        if (row > 0) {
            g.drawImage(i_07_dayglow_t, 54, -7, p);
        }
        if (row < ROW_COUNT - 1) {
            g.drawImage(i_07_dayglow_b, 54, LIST_Y_GAP + 1, p);
        }
    }

    /** Issue. */
    public void paintEntry(Graphics g, Issue issue, int row, boolean hasFocus, ListPanel p) {
        int issueType = issue.getType();
        if (issueType == Issue.DISK_WARNING_ISSUE) {
            if (!hasFocus) {
                g.drawImage(i_07_lowdisk_foc_2, 54, 1, p);
            }
            g.setColor(hasFocus ? GraphicsResource.C_0_0_0 : GraphicsResource.C_212_126_3);
            g.drawImage(i_07_icon_info_disk_s, 74, 9, p);
        } else {
            if (!hasFocus) {
                g.drawImage(i_07_issue_foc_2, 54, 1, p);
            }
            g.setColor(hasFocus ? GraphicsResource.C_0_0_0 : GraphicsResource.C_215_61_64);
            g.drawImage(i_07_icon_info_s, 74, 9, p);
        }
        g.setFont(hasFocus ? GraphicsResource.BLENDER18 : GraphicsResource.BLENDER16);
        g.drawString(issue.getTitle(), 102, 174-151);
    }

    /** InactiveSeries. */
    public void paintEntry(Graphics g, InactiveSeries is, int row, boolean hasFocus, ListPanel p) {
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_215_61_64);
        g.drawString(TextUtil.shorten(ParentalControl.getTitle(is), GraphicsResource.FM18, 370), 102, 174-151);
    }

    /** Recording. */
    public void paintEntry(Graphics g, AbstractRecording r, int row, boolean hasFocus, ListPanel p) {
        if (Log.EXTRA_ON) {
            g.setColor(java.awt.Color.green);
            g.setFont(GraphicsResource.BLENDER15);
            g.drawString(r.getIdObject() + (r.isBlocked() ? "  blocked" : "")
                    + (ParentalControl.isReleased(r) ? " released" : "")
                    + (r.isInUse() ? " using" : "")
                , 55, 12);
            GraphicUtil.drawStringRight(g, r.getString(AppData.GROUP_KEY), 470, 12);
        }
        // icons
        int shiftRight = topLevel ? 0 : 20;
        short stateGroup = r.getStateGroup();
        boolean inProgress = stateGroup == RecordingListManager.IN_PROGRESS;
        boolean inUpcomingList = p instanceof UpcomingList;
        boolean clean = inProgress || r.isClean();

        if (inProgress) {
            fillEntryBackground(g, row, hasFocus, p);
//        } else if (r.getGroupKey() != null) {
//            if (inUpcomingList) {
//                g.drawImage(hasFocus ? i_07_repeat_f : i_07_repeat, shiftRight + 69, 5, p);
//            } else if (clean) {
//                g.drawImage(hasFocus ? i_07_single_f : i_07_single, shiftRight + 69, 5, p);
//            }
        }
        g.setColor(hasFocus ? GraphicsResource.C_0_0_0 : GraphicsResource.C_230_230_230);
        boolean needTicker = false;
        if (hasFocus) {
            if (ticker.offset == 1) {
                Graphics tg = ticker.getGraphics();
                if (tg != null) {
                    // int[3] { x position of text, ui space of text, full lenghth of text }
                    int[] values = recordingRenderer.paint(tg, r, shiftRight, inProgress, clean, hasFocus, RecordingRenderer.TITLE_READY, p);
                    tg.dispose();
                    ticker.startAnimation(values[0], values[1], values[2]);
                }
            }
            needTicker = (ticker.image != null && ticker.offset < 0);
        }
        recordingRenderer.paint(g, r, shiftRight, inProgress, clean, hasFocus, needTicker ? RecordingRenderer.TITLE_MOVING : RecordingRenderer.TITLE_NORMAL, p);
        if (needTicker) {
            ticker.paint(g, p);
        }
    }

    /** RecordingFolder. */
    public void paintEntry(Graphics g, RecordingFolder folder, int row, boolean hasFocus, ListPanel p) {
        boolean needTicker = false;
        if (hasFocus) {
            if (ticker.offset == 1) {
                Graphics tg = ticker.getGraphics();
                if (tg != null) {
                    // int[3] { x position of text, ui space of text, full lenghth of text }
                    int[] values = recordingRenderer.paint(tg, folder, hasFocus, RecordingRenderer.TITLE_READY, p);
                    tg.dispose();
                    ticker.startAnimation(values[0], values[1], values[2]);
                }
            }
            needTicker = (ticker.image != null && ticker.offset < 0);
        }
        recordingRenderer.paint(g, folder, hasFocus, needTicker ? RecordingRenderer.TITLE_MOVING : RecordingRenderer.TITLE_NORMAL, p);
        if (needTicker) {
            ticker.paint(g, p);
        }
    }

    /** Separator. */
    public void paintEntry(Graphics g, Separator sp, int row, boolean hasFocus, ListPanel p) {
        fillEntryBackground(g, row, hasFocus, p);
        g.setColor(GraphicsResource.C_178_178_178);
        g.setFont(GraphicsResource.BLENDER18);
        GraphicUtil.drawStringCenter(g, sp.getTitle(), 263, 175-151);
    }

    /** FilterTitle. */
    public void paintEntry(Graphics g, FilterTitle ft, int row, boolean hasFocus, ListPanel p) {
        g.drawImage(icon_system, 69, 7, p);
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(hasFocus ? GraphicsResource.C_0_0_0 : GraphicsResource.C_230_230_230);
        g.drawString(ft.getTitle(), 102, 174-151);
    }

    /** Description. */
    private void paintDesc(Graphics g, ListPanel p) {
        ListData listData = p.listData;
        if (listData == null) {
            return;
        }
        Entry entry = listData.getCurrent();
        if (entry == null) {
            return;
        }

        String title;
        int sx = 524;

        if (entry instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) entry;
            paintDesc(g, rec, p);
            if (!rec.isClean() && rec.getStateGroup() != RecordingListManager.PENDING) {
                int lines = p.scrollText.getLineCount();
                if (lines > 3) {
                    lines = 4;
                }
                g.setColor(GraphicsResource.C_255_122_33);
                g.setFont(GraphicsResource.BLENDER18);
                //->Kenneth[2017.3.29] R7.3 : 경고 아이콘 보여줘야 함.
                g.drawImage(i_07_icon_info_disk_s, 524, 295 - ((4 - lines) * 17)-15, p);
                g.drawString(strNotRecord, 524+22, 295 - ((4 - lines) * 17));
                //g.drawString(strNotRecord, 524, 295 - ((4 - lines) * 17));
                //<-
            }
            title = ParentalControl.getTitle(rec);
            if (ParentalControl.getLevel(rec) != ParentalControl.OK) {
                sx += 30;
                g.drawImage(i_02_icon_con, 521, 111, p);
            }

        } else {
            if (entry instanceof RecordingFolder) {
                RecordingFolder folder = (RecordingFolder) entry;
                if (ParentalControl.isAccessBlocked(folder)) {
                    sx += 30;
                    g.drawImage(i_02_icon_con, 521, 111, p);
                }

                Vector entries = folder.getEntries();
                int size = (entries != null) ? Math.min(3, entries.size()) : 0;
// Note: removed description since rule changed to grouping by title
//                if (size > 0) {
//                    Object en = entries.elementAt(0);
//                    if (en instanceof AbstractRecording) {
//                        paintDesc(g, (AbstractRecording) en, p);
//                    }
//                }

                g.setFont(GraphicsResource.BLENDER16);
                g.setColor(GraphicsResource.C_182_182_182);
                g.drawString(lastest, 524, 213-55);

                g.setFont(fEpisode);
                g.setColor(GraphicsResource.C_255_255_255);

                int sy = 240-55;
                String et;
                for (int i = 0; i < size; i++) {
                    Entry en = (Entry) entries.elementAt(i);
                    if (en instanceof AbstractRecording) {
                        AbstractRecording rec = (AbstractRecording) en;
                        g.setColor(GraphicsResource.C_178_178_178);
                        g.drawString(formatter.getDayText(rec.getStartTime()), 537, sy);
                        et = ParentalControl.getEpisodeTitle(rec);
                    } else {
                        et = en.getTitle();
                    }
                    g.setColor(Color.white);
                    g.drawString(TextUtil.shorten(et, g.getFontMetrics(), 880-622), 622, sy);
                    sy += 20;
                }
                title = ParentalControl.getTitle(folder);

            } else if (entry instanceof InactiveSeries) {
                InactiveSeries is = (InactiveSeries) entry;
                paintDesc(g, is, p);
                title = ParentalControl.getTitle(is);

            } else if (entry instanceof Issue) {
                Issue is = (Issue) entry;
                g.setFont(GraphicsResource.BLENDER15);
                g.setColor(GraphicsResource.C_255_255_255);
                String desc = TextUtil.replace(is.getDescriptionInList(), "|", "\n");
                String[] tt = TextUtil.split(desc, g.getFontMetrics(), 350);
                for (int i = 0; tt != null && i < tt.length; i++) {
                    g.drawString(tt[i], 524, 148 + i * 18);
                }
                title = is.getTitle();
            } else {
                title = entry.getTitle();
            }
        }

        g.setFont(GraphicsResource.BLENDER24);
        g.setColor(GraphicsResource.C_255_203_0);
        String s = TextUtil.shorten(title, g.getFontMetrics(), 380 + 524 - sx);
        g.drawString(s, sx, 129);
    }

    private void paintDesc(Graphics g, ProgramDetail pd, ListPanel p) {
        g.setFont(GraphicsResource.BLENDER20);
        g.setColor(GraphicsResource.C_222_222_222);

        // channel
        int n = pd.getChannelNumber();
        if (n > 0) {
            g.drawString(String.valueOf(n), 524, 158);
        }
        String call = pd.getChannelName();
        g.drawString(call, 663, 158);
        Image logo = (Image) SharedMemory.getInstance().get("logo." + call);
        if (logo == null) {
            logo = (Image) SharedMemory.getInstance().get("logo.default");
        }
        if (logo != null) {
            g.drawImage(logo, 561, 138, p);
        }

        if (pd instanceof AbstractRecording) {
            AbstractRecording rec = (AbstractRecording) pd;
            // time
            String s = formatter.getTime(rec.getStartTime()) + " - " + formatter.getTime(rec.getEndTime());
            g.setFont(GraphicsResource.BLENDER18);
            g.drawString(s, 748, 157);

            if (Log.EXTRA_ON) {
                g.setColor(Color.green);
                g.drawString("state = " + rec.getState(), 570, 60);
                g.drawString("start = " + new java.util.Date(rec.getScheduledStartTime()), 570, 80);
                g.drawString("create = " + new java.util.Date(rec.getLong(AppData.CREATION_TIME)), 570, 100);
                g.drawString("YEAR_SHIFT = " + rec.getInteger(AppData.YEAR_SHIFT), 570, 120);
                long exp = rec.getExpirationTime();
                String perString;
                if (exp == Long.MAX_VALUE) {
                    perString = "Until I erase";
                } else {
                    long dur = exp - System.currentTimeMillis();
                    perString = formatter.getDurationText(dur) + " " + new java.util.Date(exp);
                }
                g.drawString(perString, 570, 140);
                int reason = rec.getInteger(AppData.FAILED_REASON);
                if (reason != 0) {
                    g.drawString("failure reason = " + RecordManager.getFailedReasonString(reason), 570, 40);
                }
            }
        }

        // genre & icons
        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_178_178_178);
        String genre = pd.getGenreName();
        int sx = 524;
        g.drawString(genre, 524, 184);
        sx += GraphicsResource.FM17.stringWidth(genre);
        sx += 15;
        int sr = sx;

        if (pd.getDefinition() == 1) {
            g.drawImage(icon_hd, sx, 172, p);
            sx += 25;
        //->Kenneth[2015.2.16] 4K
        } else if (pd.getDefinition() == 2) {
            g.drawImage(icon_uhd, sx, 172, p);
            sx += 27;
        }
        String language = pd.getLanguage();
        if ("en".equals(language)) {
            g.drawImage(icon_en, sx, 172, p);
            sx += 25;
        } else if ("fr".equals(language)) {
            g.drawImage(icon_fr, sx, 172, p);
            sx += 25;
        }
        Image iRating = ratingImages[pd.getRatingIndex()];
        if (iRating != null) {
            g.drawImage(iRating, sx, 172, p);
            sx += 25;
        }
        if (sr != sx) {
            g.drawString("|", sr - 10, 184);
        }
    }


    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        ListPanel p = (ListPanel) c;
        sortingOption = p.sortingOption;
        formatter = Formatter.getCurrent();

        recordingRenderer = getRecordingRenderer();

        paintList(g, p);
        if (recordingCount == 0 && listData.getCurrentList().size() <= 1 && !p.noRecordingBox.isVisible()) {
            paintEmptyList(g);
        }
        paintDesc(g, p);
        paintFreeSpace(g, p);

        if (Log.EXTRA_ON) {
            g.setColor(Color.green);
            g.setFont(GraphicsResource.BLENDER16);
            g.drawString(App.NAME + " " + App.VERSION + ", ID = " + ListPanel.requestId + "; " + listData.getCompletedRecordingCount(), 50, 80);
            if (App.SUPPORT_HN) {
                g.drawString("Local.root = " + HomeNetManager.getInstance().localRootContainer, 50, 95);
            }
            if (listData != null) {
                g.drawString("Remote Device = " + listData.getActiveDeviceCount() + " / " + listData.getTotalDeviceCount(), 350, 80);
            }
            g.drawString(ParentalControl.getInstance().toString(), 51, 30);
            if (p instanceof UpcomingList) {
                g.drawString("max end = " + new Date(RecordingListManager.maxEndTime), 200, 45);
            }
        }
    }

    protected abstract void paintEmptyList(Graphics g);

    protected abstract RecordingRenderer getRecordingRenderer();

    abstract class RecordingRenderer {
        public static final byte TITLE_NORMAL = 0;
        public static final byte TITLE_READY = 1;
        public static final byte TITLE_MOVING = 2;

        public abstract int[] paint(Graphics g, AbstractRecording r, int offset, boolean inProgress, boolean clean, boolean hasFocus, byte titleMode, Component p);

        public int getFolderTitleWidth() {
            return 330;
        }

        // x position of text, ui space of text, full lenghth of text
        public int[] paint(Graphics g, RecordingFolder folder, boolean hasFocus, byte titleMode, Component p) {
            if (Log.EXTRA_ON) {
                g.setColor(java.awt.Color.green);
                g.setFont(GraphicsResource.BLENDER15);
                g.drawString("   " + folder.type + (folder.isBlocked() ? " blocked" : "")
                        + (ParentalControl.isReleased(folder) ? " released " : ""), 60, 12);
            }
            g.drawImage(hasFocus ? i_07_series_f : i_07_series, 69, 7, p);
            g.setFont(GraphicsResource.BLENDER18);
            if (hasFocus) {
                g.setColor(GraphicsResource.C_0_0_0);
            } else {
                g.setColor(GraphicsResource.C_230_230_230);
            }

            switch (titleMode) {
                case TITLE_NORMAL:
                    g.drawString(folder.getDisplayTitle(ParentalControl.getTitle(folder), GraphicsResource.FM18, getFolderTitleWidth()), 102, 174-151);
                    break;
                case TITLE_READY:
                    String title = folder.getFullDisplayTitle(ParentalControl.getTitle(folder));
                    int tw = GraphicsResource.FM18.stringWidth(title);
                    g.drawString(title, 102, 174-151);
                    return new int[] { 102, getFolderTitleWidth(), tw };
            }

            if (topLevel) {
                g.drawImage(hasFocus ? icon_plus_foc : icon_plus, 452, 8, p);
            } else {
                g.drawImage(hasFocus ? icon_minus_foc : icon_minus, 452, 8, p);
            }
            return null;
        }

    }

}

package com.alticast.illico.pvr.gui;

import java.awt.*;

import com.alticast.illico.pvr.MenuController;
import com.alticast.illico.pvr.ui.PVRPortal;
import com.alticast.illico.pvr.ui.FullScreenPanel;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

public class PVRPortalRenderer extends FullScreenPanelRenderer {
    private DataCenter dataCenter = DataCenter.getInstance();
    private Rectangle bounds = new Rectangle(0, 0, 960, 540);
    /** gap between hot key image. */
    private Image imgVideoMenuBG = dataCenter.getImage("00_mainbg.png");
//    private Image imgClock = dataCenter.getImage("clock.png");
    private Image imgButtonBG = dataCenter.getImage("01_hotkeybg.png");
//    private Image imgLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
//    private Image imgHisOver = DataCenter.getInstance().getImage("his_over.png");
    private Image descrip_bg = DataCenter.getInstance().getImage("07_descrip_bg.png");

    //->Kenneth[2015.2.16] 4K flat design tuning
    private Color footerLineColor = new Color(70, 70, 70);
    private PVRPortal pvrPortal;

//    private String pvris = dataCenter.getString("pvrportal.Pvris");
//    private String descrip[] = {dataCenter.getString("pvrportal.descrip1"), dataCenter.getString("pvrportal.descrip2"),
//            dataCenter.getString("pvrportal.descrip3"), dataCenter.getString("pvrportal.descrip4"),
//            dataCenter.getString("pvrportal.descrip5")};

    protected void paintBackground(Graphics g, FullScreenPanel c) {
        g.drawImage(imgVideoMenuBG, 0, 0, c);
    }

    public void paint(Graphics g, UIComponent c) {
        super.paint(g, c);

//
//        if (imgLogo != null) {
//            g.drawImage(imgLogo, 53, 21, c);
//            String lang = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
//            int logoWidth = imgLogo.getWidth(c);
//            g.drawImage(imgHisOver, 53 + logoWidth + 12, 45, c);
//            g.setFont(GraphicsResource.BLENDER18);
//            g.setColor(GraphicsResource.C_124_124_124);
//            String txtPortalMain = dataCenter.getString("pvr.PVR");
//            g.drawString(txtPortalMain, 53 + logoWidth + 29, 57);
//        }
//
//        Formatter formatter = Formatter.getCurrent();
//        int longDateWth = 0;
//        if (formatter != null) {
//            String longDate = formatter.getLongDate();
//            if (longDate != null) {
//                g.setFont(GraphicsResource.BLENDER17);
//                g.setColor(GraphicsResource.C_255_255_255);
//                longDateWth = GraphicsResource.FM17.stringWidth(longDate);
//                g.drawString(longDate, 910 - longDateWth, 57);
//            }
//        }
//        if (imgClock != null) {
//            int imgClockWth = imgClock.getWidth(c);
//            g.drawImage(imgClock, 910 - longDateWth - 4 - imgClockWth, 43, c);
//        }

        //->Kenneth[2015.2.16] 4K flat design tuning
        // line 이미지를 없애고 java 컬러로 footer 구분선 그려준다
        g.setColor(footerLineColor);
        g.fillRect(373, 477, 587, 1);
//        if (imgButtonBG != null) {
//            g.drawImage(imgButtonBG, 236, 466, c);
//        }
        
        // draw Button BG
        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_241_241_241);
        PVRPortal p = (PVRPortal) c;

        g.drawImage(descrip_bg, 374, 386, c);
        int ci = p.pvrPortalTree.getCurrentIndex();
        if (p.pvrPortalTree.treeRenderer.focusViewing && ci < PVRPortal.menuKeys.length) {
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_222_222_222);
            int sc = PVRPortal.menuKeys[ci].getSceneCode();
            String temp[] = TextUtil.split(dataCenter.getString("pvrportal.descrip" + sc), g.getFontMetrics(), 515);
            for (int a = 0; temp != null && a < temp.length; a++) {
                g.drawString(temp[a], 387, 411 + a * 19);
            }
//            g.drawString(pvris, 387, 411);
//            g.setFont(GraphicsResource.BLENDER15);
//            g.setColor(GraphicsResource.C_197_197_197);
//
//            String temp[] = TextUtil.split(descrip[p.pvrPortalTree.getCurrentIndex()], g.getFontMetrics(), 515);
//            for (int a = 0; temp != null && a < temp.length; a++) {
//                g.drawString(temp[a], 387, 431 + a * 16);
//            }
        }
    }

    public void prepare(UIComponent c) {
        pvrPortal = (PVRPortal) c;
        if (pvrPortal.footer != null) {
            pvrPortal.footer.addButton(PreferenceService.BTN_D, "pvr.Options");
        }
        super.prepare(c);
//        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

package com.alticast.illico.pvr.gui;

import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;
import java.util.*;

public abstract class ListOptionPanelRenderer extends OptionPanelRenderer {

    public ListOptionPanelRenderer() {
    }

    public abstract Rectangle getListFocusBounds(ListOptionPanel c);

}

package com.alticast.illico.pvr.gui;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ScaleUPList extends UIComponent {
    public DataCenter dataCenter = DataCenter.getInstance();
    private static final long serialVersionUID = 1L;
    private int maxView;
    private int halfView;
    private int selectIndex;

    private String[] contents;

    private String title;
    public static final int MORE_DAYS = 7;

    public ScaleUPList() {
        setRenderer(new SortListRenderer());
        prepare();
    }

    public void start() {
        setFont(GraphicsResource.BLENDER18);
        this.setVisible(true);
    }

    public void start(String selectedValue) {
        setSelectedIndex(selectedValue);
        start();
    }

    public void stop() {
        setVisible(false);
    }

    public void setFont(Font f) {
        super.setFont(f);
    }

    public void setItem(String[] str) {
        contents = str;
        if (str.length <= 3) {
            maxView = 3;
            halfView = 1;
            selectIndex = contents.length - 1;
        } else if (str.length < 9) {
            maxView = 5;
            halfView = 2;
            selectIndex = contents.length - 2;
        } else {
            maxView = 9;
            halfView = 4;
            selectIndex = 4;
        }
    }

    public void setSelectedIndex(String value) {
        if (value == null) {
            return;
        }
        for (int i = 0; i < contents.length; i++) {
            if (value.equals(contents[i])) {
                setSelectedIndex(i);
                return;
            }
        }
    }

    public void setSelectedIndex(int index) {
        selectIndex = index - halfView;

        if (selectIndex < 0) {
            selectIndex = contents.length - Math.abs(selectIndex);
        }
    }

    public int getFocusIndex() {
        return (selectIndex + halfView) % contents.length;
    }

    public String getContentValue() {
        return contents[(selectIndex + halfView) % contents.length];
    }

    public void setUpIndexFocus() {
        selectIndex--;
        if (selectIndex < 0) {
            selectIndex = contents.length - 1;
        }
        repaint();
    }

    public void setDownIndexFocus() {
        selectIndex++;
        if (selectIndex > contents.length - 1) {
            selectIndex = 0;
        }
        repaint();
    }

    public boolean handleKey(int type) {
        Log.printInfo("type = " + type);
        switch (type) {
        case KeyEvent.VK_UP:
            setUpIndexFocus();
            repaint();
            break;
        case KeyEvent.VK_DOWN:
            setDownIndexFocus();
            repaint();
            break;
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            break;
        case HRcEvent.VK_ENTER:
            stop();
            repaint();
            break;
        case OCRcEvent.VK_EXIT:
//        case KeyCodes.LAST:
            stop();
            break;
        default:
            return false;
        }

        repaint();
        return true;
    }

    class SortListRenderer extends Renderer {

        private Image sortbg_t;
        private Image sortbg_m;
        private Image sortbg_b;
        private Image sortbg_foc;

        private Image ars_t;
        private Image ars_b;

        private Image sortsh_b;
        private Image sortsh_t;

        public void paint(Graphics g, UIComponent c) {

            Font font = g.getFont();
            g.setFont(font);

            if (maxView == 3) {
                g.drawImage(sortbg_b, 0, 86, c);
                g.drawImage(sortbg_m, 0, 59, c);
                g.drawImage(sortbg_t, 0, 40, c);

                g.setColor(GraphicsResource.C_78_78_78);
                g.fillRect(2, 15, 230, 29);

                g.drawImage(ars_t, 105, 0, c);
                g.drawImage(ars_b, 105, 103, c);
            } else if (maxView == 5) {
                g.drawImage(sortbg_t, 0, 38, c);
                for (int a = 0; a < maxView - 2; a++) {
                    g.drawImage(sortbg_m, 0, 57 + a * 27, c);
                }

                g.drawImage(sortbg_b, 0, 138, c);

                g.setColor(GraphicsResource.C_78_78_78);
                g.fillRect(2, 15, 230, 29);

                g.drawImage(ars_t, 105, 0, c);
                g.drawImage(ars_b, 105, 154, c);
            }  else if (maxView == 9) {

                g.drawImage(sortbg_t, 0, 38, c);
                for (int a = 0; a < maxView - 2; a++) {
                    g.drawImage(sortbg_m, 0, 57 + a * 27, c);
                }
                g.drawImage(sortbg_b, 0, 246, c);

                g.setColor(GraphicsResource.C_78_78_78);
                g.fillRect(2, 15, 230, 29);

                g.drawImage(ars_t, 105, 0, c);
                g.drawImage(ars_b, 105, 262, c);
            }

            title = TextUtil.shorten(title, g.getFontMetrics(), 254);
            g.setFont(GraphicsResource.BLENDER20);
            g.setColor(GraphicsResource.C_27_24_12);
            g.drawString(title, 16, 36);
            g.setColor(GraphicsResource.C_214_182_55);
            g.drawString(title, 15, 34);

            Rectangle oldClip = g.getClipBounds();
            if (maxView == 3) {
                g.clipRect(0, 45, 231, 57);
            } else if (maxView == 5) {
                g.clipRect(0, 45, 231, 109);
            }  else if(maxView == 9){
                g.clipRect(0, 45, 232, 217);
            }

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);

            for (int a = 0; a < maxView; a++) {
                g.setColor(GraphicsResource.C_182_182_182);
                if(a != halfView) {
                    if(maxView == 9){
                        g.drawString(contents[(a + selectIndex) % contents.length], 17, 49 + a * 27);
                    } else if(maxView == 5) {
                        g.drawString(contents[(a + selectIndex) % contents.length], 17, 50 + a * 27);
                    } else {
                        g.drawString(contents[(a + selectIndex) % contents.length], 17, 52 + a * 27);
                    }
                }
            }

            g.drawImage(sortsh_t, 4, 43, c);
            if (maxView == 3) {
                g.drawImage(sortsh_b, 4, 74, c);
                g.drawImage(sortbg_foc, 2, 58, c);
            } else if (maxView == 5) {
                g.drawImage(sortsh_b, 4, 126, c);
                g.drawImage(sortbg_foc, 2, 83, c);
            } else {
                g.drawImage(sortsh_b, 4, 241, c);
                g.drawImage(sortbg_foc, 2, 135, c);
            }

            g.setColor(GraphicsResource.C_0_0_0);
            if(maxView == 3) {
                g.drawString(contents[(halfView + selectIndex) % contents.length], 17, 51 + halfView * 27);
            } else {
                g.drawString(contents[(halfView + selectIndex) % contents.length], 17, 49 + halfView * 27);
            }
            g.clipRect(oldClip.x, oldClip.y, oldClip.width, oldClip.height);
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        public void prepare(UIComponent c) {
            sortsh_b = DataCenter.getInstance().getImage("sortsh_b.png");
            sortsh_t = DataCenter.getInstance().getImage("sortsh_t.png");
            sortbg_t = DataCenter.getInstance().getImage("08_op_bg_t.png");
            sortbg_m = DataCenter.getInstance().getImage("08_op_bg_m.png");
            sortbg_b = DataCenter.getInstance().getImage("08_op_bg_b.png");
            sortbg_foc = DataCenter.getInstance().getImage("08_op_foc.png");

            ars_t = DataCenter.getInstance().getImage("02_ars_t.png");
            ars_b = DataCenter.getInstance().getImage("02_ars_b.png");

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

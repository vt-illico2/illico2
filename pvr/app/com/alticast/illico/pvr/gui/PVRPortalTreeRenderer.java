package com.alticast.illico.pvr.gui;

import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.config.PvrConfig;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.ui.PVRPortal;
import com.alticast.illico.pvr.ui.PVRPortalTree;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

public class PVRPortalTreeRenderer extends Renderer {
    public static final Rectangle BOUNDS_FULL_MENU_TREE = new Rectangle(0, 58, 412, 482);

    public PVRPortalTreeText treeText;

    public static final int TREE_GAP = 34;
    private DataCenter dataCenter = DataCenter.getInstance();
    private Image imgMenuBG = dataCenter.getImage("00_menubg.png");
    private Image imgMenuBGShadow = dataCenter.getImage("00_menushadow.png");
    private Image imgMenuLine = dataCenter.getImage("00_mini_line.png");
    private Image imgBullet = dataCenter.getImage("01_bullet01.png");
    private Image imgMenuFocus = dataCenter.getImage("00_menufocus.png");
//    private Image imgPVRIcon = dataCenter.getImage("07_pvrmain_icon.png");
    private Image imgPVRIcon = dataCenter.getImage("PVR.png");
    private Image infoIcon = dataCenter.getImage("07_icon_info_s.png");
    private Image infoIconFoc = dataCenter.getImage("07_icon_info_issue.png");
    private Image infoIconDisk = dataCenter.getImage("07_icon_info_disk_s.png");
    private Image infoIconFocDisk = dataCenter.getImage("07_icon_info_disk.png");

    private Image issuebg = dataCenter.getImage("07_isseu_foc.png");
    private Image lowbg = dataCenter.getImage("07_lowdisk_foc.png");
    private Image imgButtonIconExit;
    private String lowDiskString;
    private String conflictString;

    private final int yPos = 58;

    private int currentIndex;

    public boolean focusViewing = false;

    protected void paint(Graphics g, UIComponent c) {
        PVRPortalTree pvrPortalTree = (PVRPortalTree) c;

        if (imgMenuBG != null) {
            g.drawImage(imgMenuBG, 52, 71 - yPos, c);
        }
        if (imgMenuBGShadow != null) {
            // g.drawImage(imgMenuBGShadow, 0, 477 - yPos, c);
            g.drawImage(imgMenuBGShadow, 0, 477, c);
        }

        if (imgPVRIcon != null) {
            g.drawImage(imgPVRIcon, 58, 76 - yPos, c);
        }

        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_236_211_143);
        String txtKeyInfo = (String) dataCenter.get("pvrportal.info");
        if (txtKeyInfo != null) {
            g.drawString(txtKeyInfo, 64, 503 - yPos);
            int infoWidth = g.getFontMetrics().stringWidth(txtKeyInfo);
            if (imgButtonIconExit != null) {
                g.drawImage(imgButtonIconExit, 64 + infoWidth + 5, 489 - yPos, c);
            }
        }

        String title = (String) dataCenter.get("pvrportal.menu_header");
        if (title != null) {
            g.setFont(GraphicsResource.BLENDER20);
            //->Kenneth[2015.2.12] 4K flat design
            //g.setColor(GraphicsResource.C_46_46_45);
            //g.drawString(title, 90, 95 - yPos);
            g.setColor(GraphicsResource.C_202_174_97);
            g.drawString(title, 90, 94 - yPos);
        }

        currentIndex = pvrPortalTree.getCurrentIndex();

        Issue issue = pvrPortalTree.issue;
//        int issue = pvrPortalTree.getIssueCount();

        if (issue != null && focusViewing) {
            int issueType = issue.getType();
            g.translate(0, -58);
            Image bg, normalIcon, focusIcon;
            Color textColor;
            if (issueType == Issue.DISK_WARNING_ISSUE) {//
                bg = lowbg;
                normalIcon = infoIconDisk;
                focusIcon = infoIconFocDisk;
                textColor = GraphicsResource.C_212_126_3;
            } else {
                bg = issuebg;
                normalIcon = infoIcon;
                focusIcon = infoIconFoc;
                textColor = GraphicsResource.C_215_61_64;
            }

            g.drawImage(bg, 52, 274, c);
            if (currentIndex == PVRPortal.menuKeys.length) {
                //->Kenneth[2015.4.10] : 포커스가 살짝 짧음. 그래서 2px 늘려그리기로함.
                // 얘는 맨 마지막 라인에 Disk Space 나 issue 보는 라인의 focus임
                g.drawImage(imgMenuFocus, 48, 272, 302+2, 50, c);
                //g.drawImage(imgMenuFocus, 48, 272, c);
                g.drawImage(focusIcon, 86, 281, c);
                g.setFont(GraphicsResource.BLENDER21);
                g.setColor(GraphicsResource.C_0_0_0);
            } else {
                g.drawImage(imgBullet, 81, 291, c);
                g.drawImage(normalIcon, 95, 284, c);
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(textColor);
            }
            g.drawString(issue.getTitle(), 117, 298);
            g.translate(0, 58);
        }

        for (int i = 0; i < PVRPortal.menuKeys.length; i++) {
            //->Kenneth[2015.4.10] : 마지막 라인이 focus 에 겹침. 마지막 라인은 안 그리도록
            if (i != (PVRPortal.menuKeys.length-1)) {
                g.drawImage(imgMenuLine, 66, 105 - yPos + ((i + 1) * TREE_GAP), c);
            }

            if (i == currentIndex && focusViewing) {
                //->Kenneth[2015.4.10] : 포커스가 살짝 짧음. 그래서 2px 늘려그리기로함.
                g.drawImage(imgMenuFocus, 48, 102 - yPos + (i * TREE_GAP), 302+2, 50, c);
                //g.drawImage(imgMenuFocus, 48, 102 - yPos + (i * TREE_GAP), c);
            }

            if (i != currentIndex) {
                g.drawImage(imgBullet, 81, 119 - yPos + (i * TREE_GAP), c);
            }
        }

        if (Log.EXTRA_ON) {
            PvrConfig config = ConfigManager.getInstance().pvrConfig;
            if (config != null) {
                g.setFont(GraphicsResource.BLENDER16);
                g.setColor(java.awt.Color.green);
                g.drawString("Channel Change Notice Timeout = " + config.ccNoticeTimeout, 50, 350);
                g.drawString("Series Inactivity Timeout = " + config.seriesInactiveTimeout, 50, 370);
                g.drawString("Channel Autotune Notice Duration = " + config.autoTuneDuration, 50, 390);
                g.drawString("Timeshift Buffer Keep Alive Duration = " + config.tsbKeepDuration, 50, 410);
                g.drawString("Recording Information Duration = " + config.infoDuration, 50, 430);
            }
        }
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return BOUNDS_FULL_MENU_TREE;
    }

    public void prepare(UIComponent c) {
        focusViewing = false;
        if (MenuController.getInstance().imageTable != null) {
            imgButtonIconExit = (Image) MenuController.getInstance().imageTable.get(PreferenceService.BTN_EXIT);;
        } else {
            imgButtonIconExit = DataCenter.getInstance().getImage("b_btn_exit.png");
        }

        lowDiskString = DataCenter.getInstance().getString("pvr.issue_title_disk_low");
        conflictString = DataCenter.getInstance().getString("pvr.issue_title_conflict");

        if (treeText == null) {
            treeText = new PVRPortalTreeText();
            treeText.setBounds(BOUNDS_FULL_MENU_TREE);
            c.add(treeText);
        }
        treeText.setVisible(false);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public class PVRPortalTreeText extends Container implements AnimationRequestor {

        public void paint(Graphics g) {
            g.translate(0, -58);
            for (int i = 0; i < PVRPortal.menuKeys.length; i++) {
                if (i == currentIndex && focusViewing) {
                    g.setFont(currentIndex == 0 ? GraphicsResource.BLENDER19 : GraphicsResource.BLENDER21);
                    g.setColor(GraphicsResource.C_4_4_4);
                } else {
                    g.setFont(GraphicsResource.BLENDER18);
                    g.setColor(GraphicsResource.C_230_230_230);
                }

                String txtPortalListKey = PVRPortal.menuKeys[i].getKey();
                if (txtPortalListKey == null) {
                    continue;
                }
                String txtPortalList = (String) dataCenter.get(txtPortalListKey);
                if (txtPortalList == null) {
                    continue;
                }
//                g.setColor(i == currentIndex ? GraphicsResource.C_4_4_4 : GraphicsResource.C_230_230_230);
                g.drawString(txtPortalList, 95, 128 - yPos + (i * TREE_GAP));
            }
            g.translate(0, 58);
        }

        public void update(Graphics g) {

        }

        public void animationEnded(Effect effect) {
            Log.printDebug("effect = " + effect);
            this.setVisible(true);
            focusViewing = true;
            repaint();
        }

        public void animationStarted(Effect effect) {
            focusViewing = false;
        }

        public boolean skipAnimation(Effect effect) {
            return false;
        }
    }
}

package com.alticast.illico.pvr.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.alticast.illico.pvr.App;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.ui.PVRPortalPreference;
import com.alticast.illico.pvr.ui.FullScreenPanel;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

public class PVRPortalPreferenceUIComponent extends FullScreenPanel {
    public PVRPortalPreferenceRenderer rendar = new PVRPortalPreferenceRenderer();
    PVRPortalPreference pvrPortalPreference;

    public PVRPortalPreferenceUIComponent(PVRPortalPreference p) {
        stack.push("pvr.PVR");
        pvrPortalPreference = p;
        setRenderer(rendar);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public class PVRPortalPreferenceRenderer extends FullScreenPanelRenderer {

        PVRPortalPreferenceUIComponent pvrPortalPreferenceUi;

        public static final int PREFERENCE_GAP = 36;
        private DataCenter dataCenter = DataCenter.getInstance();
        private Image imgInput = dataCenter.getImage("input_210_sc.png");
        private Image imgInput2 = dataCenter.getImage("input_210_dim.png");
        private Image imgInput3 = dataCenter.getImage("input_210_sc_dim.png");
        private Image imgInput_high = dataCenter.getImage("input_210_high.png");
        private Image imgInputFocus = dataCenter.getImage("input_210_foc2.png");

        private Image descr_sh = dataCenter.getImage("08_descr_sh.png");
        private Image top_sh = dataCenter.getImage("08_top_sh.png");
        private Image imgButtonIconBack;

        private String title;

        private String recordingbuffer;
        private String saveTime1;
        private String saveTimeSub1;
        private String saveTime2;
        private String saveTimeSub2;
        private String terminal;
        private String multiroom;

        private String recordingbufferValue;
        private String saveTime1Value;
        private String saveTime2Value;
        private String terminalValue;
//        private String multiroomValue;

        private String[] descTitle = null;
        private String[] description = null;

        public String getFocusListString() {
            switch (pvrPortalPreference.getFocus()) {
                case 0:
                    return recordingbuffer;
                case 1:
                    return saveTime1;
                case 2:
                    return saveTime2;
                case 3:
                    return terminal;
                case 4:
                    return multiroom;
            }
            return "";
        }

        public void setSaveTime1Value(String str) {
            saveTime1Value = str;
        }

        public void setSaveTime2Value(String str) {
            saveTime2Value = str;
        }

        public void setRecordingbufferValue(String str) {
            recordingbufferValue = str;
        }

        public void setTerminalValue(String str) {
            terminalValue = str;
        }

        public String getTerminalValue() {
            return terminalValue;
        }

//        public void setMultiroomValue(String str) {
//            multiroomValue = str;
//        }

        protected void paintBackground(Graphics g, FullScreenPanel p) {
        }

        protected void paint(Graphics g, UIComponent c) {
            super.paint(g, c);
            // draw Button BG
            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_241_241_241);
            if (descr_sh != null) {
                g.drawImage(descr_sh, 0, 411, c);
            }
            g.drawImage(top_sh, 0, 77, c);

            g.setFont(GraphicsResource.BLENDER23);
            g.setColor(GraphicsResource.C_255_255_255);
            g.drawString(title, 55, 114);

            int focus = pvrPortalPreference.getFocus();
            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_178_178_178);
            g.drawString(recordingbuffer, 56, 161);
            g.drawString(saveTime1, 56, 198 + 8);
            g.drawString(saveTimeSub1, 56, 198 + 16 + 8);

            g.drawString(saveTime2, 56, 235 + 19);
            g.drawString(saveTimeSub2, 56, 235 + 16 + 19);
            if (App.SUPPORT_HN) {
                g.drawString(terminal, 56, 272 + 33);
//                g.drawString(multiroom, 56, 309 + 44);
            }

            g.drawImage(imgInput, 339, 141, c);
            g.drawImage(imgInput, 339, 178 + 11, c);
            g.drawImage(imgInput, 339, 215 + 22, c);
            if (App.SUPPORT_HN) {
                g.drawImage(imgInput, 339, 252 + 33, c);
//                g.drawImage(imgInput, 339, 289 + 44, c);
            }

            if (!pvrPortalPreference.scaleUPList.isVisible()) {
                g.drawImage(imgInputFocus, 339, 141 + 48 * focus, c);
            }

            g.setFont(GraphicsResource.BLENDER18);
            if (recordingbufferValue != null) {
                g.setColor(focus == 0 ? GraphicsResource.C_255_255_255 : GraphicsResource.C_178_178_178);
                g.drawString(recordingbufferValue, 353, 162);
            }
            if (saveTime1Value != null) {
                g.setColor(focus == 1 ? GraphicsResource.C_255_255_255 : GraphicsResource.C_178_178_178);
                g.drawString(saveTime1Value, 353, 199 + 11);
            }
            if (saveTime2Value != null) {
                g.setColor(focus == 2 ? GraphicsResource.C_255_255_255 : GraphicsResource.C_178_178_178);
                g.drawString(saveTime2Value, 353, 236 + 22);
            }
            if (App.SUPPORT_HN) {
                if (terminalValue != null) {
                    g.setColor(focus == 3 ? GraphicsResource.C_255_255_255 : GraphicsResource.C_178_178_178);
                    g.drawString(TextUtil.shorten(terminalValue, g.getFontMetrics(), 190), 353, 273 + 33);
                }

//                if (multiroomValue != null) {
//                    g.setColor(focus == 4 ? GraphicsResource.C_255_255_255 : GraphicsResource.C_178_178_178);
//                    g.drawString(multiroomValue, 353, 310 + 44);
//                }
            }

            g.setFont(GraphicsResource.BLENDER15);
            g.setColor(GraphicsResource.C_218_205_162);
            String desc[] = TextUtil.split(description[focus], g.getFontMetrics(), 850, "|");
            for (int a = 0; a < desc.length; a++) {
                g.drawString(desc[a], 55, 443 + a * 20);
            }

            if (pvrPortalPreference.scaleUPList.isVisible() || pvrPortalPreference.editing) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);

                g.setColor(GraphicsResource.C_210_210_210);
                switch (pvrPortalPreference.getFocus()) {
                case 0:
                    g.setFont(GraphicsResource.BLENDER17);
                    g.drawString(recordingbuffer, 56, 161);
                    g.drawImage(imgInput_high, 339, 141, c);
                    g.setFont(GraphicsResource.BLENDER18);
                    if (recordingbufferValue != null) {
                        g.setColor(GraphicsResource.C_240_240_240);
                        g.drawString(recordingbufferValue, 353, 162);
                    }
                    break;
                case 1:
                    g.setFont(GraphicsResource.BLENDER17);
                    g.drawString(saveTime1, 56, 198 + 8);
                    g.drawString(saveTimeSub1, 56, 198 + 16 + 8);
                    g.drawImage(imgInput_high, 339, 178 + 11, c);
                    g.setFont(GraphicsResource.BLENDER18);
                    if (saveTime1Value != null) {
                        g.setColor(GraphicsResource.C_240_240_240);
                        g.drawString(saveTime1Value, 353, 199 + 11);
                    }
                    break;
                case 2:
                    g.setFont(GraphicsResource.BLENDER17);
                    g.drawString(saveTime2, 56, 235 + 19);
                    g.drawString(saveTimeSub2, 56, 235 + 16 + 19);
                    g.drawImage(imgInput_high, 339, 215 + 22, c);
                    g.setFont(GraphicsResource.BLENDER18);
                    if (saveTime2Value != null) {
                        g.setColor(GraphicsResource.C_240_240_240);
                        g.drawString(saveTime2Value, 353, 236 + 22);
                    }
                    break;
                case 3:
                    g.setFont(GraphicsResource.BLENDER17);
                    g.drawString(terminal, 56, 161 + 48 * 3);
                    g.drawImage(imgInput_high, 339, 141 + 48 * 3, c);
                    g.setFont(GraphicsResource.BLENDER18);
                    if (terminalValue != null) {
                        g.setColor(GraphicsResource.C_240_240_240);
                        g.drawString(terminalValue, 353, 273 + 33);
                    }
                    break;
//                case 4:
//                    g.setFont(GraphicsResource.BLENDER17);
//                    g.drawString(multiroom, 56, 161 + 48 * 4);
//                    g.drawImage(imgInput_high, 339, 141 + 48 * 4, c);
//                    g.setFont(GraphicsResource.BLENDER18);
//                    if (multiroomValue != null) {
//                        g.setColor(GraphicsResource.C_240_240_240);
//                        g.drawString(multiroomValue, 353, 310 + 44);
//                    }
//                    break;
                }
            }
        }


        public void prepare(UIComponent c) {
            pvrPortalPreferenceUi = (PVRPortalPreferenceUIComponent) c;

            title = dataCenter.getString("pvrportal.preference");
            recordingbuffer = dataCenter.getString("pvrportal.recordingbuffer");
            saveTime1 = dataCenter.getString("pvrportal.savetime1");
            saveTimeSub1 = dataCenter.getString("pvrportal.savetimesub1");
            saveTime2 = dataCenter.getString("pvrportal.savetime2");
            saveTimeSub2 = dataCenter.getString("pvrportal.savetimesub2");
            terminal = dataCenter.getString("pvrportal.terminal");
            multiroom = dataCenter.getString("pvrportal.multiroom");

            descTitle = new String[] {recordingbuffer, saveTime1, saveTime2, terminal, multiroom};

            description = new String[] {dataCenter.getString("pvrportal.desc1"),
                    dataCenter.getString("pvrportal.desc2"), dataCenter.getString("pvrportal.desc3"),
                    dataCenter.getString("pvrportal.desc4"), dataCenter.getString("pvrportal.desc5")};

            String value = DataCenter.getInstance().getString(PreferenceNames.RECORD_BUFFER);

            if (value == null || value.length() == 0) {
                value = pvrPortalPreference.list0[0];
            } else {
                recordingbufferValue = value;
            }

            value = DataCenter.getInstance().getString(PreferenceNames.DEFAULT_SAVE_TIME);
            if (value == null || value.length() == 0) {
                value = pvrPortalPreference.list1[0];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_UNTIL_I_ERASE)) {
                saveTime1Value = pvrPortalPreference.list1[0];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_AFTER_7_DAYS)) {
                saveTime1Value = pvrPortalPreference.list1[1];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_AFTER_14_DAYS)) {
                saveTime1Value = pvrPortalPreference.list1[2];
            }

            value = DataCenter.getInstance().getString(PreferenceNames.DEFAULT_SAVE_TIME_REPEAT);
            if (value == null || value.length() == 0) {
                saveTime2Value = pvrPortalPreference.list2[0];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_KEEP_ALL)) {
                saveTime2Value = pvrPortalPreference.list2[0];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_LAST_3)) {
                saveTime2Value = pvrPortalPreference.list2[1];
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_LAST_5)) {
                saveTime2Value = pvrPortalPreference.list2[2];
            }

//            value = DataCenter.getInstance().getString(PreferenceNames.MULTIROOM_SUPPORT);
//            if (Definitions.OPTION_VALUE_ENABLE.equals(value)) {
//                multiroomValue = pvrPortalPreference.list4[0];
//            } else {
//                multiroomValue = pvrPortalPreference.list4[1];
//            }

            terminalValue = HomeNetManager.getInstance().getDeviceName();

            //->Kenneth[2015.10.23] VDTRMASTER-5702 : 우정 가이드로 수정함.
            if (terminalValue == null) {
                terminalValue = DataCenter.getInstance().getString(PreferenceNames.TERMINAL_NAME);
            }
            //<-

            super.prepare(c);
        }
    }
}

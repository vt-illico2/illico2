package com.alticast.illico.pvr.gui;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Calendar;

import org.havi.ui.event.HRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;

public class TimeList extends UIComponent {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public DataCenter dataCenter = DataCenter.getInstance();
    public static final int START_TIME = 0;
    public static final int END_TIME = 1;
    public static final int MINUTE = 2;
    public static final int DAYS = 3;
    public static final int AMPM = 4;
    public static final int START_TIME24 = 5;
    public static final int END_TIME24 = 6;

    public static final int AFTER7 = 1;
    public static final int UNTIL_I_ERASE = 0;
    public static final int AFTER14 = 2;

    private int selectIndex;
    private int maxView;
    private int halfView;

    private int type;

    private String[] contents;

    private FocusList focusList = new FocusList();

    private Rectangle recFocusListString1 = new Rectangle(2, 17, 34, 50);
    private Rectangle recFocusListString2 = new Rectangle(2, 17, 129, 59);

    public TimeList() {
        setRenderer(new DayListRender());
        this.add(focusList);

        prepare();
    }

    public void setItem(int type) {
        this.type = type;
        if (type == START_TIME24) {
            Calendar cal = Calendar.getInstance();
            int curTime = cal.get(Calendar.HOUR_OF_DAY);

            contents = null;
            contents = new String[24];

            for (int a = 0, b = 0; a < 24; a++, b++) {
                if (a < 10) {
                    contents[b] = "0" + String.valueOf(a);
                } else {
                    contents[b] = String.valueOf(a);
                }
            }
            selectIndex = curTime;
            selectIndex = selectIndex - 1;
            if (selectIndex < 0) {
                selectIndex = 23;
            }
            focusList.setBounds(recFocusListString1);
        } else if (type == END_TIME24) {
            contents = null;
            contents = new String[24];

            for (int a = 0; a < 24; a++) {
                if (a < 10) {
                    contents[a] = "0" + String.valueOf(a);
                } else {
                    contents[a] = String.valueOf(a);
                }
            }

            Calendar cal = Calendar.getInstance();
            int curTime = cal.get(Calendar.HOUR_OF_DAY);

            selectIndex = curTime;
            selectIndex = selectIndex - 1;
            if (selectIndex < 0) {
                selectIndex = 23;
            }
            focusList.setBounds(recFocusListString1);
        } else if (type == START_TIME) {
            Calendar cal = Calendar.getInstance();
            int curTime = cal.get(Calendar.HOUR_OF_DAY);
            if(curTime == 0){
                curTime = 12;
            }
            contents = null;
            contents = new String[12];

            String language = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
            if(language.equals(Definitions.LANGUAGE_ENGLISH)){
                for (int a = 1, b = 0; a < 13; a++, b++) {
                    if (a < 10) {
                        contents[b] = "0" + String.valueOf(a);
                    } else {
                        contents[b] = String.valueOf(a);
                    }
                }
            } else {
                for (int a = 0; a < 12; a++) {
                    if (a < 10) {
                        contents[a] = "0" + String.valueOf(a);
                    } else {
                        contents[a] = String.valueOf(a);
                    }
                }
            }

            selectIndex = curTime;
            selectIndex = selectIndex - 1;
            if (selectIndex < 0) {
                selectIndex = 11;
            }
            focusList.setBounds(recFocusListString1);
        } else if (type == END_TIME) {
            contents = null;
            contents = new String[12];

            String language = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
            if (language.equals(Definitions.LANGUAGE_ENGLISH)) {
                for (int a = 1, b = 0; a < 13; a++, b++) {
                    if (a < 10) {
                        contents[b] = "0" + String.valueOf(a);
                    } else {
                        contents[b] = String.valueOf(a);
                    }
                }
            } else {
                for (int a = 0; a < 12; a++) {
                    if (a < 10) {
                        contents[a] = "0" + String.valueOf(a);
                    } else {
                        contents[a] = String.valueOf(a);
                    }
                }
            }

            Calendar cal = Calendar.getInstance();
            int curTime = cal.get(Calendar.HOUR_OF_DAY);

            if(curTime == 0){
                curTime = 12;
            }
            selectIndex = curTime;
            selectIndex = selectIndex - 1;
            if (selectIndex < 0) {
                selectIndex = 11;
            }
            focusList.setBounds(recFocusListString1);
        } else if (type == MINUTE) {
            contents = null;
            contents = new String[12];

            for (int a = 0; a < 12; a++) {
                int b = a * 5;
                if (b < 10) {
                    contents[a] = "0" + String.valueOf(b);
                } else {
                    contents[a] = String.valueOf(b);
                }
            }

            focusList.setBounds(recFocusListString1);
            selectIndex = 0;
            selectIndex = selectIndex - 1;
            if (selectIndex < 0) {
                selectIndex = 11;
            }
        } else if (type == DAYS) {
            contents = null;
            contents = new String[3];

            if(!saveType){
                contents[0] = dataCenter.getString("manual.7days");
                contents[1] = dataCenter.getString("pvr.until_i_erase");
                contents[2] = dataCenter.getString("manual.14days");
            } else {
                contents[0] = dataCenter.getString("pvrportal.All");
                contents[1] = dataCenter.getString("pvrportal.Last3");
                contents[2] = dataCenter.getString("pvrportal.Last5");
            }
            focusList.setBounds(recFocusListString2);
            selectIndex = contents.length - 1;
        } else if (type == AMPM) {
            contents = null;
            contents = new String[2];
            contents[0] = dataCenter.getString("pvr.AM");
            contents[1] = dataCenter.getString("pvr.PM");

            focusList.setBounds(recFocusListString1);
        }

        maxView = 3;
        halfView = maxView / 2;
    }

    // yes, no
    boolean saveType;
    public void setSaveTime(boolean save){
        saveType = save;
    }

    public void setSelectTimeIndex(int index) {
        selectIndex = index;
        if(type == END_TIME24 || type == START_TIME24) {
            selectIndex = selectIndex - 1;
        } else {
            selectIndex = selectIndex - 2;
        }

        if (selectIndex < 0) {
            selectIndex = contents.length - 1;
        }

        Log.printDebug("selectIndex = " + selectIndex);
    }

    public void setSelectMinuteIndex(int index) {
        selectIndex = index / 5;
        selectIndex = selectIndex - 1;
        if (selectIndex < 0) {
            selectIndex = contents.length - 1;
        }
    }

    public void setSelectedAMPM(int ampm) {
        selectIndex = (ampm % 2);
    }


    public void setUpIndexFocus() {
        selectIndex--;
        if (selectIndex < 0) {
            selectIndex = contents.length - 1;
        }
        repaint();
    }

    public void setDownIndexFocus() {
        selectIndex++;
        if (selectIndex > contents.length - 1) {
            selectIndex = 0;
        }
        repaint();
    }

    public boolean handleKey(int code) {
        switch (code) {
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            break;
        case KeyEvent.VK_UP:
            setUpIndexFocus();
            repaint();
            break;
        case KeyEvent.VK_DOWN:
            setDownIndexFocus();
            repaint();
            break;
        case HRcEvent.VK_ENTER:
//        case KeyCodes.LAST:
//            stop();
            break;
        default:
            return false;
        }
        return true;
    }

    public String getContent() {
        return contents[(selectIndex + halfView) % contents.length];
    }

    public int getFocusIndex() {
        return (selectIndex + halfView) % contents.length;
    }

    class DayListRender extends Renderer {

        Image number_p_01;
        Image number_p;
        Image ars_t;
        Image ars_b;

        Image op134_bg_t;
        Image op134_bg_m;
        Image op134_bg_b;
        Image op135_foc;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (type == DAYS) {
                g.drawImage(op134_bg_b, 0, 58, c);
                g.drawImage(op134_bg_m, 0, 31, c);
                g.drawImage(op134_bg_t, 0, 15, c);

                g.drawImage(ars_t, 56, 0, c);
                g.drawImage(ars_b, 56, 74, c);

                g.drawImage(op135_foc, 2, 28, c);

            } else {
                g.drawImage(number_p_01, 0, 16, c);

                g.drawImage(ars_t, 8, 0, c);
                g.drawImage(ars_b, 8, 68, c);
            }
        }

        public void prepare(UIComponent c) {
            number_p_01 = DataCenter.getInstance().getImage("number_p_01.png");
            ars_t = DataCenter.getInstance().getImage("02_ars_t.png");
            ars_b = DataCenter.getInstance().getImage("02_ars_b.png");

            op134_bg_t = DataCenter.getInstance().getImage("08_op135_bg_t.png");
            op134_bg_m = DataCenter.getInstance().getImage("08_op135_bg_m.png");
            op134_bg_b = DataCenter.getInstance().getImage("08_op135_bg_b.png");

            op135_foc = DataCenter.getInstance().getImage("08_op135_foc.png");

            focusList.init();
            focusList.setVisible(true);
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    class FocusList extends Container {
        /**
         *
         */
        private static final long serialVersionUID = 1L;
        private Image op_sh_b;
        private Image op_sh_t;
        private Image op135_sh_b;
        private Image op135_sh_t;

        public void paint(Graphics g) {
            g.setFont(GraphicsResource.BLENDER18);

            if (type == DAYS) {
                for (int a = 0; a < 3; a++) {
                    g.setColor(a == 1 ? GraphicsResource.C_0_0_0 : GraphicsResource.C_189_189_189);
                    g.drawString(contents[(a + selectIndex) % contents.length], 20, 7 + 27 * a);
                }

                g.drawImage(op135_sh_t, 1, 0, this);
                g.drawImage(op135_sh_b, 1, 32, this);
            } else {
                g.setFont(GraphicsResource.BLENDER18);
                for (int a = 0; a < 3; a++) {
                    g.setColor(GraphicsResource.C_182_182_182);
                    GraphicUtil.drawStringCenter(g, contents[(a + selectIndex) % contents.length], 17, 7 + 23 * a);
                }
                g.drawImage(op_sh_t, 0, 0, this);
                g.drawImage(op_sh_b, 0, 25, this);
            }
        }

        public void init() {
            op_sh_b = DataCenter.getInstance().getImage("11_op_sh_b.png");
            op_sh_t = DataCenter.getInstance().getImage("11_op_sh_t.png");
            op135_sh_b = DataCenter.getInstance().getImage("08_op135_sh_b.png");
            op135_sh_t = DataCenter.getInstance().getImage("08_op135_sh_t.png");
        }
    }
}

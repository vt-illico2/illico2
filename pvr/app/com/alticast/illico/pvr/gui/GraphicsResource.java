package com.alticast.illico.pvr.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.util.FontResource;

public class GraphicsResource {

    public static final Color C_255_255_255 = new Color(255, 255, 255);
    public static final Color C_255_203_0 = new Color(255, 203, 0);
    public static final Color C_255_75_79 = new Color(255, 75, 79);
    public static final Color C_252_202_4 = new Color(252, 202, 4);
    public static final Color C_250_200_0 = new Color(250, 200, 0);
    public static final Color C_249_195_0 = new Color(249, 195, 0);
    public static final Color C_249_87_0 = new Color(249, 87, 0);

    public static final Color C_249_0_20 = new Color(249, 0, 20);
    public static final Color C_241_241_241 = new Color(241, 241, 241);
    public static final Color C_239_239_239 = new Color(239, 239, 239);
    public static final Color C_230_230_230 = new Color(230, 230, 230);
    public static final Color C_227_227_228 = new Color(227, 227, 228);
    public static final Color C_234_201_72 = new Color(234, 201, 72);
    public static final Color C_218_205_162 = new Color(218, 205, 162);
    public static final Color C_215_61_64 = new Color(215, 61, 64);
    public static final Color C_212_126_3 = new Color(212, 126, 3);
    public static final Color C_203_27_12 = new Color(203, 27, 12);
    public static final Color C_204_204_204 = new Color(204, 204, 204);
    public static final Color C_202_174_97 = new Color(202, 174, 97);
    public static final Color C_199_159_52 = new Color(199, 159, 92);
    public static final Color C_189_189_189 = new Color(189, 189, 189);
    public static final Color C_193_191_191 = new Color(193, 191, 191);
    public static final Color C_222_222_222 = new Color(222, 222, 222);
    public static final Color C_236_211_143 = new Color(236, 211, 143);
    public static final Color C_215_215_215 = new Color(215, 215, 215);
    public static final Color C_210_210_210 = new Color(210, 210, 210);
    public static final Color C_199_198_194 = new Color(199, 198, 194);
    public static final Color C_197_197_197 = new Color(197, 197, 197);
    public static final Color C_191_187_187 = new Color(191, 187, 187);
    public static final Color C_182_182_182 = new Color(182, 182, 182);
    public static final Color C_180_180_180 = new Color(180, 180, 180);
    public static final Color C_158_175_187 = new Color(158, 175, 187);
    public static final Color C_154_154_154 = new Color(154, 154, 154);
    public static final Color C_142_19_22 = new Color(142, 19, 22);
    public static final Color C_140_139_139 = new Color(140, 139, 139);
    public static final Color C_124_124_124 = new Color(124, 124, 124);
    public static final Color C_46_46_45 = new Color(46, 46, 45);
    public static final Color C_27_24_12 = new Color(27, 24, 12);
    public static final Color C_214_182_55 = new Color(214, 182, 55);
    //->Kenneth[2015.2.16] 4K flat design tuning
    public static final Color C_251_217_89 = new Color(251, 217, 89);
    public static final Color C_0_0_0_128= new DVBColor(0, 0, 0, 128);

    public static final Color C_13_13_13 = new Color(13, 13, 13);
    public static final Color C_4_4_4 = new Color(4, 4, 4);
    public static final Color C_3_3_3 = new Color(3, 3, 3);
    public static final Color C_0_0_0 = new Color(0, 0, 0);
    public static final Color C_24_24_24 = new Color(24, 24, 24);
    public static final Color C_33_33_33 = new Color(33, 33, 33);
    public static final Color C_35_35_35 = new Color(35, 35, 35);
    public static final Color C_37_37_37 = new Color(37, 37, 37);
    public static final Color C_41_41_41 = new Color(41, 41, 41);
    public static final Color C_50_50_50 = new Color(50, 50, 50);
    public static final Color C_53_53_53 = new Color(53, 53, 53);
    public static final Color C_91_91_91 = new Color(91, 91, 91);
    public static final Color C_78_78_78 = new Color(78, 78, 78);
    public static final Color C_111_111_111 = new Color(111, 111, 111);
    public static final Color C_118_118_118 = new Color(118, 118, 118);
    public static final Color C_178_178_178 = new Color(178, 178, 178);
    public static final Color C_200_200_200 = new Color(200, 200, 200);
    public static final Color C_229_229_229 = new Color(229, 229, 229);
    public static final Color C_240_240_240 = new Color(240, 240, 240);
    public static final Color C_255_122_33 = new Color(255, 122, 33);

    public static final DVBColor C_255_255_255_230 = new DVBColor(255, 255, 255, 230);
    public static final DVBColor C_227_227_227_170 = new DVBColor(227, 227, 227, 170);
    public static final DVBColor C_249_195_0_230 = new DVBColor(249, 195, 0, 230);
    public static final Color cDimmedBG = new DVBColor(12, 12, 12, 204);

    public static final Font BLENDER14 = FontResource.BLENDER.getFont(14);
    public static final Font BLENDER15 = FontResource.BLENDER.getFont(15);
    public static final Font BLENDER16 = FontResource.BLENDER.getFont(16);
    public static final Font BLENDER17 = FontResource.BLENDER.getFont(17);
    public static final Font BLENDER18 = FontResource.BLENDER.getFont(18);
    public static final Font BLENDER19 = FontResource.BLENDER.getFont(19);
    public static final Font BLENDER20 = FontResource.BLENDER.getFont(20);
    public static final Font BLENDER21 = FontResource.BLENDER.getFont(21);
    public static final Font BLENDER22 = FontResource.BLENDER.getFont(22);
    public static final Font BLENDER23 = FontResource.BLENDER.getFont(23);
    public static final Font BLENDER24 = FontResource.BLENDER.getFont(24);
    public static final Font BLENDER26 = FontResource.BLENDER.getFont(26);
    public static final Font BLENDER29 = FontResource.BLENDER.getFont(29);
    public static final Font BLENDER31 = FontResource.BLENDER.getFont(31);
    public static final Font BLENDER32 = FontResource.BLENDER.getFont(32);
    public static final Font BLENDER35 = FontResource.BLENDER.getFont(35);

    public static final Font DINMED14 = FontResource.DINMED.getFont(14);
    public static final Font DINMED15 = FontResource.DINMED.getFont(15);
    public static final Font DINMED16 = FontResource.DINMED.getFont(16);

    public static final Font BLENDER16_BOLD = FontResource.BLENDER.getBoldFont(16);
    public static final Font BLENDER18_BOLD = FontResource.BLENDER.getBoldFont(18);
    public static final Font BLENDER19_BOLD = FontResource.BLENDER.getBoldFont(19);

    public static final FontMetrics FM15 = FontResource.getFontMetrics(BLENDER15);
    public static final FontMetrics FM17 = FontResource.getFontMetrics(BLENDER17);
    public static final FontMetrics FM18 = FontResource.getFontMetrics(BLENDER18);
    //->Kenneth[2017.3.7] R7.3 에서 필요해서 추가
    public static final FontMetrics FM24 = FontResource.getFontMetrics(BLENDER24);
    //<-
}

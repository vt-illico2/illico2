package com.alticast.illico.pvr.gui;

import com.alticast.illico.pvr.ui.FullScreenPanel;
import com.alticast.illico.pvr.Core;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;
import java.util.*;

public abstract class FullScreenPanelRenderer extends Renderer {

    protected static DataCenter dataCenter = DataCenter.getInstance();

    protected Image iHisDim = dataCenter.getImage("his_dim.png");
    protected Image iHisOver = dataCenter.getImage("his_over.png");
    protected Image iClock = dataCenter.getImage("clock.png");
    protected Image iClockBg = dataCenter.getImage("clock_bg.png");
    protected Image iLogo;

    protected static Color cDim = new Color(124, 124, 124);
    protected static Color cOver = new Color(239, 239, 239);
    protected static Color cClock = Color.white;

    protected static Font fText = FontResource.BLENDER.getFont(18);
    protected static Font fClock = FontResource.BLENDER.getFont(17);

    protected static FontMetrics fmText = FontResource.getFontMetrics(fText);
    protected static FontMetrics fmClock = FontResource.getFontMetrics(fClock);

    public FullScreenPanelRenderer() {
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    public void prepare(UIComponent c) {
        iLogo = (Image) SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    private static final int LOGO_X = 53;
    private static final int LOGO_Y = 21;

    protected int clockX = 742;

    protected abstract void paintBackground(Graphics g, FullScreenPanel p);

    protected void paint(Graphics g, UIComponent c) {
        FullScreenPanel p = (FullScreenPanel) c;
        paintBackground(g, p);

        Image im = iLogo;
        g.drawImage(im, LOGO_X, LOGO_Y, c);

        Vector v = p.stack;
        if (v != null) {
            int x = (im != null ? im.getWidth(c) : 0) + LOGO_X + 15;
            int size = v.size();
            g.setFont(fText);
            Color color;
            String s;
            for (int i = 0; i < size; i++) {
                if (i == size - 1) {
                    im = iHisOver;
//                    g.drawImage(im, x, LOGO_Y + 28, c);
//                    x += 6;
                    g.drawImage(im, x, LOGO_Y + 24, c);
                    x += 16;
                    color = cOver;
                } else {
                    im = iHisDim;
                    g.drawImage(im, x, LOGO_Y + 24, c);
                    x += 16;
                    color = cDim;
                }
                g.setColor(color);
                s = dataCenter.getString((String) v.elementAt(i));
                g.drawString(s, x, LOGO_Y + 36);
                x += fmText.stringWidth(s) + 14;
            }
        }

        if (!Core.standAloneMode) {
            Formatter formatter = Formatter.getCurrent();
            g.setColor(cClock);
            g.setFont(fClock);
            g.drawImage(iClockBg, 749, 37, c);
            String clockString = formatter.getLongDate();
            GraphicUtil.drawStringRight(g, clockString, 910, 57);
            g.drawImage(iClock, 910 - fmClock.stringWidth(clockString) - 24, 44, c);
        }
    }

}

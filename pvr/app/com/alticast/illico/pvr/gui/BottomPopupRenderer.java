package com.alticast.illico.pvr.gui;

import java.awt.*;
import java.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.popup.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;

public class BottomPopupRenderer extends Renderer {
    public static final int MAX_TEXT_WIDTH = 800;

    private int textY = 393;
    private static final int Y_GAP = 24;

    int topX = 268;

    DataCenter dataCenter = DataCenter.getInstance();
    Image i_07_pop_sha = dataCenter.getImage("07_pop_sha.png");
    Image i_large_noti_b = dataCenter.getImage("large_noti_b.png");
    Image i_large_noti_m = dataCenter.getImage("large_noti_m.png");
    Image i_large_noti_t = dataCenter.getImage("large_noti_t.png");
    Image i_01_hotkeybg = dataCenter.getImage("01_hotkeybg.png");
    Image iHd = dataCenter.getImage("icon_hd.png");

    Image i_btn_210_foc = dataCenter.getImage("btn_210_foc.png");
    Image i_btn_210_l = dataCenter.getImage("btn_210_l.png");
    Image i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
    Image i_btn_293_l = dataCenter.getImage("btn_293_l.png");

    Image titleIcon = null;

    Color cChannel = new Color(255, 255, 255);
    Font fChannel = GraphicsResource.BLENDER19;

    Color cText = Color.white;
    public static Font fText = GraphicsResource.BLENDER19;
    public static FontMetrics fmText = FontResource.getFontMetrics(fText);

    Color cButton = Color.black;
    public static Font fButton = GraphicsResource.BLENDER18;
    public static FontMetrics fmButton = FontResource.getFontMetrics(fButton);

    Color cTitle = new Color(255, 203, 0);
    Font fTitle = GraphicsResource.BLENDER24;

    boolean checkLong = false;

    //->Kenneth[2015.2.16] 4K flat design tuning
    private Color footerLineColor = new Color(50, 50, 50);

    public BottomPopupRenderer() {
        this(268);
    }

    public BottomPopupRenderer(int topX) {
        this.topX = topX;
    }

    public void setTextYPosition(int textY) {
        this.textY = textY;
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return null;
    }

    public Rectangle getButtonFocusBounds(BottomPopup c) {
        String[] buttons = c.buttons;
        if (buttons == null) {
            return null;
        }
        if (buttons.length == 2) {
            if (checkLong) {
                if (c.getFocus() == 0) {
                    return new Rectangle(181+1, 435, 293, 32);
                } else {
                    return new Rectangle(485+1, 435, 293, 32);
                }
            } else {
                if (c.getFocus() == 0) {
                    return new Rectangle(265+1, 435, 210, 32);
                } else {
                    return new Rectangle(485+1, 435, 210, 32);
                }
            }
        } else if (buttons.length == 1) {
            if (checkLong) {
                return new Rectangle(333+1, 435, 293, 32);
            } else {
                return new Rectangle(374+1, 435, 293, 32);
            }
        } else {
            return null;
        }
    }

    protected void paint(Graphics g, UIComponent c) {
        BottomPopup p = (BottomPopup) c;
        if (p.currentPopup != null) {
            return;
        }
        g.drawImage(i_07_pop_sha, 0, 79 + topX, 960, 171, c);
        int height = 290 + 50 - topX;
        if (height > 0) {
            g.drawImage(i_large_noti_m, 0, 70 + topX, 960, height, c);
        }
        g.drawImage(i_large_noti_t, 0, 0 + topX, c);
        g.drawImage(i_large_noti_b, 0, 410, c);

        g.setColor(cTitle);
        g.setFont(fTitle);
        int sx = 55;
        if (titleIcon != null) {
            g.drawImage(titleIcon, sx, 84 - 19 + topX, c);
            sx += titleIcon.getWidth(c) + 7;
        } else {
            sx = 79;
        }
        g.drawString(p.title, sx, 84 + topX);

        paintBody(g, p);

        String[] buttons = p.buttons;
        if (buttons == null) {
            return;
        }
        //->Kenneth[2015.2.16] 4K flat design tuning
        // line 이미지를 없애고 java 컬러로 footer 구분선 그려준다
        g.setColor(footerLineColor);
        g.fillRect(0, 483, 960, 1);
        //g.drawImage(i_01_hotkeybg, 236, 466, c);

        g.setColor(cButton);
        g.setFont(fButton);
        if (buttons.length == 2) {
            int focus = p.getFocus();
            if (checkLong) {
                g.drawImage(focus == 0 ? i_btn_293_foc : i_btn_293_l, 181, 435, c);
                g.drawImage(focus == 1 ? i_btn_293_foc : i_btn_293_l, 485, 435, c);
                GraphicUtil.drawStringCenter(g, buttons[0], 330, 456);
                GraphicUtil.drawStringCenter(g, buttons[1], 631, 456);
            } else {
                g.drawImage(focus == 0 ? i_btn_210_foc : i_btn_210_l, 265, 435, c);
                g.drawImage(focus == 1 ? i_btn_210_foc : i_btn_210_l, 485, 435, c);
                GraphicUtil.drawStringCenter(g, buttons[0], 369, 456);
                GraphicUtil.drawStringCenter(g, buttons[1], 589, 456);
            }
        } else if (buttons.length == 1) {
            if (checkLong) {
                g.drawImage(i_btn_293_foc, 333, 435, c);
            } else {
                g.drawImage(i_btn_210_foc, 374, 435, c);
            }
            GraphicUtil.drawStringCenter(g, buttons[0], 480, 456);
        }
    }

    protected void paintBody(Graphics g, BottomPopup p) {
        g.setColor(cText);
        g.setFont(fText);
        int fs = fText.getSize();

        int y = textY - ((p.texts.length - 1) * Y_GAP - fs) / 2 - 3;
        for (int i = 0; i < p.texts.length; i++) {
            GraphicUtil.drawStringCenter(g, p.texts[i], 480, y);
            y = y + Y_GAP;
        }
    }

    public void prepare(UIComponent c) {
        BottomPopup p = (BottomPopup) c;
        this.checkLong = false;
        if (p.buttons != null) {
            for (int i = 0; i < p.buttons.length; i++) {
                if (fmButton.stringWidth(p.buttons[i]) > 200) {
                    checkLong = true;
                    break;
                }
            }
        }
        if (p.iconName != null) {
            titleIcon = dataCenter.getImage(p.iconName);
        }
        Log.printDebug(App.LOG_HEADER+"BottomPopupRenderer.prepare: checkLong = " + checkLong);
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
}

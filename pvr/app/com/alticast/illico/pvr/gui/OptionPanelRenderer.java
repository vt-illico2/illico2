package com.alticast.illico.pvr.gui;

import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ui.MenuItem;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import java.awt.event.KeyEvent;
import java.awt.*;
import org.ocap.ui.event.OCRcEvent;
import java.util.*;

public abstract class OptionPanelRenderer extends FullScreenPanelRenderer {

    public static final int BUTTON_Y_GAP = 39;
    protected int SHADOW_X = 579;
    protected int SHADOW_Y = 467;

    protected Image iAc1 = dataCenter.getImage("01_acbg_1st.png");
    protected Image iAc2 = dataCenter.getImage("01_acbg_2nd.png");
    protected Image iAcShadow = dataCenter.getImage("01_ac_shadow.png");
    protected Image iAcGlow = dataCenter.getImage("01_acglow.png");
    protected Image iAcMiddle = dataCenter.getImage("01_acbg_m.png");
    protected Image iAcLine = dataCenter.getImage("01_acline.png");

    protected Image iBullet = dataCenter.getImage("02_detail_ar.png");
    protected Image iDisabledBullet = dataCenter.getImage("02_detail_ar_dim.png");
    protected Image iFocus = dataCenter.getImage("02_detail_bt_foc.png");

    protected static Font fButton = FontResource.BLENDER.getFont(18);
    protected static Font fFocusedButton = FontResource.BLENDER.getFont(21);
    protected static FontMetrics fmButton = FontResource.getFontMetrics(fButton);
    protected static FontMetrics fmFocusedButton = FontResource.getFontMetrics(fFocusedButton);

    protected static Color cButton = new Color(230, 230, 230);
    protected static Color cDisabledButton = new Color(110, 110, 110);
    protected static Color cFocusedButton = new Color(3, 3, 3);

    public OptionPanelRenderer() {
    }

    public Rectangle getButtonFocusBounds(OptionPanel c) {
        MenuItem menu = c.buttons;
        if (menu == null) {
            return null;
        }
        int size = menu.size();
        if (size <= 0) {
            return null;
        }
        int focus = c.getFocus();
        if (focus < 0) {
            return null;
        }
        int tx = SHADOW_X - 579 + 626 + 2;
        int ty = (3 - size + focus) * BUTTON_Y_GAP + SHADOW_Y + 184 - 467 + 173;
        return new Rectangle(tx, ty, 284, 41);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        paintButtons(g, c, ((OptionPanel) c).buttons);
    }

    /** Button */
    protected void paintButtons(Graphics g, UIComponent c, MenuItem menu) {
        if (menu == null) {
            return;
        }
        int size = menu.size();
        if (size <= 0) {
            return;
        }
        int focus = c.getFocus();
        int h = size * BUTTON_Y_GAP;

        g.translate(SHADOW_X - 579, (3 - size) * BUTTON_Y_GAP + SHADOW_Y + 184 - 467);
        g.drawImage(iAc1, 628, 173, c);
        if (size > 1) {
            g.drawImage(iAc2, 628, 216, c);
            g.drawImage(iAcMiddle, 628, 252, 284, h - 77, c);
        }

        g.drawImage(iAcGlow, 627, 154 + h, c);
        g.drawImage(iAcShadow, 581, 166 + h, c);

        int y = 213;
        for (int i = 0; i < size - 1; i++) {
            g.drawImage(iAcLine, 643, y, c);
            y += BUTTON_Y_GAP;
        }

        if (focus >= 0) {
            g.drawImage(iFocus, 626, 173 + focus * BUTTON_Y_GAP, c);
        }

        String s;
        y = 199;
        for (int i = 0; i < size; i++) {
            MenuItem item = menu.getItemAt(i);
            //->Kenneth[2017.3.15] R7.3 
            // PVR 의 Recorded List 에서 resume viewing 의 경우 MenuItem 에서 TOC 가 아닌 남은 시간을
            // toggle 로 보여줘야 한다.
            // 따라서 MenuItem 에 displayableText 를 추가한다.
            // displayableText 가 null 이 아니면 displayableText 를 그리고
            // null 인 경우 기존대로 key 를 가지고 TOC 로 그리는 코드를 OptionPanelRenderer 에 구현한다.
            if (item.getDisplayableText() == null) {
                s = dataCenter.getString(item.getKey());
            } else {
                s = item.getDisplayableText();
            }
            //<-
            if (i == focus) {
                g.setColor(cFocusedButton);
                g.setFont(fFocusedButton);
                s = TextUtil.shorten(s, fmFocusedButton, 238);
                g.drawString(s, 671, y + 1);
                //->Kenneth[2017.3.15] R7.3 
                if (item.focusedIcon != null) {
                    g.drawImage(item.focusedIcon, 651-12, y - 8-10, c);
                }
                //<-
            } else {
                Image im;
                Color color;
                if (item.isEnabled()) {
                    im = iBullet;
                    color = cButton;
                } else {
                    im = iDisabledBullet;
                    color = cDisabledButton;
                }
                //g.drawImage(im, 651, y - 8, c);
                g.setColor(color);
                g.setFont(fButton);
                s = TextUtil.shorten(s, fmButton, 235);
                g.drawString(s, 673, y);
                //->Kenneth[2017.3.15] R7.3 
                if (item.unfocusedIcon == null) {
                    g.drawImage(im, 651, y - 8, c);
                } else {
                    g.drawImage(item.unfocusedIcon, 651-12, y - 8-10, c);
                }
                //<-
            }
            y += BUTTON_Y_GAP;
        }
        g.translate(-(SHADOW_X - 579), -(3 - size) * BUTTON_Y_GAP - (SHADOW_Y + 184 - 467));
    }

}

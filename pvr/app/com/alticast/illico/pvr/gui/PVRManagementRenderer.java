package com.alticast.illico.pvr.gui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.alticast.illico.pvr.MenuController;
import com.alticast.illico.pvr.RecordingListManager;
import com.alticast.illico.pvr.StorageInfoManager;
import com.alticast.illico.pvr.ui.PVRManagement;
import com.alticast.illico.pvr.ui.FullScreenPanel;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;

public class PVRManagementRenderer extends OptionPanelRenderer {
    private Image backgound;
    private Image hotkeybg;

    private Image i_01_acglow;
    private Image i_01_acbg_1st;
    private Image i_01_acbg_2nd;
    private Image i_01_ac_shadow;
    private Image i_01_acline;

    private Image i_02_detail_bt_foc;
    private Image i_02_detail_ar;

    private Image i_07_roundbg;
    private Image i_07_roundcover;

    private Image g_color_gray;
    private Image g_color_y;
    private Image g_color_o;
    private Image g_color_r;

    private Image i_07_disksep;
    private Image i_07_capabg;
    //->Kenneth[2015.2.3]
    private Image i_07_capabg_uhd;

    private String pvrManagement = dataCenter.getString("management.PVR_Management");
    private String totalrecordings = dataCenter.getString("management.totalrecordings");
    private String availableCapacity = dataCenter.getString("management.availableCapacity");
    private String usedspace = dataCenter.getString("management.usedspace");
    private String emptyspace = dataCenter.getString("management.emptyspace");
    private String freespace = dataCenter.getString("management.freespace");

    public PVRManagementRenderer() {
    }

    protected void paintBackground(Graphics g, FullScreenPanel c) {
        g.drawImage(backgound, 0, 0, c);
    }

    protected void paint(Graphics g, UIComponent c) {
        super.paint(g, c);
        PVRManagement p = (PVRManagement) c;

        g.setFont(GraphicsResource.BLENDER31);
        g.setColor(GraphicsResource.C_255_255_255);
        g.drawString(pvrManagement, 58, 106);

        g.drawImage(i_07_roundbg, 91, 128, c);
        // g.drawImage(i_07_diskinfo, 137, 361, c);
        // 3.6 씩 올라가니까 개선 하자

        double free = StorageInfoManager.getInstance().getFreeRatio();
        int graphAngle = (int) ((1 - free) * 360);

        int freePercent = (int) (free * 100);
        g.setFont(GraphicsResource.BLENDER35);

        // draw circle graph
        if (freePercent < StorageInfoManager.CRITICAL_LEVEL) {
            g.setColor(GraphicsResource.C_249_0_20);
        } else if (freePercent < StorageInfoManager.WARNING_LEVEL) {
            g.setColor(GraphicsResource.C_249_87_0);
        } else {
            g.setColor(GraphicsResource.C_249_195_0);
        }

        g.fillArc(192, 178, 183, 183, 90, -graphAngle);
        g.drawImage(i_07_roundcover, 91, 128, c);

        g.setColor(GraphicsResource.C_140_139_139);
        GraphicUtil.drawStringRight(g, freePercent + "%", 319, 271);
        g.setFont(GraphicsResource.BLENDER22);
        GraphicUtil.drawStringRight(g, freespace, 332, 292);

        if (freePercent < StorageInfoManager.CRITICAL_LEVEL) {
            g.setColor(GraphicsResource.C_249_0_20);
        } else if (freePercent < StorageInfoManager.WARNING_LEVEL) {
            g.setColor(GraphicsResource.C_249_87_0);
        } else {
            g.setColor(GraphicsResource.C_249_195_0);
        }

        g.drawImage(g_color_gray, 297, 383, c);
        if (freePercent < StorageInfoManager.CRITICAL_LEVEL) {
            g.drawImage(g_color_r, 137, 383, c);
        } else if (freePercent < StorageInfoManager.WARNING_LEVEL) {
            g.drawImage(g_color_o, 137, 383, c);
        } else {
            g.drawImage(g_color_y, 137, 383, c);
        }

        g.setColor(GraphicsResource.C_255_255_255);
        g.setFont(GraphicsResource.BLENDER20);
        g.drawString(usedspace, 163, 398);
        g.drawString(emptyspace, 322, 398);

        g.setFont(GraphicsResource.BLENDER18);

        if (Log.EXTRA_ON) {
            g.setColor(java.awt.Color.green);
            g.drawString("MS Config = " + StorageInfoManager.WARNING_LEVEL + "% , " + StorageInfoManager.CRITICAL_LEVEL + "%", 80, 150);

//            g.drawString(String.valueOf(-graphAngle), 61, 128);
//            g.fillArc(61, 130, 20, 20, 90, -graphAngle);
//            g.setColor(java.awt.Color.magenta);
//            g.fillArc(91, 130, 20, 20, 90, -355);
//            g.fillArc(130, 130, 40, 40, 90, -355);
//            g.fillArc(180, 130, 60, 60, 90, -355);
//            g.fillArc(250, 130, 80, 80, 90, -355);
//            g.fillArc(340, 130, 100, 100, 90, -355);
        }

        g.setColor(GraphicsResource.C_249_195_0);

        String freeString = StorageInfoManager.getSizeString2(StorageInfoManager.getInstance().getFreeSpace());
        long freed = PVRManagement.freedBytes;
        String s;
        if (freed > 0) {
            s = TextUtil.replace(dataCenter.getString("management.message1"),
                            "%%%", StorageInfoManager.getSizeString2(freed));
            GraphicUtil.drawStringCenter(g, s, 287, 434);
            s = freeString + " " + dataCenter.getString("management.message2");
            GraphicUtil.drawStringCenter(g, s, 287, 434 + 18);
        }

        // draw right panel
        g.setColor(GraphicsResource.C_178_178_178);
        g.setFont(GraphicsResource.BLENDER17);
        s = totalrecordings + " :";
        g.drawString(s, 485, 210);
        int tx = g.getFontMetrics().stringWidth(s);
        g.drawString(availableCapacity + " :", 485, 251);

        g.drawImage(i_07_disksep, 418, 226, c);
        //->Keneth[2015.5.8] 4K : VDTRMASTER-5412 : UHD 는 4K 박스에서만
        if (Environment.SUPPORT_UHD) {
            g.drawImage(i_07_capabg_uhd, 485, 271, c);
        } else {
            g.drawImage(i_07_capabg, 485, 271, c);
        }
        //->Kenneth[2015.2.3] 4K
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_255_255_255_230);
        g.drawString(String.valueOf(p.recordingCount), tx + 10 + 485, 212);

        GraphicUtil.drawStringRight(g, freeString, 555, 303);

        //->Keneth[2015.5.8] 4K : VDTRMASTER-5412 : UHD 는 4K 박스에서만
        if (Environment.SUPPORT_UHD) {
            int uhd = StorageInfoManager.getInstance().getAvailableDuration(Constants.DEFINITION_4K);
            s = (uhd / 60) + "h " + (uhd % 60) + "min";
            GraphicUtil.drawStringCenter(g, s, 633, 317);

            int hd = StorageInfoManager.getInstance().getAvailableDuration(Constants.DEFINITION_HD);
            s = (hd / 60) + "h " + (hd % 60) + "min";
            GraphicUtil.drawStringCenter(g, s, 633+97, 317);

            int sd = StorageInfoManager.getInstance().getAvailableDuration(Constants.DEFINITION_SD);
            s = (sd / 60) + "h " + (sd % 60) + "min";
            GraphicUtil.drawStringCenter(g, s, 633+97*2, 317);
        } else {
            int hd = StorageInfoManager.getInstance().getAvailableDuration(Constants.DEFINITION_HD);
            s = (hd / 60) + "h " + (hd % 60) + "min";
            GraphicUtil.drawStringCenter(g, s, 633, 317);

            int sd = StorageInfoManager.getInstance().getAvailableDuration(Constants.DEFINITION_SD);
            s = (sd / 60) + "h " + (sd % 60) + "min";
            GraphicUtil.drawStringCenter(g, s, 633+97, 317);
        }

//        g.drawImage(i_01_acbg_1st, 626, 389, c);
//        g.drawImage(i_01_acbg_2nd, 626, 432, c);
//        g.drawImage(i_01_acline, 641, 427, c);
//        g.drawImage(i_01_acglow, 625, 447, c);
//        g.drawImage(i_01_ac_shadow, 579, 459, c);
//
//        if (c.getFocus() == 0) {
//            g.drawImage(i_02_detail_bt_foc, 624, 389, c);
//            g.drawImage(i_02_detail_ar, 649, 444, c);
//            g.setFont(GraphicsResource.BLENDER21);
//            g.setColor(GraphicsResource.C_4_4_4);
//            g.drawString(dataCenter.getString("management.btnWIZARD"), 671, 416);
//            g.setFont(GraphicsResource.BLENDER18);
//            g.setColor(GraphicsResource.C_230_230_230);
//            g.drawString(dataCenter.getString("management.GoRecordedList"), 671, 413 + 38);
//        } else {
//            g.drawImage(i_02_detail_bt_foc, 624, 390 + 38, c);
//            g.drawImage(i_02_detail_ar, 649, 405, c);
//            g.setFont(GraphicsResource.BLENDER18);
//            g.setColor(GraphicsResource.C_230_230_230);
//            g.drawString(dataCenter.getString("management.btnWIZARD"), 671, 413);
//            g.setFont(GraphicsResource.BLENDER21);
//            g.setColor(GraphicsResource.C_4_4_4);
//            g.drawString(dataCenter.getString("management.GoRecordedList"), 671, 416 + 38);
//        }
        rightBottomButton(g, c);
    }

    public void rightBottomButton(Graphics g, UIComponent c) {
        g.drawImage(hotkeybg, 242, 466, c);
    }

    public void prepare(UIComponent c) {
        pvrManagement = dataCenter.getString("management.PVR_Management");
        totalrecordings = dataCenter.getString("management.totalrecordings");
        availableCapacity = dataCenter.getString("management.availableCapacity");
        usedspace = dataCenter.getString("management.usedspace");
        emptyspace = dataCenter.getString("management.emptyspace");
        freespace = dataCenter.getString("management.freespace");

        backgound = dataCenter.getImage("bg.jpg");

        hotkeybg = dataCenter.getImage("01_hotkeybg.png");

        i_01_acglow = dataCenter.getImage("01_acglow.png");
        i_01_acbg_1st = dataCenter.getImage("01_acbg_1st.png");
        i_01_acbg_2nd = dataCenter.getImage("01_acbg_2nd.png");
        i_01_ac_shadow = dataCenter.getImage("01_ac_shadow.png");
        i_01_acline = dataCenter.getImage("01_acline.png");

        i_02_detail_bt_foc = dataCenter.getImage("02_detail_bt_foc.png");
        i_02_detail_ar = dataCenter.getImage("02_detail_ar.png");

        i_07_roundbg = dataCenter.getImage("07_roundbg.png");
        i_07_roundcover = dataCenter.getImage("07_rouudcover.png");
        g_color_gray = dataCenter.getImage("g_color_gray.png");
        g_color_y = dataCenter.getImage("g_color_y.png");
        g_color_o = dataCenter.getImage("g_color_o.png");
        g_color_r = dataCenter.getImage("g_color_r.png");
        i_07_disksep = dataCenter.getImage("07_disksep.png");
        i_07_capabg = dataCenter.getImage("07_capabg.png");
        i_07_capabg_uhd = dataCenter.getImage("07_capabg_uhd.png");

        super.prepare(c);
    }
}

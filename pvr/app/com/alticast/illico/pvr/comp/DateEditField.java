package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public class DateEditField extends EditField {

    private static final int MAX_DAYS = 7;

    long startDay = 0;
    DateFieldSelector selector;
    long selectedDay;

    OptionSelectionListener listener;

    public DateEditField(String name, long start) {
        super(name, true);
        selector = new DateFieldSelector(name);
        startDay = start;
        String[] options = new String[MAX_DAYS + 1];
        for (int i = 0; i < MAX_DAYS; i++) {
            options[i] = String.valueOf(i);
        }
        options[MAX_DAYS] = PvrUtil.MORE_DAYS;
        selector.setData(options);
        selector.selected(0);
    }

    public void setDay(long date) {
        if (Log.DEBUG_ON) {
            Log.printDebug("DateEditField.setDay = " + new Date(date) + ", " + date);
        }
        selectedDay = date;
        String value = selector.getDisplayValue();
        for (int i = 0; i < MAX_DAYS; i++) {
            if (value.equals(selector.getDisplayValue(i))) {
                selector.focus = i;
                selector.selectedIndex = i;
                selector.selectedKey = selector.values[i];
                return;
            }
        }
    }

    public long getDay() {
        return selectedDay;
    }

    public void setInitialValue(String current) {
        selector.setInitialValue(current);
    }

    public void setCurrentValue(String current) {
        selector.setCurrentValue(current);
    }

    public void setSelectedKey(String key) {
        selector.setSelectedKey(key);
    }

    public void setListener(OptionSelectionListener l) {
        listener = l;
    }

    public boolean handleKey(int code) {
        boolean ret = selector.handleKey(code);
        if (ret) {
            return true;
        }
        switch (code) {
            case OCRcEvent.VK_LEFT:
                return true;
            case OCRcEvent.VK_RIGHT:
                return false;
            case OCRcEvent.VK_ENTER:
                selector.start();
                return true;
        }
        return false;
    }

    public boolean isEditing() {
        return selector.started;
    }

    public Object getChangedValue() {
        return selector.getChangedValue();
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        selector.paint(g, hasFocus, c);
    }

    class DateFieldSelector extends StringFieldSelector {

        public DateFieldSelector(String title) {
            super(0, 0, title, false);
        }

        public String getDisplayValue() {
            return Formatter.getCurrent().getLongDayText(selectedDay);
        }

        public String getDisplayValue(int index) {
            index = (index + size) % size;
            if (index == MAX_DAYS) {
                return super.getDisplayValue(index);
            } else {
                return Formatter.getCurrent().getLongDayText(startDay + Constants.MS_PER_DAY * index);
            }
        }

        public void selected(int index) {
            if (index == MAX_DAYS) {
                if (DateEditField.this.listener != null) {
                    DateEditField.this.listener.selectionChanged(values[index]);
                }
            } else {
                selectedDay = startDay + Constants.MS_PER_DAY * index;
                super.selected(index);
            }
        }
    }
}


package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public class RepeatEditField extends EditField implements OptionSelectionListener {

    StringFieldSelector selector;
    public String repeatText;

//    boolean dayEditable = false;
    String currentDay = null;
    int dayBitMap = 0;
    String repeatString;
    boolean repeat;

    boolean manual = false;
    String initialKey;

    public static final String KEY_EVERY = "pvr.Every...";
    public static final String KEY_ALL = "pvr.All";
    public static final String KEY_NO = "pvr.No";

    OptionSelectionListener listener;

    public RepeatEditField(String name, boolean editable, boolean repeat, boolean manual, String repeatString) {
        super(name, editable);
        selector = new StringFieldSelector(0, 0, name, false);
        this.repeatString = repeatString;
        this.repeat = repeat;
        this.manual = manual;

        if (Log.DEBUG_ON) {
            Log.printDebug("RepeatEditField: " + repeat + ", " + manual + ", " + repeatString);
        }
//        if (editable) {
//            if (repeat) {
//                dayEditable = (repeatString != null && !RecordingScheduler.REPEAT_ALL_STRING.equals(repeatString));
//            } else {
//                dayEditable = (seriesId == null);
//            }
//        }
        resetOption();
}

    public RepeatEditField(String name, boolean editable, String groupKey, String repeatString) {
        this(name, editable, groupKey != null,
            groupKey != null && groupKey.indexOf(RecordingScheduler.GROUP_KEY_MARKER + "" + RecordingScheduler.MANUAL_REPEAT) >= 0,
            repeatString);
    }

    public void resetOption() {
        String[] options;
//        if (dayEditable) {
            if (manual) {
                options = new String[] { KEY_NO, KEY_EVERY };
            } else {
                options = new String[] { KEY_NO, KEY_ALL, KEY_EVERY };
            }
            if (repeat) {
                resetDays(repeatString);
            } else {
                dayBitMap = 0x7F;
            }
//        } else {
//            options = new String[] { KEY_NO, KEY_ALL };
//        }
        selector.setListener(this);
        setOptions(options);
        String initKey;
        if (!repeat) {
            initKey = KEY_NO;
        } else if ((repeatString != null && !RecordingScheduler.REPEAT_ALL_STRING.equals(repeatString))) {
            initKey = KEY_EVERY;
        } else {
            initKey = KEY_ALL;
            currentDay = null;
        }
        this.initialKey = initKey;
        setInitialValue(DataCenter.getInstance().getString(initKey));
    }

    private void resetDays(String repeat) {
        try {
            dayBitMap = Integer.parseInt(repeat, 2);
        } catch (Exception ex) {
            dayBitMap = 0;
        }
        resetCurrentDay();
    }

    private void resetCurrentDay() {
        String s = PvrUtil.getRepeatText(dayBitMap);
        currentDay = TextUtil.shorten(s, GraphicsResource.FM18, 124);
    }

    public void setInitialValue(String value) {
        selector.setInitialValue(value);
    }

    public void setCurrentValue(String current) {
        selector.setCurrentValue(current);
    }

    public void setOptions(String[] options) {
        selector.setData(options);
    }

    public void setListener(OptionSelectionListener l) {
        listener = l;
    }

    public void selectionChanged(Object key) {
//        if (dayEditable) {
            if (KEY_EVERY.equals(key)) {
                if (dayBitMap == 0) {
                    dayBitMap = 0x7F;
                }
                resetCurrentDay();
            } else {
                if (repeatString != null) {
                    resetDays(repeatString);
                }
                currentDay = null;
            }
//        }
        if (listener != null) {
            listener.selectionChanged(key);
        }
    }

    public boolean[] getCurrentDays() {
        boolean[] ret = new boolean[7];
        int mask = 0x40;
        int days = dayBitMap;
        for (int i = 0; i < 7; i++) {
            ret[i] = ((mask & days) != 0);
            mask = mask >> 1;
        }
        return ret;
    }

    private static String toRepeatString(int days) {
        char[] ch = new char[7];
        int mask = 0x40;
        for (int i = 0; i < 7; i++) {
            ch[i] = ((mask & days) != 0) ? '1' : '0';
            mask = mask >> 1;
        }
        return new String(ch);
    }

    public void setDays(boolean[] array) {
        int days = 0;
        for (int i = 0; i < array.length; i++) {
            int bit = array[i] ? 1 : 0;
            days = (days << 1) | bit;
        }
        dayBitMap = days;
        if (dayBitMap == 0) {
            currentDay = null;
            selector.focus = 0;
            selector.setCurrentValue(selector.getDisplayValue(0));
            selector.selectedIndex = 0;
            this.focus = 0;
            if (listener != null) {
                listener.selectionChanged(KEY_NO);
            }
        } else {
            resetCurrentDay();
        }
    }

    public boolean handleKey(int code) {
        if (!isEditable) {
            return false;
        }
        if (focus == 0) {
            boolean ret = selector.handleKey(code);
            if (ret) {
                return true;
            }
        }
        switch (code) {
            case OCRcEvent.VK_LEFT:
                focus = 0;
                return true;
            case OCRcEvent.VK_RIGHT:
                if (currentDay != null) {
                    if (focus != 1) {
                        focus = 1;
                        return true;
                    }
                }
                return false;
            case OCRcEvent.VK_ENTER:
                if (focus == 0) {
                    selector.start();
                } else {
                    return false;
                }
                return true;
        }
        return false;
    }

    public boolean isEditing() {
        return selector.started;
    }

    public Object getChangedValue() {
        Object key = selector.getChangedValue();
        if (key == null) {
            if (initialKey == KEY_EVERY) {
                // every 인 상태에서 요일만 바꿀 수 있다.
                key = KEY_EVERY;
            } else {
                return null;
            }
        }

        if (repeat) {
            if (KEY_NO.equals(key)) {
                return KEY_NO;  // non-repeat
            } else if (KEY_ALL.equals(key)) {
                if (initialKey == KEY_ALL) {
                    return null;    // not changed
                } else {
                    return RecordingScheduler.REPEAT_ALL_STRING;
                }
            } else { // KEY_EVERY
                String rp = toRepeatString(dayBitMap);
                if (rp.equals(repeatString)) {
                    return null;
                } else {
                    return rp;
                }
            }
        } else {
            if (KEY_NO.equals(key)) {
                return null;
            } else if (KEY_ALL.equals(key)) {
                return RecordingScheduler.REPEAT_ALL_STRING;
            } else { // KEY_EVERY
                return toRepeatString(dayBitMap);
            }
        }
    }

    public String getRepeatString() {
        Object key = selector.getSelectedValue();
        if (KEY_NO.equals(key)) {
            return null;
        } else if (KEY_ALL.equals(key)) {
            return RecordingScheduler.REPEAT_ALL_STRING;
        } else { // KEY_EVERY
            return toRepeatString(dayBitMap);
        }
    }

    Image i_input_142 = dataCenter.getImage("input_142.png");
    Image i_input_142_foc = dataCenter.getImage("input_142_foc.png");

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_182_182_182);
        if (!isEditable) {
            g.drawString(repeatText, 1, 21);
            return;
        }
        if (currentDay != null) {
            if (hasFocus && focus == 1) {
                g.drawImage(i_input_142_foc, 536-387, 0, c);
                g.setColor(Color.white);
            } else {
                g.drawImage(i_input_142, 537-387, 0, c);
            }
            g.drawString(currentDay, 550-387, 21);
        }
        selector.paint(g, hasFocus && focus == 0, c);
    }
}


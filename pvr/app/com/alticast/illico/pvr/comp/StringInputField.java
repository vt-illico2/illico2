package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import java.rmi.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.pvr.communication.KeyboardListenerAction;

public class StringInputField extends EditField implements KeyboardListener {

    Image iNormal = dataCenter.getImage("input_292.png");
    Image iFocus = dataCenter.getImage("input_292_foc_nor.png");

    String value;

    boolean started = false;

    public StringInputField(String name) {
        super(name, true);
    }

    public void setCurrentValue(String current) {
        value = current;
    }

    public boolean handleKey(int code) {
        switch (code) {
            case OCRcEvent.VK_LEFT:
                return true;
            case OCRcEvent.VK_RIGHT:
                return false;
            case OCRcEvent.VK_ENTER:
                synchronized (this) {
                    KeyboardListenerAction.getInstance().openVirtualKeyboard(
                        KeyboardOption.TYPE_ALPHANUMERIC, 404, 151, value, true, this);
                    started = true;
                }
                return true;
            case OCRcEvent.VK_LAST:
                return started;
        }
        return false;
    }

    public boolean isEditing() {
        return started;
    }

    public Object getChangedValue() {
        return value;
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        Image im;
        Color color;
        if (hasFocus && !started) {
            im = iFocus;
            color = Color.white;
        } else {
            im = iNormal;
            color = GraphicsResource.C_182_182_182;
        }
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(color);
        g.drawImage(im, 0, 0, c);
        g.drawString(TextUtil.shorten(value, GraphicsResource.FM18, 260), 400-387, 242-221);
    }


    public synchronized void closeVirtualKeyboard() {
        KeyboardListenerAction.getInstance().closeVirtualKeyboard(this);
        started = false;
    }

    /////////////////////////////////////////////////////////////////////
    // KeyboardListener
    /////////////////////////////////////////////////////////////////////

    public void cursorMoved(int position) throws RemoteException {
    }

    public void focusOut(int direction) throws RemoteException {
    }

    public void inputCanceled(String text) throws RemoteException {
        closeVirtualKeyboard();
    }

    public void inputEnded(String text) throws RemoteException {
        setCurrentValue(text);
        closeVirtualKeyboard();
    }

    public void modeChanged(int mode) throws RemoteException {
    }

    public void textChanged(String ext) throws RemoteException {
    }

    public void inputCleared() throws RemoteException {
    }

}


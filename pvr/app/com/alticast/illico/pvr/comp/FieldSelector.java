package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public abstract class FieldSelector {
    int x, y;
    boolean started;
    int focus;
    int size;

    public FieldSelector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void start() {
        started = true;
    }

    public boolean handleKey(int code) {
        if (started) {
            switch (code) {
                case OCRcEvent.VK_ENTER:
                    started = false;
                    selected(focus);
                    return true;
                case OCRcEvent.VK_EXIT:
                    started = false;
                    return true;
                case OCRcEvent.VK_UP:
                    focus = (focus - 1 + size) % size;
                    return true;
                case OCRcEvent.VK_DOWN:
                    focus = (focus + 1) % size;
                    return true;
                case OCRcEvent.VK_LEFT:
                case OCRcEvent.VK_RIGHT:
                case OCRcEvent.VK_LAST:
                    return true;
            }
        } else {
            switch (code) {
                case OCRcEvent.VK_ENTER:
                    start();
                    return true;
            }
        }
        return false;
    }

    public abstract void selected(int index);

    public abstract void paint(Graphics g, boolean hasFocus, UIComponent c);


}

package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public abstract class TimeFieldSelector extends FieldSelector {
    Image i_input_38 = DataCenter.getInstance().getImage("input_38.png");
    Image i_input_38_foc = DataCenter.getInstance().getImage("input_38_foc.png");
    Image i_number_p_01 = DataCenter.getInstance().getImage("number_p_01.png");
    Image i_02_ars_t = DataCenter.getInstance().getImage("02_ars_t.png");
    Image i_02_ars_b = DataCenter.getInstance().getImage("02_ars_b.png");
    Image i_11_op_sh_t = DataCenter.getInstance().getImage("11_op_sh_t.png");
    Image i_11_op_sh_b = DataCenter.getInstance().getImage("11_op_sh_b.png");

    public TimeFieldSelector(int x, int y) {
        super(x, y);
    }

    public abstract String getDisplayValue();

    public abstract String getDisplayValue(int index);

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        if (!started) {
            Image im;
            Color color;
            if (hasFocus) {
                im = i_input_38_foc;
                color = Color.white;
            } else {
                im = i_input_38;
                color = GraphicsResource.C_182_182_182;
            }
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(color);
            g.drawImage(im, x, y, c);
            GraphicUtil.drawStringCenter(g, getDisplayValue(), x+406-387, y+242-221);
        } else {
            g.drawImage(i_number_p_01, x, y-11, c);
            g.drawImage(i_02_ars_t, x+8, 195-221, c);
            g.drawImage(i_02_ars_b, x+8, 262-221, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_182_182_182);

            Rectangle oldClip = g.getClipBounds();
    		g.clipRect(x+493-491, y+212-221, 34, 50);

            int sx = x+510-491;
            int sy = y+219-221;
            for (int i = -1; i <= 1; i++) {
                GraphicUtil.drawStringCenter(g, getDisplayValue(focus + i), sx, sy);
                sy += 23;
            }
            g.drawImage(i_11_op_sh_b, x+493-491, 237-221, c);
            g.drawImage(i_11_op_sh_t, x+493-491, 212-221, c);
    		g.setClip(oldClip);
        }
    }
}


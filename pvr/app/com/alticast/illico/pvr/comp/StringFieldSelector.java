package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.Log;

public class StringFieldSelector extends OptionFieldSelector {

    String current;

    public StringFieldSelector(int x, int y, String title, boolean wide) {
        super(x, y, title, wide);
    }

    public void setInitialValue(String value) {
        setCurrentValue(value);
        for (int i = 0; i < values.length; i++) {
            if (getDisplayValue(i).equals(value)) {
                focus = i;
                return;
            }
        }
        focus = 0;
        selectedIndex = 0;
    }

    public void setCurrentValue(String current) {
        this.current = current;
        selectedKey = null;
    }

    public String getDisplayValue() {
        return current;
    }

    public String getDisplayValue(int index) {
        return dataCenter.getString(values[(index + size) % size].toString());
    }

    public void setSelectedKey(Object key) {
        for (int i = 0; i < values.length; i++) {
            if (key.equals(values[i])) {
                selectedKey = values[i];
                current = getDisplayValue(i);
                focus = i;
                selectedIndex = i;
                return;
            }
        }
    }

    public void selected(int index) {
        current = getDisplayValue(index);
        super.selected(index);
    }


    protected void paintNormal(Graphics g, int x, int y) {
        g.drawString(TextUtil.shorten(getDisplayValue(), GraphicsResource.FM18, wide ? 260 : 114), x, y);
    }

    protected void paintEntry(Graphics g, int index, int x, int y) {
        g.drawString(getDisplayValue(index), x, y);
    }

}


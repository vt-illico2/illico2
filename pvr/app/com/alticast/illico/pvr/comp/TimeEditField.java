package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public class TimeEditField extends EditField {

    boolean hour24;
    long time;
    String timeString = "";
    int hour;
    int min;
    int ampm;

    int inputCount = 0;
    int inputValue = 0;

    FieldSelector[] selectors;
    NumberFieldSelector hourSelector;
    NumberFieldSelector minSelector;
    StringFieldSelector ampmSelector;

    public TimeEditField(String name, boolean editable, boolean hour24) {
        super(name, editable);
        this.hour24 = hour24;
        selectors = new FieldSelector[hour24 ? 2 : 3];

        if (!editable) {
            return;
        }

        hourSelector = new NumberFieldSelector(387-387, 0);
        int[] hours;
        if (hour24) {
            hours = new int[24];
            for (int i = 0; i < hours.length; i++) {
                hours[i] = i;
            }
            hourSelector.minValue = 0;
        } else {
            hours = new int[12];
            for (int i = 0; i < hours.length; i++) {
                hours[i] = i + 1;
            }
            hourSelector.minValue = 1;

            ampmSelector = new StringFieldSelector(491-387, 0);
            ampmSelector.setData(new String[] { "am", "pm" });
            selectors[2] = ampmSelector;
        }
        hourSelector.setData(hours);
        hourSelector.maxValue = hours[hours.length - 1];
        selectors[0] = hourSelector;

        int[] mins = new int[60 / 5];
        for (int i = 0; i < mins.length; i++) {
            mins[i] = i * 5;
        }
        minSelector = new NumberFieldSelector(439-387, 0);
        minSelector.setData(mins);
        minSelector.maxValue = 59;
        selectors[1] = minSelector;
    }

    public void setTime(long time) {
        this.time = time;
        if (!isEditable) {
            timeString = Formatter.getCurrent().getTime(time);
        } else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date(time));
            min = cal.get(Calendar.MINUTE);
            if (hour24) {
                hour = cal.get(Calendar.HOUR_OF_DAY);
            } else {
                hour = cal.get(Calendar.HOUR);
                if (hour == 0) {
                    hour = 12;
                }
                ampmSelector.focus = (cal.get(Calendar.AM_PM) == Calendar.AM) ? 0 : 1;
            }
            hourSelector.setValue(hour);
            minSelector.setValue(min);
        }
    }

    public long getTime() {
        if (!isEditable) {
            return time;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date(time));
        cal.set(Calendar.MINUTE, minSelector.currentValue);
        if (hour24) {
            cal.set(Calendar.HOUR_OF_DAY, hourSelector.currentValue);
        } else {
            int hour = hourSelector.currentValue;
            if (hour == 12) {
                hour = 0;
            }
            cal.set(Calendar.HOUR, hour);
            cal.set(Calendar.AM_PM, ampmSelector.focus == 0 ? Calendar.AM : Calendar.PM);
        }
        return cal.getTimeInMillis();
    }

    public Object getChangedValue() {
        long newTime = getTime();
        if (newTime == time) {
            return null;
        } else {
            return new Date(newTime);
        }
    }

    private void setValue(NumberFieldSelector ns, int value) {
        if (value == 0 && ns.minValue > 0) {
            value = ns.maxValue;
        }
        ns.setValue(value);
        inputCount = 0;
        inputValue = 0;
    }


    private void setValueAndMoveFocus(NumberFieldSelector ns, int value) {
        setValue(ns, value);
        if (focus < selectors.length - 1) {
            focus++;
        }
    }

    public boolean handleKey(int code) {
        if (!isEditable) {
            return false;
        }
        if (!isEditing()) {
            if (focus < 2 && code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                inputCount++;
                inputValue = inputValue * 10 + (code - OCRcEvent.VK_0);
                NumberFieldSelector ns = (NumberFieldSelector) selectors[focus];
                if (inputCount == 2) {
                    setValueAndMoveFocus(ns, Math.min(inputValue, ns.maxValue));
                } else if (inputCount == 1 && inputValue * 10 > ns.maxValue) {
                    setValueAndMoveFocus(ns, inputValue);
                }
                return true;
            } else {
                if (focus < 2 && inputCount > 0) {
                    setValue((NumberFieldSelector) selectors[focus], inputValue);
                }
                inputCount = 0;
                inputValue = 0;
            }
        }

        boolean ret = selectors[focus].handleKey(code);
        if (ret) {
            return true;
        }

        switch (code) {
            case OCRcEvent.VK_LEFT:
                if (focus > 0) {
                    focus--;
                    return true;
                }
                return false;
            case OCRcEvent.VK_RIGHT:
                if (focus < selectors.length - 1) {
                    focus++;
                    return true;
                }
                return false;
            case OCRcEvent.VK_ENTER:
                selectors[focus].start();
                return true;
        }
        return false;
    }

    public void resetFocus(int reqFocus) {
        focus = Math.min(reqFocus, selectors.length - 1);
    }

    public boolean isEditing() {
        return selectors[focus].started;
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        if (!isEditable) {
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_182_182_182);
            g.drawString(timeString, 1, 21);
        } else if (isEditing() && hasFocus) {
            selectors[focus].paint(g, true, c);
        } else {
            g.setFont(GraphicsResource.BLENDER19);
            g.setColor(GraphicsResource.C_140_139_139);
            g.drawString(":", 432-387, 21);

            for (int i = 0; i < selectors.length; i++) {
                selectors[i].paint(g, hasFocus && focus == i, c);
            }
        }
    }

    class NumberFieldSelector extends TimeFieldSelector {
        int[] values;
        int currentValue;
        int minValue;
        int maxValue;

        DecimalFormat df = new DecimalFormat("00");

        public NumberFieldSelector(int x, int y) {
            super(x, y);
        }

        public String getDisplayValue() {
            if (selectors[TimeEditField.this.focus] == this && inputCount > 0) {
                return String.valueOf(inputValue);
            } else {
                return df.format(currentValue);
            }
        }

        public String getDisplayValue(int index) {
            return df.format(values[(index + size) % size]);
        }

        public void setData(int[] values) {
            this.values = values;
            size = values.length;
        }

        public void setValue(int value) {
            currentValue = value;
            int index = Arrays.binarySearch(values, value);
            if (index < 0) {
                index = -(index + 1);
            }
            if (index < 0) {
                index = 0;
            } else if (index >= size) {
                index = size - 1;
            }
            focus = index;
        }

        public void selected(int index) {
            setValue(values[index]);
        }

    }

    class StringFieldSelector extends TimeFieldSelector {
        String[] values;

        public StringFieldSelector(int x, int y) {
            super(x, y);
        }

        public String getDisplayValue() {
            return getDisplayValue(focus);
        }

        public String getDisplayValue(int index) {
            return values[(index + size) % size];
        }

        public void setData(String[] values) {
            this.values = values;
            size = values.length;
        }

        public void selected(int index) {
        }
    }

}

package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;

public abstract class EditField {
    public String fieldName;
    public boolean isEditable = false;
    public boolean hasFocus = false;
    public int focus = 0;

    Color color = GraphicsResource.C_182_182_182;

    DataCenter dataCenter = DataCenter.getInstance();

    public EditField(String name, boolean editable) {
        this.fieldName = name + " :";
        this.isEditable = editable;
    }

    public boolean handleKey(int code) {
        return false;
    }

    public boolean isEditing() {
        return false;
    }

    public void resetFocus(int reqFocus) {
        focus = 0;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract Object getChangedValue();

    public abstract void paint(Graphics g, boolean hasFocus, UIComponent c);

}

package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.Log;


public class OptionEditField extends EditField {

    StringFieldSelector selector;
    OptionSelectionListener listener;

    public OptionEditField(String name) {
        super(name, true);
        selector = new StringFieldSelector(0, 0, name, true);
    }

    public OptionEditField(String name, int scaleXOffset) {
        super(name, true);
        selector = new StringFieldSelector(0, 0, name, true);
        selector.scaleXOffset = scaleXOffset;
    }

    public void setInitialValue(String current) {
        selector.setInitialValue(current);
    }

    public void setCurrentValue(String current) {
        selector.setCurrentValue(current);
    }

    public void setSelectedKey(String key) {
        selector.setSelectedKey(key);
    }

    public void setOptions(String[] options) {
        selector.setData(options);
    }

    public void setListener(OptionSelectionListener listener) {
        selector.setListener(listener);
    }

    public Object getSelectedValue() {
        return selector.getSelectedValue();
    }

    public boolean handleKey(int code) {
        if (!isEditable) {
            return false;
        }
        boolean ret = selector.handleKey(code);
        if (ret) {
            return true;
        }
        switch (code) {
            case OCRcEvent.VK_LEFT:
                return false;
            case OCRcEvent.VK_RIGHT:
                return false;
            case OCRcEvent.VK_ENTER:
                Log.printDebug(App.LOG_HEADER+"OptionEditField.handlKey(VK_ENTER)");
                selector.start();
                return true;
        }
        return false;
    }

    public boolean isEditing() {
        return selector.started;
    }

    public Object getChangedValue() {
        return selector.getChangedValue();
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        selector.paint(g, hasFocus, c);
    }
}


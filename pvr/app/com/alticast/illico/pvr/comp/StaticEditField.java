package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;

public class StaticEditField extends EditField {
    String value;

    public StaticEditField(String name, String value) {
        super(name, false);
        this.value = value;
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_182_182_182);
        g.drawString(value, 1, 21);
    }

    public Object getChangedValue() {
        return null;
    }
}

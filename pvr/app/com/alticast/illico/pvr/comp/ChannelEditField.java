package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.*;
import javax.tv.util.*;

public class ChannelEditField extends EditField implements TVTimerWentOffListener{

    int digit = 3;

    DecimalFormat df;

    Object[][] channelData;
    ChannelFieldSelector selector;

    int inputCount = 0;
    int inputValue = 0;
    String inputString = "";

    //->Kenneth[2015.5.23] R5
    private TVTimerSpec inputTimer = new TVTimerSpec();
    ManualRecordingPanel parent;

    public ChannelEditField(String name, ManualRecordingPanel parent) {
        super(name, true);
        this.parent = parent;
        selector = new ChannelFieldSelector(name);

        channelData = (Object[][]) SharedMemory.getInstance().get(SharedDataKeys.EPG_CHANNELS);
        // build channel list
        Hashtable caTable = (Hashtable) SharedMemory.getInstance().get(SharedDataKeys.EPG_CA_MAP);
        Vector vChannels = new Vector();
        Vector vOtherChannel = new Vector();

        for (int i = 0; i < channelData.length; i++) {
            Boolean recordable = (Boolean) channelData[i][3];
            if (recordable != null && recordable.booleanValue()) {
                Integer type = (Integer) channelData[i][4];
                if (type == null || ((short) type.intValue()) != TvChannel.TYPE_PPV) {
                    boolean isSubscribed = true;
                    if (caTable != null) {
                        Integer sourceId = (Integer) channelData[i][5];
                        Boolean subscribed = (Boolean) caTable.get(sourceId);
                        if (subscribed != null) {
                            isSubscribed = subscribed.booleanValue();
                        }
                    }
                    if (isSubscribed) {
                        vChannels.add(channelData[i]);
                    }
                }
            }
        }
        Object[][] builtChannelData = new Object[vChannels.size()][];
        vChannels.copyInto(builtChannelData);
        Integer in = (Integer) builtChannelData[builtChannelData.length - 1][0];
        if (in != null) {
            digit = in.intValue() >= 1000 ? 4 : 3;
        }
        char[] digitChar = new char[digit];
        for (int i = 0; i < digitChar.length; i++) {
            digitChar[i] = '0';
        }
        df = new DecimalFormat(new String(digitChar));

        selector.setData(builtChannelData);

        try {
            TvChannel ch = Core.epgService.getChannelContext(0).getCurrentChannel();
            selector.setInitialValue(ch.getNumber());
        } catch (Exception ex) {
        }

        //->Kenneth[2015.5.23] 
        inputTimer.setDelayTime(1500L);
        inputTimer.setRepeat(false);
        inputTimer.setRegular(false);
        inputTimer.addTVTimerWentOffListener(this);
    }

    //->Kenneth[2015.5.23] : 입력된 숫자에 맞는 채널이 있는 확인해서 처리하고 기존의 input 관련 정보를 초기화한다.
    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelEditField.timerWentOff()");
        setValue(inputValue);
    }

    //->Kenneth[2015.5.23] : 사용자가 숫자키를 입력할 때마다 reset 시킨다
    private void resetInputTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(inputTimer);
        try {
            inputTimer = timer.scheduleTimerSpec(inputTimer);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    //->Kenneth[2015.5.23] : inputTimer 를 deschedule 시킨다
    private void stopInputTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(inputTimer);
    }

    //->Kenneth[2015.5.23] : timer listener 를 제거한다
    // ManualRecordingPanel 에서 불러준다.
    public void removeInputTimerListener() {
        if (inputTimer != null) {
            inputTimer.removeTVTimerWentOffListener(this);
            inputTimer = null;
        }
    }

    private void setValue(int value) {
        int index = selector.findIndex(value);
        if (index >= 0) {
            selector.setIndex(index);
        }
        inputCount = 0;
        inputValue = 0;
        inputString = "";
        stopInputTimer();
        parent.repaint();
    }


    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ChannelEditField.handleKey("+code+") : isEditing() = "+isEditing());
        //->Kenneth[2015.5.23] : R5 : 키처리하는 부분 다시 작성함
        // 팝업유무, 숫자키여부 로 분기해서 코드를 짜놓았는데, 사실상 비슷하거나 동일한 코드이긴 하지만
        // 요구사항이 어떻게 바뀔지 몰라서 이렇게 분기해 놓는게 유리하다는 판단으로 중복된 코드이더라도 분리해 놓았음.
        if (isEditing()) {
            // selector 팝업이 보이는 경우
            if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                // 숫자키가 들어온 경우
                resetInputTimer();
                inputCount++;
                int num = (code - OCRcEvent.VK_0);
                inputValue = inputValue * 10 + num;
                inputString = inputString + ((char) code);
                if (inputCount > digit) {
                    setValue(inputValue);
                }
                return true;
            } else {
                // 숫자키가 아닌 경우 
                stopInputTimer();
                setValue(-1);
                boolean ret = selector.handleKey(code);
                if (ret) {
                    return true;
                }
            }
        } else {
            // 팝업 없는 Normal 경우
            if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                // 숫자키가 들어온 경우
                resetInputTimer();
                inputCount++;
                int num = (code - OCRcEvent.VK_0);
                inputValue = inputValue * 10 + num;
                inputString = inputString + ((char) code);
                if (inputCount > digit) {
                    setValue(inputValue);
                }
                return true;
            } else {
                // 숫자키가 아닌 경우 
                stopInputTimer();
                setValue(-1);
                switch (code) {
                    case OCRcEvent.VK_ENTER:
                        // Selector 팝업 띄움
                        selector.start();
                        return true;
                }
            }
        }
        return false;
        /*
        if (!isEditing()) {
            if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                inputCount++;
                int num = (code - OCRcEvent.VK_0);
                inputValue = inputValue * 10 + num;
                inputString = inputString + ((char) code);
                if (inputCount == digit) {
                    setValue(inputValue);
                }
                return true;
            } else {
                if (inputCount > 0) {
                    setValue(inputValue);
                    if (code == OCRcEvent.VK_ENTER) {
                        return true;
                    }
                }
                inputCount = 0;
                inputValue = 0;
                inputString = "";
            }
        }
        boolean ret = selector.handleKey(code);
        if (ret) {
            return true;
        }
        switch (code) {
            case OCRcEvent.VK_LEFT:
                return true;
            case OCRcEvent.VK_RIGHT:
                return false;
            case OCRcEvent.VK_ENTER:
                selector.start();
                return true;
        }
        return false;
        */
    }

    public boolean isEditing() {
        return selector.started;
    }

    public Object getChangedValue() {
        return selector.getChangedValue();
    }

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        selector.paint(g, hasFocus, c);
    }

    class ChannelFieldSelector extends OptionFieldSelector {

        public ChannelFieldSelector(String title) {
            //->Kenneth[2015.5.23] R5 : wide false -> true
            super(0, 0, title, true);
        }

        private int findIndex(int number) {
            for (int i = 0; i < values.length; i++) {
                if (((Integer) ((Object[]) values[i])[0]).intValue() == number) {
                    return i;
                }
            }
            return -1;
        }

        private void setIndex(int index) {
            focus = index;
            selectedIndex = index;
        }

        public void setInitialValue(int number) {
            setIndex(Math.max(findIndex(number), 0));
        }

        // 얘가 채널 선택하는 팝업 뜨기 전의 채널 정보를 그려주는 데 사용된다.
        protected void paintNormal(Graphics g, int x, int y) {
            if (inputCount > 0) {
                //->Kenneth : 아래의 drawString 에 의해서 사용자가 입력한 숫자가 그려진다
                g.drawString(inputString, x, y);
            } else {
                //->Kenneth : 아래의 drawString 에 의해서 사용자가 입력한 숫자가 아닌 channel number 와 full name 이
                // 그려진다
                //->Kenneth[2015.5.23] R5 : 사이즈 넘어가면 ... 붙임. 더불어 fullName x 좌표를 땡김.
                // 따라서 paintEntry 말고 그냥 여기서 그리도록 한다.
                //paintEntry(g, selectedIndex, x, y);
                Object[] data = (Object[]) values[(selectedIndex + size) % size];
                g.drawString(data[0].toString(), x, y);
                String fullName = null;
                try {
                    fullName = data[6].toString();
                } catch (Exception e) {
                }

                if (fullName != null && fullName.length() > 0) {
                    fullName = TextUtil.shorten(fullName, GraphicsResource.FM18, 160);
                    g.drawString(fullName, x + 50, y);
                }
            }
        }

        protected void paintEntry(Graphics g, int index, int x, int y) {
            Color fontColor = g.getColor();
            if (inputCount > 0 && FOCUSED_FONT_COLOR.equals(fontColor)) {
                g.drawString(inputString, x, y);
            } else {
                Object[] data = (Object[]) values[(index + size) % size];
                g.drawString(data[0].toString(), x, y);
                String fullName = null;
                try {
                    fullName = data[6].toString();
                } catch (Exception e) {
                }

                if (fullName != null && fullName.length() > 0) {
                    fullName = TextUtil.shorten(fullName, GraphicsResource.FM18, 160);
                    g.drawString(fullName, x + 50, y);
                }
            }
            //->Kenneth[2015.5.23] : R5 : Selector 팝업에서도 입력값보여야 해서 다시 작성함
            /*
            Object[] data = (Object[]) values[(index + size) % size];
            g.drawString(data[0].toString(), x, y);
            String fullName = null;
            try {
                fullName = data[6].toString();
            } catch (Exception e) {
            }

            if (fullName != null && fullName.length() > 0) {
                g.drawString(fullName, x + 50, y);
            }
            */
        }

        public Object getChangedValue() {
            return ((Object[]) values[selectedIndex])[0];
        }

    }

}


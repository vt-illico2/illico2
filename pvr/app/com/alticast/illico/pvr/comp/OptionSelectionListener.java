package com.alticast.illico.pvr.comp;

public interface OptionSelectionListener {
    public void selectionChanged(Object key);
}
package com.alticast.illico.pvr.comp;

import java.awt.*;
import java.util.*;
import java.text.*;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.gui.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.log.Log;

public abstract class OptionFieldSelector extends FieldSelector {
    DataCenter dataCenter = DataCenter.getInstance();

    Image i_08_op_bg_b = dataCenter.getImage("08_op_bg_b.png");
    Image i_08_op_bg_m = dataCenter.getImage("08_op_bg_m.png");
    Image i_08_op_bg_t = dataCenter.getImage("08_op_bg_t.png");
    Image i_11_op_high = dataCenter.getImage("11_op_high.png");
    Image i_08_op_foc = dataCenter.getImage("08_op_foc.png");
    Image i_02_ars_t = dataCenter.getImage("02_ars_t.png");
    Image i_02_ars_b = dataCenter.getImage("02_ars_b.png");
    Image i_08_op_sh_b = dataCenter.getImage("08_op_sh_b.png");
    Image i_08_op_sh_t = dataCenter.getImage("08_op_sh_t.png");

    Image iNormal;
    Image iFocus;

    String title;
    Object[] values;
    boolean wide;

    int viewSize;

    Object selectedKey;
    int selectedIndex;

    OptionSelectionListener listener;

    int scaleXOffset = 214;
    //->Kenneth[2015.5.23] : R5 : focus 된 놈의 color 를 가지고 ChannelFieldSelector 에서 focus 되었는지 확인하므로
    // 그 컬러를 상수로 정의해서 key 값처럼 쓴다.
    public static final Color FOCUSED_FONT_COLOR = Color.black;

    public OptionFieldSelector(int x, int y, String title, boolean wide) {
        super(x, y);
        this.title = title;
        this.wide = wide;
        if (wide) {
            iNormal = dataCenter.getImage("input_292_sc.png");
            iFocus = dataCenter.getImage("input_292_sc_foc.png");
        } else {
            iNormal = dataCenter.getImage("input_142_sc.png");
            iFocus = dataCenter.getImage("input_142_sc_foc.png");
        }
    }

    public void setListener(OptionSelectionListener l) {
        listener = l;
    }

    private static final int Y_GAP = 27;

    public void paint(Graphics g, boolean hasFocus, UIComponent c) {
        if (!started || hasFocus) {
            Image im;
            Color color;
            if (hasFocus && !started) {
                im = iFocus;
                color = Color.white;
            } else {
                im = iNormal;
                color = GraphicsResource.C_182_182_182;
            }
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(color);
            g.drawImage(im, x, y, c);
            //->Kenneth : 아래 paintNormal 에 의해서 selector 가 뜨지 않은 EditField 의 내용이 그려진다.
            paintNormal(g, x+400-387, y+242-221);
//            g.drawString(TextUtil.shorten(getDisplayValue(), GraphicsResource.FM18, wide ? 260 : 114),
//                         x+400-387, y+242-221);
        }

        if (started && hasFocus) {
            int addedHeight = (viewSize - 3) * Y_GAP;
            int addedHalf = addedHeight / 2;

            int sx = (wide ? x-387+105 : x-387) - 214 + scaleXOffset;
            int sy = y-301 - addedHalf;

            int iy = sy+285;
            g.drawImage(i_08_op_bg_t, sx+496, iy, c);
            iy += 19;
            for (int i = 0; i < viewSize - 2; i++) {
                g.drawImage(i_08_op_bg_m, sx+496, iy, c);
                iy += Y_GAP;
            }
            g.drawImage(i_08_op_bg_b, sx+496, iy, c);

            // header
            g.setColor(GraphicsResource.C_78_78_78);
            g.fillRect(sx+498, sy+258, 230, 29);
            g.drawImage(i_11_op_high, sx+501, sy+273, c);

            g.setFont(GraphicsResource.BLENDER19);
            //->Kenneth[2015.2.12] 4K flat design
            //g.setColor(GraphicsResource.C_27_24_12);
            //g.drawString(title, sx+512, sy+279);
            g.setColor(GraphicsResource.C_214_182_55);
            g.drawString(title, sx+511, sy+278);


            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_182_182_182);

            Rectangle oldClip = g.getClipBounds();
    		g.clipRect(sx+500, sy+287, 226, 60 + addedHeight);

            g.setColor(GraphicsResource.C_193_191_191);
            iy = sy+295;
            for (int i = (viewSize - 1) / 2; i > 0; i--) {
                //->Kenneth : 아래의 paintEntry 에 의해서 center focus 위의 아이템들이 그려진다
                paintEntry(g, focus - i, sx+515, iy);
                iy += Y_GAP;
            }
            iy += Y_GAP;
            for (int i = 1; i <= (viewSize - 1) / 2; i++) {
                //->Kenneth : 아래의 paintEntry 에 의해서 center focus 아래의 아이템들이 그려진다
                paintEntry(g, focus + i, sx+515, iy);
                iy += Y_GAP;
            }

    		g.setClip(oldClip);
            g.drawImage(i_08_op_sh_t, sx+500, sy+287, c);
            g.drawImage(i_08_op_sh_b, sx+500, sy+320 + addedHeight, c);
            g.drawImage(i_02_ars_t, sx+602, sy+243, c);
            g.drawImage(i_02_ars_b, sx+602, sy+347 + addedHeight, c);

            g.drawImage(i_08_op_foc, sx+498, sy+301 + addedHalf, c);
            g.setColor(FOCUSED_FONT_COLOR);
            //->Kenneth : 아래의 paintEntry 에 의해서 center focus 의 아이템이 그려진다
            paintEntry(g, focus, sx+516, sy+322 + addedHalf);

        }
    }

    protected abstract void paintNormal(Graphics g, int x, int y);

    protected abstract void paintEntry(Graphics g, int index, int x, int y);

    public void selected(int index) {
        setSelectedIndex(index);
        Log.printDebug("OptionFieldSelector.selected = " + index + ", " + selectedKey);
        if (listener != null) {
            listener.selectionChanged(selectedKey);
        }
    }

    public void setSelectedIndex(int index) {
        selectedIndex = index;
        selectedKey = values[index];
    }

    public void setData(Object[] values) {
        this.values = values;
        size = values.length;
        if (size < 5) {
            viewSize = 3;
        } else if (size < 9) {
            viewSize = 5;
        } else {
            viewSize = 9;
        }
    }

    public Object getChangedValue() {
        return selectedKey;
    }

    public Object getSelectedValue() {
        return values[selectedIndex];
    }

}


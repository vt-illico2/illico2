package com.alticast.illico.pvr;

import com.videotron.tvi.illico.ixc.epg.TvChannel;

public class TunerRequestor {
    public static final byte DUAL_TUNER_NO_CONFIRM = 0;
    public static final byte DUAL_TUNER_WITH_CONFIRM = 1;
    public static final byte MANY_TUNER_BY_CHANNEL = 2;
    public static final byte MANY_TUNER_BY_VOD = 3;
    public byte type;
    public int videoIndex;

    public boolean result;

    public TunerRequestor(byte type, int videoIndex) {
        this.type = type;
        this.videoIndex = videoIndex;
    }
}


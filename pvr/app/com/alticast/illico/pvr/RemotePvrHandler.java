package com.alticast.illico.pvr;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;
import java.util.HashSet;
import java.util.Calendar;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import javax.tv.service.Service;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.daemon.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.hn.HomeNetManager;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.list.LocalRecording;
import com.alticast.illico.pvr.ui.UpcomingList;
import com.alticast.illico.pvr.issue.ConflictResolveSession;

public class RemotePvrHandler implements RemoteRequestListener, RecordingChangedListener {

    private static final String DATE_FORMAT = "dd-MM-yyyy:HH:mm:ss";

    public static int recordedVersion = 0;
    public static int scheduledVersion = 0;

    private static final int[] RATING_VALUE_CONVERT_TABLE = {
        0, 4, 3, 6, 7, 10
    };

    private static final int MAX_CONFLICT_CHECK = 5;

    private static RemotePvrHandler instance = new RemotePvrHandler();

    public static RemotePvrHandler getInstance() {
        return instance;
    }

    private RemotePvrHandler() {
        recordedVersion = (int) (Math.random() * Short.MAX_VALUE * 2) + 1;
        scheduledVersion = (int) (Math.random() * Short.MAX_VALUE * 2) + 1;

        FrameworkMain.getInstance().printSimpleLog(recordedVersion + "," + scheduledVersion);

        RecordingManager.getInstance().addRecordingChangedListener(this);
    }

    public static void updateVersions() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.updateVersions()");
        recordedVersion++;
        scheduledVersion++;
    }

    public static void updateRecordedVersion() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.updateRecordedVersions()");
        recordedVersion++;
    }

    public static void updateScheduledVersion() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.updateScheduleVersion()");
        scheduledVersion++;
    }

    public static void updateVersion(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.updateVersion("+req+")");
        try {
            int state = req.getState();
            switch (state) {
                // PENDING_STATES
                case OcapRecordingRequest.PENDING_NO_CONFLICT_STATE:
                case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                    scheduledVersion++;
                    break;
                // IN_PROGRESS_STATES
                case OcapRecordingRequest.IN_PROGRESS_STATE:
                case OcapRecordingRequest.IN_PROGRESS_INSUFFICIENT_SPACE_STATE:
                case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
                case OcapRecordingRequest.IN_PROGRESS_INCOMPLETE_STATE:
                    scheduledVersion++;
                    recordedVersion++;
                    break;
                // COMPLETED_STATES
                case OcapRecordingRequest.INCOMPLETE_STATE:
                case OcapRecordingRequest.COMPLETED_STATE:
                    recordedVersion++;
                    break;
            }
        } catch (Exception ex) {
            Log.print(ex);
            recordedVersion++;
            scheduledVersion++;
        }
    }

    /** Pass request to host application. */
    public byte[] request(String path, byte[] body) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.request("+path+")");
        if (path == null) {
            if (body == null) {
                Log.printWarning("RemotePvrHandler.request : no path, no bytes");
            } else {
                Log.printWarning("RemotePvrHandler.request : no path, " + body.length + " bytes");
            }
            return null;
        }
        String response;
        synchronized (this) {
            Log.printInfo("RemotePvrHandler.request() : " + path);
            UpcomingList ul = UpcomingList.getInstance();
            ul.enableListener(false);
            response = process(path, body);
            ul.enableListener(true);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.request() : Call UpcomingList.updateFull()");
            ul.updateFull();
        }
        return response.getBytes();
    }

    private String process(String path, byte[] body) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.process("+path+")");
        if (!Core.getInstance().checkPermission()) {
            return "result=-11"; // Recording operations are unauthorized on this STB.
        }
        Parameters param = new Parameters(body);

        if ("scheduleRecording".equalsIgnoreCase(path)) {
            return scheduleRecording(param);
        } else if ("getRecordingsList".equalsIgnoreCase(path)) {
            return getRecordingsList(param);
        } else if ("deleteRecordings".equalsIgnoreCase(path)) {
            return deleteRecordings(param);
        } else if ("editSchedule".equalsIgnoreCase(path)) {
            return editSchedule(param);
        } else if ("getRecordingsMetadata".equalsIgnoreCase(path)) {
            return getRecordingsMetadata(param);
        } else if ("getListVersions".equalsIgnoreCase(path)) {
            return getListVersions(param);
        } else {
            return "result=-1"; // generic error
        }
    }

    /** 2.2.1 Schedule a recording. */
    private String scheduleRecording(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording("+table+")");
        if (IssueManager.getInstance().isConflictResolving()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-13");
            return "result=-13"; // conflict resolving
        }
        EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (epgService == null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-1 -> case1");
            return "result=-1"; // generic error
        }
        int channel = table.getInt("channel");
        long start = getDateFromString(table.getString("start"));
        if (start <= 0) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-1 -> case2");
            return "result=-1"; // generic error
        }
        long duration = table.getInt("duration") * Constants.MS_PER_SECOND;
        int save = table.getInt("save");
        int repeat = table.getInt("repeat");
        int api = table.getInt("api");

        TvChannel ch = null;
        try {
            ch = epgService.getChannelByNumber(channel);
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (ch == null) {
            Log.printWarning("RemotePvrHandler: not found channel = " + channel);
            return "result=-5"; // generic error;
        }

        //->Kenneth[2015.3.3] 4K : Non-4K terminal 에 4K recording 요청이 오면 E20 돌려준다.
        //->Kenneth[2015.6.18] 4K : VDTRMASTER-5479 : 따라서 얘를 -11 보다 먼저 체크한다.
        //->Kenneth[2015.6.24] DDC-107
        if (Core.getInstance().needToShowIncompatibilityPopup(ch, false)) {
        //if (Core.getInstance().needToShowIncompatibilityPopup(ch)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : return result=-20");
            return "result=-20";
        }
        //<-

        try {
            if (!ch.isRecordable()) {
                Log.printWarning("RemotePvrHandler: not recordable channel = " + ch);
                return "result=-5"; // generic error;
            } else if (!ch.isSubscribed()) {
                Log.printWarning("RemotePvrHandler: not subscribed channel = " + ch);
                return "result=-11";
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        //->Kenneth[2015.3.3] 4K : Non-4K terminal 에 4K recording 요청이 오면 E20 돌려준다.
        //if (Core.getInstance().needToShowIncompatibilityPopup(ch)) {
        //    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : return result=-20");
        //    return "result=-20";
        //}
        //<-

        String program = table.getString("program");
        String seriesId = null;
        RecordingOption option;
        TvProgram p = null;
        if (program != null) {
            long programStart = getDateFromString(table.getString("progstart"));
            if (programStart == 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-1 -> case3");
                return "result=-1"; // generic error;
            }
            Log.printWarning("RemotePvrHandler: progstart = " + new Date(programStart));
            try {
                String call = ch.getCallLetter();
                p = getProgram(call, programStart, program);
                if (p == null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-1 -> case4");
                    return "result=-1";
                }
                if (RecordManager.getInstance().getRequest(p) != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-16");
                    return "result=-16";
                }
                option = new RecordingOption(ch, p, start - p.getStartTime(), start + duration - p.getEndTime());
                seriesId = p.getSeriesId();
            } catch (Exception ex) {
                Log.print(ex);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns result=-1 -> case5");
                return "result=-1";
            }
        } else {
            // manual recording
            option = new RecordingOption(ch, start, start + duration);
            option.title = DataCenter.getInstance().getString("manual.ManualRecording");
        }
        option.creationMethod = AppData.BY_REMOTE_REQUEST;
        option.onlyFromEpgCache = true;
        if (repeat == 1) {
            option.type = (seriesId != null && seriesId.length() > 0)
                        ? RecordingOption.TYPE_SERIES : RecordingOption.TYPE_REPEAT;
            option.all = true;
            option.keepOption = save;
            option.setRepeat(RecordingOption.REPEAT_ANY, null);
        } else {
            option.type = RecordingOption.TYPE_SINGLE;
            option.all = false;
            option.saveUntil = getSaveDuration(save);
        }
        if (repeat == 1) {
            Vector conflicts = checkNewConflict(ch, p, option.startTime, option.endTime, api);
            if (conflicts != null && conflicts.size() > 0) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() conflicts.size() > 0");
                return getConflictResult(conflicts, api);
            }
        }

        option.suppressConflict = true;
        if (option.startTime < System.currentTimeMillis()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() instant recording");
            String result = getConflictResult(option, api);
            if (result != null) {
                return result;
            }
        }
        RecordingRequest req = RecordManager.getInstance().record(option, true);
        if (req == null) {
            return "result=-1";
        }
        if (!RecordManager.getInstance().checkDiskSpace(req, false)) {
            try {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : Before call req.delete() -- 1");
                req.delete();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : After call req.delete() -- 1");
            } catch (Exception ex) {
                Log.print(ex);
            }
            return "result=-14";
        }
        String ret = "result=0";
        //->Kenneth[2015.3.4] 4K
        if (IssueManager.getInstance().hasMaxUhdConflict(req)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : There is MAX UHD Conflict");
            ret = getUhdConflictResult(req, api);
            try {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : Before call req.delete() -- 2");
                req.delete();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : After call req.delete() -- 2");
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() returns "+ret);
            return ret;
        }
        switch (req.getState()) {
            case OcapRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE:
            case OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE:
                ret = getConflictResult(req, api);
                try {
                    //->Kenneth[2015.7.15] VDTRMASTER-5487 : 아래 delete 하는 부분이 먼저 불리우면
                    // RecordManager.recordingChanged() 에서 checkSdvFailed 에서 SDV 채널이어서 error state 로 간 것인지
                    // 판단하는 부분에서 IllegalArgumentException 생김. (이미 지워진 RecordingRequest 에 대해서 먼가
                    // 하려 하므로) 따라서 recordingChanged 가 끝날 수 있도록 아래의 delete 하는 부분을 좀 늦춘다.
                    // 완전 깔끔한 방법을 아니지만.
                    int chType = TvChannel.TYPE_UNKNOWN;
                    try {
                        chType = ch.getType();
                    } catch (Exception ex1) {
                        Log.print(ex1);
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : ch type = "+chType);
                    if (chType == TvChannel.TYPE_SDV) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : SDV channel, so sleep a few seconds");
                        try {
                            Thread.sleep(5000);
                        } catch (Exception ex2) {
                            Log.print(ex2);
                        }
                    }
                    //<-
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : Before call req.delete() -- 3");
                    req.delete();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : After call req.delete() -- 3");
                } catch (Exception ex) {
                    Log.print(ex);
                }
                break;
            default:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.scheduleRecording() : AppData.remove(req)");
                AppData.remove(req, AppData.SUPPRESS_CONFLICT);
                break;
        }
        return ret;
    }

    /** 2.2.2 Get a list of recordings. */
    private String getRecordingsList(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getRecordingsList("+table+")");
        Object type = table.get("type");
        StringBuffer sb = new StringBuffer(100);
        int version;
        RecordingList list;
        int count;
        int api = table.getInt("api");
        if ("0".equals(type)) { // recorded
            list = RecordingListManager.getInstance().getPlayableList();
            count = list.size();
            sb.append("result=");
            sb.append(count > 0 ? 0 : -8);
            if (api >= 2) {
                sb.append("&udn=");
                sb.append(getUdnString());
            }
            sb.append("&version=");
            sb.append(recordedVersion);
        } else if ("1".equals(type)) {
            list = RecordingListManager.getInstance().getCancelableList();
            list = RecordingListManager.reverseFilter(list, AppData.UNSET, Boolean.TRUE, true);
            count = list == null ? 0 : list.size();
            sb.append("result=");
            sb.append(count > 0 ? 0 : -8);
            if (api >= 2) {
                sb.append("&udn=");
                sb.append(getUdnString());
            }
            sb.append("&version=");
            sb.append(scheduledVersion);
        } else if ("3".equals(type)) {
            list = RecordingListManager.getInstance().getValidList();
            list = RecordingListManager.reverseFilter(list, AppData.UNSET, Boolean.TRUE, true);
            count = list == null ? 0 : list.size();
            sb.append("result=");
            sb.append(count > 0 ? 0 : -8);
            if (api >= 2) {
                sb.append("&udn=");
                sb.append(getUdnString());
            }
        } else {
            return "result=-2"; // list type
        }
        if (count > 0) {
            boolean meta = "1".equals(table.get("meta"));
            sb.append("&count=");
            sb.append(count);
            for (int i = 0; i < count; i++) {
                RecordingRequest req = list.getRecordingRequest(i);
                sb.append('&');
                sb.append(getRecordingString(req, true, api));
                if (meta && AppData.get(req, AppData.PROGRAM_ID) != null) {
                    sb.append('&');
                    sb.append(getMetadataString(req));
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getRecordingsList() returns "+sb.toString());
        return sb.toString();
    }

    /** 2.2.3 Delete a list of recordings. */
    private String deleteRecordings(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.deleteRecordings("+table+")");
        if (IssueManager.getInstance().isConflictResolving()) {
            return "result=-13"; // conflict resolving
        }
        boolean skip = "1".equals(table.get("skip"));
        boolean keep = "1".equals(table.get("stop"));
        int count;
        try {
            count = Integer.parseInt((String) table.get("count"));
        } catch (Exception ex) {
            Log.print(ex);
            return "result=-1"; // generic error
        }
        if (count == 0) {
            return "result=-8"; // no items
        }
        Object o = table.get("id");
        if (o == null) {
            return "result=-1"; // generic error
        }
        String[] ids;
        if (o instanceof String) {
            ids = new String[] { (String) o };
        } else if (o instanceof Vector) {
            Vector v = (Vector) o;
            ids = new String[v.size()];
            v.copyInto(ids);
        } else {
            return "result=-1"; // generic error
        }

        Vector remained = new Vector();

        boolean inProgress = false;
        boolean pending = false;
        boolean recorded = false;

        int firstError = 0;

        for (int i = 0; i < ids.length; i++) {
            boolean deleted = false;
            try {
                int id = Integer.parseInt(ids[i]);
                LeafRecordingRequest req = (LeafRecordingRequest) RecordingManager.getInstance().getRecordingRequest(id);
                if (req != null && !isPlaying(req)) {
                    int state = req.getState();
                    if (RecordingListManager.inProgressFilter.accept(state)) { // in progress
                        if (keep) {
                            req.stop();
                            deleted = true;
                        } else {
                            deleted = RecordManager.deleteIfPossible(req);
                        }
                        inProgress = true;
                    } else if (RecordingListManager.pendingFilter.accept(state)) {  // pending
                        String groupKey = (String) AppData.get(req, AppData.GROUP_KEY);
                        String seriesId = (String) AppData.get(req, AppData.SERIES_ID);
                        if (groupKey == null) { // not-series
                            req.delete();
                        } else {    // series
                            if (skip) {
                                RecordManager.getInstance().setSkipped(req, true);
                            } else {
                                req.delete();
                                RecordingScheduler.getInstance().unset(groupKey);
                            }
                        }
                        deleted = true;
                        pending = true;
                    } else if (RecordingListManager.recordedFilter.accept(state)) {
                        deleted = RecordManager.deleteIfPossible(req);
                        recorded = true;
                    }
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (!deleted) {
                remained.addElement(ids[i]);
            }
        }
        int size = remained.size();
        if (size == 0) {
            return "result=0";
        }
        // not removed items
        StringBuffer sb = new StringBuffer();
        sb.append("result=-10");
        sb.append("&count=");
        sb.append(size);
        for (int i = 0; i < size; i++) {
            sb.append("&id=");
            sb.append(remained.elementAt(i));
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.deleteRecordings() returns "+sb.toString());
        return sb.toString();
    }

    /** 2.2.4 Edit a scheduled recording. */
    private String editSchedule(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule("+table+")");
        if (IssueManager.getInstance().isConflictResolving()) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : isConflictResolving returns false");
            return "result=-13"; // conflict resolving
        }
        String s = (String) table.get("id");
        if (s == null) {
            return "result=-1"; // generic error
        }
        int id;
        try {
            id = Integer.parseInt(s);
        } catch (Exception ex) {
            return "result=-1";
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : id = "+id);
        int api = table.getInt("api");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : api = "+api);

        RecordingRequest req = null;
        try {
            req = RecordingManager.getInstance().getRecordingRequest(id);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : req = "+req);
            int state = req.getState();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : state = "+state);
            if (!RecordingListManager.pendingFilter.accept(state)) {  // pending
                return "result=-4";
            }
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
            return "result=-4";
        } catch (Exception ex) {
            Log.print(ex);
            return "result=-1";
        }
        String groupKey = (String) AppData.get(req, AppData.GROUP_KEY);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : groupKey = "+groupKey);

        long oldStartTime = AppData.getScheduledStartTime(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : oldStartTime = "+oldStartTime);
        long oldDuration = AppData.getScheduledDuration(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : oldDuration = "+oldDuration);
        s = (String) table.get("start");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : s = "+s);
        long newStartTime = 0;
        if (s != null) {
            newStartTime = getDateFromString(s);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : newStartTime = "+newStartTime);
        }
        if (newStartTime == 0) {
            newStartTime = oldStartTime;
        }
        long duration = (table.getInt("duration") * Constants.MS_PER_SECOND);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : duration = "+duration);
        final long newDuration = duration == 0 ? oldDuration : duration;

        Object repeat = table.get("repeat");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : repeat = "+repeat);
        if (repeat == null) {
            repeat = (groupKey == null) ? "0" : "1";
        }

        final long newStartOffset = newStartTime - oldStartTime;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : newStartOffset = "+newStartOffset);
        if ("0".equals(repeat)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : CASE -- 1 : repeat = 0");
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call rescheduleIfPossible()");
            Object o = rescheduleIfPossible(req, newStartOffset, newDuration, api);
            if (!(o instanceof RecordingRequest)) {
                if (o != null) {
                    return o.toString();
                } else {
                    return "result=-1";
                }
            }
            req = (RecordingRequest) o;

            if (groupKey != null) { // make single
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call RecordingScheduler.makeSingle(req, true)");
                RecordingScheduler.getInstance().makeSingle(req, true);
                groupKey = null;
            }

        } else if ("1".equals(repeat)) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : CASE -- 2 : repeat = 1");
            if (groupKey == null) { // make repeat
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : CASE -- 2-1 : groupKey is null");
                String programId = (String) AppData.get(req, AppData.PROGRAM_ID);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : programId = "+programId);

                Serializable oldUnset = AppData.get(req, AppData.UNSET);
                AppData.set(req, AppData.UNSET, Boolean.TRUE);
                String call = AppData.getString(req, AppData.CHANNEL_NAME);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : call = "+call);

                TvProgram orgPr = null;
                if (programId != null) {
                    orgPr = getProgram(call, AppData.getLong(req, AppData.PROGRAM_START_TIME), programId);
                }

                Vector conflicts = checkNewConflict(call, orgPr, newStartTime, newStartTime + newDuration, api);
                if (conflicts != null && conflicts.size() > 0) {
                    String result = getConflictResult(conflicts, api);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : conflict result = "+result);
                    if (oldUnset == null) {
                        AppData.remove(req, AppData.UNSET);
                    } else {
                        AppData.set(req, AppData.UNSET, oldUnset);
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call resetPrioritized()");
                    RecordManager.getInstance().resetPrioritized(req);
                    return result;
                }
                if (oldUnset == null) {
                    AppData.remove(req, AppData.UNSET);
                } else {
                    AppData.set(req, AppData.UNSET, oldUnset);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call rescheduleIfPossible()");
                Object o = rescheduleIfPossible(req, newStartOffset, newDuration, api);
                if (o != null && o instanceof RecordingRequest) {
                    req = (RecordingRequest) o;
                    // 위에서 conflict check를 했으니, 굳이 else check는 필요 없다.
                }

                if (programId == null) {
                    // for manual
                    AppData.set(req, AppData.PROGRAM_START_TIME, new Long(newStartTime));
                    AppData.set(req, AppData.PROGRAM_END_TIME, new Long(newStartTime + newDuration));
                }
                groupKey = RecordingScheduler.getInstance().makeRepeat(req, "1111111", table.getInt("save"), true);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : groupKey = "+groupKey);
            } else {    // edit all episodes
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : CASE -- 2-2 : groupKey is not null");
                // many to many
                RecordingList pendings = RecordingListManager.getInstance().getPendingList();
                final RecordingList list = RecordingListManager.filter(pendings, AppData.GROUP_KEY, groupKey);
                if (list != null) {
                    final int size = list.size();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : size = "+size);
                    final long beforeStartTime[] = new long[size];
                    final long beforeDurations[] = new long[size];
                    Vector conflicts = new Vector();
                    int movedCount = 0;
                    boolean conflictDetected = false;
                    for (int i = 0; i < size; i++) {
                        movedCount++;
                        RecordingRequest rr = list.getRecordingRequest(i);
                        beforeStartTime[i] = AppData.getScheduledStartTime(rr);
                        beforeDurations[i] = AppData.getScheduledDuration(rr);
                        AppData.set(rr, AppData.SUPPRESS_CONFLICT, Boolean.TRUE);
                        long targetStart = beforeStartTime[i] + newStartOffset;
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : targetStart["+i+"] = "+targetStart);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call rescheduleSimple()");
                        rr = rescheduleSimple(rr, beforeStartTime[i] + newStartOffset, newDuration);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : rr = "+rr);
                        if (rr.getState() == OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                            conflictDetected = true;
                            addConflictRecordings(conflicts, rr, api);
                            break;
                        }
                        if (conflictDetected || movedCount >= MAX_CONFLICT_CHECK) {
                            break;
                        }
                    }

                    final int conflictSize = conflicts.size();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : conflictSize = "+conflictSize);
                    final boolean hasConflict = conflictSize > 0;
                    final int lastIndex = movedCount;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : lastIndex = "+lastIndex);

                    String result = null;
                    if (hasConflict) {
                        result = getConflictResult(conflicts, api);
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : conflict result = "+result);
                        Thread t = new Thread("edit_back") {
                            public void run() {
                                for (int i = 0; i < lastIndex; i++) {
                                    RecordingRequest rr = list.getRecordingRequest(i);
                                    AppData.remove(rr, AppData.SUPPRESS_CONFLICT);
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_back["+i+"] : Call rescheduleSimple()");
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_back["+i+"] : beforeStartTime = "+beforeStartTime[i]);
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_back["+i+"] : beforeDurations = "+beforeDurations[i]);
                                    rescheduleSimple(rr, beforeStartTime[i], beforeDurations[i]);
                                }
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_back : Call updateFull()");
                                UpcomingList.getInstance().updateFull();
                            }
                        };
                        t.start();
                    } else {
                        Thread t = new Thread("edit_all") {
                            public void run() {
                                for (int i = 0; i < size; i++) {
                                    RecordingRequest rr = list.getRecordingRequest(i);
                                    AppData.set(rr, AppData.SUPPRESS_CONFLICT, Boolean.TRUE);
                                    long st = beforeStartTime[i];
                                    if (st == 0) {
                                        st = AppData.getScheduledStartTime(rr);
                                    }
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_all["+i+"] : Call replace()");
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_all["+i+"] : newStartTime = "+(st+newStartOffset));
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_all["+i+"] : newDurations = "+newDuration);
                                    rr = RecordManager.getInstance().replace(rr, st + newStartOffset, newDuration);
                                    AppData.remove(rr, AppData.SUPPRESS_CONFLICT);
                                }
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule().edit_all : Call updateFull()");
                                UpcomingList.getInstance().updateFull();
                            }
                        };
                        t.start();
                    }
                    if (result != null) {
                        return result;
                    }
                }
            }
        }

        s = (String) table.get("save");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : save = "+s);
        if (s != null) {
            try {
                int save = Integer.parseInt(s);
                if (groupKey != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call setKeepCount()");
                    RecordingScheduler.getInstance().setKeepCount(groupKey, save);
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : Call setExpirationPeriod()");
                    RecordManager.setExpirationPeriod(req, getSaveDuration(save));
                }
            } catch (Exception ex) {
            }
        }

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.editSchedule() : returns result=0");
        return "result=0";

//        if (newStartTime != oldStartTime || newDuration != oldDuration) {
//            AppData.set(req, AppData.SUPPRESS_CONFLICT, Boolean.TRUE);
//            req = RecordManager.getInstance().replace(req, newStartTime, newDuration);
//
//            int state = req.getState();
//            if (state == OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
//                return getConflictResult(req, api);
//            } else {
//                AppData.remove(req, AppData.SUPPRESS_CONFLICT);
//                return "result=0";
//            }
//        } else {
//            return "result=0";
//        }
    }

    // returns string if conflict occurs, RecordingRequest if ok
    private Object rescheduleIfPossible(RecordingRequest req, long startOffset, long newDuration, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.rescheduleIfPossible("+req+", "+startOffset+", "+newDuration+", "+api+")");
        long oldStartTime = AppData.getScheduledStartTime(req);
        long oldDuration = AppData.getScheduledDuration(req);

        if (startOffset != 0 || newDuration != oldDuration) {
            // reschedule only one episode.
            AppData.set(req, AppData.SUPPRESS_CONFLICT, Boolean.TRUE);
            RecordingRequest rr = rescheduleSimple(req, oldStartTime + startOffset, newDuration);
            int state = rr.getState();
            if (state == OcapRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                String result = getConflictResult(rr, api);
                rescheduleSimple(rr, oldStartTime, oldDuration);
                AppData.remove(rr, AppData.SUPPRESS_CONFLICT);
                return result;
            } else {
                //->Kenneth[2015.3.4] 4K
                if (IssueManager.getInstance().hasMaxUhdConflict(req)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.rescheduleIfPossible() : There is MAX UHD Conflict");
                    String result = getUhdConflictResult(req, api);
                    // 다시 원복
                    rescheduleSimple(rr, oldStartTime, oldDuration);
                    AppData.remove(rr, AppData.SUPPRESS_CONFLICT);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.rescheduleIfPossible() returns "+result);
                    return result;
                }

                AppData.remove(rr, AppData.SUPPRESS_CONFLICT);
                return RecordManager.getInstance().replace(rr, oldStartTime + startOffset, newDuration);
            }
        }
        return req;
    }


    /** 2.2.5 Get recordings metadata. */
    private String getRecordingsMetadata(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getRecordingsMetaData("+table+")");
        int count;
        try {
            count = Integer.parseInt((String) table.get("count"));
        } catch (Exception ex) {
            Log.print(ex);
            return "result=-1"; // generic error
        }
        if (count == 0) {
            return "result=-8"; // no items
        }
        Object o = table.get("id");
        if (o == null) {
            return "result=-1"; // generic error
        }
        String[] ids;
        if (o instanceof String) {
            ids = new String[] { (String) o };
        } else if (o instanceof Vector) {
            Vector v = (Vector) o;
            ids = new String[v.size()];
            v.copyInto(ids);
        } else {
            return "result=-1"; // generic error
        }
        int retCount = 0;
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < ids.length; i++) {
            try {
                int id = Integer.parseInt(ids[i]);
                RecordingRequest req = RecordingManager.getInstance().getRecordingRequest(id);
                if (req != null) {
                    String s = getMetadataString(req);
                    sb.append("&id=");
                    sb.append(id);
                    sb.append('&');
                    sb.append(s);
                    retCount++;
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return "result=" + (retCount >= count ? 0 : -10) + "&count=" + retCount + sb.toString();
    }

    /** 2.2.6 Get list versions. */
    private String getListVersions(Parameters table) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getListVersions("+table+")");
        int api = table.getInt("api");
        StringBuffer sb = new StringBuffer();
        sb.append("result=0");
        if (api >= 2) {
            sb.append("&udn=");
            sb.append(getUdnString());
        }
        sb.append("&recorded=");
        sb.append(String.valueOf(recordedVersion));
        sb.append("&scheduled=");
        sb.append(String.valueOf(scheduledVersion));
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getListVersions() returns "+sb.toString());
        return sb.toString();
    }

    private String getDummyRecordingString(long start, long dur) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getDummyRecordingString("+start+", "+dur+")");
        StringBuffer sb = new StringBuffer();
        sb.append("id=");

        sb.append("&program=");
        sb.append("&progstart=");

        sb.append("&channel=");
        sb.append("&call=");

        sb.append("&start=");
        sb.append(getDateString(start));
        sb.append("&duration=");
        dur = Math.round(((double) dur) / Constants.MS_PER_SECOND);
        sb.append(dur);

        sb.append("&save=");
        sb.append("&repeat=");
        sb.append("&playback=");
        sb.append("&record=");

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getDummyRecordingString() returns "+sb.toString());
        return sb.toString();
    }

    private String getDummyRecordingString(TvProgram p, long start, long dur) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getDummyRecordingString("+p+", "+start+", "+dur+")");
        StringBuffer sb = new StringBuffer();
        sb.append("id=");

        sb.append("&program=");
        try {
            sb.append(p.getId());
        } catch (Exception ex) {
            Log.print(ex);
        }

        sb.append("&progstart=");
        try {
            sb.append(getDateString(p.getStartTime()));
        } catch (Exception ex) {
            Log.print(ex);
        }

        sb.append("&channel=");
        try {
            TvChannel ch = p.getChannel();
            sb.append(ch.getNumber());
        } catch (Exception ex) {
            Log.print(ex);
        }
        sb.append("&call=");
        try {
            sb.append(p.getCallLetter());
        } catch (Exception ex) {
            Log.print(ex);
        }

        sb.append("&start=");
        sb.append(getDateString(start));
        sb.append("&duration=");
        dur = Math.round(((double) dur) / Constants.MS_PER_SECOND);
        sb.append(dur);

        sb.append("&save=0");
        sb.append("&repeat=1");
        sb.append("&playback=0");
        sb.append("&record=0");

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getDummyRecordingString() returns "+sb.toString());
        return sb.toString();
    }


    private String getScheduledRecordingString(RecordingOption option, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getScheduledRecordingString()");
        AppData appData = AppData.create(option);
        StringBuffer sb = new StringBuffer();
        sb.append("id=");
        String programId = (String) appData.get(AppData.PROGRAM_ID);
        String groupKey = (String) appData.get(AppData.GROUP_KEY);
        if (programId != null && appData.getInteger(AppData.PROGRAM_TYPE) != TvProgram.TYPE_UNKNOWN) {
            // non-manual
            sb.append("&program=");
            sb.append(programId);
            sb.append("&progstart=");
            sb.append(getDateString(appData.getLong(AppData.PROGRAM_START_TIME)));
            if (groupKey != null) {
                String seriesId = appData.getString(AppData.SERIES_ID);
                if (seriesId.length() > 0) {
                    sb.append("&series=");
                    sb.append(seriesId);
                }
            }
        }
        sb.append("&channel=");
        sb.append(appData.getInteger(AppData.CHANNEL_NUMBER));
        sb.append("&call=");
        sb.append(TextUtil.urlEncode(appData.getString(AppData.CHANNEL_NAME)));
        sb.append("&start=");
        sb.append(getDateString(option.startTime));
        sb.append("&duration=");
        long dur = option.endTime - option.startTime;
        dur = Math.round(((double) dur) / Constants.MS_PER_SECOND);
        sb.append(dur);

        sb.append("&save=");
        if (groupKey != null) {
            sb.append(RecordingScheduler.getInstance().getKeepCount(groupKey));
        } else {
            sb.append(getSaveOptionValue(option.saveUntil));
        }

        sb.append("&repeat=");
        sb.append(groupKey != null ? '1' : '0');
        sb.append("&playback=0");
        sb.append("&record=0");

        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getScheduledRecordingString() returns "+sb.toString());
        return sb.toString();
    }

    private String getRecordingString(RecordingRequest req, boolean withId, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getRecordingString()");
        StringBuffer sb = new StringBuffer();
        sb.append("id=");
        if (withId) {
            sb.append(req.getId());
        }

        String programId = (String) AppData.get(req, AppData.PROGRAM_ID);
        String groupKey = (String) AppData.get(req, AppData.GROUP_KEY);
        if (programId != null && AppData.getInteger(req, AppData.PROGRAM_TYPE) != TvProgram.TYPE_UNKNOWN) {
            // non-manual
            sb.append("&program=");
            sb.append(programId);
            sb.append("&progstart=");
            sb.append(getDateString(AppData.getLong(req, AppData.PROGRAM_START_TIME)));
            if (groupKey != null) {
                String seriesId = AppData.getString(req, AppData.SERIES_ID);
                if (seriesId.length() > 0) {
                    sb.append("&series=");
                    sb.append(seriesId);
                }
            }
        }
        sb.append("&channel=");
        sb.append(AppData.getInteger(req, AppData.CHANNEL_NUMBER));
        sb.append("&call=");
        sb.append(TextUtil.urlEncode(AppData.getString(req, AppData.CHANNEL_NAME)));
        sb.append("&start=");
        sb.append(getDateString(AppData.getScheduledStartTime(req)));
        sb.append("&duration=");
        long dur = 0;
        try {
            if (RecordingListManager.recordedFilter.accept(req.getState())) {
                RecordedService service = ((LeafRecordingRequest) req).getService();
                if (service != null) {
                    dur = service.getRecordedDuration();
                }
            }
        } catch (Exception ex) {
        }
        if (dur == 0) {
            dur = AppData.getScheduledDuration(req);
        }
        dur = Math.round(((double) dur) / Constants.MS_PER_SECOND);
        sb.append(dur);
//        sb.append(AppData.getScheduledDuration(req) / Constants.MS_PER_SECOND);

        int state = req.getState();
        if (RecordingListManager.cancelableFilter.accept(state)) {
            sb.append("&save=");
            if (groupKey != null) {
                sb.append(RecordingScheduler.getInstance().getKeepCount(groupKey));
            } else {
                sb.append(getSaveOptionValue(RecordManager.getExpirationPeriod(req)));
            }
        }
        if (RecordingListManager.playableFilter.accept(state)) {
            // recorded & in progress
            long expTime = RecordManager.getExpirationTime(req);
            if (expTime < Long.MAX_VALUE) {
                sb.append("&lifetime=");
                sb.append(getDateString(expTime));
            }
        }
        sb.append("&repeat=");
        sb.append(groupKey != null ? '1' : '0');
        sb.append("&playback=");
        sb.append(isPlaying(req) ? '1' : '0');

        sb.append("&record=");
        sb.append(RecordingListManager.inProgressFilter.accept(state) ? '1' : '0');

        if (RecordingListManager.recordedFilter.accept(state)) {
            sb.append("&finish=");
            sb.append(getConvertedReason(AppData.getInteger(req, AppData.FAILED_REASON)));
        }

        if (api >= 2 && RecordingListManager.playableFilter.accept(state)) {
            sb.append("&viewed=");
            int viewCount = AppData.getInteger(req, AppData.VIEW_COUNT);
            if (viewCount > 0) {
                sb.append('2'); // Completely viewed
            } else {
                long mediaTime = LocalRecording.getMediaTime(req);
                if (mediaTime == 0) {
                    sb.append('0'); // Not started
                } else {
                    sb.append('1'); // Partially viewed
                }
            }

        }
        int yearShift = AppData.getInteger(req, AppData.YEAR_SHIFT);
        sb.append("&yearshift=");
        sb.append(yearShift);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getRecordingString() returns "+sb.toString());
        return sb.toString();
    }

    private boolean isPlaying(RecordingRequest req) {
        Service s = Environment.getServiceContext(0).getService();
        if (s != null && s instanceof RecordedService) {
            return req == ((RecordedService) s).getRecordingRequest();
        }
        return false;
    }

    private String getMetadataString(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getMetadataString()");
        StringBuffer sb = new StringBuffer();
        sb.append("title=");
        sb.append(TextUtil.urlEncode(AppData.getString(req, AppData.TITLE)));
        Object episode = AppData.get(req, AppData.EPISODE_TITLE);
        if (episode != null) {
            sb.append("&episode=");
            sb.append(TextUtil.urlEncode(episode.toString()));
        }
        sb.append("&description=");
        sb.append(TextUtil.urlEncode(AppData.getString(req, AppData.FULL_DESCRIPTION)));

        long pst = AppData.getLong(req, AppData.PROGRAM_START_TIME);
        long pet = AppData.getLong(req, AppData.PROGRAM_END_TIME);
        if (pst < pet) {
            sb.append("&progduration=");
            sb.append((pet - pst) / Constants.MS_PER_SECOND);
        }
        sb.append("&stars=");
        sb.append(AppData.getInteger(req, AppData.STAR_RATING));
        sb.append("&rating=");
        sb.append(getRatingValue(AppData.getString(req, AppData.PARENTAL_RATING)));

        sb.append("&category=");
        sb.append(AppData.getInteger(req, AppData.CATEGORY));
        Integer subc = (Integer) AppData.get(req, AppData.SUBCATEGORY);
        if (subc != null) {
            sb.append("&subcategory=");
            sb.append(subc);
        }
        sb.append("&cc=");
        sb.append(AppData.getBoolean(req, AppData.CLOSED_CAPTION) ? '1' : '0');

        int audioType = AppData.getInteger(req, AppData.AUDIO_TYPE);
        sb.append("&stereo=");
        if (audioType == TvProgram.AUDIO_STEREO || audioType == TvProgram.AUDIO_STEREO_AND_SURROUND) {
            sb.append('1');
        } else {
            sb.append('0');
        }
        sb.append("&surround=");
        if (audioType == TvProgram.AUDIO_SURROUND || audioType == TvProgram.AUDIO_STEREO_AND_SURROUND) {
            sb.append('1');
        } else {
            sb.append('0');
        }
        sb.append("&sap=");
        sb.append(AppData.getBoolean(req, AppData.SAP) ? '1' : '0');
        sb.append("&hd=");
        sb.append(AppData.getInteger(req, AppData.DEFINITION));
        sb.append("&lang=");
        sb.append("fr".equals(AppData.get(req, AppData.LANGUAGE)) ? '1' : '0');
        sb.append("&rerun=");
        sb.append(AppData.getBoolean(req, AppData.LIVE) ? '0' : '1');
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getMetadataString() returns "+sb.toString());
        return sb.toString();
    }

    private String getDateString(long time) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        return dateFormat.format(new Date(time));
    }

    private long getDateFromString(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        try {
            return dateFormat.parse(date).getTime();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return 0;
    }

    private int getRatingValue(String rating) {
        try {
            return RATING_VALUE_CONVERT_TABLE[PvrUtil.getRatingIndex(rating)];
        } catch (Exception ex) {
            return 0;
        }
    }

    private TvProgram getProgram(String call, long start, String id) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getProgram("+call+")");
        EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (epgService == null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getProgram() returns null");
            return null;
        }
        try {
            TvProgram p = epgService.getProgram(call, start);
            if (p == null || !id.equals(p.getId())) {
                Log.printWarning("RemotePvrHandler: requested program not matched = " + p);
                p = null;
            }
            if (p == null) {
                p = epgService.requestProgram(call, start);
                Log.printInfo("RemotePvrHandler: program from HTTP = " + p);
            }
            if (p == null || !id.equals(p.getId())) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getProgram() returns null");
                return null;
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getProgram() returns "+p);
            return p;
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getProgram() returns null");
        return null;
    }

    private Vector checkNewConflict(Object chObj, TvProgram p, long start, long end, int api) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.checkNewConflict : " + p + " : " + new Date(start) + " : " + new Date(end));
        }
        Vector conflicts = new Vector();
        long dur = end - start;
        if (start <= System.currentTimeMillis()) {
            start += Constants.MS_PER_DAY;
        }
        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(start);

        EpgService epgService = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (epgService == null) {
            return null;
        }
        TvChannel ch;
        if (chObj instanceof TvChannel) {
            ch = (TvChannel) chObj;
        } else {
            try {
                ch = epgService.getChannel(chObj.toString());
            } catch (Exception ex) {
                Log.printDebug(ex);
                return conflicts;
            }
        }

        RecordingRequest temp = null;
        try {
            String seriesId = null;
            String title = null;
            long offset = 0;
            if (p != null) {
                offset = start - p.getStartTime();
                seriesId = p.getSeriesId();
                if (seriesId != null && seriesId.length() == 0) {
                    seriesId = null;
                }
                title = p.getTitle();
                if (title != null && title.length() == 0) {
                    title = null;
                }
                Log.printInfo("RemotePvrHandler.checkConflictRepeat: " + seriesId + " " + title);
            }

            int movedCount = 0;
            while (true) {
                long from = calFrom.getTimeInMillis();
                if (from > System.currentTimeMillis() + RecordingScheduler.MANUAL_MAX_DAY) {
                    break;
                }
                if (from > RecordingListManager.maxEndTime) {
                    break;
                }
                boolean needToCheck = true;
                TvProgram pr = null;
                boolean conflictDetected = false;
                if (p != null) {
                    pr = epgService.getProgram(ch.getCallLetter(), from - offset);
                    if (pr == null) {
                        Log.printInfo("RemotePvrHandler: no more program");
                        break;
                    }
                    Log.printDebug("RemotePvrHandler: pr = " + pr + " : " + new Date(from - offset));
                    if (seriesId != null) {
                        needToCheck = seriesId.equals(pr.getSeriesId());
                    } else if (title != null) {
                        needToCheck = title.equals(pr.getTitle());
                    }
                }
                Log.printInfo("RemotePvrHandler: repeat check : date = " + new Date(from) + " : " + new Date(RecordingListManager.maxEndTime));

                if (needToCheck) {
                    movedCount++;
                    if (temp == null) {
                        temp = createTempRecording(ch, from, dur);
                    } else {
                        if (temp.getState() == LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                            Log.printDebug("RemotePvrHandler: temp was conflict");
                            rescheduleSimple(temp, from + 400 * Constants.MS_PER_DAY, dur);
                        }
                        rescheduleSimple(temp, from, dur);
                    }
                    if (temp.getState() == LeafRecordingRequest.PENDING_WITH_CONFLICT_STATE) {
                        Log.printInfo("RemotePvrHandler: new temp causes conflict : date = " + new Date(from));
                        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(temp);
                        if (list != null) {
                            conflictDetected = true;
                            if (api >= 2) {
                                conflicts.addElement(getDummyRecordingString(pr, from, dur));
                            }
                            int listSize = list.size();
                            Log.printInfo("RemotePvrHandler: new temp causes conflict : size = " + listSize);
                            for (int i = 0; i < listSize; i++) {
                                RecordingRequest req = list.getRecordingRequest(i);
                                conflicts.addElement(req);
                            }
                        }
                    }
                } else {
                    Log.printInfo("RemotePvrHandler: no need to check");
                }
                if (conflictDetected || movedCount >= MAX_CONFLICT_CHECK) {
                    break;
                }
                calFrom.add(Calendar.DAY_OF_MONTH, 1);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (temp != null) {
            try {
                temp.delete();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return conflicts;
    }

    private RecordingRequest createTempRecording(TvChannel ch, long start, long dur) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.createTempRecording("+ch+")");
        return RecordManager.getInstance().record(ch, start, dur,
                new String[] { AppData.SUPPRESS_CONFLICT, AppData.PROGRAM_TYPE },
                new Serializable[] { Boolean.TRUE, AppData.TEMP_TYPE }, Long.MAX_VALUE);
    }

    private RecordingRequest rescheduleSimple(RecordingRequest r, long start, long dur) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.rescheduleSimple()");
        if (Log.INFO_ON) {
            Log.printInfo("RemotePvrHandler.rescheduleSimple: from = " + new Date(start) + ", dur = " + dur);
        }
        RecordingSpec spec = r.getRecordingSpec();
        if (spec instanceof LocatorRecordingSpec) {
            LocatorRecordingSpec lrs = (LocatorRecordingSpec) spec;
            try {
                LocatorRecordingSpec newSpec = new LocatorRecordingSpec(lrs.getSource(),
                    new Date(start), dur, lrs.getProperties());
                r.reschedule(newSpec);
            } catch (Exception ex) {
                Log.print(ex);
            }
        } else {
            Log.printWarning("RecordManager.rescheduleSimple: not LocatorRecordingSpec = " + spec);
        }
        return r;
    }

    private String getConflictResult(RecordingOption option, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult("+option+")");
        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(option.startTime, option.endTime);
        if (list == null) {
            return null;
        }
        RecordingRequest[] array = new RecordingRequest[list.size()];
        for (int i = 0; i < array.length; i++) {
            array[i] = list.getRecordingRequest(i);
        }
        boolean result = ConflictResolveSession.checkConflict(array, Environment.TUNER_COUNT - 1);
        if (!result) {
            return null;
        }
        StringBuffer sb = new StringBuffer();
        sb.append("result=-7");
        sb.append("&tuners=");
        sb.append(Environment.TUNER_COUNT);
        sb.append("&count=");
        int size = list.size();
        if (api >= 2) {
            sb.append(size+1);
            sb.append('&');
            sb.append(getScheduledRecordingString(option, api));
        } else {
            sb.append(size);
        }
        for (int i = 0; i < size; i++) {
            sb.append('&');
            sb.append(getRecordingString(list.getRecordingRequest(i), true, api));
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult() returns "+sb.toString());
        return sb.toString();

    }

    private String getConflictResult(Vector conflicts, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult("+conflicts+")");
        StringBuffer sb = new StringBuffer();
        sb.append("result=-7");
        sb.append("&tuners=");
        sb.append(Environment.TUNER_COUNT);
        sb.append("&count=");
        int size = conflicts.size();
        sb.append(size);
        for (int i = 0; i < size; i++) {
            sb.append('&');
            Object o = conflicts.elementAt(i);
            if (o instanceof RecordingRequest) {
                sb.append(getRecordingString((RecordingRequest) o, true, api));
            } else if (o instanceof String) {
                sb.append(o.toString());
            }
//            } else if (o instanceof Object[]) {
//                Object[] array = (Object[]) o;
//                sb.append(getDummyRecordingString((TvProgram) array[0], ((Long) array[1]).longValue(), ((Long) array[2]).longValue()));
//            } else if (o instanceof long[]) {
//                long[] data = (long[]) o;
//                sb.append(getDummyRecordingString(data[0], data[1]));
//            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult() returns "+sb.toString());
        return sb.toString();
    }

    private String getConflictResult(RecordingRequest req, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult("+req+")");
        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(req);
        if (list == null || list.size() < Environment.TUNER_COUNT) {
            return "result=0";
        }
        StringBuffer sb = new StringBuffer();
        sb.append("result=-7");
        sb.append("&tuners=");
        sb.append(Environment.TUNER_COUNT);
        sb.append("&count=");
        int size = list.size();
        if (api >= 2) {
            sb.append(size+1);
            sb.append('&');
            sb.append(getRecordingString(req, false, api));
        } else {
            sb.append(size);
        }
        for (int i = 0; i < size; i++) {
            sb.append('&');
            sb.append(getRecordingString(list.getRecordingRequest(i), true, api));
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConflictResult() returns "+sb.toString());
        return sb.toString();
    }

    //->Kenneth[2015.3.4] 4K : Remote PVR spec 에서 요구하는 대로 tuners 의 값을 돌려준다.
    private String getUhdConflictResult(RecordingRequest req, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUhdConflictResult("+req+")");
        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(req);
        //->Kenneth[2015.4.28] 4K : 아래에서 tuner_count 비교하는 부분 제거.
        if (list == null) {
        //if (list == null || list.size() < Environment.TUNER_COUNT) {
            return "result=0";
        }
        StringBuffer sb = new StringBuffer();
        sb.append("result=-7");
        sb.append("&tuners=");
        int maxUhdLimit = ConfigManager.getInstance().pvrConfig.uhdMaxTuners;
        //->Kenneth : max+1 로 넣어주면 될지.. 아니면 실제 UHD recording 개의 갯수를 세서 줄지..
        // 2015.4.28 Alex 메일에 의거 max tuner 값을 그냥 돌려주면 된다.
        sb.append(maxUhdLimit);
        sb.append("&count=");
        int size = list.size();
        if (api >= 2) {
            sb.append(size+1);
            sb.append('&');
            sb.append(getRecordingString(req, false, api));
        } else {
            sb.append(size);
        }
        for (int i = 0; i < size; i++) {
            sb.append('&');
            sb.append(getRecordingString(list.getRecordingRequest(i), true, api));
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUhdConflictResult() returns "+sb.toString());
        return sb.toString();
    }

    private void addConflictRecordings(Vector conflicts, RecordingRequest req, int api) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.addConflictRecordings()");
        RecordingList list = RecordingListManager.getInstance().getActiveOverlappingList(req);
        if (list != null) {
            int listSize = list.size();
            if (listSize >= 2) {
                if (api >= 2) {
                    conflicts.addElement(getRecordingString(req, false, api));
                }
                for (int i = 0; i < listSize; i++) {
                    conflicts.addElement(list.getRecordingRequest(i));
                }
            }
        }
    }

    private int getConvertedReason(int reason) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getConvertedReason("+reason+")");
        switch (reason) {
            case RecordingFailedException.CA_REFUSAL:
                return 6;   // 6: EAS activation
            case RecordingFailedException.CONTENT_NOT_FOUND:
                return 5;   // 5: PatPmtChange
            case RecordingFailedException.TUNING_FAILURE:
                return 3;   // 3: SwitchedDigital service unavailable
            case RecordingFailedException.SERVICE_VANISHED:
                return 4;   // 4: SwitchedDigital service redirected
            case RecordingFailedException.TUNED_AWAY:
            case RecordingFailedException.USER_STOP:
            case RecordingFailedException.RESOURCES_REMOVED:
            case RecordingFailedException.INSUFFICIENT_RESOURCES:
                return 2;   // 2: User initiated
            case RecordingFailedException.SPACE_FULL:
                return 1;   // 1: Out of disk space
            case RecordingFailedException.POWER_INTERRUPTION:
                return 7;   // 7: STB Reboot
            case RecordingFailedException.ACCESS_WITHDRAWN:
                return 10;
            case RecordingFailedException.RESOLUTION_ERROR:
                return 11;
            case RecordingFailedException.OUT_OF_BANDWIDTH:
                return 12;   // unknown
            default:
                return 0;   // 0: Normal (end time) or unknown
        }
    }

    private int getSaveOptionValue(long exp) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getSaveOptionValue("+exp+")");
        if (exp <= Constants.MS_PER_DAY) {
            return 1;
        } else if (exp <= 2 * Constants.MS_PER_DAY) {
            return 2;
        } else if (exp <= Constants.MS_PER_WEEK) {
            return 3;
        } else if (exp <= 2 * Constants.MS_PER_WEEK) {
            return 4;
        } else {
            return 0;
        }
    }

    private long getSaveDuration(int save) {
        switch (save) {
            case 1:
                return Constants.MS_PER_DAY;
            case 2:
                return 2 * Constants.MS_PER_DAY;
            case 3:
                return Constants.MS_PER_WEEK;
            case 4:
                return 2 * Constants.MS_PER_WEEK;
        }
        return Long.MAX_VALUE;
    }

    private String getUdnString() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUdnString()");
        String udn = HomeNetManager.localUdn;
        if (udn == null) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUdnString() returns 0 -> no udn");
            return "0";
        }

        MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        try {
            if (!ms.isMultiroomEnabled()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUdnString() returns 0 -> no multiroom");
                return "0";
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.getUdnString() returns "+udn);
        return TextUtil.urlEncode(udn);
    }

    /** RecordingChangedListener */
    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RemotePvrHandler.recordingChanged("+e+")");
        RecordingRequest req = e.getRecordingRequest();
        int change = e.getChange();
        int oldState = e.getOldState();
        int newState = e.getState();

        String changeString;
        switch (change) {
            case RecordingChangedEvent.ENTRY_DELETED:
                if (RecordingListManager.cancelableFilter.accept(oldState)) {
                    scheduledVersion++;
                }
                if (RecordingListManager.playableFilter.accept(oldState)) {
                    recordedVersion++;
                }
                changeString = "deleted";
                break;
            case RecordingChangedEvent.ENTRY_ADDED:
                if (RecordingListManager.cancelableFilter.accept(newState)) {
                    scheduledVersion++;
                }
                if (RecordingListManager.playableFilter.accept(newState)) {
                    recordedVersion++;
                }
                changeString = "added";
                break;
            case RecordingChangedEvent.ENTRY_STATE_CHANGED:
                if (RecordingListManager.cancelableFilter.accept(oldState)
                        != RecordingListManager.cancelableFilter.accept(newState)) {
                    scheduledVersion++;
                }
                if (RecordingListManager.playableFilter.accept(oldState)
                        != RecordingListManager.playableFilter.accept(newState)) {
                    recordedVersion++;
                }
                changeString = "changed";
                break;
            default:
                return;
        }
        String s = recordedVersion + "," + scheduledVersion + "; "
                + changeString + " " + oldState + " > " + newState + "; "
                + AppData.getString(req, AppData.CHANNEL_NAME) + "; "
                + AppData.getString(req, AppData.TITLE);
        if (Log.INFO_ON) {
            Log.printInfo("RecordingChangedEvent = " + s);
        }
        FrameworkMain.getInstance().printSimpleLog(s);
    }

    class Parameters {
        Hashtable table = new Hashtable();

        public Parameters(byte[] body) {
            if (body == null || body.length == 0) {
                return;
            }
            String[] params = TextUtil.tokenize(new String(body), '&');
            for (int i = 0; params != null && i < params.length; i++) {
                String s = params[i];
                int pos = s.indexOf('=');
                if (pos != -1) {
                    String key = s.substring(0, pos);
                    String value = s.substring(pos + 1);
                    Object o = table.get(key);
                    if (o == null) {
                        table.put(key, value);
                    } else if (o instanceof Vector) {
                        Vector v = (Vector) o;
                        v.addElement(value);
                    } else {
                        Vector v = new Vector();
                        v.addElement(o);
                        v.addElement(value);
                        table.put(key, v);
                    }
                }
            }
        }

        public Object get(String key) {
            return table.get(key);
        }

        public String getString(String key) {
            return (String) table.get(key);
        }

        public int getInt(String key) {
            return getInt(key, 0);
        }

        public int getInt(String key, int defaultValue) {
            try {
                return Integer.parseInt((String) table.get(key));
            } catch (Exception ex) {
                return defaultValue;
            }
        }

    }

}


package com.alticast.illico.pvr.impl;

import java.awt.Point;
import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.framework.DataCenter;

/**
 * This class implemented KeyboardOption interface.
 *
 * @author Sangjoon Kwon
 */
public class KeyboardOptionImpl implements KeyboardOption {
    /** The Constant KEYBOARD_DEFAULT_TEXT. */
    public static String KEYBOARD_DEFAULT_TEXT = "";
    /** The Constant KEYBOARD_DEFAULT_ECHO_CHAR. */
    public static final char KEYBOARD_DEFAULT_ECHO_CHAR = '*';
    /** The Constant KEYBOARD_DEFAULT_MAX_CHAR. */
    public static final int KEYBOARD_DEFAULT_MAX_CHAR = 30;
    /** The Constant KEYBOARD_DEFAULT_MODE. */
    public static final short KEYBOARD_DEFAULT_MODE = KeyboardOption.MODE_POPUP;
    public boolean tempView = true;
    /**
     * KeyBoardOption Type <br>
     * KeyboardOption.TYPE_VIRTUAL : Keyboard Standard <br>
     * KeyboardOption.TYPE_VIRTUAL_EXTENDED : Keyboard Extended <br>
     * KeyboardOption.TYPE_MULTI_TAP : Keyboard T9 <br>
     */
    private int keyBoardOptionType = TYPE_VIRTUAL;

    private int keyboardX;
    private int keyboardY;

    public void setKeyboardPosition(int x, int y){
        keyboardX = x;
        keyboardY = y;
    }

    /**
     * Sets the Keyboard Option type.
     *
     * @param type
     *            the new keyboard option type
     */
    public void setType(int type) {
        this.keyBoardOptionType = type;
    }

    public void setDefaultText(String str){
        KEYBOARD_DEFAULT_TEXT = str;
    }

    /**
     * gets Default Text.
     *
     * @return the default text
     * @throws RemoteException
     *             the remote exception
     */
    public String getDefaultText() throws RemoteException {
        return KEYBOARD_DEFAULT_TEXT;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#getEchoChars()
     */
    public char getEchoChars() throws RemoteException {
        return KEYBOARD_DEFAULT_ECHO_CHAR;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#getInputPattern()
     */
    public String getInputPattern() throws RemoteException {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#getMaxChars()
     */
    public int getMaxChars() throws RemoteException {
        return KEYBOARD_DEFAULT_MAX_CHAR;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#getMode()
     */
    public short getMode() throws RemoteException {
        return KEYBOARD_DEFAULT_MODE;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#getType()
     */
    public int getType() throws RemoteException {
        return keyBoardOptionType;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.videotron.tvi.illico.ixc.keyboard.KeyboardOption#isClosedWithMaxInput()
     */
    public boolean isClosedWithMaxInput() throws RemoteException {
        return false;
    }

    public Point getPosition() throws RemoteException {
        return  new Point(keyboardX, keyboardY);
    }

    public void setTempDefaultText(boolean temp){
        tempView = temp;
    }

    public boolean getTempDefaultText() throws RemoteException {
        return tempView;
    }

    public String getTitle() throws RemoteException {
        return DataCenter.getInstance().getString("pvr.PVR");
    }

    public int getAlphanumericType() throws RemoteException {
        return ALPHANUMERIC_SAMLL;
    }

    public boolean isKeyboardChangedDeactivate() throws RemoteException {
        return false;
    }
}

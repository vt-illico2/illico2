package com.alticast.illico.pvr.data;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;

public class ProgramRemainder implements NotificationOption {

    public String getApplicationName() throws RemoteException {
        return FrameworkMain.getInstance().getApplicationName();
    }

    public int getCategories() throws RemoteException {
        return 0;
    }

    public long getDisplayTime() throws RemoteException {
        return System.currentTimeMillis();
    }

    public String getMessage() throws RemoteException {
        return DataCenter.getInstance().getString("pvr.low5PopupTitle");
    }

    public String getMessageID() throws RemoteException {
        return null;
    }

    public int getNotificationAction() throws RemoteException {
        return ACTION_APPLICATION;
    }

    public String[] getNotificationActionData() throws RemoteException {
        return new String[] {FrameworkMain.getInstance().getApplicationName(), NotificationService.IXC_NAME,
                String.valueOf(PvrService.MENU_PVR_MANAGEMENT)};
    }

    public int getNotificationPopupType() throws RemoteException {
        return NOTIFICAION_APPNAME_POPUP;
    }

    public long getRemainderDelay() throws RemoteException {
        return 0;
    }

    public int getRemainderTime() throws RemoteException {
        return 0;
    }

    public long getCreateDate() throws RemoteException {
        return 0;
    }

    public int getDisplayCount() throws RemoteException {
        return 0;
    }

    public boolean isViewed() throws RemoteException {
        return false;
    }

    public String getLargePopupMessage() throws RemoteException {
        return null;
    }

    public String getSubMessage() throws RemoteException {
        return "";
    }

    public String[][] getButtonNameAction() throws RemoteException {
        return null;
    }

    public String getLargePopupSubMessage() throws RemoteException {
        return null;
    }

    public boolean isCountDown() throws RemoteException {
        return false;
    }
}

package com.alticast.illico.pvr.data;

import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import org.ocap.shared.dvr.*;
import javax.tv.service.Service;
import java.util.Date;

public class RecordedContentImpl extends PvrContentImpl implements RecordedContent {

    Service service;

    public RecordedContentImpl(AbstractRecording r) {
        super(r);
        service = r.getService();
    }

    public long getRecordingStartTime() {
        Date d = request.getRecordingStartTime();
        if (d != null) {
            return d.getTime();
        }
        return getScheduledStartTime();
    }

    public long getDuration() {
        return request.getRecordedDuration();
    }
}

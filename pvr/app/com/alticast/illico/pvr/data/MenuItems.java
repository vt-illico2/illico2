package com.alticast.illico.pvr.data;

import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.pvr.PvrService;

public class MenuItems {

    public static final MenuItem MULTI_DELETE = new MenuItem("option.mutiDelete");

    public static final MenuItem GO_MANUAL = new SceneChangeMenuItem("option.CreateRecording", PvrService.MENU_CREATE_RECORDING);
    public static final MenuItem GO_MANAGEMENT = new SceneChangeMenuItem("pvr.PVR_management", PvrService.MENU_PVR_MANAGEMENT);
    public static final MenuItem GO_WIZARD = new SceneChangeMenuItem("management.btnWIZARD", PvrService.MENU_WIZARD);
    public static final MenuItem GO_PREFERENCES = new SceneChangeMenuItem("option.GoPreferences", PvrService.MENU_PREFERENCE);
    public static final MenuItem GO_RECORDED = new SceneChangeMenuItem("option.Seelistofrecorded", PvrService.MENU_RECORDING_LIST);
    public static final MenuItem GO_UPCOMING = new SceneChangeMenuItem("option.DOPList2", PvrService.MENU_SCHEDULED_RECORDINGS);
    public static final MenuItem GO_HELP = new MenuItem("option.Help");

    public static final MenuItem PC_ACTIVATE = new MenuItem("TxtParetalResultPopup.Activate_Parental_Control");
    public static final MenuItem PC_SUSPEND  = new MenuItem("TxtParetalResultPopup.Suspend_Parental_Control");
    public static final MenuItem PC_ENABLE   = new MenuItem("TxtParetalResultPopup.Enable_Parental_Control");

    public static final MenuItem BLOCK_MANUALLY = new MenuItem("option.BlockManually");
    public static final MenuItem UNBLOCK_MANUALLY = new MenuItem("option.UnblockManually");

//    public static final MenuItem BLOCK_RECORDING = new MenuItem("option.BlockRecording");
    public static final MenuItem UNBLOCK_RECORDING = new MenuItem("option.UnblockRecording");

    public static final MenuItem PLAY = new MenuItem("List.PLAY");
    public static final MenuItem EDIT = new MenuItem("List.Edit");
    public static final MenuItem STOP = new MenuItem("List.STOP");
    public static final MenuItem DELETE = new MenuItem("List.Delete");
    public static final MenuItem CANCEL = new MenuItem("List.Cancel");
    public static final MenuItem MORE_DETAILS = new MenuItem("List.More Details");
    public static final MenuItem VIEW_EPISODES = new MenuItem("pvr.ViewEpisodes");
    public static final MenuItem BACK = new MenuItem("pvr.Back");
    public static final MenuItem CANCEL_SERIES = new MenuItem("List.CancelSeries");
    public static final MenuItem EDIT_SERIES = new MenuItem("List.ModifySeries");
    public static final MenuItem SKIP = new MenuItem("List.SkipRecording");
    public static final MenuItem RESET = new MenuItem("List.ResetRecording");
    //->Kenneth[2017.3.14] R7.3 viewing status 세분화 + search option 추가
    public static final MenuItem REPLAY = new MenuItem("List.play_again");
    public static final MenuItem RESUME_VIEWING = new MenuItem("List.resume_viewing");
    public static final MenuItem MORE_OPTIONS = new MenuItem("List.more_options");
    public static final MenuItem SEARCH = new MenuItem("pvr.Search");
    //<-

    public static final MenuItem getParentalControlMenu() {
        DataCenter dc = DataCenter.getInstance();
        Object pcValue = DataCenter.getInstance().get(RightFilter.PARENTAL_CONTROL);
        if (Definitions.OPTION_VALUE_ON.equals(pcValue)) {
            return PC_SUSPEND;     // ON
        } else if (Definitions.OPTION_VALUE_OFF.equals(pcValue)) {
            return PC_ACTIVATE;    // OFF
        } else {
            return PC_ENABLE;      // Suspended
        }
    }

}

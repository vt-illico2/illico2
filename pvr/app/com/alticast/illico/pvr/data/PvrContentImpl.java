package com.alticast.illico.pvr.data;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import org.ocap.shared.dvr.RecordingRequest;
import java.io.Serializable;

public class PvrContentImpl implements PvrContent, Serializable {

    protected transient AbstractRecording request;

    public PvrContentImpl(AbstractRecording r) {
        request = r;
    }

    public String getId() {
        return "" + request.getIdObject();
    }

    public int getRecordingRequestState() {
        return request.getState();
    }

    public short getContentState() {
        return request.getStateGroup();
    }

    public String getTitle() {
        return request.getTitle();
    }

    //->Kenneth[2017.8.22] R7.4
    public String getEnglishTitle() {
        return request.getEnglishTitle();
    }
    //<-

    public String getChannelName() {
        return request.getString(AppDataKeys.CHANNEL_NAME);
    }

    public long getProgramStartTime() {
        return request.getLong(AppDataKeys.PROGRAM_START_TIME);
    }

    public long getProgramEndTime() {
        return request.getLong(AppDataKeys.PROGRAM_END_TIME);
    }

    public long getScheduledStartTime() {
        return request.getScheduledStartTime();
    }

    public long getScheduledDuration() {
        return request.getScheduledDuration();
    }

    public int getChannelNumber() {
        return request.getInteger(AppDataKeys.CHANNEL_NUMBER);
    }

    public Serializable getAppData(String key) {
        Object o = request.getAppData(key);
        if (o != null && o instanceof Serializable) {
            return (Serializable) o;
        } else {
            return null;
        }
    }

    public String getLocation() {
        return request.getLocation();
    }

    public String toString() {
        return "PvrContentImpl[" + getChannelName() + " " + getProgramStartTime()
                + " " + getRecordingRequestState() + "]";
    }

}

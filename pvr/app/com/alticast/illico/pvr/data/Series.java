package com.alticast.illico.pvr.data;

import java.rmi.*;
import java.io.*;
import java.util.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.issue.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.framework.*;

public class Series {
    public String groupKey;
    public String channel;
    public char type;
    public int startTimeSlot;
    public int endTimeSlot;
    public String comparator;  // title or series_id

//    String firstLine;
    public char state = RecordingScheduler.STATE_NORMAL;
    public String title;
    public long firstTime;
    public String repeatString;
    public int keepCount;
    public int category;
    public int subCategory;
    public int definition;
    public String language;
    public String rating;
    private long startOffset;
    private long endOffset;

    public long lastTime;
    public Vector list = new Vector();

    // from file to set
    public Series(File f) {
        setGroupKey(f.getName());
        String[] lines = TextReader.read(f);
        setFirstLine(lines[0]);
        for (int i = 1; i < lines.length; i++) {
            add(new SeriesEpisode(lines[i]));
        }
    }

    public static String createGroupKey(String callLetter, char type, long from, long to, String comparator) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(from);
        int startSlot = cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE);
        cal.setTimeInMillis(to);
        int endSlot = cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE);
        String result = callLetter + RecordingScheduler.GROUP_KEY_MARKER
                + type + startSlot + "_" + endSlot + RecordingScheduler.GROUP_KEY_MARKER
                + TextUtil.urlEncode(comparator);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.createGroupKey() returns "+result);
        return result;
    }

    public static String createGroupKey(SeriesEpisode sp, char type, String comparator) {
        return createGroupKey(sp.callLetter, type, sp.from, sp.to, comparator);
    }

    // newly created
    public Series(String filename, String title, long firstTime, String repeatString, int keepCount,
                            int cat, int subCat, int def, String language, String rating, long startOffset, long endOffset) {
        setGroupKey(filename);
        this.title = title;
        this.firstTime = firstTime;
        this.repeatString = repeatString;
        this.keepCount = keepCount;
        this.category = cat;
        this.subCategory = subCat;
        this.definition = def;
        this.language = language;
        this.rating = rating;
        if (Math.abs(startOffset) > Constants.MS_PER_DAY) {
            startOffset = 0;
        }
        if (Math.abs(endOffset) > Constants.MS_PER_DAY) {
            endOffset = 0;
        }
        this.startOffset = startOffset;
        this.endOffset = endOffset;
    }

    // file name    = call_letter#type????_????#[title or series_id]
    private void setGroupKey(String groupKey) {
        this.groupKey = groupKey;
        String s;
        int l = groupKey.lastIndexOf(RecordingScheduler.GROUP_KEY_MARKER);
        this.comparator = TextUtil.urlDecode(groupKey.substring(l + 1));
        int r = l;
        l = groupKey.lastIndexOf(RecordingScheduler.GROUP_KEY_MARKER, r - 1);
        type = groupKey.charAt(l + 1);
        int p = groupKey.indexOf('_', l + 1);
        this.startTimeSlot = Integer.parseInt(groupKey.substring(l + 2, p));
        this.endTimeSlot = Integer.parseInt(groupKey.substring(p + 1, r));
        this.channel = groupKey.substring(0, l);
    }

    // first line   = state|title|first_time|repeat_string|keep_count
    // first line   = state|title|first_time|repeat_string|keep_count|cat|sub_cat|definition|language|rating|startOffset|endOffset
    private void setFirstLine(String fl) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.setFirstLine("+fl+")");
//        this.firstLine = fl;
        String[] tokens = TextUtil.tokenize(fl, '|');
        try {
            this.state = fl.charAt(0);
        } catch (Exception ex) {
            Log.print(ex);
        }
        this.title = TextUtil.urlDecode(tokens[1]);
        try {
            this.firstTime = Long.parseLong(tokens[2]);
        } catch (Exception ex) {
            Log.print(ex);
        }
        this.repeatString = tokens[3];
        try {
            this.keepCount = Integer.parseInt(tokens[4]);

            this.category = Integer.parseInt(tokens[5]);
            this.subCategory = Integer.parseInt(tokens[6]);
            this.definition = Integer.parseInt(tokens[7]);
            this.language = tokens[8];
            this.rating = tokens[9];
            this.startOffset = Long.parseLong(tokens[10]);
            this.endOffset = Long.parseLong(tokens[11]);
        } catch (Exception ex) {
            Log.print(ex);
        }
        // 버그로 이게 -program time이 될 수가 있더라
        if (Math.abs(startOffset) > Constants.MS_PER_DAY) {
            startOffset = 0;
        }
        if (Math.abs(endOffset) > Constants.MS_PER_DAY) {
            endOffset = 0;
        }
    }

    public void add(SeriesEpisode sp) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.add("+sp+")");
        if (!list.contains(sp)) {
            this.lastTime = Math.max(lastTime, sp.to);
            list.addElement(sp);
        }
    }

    public synchronized SeriesEpisode find(String channel, long time) {
        for (int i = 0; i < list.size(); i++) {
            SeriesEpisode p = (SeriesEpisode) list.elementAt(i);
            if (p.equals(channel, time)) {
                return p;
            }
        }
        return null;
    }

    public synchronized boolean remove(String channel, long time) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.remove("+channel+", "+time+")");
        boolean removed = false;
        for (int i = list.size() - 1; i >= 0; i--) {
            SeriesEpisode p = (SeriesEpisode) list.elementAt(i);
            if (p.equals(channel, time)) {
                list.remove(p);
                removed = true;
            }
        }
        if (removed) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.remove() : Call writeToFile()");
            writeToFile();
        }
        return removed;
    }

    public synchronized void updateEpisodeTitle(RecordingRequest req, String newEpisodeTitle) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.updateEpisodeTitle("+newEpisodeTitle+")");
        boolean added = newEpisodeTitle.length() > 0;
        Enumeration en = list.elements();
        SeriesEpisode target = null;
        long startTime = AppData.getLong(req, AppData.PROGRAM_START_TIME);
        boolean found = false;
        while (en.hasMoreElements()) {
            SeriesEpisode sp = (SeriesEpisode) en.nextElement();
            if (sp.from == startTime) {
                target = sp;
                sp.episodeTitle = newEpisodeTitle;
            } else if (added && newEpisodeTitle.equals(sp.episodeTitle)) {
                found = true;
                Log.printWarning(App.LOG_HEADER+"Series.updateEpisodeTitle(): found duplicated episode = " + sp);
            }
        }
        if (found) {
            if (target != null) {
                list.remove(target);
            }
            try {
                req.delete();
//                RecordManager.getInstance().cancel(req);
            } catch (Exception ex) {
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.updateEpisodeTitle() : Call writeToFile()");
        writeToFile();
    }

    public boolean checkInactive(long timeout) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.checkInactive("+timeout+")");
        if (state != RecordingScheduler.STATE_INACTIVE) {
            if (lastTime + timeout < System.currentTimeMillis()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.checkInactive() : state = STATE_INACTIVE");
                state = RecordingScheduler.STATE_INACTIVE;
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.checkInactive() : Call writeToFile()");
                writeToFile();
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    public synchronized void writeToFile() {
        try {
            File f = new File(RecordingScheduler.SERIES_DIRECTORY, groupKey);
            String[] s = new String[list.size() + 1];
            s[0] = state + "|" + TextUtil.urlEncode(title) + "|" + firstTime + "|" + repeatString + "|" + keepCount
                         + "|" + category + "|" + subCategory + "|" + definition + "|" + language + "|" + rating
                         + "|" + startOffset + "|" + endOffset;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.writeToFile() : First Line = "+s[0]);
            for (int i = 1; i < s.length; i++) {
                s[i] = list.elementAt(i - 1).toString();
            }
            TextWriter.write(s, f);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public boolean checkTimeSlot(SeriesEpisode sp) {
        if (RecordingScheduler.REPEAT_ALL_STRING.equals(repeatString)) {
            return true;
        }
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(sp.from);
        // 1=sun, 7=sat -> 0=mon, 6=sun
        if (repeatString.charAt((cal.get(Calendar.DAY_OF_WEEK) + 5) % 7) != '1') {
            return false;
        }

        if (startTimeSlot != cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE)) {
            return false;
        }
//        cal.setTimeInMillis(sp.to);
//        if (endTimeSlot != cal.get(Calendar.HOUR_OF_DAY) * 100 + cal.get(Calendar.MINUTE)) {
//            return false;
//        }
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////
    // add programs
    ///////////////////////////////////////////////////////////////////////////

    public void startThread(final SeriesEpisode sp, boolean onlyFromEpgCache, final boolean fromStart, final boolean sync) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.startThread("+sp+", "+onlyFromEpgCache+", "+fromStart+", "+sync+")");
        if (state == RecordingScheduler.STATE_INACTIVE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.startThread() : Return, it's STATE_INACTIVE");
            return;
        }
        final long maxTime = onlyFromEpgCache ? Long.MAX_VALUE - 1 : Long.MAX_VALUE;
        if (sync) {
            addPrograms(sp, maxTime, fromStart, sync);
        } else {
            Thread t = new Thread(this.toString()) {
                public void run() {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.startThread().t.run()");
                    addPrograms(sp, maxTime, fromStart, sync);
                }
            };
            t.start();
        }
    }

    // at the first time
    private void addPrograms(SeriesEpisode sp, long maxTime, boolean fromStart, boolean sync) {
        if (state == RecordingScheduler.STATE_INACTIVE) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : Return, it's STATE_INACTIVE");
            return;
        }
        try {
            addPrograms(sp, fromStart ? System.currentTimeMillis() : lastTime, maxTime, sync);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public synchronized void addPrograms(SeriesEpisode target, long from, long to, boolean sync) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms("+target+", "+new Date(from)+", "+new Date(to)+", "+sync+")");
        if (state == RecordingScheduler.STATE_DELETING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : Return, it's STATE_DELETING");
            return;
        }
        int recordingOptionType;
        RemoteIterator it;
        // to가 MAX인 경우는 HTTP로 14일치를 전부 가져온다. 아닌 경우는 cache 이내에서 처리
        boolean fromUpdater = (to != Long.MAX_VALUE);
        boolean fromRemotePvr = (to == Long.MAX_VALUE - 1);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : fromUpdater = "+fromUpdater);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : fromRemotePvr = "+fromRemotePvr);
        switch (type) {
            case RecordingScheduler.SERIES:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : It's a SERIES.");
                if (fromUpdater) {
                    it = Core.epgService.findPrograms(channel, null, comparator, from, to);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : it = "+it);
                } else {
                    it = Core.epgService.requestPrograms(channel, null, comparator, from);
                }
                recordingOptionType = RecordingOption.TYPE_SERIES;
                break;
            case RecordingScheduler.PROGRAM_REPEAT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : It's a PROGRAM_REPEAT.");
                if (fromUpdater) {
                    it = Core.epgService.findProgramsByTitle(channel, title, from, to);
                } else {
                    it = Core.epgService.requestProgramsByTitle(channel, title, from);
                }
                recordingOptionType = RecordingOption.TYPE_REPEAT;
                break;
            case RecordingScheduler.MANUAL_REPEAT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : It's a MANUAL_REPEAT.");
                boolean added = processManual(fromUpdater);
                if (added) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : Call writeToFile() --- 0");
                    writeToFile();
                }
                return;
            default:
                return;
        }
        boolean added = !fromUpdater;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : added = "+added);
        Vector vector = new Vector();
        Remote o;
        while ((o = it.next()) != null) {
            if (state == RecordingScheduler.STATE_DELETING) {
                return;
            }
            TvProgram p = (TvProgram) o;
            vector.addElement(p);
        }
        int size = vector.size();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : size = "+size);
        if (size <= 0) {
            Log.printWarning("Series.addPrograms: program not found.");
        } else {
            if (!fromUpdater && !sync
                             && size > RecordingScheduler.MAX_SCHEDULED_PROGRAMS
                             && RecordingScheduler.REPEAT_ALL_STRING.equals(repeatString)
                             && target != null && target.program != null) {
                Log.printWarning("Series.addPrograms: too many programs ! size=" + size);
                showTooManyRecordingPopup(target.program);
                return;
            }
            
            //->Kenneth[2015.9.30] VDTRMASTER-5658 
            // 10.2 : 조건 변경
            //if (target == null && state == RecordingScheduler.STATE_INACTIVE && fromUpdater && recordingOptionType == RecordingOption.TYPE_SERIES) {
            if (state == RecordingScheduler.STATE_INACTIVE) {
                if (type == RecordingScheduler.SERIES || type == RecordingScheduler.PROGRAM_REPEAT) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : This Series is INACTIVE but new episode registered. So change to STATE_NORMAL.");
                    state = RecordingScheduler.STATE_NORMAL;
                    RecordingScheduler.getInstance().removeSeriesFromInactivceGroup(this);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : call writeToFile() --- 1");
                    writeToFile();
                }
            }
            //<-

            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : size ="+size);
            if (!fromUpdater && sync) {
                size = Math.min(RecordingScheduler.MAX_SCHEDULED_PROGRAMS, size);
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : size(2) ="+size);
        }

        RecordingRequest req;
        for (int i = 0; i < size; i++) {
            TvProgram p = (TvProgram) vector.elementAt(i);
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : program = " + p);
                Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : offset = " + startOffset + ", " + endOffset);
            }
            if (p.getStartTime() + startOffset < System.currentTimeMillis()) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : not a future program = " + p);
                }
                continue;
            }
            SeriesEpisode sp = new SeriesEpisode(p);
            if (!checkTimeSlot(sp)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : time slot is not matched = " + p);
                }
            } else if (list.contains(sp)) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : already scheduled = " + p);
                }
            } else {
                req = RecordManager.getInstance().seriesRecord(p, startOffset, endOffset, groupKey,
                            recordingOptionType, fromRemotePvr, fromUpdater ? AppData.NEW_EPISODE_FROM_EPG_DATA : AppData.NEW_SERIES);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : req = "+req);
                if (req != null) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : Call add("+sp+")");
                    add(sp);
                    added = true;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : Set added = true");
                    if (state == RecordingScheduler.STATE_DELETING) {
                        try {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() ["+i+"] : STATE_DELETING : Call req.delete()");
                            req.delete();
                        } catch (Exception ex) {
                        }
                        return;
                    }
                }
            }
        }

        if (added) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.addPrograms() : call writeToFile() --- 2");
            writeToFile();
        }
    }

    private boolean processManual(boolean fromUpdater) {
        boolean added = !fromUpdater;
        Log.printDebug(App.LOG_HEADER+"Series.processManual: groupKey = " + groupKey + ", " + new Date(lastTime));

        long time = lastTime;
        Calendar calStart = Calendar.getInstance();
        calStart.setTimeInMillis(lastTime);
        int year = calStart.get(Calendar.YEAR);
        int month = calStart.get(Calendar.MONTH);
        int day = calStart.get(Calendar.DAY_OF_MONTH);

        Calendar calFrom = Calendar.getInstance();
        calFrom.setTimeInMillis(firstTime);
        calFrom.set(Calendar.YEAR, year);
        calFrom.set(Calendar.MONTH, month);
        calFrom.set(Calendar.DAY_OF_MONTH, day);
        Date date = calFrom.getTime();

        int startHour = startTimeSlot / 100;
        int startMin = startTimeSlot % 100;
        int endHour = endTimeSlot / 100;
        int endMin = endTimeSlot % 100;

        long dur = (endHour - startHour) * Constants.MS_PER_HOUR
                   + (endMin - startMin) * Constants.MS_PER_MINUTE;
        if (dur <= 0) {
            dur += Constants.MS_PER_DAY;
        }
        RecordingRequest req;
        while (calFrom != null) {
            if (state == RecordingScheduler.STATE_DELETING) {
                return false;
            }
            int dow = (calFrom.get(Calendar.DAY_OF_WEEK) + 5) % 7; // 0=sun, 6=sat
            if (repeatString.charAt(dow) == '1') {
                long newFrom = calFrom.getTimeInMillis();
                if (newFrom > System.currentTimeMillis() + RecordingScheduler.MANUAL_MAX_DAY) {
                    return added;
                }
                long newTo = newFrom + dur;
                SeriesEpisode sp = new SeriesEpisode(channel, newFrom, newTo, "", "");
                if (!list.contains(sp)) {
                    add(sp);
                    req = RecordManager.getInstance().seriesRecord(channel, newFrom, newTo, title, groupKey,
                                    fromUpdater ? AppData.NEW_EPISODE_FROM_EPG_DATA : AppData.NEW_SERIES);
                    if (state == RecordingScheduler.STATE_DELETING) {
                        if (req != null) {
                            try {
                                req.delete();
                            } catch (Exception ex) {
                            }
                        }
                        return false;
                    }
                    added = true;
                }
            }
            calFrom.add(Calendar.DAY_OF_MONTH, 1);
        }
        return added;
    }

    public boolean equals(Object o) {
        return o.toString().equals(toString());
    }

    public String toString() {
        return "Series[" + groupKey + ", " + comparator + ", " + list.size() + " items, " + new Date(lastTime) + "]";
    }

    private void showTooManyRecordingPopup(final TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.showTooManyRecordingPopup("+p+")");
        IssueManager.getInstance().setTooManyProgram(p);
        DataCenter dataCenter = DataCenter.getInstance();
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString("pvr.many_programs_title"));
        popup.setButton("pvr.many_programs_btn0", "pvr.many_programs_btn1");

        String s = title + "\n" + TextUtil.replace(dataCenter.getString("pvr.many_programs_msg"), "|", "\n");
        popup.setDescription(s);
        PopupAdapter listener = new PopupAdapter() {
            public boolean handleHotKey(int code) {
                switch (code) {
                    case OCRcEvent.VK_EXIT:
                    case OCRcEvent.VK_PAGE_UP:
                    case OCRcEvent.VK_PAGE_DOWN:
                    case OCRcEvent.VK_GUIDE:
                    case OCRcEvent.VK_RECORD:
                    case OCRcEvent.VK_STOP:
                    case KeyCodes.SEARCH:
                        return true;
                    default:
                        return (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
                }
            }

            public void selected(int focus) {
                if (focus == 0) {
                    cancel(p);
                    int popupType = type == RecordingScheduler.SERIES ? PopupController.POPUP_RECORD_SERIES : PopupController.POPUP_RECORD_REPEAT;
                    try {
                        PopupController.getInstance().showRecordingPopup(p.getChannel(), p, popupType, true);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                } else if (focus == 1) {
                    showCancelPopup(p);
                }
            }
        };
        popup.setListener(listener);
        PopupController.getInstance().showPopup(popup);
    }

    private void showCancelPopup(final TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.showCancelPopup("+p+")");
        DataCenter dataCenter = DataCenter.getInstance();
        ListSelectPopup popup = new ListSelectPopup();
        popup.setPopupTitle(dataCenter.getString("pvr.cancel_series_popup_title"));
        popup.setDescription(dataCenter.getString("pvr.cancel_series_popup_msg") + "\n'"
                + TextUtil.shorten(title, GraphicsResource.FM18, 250) + "' ?");

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    cancel(p);
                } else {
                    showTooManyRecordingPopup(p);
                }
            }
        };
        popup.setListener(listener);
        PopupController.getInstance().showPopup(popup);
    }

    private void cancel(TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"Series.cancel("+p+")");
        this.state = RecordingScheduler.STATE_DELETING;
        boolean ret = RecordManager.getInstance().stop(p);
        if (ret) {
            RecordingScheduler.getInstance().remove(groupKey);
        } else {
            RecordingScheduler.getInstance().unset(groupKey);
        }
        IssueManager.getInstance().setTooManyProgram(null);
    }

}

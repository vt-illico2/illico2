package com.alticast.illico.pvr.data;

import com.alticast.illico.pvr.MenuController;
import com.videotron.tvi.illico.ui.MenuItem;

public class SceneChangeMenuItem extends MenuItem {

    int sceneCode;

    public SceneChangeMenuItem(String name, int code) {
        super(name);
        sceneCode = code;
    }

    public int getSceneCode() {
        return sceneCode;
    }

    public void changeScene() {
        MenuController.getInstance().showMenu(sceneCode);
    }

}


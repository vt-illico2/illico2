package com.alticast.illico.pvr.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Date;

import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.guide.ProgramEvent;

import org.ocap.net.OcapLocator;
import org.ocap.shared.dvr.*;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.epg.*;

import com.alticast.illico.pvr.RecordingOption;
import com.alticast.illico.pvr.RecordingScheduler;
import com.alticast.illico.pvr.RemotePvrHandler;

/**
 * 녹화물 AppData 관련
 *
 * @version $Revision: 1.3 $ $Date: 2017/01/09 20:41:18 $
 * @author  June Park
 */
public class AppDataEntries {

    private String[] keys;
    private Serializable[] values;

    protected AppDataEntries(Hashtable table) {
        ArrayList keyList = new ArrayList();
        ArrayList valueList = new ArrayList();

        Enumeration em = table.keys();
        while (em.hasMoreElements()) {
            Object o = em.nextElement();
            keyList.add(o);
            valueList.add(table.get(o));
        }

        keys = new String[keyList.size()];
        keys = (String[]) keyList.toArray(keys);

        values = new Serializable[valueList.size()];
        values = (Serializable[]) valueList.toArray(values);
    }

    public String[] getKeys() {
        return this.keys;
    }

    public Serializable[] getValues() {
        return this.values;
    }

}

package com.alticast.illico.pvr.data;

import java.rmi.*;
import java.io.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.util.*;

public class SeriesEpisode {
    public String callLetter;
    public long from;
    public long to;
    public String programId;
    public String episodeTitle;
    public String reserved;

    // null 일 수 있음.
    public TvProgram program;

    // program line = call_letter|from|to|program_id|episode_title|reserved
    public SeriesEpisode(String line) {
        String[] tokens = TextUtil.tokenize(line, '|');
        callLetter = tokens[0];
        from = Long.parseLong(tokens[1]);
        to = Long.parseLong(tokens[2]);
        programId = tokens[3];
        episodeTitle = TextUtil.urlDecode(tokens[4]);
        reserved = tokens[5];
    }

    public SeriesEpisode(TvProgram p) throws RemoteException {
        program = p;
        callLetter = p.getCallLetter();
        from = p.getStartTime();
        to = p.getEndTime();
        programId = p.getId();
        episodeTitle = p.getEpisodeTitle();
        reserved = "0";
    }

    public SeriesEpisode(String callLetter, long from, long to, String programId, String episodeTitle) {
        this.callLetter = callLetter;
        this.from = from;
        this.to = to;
        this.programId = programId;
        this.episodeTitle = episodeTitle;
        this.reserved = "0";
    }

    public String toString() {
        return callLetter + "|" + from + "|" + to + "|" + programId + "|" + TextUtil.urlEncode(episodeTitle) + "|" + reserved;
    }

    public boolean equals(Object o) {
        if (!(o instanceof SeriesEpisode)) {
            return false;
        }
        SeriesEpisode sp = (SeriesEpisode) o;
        if (episodeTitle.length() > 0) {
            return episodeTitle.equals(sp.episodeTitle);
        }
        return from == sp.from;
    }

    public boolean equals(String channel, long time) {
        return time == from && channel.equals(callLetter);
    }
}

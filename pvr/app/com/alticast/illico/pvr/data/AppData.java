package com.alticast.illico.pvr.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Date;

import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.guide.ProgramEvent;

import org.ocap.net.OcapLocator;
import org.ocap.shared.dvr.*;
import org.ocap.dvr.OcapRecordingRequest;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.epg.*;

import com.alticast.illico.pvr.RecordManager;
import com.alticast.illico.pvr.RecordingOption;
import com.alticast.illico.pvr.RecordingScheduler;
import com.alticast.illico.pvr.RemotePvrHandler;

import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.*;

/**
 * 녹화물 AppData 관련
 *
 * @author  June Park
 */
public class AppData implements AppDataKeys {

    public static final Integer TEMP_TYPE = new Integer(Integer.MIN_VALUE);

    private Hashtable table;
//    private String[] keys;
//    private Serializable[] values;
    public long saveUntil;

    private AppData(Hashtable table, long saveUntil) {
        this.table = table;
        this.saveUntil = saveUntil;

        Long oldTime = (Long) table.get(CREATION_TIME);
        if (oldTime == null) {
            table.put(CREATION_TIME, new Long(System.currentTimeMillis()));
        }

        table.put(YEAR_SHIFT, new Integer(Core.getInstance().getYearShift()));
    }

    public Object get(String key) {
        // DDC-001 : to cover null of old recording for YEAR_SHIFT
        Object val = table.get(key);
        if (val == null && YEAR_SHIFT.equals(key)) {
            if (Log.INFO_ON) Log.printInfo("No AppData for YEAR_SHIFT, so return 0");
            val = new Integer(0);
        }
        return val;
    }

    public int getInteger(String name) {
        try {
            return ((Integer) get(name)).intValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public long getLong(String name) {
        try {
            return ((Long) get(name)).longValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public String getString(String name) {
        try {
            return get(name).toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static AppData create(RecordingRequest req) {
        Hashtable table = new Hashtable();
        String[] keys = req.getKeys();
        if (keys != null) {
            for (int i = 0; i < keys.length; i++) {
                table.put(keys[i], get(req, keys[i]));
            }
        }
        return new AppData(table, RecordManager.getExpirationPeriod(req));
    }

    /** Recording from option. */
    public static AppData create(RecordingOption o) {
        Hashtable table = new Hashtable();
        TvProgram p = o.program;
        TvChannel ch = o.channel;

        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        if (p == null) {
            // manual recording
            TvProgram manualProgram = null;
            try {
                manualProgram = es.getProgram(ch.getId(), o.startTime);
            } catch (Exception ex) {
                Log.print(ex);
            }
            Log.printDebug("AppData.create: manual program = " + p);

            table.put(PROGRAM_START_TIME, new Long(o.startTime));
            table.put(PROGRAM_END_TIME, new Long(o.endTime));
            try {
                //->Kenneth[2015.3.19] 4K : 테스트 하다 보니 manual recording 의 경우 빠져 있어서
                // ENTRY_ADDED 로 날아오는 RecordingRequest 의 해상도가 0 으로 되어 있었음.
                if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
                    // UHD 의 경우 채널의 정보만을 사용해야 한다
                    table.put(DEFINITION, new Integer(TvChannel.DEFINITION_4K));
                } else {
                    table.put(DEFINITION, new Integer(ch.isHd() ? 1 : 0));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // program recording
            try {
                table.put(PROGRAM_START_TIME, new Long(p.getStartTime()));
                table.put(PROGRAM_END_TIME, new Long(p.getEndTime()));

                table.put(CATEGORY, new Integer(p.getCategory()));
                int subc = p.getSubCategory();
                if (subc >= 0) {
                    table.put(SUBCATEGORY, new Integer(subc));
                }
                table.put(LANGUAGE, p.getLanguage());
                table.put(PROGRAM_ID, p.getId());
                table.put(PROGRAM_TYPE, new Integer(p.getType()));
                table.put(PARENTAL_RATING, p.getRating());
                //->Kenneth[2015.2.14] 4K
                //table.put(DEFINITION, new Integer(ch.isHd() && p.isHd() ? 1 : 0));
                if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
                    // UHD 의 경우 채널의 정보만을 사용해야 한다
                    table.put(DEFINITION, new Integer(TvChannel.DEFINITION_4K));
                } else {
                    //->Kenneth[2015.7.8] VDTRMASTER-5474 : SD/HD 구분은 채널 정보만으로 하자.
                    table.put(DEFINITION, new Integer(ch.isHd() ? 1 : 0));
                    //table.put(DEFINITION, new Integer(ch.isHd() && p.isHd() ? 1 : 0));
                    //<-
                }

                table.put(STAR_RATING, new Integer(p.getStarRating()));
                table.put(CLOSED_CAPTION, new Boolean(p.isCaptioned()));
                table.put(AUDIO_TYPE, new Integer(p.getAudioType()));
                table.put(SAP, new Boolean(p.isSapEnabled()));
                table.put(LIVE, new Boolean(p.isLive()));

                table.put(FULL_DESCRIPTION, p.getFullDescription());
                String year = p.getProductionYear();
                if (year != null && year.length() > 0) {
                    table.put(PRODUCTION_YEAR, year);
                }
                String seriesId = p.getSeriesId();
                if (seriesId != null && seriesId.length() > 0) {
                    table.put(SERIES_ID, p.getSeriesId());
                }
                String episodeTitle = p.getEpisodeTitle();
                if (episodeTitle != null && episodeTitle.length() > 0) {
                    table.put(EPISODE_TITLE, episodeTitle);
                }
                long eid = p.getPpvEid();
                if (eid != 0) {
                    table.put(PPV_EID, new Long(eid));
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        try {
            String cl = ch.getCallLetter();
            try {
                TvProgram high = es.getHighestRatingProgram(cl, o.startTime, o.endTime);
                table.put(PARENTAL_RATING, high.getRating());
            } catch (Exception ex) {
                if (p != null) {
                    table.put(PARENTAL_RATING, p.getRating());
                }
            }
            table.put(CHANNEL_NAME, cl);
            table.put(CHANNEL_NUMBER, new Integer(ch.getNumber()));
            table.put(SOURCE_ID, new Integer(ch.getId()));
            table.put(TITLE, o.title);
            Log.printDebug("o.title = " + o.title);
            //->Kenneth[2017.8.22] R7.4 : Search 에서 사용할 keyword 저장
            table.put(ENGLISH_TITLE, FrTranscoder.getInstance().transcode(o.title));
            //<-
            if (o.groupKey != null) {
                table.put(GROUP_KEY, o.groupKey);
            }
//            switch (o.type) {
//                case RecordingOption.TYPE_REPEAT:
//                    if (o.all) {
//                        // once인 경우는 repeat이 아니다.
//                        if (p != null) {
//                            table.put(GROUP_KEY, cl + RecordingScheduler.GROUP_KEY_MARKER + TextUtil.urlEncode(o.title));
//                        } else {
//                            table.put(GROUP_KEY, cl + RecordingScheduler.GROUP_KEY_MARKER + RecordingScheduler.createManualProgramId(o.startTime, o.endTime));
//                        }
//                    }
//                    break;
//                case RecordingOption.TYPE_SERIES:
//                    // manual 인 경우 이 경우는 없다. 따라서 p != null
//                    if (o.all) {
//                        if (p != null) {
//                            table.put(GROUP_KEY, cl + RecordingScheduler.GROUP_KEY_MARKER + p.getSeriesId());
//                        }
//                    }
//                    break;
//                case RecordingOption.TYPE_SINGLE:
//                    break;
//            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (o.suppressConflict) {
            table.put(SUPPRESS_CONFLICT, Boolean.TRUE);
        }
        if (o.creationMethod != 0) {
            table.put(CREATION_METHOD, new Integer(o.creationMethod));
        }
        return new AppData(table, o.saveUntil);
    }

    public AppDataEntries getEntries() {
        return new AppDataEntries(this.table);
    }

    /** from updater. */
    public static boolean updateManual(RecordingRequest req, long from, long to) {
        boolean changed = false;
        try {
            EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
            int sourceId = getInteger(req, SOURCE_ID);
            String channel = AppData.getString(req, AppData.CHANNEL_NAME);
            TvProgram high = es.getHighestRatingProgram(channel, from, to);
            if (high != null) {
                String rating = high.getRating();
                if (rating != null) {
                    Object oldRating = get(req, PARENTAL_RATING);
                    if (oldRating == null || !rating.equals(oldRating)) {
                        Log.printDebug("AppData.updateManual : rating = " + rating);
                        set(req, PARENTAL_RATING, rating);
                        changed = true;
                    }
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return changed;
    }

    /** from updater. */
    public static boolean update(RecordingRequest req, TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update("+req+", "+p+")");
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : Before Dump Old REQ ************************************");
        printAppData(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : After Dump Old REQ ************************************");
        if (Log.DEBUG_ON) Log.printDebug("");
        AppData old = create(req);
        TvChannel ch = null;
        try {
            ch = p.getChannel();
        } catch (Exception ex) {
            Log.print(ex);
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : Call old.update(ch, p)");
        old.update(ch, p);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : Call setAll(req, old)");
        boolean changed = setAll(req, old);
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : Before Dump New REQ ************************************");
        printAppData(req);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : After Dump New REQ ************************************");
        if (Log.DEBUG_ON) Log.printDebug("");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"AppData.update() : returns "+changed);
        return changed;
    }

    public void update(TvChannel ch, TvProgram p) {
        try {
            table.put(TITLE, p.getTitle());

            table.put(PROGRAM_START_TIME, new Long(p.getStartTime()));
            table.put(PROGRAM_END_TIME, new Long(p.getEndTime()));

            table.put(CATEGORY, new Integer(p.getCategory()));
            int subc = p.getSubCategory();
            if (subc >= 0) {
                table.put(SUBCATEGORY, new Integer(subc));
            }
            table.put(LANGUAGE, p.getLanguage());
            table.put(PROGRAM_ID, p.getId());
            table.put(PROGRAM_TYPE, new Integer(p.getType()));
            table.put(PARENTAL_RATING, p.getRating());

            table.put(STAR_RATING, new Integer(p.getStarRating()));
            table.put(CLOSED_CAPTION, new Boolean(p.isCaptioned()));
            table.put(AUDIO_TYPE, new Integer(p.getAudioType()));
            table.put(SAP, new Boolean(p.isSapEnabled()));
            table.put(LIVE, new Boolean(p.isLive()));
            table.put(FULL_DESCRIPTION, p.getFullDescription());
            table.put(PRODUCTION_YEAR, p.getProductionYear());
            String episodeTitle = p.getEpisodeTitle();
            table.put(EPISODE_TITLE, episodeTitle != null ? episodeTitle : "");
            //table.put(DEFINITION, new Integer((ch == null || ch.isHd()) && p.isHd() ? 1 : 0));
            //->Kenneth[2015.3.19] 4K : 소스 보다가 4K 반영 안된 부분 발견
            if (ch == null) {
                if (p != null) {
                    table.put(DEFINITION, new Integer(p.isHd() ? 1 : 0));
                }
            } else {
                if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
                    // UHD 의 경우 채널의 정보만을 사용해야 한다
                    table.put(DEFINITION, new Integer(TvChannel.DEFINITION_4K));
                } else {
                    //->Kenneth[2015.7.8] VDTRMASTER-5474 : SD/HD 구분은 채널 정보만으로 하자.
                    table.put(DEFINITION, new Integer(ch.isHd() ? 1 : 0));
                    //table.put(DEFINITION, new Integer(ch.isHd() && (p != null && p.isHd()) ? 1 : 0));
                    //<-
                }
            }

        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static boolean setAll(RecordingRequest req, AppData data) {
        AppDataEntries entries = data.getEntries();
        String[] keys = entries.getKeys();
        Serializable[] values = entries.getValues();
        boolean changed = false;
        for (int i = 0; i < keys.length; i++) {
            String k = keys[i];
            Serializable v = values[i];
            Serializable old = get(req, k);
            if (v == null || ((v instanceof String) && ((String) v).length() == 0)) {
                if (old != null) {
                    try {
                        req.removeAppData(k);
                        changed = true;
                    } catch (Exception ex) {
                    }
                    changed = true;
                }
            } else {
                if (!v.equals(old)) {
                    try {
                        req.addAppData(k, v);
                        changed = true;
                    } catch (Exception ex) {
                    }
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("AppData.setAll : changed = " + changed);
        }
        if (changed) {
            RemotePvrHandler.updateScheduledVersion();
        }
        return changed;
    }

    public static void set(RecordingRequest req, String key, Serializable value) {
        try {
            req.addAppData(key, value);
            RemotePvrHandler.updateVersion(req);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static void remove(RecordingRequest req, String key) {
        try {
            req.removeAppData(key);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static Serializable get(RecordingRequest req, String name) {
        return req.getAppData(name);
    }

    public static int getInteger(RecordingRequest req, String name) {
        try {
            return ((Integer) req.getAppData(name)).intValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static long getLong(RecordingRequest req, String name) {
        try {
            return ((Long) req.getAppData(name)).longValue();
        } catch (Exception ex) {
            return 0;
        }
    }

    public static String getString(RecordingRequest req, String name) {
        try {
            return req.getAppData(name).toString();
        } catch (Exception ex) {
            return "";
        }
    }

    public static boolean getBoolean(RecordingRequest req, String name) {
        try {
            return ((Boolean) req.getAppData(name)).booleanValue();
        } catch (Exception ex) {
            return false;
        }
    }

    public static long getScheduledStartTime(RecordingRequest req) {
        if (req == null) {
            return 0;
        }
        try {
            RecordingSpec spec = req.getRecordingSpec();
            if (spec instanceof LocatorRecordingSpec) {
                return ((LocatorRecordingSpec) spec).getStartTime().getTime();
            } else if (spec instanceof ServiceContextRecordingSpec) {
                return ((ServiceContextRecordingSpec) spec).getStartTime().getTime();
            } else if (spec instanceof ServiceRecordingSpec) {
                return ((ServiceRecordingSpec) spec).getStartTime().getTime();
            }
        } catch (Exception ex) {
        }
        return 0;
    }

    public static long getScheduledDuration(RecordingRequest req) {
        if (req == null) {
            return 0;
        }
        try {
            RecordingSpec spec = req.getRecordingSpec();
            if (spec instanceof LocatorRecordingSpec) {
                return ((LocatorRecordingSpec) spec).getDuration();
            } else if (spec instanceof ServiceContextRecordingSpec) {
                return ((ServiceContextRecordingSpec) spec).getDuration();
            } else if (spec instanceof ServiceRecordingSpec) {
                return ((ServiceRecordingSpec) spec).getDuration();
            }
        } catch (Exception ex) {
        }
        return 0;
    }

/*
    public static int _getFailedReason(RecordingRequest req) {
        try {
            if (req.getState() == OcapRecordingRequest.CANCELLED_STATE) {
                return RecordManager.CANCELLED;
            }
        } catch (Exception ex) {
        }
//        int reason = getInteger(req, FAILED_REASON);
//        if (reason != 0) {
//            return reason;
//        }
        LeafRecordingRequest r = (LeafRecordingRequest) req;
        // 실패 원인
        String exception = null;
        try {
            // RecordingFailedException
            RecordingFailedException ex = (RecordingFailedException) r.getFailedException();
            if (ex != null) {
                int reason = ex.getReason();
                if (reason == RecordingFailedException.USER_STOP) {
                    int nr = getInteger(req, FAILED_REASON);
                    if (nr != 0) {
                        return nr;
                    }
                }
                return reason;
            }
        } catch (Exception ex) {
        }
        return 0;
    }
*/

    public static boolean isSingleProgram(int type) {
        switch (type) {
            case TvProgram.TYPE_MOVIE:
            case TvProgram.TYPE_EVENT:
            case TvProgram.TYPE_UNKNOWN:
                return true;
            default:
                return false;
        }
    }

    public static int updateFailedReason(RecordingRequest req) {
        int reason = readFailedReason(req);
        if (reason != 0 && reason != RecordingFailedException.USER_STOP) {
            set(req, FAILED_REASON, new Integer(reason));
            return reason;
        } else {
            return getInteger(req, FAILED_REASON);
        }
    }

    private static int readFailedReason(RecordingRequest req) {
        try {
            if (req.getState() == OcapRecordingRequest.CANCELLED_STATE) {
                return RecordManager.CANCELLED;
            }
            LeafRecordingRequest r = (LeafRecordingRequest) req;
            RecordingFailedException ex = (RecordingFailedException) r.getFailedException();
            if (ex != null) {
                return ex.getReason();
            }
        } catch (Exception ex) {
        }
        return 0;
    }


    public static void printAppData(RecordingRequest req) {
        if (Log.DEBUG_ON) {
            String[] keys = req.getKeys();
            Log.printDebug("#################################");
            Log.printDebug("Recording Information for req = " + req.getId());
            long start = getScheduledStartTime(req);
            Log.printDebug("start time = " + new Date(start));
            Log.printDebug("end time   = " + new Date(start + getScheduledDuration(req)));
            if (keys == null || keys.length <= 0) {
                Log.printDebug("AppData: no appdata keys");
            } else {
                for (int i = 0; i < keys.length; i++) {
                    Log.printDebug("AppData: appdata[" + i + "] : " + keys[i] +  " = " + get(req, keys[i]));
                }
            }
        }
    }

}

package com.alticast.illico.pvr;

import org.davic.net.*;
import org.davic.net.tuning.*;
import org.davic.resources.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.resource.*;
import org.ocap.service.*;
import org.ocap.net.*;
import org.davic.net.tuning.*;
import java.util.*;
import javax.tv.service.Service;
import javax.tv.service.selection.*;
import com.alticast.illico.pvr.config.PvrConfig;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;

/**
 * TunerResourceManager
 *
 * @author  June Park
 */
public class TunerResourceManager implements DataUpdateListener {

    private ResourceContentionManager manager;

    private static TunerResourceManager instance = new TunerResourceManager();

    public static TunerResourceManager getInstance() {
        return instance;
    }

    private TunerResourceManager() {
        manager = ResourceContentionManager.getInstance();
        manager.setWarningPeriod((int) (2 * Constants.MS_PER_MINUTE));

        DataCenter.getInstance().addDataUpdateListener(ConfigManager.PVR_CONFIG_INSTANCE, this);
        Object config = DataCenter.getInstance().get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config != null) {
            dataUpdated(ConfigManager.PVR_CONFIG_INSTANCE, config, config);
        }
    }

    public void init() {
    }

    public void dataUpdated(String key, Object old, Object value) {
        PvrConfig config = (PvrConfig) value;
        int time = config.autoTuneDuration * 1000;
        manager.setWarningPeriod(time);
        Log.printInfo("TunerResourceManager.setWarningPeriod = " + time);
    }

    public void dataRemoved(String key)  {
    }

}


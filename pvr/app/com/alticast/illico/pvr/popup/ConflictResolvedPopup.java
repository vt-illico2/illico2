package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import com.alticast.illico.pvr.MenuController;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.log.Log;

public class ConflictResolvedPopup extends Popup implements Runnable {
    OwnRenderer renderer = new OwnRenderer();
    boolean modeResolved;

    ConflictResolveSession session;

    public ConflictResolvedPopup(ConflictResolveSession session) {
        this.session = session;
        setRenderer(renderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public void setMode(boolean resolved) {
        modeResolved = resolved;
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolvedPopup.start()");
        super.start();
        this.setVisible(true);

        new Thread(this).start();
    }

    public boolean handleKey(int keyCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolvedPopup.handleKey("+keyCode+")");
        return true;
    }

    public void run() {
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolvedPopup.run() : MenuController.issueResolved()");
        MenuController.getInstance().issueResolved();
        //PopupController.getInstance().currentSceneStop();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolvedPopup.run() : session.notifyConflictResolved()");
        session.notifyConflictResolved();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictResolvedPopup.stop()");
        super.stop();
        this.setVisible(false);
    }

    public void startClickingEffect() {
    }

    private class OwnRenderer extends Renderer {
        Image i_pop_high_350;
        Image i_pop_gap_379;
        Image i_pop_sha;
        Image i_05_pop_glow_t;
        Image i_05_pop_glow_m;
        Image i_05_pop_glow_b;
//      Image i_icon_g_check;
//      Image i_07_icon_info_disk;

        String title;
        Image icon;
        String[] desc;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);

//          if (modeResolved) {
                g.drawImage(i_05_pop_glow_t, 276, 113, c);
                g.drawImage(i_05_pop_glow_m, 276, 193, 410, 124, c);
                g.drawImage(i_05_pop_glow_b, 276, 317, c);
//          } else {
//              g.drawImage(i_pop_red_478, 242, 79, c);
//          }
            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(i_pop_sha, 306, 364, 350, 79, c);
            g.drawImage(i_pop_high_350, 306, 143, c);
            g.drawImage(i_pop_gap_379, 285, 181, c);

            int w = icon.getWidth(c);
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);

            int x = GraphicUtil.drawStringCenter(g, title, 480 + w / 2, 169);
            g.drawImage(icon, x - w - 5, 150, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_229_229_229);

            int y = 264 + 18 - (18 + (desc.length - 1) * 22) / 2;
            for (int i = 0; i < desc.length; i++) {
                GraphicUtil.drawStringCenter(g, desc[i], 480, y + i * 22);
            }
        }

        public void prepare(UIComponent c) {
            i_pop_high_350 = dataCenter.getImage("pop_high_350.png");
            i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_05_pop_glow_t = dataCenter.getImage("05_pop_glow_t.png");
            i_05_pop_glow_m = dataCenter.getImage("05_pop_glow_m.png");
            i_05_pop_glow_b = dataCenter.getImage("05_pop_glow_b.png");
//          i_icon_g_check = dataCenter.getImage("icon_g_check.png");
//          i_07_icon_info_disk = dataCenter.getImage("07_icon_info_disk.png");

            boolean isDual = session instanceof DualTunerConflictResolveSession;
            if (modeResolved) {
                icon = dataCenter.getImage("icon_g_check.png");
                title = dataCenter.getString("pvr.popup_title_resolved");
                if (isDual) {
                    desc = new String[] { dataCenter.getString("pvr.conflictResolvedDesc") };
                } else {
                    desc = TextUtil.split(dataCenter.getString("pvr.conflict_resovled_many_schedules"), GraphicsResource.FM18, 270, "|");
                }
            } else {
                icon = dataCenter.getImage("07_icon_info_disk.png");
                if (isDual) {
                    title = dataCenter.getString("pvr.popup_title_not_resolved");
                    desc = TextUtil.split(dataCenter.getString("pvr.conflictNotResolved"), GraphicsResource.FM18, 270);
                } else {
                    title = dataCenter.getString("pvr.popup_title_not_resolved_many");
                    desc = TextUtil.split(dataCenter.getString("pvr.conflict_not_resovled_schedules"), GraphicsResource.FM18, 270);
                }
            }

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

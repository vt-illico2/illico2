package com.alticast.illico.pvr.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.*;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.DualTunerConflictResolveSession;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public abstract class CancelOptionPopup extends Popup {
    protected CancelOptionPopupRenderer ren = new CancelOptionPopupRenderer();

    protected CancelOptionPopup() {
        setRenderer(ren);
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public boolean handleKey(int type){
        switch (type) {
        case KeyEvent.VK_UP:
            focus--;
            if (focus < 0) {
                focus = 0;
            }
            break;
        case KeyEvent.VK_DOWN:
            focus++;
            if (focus > 2) {
                focus = 2;
            }
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"CancelOptionPopup.handlKey(VK_ENTER)");
            startClickingEffect();
        	switch (focus) {
        	case 0: // skip this
                processSkipThis();
        		break;
        	case 1: // delete entire
                processDeleteEntire();
        		break;
        	case 2: // cancel
                processCancel();
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_LAST:
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            return true;
        case OCRcEvent.VK_EXIT:
            processCancel();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    protected abstract void processSkipThis();

    protected abstract void processDeleteEntire();

    protected abstract void processCancel();


    public void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        clickEffect.start(334, 279 + (focus * 37), 293, 32);
    }
}


class CancelOptionPopupRenderer extends Renderer {
    Image pop_glow_02;
    Image pop_sha;

    Image pop_high_402;
    Image pop_gap_379;

    Image btn_293;
    Image btn_293_foc;

    String title;
    String btn1;
    String btn2;
    String cancel;
    String[] desc;

    FontMetrics fm17 = FontResource.getFontMetrics(GraphicsResource.BLENDER17);

    public Rectangle getPreferredBounds(UIComponent c) {
        return null;
    }

    public void prepare(UIComponent c) {
        DataCenter dataCenter = DataCenter.getInstance();
        pop_sha = dataCenter.getImage("pop_sha.png");
        pop_glow_02 = dataCenter.getImage("05_pop_glow_02.png");

        pop_high_402 = dataCenter.getImage("pop_high_402.png");
        pop_gap_379 = dataCenter.getImage("pop_gap_379.png");

        btn_293 = dataCenter.getImage("btn_293.png");
        btn_293_foc = dataCenter.getImage("btn_293_foc.png");

        title = dataCenter.getString("pvr.cancelOptionTitle");
        btn1 = dataCenter.getString("pvr.cancelOptionBtn1");
        btn2 = dataCenter.getString("pvr.cancelOptionBtn2");
        cancel = dataCenter.getString("pvr.Cancel");

        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public void setMessage(String msg) {
        desc = TextUtil.split(msg, fm17, 330, "|");
    }

    protected void paint(Graphics g, UIComponent c) {
        g.setColor(GraphicsResource.cDimmedBG);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(pop_sha, 280, 396, 404, 79, c);
        g.drawImage(pop_glow_02, 250, 94, c);

        g.setColor(GraphicsResource.C_35_35_35);
        g.fillRect(280, 124, 402, 276);

        g.drawImage(pop_high_402, 280, 124, c);
        g.drawImage(pop_gap_379, 292, 162, c);

        g.setFont(GraphicsResource.BLENDER24);
        g.setColor(GraphicsResource.C_252_202_4);
        GraphicUtil.drawStringCenter(g, title, 484, 151);

        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_255_255_255);
        for(int a = 0; a < desc.length; a++){
            GraphicUtil.drawStringCenter(g, desc[a], 481, 211 + a * 18);
        }

        g.drawImage(btn_293, 334, 279, c);
        g.drawImage(btn_293, 334, 316, c);
        g.drawImage(btn_293, 334, 353, c);

        g.drawImage(btn_293_foc, 334, 279 + c.getFocus() * 37, c);

        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_0_0_0);
        GraphicUtil.drawStringCenter(g, btn1, 483, 300);
        GraphicUtil.drawStringCenter(g, btn2, 480, 337);
        GraphicUtil.drawStringCenter(g, cancel, 480, 374);
    }
}


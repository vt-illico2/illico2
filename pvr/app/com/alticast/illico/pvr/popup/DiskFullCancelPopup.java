package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class DiskFullCancelPopup extends Popup {

    DataCenter dataCenter = DataCenter.getInstance();
	private OwnRenderer renderer = new OwnRenderer();

    RecordingList list;

	public DiskFullCancelPopup(RecordingList list) {
		setRenderer(renderer);
		prepare();
		this.setBounds(0, 0, 960, 540);
		this.setVisible(false);

        this.list = list;
		focus = 0;
	}

	public boolean handleKey(int type){
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"DiskFullCancelPopup.handleKey("+type+")");
        Log.printInfo("type = " + type);
        switch (type) {
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
        	if (focus == 1) {
                focus = 0;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (focus == 0) {
                focus = 1;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
        	switch (focus) {
        	case 0: // disk
                Core.getInstance().moveToMenu(PvrService.MENU_PVR_MANAGEMENT);
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	case 1: // cancel all
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_EXIT:
        	IssueManager.getInstance().stopPopup(this);
            break;
        case KeyCodes.LAST:
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

	public void stop() {
		this.setVisible(false);
	}

    public void startClickingEffect() {
        clickEffect.start(renderer.getButtonFocusBounds(focus));
    }

    class OwnRenderer extends IssuePopupRenderer {
        public OwnRenderer() {
            super("pvr.almost_popup_title", "pvr.almost_popup_msg_1", "pvr.almost_popup_msg_2",
                "pvr.disk_popup_button_clean", "pvr.Cancel", true);
        }

        public int getSize() {
            return list.size();
        }
        public RecordingRequest getRecordingRequest(int i) {
            return list.getRecordingRequest(i);
        }
    }


}

package com.alticast.illico.pvr.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.alticast.illico.pvr.*;

public class InfoThreeButtonPopup extends Popup {
    public DataCenter dataCenter = DataCenter.getInstance();
    PopupRander popupRander = new PopupRander();
    public static final int BUTTON1 = 0;
    public static final int BUTTON2 = 1;
    public static final int BUTTON3 = 2;

    String programTitle = "";

    String popupButton1 = "";
    String popupButton2 = "";
    String popupButton3 = "";

    public int buttonLen = 3;

    private int focusIndex = 0;

    final String popupTitle;

    public int caller;

    public PvrFooter footer;

    public boolean fillBg = false;

    public InfoThreeButtonPopup(String title) {
        this(title, null);
    }

    public InfoThreeButtonPopup(String title, String footerKey) {
        popupTitle = title;
        setRenderer(popupRander);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);

        if (footerKey != null) {
            footer = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
            footer.addButton(PreferenceService.BTN_EXIT, footerKey);
            footer.setBounds(435, 407, 244, 30);
            this.add(footer);
        }
    }

    public void start(int num) {
        caller = num;
        focusIndex = 0;
        this.setVisible(true);
    }

    public int getCaller(){
        return caller;
    }

    public void stop(){
        this.setVisible(false);
    }

    public void setFocusIndex(int focusIndex) {
        this.focusIndex = focusIndex;
    }

    public int getFocusIndex(){
        return focusIndex;
    }

    public String getProgramTitle() {
        return programTitle;
    }

    public void setProgramTitle(String programTitle) {
        this.programTitle = programTitle;
    }

    public void setButtonMessage(String b1, String b2, String b3) {
        popupButton1 = b1;
        popupButton2 = b2;
        popupButton3 = b3;
        if (popupButton3 == null) {
            buttonLen = 2;
        } else {
            buttonLen = 3;
        }
    }

    public boolean handleKey(int type){
        return keyPressed(type);
    }

    public boolean keyPressed(int type) {
        Log.printInfo("type = " + type);
        switch (type) {
        case KeyEvent.VK_UP:
            focusIndex--;
            if (focusIndex < 0) {
                focusIndex = 0;
            }
            break;
        case KeyEvent.VK_DOWN:
            if (focusIndex < buttonLen - 1) {
                focusIndex++;
            }
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"InfoThreeButtonPopup.handlKey(VK_ENTER)");
            startClickingEffect();
            if (listener != null) {
                listener.selected(focusIndex);
            }
            close();
            break;
        case OCRcEvent.VK_EXIT:
            if (footer != null) {
                footer.clickAnimationByKeyCode(type);
            }
            if (listener != null) {
                listener.selected(-1);
            }
            close();
            break;
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            return true;
        default:
            return false;
        }
        repaint();
        return true;
    }

    private class PopupRander extends Renderer {
        Image pop_glow_02;
        Image pop_sha;

        Image pop_high_402;
        Image pop_gap_379;

        Image btn_293;
        Image btn_293_foc;


        FontMetrics f17 = FontResource.getFontMetrics(GraphicsResource.BLENDER17);

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (fillBg) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);
            }
            g.drawImage(pop_sha, 280, 396, 404, 79, c);
            g.drawImage(pop_glow_02, 250, 94, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(280, 124, 402, 276);

            g.drawImage(pop_high_402, 280, 124, c);
            g.drawImage(pop_gap_379, 292, 162, c);

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 484, 151);

            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_255_255_255);
            String[] str = TextUtil.split(programTitle, g.getFontMetrics(), 330);
            for(int a= 0; a < str.length; a++){
                GraphicUtil.drawStringCenter(g, str[a], 481, 211 + a * 18);
            }

            g.drawImage(btn_293, 334, 279, c);
            g.drawImage(btn_293, 334, 316, c);
            g.drawImage(btn_293, 334, 353, c);

            g.drawImage(btn_293_foc, 334, 279 + focusIndex * 37, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_0_0_0);
            GraphicUtil.drawStringCenter(g, popupButton1, 483, 300);
            GraphicUtil.drawStringCenter(g, popupButton2, 480, 337);
            if (popupButton3 != null) {
                GraphicUtil.drawStringCenter(g, popupButton3, 480, 374);
            }
        }

        public void prepare(UIComponent c) {
            pop_sha = DataCenter.getInstance().getImage("pop_sha.png");
            pop_glow_02 = DataCenter.getInstance().getImage("05_pop_glow_02.png");

            pop_high_402 = DataCenter.getInstance().getImage("pop_high_402.png");
            pop_gap_379 = DataCenter.getInstance().getImage("pop_gap_379.png");

            btn_293 = DataCenter.getInstance().getImage("btn_293.png");
            btn_293_foc = DataCenter.getInstance().getImage("btn_293_foc.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        clickEffect.start(334, 279 + (focusIndex * 37), 293, 32);
    }

}

package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ListSelectPopup extends Popup {

    private String description;

    private String popupTitle;
    private String button02 = "pvr.No";
    private String button01 = "pvr.Yes";

    private Image icon;
    private PvrFooter footer;

    public boolean fillBg = false;

    public ListSelectPopup() {
        this(null);
    }

    public ListSelectPopup(String footerKey) {
        setRenderer(new PopupRander());
        clickEffect = new ClickingEffect(this, 5);
        if (footerKey != null) {
            footer = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
            footer.addButton(PreferenceService.BTN_EXIT, footerKey);
            footer.setBounds(135 + 306, 370, 215, 30);
            this.add(footer);
        }
    }

    public void setPopupTitle(String title) {
        popupTitle = title;
    }

    public void setButton(String button1, String button2) {
        this.button01 = button1;
        this.button02 = button2;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public void setDescription(String cont) {
        description = cont;
    }

    public boolean handleKey(int type) {
        if (listener != null) {
            boolean ret = listener.handleHotKey(type);
            if (ret) {
                return true;
            }
            if (listener.consumeKey(type)) {
                return true;
            }
        }
        switch (type) {
        case KeyEvent.VK_UP:
        case KeyEvent.VK_DOWN:
            return true;
        case KeyEvent.VK_LEFT:
            focus = 0;
            break;
        case KeyEvent.VK_RIGHT:
            focus = 1;
            break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
            if (listener != null) {
                listener.selected(focus);
            }
            close();
            break;
        case OCRcEvent.VK_EXIT:
            if (footer != null) {
                footer.clickAnimationByKeyCode(type);
            }
            if (listener != null) {
                listener.selected(-1);
            }
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(321 +1 + (focus * 164), 318, 154, 32);
    }

    private class PopupRander extends Renderer {

        Image popgolw_m = dataCenter.getImage("05_pop_glow_m.png");
        Image popgolw_b = dataCenter.getImage("05_pop_glow_b.png");
        Image popgolw_t = dataCenter.getImage("05_pop_glow_t.png");
        Image popsha = dataCenter.getImage("pop_sha.png");
        Image pophigh = dataCenter.getImage("pop_high_350.png");
        Image popgap = dataCenter.getImage("pop_gap_379.png");

        Image pop_focus_dim = dataCenter.getImage("05_focus_dim.png");
        Image pop_focus = dataCenter.getImage("05_focus.png");

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (fillBg) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);
            }
            g.drawImage(popgolw_m, 276, 193, 410, 124, c);
            g.drawImage(popgolw_b, 276, 317, c);
            g.drawImage(popgolw_t, 276, 113, c);

            g.drawImage(popsha, 306, 364, 350, 0, c);
            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(306, 143, 350, 224);
            g.drawImage(pophigh, 306, 143, c);
            g.drawImage(popgap, 285, 181, c);

            if (focus == 0) {
                g.drawImage(pop_focus, 321, 318, c);
                g.drawImage(pop_focus_dim, 485, 318, c);
            } else if (focus == 1) {
                g.drawImage(pop_focus_dim, 321, 318, c);
                g.drawImage(pop_focus, 485, 318, c);
            }

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 484, 169);

            int strWidthHalf = g.getFontMetrics().stringWidth(popupTitle) / 2;

            if (icon != null) {
                g.drawImage(icon, 484 - strWidthHalf - 34, 151, c);
            }
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);

            String[] str = TextUtil.split(description, g.getFontMetrics(), 290);

            for(int a= 0; a < str.length; a++){
                int width = (str.length - 1) * 5 ;
                GraphicUtil.drawStringCenter(g, str[a], 482, 234 - width + a * 18);
            }
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_4_4_4);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(button01), 399, 339);
            GraphicUtil.drawStringCenter(g, dataCenter.getString(button02), 564, 339);
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

}
package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.util.*;
import org.ocap.shared.dvr.RecordingRequest;
import org.dvb.ui.*;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.gui.GraphicsResource;


public class ManyConflictCautionPopup extends Popup {

    ManyTunerConflictResolveSession session;
    long min = Long.MAX_VALUE;
    long max = 0;
    int listSize;
    ArrayList renderingItems;
    ArrayList recordings;

    ArrayList conflictRanges;

    ManyConflictCautionPopupRenderer ren = new ManyConflictCautionPopupRenderer();
    boolean isRecordingConflict;

    public RecordingRequest firstValid = null;


    public ManyConflictCautionPopup(ManyTunerConflictResolveSession session) {
        this.session = session;
        setRenderer(ren);
        isRecordingConflict = session.getTunerRequestor() == null;
    }

    public RecordingRequest[] setRecordings(RecordingRequest req, RecordingRequest[] overlapped) {
        recordings = new ArrayList(overlapped.length + 2);
        if (req != null) {
            recordings.add(req);
        }
        for (int i = 0; i < overlapped.length; i++) {
            recordings.add(overlapped[i]);
        }

        this.listSize = recordings.size();
        RecordingRequest[] ret = new RecordingRequest[listSize];

        renderingItems = new ArrayList(listSize + 1);
        long cur = System.currentTimeMillis();
        for (int i = 0; i < listSize; i++) {
            RecordingRequest r = (RecordingRequest) recordings.get(i);
            ret[i] = r;
            ConflictRenderingItem cri = new ConflictRenderingItem(r);
            renderingItems.add(cri);
            min = Math.min(cri.from, min);
            max = Math.max(cri.to, max);

            if (firstValid == null && cri.from > 0) {
                firstValid = r;
            }

            if (cri.stateGroup == RecordingListManager.PENDING) {
                try {
                    TvProgram[] p = ConflictResolveSession.getOtherShowtimes(r, cur);
                    if (p != null && p.length > 0) {
                        cri.otherShowtimes = p;
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        updateConflictRanges();
        return ret;
    }

    public void updateConflictRanges() {
        this.conflictRanges = getConflictRanges();
        ren.setConflictRanges(this.conflictRanges, min, max);
    }

    private ArrayList getConflictRanges() {
        Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.getConflictRanges() : isRecordingConflict = "+isRecordingConflict);
        if (isRecordingConflict) {
            //->Kenneth[2015.3.14] UHD 의 경우 max 를 다르게 넣어주도록 함
            if (session.isUhdConflict) {
                return ManyTunerConflictResolveSession.getConflictRanges(renderingItems, ConfigManager.getInstance().pvrConfig.uhdMaxTuners);
            } else {
                return ManyTunerConflictResolveSession.getConflictRanges(renderingItems, Environment.TUNER_COUNT);
            }
        } else {
            int size = renderingItems.size();
            boolean found = false;
            for (int i = 0; i < size; i++) {
                ConflictRenderingItem cri = (ConflictRenderingItem) renderingItems.get(i);
                if (cri != null && cri.action != ConflictRenderingItem.KEEP) {
                    return new ArrayList(4);
                }
            }
            ArrayList list = new ArrayList(4);
            list.add(new long[] { min, max });
            return list;
        }
    }

    // overriden
    public void stop() {
        Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.stop()");
        super.stop();
        ren.stop();
    }

    // overriden
    public void startClickingEffect() {
//        clickEffect.start(333+1, 365 + btnFocus * 37, 293, 32);
    }

    public long getDate() {
        return min;
    }

    public boolean handleKey(int code) {
        switch (code) {
            case OCRcEvent.VK_POWER:
            case OCRcEvent.VK_EXIT:
                Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.handlKey(VK_EXIT||VK_ENTER)");
                session.showCanceledPopup();
                return false;
            case OCRcEvent.VK_UP:
                if (focus > 0) {
                    focus--;
                    repaint();
                }
                return true;
            case OCRcEvent.VK_DOWN:
                if (focus < listSize - 1) {
                    focus++;
                    repaint();
                }
                return true;
            case OCRcEvent.VK_ENTER:
                Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.handlKey(VK_ENTER)");
                keyEnter();
                return true;
            case KeyCodes.COLOR_A:
                Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.handlKey(COLOR_A)");
                keyA();
                return true;
            case KeyCodes.COLOR_C:
                Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.handlKey(COLOR_C)");
                keyC();
                return true;
            case KeyCodes.COLOR_B:
                Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.handlKey(COLOR_B)");
                if (Log.EXTRA_ON) {
                    MetadataDiagScreen.getInstance().startRecording(((ConflictRenderingItem) renderingItems.get(focus)).request, false);
                }
                return false;
//            case KeyCodes.PIP_MOVE:
//                if (Log.EXTRA_ON) {
//                    try {
//                        ((ConflictRenderingItem) renderingItems.get(focus)).request.delete();
//                    } catch (Exception ex) {
//                        Log.print(ex);
//                    }
//                }
//                return false;

        }
        return false;
    }

    private void keyEnter() {
        int index = focus;
        ConflictRenderingItem cri = (ConflictRenderingItem) renderingItems.get(index);
        if (cri == null) {
            return;
        }
        if (cri.action == ConflictRenderingItem.DELETE || cri.action == ConflictRenderingItem.SAVE) {
            cri.action = ConflictRenderingItem.KEEP;
        } else {
            switch (cri.stateGroup) {
                case RecordingListManager.PENDING:
                    if (cri.groupKey != null) {
                        session.showCancelOptionPopup(cri);
                        return;
                    }
                    cri.action = ConflictRenderingItem.DELETE;
                    break;
                case RecordingListManager.IN_PROGRESS:
                    cri.action = ConflictRenderingItem.SAVE;
                    break;
                default:
                    return;
            }
        }
        updateConflictRanges();
        repaint();
    }

    private void keyC() {
        ConflictRenderingItem cri = (ConflictRenderingItem) renderingItems.get(focus);
        if (cri == null || cri.otherShowtimes == null) {
            return;
        }
        switch (cri.action) {
            case ConflictRenderingItem.KEEP:
            case ConflictRenderingItem.RESCHEDULE:
                session.showOtherShowTimesPopup(cri);
                break;
        }
    }

    private void keyA() {
        ArrayList ranges = conflictRanges;
        if (ranges != null) {
            Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.keyA() : conflictRanges = "+ranges.size());
        }
        if (ranges != null && !ranges.isEmpty()) {
            Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.keyA() : Do nothing");
            return;
        }
        Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.keyA() : Call session.showResolvedPopup("+renderingItems+")");
        session.showResolvedPopup(renderingItems);
    }

}


class ManyConflictCautionPopupRenderer extends Renderer {
    DataCenter dataCenter = DataCenter.getInstance();

    private static final int Y_GAP = 34;
    private static final int GRAPH_X = 409;
    private static final int GRAPH_WIDTH = 230;

    Color cBackground = new DVBColor(0, 0, 0, 204);
    Color cPopupBackground = new Color(33, 33, 33);
    Color cBlock = new Color(41, 41, 41);
    Color cGrayText = new Color(133, 133, 133);
    Color cDiv1 = new Color(200, 200, 200, 150);
    Color cDiv2 = new Color(150, 150, 150, 120);

    Image i_05_pop_rmdbg;
    Image i_07_respop_glow_2;
    Image i_07_respop_title;
    Image i_07_respop_high_2;
    Image i_12_ppop_sha;
    Image i_icon_noti_red;
    Image i_07_res_pgbg_b_2;
    Image i_07_res_pgbg_m_2;
    Image i_07_res_pgbg_t_2;
    Image i_07_res_foc_rc;
    Image i_07_res_line;
    Image i_07_res_line_foc;
    Image i_07_bar_rec02;
    Image i_07_bar_rec03;
    Image i_07_bar_rec04;
    Image i_07_bar_rec06;
    Image i_02_ars_t;
    Image i_02_ars_b;
    Image i_07_res_pgsh_t_2;
    Image i_07_res_pgsh_b_2;
    Image i_icon_reschedule;
    Image i_icon_reschedule_f;
    Image i_btn_a_dim;

    Image i_btn_c;
    Image i_btn_ok;
    Image i_btn_a;
    Image i_btn_exit;

    String title;
    String desc0;
    String desc1;

    String sKeep;
    String sDelete;
    String sModify;
    String sStopAndSave;
    String sWillBeDeleted;
    String sWillBeStoppedSaved;
    String sCancel;
    String sApply;

    boolean isRecordingConflict;
    int rowCount;
    int middlePos;

    long min, max;

    DVBBufferedImage bar;
    ArrayList conflictPositions;

    //->Kenneth[2015.2.14] 4K
    boolean isUhd = false;

    public Rectangle getPreferredBounds(UIComponent c) {
        return Constants.SCREEN_BOUNDS;
    }

    public void prepare(UIComponent c) {
        Log.printDebug("ManyConflictCautionPopupRenderer.prepare");
        i_05_pop_rmdbg = dataCenter.getImage("05_pop_rmdbg.png");
        i_07_respop_glow_2 = dataCenter.getImage("07_respop_glow_2.png");
        i_07_respop_title = dataCenter.getImage("07_respop_title.png");
        i_07_respop_high_2 = dataCenter.getImage("07_respop_high_2.png");
        i_12_ppop_sha = dataCenter.getImage("12_ppop_sha.png");
        i_icon_noti_red = dataCenter.getImage("icon_noti_red.png");
        i_07_res_pgbg_b_2 = dataCenter.getImage("07_res_pgbg_b_2.png");
        i_07_res_pgbg_m_2 = dataCenter.getImage("07_res_pgbg_m_2.png");
        i_07_res_pgbg_t_2 = dataCenter.getImage("07_res_pgbg_t_2.png");
        i_07_res_foc_rc = dataCenter.getImage("07_res_foc_rc.png");
        i_07_res_line = dataCenter.getImage("07_res_line.png");
        i_07_res_line_foc = dataCenter.getImage("07_res_line_foc.png");
        i_07_bar_rec02 = dataCenter.getImage("07_bar_rec02.png");
        i_07_bar_rec03 = dataCenter.getImage("07_bar_rec03.png");
        i_07_bar_rec04 = dataCenter.getImage("07_bar_rec04.png");
        i_07_bar_rec06 = dataCenter.getImage("07_bar_rec06.png");
        i_02_ars_t = dataCenter.getImage("02_ars_t.png");
        i_02_ars_b = dataCenter.getImage("02_ars_b.png");
        i_07_res_pgsh_t_2 = dataCenter.getImage("07_res_pgsh_t_2.png");
        i_07_res_pgsh_b_2 = dataCenter.getImage("07_res_pgsh_b_2.png");
        i_icon_reschedule_f = dataCenter.getImage("icon_reschedule_f.png");
        i_icon_reschedule = dataCenter.getImage("icon_reschedule.png");
        i_btn_a_dim = dataCenter.getImage("btn_a_dim.png");

        Hashtable btnTable = (Hashtable) SharedMemory.getInstance().get("Footer Img");
        if (btnTable != null) {
            i_btn_c = (Image) btnTable.get(PreferenceService.BTN_C);
//            i_btn_ok = (Image) btnTable.get(PreferenceService.BTN_OK);
            i_btn_a = (Image) btnTable.get(PreferenceService.BTN_A);
            i_btn_exit = (Image) btnTable.get(PreferenceService.BTN_EXIT);
        }
        i_btn_ok = dataCenter.getImage("btn_ok.png");

        ManyConflictCautionPopup p = (ManyConflictCautionPopup) c;
        isRecordingConflict = p.isRecordingConflict;
        isUhd = p.session.isUhdConflict;
        int listSize = p.listSize;
        if (isRecordingConflict) {
            rowCount = 9;
            title = dataCenter.getString("pvr.gw_conflict_popup_title");
            //->Kenneth[2015.2.14] 4K
            if (isUhd) {
                desc0 = dataCenter.getString("pvr.uhd_conflict_desc0");
                desc0 = TextUtil.replace(dataCenter.getString("pvr.uhd_conflict_desc0"),
                            "[UHD MAX]", ConfigManager.getInstance().pvrConfig.uhdMaxTuners+"");
                desc1 = dataCenter.getString("pvr.uhd_conflict_desc1");
            } else {
                if (listSize <= 9) {
                    desc0 = TextUtil.replace(dataCenter.getString("pvr.gw_conflict_popup_desc0"),
                                "[DATE]", Formatter.getCurrent().getDayText(p.getDate()).toUpperCase());
                    desc1 = dataCenter.getString("pvr.gw_conflict_popup_desc1");
                } else {
                    desc0 = TextUtil.replace(
                                TextUtil.replace(dataCenter.getString("pvr.gw_conflict_popup_desc3"), "%%", String.valueOf(listSize)),
                                "[DATE]", Formatter.getCurrent().getDayText(p.getDate()).toUpperCase() );
                    desc1 = dataCenter.getString("pvr.gw_conflict_popup_desc4");
                }
            }
        } else {
            rowCount = 8;
            title = dataCenter.getString("pvr.gw_conflict_popup_title2");
            desc0 = dataCenter.getString("pvr.gw_conflict_popup_desc5");
            desc1 = dataCenter.getString("pvr.gw_conflict_popup_desc6");
        }
        middlePos = (rowCount - 1) / 2;

        min = p.min;
        max = p.max;
        ArrayList list = p.renderingItems;
        for (int i = 0; i < list.size(); i++) {
            ConflictRenderingItem cri = (ConflictRenderingItem) list.get(i);
            cri.prepare(GraphicsResource.FM18, 172, min, max, GRAPH_X, GRAPH_WIDTH);
        }

        sDelete = dataCenter.getString("pvr.delete");
        sModify = dataCenter.getString("pvr.Modify");
        sStopAndSave = dataCenter.getString("pvr.stop_and_save");
        sWillBeDeleted = dataCenter.getString("pvr.will_be_deleted");
        sWillBeStoppedSaved = dataCenter.getString("pvr.will_be_stopped_and_saved");
        sKeep = dataCenter.getString("pvr.Keep");
        sCancel = dataCenter.getString("pvr.Cancel");
        sApply = dataCenter.getString("pvr.apply_changes");

        FrameworkMain.getInstance().getImagePool().waitForAll();

        if (bar != null) {
            bar.dispose();
        }
        bar = new DVBBufferedImage(230, 9);
        Graphics g = bar.getGraphics();
        g.drawImage(i_07_bar_rec03, 0, 0, c);
        g.drawImage(i_07_bar_rec03, 110, 0, c);
        g.drawImage(i_07_bar_rec03, 220, 0, c);
        g.dispose();

    }

    protected void stop() {
        if (bar != null) {
            bar.dispose();
            bar = null;
        }
    }

    protected void setConflictRanges(ArrayList conflictRanges, long min, long max) {
        Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.setConflictRanges("+conflictRanges+", "+min+", "+max+")");
        //->Kenneth[2015.3.14] : 4K : 그냥 null 체크 코드하나 넣었음.
        if (conflictRanges == null) return;

        this.min = min;
        this.max = max;
        int size = conflictRanges.size();
        Log.printDebug(App.LOG_HEADER+"ManyConflictCautionPopup.setConflictRanges() : size = "+size);
        ArrayList list = new ArrayList(size + 1);
        for (int i = 0; i < size; i++) {
            long[] range = (long[]) conflictRanges.get(i);
            list.add(new int[] { getX(range[0]), getX(range[1]) });
        }
        conflictPositions = list;
    }

    protected void paint(Graphics g, UIComponent c) {
        ManyConflictCautionPopup p = (ManyConflictCautionPopup) c;
        // background
        g.setColor(cBackground);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(i_05_pop_rmdbg, 429, 82, c);
        int shadowY = 479;
        if (!isRecordingConflict) {
            g.translate(0, 10);
            shadowY -= 20;
        }
        g.drawImage(i_12_ppop_sha, 81, shadowY, 796, 56, c);
        g.drawImage(i_07_respop_glow_2, 0, 0, c);
        g.setColor(cPopupBackground);
        g.fillRect(81, 49, 796, isRecordingConflict ? 433 : 413);
        g.drawImage(i_07_respop_title, 203, 87, c);
        g.drawImage(i_07_respop_high_2, 81, 49, c);

        // title
        int w = i_icon_noti_red.getWidth(c);
        g.setFont(GraphicsResource.BLENDER24);
        g.setColor(GraphicsResource.C_255_203_0);
        int x = GraphicUtil.drawStringCenter(g, title, 483 + w/2, 76);
        g.drawImage(i_icon_noti_red, x - w - 5, 57, c);

        // desc
        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(Color.white);
        GraphicUtil.drawStringCenter(g, desc0, 478, 118);
        GraphicUtil.drawStringCenter(g, desc1, 478, 138);

        int bodyH = rowCount * Y_GAP - 16;
        // contents
        g.drawImage(i_07_res_pgbg_b_2, 101, 168 + bodyH, c);
        g.drawImage(i_07_res_pgbg_m_2, 101, 168, 756, bodyH, c);
        g.drawImage(i_07_res_pgbg_t_2, 101, 158, c);

        int focus = c.getFocus();
        int listSize = p.listSize;
        int over = listSize - rowCount;
        int move = focus - middlePos;
        int firstIndex;
        if (over > 0 && move > 0) {
            firstIndex = Math.min(over, move);
        } else {
            firstIndex = 0;
        }

        g.translate(0, 157);

        int size = Math.min(rowCount, listSize);
        int index = firstIndex;

        ConflictRenderingItem cri;
        for (int i = 0; i < rowCount; i++) {
            if (index % 2 == 1) {
                g.setColor(cBlock);
                g.fillRect(102, 3 + Y_GAP * i, 754, 34);
            }
            index++;
        }

        // focus
        g.drawImage(i_07_res_foc_rc, 99, (focus - firstIndex) * Y_GAP, c);


        ArrayList positions = conflictPositions;
        boolean hasConflict = positions != null && !positions.isEmpty();

        //->Kenneth[2015.2.14] 4K : UHD conflict 의 경우 컬러를 그린이 아닌 레드로 그리기 위해서
        // 2015.3.14 : 강제로 hasConflict 를 해서는 안될 것 같음. 제대로 conflictPosition 에 값을 넣어야 할 것임
        // 강제로 hasConflict 만 바꿨더니 제대로 동작하지 않음.
//        if (isUhd) {
//            hasConflict = true;
//        }

        index = firstIndex;
        for (int i = 0; i < size; i++) {
            cri = null;
            try {
                cri = (ConflictRenderingItem) p.renderingItems.get(index);
            } catch (Exception ex) {
            }

            if (cri != null) {
                g.setFont(GraphicsResource.BLENDER17);
                int sx = 843;
                boolean hasFocus = index == focus;
                if (hasFocus) {
                    // with focus
                    g.drawImage(i_07_res_line_foc, 397, 168-157, c);
                    g.setColor(Color.black);

                    if (cri.action == ConflictRenderingItem.DELETE || cri.action == ConflictRenderingItem.SAVE) {
                        // revert button on focus
                        sx -= GraphicsResource.FM17.stringWidth(sKeep);
                        g.drawString(sKeep, sx, 182-157);
                        sx -= (4 + i_btn_ok.getWidth(c));
                        g.drawImage(i_btn_ok, sx, 166-157, c);
                    } else {
                        switch (cri.stateGroup) {
                        case RecordingListManager.PENDING:
                            // delete button on focus
                            sx -= GraphicsResource.FM17.stringWidth(sDelete);
                            g.drawString(sDelete, sx, 182-157);
                            sx -= (4 + i_btn_ok.getWidth(c));
                            g.drawImage(i_btn_ok, sx, 166-157, c);
                            // modify button on focus
                            if (cri.otherShowtimes != null) {
                                sx -= (9 + GraphicsResource.FM17.stringWidth(sModify));
                                g.drawString(sModify, sx, 182-157);
                                sx -= (4 + i_btn_c.getWidth(c));
                                g.drawImage(i_btn_c, sx, 166-157, c);
                            }
                            break;
                        case RecordingListManager.IN_PROGRESS:
                            // stop & save button on focus
                            sx -= GraphicsResource.FM17.stringWidth(sStopAndSave);
                            g.drawString(sStopAndSave, sx, 182-157);
                            sx -= (4 + i_btn_ok.getWidth(c));
                            g.drawImage(i_btn_ok, sx, 166-157, c);
                            break;
                        }
                    }

                } else {
                    // no focus
                    g.drawImage(i_07_res_line, 396, 170-157, c);
                    switch (cri.action) {
                    case ConflictRenderingItem.DELETE:
                        g.setColor(cGrayText);
                        GraphicUtil.drawStringRight(g, sWillBeDeleted, sx, 182-157);
                        break;
                    case ConflictRenderingItem.SAVE:
                        g.setColor(cGrayText);
                        GraphicUtil.drawStringRight(g, sWillBeStoppedSaved, sx, 182-157);
                        break;
                    case ConflictRenderingItem.RESCHEDULE:
                        g.setColor(cGrayText);
                        break;
                    default:
                        g.setColor(Color.white);
                    }
                }
                // logo & title
                g.drawImage(cri.logo, 112, 162-157, c);
                g.setFont(GraphicsResource.BLENDER18);
                g.drawString(cri.shortenTitle, 220, 216-157-Y_GAP);

                g.setFont(GraphicsResource.BLENDER15);
                switch (cri.action) {
                    case ConflictRenderingItem.KEEP:
                        // green or gray bar
                        if (hasConflict) {
                            g.drawImage(i_07_bar_rec02, cri.graphLeft, 178-157, cri.graphRight - cri.graphLeft, 9, c);
                        } else {
                            g.drawImage(i_07_bar_rec04, cri.graphLeft, 178-157, cri.graphRight - cri.graphLeft, 9, c);
                        }
                        g.drawString(cri.time, Math.max(cri.graphRight - GraphicsResource.FM15.stringWidth(cri.time), GRAPH_X), 175-157);
                        break;
                    case ConflictRenderingItem.DELETE:
                    case ConflictRenderingItem.SAVE:
                        // gray check bar
                        g.drawImage(i_07_bar_rec06, cri.graphLeft, 178-157, cri.graphRight, 178-157+9,
                                                    cri.graphLeft-GRAPH_X, 0, cri.graphRight-GRAPH_X, 9, c);
                        g.drawString(cri.time, Math.max(cri.graphRight - GraphicsResource.FM15.stringWidth(cri.time), GRAPH_X), 175-157);
                        break;
                    case ConflictRenderingItem.RESCHEDULE:
                        g.drawImage(hasFocus ? i_icon_reschedule_f : i_icon_reschedule, 405, 168-157, c);
                        g.drawString(cri.rescheduleText, 429, 182-157);
                        break;

                }
//                if (Log.EXTRA_ON) {
//                    g.setColor(Color.green);
//                    g.drawString(cri.graphLeft + " " + cri.graphRight, cri.graphRight + 2, 175-157);
//                }
            }
            index++;
            g.translate(0, Y_GAP);
        }
        g.translate(0, -Y_GAP * size - 157);

        if (hasConflict) {
            // conflict red bar
            int pSize = positions.size();
            for (int j = 0; j < pSize; j++) {
                int[] range = (int[]) positions.get(j);
                index = firstIndex;
                for (int i = 0; i < size; i++) {
                    cri = null;
                    try {
                        cri = (ConflictRenderingItem) p.renderingItems.get(index);
                    } catch (Exception ex) {
                    }
                    if (cri != null && cri.action == ConflictRenderingItem.KEEP) {
                        int left = Math.max(cri.graphLeft, range[0]);
                        int right = Math.min(cri.graphRight, range[1]);
                        if (range[0] == range[1]) {
                            if (right == range[1]) {
                                left--;
                            } else {
                                right++;
                            }
                        }
                        if (left < right) {
                            int y = 178 + Y_GAP * i;
                            g.drawImage(bar, left, y, right, y+9, left-GRAPH_X, 0, right-GRAPH_X, 9, c);
                        }
                    }
                    index++;
                }
            }
        }

        if (firstIndex > 0) {
            g.drawImage(i_07_res_pgsh_t_2, 102, 159, c);
            g.drawImage(i_02_ars_t, 472, 150, c);
        }

        if (firstIndex + rowCount < listSize) {
            g.drawImage(i_07_res_pgsh_b_2, 102, 432, c);
            g.drawImage(i_02_ars_b, 471, 459, c);
        }

        g.setFont(GraphicsResource.BLENDER17);
        g.setColor(GraphicsResource.C_241_241_241);

        if (!isRecordingConflict) {
            g.translate(0, -20);
        }

        // cancel footer
        int sx = 864;
        sx -= GraphicsResource.FM17.stringWidth(sCancel);
        g.drawString(sCancel, sx, 493+10);
        sx -= (3 + i_btn_exit.getWidth(c));
        g.drawImage(i_btn_exit, sx, 478+10, c);
        // divider
        sx -= 10;
        g.setColor(cDiv1);
        g.drawLine(sx, 482+10, sx, 482+13+10);
        g.setColor(cDiv2);
        g.drawLine(sx+1, 482+10, sx+1, 482+13+10);
        // a button
        sx -= 10;
        sx -= GraphicsResource.FM17.stringWidth(sApply);
        Image imgA;
        if (hasConflict) {
            g.setColor(cGrayText);
            g.drawString(sApply, sx, 493+10);
            imgA = i_btn_a_dim;
        } else {
            g.setColor(GraphicsResource.C_241_241_241);
            g.drawString(sApply, sx, 493+10);
            imgA = i_btn_a;
        }
        sx -= (6 + imgA.getWidth(c));
        g.drawImage(imgA, sx, 478+10, c);

        if (!isRecordingConflict) {
            g.translate(0, 10);
        }
    }

    private int getX(long time) {
        return (int) ((time - min) * GRAPH_WIDTH / (max - min)) + GRAPH_X;
    }


}


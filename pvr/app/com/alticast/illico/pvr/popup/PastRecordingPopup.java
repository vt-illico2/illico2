package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.tv.util.*;

import org.havi.ui.event.HRcEvent;
import org.ocap.shared.media.TimeShiftControl;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;

public class PastRecordingPopup extends BottomPopup
                            implements PopupListener, TVTimerWentOffListener, ChannelEventListener {

    public static TunerController tunerController = null;
    public static TimeShiftControl timeShiftControl = null;
    public static long startTime = 0;
    public static long endTime = 0;
    public static TvProgram current;
    public static TvProgram program;
    public static TvChannel channel;

    private String programTitle;
    private String currentTitle;

    private TVTimerSpec refreshTimer;

    public PastRecordingPopup() {
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.record_past_btn_3");
        setListener(this);

        refreshTimer = new TVTimerSpec();
        refreshTimer.setTime(5000L);
        refreshTimer.setRepeat(true);
        refreshTimer.setAbsolute(false);
        refreshTimer.setRegular(false);
        refreshTimer.addTVTimerWentOffListener(this);
    }

    public synchronized void start() {
        DataCenter dataCenter = DataCenter.getInstance();
        title = dataCenter.getString("pvr.record_past_title");

        try {
            programTitle = program.getTitle();
            currentTitle = current.getTitle();
        } catch (Exception ex) {
            Log.print(ex);
        }

        this.buttons = new String[] { dataCenter.getString("pvr.record_past_btn_1"),
                                   dataCenter.getString("pvr.record_past_btn_2") };
        this.texts = new String[] {
            dataCenter.getString("pvr.record_past_msg_1"),
            dataCenter.getString("pvr.record_past_msg_2"),
            dataCenter.getString("pvr.record_past_msg_3")
        };
        int width = BottomPopupRenderer.fmText.stringWidth(texts[0]);
        String s = TextUtil.shorten(programTitle, BottomPopupRenderer.fmText, BottomPopupRenderer.MAX_TEXT_WIDTH - width);
        texts[0] = TextUtil.replace(texts[0], "%1", s);

        width = BottomPopupRenderer.fmText.stringWidth(texts[1]);
        s = TextUtil.shorten(currentTitle, BottomPopupRenderer.fmText, BottomPopupRenderer.MAX_TEXT_WIDTH - width);
        texts[1] = TextUtil.replace(texts[1], "%2", s);

        width = BottomPopupRenderer.fmText.stringWidth(buttons[1]);
        s = TextUtil.shorten(currentTitle, BottomPopupRenderer.fmButton, 275 - width);
        buttons[1] = buttons[1] + " " + s;

        updateLastMinutes();

        TVTimer.getTimer().deschedule(refreshTimer);
        try {
            refreshTimer = TVTimer.getTimer().scheduleTimerSpec(refreshTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
        super.start();

        EpgService es = (EpgService) dataCenter.get(EpgService.IXC_NAME);
        try {
            es.getChannelContext(0).addChannelEventListener(this);
            es.getChannelContext(1).addChannelEventListener(this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    public synchronized void stop() {
        TVTimer.getTimer().deschedule(refreshTimer);
        super.stop();
        timeShiftControl = null;

        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        try {
            es.getChannelContext(0).removeChannelEventListener(this);
            es.getChannelContext(1).removeChannelEventListener(this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    /** ChannelEventListener implemetation */
    public void selectionRequested(int id, short type) {
        close();
    }

    public void selectionBlocked(int sid, short type) {
        close();
    }

    private long updateLastMinutes() {
        long bb = PvrUtil.getTime(timeShiftControl.getBeginningOfBuffer());
        long time = endTime - Math.max(startTime, bb);

        int min = (int) Math.ceil((double) time / Constants.MS_PER_MINUTE);

//        this.texts[1] = TextUtil.replace(DataCenter.getInstance().getString("pvr.record_past_msg_2"),
//                         TextUtil.TEXT_REPLACE_MARKER, String.valueOf(min));

        String s = TextUtil.replace(DataCenter.getInstance().getString("pvr.record_past_btn_1"),
                        TextUtil.TEXT_REPLACE_MARKER, String.valueOf(min));

        int width = BottomPopupRenderer.fmText.stringWidth(s);
        String title = TextUtil.shorten(programTitle, BottomPopupRenderer.fmButton, 275 - width);
        buttons[0] = s + " " + title;
        return time;
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        long time = updateLastMinutes();
        if (time <= 0) {
            close();
        } else {
            repaint();
        }
    }

    public boolean handleHotKey(int code) {
        return false;
    }

    public boolean consumeKey(int code) {
        return false;
    }

    public void canceled() {
    }

    public void selected(int focus) {
        Log.printDebug("PastRecordingPopup.selected = " + focus);
        close();
        switch (focus) {
            case 0: // save
                RecordManager.getInstance().saveBuffer(tunerController.getTunerIndex(), channel, program);
                break;
            case 1: // record current
                try {
                    PvrServiceImpl.getInstance().record(channel, current);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                break;
        }
    }

}

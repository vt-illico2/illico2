package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class DiskIssuePopup extends Popup {

    DataCenter dataCenter = DataCenter.getInstance();
	private OwnRenderer renderer = new OwnRenderer();

    String title;
    byte diskStatus;

	public DiskIssuePopup(byte status) {
		setRenderer(renderer);
		prepare();
		this.setBounds(0, 0, 960, 540);
		this.setVisible(false);

        this.diskStatus = status;
        title = dataCenter.getString("pvr.disk_popup_title_" + status);
        if (diskStatus < DiskIssue.DISK_FULL_ISSUE) {
            title = title + " " + StorageInfoManager.getInstance().getFreePercent() + "%";
        }
	}

	public void start() {
		focus = 0;
		super.start();
        setVisible(true);
	}

	public boolean handleKey(int type){
        Log.printInfo("type = " + type);
        switch (type) {
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
        	if (focus == 1) {
                focus = 0;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (focus == 0) {
                focus = 1;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
        	switch (focus) {
        	case 0: // clean
                Core.getInstance().moveToMenu(PvrService.MENU_PVR_MANAGEMENT);
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	case 1: // cancel
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_EXIT:
        	IssueManager.getInstance().stopPopup(this);
            break;
        case KeyCodes.LAST:
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

	public void stop() {
		this.setVisible(false);
	}

    public void startClickingEffect() {
        if (focus == 0) {
            clickEffect.start(321+1, 318, 154, 32);
        } else {
            clickEffect.start(485+1, 318, 154, 32);
        }
    }

	private class OwnRenderer extends Renderer {
		Image i_05_pop_glow_m;
		Image i_05_pop_glow_b;
		Image i_05_pop_glow_t;
		Image i_pop_sha;
		Image i_pop_high_350;
		Image i_pop_gap_379;
		Image i_07_icon_info_disk;
		Image i_05_focus_dim;
		Image i_05_focus;

        String[] message;
        int textY;

		public Rectangle getPreferredBounds(UIComponent c) {
			return null;
		}

		protected void paint(Graphics g, UIComponent c) {
			g.setColor(GraphicsResource.cDimmedBG);
        	g.fillRect(0, 0, 960, 540);

            g.drawImage(i_05_pop_glow_m, 276, 193, 410, 124, c);
            g.drawImage(i_05_pop_glow_b, 276, 317, c);
            g.drawImage(i_05_pop_glow_t, 276, 113, c);
            g.drawImage(i_pop_sha, 306, 364, 350, 79, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(306, 143, 350, 224);

            g.drawImage(i_pop_high_350, 306, 143, c);
            g.drawImage(i_pop_gap_379, 285, 181, c);

			Image icon = i_07_icon_info_disk;
			int w = icon.getWidth(c);
			g.setFont(GraphicsResource.BLENDER24);
			g.setColor(GraphicsResource.C_255_203_0);
			int x = GraphicUtil.drawStringCenter(g, title, 483 + w / 2, 169);
			g.drawImage(icon, x - w - 5, 151, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(Color.white);

            for (int i = 0; i < message.length; i++) {
                GraphicUtil.drawStringCenter(g, message[i], 483, textY + i * 20);
            }

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_3_3_3);
            if (focus == 0) {
                g.drawImage(i_05_focus, 321, 318, c);
                g.drawImage(i_05_focus_dim, 485, 318, c);
            } else {
                g.drawImage(i_05_focus_dim, 321, 318, c);
                g.drawImage(i_05_focus, 485, 318, c);
            }
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.disk_popup_button_clean"), 399, 339);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.disk_popup_button_later_" + diskStatus), 564, 339);
		}

		public void prepare(UIComponent c) {
            i_05_pop_glow_m = dataCenter.getImage("05_pop_glow_m.png");
            i_05_pop_glow_b = dataCenter.getImage("05_pop_glow_b.png");
            i_05_pop_glow_t = dataCenter.getImage("05_pop_glow_t.png");
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_pop_high_350 = dataCenter.getImage("pop_high_350.png");
            i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
            i_07_icon_info_disk = dataCenter.getImage("07_icon_info_disk.png");
            i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
            i_05_focus = dataCenter.getImage("05_focus.png");

            String msg = dataCenter.getString("pvr.disk_popup_msg_" + diskStatus);
            if (diskStatus == DiskIssue.DISK_WARNING_ISSUE) {
                msg = TextUtil.replace(msg, "@", String.valueOf(StorageInfoManager.WARNING_LEVEL));
            } else if (diskStatus == DiskIssue.DISK_CRITICAL_ISSUE) {
                msg = TextUtil.replace(msg, "@", String.valueOf(StorageInfoManager.CRITICAL_LEVEL));
            }
            FontMetrics fm = FontResource.getFontMetrics(GraphicsResource.BLENDER18);
            message = TextUtil.split(msg, fm, 320, "|");

            textY = 253 - ((message.length - 1) * 20 - 18) / 2 - 3;

			FrameworkMain.getInstance().getImagePool().waitForAll();
		}
	}

}

package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Vector;
import java.util.Date;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class MultipleIssuePopup extends Popup implements IssueChangedListener {
    private static final int ROW_COUNT = 7;
    private static final int MIDDLE_POS = (ROW_COUNT - 1) / 2;

    private OwnRenderer renderer = new OwnRenderer();
    private String[] contents;

    private String smallTitle;
    private String buttonString;

    private int listFocus;
    private int focus; // focus == -1, focus on list

    ScrollTexts scroll;

    MultipleIssue issue;

    PvrFooter footer = new PvrFooter();

    public MultipleIssuePopup() {
        setRenderer(renderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);

        scroll = new ScrollTexts();
        scroll.setBounds(516, 230, 237, 103);
        scroll.setFont(GraphicsResource.BLENDER17);
        scroll.setForeground(GraphicsResource.C_200_200_200);
        scroll.setRowHeight(18);
        scroll.setRows(5);
        scroll.setInsets(6, 11, 0, 5);
        scroll.setBottomMaskImage(dataCenter.getImage("07_iss_txt_sha.png"));
        scroll.setBottomMaskImagePosition(0, 73);
        scroll.setTopMaskImage(dataCenter.getImage("07_iss_txt_sha_t.png"));
        scroll.setTopMaskImagePosition(0, 0);

        add(scroll);

        Image iconScroll = footer.addButton(PreferenceService.BTN_PAGE, "pvr.Scroll Description");
        footer.linkWithScrollTexts(iconScroll, scroll);
        footer.setBounds(500, 443, 263, 25);
        add(footer);

        listFocus = 0;
        IssueManager.getInstance().addListener(this);
    }

    public void setIssue(MultipleIssue issue) {
        //->Kenneth[2017.3.29] R7.3 : VDTRMASTER-5703 힌트 찾기 위한 로그 print 및 exception catch
        try {
            int issueCount = issue.getIssueCount();

            String[] contents = new String[issueCount];
            for (int i = 0; i < issueCount; i++) {
                contents[i] = issue.getIssue(i).getContent();
            }
            this.issue = issue;
            this.contents = contents;

            if (listFocus >= issueCount) {
                listFocus = issueCount - 1;
            }
            if (issueCount <= 0) {
                focus = 1;
            }
            updateDesc();
        } catch (Exception e) {
            Log.printFDR(e.toString());
        }
        //<-
    }

    public void resetFocus() {
        listFocus = 0;
        if (contents.length > 0) {
            focus = -1;
        } else {
            focus = 1;
        }
        updateDesc();
    }

    public synchronized void start() {
        super.start();
        this.setVisible(true);
    }

    public synchronized void stop() {
        this.setVisible(false);
    }

    /** Issue 변경. */
    public synchronized void issueChanged(Issue old, Issue issue) {
        Log.printInfo("MultipleIssuePopup.issueChanged: " + old + ", " + issue);
        MultipleIssue mi;
        if (issue == null) {
            mi = new MultipleIssue(new Vector(4));
        } else if (issue instanceof MultipleIssue) {
            mi = (MultipleIssue) issue;
        } else {
            Vector vector = new Vector(4);
            vector.addElement(issue);
            mi = new MultipleIssue(vector);
        }
        setIssue(mi);
        repaint();
    }

    public boolean handleKey(int type) {
        switch (type) {
        case KeyEvent.VK_UP:
            if (focus >= 0) { // btn, focus to list
                if (contents.length > 0) {
                    focus = 0;
                    repaint();
                }
            } else { // list
                if (listFocus > 0) {
                    listFocus--;
                    updateDesc();
                }
            }
            break;
        case KeyEvent.VK_DOWN:
            if (focus >= 0) { // btn
                focus = 1;
                repaint();
            } else { // list
                if (listFocus < contents.length - 1) {
                    listFocus++;
                    updateDesc();
                }
            }
            break;
        case KeyEvent.VK_LEFT:
            if (focus >= 0 && contents.length > 0) {
                focus = -1;
                repaint();
            }
            break;
        case KeyEvent.VK_RIGHT:
            if (focus < 0) {
                focus = 0;
                repaint();
            }
            break;
        case KeyEvent.VK_ENTER:
            //->Kenneth[2017.3.29] R7.3 : VDTRMASTER-5703 수정을 위한 FDR 로그 추가, Exception 체크
            try {
                startClickingEffect();
                switch (focus) {
                case 0: // ok
                    Issue is = issue.getIssue(listFocus);
                    if (is instanceof ConflictIssue) {
                        IssueManager.getInstance().showConflictIssue((ConflictIssue) is, this);
                    } else if (is instanceof DiskIssue) {
                        try {
                            String app = Core.getInstance().getCurrentActivatedApplicationName();
                            if (App.NAME.equals(app)) {
                                MenuController.getInstance().showMenu(PvrService.MENU_PVR_MANAGEMENT);
                            } else {
                                Core.getInstance().startUnboundApp(App.NAME, new String[] {App.NAME, String.valueOf(PvrService.MENU_PVR_MANAGEMENT)});
                            }
                        } catch (Exception e) {
                            Log.printWarning(e);
                        }
                        IssueManager.getInstance().stopPopup(this);
                    } else if (is instanceof FailedIssue) {
                        String log = "ENTER in MultipleIssuePopup : reason = "+((FailedIssue)is).getReason()+" : count(before) = "+this.issue.getIssueCount()+" : ";
                        this.issue.remove(is);
                        log = log + "count(after) = "+this.issue.getIssueCount()+" : ";
                        setIssue(this.issue);
                        log = log + "listFocus = "+listFocus+" : focus = "+focus+" : ";
                        repaint();
                        FailedIssue fi = (FailedIssue) is;
                        boolean deleteResult = fi.deleteRequest();
                        log = log + "deleteResult = "+deleteResult+" : rec = "+fi.getRecording()+" : ";
                        boolean isLocal = fi.getRecording() instanceof LocalRecording;
                        log = log + "isLocal = " + isLocal + " : groupKey = "+fi.getRecording().getGroupKey();
                        Log.printDebug(log);
                        Log.printFDR(log);
    //                    IssueManager.getInstance().hideAll();
                    }
                    break;
                case 1: // cancel
                    IssueManager.getInstance().stopPopup(this);
                    break;
                default: // on list
                    focus = 0;
                    break;
                }
            } catch (Exception e) {
                Log.printFDR(e.toString());
            }
            //<-
            repaint();
            break;
        case KeyEvent.VK_PAGE_DOWN:
            footer.clickAnimationByKeyCode(type);
            scroll.showNextPage();
            break;
        case KeyEvent.VK_PAGE_UP:
            footer.clickAnimationByKeyCode(type);
            scroll.showPreviousPage();
            break;
        case OCRcEvent.VK_EXIT:
        	IssueManager.getInstance().stopPopup(this);
            break;
        case KeyCodes.LAST:
            break;
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_B:
            if (Log.EXTRA_ON) {
                Issue is = issue.getIssue(listFocus);
                if (is instanceof FailedIssue) {
                    AbstractRecording rec = ((FailedIssue) is).getRecording();
                    if (rec instanceof LocalRecording) {
                        MetadataDiagScreen.getInstance().startRecording(rec, type == KeyCodes.COLOR_B);
                    }
                } else {
                    MetadataDiagScreen.getInstance().startRoot();
                }
            }
            return false;
        case KeyCodes.PIP_MOVE:
            if (Log.EXTRA_ON) {
                try {
                    int count = issue.getIssueCount();
                    for (int i = count - 1; i >= 0; i--) {
                        Issue is = issue.getIssue(i);
                        if (is instanceof FailedIssue) {
                            FailedIssue fi = (FailedIssue) is;
                            fi.deleteRequest();
                        }
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            return false;
        default:
            return false;
        }
        repaint();
        return true;
    }

    private void updateDesc() {
        Issue is = issue.getIssue(listFocus);
        if (is != null) {
            smallTitle = is.getTitleInPopup();
            scroll.setContents(TextUtil.replace(is.getDescription(), "|", "\n"));
            buttonString = is.getButtonText();
        } else {
            smallTitle = "";
            scroll.setContents(" ");
            buttonString = "";
        }
    }

    public void startClickingEffect() {
        if (focus == 0) {
            clickEffect.start(531 + 1, 351, 210, 32);
        } else if (focus == 1) {
            clickEffect.start(531 + 1, 388, 210, 32);
        }
    }

    private class OwnRenderer extends Renderer {
        Image i_pop_sha;
        Image i_pop_631_bg;
        Image i_07_iss_t;
        Image i_07_iss_m;
        Image i_07_iss_b;
        Image i_07_issues_top;
        Image i_07_issues_bottom;
        Image i_07_iss_foc;
        Image i_07_iss_foc_dim;
        Image i_02_ars_t;
        Image i_02_ars_b;
        Image i_btn_210_foc;
        Image i_btn_210;
        Image i_icon_noti_red;
        String failed;
        String cancelled;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);

            g.drawImage(i_pop_sha, 196, 434, 571, 79, c);
            g.drawImage(i_pop_631_bg, 166, 66, c);

            Image icon = i_icon_noti_red;
            String t = dataCenter.getString("pvr.pvrIssueTitle");
            int w = icon.getWidth(c);
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            int x = GraphicUtil.drawStringCenter(g, t, 483 + w / 2, 123);
            g.drawImage(icon, x - w - 5, 106, c);

            // desc
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            t = issue.getContent();
            GraphicUtil.drawStringCenter(g, t, 474, 171);
            int sy = 216;
            Issue is = issue.getIssue(listFocus);
            if (is != null && is instanceof FailedIssue) {
                g.drawString(((FailedIssue) is).getChannelAndDate(), 527, 216+9);
                sy = 216 + 9 - 18;
            }
            g.drawString(smallTitle, 527, sy);

            g.drawImage(i_07_iss_t, 209, 201, c);
            g.drawImage(i_07_iss_m, 209, 233, c);
            g.drawImage(i_07_iss_m, 209, 264, c);
            g.drawImage(i_07_iss_m, 209, 295, c);
            g.drawImage(i_07_iss_m, 209, 326, c);
            g.drawImage(i_07_iss_m, 209, 357, c);
            g.drawImage(i_07_iss_b, 209, 388, c);

            int listSize = contents.length;
            int over = listSize - ROW_COUNT;
            int move = listFocus - MIDDLE_POS;
            int firstIndex;
            if (over > 0 && move > 0) {
                firstIndex = Math.min(over, move);
            } else {
                firstIndex = 0;
            }

            // list..
            g.setFont(GraphicsResource.BLENDER18);
            int size = Math.min(ROW_COUNT, listSize);
            int index = firstIndex;
            sy = 202;
            for (int i = 0; i < size; i++) {
                if (listFocus == index) {
                    if (focus < 0) {
                        g.drawImage(i_07_iss_foc, 209, sy, c);
                    } else {
                        g.drawImage(i_07_iss_foc_dim, 209, sy, c);
                    }
                    g.setColor(GraphicsResource.C_0_0_0);
                } else {
                    g.setColor(GraphicsResource.C_210_210_210);
                }
                is = issue.getIssue(index);
                if (is instanceof FailedIssue) {
                    FailedIssue fi = (FailedIssue) is;
                    boolean cancel = fi.getReason() == RecordManager.CANCELLED;
                    String reasonStr;
                    int sw;
                    if (cancel) {
                        reasonStr = cancelled;
                        sw = 190;
                    } else {
                        reasonStr = failed;
                        sw = 225;
                    }
                    g.drawString(TextUtil.shorten(contents[index], g.getFontMetrics(), sw), 220, sy + 22);
                    GraphicUtil.drawStringRight(g, reasonStr, 496, sy + 22);
                } else {
                    g.drawString(TextUtil.shorten(contents[index], g.getFontMetrics(), 283), 220, sy + 22);
                }
                index++;
                sy += 31;
            }
            if (Log.EXTRA_ON) {
                is = issue.getIssue(listFocus);
                if (is != null && is instanceof FailedIssue) {
                    g.setColor(Color.green);
                    g.drawString(new Date(((FailedIssue) is).getRecording().getExpirationTime()).toString(), 400, 150);
                    g.drawString(RecordManager.getFailedReasonString(((FailedIssue) is).getReason()), 350, 190);
                }
            }

            // shadow and arrows
            if (firstIndex > 0) {
                g.drawImage(i_07_issues_top, 210, 202, c);
                g.drawImage(i_02_ars_t, 354, 192, c);
            }
            if (firstIndex + ROW_COUNT < listSize) {
                g.drawImage(i_07_issues_bottom, 210, 386, c);
                g.drawImage(i_02_ars_b, 353, 412, c);
            }

            // buttons
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_3_3_3);
            if (contents.length > 0) {
                g.drawImage(focus == 0 ? i_btn_210_foc : i_btn_210, 531, 351, c);
                GraphicUtil.drawStringCenter(g, buttonString, 635, 372);
            }
            g.drawImage(focus == 1 ? i_btn_210_foc : i_btn_210, 531, 388, c);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.Close"), 635, 408);
        }

        public void prepare(UIComponent c) {
            failed = dataCenter.getString("pvr.issue_msg_failed");
            cancelled = dataCenter.getString("pvr.issue_msg_cancelled");
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_pop_631_bg = dataCenter.getImage("pop_631_bg.png");
            i_07_iss_t = dataCenter.getImage("07_iss_t.png");
            i_07_iss_m = dataCenter.getImage("07_iss_m.png");
            i_07_iss_b = dataCenter.getImage("07_iss_b.png");
            i_07_issues_top = dataCenter.getImage("07_issues_top.png");
            i_07_issues_bottom = dataCenter.getImage("07_issues_bottom.png");
            i_07_iss_foc = dataCenter.getImage("07_iss_foc.png");
            i_07_iss_foc_dim = dataCenter.getImage("07_iss_foc_dim.png");
            i_02_ars_t = dataCenter.getImage("02_ars_t.png");
            i_02_ars_b = dataCenter.getImage("02_ars_b.png");
            i_btn_210_foc = dataCenter.getImage("btn_210_foc.png");
            i_btn_210 = dataCenter.getImage("btn_210.png");
            i_icon_noti_red = dataCenter.getImage("icon_noti_red.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

}

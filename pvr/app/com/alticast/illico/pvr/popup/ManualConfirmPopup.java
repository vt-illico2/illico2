package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;

public class ManualConfirmPopup extends Popup {
    private String recordingNameValue = "";
    private String saveKey;
    private String repeatText;

    private String channelName = "cw";

    private String date = "Fri.";
    private String time = "10:00";
    private String duration = "60";
    private String chnnelNumber = "CH";

    private Image chLogo;

    public ManualConfirmPopup(RecordingOption option, String repeatString, String saveKey) {
        if (repeatString != null) {
            this.repeatText = TextUtil.shorten(PvrUtil.getRepeatText(repeatString), GraphicsResource.FM18, 190);
        } else {
            this.repeatText = dataCenter.getString("pvr.No");
        }
        this.saveKey = saveKey;
        this.recordingNameValue = option.title;

        long start = option.startTime;
        long end = option.endTime;

        Formatter fm = Formatter.getCurrent();
        this.date = fm.getLongDayText(start);
        this.time = fm.getTime(start) + " - " + fm.getTime(end);
        this.duration = fm.getDurationText(end - start);
        try {
            this.channelName = option.channel.getCallLetter();
            this.chnnelNumber = String.valueOf(option.channel.getNumber());
        } catch (Exception ex) {
        }

        chLogo = (Image) SharedMemory.getInstance().get("logo." + channelName);
        if (chLogo == null) {
            chLogo = (Image) SharedMemory.getInstance().get("logo.default");
        }

        setRenderer(new PopupRenderer());
        prepare();
        this.setBounds(0, 0, 960, 540);
    }

    public void start() {
        focus = 0;
        super.start();
    }

    public boolean handleKey(int type) {
        switch (type) {
        case KeyEvent.VK_UP:
            if (focus > 0) {
                focus--;
            }
            break;
        case KeyEvent.VK_DOWN:
            if (focus < 1) {
                focus++;
            }
            break;
        case KeyEvent.VK_RIGHT:
            break;
        case KeyEvent.VK_LEFT:
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"ManualConfirmPopup.handlKey(VK_ENTER)");
            if (clickEffect == null) {
                clickEffect = new ClickingEffect(this, 5);
            }
            clickEffect.start(335, 341 + (focus * 37), 293, 32);
            if (listener != null) {
                listener.selected(focus);
            }
            close();
            break;
        case KeyCodes.LAST:
        case OCRcEvent.VK_EXIT:
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        if (focus == 0) {
            clickEffect.start(334+1, 341, 293, 32);
        } else if (focus == 1) {
            clickEffect.start(334+1, 378, 293, 32);
        }
    }

    private class PopupRenderer extends Renderer {
        Image pop566bg = dataCenter.getImage("pop_566_bg.png");

        Image btn293 = dataCenter.getImage("btn_293.png");
        Image btn293f = dataCenter.getImage("btn_293_foc.png");
        Image slash_clar = dataCenter.getImage("slash_clar.png");

        String recordingName = dataCenter.getString("manual.recordingName");
        String saveTime = dataCenter.getString("pvr.Save");
        String repeat = dataCenter.getString("manual.Repeat");

        String modify = dataCenter.getString("pvr.Cancel");
        String record = dataCenter.getString("pvr.Record");

        String popupTitle = dataCenter.getString("pvr.TitleConfirm");

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.translate(0, 21);
            g.drawImage(pop566bg, 197, 59, c);

            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_182_182_182);
            g.drawString(repeat + " :", 285, 232);
            g.drawString(saveTime + " :", 285, 260);
            g.drawString(recordingName + " :", 285, 287);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            g.drawString(repeatText, 474, 234);
            g.drawString(dataCenter.getString(saveKey), 474, 261);

            g.drawString(TextUtil.shorten(recordingNameValue, g.getFontMetrics(), 190), 474, 288);

            if (chLogo != null) {
                g.drawImage(chLogo, 338, 149, c);
            }

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            g.drawString(channelName, 447, 169);
            g.drawString(chnnelNumber, 286, 169);

            g.setColor(GraphicsResource.C_210_210_210);

            g.drawString(date, 285, 194);
            int dateWidth = g.getFontMetrics().stringWidth(date) + 9;
            g.drawImage(slash_clar, 285 + dateWidth , 183, c);
            dateWidth = dateWidth + 10;
            g.drawString(time, 285 + dateWidth, 194);
            dateWidth = dateWidth +  g.getFontMetrics().stringWidth(time) + 9;
            g.drawImage(slash_clar, 285 + dateWidth , 183, c);
            dateWidth = dateWidth + 10;
            g.drawString(duration, 285 + dateWidth, 194);

            g.setFont(GraphicsResource.BLENDER23);
            g.setColor(GraphicsResource.C_255_203_0);
            GraphicUtil.drawStringCenter(g, popupTitle, 480, 116);

            g.translate(0, -21);

            g.drawImage(btn293, 335, 341, c);
            g.drawImage(btn293, 335, 378, c);

            if (focus == 0) {
                g.drawImage(btn293f, 334, 341, c);
            } else if (focus == 1) {
                g.drawImage(btn293f, 334, 378, c);
            }
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_4_4_4);
            GraphicUtil.drawStringCenter(g, record, 481, 362);
            GraphicUtil.drawStringCenter(g, modify, 481, 400);
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

}

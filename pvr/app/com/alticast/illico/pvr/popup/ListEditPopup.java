package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.ui.DVBColor;
import org.ocap.shared.dvr.*;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.comp.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.gui.ScaleUPList;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.epg.RemoteIterator;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;

public class ListEditPopup extends Popup implements PopupContainer, OptionSelectionListener {

    private Popup currentPopup;

    String popupTitle;

    AbstractRecording rec;

    long start;
    long end;

    TimeEditField startField;
    TimeEditField endField;
    RepeatEditField repeatField;
    OptionEditField saveField;

    EditField[] fields;

    String title;
    long programStartTime;
    long programEndTime;

    String groupKey;

    boolean enabled = true;

    static OriginalRecordingInfo original;

    public ListEditPopup(AbstractRecording r) {
        setRenderer(new PopupRenderer());
        setFont(GraphicsResource.BLENDER17);
        setForeground(GraphicsResource.C_182_182_182);
        popupTitle = dataCenter.getString("pvr.modify_recording");

        this.rec = r;
        start = r.getStartTime();
        end = r.getEndTime();

        short stateGroup = r.getStateGroup();
        this.groupKey = r.getGroupKey();

        int programType = r.getInteger(AppData.PROGRAM_TYPE);
        boolean isSingleProgram = AppData.isSingleProgram(programType);

        boolean hour24 = Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage());

        boolean pending = stateGroup == RecordingListManager.PENDING;

        String repeatString = RecordingScheduler.getInstance().getRepeatString(groupKey);
        String seriesId = (String) rec.getAppData(AppData.SERIES_ID);

        startField = new TimeEditField(dataCenter.getString("pvr.Start"), pending, hour24);
        endField = new TimeEditField(dataCenter.getString("pvr.End"), pending, hour24);
        repeatField = new RepeatEditField(dataCenter.getString("pvr.Repeat"), pending && !isSingleProgram,
                                          groupKey, repeatString);
        repeatField.setListener(this);
        saveField = new OptionEditField(dataCenter.getString("pvr.Save"));

        if (!repeatField.isEditable) {
            repeatField.repeatText = r.getRepeatText();
        }

        resetSaveOption(groupKey != null);
        saveField.setInitialValue(r.getSaveText());

        if (r.getAppData(AppData.PROGRAM_ID) != null) {
            // non manual
            programStartTime = r.getLong(AppData.PROGRAM_START_TIME);
            programEndTime = r.getLong(AppData.PROGRAM_END_TIME);
        }
        title = r.getTitle();
        Vector list = new Vector();
        list.add(new StaticEditField(dataCenter.getString("pvr.TITLE"),
                    TextUtil.shorten(title, GraphicsResource.FM18, 320)));
        list.add(new StaticEditField(dataCenter.getString("pvr.Date"),
                    Formatter.getCurrent().getLongDayText(start)));
        list.add(startField);
        list.add(endField);
        list.add(repeatField);
        list.add(saveField);

        fields = new EditField[list.size()];
        list.copyInto(fields);

        startField.setTime(start);
        endField.setTime(end);

        focus = -1;
        for (int i = 0; i < fields.length; i++) {
            if (fields[i].isEditable) {
                focus = i;
                break;
            }
        }

    }

    private void resetSaveOption(boolean repeat) {
        saveField.setOptions(repeat ? PvrUtil.SAVE_SERIES : PvrUtil.SAVE_RECORDING);
        if (repeat) {
            if (groupKey == null) {
                int count = PvrUtil.getDefaultKeepCount();
                if (count == 3) {
                    saveField.setSelectedKey(PvrUtil.LAST_3);
                } else if (count == 5) {
                    saveField.setSelectedKey(PvrUtil.LAST_5);
                } else {
                    saveField.setSelectedKey(PvrUtil.LAST_MAX);
                }
            } else {
                // 원래 값...
                saveField.setCurrentValue(rec.getSaveText());
            }
        } else {
            if (groupKey == null) {
                // 원래 값...
                saveField.setCurrentValue(rec.getSaveText());
            } else {
                long exp = PvrUtil.getDefaultExpirationPeriod();
                if (exp == 7 * Constants.MS_PER_DAY) {
                    saveField.setSelectedKey(PvrUtil.SAVE_7);
                } else if (exp == 14 * Constants.MS_PER_DAY) {
                    saveField.setSelectedKey(PvrUtil.SAVE_14);
                } else {
                    saveField.setSelectedKey(PvrUtil.SAVE_MAX);
                }
            }
        }
    }

    public void showDaysPopup() {
        Log.printInfo("ListEditPopup.showDaysPopup");
        final SelectDaysPopup popup = new SelectDaysPopup(repeatField.getCurrentDays());
        popup.fillBg = true;
        this.add(popup);
        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int focus) {
                repeatField.setDays(popup.getDays());
            }
        };
        popup.setListener(popupListener);
        showPopup(popup);
    }

    public void showPopup(Popup popup) {
        synchronized (this) {
            if (currentPopup != null) {
                hidePopup(currentPopup);
            }
            popup.prepare();
            popup.start(this);
            popup.setVisible(false);
            currentPopup = popup;
            this.add(popup, 0);
            popup.startEffect();
        }
    }

    protected void hidePopup() {
        synchronized (this) {
            hidePopup(currentPopup);
        }
    }

    public void hidePopup(Popup popup) {
        synchronized (this) {
            if (currentPopup == popup) {
                if (popup != null) {
                    popup.stop();
                    this.remove(popup);
                }
                currentPopup = null;
            }
        }
        repaint();
    }

    public boolean handleKey(int code) {
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code);
        }
        if (focus >= 0) {
            boolean ret = fields[focus].handleKey(code);
            if (ret) {
                repaint();
                return true;
            }
        }
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (enabled) {
                    startClickingEffect();
                    if (focus == -2) {
                        close();
                    } else if (focus == -1) {
                        checkAndModify();
                    } else if (focus >= 0 && fields[focus] == repeatField && repeatField.focus == 1) {
                        showDaysPopup();
                    }
                }
                break;
            case OCRcEvent.VK_UP:
                keyUp();
                break;
            case OCRcEvent.VK_DOWN:
                keyDown();
                break;
            case OCRcEvent.VK_LEFT:
                if (focus == -2) {
                    focus = -1;
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (focus == -1) {
                    focus = -2;
                }
                break;
            case OCRcEvent.VK_EXIT:
                close();
                break;
            default:
                return false;
        }
        repaint();
        return true;
    }

    private void keyUp() {
        EditField last = focus >= 0 ? fields[focus] : null;
        int i = (focus < 0) ? fields.length - 1 : focus - 1;
        for (; i >= 0; i--) {
            if (fields[i].isEditable) {
                focus = i;
                if (last != null && last instanceof TimeEditField && fields[i] instanceof TimeEditField) {
                    fields[i].resetFocus(last.focus);
                } else {
                    fields[i].resetFocus(0);
                }
                return;
            }
        }
    }

    private void keyDown() {
        if (focus < 0) {
            return;
        }
        EditField last = fields[focus];
        int i = focus + 1;
        for (; i < fields.length; i++) {
            if (fields[i].isEditable) {
                focus = i;
                if (last != null && last instanceof TimeEditField && fields[i] instanceof TimeEditField) {
                    fields[i].resetFocus(last.focus);
                } else {
                    fields[i].resetFocus(0);
                }
                return;
            }
        }
        focus = -1;
    }

    // OptionSelectionListener
    public void selectionChanged(Object key) {
        Log.printInfo("ListEditPopup.selected = " + key);
        boolean repeat = !RepeatEditField.KEY_NO.equals(key);
        resetSaveOption(repeat);
    }

    private void checkAndModify() {
        enabled = false;
        String repeatValue = (String) repeatField.getChangedValue();
        if (rec instanceof LocalRecording) {
            LocalRecording r = (LocalRecording) rec;
            if (repeatValue != null) {
                boolean repeat = (groupKey != null);
                if (RepeatEditField.KEY_NO.equals(repeatValue)) {
                    // make single
                    if (repeat) {
                        showRemoveRepeatPopup(r);
                        return;
                    }
                } else if (RecordingScheduler.REPEAT_ALL_STRING.equals(repeatValue)) {
                    // make all
                    checkTooMany(r);
                    return;
                }
            }
        }
        processModify();
    }

    private void processModify() {
        original = new OriginalRecordingInfo(groupKey,
                    RecordingScheduler.getInstance().getRepeatString(groupKey));
        AbstractRecording rec = this.rec;
        Date startValue = (Date) startField.getChangedValue();
        Date endValue = (Date) endField.getChangedValue();
        String saveValue = (String) saveField.getChangedValue();
        String repeatValue = (String) repeatField.getChangedValue();

        boolean updated = false;
        // reschedule
        long newStart = startValue != null ? PvrUtil.getAdjustedTimeSlot(startValue.getTime(), start) : start;
        long newEnd = endValue != null ? PvrUtil.getAdjustedTimeSlot(endValue.getTime(), end) : end;

        boolean rescheduled = false;
        if (newStart != start || newEnd != end) {
            if (Log.INFO_ON) {
                Log.printInfo("ListEditPopup.processModify: start = " + new Date(newStart));
                Log.printInfo("ListEditPopup.processModify: end   = " + new Date(newEnd));
            }
            if (newEnd <= newStart) {
                newEnd = newEnd + Constants.MS_PER_DAY;
            } else if (newEnd > newStart + Constants.MS_PER_DAY) {
                newEnd = newEnd - Constants.MS_PER_DAY;
            }
            if (programStartTime != 0) {
                if (newStart >= programEndTime || newEnd <= programStartTime) {
                    showTimeErrorPopup(newStart, newEnd);
                    return;
                }
                if (rec.getLong(AppData.PPV_EID) != 0L) {
                    // case PPV
                    newStart = Math.max(newStart, programStartTime);
                    newEnd = Math.min(newEnd, programEndTime);
                }
            } else {
                rec.setAppData(AppData.PROGRAM_START_TIME, new Long(newStart));
                rec.setAppData(AppData.PROGRAM_END_TIME, new Long(newEnd));
            }

            if (rec instanceof LocalRecording) {
                Log.printInfo("ListEditPopup.processModify: reschedule.");
                RecordingRequest req = ((LocalRecording) rec).reschedule(newStart, newEnd - newStart);
                if (req != null) {
                    rec = new LocalRecording((LeafRecordingRequest) req);
                }
                original.setRescheduleJob(start, end);
            }
            rescheduled = true;
            updated = true;
        }
        if (rec instanceof LocalRecording) {
            original.setRecording((LocalRecording) rec);
        }

        // repeat
        if (Log.DEBUG_ON) {
            Log.printDebug("ListEditPopup.processModify: repeatValue = " + repeatValue);
        }
        if (rec instanceof LocalRecording) {
            if (repeatValue != null) {
                LocalRecording r = (LocalRecording) rec;
                boolean repeat = (groupKey != null);

                if (RepeatEditField.KEY_NO.equals(repeatValue)) {
                    // make single
                    if (repeat) {
                        RecordingScheduler.getInstance().makeSingle(r.getRecordingRequest(), true);
                        original.setRepeatJob(OriginalRecordingInfo.MAKE_REPEAT);
                    }
                } else {
                    if (repeat) {
                        // change
                        RecordingScheduler.getInstance().setRepeatString(groupKey, repeatValue, r.getRecordingRequest());
                        original.setRepeatJob(OriginalRecordingInfo.CHANGE_REPEAT);
                    } else {
                        // make repeat
                        RecordingScheduler.getInstance().makeRepeat(r.getRecordingRequest(), repeatValue, PvrUtil.getDefaultKeepCount(), false, true);
                        original.setRepeatJob(OriginalRecordingInfo.MAKE_SINGLE);
                    }
                }
                updated = true;
            } else if (rescheduled && groupKey != null) {
                RecordingScheduler.getInstance().reset(groupKey, ((LocalRecording) rec).getRecordingRequest());
                original.setRepeatJob(OriginalRecordingInfo.RESET);
                updated = true;
            }
        }

        // save
        if (Log.INFO_ON) {
            Log.printInfo("ListEditPopup.processModify: saveValue = " + saveValue);
        }
        if (saveValue != null) {
            long exp = 0;
            int keepOption = 0;
            if (PvrUtil.SAVE_MAX.equals(saveValue)) {
                exp = Long.MAX_VALUE;
            } else if (PvrUtil.SAVE_14.equals(saveValue)) {
                exp = Math.max(System.currentTimeMillis(), rec.getScheduledStartTime()) + Constants.MS_PER_DAY * 14;
            } else if (PvrUtil.SAVE_7.equals(saveValue)) {
                exp = Math.max(System.currentTimeMillis(), rec.getScheduledStartTime()) + Constants.MS_PER_DAY * 7;
            } else if (PvrUtil.LAST_MAX.equals(saveValue)) {
                keepOption = 0;
            } else if (PvrUtil.LAST_3.equals(saveValue)) {
                keepOption = 3;
            } else if (PvrUtil.LAST_5.equals(saveValue)) {
                keepOption = 5;
            }
            if (exp > 0) {
                rec.setExpirationTime(exp);
            } else {
                RecordingScheduler.getInstance().setKeepCount(rec.getGroupKey(), keepOption);
            }
            updated = true;
        }

        if (listener != null) {
            listener.selected(updated ? 0 : 1);
        }
        close();
    }

    private void showRemoveRepeatPopup(LocalRecording r) {
        ListSelectPopup popup = new ListSelectPopup();
        popup.fillBg = true;
        popup.setPopupTitle(dataCenter.getString("pvr.remove_repeat_title"));
        popup.setDescription(TextUtil.replace(dataCenter.getString("pvr.remove_repeat_msg"), "|", "\n"));

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    Thread t = new Thread("ListEditPopup.remove_repeat") {
                        public void run() {
                            Core.getInstance().showLoading();
                            processModify();
                            Core.getInstance().hideLoading();
                        }
                    };
                    t.start();
                } else {
                    repeatField.resetOption();
                    resetSaveOption(true);
                    enabled = true;
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    private void checkTooMany(final LocalRecording r) {
        Thread t = new Thread("ListEditPopup.checkTooMany") {
            public void run() {
                Core.getInstance().showLoading();
                try {
                    checkTooManyImpl(r);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                Core.getInstance().hideLoading();
            }
        };
        t.start();
    }

    private void checkTooManyImpl(LocalRecording r) throws Exception {
        RemoteIterator it = Core.epgService.requestProgramsByTitle(r.getChannelName(), r.getTitle(), r.getScheduledStartTime());
        if (it == null || it.size() <= RecordingScheduler.MAX_SCHEDULED_PROGRAMS) {
            processModify();
            return;
        }
        ListSelectPopup popup = new ListSelectPopup();
        popup.fillBg = true;
        popup.setPopupTitle(dataCenter.getString("pvr.edit_too_many_title"));
        popup.setDescription(TextUtil.replace(dataCenter.getString("pvr.edit_too_many_msg"), "|", "\n"));
        popup.setButton("pvr.OK", "pvr.edit_too_many_btn1");

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    processModify();
                } else {
                    enabled = true;
                }
            }
        };
        popup.setListener(listener);
        showPopup(popup);
    }

    private void showTimeErrorPopup(long from, long to) {
        Log.printInfo("ListEditPopup.showTimeErrorPopup");
        InfoOneButtonPopup timeErrorPopup = new InfoOneButtonPopup(dataCenter.getString("pvr.time_error_popup_title"));
        timeErrorPopup.fillBg = true;
        Formatter fm = Formatter.getCurrent();
        String s1 = TextUtil.shorten(title, GraphicsResource.FM18, 150);
        String s2 = fm.getTime(programStartTime) + " - " + fm.getTime(programEndTime);
        String s3 = fm.getTime(from) + " - " + fm.getTime(to);
        String desc = TextUtil.replace(dataCenter.getString("pvr.time_error_popup_msg"), "|", "\n");
        desc = TextUtil.replace(desc, "%1", s1);
        desc = TextUtil.replace(desc, "%2", s2);
        desc = TextUtil.replace(desc, "%3", s3);
        timeErrorPopup.setDescription(desc);
        PopupAdapter popupListener = new PopupAdapter() {
            public void selected(int focus) {
                enabled = true;
                startField.setTime(start);
                endField.setTime(end);
            }
        };
        timeErrorPopup.setListener(popupListener);
        showPopup(timeErrorPopup);
    }

    public void startClickingEffect() {
        if (focus == -2) {
            clickEffect.start(484+1, 394, 154, 32);
        } else if (focus == -1) {
            clickEffect.start(320+1, 394, 154, 32);
        }
    }

    class PopupRenderer extends Renderer {
        Color cDim = new DVBColor(33, 33, 33, 204);
        Image bg = dataCenter.getImage("pop_506_bg.png");

        Image pop_focus_dim = dataCenter.getImage("05_focus_dim.png");
        Image pop_focus = dataCenter.getImage("05_focus.png");

        Image input38 = dataCenter.getImage("input_38.png");
        Image input38foc = dataCenter.getImage("input_38_foc.png");
        Image inbox142 = dataCenter.getImage("input_142.png");
        Image inbox142foc = dataCenter.getImage("input_142_foc.png");
        Image input142sc = dataCenter.getImage("input_142_sc.png");
        Image input142scfoc = dataCenter.getImage("input_142_sc_foc.png");

        Image input292 = dataCenter.getImage("input_292.png");
        Image input292focnor = dataCenter.getImage("input_292_foc_nor.png");
        Image input292scFoc = dataCenter.getImage("input_292_sc_foc.png");
        Image input292sc = dataCenter.getImage("input_292_sc.png");

        String save = dataCenter.getString("pvr.OK");
        String cancel = dataCenter.getString("pvr.Cancel");

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paint(Graphics g, UIComponent c) {
            EditField editing = null;
            if (focus >= 0) {
                if (fields[focus].isEditing()) {
                    editing = fields[focus];
                }
            }

            g.drawImage(bg, 197, 59, c);
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 480, 116);

            g.translate(387, 141);
            for (int i = 0; i < fields.length; i++) {
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(fields[i].fieldName, 275-387, 20);
                fields[i].paint(g, focus == i && editing == null, c);
                g.translate(0, 40);
            }
            g.translate(-387, -141 - 40 * fields.length);

            g.drawImage(enabled && focus == -1 ? pop_focus : pop_focus_dim, 320, 394, c);
            g.drawImage(enabled && focus == -2 ? pop_focus : pop_focus_dim, 484, 394, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_4_4_4);
            GraphicUtil.drawStringCenter(g, save, 398, 415);
            GraphicUtil.drawStringCenter(g, cancel, 562, 415);

            if (editing != null) {
                g.setColor(cDim);
                g.fillRect(227, 89, 506, 361);

                g.translate(387, 141 + 40 * focus);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(editing.fieldName, 275-387, 20);

                editing.paint(g, true, c);
                g.translate(-387, -141 - 40 * focus);
            }
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public static boolean restoreIfModified(RecordingRequest r, long sessionStartTime) {
        OriginalRecordingInfo ori = original;
        if (ori == null) {
            Log.printDebug("ListEditPopup.restoreIfModified: not modified");
            return false;
        }
        if (ori.checkRestore(r, sessionStartTime)) {
            ori.processRestore();
            return true;
        }
        return false;
    }

}


class OriginalRecordingInfo {

    static final int MAKE_SINGLE = 1;
    static final int MAKE_REPEAT = 2;
    static final int CHANGE_REPEAT = 3;
    static final int RESET = 4;

    String originalGroupKey;
    String originalRepeatString;
    LocalRecording recording;
    long creationTime = System.currentTimeMillis();

    long[] time;
    int repeatJob = 0;

    public OriginalRecordingInfo(String gk, String rs) {
        this.originalGroupKey = gk;
        this.originalRepeatString = rs;
    }

    public void setRecording(LocalRecording lr) {
        this.recording = lr;
    }

    public void setRescheduleJob(long start, long end) {
        this.time = new long[2];
        time[0] = start;
        time[1] = end;
    }

    public void setRepeatJob(int job) {
        this.repeatJob = job;
    }

    public boolean checkRestore(RecordingRequest r, long sessionStartTime) {
        if (recording == null || r == null) {
            Log.printDebug("ListEditPopup.checkRestore: reject - invalid recording");
            return false;
        }
        if (sessionStartTime - creationTime >= Constants.MS_PER_MINUTE) {
            Log.printDebug("ListEditPopup.checkRestore: reject - too old");
            return false;
        }
        if (r.getId() == recording.getId()) {
            Log.printDebug("ListEditPopup.checkRestore: OK - same id");
            return true;
        }
        String recordingGroupKey = AppData.getString(r, AppData.GROUP_KEY);
        if (recordingGroupKey.equals(originalGroupKey)) {
            Log.printDebug("ListEditPopup.checkRestore: OK - original group key");
            return true;
        }
        if (recordingGroupKey.equals(AppData.get(recording.getRecordingRequest(), AppData.GROUP_KEY))) {
            Log.printDebug("ListEditPopup.checkRestore: OK - new group key");
            return true;
        }
        Log.printDebug("ListEditPopup.checkRestore: reject - mismatch");
        return false;
    }

    public void processRestore() {
        Log.printInfo("ListEditPopup.processRestore");
        LocalRecording rec = recording;
        if (time != null) {
            Log.printInfo("ListEditPopup.processRestore: reschedule.");
            RecordingRequest req = rec.reschedule(time[0], time[1] - time[0]);
            if (req != null) {
                rec = new LocalRecording((LeafRecordingRequest) req);
            }
        }

        switch (repeatJob) {
            case MAKE_SINGLE:
                RecordingScheduler.getInstance().makeSingle(rec.getRecordingRequest(), true);
                break;
            case MAKE_REPEAT:
                RecordingScheduler.getInstance().makeRepeat(rec.getRecordingRequest(), originalRepeatString, PvrUtil.getDefaultKeepCount(), false, true);
                break;
            case CHANGE_REPEAT:
                RecordingScheduler.getInstance().setRepeatString(originalGroupKey, originalRepeatString, rec.getRecordingRequest());
                break;
            case RESET:
                RecordingScheduler.getInstance().reset(originalGroupKey, rec.getRecordingRequest());
                break;

        }

        ListEditPopup.original = null;

    }
}

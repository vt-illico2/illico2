package com.alticast.illico.pvr.popup;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.config.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.gui.BottomPopupRenderer;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;

public class BottomPopup extends Popup implements PopupContainer {

    public String title = "";
    public String[] buttons;
    public String[] texts;

    public String iconName;

    public Popup currentPopup;

    public PvrFooter footer = new PvrFooter();

    public BottomPopup() {
        this(268);
    }

    public BottomPopup(int topX) {
        setRenderer(new BottomPopupRenderer(topX));
        this.setBounds(0, 0, 960, 540);
        footer.setBounds(469, 198 + 290, 434, 25);
        this.add(footer);
        setDefaultAutoClose();
    }

    public void setTextYPosition(int textY) {
        ((BottomPopupRenderer) renderer).setTextYPosition(textY);
    }

    public boolean handleKey(int code) {
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code);
        }
        if (listener != null) {
            boolean ret = listener.handleHotKey(code);
            if (ret) {
                close();
                repaint();
                return true;
            }
            if (listener.consumeKey(code)) {
                return true;
            }
        }
        startTimer();
        switch (code) {
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
            if (focus > 0) {
                focus--;
            }
            repaint();
            break;
        case KeyEvent.VK_RIGHT:
            if (focus < buttons.length - 1) {
                focus++;
            }
            repaint();
            break;
        case KeyEvent.VK_ENTER:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"BottomPopup.handlKey(VK_ENTER)");
            startClickingEffect();
            if (listener != null) {
                listener.selected(focus);
            }
            break;
//        case KeyCodes.LAST:
        case OCRcEvent.VK_EXIT:
            if (listener != null) {
                listener.canceled();
            }
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(((BottomPopupRenderer)renderer).getButtonFocusBounds(this));
    }

    public synchronized void showPinEnabler(String msg, PinEnablerListener l) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"BottomPopup.showPinEnbler("+msg+")");
        DummyPopup dummy = new DummyPopup(l);
        showPopup(dummy);
        PreferenceProxy.getInstance().showPinEnabler(msg, dummy);
    }

    public synchronized void showPopup(Popup p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"BottomPopup.showPopup("+p+")");
        if (currentPopup != null) {
            hidePopup(currentPopup);
        }
        p.setVisible(true);
        p.prepare();
        this.add(p);
        p.start(this);
        currentPopup = p;
        footer.setVisible(false);
        repaint();
    }

    public synchronized void hidePopup(Popup p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"BottomPopup.hidePopup("+p+")");
        Log.printDebug("BottomPopup.hidePopup = " + p);
        try {
            this.remove(p);
            p.stop();
            repaint();
        } catch (Exception ex) {
        }
        if (currentPopup == p) {
            currentPopup = null;
            footer.setVisible(true);
        }
    }

    class DummyPopup extends Popup implements PinEnablerListener {
        PinEnablerListener listener;

        public DummyPopup(PinEnablerListener l) {
            listener = l;
        }

        public void startClickingEffect() {
        }

        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"BottomPopup.DummyPopup.receivePinEnablerResult("+detail+")");
            hidePopup(DummyPopup.this);
            try {
                listener.receivePinEnablerResult(response, detail);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }

    }

}

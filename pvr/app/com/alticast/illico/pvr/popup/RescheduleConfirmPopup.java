package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class RescheduleConfirmPopup extends Popup {

    PopupRenderer popupRenderer = new PopupRenderer();

    String title, msg, btn0, btn1;
    String line1, line2;
    RecordingRequest request;
    TvProgram program;

    DualTunerConflictResolveSession session;

	public RescheduleConfirmPopup(DualTunerConflictResolveSession session, RecordingRequest req, TvProgram p) {
        this.session = session;
        this.request = req;
        this.program = p;
		setRenderer(popupRenderer);
		prepare();
		this.setBounds(0, 0, 960, 540);
		this.setVisible(false);

        Formatter fm = Formatter.getCurrent();

        line1 = AppData.getString(req, AppData.CHANNEL_NUMBER) + "  " + ParentalControl.getTitle(req);
        line1 = TextUtil.shorten(line1, FontResource.getFontMetrics(GraphicsResource.BLENDER18), 300);

        try {
            long s = p.getStartTime();
            long e = p.getEndTime();
            line2 = fm.getLongDayText(s) + "  " + fm.getTime(s) + " - " + fm.getTime(e);
        } catch (Exception e) {
            Log.print(e);
            line2 = " ";
        }

        title = dataCenter.getString("pvr.findOtherTitle");
        msg = dataCenter.getString("pvr.conflictConfirmDesc3");
        btn0 = dataCenter.getString("pvr.OK");
        btn1 = dataCenter.getString("pvr.Cancel");
	}

	public boolean handleKey(int type){
        Log.printInfo("type = " + type);
        switch (type) {
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
        	if (focus == 1) {
                focus = 0;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (focus == 0) {
                focus = 1;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
        	switch (focus) {
        	case 0: // resolve
                processSelect();
        		break;
        	case 1: // cancel
                processCancel();
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_EXIT:
        case KeyCodes.LAST:
            processCancel();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

	public void stop() {
		this.setVisible(false);
	}

    public void startClickingEffect() {
        clickEffect.start(popupRenderer.getButtonFocusBounds(focus));
    }

    private void processSelect() {
        session.pickAnotherShowtime(request, program);
    }

    private void processCancel() {
        session.showConflictPopup();
    }

    public class PopupRenderer extends Renderer {
        DataCenter dataCenter = DataCenter.getInstance();

        Image i_pop_sha;
        Image i_pop_high_402;
        Image i_pop_gap_379;
        Image i_05_pop_glow_02;
        Image i_05_focus_dim;
        Image i_05_focus;

        String[] topMessage = new String[0];
        String[] bottomMessage = new String[0];

        Color cGray = new Color(150, 150, 150);

        public PopupRenderer() {
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);

            g.drawImage(i_pop_sha, 280, 396, 404, 79, c);
            g.drawImage(i_05_pop_glow_02, 250, 94, c);
            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(280, 124, 402, 276);
            g.drawImage(i_pop_high_402, 280, 124, c);
            g.drawImage(i_pop_gap_379, 292, 162, c);

            int w = 0;
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            int x = GraphicUtil.drawStringCenter(g, title, 483 + w / 2, 151);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(Color.white);
            GraphicUtil.drawStringCenter(g, msg, 480, 220);
            GraphicUtil.drawStringCenter(g, line1, 480, 220 + 20 * 2);
            GraphicUtil.drawStringCenter(g, line2, 480, 220 + 20 * 3);

            g.setColor(GraphicsResource.C_3_3_3);
            if (c.getFocus() == 0) {
                g.drawImage(i_05_focus, 321, 353, c);
                g.drawImage(i_05_focus_dim, 485, 353, c);
            } else {
                g.drawImage(i_05_focus_dim, 321, 353, c);
                g.drawImage(i_05_focus, 485, 353, c);
            }
            GraphicUtil.drawStringCenter(g, btn0, 399, 374);
            GraphicUtil.drawStringCenter(g, btn1, 564, 374);
        }

        public void prepare(UIComponent c) {
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_pop_high_402 = dataCenter.getImage("pop_high_402.png");
            i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
            i_05_pop_glow_02 = dataCenter.getImage("05_pop_glow_02.png");
            i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
            i_05_focus = dataCenter.getImage("05_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getButtonFocusBounds(int focus) {
            if (focus == 0) {
                return new Rectangle(321+1, 353, 154, 32);
            } else if (focus == 1) {
                return new Rectangle(485+1, 353, 154, 32);
            } else {
                return null;
            }
        }

    }

}

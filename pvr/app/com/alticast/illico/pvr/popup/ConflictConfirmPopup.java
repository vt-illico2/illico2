package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.rmi.RemoteException;

import javax.tv.locator.Locator;
import javax.tv.service.selection.InvalidServiceComponentException;

import org.ocap.shared.dvr.AccessDeniedException;
import org.ocap.shared.dvr.LocatorRecordingSpec;
import org.ocap.shared.dvr.RecordingProperties;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.RecordingSpec;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;


public class ConflictConfirmPopup extends ConflictPopup {
    OwnRenderer renderer = new OwnRenderer();

    private int[] listOffset = new int[2];
    private int[] listFocus = new int[2];
    private int btnFocus; // btnFocus == -1, focus on list
    private int caller;

    private ArrayList selected;
    private ArrayList repeated;
    private HashMap other;

    public ConflictConfirmPopup(DualTunerConflictResolveSession session) {
        super(session);
        setRenderer(renderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    // set collection of will be recorded RecordingRequest
    // items in collection are should be item of array of setConflictRecording()
    public void setWillBeRecordedRecording(ArrayList list) {
    	selected = list;
    }

    public void setRepeatedRecording(ArrayList list) {
    	repeated = list;
    }

    public void setOtherShowtime(HashMap other) {
    	this.other = other;
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.start()");
//    	if (Environment.EMULATOR) {
//    		setTargetRecording(dTarget);
//    		setConflictRecording(dConflicts);
//    		ArrayList a = new ArrayList();
//    		a.add(dTarget);
//    		a.add(dConflicts[0]);
//    		a.add(dConflicts[1]);
//    		setWillBeRecordedRecording(a);
//    	}
    	listOffset[0] = listFocus[0] = 0;
    	listOffset[1] = listFocus[1] = 0;
    	btnFocus = 0;

        super.start();
        this.setVisible(true);
    }

    public void start(int num) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.start("+num+")");
    	listOffset[0] = listFocus[0] = 0;
    	listOffset[1] = listFocus[1] = 0;
    	btnFocus = 0;
        super.start();
        this.setVisible(true);
        caller = num;
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.stop()");
        super.stop();
        this.setVisible(false);
    }

    private void keyEnter() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.keyEnter()");
    	if (btnFocus < 0) { // list focused
    		return;
    	}
        startClickingEffect();
    	switch (btnFocus) {
    	case 0: // confirm
            if (ParentalControl.isProtected()) {
                PinEnablerListener l = new PinEnablerListener() {
                    public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                        if (PreferenceService.RESPONSE_SUCCESS == response) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.keyEnter().PinEneblerListener.receivePinEnablerResult()");
                            confirm();
                        }
                    }
                };
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.keyEnter() : Call PreferenceProxy.showPinEnabler()");
                PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.stop"), l);
            } else {
                confirm();
            }
    		break;
    	case 1: // cancel
            close();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.keyEnter() : Call session.showConflictPopup()");
    		session.showConflictPopup();
    		break;
    	}
    }

    private void confirm() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.confirm()");
        close();
        ArrayList progress = new ArrayList();
        for (int i = 0; i < conflicts.length; i++) {
            if (!selected.contains(conflicts[i])) {
                if (other == null || !other.containsKey(conflicts[i]) ) {
                    if (RecordingListManager.inProgressValidFilter.accept(conflicts[i])) { // in progress
                        progress.add(conflicts[i]);
                    }
                }
            }
        }
        if (progress.size() > 0) {
            StopRecordingPopupHandler handler = new StopRecordingPopupHandler(progress, this);
            handler.start();
        } else {
            resolve(null);
        }
    }

    protected void resolve(ArrayList toStop) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.resolve()");
        session.resolve(conflicts, selected, repeated, toStop);
    }

    public boolean handleKey(int type){
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictConfirmPopup.handleKey("+type+")");
        switch (type) {
        case KeyEvent.VK_UP:
        	int len = btnFocus >= 0 ? conflicts.length - selected.size() : selected.size();
        	if (btnFocus >= 0) { // btn
        		btnFocus = -1;
    			listFocus[1] = Math.min(1, len-1);
    			listOffset[1] = Math.max(0, len-2);
        	} else { // list
        		if (moveFocus(true) && btnFocus == -1 && selected.size() > 0) {
        			btnFocus = -2;
        			listFocus[0] = Math.min(2, len-1);
        			listOffset[0] = Math.max(0, len-3);
        		}
        	}
            break;
        case KeyEvent.VK_DOWN:
        	if (btnFocus >= 0) { // btn

        	} else { // list
        		if (moveFocus(false)) {
        			btnFocus++;
        		}
        	}
            break;
        case KeyEvent.VK_LEFT:
        	if (btnFocus >= 0) { // btn
        		btnFocus = 0;
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (btnFocus >= 0) { // btn
        		btnFocus = 1;
        	}
        	break;
        case KeyEvent.VK_ENTER:
            keyEnter();
            break;
        case OCRcEvent.VK_LAST:
        case KeyEvent.VK_PAGE_UP:
        case KeyEvent.VK_PAGE_DOWN:
            return true;
        case OCRcEvent.VK_EXIT:
        	session.showConflictPopup();
            break;
        default:
            return false;
        }

        repaint();
        return true;
    }

    private boolean moveFocus(boolean up) {
    	int idx = btnFocus == -1 ? 1 : 0;
    	int len = btnFocus == -1 ? conflicts.length - selected.size() : selected.size();
    	int max = btnFocus == -1 ? 2 : 3;
    	if (canMoveFocus(up)) {
    		if (up) {
    			int old = listFocus[idx];
    			listFocus[idx] = Math.max(0, listFocus[idx] - 1);
    			return (old == listFocus[idx]); // no move, focus to upper list
    		} else { // down
    			int old = listFocus[idx];
    			listFocus[idx] = Math.min(Math.min(max - 1, len - 1), listFocus[idx] + 1);
    			return (old == listFocus[idx]); // no move, focus to button
    		}
    	} else {
    		listOffset[idx] += up ? -1 : 1;
    		return false;
    	}
    }

    private boolean canMoveFocus(boolean up) {
    	int idx = btnFocus == -1 ? 1 : 0;
    	int len = btnFocus == -1 ? conflicts.length - selected.size() : selected.size();
    	int max = btnFocus == -1 ? 2 : 3;
    	if (len < max) return true; // less than 1 page

    	if (up) {
    		return listFocus[idx] > 1 || listOffset[idx] == 0;
    	} else { // down
    		return listFocus[idx] < 1 || listOffset[idx] >= len - max;
    	}
    }

    private RecordingRequest getRR(int z, int idx) { // get one of selected or conflict except selected
    	if (z == 0) {
    		return (RecordingRequest) selected.toArray()[idx];
    	}
    	for (int i = 0; i < conflicts.length; i++) {
    		if (selected.contains(conflicts[i])) {
    			idx++;
    		}
    		if (i == idx) {
    			return conflicts[i];
    		}
    	}
    	return null;
    }

    private int find(RecordingRequest rr) { // find on conflicts[]
    	for (int i = 0; i < conflicts.length; i++) {
    		if (conflicts[i].equals(rr)) {
    			return i;
    		}
    	}
    	return -1;
    }

    public void startClickingEffect() {
        if (btnFocus == 0) {
            clickEffect.start(317+1, 415, 154, 32);
        } else if (btnFocus == 1) {
            clickEffect.start(481+1, 415, 154, 32);
        }
    }

    private class OwnRenderer extends CommonRenderer {
    	Image i_05_focus;
        Image i_05_focus_dim;
        Image check, check_box, check_box_foc;

        protected void paint(Graphics g, UIComponent c) {
            //->Kenneth[2015.5.24] R5 : 요구사항이 2 tuner 인 경우만 R5 UI 를 적용하라는 것이어서 
            // 그리는 코드를 중복되는 부분이 있어도 아예 두개로 분할함. MANY_TUNER 인 경우는 기존대로 그려짐
            if (App.MANY_TUNER) {
                //////////////////////////////////////////////////////////////
                // 여기는 이전 그리는 코드 그대로임
                //////////////////////////////////////////////////////////////
                int x, w;
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);

                g.drawImage(i_07_respop_glow, 99, 47, c);
                g.setColor(GraphicsResource.C_33_33_33);
                g.fillRect(204, 74, 550, 387);
                g.drawImage(i_07_respop_sh, 202, 458, c);
                g.drawImage(i_07_respop_title, 203, 112, c);
                g.drawImage(i_07_respop_high, 204, 74, c);

                // title
                g.setFont(GraphicsResource.BLENDER24);
                g.setColor(GraphicsResource.C_255_203_0);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmTitle"), 483, 101);

                // desc
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(GraphicsResource.C_255_255_255);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc1"), 480, 146);
                if (other == null) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc2"), 480, 301);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc3"), 480, 301);
                }

                // upper - list bg
                g.drawImage(i_07_res_pgbg_t, 221, 166, c);
                g.drawImage(i_07_res_pgbg_m, 221, 176, 516, 86, c);
                g.drawImage(i_07_res_pgbg_b, 221, 262, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(222, 202, 514, 34);
                int len = selected.size();
                for (int i = 0; i < 3 && i < len; i++) {
                    boolean foc = btnFocus == -2 && listFocus[0] == i;
                    if (foc) {
                        g.drawImage(i_07_res_foc, 219, 166 + listFocus[0] * 34, c);
                        g.setColor(GraphicsResource.C_0_0_0);
                        g.drawImage(i_07_res_line_foc, 474, 178 + listFocus[0] * 34, c);
                    } else {
                        g.drawImage(i_07_res_line, 474, 178 + i * 34, c);
                    }
                    int idx = find(getRR(0, i+listOffset[0]));
                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[idx], AppData.CHANNEL_NUMBER);
                    g.drawString(t, 233, 191 + i * 34);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[idx]), g.getFontMetrics(), 195);
                    g.drawString(t, 275, 191 + i * 34);

                    g.setFont(GraphicsResource.BLENDER15);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_240_240_240);
                    }
                    GraphicUtil.drawStringRight(g, timeStr[idx], graphX[idx][1], 183 + i * 34, 486, g.getFontMetrics());

                    g.setColor(GraphicsResource.C_37_37_37); // shadow
                    g.fillRect(graphX[idx][0]+1, 188 + i * 34, graphX[idx][2], 9);
                    g.drawImage(i_07_bar_rec04, graphX[idx][0], 187 + i * 34, graphX[idx][2], 9, c);
                }

                // upper - list shadow, arrow
                if (listOffset[0] > 0) {
                    g.drawImage(i_07_res_pgsh_t, 222, 167, c);
                    g.drawImage(i_02_ars_t, 464, 158, c);
                }
                if (listOffset[0] + 2 < len) {
                    g.drawImage(i_07_res_pgsh_b, 222, 236, c);
                    g.drawImage(i_02_ars_b, 464, 262, c);
                }

                // lower - list bg
                g.drawImage(i_07_res_pgbg_t, 221, 320, c);
                g.drawImage(i_07_res_pgbg_m, 221, 330, 516, 53, c);
                g.drawImage(i_07_res_pgbg_b, 221, 383, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(222, 357, 514, 34);
                len = conflicts.length - selected.size();
                g.setFont(GraphicsResource.BLENDER18);
                for (int i = 0; i < 2 && i < len; i++) {
                    boolean foc = btnFocus == -1 && listFocus[1] == i;
                    if (foc) {
                        g.drawImage(i_07_res_foc, 219, 320 + listFocus[1] * 34, c);
                        g.setColor(GraphicsResource.C_0_0_0);
                        g.drawImage(i_07_res_line_foc, 474, 332 + listFocus[1] * 34, c);
                    } else {
                        g.drawImage(i_07_res_line, 474, 332 + i * 34, c);
                    }
                    int idx = find(getRR(1, i+listOffset[1]));
                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[idx], AppData.CHANNEL_NUMBER);
                    g.drawString(t, 233, 345 + i * 34);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[idx]), g.getFontMetrics(), 195);
                    g.drawString(t, 275, 345 + i * 34);

                    if (other != null && other.containsKey(conflicts[idx])) {
                        TvProgram otherProgram = (TvProgram) other.get(conflicts[idx]);
                        try {
                            long s = otherProgram.getStartTime();
                            long e = otherProgram.getEndTime();
                            t = Formatter.getCurrent().getLongDayText(s);
                            g.drawString(t, 485, 345 + i * 34);
                            t = Formatter.getCurrent().getTime(s) + " - " +
                                Formatter.getCurrent().getTime(e);
                            g.drawString(t, 589, 345 + i * 34);
                        } catch (Exception e) {
                            Log.printWarning(e);
                        }
                    } else {
                        g.setFont(GraphicsResource.BLENDER15);
                        if (!foc) {
                            g.setColor(GraphicsResource.C_240_240_240);
                        }
                        GraphicUtil.drawStringRight(g, timeStr[idx], graphX[idx][1], 337 + i * 34, 486, g.getFontMetrics());
                        g.setColor(GraphicsResource.C_37_37_37); // shadow
                        g.fillRect(graphX[idx][0]+1, 342 + i * 34, graphX[idx][2], 9);
                        //g.drawImage(i_07_bar_rec05, graphX[idx][0], 341 + i * 34, graphX[idx][2], 9, c);

                        Rectangle ori = g.getClipBounds();
                        x = graphX[idx][0];
                        w = i_07_bar_rec05.getWidth(c);
                        g.clipRect(x, 341 + i * 34, graphX[idx][2], 9);
                        g.drawImage(i_07_bar_rec05, x, 341 + i * 34, c);
                        if (graphX[idx][2] > w) {
                            x += (w - 1);
                            g.drawImage(i_07_bar_rec05, x, 341 + i * 34, c);
                        }
                        if (graphX[idx][2] > w + w - 1) {
                            x += (w - 1);
                            g.drawImage(i_07_bar_rec05, x, 341 + i * 34, c);
                        }
                        g.setClip(ori);
                    }
                }
                if (listOffset[1] > 0) {
                    g.drawImage(i_07_res_pgsh_t, 222, 321, c);
                    g.drawImage(i_02_ars_t, 464, 312, c);
                }
                if (listOffset[1] + 2 < len) {
                    g.drawImage(i_07_res_pgsh_b, 222, 357, c);
                    g.drawImage(i_02_ars_b, 464, 383, c);
                }
                // buttons
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(GraphicsResource.C_3_3_3);
                g.drawImage(btnFocus == 0 ? i_05_focus : i_05_focus_dim, 317, 415, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.OK"), 395, 436);
                g.drawImage(btnFocus == 1 ? i_05_focus : i_05_focus_dim, 481, 415, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.Cancel"), 560, 436);
            } else {
                //////////////////////////////////////////////////////////////
                // R5 에서 새롭게 수정된 코드
                //////////////////////////////////////////////////////////////
                int x, w;
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);

                g.drawImage(i_07_respop_glow, 99, 47, c);
                g.setColor(GraphicsResource.C_33_33_33);
                g.fillRect(144, 74, 670, 387);
                //g.fillRect(204, 74, 550, 387);
                g.drawImage(i_07_respop_sh, 202, 458, c);
                g.drawImage(i_07_respop_title, 203, 112, c);
                g.drawImage(i_07_respop_high, 204, 74, c);

                // title
                g.setFont(GraphicsResource.BLENDER24);
                g.setColor(GraphicsResource.C_255_203_0);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmTitle"), 483, 101);

                // desc
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(GraphicsResource.C_255_255_255);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc1"), 480, 146);
                if (other == null) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc2"), 480, 301);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictConfirmDesc3"), 480, 301);
                }

                int xShift = 60;
                // upper - list bg
                g.drawImage(i_07_res_pgbg_t_3, 161, 166, c);
                g.drawImage(i_07_res_pgbg_m_3, 161, 176, 636, 86, c);
                g.drawImage(i_07_res_pgbg_b_3, 161, 262, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(162, 202, 634, 34);
                int len = selected.size();
                // 리스트
                for (int i = 0; i < 3 && i < len; i++) {
                    boolean foc = btnFocus == -2 && listFocus[0] == i;
                    if (foc) {
                        // 노란 포커스 바
                        g.drawImage(i_07_res_foc_3, 162, 166 + i * 34, c);
                        //g.drawImage(i_07_res_foc, 219, 166 + listFocus[0] * 34, c);
                        // 가운데 구분 바
                        g.drawImage(i_07_res_line_foc, 474+xShift, 178 + listFocus[0] * 34, c);
                        g.setColor(GraphicsResource.C_0_0_0);
                    } else {
                        // 가운데 구분 바
                        g.drawImage(i_07_res_line, 474+xShift, 178 + i * 34, c);
                    }
                    // 채널 넘버
                    int idx = find(getRR(0, i+listOffset[0]));
                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[idx], AppData.CHANNEL_NUMBER);
                    g.drawString(t, 233-xShift, 191 + i * 34);

                    // 채널 로고 (R5 에서 새롭게 추가) : 매번 object 를 생성하는 게 그렇지만 귀찮아서 그냥 씀.
                    ConflictRenderingItem cri = new ConflictRenderingItem(conflicts[idx]);
                    if (cri.logo != null) {
                        g.drawImage(cri.logo, 215, 170+i*34, c);
                    }

                    // 프로그램 타이틀
                    if (!foc) {
                        g.setColor(GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[idx]), g.getFontMetrics(), 195);
                    g.drawString(t, 275+xShift, 191 + i * 34);

                    // 시간
                    g.setFont(GraphicsResource.BLENDER15);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_240_240_240);
                    }
                    GraphicUtil.drawStringRight(g, timeStr[idx], graphX[idx][1], 183 + i * 34, 486+xShift, g.getFontMetrics());

                    // 프로그레스 바
                    g.setColor(GraphicsResource.C_37_37_37); // shadow
                    g.fillRect(graphX[idx][0]+1+xShift, 188 + i * 34, graphX[idx][2], 9);
                    g.drawImage(i_07_bar_rec04, graphX[idx][0]+xShift, 187 + i * 34, graphX[idx][2], 9, c);
                }

                // upper - list shadow, arrow
                if (listOffset[0] > 0) {
                    g.drawImage(i_07_res_pgsh_t, 222, 167, c);
                    g.drawImage(i_02_ars_t, 464, 158, c);
                }
                if (listOffset[0] + 2 < len) {
                    g.drawImage(i_07_res_pgsh_b, 222, 236, c);
                    g.drawImage(i_02_ars_b, 464, 262, c);
                }

                // lower - list bg
                g.drawImage(i_07_res_pgbg_t_3, 161, 320, c);
                g.drawImage(i_07_res_pgbg_m_3, 161, 330, 636, 53, c);
                g.drawImage(i_07_res_pgbg_b_3, 161, 383, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(162, 357, 634, 34);
                len = conflicts.length - selected.size();
                g.setFont(GraphicsResource.BLENDER18);
                // 리스트
                for (int i = 0; i < 2 && i < len; i++) {
                    boolean foc = btnFocus == -1 && listFocus[1] == i;
                    if (foc) {
                        // 노란 포커스 바
                        g.drawImage(i_07_res_foc_3, 162, 320 + i * 34, c);
                        //g.drawImage(i_07_res_foc, 219, 320 + listFocus[1] * 34, c);
                        // 가운데 구분 바
                        g.drawImage(i_07_res_line_foc, 474+xShift, 332 + listFocus[1] * 34, c);
                        g.setColor(GraphicsResource.C_0_0_0);
                    } else {
                        // 가운데 구분 바
                        g.drawImage(i_07_res_line, 474+xShift, 332 + i * 34, c);
                    }

                    // 채널 넘버
                    int idx = find(getRR(1, i+listOffset[1]));
                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[idx], AppData.CHANNEL_NUMBER);
                    g.drawString(t, 233-xShift, 345 + i * 34);

                    // 채널 로고 (R5 에서 새롭게 추가) : 매번 object 를 생성하는 게 그렇지만 귀찮아서 그냥 씀.
                    ConflictRenderingItem cri = new ConflictRenderingItem(conflicts[idx]);
                    if (cri.logo != null) {
                        g.drawImage(cri.logo, 215, 170+i*34+154, c);
                    }

                    // 프로그램 타이틀
                    if (!foc) {
                        g.setColor(GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[idx]), g.getFontMetrics(), 195);
                    g.drawString(t, 275+xShift, 345 + i * 34);

                    if (other != null && other.containsKey(conflicts[idx])) {
                        // 시간
                        TvProgram otherProgram = (TvProgram) other.get(conflicts[idx]);
                        try {
                            long s = otherProgram.getStartTime();
                            long e = otherProgram.getEndTime();
                            t = Formatter.getCurrent().getLongDayText(s);
                            g.drawString(t, 485+xShift, 345 + i * 34);
                            t = Formatter.getCurrent().getTime(s) + " - " +
                                Formatter.getCurrent().getTime(e);
                            g.drawString(t, 589+xShift, 345 + i * 34);
                        } catch (Exception e) {
                            Log.printWarning(e);
                        }
                    } else {
                        // 시간
                        g.setFont(GraphicsResource.BLENDER15);
                        if (!foc) {
                            g.setColor(GraphicsResource.C_240_240_240);
                        }
                        GraphicUtil.drawStringRight(g, timeStr[idx], graphX[idx][1], 337 + i * 34, 486+xShift, g.getFontMetrics());
                        // 프로그레스 바
                        g.setColor(GraphicsResource.C_37_37_37); // shadow
                        g.fillRect(graphX[idx][0]+1+xShift, 342 + i * 34, graphX[idx][2], 9);

                        Rectangle ori = g.getClipBounds();
                        x = graphX[idx][0];
                        w = i_07_bar_rec05.getWidth(c);
                        g.clipRect(x+xShift, 341 + i * 34, graphX[idx][2], 9);
                        g.drawImage(i_07_bar_rec05, x, 341 + i * 34, c);
                        if (graphX[idx][2] > w) {
                            x += (w - 1);
                            g.drawImage(i_07_bar_rec05, x+xShift, 341 + i * 34, c);
                        }
                        if (graphX[idx][2] > w + w - 1) {
                            x += (w - 1);
                            g.drawImage(i_07_bar_rec05, x+xShift, 341 + i * 34, c);
                        }
                        g.setClip(ori);
                    }
                }
                if (listOffset[1] > 0) {
                    g.drawImage(i_07_res_pgsh_t, 222, 321, c);
                    g.drawImage(i_02_ars_t, 464, 312, c);
                }
                if (listOffset[1] + 2 < len) {
                    g.drawImage(i_07_res_pgsh_b, 222, 357, c);
                    g.drawImage(i_02_ars_b, 464, 383, c);
                }
                // buttons
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(GraphicsResource.C_3_3_3);
                g.drawImage(btnFocus == 0 ? i_05_focus : i_05_focus_dim, 317, 415, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.OK"), 395, 436);
                g.drawImage(btnFocus == 1 ? i_05_focus : i_05_focus_dim, 481, 415, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.Cancel"), 560, 436);
            }
        }

        public void prepare(UIComponent c) {
        	super.prepare(c);
        	i_05_focus = DataCenter.getInstance().getImage("05_focus.png");
            i_05_focus_dim = DataCenter.getInstance().getImage("05_focus_dim.png");
            check = DataCenter.getInstance().getImage("check.png");
            check_box = DataCenter.getInstance().getImage("check_box.png");
            check_box_foc = DataCenter.getInstance().getImage("check_box_foc.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

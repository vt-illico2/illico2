package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;
import java.awt.EventQueue;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.*;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class StopRecordingPopupHandler extends PopupAdapter {
    DataCenter dataCenter = DataCenter.getInstance();

    ConflictConfirmPopup confirmPopup;
    InfoThreeButtonPopup popup;
    int currentIndex = 0;

    ArrayList list;
    ArrayList toStop = new ArrayList();

    DualTunerConflictResolveSession session;

	public StopRecordingPopupHandler(ArrayList progress, ConflictConfirmPopup confirm) {
        this.confirmPopup = confirm;
        session = confirm.session;
        this.list = progress;
	}

	public void start() {
        start(0);
	}

    private void start(int index) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.start("+index+")");
        LeafRecordingRequest request = (LeafRecordingRequest) list.get(index);
        InfoThreeButtonPopup p = new InfoThreeButtonPopup(dataCenter.getString("pvr.stop_recording"), "pvr.Cancel");
        p.fillBg = true;
        p.setButtonMessage(dataCenter.getString("pvr.issue_stop_popup_btn_0"),
                           dataCenter.getString("pvr.issue_stop_popup_btn_1"),
                           dataCenter.getString("pvr.issue_stop_popup_btn_2"));
        p.setListener(this);
        p.setVisible(false);

        String msg = "'" + ParentalControl.getTitle(request) + "' "
                    + TextUtil.replace(dataCenter.getString("pvr.issue_stop_popup_msg"), "|", "\n");
        p.setProgramTitle(msg);
        p.start(currentIndex);
        session.startPopup(p);
    }

    private void resolve() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.resolve()");
        confirmPopup.resolve(toStop);
    }

    private void cancel() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.cancel()");
        session.startPopup(confirmPopup);
    }

    /*
    public boolean handleHotKey(int code) {
        return false;
    }

    public boolean consumeKey(int code) {
        return false;
    }
    */

    public void selected(int focus) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.selected("+focus+")");
        if (focus == 0) {
            toStop.add(list.get(currentIndex));
        } else if (focus == 1) {
            Log.printDebug("StopRecordingPopupHandler: stop & delete");
            // delete
            LeafRecordingRequest req = (LeafRecordingRequest) list.get(currentIndex);
            if (req != null && RecordManager.isInUseLocalOrRemote(req)) {
                Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.selected() : Call CannotDeleteHandler.start()");
                CannotDeleteHandler cdh = new CannotDeleteHandler(req);
                cdh.start();
                return;
            }
        } else {
            cancel();
            return;
        }
        currentIndex++;
        if (currentIndex < list.size()) {
            start(currentIndex);
        } else {
            resolve();
        }
    }

    class CannotDeleteHandler extends PopupAdapter implements Runnable {
        LeafRecordingRequest request;

        public CannotDeleteHandler(LeafRecordingRequest req) {
            request = req;
        }

        public void selected(int focus) {
            Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.CannotDeleteHandler.selected() : Call StopRecordingPopupHandler.start()");
            StopRecordingPopupHandler.this.start(currentIndex);
        }

        public void run() {
            Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.CannotDeleteHandler.run() : Call PopupController.showCannotDeletePopup()");
            PopupController.showCannotDeletePopup(request, PopupController.getInstance(), CannotDeleteHandler.this);
        }

        public void start() {
            Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.CannotDeleteHandler.start() : before EventQueue.invokeLater(this)");
            EventQueue.invokeLater(this);
            Log.printDebug(App.LOG_HEADER+"StopRecordingPopupHandler.CannotDeleteHandler.start() : after EventQueue.invokeLater(this)");
        }
    }

}

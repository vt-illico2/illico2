package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import org.ocap.shared.dvr.RecordingRequest;

import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.issue.DualTunerConflictResolveSession;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Formatter;

// class for popup about conflict - "conflict resolution", "pick show to record",
// "see suggested option", "confirm"
// it has an array of RecordingRequest, and controls the data of graph

public abstract class ConflictPopup extends Popup {
	protected DataCenter dataCenter = DataCenter.getInstance();
    protected RecordingRequest target;
    protected RecordingRequest[] conflicts = new RecordingRequest[0];
    protected int[][] graphX;
    protected String[] timeStr;
    protected long earliest, latest;
    protected int gap; // for graph, latest time - earliest time

    Image i_07_respop_glow;
    Image i_07_respop_high;
    Image i_07_respop_title;
    Image i_07_respop_sh;
    Image i_07_respop_icon01;
    Image i_07_respop_icon02;
    Image i_07_res_pgbg_t;
    Image i_07_res_pgbg_m;
    Image i_07_res_pgbg_b;
    Image i_07_res_line;
    Image i_07_res_line_foc;
    Image i_07_res_foc;
    Image i_07_res_pgsh_t;
    Image i_07_res_pgsh_b;
    Image i_07_bar_rec01; // red
    Image i_07_bar_rec02; // gray
    Image i_07_bar_rec03; // red hatch
    Image i_07_bar_rec04; // green
    Image i_07_bar_rec05; // gray hatch
    Image i_02_ars_t;
    Image i_02_ars_b;
    Image i_icon_noti_red;
    Image i_icon_g_check;
    Image i_btn_293;
    Image i_btn_293_foc;
    Image i_btn_293_in;
    Image i_12_ppop_sha;
    Image i_pop_sha;
    Image i_scrbg_conf;
    Image i_scr_bar;

    //->Kenneth[2015.5.24] R5
    Image i_07_res_pgbg_t_3;
    Image i_07_res_pgbg_m_3;
    Image i_07_res_pgbg_b_3;
    Image i_07_res_foc_3;

//    protected static DummyRecordingRequest dTarget;
//    protected static DummyRecordingRequest[] dConflicts;
//    static {
//    	if (Environment.EMULATOR) {
//    		dTarget = new DummyRecordingRequest("CH 101", "Program title A", "18:30", "19:30");
//    		dConflicts = new DummyRecordingRequest[] {
//	    			new DummyRecordingRequest("CH 201", "Program title 02", "18:00", "19:00"),
//	    			new DummyRecordingRequest("CH 500", "Program title 03", "19:10", "20:10"),
//	    			new DummyRecordingRequest("CH 501", "Program title 04 - long long long name", "19:00", "20:00"),
//	    			new DummyRecordingRequest("CH 505", "Program title 05", "18:00", "19:00"),
////	    			new DummyRecordingRequest("CH 600", "Program title 06", "18:20", "19:20"),
////	    			new DummyRecordingRequest("CH 700", "Program title 07", "17:50", "20:50"),
////	    			new DummyRecordingRequest("CH 800", "Program title 08", "18:15", "20:30"),
////	    			new DummyRecordingRequest("CH 900", "Program title 09", "18:50", "19:50"),
//	    	};
//    	}
//    }

    DualTunerConflictResolveSession session;

    public ConflictPopup(DualTunerConflictResolveSession session) {
        this.session = session;
    }

    public RecordingRequest[] getConflicts() {
    	return conflicts;
    }

    // set RecordingRequest to record
    public void setTargetRecording(RecordingRequest recording) {
    	target = recording;
    }

    // set conflictedRecordingRequest[], call this after setTargetRecording()
    public void setConflictRecording(RecordingRequest[] recordings) {
    	ArrayList list = new ArrayList();
    	if (target != null) {
    		try {
    			target.getState();
    			list.add(target);
    		} catch (IllegalStateException ise) {}
    	}
    	for (int i = 0; recordings != null && i < recordings.length; i++) {
    		try {
    			recordings[i].getState();
    			list.add(recordings[i]);
    		} catch (IllegalStateException ise) {}
    	}
    	conflicts = new RecordingRequest[list.size()];
    	list.toArray(conflicts);
//    	if (target != null) {
//    		conflicts = new RecordingRequest[recordings.length+1];
//        	conflicts[0] = target;
//        	System.arraycopy(recordings, 0, conflicts, 1, recordings.length);
//    	} else {
//    		conflicts = recordings;
//    	}

    	long min = Long.MAX_VALUE;
    	long max = Long.MIN_VALUE;
    	for (int i = 0; i < conflicts.length; i++) {
    		long start = AppData.getScheduledStartTime(conflicts[i]);
    		long end = start + AppData.getScheduledDuration(conflicts[i]);
    		if (min > start) {
    			min = start;
    		}
    		if (max < end) {
    			max = end;
    		}
    	}
    	gap = (int) (max - min);
    	earliest = min;
    	latest = max;
    	if (Log.DEBUG_ON) {
    		Log.printDebug("called setConflictRecording(), len = " + recordings.length
    				+ ", min = " + new Date(min)
    				+ ", max = " + new Date(max)
    				+ ", gap = " + gap);
    	}
    	graphX = new int[conflicts.length][6]; // start time, end time, width, conflict start, conflict end, conflict width
    	timeStr = new String[conflicts.length];
    	long s, e;
    	for (int i = 0; i < graphX.length; i++) {
    		s = AppData.getScheduledStartTime(conflicts[i]);
    		e = s + AppData.getScheduledDuration(conflicts[i]);
    		graphX[i][0] = getX(s);
    		graphX[i][1] = getX(e);
    		graphX[i][2] = graphX[i][1] - graphX[i][0];
    		timeStr[i] = Formatter.getCurrent().getTime(s)
    			+ " - " + Formatter.getCurrent().getTime(e);
    	}

//    	if (target != null) {
//    		for (int i = 1; i < graphX.length; i++) {
//    			graphX[i][3] = Math.max(graphX[0][0], graphX[i][0]);
//        		graphX[i][4] = Math.min(graphX[0][1], graphX[i][1]) - graphX[i][3];
//    		}
//    		graphX[0][3] = graphX[0][0];
//    		graphX[0][4] = graphX[0][2];
//    	} else
    	if (graphX.length > 2) {
    		ArrayList start = new ArrayList();
    		ArrayList end = new ArrayList();
        	for (int i = 0; i < graphX.length; i++) {
        		start.add(new Integer(graphX[i][0]));
        		end.add(new Integer(graphX[i][1]));
        	}
        	Collections.sort(start);
        	Collections.sort(end);
        	int conS = ((Integer) start.get(2)).intValue();
        	int conE = ((Integer) end.get(end.size()-3)).intValue();
        	for (int i = 0; i < graphX.length; i++) {
        		graphX[i][3] = Math.max(conS, graphX[i][0]);
        		graphX[i][4] = Math.min(conE, graphX[i][1]);
        		graphX[i][5] = graphX[i][4] - graphX[i][3];
        	}
    	}
    }

    private int getX(long t) { // 485~727(242px)
    	// gap : 242 = t : ?
    	int time = (int) (t - earliest);
    	int x = (int) ((242L-13L) * time / gap);
    	return x + 485;
    }

    protected class CommonRenderer extends Renderer {

		public Rectangle getPreferredBounds(UIComponent c) {
			return null;
		}

		protected void paint(Graphics g, UIComponent c) {}

		public void prepare(UIComponent c) {
			i_07_respop_glow = dataCenter.getImage("07_respop_glow.png");
        	i_07_respop_high = dataCenter.getImage("07_respop_high.png");
        	i_07_respop_title = dataCenter.getImage("07_respop_title.png");
        	i_07_respop_sh = dataCenter.getImage("07_respop_sh.png");
        	i_07_respop_icon01 = dataCenter.getImage("07_respop_icon01.png");
        	i_07_respop_icon02 = dataCenter.getImage("07_respop_icon02.png");
            i_07_res_line = dataCenter.getImage("07_res_line.png");
            i_07_res_line_foc = dataCenter.getImage("07_res_line_foc.png");
            i_07_res_foc = dataCenter.getImage("07_res_foc.png");
            i_07_res_pgbg_t = dataCenter.getImage("07_res_pgbg_t.png");
            i_07_res_pgbg_m = dataCenter.getImage("07_res_pgbg_m.png");
            i_07_res_pgbg_b = dataCenter.getImage("07_res_pgbg_b.png");
            i_07_res_pgsh_t = dataCenter.getImage("07_res_pgsh_t.png");
            i_07_res_pgsh_b = dataCenter.getImage("07_res_pgsh_b.png");
            i_07_bar_rec01 = dataCenter.getImage("07_bar_rec01.png");
            i_07_bar_rec02 = dataCenter.getImage("07_bar_rec02.png");
            i_07_bar_rec03 = dataCenter.getImage("07_bar_rec03.png");
            i_07_bar_rec04 = dataCenter.getImage("07_bar_rec04.png");
            i_07_bar_rec05 = dataCenter.getImage("07_bar_rec05.png");
            i_02_ars_t = dataCenter.getImage("02_ars_t.png");
            i_02_ars_b = dataCenter.getImage("02_ars_b.png");
            i_icon_noti_red = dataCenter.getImage("icon_noti_red.png");
            i_icon_g_check = dataCenter.getImage("icon_g_check.png");
            i_btn_293 = dataCenter.getImage("btn_293.png");
            i_btn_293_foc = dataCenter.getImage("btn_293_foc.png");
            i_btn_293_in = dataCenter.getImage("btn_293_in.png");
            i_12_ppop_sha = dataCenter.getImage("12_ppop_sha.png");
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_scrbg_conf = dataCenter.getImage("scrbg_conf.png");
            i_scr_bar = dataCenter.getImage("scr_bar.png");

            //->Kenneth[2015.5.24] R5
            i_07_res_pgbg_t_3 = dataCenter.getImage("07_res_pgbg_t_3.png");
            i_07_res_pgbg_m_3 = dataCenter.getImage("07_res_pgbg_m_3.png");
            i_07_res_pgbg_b_3 = dataCenter.getImage("07_res_pgbg_b_3.png");
            i_07_res_foc_3 = dataCenter.getImage("07_res_foc_3.png");
		}

    }
}

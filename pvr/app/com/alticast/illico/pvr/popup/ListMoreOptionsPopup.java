package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.ocap.ui.event.OCRcEvent;
import org.dvb.ui.DVBColor;
import org.ocap.shared.dvr.*;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.comp.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.gui.ScaleUPList;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardOption;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.epg.RemoteIterator;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
//->Kenneth[2017.3.7] R7.3 에 새로 추가되는 팝업임
public class ListMoreOptionsPopup extends Popup {
    // OK 버튼만 있는 static 팝업 타입
    public static final int TYPE_STATIC = 0;
    // Save 를 수정할 수 있는 OK/CANCEL 의 두개의 버튼만 있는 팝업 타입
    public static final int TYPE_EDIT = 1;
    private int type;

    // focus 가 OK 버튼에 있음
    public static final int FOCUS_OK = 0;
    // focus 가 CANCEL 버튼에 있음
    public static final int FOCUS_CANCEL = 1;
    // focus 가 Save 선택창에 있음
    public static final int FOCUS_SAVE = 2;
    private int focusIndex;

    // Recording 의 title
    private String title;

    private Vector names = new Vector();
    private Vector values = new Vector();

    private AbstractRecording rec;
    OptionEditField saveField;
    String groupKey;

    public ListMoreOptionsPopup(AbstractRecording r, int type) {
        if (Log.DEBUG_ON) Log.printDebug("ListMoreOptionsPopup("+type+")");
        this.rec = r;
        this.type = type;
        this.groupKey = r.getGroupKey();
        title = TextUtil.shorten(r.getTitle(), GraphicsResource.FM24, 400);
        if (type == TYPE_STATIC) {
            focusIndex = FOCUS_OK;
        } else {
            focusIndex = FOCUS_SAVE;
        }

        setRenderer(new PopupRenderer());
        names.add(dataCenter.getString("List.Size"));
        values.add(StorageInfoManager.getSizeString(r.getRecordedSize()));
        names.add(dataCenter.getString("List.Save_Location"));
        values.add(r.getLocation());
        names.add(dataCenter.getString("List.Repeat"));
        values.add(r.getRepeatText());
        names.add(dataCenter.getString("List.Save"));
        values.add(r.getSaveText());

        saveField = new OptionEditField(dataCenter.getString("pvr.Save"));
        resetSaveOption(groupKey != null);
        saveField.setInitialValue(r.getSaveText());
    }

    private void resetSaveOption(boolean repeat) {
        saveField.setOptions(repeat ? PvrUtil.SAVE_SERIES : PvrUtil.SAVE_RECORDING);
        if (repeat) {
            if (groupKey == null) {
                int count = PvrUtil.getDefaultKeepCount();
                if (count == 3) {
                    saveField.setSelectedKey(PvrUtil.LAST_3);
                } else if (count == 5) {
                    saveField.setSelectedKey(PvrUtil.LAST_5);
                } else {
                    saveField.setSelectedKey(PvrUtil.LAST_MAX);
                }
            } else {
                // 원래 값...
                saveField.setCurrentValue(rec.getSaveText());
            }
        } else {
            if (groupKey == null) {
                // 원래 값...
                saveField.setCurrentValue(rec.getSaveText());
            } else {
                long exp = PvrUtil.getDefaultExpirationPeriod();
                if (exp == 7 * Constants.MS_PER_DAY) {
                    saveField.setSelectedKey(PvrUtil.SAVE_7);
                } else if (exp == 14 * Constants.MS_PER_DAY) {
                    saveField.setSelectedKey(PvrUtil.SAVE_14);
                } else {
                    saveField.setSelectedKey(PvrUtil.SAVE_MAX);
                }
            }
        }
    }

    public boolean handleKey(int code) {
        if (type == TYPE_EDIT && focusIndex == FOCUS_SAVE) {
            boolean consumed = saveField.handleKey(code);
            if (consumed) {
                repaint();
                return true;
            }
        }
        switch (code) {
            case OCRcEvent.VK_ENTER:
                if (type == TYPE_STATIC) {
                    close();
                } else {
                    startClickingEffect();
                    if (focusIndex == FOCUS_OK) {
                        checkAndModify();
                        close();
                    } else if (focusIndex == FOCUS_CANCEL) {
                        close();
                    }
                }
                break;
            case OCRcEvent.VK_UP:
                if (type == TYPE_EDIT) {
                    if (focusIndex == FOCUS_OK || focusIndex == FOCUS_CANCEL) {
                        focusIndex = FOCUS_SAVE;
                    }
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (type == TYPE_EDIT) {
                    if (focusIndex == FOCUS_SAVE) {
                        focusIndex = FOCUS_OK;
                    }
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (type == TYPE_EDIT) {
                    if (focusIndex == FOCUS_CANCEL) {
                        focusIndex = FOCUS_OK;
                    }
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (type == TYPE_EDIT) {
                    if (focusIndex == FOCUS_OK) {
                        focusIndex = FOCUS_CANCEL;
                    }
                }
                break;
            case OCRcEvent.VK_EXIT:
                close();
                break;
            default:
                return false;
        }
        repaint();
        return true;
    }

    private void checkAndModify() {
        String saveValue = (String) saveField.getChangedValue();
        if (Log.DEBUG_ON) Log.printDebug("ListMoreOptionsPopup.checkAndModify() : saveValue = "+saveValue);
        if (saveValue != null) {
            try {
                long exp = 0;
                int keepOption = 0;
                if (PvrUtil.SAVE_MAX.equals(saveValue)) {
                    exp = Long.MAX_VALUE;
                } else if (PvrUtil.SAVE_14.equals(saveValue)) {
                    exp = Math.max(System.currentTimeMillis(), rec.getScheduledStartTime()) + Constants.MS_PER_DAY * 14;
                } else if (PvrUtil.SAVE_7.equals(saveValue)) {
                    exp = Math.max(System.currentTimeMillis(), rec.getScheduledStartTime()) + Constants.MS_PER_DAY * 7;
                } else if (PvrUtil.LAST_MAX.equals(saveValue)) {
                    keepOption = 0;
                } else if (PvrUtil.LAST_3.equals(saveValue)) {
                    keepOption = 3;
                } else if (PvrUtil.LAST_5.equals(saveValue)) {
                    keepOption = 5;
                }
                if (exp > 0) {
                    if (Log.DEBUG_ON) Log.printDebug("ListMoreOptionsPopup.checkAndModify() : Call rec.setExpirationTime(exp)");
                    rec.setExpirationTime(exp);
                } else {
                    if (Log.DEBUG_ON) Log.printDebug("ListMoreOptionsPopup.checkAndModify() : Call RecordingScheduler.setKeepCount()");
                    RecordingScheduler.getInstance().setKeepCount(rec.getGroupKey(), keepOption);
                }
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    public void startClickingEffect() {
        if (type == TYPE_STATIC) {
            clickEffect.start(403+1, 394, 154, 32);
        } else {
            if (focusIndex == FOCUS_OK) {
                clickEffect.start(320+1, 394, 154, 32);
            } else if (focusIndex == FOCUS_CANCEL) {
                clickEffect.start(484+1, 394, 154, 32);
            }
        }
    }

    private class PopupRenderer extends Renderer {
        Color cDim = new DVBColor(33, 33, 33, 204);
        Image bg = dataCenter.getImage("pop_506_bg.png");

        Image pop_focus_dim = dataCenter.getImage("05_focus_dim.png");
        Image pop_focus = dataCenter.getImage("05_focus.png");

        Image input38 = dataCenter.getImage("input_38.png");
        Image input38foc = dataCenter.getImage("input_38_foc.png");
        Image inbox142 = dataCenter.getImage("input_142.png");
        Image inbox142foc = dataCenter.getImage("input_142_foc.png");
        Image input142sc = dataCenter.getImage("input_142_sc.png");
        Image input142scfoc = dataCenter.getImage("input_142_sc_foc.png");

        Image input292 = dataCenter.getImage("input_292.png");
        Image input292focnor = dataCenter.getImage("input_292_foc_nor.png");
        Image input292scFoc = dataCenter.getImage("input_292_sc_foc.png");
        Image input292sc = dataCenter.getImage("input_292_sc.png");

        String save = dataCenter.getString("pvr.OK");
        String cancel = dataCenter.getString("pvr.Cancel");

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paint(Graphics g, UIComponent c) {
            // BG
            g.drawImage(bg, 197, 59, c);
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);

            // Header 에 title 그림
            GraphicUtil.drawStringCenter(g, title, 480, 116);

            // 하단의 버튼
            if (type == TYPE_STATIC) {
                g.drawImage(pop_focus, 320+83, 394, c);
                g.setColor(GraphicsResource.C_4_4_4);
                GraphicUtil.drawStringCenter(g, save, 398+83, 415);
            } else {
                g.drawImage(focusIndex == FOCUS_OK ? pop_focus : pop_focus_dim, 320, 394, c);
                g.drawImage(focusIndex == FOCUS_CANCEL ? pop_focus : pop_focus_dim, 484, 394, c);
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(GraphicsResource.C_4_4_4);
                GraphicUtil.drawStringCenter(g, save, 398, 415);
                GraphicUtil.drawStringCenter(g, cancel, 562, 415);
            }

            int nameX = 270;
            int valueX = 400;
            int startY = 190;
            int yGap = 47;
            int index = 0;
            g.setFont(GraphicsResource.BLENDER20);
            g.setColor(GraphicsResource.C_182_182_182);
            // Size
            g.drawString(names.elementAt(index).toString(), nameX, startY+index*yGap);
            g.drawString(values.elementAt(index).toString(), valueX, startY+index*yGap);
            index ++;
            // Location
            g.drawString(names.elementAt(index).toString(), nameX, startY+index*yGap);
            if (Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage())) {
                g.drawString("ENP "+values.elementAt(index).toString(), valueX, startY+index*yGap);
            } else {
                g.drawString(values.elementAt(index).toString()+" PVR", valueX, startY+index*yGap);
            }
            index ++;
            // Repeat 
            g.drawString(names.elementAt(index).toString(), nameX, startY+index*yGap);
            g.drawString(values.elementAt(index).toString(), valueX, startY+index*yGap);
            index ++;
            // Save
            g.drawString(names.elementAt(index).toString(), nameX, startY+index*yGap);
            if (type == TYPE_STATIC) {
                g.drawString(values.elementAt(index).toString(), valueX, startY+index*yGap);
            } else {
                g.translate(valueX, startY+index*yGap-22);
                saveField.paint(g, focusIndex == FOCUS_SAVE && !saveField.isEditing(), c);
                g.translate(-valueX, -(startY+index*yGap-22));
                //Save field 룰 수정중인 경우 
                if (saveField.isEditing()) {
                    g.setColor(cDim);
                    g.fillRect(227, 89, 506, 361);
                    g.setColor(GraphicsResource.C_182_182_182);
                    g.drawString(names.elementAt(index).toString(), nameX, startY+index*yGap);
                    g.translate(valueX, startY+index*yGap-22);
                    saveField.paint(g, true, c);
                    g.translate(-valueX, -(startY+index*yGap-22));
                }
            }
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

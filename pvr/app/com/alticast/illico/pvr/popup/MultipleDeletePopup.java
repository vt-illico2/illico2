package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.*;
import java.rmi.*;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.navigation.RecordingList;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.hn.recording.RecordingContentItem;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.hn.*;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.pvr.communication.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;

public class MultipleDeletePopup extends Popup implements PopupContainer {

    public static final int ALL = 0;
    public static final int DELETE = 1;
    public static final int CANCEL = 2;

    private Popup currentPopup;

    MultipleDeleteRenderer multipleDeleteRenderer = new MultipleDeleteRenderer();

    private int listFocus = 0;

    private boolean[] deleteCheck;
    private AbstractRecording[] recorders;

    private Vector deleteRecords = new Vector();

    private String[] buttons;

    private int selectedCount = 0;

    public MultipleDeletePopup(Vector list) {
        setRenderer(new MultipleDeleteRenderer());
        Vector vector = new Vector();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            Object o = list.elementAt(i);
            if (o instanceof LeafRecordingRequest) {
                vector.addElement(new LocalRecording((LeafRecordingRequest) o));
            } else if (o instanceof RecordingContentItem) {
                RemoteRecording rr = new RemoteRecording((RecordingContentItem) o);
                if (rr.getStateGroup() == RecordingListManager.COMPLETED) {
                    vector.addElement(rr);
                }
            }
        }
        vector = (new SizeSortingOption()).sort(vector);
        recorders = new AbstractRecording[vector.size()];
        vector.copyInto(recorders);
        deleteCheck = new boolean[recorders.length];

        focus = -1;
        this.setVisible(true);

        buttons = new String[] {
            dataCenter.getString("pvr.select_all"),
            dataCenter.getString("pvr.multiDeleteBtn1"),
            dataCenter.getString("pvr.Cancel"),
        };
    }

    public void stop() {
        deleteCheck = null;
        recorders = null;
        this.setVisible(false);
    }

    public void dispose() {
        deleteRecords.clear();
        deleteRecords = null;
    }

    public boolean handleKey(int code) {
        Popup p = currentPopup;
        if (p != null) {
            return p.handleKey(code);
        }
        switch (code) {
        case OCRcEvent.VK_LEFT:
            if (focus >= 0) {
                focus = -1;
            }
            break;
        case OCRcEvent.VK_RIGHT:
            if (focus < 0) {
                focus = 0;
            }
            break;
        case OCRcEvent.VK_UP:
            if (focus < 0) {
                if (listFocus > 0) {
                    listFocus--;
                }
            } else if (focus > 0) {
                focus--;
                if (focus == DELETE && selectedCount <= 0) {
                    focus--;
                }
            }
            break;
        case OCRcEvent.VK_DOWN:
            if (focus < 0) {
                if (listFocus < recorders.length - 1) {
                    listFocus++;
                }
            } else if (focus < CANCEL) {
                focus++;
                if (focus == DELETE && selectedCount <= 0) {
                    focus++;
                }
            }
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"MultipleDeletePopup.handlKey(VK_ENTER)");
            if (focus >= 0) {
                startClickingEffect();
                switch (focus) {
                    case ALL:
                        checkAll();
                        break;
                    case DELETE:
                        deleteAll();
                        break;
                    case CANCEL:
                        close();
                        break;

                }
            } else {
                if (deleteCheck != null) {
                    boolean old = deleteCheck[listFocus];
                    deleteCheck[listFocus] = !old;
                    if (old) {
                        selectedCount--;
                    } else {
                        selectedCount++;
                    }
                    changeButton(selectedCount > 0);
                }
            }
            break;
        case OCRcEvent.VK_EXIT:
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public synchronized void showPopup(Popup p) {
        p.setVisible(true);
        p.prepare();
        this.add(p);
        p.start(this);
        currentPopup = p;
        repaint();
    }

    public synchronized void hidePopup(Popup p) {
        try {
            this.remove(p);
            p.stop();
            repaint();
        } catch (Exception ex) {
        }
        currentPopup = null;
    }

    private void changeButton(boolean unselect) {
        if (unselect) {
            buttons[0] = dataCenter.getString("pvr.unselect_all");
        } else {
            buttons[0] = dataCenter.getString("pvr.select_all");
        }
    }

    private boolean isAnyChecked() {
        for (int a = 0; a < deleteCheck.length; a++) {
            if (deleteCheck[a]) {
                return true;
            }
        }
        return false;
    }

    private void checkAll() {
        boolean check = dataCenter.getString("pvr.select_all").equals(buttons[0]);
        for (int a = 0; a < deleteCheck.length; a++) {
            deleteCheck[a] = check;
        }
        if (check) {
            selectedCount = deleteCheck.length;
        } else {
            selectedCount = 0;
        }
        changeButton(check);
    }

    private void deleteAll() {
        deleteRecords.clear();
        for (int a = 0; a < deleteCheck.length; a++) {
            if (deleteCheck[a]) {
                deleteRecords.add(recorders[a]);
            }
        }

        if (!ParentalControl.isProtected(deleteRecords)) {
            showConfirmPopup();
        } else {
            PinEnablerListener pl = new PinEnablerListener() {
                public void receivePinEnablerResult(int response, String detail) throws RemoteException {
                    if (response != PreferenceService.RESPONSE_SUCCESS) {
                        return;
                    }
                    showConfirmPopup();
                }
            };
            PreferenceProxy.getInstance().showPinEnabler(dataCenter.getString("pin.delete"), pl);
        }
    }

    private void showConfirmPopup() {
        long deleteSumSize = 0;
        for (int a = 0; a < deleteCheck.length; a++) {
            if (deleteCheck[a]) {
                deleteSumSize = deleteSumSize + recorders[a].getRecordedSize();
            }
        }
        Log.printDebug("deleteSumSize = " + deleteSumSize);

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                if (focus == 0) {
                    RecordedList.getInstance().deleteRecordings(deleteRecords, false);
                    MultipleDeletePopup.this.close();
                }
            }
        };

        ListSelectPopup confirmPopup = new ListSelectPopup();
        confirmPopup.setListener(listener);
        confirmPopup.setPopupTitle(dataCenter.getString("wizard.CONFIRM DELETION"));

        StringBuffer buf = new StringBuffer();
        int count = deleteRecords.size();
        String str = dataCenter.getString("wizard.delConfirm1");
        str = TextUtil.replace(str, "%1", String.valueOf(count));
        str = TextUtil.replace(str, "%2", count > 1 ? "s" : "");
        buf.append(str);
        buf.append('\n');
        buf.append(TextUtil.replace(dataCenter.getString("wizard.delConfirm2"),
                    "%%", StorageInfoManager.getSizeString(deleteSumSize)));

        confirmPopup.setDescription(buf.toString());
        showPopup(confirmPopup);
    }

    public boolean[] getDeleteCheck() {
        return deleteCheck;
    }

    public void startClickingEffect() {
        if (focus >= 0) {
            clickEffect.start(552, 399 - 37 + (focus * 37), 293, 32);
        }
    }

    class MultipleDeleteRenderer extends Renderer {
        public final int ROW_COUNT = 9;
        public final int MIDDLE_POS = (ROW_COUNT - 1) / 2;
        public final int LIST_Y_GAP = 32;

        private Image pop759bg = dataCenter.getImage("pop_759_bg.png");
        private Image popSh = dataCenter.getImage("12_ppop_sha.png");
        private Image check = dataCenter.getImage("check.png");
        private Image checkFoc = dataCenter.getImage("check_foc.png");
        private Image checkBox = dataCenter.getImage("check_box.png");
        private Image checkBoxFoc = dataCenter.getImage("check_box_foc.png");
        private Image line = dataCenter.getImage("07_del_line.png");

        private Image del_pgbg_b = dataCenter.getImage("07_del_pgbg_b.png");
        private Image del_pgbg_m = dataCenter.getImage("07_del_pgbg_m.png");
        private Image del_pgbg_t = dataCenter.getImage("07_del_pgbg_t.png");

        private Image del_foc = dataCenter.getImage("07_del_foc.png");
        private Image del_foc_dim = dataCenter.getImage("07_del_foc_dim.png");

        private Image del_pgsh_t = dataCenter.getImage("07_del_pgsh_t.png");
        private Image del_pgsh_b = dataCenter.getImage("07_del_pgsh_b.png");

        private Image ars_t = dataCenter.getImage("02_ars_t.png");
        private Image ars_b = dataCenter.getImage("02_ars_b.png");

        private Image btn_dim = dataCenter.getImage("btn_293.png");
        private Image btn_in = dataCenter.getImage("btn_293_in.png");
        private Image btn = dataCenter.getImage("btn_293_foc.png");

        private String description = dataCenter.getString("pvr.multiDeleteDesc1");
        private String title = dataCenter.getString("pvr.multiDeleteTitle");

        private Color c110 = new Color(110, 110, 110);
        private Color c50 = new Color(50, 50, 50);

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        protected void paint(Graphics g, UIComponent c) {
            boolean[] isChecked = deleteCheck;

            g.drawImage(popSh, 101, 479, 759, 57, c);
            g.drawImage(pop759bg, 71, 26, c);

            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_255_255_255);

            String des[] = TextUtil.split(description, g.getFontMetrics(), 480);
            for (int a = 0; des != null && a < des.length; a++) {
                int upY = 0;
                if (des.length == 2) {
                    upY = 7;
                }
                if (des[a] != null) {
                    GraphicUtil.drawStringCenter(g, des[a], 478, 139 + a * 17 - upY);
                }
            }

            g.drawImage(del_pgbg_b, 116, 458, c);
            g.drawImage(del_pgbg_m, 116, 184, 426, 277, c);
            g.drawImage(del_pgbg_t, 116, 174, c);

            g.setFont(GraphicsResource.BLENDER18);
            Formatter formatter = Formatter.getCurrent();

            int listSize = recorders.length;
            ///////////////////

            int over = listSize - ROW_COUNT;
            int move = listFocus - MIDDLE_POS;
            int firstIndex;
            if (over > 0 && move > 0) {
                firstIndex = Math.min(over, move);
            } else {
                firstIndex = 0;
            }
            // focus
            if (listFocus >= 0 && listSize > 0) {
                Image im = focus < 0 ? del_foc : del_foc_dim;
                g.drawImage(im, 114, 174 + (listFocus - firstIndex) * LIST_Y_GAP, c);
            }

            int size = Math.min(ROW_COUNT, listSize);
            int index = firstIndex;
            for (int i = 0; i < size; i++) {
                g.drawImage(line, 127, 209 + i * LIST_Y_GAP, c);

                if (index == listFocus) {
                    Image im = focus < 0 ? del_foc : del_foc_dim;
                    g.drawImage(im, 114, 174 + (index - firstIndex) * LIST_Y_GAP, c);
                }

                boolean hasFocus = (index == listFocus);
                AbstractRecording r = recorders[index];

                g.setColor(hasFocus ? GraphicsResource.C_0_0_0 : GraphicsResource.C_255_255_255);
                String str = TextUtil.shorten(ParentalControl.getTitle(r), g.getFontMetrics(), 195);
                g.drawString(str, 153, 199 + LIST_Y_GAP * i);

                g.drawString(formatter.getDayText(r.getScheduledStartTime()), 407, 199 + LIST_Y_GAP * i);

                String sizeStr = StorageInfoManager.getSizeString(r.getRecordedSize());
                GraphicUtil.drawStringRight(g, sizeStr, 531, 199 + LIST_Y_GAP * i);

                g.drawImage(hasFocus ? checkBoxFoc : checkBox, 127, 184 + LIST_Y_GAP * i, c);
                if (isChecked[index]) {
                    g.drawImage(hasFocus ? checkFoc : check, 130, 183 + LIST_Y_GAP * i, c);
                }
                index++;
            }

            if (firstIndex > 0) {
                g.drawImage(del_pgsh_t, 117, 175, c);
                g.drawImage(ars_t, 312, 168, c);
            }

            if (firstIndex + ROW_COUNT < listSize) {
                g.drawImage(del_pgsh_b, 117, 432, c);
                g.drawImage(ars_b, 312, 458, c);
            }

            g.setFont(GraphicsResource.BLENDER18);
            int sy = 362;
            for (int i = 0; i < buttons.length; i++) {
                Image im;
                if (i == DELETE && selectedCount <= 0) {
                    g.drawImage(btn_in, 552, sy, c);
                    g.setColor(c110);
                    GraphicUtil.drawStringCenter(g, buttons[i], 699+1, sy + 22+1);
                    g.setColor(c50);
                } else {
                    if (focus == i) {
                        g.drawImage(btn, 552, sy, c);
                    } else {
                        g.drawImage(btn_dim, 552, sy, c);
                    }
                    g.setColor(GraphicsResource.C_0_0_0);
                }
                GraphicUtil.drawStringCenter(g, buttons[i], 699, sy + 22);
                sy += 37;
            }

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            GraphicUtil.drawStringCenter(g, title, 478, 82);

            if (currentPopup != null) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);
            }
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

    }

}

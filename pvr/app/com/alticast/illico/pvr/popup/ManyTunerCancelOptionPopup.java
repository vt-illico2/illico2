package com.alticast.illico.pvr.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.*;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.issue.ManyTunerConflictResolveSession;
import com.alticast.illico.pvr.issue.ConflictRenderingItem;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ManyTunerCancelOptionPopup extends CancelOptionPopup {

    public static final int CANCEL = 0;
    public static final int SKIP_THIS = 1;
    public static final int DELETE_ALL = 2;

    ManyTunerConflictResolveSession session;
    ConflictRenderingItem cri;

    public ManyTunerCancelOptionPopup(ManyTunerConflictResolveSession session, ConflictRenderingItem cri) {
        ren.setMessage(TextUtil.replace(dataCenter.getString("pvr.cancelOptionDesc"), "%%", cri.title));
        this.session = session;
        this.cri = cri;
    }

    public void start() {
    	this.focus = 0;
        super.start();
    }

    protected void processSkipThis() {
        cri.action = ConflictRenderingItem.DELETE;
        cri.deleteOption = ConflictRenderingItem.DELETE_OPTION_THIS;
        session.showCurrentConflictPopup();
    }

    protected void processDeleteEntire() {
        cri.action = ConflictRenderingItem.DELETE;
        cri.deleteOption = ConflictRenderingItem.DELETE_OPTION_ALL;
        session.showCurrentConflictPopup();
    }

    protected void processCancel() {
        cri.action = ConflictRenderingItem.KEEP;
        session.showCurrentConflictPopup();
    }


}


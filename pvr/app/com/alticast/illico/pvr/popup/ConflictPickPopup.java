package com.alticast.illico.pvr.popup;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Vector;
import java.util.HashMap;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.RecordManager;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ConflictPickPopup extends ConflictPopup {
    OwnRenderer renderer = new OwnRenderer();

    private int listOffset, listFocus;
    private int btnFocus; // btnFocus == -1, focus on list
    private int caller;

    private boolean modeOthershow;
    private HashMap otherShowtimes;
    private boolean[] checked;

    private PvrFooter footer;

    public ConflictPickPopup(DualTunerConflictResolveSession session) {
        super(session);
        setRenderer(renderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);

        footer = new PvrFooter();
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        footer.setBounds(545, 466, 200, 30);
        add(footer);
    }

    public void setConflictRecording(RecordingRequest[] recordings) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictPickPopup.setConflictRecording("+recordings+")");
    	if (modeOthershow && otherShowtimes != null) {
    		// DDC 34
    		// re-order to available and not available
    		ArrayList avail = new ArrayList();
    		ArrayList na = new ArrayList();
    		for (int i = 0; i < recordings.length; i++) {
    			if (otherShowtimes.containsKey(recordings[i]) && ((TvProgram[]) otherShowtimes.get(recordings[i])).length >= 1) {
    				avail.add(recordings[i]);
    			} else {
    				na.add(recordings[i]);
    			}
    		}
    		RecordingRequest[] array = new RecordingRequest[recordings.length];

            int avSize = avail.size();
    		for (int i = 0; i < avSize; i++) {
    			array[i] = (RecordingRequest) avail.get(i);
    		}
    		for (int i = 0; i < na.size(); i++) {
    			array[i + avSize] = (RecordingRequest) na.get(i);
    		}
            recordings = array;
    	}

    	super.setConflictRecording(recordings);
    	checked = new boolean[conflicts.length];
    	for (int i = 0; i < checked.length; i++) { // DDC 34
    		checked[i] = true;
    	}
    	hasConflict = -1;
    }

    public void setMode(boolean otherShow) {
    	modeOthershow = otherShow;
        footer.setLocation(545, otherShow ? 423 : 466);
    }

    public void setOtherShowtimes(HashMap otherShowtimes) {
    	this.otherShowtimes = otherShowtimes;
    }

    public void recordingMoved(RecordingRequest moved) {
    	if (otherShowtimes != null) {
    		otherShowtimes.remove(moved);
    	}
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictPickPopup.start()");
    	listOffset = listFocus = 0;
    	btnFocus = -1;
        super.start();
        this.setVisible(true);
    }

    public void start(int num) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictPickPopup.start("+num+")");
    	listOffset = listFocus = 0;
    	btnFocus = -1;
        super.start();
        this.setVisible(true);
        caller = num;
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictPickPopup.stop()");
        super.stop();
        this.setVisible(false);
    }

    private void keyEnter() {
    	if (btnFocus < 0) { // list focused
    		checked[listFocus+listOffset] = !checked[listFocus+listOffset];
    		hasConflict = -1;
    		return;
    	}
        startClickingEffect();
        close();
    	switch (btnFocus) {
    	case 0: // confirm
    		ArrayList willBeRecorded = new ArrayList();
    		ArrayList repeated = new ArrayList();
    		for (int i = 0; i < checked.length; i++) {
    			if (checked[i]) {
    				willBeRecorded.add(conflicts[i]);
    			} else {
    				String groupKey = AppData.getString(conflicts[i], AppData.GROUP_KEY);
    				if (groupKey != null && groupKey.length() > 0) {
    					repeated.add(conflicts[i]);
    				}
    			}
    		}
    		if (repeated.size() == 0) {
    			session.showConfirmPopup(willBeRecorded, null);
    		} else {
    			session.showCancelOptionPopup(willBeRecorded, repeated);
    		}
    		break;
//    	case 1: // cancel
//    		IssueManager.getInstance().showConflictPopup();
//    		break;
    	}
    }

    public boolean handleKey(int type){
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictPickPopup.handleKey("+type+")");
        switch (type) {
        case KeyEvent.VK_UP:
        	if (btnFocus >= 0) { // btn
        		btnFocus = -1;
        		if (btnFocus < 0) { // focus to list
        			listFocus = Math.min(4, conflicts.length-1);
        			listOffset = Math.max(0, conflicts.length-5);
        		}
        	} else { // list
        		moveFocus(true);
        	}
            break;
        case KeyEvent.VK_DOWN:
        	if (btnFocus >= 0) { // btn

        	} else { // list
        		if (moveFocus(false)) {
                    if (!modeOthershow && !hasConflict()) {
                        btnFocus = 0;
                    }
        		}
        	}
            break;
        case KeyEvent.VK_LEFT:
        	break;
        case KeyEvent.VK_RIGHT:
        	break;
        case KeyEvent.VK_ENTER:
        	if (modeOthershow) {
        		switch (btnFocus) {
        		case 0: // OK
        			break;
//        		case 1: // cancel
//        			IssueManager.getInstance().showConflictPopup();
//        			break;
        		default: // on list
        			TvProgram[] others = (TvProgram[]) otherShowtimes.get(conflicts[listFocus+listOffset]);
        			if (others != null && others.length > 0) { // DDC 34
        				session.showTimetable(conflicts[listFocus+listOffset], others);
        			}
//        			IssueManager.getInstance().showTimetable(conflicts[listFocus+listOffset],
//                            (TvProgram[]) otherShowtimes.get(conflicts[listFocus+listOffset]) );
        			break;
        		}
        	} else {
        		keyEnter();
        	}
            break;
        case KeyCodes.LAST:
        case KeyEvent.VK_PAGE_UP:
        case KeyEvent.VK_PAGE_DOWN:
            return true;
        case OCRcEvent.VK_EXIT:
        	session.showConflictPopup();
            break;
        case OCRcEvent.VK_DISPLAY_SWAP:
            if (Log.EXTRA_ON) {
                // TO TEST
                try {
                    conflicts[listFocus+listOffset].delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            break;
        default:
            return false;
        }

        repaint();
        return true;
    }

    private boolean moveFocus(boolean up) {
    	int len = conflicts.length;
    	if (canMoveFocus(up)) {
    		if (up) {
    			listFocus = Math.max(0, listFocus - 1);
    			return false;
    		} else { // down
    			int old = listFocus;
    			listFocus = Math.min(Math.min(4, len - 1), listFocus + 1);
    			return (old == listFocus); // no move, focus to button
    		}

    	} else {
    		listOffset += up ? -1 : 1;
    		return false;
    	}
    }

    private boolean canMoveFocus(boolean up) {
    	int len = conflicts.length;
    	if (len < 5) return true; // less than 1 page

    	if (up) {
    		return listFocus > 2 || listOffset == 0;
    	} else { // down
    		return listFocus < 2 || listOffset >= len - 5;
    	}
    }

    private int hasConflict = -1;
    private boolean hasConflict() {
    	if (hasConflict == -1) { // -1 means "need check"
    		if (checkCnt() <= 2) {
    			hasConflict = 1; // 1 means "no conflict"
    			return false;
        	}
        	ArrayList list = new ArrayList();
        	for (int i = 0; i < checked.length; i++) {
        		if (checked[i]) {
        			list.add(conflicts[i]);
        		}
        	}
        	RecordingRequest[] recordings = new RecordingRequest[list.size()];
        	list.toArray(recordings);
//            boolean foundConflict = IssueManager.hasConflict(recordings, earliest, latest);
            boolean foundConflict = ConflictResolveSession.hasConflict(recordings);
        	hasConflict = foundConflict ? 2 : 1; // 2 means "has conflict"
    	}
    	return hasConflict == 2;
    }

    private int checkCnt() {
    	if (checked == null) {
    		return 0;
    	}
    	int cnt = 0;
    	for (int i = 0; i < checked.length; i++) {
    		if (checked[i]) cnt++;
    	}
    	return cnt;
    }

    public void startClickingEffect() {
        if (!modeOthershow && btnFocus == 0) {
            clickEffect.start(317+83+1, 415, 154, 32);
        }
    }


    private class OwnRenderer extends CommonRenderer {
        Image i_05_focus;
        Image i_05_focus_dim;
        Image check, check_foc, check_box, check_box_foc;
        Image i_pglow610_t;
        Image i_pglow610_m;
        Image i_pglow610_b;

        protected void paint(Graphics g, UIComponent c) {
            //->Kenneth[2015.5.24] R5 : 요구사항이 2 tuner 인 경우만 R5 UI 를 적용하라는 것이어서 
            // 그리는 코드를 중복되는 부분이 있어도 아예 두개로 분할함. MANY_TUNER 인 경우는 기존대로 그려짐
            if (App.MANY_TUNER) {
                //////////////////////////////////////////////////////////////
                // 여기는 이전 그리는 코드 그대로임
                //////////////////////////////////////////////////////////////
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);

                g.setColor(GraphicsResource.C_33_33_33);
                if (modeOthershow) {
                    g.drawImage(i_pop_sha, 204, 415, 550, 79, c);
                    g.drawImage(i_pglow610_t, 174, 66+14, c);
                    g.drawImage(i_pglow610_m, 174, 109+14, 610, 304, c);
                    g.drawImage(i_pglow610_b, 174, 413+14, c);
                    g.fillRect(204, 96+14, 550, 308);
                    g.drawImage(i_07_respop_title, 203, 134+14, c);
                    g.drawImage(i_07_respop_high, 204, 96+14, c);
                } else {
                    g.drawImage(i_pglow610_t, 174, 44, c);
                    g.drawImage(i_pglow610_m, 174, 87, 610, 350, c);
                    g.drawImage(i_pglow610_b, 174, 437, c);
                    g.fillRect(204, 74, 550, 387);
                    g.drawImage(i_07_respop_sh, 202, 458, c);
                    g.drawImage(i_07_respop_title, 203, 112, c);
                    g.drawImage(i_07_respop_high, 204, 74, c);
                }

                // title
                g.setFont(GraphicsResource.BLENDER24);
                g.setColor(GraphicsResource.C_255_203_0);
                if (modeOthershow) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.findOtherTitle"), 480, 123+14);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickTitle"), 480, 101);
                }


                // desc
                g.setFont(GraphicsResource.BLENDER17);
                g.setColor(GraphicsResource.C_255_255_255);
                if (modeOthershow) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.findOtherDesc"), 480, 171+14);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc1"), 480, 149);
                }

                if (modeOthershow) {
                    g.translate(0, 38);
                }
                // list bg
                g.drawImage(i_07_res_pgbg_t, 221, 172, c);
                g.drawImage(i_07_res_pgbg_m, 221, 182, 516, 154, c);
                g.drawImage(i_07_res_pgbg_b, 221, 336, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(222, 208, 514, 34);
                g.fillRect(222, 276, 514, 34);

                boolean hasConflict = modeOthershow ? false : hasConflict();
                // list
                int x, w;
                for (int i = 0; i < 5 && i < conflicts.length; i++) {
                    boolean foc = btnFocus < 0 && listFocus == i;
                    if (foc) {
                        g.drawImage(i_07_res_foc, 219, 172 + i * 34, c);
                        g.setColor(GraphicsResource.C_0_0_0);
                        g.drawImage(i_07_res_line_foc, 475, 184 + i * 34, c);
                        if (modeOthershow == false) {
                            g.drawImage(check_box_foc, 231, 184 + listFocus * 34, c);
                            if (checked[i+listOffset]) {
                                g.drawImage(check_foc, 231, 183 + i * 34, c);
                            }
                        }
                    } else {
                        g.drawImage(i_07_res_line, 475, 184 + i * 34, c);
                        if (modeOthershow == false) {
                            g.drawImage(check_box, 230, 183 + i * 34, c);
                            if (checked[i+listOffset]) {
                                g.drawImage(check, 233, 182 + i * 34, c);
                            }
                        }
                    }
                    boolean naShow = modeOthershow && otherShowtimes != null// DDC 34
                        && (otherShowtimes.containsKey(conflicts[i+listOffset]) == false
                                || ((TvProgram[]) otherShowtimes.get(conflicts[i+listOffset])).length < 1);

                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[i+listOffset], AppData.CHANNEL_NUMBER);
                    g.drawString(t, modeOthershow ? 233 : 256, 197 + i * 34);
                    if (!foc) {
                        g.setColor(naShow ? GraphicsResource.C_118_118_118 : GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[i+listOffset]), g.getFontMetrics(), 170);
                    g.drawString(t, modeOthershow ? 276 : 298, 197 + i * 34);

                    if (naShow) { // DDC 34
                        if (!foc) {
                            g.setColor(GraphicsResource.C_118_118_118);
                        }
                        g.drawString("N/A", 488, 197 + i * 34);
                    } else {
                        g.setFont(GraphicsResource.BLENDER15);
                        if (!foc) {
                            g.setColor(GraphicsResource.C_240_240_240);
                        }
                        GraphicUtil.drawStringRight(g, timeStr[i+listOffset], graphX[i+listOffset][1], 190 + i * 34, 485, g.getFontMetrics());

                        g.setColor(GraphicsResource.C_37_37_37); // shadow
                        g.fillRect(graphX[i+listOffset][0]+1, 194 + i * 34, graphX[i+listOffset][2], 9);

                        g.drawImage(checked[i+listOffset] ? (hasConflict ? i_07_bar_rec01 : i_07_bar_rec04) : i_07_bar_rec02, graphX[i+listOffset][0], 193 + i * 34, graphX[i+listOffset][2], 9, c);
                    }
                }

                // list shadow, arrow
                if (listOffset > 0) {
                    g.drawImage(i_02_ars_t, 464, 164, c);
                    g.drawImage(i_07_res_pgsh_t, 222, 173, c);
                }
                if (conflicts != null && listOffset + 5 < conflicts.length) {
                    g.drawImage(i_02_ars_b, 464, 337, c);
                    g.drawImage(i_07_res_pgsh_b, 222, 310, c);
                }

                if (!modeOthershow) {
                    // desc
                    g.setFont(GraphicsResource.BLENDER17);
                    g.setColor(GraphicsResource.C_182_182_182);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc2"), 480, 373);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc3"), 480, 391);

                    // buttons
                    g.setFont(GraphicsResource.BLENDER18);
                    g.setColor(hasConflict() ? GraphicsResource.C_91_91_91 : GraphicsResource.C_3_3_3);
                    g.drawImage(btnFocus == 0 ? i_05_focus : i_05_focus_dim, 317+83, 415, c);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.OK"), 395+83, 436);
                }

                if (modeOthershow) {
                    g.translate(0, -38);
                }
            } else {
                //////////////////////////////////////////////////////////////
                // R5 에서 새롭게 수정된 코드
                //////////////////////////////////////////////////////////////
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);

                g.setColor(GraphicsResource.C_33_33_33);
                if (modeOthershow) {
                    g.drawImage(i_pop_sha, 204, 415, 550, 79, c);
                    g.drawImage(i_pglow610_t, 174, 66+14, c);
                    g.drawImage(i_pglow610_m, 174, 109+14, 610, 304, c);
                    g.drawImage(i_pglow610_b, 174, 413+14, c);
                    g.fillRect(144, 96+14, 670, 308);
                    //g.fillRect(204, 96+14, 550, 308);
                    g.drawImage(i_07_respop_title, 203, 134+14, c);
                    g.drawImage(i_07_respop_high, 204, 96+14, c);
                } else {
                    g.drawImage(i_pglow610_t, 174, 44, c);
                    g.drawImage(i_pglow610_m, 174, 87, 610, 350, c);
                    g.drawImage(i_pglow610_b, 174, 437, c);
                    g.fillRect(144, 74, 670, 387);
                    //g.fillRect(204, 74, 550, 387);
                    g.drawImage(i_07_respop_sh, 202, 458, c);
                    g.drawImage(i_07_respop_title, 203, 112, c);
                    g.drawImage(i_07_respop_high, 204, 74, c);
                }

                // title
                g.setFont(GraphicsResource.BLENDER24);
                g.setColor(GraphicsResource.C_255_203_0);
                if (modeOthershow) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.findOtherTitle"), 480, 123+14);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickTitle"), 480, 101);
                }


                // desc
                g.setFont(GraphicsResource.BLENDER17);
                g.setColor(GraphicsResource.C_255_255_255);
                if (modeOthershow) {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.findOtherDesc"), 480, 171+14);
                } else {
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc1"), 480, 149);
                }

                if (modeOthershow) {
                    g.translate(0, 38);
                }

                // list bg
                g.drawImage(i_07_res_pgbg_t_3, 161, 172, c);
                g.drawImage(i_07_res_pgbg_m_3, 161, 182, 636, 154, c);
                g.drawImage(i_07_res_pgbg_b_3, 161, 336, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(162, 208, 634, 34);
                g.fillRect(162, 276, 634, 34);
                /*
                g.drawImage(i_07_res_pgbg_t, 221, 172, c);
                g.drawImage(i_07_res_pgbg_m, 221, 182, 516, 154, c);
                g.drawImage(i_07_res_pgbg_b, 221, 336, c);
                g.setColor(GraphicsResource.C_41_41_41);
                g.fillRect(222, 208, 514, 34);
                g.fillRect(222, 276, 514, 34);
                */

                boolean hasConflict = modeOthershow ? false : hasConflict();
                // list
                int x, w;
                int xShift = 60;
                for (int i = 0; i < 5 && i < conflicts.length; i++) {
                    boolean foc = btnFocus < 0 && listFocus == i;
                    if (foc) {
                        // 노란 포커스 바
                        g.drawImage(i_07_res_foc_3, 162, 172 + i * 34, c);
                        //g.drawImage(i_07_res_foc, 219, 172 + i * 34, c);

                        // 가운데 구분 바
                        g.drawImage(i_07_res_line_foc, 475+xShift, 184 + i * 34, c);
                        // 체크박스
                        g.setColor(GraphicsResource.C_0_0_0);
                        if (modeOthershow == false) {
                            g.drawImage(check_box_foc, 231-xShift, 184 + listFocus * 34, c);
                            if (checked[i+listOffset]) {
                                g.drawImage(check_foc, 231-xShift, 183 + i * 34, c);
                            }
                        }
                    } else {
                        // 가운데 구분 바
                        g.drawImage(i_07_res_line, 475+xShift, 184 + i * 34, c);
                        // 체크박스
                        if (modeOthershow == false) {
                            g.drawImage(check_box, 230-xShift, 183 + i * 34, c);
                            if (checked[i+listOffset]) {
                                g.drawImage(check, 233-xShift, 182 + i * 34, c);
                            }
                        }
                    }
                    boolean naShow = modeOthershow && otherShowtimes != null// DDC 34
                        && (otherShowtimes.containsKey(conflicts[i+listOffset]) == false
                                || ((TvProgram[]) otherShowtimes.get(conflicts[i+listOffset])).length < 1);

                    // 채널 넘버
                    g.setFont(GraphicsResource.BLENDER18);
                    if (!foc) {
                        g.setColor(GraphicsResource.C_200_200_200);
                    }
                    String t = AppData.getString(conflicts[i+listOffset], AppData.CHANNEL_NUMBER);
                    g.drawString(t, modeOthershow ? 233-xShift : 256-xShift, 197 + i * 34);

                    // 채널 로고 (R5 에서 새롭게 추가) : 매번 object 를 생성하는 게 그렇지만 귀찮아서 그냥 씀.
                    ConflictRenderingItem cri = new ConflictRenderingItem(conflicts[i+listOffset]);
                    if (cri.logo != null) {
                        g.drawImage(cri.logo, 235, 176+i*34, c);
                    }

                    // 프로그램 타이틀
                    if (!foc) {
                        g.setColor(naShow ? GraphicsResource.C_118_118_118 : GraphicsResource.C_255_255_255);
                    }
                    t = TextUtil.shorten(ParentalControl.getTitle(conflicts[i+listOffset]), g.getFontMetrics(), 170);
                    g.drawString(t, modeOthershow ? 276+xShift : 298+xShift, 197 + i * 34);

                    if (naShow) { // DDC 34
                        // N/A
                        if (!foc) {
                            g.setColor(GraphicsResource.C_118_118_118);
                        }
                        g.drawString("N/A", 488+xShift, 197 + i * 34);
                    } else {
                        // 시간
                        g.setFont(GraphicsResource.BLENDER15);
                        if (!foc) {
                            g.setColor(GraphicsResource.C_240_240_240);
                        }
                        GraphicUtil.drawStringRight(g, timeStr[i+listOffset], graphX[i+listOffset][1], 190 + i * 34, 485+xShift, g.getFontMetrics());

                        // 프로그레스 바
                        g.setColor(GraphicsResource.C_37_37_37); // shadow
                        g.fillRect(graphX[i+listOffset][0]+1+xShift, 194 + i * 34, graphX[i+listOffset][2], 9);

                        g.drawImage(checked[i+listOffset] ? (hasConflict ? i_07_bar_rec01 : i_07_bar_rec04) : i_07_bar_rec02, graphX[i+listOffset][0]+xShift, 193 + i * 34, graphX[i+listOffset][2], 9, c);
                    }
                }

                // list shadow, arrow
                if (listOffset > 0) {
                    g.drawImage(i_02_ars_t, 464, 164, c);
                    g.drawImage(i_07_res_pgsh_t, 222, 173, c);
                }
                if (conflicts != null && listOffset + 5 < conflicts.length) {
                    g.drawImage(i_02_ars_b, 464, 337, c);
                    g.drawImage(i_07_res_pgsh_b, 222, 310, c);
                }

                if (!modeOthershow) {
                    // desc
                    g.setFont(GraphicsResource.BLENDER17);
                    g.setColor(GraphicsResource.C_182_182_182);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc2"), 480, 373);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictPickDesc3"), 480, 391);

                    // buttons
                    g.setFont(GraphicsResource.BLENDER18);
                    g.setColor(hasConflict() ? GraphicsResource.C_91_91_91 : GraphicsResource.C_3_3_3);
                    g.drawImage(btnFocus == 0 ? i_05_focus : i_05_focus_dim, 317+83, 415, c);
                    GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.OK"), 395+83, 436);
                }

                if (modeOthershow) {
                    g.translate(0, -38);
                }
            }
        }

        public void prepare(UIComponent c) {
        	super.prepare(c);
        	i_05_focus = dataCenter.getImage("05_focus.png");
            i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
            check = dataCenter.getImage("check.png");
            check_foc = dataCenter.getImage("check_foc.png");
            check_box = dataCenter.getImage("check_box.png");
            check_box_foc = dataCenter.getImage("check_box_foc.png");
            i_pglow610_t = dataCenter.getImage("pglow610_t.png");
            i_pglow610_m = dataCenter.getImage("pglow610_m.png");
            i_pglow610_b = dataCenter.getImage("pglow610_b.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

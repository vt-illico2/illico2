package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.RecordManager;
import com.alticast.illico.pvr.RecordingListManager;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.RemoteIterator;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ConflictCautionPopup extends ConflictPopup {
    OwnRenderer renderer = new OwnRenderer();

    private int listOffset, listFocus;
    private int btnFocus; // btnFocus == -1, focus on list
    private int caller;
    private boolean otherShowtimes;
    private HashMap otherShowtimesMap = new HashMap();

    String[] desc;
    String[] btnStr;

    PvrFooter footer;

    public ConflictCautionPopup(DualTunerConflictResolveSession session) {
        super(session);
        setRenderer(renderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
        footer = new PvrFooter();
        footer.setBounds(344, 488, 400, 30);
        add(footer);
    }

    public void init() {

    }

    public void setTargetRecording(RecordingRequest recording) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.setTargetRecording("+recording+")");
    	super.setTargetRecording(recording);
        desc = new String[] {
//            dataCenter.getString(recording == null ? "pvr.conflictDesc0" : "pvr.conflictDesc1"),
            dataCenter.getString("pvr.conflictDesc0"),
            dataCenter.getString("pvr.conflictDesc2"),
            dataCenter.getString("pvr.conflictDesc3"),
        };
    }

    public void setConflictRecording(RecordingRequest[] recordings) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.setConflictRecording("+recordings+")");
        super.setConflictRecording(recordings);

    	otherShowtimes = false;
        try { // check other show times
            long cur = System.currentTimeMillis();
            for (int i = 0; i < conflicts.length; i++) {
                if (RecordingListManager.isInProgress(conflicts[i])) {
                    continue; // except in progress
                }
                TvProgram[] p = (TvProgram[]) otherShowtimesMap.get(conflicts[i]);

//	    			RemoteIterator ri = (RemoteIterator) otherShowtimesMap.get(conflicts[i]);
                if (p == null) {
                    p = ConflictResolveSession.getOtherShowtimes(conflicts[i], cur);
//		        		ri = svc.findProgramsById(callLetter, pid, cur);
                }
                if (p.length > 0) {
                    otherShowtimes = true;
                    //otherShowtimesMap.put(conflicts[i], p);
                }
                otherShowtimesMap.put(conflicts[i], p); // DDC 34
            }
        } catch (Exception e) {
            Log.printWarning(e);
        }
        if (otherShowtimes) {
            btnStr = new String[3];
            btnStr[0] = dataCenter.getString("pvr.conflictBtn1");
            btnStr[1] = dataCenter.getString("pvr.conflictBtn2");
            btnStr[2] = dataCenter.getString("pvr.conflictBtn3");
        } else {
            btnStr = new String[2];
            btnStr[0] = dataCenter.getString("pvr.conflictBtn1");
            btnStr[1] = dataCenter.getString("pvr.conflictBtn3");
        }
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.start()");
        footer.reset();
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        if (conflicts.length > 5) {
            footer.addButton(PreferenceService.BTN_PAGE, "pvr.page_up_down");
        }
    	listOffset = listFocus = 0;
    	btnFocus = 0;
        super.start();
        this.setVisible(true);
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.stop()");
        super.stop();
        this.setVisible(false);
    }

    private void keyEnter() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.keyEnter()");
    	if (btnFocus < 0) { // list focused
    		return;
    	}
        startClickingEffect();
        int index = btnFocus;
        close();
        if (index == 0) {
            // select shows to record
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.keyEnter() : Call session.showPickupPopup()");
    		session.showPickupPopup();
        } else {
            if (!otherShowtimes) {
                index++;
            }
            if (index == 1) {
    	        // see other show
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.keyEnter() : Call session.showOtherTimesPopup()");
        		session.showOtherTimesPopup(otherShowtimesMap);
            } else if (index == 2) {
                // Resolve later
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.keyEnter() : Call session.showResolvedPopup()");
                session.showResolvedPopup(false);
            }
        }
    }

    public boolean handleKey(int type){
        switch (type) {
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            return true;
        case KeyEvent.VK_UP:
        	if (btnFocus > 0) { // btn
        		btnFocus--;
        	}
            break;
        case KeyEvent.VK_DOWN:
            btnFocus = Math.min(btnFocus + 1, btnStr.length - 1);
            break;
        case KeyEvent.VK_PAGE_UP:
            if (listOffset > 0) {
                listOffset = Math.max(0, listOffset - 5);
            }
            break;
        case KeyEvent.VK_PAGE_DOWN:
            if (listOffset + 5 < conflicts.length) {
                listOffset = listOffset + 5;
            }
            break;
        case KeyEvent.VK_ENTER:
            keyEnter();
            break;
        case OCRcEvent.VK_LAST:
            return true;
        case OCRcEvent.VK_EXIT:
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictCautionPopup.handleKey() : EXIT : Call session.showResolvedPopup(false)");
            session.showResolvedPopup(false);
    		break;
        case OCRcEvent.VK_DISPLAY_SWAP:
            if (Log.EXTRA_ON) {
                IssueManager.getInstance().stopSession(session);
            }
    		break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(333+1, 365 + btnFocus * 37, 293, 32);
    }

    private class OwnRenderer extends CommonRenderer {

        protected void paint(Graphics g, UIComponent c) {
        	g.setColor(GraphicsResource.cDimmedBG);
        	g.fillRect(0, 0, 960, 540);

            g.drawImage(i_12_ppop_sha, 204, 479, 550, 57, c);

        	g.drawImage(i_07_respop_glow, 108, 0, c);

            g.setColor(GraphicsResource.C_33_33_33);
            //->Kenneth[2015.5.24] R5
            g.fillRect(144, 59, 670, 423);
            //g.fillRect(204, 59, 550, 423);

            g.drawImage(i_07_respop_title, 203, 97, c);
            g.drawImage(i_07_respop_high, 204, 59, c);

//        	g.drawImage(i_07_respop_sh, 202, 499, c);

            g.translate(0, 13);

            // title
            int w = i_icon_noti_red.getWidth(c);
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            int x = GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.conflictTitle"), 483 + w/2, 73);
            g.drawImage(i_icon_noti_red, x - w - 5, 54, c);

            // desc
            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_255_255_255);
            for(int i = 0; desc != null && i < desc.length; i++){
                GraphicUtil.drawStringCenter(g, desc[i], 480, 114 + i * 17);
            }

            // list bg
            //->Kenneth[2015.5.24] R5
            g.drawImage(i_07_res_pgbg_t_3, 161, 165, c);
            g.drawImage(i_07_res_pgbg_m_3, 161, 175, 636, 154, c);
            g.drawImage(i_07_res_pgbg_b_3, 161, 329, c);
            g.setColor(GraphicsResource.C_41_41_41);
            g.fillRect(162, 201, 634, 34);
            g.fillRect(162, 269, 634, 34);
            /*
            g.drawImage(i_07_res_pgbg_t, 221, 165, c);
            g.drawImage(i_07_res_pgbg_m, 221, 175, 516, 154, c);
            g.drawImage(i_07_res_pgbg_b, 221, 329, c);
            g.setColor(GraphicsResource.C_41_41_41);
            g.fillRect(222, 201, 514, 34);
            g.fillRect(222, 269, 514, 34);
            */

            // list
            int xShift = 60;
            for (int i = 0; i < 5 && listOffset + i < conflicts.length; i++) {
                //->Kenneth[2015.5.24] R5
                boolean isTarget = i+listOffset == 0 && target != null;

                // 채널 넘버
                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_200_200_200);
            	String t = AppData.getString(conflicts[i+listOffset], AppData.CHANNEL_NUMBER);
            	g.drawString(t, 233-xShift, 190 + i * 34);

                // 채널 로고 (R5 에서 새롭게 추가) : 매번 object 를 생성하는 게 그렇지만 귀찮아서 그냥 씀.
                ConflictRenderingItem cri = new ConflictRenderingItem(conflicts[i+listOffset]);
                if (cri.logo != null) {
                    g.drawImage(cri.logo, 215, 168+i*34, c);
                }

                // 프로그램 타이틀
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_255_255_255);
            	t = TextUtil.shorten(ParentalControl.getTitle(conflicts[i+listOffset]), g.getFontMetrics(), 195);
            	g.drawString(t, 275+xShift, 190 + i * 34);

                // 가운데 구분 바
                g.drawImage(i_07_res_line, 475+xShift, 177 + i * 34, c);

                // 시간
            	g.setFont(GraphicsResource.BLENDER15);
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_240_240_240);
            	GraphicUtil.drawStringRight(g, timeStr[i+listOffset], graphX[i+listOffset][1], 183 + i * 34, 485+xShift, g.getFontMetrics());

                // 프로그래스 바
            	g.setColor(GraphicsResource.C_37_37_37); // shadow
            	g.fillRect(graphX[i+listOffset][0]+1+xShift, 187 + i * 34, graphX[i+listOffset][2], 9);
            	g.drawImage(i_07_bar_rec04, graphX[i+listOffset][0]+xShift, 186 + i * 34, graphX[i+listOffset][2], 9, c);
            	Rectangle ori = g.getClipBounds();
            	x = graphX[i+listOffset][3];
            	w = graphX[i+listOffset][5];

            	g.clipRect(x+xShift, 186 + i * 34, w, 9);
            	g.drawImage(i_07_bar_rec03, x+xShift, 186 + i * 34, c);
            	int imgW = i_07_bar_rec03.getWidth(c);
            	if (w > imgW) {
            		x += (imgW - 1);
            		g.drawImage(i_07_bar_rec03, x+xShift, 186 + i * 34, c);
            	}
            	if (w > imgW + imgW - 1) {
            		x += (imgW - 1);
            		g.drawImage(i_07_bar_rec03, x+xShift, 186 + i * 34, c);
            	}
            	g.setClip(ori);
                /*
                boolean isTarget = i+listOffset == 0 && target != null;
                g.drawImage(i_07_res_line, 475, 177 + i * 34, c);

                g.setFont(GraphicsResource.BLENDER18);
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_200_200_200);

            	String t = AppData.getString(conflicts[i+listOffset], AppData.CHANNEL_NUMBER);
            	g.drawString(t, 233, 190 + i * 34);
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_255_255_255);

            	t = TextUtil.shorten(ParentalControl.getTitle(conflicts[i+listOffset]), g.getFontMetrics(), 195);
            	g.drawString(t, 275, 190 + i * 34);

            	g.setFont(GraphicsResource.BLENDER15);
                g.setColor(isTarget ? GraphicsResource.C_255_203_0 : GraphicsResource.C_240_240_240);
            	GraphicUtil.drawStringRight(g, timeStr[i+listOffset], graphX[i+listOffset][1], 183 + i * 34, 485, g.getFontMetrics());

            	g.setColor(GraphicsResource.C_37_37_37); // shadow
            	g.fillRect(graphX[i+listOffset][0]+1, 187 + i * 34, graphX[i+listOffset][2], 9);
            	g.drawImage(i_07_bar_rec04, graphX[i+listOffset][0], 186 + i * 34, graphX[i+listOffset][2], 9, c);
            	Rectangle ori = g.getClipBounds();
            	x = graphX[i+listOffset][3];
            	w = graphX[i+listOffset][5];

            	g.clipRect(x, 186 + i * 34, w, 9);
            	g.drawImage(i_07_bar_rec03, x, 186 + i * 34, c);
            	int imgW = i_07_bar_rec03.getWidth(c);
            	if (w > imgW) {
            		x += (imgW - 1);
            		g.drawImage(i_07_bar_rec03, x, 186 + i * 34, c);
            	}
            	if (w > imgW + imgW - 1) {
            		x += (imgW - 1);
            		g.drawImage(i_07_bar_rec03, x, 186 + i * 34, c);
            	}
            	g.setClip(ori);
                */
            }

            g.translate(0, -13);

            // scroll bar : R5 : xShift 만큼 우측 이동
            if (conflicts.length > 5) {
                g.drawImage(i_scrbg_conf, 722+xShift, 186, c);
                int totalPage = ((conflicts.length - 1) / 5) + 1;
                int currentPage = listOffset / 5;
                int y = 186 + ((157 + 2 - totalPage * 28) / (totalPage - 1) + 28) * currentPage;
                g.drawImage(i_scr_bar, 719+xShift, y, c);

                if (currentPage > 0) {
                	g.drawImage(i_07_res_pgsh_t, 222+xShift, 166+13, c);
                }
                if (currentPage < totalPage - 1) {
                	g.drawImage(i_07_res_pgsh_b, 222+xShift, 303+13, c);
                }
            }

            // buttons
            g.setFont(GraphicsResource.BLENDER18);
            for (int i = 0; i < btnStr.length; i++) {
                String btn = TextUtil.shorten(btnStr[i], g.getFontMetrics(), 280);
                if (i == btnFocus) {
                    g.drawImage(i_btn_293_foc, 333, 365 + i * 37, c);
                    GraphicUtil.drawStringCenter(g, btn, 480, 387 + i * 37);
//                } else if (i == 1 && otherShowtimes == false) {
//                    g.drawImage(i_btn_293_in, 334, 365 + i * 37, c);
//                    g.setColor(GraphicsResource.C_111_111_111);
//                    GraphicUtil.drawStringCenter(g, btn, 480, 387 + i * 37);
//                    g.setColor(GraphicsResource.C_50_50_50);
//                    GraphicUtil.drawStringCenter(g, btn, 479, 386 + i * 37);
                } else {
                	g.setColor(GraphicsResource.C_3_3_3);
                    g.drawImage(i_btn_293, 334, 365 + i * 37, c);
                    GraphicUtil.drawStringCenter(g, btn, 480, 387 + i * 37);
                }
            }
        }

        public void prepare(UIComponent c) {
        	super.prepare(c);
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}



package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.*;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.gui.ScaleUPList;
import com.alticast.illico.pvr.gui.TimeList;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.comp.*;
import com.alticast.illico.pvr.config.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.data.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;

public class GeneralRecordingPopup extends Popup implements PopupListener, ChannelEventListener, OptionSelectionListener {

    TimeEditField startField;
    TimeEditField endField;
    OptionEditField repeatField;
    OptionEditField saveField;
    OptionEditField formatField;
    EditField[] fields;

    OptionEditField freqField;

    protected PopupRenderer popupRenderer = new PopupRenderer();

    private static final long ONEHOUR = Constants.MS_PER_HOUR;
    private static final long ONEDAY = Constants.MS_PER_DAY;

    private static final Character RECORD_LOCK = new Character('j');

//    protected TvChannel ch;
    protected TvChannel sisterCh;
    protected TvProgram sisterProgram;
    protected String title = "";
    protected String programStartTime = "";
    protected long startTime = 0;
    protected long endTime = 0;
    protected int dayOfWeek;
    protected boolean isHd = false;
    //->Kenneth[2015.2.16] 4k
    protected boolean isUhd = false;
    protected int sisterChannelNumber = 0;
    protected String channelName = "";
    protected int channelNumber = 0;
    //->Kenneth[2015.4.24] R5
    protected String fullName = "";

    String strSave = "";
    String strCancel = "";
    String strCheckBox = "";

    private String start;
    private String end;
    private String repeat;
    private String save;
    private String frequency;
    private String hdavailable;

    private String[] strWeek;

    protected String[] buttons = new String[0];

    protected RecordingOption option;

    protected PvrFooter footer = new PvrFooter();
    protected OptionPopup optionPopup;

    protected InfoOneButtonPopup timeErrorPopup;

    protected int optionType;

    Color cTitle = new Color(255, 203, 0);
    Font fTitle = GraphicsResource.BLENDER24;
    FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

    Color cTime = new Color(224, 224, 224);
    Font fTime = GraphicsResource.BLENDER19;

    Color cHDMsg = new Color(200, 200, 200);
    Font fHDMsg = GraphicsResource.BLENDER18;

    Color cChannel = new Color(255, 255, 255);
    Font fChannel = GraphicsResource.BLENDER19;

    protected int focusIndex;

    private int sisterChNum;
    private boolean sisterChCheck;

    private boolean needCheckHd = false;

    private boolean isBlocked = false;
    String strRecOption = "";

    //->Kenneth[2015.2.16] 4K flat design tuning
    private Color footerLineColor = new Color(50, 50, 50);

    //->Kenneth[2015.4.24] R5
    Font f22 = GraphicsResource.BLENDER22;
    Font f23 = GraphicsResource.BLENDER23;
    FontMetrics fm22 = FontResource.getFontMetrics(f22);
    FontMetrics fm23 = FontResource.getFontMetrics(f23);
    private boolean sdSelected = false;

    public GeneralRecordingPopup(int type) {
        this.optionType = type;
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup: optionType = " + optionType);
        sisterChCheck = false;
        setRenderer(popupRenderer);
        this.setBounds(0, 0, 960, 540);
        footer.setBounds(469, 198 + 290, 434, 25);
        footer.addButton(PreferenceService.BTN_B, "pvr.rec_opt");
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        this.add(footer);
        setListener(this);

        optionPopup = new OptionPopup();
        this.add(optionPopup, 0);
        optionPopup.setVisible(false);
        strRecOption = dataCenter.getString("pvr.RecOpt");

        timeErrorPopup = new InfoOneButtonPopup(dataCenter.getString("pvr.time_error_popup_title"));
        timeErrorPopup.fillBg = true;
        timeErrorPopup.setVisible(false);
        this.add(timeErrorPopup, 0);

        timeErrorPopup.setListener(new PopupAdapter() {
            public void selected(int focus) {
                optionPopup.resetTimes();
            }
        });
        switch (type) {
            case RecordingOption.TYPE_REPEAT:
                buttons = new String[] {"pvr.rec_once", "pvr.rec_all"};
                break;
            case RecordingOption.TYPE_SERIES:
                buttons = new String[] {"pvr.rec_this_ep", "pvr.rec_all_ep"};
                break;
            case RecordingOption.TYPE_SINGLE:
                buttons = new String[] {"pvr.Record"};
                break;
        }

        long autoCloseDelay = 0;
        PvrConfig config = (PvrConfig) dataCenter.get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config != null) {
            autoCloseDelay = config.ccNoticeTimeout * Constants.MS_PER_SECOND;
        }
        setDefaultAutoClose();
    }

    // OptionSelectionListener
    public void selectionChanged(Object key) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selectionChanged("+key+")");
        boolean repeat = !RepeatEditField.KEY_NO.equals(key);
        resetSaveOption(repeat);
    }

    //->Kenneth[2015.4.25] R5
    class DefinitionOptionSelectionListenerImpl implements OptionSelectionListener {
        public void selectionChanged(Object key) {
            Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.DefinitionOptionSelectionListenerImpl.selectionChanged("+key+")");
            if ("pvr.sd".equals(key)) {
                sdSelected = true;
            } else if ("pvr.hd".equals(key)) {
                sdSelected = false;
            }
        }
    }

    private void resetSaveOption(boolean repeat) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.resetSaveOption("+repeat+")");
        option.all = repeat;
        saveField.setOptions(repeat ? PvrUtil.SAVE_SERIES : PvrUtil.SAVE_RECORDING);
        if (repeat) {
            int count = PvrUtil.getDefaultKeepCount();
            if (count == 3) {
                saveField.setSelectedKey(PvrUtil.LAST_3);
            } else if (count == 5) {
                saveField.setSelectedKey(PvrUtil.LAST_5);
            } else {
                saveField.setSelectedKey(PvrUtil.LAST_MAX);
            }
        } else {
            long exp = PvrUtil.getDefaultExpirationPeriod();
            if (exp == 7 * Constants.MS_PER_DAY) {
                saveField.setSelectedKey(PvrUtil.SAVE_7);
            } else if (exp == 14 * Constants.MS_PER_DAY) {
                saveField.setSelectedKey(PvrUtil.SAVE_14);
            } else {
                saveField.setSelectedKey(PvrUtil.SAVE_MAX);
            }
        }
    }

    private void showTimeErrorPopup(long from, long to) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.showTimeErrorPopup()");
        Formatter fm = Formatter.getCurrent();
        String s1 = TextUtil.shorten(title, GraphicsResource.FM18, 150);
        String s2 = fm.getTime(option.programStartTime) + " - " + fm.getTime(option.programEndTime);
        String s3 = fm.getTime(from) + " - " + fm.getTime(to);
        String desc = TextUtil.replace(dataCenter.getString("pvr.time_error_popup_msg"), "|", "\n");
        desc = TextUtil.replace(desc, "%1", s1);
        desc = TextUtil.replace(desc, "%2", s2);
        desc = TextUtil.replace(desc, "%3", s3);
        timeErrorPopup.setDescription(desc);

        timeErrorPopup.prepare();
        timeErrorPopup.setVisible(true);
    }

    public void start(TvChannel ch, TvProgram p, boolean tooManyEpisodes) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start("+ch+")");
        EpgService es = (EpgService) dataCenter.get(EpgService.IXC_NAME);
        sisterCh = null;
        optionPopup.setVisible(false);

        footer.setVisible(true);
        option = new RecordingOption(ch, p);
        option.creationMethod = AppData.BY_USER;
        option.type = optionType;

        boolean hour24 = Constants.LANGUAGE_FRENCH.equals(FrameworkMain.getInstance().getCurrentLanguage());
        startField = new TimeEditField(dataCenter.getString("pvr.Start"), true, hour24);
        endField = new TimeEditField(dataCenter.getString("pvr.End"), true, hour24);
        repeatField = new OptionEditField(dataCenter.getString("pvr.Repeat"));
        repeatField.setListener(this);
        saveField = new OptionEditField(dataCenter.getString("pvr.Save"));

        repeatField.setOptions(new String[] { "pvr.No", "pvr.Yes"} );
        repeatField.setSelectedKey(tooManyEpisodes ? "pvr.Yes" : "pvr.No");

        // R5
        formatField = new OptionEditField(dataCenter.getString("pvr.format"));
        formatField.setListener(new DefinitionOptionSelectionListenerImpl());

        formatField.setOptions(new String[] { "pvr.sd", "pvr.hd"} );
        try {
            if (ch != null && ch.getDefinition() == ch.DEFINITION_HD) {
                formatField.setSelectedKey("pvr.hd");
                sdSelected = false;
            } else {
                formatField.setSelectedKey("pvr.sd");
                sdSelected = true;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }

        startField.setTime(option.startTime);
        endField.setTime(option.endTime);

        freqField = new OptionEditField(dataCenter.getString("manual.Frequency"), 80);

        strWeek = new String[] {dataCenter.getString("pvr.Sun."), dataCenter.getString("pvr.Mon."),
                dataCenter.getString("pvr.Tue."), dataCenter.getString("pvr.Wed."), dataCenter.getString("pvr.Thu."),
                dataCenter.getString("pvr.Fri."), dataCenter.getString("pvr.Sat."),
                dataCenter.getString("pvr.More Days")};

        Calendar c1 = Calendar.getInstance();
        c1.setTimeInMillis(option.programStartTime);
        dayOfWeek = c1.get(Calendar.DAY_OF_WEEK) - 1;
        String ss = Formatter.getCurrent().getTime(option.programStartTime);

        dataCenter.put("freq_every", dataCenter.getString("manual.Every") + " " + strWeek[dayOfWeek] + " " + dataCenter.getString("manual.at") + " " + ss);
        dataCenter.put("freq_any", dataCenter.getString("manual.Any day at") + " " + ss);
        freqField.setOptions(new String[] { "pvr.All", "freq_every", "freq_any" });
        freqField.setSelectedKey("pvr.All");

        try {
            isBlocked = es.isAdultBlocked(p);
        } catch (Exception ex) {
            isBlocked = false;
        }
        title = isBlocked ? dataCenter.getString("List.blocked") : option.title;
        // title = strRecOption + " " + title;
        startTime = option.startTime;
        endTime = option.endTime;

        long pst = startTime;
        try {
            boolean hdChannel = ch.isHd();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start() : hdChannel? = "+hdChannel);
            boolean hdProgram = p.isHd();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start() : hdProgram? = "+hdProgram);
            //->Kenneth[2015.7.8] VDTRMASTER-5474 : SD/HD 구분은 채널 정보만으로 하자.
            isHd = hdChannel;
            //isHd = hdProgram && hdChannel;
            //<-
            //->Kenneth[2015.2.16] 4k
            if (ch.getDefinition() == TvChannel.DEFINITION_4K) {
                isUhd = true;
            }
            channelName = ch.getCallLetter();
            channelNumber = ch.getNumber();
            fullName = ch.getFullName();
            sisterCh = ch.getSisterChannel();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start() : sisterCh = "+sisterCh);
            if (sisterCh != null && !sisterCh.isSubscribed()) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start() : sisterCh is unsubscribed, so sisterCh = null");
                sisterCh = null;
            }

            //->Kenneth[2016.3.8] R7 : Ungrouped 에서는 sister 체크하지 않는다.
            if (!Core.isChannelGrouped) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.start() : isChannelGrouped = null, so sisterCh = null");
                sisterCh = null;
            }
            //<-

            pst = p.getStartTime();
            if (sisterCh != null) {
                sisterChNum = sisterCh.getNumber();
                sisterProgram = es.getProgram(sisterCh, pst);
                if (sisterProgram == null || sisterProgram.getStartTime() != pst
                            || !option.title.equals(sisterProgram.getTitle()) ) {
                            //|| !option.title.equals(sisterProgram.getTitle())
                            //|| RecordManager.getInstance().getRequest(sisterProgram) != null) {
                    Log.printInfo(App.LOG_HEADER+"GeneralRecordingPopup: sister progam is null or recording");
                    sisterCh = null;
                    sisterProgram = null;
                }
            }

            sisterChCheck = false;
            needCheckHd = hdProgram && option.startTime > System.currentTimeMillis() && !hdChannel && sisterCh != null;

            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"isUhd = " + isUhd);
                Log.printDebug(App.LOG_HEADER+"ch.isHd() = " + hdChannel);
                Log.printDebug(App.LOG_HEADER+"sisterCh = " + sisterCh);
                Log.printDebug(App.LOG_HEADER+"sisterProgram = " + sisterProgram);
                Log.printDebug(App.LOG_HEADER+"option.startTime = " + (option.startTime > System.currentTimeMillis()));
                Log.printDebug(App.LOG_HEADER+"needCheckHd = " + needCheckHd);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }

        if (optionType == RecordingOption.TYPE_SINGLE) {
            if (sisterCh == null && sisterProgram == null) {
                fields = new EditField[] { startField, endField, saveField };
            } else {
                fields = new EditField[] { startField, endField, saveField, formatField };
            }
        } else {
            if (sisterCh == null && sisterProgram == null) {
                fields = new EditField[] { startField, endField, repeatField, saveField };
            } else {
                fields = new EditField[] { startField, endField, repeatField, saveField, formatField };
            }
        }

        programStartTime = Formatter.getCurrent().getLongDate(pst);

        prepare();
        focus = 0;

        start = dataCenter.getString("pvr.Start");
        end = dataCenter.getString("pvr.End");
        repeat = dataCenter.getString("manual.Repeat");
        save = dataCenter.getString("pvr.Save");
        frequency = dataCenter.getString("manual.Frequency");
        hdavailable = dataCenter.getString("pvr.hdavailable");

        strSave = dataCenter.getString("manual.record");
        strCancel = dataCenter.getString("manual.clear");
        strCheckBox = dataCenter.getString("pvr.UseHD");

        optionPopup.prepare();
        try {
            es.getChannelContext(0).addChannelEventListener(this);
            es.getChannelContext(1).addChannelEventListener(this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        optionPopup.init(tooManyEpisodes);
        if (tooManyEpisodes) {
            optionPopup.setVisible(true);
            footer.setVisible(false);
        }
        startTimer();
    }

    public void close() {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.close");
        }

        EpgService es = (EpgService) dataCenter.get(EpgService.IXC_NAME);
        try {
            es.getChannelContext(0).removeChannelEventListener(this);
            es.getChannelContext(1).removeChannelEventListener(this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        optionPopup.setVisible(false);
        strWeek = null;
        // setVisible(false);
        super.close();
    }

    /** ChannelEventListener implemetation */
    public void selectionRequested(int id, short type) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selectionRequested() : Call close()");
        close();
    }

    public void selectionBlocked(int sid, short type) {
        Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selectionBlocked() : Call close()");
        close();
    }

    protected class PopupRenderer extends Renderer {

        //Image i_07_pop_sha = dataCenter.getImage("07_pop_sha.png");
        Image i_large_noti_b = dataCenter.getImage("large_noti_b.png");
        Image i_large_noti_m = dataCenter.getImage("large_noti_m.png");
        Image i_large_noti_t = dataCenter.getImage("large_noti_t.png");
        Image i_01_hotkeybg = dataCenter.getImage("01_hotkeybg.png");
        Image iHd = dataCenter.getImage("icon_hd.png");
        //->Kenneth[2015.2.16] 4k
        Image iUhd = dataCenter.getImage("icon_uhd.png");

        Image i_btn_210_foc = dataCenter.getImage("btn_210_foc.png");
        Image i_btn_210_l = dataCenter.getImage("btn_210_l.png");

        Color cChannel = new Color(255, 255, 255);
        Color cButton = Color.black;
        Font fButton = GraphicsResource.BLENDER18;
        Color cTime = new Color(180, 180, 180);

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (optionPopup.isVisible()) {
                return;
            }
            // BG
            g.drawImage(i_large_noti_t, 0, 250, 960, 70, c);
            g.drawImage(i_large_noti_m, 0, 320, 960, 90, c);
            g.drawImage(i_large_noti_b, 0, 410, 960, 130, c);

            // Channel Number
            g.setColor(cChannel);
            g.setFont(f23);
            g.drawString(channelNumber+"", 79, 336);

            // HD/UDH
            if (isUhd) {
                g.drawImage(iUhd, 120+10, 323, c);
            } else if (isHd) {
                g.drawImage(iHd, 120+10, 323, c);
            }

            // channel logo
            Image logo = (Image) SharedMemory.getInstance().get("logo." + channelName);
            if (logo == null) {
                logo = (Image) SharedMemory.getInstance().get("logo.default");
            }
            g.drawImage(logo, 153+10, 314, c);

            // Full Name
            if (fullName != null) {
                g.setFont(f22);
                g.setColor(cChannel);
                GraphicUtil.drawStringRight(g, fullName, 910, 335);
            }

            // program title
            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(title, 79, 369);

            // start time
            g.setColor(cTime);
            g.setFont(fButton);
            g.drawString(programStartTime, 79, 393);


            // Button
            g.setColor(cButton);
            g.setFont(fButton);
            if (buttons.length == 1) {
                g.drawImage(i_btn_210_foc, 370, 128 + 300, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[0]), 475, 149 + 300);
            } else if (buttons.length == 2) {
                if (focus == 0) {
                    g.drawImage(i_btn_210_foc, 265, 128 + 300, c);
                    g.drawImage(i_btn_210_l, 485, 128 + 300, c);
                } else {
                    g.drawImage(i_btn_210_l, 266, 128 + 300, c);
                    g.drawImage(i_btn_210_foc, 484, 128 + 300, c);
                }
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[0]), 369, 149 + 300);
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[1]), 589, 149 + 300);
            }
            /*
            g.drawImage(i_07_pop_sha, 0, 79 + 290, 960, 171, c);
            g.drawImage(i_large_noti_b, 0, 120 + 290, c);
            g.drawImage(i_large_noti_m, 0, 70 + 290, 960, 50, c);
            g.drawImage(i_large_noti_t, 0, 0 + 290, c);

            //->Kenneth[2015.2.16] 4k
            g.setColor(footerLineColor);
            g.fillRect(0, 483, 960, 1);
            //g.drawImage(i_01_hotkeybg, 236, 176 + 290, c);

            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(title, 79, 85 + 290);
            int x = fmTitle.stringWidth(title) + 9 + 79;
            //->Kenneth[2015.2.16] 4k
            if (isUhd) {
                g.drawImage(iUhd, x, 72 + 290, c);
            } else if (isHd) {
                g.drawImage(iHd, x, 72 + 290, c);
            }

            if (needCheckHd) {
                g.setColor(cHDMsg);
                g.setFont(fHDMsg);
                GraphicUtil.drawStringRight(g, hdavailable + " " + sisterChNum, 909, 396);
            }
            g.setColor(cChannel);
            g.setFont(fChannel);
            g.drawString(programStartTime, 79, 109 + 290);
            GraphicUtil.drawStringRight(g, channelNumber + "  " + channelName, 910, 85 + 290);

            g.setColor(cButton);
            g.setFont(fButton);

            if (buttons.length == 1) {
                g.drawImage(i_btn_210_foc, 370, 128 + 290, c);
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[0]), 475, 149 + 290);
            } else if (buttons.length == 2) {
                if (focus == 0) {
                    g.drawImage(i_btn_210_foc, 265, 128 + 290, c);
                    g.drawImage(i_btn_210_l, 485, 128 + 290, c);
                } else {
                    g.drawImage(i_btn_210_l, 266, 128 + 290, c);
                    g.drawImage(i_btn_210_foc, 484, 128 + 290, c);
                }
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[0]), 369, 149 + 290);
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[1]), 589, 149 + 290);
            }
            */
        }
    }

    private void record() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record()");
        option.startOffset = option.startTime - option.programStartTime;
        option.endOffset = option.endTime - option.programEndTime;
        Thread t = new Thread("record_thread") {
            public void run() {
                synchronized (RECORD_LOCK) {
                    //->Kenneth[2015.4.24] R5 : 사용자가 명시적으로 SD 를 recording 하겠다고 format 을 선택한 경우
                    //->Kenneth[2015.5.25] R5 : HD 채널에서 SD 뿐 아니라 SD 채널에서 HD 도 선택할수 있어야 함. 
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : sdSelected = "+sdSelected);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : sisterCh = "+sisterCh);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : sisterProgram = "+sisterProgram);
                    //if (sdSelected && sisterCh != null && sisterProgram != null) {
                    if (sisterCh != null && sisterProgram != null) {
                        try {
                            int sisterDef = sisterCh.getDefinition();
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : sisterCh.getDefinition() = "+sisterDef);
                            if ((sisterDef == TvChannel.DEFINITION_SD && sdSelected) || (sisterDef == TvChannel.DEFINITION_HD && !sdSelected)) {
                                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : Replace to Sister Channel");
                                // sister 가 Sd 인데 사용자가 sd 를 선택한 경우 혹은 Sister 가 HD 인데 사용자가 hd 를
                                // 선택한 경우
                                if (RecordManager.getInstance().getSisterProgramRecordingRequest(option.program) != null) {
                                    // sister 프로그램이 이미 스케쥴링 되어 있음.
                                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.record : Show Duplicated Popup");
                                    PopupController.getInstance().showRecordingPopup(option.channel, option.program,
                                            PopupController.POPUP_DUPLICATED_RECORDING);
                                    return;
                                }
                                option.channel = sisterCh;
                                option.program = sisterProgram;
                            }
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }

                    RecordManager rm = RecordManager.getInstance();
                    if (option.program != null) {
                        // 중복 방지
                        if (rm.getRequest(option.program) != null) {
                            Log.printWarning(App.LOG_HEADER+"GeneralRecordingPopup.record: already recorded !!");
                            return;
                        }
                    }
                    RecordingRequest req = rm.record(option);
                    rm.checkDiskSpace(req);
                    PvrVbmController.getInstance().writeUpcomingRecording(req, option);
                }
            }
        };
        t.start();
    }

    public void canceled() {
    }

    // added the save time. @Pellos
    public void selected(int focus) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selected("+focus+")");
        // enter key
        // option.all = (focus == 1);

        if (focus == 0) {
            String value = dataCenter.getString(PreferenceNames.DEFAULT_SAVE_TIME);
            Calendar cur = Calendar.getInstance();
            int time = cur.get(Calendar.HOUR_OF_DAY);

            if (value == null || value.length() == 0) {
                option.saveUntil = Long.MAX_VALUE;
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_UNTIL_I_ERASE)) {
                option.saveUntil = Long.MAX_VALUE;
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_AFTER_7_DAYS)) {
                option.saveUntil = 7 * ONEDAY;
            } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_AFTER_14_DAYS)) {
                option.saveUntil = 14 * ONEDAY;
            }
            option.all = false;
        } else {
            option.all = true;
            if (option.type == RecordingOption.TYPE_REPEAT) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selected() : TYPE_REPEAT");
                repeatField.setSelectedKey("pvr.Yes");
                freqField.setSelectedKey("freq_every");
                optionPopup.init(true);
                optionPopup.setVisible(true);
                footer.setVisible(false);
                return;
            } else {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selected() : non TYPE_REPEAT");
                String value = dataCenter.getString(PreferenceNames.DEFAULT_SAVE_TIME_REPEAT);
                Log.printDebug("setDefaultRepeatSaveTime = " + value);
                if (value == null || value.length() == 0) {
                    option.keepOption = 0;
                } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_KEEP_ALL)) {
                    option.keepOption = 0;
                } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_LAST_3)) {
                    option.keepOption = 3;
                } else if (value.equals(Definitions.DEFAULT_SAVE_TIME_LAST_5)) {
                    option.keepOption = 5;
                }
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.selected() : Call record()");
        record();
        close();
    }

    public boolean handleHotKey(int code) {
        return false;
    }

    public boolean handleKey(int code) {
        startTimer();
        if (timeErrorPopup.isVisible()) {
            return timeErrorPopup.handleKey(code);
        }
        if (optionPopup.isVisible()) {
            return optionPopup.handleKey(code);
        }
        switch (code) {
        case KeyCodes.COLOR_B:
            footer.clickAnimationByKeyCode(code);
            optionPopup.setVisible(true);
            footer.setVisible(false);
            break;
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
            if (focus > 0) {
                focus--;
            }
            repaint();
            break;
        case KeyEvent.VK_RIGHT:
            if (focus < buttons.length - 1) {
                focus++;
            }
            repaint();
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.handlKey(VK_ENTER)");
            startClickingEffect();
            if (listener != null) {
                listener.selected(focus);
            }
            break;
        // case KeyCodes.LAST:
        case KeyCodes.RECORD:
        case OCRcEvent.VK_EXIT:
            close();
            break;
        case KeyEvent.VK_PAGE_UP:
        case KeyEvent.VK_PAGE_DOWN:
        case OCRcEvent.VK_GUIDE:
        case KeyCodes.SEARCH:
        case KeyCodes.WIDGET:
            return true;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        Rectangle r = new Rectangle(210, 32);
        //->Kenneth[2016.3.11] 테스트 중에 click animation 위치 이상함 발견. R5 때 생긴
        // 문제인데 발견하지 못한건가? --ㅋ
        // 하여간 yGap 을 통해서 click 위치를 아래로 내림.
        int yGap = 10;
        if (buttons.length == 1) {
            r.setLocation(370 + 1, 128 + 290+yGap);
        } else if (buttons.length == 2) {
            if (focus == 0) {
                r.setLocation(265 + 1, 128 + 290+yGap);
            } else if (focus == 1) {
                r.setLocation(484 + 1, 128 + 290+yGap);
            } else {
                return;
            }
        } else {
            return;
        }
        /*
        if (buttons.length == 1) {
            r.setLocation(370 + 1, 128 + 290);
        } else if (buttons.length == 2) {
            if (focus == 0) {
                r.setLocation(265 + 1, 128 + 290);
            } else if (focus == 1) {
                r.setLocation(484 + 1, 128 + 290);
            } else {
                return;
            }
        } else {
            return;
        }
        */
        //<-
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(r);
    }

    public boolean consumeKey(int code) {
        return false;
    }

    class OptionPopup extends Popup {

        OptionPopupRenderer ren = new OptionPopupRenderer();

        public OptionPopup() {
            setRenderer(ren);
            setFont(GraphicsResource.BLENDER17);
            setForeground(GraphicsResource.C_182_182_182);
            this.setBounds(0, 0, 960, 540);
        }

        public void init(boolean repeat) {
            if (!repeat) {
                focusIndex = -1;
            } else {
                focusIndex = -4;
            }
            resetSaveOption(repeat);
        }

        public void resetTimes() {
            startField.setTime(option.startTime);
            endField.setTime(option.endTime);
        }

        private void recordWithOption() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup.OptionPopup.recordWithOption()");
            if (needCheckHd && sisterChCheck && sisterCh != null) {
                option.channel = sisterCh;
                Log.printDebug(App.LOG_HEADER+"GeneralRecordingPopup: selected sister channel");
            }

            Date startValue = (Date) startField.getChangedValue();
            Date endValue = (Date) endField.getChangedValue();
            long newStart = startValue != null ? PvrUtil.getAdjustedTimeSlot(startValue.getTime(), option.startTime) : option.startTime;
            long newEnd = endValue != null ? PvrUtil.getAdjustedTimeSlot(endValue.getTime(), option.endTime) : option.endTime;

            long dur = newEnd - newStart;

            if (dur <= 0) {
                newEnd += Constants.MS_PER_DAY;
                dur += Constants.MS_PER_DAY;
            } else if (dur > Constants.MS_PER_DAY) {
                newEnd -= Constants.MS_PER_DAY;
                dur -= Constants.MS_PER_DAY;
            }

            if (newStart >= option.programEndTime || newEnd <= option.programStartTime || dur <= 0) {
                showTimeErrorPopup(newStart, newEnd);
                return;
            }
            option.startTime = newStart;
            option.endTime = newEnd;

            String saveValue = (String) saveField.getSelectedValue();

            repeatField.getChangedValue();
            if (!option.all) {
//                option.setRepeatString();
                long exp = Long.MAX_VALUE;;
                if (PvrUtil.SAVE_14.equals(saveValue)) {
                    exp = Constants.MS_PER_DAY * 14;
                } else if (PvrUtil.SAVE_7.equals(saveValue)) {
                    exp = Constants.MS_PER_DAY * 7;
                }
                option.saveUntil = exp;
            } else {
                int keepOption = 0;
                if (PvrUtil.LAST_3.equals(saveValue)) {
                    keepOption = 3;
                } else if (PvrUtil.LAST_5.equals(saveValue)) {
                    keepOption = 5;
                }
                option.keepOption = keepOption;


                String freqValue = (String) freqField.getSelectedValue();
                String repeatString;
                if ("pvr.All".equals(freqValue)) {
                } else if ("freq_every".equals(freqValue)) {
                    // mon - sun
                    int index = (dayOfWeek == 0) ? 6 : dayOfWeek - 1;
                    boolean[] repeatDays = new boolean[] {false, false, false, false, false, false, false};
                    repeatDays[index] = true;
                    option.setRepeat(RecordingOption.REPEAT_DAY, repeatDays);
                } else if ("freq_any".equals(freqValue)) {
                    option.setRepeat(RecordingOption.REPEAT_ANY, null);
                } else {
                    option.setRepeat(RecordingOption.REPEAT_ALL, null);
                }
            }
            record();
            GeneralRecordingPopup.this.close();
        }

        private void keyUp() {
            if (focusIndex == -3) {
                focusIndex = fields.length - 1;
            } else if (focusIndex == -4) {
                focusIndex = 0;
            } else if (focusIndex < 0) {
                if (needCheckHd) {
                    focusIndex = -3;
                } else {
                    focusIndex = fields.length - 1;
                }
            } else if (focusIndex > 1) {
                focusIndex--;
                if (focusIndex == 1) {
                    focusIndex = 0;
                }
                if (fields[focusIndex] instanceof TimeEditField) {
                    fields[focusIndex].resetFocus(0);
                }
            }
        }

        private void keyDown() {
            if (focusIndex < 0) {
                if (focusIndex == -3) {
                    focusIndex = -1;
                } else if (focusIndex == -4) {
                    focusIndex = 3;
                }
            } else if (focusIndex < 2) {
                focusIndex = 2;
            } else {
                focusIndex++;
                if (focusIndex == fields.length) {
                    if (needCheckHd) {
                        focusIndex = -3;
                    } else {
                        focusIndex = -1;
                    }
                }
            }
        }

        public boolean handleKey(int code) {
            if (focusIndex >= 0) {
                boolean ret = fields[focusIndex].handleKey(code);
                if (ret) {
                    repaint();
                    return true;
                }
            } else if (focusIndex == -4) {
                boolean ret = freqField.handleKey(code);
                if (ret) {
                    repaint();
                    return true;
                }
            }
            switch (code) {
                case OCRcEvent.VK_ENTER:
                    if (focusIndex == -3) {
                        sisterChCheck = !sisterChCheck;
                    }
                    startClickingEffect();
                    if (focusIndex == -2) {
                        GeneralRecordingPopup.this.close();
                    } else if (focusIndex == -1) {
                        recordWithOption();
                    }
                    break;
                case OCRcEvent.VK_UP:
                    keyUp();
                    break;
                case OCRcEvent.VK_DOWN:
                    keyDown();
                    break;
                case OCRcEvent.VK_LEFT:
                    if (focusIndex == 1) {
                        focusIndex = 0;
                        fields[0].resetFocus(3);
                    } else if (focusIndex == -2) {
                        focusIndex = -1;
                    } else if (focusIndex == -4) {
                        focusIndex = 2;
                    }
                    break;
                case OCRcEvent.VK_RIGHT:
                    if (focusIndex == 0) {
                        focusIndex = 1;
                        fields[1].resetFocus(0);
                    } else if (focusIndex == -1) {
                        focusIndex = -2;
                    } else if (focusIndex == 2 && option.all) {
                        focusIndex = -4;
                    }
                    break;
                case OCRcEvent.VK_EXIT:
                    GeneralRecordingPopup.this.close();
                    break;
                case KeyEvent.VK_PAGE_UP:
                case KeyEvent.VK_PAGE_DOWN:
                case OCRcEvent.VK_GUIDE:
                case KeyCodes.SEARCH:
                case KeyCodes.WIDGET:
                case KeyCodes.COLOR_B:
                    return true;
//                case OCRcEvent.VK_DISPLAY_SWAP:
//                    if (Log.EXTRA_ON) {
//                        Thread th = new Thread() {
//                            public void run() {
//                                try {
//                                    Date startValue = (Date) startField.getChangedValue();
//                                    Date endValue = (Date) endField.getChangedValue();
//                                    long newStart = startValue != null ? PvrUtil.getAdjustedTimeSlot(startValue.getTime(), option.startTime) : option.startTime;
//                                    long newEnd = endValue != null ? PvrUtil.getAdjustedTimeSlot(endValue.getTime(), option.endTime) : option.endTime;
//
//                                    long start = newStart;
//                                    while (start <= newEnd) {
//                                        RecordingOption ro = new RecordingOption(option.channel, option.program);
//                                        ro.startTime = start;
//                                        ro.endTime = start + 18 * Constants.MS_PER_SECOND;
//                                        ro.title = option.channel.getCallLetter() + ":" + new Date(newStart);
//                                        RecordManager.getInstance().record(ro);
//                                        start += 20 * Constants.MS_PER_SECOND;
//                                    }
//                                } catch (Exception ex) {
//                                    Log.print(ex);
//                                }
//                            }
//                        };
//                        th.start();
//                    }
//                    return true;
                default:
                    return false;
            }
            repaint();
            return true;
        }

        public void startClickingEffect() {
            int yHeight = 0;
            if (needCheckHd) {
                yHeight = 23;
            }
            if (optionType == RecordingOption.TYPE_REPEAT || optionType == RecordingOption.TYPE_SERIES) {
                if (focusIndex == -1) {
                    clickEffect.start(320+1, 446 + yHeight, 154, 32);
                } else if (focusIndex == -2) {
                    clickEffect.start(484+1, 446 + yHeight, 154, 32);
                }
            } else if (optionType == RecordingOption.TYPE_SINGLE) {
                if (focusIndex == -1) {
                    clickEffect.start(320+1, 446 + yHeight, 154, 32);
                } else if (focusIndex == -2) {
                    clickEffect.start(484+1, 446 + yHeight, 154, 32);
                }
            }
        }
    }

    class OptionPopupRenderer extends Renderer {
        Image i_07_pop_sha = dataCenter.getImage("07_pop_sha.png");
        Image i_large_noti_b = dataCenter.getImage("large_noti_b.png");
        Image i_large_noti_m = dataCenter.getImage("large_noti_m.png");
        Image i_large_noti_t = dataCenter.getImage("large_noti_t.png");

        Image input_38 = dataCenter.getImage("input_38.png");
        Image input_38_foc = dataCenter.getImage("input_38_foc.png");
        Image input281 = dataCenter.getImage("input_281.png");
        Image input281_foc = dataCenter.getImage("input_281_foc.png");
        Image input281_foc_dim = dataCenter.getImage("input_281_foc_dim.png");
        Image btn = dataCenter.getImage("05_focus_dim.png");
        Image btn_foc = dataCenter.getImage("05_focus.png");

        Image checkBoxDim = dataCenter.getImage("check_box.png");
        Image checkBox = dataCenter.getImage("check_box_rim.png");
        Image check = dataCenter.getImage("check.png");
        Image iHd = dataCenter.getImage("icon_hd.png");
        Image iUhd = dataCenter.getImage("icon_uhd.png");

        Font fButton = GraphicsResource.BLENDER18;

        String optionTitle;

        public void prepare(UIComponent c) {
            optionTitle = TextUtil.shorten(dataCenter.getString("pvr.rec_opt") + " : " + title, fmTitle, 700);
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            // background
            int topY = 90;
            g.drawImage(i_large_noti_b, 0, 410, c);
            if (optionType == RecordingOption.TYPE_SINGLE && (sisterCh == null || sisterProgram == null)) {
                topY += 50;
            }

//            if (optionType == RecordingOption.TYPE_REPEAT || optionType == RecordingOption.TYPE_SERIES) {
//                topY = 159;
//            } else {
//                topY = 204;
//            }
//            if (sisterCh != null) {
//                topY = topY-69;
//            }
            g.drawImage(i_large_noti_m, 0, topY + 70, 960, 181 + 159 - topY, c);
            g.drawImage(i_large_noti_t, 0, topY, c);

            int yGap = -255;
            //->Kenneth[2015.6.15] R5 : SD/HD 전환시마다 상단 채널 정보가 그에 맞게 변경되어야 함
            int _channelNumber= channelNumber;
            boolean _isHd = isHd;
            String _channelName = channelName;
            String _fullName = fullName;

            try {
                if (sisterCh != null && sisterProgram != null) {
                    int sisterDef = sisterCh.getDefinition();
                    if ((sisterDef == TvChannel.DEFINITION_SD && sdSelected) || (sisterDef == TvChannel.DEFINITION_HD && !sdSelected)) {
                        _channelNumber = sisterCh.getNumber();
                        if (sisterCh.getDefinition() == TvChannel.DEFINITION_HD) {
                            _isHd = true;
                        } else if (sisterCh.getDefinition() == TvChannel.DEFINITION_SD) {
                            _isHd = false;
                        }
                        _channelName = sisterCh.getCallLetter();
                        _fullName = sisterCh.getFullName();
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }

            // Channel Number
            g.setColor(cChannel);
            g.setFont(f23);
            g.drawString(_channelNumber+ "", 79, 336+topY+yGap);
            //g.drawString(channelNumber+"", 79, 336+topY+yGap);

            // HD/UDH
            if (isUhd) {
                g.drawImage(iUhd, 120+10, 323+topY+yGap, c);
            } else if (_isHd) {
                g.drawImage(iHd, 120+10, 323+topY+yGap, c);
            }

            // channel logo
            Image logo = (Image) SharedMemory.getInstance().get("logo." + _channelName);
            if (logo == null) {
                logo = (Image) SharedMemory.getInstance().get("logo.default");
            }
            g.drawImage(logo, 153+10, 314+topY+yGap, c);

            // Full Name
            if (_fullName != null) {
                g.setFont(f22);
                g.setColor(cChannel);
                GraphicUtil.drawStringRight(g, _fullName, 910, 335+topY+yGap);
            }

            // program title
            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(title, 79, 369+topY+yGap);

            // start time
            g.setColor(cTime);
            g.setFont(fButton);
            g.drawString(programStartTime, 79, 393+topY+yGap);

            // 하단 버튼
            topY += 25;
            g.drawImage(focusIndex == -1 ? btn_foc : btn, 320, 446, c);
            g.drawImage(focusIndex == -2 ? btn_foc : btn, 484, 446, c);

            g.setColor(GraphicsResource.C_0_0_0);
            g.setFont(GraphicsResource.BLENDER18);
            GraphicUtil.drawStringCenter(g, strSave, 400, 467);
            GraphicUtil.drawStringCenter(g, strCancel, 562, 467);

            // 옵션들
            EditField editing = null;
            if (focusIndex >= 0) {
                if (fields[focusIndex].isEditing()) {
                    editing = fields[focusIndex];
                }
            } else if (focusIndex == -4) {
                if (freqField.isEditing()) {
                    editing = freqField;
                }
            }

            g.translate(165 + 14, 299-159 + topY);
            g.setFont(c.getFont());
            g.setColor(c.getForeground());
            g.drawString(fields[0].fieldName, -100, 20);
            fields[0].paint(g, focusIndex == 0 && editing == null, c);

            g.translate(242, 0);
            g.setFont(c.getFont());
            g.setColor(c.getForeground());
            g.drawString(fields[1].fieldName, 33-86, 20);
            fields[1].paint(g, focusIndex == 1 && editing == null, c);
            g.translate(-242, 0);

            if (option.all) {
                g.translate(429, 44);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(freqField.fieldName, -109, 20);
                freqField.paint(g, focusIndex == -4 && editing == null, c);
                g.translate(-429, -44);
            }

            for (int i = 2; i < fields.length; i++) {
                g.translate(0, 44);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(fields[i].fieldName, -100, 20);
                fields[i].paint(g, focusIndex == i && editing == null, c);
            }
            g.translate(-165-14, -(299-159 + topY) - 44 * (fields.length - 2));

            if (editing != null) {
                int tx, ty, sx;
                g.setColor(GraphicsResource.cDimmedBG);
                //->Kenneth[2016.3.8] R7 작업하다가 BG 가 좀 이상해서 y 좌표 수정함.
                g.fillRect(0, 184 - 159 + topY, 960, 540 - (184 - 159 + topY));
                //g.fillRect(0, 208 - 159 + topY, 960, 540 - (208 - 159 + topY));
                //<-
                if (focusIndex == -4) {
                    tx = 429 + 165 + 14;
                    ty = 44 + 299-159 + topY;
                    sx = -109;
                } else if (focusIndex == 1) {
                    tx = 165 + 14 + 242;
                    ty = 299-159 + topY;
                    sx = 33-86;
                } else {
                    tx = 165 + 14;
                    ty = (focusIndex > 0 ? focusIndex - 1 : focusIndex) * 44 + 299-159 + topY;
                    sx = -100;
                }
                g.translate(tx, ty);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(editing.fieldName, sx, 20);

                editing.paint(g, true, c);
                g.translate(-tx, -ty);
            }
            /*
            int topY;
            g.drawImage(i_07_pop_sha, 0, 369, 960, 171, c);
            g.drawImage(i_large_noti_b, 0, 410, c);

            if (optionType == RecordingOption.TYPE_REPEAT || optionType == RecordingOption.TYPE_SERIES) {
                topY = 159;
            } else {
                topY = 204;
            }
            if (sisterCh != null) {
                topY = topY-69;
            }
            g.drawImage(i_large_noti_m, 0, topY + 70, 960, 181 + 159 - topY, c);
            g.drawImage(i_large_noti_t, 0, topY, c);

            g.setColor(cTitle);
            g.setFont(fTitle);
            g.drawString(optionTitle, 79, 248-159 + topY);

            g.setColor(cTime);
            g.setFont(fTime);
            g.drawString(Formatter.getCurrent().getLongDate(startTime), 79, 272-159 + topY);
            g.setColor(cChannel);
            g.setFont(fChannel);
            GraphicUtil.drawStringRight(g, channelNumber + "  " + channelName, 878, 249-159 + topY);

            int yHeight = 0;
            if (needCheckHd) {
                g.drawImage(focusIndex == -3 ? checkBox : checkBoxDim, 165 + 14, 429, c);
                g.setFont(GraphicsResource.BLENDER19);
                g.setColor(focusIndex == -3 ? GraphicsResource.C_255_203_0 : GraphicsResource.C_182_182_182);
                g.drawString(strCheckBox + "(" + sisterChNum + ")", 193 + 14, 444);
                yHeight = 23;

                if (sisterChCheck) {
                    g.drawImage(check, 168 + 14, 428, c);
                }
            }

            g.drawImage(focusIndex == -1 ? btn_foc : btn, 320, 446 + yHeight, c);
            g.drawImage(focusIndex == -2 ? btn_foc : btn, 484, 446 + yHeight, c);

            g.setColor(GraphicsResource.C_0_0_0);
            g.setFont(GraphicsResource.BLENDER18);
            GraphicUtil.drawStringCenter(g, strSave, 400, 467 + yHeight);
            GraphicUtil.drawStringCenter(g, strCancel, 562, 467 + yHeight);

            EditField editing = null;
            if (focusIndex >= 0) {
                if (fields[focusIndex].isEditing()) {
                    editing = fields[focusIndex];
                }
            } else if (focusIndex == -4) {
                if (freqField.isEditing()) {
                    editing = freqField;
                }
            }

            g.translate(165 + 14, 299-159 + topY);
            g.setFont(c.getFont());
            g.setColor(c.getForeground());
            g.drawString(fields[0].fieldName, -100, 20);
            fields[0].paint(g, focusIndex == 0 && editing == null, c);

            g.translate(242, 0);
            g.setFont(c.getFont());
            g.setColor(c.getForeground());
            g.drawString(fields[1].fieldName, 33-86, 20);
            fields[1].paint(g, focusIndex == 1 && editing == null, c);
            g.translate(-242, 0);

            if (option.all) {
                g.translate(429, 44);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(freqField.fieldName, -109, 20);
                freqField.paint(g, focusIndex == -4 && editing == null, c);
                g.translate(-429, -44);
            }

            for (int i = 2; i < fields.length; i++) {
                g.translate(0, 44);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(fields[i].fieldName, -100, 20);
                fields[i].paint(g, focusIndex == i && editing == null, c);
            }
            g.translate(-165-14, -(299-159 + topY) - 44 * (fields.length - 2));

            if (editing != null) {
                int tx, ty, sx;
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 208 - 159 + topY, 960, 540 - (208 - 159 + topY));
                if (focusIndex == -4) {
                    tx = 429 + 165 + 14;
                    ty = 44 + 299-159 + topY;
                    sx = -109;
                } else if (focusIndex == 1) {
                    tx = 165 + 14 + 242;
                    ty = 299-159 + topY;
                    sx = 33-86;
                } else {
                    tx = 165 + 14;
                    ty = (focusIndex > 0 ? focusIndex - 1 : focusIndex) * 44 + 299-159 + topY;
                    sx = -100;
                }
                g.translate(tx, ty);
                g.setFont(c.getFont());
                g.setColor(c.getForeground());
                g.drawString(editing.fieldName, sx, 20);

                editing.paint(g, true, c);
                g.translate(-tx, -ty);
            }
            */
        }
    }

}

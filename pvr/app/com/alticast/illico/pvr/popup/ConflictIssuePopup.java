package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class ConflictIssuePopup extends Popup {

	private OwnRenderer renderer = new OwnRenderer();

    RecordingRequest[] array;

    ConflictIssue issue;

	public ConflictIssuePopup(ConflictIssue issue) {
        this.issue = issue;
		setRenderer(renderer);
		prepare();
		this.setBounds(0, 0, 960, 540);
		this.setVisible(false);

        array = issue.getRecordings();
	}

	public boolean handleKey(int type){
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ConflictIssuePopup.handleKey("+type+")");
        switch (type) {
        case KeyEvent.VK_UP:
            break;
        case KeyEvent.VK_DOWN:
            break;
        case KeyEvent.VK_LEFT:
        	if (focus == 1) {
                focus = 0;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (focus == 0) {
                focus = 1;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
        	switch (focus) {
        	case 0: // resolve
                boolean shown = false;
                try {
                    shown = IssueManager.getInstance().showConflictIssue(issue, null);
                } catch (Exception e) {
                    Log.print(e);
                }
                if (!shown) {
                    IssueManager.getInstance().stopPopup(this);
                }
        		break;
        	case 1: // cancel
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_EXIT:
        	IssueManager.getInstance().stopPopup(this);
            break;
        case KeyCodes.LAST:
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

	public void stop() {
		this.setVisible(false);
	}

    public void startClickingEffect() {
        clickEffect.start(renderer.getButtonFocusBounds(focus));
    }

    class OwnRenderer extends IssueRecordingsRenderer {
        public OwnRenderer() {
            super("pvr.conflict_popup_title", "pvr.conflict_popup_msg", null,
                "pvr.conflict_popup_button_0", "pvr.conflict_popup_button_1", true);
        }

        public int getSize() {
            return array.length;
        }
        public RecordingRequest getRecordingRequest(int i) {
            return array[i];
        }
    }
}

abstract class IssueRecordingsRenderer extends Renderer {
    DataCenter dataCenter = DataCenter.getInstance();

    Image i_pop_sha;
    Image i_pop_high_402;
    Image i_pop_gap_379;
    Image i_05_pop_glow_02;
    Image i_icon_noti_red;
    Image i_05_focus_dim;
    Image i_05_focus;

    String title;
    String[] topMessage = new String[0];
    String[] bottomMessage = new String[0];

    Color cGray = new Color(150, 150, 150);

    FontMetrics fmText = FontResource.getFontMetrics(GraphicsResource.BLENDER18);

    String titleKey, msgKey0, msgKey1, btnKey0, btnKey1;
    boolean hasIcon;

    public IssueRecordingsRenderer(String title, String msg1, String msg2, String btn0, String btn1, boolean icon) {
        this.titleKey = title;
        this.msgKey0 = msg1;
        this.msgKey1 = msg2;
        this.btnKey0 = btn0;
        this.btnKey1 = btn1;
        this.hasIcon = icon;
    }

    public Rectangle getPreferredBounds(UIComponent c) {
        return null;
    }

    protected void paint(Graphics g, UIComponent c) {
        g.setColor(GraphicsResource.cDimmedBG);
        g.fillRect(0, 0, 960, 540);

        g.drawImage(i_pop_sha, 280, 396, 404, 79, c);
        g.drawImage(i_05_pop_glow_02, 250, 94, c);
        g.setColor(GraphicsResource.C_35_35_35);
        g.fillRect(280, 124, 402, 276);
        g.drawImage(i_pop_high_402, 280, 124, c);
        g.drawImage(i_pop_gap_379, 292, 162, c);

        int w = hasIcon ? i_icon_noti_red.getWidth(c) : 0;
        g.setFont(GraphicsResource.BLENDER24);
        g.setColor(GraphicsResource.C_255_203_0);
        int x = GraphicUtil.drawStringCenter(g, title, 483 + w / 2, 151);
        if (hasIcon) {
            g.drawImage(i_icon_noti_red, x - w - 5, 151-18, c);
        }

        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(Color.white);
        int y = 188;
        for (int i = 0; i < topMessage.length; i++) {
            GraphicUtil.drawStringCenter(g, topMessage[i], 480, y);
            y += 20;
        }
        y = 336;
        for (int i = bottomMessage.length - 1; i >= 0; i--) {
            GraphicUtil.drawStringCenter(g, bottomMessage[i], 480, y);
            y -= 20;
        }

        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(GraphicsResource.C_255_203_0);
        int size = getSize();
        if (size > 3) {
            g.setColor(cGray);
            g.drawString("(" + (size - 2) + " " + dataCenter.getString("pvr.more_programs") + ")", 295, 245 + 44);
            size = 2;
        }
        Formatter f = Formatter.getCurrent();
        y = 245;
        for (int i = 0; i < size; i++) {
            RecordingRequest r = getRecordingRequest(i);
            g.setColor(GraphicsResource.C_255_203_0);
            g.drawString(TextUtil.shorten(ParentalControl.getTitle(r), fmText, 194), 295, y);
            g.setColor(GraphicsResource.C_210_210_210);
            g.drawString(f.getLongDate(AppData.getScheduledStartTime(r)), 494, y);
            y += 22;
        }

        g.setColor(GraphicsResource.C_3_3_3);
        if (c.getFocus() == 0) {
            g.drawImage(i_05_focus, 321, 353, c);
            g.drawImage(i_05_focus_dim, 485, 353, c);
        } else {
            g.drawImage(i_05_focus_dim, 321, 353, c);
            g.drawImage(i_05_focus, 485, 353, c);
        }
        GraphicUtil.drawStringCenter(g, dataCenter.getString(btnKey0), 399, 374);
        GraphicUtil.drawStringCenter(g, dataCenter.getString(btnKey1), 564, 374);
    }

    public void prepare(UIComponent c) {
        i_pop_sha = dataCenter.getImage("pop_sha.png");
        i_pop_high_402 = dataCenter.getImage("pop_high_402.png");
        i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
        i_05_pop_glow_02 = dataCenter.getImage("05_pop_glow_02.png");
        i_icon_noti_red = dataCenter.getImage("icon_noti_red.png");
        i_05_focus_dim = dataCenter.getImage("05_focus_dim.png");
        i_05_focus = dataCenter.getImage("05_focus.png");

        title = dataCenter.getString(titleKey);
        FontMetrics fm = FontResource.getFontMetrics(GraphicsResource.BLENDER18);
        if (msgKey0 != null) {
            topMessage = TextUtil.split(TextUtil.replace(dataCenter.getString(msgKey0), "|", "\n"), fm, 360);
        }
        if (msgKey1 != null) {
            bottomMessage = TextUtil.split(TextUtil.replace(dataCenter.getString(msgKey1), "|", "\n"), fm, 360);
        }

        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    public abstract int getSize();

    public abstract RecordingRequest getRecordingRequest(int i);

    public Rectangle getButtonFocusBounds(int focus) {
        if (focus == 0) {
            return new Rectangle(321+1, 353, 154, 32);
        } else if (focus == 1) {
            return new Rectangle(485+1, 353, 154, 32);
        } else {
            return null;
        }
    }

}


package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.tv.util.*;

import org.havi.ui.event.HRcEvent;
import org.ocap.shared.dvr.RecordingRequest;

import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.Core;
import com.alticast.illico.pvr.App;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.PopupListener;
import com.alticast.illico.pvr.RecordManager;
import com.alticast.illico.pvr.RecordingOption;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;

public class RecordingNoticePopup extends BottomPopup {

    String seg0;
    String seg1;
    String recordingTitle;

    public long recordingTime;

    TVTimerSpec secTimer = new TVTimerSpec();

    RecordingRequest req;
    int tunerIndex;

    public RecordingNoticePopup(RecordingRequest r, int tunerIndex) {
        super();
        req = r;
        this.tunerIndex = tunerIndex;
        DataCenter dataCenter = DataCenter.getInstance();
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");

        recordingTitle = ParentalControl.getTitle(r);
        String s1 = dataCenter.getString("pvr.rec_con_msg_1");
        String s2 = dataCenter.getString("pvr.rec_con_msg_2");

        String[] array1 = TextUtil.tokenize(s1, "%1");
        String[] array2 = TextUtil.tokenize(array1[1], "%2");
        seg0 = array1[0] + recordingTitle + array2[0];
        seg1 = array2[1];

        recordingTime = AppData.getScheduledStartTime(r);
        setAutoClose(recordingTime - Constants.MS_PER_SECOND);

        this.texts = new String[2];
        this.texts[1] = s2;
        refreshTexts();

        this.title = dataCenter.getString("pvr.rec_con_title");
        this.buttons = new String[] { dataCenter.getString("pvr.rec_con_btn_1"),
                                   dataCenter.getString("pvr.rec_con_btn_2") };

        secTimer.setDelayTime(500L);
        secTimer.setRegular(true);
        secTimer.setRepeat(true);
        secTimer.addTVTimerWentOffListener(new TVTimerWentOffListener() {
            public void timerWentOff(TVTimerWentOffEvent event) {
                refreshTexts();
                repaint();
            }
        });
    }

    private void refreshTexts() {
        long dur = Math.max(0, recordingTime - System.currentTimeMillis());
        int sec = (int) (dur / Constants.MS_PER_SECOND);
        int min = sec / 60;
        sec = sec % 60;
        String time = min + (sec < 10 ? ":0" : ":") + sec;
        texts[0] = seg0 + time + seg1;
    }

    public void start() {
        super.start();
        autoClose = false;
        try {
            secTimer = TVTimer.getTimer().scheduleTimerSpec(secTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    public void stop() {
        super.stop();
        TVTimer.getTimer().deschedule(secTimer);
    }

    /** overriden. */
    public void setAutoClose(long time) {
        if (time <= 0) {
            autoClose = false;
        } else {
            autoClose = true;
            autoCloseTimer.setAbsoluteTime(time);
        }
    }

    public void tuneAndRecord() {
        String appName = Core.getInstance().getCurrentActivatedApplicationName();
        boolean appVideoContext = App.R3_TARGET && !"EPG".equals(Core.getInstance().getVideoContextName());
        boolean inAppState = (appName != null && !appName.equals("EPG")) || appVideoContext;
        if (Log.DEBUG_ON) {
            Log.printDebug("RecordingNoticePopup: inAppState = " + inAppState);
        }
        try {
            int sid = AppData.getInteger(req, AppData.SOURCE_ID);
            if (inAppState) {
                if (appVideoContext) {
                    Core.monitorService.releaseVideoContext();
                }
                Core.monitorService.exitToChannel(sid);
            } else {
                Core.epgService.getChannelContext(tunerIndex).changeChannel(sid);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }
}

package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.KeyCodes;

public class ButtonListPopup extends Popup {

    PopupRenderer popupRenderer = new PopupRenderer();

    private int buttonSize;
    private String title;
    private String[] buttons;

    public ButtonListPopup() {
        this(3);
    }

    public ButtonListPopup(int buttonSize) {
        this.buttonSize = buttonSize;
        setRenderer(popupRenderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public void setTitle(String s) {
        title = TextUtil.shorten(s, popupRenderer.fmTitle, 310);
        //->Kenneth[2017.4.19] VDTRMASTER-6110 [] 을 없앤다.
        //title = "["+title+"]";
    }

    public void setButtons(String[] s) {
        buttons = s;
    }

    public void start() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ButtonListPopup.start()");
        //->Kenneth[2017.3.15] R7.3 : focus 가 0 이 아니어야 하는 경우도 생김
        // 이때 setFocus() 를 사용하는데 start 에서 다시 focus = 0 으로 하면 안됨.
        // 이 팝업은 항상 new 로 instance 생성해서 보여주므로 focus 는 기본적으로
        // 항상 0 이 될 것이므로 여기서 강제로 0 으로 안해 됨.
        //focus = 0;
        //<-
        this.setVisible(true);
        super.start();
    }

    public void stop() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ButtonListPopup.stop()");
        this.setVisible(false);
        listener = null;
        super.stop();
    }

    //Kenneth[2017.3.15] R7.3 : ListPanel 의 processPlay 에서 호출 되는 경우는
    // Exit 키를 받아서 close 해줘야 한다.
    // 그 외의 경로를 통해서 받은 경우는 기존처럼 Exit 키를 consume 하지 않는다.
    private boolean needToConsumeExitKey = false;
    public void consumeExit() {
        needToConsumeExitKey = true;
    }
    //<-

    public boolean handleKey(int code) {
        if (listener != null) {
            boolean ret = listener.handleHotKey(code);
            if (ret) {
                close();
                repaint();
                return true;
            }
        }
        switch (code) {
        case KeyEvent.VK_UP:
            if (focus > 0) {
                focus--;
            }
            break;
        case KeyEvent.VK_DOWN:
            if (focus < buttonSize - 1) {
                focus++;
            }
            break;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"BottomListPopup.handlKey(VK_ENTER)");
            startClickingEffect();
            if (listener != null) {
                listener.selected(focus);
            }
            close();
            break;
        case KeyEvent.VK_LEFT:
            return true;
        case KeyEvent.VK_RIGHT:
            return true;
        //->Kenneth[2017.3.15] R7.3
        case OCRcEvent.VK_EXIT:
            if (needToConsumeExitKey) {
                close();
                return true;
            }
            return false;
        //<-
        default:
            if (listener != null) {
                return listener.consumeKey(code);
            }
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(333+1, 260-37 + focus * 37, 293, 32);
    }

    private class PopupRenderer extends Renderer {

        Color fg = new Color(35, 35, 35);

        Image bgglow_m = dataCenter.getImage("05_bgglow_m.png");
        Image bgglow_b = dataCenter.getImage("05_bgglow_b.png");
        Image bgglow_t = dataCenter.getImage("05_bgglow_t.png");
        Image popup_sha = dataCenter.getImage("05_popup_sha.png");
        Image sep = dataCenter.getImage("05_sep.png");
        Image high = dataCenter.getImage("05_high.png");

        Image btn_293 = dataCenter.getImage("btn_293.png");
        Image btn_293_foc = dataCenter.getImage("btn_293_foc.png");

        Color cTitle = new Color(252, 202, 4);
        Color cButton = Color.black;
        Font fTitle = GraphicsResource.BLENDER24;
        Font fButton = GraphicsResource.BLENDER18;
        FontMetrics fmTitle = FontResource.getFontMetrics(GraphicsResource.BLENDER24);

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
//            g.drawImage(pop_rmdbg, 177, -28, c);

            g.drawImage(bgglow_t, 276, 113, c);
            g.drawImage(bgglow_b, 276, 340, c);
            g.drawImage(bgglow_m, 276, 168, 410, 172, c);

            g.drawImage(popup_sha, 304, 364, c);
            g.setColor(fg);
            g.fillRect(306, 143, 350, 224);

            g.drawImage(sep, 285, 181, c);
            g.drawImage(high, 306, 143, c);

            g.setFont(fTitle);
            g.setColor(cTitle);
            GraphicUtil.drawStringCenter(g, title, 484, 170);

            g.setFont(fButton);
            g.setColor(cButton);
            int y = 260 - 37;
            for (int i = 0; i < buttonSize; i++) {
                if (i == focus) {
                    g.drawImage(btn_293_foc, 333, y, c);
                } else {
                    g.drawImage(btn_293, 334, y, c);
                }
                GraphicUtil.drawStringCenter(g, dataCenter.getString(buttons[i]), 480, y+20);
                y = y + 37;
            }
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

package com.alticast.illico.pvr.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.list.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.TextUtil;

public class ListThreeButtonPopup extends Popup {
    String[] desc;

    String popupButton1 = "";
    String popupButton2 = "";
    String popupButton3 = "";

    public int buttonLen = 3;

    final String popupTitle;

    public ListThreeButtonPopup(String title) {
        popupTitle = title;
        setRenderer(new PopupRander());
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

    public void setMessage(String msg) {
        desc = TextUtil.split(msg, GraphicsResource.FM17, 330);
    }

    public void setButtonMessage(String b1, String b2, String b3) {
        popupButton1 = b1;
        popupButton2 = b2;
        popupButton3 = b3;
        if (popupButton3 == null) {
            buttonLen = 2;
        } else {
            buttonLen = 3;
        }
    }

    public boolean handleKey(int type){
        switch (type) {
        case KeyEvent.VK_UP:
            focus--;
            if (focus < 0) {
                focus = 0;
            }
            break;
        case KeyEvent.VK_DOWN:
            if (focus < buttonLen - 1) {
                focus++;
            }
            break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
            if (listener != null) {
                listener.selected(focus);
            }
            close();
            break;
        case OCRcEvent.VK_EXIT:
            if (listener != null) {
                listener.selected(-1);
            }
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    private class PopupRander extends Renderer {
        Image pop_glow_02;
        Image pop_sha;

        Image pop_high_402;
        Image pop_gap_379;

        Image btn_293;
        Image btn_293_foc;


        FontMetrics f17 = FontResource.getFontMetrics(GraphicsResource.BLENDER17);

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.drawImage(pop_sha, 280, 396, 404, 79, c);
            g.drawImage(pop_glow_02, 250, 94, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(280, 124, 402, 276);

            g.drawImage(pop_high_402, 280, 124, c);
            g.drawImage(pop_gap_379, 292, 162, c);

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 484, 151);

            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_255_255_255);
            for (int a = 0; a < desc.length; a++){
                GraphicUtil.drawStringCenter(g, desc[a], 481, 211 + a * 18);
            }

            g.drawImage(btn_293, 334, 279, c);
            g.drawImage(btn_293, 334, 316, c);
            g.drawImage(btn_293, 334, 353, c);

            g.drawImage(btn_293_foc, 334, 279 + focus * 37, c);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_0_0_0);
            GraphicUtil.drawStringCenter(g, popupButton1, 483, 300);
            GraphicUtil.drawStringCenter(g, popupButton2, 480, 337);
            if (popupButton3 != null) {
                GraphicUtil.drawStringCenter(g, popupButton3, 480, 374);
            }
        }

        public void prepare(UIComponent c) {
            pop_sha = DataCenter.getInstance().getImage("pop_sha.png");
            pop_glow_02 = DataCenter.getInstance().getImage("05_pop_glow_02.png");

            pop_high_402 = DataCenter.getInstance().getImage("pop_high_402.png");
            pop_gap_379 = DataCenter.getInstance().getImage("pop_gap_379.png");

            btn_293 = DataCenter.getInstance().getImage("btn_293.png");
            btn_293_foc = DataCenter.getInstance().getImage("btn_293_foc.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

    public void startClickingEffect() {
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this, 5);
        }
        clickEffect.start(334, 279 + (focus * 37), 293, 32);
    }

}

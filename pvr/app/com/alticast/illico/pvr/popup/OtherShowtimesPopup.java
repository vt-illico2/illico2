package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.pvr.AppDataKeys;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class OtherShowtimesPopup extends Popup {
    public DataCenter dataCenter = DataCenter.getInstance();
    PopupRenderer popupRenderer = new PopupRenderer();

    private int listOffset, listFocus;
    private int selected, original;
    private RecordingRequest toFind;
    private TvProgram[] programs;
    private String title;
    private ConflictRenderingItem cri;

    ConflictResolveSession session;

    public OtherShowtimesPopup(ConflictResolveSession session) {
        this.session = session;
        setRenderer(popupRenderer);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);

        PvrFooter footer = new PvrFooter();
        footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        footer.setBounds(466, 401, 200, 30);
        add(footer);
    }

    public void start() {
        listOffset = 0;
        listFocus = 0;
        try {
            // ri.reset();
            // ArrayList list = new ArrayList();
            // while (ri.hasNext()) {
            // list.add(ri.next());
            // }
            // programs = new TvProgram[list.size()];
            // list.toArray(programs);
            selected = 0;
            original = -1;
            long s = AppData.getScheduledStartTime(toFind);
            for (int i = 0; i < programs.length; i++) {
                if (programs[i].getStartTime() == s) {
                    selected = i;
                    original = i;
                    break;
                }
            }
        } catch (Exception e) {
            Log.print(e);
            programs = new TvProgram[0];
        }
        title = ParentalControl.getTitle(toFind);

        super.start();
        this.setVisible(true);
    }

    public void stop() {
        super.stop();
        this.setVisible(false);
    }

    public void setConflictRenderingItem(ConflictRenderingItem cri) {
        this.cri = cri;
        setRecordingRequest(cri.request);
        setPrograms(cri.otherShowtimes);
    }

    public void setRecordingRequest(RecordingRequest rr) {
        toFind = rr;
    }

    public void setPrograms(TvProgram[] programs) {
        this.programs = programs;
    }

    public boolean handleKey(int type) {
        return keyPressed(type);
    }

    private boolean keyPressed(int type) {
        switch (type) {
        case KeyEvent.VK_UP:
            moveFocus(true);
            break;
        case KeyEvent.VK_DOWN:
            moveFocus(false);
            break;
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
            selected = listOffset + listFocus;
            if (session instanceof DualTunerConflictResolveSession) {
                DualTunerConflictResolveSession ds = (DualTunerConflictResolveSession) session;
                if (selected == original) {
                    ds.showOtherTimesPopup(null);
                    break;
                }
                ds.showRescheduleConfirmPopup(toFind, programs[selected]);
            } else if (session instanceof ManyTunerConflictResolveSession) {
                ManyTunerConflictResolveSession ms = (ManyTunerConflictResolveSession) session;
                if (selected == original) {
                    setTarget(cri, null);
                } else {
                    setTarget(cri, programs[selected]);
                }
                ms.showCurrentConflictPopup();
            }
            break;
        case OCRcEvent.VK_LAST:
        case OCRcEvent.VK_PAGE_UP:
        case OCRcEvent.VK_PAGE_DOWN:
            return true;
        case OCRcEvent.VK_EXIT:
            if (session instanceof DualTunerConflictResolveSession) {
                ((DualTunerConflictResolveSession) session).showOtherTimesPopup(null);
            } else if (session instanceof ManyTunerConflictResolveSession) {
                setTarget(cri, null);
                ((ManyTunerConflictResolveSession) session).showCurrentConflictPopup();
            }
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    private void setTarget(ConflictRenderingItem cri, TvProgram p) {
        if (cri != null) {
            cri.setRescheduleTarget(p);
        }
    }

    private boolean moveFocus(boolean up) {
        int len = programs.length;
        if (canMoveFocus(up)) {
            if (up) {
                listFocus = Math.max(0, listFocus - 1);
                return false;
            } else { // down
                int old = listFocus;
                listFocus = Math.min(Math.min(2, len - 1), listFocus + 1);
                return (old == listFocus); // no move, focus to button
            }

        } else {
            listOffset += up ? -1 : 1;
            return false;
        }
    }

    private boolean canMoveFocus(boolean up) {
        int len = programs.length;
        if (len < 3)
            return true; // less than 1 page

        if (up) {
            return listFocus > 1 || listOffset == 0;
        } else { // down
            return listFocus < 1 || listOffset >= len - 3;
        }
    }

    public void startClickingEffect() {
        clickEffect.start(297 + 1, 263 + listFocus * 31, 367, 32);
    }

    private class PopupRenderer extends Renderer {
        Image i_pop_sha;
        Image i_05_pop_glow_02;
        Image i_pop_high_402;
        Image i_pop_gap_379;
        Image i_05_btn_375;
        Image i_05_issue_bg;
        Image i_07_othersha_top;
        Image i_07_othersha_bottom;
        Image i_02_ars_t;
        Image i_02_ars_b;
        FontMetrics f17 = FontResource.getFontMetrics(GraphicsResource.BLENDER17);

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        private boolean isHd(TvProgram p) throws RemoteException {
            boolean hd = p.isHd();
            if (!hd) {
                return false;
            }
            TvChannel ch = p.getChannel();
            if (ch != null) {
                return ch.isHd();
            } else {
                return true;
            }
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);

            g.drawImage(i_pop_sha, 280, 394, 404, 79, c);
            g.drawImage(i_05_pop_glow_02, 250, 113, c);
            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(280, 143, 402, 254);
            g.drawImage(i_pop_high_402, 280, 143, c);
            g.drawImage(i_pop_gap_379, 292, 181, c);

            // title
            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.selectOtherTitle"), 480, 171);

            // desc
            g.setFont(GraphicsResource.BLENDER17);
            g.setColor(GraphicsResource.C_255_255_255);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.selectOtherDesc"), 480, 219);
            g.setColor(GraphicsResource.C_255_203_0);
            GraphicUtil.drawStringCenter(g, TextUtil.shorten(title, g.getFontMetrics(), 360), 480, 219 + 18);

            // list bg
            g.drawImage(i_05_issue_bg, 297, 263, c);

            // g.drawString(""+programs.length, 320, 235);
            // list
            g.setFont(GraphicsResource.BLENDER18);
            for (int i = 0; i < 3 && i < programs.length; i++) {
                if (listFocus == i) {
                    g.drawImage(i_05_btn_375, 297, 263 + i * 31, c);
                    g.setColor(GraphicsResource.C_0_0_0);
                    // g.drawImage(check_box_foc, 336, 245 + listFocus * 34, c);
                    // if (selected == i + listOffset) {
                    // g.drawImage(radio_btn_foc, 340, 249 + i * 34, c);
                    // }
                } else {
                    g.setColor(GraphicsResource.C_255_255_255);
                    // g.drawImage(check_box, 336, 245 + i * 34, c);
                    // if (selected == i + listOffset) {
                    // g.drawImage(radio_btn, 340, 249 + i * 34, c);
                    // }
                }
                try {
                    long s = programs[i + listOffset].getStartTime();
                    long e = programs[i + listOffset].getEndTime();
                    Formatter fmt = Formatter.getCurrent();
                    String t = fmt.getLongDayText(s) + "  " + fmt.getTime(s) + " - " + fmt.getTime(e);
                    g.drawString(t, 312, 286 + i * 31);
                    g.drawString(isHd(programs[i + listOffset]) ? "HD" : "SD", 625, 286 + i * 31);

                } catch (Exception re) {
                    Log.printWarning(re);
                }
            }

            // shadow and arrows
            if (listOffset > 0) {
                g.drawImage(i_07_othersha_top, 298, 264, c);
                g.drawImage(i_02_ars_t, 472, 255, c);
            }
            if (programs != null && listOffset + 3 < programs.length) {
                g.drawImage(i_07_othersha_bottom, 298, 325, c);
                g.drawImage(i_02_ars_b, 471, 350, c);
            }
        }

        public void prepare(UIComponent c) {
            i_pop_sha = dataCenter.getImage("pop_sha.png");
            i_05_pop_glow_02 = dataCenter.getImage("05_pop_glow_02.png");
            i_pop_high_402 = dataCenter.getImage("pop_high_402.png");
            i_pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
            i_05_btn_375 = dataCenter.getImage("05_btn_375.png");
            i_05_issue_bg = dataCenter.getImage("05_issue_bg.png");
            i_07_othersha_top = dataCenter.getImage("07_othersha_top.png");
            i_07_othersha_bottom = dataCenter.getImage("07_othersha_bottom.png");
            i_02_ars_t = dataCenter.getImage("02_ars_t.png");
            i_02_ars_b = dataCenter.getImage("02_ars_b.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

    }
}

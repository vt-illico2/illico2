package com.alticast.illico.pvr.popup;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.*;

import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.issue.DualTunerConflictResolveSession;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.ParentalControl;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class DualTunerCancelOptionPopup extends CancelOptionPopup {

    ArrayList selected;
    ArrayList repeated;
    ArrayList toDelete;

    HashSet keyEpisode;
    HashSet keyEntire;

    DualTunerConflictResolveSession session;

    public DualTunerCancelOptionPopup(DualTunerConflictResolveSession session) {
        this.session = session;
    }

    public void start() {
    	this.focus = 0;
    	toDelete = new ArrayList();
        keyEpisode = new HashSet();
        keyEntire = new HashSet();
        if (Log.DEBUG_ON) {
    	    Log.printDebug("CancelOptionPopup, " + repeated);
        }
        resetTarget();
        super.start();
    	this.setVisible(true);
    }

    public void stop(){
        this.setVisible(false);
    }

    public void setWillBeRecordedRecording(ArrayList list) {
    	selected = list;
    }

    public void setRepeatedRecording(ArrayList list) {
    	repeated = list;
    }

    private boolean resetTarget() {
        try {
            if (repeated.isEmpty()) {
                return false;
            }
            RecordingRequest rr = (RecordingRequest) repeated.get(0);
            String s = AppData.getString(rr, AppData.GROUP_KEY);
            if (s != null && s.length() > 0) {
                if (keyEntire.contains(s) || keyEpisode.contains(s)) {
                    repeated.remove(0);
                    return resetTarget();
                }
            }
            ren.setMessage(TextUtil.replace(dataCenter.getString("pvr.cancelOptionDesc"), "%%", ParentalControl.getTitle(rr)));
            return true;
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }

    protected void processSkipThis() {
        RecordingRequest req = (RecordingRequest) repeated.remove(0);
        String s = AppData.getString(req, AppData.GROUP_KEY);
        if (s != null && s.length() > 0) {
            keyEpisode.add(s);
        }
        focus = 0;
        if (!resetTarget()) {
            session.showConfirmPopup(selected, toDelete);
        }
    }

    protected void processDeleteEntire() {
        RecordingRequest req = (RecordingRequest) repeated.remove(0);
        toDelete.add(req);
        String s = AppData.getString(req, AppData.GROUP_KEY);
        if (s != null && s.length() > 0) {
            keyEntire.add(s);
        }
        focus = 0;
        if (!resetTarget()) {
            session.showConfirmPopup(selected, toDelete);
        }
    }

    protected void processCancel() {
        session.showPickupPopup();
    }


}


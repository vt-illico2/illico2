package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Calendar;
import java.util.Vector;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;

public class SelectDaysPopup extends Popup {

    public boolean fillBg = false;

    PopupRenderer popupRander = new PopupRenderer();
    boolean[] check = new boolean[8];

    String[] weeks = new String[8];

    int x, y;

    public SelectDaysPopup(boolean[] days) {
        setRenderer(popupRander);
        weeks[0] = dataCenter.getString("pvr.Every day");
        Formatter formatter = Formatter.getCurrent();
        for (int i = 1; i <= 7; i++) {
            weeks[i] = formatter.getFullDay(i % 7);
        }
        if (days != null) {
            System.arraycopy(days, 0, check, 1, 7);
        }
        resetEvery();
        setPosition(196, 73);
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean handleKey(int type) {
        switch (type) {
            case OCRcEvent.VK_UP:
                if (focus == -1 || focus == -2) {
                    focus = 7;
                } else if (focus > 0) {
                    focus--;
                }
                break;
            case OCRcEvent.VK_DOWN:
                if (focus == 7) {
                    focus = -1;
                } else if (focus >= 0) {
                    focus++;
                }
                break;
            case OCRcEvent.VK_RIGHT:
                if (focus == -1) {
                    focus = -2;
                }
                break;
            case OCRcEvent.VK_LEFT:
                if (focus == -2) {
                    focus = -1;
                }
                break;
            case OCRcEvent.VK_ENTER:
                if (focus == -1) {
                    startClickingEffect();
                    if (listener != null) {
                        listener.selected(0);
                    }
                    close();
                } else if (focus == -2) {
                    startClickingEffect();
                    close();
                } else {
                    if (focus == 0) {
                        if (check[0]) {
                            for (int i = 0; i <= 7; i++) {
                                check[i] = false;
                            }
                        } else {
                            for (int i = 0; i <= 7; i++) {
                                check[i] = true;
                            }
                        }
                    } else {
                        check[focus] = !check[focus];
                        resetEvery();
                    }
                }
                break;
            case OCRcEvent.VK_EXIT:
                close();
                break;
            default:
                return false;
        }
        repaint();
        return true;
    }

    private void resetEvery() {
        for (int i = 1; i <= 7; i++) {
            if (!check[i]) {
                check[0] = false;
                return;
            }
        }
        check[0] = true;
    }

    public boolean[] getDays() {
        boolean[] days = new boolean[7];
        System.arraycopy(check, 1, days, 0, 7);
        return days;
    }

    public void startClickingEffect() {
        Rectangle r = new Rectangle(154, 32);
        if (focus == -1) {
            r.setLocation(514 + 1 -303 + x - 196, 417 + y - 73);
        } else if (focus == -2) {
            r.setLocation(678 + 1 -303 + x - 196, 417 + y - 73);
        } else {
            return;
        }
        if (clickEffect == null) {
            clickEffect = new ClickingEffect(this);
        }
        clickEffect.start(r);
    }

    private class PopupRenderer extends Renderer {
        Image pop_glow_t = dataCenter.getImage("05_pop_glow_t.png");
        Image pop_glow_b = dataCenter.getImage("05_pop_glow_b.png");
        Image pop_glow_m = dataCenter.getImage("05_pop_glow_m.png");

        Image pop_sha = dataCenter.getImage("pop_sha.png");
        Image pop_gap_379 = dataCenter.getImage("pop_gap_379.png");
        Image pop_high_350 = dataCenter.getImage("pop_high_350.png");

        Image pop_list = dataCenter.getImage("pop_list272x220.png");
        Image pop_listf = dataCenter.getImage("pop_list272x220_f.png");
        Image i_check = dataCenter.getImage("check.png");
        Image check_foc = dataCenter.getImage("check_foc.png");
        Image check_box = dataCenter.getImage("check_box.png");
        Image check_box_foc = dataCenter.getImage("check_box_foc.png");
        Image pop_focus_dim = dataCenter.getImage("05_focus_dim.png");
        Image pop_focus = dataCenter.getImage("05_focus.png");

        String popupTitle = dataCenter.getString("pvr.repeatePopTitle");
        String description = dataCenter.getString("pvr.MsgRepeat1");
        String ok = dataCenter.getString("pvr.OK");
        String cancel = dataCenter.getString("pvr.Cancel");

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (fillBg) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);
            }

            g.translate(-303 + x - 196, y - 73);

            g.drawImage(pop_glow_m, 469, 122, 0, 294, c);
            g.drawImage(pop_glow_b, 469, 416, c);
            g.drawImage(pop_glow_t, 469, 42, c);

            g.drawImage(pop_sha, 499, 463, 350, 0, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(499, 72, 350, 394);
            g.drawImage(pop_gap_379, 478, 110, c);
            g.drawImage(pop_high_350, 499, 72, c);

            g.drawImage(pop_list, 536, 154, c);

            g.drawImage(pop_focus_dim, 678, 417, c);
            g.drawImage(pop_focus_dim, 514, 417, c);

            if (focus == -1) {
                g.drawImage(pop_focus, 514, 417, c);
            } else if (focus == -2) {
                g.drawImage(pop_focus, 678, 417, c);
            }

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 671, 98);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            GraphicUtil.drawStringCenter(g, description, 674, 139);
            g.setColor(GraphicsResource.C_4_4_4);
            GraphicUtil.drawStringCenter(g, ok, 592, 438);
            GraphicUtil.drawStringCenter(g, cancel, 757, 438);

            for (int a = 0; a < weeks.length; a++) {
                g.setFont(GraphicsResource.BLENDER18);
                if (a == focus) {
                    g.drawImage(pop_listf, 536, 156 + a * 31, c);
                    g.setColor(GraphicsResource.C_0_0_0);
                } else {
                    g.setColor(GraphicsResource.C_215_215_215);
                }

                g.drawString(weeks[a], 577, 176 + a * 31);
                g.drawImage(a == focus ? check_box_foc : check_box, 549, 163 + a * 31, c);
                if (check[a]) {
                    if (a == focus) {
                        g.drawImage(check_foc, 549, 163 + a * 31, c);
                    } else {
                        g.drawImage(i_check, 549, 163 + a * 31, c);
                    }
                }
            }
            g.translate(303 - x + 196, 73 - y);
        }

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

package com.alticast.illico.pvr.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.awt.FontMetrics;

import org.ocap.ui.event.OCRcEvent;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;

import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.issue.*;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.util.FontResource;

public class RecordingFailedPopup extends Popup {

	private OwnRenderer renderer = new OwnRenderer();

    FailedIssue issue;
    int failedReason;
    String[] desc;

	public RecordingFailedPopup(FailedIssue issue) {
        this.issue = issue;
        failedReason = issue.getReason();
		setRenderer(renderer);
		prepare();
		this.setBounds(0, 0, 960, 540);
		this.setVisible(false);

        String st = dataCenter.getString("fail." + (700 + failedReason));
        if (st.length() == 0) {
            st = dataCenter.getString("fail.700");
        }
        desc = TextUtil.split(st, GraphicsResource.FM17, 430, "|");
	}

	public boolean handleKey(int type){
        switch (type) {
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_B:
            if (Log.EXTRA_ON) {
                MetadataDiagScreen.getInstance().startRecording(issue.getRecording(), type == KeyCodes.COLOR_B);
            }
            return false;
        case KeyEvent.VK_UP:
            if (Log.EXTRA_ON) {
                failedReason++;
                String st = dataCenter.getString("fail." + (700 + failedReason));
                if (st.length() == 0) {
                    st = dataCenter.getString("fail.700");
                }
                desc = TextUtil.split(st, GraphicsResource.FM17, 430, "|");
            }
            break;
        case KeyEvent.VK_DOWN:
            if (Log.EXTRA_ON) {
                failedReason--;
                String st = dataCenter.getString("fail." + (700 + failedReason));
                if (st.length() == 0) {
                    st = dataCenter.getString("fail.700");
                }
                desc = TextUtil.split(st, GraphicsResource.FM17, 430, "|");
            }
            break;
        case KeyEvent.VK_LEFT:
        	if (focus == 1) {
                focus = 0;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_RIGHT:
        	if (focus == 0) {
                focus = 1;
        		repaint();
        	}
        	break;
        case KeyEvent.VK_ENTER:
            startClickingEffect();
        	switch (focus) {
        	case 0: // delete
                try {
                    issue.getRecording().delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	case 1: // cancel all
            	IssueManager.getInstance().stopPopup(this);
        		break;
        	}
        	repaint();
            break;
        case OCRcEvent.VK_EXIT:
        	IssueManager.getInstance().stopPopup(this);
            break;
        case KeyCodes.LAST:
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

	public void stop() {
		this.setVisible(false);
	}

    public void startClickingEffect() {
        if (focus == 0) {
            clickEffect.start(266 + 1, 384, 210, 32);
        } else if (focus == 1) {
            clickEffect.start(486 + 1, 384, 210, 32);
        }
    }

    class OwnRenderer extends Renderer {
        DataCenter dataCenter = DataCenter.getInstance();

        Image i_pop_566_bg;
        Image i_07_pro_Bg1;
        Image i_btn_210_l;
        Image i_btn_210_foc;

        String title, msgTop, programTitle, channel, date;

        Color cGray = new Color(150, 150, 150);

        FontMetrics fmText = FontResource.getFontMetrics(GraphicsResource.BLENDER18);

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);

            g.drawImage(i_pop_566_bg, 197, 80, c);
            g.drawImage(i_07_pro_Bg1, 250, 199, c);

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_255_203_0);
            GraphicUtil.drawStringCenter(g, title, 480, 137);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(Color.white);
            GraphicUtil.drawStringCenter(g, msgTop, 478, 179);

            g.setColor(GraphicsResource.C_210_210_210);
            int x = GraphicUtil.drawStringRight(g, date, 694, 223);

            g.setColor(Color.white);
            g.drawString(channel, 264, 223);
            g.drawString(TextUtil.shorten(programTitle, g.getFontMetrics(), x - 342 - 5), 342, 223);

            g.setColor(GraphicsResource.C_182_182_182);
            g.setFont(GraphicsResource.BLENDER17);
            int y = 267;
            for (int i = 0; i < Math.min(desc.length, 7); i++) {
                g.drawString(desc[i], 270, y);
                y += 17;
            }

            if (Log.EXTRA_ON) {
                g.setColor(Color.green);
                g.drawString(RecordManager.getFailedReasonString(failedReason) + " " + failedReason, 340, 195);
            }

            // buttons
            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_3_3_3);
            g.drawImage(focus == 0 ? i_btn_210_foc : i_btn_210_l, 266, 384, c);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.issue_btn_failed"), 371, 405);
            g.setColor(GraphicsResource.C_3_3_3);
            g.drawImage(focus == 1 ? i_btn_210_foc : i_btn_210_l, 486, 384, c);
            GraphicUtil.drawStringCenter(g, dataCenter.getString("pvr.Close"), 592, 405);
        }

        public void prepare(UIComponent c) {
            i_pop_566_bg = dataCenter.getImage("pop_566_bg.png");
            i_07_pro_Bg1 = dataCenter.getImage("07_pro_Bg1.png");
            i_btn_210_foc = dataCenter.getImage("btn_210_foc.png");
            i_btn_210_l = dataCenter.getImage("btn_210_l.png");

            if (failedReason == RecordManager.CANCELLED) {
                title = dataCenter.getString("pvr.cancelled_popup_title");
                msgTop = dataCenter.getString("pvr.cancelled_popup_msg");
            } else {
                title = dataCenter.getString("pvr.failed_popup_title");
                msgTop = dataCenter.getString("pvr.failed_popup_msg");
            }

            AbstractRecording rec = issue.getRecording();

            programTitle = rec.getTitle();
            channel = "CH " + rec.getString(AppData.CHANNEL_NUMBER);
            date = Formatter.getCurrent().getShortDate(rec.getScheduledStartTime());

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

    }



}

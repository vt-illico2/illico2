package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.util.*;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class ListDetailsPopup extends Popup {

    String[] titles;
    String popupTitle = dataCenter.getString("pvr.More Details");

    int type;

    Vector names = new Vector();
    Vector values = new Vector();

    long bps;   // for test

    public ListDetailsPopup(AbstractRecording r, int type) {
        this.type = type;
        titles = TextUtil.split(r.getTitle(), GraphicsResource.FM18, 330, 2);
        setRenderer(new PopupRenderer());
        if (type == ListPanel.RECORDED_LIST) {
            names.add(dataCenter.getString("List.Size"));
            values.add(StorageInfoManager.getSizeString(r.getRecordedSize()));
        } else {
            names.add(dataCenter.getString("List.Repeat"));
            values.add(r.getRepeatText());
        }
        names.add(dataCenter.getString("List.Save"));
        values.add(r.getSaveText());
        if (App.SUPPORT_HN) {
            names.add(dataCenter.getString("List.Save_Location"));
            values.add(r.getLocation());
        }
        
        if (r.getRecordedDuration() != 0) {
        	bps = r.getRecordedSize() * 8 * 1000 / r.getRecordedDuration();
        }
    }

    public boolean handleKey(int code) {
        switch (code) {
        case KeyEvent.VK_ENTER:
            startClickingEffect();
            close();
            break;
        case OCRcEvent.VK_EXIT:
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(403+1, 334, 154, 32);
    }

    private class PopupRenderer extends Renderer {

        Image bgglow_m;
        Image bgglow_b;
        Image bgglow_t;
        Image popup_sha;
        Image sep;
        Image high;

        Image focus;
        String ok = dataCenter.getString("pvr.OK");

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            g.drawImage(bgglow_m, 276, 145, 410, 211, c);
            g.drawImage(bgglow_b, 276, 356, c);
            g.drawImage(bgglow_t, 276, 90, c);

            g.drawImage(popup_sha, 304, 380, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(306, 120, 350, 263);

            g.drawImage(sep, 285, 158, c);
            g.drawImage(high, 306, 120, c);

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, popupTitle, 481, 146);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            for (int i = 0; i < titles.length; i++) {
                GraphicUtil.drawStringCenter(g, titles[i], 481, 187 + 21 * i);
            }

            g.setFont(GraphicsResource.BLENDER16);
            for (int i = 0; i < names.size(); i++) {
                g.setColor(GraphicsResource.C_182_182_182);
                g.drawString(names.elementAt(i).toString(), 344, 242 + i * 22);
                g.setColor(GraphicsResource.C_255_255_255);
                g.drawString(TextUtil.shorten(values.elementAt(i).toString(), g.getFontMetrics(), 180), 465, 242 + i * 22);
            }
            if (Log.EXTRA_ON) {
                g.setColor(java.awt.Color.green);
                g.drawString(bps + " bps", 344, 300);
            }
            g.setColor(GraphicsResource.C_4_4_4);
            g.setFont(GraphicsResource.BLENDER18);
            g.drawImage(focus, 403, 334, c);
            GraphicUtil.drawStringCenter(g, ok, 481, 355);
        }

        public void prepare(UIComponent c) {

            bgglow_m = DataCenter.getInstance().getImage("05_bgglow_m.png");
            bgglow_b = DataCenter.getInstance().getImage("05_bgglow_b.png");;
            bgglow_t = DataCenter.getInstance().getImage("05_bgglow_t.png");
            popup_sha = DataCenter.getInstance().getImage("05_popup_sha.png");
            sep = DataCenter.getInstance().getImage("05_sep.png");
            high = DataCenter.getInstance().getImage("05_high.png");
            focus = DataCenter.getInstance().getImage("05_focus.png");

            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }

}

package com.alticast.illico.pvr.popup;

import java.awt.*;
import javax.tv.util.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.config.PvrConfig;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.*;
import com.videotron.tvi.illico.log.Log;

public abstract class Popup extends UIComponent implements TVTimerWentOffListener {

    protected DataCenter dataCenter = DataCenter.getInstance();

    protected PopupListener listener;
    protected PopupContainer container;

    protected Effect showingEffect;
    protected ClickingEffect clickEffect;

    protected TVTimerSpec autoCloseTimer;
    protected boolean autoClose;

    public Popup() {
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.setDelayTime(5000L);
        autoCloseTimer.addTVTimerWentOffListener(this);
        autoClose = false;

        setBounds(Constants.SCREEN_BOUNDS);
        clickEffect = new ClickingEffect(this);
    }

    public synchronized void start(PopupContainer con) {
        container = con;
        this.start();
    }

    public synchronized void start() {
        startTimer();
        super.start();
    }

    public synchronized void stop() {
        stopTimer();
        super.stop();
    }

    public void close() {
        if (container == null) {
            container = PopupController.getInstance();
        }
        container.hidePopup(this);
    }

    public void notifyPowerOff() {
    }

    public void startEffect() {
        if (showingEffect == null) {
            showingEffect = new MovingEffect(this, 8,
                            new Point(0, 40), new Point(0, 0),
                            MovingEffect.FADE_IN, MovingEffect.ANTI_GRAVITY);
        }
        showingEffect.start();
    }

    public void setListener(PopupListener l) {
        listener = l;
    }

    public PopupListener getListener() {
        return listener;
    }

    public void animationEnded(Effect effect) {
        if (effect == showingEffect) {
            this.setVisible(true);
        }
    }

    public abstract void startClickingEffect();

    public void setDefaultAutoClose() {
        long autoCloseDelay = 0;
        PvrConfig config = (PvrConfig) dataCenter.get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config != null) {
            autoCloseDelay = config.ccNoticeTimeout * Constants.MS_PER_SECOND;
        }
        setAutoClose(autoCloseDelay);
    }

    public void setAutoClose(long time) {
        Log.printDebug("Popup.setAutoClose = " + time);
        if (time <= 0) {
            autoClose = false;
        } else {
            autoClose = true;
            autoCloseTimer.setDelayTime(time);
        }
    }

    protected synchronized void startTimer() {
        if (!autoClose) {
            return;
        }
        Log.printDebug("Popup.startTimer");
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    protected synchronized void stopTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
    }

    public synchronized void timerWentOff(TVTimerWentOffEvent event) {
        Log.printDebug("Popup.timerWentOff");
        if (listener != null) {
            listener.canceled();
        }
        close();
    }


}


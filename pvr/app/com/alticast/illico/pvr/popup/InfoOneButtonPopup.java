package com.alticast.illico.pvr.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import org.ocap.ui.event.OCRcEvent;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.PopupController;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.TextUtil;

public class InfoOneButtonPopup extends Popup {
    PopupRander popupRander = new PopupRander();

    public static final int SAVE = 0;

    String description;
    String title;
    String button = dataCenter.getString("pvr.OK");
    public int caller;

    public boolean fillBg = false;

    public InfoOneButtonPopup(String popupTitle) {
        this.title = popupTitle;
//        title = dataCenter.getString("pvr.NOTIFICATION");
        setRenderer(popupRander);
        prepare();
        this.setBounds(0, 0, 960, 540);
        this.setVisible(false);
    }

//    public void setTitle(String t) {
//        title = t;
//    }

    public void setDescription(String cont) {
        description = cont;
    }

    public void setButton(String b) {
        button = b;
    }

    public void start(int num) {
        fillBg = false;
        this.setVisible(true);
        caller = num;
    }

    public void stop() {
        this.setVisible(false);
    }

    public boolean handleKey(int code) {
        return keyPressed(code);
    }

    public boolean keyPressed(int type) {
        Log.printInfo("type = " + type);
        if (listener != null && listener.consumeKey(type)) {
            return true;
        }
        switch (type) {
        case KeyEvent.VK_UP:
        case KeyEvent.VK_DOWN:
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            return true;
        case KeyEvent.VK_ENTER:
            Log.printDebug(App.LOG_HEADER+"InfoOneButtonPopup.handlKey(VK_ENTER)");
            startClickingEffect();
        case OCRcEvent.VK_EXIT:
            if (listener != null) {
                listener.selected(0);
            }
            close();
            break;
        default:
            return false;
        }
        repaint();
        return true;
    }

    public void startClickingEffect() {
        clickEffect.start(403+1, 318, 154, 32);
    }

    private class PopupRander extends Renderer {

        Image pop_glow_m;
        Image pop_glow_b;
        Image pop_glow_t;

        Image pop_sha;
        Image pop_high_350;
        Image pop_gap_379;
        Image focus;

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        protected void paint(Graphics g, UIComponent c) {
            if (fillBg) {
                g.setColor(GraphicsResource.cDimmedBG);
                g.fillRect(0, 0, 960, 540);
            }
            g.drawImage(pop_glow_m, 276, 193, 410, 124, c);
            g.drawImage(pop_glow_b, 276, 317, 410, 124, c);
            g.drawImage(pop_glow_t, 276, 113, 410, 124, c);

            g.drawImage(pop_sha, 306, 364, 350, 79, c);

            g.setColor(GraphicsResource.C_35_35_35);
            g.fillRect(306, 143, 350, 224);

            g.drawImage(pop_high_350, 306, 143, c);
            g.drawImage(pop_gap_379, 285, 181, c);

            g.setFont(GraphicsResource.BLENDER24);
            g.setColor(GraphicsResource.C_252_202_4);
            GraphicUtil.drawStringCenter(g, title, 481, 169);

            g.setFont(GraphicsResource.BLENDER18);
            g.setColor(GraphicsResource.C_255_255_255);
            String[] str = TextUtil.split(description, g.getFontMetrics(), 300);

            int sy = 241 + 18 - str.length * 9;
            for (int a = 0; a < str.length; a++) {
                GraphicUtil.drawStringCenter(g, str[a], 483, sy);
                sy += 18;
            }

            g.setColor(GraphicsResource.C_4_4_4);
            g.drawImage(focus, 403, 318, c);
            GraphicUtil.drawStringCenter(g, button, 481, 339);
        }

        public void prepare(UIComponent c) {

            pop_glow_m = DataCenter.getInstance().getImage("05_pop_glow_m.png");
            pop_glow_b = DataCenter.getInstance().getImage("05_pop_glow_b.png");
            pop_glow_t = DataCenter.getInstance().getImage("05_pop_glow_t.png");

            pop_sha = DataCenter.getInstance().getImage("pop_sha.png");

            pop_high_350 = DataCenter.getInstance().getImage("pop_high_350.png");
            pop_gap_379 = DataCenter.getInstance().getImage("pop_gap_379.png");
            focus = DataCenter.getInstance().getImage("05_focus.png");
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }
    }
}

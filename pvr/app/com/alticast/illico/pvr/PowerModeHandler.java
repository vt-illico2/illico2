package com.alticast.illico.pvr;

import org.ocap.dvr.*;
import org.ocap.resource.*;
import org.ocap.hardware.*;
import org.ocap.shared.dvr.*;
import javax.tv.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.util.Constants;

public class PowerModeHandler implements PowerModeChangeListener, TVTimerWentOffListener {

    private static final PowerModeHandler instance = new PowerModeHandler();

    public static PowerModeHandler getInstance() {
        return instance;
    }

    TVTimerSpec stopBufferingTimer;

    private PowerModeHandler() {
        stopBufferingTimer = new TVTimerSpec();
    }

    public void init() {
        Host.getInstance().addPowerModeChangeListener(this);

        stopBufferingTimer = new TVTimerSpec();
        stopBufferingTimer.setTime(
                DataCenter.getInstance().getLong("LOW_POWER_CANCEL_BUFFERING_DELAY", Constants.MS_PER_MINUTE));
        stopBufferingTimer.setRepeat(false);
        stopBufferingTimer.setAbsolute(false);
        stopBufferingTimer.addTVTimerWentOffListener(this);
    }

    public void dispose() {
        Host.getInstance().removePowerModeChangeListener(this);
        stopBufferingTimer.removeTVTimerWentOffListener(this);
    }

    public void powerModeChanged(int newPowerMode) {
        if (newPowerMode == Host.LOW_POWER) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged(LOW_POWER) : Call PopupController.notifyPowerOff()");
            PopupController.getInstance().notifyPowerOff();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged(LOW_POWER) : Call startTimer()");
            startTimer();
        } else {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.powerModeChanged(LOW_POWER) : Call stopTimer()");
            stopTimer();
        }
    }

    private synchronized void startTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(stopBufferingTimer);
        try {
            stopBufferingTimer = timer.scheduleTimerSpec(stopBufferingTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    private synchronized void stopTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(stopBufferingTimer);
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PowerModeHandler.timerWentOff() : Call RecordManager.cancelAllBufferingRequests()");
        RecordManager.getInstance().cancelAllBufferingRequests();
    }
}

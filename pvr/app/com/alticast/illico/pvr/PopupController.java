package com.alticast.illico.pvr;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.rmi.RemoteException;

import org.dvb.event.UserEvent;
import org.ocap.shared.dvr.AccessDeniedException;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.navigation.RecordingList;
import org.ocap.shared.media.TimeShiftControl;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.config.PvrConfig;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.gui.BottomPopupRenderer;
import com.alticast.illico.pvr.ui.PvrFooter;
import com.alticast.illico.pvr.list.*;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.RemoteIterator;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.mainmenu.*;

public class PopupController extends Container implements LayeredKeyHandler, PopupContainer {
    public static final int POPUP_RECORD_PAST_PROGRAM = 1;
    public static final int POPUP_RECORD_REPEAT = 2;
    public static final int POPUP_RECORD_SERIES = 3;
    public static final int POPUP_RECORD_SINGLE = 4;

    public static final int POPUP_DUPLICATED_RECORDING = 5;
    public static final int POPUP_PAST_NOT_AVAILABLE = 6;

    public static final int POPUP_FUTURE_RECORDING_CANCEL = 7;
//    static final int POPUP_RECORDING_CANCEL = 8;
    public static final int POPUP_RECORDING_CANCEL_WATCH_TV = 9;

    private static final long serialVersionUID = 1L;
    LayeredWindow window;
    LayeredUI popupUI;

    Popup current;

    TvProgram tvProgram;
    RecordingRequest req;

    private static DataCenter dataCenter = DataCenter.getInstance();
    private static PopupController instance = new PopupController();

    String channelNumber;

    public static PopupController getInstance() {
        return instance;
    }

    private PopupController() {
        window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.setVisible(true);
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);
        window.add(this);
        popupUI = WindowProperty.PVR_POPUP.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        popupUI.deactivate();
    }

    public void setChannelNumber(String chNumber) {
        channelNumber = chNumber;
    }

    boolean isVisiblePinPopup = false;

    public void showRecordingPopup(TvChannel ch, TvProgram p, int popupId) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showRecordingPopup()");
        showRecordingPopup(ch, p, popupId, false);
    }

    public synchronized void showRecordingPopup(TvChannel ch, TvProgram p, int popupId, boolean showOption) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showRecordingPopup() -- 2");
        if (isVisiblePinPopup) {
            Log.printWarning("PopupController.showPopup: waiting PIN. Skip new popup = " + popupId);
            return;
        }
        tvProgram = p;
        Popup newPopup;
        switch (popupId) {
        case POPUP_RECORD_REPEAT:
            {
            GeneralRecordingPopup popup = new GeneralRecordingPopup(RecordingOption.TYPE_REPEAT);
            popup.start(ch, p, showOption);
            newPopup = popup;
            }
            break;
        case POPUP_RECORD_SERIES:
            {
            GeneralRecordingPopup popup = new GeneralRecordingPopup(RecordingOption.TYPE_SERIES);
            popup.start(ch, p, showOption);
            newPopup = popup;
            }
            break;
        case POPUP_RECORD_SINGLE:
            {
            GeneralRecordingPopup popup = new GeneralRecordingPopup(RecordingOption.TYPE_SINGLE);
            popup.start(ch, p, showOption);
            newPopup = popup;
            }
            break;
        case POPUP_RECORD_PAST_PROGRAM:
            newPopup = new PastRecordingPopup();
            break;
        case POPUP_DUPLICATED_RECORDING:
            InfoOneButtonPopup op = new InfoOneButtonPopup(dataCenter.getString("pvr.dup_rec_title"));
            String title;
            boolean isBlocked;
            try {
                isBlocked = Core.epgService.isAdultBlocked(p);
            } catch (Exception ex) {
                isBlocked = false;
            }
            if (isBlocked) {
                title = dataCenter.getString("List.blocked");
            } else {
                try {
                    title = p.getTitle();
                } catch (Exception ex) {
                    title = " ";
                }
            }
            String desc = TextUtil.replace(dataCenter.getString("pvr.dup_rec_msg"), "%1", title);
            op.setDescription(desc);
            newPopup = op;
            break;
        case POPUP_PAST_NOT_AVAILABLE:
            InfoOneButtonPopup opp = new InfoOneButtonPopup(dataCenter.getString("pvr.cannot_past_title"));
            opp.setDescription(dataCenter.getString("pvr.cannot_past_msg"));
            newPopup = opp;
            break;
        default:
            Log.printWarning("PopupController.showPopup: not found id = " + popupId);
            return;
        }
        //->Kenneth[2015.7.6] Tank
        if (Core.mainMenuService != null) {
            try {
                Log.printDebug(App.LOG_HEADER+"PopupController.showRecordingPopup() : Call MainMenu.hideMenu()");
                Core.mainMenuService.hideMenu();
            } catch (Exception e) {
                Log.print(e);
            }
        }
        //<-
        showPopup(newPopup);
    }

    public void showCancelPopup(TvProgram p, RecordingRequest r, int popupId) {
        showCancelPopup(p, r, popupId, true);
    }

    private synchronized void showCancelPopup(TvProgram pr, final RecordingRequest r, int popupId, boolean checkProtected) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup()");
        if (isVisiblePinPopup) {
            Log.printWarning("PopupController.showPopup: waiting PIN. Skip new popup = " + popupId);
            return;
        }
        // 아래에서 복사했다.
        tvProgram = pr;
        req = r;
        Popup newPopup;
        if (checkProtected && isProtected(pr)) {
            isVisiblePinPopup = true;
            String msg = dataCenter.getString(popupId == POPUP_FUTURE_RECORDING_CANCEL ? "pin.delete" : "pin.stop");
            PreferenceProxy.getInstance().showPinEnabler(msg, new PinEnabler(pr, r, popupId));
            return;
        }
        isVisiblePinPopup = false;

        //->Kenneth[2016.3.8] R7 : Ungrouped 에서는 getSisterProgramRecordingRequest 가 null 을 리턴할 것이므로
        // 여기에서 특별히 다른 처리는 하지 않는다.

        //->Kenneth[2015.5.25] R5 : SD/HD Grouping 이 Grid EPG 에 추가되면서 변경되는 사항
        // EPG 뜬 상태에서 Recording 취소시 Sister 채널 Recording 이 있으면 같이 취소되어야 함.
        final RecordingRequest sisterReq = RecordManager.getInstance().getSisterProgramRecordingRequest(pr);
        Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : sisterReq = "+sisterReq);
        //<-

        switch (popupId) {
        case POPUP_FUTURE_RECORDING_CANCEL:
            ListSelectPopup dp = new ListSelectPopup();
            String fileName = null;
            Object fileNameTemp = AppData.get(req, AppData.TITLE);
            if (fileNameTemp != null) {
                fileName = (String) fileNameTemp;
            } else {
                fileName = "";
            }
            PopupListener deleteListener = new PopupAdapter() {

                public boolean consumeKey(int code) {
                    switch (code) {
                        case KeyEvent.VK_PAGE_UP:
                        case KeyEvent.VK_PAGE_DOWN:
                        case OCRcEvent.VK_GUIDE:
                        case KeyCodes.SEARCH:
                        case KeyCodes.WIDGET:
                            return true;
                        default:
                            return false;
                    }
                }

                public void selected(int focus) {
                    if (focus == 0) {
                        try {
                            Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_FUTURE_RECORDING_CANCEL : Call r.delete()");
                            r.delete();
                            //->Kenneth[2015.5.25] R5 : sisterReq 도 같이 취소되어야 한다.
                            Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_FUTURE_RECORDING_CANCEL : sisterReq = "+sisterReq);
                            if (sisterReq != null && Core.getInstance().isEpgLaunched()) {
                                Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_FUTURE_RECORDING_CANCEL : Call sisterReq.delete()");
                                sisterReq.delete();
                            }
                        } catch (Exception e1) {
                            Log.print(e1);
                        }
                    }
                    currentSceneStop();
                }
            };
            dp.setListener(deleteListener);
            String deleteFileMessage = dataCenter.getString("pvr.cancel_popup_msg");
            String msg = deleteFileMessage + "\n'" + fileName + "' ?";
            dp.setPopupTitle(dataCenter.getString("pvr.cancel_popup_title"));
            dp.setDescription(msg);
//            dp.setKeyUp(); // for focus reset
            newPopup = dp;
            break;
        case POPUP_RECORDING_CANCEL_WATCH_TV:
            BottomPopup p = new BottomPopup(268);
            p.footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");

            PopupListener cancelListener = new PopupAdapter() {
                public boolean consumeKey(int code) {
                    switch (code) {
                        case KeyEvent.VK_PAGE_UP:
                        case KeyEvent.VK_PAGE_DOWN:
                        case OCRcEvent.VK_GUIDE:
                        case KeyCodes.SEARCH:
                        case KeyCodes.WIDGET:
                            return true;
                        default:
                            return false;
                    }
                }

                public void selected(final int focus) {
                    Thread t = new Thread("pvr.cancel") {
                        public void run() {
                            switch (focus) {
                            case 0: // stop & save
                                Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Stop & Save : Call r.stop()");
                                try {
                                    ((LeafRecordingRequest) r).stop();
                                    //->Kenneth[2015.5.25] R5 : sisterReq 도 같이 취소되어야 한다.
                                    Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV: sisterReq = "+sisterReq);
                                    if (sisterReq != null && Core.getInstance().isEpgLaunched()) {
                                        Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Stop & Save : Call sisterReq.stop()");
                                        ((LeafRecordingRequest)sisterReq).stop();
                                    }
                                } catch (AccessDeniedException e1) {
                                    Log.print(e1);
                                }
                                break;
                            case 1: // stop & delete
                                Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Stop & Delete : Call RecordManager.deleteIfNotUsing(r)");
                                boolean result = RecordManager.deleteIfNotUsing(r);
                                //->Kenneth[2015.5.25] R5 : sisterReq 도 같이 취소되어야 한다.
                                Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : sisterReq = "+sisterReq);
                                if (sisterReq != null && Core.getInstance().isEpgLaunched()) {
                                    Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Stop & Delete : Call RecordManager.deleteIfNotUsing(sisterReq)");
                                    boolean sisterResult = RecordManager.deleteIfNotUsing(sisterReq);
                                    Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Stop & Delete : sisterResult = "+sisterResult);
                                }
                                currentSceneStop();
                                if (!result) {
                                    Log.printDebug(App.LOG_HEADER+"PopupController.showCancelPopup() : POPUP_RECORDING_CANCEL_WATCH_TV : Call showCannotDeletePopup()");
                                    showCannotDeletePopup(r, PopupController.this);
                                }
                                return;
                            }
                            currentSceneStop();
                        }
                    };
                    t.start();
                }
            };
            p.setListener(cancelListener);
            String[] texts = new String[2];
            texts[0] = "'" + AppData.getString(r, AppData.TITLE) + "' "
                    + dataCenter.getString("pvr.stop_recording_msg_1");
            texts[1] = dataCenter.getString("pvr.stop_recording_msg_2");
            p.texts = texts;
            p.title = dataCenter.getString("pvr.stop_recording");
            p.buttons = new String[] {dataCenter.getString("pvr.stop_and_save"), dataCenter.getString("pvr.stop_and_delete")};
            newPopup = p;
            break;
        default:
            return;
        }
        showPopup(newPopup);
    }

    public synchronized void showPopup(Popup popup) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showPopup()");
        Core.resetScreenSaverTimer();
        currentSceneStop();
        removeAll();
//        popup.prepare();
        popup.start();
        popup.setVisible(false);
        this.add(popup);
        popupUI.activate();
        current = popup;
        popup.startEffect();
        Log.printDebug("PopupController.showPopup: current = " + current);
    }

    //->Kenneth[2017.7.15] R7.3 : 팝업 관련 수정
    public void showPlayStopPopup(String title, PopupListener l) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showPlayStopPopup("+title+")");
        ButtonListPopup p = new ButtonListPopup(3);

        PvrFooter left = new PvrFooter(PvrFooter.ALIGN_LEFT, false);
        left.addButton(PreferenceService.BTN_EXIT, "pvr.Live_TV");
        PvrFooter right = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
        right.addButton(PreferenceService.BTN_LIST, "pvr.Return_list");
        left.setBounds(315, 378, 150, 30);
        right.setBounds(488, 378, 161, 30);
        p.add(left);
        p.add(right);

        p.setTitle(title);
        p.setButtons(new String[] {"pvr.resume_viewing", "pvr.play_from_beginning", "pvr.delete"});
        p.setListener(l);

        showPopup(p);
    }
    /*
    public void showPlayStopPopup(PopupListener l) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showPlayStopPopup()");
        ButtonListPopup p = new ButtonListPopup(3);

        PvrFooter left = new PvrFooter(PvrFooter.ALIGN_LEFT, false);
        left.addButton(PreferenceService.BTN_EXIT, "pvr.Live_TV");
        PvrFooter right = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
        right.addButton(PreferenceService.BTN_LIST, "pvr.Return_list");
        left.setBounds(315, 378, 150, 30);
        right.setBounds(488, 378, 161, 30);
        p.add(left);
        p.add(right);

        p.setTitle(DataCenter.getInstance().getString("pvr.stopped_title"));
        p.setButtons(new String[] {"pvr.continue_view", "pvr.play_begin", "pvr.delete"});
        p.setListener(l);

        showPopup(p);
    }
    */
    //<-

    public void showPlayFinishedPopup(String title, PopupListener l) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showPlayFinishedPopup()");
        synchronized (this) {
            Popup popup = current;
            if (popup != null && popup instanceof ButtonListPopup) {
                PopupListener pl = popup.getListener();
                if (pl != null && pl.getClass().equals(l.getClass())) {
                    Log.printError("PopupController.showPlayFinishedPopup: duplicated : " + pl.getClass());
                    return;
                }
            }
        }

        ButtonListPopup p = new ButtonListPopup(2);

        PvrFooter left = new PvrFooter(PvrFooter.ALIGN_LEFT, false);
        left.addButton(PreferenceService.BTN_EXIT, "pvr.Live_TV");
        PvrFooter right = new PvrFooter(PvrFooter.ALIGN_RIGHT, false);
        right.addButton(PreferenceService.BTN_LIST, "pvr.Return_list");
        left.setBounds(315, 378, 150, 30);
        right.setBounds(488, 378, 161, 30);
        p.add(left);
        p.add(right);

        //->Kenneth[2017.3.15] R7.3
        p.setTitle(title);
        p.setButtons(new String[] {"pvr.delete", "pvr.play_from_beginning"});
        /*
        title = TextUtil.shorten(title, GraphicsResource.FM18, 195);
        p.setTitle(dataCenter.getString("pvr.finished_title"));
        dataCenter.put("delete program", dataCenter.getString("pvr.delete") + " " + title);
        p.setButtons(new String[] {"delete program", "pvr.play_begin"});
        */
        //<-

        p.setListener(l);
        showPopup(p);
    }

    public Popup showSaveBuffer(final ChannelChangeRequestor requestor) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showSaveBuffer()");
        if (current != null && current instanceof RecordingNoticePopup) {
            Log.printWarning("PopupController.showSaveBuffer: recording notice popup is shown !");
            return null;
        }

        final BottomPopup p = new BottomPopup() {
            public void stop() {
                super.stop();
                synchronized (requestor) {
                    requestor.notifyAll();
                }
            }
        };
        p.footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");

        TunerController tc = Core.getInstance().getTunerController(requestor.videoIndex);
        TimeShiftControl tsc = tc.getControl();
        final long bb;
        final long eb;
        if (tsc != null) {
            bb = PvrUtil.getTime(tsc.getBeginningOfBuffer());
            eb = PvrUtil.getTime(tsc.getEndOfBuffer());
        } else {
            bb = 0;
            eb = 0;
        }

        final Thread saveThread = new Thread() {
            public void run() {
                p.texts = new String[] {dataCenter.getString("pvr.sb_msg_wait")};
                p.buttons = null;
                p.repaint();

                try {
                    if (Environment.EMULATOR) {
                        sleep(3000L);
                    } else {
                        RecordManager.getInstance().saveBuffer(requestor.videoIndex, requestor.current, bb, eb);
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }

                requestor.result = true;
                currentSceneStop();
            }
        };

        PopupListener cancelListener = new PopupAdapter() {

            public boolean consumeKey(int code) {
                if (p.buttons == null) {
                    return true;
                }
                return false;
            }

            public void selected(int focus) {
                switch (focus) {
                case 0: // save & change channel
                    requestor.result = true;
                    saveThread.start();
                    break;
                case 1: // don't save & change channel
                    requestor.result = true;
                    currentSceneStop();
                    break;
                }
            }
        };

        int min = (int) Math.ceil((double) (eb - bb) / Constants.MS_PER_MINUTE);

        p.setListener(cancelListener);
        String[] texts = new String[2];
        texts[0] = dataCenter.getString("pvr.sb_msg_1");
        texts[1] = TextUtil.replace(dataCenter.getString("pvr.sb_msg_2"), "%%", String.valueOf(min));
        p.texts = texts;
        p.title = dataCenter.getString("pvr.sb_title");
        p.buttons = new String[] {dataCenter.getString("pvr.sb_btn_1"), dataCenter.getString("pvr.sb_btn_2")};
        showPopup(p);
        return p;
    }

    public void showChannelChangeNotice(final ChannelChangeRequestor requestor) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showChannelChangeNotice()");
        final BottomPopup p = new BottomPopup() {
            public void stop() {
                super.stop();
                synchronized (requestor) {
                    requestor.notifyAll();
                }
            }

            public void notifyPowerOff() {
                Log.printInfo("ChannelChangeNotice.notifyPowerOff");
                currentSceneStop();
            }
        };
        p.footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");
        String title = null;
        final LeafRecordingRequest req = RecordManager.getInstance().getRequest(requestor.current);
        if (req != null) {
            title = ParentalControl.getTitle(req);
        }
        if (title == null) {
            try {
                title = Core.epgService.getProgram(requestor.current, System.currentTimeMillis()).getTitle();
            } catch (Exception ex) {
            }
        }
        if (title == null) {
            try {
                title = tvProgram.getTitle();
            } catch (Exception ex) {
                title = " ";
            }
        }

        PopupPinAdapter cancelListener = new PopupPinAdapter() {

            public void notifyPinResult(boolean checked) {
                if (checked) {
                    stopAndChange();
                }
            }

            public void selected(int focus) {
                switch (focus) {
                case 0: // don't change channel
                    requestor.result = false;
                    currentSceneStop();
                    break;
                case 1: // stop recording & change channel
                    if (ParentalControl.isProtected()) {
                        p.showPinEnabler(dataCenter.getString("pin.stop"), this);
                    } else {
                        stopAndChange();
                    }
                    break;
                default:
                    currentSceneStop();
                    break;
                }
            }

            private void stopAndChange() {
                LeafRecordingRequest req = RecordManager.getInstance().getRequest(requestor.current);
                if (req == null) {
                    Log.printWarning("ChannelChangeNotice: Not found recording request for " + current);
                    // TODO - 개선
//                        TunerResourceManager.getInstance().setDropObject(
//                                Environment.getServiceContext(requestor.tunerIndex));
                } else {
                    AppData.set(req, AppData.UNSET, Boolean.TRUE);
                    // TODO - 개선
//                        TunerResourceManager.getInstance().setDropObject(req);
                    try {
                        req.stop();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
                requestor.result = true;
                currentSceneStop();
            }
        };
        p.setListener(cancelListener);

        String msg0 = dataCenter.getString("pvr.cc_notice_msg_1");

        int width = BottomPopupRenderer.fmText.stringWidth(msg0);
        title = TextUtil.shorten(title, BottomPopupRenderer.fmText, BottomPopupRenderer.MAX_TEXT_WIDTH - width);
        msg0 = TextUtil.replace(msg0, "%%", title);

        String[] texts = new String[2];
        texts[0] = msg0;
        texts[1] = dataCenter.getString("pvr.cc_notice_msg_2");
        p.texts = texts;
        p.title = dataCenter.getString("pvr.cc_notice_title");
        p.buttons = new String[] {dataCenter.getString("pvr.cc_notice_btn_1"),
                dataCenter.getString("pvr.cc_notice_btn_2")};
        showPopup(p);
    }

    /** Recording about to start. from resourceContentionWarning. */
    public void showRecordingNotice(final RecordingRequest r, final int tunerIndex) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showRecordingNotice()");
        if (current != null && current instanceof RecordingNoticePopup) {
            Log.printWarning("PopupController.showRecordingNotice: recording notice popup is shown !");
            return;
        }
        final RecordingNoticePopup p = new RecordingNoticePopup(r, tunerIndex);

        PopupListener cancelListener = new PopupAdapter() {
            public boolean handleHotKey(int code) {
                if (code == OCRcEvent.VK_EXIT) {
                    p.tuneAndRecord();
                    return true;
                }
                return false;
            }

            public void selected(int focus) {
                switch (focus) {
                case 0: // tune and record
                    p.tuneAndRecord();
                    break;
                case 1: // Don't record
                    LeafRecordingRequest req = (LeafRecordingRequest) r;
                    try {
                        req.delete();
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                    break;
                }
                currentSceneStop();
            }

        };
        p.setListener(cancelListener);
        showPopup(p);
    }

    public void showTunerRequest(final LocalRecording[] array, final TunerRequestor requestor) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showTunerRequest()");
        final BottomPopup p = new BottomPopup() {
            public void stop() {
                super.stop();
                synchronized (requestor) {
                    requestor.notifyAll();
                }
            }

            public void notifyPowerOff() {
                Log.printInfo("TunerRequest.notifyPowerOff");
                currentSceneStop();
            }
        };
        p.footer.addButton(PreferenceService.BTN_EXIT, "pvr.Cancel");

        PopupPinAdapter cancelListener = new PopupPinAdapter() {
            int selectedFocus;

            public void notifyPinResult(boolean result) {
                if (result) {
                    stop(array, requestor, selectedFocus, p);
                }
            }

            private void stop(LocalRecording[] array, TunerRequestor requestor, int focus, BottomPopup p) {
                if (requestor.type == TunerRequestor.DUAL_TUNER_WITH_CONFIRM) {
                    showStopOption(array, requestor, focus, p);
                } else {
                    LocalRecording r = array[focus];
                    r.setAppData(AppData.UNSET, Boolean.TRUE);
                    r.stop();
                    requestor.result = true;
                    currentSceneStop();
                }
            }

            public void selected(int focus) {
                selectedFocus = focus;
                switch (focus) {
                case 0:
                case 1:
                    if (ParentalControl.isProtected()) {
                        p.showPinEnabler(dataCenter.getString("pin.stop"), this);
                    } else {
                        stop(array, requestor, focus, p);
                    }
                    break;
                default:
                    currentSceneStop();
                    break;
                }
            }
        };
        p.setListener(cancelListener);

        String btnTxt = dataCenter.getString("pvr.tuner_request_btn");

        String[] buttons = new String[2];
        buttons[0] = btnTxt + " \"" + TextUtil.shorten(ParentalControl.getTitle(array[0]), BottomPopupRenderer.fmButton, 190) + "\"";
        buttons[1] = btnTxt + " \"" + TextUtil.shorten(ParentalControl.getTitle(array[1]), BottomPopupRenderer.fmButton, 190) + "\"";

        String[] texts = new String[2];
        texts[0] = dataCenter.getString("pvr.tuner_request_msg_1");
        texts[1] = dataCenter.getString("pvr.tuner_request_msg_2");
        p.texts = texts;
        p.title = dataCenter.getString("pvr.tuner_request_title");
        p.buttons = buttons;
        showPopup(p);
    }

    private void showStopOption(final LocalRecording[] array, final TunerRequestor requestor, int index, final BottomPopup p) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showStopOption()");
        final ListThreeButtonPopup popup = new ListThreeButtonPopup(dataCenter.getString("pvr.stop_recording"));
        popup.setButtonMessage(dataCenter.getString("pvr.stop_and_save"), dataCenter.getString("pvr.stop_and_delete"),
                               dataCenter.getString("pvr.continue_recording"));
        final LocalRecording r = array[index];
        String msg = '\"' + ParentalControl.getTitle(r) + "\" " + TextUtil.replace(dataCenter.getString("pvr.is_recording_stop_msg"), "|", "\n");
        popup.setMessage(msg);

        PopupAdapter listener = new PopupAdapter() {
            public void selected(int focus) {
                boolean result = true;
                switch (focus) {
                    case 0:
                        r.stop();
                        break;
                    case 1:
                        result = r.deleteIfPossible();
//                        showDeletePopup(r);
                        break;
                    default:
                        p.hidePopup(popup);
                        return;
                }
                requestor.result = result;
                synchronized (requestor) {
                    requestor.notifyAll();
                }
                currentSceneStop();
                if (!result) {
                    Log.printDebug(App.LOG_HEADER+"PopupController.showStopOption() : Call showCannotDeletePopup()");
                    showCannotDeletePopup(r, PopupController.this);
                }
            }
        };
        popup.setListener(listener);
        p.showPopup(popup);
    }

    public static void showCannotDeletePopup(AbstractRecording r, PopupContainer pc) {
        showCannotDeletePopup(ParentalControl.getTitle(r), r.getScheduledStartTime(), pc, null);
    }

    public static void showCannotDeletePopup(RecordingRequest r, PopupContainer pc) {
        showCannotDeletePopup(r, pc, null);
    }

    public static void showCannotDeletePopup(RecordingRequest r, PopupContainer pc, PopupListener l) {
        showCannotDeletePopup(ParentalControl.getTitle(r), AppData.getScheduledStartTime(r), pc, l);
    }

    private static void showCannotDeletePopup(String title, long time, PopupContainer pc, PopupListener l) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showCannotDeletePopup("+title+")");
        InfoOneButtonPopup popup = new InfoOneButtonPopup(dataCenter.getString("multiroom.cannot_delete_title"));
        popup.fillBg = false;
        Formatter fm = Formatter.getCurrent();
        StringBuffer sb = new StringBuffer(200);
        sb.append(TextUtil.shorten(title, GraphicsResource.FM18, 190));
        sb.append(' ');
        sb.append('(');
        sb.append(fm.getDayText(time));
        sb.append(')');
        sb.append('\n');
        sb.append(' ');
        sb.append('\n');
        sb.append(dataCenter.getString("multiroom.cannot_delete_msg"));
        popup.setDescription(sb.toString());
        popup.setListener(l);
        pc.showPopup(popup);
    }

    public static void showCannotDeletePopup(int failedCount, PopupContainer pc) {
        Log.printDebug(App.LOG_HEADER+"PopupController.showCannotDeletePopup("+failedCount+")");
        InfoOneButtonPopup popup = new InfoOneButtonPopup(dataCenter.getString("multiroom.cannot_delete_multi_title"));
        popup.fillBg = false;
        popup.setDescription(dataCenter.getString("multiroom.cannot_delete_multi_msg"));
        pc.showPopup(popup);
    }

    public synchronized void hidePopup(Popup toStop) {
        Log.printDebug(App.LOG_HEADER+"PopupController.hidePopup("+toStop+")");
        if (toStop == current) {
            currentSceneStop(toStop);
            return;
        }

        if (toStop != null) {
            toStop.stop();
            remove(toStop);
        }
        if (getComponentCount() == 0 && popupUI != null) {
            current = null;
            popupUI.deactivate();
        }
    }

    public Popup getCurrent() {
        return current;
    }

    public synchronized void currentSceneStop() {
        if (current != null) {
            currentSceneStop(current);
        }
    }

    private synchronized void currentSceneStop(Popup c) {
        Log.printDebug(App.LOG_HEADER+"PopupController.currentSceneStop("+c+")");
        if (current == c) {
            current.stop();
            remove(current);
            current = null;
            if (getComponentCount() == 0 && popupUI != null) {
                current = null;
                popupUI.deactivate();
            } else {
                current = (Popup) getComponent(0);
            }
        } else {
            c.stop();
        }
    }

    synchronized void notifyPowerOff() {
        Log.printDebug(App.LOG_HEADER+"PopupController.notifyPowerOff()");
        if (current != null) {
            current.notifyPowerOff();
        }
    }

    public void dispose() {
        Log.printDebug(App.LOG_HEADER+"PopupController.dispose()");
        LayeredUIManager.getInstance().disposeLayeredUI(popupUI);
    }

    public boolean handleKeyEvent(UserEvent e) {
        Log.printDebug(App.LOG_HEADER+"PopupController.handleKeyEvent("+e+")");
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }

        int code = e.getCode();
        if (Log.DEBUG_ON) {
            Log.printDebug("PopupController: code = " + code);
        }
        if (current != null && current.isVisible()) {
            boolean result = current.handleKey(code);
            if (result) {
                return true;
            }
        }
        // moved to here, because some popup uses EXIT key.
        switch (code) {
        case KeyCodes.LAST:
            return true;
        case OCRcEvent.VK_EXIT:
            currentSceneStop();
            return true;
        case KeyCodes.COLOR_A:
        case KeyCodes.COLOR_B:
        case KeyCodes.COLOR_C:
        case KeyCodes.COLOR_D:
        case OCRcEvent.VK_INFO:
            return true;
        //->Kenneth[2015.7.20] Tank : 팝업 밑으로 menu 뜨는 거 방지
        case KeyCodes.MENU:
            return true;
        //<-
        //->Kenneth[2015.8.24] Tank : 팝업 밑으로 * popup 뜨는 거 방지
        case KeyCodes.STAR:
            return true;
        //<-
        }
        return false;
    }

    public void paint(Graphics g) {
        boolean fillBg = Core.getInstance().getMonitorState() != MonitorService.TV_VIEWING_STATE;
        if (App.R3_TARGET) {
            // Note - R3 에서는 투명처리 되기 때문에...
            fillBg = fillBg || !"EPG".equals(Core.getInstance().getVideoContextName());
        }
        if (fillBg) {
            g.setColor(GraphicsResource.cDimmedBG);
            g.fillRect(0, 0, 960, 540);
        }
        super.paint(g);
    }

    private boolean isProtected(TvProgram p) {
        EpgService es = (EpgService) DataCenter.getInstance().get(EpgService.IXC_NAME);
        boolean pinCheck = ParentalControl.isProtected();
        try {
            pinCheck = pinCheck || (p != null && es.isAdultBlocked(p));
        } catch (Exception re) {
            Log.print(re);
        }
        Log.printDebug("PopupController.isProtected = " + pinCheck);
        return pinCheck;
    }

    abstract class PopupPinAdapter extends PopupAdapter implements PinEnablerListener {
        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug(App.LOG_HEADER+"PopupController.popupPinAdapter.receivePinEnablerResult("+response+")");
            notifyPinResult(PreferenceService.RESPONSE_SUCCESS == response);
        }

        public abstract void notifyPinResult(boolean checked);
    }

    class PinEnabler implements PinEnablerListener {
        TvProgram program;
        RecordingRequest request;
        int popupId;
        public PinEnabler(TvProgram p, RecordingRequest req, int id) {
            this.program = p;
            this.request = req;
            this.popupId = id;
        }

        public void receivePinEnablerResult(int response, String detail) throws RemoteException {
            Log.printDebug(App.LOG_HEADER+"PopupController.PinEnabler: result = " + response + " detail = " + detail);
            isVisiblePinPopup = false;
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                Log.printDebug(App.LOG_HEADER+"PopupController.PinEnabler: Call showCancelPopup()");
                showCancelPopup(program, request, popupId, false);
            }
        }
    }

}

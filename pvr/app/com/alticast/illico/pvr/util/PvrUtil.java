package com.alticast.illico.pvr.util;

import javax.media.Time;
import java.util.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.data.AppData;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.ixc.epg.*;
import org.ocap.shared.dvr.RecordingRequest;

public class PvrUtil {

    public static final String SAVE_MAX = "pvr.until_i_erase";
    public static final String SAVE_7   = "pvr.7days";
    public static final String SAVE_14  = "pvr.14days";
    public static final String LAST_MAX = "pvr.keep_all";
    public static final String LAST_3   = "pvr.last3";
    public static final String LAST_5   = "pvr.last5";
    public static final String[] SAVE_RECORDING = new String[] { SAVE_MAX, SAVE_7, SAVE_14 };
    public static final String[] SAVE_SERIES    = new String[] { LAST_MAX, LAST_3, LAST_5 };

    public static final String MORE_DAYS = "pvr.More Days";

    public static long getTime(Time time) {
        if (time == null) {
            return 0;
        }
        return ((long) time.getSeconds() * 1000L)
                + (time.getNanoseconds() / 1000000L) % 1000L;
    }

    public static int getDefinition(TvProgram p) {
        // TODO - for 4K
        try {
            boolean hd = p.isHd();
            if (hd) {
                TvChannel ch = p.getChannel();
                if (ch != null) {
                    hd = ch.isHd();
                }
            }
            return hd ? 1 : 0;
        } catch (Exception ex) {
        }
        return 0;
    }

    public static int getConvertedCategory(int category, int subCategory) {
        if (category == 15) {
            // movie
            if (subCategory > 0) {
                return subCategory;
            } else {
                return category;
            }
        } else {
            return category;
        }
    }

    public static int getRatingIndex(String rating) {
        for (int i = 0; i < Constants.PARENTAL_RATING_NAMES.length; i++) {
            if (Constants.PARENTAL_RATING_NAMES[i].equals(rating)) {
                return i;
            }
        }
        return 0;
    }

    public static int getRatingIndex(RecordingRequest r) {
        try {
            Object rating = AppData.get(r, AppData.PARENTAL_RATING);
            if (rating != null) {
                return PvrUtil.getRatingIndex(rating.toString());
            }
        } catch (Exception ex) {
        }
        return 0;
    }


    public static String getRepeatText(String repeatString) {
        DataCenter dataCenter = DataCenter.getInstance();
        if (repeatString == null) {
            return dataCenter.getString("pvr.Yes");
        } else if (RecordingScheduler.REPEAT_ALL_STRING.equals(repeatString)) {
            return dataCenter.getString("pvr.Yes");
        } else if ("1111111".equals(repeatString)) {
            return dataCenter.getString("pvr.Everyday");
        } else {
            try {
                return getRepeatText(Integer.parseInt(repeatString, 2));
            } catch (Exception ex) {
                return dataCenter.getString("pvr.Yes");
            }
        }
    }

    public static String getRepeatText(int days) {
        DataCenter dataCenter = DataCenter.getInstance();
        if (days == 0) {
            return dataCenter.getString("pvr.No");
        } else if (days == 0x03) {
            return dataCenter.getString("pvr.Weekends");
        } else if (days == 0x7C) {
            return dataCenter.getString("pvr.Weekdays");
        } else if (days == 0x7F) {
            return dataCenter.getString("pvr.Everyday");
        } else {
            int count = 0;
            int day = 0;
            int mask = 0x40;
            StringBuffer sb = new StringBuffer(40);
            Formatter formatter = Formatter.getCurrent();
            for (int i = 0; i < 7; i++) {
                if ((mask & days) != 0) {
                    count++;
                    day = i;
                    sb.append(formatter.getDay((i + 1) % 7));
                    sb.append(' ');
                }
                mask = mask >> 1;
            }
            if (count == 1) {
                return formatter.getFullDay((day + 1) % 7);
            } else {
                return sb.toString();
            }
        }
    }

    public static int getDefaultKeepCount() {
        Object value = DataCenter.getInstance().get(PreferenceNames.DEFAULT_SAVE_TIME_REPEAT);
        if (Definitions.DEFAULT_SAVE_TIME_LAST_3.equals(value)) {
            return 3;
        } else if (Definitions.DEFAULT_SAVE_TIME_LAST_5.equals(value)) {
            return 5;
        }
        return 0;
    }

    public static long getDefaultExpirationPeriod() {
        Object value = DataCenter.getInstance().get(PreferenceNames.DEFAULT_SAVE_TIME);
        if (Definitions.DEFAULT_SAVE_TIME_AFTER_7_DAYS.equals(value)) {
            return 7 * Constants.MS_PER_DAY;
        } else if (Definitions.DEFAULT_SAVE_TIME_AFTER_14_DAYS.equals(value)) {
            return 14 * Constants.MS_PER_DAY;
        }
        return Long.MAX_VALUE;
    }

    public static int convertBufferMinute(Object value) {
        String s = value.toString();
        if (s.endsWith(" min")) {
            try {
                return Integer.parseInt(s.substring(0, s.length() - 4));
            } catch (Exception ex) {
            }
        }
        return 0;
    }

    public static long getAdjustedTimeSlot(long newTime, long oldTime) {
        if (newTime > oldTime) {
            if (newTime - oldTime > 12 * Constants.MS_PER_HOUR) {
                return newTime - Constants.MS_PER_DAY;
            }
        } else if (newTime < oldTime) {
            if (oldTime - newTime > 12 * Constants.MS_PER_HOUR) {
                return newTime + Constants.MS_PER_DAY;
            }
        }
        return newTime;
    }

    public static String toString(String[] s) {
        if (s == null) {
            return "null";
        }
        if (s.length == 0) {
            return "[]";
        }
        StringBuffer sb = new StringBuffer();
        sb.append('[');
        int i = 0;
        for (i = 0; i < s.length - 1; i++) {
            sb.append(s[i]);
            sb.append(',');
        }
        sb.append(s[i]);
        sb.append(']');
        return sb.toString();
    }
}

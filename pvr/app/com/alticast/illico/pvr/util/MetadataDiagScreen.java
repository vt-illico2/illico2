package com.alticast.illico.pvr.util;

import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.*;
import com.alticast.illico.pvr.*;
import com.alticast.illico.pvr.hn.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.gui.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;
import javax.media.*;
import javax.tv.util.*;
import javax.tv.service.Service;
import org.dvb.user.*;
import org.dvb.event.*;
import com.alticast.ui.*;
import org.dvb.ui.DVBColor;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;

public class MetadataDiagScreen extends LayeredWindow implements LayeredKeyHandler {

    LayeredUI ui;
    AbstractRecording rec;
    FixedSizeList homeNetLogs = new FixedSizeList(26);
    FixedSizeList localLogs = new FixedSizeList(26);

    private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");

    private static MetadataDiagScreen instance = new MetadataDiagScreen();

    public static MetadataDiagScreen getInstance() {
        return instance;
    }

    private MetadataDiagScreen() {
        this.setBounds(Constants.SCREEN_BOUNDS);
        this.setVisible(true);
    }

    public synchronized void startRecording(RecordingRequest r, boolean file) {
        startRecording(new LocalRecording((LeafRecordingRequest) r), file);
    }

    public synchronized void startRecording(AbstractRecording r, boolean file) {
        this.rec = r;
        removeAll();
        if (file && r instanceof LocalRecording) {
            add(new MetaFileScreen(((LocalRecording) r).getRecordingRequest()));
        } else {
            add(new AppDataScreen(r));
        }
        start();
    }

    public synchronized void startRoot() {
    }

    public synchronized void startHomeNetLogs() {
        removeAll();
        add(new LogsScreen("HomeNet Logs", homeNetLogs, new DVBColor(150, 70, 0, 180)));
        start();
    }

    public synchronized void startLocalLogs() {
        removeAll();
        add(new LogsScreen("Local recording changes", localLogs, new DVBColor(0, 150, 70, 180)));
        start();
    }

    public void writeLog(String log) {
        homeNetLogs.addFirst(timeFormat.format(new Date()) + " : " + log);
    }

    public void writeLocalLog(String log) {
        localLogs.addFirst(timeFormat.format(new Date()) + " : " + log);
    }

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        switch (code) {
            case KeyCodes.COLOR_B:
                if (getComponent(0) instanceof AppDataScreen) {
                    startRecording(rec, true);
                } else {
                    stop();
                }
                return true;
            case KeyCodes.COLOR_C:
                if (getComponent(0) instanceof AppDataScreen) {
                    stop();
                } else {
                    startRecording(rec, false);
                }
                return true;
            case OCRcEvent.VK_STOP:
                if (getComponent(0) instanceof AppDataScreen) {
                    if (rec instanceof LocalRecording) {
                        try {
                            rec.setExpirationTime(System.currentTimeMillis() + RecordManager.FAILED_RECORDING_KEEP_DURATION);
                            ((LocalRecording) rec).getRecordingRequest().cancel();

                            Object groupKey = rec.getAppData(AppData.GROUP_KEY);
                            if (groupKey != null) {
                                RecordingScheduler.getInstance().remove(groupKey.toString(), rec.getString(AppData.CHANNEL_NAME), rec.getLong(AppData.PROGRAM_START_TIME));
                            }
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                }
                return true;
            case OCRcEvent.VK_PINP_MOVE:
                if (rec != null && (rec instanceof LocalRecording)) {
                    RecordingList list = ((OcapRecordingRequest) ((LocalRecording) rec).getRecordingRequest()).getOverlappingEntries();
                    Log.printDebug("MetadataDiagScreen.getOverlappingEntries");
                    if (list != null) {
                        Log.printDebug("MetadataDiagScreen.getOverlappingEntries: size = " + list.size());
                        for (int i = 0; i < list.size(); i++) {
                            AppData.printAppData(list.getRecordingRequest(i));
                        }
                    }
                } else {
                    Component c = getComponent(0);
                    if (c instanceof LogsScreen) {
                        String s = NetSecurityManagerImpl.getInstance().getNetworkPassword();
                        writeLog("password = " + s);
                        c.repaint();
                    }
                }
                return true;
            case OCRcEvent.VK_DISPLAY_SWAP:
                Component c = getComponent(0);
                if (c instanceof MetaFileScreen) {
                    ((MetaFileScreen) c).swap();
                } else if (c instanceof AppDataScreen) {
                    ((AppDataScreen) c).nextPage();
                } else if (c instanceof LogsScreen) {
                    ((LogsScreen) c).clear();
                }
                return true;
        }
        return false;
    }

    public synchronized void start() {
        if (ui == null) {
            ui = WindowProperty.DEBUG_SCREEN.createLayeredDialog(this, Constants.SCREEN_BOUNDS, this);
        }
        ui.activate();
    }

    public synchronized void stop() {
        removeAll();
        rec = null;
        if (ui != null) {
            ui.deactivate();
            WindowProperty.dispose(ui);
            ui = null;
        }
    }
}

class AppDataScreen extends Component {

    public static final int ROW_SIZE = 24;
    private static final int ROW_GAP = 18;
    private static final int COL_GAP = 450;
    private static final int Y1 = 63;
    private static final int[] X_POS_1 = { 50, 78, 220 };
    private static final int[] X_POS_2 = { 50, 78, 280 };

    String[] keys;
    Object[] values;
    Object id;
    String stateString;
    String specString;
    long start;
    long dur;
    long exp;

    Service service;
    long size;
    long len;
    long mediaTime;
    long reqSpace;
    int segments;

    boolean local;

    int totalPage;
    int currentPage;

    public AppDataScreen(AbstractRecording r) {
        local = r instanceof LocalRecording;
        try {
            id = r.getIdObject();
            stateString = RecordManager.getStateString(r.getState());
            keys = r.getKeys();
            Arrays.sort(keys);
            values = new Object[keys.length];
            for (int i = 0; i < keys.length; i++) {
                values[i] = r.getAppData(keys[i]);
                if (values[i] != null && values[i] instanceof Long) {
                    long value = ((Long) values[i]).longValue();
                    if (value > 123456789012L) {
                        values[i] = "L: " + new Date(value);
                    }
                }
            }
            start = r.getScheduledStartTime();
            dur = r.getScheduledDuration();
            exp = r.getExpirationPeriod();

            len = r.getRecordedDuration();
            mediaTime = r.getMediaTime();
            size = r.getRecordedSize();
            reqSpace = r.getSpaceRequired();

            service = r.getService();
            if (service != null && service instanceof SegmentedRecordedService) {
                SegmentedRecordedService s = (SegmentedRecordedService) service;
                segments = s.getSegments().length;
            }
            totalPage = (keys.length - 1) / (ROW_SIZE * 2) + 1;
        } catch (Exception ex) {
        }
        setBackground(new DVBColor(150, 0, 70, 180));
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void nextPage() {
        currentPage = (currentPage + 1) % totalPage;
    }

    public void paint(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, 960, 540);
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(Color.white);
        g.drawString("Recording App Data", 50, 40);
        GraphicUtil.drawStringRight(g, "<B> meta file   <C> close  <SWAP> page", 890, 40);
        g.setColor(Color.yellow);
        g.drawString("ID = " + id + ",  state = " + stateString, 200, 40);

        int y = Y1;
        g.setColor(Color.cyan);
        g.drawString("start = " + new Date(start) +
                     ",  dur = " + Formatter.getCurrent().getDuration(dur) +
                     ",  exp = " + new Date(start + exp) + " (" + exp + ")", 50, y);
        y += ROW_GAP;
        if (service != null) {
            g.setColor(Color.green);
            g.drawString("size = " + size +
                         ",  len = " + Formatter.getCurrent().getDuration(len) +
                         ",  no. of segments = " + segments +
                         ",  mediaTime = " + Formatter.getCurrent().getDuration(mediaTime) + " (" + mediaTime + ")", 50, y);
        }
        g.setColor(Color.yellow);
        GraphicUtil.drawStringRight(g, "space required = " + reqSpace, 890, y);

        if (keys == null || values == null) {
            return;
        }

        int[] X_POS = local ? X_POS_1 : X_POS_2;
        int i = currentPage * ROW_SIZE * 2;
        int sx = 0;
        for (int c = 0; c < 2; c++) {
            y = Y1 + ROW_GAP * 2;
            for (; i < Math.min(keys.length, (c + 1) * ROW_SIZE); i++) {
                int x = 0;
                g.setColor(Color.green);
                g.drawString((i + 1) + ":", sx + X_POS[x++], y);
                g.setColor(Color.yellow);
                g.drawString("" + keys[i], sx + X_POS[x++], y);
                g.setColor(Color.white);
                int xp = X_POS[x++];
                g.drawString(TextUtil.shorten("" + values[i], g.getFontMetrics(), COL_GAP - xp + 45), sx + xp, y);
                y += ROW_GAP;
            }
            sx += COL_GAP;
        }

    }
}

class MetaFileScreen extends Component {

    public static final int ROW_SIZE = 25;
    private static final int ROW_GAP = 18;
    private static final int COL_GAP = 300;
    private static final int Y1 = 63;
    private static final int[] X_POS = { 50, 78 };

    int id;
    String dir;
    String[] data = null;
    String path;
    int lines = 0;
    RecordingRequest req;
    boolean backup;

    public MetaFileScreen(RecordingRequest req) {
        this.req = req;
        init(false);
    }

    public void swap() {
        init(!backup);
        repaint();
    }

    public void init(boolean backup) {
        this.backup = backup;
        try {
            this.id = req.getId();
            dir = Environment.HW_VENDOR == Environment.HW_VENDOR_SAMSUNG ? "/itfs/metadata" : "/metadata";
            File f = new File(dir, "leaf_" + id + (backup ? ".dat.bak" : ".dat"));
            path = f.getAbsolutePath();
            data = TextReader.read(f);
            lines = data.length;
        } catch (Exception ex) {
        }
        setBackground(new DVBColor(0, 70, 150, 180));
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void paint(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, 960, 540);
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(Color.white);
        g.drawString("Metadata File", 50, 40);
        GraphicUtil.drawStringRight(g, "<B> close   <C> show App Data   <SWAP> main / backup", 890, 40);
        g.setColor(Color.yellow);
        g.drawString("path = " + path, 200, 40);

        if (data == null) {
            return;
        }

        int y;
        int i = 0;
        int sx = 0;
        for (int c = 0; c < 3; c++) {
            y = Y1;
            for (; i < Math.min(data.length, (c + 1) * ROW_SIZE); i++) {
                int x = 0;
                g.setColor(Color.green);
                g.drawString((i + 1) + ":", sx + X_POS[x++], y);
                g.setColor(Color.white);
                g.drawString(TextUtil.shorten("" + data[i], GraphicsResource.FM18, 300 - 30), sx + X_POS[x++], y);
                y += ROW_GAP;
            }
            sx += COL_GAP;
        }
    }
}

class LogsScreen extends Component {

    public static final int ROW_SIZE = 25;
    private static final int ROW_GAP = 18;
//    private static final int COL_GAP = 300;
    private static final int Y1 = 63;
    private static final int[] X_POS = { 50, 78 };

    FixedSizeList list;
    String title;
    public LogsScreen(String title, FixedSizeList list, Color c) {
        this.title = title;
        this.list = list;
        setBackground(c);
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void clear() {
        list.clear();
    }

    public void paint(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, 960, 540);
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(Color.yellow);
        g.drawString(title, 50, 40);
        g.setColor(Color.white);
        GraphicUtil.drawStringRight(g, "<SWAP> clear  <B> close", 890, 40);

        Iterator it = list.iterator();
        int y = Y1;
        int sx = 0;
        int i = 0;
        while (it.hasNext()) {
            String item = (String) it.next();
            int x = 0;
            g.setColor(Color.green);
            g.drawString(++i + ":", sx + X_POS[x++], y);
            g.setColor(Color.white);
            g.drawString(item, sx + X_POS[x++], y);
            y += ROW_GAP;
        }
    }
}
/*
class TestScreen extends Component {

    public static final int ROW_SIZE = 25;
    private static final int ROW_GAP = 18;
    private static final int COL_GAP = 300;
    private static final int Y1 = 63;
    private static final int[] X_POS = { 50, 78 };

    Vector vector;

    public TestScreen() {
        vector = (Vector) SharedMemory.getInstance().get("MIGRATION_TEST");
        setBackground(new DVBColor(0, 150, 70, 180));
        setBounds(Constants.SCREEN_BOUNDS);
        setVisible(true);
    }

    public void paint(Graphics g) {
        g.setColor(getBackground());
        g.fillRect(0, 0, 960, 540);
        g.setFont(GraphicsResource.BLENDER18);
        g.setColor(Color.yellow);
        g.drawString("Migration Test", 50, 40);

        if (vector == null) {
            return;
        }
        g.drawString("size = " + vector.size(), 350, 40);

        g.setColor(Color.white);
        int y = Y1;
        int size = Math.max(vector.size(), ROW_SIZE);
        for (int i = 0; i < ROW_SIZE; i++) {
            g.drawString("" + vector.get(i), 50, y);
            y += ROW_GAP;
        }

    }
}
*/

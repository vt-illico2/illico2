package com.alticast.illico.pvr;

import com.videotron.tvi.illico.ixc.epg.TvChannel;

public class ChannelChangeRequestor {
    public int videoIndex;
    public TvChannel current;
    public TvChannel next;

    public boolean result;

    public ChannelChangeRequestor(int videoIndex, TvChannel current, TvChannel next) {
        this.videoIndex = videoIndex;
        this.current = current;
        this.next = next;
    }
}


package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** AppData로 filtering */
public class AppDataFilter extends RecordingListFilter {
    String key;
    Object value;
    int id;

    public AppDataFilter(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public boolean accept(RecordingRequest r) {
        if (r == null) {
            return false;
        }
        Object o = AppData.get(r, key);
        if (o == null) {
            return false;
        }
        return value.equals(o);
    }
}


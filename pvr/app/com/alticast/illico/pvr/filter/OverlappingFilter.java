package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** 녹화 시작 시간 겹치는 것 체크. */
public class OverlappingFilter extends RecordingListFilter {
    long from;
    long to;
    public OverlappingFilter(long from, long to) {
        this.from = from;
        this.to = to;
    }

    public boolean accept(RecordingRequest r) {
        long st = AppData.getScheduledStartTime(r);
        long et = st + AppData.getScheduledDuration(r);
        return isOverlappedDuration(from, to, st, et);
    }

    private static boolean isOverlappedDuration(long s1, long e1, long s2, long e2) {
        return s1 < e2 && s2 < e1;
    }
}

package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** 녹화 시작 시간으로 */
public class ScheduledDateFilter extends RecordingListFilter {
    long maxStartTime;
    public ScheduledDateFilter(long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public boolean accept(RecordingRequest r) {
        return AppData.getScheduledStartTime(r) <= maxStartTime;
    }
}


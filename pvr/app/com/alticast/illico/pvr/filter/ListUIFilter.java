package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.RecordingListManager;

public class ListUIFilter extends MultipleStateFilter {
    private MultipleStateFilter subFilter;

    public ListUIFilter(MultipleStateFilter subFilter) {
        super(subFilter.states);
        this.subFilter = subFilter;
    }

    public boolean accept(RecordingRequest r) {
        if (r == null) {
            return false;
        }
        try {
            Integer id = new Integer(r.getId());
            if (RecordingListManager.toBeDeleted.contains(id)) {
                return false;
            }
            if (AppData.TEMP_TYPE.equals(AppData.get(r, AppData.PROGRAM_TYPE))) {
                return false;
            }
            return subFilter.accept(r);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return false;
    }
}

package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** AppData로 reverse filtering */
public class ReverseAppDataFilter extends AppDataFilter {
    boolean includeNull;

    public ReverseAppDataFilter(String key, Object value, boolean includeNull) {
        super(key, value);
        this.includeNull = includeNull;
    }

    public boolean accept(RecordingRequest r) {
        if (r == null) {
            return false;
        }
        Object o = AppData.get(r, key);
        if (o == null) {
            return includeNull;
        }
        return !value.equals(o);
    }
}


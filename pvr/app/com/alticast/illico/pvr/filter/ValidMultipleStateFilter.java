package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.list.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** 여러 state를 filtering 할수 있는 class */
public class ValidMultipleStateFilter extends MultipleStateFilter {

    public ValidMultipleStateFilter(int[] recordingStates) {
        super(recordingStates);
    }

    public boolean accept(AbstractRecording r) {
        if (r == null) {
            return false;
        }
        try {
            int state = r.getState();
            boolean accept = super.accept(state);
            if (!accept) {
                return false;
            }
            if (state == LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                if (r.getRecordedDuration() <= 0) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            // 이상하게 delete해도 IllegalStateException 발생된다
            // java.lang.IllegalStateException: This RecordingRequest has been deleted.
            Log.print(ex);
            return false;
        }
    }

    public boolean accept(RecordingRequest r) {
        if (r == null) {
            return false;
        }
        try {
            int state = r.getState();
            boolean accept = super.accept(state);
            if (!accept) {
                return false;
            }
            if (state == LeafRecordingRequest.IN_PROGRESS_WITH_ERROR_STATE) {
                RecordedService s = ((LeafRecordingRequest) r).getService();
                if (s == null) {
                    return false;
                }
                if (s.getRecordedDuration() <= 0) {
                    return false;
                }
            }
            return true;
        } catch (Exception ex) {
            // 이상하게 delete해도 IllegalStateException 발생된다
            // java.lang.IllegalStateException: This RecordingRequest has been deleted.
            Log.print(ex);
            return false;
        }
    }
}

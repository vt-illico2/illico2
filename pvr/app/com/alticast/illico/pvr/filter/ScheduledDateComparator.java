package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** 녹화 시작 시간으로 */
public class ScheduledDateComparator implements RecordingListComparator {

    public int compare(RecordingRequest first, RecordingRequest second) {
        long s1 = AppData.getScheduledStartTime(first);
        long s2 = AppData.getScheduledStartTime(second);
        if (s1 > s2) {
            return 1;
        } else if (s1 < s2) {
            return -1;
        } else {
            s1 = AppData.getScheduledDuration(first);
            s2 = AppData.getScheduledDuration(second);
            if (s1 > s2) {
                return -1;
            } else if (s1 < s2) {
                return 1;
            } else {
                return 0;
            }
        }
    }
}


package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** Comparable AppData로 sorting */
public class AppDataComparator implements RecordingListComparator, Comparator {
    String key;

    public AppDataComparator(String key) {
        this.key = key;
    }

    public int compare(RecordingRequest first, RecordingRequest second) {
        try {
            Object o1 = AppData.get(first, key);
            if (o1 == null) {
                return 1;
            }
            Object o2 = AppData.get(second, key);
            if (o2 == null) {
                return -1;
            }
            if (o1 instanceof Comparable && o2 instanceof Comparable) {
                return ((Comparable) o1).compareTo((Comparable) o2);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        return 0;
    }

    public int compare(Object first, Object second) {
        if (first instanceof AbstractRecording && second instanceof AbstractRecording) {
            Object o1 = ((AbstractRecording) first).getAppData(key);
            if (o1 == null) {
                return 1;
            }
            Object o2 = ((AbstractRecording) second).getAppData(key);
            if (o2 == null) {
                return -1;
            }
            if (o1 instanceof Comparable && o2 instanceof Comparable) {
                return ((Comparable) o1).compareTo((Comparable) o2);
            }
        }
        return 0;
    }

    public boolean equals(Object obj) {
        if (obj instanceof AppDataComparator) {
            AppDataComparator a = (AppDataComparator) obj;
            return key.equals(a.key);
        } else {
            return false;
        }
    }
}

package com.alticast.illico.pvr.filter;

import com.videotron.tvi.illico.log.Log;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import java.util.*;

/** 여러 state를 filtering 할수 있는 class */
public class MultipleStateFilter extends RecordingListFilter {
    int[] states;

    public MultipleStateFilter(int[] recordingStates) {
        this.states = recordingStates;
        Arrays.sort(states);
    }

    public boolean accept(int state) {
        return Arrays.binarySearch(states, state) >= 0;
    }

    public boolean accept(RecordingRequest r) {
        if (r == null) {
            return false;
        }
        try {
            return accept(r.getState());
        } catch (Exception ex) {
            // 이상하게 delete해도 IllegalStateException 발생된다
            // java.lang.IllegalStateException: This RecordingRequest has been deleted.
            Log.print(ex);
            return false;
        }
    }
}

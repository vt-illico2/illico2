package com.alticast.illico.pvr;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.vbm.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import java.rmi.*;
import java.util.*;
import javax.tv.util.*;
import org.ocap.shared.dvr.*;

public class PvrVbmController extends VbmController {

    public static final long MID_UPCOMING_RECORDINGS_CHANNEL    = 1007000001L;
    public static final long MID_UPCOMING_RECORDINGS_ID         = 1007000009L;
    public static final long MID_UPCOMING_RECORDINGS_REPEAT     = 1007000010L;
    public static final long MID_UPCOMING_RECORDINGS_TIME       = 1007000011L;

    public static final long MID_START_RECORDING_WATCH          = 1007000002L;
    public static final long MID_START_RECORDING_WATCH_CHANNEL  = 1007000012L;
    public static final long MID_START_RECORDING_WATCH_LOCATION = 1007000008L;
    public static final long MID_END_RECORDING_WATCH            = 1007000003L;

    public static final long MID_PVR_BUTTON_SELECTION           = 1007000004L;
    public static final long MID_PVR_APPLICATION_START          = 1007000005L;
    public static final long MID_PVR_APPLICATION_EXIT           = 1007000006L;
    public static final long MID_PVR_PARENT_APPLICATION         = 1007000007L;

    public static final long MID_CURRENT_INTERNAL_DISK_USAGE    =    7000001L;
    public static final long MID_CURRENT_INTERNAL_DISK_CAPACITY =    7000002L;


    public static final long MID_UPCOMING_RECORDINGS_DURATION   = 1007000013L;

    public static final long MID_DELETE_FROM_LIST       = 1007000021L;
    public static final long MID_DELETE_FROM_MULTI      = 1007000022L;
    public static final long MID_DELETE_FROM_FREE_UP    = 1007000023L;
    public static final long MID_DELETE_FROM_STOP_PLAY  = 1007000024L;
    public static final long MID_DELETE_FROM_END_PLAY   = 1007000025L;

    //->Kenneth[2015.5.27] R5 
    public static final long MID_NEW_RECORDING_SCHEDULED        = 1007000014L;
    public static final long MID_ACCESS_LIST_PAGE               = 1007000015L;
    public static final long MID_EXIT_BY_BACK                   = 1007000016L;

    public static final String[] MENU_NAMES = {
        "RecordedList", "UpcomingList", "ManualRecording", "Manage", "Settings"
    };

    String viewSession = " ";
    String viewId = " ";
    long appSession = 0;

    protected static PvrVbmController instance = new PvrVbmController();

    public static PvrVbmController getInstance() {
        return instance;
    }

    protected PvrVbmController() {
    }

    //->Kenneth[2015.5.29] R5
    public void writeNewRecordingScheduled() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeNewRecordingScheduled()");
        try {
            long appSession = System.currentTimeMillis();
            write(MID_NEW_RECORDING_SCHEDULED, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void writeAccessListPage(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeAccessListPage("+appSession+")");
        write(MID_ACCESS_LIST_PAGE, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }

    public void writeExitByBack(long appSession) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeExitByBack("+appSession+")");
        write(MID_EXIT_BY_BACK, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
    }
    //<-

    public void writeParentApp(String name) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeParentApp("+name+")");
        if (ENABLED) {
            if (name.equalsIgnoreCase(MonitorService.REQUEST_APPLICATION_HOT_KEY)) {
                name = "Remote";
            }
            write(MID_PVR_PARENT_APPLICATION, DEF_MEASUREMENT_GROUP, name);
        }
    }

    public void writeApplicationStart() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeApplicationStart()");
        if (ENABLED) {
            appSession = System.currentTimeMillis();
            write(MID_PVR_APPLICATION_START, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
        }
    }

    public void writeApplicationExit() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeApplicationExit()");
        if (ENABLED) {
            write(MID_PVR_APPLICATION_EXIT, DEF_MEASUREMENT_GROUP, String.valueOf(appSession));
        }
    }

    public void writeStartRecordingWatch(AbstractRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeStartRecordingWatch("+r+")");
        if (ENABLED) {
            if (r != null) {
                viewSession = String.valueOf(System.currentTimeMillis());
                viewId = r.getString(AppData.PROGRAM_ID);
                if (viewId.length() == 0) {
                    viewId = "Manual";
                }
                write(MID_START_RECORDING_WATCH, viewSession, viewId);
                write(MID_START_RECORDING_WATCH_CHANNEL, viewSession, r.getString(AppData.CHANNEL_NAME));
                write(MID_START_RECORDING_WATCH_LOCATION, viewSession, r instanceof LocalRecording ? "LOCAL" : "REMOTE");
            }
        }
    }

    public void writeEndRecordingWatch() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeEndRecordingWatch()");
        if (ENABLED) {
            write(MID_END_RECORDING_WATCH, viewSession, viewId);
            viewSession = " ";
            viewId = " ";
        }
    }

    public void writeUpcomingRecording(RecordingRequest req, RecordingOption option) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeUpcomingRecording("+req+")");
        if (ENABLED) {
            String group = String.valueOf(System.currentTimeMillis());
            String id = (String) AppData.get(req, AppData.PROGRAM_ID);
            if (id == null) {
                id = "Manual";
            }
            write(MID_UPCOMING_RECORDINGS_CHANNEL, group, AppData.getString(req, AppData.CHANNEL_NAME));
            write(MID_UPCOMING_RECORDINGS_ID, group, id);
            write(MID_UPCOMING_RECORDINGS_REPEAT, group, option.all ? "Yes" : "No");
            write(MID_UPCOMING_RECORDINGS_TIME, group, String.valueOf(AppData.getScheduledStartTime(req)));
            write(MID_UPCOMING_RECORDINGS_DURATION, group, String.valueOf(AppData.getScheduledDuration(req)));
        }
    }

    public void writeRecordingDeleted(RecordingRequest req, long measurementId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writeRecordingDeleted("+req+")");
        if (ENABLED) {
            String id = (String) AppData.get(req, AppData.PROGRAM_ID);
            if (id == null) {
                id = "Manual";
            }
            write(measurementId, DEF_MEASUREMENT_GROUP, id);
        }
    }

    public void writePvrButtonSelection(int menuId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.writePvrButtonSelection("+menuId+")");
        if (ENABLED && menuId >= 0 && menuId < MENU_NAMES.length) {
            write(MID_PVR_BUTTON_SELECTION, DEF_MEASUREMENT_GROUP, MENU_NAMES[menuId]);
        }
    }

    // overriden
	public void triggerLogGathering(long measurementId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrVbmController.triggerLogGathering("+measurementId+")");
        super.triggerLogGathering(measurementId);
        if (!Environment.SUPPORT_DVR) {
            return;
        }
        if (measurementId == MID_CURRENT_INTERNAL_DISK_USAGE) {
            long usage = StorageInfoManager.getInstance().getUsingSpace();
            if (StorageInfoManager.KB_UNIT_1024) {
                usage = (int) (usage >> 20);
            } else {
                usage = (int) (usage / 1000000);
            }
            write(measurementId, DEF_MEASUREMENT_GROUP, String.valueOf(usage));
        } else if (measurementId == MID_CURRENT_INTERNAL_DISK_CAPACITY) {
            long usage = StorageInfoManager.getInstance().getAllocatedSpace();
            if (StorageInfoManager.KB_UNIT_1024) {
                usage = (int) (usage >> 20);
            } else {
                usage = (int) (usage / 1000000);
            }
            write(measurementId, DEF_MEASUREMENT_GROUP, String.valueOf(usage));
        }
    }


}


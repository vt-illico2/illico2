package com.alticast.illico.pvr;

import java.io.File;
import java.rmi.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import java.util.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.data.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import com.videotron.tvi.illico.util.SharedMemory;

public class RecordingUpdater implements EpgDataListener {

    private static final RecordingUpdater instance = new RecordingUpdater();

    public static RecordingUpdater getInstance() {
        return instance;
    }

    private static final Integer PROGRAM_TYPE_UNKNOWN = new Integer(TvProgram.TYPE_UNKNOWN);

    private RecordingUpdater() {
    }

    public void epgDataUpdated(byte type, long from, long to) throws RemoteException {
        Log.printDebug(App.LOG_HEADER+"RecordingUpdater.epgDataUpdated: " + new Date(from) + " - " + new Date(to));
        if (type == EPG_DATA) {
            boolean changed = updateRecordings(from, to);
            if (changed) {
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.epgDataUpdated() : Call UpcomingList.updateFull()");
                UpcomingList.getInstance().updateFull();
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////
    //->Kenneth : 이 method 가 매우 중요함. EPG update 시에 recording 을 업데이트하는 로직이 여기 있음.
    /////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////
    private boolean updateRecordings(long from, long to) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings("+new Date(from)+", "+new Date(to)+")");
        boolean changed = false;
        // 1. Upcoming list 의 recording 을 가져옴.
        RecordingList list = RecordingListManager.getInstance().getPendingList();
        if (list == null || list.size() == 0) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() return false -- 1");
            return false;
        }
        // 2. 모든 Upcoming recording 에 대해서 아래와 같이 처리
        for (int i = 0; i < list.size(); i++) {
            RecordingRequest req = list.getRecordingRequest(i);
            Object programType = AppData.get(req, AppData.PROGRAM_TYPE);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : programType = "+programType);
            // 2-1. Manual recording 인 경우 
            if (programType == null) {  // Manual 인 경우.
                long et = AppData.getLong(req, AppData.PROGRAM_END_TIME);
                // 2-1-1 Update 된 기간내의 recording 인 경우 updateManaual 을 무조건 불러준다.
                if (from < et && et <= to) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call AppData.updateManual()");
                    changed = AppData.updateManual(req, AppData.getLong(req, AppData.PROGRAM_START_TIME), et) || changed;
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 1");
                continue;
            }

            // 2-2. 해당 레코딩이 Program ID 가 없으면 아무일도 하지 않는다.
            Object programIdFromRecording = AppData.get(req, AppData.PROGRAM_ID);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : programIdFromRecording = "+programIdFromRecording);
            if (programIdFromRecording == null) {    // program ID 체크 - manual은 skiip
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 2");
                continue;
            }
            // 2-3 해당 레코딩의 start time 이 변경된 EPG data 기간내에 위치하지 않으면 아무 일도 하지 않는다.
            long st = AppData.getLong(req, AppData.PROGRAM_START_TIME);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : PROGRAM_START_TIME = "+new Date(st));
            if (from > st || st >= to) {    // 변경된 EPG data 범위 안에 있는지
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Not in period, continue --- 3");
                continue;
            }

            // 2-4 해당 레코딩이 속한 채널에서 start time 에 편성된 program 이 없으면 아무 일도 하지 않는다. (있으면
            // 안되는 케이스 같은데..)
            EpgService es = Core.epgService;
            String channel = AppData.getString(req, AppData.CHANNEL_NAME);
            TvProgram programFromEpg = es.getProgram(channel, st);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : TvProgram = "+programFromEpg);
            if (programFromEpg == null) {    // Program 찾기
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Null program, continue --- 4");
                continue;
            }
            if (Log.DEBUG_ON) {
                Log.printDebug(App.LOG_HEADER+"RecordingUpdater ["+i+"]: target recording = " + AppData.get(req, AppData.TITLE) + " : " + programIdFromRecording);
                Log.printDebug(App.LOG_HEADER+"RecordingUpdater ["+i+"]: new program = " + programFromEpg + " : " + programFromEpg.getId());
            }
            // 2-5 프로그램 타입이 UNKNOWN 이면 다시 찾아본다. (Start time 과 End time 이 같은 경우만 업데이트)
            if (PROGRAM_TYPE_UNKNOWN.equals(programType)) {
                // 프로그램이 있는데 unknown 인 경우 다시 찾아보자.
                if (programFromEpg.getStartTime() == st && programFromEpg.getEndTime() == AppData.getLong(req, AppData.PROGRAM_END_TIME)) {
                    Log.printInfo(App.LOG_HEADER+"RecordingUpdater ["+i+"]: found program : " + programFromEpg);
                    changed = AppData.update(req, programFromEpg) || changed; // Rating등 기타 metadata 변경
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 5");
                continue;
            }

            Object groupKey = AppData.get(req, AppData.GROUP_KEY);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : groupKey = "+groupKey);
            // 2-6 해당 레코딩과 Program 이 program_ID 가 같거나 title 이 같은 경우
            if (programIdFromRecording.equals(programFromEpg.getId()) || AppData.getString(req, AppData.TITLE).equals(programFromEpg.getTitle())) {
                // 2-6-1 end time 이 다른 경우 : replace 해주고 끝.
                if (programFromEpg.getEndTime() != AppData.getLong(req, AppData.PROGRAM_END_TIME)) {
                    Log.printInfo(App.LOG_HEADER+"RecordingUpdater: ["+i+"] changed end time : " + programFromEpg);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordManager.replace()");
                    RecordManager.getInstance().replace(req, programFromEpg);
                    changed = true;
                // 2-6-2 end time 이 같은 경우 : GroupKey 가 있으면 update 해주고 끝.
                } else {
                    Log.printInfo(App.LOG_HEADER+"RecordingUpdater: ["+i+"] not changed : " + programFromEpg);
                    if (groupKey != null) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordingScheduler.update()");
                        RecordingScheduler.getInstance().update(groupKey.toString(), req, programFromEpg);
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call AppData.update()");
                    changed = AppData.update(req, programFromEpg) || changed; // Rating등 기타 metadata 변경
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 6");
                continue;
            }

            // 2-7 시리즈 레코딩
            if (groupKey != null) {
                Object seriesId = AppData.get(req, AppData.SERIES_ID);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : seriesId = "+seriesId);
                boolean isChanged;
                if (seriesId != null) {
                    isChanged = !seriesId.equals(programFromEpg.getSeriesId());  // real series

                    //->Kenneth[2016.2.25] VDTRMASTER-5639 VT 에 릴리즈 해야 해서 일단 아직 검증 안된 아래 수정 사항은 막는다.
                    /*
                    //->Kenneth[2015.10.28] VDTRMASTER-5639 : program ID 가 다른 경우도 프로그램 없어진 것으로 처리한다.
                    // RecordingSchduler.epgDataUpdated 에 의해서 다른 ID 의 program 이 존재하면 걔가 추가될 것임.
                    if (!isChanged) {
                        String idFromEpg = programFromEpg.getId();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["
                                +i+"] : idFromEpg("+idFromEpg+") : idFromRecording("+programIdFromRecording+")");
                        if (!programIdFromRecording.equals(idFromEpg)) {
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Set isChanged = true");
                            isChanged = true;
                        }
                    }
                    //<-
                    */

                } else {
                    isChanged = isChanged(programFromEpg, req, programIdFromRecording);       // forced series
                }
                if (isChanged) {
                    // 2-7-1 Seried ID 가 바뀐 경우 : 레코딩 취소한다.
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] = PVR714");
                    Log.printError("PVR714: series recording canceled. " + AppData.get(req, AppData.TITLE) + " is changed to " + programFromEpg);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordingScheduler.remove()");
                    RecordingScheduler.getInstance().remove(groupKey.toString(), channel, st);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordManager.cancel()");
                    RecordManager.getInstance().cancel(req);
                    changed = true;
                } else {
                    //->Kenneth[2016.2.25] VDTRMASTER-5639 VT 에 릴리즈 해야 해서 일단 아직 검증 안된 아래 수정 사항은 막는다.
                    /*
                    //->Kenneth[2015.10.28] VDTRMASTER-5639 : program ID 를 제외한 몇가지 주요 데이터의 업데이트의 경우도 수정되어야 함.
                    // start time, end time, episode title 이 바뀐 경우는 replace 하도록 하자. 그러지 않으면 정상적으로 GridEpg,
                    // UpcomingList 등에 반영되지 않는다.

                    // 2-7-2 Seried ID 가 그대로인 경우
                    long startTimeFromEpg = programFromEpg.getStartTime();
                    long startTimeFromRecording = st;
                    long endTimeFromEpg = programFromEpg.getEndTime();
                    long endTimeFromRecording = AppData.getLong(req, AppData.PROGRAM_END_TIME);
                    String episodeTitleFromEpg = programFromEpg.getEpisodeTitle();
                    String episodeTitleFromRecording = AppData.getString(req, AppData.EPISODE_TITLE);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["
                            +i+"] : startTimeFromEpg("+new Date(startTimeFromEpg)+") : startTimeFromRecording("+new Date(startTimeFromRecording)+")");
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["
                            +i+"] : endTimeFromEpg("+new Date(endTimeFromEpg)+") : endTimeFromRecording("+new Date(endTimeFromRecording)+")");
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["
                            +i+"] : episodeTitleFromEpg("+episodeTitleFromEpg+") : episodeTitleFromRecording("+episodeTitleFromRecording+")");

                    if (startTimeFromEpg != startTimeFromRecording || endTimeFromEpg != endTimeFromRecording ||
                            (episodeTitleFromEpg != null && !episodeTitleFromEpg.equals(episodeTitleFromRecording))) {
                        // 2-7-2-1 start time/end time/episode title 중 하나라도 다른 경우 : replace
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordManager.replace("+req+", "+programFromEpg+")");
                        RecordManager.getInstance().replace(req, programFromEpg);
                    } else {
                        // 2-7-2-2 start time/end time/episode title 가 모두 동일한 경우 : 그냥 update
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] = Call RecordingScheduler.update()");
                        changed = RecordingScheduler.getInstance().update(groupKey.toString(), req, programFromEpg) || changed;
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] changed = "+changed);
                    }

                    // 2-7-2 Seried ID 가 그대로인 경우 : 업데이트 한다.
                    //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] = Call RecordingScheduler.update()");
                    //changed = RecordingScheduler.getInstance().update(groupKey.toString(), req, programFromEpg) || changed;
                    //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] changed = "+changed);
                    //<-
                    */

                    // 2-7-2 Seried ID 가 그대로인 경우 : 업데이트 한다.
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] = Call RecordingScheduler.update()");
                    changed = RecordingScheduler.getInstance().update(groupKey.toString(), req, programFromEpg) || changed;
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] changed = "+changed);

                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 7");
                continue;
            }

            // 2-8 single 레코딩 바뀐 게 없는 경우
            if (!isChanged(programFromEpg, req, programIdFromRecording)) {
                Log.printInfo("RecordingUpdater: ["+i+"] single program is not changed : " + programFromEpg);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call AppData.update()");
                changed = AppData.update(req, programFromEpg) || changed; // Rating등 기타 metadata 변경
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : continue --- 8");
                continue;
            }

            // 2-9 single 레코딩 바뀐 게 있는 경우
            changed = true;
            long targetStartTime = 0;
            TvProgram target = null;
            RemoteIterator it = es.findPrograms(channel, programIdFromRecording.toString(), null, System.currentTimeMillis(), Long.MAX_VALUE);
            Remote o;
            while ((o = it.next()) != null) {
                TvProgram np = (TvProgram) o;
                long ps = np.getStartTime();
                if (Math.abs(ps - st) < Math.abs(targetStartTime - st)) {
                    target = np;
                    targetStartTime = ps;
                }
            }
            if (target != null) {
                // 2-9-1 Recording 에 해당하는 program 을 찾은 경우 : replace 한다.
                Log.printWarning("RecordingUpdater: ["+i+"] new target = " + target);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordManager.replace("+req+", "+target+")");
                RecordManager.getInstance().replace(req, target);
            } else {
                // 2-9-2 Recording 에 해당하는 program 이 없는 경우 : 레코딩을 cancel 한다.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] = PVR714");
                Log.printError("PVR714: scheduled recording canceled. " + AppData.get(req, AppData.TITLE) + " is changed to " + programFromEpg);
//                Log.printWarning("RecordingUpdater: program removed = " + req.getId());
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() ["+i+"] : Call RecordManager.cancel(req)");
                RecordManager.getInstance().cancel(req);
            }
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingUpdater.updateRecordings() return "+changed);
        return changed;
    }

    private boolean isChanged(TvProgram p, RecordingRequest req, Object programId) throws RemoteException {
        return !programId.equals(p.getId()) && !AppData.getString(req, AppData.TITLE).equals(p.getTitle());
    }

}

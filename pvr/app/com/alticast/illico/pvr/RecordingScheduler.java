package com.alticast.illico.pvr;

import java.io.*;
import java.rmi.*;
import java.util.*;
import javax.tv.service.*;
import javax.tv.locator.*;
import javax.tv.util.*;
import org.ocap.net.*;
import org.ocap.shared.dvr.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.illico.pvr.config.PvrConfig;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.popup.ListSelectPopup;
import com.alticast.illico.pvr.gui.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.hn.HomeNetManager;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.framework.io.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.pvr.*;

/**
 * 시리즈/ repeat 예약 녹화 관리.
 *
 * @author June Park
 */
public class RecordingScheduler implements TVTimerWentOffListener, EpgDataListener {

    public static final char GROUP_KEY_MARKER = '#';

    public static final char STATE_NORMAL   = '_';
    public static final char STATE_INACTIVE = 'i';
    public static final char STATE_DELETING = 'D';

    public static final char UNKNOWN        = '?';
    public static final char SERIES         = 'S';
    public static final char PROGRAM_REPEAT = 'F';
    public static final char MANUAL_REPEAT  = 'M';

    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty("dvb.persistent.root");
    private static final String VERSION = "5";
    public static final String SERIES_DIRECTORY = REPOSITORY_ROOT + "/pvr/series_" + VERSION + "/";

    ////////////////////////////////////////////////////////////////////
    // file name    = call_letter#type????_????#[title or series_id]    CBMT#F1200_1300#Baseball
    // first line   = state|title|first_time|repeat_string|keep_count
    // program line = call_letter|from|to|program_id|episode_title|reserved


    ////////////////////////////////////////////////////////////////////
    // --- Series
    // file name    = call_letter.series_id
    // first line   = state(inactive)|title|keep_count|
    // program line = call_letter|from|to|program_id|episode_title|reserved

    ////////////////////////////////////////////////////////////////////
    // --- Repeat
    // file name    = call_letter.title (= group_key of AppData), for manual program_id = 940_1040
    // first line   = state(inactive)|title|from|to|repeat_string|keep_count|manual(false/true)
    // program line = call_letter|from|to|program_id|episode_title|reserved

    public static final String REPEAT_ALL_STRING = "xxxxxxx";

    public static final long MANUAL_MAX_DAY = 14 * Constants.MS_PER_DAY;

    public static int MAX_SCHEDULED_PROGRAMS;

    // filename - Series
    private Hashtable series = new Hashtable();

    private Vector inactiveGroupKeys = new Vector();

    private TVTimerSpec checkInactivityTimer;

    private RecordManager rm = RecordManager.getInstance();
    private RecordingListManager lm = RecordingListManager.getInstance();

    private static RecordingScheduler instance = new RecordingScheduler();

    private Worker epgDataUpdateWorker = new Worker("RecordingScheduler", true);

    public static RecordingScheduler getInstance() {
        return instance;
    }

    private RecordingScheduler() {
        checkInactivityTimer = new TVTimerSpec();
        checkInactivityTimer.setTime(10 * Constants.MS_PER_MINUTE);
        checkInactivityTimer.setRepeat(true);
        checkInactivityTimer.setAbsolute(false);
        checkInactivityTimer.addTVTimerWentOffListener(this);

        MAX_SCHEDULED_PROGRAMS = DataCenter.getInstance().getInt("MAX_SCHEDULED_PROGRAMS", 30);
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.init()");
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"SERIES_DIRECTORY = "+SERIES_DIRECTORY);
        File dir = new File(SERIES_DIRECTORY);
        if (!dir.exists()) {
            if (Log.DEBUG_ON) {
                Log.printDebug(dir + " is not exist, so mkdirs");
            }
            dir.mkdirs();
        } else {
            // read series
            File[] files = dir.listFiles();
            for (int i = 0; i < files.length; i++) {
                File f = files[i];
                if (!f.isDirectory()) {
                    try {
                        Series s = new Series(f);
                        Log.printDebug("RecordingScheduler: found " + s);
                        // file name == group_key
                        series.put(f.getName(), s);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
            }
        }

        Map.Entry entry;
        Set set = series.entrySet();
        Iterator it = set.iterator();
        while (it.hasNext()) {
            entry = (Map.Entry) it.next();
            Series s = (Series) entry.getValue();
            if (s.state == STATE_INACTIVE || Environment.EMULATOR) {
                inactiveGroupKeys.addElement(entry.getKey());
            }
        }
        startTimer();
    }

    /////////////////////////////////////////////////////////////////////////
    // edit APIs
    /////////////////////////////////////////////////////////////////////////

    public String makeRepeat(RecordingRequest req, String repeatString, int keepOption) {
        return makeRepeat(req, repeatString, keepOption, false, false);
    }

    public String makeRepeat(RecordingRequest req, String repeatString, int keepOption, boolean onlyFromEpgCache) {
        return makeRepeat(req, repeatString, keepOption, onlyFromEpgCache, false);
    }

    public String makeRepeat(RecordingRequest req, String repeatString, int keepOption, boolean onlyFromEpgCache, boolean fromStart) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.makeRepeat("+req+", "+repeatString+", "+keepOption+", "+onlyFromEpgCache+", "+fromStart+")");
        Object programId = AppData.get(req, AppData.PROGRAM_ID);
        RecordManager.setExpirationPeriod(req, Long.MAX_VALUE);

        String newGroupKey = null;
        if (programId == null) {
            // manual
            try {
                newGroupKey = setManualRepeat(req, repeatString, keepOption, onlyFromEpgCache, fromStart, false);
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.makeRepeat() : Manual GroupKey = "+newGroupKey);
                AppData.set(req, AppData.GROUP_KEY, newGroupKey);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
        } else {
            Object seriesId = AppData.get(req, AppData.SERIES_ID);
            if (seriesId == null) {
                // forced series
                try {
                    newGroupKey = setForcedRepeat(req, repeatString, keepOption, onlyFromEpgCache, fromStart);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.makeRepeat() : Forced Series GroupKey = "+newGroupKey);
                    AppData.set(req, AppData.GROUP_KEY, newGroupKey);
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            } else {
                // series
                try {
                    newGroupKey = setSeries(req, repeatString, keepOption, onlyFromEpgCache, fromStart);
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.makeRepeat() : Series GroupKey = "+newGroupKey);
                    AppData.set(req, AppData.GROUP_KEY, newGroupKey);
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            }
        }
        UpcomingList.getInstance().updateRecording(req);
        return newGroupKey;
    }

    public void makeSingle(RecordingRequest req) {
        makeSingle(req, true);
    }

    public void makeSingle(RecordingRequest req, boolean removeAll) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.makeSingle("+req+", "+removeAll+")");
        String groupKey = AppData.getString(req, AppData.GROUP_KEY);
        AppData.remove(req, AppData.GROUP_KEY);
        UpcomingList.getInstance().updateRecording(req);
        if (groupKey == null || groupKey.length() == 0) {
            return;
        }
        if (!removeAll) {
            return;
        }
        unset(groupKey);
    }

    public static String createManualProgramId(long from, long to) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(from);
        StringBuffer sb = new StringBuffer(10);
        sb.append(c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE));
        sb.append('_');
        c.setTimeInMillis(to);
        sb.append(c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE));
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.createManualProgramId() returns "+sb.toString());
        return sb.toString();
    }

    /////////////////////////////////////////////////////////////////////////
    // get / set APIs
    /////////////////////////////////////////////////////////////////////////

    public boolean contains(String groupKey) {
        return get(groupKey) != null;
    }

    public Series get(String groupKey) {
        if (groupKey == null || groupKey.length() == 0) {
            return null;
        }
        return (Series) series.get(groupKey);
    }

    public Series remove(String groupKey) {
        if (groupKey == null || groupKey.length() == 0) {
            return null;
        }
        return (Series) series.remove(groupKey);
    }

    public int getType(String groupKey) {
        Series s = get(groupKey);
        if (s == null) {
            return UNKNOWN;
        } else {
            return s.type;
        }
    }

    public String getCallLetter(String groupKey) {
        Series s = get(groupKey);
        if (s != null) {
            return s.channel;
        } else {
            return null;
        }
    }

    public String getTitle(String groupKey) {
        Series s = get(groupKey);
        if (s != null) {
            return s.title;
        } else {
            return null;
        }
    }

//    public void setTitle(String groupKey, String title) {
//        Series s = get(groupKey);
//        if (s != null) {
//            s.title = title;
//            s.writeToFile();
//        }
//    }

    public int getKeepCount(String groupKey) {
        Series s = get(groupKey);
        if (s != null) {
            return s.keepCount;
        }
        return 0;
    }

    public void setKeepCount(String groupKey, int keepCount) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setKeepCount("+groupKey+", "+keepCount+")");
        Series s = get(groupKey);
        if (s != null) {
            s.keepCount = keepCount;
            s.writeToFile();
            deleteRecordings(groupKey, keepCount);
        }
        RemotePvrHandler.updateVersions();
        if (App.SUPPORT_HN) {
            HomeNetManager.getInstance().updateKeepCount(groupKey, keepCount);
        }
    }

    public String getRepeatString(String groupKey) {
        if (groupKey == null) {
            return null;
        }
        Series s = (Series) series.get(groupKey);
        if (s != null) {
            return s.repeatString;
        }
        return null;
    }

    public void reset(String groupKey, RecordingRequest target) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.reset("+groupKey+", "+target+")");
        String rs = getRepeatString(groupKey);
        if (rs == null) {
            rs = REPEAT_ALL_STRING;
        }
        setRepeatString(groupKey, rs, target);
    }

    public void setRepeatString(String groupKey, String string, RecordingRequest target) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setRepeatString("+groupKey+", "+string+")");
        int keep = getKeepCount(groupKey);
        makeSingle(target);
        makeRepeat(target, string, keep, false, true);
        RemotePvrHandler.updateScheduledVersion();
    }

    /** Returns the list of group key. */
    public Vector getInactiveList() {
        return inactiveGroupKeys;
    }

    private synchronized void startTimer() {
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(checkInactivityTimer);
        try {
            checkInactivityTimer = timer.scheduleTimerSpec(checkInactivityTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.timerWentOff()");
        if (Log.DEBUG_ON) Log.printDebug("RecordingScheduler.checkInactivity...");
        PvrConfig config = (PvrConfig) DataCenter.getInstance().get(ConfigManager.PVR_CONFIG_INSTANCE);
        if (config == null) {
            return;
        }
        //->Kenneth[2015.9.30] VDTRMASTER-5658 : VT Test 용으로 임시로 하루를 한시간으로 인식하게 함.
        //long timeout = config.seriesInactiveTimeout * Constants.MS_PER_HOUR;
        long timeout = config.seriesInactiveTimeout * Constants.MS_PER_DAY;
        //<-
        if (Log.DEBUG_ON) Log.printDebug("RecordingScheduler.checkInactivity : timeout = " + timeout);

        //->Kenneth[2015.9.30] VDTRMASTER-5658 : 관련하여 미구현 된 부분 구현함
        if (timeout == 0) {
            if (Log.DEBUG_ON) Log.printDebug("RecordingScheduler.checkInactivity : Return without INACTIVATING");
            return;
        }
        //<-

        Enumeration en = series.elements();
        boolean inactive;
        while (en.hasMoreElements()) {
            Series s = (Series) en.nextElement();
            inactive = s.checkInactive(timeout);
            if (inactive && !inactiveGroupKeys.contains(s.groupKey)) {
                inactiveGroupKeys.addElement(s.groupKey);
            }
        }
    }

    /////////////////////////////////////////////////////////////////////////
    // Create / delete APIs
    /////////////////////////////////////////////////////////////////////////

    // 시리즈 지우기
    public synchronized void unset(String groupKey) {
        // TODO - file을 지우지 않고, 남겨둔다. List는 남겨둬야 하기 때문에 inactive 처리한다.
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.unset("+groupKey+")");
        Object o = series.remove(groupKey);
        if (o != null) {
            Series s = (Series) o;
            s.state = STATE_DELETING;
            if (Log.DEBUG_ON) {
                Log.printDebug("RecordingScheduler.unset : found = " + s);
            }
            try {
                File f = new File(SERIES_DIRECTORY, s.groupKey);
                f.delete();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        deleteRecordings(groupKey);
        inactiveGroupKeys.remove(groupKey);
    }

    private static void deleteRecordings(String groupKey) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.deleteRecordings("+groupKey+")");
        RecordingList list = RecordingListManager.getInstance().getPendingList();
        list = RecordingListManager.filter(list, AppData.GROUP_KEY, groupKey);
        if (list != null) {
            for (int i = 0; i < list.size(); i++) {
                RecordingRequest r = list.getRecordingRequest(i);
                try {
                    r.delete();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
    }

    // from RecordingUpdater
    public boolean remove(String groupKey, String channel, long time) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.remove("+groupKey+", "+channel+", "+new Date(time)+")");
        Series s = get(groupKey);
        if (s == null) {
            return false;
        }
        return s.remove(channel, time);
    }

    // series 만드는거
    public String setSeries(TvProgram p, RecordingOption option) throws RemoteException {
        SeriesEpisode sp = new SeriesEpisode(p);
        return setSeriesImpl(sp, option.startOffset, option.endOffset, p.getTitle(),
            p.getCategory(), p.getSubCategory(), PvrUtil.getDefinition(p), p.getLanguage(), p.getRating(),
            p.getSeriesId(), option.getRepeatString(), option.keepOption, option.onlyFromEpgCache, false);
    }

    private String setSeries(RecordingRequest req, String repeatString, int keepCount,
                             boolean onlyFromEpgCache, boolean fromStart) throws RemoteException {
        long start = AppData.getScheduledStartTime(req);
        long end = start + AppData.getScheduledDuration(req);
        long pst = AppData.getLong(req, AppData.PROGRAM_START_TIME);
        long pet = AppData.getLong(req, AppData.PROGRAM_END_TIME);
        long startOffset = start - pst;
        long endOffset = end - pet;
        SeriesEpisode sp = new SeriesEpisode(
                    AppData.getString(req, AppData.CHANNEL_NAME),
                    pst,
                    pet,
                    AppData.getString(req, AppData.PROGRAM_ID),
                    AppData.getString(req, AppData.EPISODE_TITLE));
        return setSeriesImpl(sp,
                    startOffset,
                    endOffset,
                    AppData.getString(req, AppData.TITLE),
                    AppData.getInteger(req, AppData.CATEGORY),
                    AppData.getInteger(req, AppData.SUBCATEGORY),
                    AppData.getInteger(req, AppData.DEFINITION),
                    AppData.getString(req, AppData.LANGUAGE),
                    AppData.getString(req, AppData.PARENTAL_RATING),
                    AppData.getString(req, AppData.SERIES_ID),
                    repeatString, keepCount, onlyFromEpgCache, fromStart);
    }


    private String setSeriesImpl(SeriesEpisode sp, long startOffset, long endOffset, String title,
            int cat, int subCat, int def, String language, String rating,
            String seriesId, String repeatString, int keepCount, boolean onlyFromEpgCache, boolean fromStart) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setSeriesImpl("+title+")");
        String groupKey = Series.createGroupKey(sp, SERIES, seriesId);
        Series s = (Series) series.get(groupKey);
        if (s == null) {
            s = new Series(groupKey, title, sp.from, repeatString, keepCount, cat, subCat, def, language, rating, startOffset, endOffset);
            series.put(groupKey, s);
            Log.printDebug("RecordingScheduler.setSeries : created = " + s);
        } else {
            s.keepCount = keepCount;
            s.repeatString = mergeRepeatString(s.repeatString, repeatString);
        }
        s.add(sp);
        s.startThread(sp, onlyFromEpgCache, fromStart, false);
        return groupKey;
    }

    // manual from IXC
    public String setManualRepeat(TvChannel ch, String title, long from, long to,
                    String repeatString, int keepCount, boolean onlyFromEpgCache, boolean sync) throws RemoteException {
        return setManualRepeat(ch.getCallLetter(), title, from, to, 0, 0, 0, "", "", repeatString, keepCount, onlyFromEpgCache, false, sync);
    }

    // manual from RecordManager
    public String setManualRepeat(RecordingRequest req, String repeatString, int keepCount,
                                  boolean onlyFromEpgCache, boolean fromStart, boolean sync) throws RemoteException {
        return setManualRepeat(
            AppData.getString(req, AppData.CHANNEL_NAME),
            AppData.getString(req, AppData.TITLE),
            AppData.getLong(req, AppData.PROGRAM_START_TIME),
            AppData.getLong(req, AppData.PROGRAM_END_TIME),
            AppData.getInteger(req, AppData.CATEGORY),
            AppData.getInteger(req, AppData.SUBCATEGORY),
            AppData.getInteger(req, AppData.DEFINITION),
            AppData.getString(req, AppData.LANGUAGE),
            AppData.getString(req, AppData.PARENTAL_RATING),
            repeatString, keepCount, onlyFromEpgCache, fromStart, sync);
    }

    // manual
    private String setManualRepeat(String callLetter, String title, long from, long to,
                    int cat, int subCat, int def, String language, String rating,
                    String repeatString, int keepCount, boolean onlyFromEpgCache, boolean fromStart, boolean sync) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setManualRepeat("+title+")");
        String id = createManualProgramId(from, to);
        SeriesEpisode sp = new SeriesEpisode(callLetter, from, to, id, "");
        return setRepeatImpl(sp, 0, 0, title, cat, subCat, def, language, rating, repeatString, keepCount, true, onlyFromEpgCache, fromStart, sync);
    }

    // forced
    public String setForcedRepeat(TvProgram p, RecordingOption option, boolean sync)
                                                    throws RemoteException {
        return setForcedRepeat(p, option.startOffset, option.endOffset, option.getRepeatString(), option.keepOption, option.onlyFromEpgCache, sync);
    }

    public String setForcedRepeat(TvProgram p, long startOffset, long endOffset, String repeatString, int keepCount,
                                  boolean onlyFromEpgCache, boolean sync) throws RemoteException {
        SeriesEpisode sp = new SeriesEpisode(p);
        return setRepeatImpl(sp, startOffset, endOffset, p.getTitle(),
                p.getCategory(), p.getSubCategory(), PvrUtil.getDefinition(p), p.getLanguage(), p.getRating(),
                repeatString, keepCount, false, onlyFromEpgCache, false, sync);
    }

    // forced (modify)
    private String setForcedRepeat(RecordingRequest req, String repeatString, int keepCount,
                                                boolean onlyFromEpgCache, boolean fromStart) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setForcedRepeat("+req+")");
        long start = AppData.getScheduledStartTime(req);
        long end = start + AppData.getScheduledDuration(req);
        long pst = AppData.getLong(req, AppData.PROGRAM_START_TIME);
        long pet = AppData.getLong(req, AppData.PROGRAM_END_TIME);
        long startOffset = start - pst;
        long endOffset = end - pet;
        SeriesEpisode sp = new SeriesEpisode(
                    AppData.getString(req, AppData.CHANNEL_NAME),
                    pst,
                    pet,
                    AppData.getString(req, AppData.PROGRAM_ID),
                    AppData.getString(req, AppData.EPISODE_TITLE));
        return setRepeatImpl(sp,
                    startOffset,
                    endOffset,
                    AppData.getString(req, AppData.TITLE),
                    AppData.getInteger(req, AppData.CATEGORY),
                    AppData.getInteger(req, AppData.SUBCATEGORY),
                    AppData.getInteger(req, AppData.DEFINITION),
                    AppData.getString(req, AppData.LANGUAGE),
                    AppData.getString(req, AppData.PARENTAL_RATING),
                    repeatString, keepCount, false, onlyFromEpgCache, fromStart, false);
    }

    private String setRepeatImpl(SeriesEpisode sp, long startOffset, long endOffset, String title,
            int cat, int subCat, int def, String language, String rating,
            String repeatString, int keepCount, boolean manual, boolean onlyFromEpgCache, boolean fromStart, boolean sync) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.setRepeatImpl("+title+")");
        String groupKey;
        if (manual) {
            groupKey = Series.createGroupKey(sp, MANUAL_REPEAT, "_");
        } else {
            groupKey = Series.createGroupKey(sp, PROGRAM_REPEAT, title);
        }
        if (manual && REPEAT_ALL_STRING.equals(repeatString)) {
            repeatString = "1111111";
        }

        Series s = (Series) series.get(groupKey);
        if (s == null) {
            s = new Series(groupKey, title, sp.from, repeatString, keepCount, cat, subCat, def, language, rating, startOffset, endOffset);
            series.put(groupKey, s);
            Log.printDebug("RecordingScheduler.setRepeat : created = " + s);
        } else {
            s.keepCount = keepCount;
            s.repeatString = mergeRepeatString(s.repeatString, repeatString);
        }
        s.add(sp);
        s.startThread(sp, onlyFromEpgCache, fromStart, sync);
        return groupKey;
    }

    private String mergeRepeatString(String old, String rs) {
        if (old == null) {
            return rs;
        }
        if (old.startsWith(REPEAT_ALL_STRING) || rs.equals(REPEAT_ALL_STRING)) {
            return REPEAT_ALL_STRING;
        }
        char[] c = new char[7];
        for (int i = 0; i < 7; i++) {
            c[i] = (old.charAt(i) == '1' || rs.charAt(i) == '1') ? '1' : '0';
        }
        return new String(c);
    }

    public boolean update(String groupKey, RecordingRequest req, TvProgram p) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.update("+groupKey+", "+req+", "+p+")");
        boolean changed = false;

        String oldEpisodeTitle = AppData.getString(req, AppData.EPISODE_TITLE);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.update() : Call AppData.update()");
        changed = AppData.update(req, p) || changed;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.update() : changed = "+changed);

        Series targetSeries = get(groupKey);
        if (targetSeries == null) {
            Log.printWarning("RecordingScheduler.update : not found Series");
            return changed;
        }

        String newEpisodeTitle = AppData.getString(req, AppData.EPISODE_TITLE);
        if (!oldEpisodeTitle.equals(newEpisodeTitle)) {
            if (Log.INFO_ON) {
                Log.printInfo(App.LOG_HEADER+"RecordingScheduler.update : changed episode title = " + oldEpisodeTitle + " -> " + newEpisodeTitle);
            }
            targetSeries.updateEpisodeTitle(req, newEpisodeTitle);
            changed = true;
        }
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.update() returns "+changed);
        return changed;
    }

    public void epgDataUpdated(byte type, long from, long to) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.epgDataUpdated("+type+", "+new Date(from)+", "+new Date(to)+")");
        if (type == EPG_DATA) {
            epgDataUpdateWorker.push(new ScheduleUpdater(from, to));
        }
    }

    /** Conflict에 의해 다른 episode를 선택. */
    public void rescheduleSeries(RecordingRequest req, String groupKey, TvProgram p) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.rescheduleSeries("+groupKey+", "+p+")");
        Series s = (Series) series.get(groupKey);
        if (s != null) {
            s.remove(AppData.getString(req, AppData.CHANNEL_NAME), AppData.getLong(req, AppData.PROGRAM_START_TIME));
            SeriesEpisode sp = new SeriesEpisode(p);
            s.add(sp);
            s.writeToFile();
        }
        long start = AppData.getScheduledStartTime(req);
        long end = start + AppData.getScheduledDuration(req);
        long pst = AppData.getLong(req, AppData.PROGRAM_START_TIME);
        long pet = AppData.getLong(req, AppData.PROGRAM_END_TIME);
        long startOffset = start - pst;
        long endOffset = end - pet;
        try {
            req.delete();
        } catch (Exception ex) {
            Log.print(ex);
        }
        RecordManager.getInstance().seriesRecord(p, startOffset, endOffset, groupKey, RecordingOption.TYPE_SERIES, false, AppData.NEW_TIMESLOT_BY_CONFLICT);
    }

    public void notifyRecordingCompleted(RecordingRequest req) {
        Object obj = AppData.get(req, AppData.GROUP_KEY);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.notifyRecordingCompleted() : groupKey = "+obj);
        if (obj == null) {
            return;
        }
        String groupKey = obj.toString();
        deleteRecordings(groupKey, getKeepCount(groupKey));
    }

    public synchronized void deleteRecordings(final String groupKey, final int keepCount) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.deleteRecordings("+groupKey+", "+keepCount+")");
        if (keepCount == 0) {
            return;
        }
        Thread t = new Thread("RecordingScheduler.deleteRecordings") {
            public void run() {
                synchronized (RecordingScheduler.this) {
                    RecordingList list = RecordingListManager.getInstance().getRecordedList();
                    list = RecordingListManager.filter(list, AppData.GROUP_KEY, groupKey);
                    list = RecordingListManager.sortByDate(list);
                    Vector v = new Vector();
                    if (list != null) {
                        int size = list.size();
                        for (int i = size - keepCount - 1; i >= 0; i--) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("RecordingScheduler.deleteRecordings : index = " + i + " / " + size);
                            }
                            RecordingRequest r = list.getRecordingRequest(i);
                            RecordingListManager.markToBeDeleted(r);
//                            AppData.set(r, AppData.PROGRAM_TYPE, AppData.TO_BE_DELETED);
                            v.addElement(r);
                        }
                    }
                    Enumeration en = v.elements();
                    while (en.hasMoreElements()) {
                        RecordingRequest r = (RecordingRequest) en.nextElement();
                        try {
                            r.delete();
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                }
            }
        };
        t.start();
    }

    class ScheduleUpdater implements Runnable {
        long from, to;

        ScheduleUpdater(long from, long to) {
            this.from = from;
            this.to = to;
        }

        public void run() {
            Map.Entry entry;
            Set set = series.entrySet();
            Iterator it = set.iterator();
            while (it.hasNext()) {
                entry = (Map.Entry) it.next();
                Series s = (Series) entry.getValue();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.ScheduleUpdater.run() : from = "+new Date(from));
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.ScheduleUpdater.run() : to = "+new Date(to));
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.ScheduleUpdater.run() : Series = "+s);
                //->Kenneth[2015.9.30] VDTRMASTER-5658 
                // Inactive series 도 addPrograms 하도록 한다. 이로인한 부하가 생길 수 있음.
                try {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.ScheduleUpdater.run() : Call Series.addPrograms(null)");
                    s.addPrograms(null, from, to, false);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                /*
                if (s.state != STATE_INACTIVE) {
                    try {
                        s.addPrograms(null, from, to, false);
                    } catch (Exception ex) {
                        Log.print(ex);
                    }
                }
                */
                //<-
            }
        }

        public String toString() {
            return "ScheduleUpdater";
        }
    }

    //->Kenneth[2015.9.30] VDTRMASTER-5658 
    public void removeSeriesFromInactivceGroup(Series s) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingScheduler.removeSeriesFromInactivceGroup("+s+")");
        inactiveGroupKeys.remove(s.groupKey);
    }
    //<-
}

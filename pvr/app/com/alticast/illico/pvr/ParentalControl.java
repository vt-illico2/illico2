package com.alticast.illico.pvr;

import java.rmi.*;
import java.util.*;
import javax.tv.util.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.util.*;
import com.alticast.illico.pvr.ui.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.ui.*;
import com.videotron.tvi.illico.ixc.upp.*;
import org.ocap.shared.dvr.RecordingRequest;

/**
 * ParentalControl.
 */
public class ParentalControl implements DataUpdateListener {

    private static ParentalControl instance = new ParentalControl();

    public static ParentalControl getInstance() {
        return instance;
    }

    public static boolean enabled;
    public static int ratingIndex = Constants.RATING_INDEX_18 + 10;
    public static boolean hideAdult;
    public static boolean pinChecked = false;
    public static boolean protect = false;

    public static final int OK              = 0;
    public static final int RATING_OVER     = 1;
    public static final int ADULT_CONTENT   = 2;
    public static final int BLOCKED_CHANNEL = 3;
    public static final int MANUAL_BLOCKED  = 4;

    private static final HashSet unblockedRecordings = new HashSet();
    private static HashSet blockedChannelSet = new HashSet();

    private ParentalControl() {
        DataCenter dc = DataCenter.getInstance();
        dc.addDataUpdateListener(RightFilter.PARENTAL_CONTROL, this);
        dc.addDataUpdateListener(RightFilter.BLOCK_BY_RATINGS, this);
        dc.addDataUpdateListener(RightFilter.HIDE_ADULT_CONTENT, this);
        dc.addDataUpdateListener(RightFilter.PIN_CODE_UNBLOCKS_CHANNEL, this);
        dc.addDataUpdateListener(RightFilter.CHANNEL_RESTRICTION, this);
        dc.addDataUpdateListener(RightFilter.PROTECT_PVR_RECORDINGS, this);
    }

    public void init() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.init()");
    }

    public void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.pause()");
        unblockedRecordings.clear();
        pinChecked = false;
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (RightFilter.PARENTAL_CONTROL.equals(key)) {
            enabled = Definitions.OPTION_VALUE_ON.equals(value);
            Log.printInfo("ParentalControl.enabled = " + enabled);

        } else if (RightFilter.BLOCK_BY_RATINGS.equals(key)) {
            int r = PvrUtil.getRatingIndex((String) value);
            if (r == Constants.RATING_INDEX_G) {
                r = Constants.RATING_INDEX_18 + 10;
            }
            ratingIndex = r;
            Log.printInfo("ParentalControl.ratingIndex = " + ratingIndex);

        } else if (RightFilter.HIDE_ADULT_CONTENT.equals(key)) {
            hideAdult = Definitions.OPTION_VALUE_YES.equals(value);
            Log.printInfo("ParentalControl.hideAdult = " + hideAdult);

        } else if (RightFilter.CHANNEL_RESTRICTION.equals(key)) {
            Log.printInfo("ParentalControl.blocked_channels = " + value);
            resetBlockedChannels((String) value);

        } else if (RightFilter.PROTECT_PVR_RECORDINGS.equals(key)) {
            Log.printInfo("ParentalControl.protect = " + value);
            protect = Definitions.OPTION_VALUE_YES.equals(value);
        }

        UIComponent comp = MenuController.getInstance().getCurrentScene();
        if (comp != null) {
            if (comp instanceof ListPanel) {
                ((ListPanel) comp).focusChanged();
            }
//            comp.setVisible(true);
        }
    }

    public void dataRemoved(String key) {
    }

    private synchronized void resetBlockedChannels(String line) {
        HashSet set = new HashSet();
        if (line != null) {
            String[] tokens = TextUtil.tokenize(line, PreferenceService.PREFERENCE_DELIMETER);
            for (int i = 0; i < tokens.length; i++) {
                set.add(tokens[i]);
            }
        }
        blockedChannelSet = set;
    }

    // 지금 local system이 protected 설정인지
    public static boolean isProtected() {
        return enabled && !pinChecked && protect;
    }

    public static boolean isProtected(AbstractRecording r) {
        return !pinChecked && r.isProtected();
    }

    public static boolean isProtected(Vector recordings) {
        if (pinChecked) {
            return false;
        }
        try {
            int size = recordings.size();
            for (int i = 0; i < size; i++) {
                Object o = recordings.elementAt(i);
                if (o instanceof AbstractRecording) {
                    AbstractRecording r = (AbstractRecording) o;
                    if (r.isProtected() || isAccessBlocked(r)) {
                        return true;
                    }
                } else if (o instanceof RecordingRequest) {
                    RecordingRequest r = (RecordingRequest) o;
                    if (isProtected() || isAccessBlocked(r)) {
                        return true;
                    }
                }
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public static void unblock(ProgramDetail r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.unblock("+r+")");
        unblockedRecordings.add(r.getIdObject());
    }

    public static void unblock(RecordingFolder folder) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.unblock("+folder+")");
        unblockedRecordings.add(folder.getTitle());
    }

    public static void unblock(String title) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.unblock("+title+")");
        unblockedRecordings.add(title);
    }

    public static void unblock(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.unblock("+req+")");
        try {
            unblockedRecordings.add(new Integer(req.getId()));
        } catch (Exception ex) {
        }
    }

    public static void clear(ProgramDetail r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.clear("+r+")");
        unblockedRecordings.remove(r.getIdObject());
    }

    public static void clear(String title) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.clear("+title+")");
        unblockedRecordings.remove(title);
    }

    public static void clear(RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"ParentalControl.clear("+req+")");
        try {
            unblockedRecordings.remove(new Integer(req.getId()));
        } catch (Exception ex) {
        }
    }

    public static boolean isAccessBlocked(ProgramDetail r) {
        return getLevel(r) != OK;
    }

    public static boolean isAccessBlocked(RecordingFolder folder) {
        return getLevel(folder) != OK;
    }

    public static boolean isAccessBlocked(RecordingRequest r) {
        return getLevel(r) != OK;
    }

    public static boolean isReleased(RecordingRequest r) {
        return unblockedRecordings.contains(new Integer(r.getId()));
    }

    public static boolean isReleased(ProgramDetail r) {
        return unblockedRecordings.contains(r.getIdObject());
    }

    public static boolean isReleased(RecordingFolder folder) {
        return unblockedRecordings.contains(folder.getTitle());
    }

    public static int getLevel(ProgramDetail r) {
        if (r == null || pinChecked || isReleased(r)) {
            return OK;
        }
        int level = getLevel(r.getRatingIndex());
        if (level != OK) {
            return level;
        }
        if (r.isBlocked()) {
            return MANUAL_BLOCKED;
        }
        if (!enabled) {
            return OK;
        }
        if (blockedChannelSet.contains(r.getChannelName())) {
            return BLOCKED_CHANNEL;
        } else {
            return OK;
        }
    }

    public static int getLevel(RecordingRequest r) {
        if (r == null || pinChecked || isReleased(r)) {
            return OK;
        }
        int level = getLevel(PvrUtil.getRatingIndex(r));
        if (level != OK) {
            return level;
        }
        if (AppData.getBoolean(r, AppData.BLOCK)) {
            return MANUAL_BLOCKED;
        }
        if (!enabled) {
            return OK;
        }
        if (blockedChannelSet.contains(AppData.getString(r, AppData.CHANNEL_NAME))) {
            return BLOCKED_CHANNEL;
        } else {
            return OK;
        }
    }

    public static int getLevel(RecordingFolder folder) {
        if (folder == null || pinChecked || isReleased(folder)) {
            return OK;
        }
        int level = getLevel(folder.getRatingIndex());
        if (level != OK) {
            return level;
        }
        if (folder.isBlocked()) {
            return MANUAL_BLOCKED;
        }
        return OK;
    }

    public static int getLevel(int contentRating) {
        if (!enabled || pinChecked) {
            return OK;
        }
        if (hideAdult && contentRating >= Constants.RATING_INDEX_18) {
            return ADULT_CONTENT;
        }
        if (contentRating >= ratingIndex) {
            return RATING_OVER;
        } else {
            return OK;
        }
    }

    public static String getTitle(RecordingFolder folder) {
        if (folder == null) {
            return "";
        }
        int level = getLevel(folder);
        switch (level) {
//            case MANUAL_BLOCKED:
//                return DataCenter.getInstance().getString("List.manual_blocked");
            case ADULT_CONTENT:
                return DataCenter.getInstance().getString("List.blocked");
            default:
                return folder.getTitle();
        }
    }

    public static String getTitle(ProgramDetail r) {
        if (r == null) {
            return "";
        }
        int level = getLevel(r);
        switch (level) {
//            case MANUAL_BLOCKED:
//                return DataCenter.getInstance().getString("List.manual_blocked");
            case ADULT_CONTENT:
                return DataCenter.getInstance().getString("List.blocked");
            default:
                return r.getTitle();
        }
    }

    public static String getTitle(RecordingRequest r) {
        int level = OK;
        if (r != null && enabled && !pinChecked && hideAdult) {
            int ratingIndex = PvrUtil.getRatingIndex(r);
            if (ratingIndex >= Constants.RATING_INDEX_18) {
                level = ADULT_CONTENT;
            }
        }
        switch (level) {
//            case MANUAL_BLOCKED:
//                return DataCenter.getInstance().getString("List.manual_blocked");
            case ADULT_CONTENT:
                return DataCenter.getInstance().getString("List.blocked");
            default:
                return AppData.getString(r, AppData.TITLE);
        }
    }

    public static String getEpisodeTitle(AbstractRecording r) {
        if (r == null) {
            return "";
        }
        int level = getLevel(r);
        switch (level) {
//            case MANUAL_BLOCKED:
//                return DataCenter.getInstance().getString("List.manual_blocked");
            case ADULT_CONTENT:
                return DataCenter.getInstance().getString("List.blocked");
            default:
                return r.getEpisodeTitle();
        }
    }

    public String toString() {
        DataCenter dataCenter = DataCenter.getInstance();
        return "Setting=" + dataCenter.getString(RightFilter.PARENTAL_CONTROL)
            + ", Rating=" + dataCenter.getString(RightFilter.BLOCK_BY_RATINGS) + "(" + ratingIndex
            + "), Adult=" + dataCenter.getString(RightFilter.HIDE_ADULT_CONTENT)
            + ", Protect=" + dataCenter.getString(RightFilter.PROTECT_PVR_RECORDINGS)
            + ", PIN Checked=" + pinChecked;
    }

}

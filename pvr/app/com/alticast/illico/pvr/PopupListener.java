package com.alticast.illico.pvr;

public interface PopupListener {

    public boolean handleHotKey(int code);

    public boolean consumeKey(int code);

    public void selected(int focus);

    public void canceled();

}

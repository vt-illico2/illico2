package com.alticast.illico.pvr;

import java.rmi.RemoteException;
import org.ocap.shared.media.TimeShiftControl;
import com.alticast.illico.pvr.util.PvrUtil;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

public class RecordingOption {

    public static final long MAX_UNKNOWN_PROGRAM_DURATION = 2 * Constants.MS_PER_HOUR;
    public static final long DEFAULT_UNKNOWN_PROGRAM_DURATION = Constants.MS_PER_HOUR;

    public static final int TYPE_REPEAT = 0;
    public static final int TYPE_SERIES = 1;
    public static final int TYPE_SINGLE = 2;

    public int type = TYPE_SINGLE;

    public static final short REPEAT_ALL   = 0; // any day, any time
    public static final short REPEAT_DAY   = 1; // every same day, same time
    public static final short REPEAT_ANY   = 2; // ant day, same time

    private short repeat = REPEAT_ALL;

    private String repeatString = RecordingScheduler.REPEAT_ALL_STRING;

    /** Record all or one. */
    public boolean all = false;

    public int keepOption = 0;  // 0 = keep all, 5 = keep 5 episodes
    public long saveUntil = Long.MAX_VALUE;

    // for non-manual recording, 편성 시간
    public long programStartTime;
    public long programEndTime;
    // 실제 녹화할 시간
    public long startTime;
    public long endTime;
    // buffer
    public long startOffset;
    public long endOffset;

    public String title;

    // remote로 녹화한 것은 conflict 를 보여주지 않고 지우기 때문에 true로 set 되어 들어온다
    public boolean suppressConflict;

    public TvProgram program;
    public TvChannel channel;

    // for another episodes of series
    public String groupKey;

    public boolean onlyFromEpgCache = false;

    public int creationMethod;

    /** For manual recording. */
    public RecordingOption(TvChannel ch, long start, long end) {
        channel = ch;
        startTime = start;
        endTime = end;
    }

    /** For program recording. */
    public RecordingOption(TvChannel ch, TvProgram p) {
        this(ch, p, RecordManager.getInstance().getRecordingBuffer());
    }

    /** For program recording. */
    public RecordingOption(TvChannel ch, TvProgram p, long buffer) {
        this(ch, p, -buffer, buffer);
    }

    public RecordingOption(TvChannel ch, TvProgram p, long startOffset, long endOffset) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"RecordingOption: offset = " + startOffset + ", " + endOffset);
        }
        program = p;
        channel = ch;
        this.startOffset = startOffset;
        this.endOffset = endOffset;
        int programType = TvProgram.TYPE_UNKNOWN;
        try {
            programStartTime = p.getStartTime();
            programEndTime = p.getEndTime();
            startTime = programStartTime + startOffset;
            endTime = programEndTime + endOffset;
            title = p.getTitle();
            programType = p.getType();
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        if (programType == TvProgram.TYPE_UNKNOWN && (endTime - startTime) > MAX_UNKNOWN_PROGRAM_DURATION) {
            Log.printInfo("RecordingOption: TYPE_UNKNOWN proram has long duration : " + p);
            startTime = System.currentTimeMillis();
            endTime = startTime + DEFAULT_UNKNOWN_PROGRAM_DURATION;
        } else {
            // 이미 시작한 프로그램
/*
            long nst = System.currentTimeMillis();
            if (startTime < nst) {
                TunerController tc = Core.getInstance().getTunerController(channel);
                if (tc != null) {
                    TimeShiftControl tsc = tc.getControl();
                    if (tsc != null) {
                        long bb = PvrUtil.getTime(tsc.getBeginningOfBuffer());
                        if (bb < nst) {
                            nst = Math.max(bb, startTime);
                            Log.printInfo("RecordingOption: reset start time from TSB");
                        }
                    }
                }
                startTime = nst;
            }
*/
        }
    }

    public void setRepeat(short newRepeat, boolean[] days) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingOption.setRepeat("+newRepeat+")");
        repeat = newRepeat;
        switch (repeat) {
            case REPEAT_ALL:
                repeatString = RecordingScheduler.REPEAT_ALL_STRING;
                break;
            case REPEAT_DAY:
                char[] c = new char[7];
                for (int i = 0; i < 7; i++) {
                    c[i] = (days.length > i && days[i]) ? '1' : '0';
                }
                repeatString = new String(c);
                break;
            case REPEAT_ANY:
                repeatString = "1111111";
                break;
        }
    }

    public void setRepeatString(String repeatString) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"RecordingOption.setRepeatString("+repeatString+")");
        this.repeatString = repeatString;
    }

    public String getRepeatString() {
        return repeatString;
    }

}

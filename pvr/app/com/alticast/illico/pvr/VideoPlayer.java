package com.alticast.illico.pvr;

import java.awt.event.KeyEvent;
import javax.media.*;
import javax.media.Time;
import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.selection.*;
import java.util.Hashtable;
import org.ocap.dvr.OcapRecordingManager;
import org.ocap.dvr.TimeShiftEvent;
import org.ocap.dvr.TimeShiftListener;
import org.ocap.dvr.TimeShiftProperties;
import org.ocap.service.*;
import org.ocap.shared.media.BeginningOfContentEvent;
import org.ocap.shared.media.EndOfContentEvent;
import org.ocap.shared.media.EnteringLiveModeEvent;
import org.ocap.shared.media.LeavingLiveModeEvent;
import org.ocap.shared.media.TimeShiftControl;
import org.dvb.event.*;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.storage.ExtendedFileAccessPermissions;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.hn.Device;
import org.ocap.hn.recording.RecordingContentItem;
import org.ocap.hn.service.RemoteService;

import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.popup.*;
import com.alticast.illico.pvr.hn.*;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.flipbar.*;
//->Kenneth[2015.6.24] DDC-107
import com.videotron.tvi.illico.util.VideoOutputUtil;
import com.videotron.tvi.illico.util.VideoOutputListener;
//<-

/**
 * VideoPlayer
 *
 * @author  June Park
 */
public class VideoPlayer extends TunerController implements UserEventListener, VideoOutputListener /*DDC-107*/{

    VideoFeedback feedback = VideoFeedback.getInstance();

    private UserEventRepository keyEvents;

    private static VideoPlayer instance = new VideoPlayer();

    public static VideoPlayer getInstance() {
        return instance;
    }

    int viewedThreshold = 95;
    boolean loading;
    AbstractRecording lastRecording = null;

    float startRate = 1.0f;

    private LogStateListener logStateListener;

    public static final String ERROR_CODE_VIEWING_FAIL_LOCAL  = "PVR801";
    public static final String ERROR_CODE_VIEWING_FAIL_REMOTE = "PVR802";

    public static final String VIEWING_FAILED_NO_SERVICE = "no service";
    public static final String VIEWING_FAILED_DEVICE_REMOVED = "device removed";

    protected VideoPlayer() {
        keyEvents = new UserEventRepository("PVR.VideoControlEvents");
        keyEvents.addKey(KeyCodes.FAST_FWD);
        keyEvents.addKey(KeyCodes.REWIND);
        keyEvents.addKey(KeyCodes.PLAY);
        keyEvents.addKey(KeyCodes.PAUSE);
        keyEvents.addKey(KeyCodes.LIVE);
        keyEvents.addKey(KeyCodes.STOP);
        keyEvents.addKey(KeyCodes.RECORD);
        keyEvents.addKey(KeyCodes.FORWARD);
        keyEvents.addKey(KeyCodes.BACK);
        keyEvents.addKey(OCRcEvent.VK_ENTER);
        keyEvents.addKey(OCRcEvent.VK_EXIT);

        viewedThreshold = DataCenter.getInstance().getInt("COMPLETELY_VIEWED_THRESHOLD");
        viewedThreshold = Math.min(100, viewedThreshold);
        //->Kenneth[2015.6.24] DDC-107
        VideoOutputUtil.getInstance().addVideoOutputListener(this);
        //<-
    }

    public void init(int tunerIndex) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.init("+tunerIndex+")");
        super.init(tunerIndex);
        EventManager.getInstance().addUserEventListener(this, keyEvents);
    }

    public void dispose() {
        super.dispose();
        EventManager.getInstance().removeUserEventListener(this);
    }

    public void pauseXlet() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.pauseXlet()");
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ".pauseXlet : state = " + getStateString(state));
        }
        if (state == PLAY_RECORDING) {
            notifyStopped();
            updateMediaTime();
        }
    }

    protected void setState(short newState) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.setState("+newState+")");
        synchronized (STATE_LOCK) {
            int oldState = state;
            super.setState(newState);
            FlipBarWindow fw = FlipBarWindow.getInstance();
            switch (newState) {
                case LIVE_TV:
                    fw.ready(currentId, channel, program);
                    break;
                case PLAY_RECORDING:
                    fw.ready(currentRecording);
                    fw.setFocus(FlipBarAction.PAUSE);
                    fw.start();
                    sendPlayLog();
                    break;
                case STOPPED:
                    fw.stop();
                    break;

            }
            if (oldState != newState && newState == PLAY_RECORDING || oldState == PLAY_RECORDING) {
                // PLAY_RECORDING 상태가 되었거나, 아니게 되었거나
                stateChanged(Core.getInstance().getMonitorState());
                if (newState != PLAY_RECORDING) {
                    PlayInfo.getInstance().stop();
                }
            }
        }
    }

    public void playRecording(AbstractRecording r, int option) {
        playRecording(r, option, 1.0f);
    }

    protected void playRecording(AbstractRecording r, int option, float rate) {
        Log.printDebug(App.LOG_HEADER+"VideoPlayer.playRecording: option = " + option + ", rate = " + rate);
        startRate = rate;
        currentRecording = r;
        lastRecording = r;
        setState(STOPPED);
        try {
            if (r != null) {
                switch (option) {
                    case PLAY_FROM_BEGINNING:
                        r.setMediaTime(0);
                        break;
                    case PLAY_RESUME:
                        break;
                    case PLAY_CURRENT:
                        r.setMediaTime(Long.MAX_VALUE);
                        break;
                }
            }
            synchronized (this) {
                loading = true;
                Service s = null;
                if (r != null) {
                    s = r.getService();
                }
                Log.printDebug("VideoPlayer.playRecording: service = " + s);
                if (s != null) {
                    if (App.SUPPORT_HN) {
                        if (r != null && r instanceof RemoteRecording) {
                            Core.getInstance().showLoading();
                        }
                    }
                    if (App.R3_TARGET) {
                        SharedMemory sm = SharedMemory.getInstance();
                        sm.put(App.NAME + SharedDataKeys.VIDEO_PROGRAM_NAME, r.getTitle());
                        sm.put(App.NAME + SharedDataKeys.VIDEO_RESOLUTION, new Boolean(r.getDefinition() == 1));
                        Core.monitorService.reserveVideoContext(App.NAME, null);
                    }
                    Environment.getServiceContext(0).select(s);
                    //->Kenneth[2015.6.24] DDC-107
                    if (Environment.SUPPORT_UHD && r.getDefinition() == TvChannel.DEFINITION_4K) {
                        boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.playRecording() : canPlayUhd = "+canPlayUhd);
                        if (canPlayUhd) {
                            // HDMI 가 옳게 연결되어 있으므로 analog blocking 을 하도록 한다.
                            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.playRecording() : Call setEnableOfAnalogueOutputPorts(false)");
                            VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(false);
                        }
                    } else {
                        // Analog blocking 을 푼다
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.playRecording() : Call setEnableOfAnalogueOutputPorts(true)");
                        VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
                    }
                    //<-
                } else {
                    showViewFailedPopup(r, VIEWING_FAILED_NO_SERVICE, true);
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
//            setState(PLAY_RECORDING);
            showViewFailedPopup(r, ex.toString(), true);
        }
        PvrVbmController.getInstance().writeStartRecordingWatch(r);
//        PvrVbmController.getInstance().writeViewingLocation(r);
    }

    //->Kenneth[2015.6.24] DDC-107
    public void updateVideoOutputStatus() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus()");
        //->Kenneth[2015.7.30] EPG 와 PVR 에서 중복되게 이벤트 처리하는 이슈 발견
        // 따라서 PVR 플레이 중이 아니면 처리하지 않는다.
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : state = "+state);
        if (state != PLAY_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Not PLAY_RECORDING. So do nothing");
            return;
        } 
        //<-
        boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : canPlayUhd = "+canPlayUhd);
        boolean analogBlocked = VideoOutputUtil.getInstance().isAnalogPortsBlocked();
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : analogBlocked = "+analogBlocked);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : SUPPORT_UHD = "+Environment.SUPPORT_UHD);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : lastRecording = "+lastRecording);
        if (!canPlayUhd && analogBlocked) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call setEnableOfAnalogueOutputPorts(true)");
            //->Kenneth[2015.7.30] analog output 켜기 전에 content 가 보호 되어야 하는 경우
            // 리스트로 나가도록 한다. 중요한 것은 여기서 HDCP2.2 를 구분하지 않는다. 왜냐하면 그에 관계 없이 analog
            // output 에 video 가 나가는 것을 막는 것 뿐이므로
            if (Environment.SUPPORT_UHD && lastRecording != null && lastRecording.getDefinition() == TvChannel.DEFINITION_4K) {
                // 리스트 화면으로 이동하고 동시에 Incompatibility popup 을 띄움.
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call exitToList() -- 1");
                exitToList();
                //->Kenneth[2015.7.30] hot plugged 받았다고 굳이 팝업을 보여줄 이유가 없음. 
                //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call showIncompatibilityErrorMessage() -- 1");
                //Core.showIncompatibilityErrorMessage();
            }
            VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
            return;
            //<-
        //->Kenneth[2015.7.30] blocking 하는 코드 추가
        } else if (canPlayUhd && !analogBlocked) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call setEnableOfAnalogueOutputPorts(false)");
            VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(false);
        //<-
        }
        //->Kenneth[2015.7.28] HDCP2.2 체크도 추가
        if (Environment.SUPPORT_UHD && lastRecording != null && lastRecording.getDefinition() == TvChannel.DEFINITION_4K) {
            if (VideoOutputUtil.getInstance().existHdcpApi()) {
                boolean hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : hdcp22Supported = "+hdcp22Supported);
                if (!hdcp22Supported) {
                    // 리스트 화면으로 이동하고 동시에 Incompatibility popup 을 띄움.
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call exitToList()");
                    exitToList();
                    //->Kenneth[2015.7.30] hot plugged 받았다고 굳이 팝업을 보여줄 이유가 없음. 
                    //if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Call showIncompatibilityErrorMessage()");
                    //Core.showIncompatibilityErrorMessage();
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateVideoOutputStatus() : Leave PVR851");
                    Log.printError("PVR851");
                }
            }
        }
        //<-
    }
    //<-

    public synchronized boolean playLastRecording() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.playLastRecording() : lastRecording = "+lastRecording);
        boolean toPlay = false;
        toPlay = true;
        AbstractRecording r = lastRecording;
//        if (r != null) {
//            toPlay = true;
//            Service s = getService();
//            if (s != null) {
//                toPlay = !(s instanceof RemoteService) && !(s instanceof RecordedService);
//            } else {
//                Log.printWarning("VideoPlayer.playLastRecording : service is null");
//            }
//        } else {
//            Log.printWarning("VideoPlayer.playLastRecording : recording is null");
//        }
        if (toPlay) {
            if (r != null) {
                playRecording(r, PLAY_RESUME, recordingRate);
            } else {
                Log.printWarning("VideoPlayer.playLastRecording : can't find last recording");
                return false;
            }
        } else {
//            try {
//                Core.monitorService.reserveVideoContext(App.NAME, null);
//            } catch (Exception ex) {
//                Log.print(ex);
//            }
            Log.printWarning("VideoPlayer.playLastRecording : can't play");
        }
        return toPlay;
    }

    public void contentChanged(RecordingContentItem ce) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.contentChanged("+ce+")");
        AbstractRecording l = lastRecording;
        RemoteRecording rr = null;
        if (l != null && l instanceof RemoteRecording && l.getIdObject().toString().startsWith(ce.getID())) {
            rr = new RemoteRecording(ce);
            Log.printDebug("VideoPlayer.contentChanged : lastRecording");
            lastRecording = rr;
        }
        AbstractRecording r = currentRecording;
        if (r != null && r instanceof RemoteRecording && state == PLAY_RECORDING && r.getIdObject().toString().startsWith(ce.getID())) {
            if (rr == null) {
                rr = new RemoteRecording(ce);
            }
            Log.printDebug("VideoPlayer.contentChanged : currentRecording");
            currentRecording = rr;
            FlipBarWindow.getInstance().update(rr);
        }
    }

    public synchronized void notifyDeviceRemoved(Device dev) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.notifyDeviceRemoved("+dev+")");
        if (dev == null || !loading) {
            return;
        }
        AbstractRecording rec = currentRecording;
        if (rec != null && rec instanceof RemoteRecording) {
            RemoteRecording rr = (RemoteRecording) rec;
            if (dev.equals(rr.getDevice())) {
                Log.printWarning("VideoPlayer.notifyDeviceRemoved: removed device for current recording !");
                if (loading) {
                    Core.getInstance().hideLoading();
                    loading = false;
                }
                showViewFailedPopup(rec, VIEWING_FAILED_DEVICE_REMOVED, false);
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Video Control APIs
    ///////////////////////////////////////////////////////////////////////

    public void skipForward() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.skipForward()");
        if (state == PLAY_RECORDING || state == LIVE_TV && !isLiveMode && Environment.SUPPORT_DVR) {
            feedback.start(VideoFeedback.SKIP_FORWARD);
        }
        super.skipForward();
    }

    public void skipBackward() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.skipBackward()");
        if (state == PLAY_RECORDING || state == LIVE_TV && Environment.SUPPORT_DVR) {
            feedback.start(VideoFeedback.SKIP_BACKWARD);
        }
        super.skipBackward();
    }

    ///////////////////////////////////////////////////////////////////////
    // Key Controls
    ///////////////////////////////////////////////////////////////////////

    public void playSlow() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.playSlow()");
        boolean check = true;
        if (state == PLAY_RECORDING && App.SUPPORT_HN) {
            check = HomeNetManager.checkPlaySlow(currentRecording);
        }
        if (check) {
            super.playSlow();
        } else {
            Log.printWarning("VideoPlayer: slow playing is blocked with remote recording.");
        }
    }

    /** UserEventListener */
    public void userEventReceived(UserEvent event) {
        if (event.getType() != KeyEvent.KEY_PRESSED) {
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("VideoPlayer: userEventReceived = " + event);
        }
        int code = event.getCode();
        processUserEvent(code);
    }

    public boolean processUserEvent(int code) {
        Popup p = PopupController.getInstance().getCurrent();
        if (p != null) {
            boolean toBlock = true;
            if (code == KeyCodes.PLAY) {
                PopupListener pl = p.getListener();
                if (pl != null && pl instanceof StopPopupListener) {
                    PopupController.getInstance().currentSceneStop();
                    Log.printDebug("VideoPlayer: play request with stop popup.");
                    toBlock = false;
                }
            }
            if (toBlock) {
                Log.printInfo("VideoPlayer: no action under popup.");
                return false;
            }
        }
        boolean trickable = state == PLAY_RECORDING || bufferFound;
        FlipBarWindow fw = FlipBarWindow.getInstance();
        switch (code) {
            case KeyCodes.FAST_FWD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(FAST_FWD)");
                if (trickable) {
                    fw.setFocus(FlipBarAction.FAST_FORWARD);
                    fastForward();
                }
                break;
            case KeyCodes.REWIND:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(REWIND)");
                if (trickable) {
                    fw.setFocus(FlipBarAction.REWIND);
                    rewind();
                }
                break;
            case KeyCodes.PLAY:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(PLAY)");
                if (trickable) {
                    fw.setFocus(FlipBarAction.PLAY);
                    togglePlaySlow();
                }
                break;
            case KeyCodes.PAUSE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(PAUSE)");
                if (trickable) {
                    boolean pause = togglePause();
                    if (pause) {
                        fw.setFocus(FlipBarAction.PAUSE);
                    } else {
                        fw.setFocus(FlipBarAction.PLAY);
                    }
                }
                break;
            case KeyCodes.FORWARD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(FORWARD)");
                if (trickable) {
                    fw.setFocus(FlipBarAction.SKIP_FORWARD);
                    skipForward();
                }
                break;
            case KeyCodes.BACK:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(BACK)");
                if (trickable) {
                    fw.setFocus(FlipBarAction.SKIP_BACKWARD);
                    skipBackward();
                }
                break;
            case KeyCodes.LIVE:
            case OCRcEvent.VK_EXIT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(EXIT or LIVE)");
                if (state == LIVE_TV) {
                    playLive();
                } else if (state == PLAY_RECORDING) {
                    handleKey(code);
                }
                break;
            case KeyCodes.STOP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(STOP)");
                fw.setFocus(FlipBarAction.STOP);
                stop();
                break;
            case KeyCodes.RECORD:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(RECORD)");
                if (state != PLAY_RECORDING) {
                    fw.setFocus(FlipBarAction.RECORD);
                    record();
                }
                break;
            case OCRcEvent.VK_ENTER:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.processUserEvent(VK_ENTER)");
                keyEnter();
                break;
        }
        return trickable;
    }

    /** from MenuController (in PVR app mode. not watch TV mode). */
    public boolean handleKey(int code) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey("+code+")");
        AbstractRecording rs = currentRecording;
        if (Log.DEBUG_ON) {
            Log.printDebug("VideoPlayer.handleKey: state = " + getStateString(state) + ", recording = " + rs);
        }
        if (state != PLAY_RECORDING || rs == null) {
            // 녹화물 play 중이 아니면 skip.
            if (state == STOPPED && rs != null) {
                if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
                    return true;
                }
            }
            if (code == KeyCodes.LIST) {
                exitToList();
            } else {
                return false;
            }
        }
        // TODO - watch tv state로 나가면 number키를 못받을 수 있다. 처리...
        if (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9) {
            if (FlipBarWindow.getInstance().isSkippable()) {
                int number = code - OCRcEvent.VK_0;
                if (Log.DEBUG_ON) {
                    Log.printDebug("VideoPlayer.handleKey: number key = " + number);
                }
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : Call play()");
                play(rs.getRecordedDuration() * number / 10);
                return true;
            }
            if (App.R3_TARGET) {
                return false;
            }
            return true;
        }
        switch (code) {
            case OCRcEvent.VK_INFO:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : VK_INFO");
                FlipBarWindow.getInstance().hideAll();
                PlayInfo.getInstance().start(rs);
                return true;
            case OCRcEvent.VK_LAST:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : VK_LAST");
                if (App.R3_TARGET) {
                    updateMediaTime();
                    stopAndExit(false);
                    return true;
                } else {
                    return false;
                }
            case OCRcEvent.VK_EXIT:
            case KeyCodes.LIVE:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : VK_EXIT or LIVE");
                updateMediaTime();
                stopAndExit(false);
                return true;
            case KeyCodes.LIST:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : LIST");
                updateMediaTime();
                exitToList();
                return true;
            case KeyCodes.PIP:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : PIP");
                if (App.R3_TARGET) {
                    return false;
                } else {
                    // show feed back
                    NoPipIcon.getInstance().start();
                    return true;
                }
            case KeyCodes.SEARCH:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : SEARCH");
                if (Core.standAloneMode || state == TunerController.PLAY_RECORDING) {
                    NoPipIcon.getInstance().start();
                    return true;
                }
                return false;
            case OCRcEvent.VK_CHANNEL_UP:
            case OCRcEvent.VK_CHANNEL_DOWN:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : CHANNEL_UP/DOWN");
                // show feed back
                NoPipIcon.getInstance().start();
                return true;
            case KeyCodes.WIDGET:
            case KeyCodes.MENU:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : WIDGET/MENU");
                FlipBarWindow.getInstance().hideAll();
                return false;
            case KeyCodes.COLOR_A:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : COLOR_A");
                return true;
            case OCRcEvent.VK_UP:
            case OCRcEvent.VK_DOWN:
            case OCRcEvent.VK_LEFT:
            case OCRcEvent.VK_RIGHT:
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.handleKey() : UP/DOWN/LEFT/RIGHT");
                FlipBarWindow.getInstance().start();
                return true;
//                updateMediaTime();
//                stopAndExit();
//                return false;
        }
        return false;
    }

    public boolean showHideFlipBar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.showHideFlipBar()");
        FlipBarWindow fw = FlipBarWindow.getInstance();
        if (fw.isStarted()) {
            fw.hideAll();
            return true;
        } else {
            return keyEnter();
        }
    }

    public boolean keyEnter() {
        if (state == PLAY_RECORDING || state == LIVE_TV && !isLiveMode && bufferFound) {
            FlipBarWindow.getInstance().start();
            return true;
        } else {
            return false;
        }
    }

    public void record() {
        Log.printDebug(App.LOG_HEADER+"VideoPlayer.record()");
        if (state != LIVE_TV) {
            Log.printInfo(App.LOG_HEADER+"VideoPlayer.record: Not in LIVE_TV state =" + getStateString(state));
            return;
        }
        if (!Core.getInstance().checkPermission()) {
            Log.printWarning(App.LOG_HEADER+"VideoPlayer.record: no PVR permission.");
            return;
        }
        try {
            EpgService es = Core.epgService;
            TvChannel ch = es.getChannelContext(0).getCurrentChannel();
            if (!ch.isRecordable() || !ch.isSubscribed()) {
                Log.printInfo(App.LOG_HEADER+"VideoPlayer.record: not recordable channel = " + ch);
                return;
            }
            long mediaTime = getMediaTime();
            TvProgram program = es.getProgram(ch, mediaTime);
            if (program != null) {
                RecordingRequest req = RecordManager.getInstance().getRequest(program);
                if (req == null) {
                    Log.printDebug(App.LOG_HEADER+"VideoPlayer.record() : Call PvrServiceImpl.record()");
                    PvrServiceImpl.getInstance().record(ch, program);
                } else {
                    Log.printDebug(App.LOG_HEADER+"VideoPlayer.record() : Call RecordingIcon.start()");
                    RecordingIcon.getInstance().start();
                }
            } else {
                Log.printInfo(App.LOG_HEADER+"VideoPlayer.record: program is null. time = " + mediaTime);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+"VideoPlayer: stop. state = " + getStateString(state));
        }
        switch (state) {
            case PLAY_RECORDING:
                AbstractRecording r = currentRecording;
                feedback.start(VideoFeedback.STOP);
                pause();
                String s = "";
                if (r != null) {
                    updateMediaTime();
                    Core.getInstance().stopOverlappingApplication();
                    //->Kenneth[2017.3.15] : R7.3
                    PopupController.getInstance().showPlayStopPopup(r.getTitle(), new StopPopupListener(r));
                    //PopupController.getInstance().showPlayStopPopup(new StopPopupListener(r));
                    //<-
                    sendStoppedLog();
                }
                break;
            case LIVE_TV:
                // check recording
                RecordingRequest req = RecordManager.getInstance().getRequest(currentId);
                if (req != null) {
                    PvrServiceImpl.getInstance().cancel(req);
                }
                break;
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////

//    /** TimeShiftListener implemetation */
//    public synchronized void receiveTimeShiftevent(TimeShiftEvent e) {
//        super.receiveTimeShiftevent(e);
//    }

    /** ControllerListener implemetation */
    public synchronized void controllerUpdate(ControllerEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.controllerUpdate("+e+")");
        boolean beforeLiveMode = isLiveMode;
        float oldRate = currentRate;
        super.controllerUpdate(e);

        if (e instanceof RateChangeEvent || (e instanceof MediaTimeSetEvent && (!isLiveMode || !beforeLiveMode || state == PLAY_RECORDING))) {
            FlipBarWindow.getInstance().start();
            if (e instanceof RateChangeEvent) {
                float rate = ((RateChangeEvent) e).getRate();
                feedback.rateChanged(rate);
                sendPausedLog(Math.abs(rate) < 0.01f);
            }
            if (e instanceof EndOfContentEvent) {
                AbstractRecording r = currentRecording;
                // 녹화물이 끝까지 온 경우
                if (state == PLAY_RECORDING && r != null
                                && ((EndOfContentEvent) e).getRate() == 0.0f) {
                    if (r != null) {
                        // 2013_02_01 - rolled back by request from VDTRSUPPORT-241
//                        if (oldRate > 1.5f) {
//                            long dur = r.getRecordedDuration();
//                            if (dur > 16 * Constants.MS_PER_SECOND) {
//                                setMediaTime(dur - 15 * Constants.MS_PER_SECOND);
//                            }
//                        }
                        increaseViewedCount(r);
                        r.setMediaTime(0);
                        Core.getInstance().stopOverlappingApplication();
                        PopupController.getInstance().showPlayFinishedPopup(r.getTitle(), new FinishedPopupListener(r));
                        try {
                            pause();
                        } catch (Exception ex) {
                        }
                        try {
                            if (logStateListener != null) {
                                logStateListener.playbackStopped();
                            }
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                }
            }
        } else if (beforeLiveMode != isLiveMode && bufferFound) {
            Service nowService = getService();
            boolean changed = false;
            if (nowService != null) {
                if (e instanceof EnteringLiveModeEvent) {
                    TvChannel currentChannel = channel;
                    if (currentChannel != null) {
                        try {
                            TvChannel ch = Core.epgService.getChannelContext(tunerIndex).getCurrentChannel();
                            if (ch != null && currentChannel.getId() == ch.getId()) {
                                changed = true;
                            }
                        } catch (Exception ex) {
                            Log.print(ex);
                        }
                    }
                } else {
                    changed = true;
                }
            }
            if (changed) {
                FlipBarWindow.getInstance().start();
            } else {
                FlipBarWindow.getInstance().stop();
            }
        }

        if (e instanceof BeginningOfContentEvent) {
            Core.resetScreenSaverTimer();
        }
    }

    /** ServiceStateListener. (Monitor) from Core. */
    public void stateChanged(int newMonitorState) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.stateChanged("+newMonitorState+")");
        synchronized (STATE_LOCK) {
            boolean enable;
            if (App.R3_TARGET) {
                enable = (newMonitorState != MonitorService.FULL_SCREEN_APP_STATE);
            } else {
                enable = (state == PLAY_RECORDING)
                    || (newMonitorState != MonitorService.FULL_SCREEN_APP_STATE);
            }
            FlipBarWindow.getInstance().enableFlipBar(enable);
            VideoFeedback.getInstance().enableFeedback(enable);
        }
    }

    /**
     * ServiceContextListener implemetation.
     */
    public synchronized void receiveServiceContextEvent(ServiceContextEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.receiveServiceContextEvent("+e+")");
        if (App.R3_TARGET) {
            String failedEvent = null;
            int reason = -1;
            if (e instanceof SelectionFailedEvent) {
                failedEvent = "SelectionFailedEvent";
                reason = ((SelectionFailedEvent) e).getReason();
            } else if (e instanceof AlternativeContentErrorEvent) {
                failedEvent = "AlternativeContentErrorEvent";
                reason = ((AlternativeContentErrorEvent) e).getReason();
            }
            if (loading) {
                Core.getInstance().hideLoading();
                loading = false;
                if (failedEvent != null) {
                    showViewFailedPopup(currentRecording, failedEvent + ".reason = " + reason + " : from start", false);
                }
            } else if (state == PLAY_RECORDING) {
                // playing
                if (e instanceof PresentationTerminatedEvent) {
                    reason = ((PresentationTerminatedEvent) e).getReason();
                    Log.printDebug("VideoPlayer.receiveServiceContextEvent: PresentationTerminatedEvent.reason = " + reason);
                    if (reason == PresentationTerminatedEvent.SERVICE_VANISHED || reason == PresentationTerminatedEvent.RESOURCES_REMOVED) {
                        showViewFailedPopup(currentRecording, "PresentationTerminatedEvent.reason = " + reason, false);
                    }
                } else if (failedEvent != null) {
                    if (!(e instanceof SelectionFailedEvent) || reason != SelectionFailedEvent.INTERRUPTED) {
                        showViewFailedPopup(currentRecording, failedEvent + ".reason = " + reason + " : while playing", false);
                    }
                }
            }
        }
        super.receiveServiceContextEvent(e);
        if (state == PLAY_RECORDING) {
            float absRate = Math.abs(startRate);
            if (absRate < 0.1f || absRate >= 2.0f) {
                Log.printDebug("VideoPlayer.receiveServiceContextEvent: Call pause() and startRate = 1.0f");
                pause();
                startRate = 1.0f;
            //->Kenneth[2017.9.28] VDTRMASTER-6214 : recordingRate 값이 setRate 시에만 남김
            // 따라서 pause 후 다른 recording 을 정상 play 해도 setRate 한 것은 아니기 때문에 playLastRecording 에서 여전히 pause 
            // 기록이 남아서 back 키 이동시에 pause 발생. 따라서 여기서 reset 해준다
            } else if (startRate == 1.0f) {
                Log.printDebug("VideoPlayer.receiveServiceContextEvent: Set recordingRate = 1.0f");
                recordingRate = 1.0f;
            //<-
            }
        } else if (state == LIVE_TV) {
            if (!isLiveMode && bufferFound) {
                FlipBarWindow.getInstance().start();
            } else {
                FlipBarWindow.getInstance().hideAll();
            }
        }
    }

    /** cid = current channel id */
    protected void updateRecordingStatus(int cid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateRecordingStatus("+cid+")");
        super.updateRecordingStatus(cid);
        FlipBar bar = FlipBarWindow.getInstance().getTimeshiftFlipBar();
        Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateRecordingStatus() : recording = "+recording);
        Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateRecordingStatus() : state = "+state);
        if (recording) {
            if (state == LIVE_TV) {
                Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateRecordingStatus() : Call RecordingIcon.start()");
                RecordingIcon.getInstance().start();
            }
        } else {
            Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateRecordingStatus() : Call RecordingIcon.stop()");
            RecordingIcon.getInstance().stop();
        }
        bar.setEnabled(FlipBarAction.STOP, recording);
        bar.setEnabled(FlipBarAction.RECORD, !recording);
    }

    private void notifyStopped() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.notifyStopped()");
//        if (App.SUPPORT_HN) {
//            AbstractRecording r = currentRecording;
//            if (r != null && r instanceof LocalRecording) {
//                r.setAppData(AppData.PLAYING, Boolean.FALSE);
//            }
//        }
        FlipBarWindow.getInstance().stop();
        PvrVbmController.getInstance().writeEndRecordingWatch();
    }

    public boolean stopPlaying(boolean stop) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.stopPlaying: state = " + getStateString(state));
        synchronized (this) {
            if (loading) {
                Core.getInstance().hideLoading();
                loading = false;
            }
        }
        if (state == LIVE_TV) {
            return false;
        }
        notifyStopped();
        setState(STOPPED);
        if (stop || Core.standAloneMode) {
            if (App.R3_TARGET) {
                try {
                    Core.epgService.getChannelContext(0).stopChannel();
                } catch (Exception ex) {
                    Log.print(ex);
                }
            } else {
                Environment.getServiceContext(0).stop();
            }
        }
        if (App.R3_TARGET) {
            try {
                Core.monitorService.releaseVideoContext();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        //->Kenneth[2015.6.24] DDC-107
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.stopPlaying() : Call setEnableOfAnalogueOutputPorts(true)");
        VideoOutputUtil.getInstance().setEnableOfAnalogueOutputPorts(true);
        //<-
        return true;
    }

    private void stopAndExit(boolean stop) {
        Log.printDebug(App.LOG_HEADER+"VideoPlayer.stopAndExit("+stop+") state = " + getStateString(state));
        stopPlaying(stop);
        currentRecording = null;
//        try {
//            Core.epgService.getChannelContext(0).changeChannel(-1);
//        } catch (Exception ex) {
//            Log.print(ex);
//        }
        Core.getInstance().exitToChannel();
    }

    public ListPanel exitToList() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.exitToList()");
        lastRecording = null;
        stopPlaying(true);
        UIComponent comp = MenuController.getInstance().getCurrentScene();
        ListPanel ret = null;
        if (comp != null) {
            if (comp instanceof ListPanel) {
                ret = (ListPanel) comp;
                ret.updateFolder();
                //->Kenneth[2017.3.15] R7.3 : list 의 button 의 state 가 바뀔 수 있으므로 이에 대한
                // 업데이트를 해줘야 하기 때문에 추가
                ret.focusChanged();
                //<-
            }
            comp.setVisible(true);
        }
        return ret;
    }

    public void updateMediaTime() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.updateMediaTime()");
        AbstractRecording r = currentRecording;
        if (r != null) {
            long mt = getMediaTime();
            long dur = r.getRecordedDuration();
            if (viewedThreshold < 100 && mt * 100 >= dur * viewedThreshold) {
                increaseViewedCount(r);
            }
            if (Log.DEBUG_ON) {
                Log.printDebug("VideoPlayer: updateMediaTime = " + mt);
            }
            if (mt < 0 || mt > dur) {
                Log.printWarning("VideoPlayer: updateMediaTime. negative or range over");
                return;
            }
            r.setMediaTime(mt);
            if (r instanceof LocalRecording) {
                RemotePvrHandler.updateRecordedVersion();
            }
        }
    }

    private void increaseViewedCount(AbstractRecording r) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.increaseViewedCount()");
        if (r.getStateGroup() == RecordingListManager.COMPLETED) {
            r.setAppData(AppData.VIEW_COUNT, new Integer(r.getViewedCount() + 1));
        } else {
            Log.printDebug("VideoPlayer.increaseViewedCount: recording is in-progress.");
        }
    }

    ////////////////////////////////////////////////////////////////////
    // Log state
    ////////////////////////////////////////////////////////////////////

    public void setLogStateListener(LogStateListener l) {
        logStateListener = l;
        synchronized (this) {
            if (l != null && state == PLAY_RECORDING) {
                sendPlayLog();
                if (isPaused()) {
                    sendPausedLog(true);
                }
            }
        }
    }

    private void sendPausedLog(boolean paused) {
        LogStateListener l = logStateListener;
        if (l != null) {
            try {
                l.playbackPaused(paused);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void sendStoppedLog() {
        LogStateListener l = logStateListener;
        if (l != null) {
            try {
                l.playbackStopped();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    private void sendPlayLog() {
        if (state != PLAY_RECORDING) {
            return;
        }
        LogStateListener l = logStateListener;
        AbstractRecording ar = currentRecording;
        if (l == null || ar == null) {
            return;
        }
        Hashtable t = new Hashtable();
        t.put(LogStateListener.KEY_STATE, "pvr");
        t.put(LogStateListener.KEY_CONTENT_ID, String.valueOf(ar.getLocalId()));
        t.put(LogStateListener.KEY_LENGTH, new Integer((int) (ar.getRecordedDuration() / Constants.MS_PER_SECOND)));
        t.put(LogStateListener.KEY_TITLE, ar.getTitle());
        t.put(LogStateListener.KEY_REC_LIST_VERSION, new Integer(RemotePvrHandler.recordedVersion));
        t.put(LogStateListener.KEY_SCH_LIST_VERSION, new Integer(RemotePvrHandler.scheduledVersion));
        t.put(LogStateListener.KEY_UDN, ar.getUdn());
        try {
            l.logStateChanged(t);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Popup
    ///////////////////////////////////////////////////////////////////////

    public void showViewFailedPopup(AbstractRecording r, String str, boolean forced) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.showViewFailedPopup("+str+", "+forced+")");
        if (!forced && !App.NAME.equals(Core.getInstance().getVideoContextName())) {
            Log.printDebug("VideoPlayer.showViewFailedPopup : skipped by videoContext");
            return;
        }
        String code = ERROR_CODE_VIEWING_FAIL_LOCAL;
        if (r != null) {
            if (r instanceof RemoteRecording) {
                code = ERROR_CODE_VIEWING_FAIL_REMOTE;
                Log.printError(ERROR_CODE_VIEWING_FAIL_REMOTE + " : " + str);
            } else {
                Log.printError(ERROR_CODE_VIEWING_FAIL_LOCAL + " : " + str);
            }
        }
        exitToList();
        Core.getInstance().showErrorMessage(code, null);
//        DataCenter dataCenter = DataCenter.getInstance();
//        InfoOneButtonPopup popup = new InfoOneButtonPopup(dataCenter.getString("multiroom.cannot_view_title"));
//        popup.fillBg = true;
//        popup.setDescription(TextUtil.replace(dataCenter.getString("multiroom.cannot_view_msg"), "|", "\n"));
//        PopupAdapter popupListener = new PopupAdapter() {
//            public void selected(int focus) {
//                exitToList();
//            }
//            public boolean consumeKey(int code) {
//                switch (code) {
//                    case KeyCodes.LIST:
//                    case KeyCodes.SEARCH:
//                    case KeyCodes.MENU:
//                    case KeyCodes.SETTINGS:
//                    case KeyCodes.WIDGET:
//                    case KeyCodes.VOD:
//                    case KeyCodes.PIP:
//                    case OCRcEvent.VK_GUIDE:
//                    case OCRcEvent.VK_CHANNEL_UP:
//                    case OCRcEvent.VK_CHANNEL_DOWN:
//                        return true;
//                    default:
//                        return false;
//                }
//            }
//        };
//        popup.setListener(popupListener);
//        PopupController.getInstance().showPopup(popup);
    }

    class StopPopupListener extends FinishedPopupListener {

        public StopPopupListener(AbstractRecording ar) {
            super(ar);
        }

        public boolean handleHotKey(int code) {
            boolean videoKey = false;
            switch (code) {
                case KeyCodes.STOP:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.handleHotKey() : STOP");
                    FlipBarWindow.getInstance().start();
                    return false;
                case KeyCodes.PLAY:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.handleHotKey() : PLAY");
                    PopupController.getInstance().currentSceneStop();
                    return false;
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.handleHotKey() : super.handleHotKey()");
            return super.handleHotKey(code);
        }

        public void selected(int focus) {
            switch (focus) {
                case 0: // continue viewing
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.selected("+focus+") : Call play()");
                    play();
                    break;
                case 1: // play from beginning
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.selected("+focus+") : Call play(0)");
                    play(0);
                    break;
                case 2: // delete
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.StopPopupListener.selected("+focus+") : Call showDeletePopup()");
                    showDeletePopup();
                    break;
            }
        }

    }

    class FinishedPopupListener extends PopupAdapter implements PinEnablerListener {
        AbstractRecording recording;

        public FinishedPopupListener(AbstractRecording ar) {
            recording = ar;
        }

        public boolean handleHotKey(int code) {
            switch (code) {
                case OCRcEvent.VK_EXIT:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.handleHotKey() : EXIT");
                    stopAndExit(true);
                    return true;
                case KeyCodes.LIST:
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.handleHotKey() : LIST");
                    exitToList();
                    return true;
            }
            return false;
        }

        public boolean consumeKey(int code) {
            switch (code) {
                case KeyCodes.FAST_FWD:
                case KeyCodes.REWIND:
                case KeyCodes.PLAY:
                case KeyCodes.PAUSE:
                case KeyCodes.LIVE:
                case KeyCodes.STOP:
                case KeyCodes.RECORD:
                case KeyCodes.FORWARD:
                case KeyCodes.BACK:
                case OCRcEvent.VK_INFO:
                    return true;
                default:
                    return (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
            }
        }

        public void selected(int focus) {
            switch (focus) {
                case 0: // delete
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.selected() : Call showDeletePopup()");
                    showDeletePopup();
                    break;
                case 1: // play from beginning
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.selected() : Call play(0)");
                    play(0);
                    break;
            }
        }

        protected void showDeletePopup() {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.showDeletePopup()");
            DataCenter dataCenter = DataCenter.getInstance();
            String title = "pvr.delete_popup_title";
            String msg = "pvr.delete_popup_msg";
            ListSelectPopup popup = new ListSelectPopup();
            popup.setPopupTitle(dataCenter.getString(title));
            final String programTitle = recording.getTitle();
            popup.setDescription(dataCenter.getString(msg) + " '" + programTitle + "' ?");
            PopupAdapter listener = new PopupAdapter() {
                public boolean consumeKey(int code) {
                    switch (code) {
                        case KeyCodes.FAST_FWD:
                        case KeyCodes.REWIND:
                        case KeyCodes.PLAY:
                        case KeyCodes.PAUSE:
                        case KeyCodes.STOP:
                        case KeyCodes.RECORD:
                        case KeyCodes.FORWARD:
                        case KeyCodes.BACK:
                        case OCRcEvent.VK_INFO:
                            return true;
                        default:
                            return (code >= OCRcEvent.VK_0 && code <= OCRcEvent.VK_9);
                    }
                }
                public void selected(int focus) {
                    if (Log.DEBUG_ON)
                        Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.showDeletePopup().PopupAdapter.selected()");
                    if (focus == 0) {
                        delete();
                    } else {
                        if (FinishedPopupListener.this instanceof StopPopupListener) {
                            //->Kenneth[2017.3.15] : R7.3
                            PopupController.getInstance().showPlayStopPopup(programTitle, new StopPopupListener(recording));
                            //PopupController.getInstance().showPlayStopPopup(new StopPopupListener(recording));
                            //<-
                        } else {
                            PopupController.getInstance().showPlayFinishedPopup(programTitle, new FinishedPopupListener(recording));
                        }
                    }
                }
            };
            popup.setListener(listener);
            PopupController.getInstance().showPopup(popup);
        }

        protected void delete() {
            Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.delete()");
            AbstractRecording r = currentRecording;
            if (r == null || !ParentalControl.isProtected(r)) {
                deleteAndList();
            } else {
                PreferenceProxy.getInstance().showPinEnabler(DataCenter.getInstance().getString("pin.delete"), this);
            }
        }

        protected void deleteAndList() {
            Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.deleteAndList()");
            ListPanel list = exitToList();
            long mid = (FinishedPopupListener.this instanceof StopPopupListener)
                        ? PvrVbmController.MID_DELETE_FROM_STOP_PLAY
                        : PvrVbmController.MID_DELETE_FROM_END_PLAY;
            list.deleteRecording(currentRecording, mid);
//            Thread t = new Thread("deleteAndList") {
//                public void run() {
//                    Log.printDebug("VideoPlayer.delete");
//                    try {
//                        AbstractRecording r = currentRecording;
//                        boolean result = r.deleteIfPossible();
//                        if (!result) {
//                            PopupController.showCannotDeletePopup(r, PopupController.getInstance());
//                        }
//                    } catch (Exception ex) {
//                        Log.print(ex);
//                    }
//                }
//            };
//            t.start();
        }

        public void receivePinEnablerResult(int response, String detail) {
            Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.receivePinEnablerResult("+response+")");
            if (PreferenceService.RESPONSE_SUCCESS == response) {
                Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.receivePinEnablerResult() : Call deleteAndList()");
                deleteAndList();
            } else {
                Log.printDebug(App.LOG_HEADER+"VideoPlayer.FinishedPopupListener.receivePinEnablerResult() : Call exitToList()");
                exitToList();
            }
        }
    }

}

package com.alticast.illico.pvr.config;

import com.alticast.illico.pvr.ConfigManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import java.io.File;

public class PvrConfig {

    public int version;
    public int ccNoticeTimeout;
    public int warningSpaceLevel;
    public int criticalSpaceLevel;
    public int seriesInactiveTimeout;
    public int autoTuneDuration;
    public int tsbKeepDuration;
    public int infoDuration;
    //->Kenneth[2015.2.7] 4K
    public int uhdMaxTuners = -1;

    public static PvrConfig create(File file) {
        return create(BinaryReader.read(file));
    }

    public static PvrConfig create(byte[] data) {
        try {
            return new PvrConfig(data);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return null;
    }

    private PvrConfig(byte[] data) {
        int index = 0;
        this.version = ConfigManager.oneByteToInt(data, index++);
        this.ccNoticeTimeout = ConfigManager.twoBytesToInt(data, index);
        index += 2;
        this.warningSpaceLevel = ConfigManager.oneByteToInt(data, index++);
        this.criticalSpaceLevel = ConfigManager.oneByteToInt(data, index++);
        this.seriesInactiveTimeout = ConfigManager.twoBytesToInt(data, index);
        index += 2;
        this.autoTuneDuration = ConfigManager.twoBytesToInt(data, index);
        index += 2;
        this.tsbKeepDuration = ConfigManager.twoBytesToInt(data, index);
        index += 2;
        this.infoDuration = ConfigManager.twoBytesToInt(data, index);
        //->Kenneth[2015.2.7] 4K
        index += 2;
        try {
            if (index >= data.length) return;
            this.uhdMaxTuners = ConfigManager.oneByteToInt(data, index++);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public int getVersion() {
        return version;
    }

    public String toString() {
        return "PvrConfig [ccNoticeTimeout=" + ccNoticeTimeout +
                ", warningSpaceLevel=" + warningSpaceLevel +
                ", criticalSpaceLevel=" + criticalSpaceLevel +
                ", seriesInactiveTimeout=" + seriesInactiveTimeout +
                ", autoTuneDuration=" + autoTuneDuration +
                ", tsbKeepDuration=" + tsbKeepDuration +
                ", infoDuration=" + infoDuration +
                ", uhdMaxTuners=" + uhdMaxTuners+
                ", version=" + version + "]";
    }

    /** check version. */
    public boolean equals(Object o) {
        if (o != null && o instanceof PvrConfig) {
            return ((PvrConfig)o).getVersion() == version;
        }
        return false;
    }

}

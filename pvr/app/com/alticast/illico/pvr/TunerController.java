package com.alticast.illico.pvr;

import java.awt.event.KeyEvent;
import javax.media.*;
import javax.media.Time;
import javax.tv.locator.Locator;
import javax.tv.service.Service;
import javax.tv.service.selection.*;
import org.ocap.dvr.OcapRecordingManager;
import org.ocap.dvr.TimeShiftEvent;
import org.ocap.dvr.TimeShiftListener;
import org.ocap.dvr.TimeShiftProperties;
import org.ocap.shared.dvr.RecordingManager;
import org.ocap.shared.media.EndOfContentEvent;
import org.ocap.shared.media.EnteringLiveModeEvent;
import org.ocap.shared.media.LeavingLiveModeEvent;
import org.ocap.shared.media.TimeShiftControl;
import org.ocap.service.AlternativeContentErrorEvent;
import org.dvb.event.*;
import org.dvb.media.NoComponentSelectedEvent;
import org.ocap.shared.dvr.navigation.*;
import org.ocap.dvr.*;
import org.ocap.shared.dvr.*;
import org.ocap.storage.ExtendedFileAccessPermissions;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.hn.service.RemoteService;

import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.upp.*;
import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.pvr.communication.PreferenceProxy;
import com.alticast.illico.pvr.data.*;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.util.PvrUtil;
import com.videotron.tvi.illico.ixc.epg.ChannelEventListener;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.monitor.*;

/**
 * TunerController
 *
 * @author  June Park
 */
public class TunerController implements TimeShiftListener, ControllerListener,
                                        ServiceContextListener, RecordingChangedListener {

    private static final boolean REQUEST_BUFFERING_WHEN_SELECT = true;

    public static final int PLAY_FROM_BEGINNING = 0;
    public static final int PLAY_RESUME         = 1;
    public static final int PLAY_CURRENT        = 2;

    protected static final short STOPPED        = 0;
    protected static final short PLAY_RECORDING = 1;
    protected static final short LIVE_TV        = 2;
    protected static final short UNKNOWN        = 3;

    protected short state = UNKNOWN;

    public static final float SLOW_PLAY_RATE = 0.25f;
    public static final int MIN_FAST_RATE = 4;
    public static final int MAX_FAST_RATE = 128;

    public static long SKIP_FORWARD_DURATION = 30 * Constants.MS_PER_SECOND;
    public static long SKIP_BACKWARD_DURATION = 8 * Constants.MS_PER_SECOND;
    public static final long END_SKIP_MARGIN = 15 * Constants.MS_PER_SECOND;

    protected int tunerIndex;

    protected ServiceContext serviceContext;

    protected TimeShiftProperties tsp;
    protected Player player;
    protected Player originalPlayer;

    protected boolean isLiveMode = true;
    protected boolean tsbEnabled = true;
    protected boolean bufferFound = false;

    protected TvChannel channel;
    protected TvProgram program;
    protected int currentId = -1;
    protected boolean recording;
    protected AbstractRecording currentRecording;
    protected Service currentService;

    protected float currentRate = 1.0f;
    protected float recordingRate = 1.0f;

    private static final long TSB_DURATION = Constants.MS_PER_HOUR / Constants.MS_PER_SECOND;

    private ExtendedFileAccessPermissions defaultPermission
            = new ExtendedFileAccessPermissions(true, true, true, true, true, true, null, null);

    public String logPrefix = "TunerController";

    protected Object STATE_LOCK = new Byte((byte) 123);

    private OcapRecordingManager manager;

    protected TunerController() {
        SKIP_FORWARD_DURATION = DataCenter.getInstance().getLong("SKIP_FORWARD_DURATION");
        SKIP_BACKWARD_DURATION = DataCenter.getInstance().getLong("SKIP_BACKWARD_DURATION");
    }

    public void init(int tunerIndex) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.init("+tunerIndex+")");
        this.tunerIndex = tunerIndex;
        serviceContext = Environment.getServiceContext(tunerIndex);
        if (serviceContext == null) {
            Log.printWarning(logPrefix + ": serviceContext is null.");
            return;
        }
        serviceContext.addListener(this);

        RecordingManager rm = RecordingManager.getInstance();
        if (rm instanceof OcapRecordingManager) {
            manager = (OcapRecordingManager) rm;
            manager.addRecordingChangedListener(this);
        }

        if (serviceContext instanceof TimeShiftProperties) {
            tsp = (TimeShiftProperties) serviceContext;
            tsp.addTimeShiftListener(this);
        }
        resetPlayer();
        enableBuffering(true);
        try {
            tsp.setMinimumDuration(TSB_DURATION);
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
        }
        try {
            tsp.setMaximumDuration(TSB_DURATION);
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
        }
        tsp.setLastServiceBufferedPreference(false);
    }

    protected synchronized void resetPlayer() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.resetPlayer()");
        Player old = player;
        // set player
        ServiceContentHandler[] handlers = serviceContext.getServiceContentHandlers();
        for (int i = 0; i < handlers.length; i++) {
            if (handlers[i] instanceof Player) {
                player = (Player) handlers[i];
                break;
            }
        }
        if (player == null) {
            Log.printWarning(logPrefix + ": player not found.");
            player = originalPlayer;
        } else {
            if (Log.INFO_ON) {
                Log.printInfo(logPrefix + ": player found = " + player);
            }
            if (old != null && old != originalPlayer) {
                old.removeControllerListener(this);
            }
            if (player != originalPlayer) {
                player.addControllerListener(this);
            }
            if (originalPlayer == null) {
                originalPlayer = player;
            }
        }
    }

    public void dispose() {
        if (player != null) {
            player.removeControllerListener(this);
        }
        if (serviceContext != null) {
            serviceContext.removeListener(this);
        }
    }

    public int getTunerIndex() {
        return tunerIndex;
    }

    public void enableBuffering(boolean enable) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.enableBuffering("+enable+")");
        this.tsbEnabled = enable;
        OcapRecordingManager manager = (OcapRecordingManager) RecordingManager.getInstance();
        if (enable) {
            manager.enableBuffering();
        } else {
            manager.disableBuffering();
        }
    }

    public boolean isBufferingEnabled() {
        return this.tsbEnabled;
    }

    public boolean isLiveMode() {
        return isLiveMode;
    }

    /** returns TimeShiftControl of Player */
    public TimeShiftControl getControl() {
        TimeShiftControl control = null;
        try {
            control = (TimeShiftControl) player.getControl("org.ocap.shared.media.TimeShiftControl");
        } catch (Exception ex) {
        }
        return control;
    }

    public Player getPlayer() {
        return player;
    }

    public TvChannel getChannel() {
        return channel;
    }

    public Service getService() {
        return serviceContext.getService();
    }

    protected void setState(short newState) {
        synchronized (STATE_LOCK) {
            if (Log.DEBUG_ON) {
                Log.printDebug(logPrefix + ": setState = " + getStateString(newState));
            }
            this.state = newState;
            if (state == LIVE_TV) {
                updateRecordingStatus(currentId);
            } else {
                recording = false;
            }
        }
    }

    public short getState() {
        return state;
    }

    public boolean isPlaying(RecordingRequest r) {
        if (r == null || state != PLAY_RECORDING) {
            return false;
        }
        AbstractRecording rec = currentRecording;
        if (rec != null && rec instanceof LocalRecording) {
            return r.equals(((LocalRecording) rec).getRecordingRequest());
        } else {
            return false;
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // Video Control APIs
    ///////////////////////////////////////////////////////////////////////

    protected long getMediaTime() {
        return PvrUtil.getTime(player.getMediaTime());
    }

    /** 재생 */
    public void play() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.play()");
        player.setRate(1.0f);
    }

    /** 일시 정지 */
    public void pause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.pause()");
        player.setRate(0.0f);
    }

    /** 라이브 따라잡기 */
    public void playLive() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.playLive()");
        player.setMediaTime(new Time(Double.POSITIVE_INFINITY));
        player.setRate(1.0f);
    }

    /** 특정 시점 부터 재생 */
    public void play(long ms) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.play("+ms+")");
        setMediaTime(ms);
        player.setRate(1.0f);
    }

    protected void setMediaTime(long ms) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.setMediaTime("+ms+")");
        player.setMediaTime(new Time(ms * 1000000L));
    }

    public void move(long ms) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.move("+ms+")");
        setMediaTime(Math.max(getMediaTime() + ms, 0));
    }

    public void skipForward() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward()");
        if (state == LIVE_TV) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : LIVE_TV");
            move(SKIP_FORWARD_DURATION);
        } else if (state == PLAY_RECORDING) {
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : PLAY_RECORDING");
            AbstractRecording ar = currentRecording;
            long dur = ar.getRecordedDuration();
            long time = getMediaTime();
            long nt = time + SKIP_FORWARD_DURATION;
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : dur = "+dur);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : time = "+time);
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : nt -- 1 = "+nt);
            if (nt >= dur) {
                nt = dur - END_SKIP_MARGIN;
            }
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipForward() : nt -- 2 = "+nt);
            if (nt > time) {
                setMediaTime(nt);
            }
        }
    }

    public void skipBackward() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.skipBackward()");
        if (state == LIVE_TV) {
            move(-SKIP_BACKWARD_DURATION);
        } else if (state == PLAY_RECORDING) {
            long time = getMediaTime();
            long nt = Math.max(time - SKIP_BACKWARD_DURATION, 0);
            setMediaTime(nt);
            if (nt < 1) {
                play();
            }
        }
    }

    /** 뒤로 */
    public void rewind() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.rewind()");
        int rate = (int) player.getRate();
        if (rate <= -MIN_FAST_RATE && rate > -MAX_FAST_RATE && rate % 2 == 0) {
            // 이미 2배속 이상 진행중이면,
            rate = Math.max(rate * 4, -MAX_FAST_RATE);
        } else {
            rate = -MIN_FAST_RATE;
        }
        player.setRate((float) rate);
    }

    /** 앞으로 */
    public void fastForward() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.fastForward()");
        if (state != PLAY_RECORDING && isLiveMode) {
            return;
        }

        int rate = (int) player.getRate();
        if (rate >= MIN_FAST_RATE && rate < MAX_FAST_RATE && rate % 2 == 0) {
            // 이미 2배속 이상 진행중이면,
            rate = Math.min(rate * 4, MAX_FAST_RATE);
        } else {
            rate = MIN_FAST_RATE;
        }
        player.setRate((float) rate);
    }

    /** 느린 재생 */
    public void playSlow() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.playSlow()");
        player.setRate(SLOW_PLAY_RATE);
    }

    /** 재생->느린재생 토글은 hot key로 가능하다. */
    public boolean togglePlaySlow() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.togglePlaySlow()");
        int rate = (int) player.getRate();
        boolean isPlaying = rate == 1;
        if (isPlaying) {
            playSlow();
        } else {
            play();
        }
        return isPlaying;
    }

    public boolean togglePause() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.togglePause()");
        boolean paused = isPaused();
        if (paused) {
            play();
        } else {
            pause();
        }
        return !paused;
    }

    protected boolean isPaused() {
        float rate = player.getRate();
        return (rate > -0.01f && rate < 0.01f );
    }

    ///////////////////////////////////////////////////////////////////////
    // Play Recording
    ///////////////////////////////////////////////////////////////////////

//    protected void startNewBuffering(Service s) {
//        BufferingRequest br = BufferingRequest.createInstance(s, TSB_DURATION, TSB_DURATION, defaultPermission);
//        if (Log.DEBUG_ON) {
//            dumpBuffers();
//            Log.printDebug(logPrefix + ": startNewBuffering : " + br);
//        }
//        try {
//            manager.requestBuffering(br);
//        } catch (Exception ex) {
//            Log.print(ex);
//        }
//        if (Log.DEBUG_ON) {
//            dumpBuffers();
//        }
//    }

    private void dumpBuffers() {
        BufferingRequest[] br = manager.getBufferingRequests();
        for (int i = 0; i < br.length; i++) {
            if (br[i] != null && br[i].getService() != null) {
                Locator loc = br[i].getService().getLocator();
                Log.printDebug("BufferingRequest[" + i + "] = " + loc);
            }
        }
    }

    /** cid = current channel id */
    protected void updateRecordingStatus(int cid) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.updateRecordingStatus("+cid+")");
        this.recording = RecordManager.getInstance().getRequest(cid) != null;
    }

    ///////////////////////////////////////////////////////////////////////
    // Listeners
    ///////////////////////////////////////////////////////////////////////

    /** RecordingChangedListener. from RecordManager */
    public void recordingChanged(RecordingChangedEvent e) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"TunerController.recordingChanged("+e+")");
        if (state != LIVE_TV) {
            return;
        }
        RecordingRequest req = e.getRecordingRequest();
        if (AppData.getInteger(req, AppData.SOURCE_ID) == currentId) {
            updateRecordingStatus(currentId);
        }
    }

    /** TimeShiftListener implemetation */
    public synchronized void receiveTimeShiftevent(TimeShiftEvent e) {
        int reason = e.getReason();
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+logPrefix + ": receiveTimeShiftevent = " + getTimeShiftEventReason(reason));
        }
        if (reason == TimeShiftEvent.TIME_SHIFT_BUFFER_FOUND) {
            bufferFound = true;
        } else if (reason == TimeShiftEvent.NO_TIME_SHIFT_BUFFER) {
            bufferFound = false;
        }
//        isLiveMode = true;
    }

    private static String getTimeShiftEventReason(int reason) {
        switch (reason) {
            case TimeShiftEvent.TIME_SHIFT_BUFFER_FOUND:
                return "TIME_SHIFT_BUFFER_FOUND";
            case TimeShiftEvent.NO_TIME_SHIFT_BUFFER:
                return "NO_TIME_SHIFT_BUFFER";
            case TimeShiftEvent.TIME_SHIFT_PROPERTIES_CHANGED:
                return "TIME_SHIFT_PROPERTIES_CHANGED";
        }
        return "UNKNOWN";
    }

    /** ControllerListener implemetation */
    public synchronized void controllerUpdate(ControllerEvent e) {
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+logPrefix + ": controllerUpdate = " + e);
        }
        if (e instanceof LeavingLiveModeEvent) {
            isLiveMode = false;
        } else if (e instanceof EnteringLiveModeEvent) {
            isLiveMode = true;
        }
        if (e instanceof RateChangeEvent) {
            currentRate = ((RateChangeEvent) e).getRate();
            if (state == PLAY_RECORDING) {
                recordingRate = currentRate;
            }
        }
        if (e instanceof NoComponentSelectedEvent) {
            bufferFound = false;
            isLiveMode = true;
        }
    }

    /**
     * ServiceContextListener implemetation.
     */
    public synchronized void receiveServiceContextEvent(ServiceContextEvent e) {
        //->Kenneth[2015.9.1] null check 꼼꼼히 해주기
        if (Log.DEBUG_ON) {
            Log.printDebug(App.LOG_HEADER+logPrefix + ": receiveServiceContextEvent = " + e);
        }
        if (e instanceof SelectionFailedEvent || e instanceof AlternativeContentErrorEvent) {
            currentService = null;
            setState(STOPPED);
            return;
        }
        if (!(e instanceof PresentationChangedEvent)) {
            currentService = null;
            setState(STOPPED);
            return;
        }
        boolean isNormal = e instanceof NormalContentEvent;
        if (isNormal) {
            resetPlayer();
        }
        Service s = e.getServiceContext().getService();
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": receiveServiceContextEvent.service = " + s);
        }
        if (s != null) {
            if (s instanceof RecordedService || s instanceof RemoteService) {
//                isLiveMode = false;
                AbstractRecording ar = currentRecording;
                if (ar != null && s.equals(ar.getService())) {
                    setState(PLAY_RECORDING);
                } else {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(logPrefix + ": receiveServiceContextEvent.currentRecording = " + ar);
                        if (ar != null) {
                            Service as = ar.getService();
                            Log.printDebug(logPrefix + ": receiveServiceContextEvent.currentRecording.service = " + as + " : " + s.equals(as));
                        }
                    }
                    setState(STOPPED);
                }
                return;
            } else {
                currentRecording = null;
//                if (REQUEST_BUFFERING_WHEN_SELECT && isNormal) {
//                    startNewBuffering(s);
//                }
            }
        } else {
            currentRecording = null;
        }
        currentService = s;
        short targetState = UNKNOWN;
        int cid = -1;
        TvChannel ch = null;
        try {
            ChannelContext cContext = Core.epgService.getChannelContext(tunerIndex);
            if (cContext != null) ch = cContext.getCurrentChannel();
            if (ch != null && s != null) {
                // TODO - 아래 locator 비교 안쪽으로 옮기는게 좋지 않을까?
                Locator loc = s.getLocator();
                if (loc != null) {
                    String chLocatorString = ch.getLocatorString();
                    if (loc.toExternalForm().equals(chLocatorString)) {
                        targetState = LIVE_TV;
                        cid = ch.getId();
                        this.channel = ch;
                        this.program = Core.epgService.getCurrentProgram(cid);
                    } else {
                        Log.printDebug(logPrefix + ": different locator. "
                                        + loc.toExternalForm() + ", " + chLocatorString);
                    }
                } else {
                    Log.printDebug(logPrefix + ": service's locator is null.");
                }
            } else {
                if (Log.DEBUG_ON) Log.printDebug(logPrefix + ": Channel or Service is null");
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        this.currentId = cid;
        if (cid == -1) {
            this.channel = null;
            this.program = null;
        }
        setState(targetState);
        if (Log.DEBUG_ON) {
            Log.printDebug(logPrefix + ": current channel = " + channel);
            Log.printDebug(logPrefix + ": current program = " + program);
        }
    }

    protected static String getStateString(short s) {
        switch (s) {
            case STOPPED:
                return "STOPPED";
            case PLAY_RECORDING:
                return "PLAY_RECORDING";
            case LIVE_TV:
                return "LIVE_TV";
            case UNKNOWN:
                return "UNKNOWN";
            default:
                return "NONE";
        }
    }
}

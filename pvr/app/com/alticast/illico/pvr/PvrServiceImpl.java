package com.alticast.illico.pvr;

import java.rmi.RemoteException;
import java.util.Date;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import org.ocap.shared.dvr.RecordingRequest;
import org.ocap.shared.dvr.LeafRecordingRequest;
import org.ocap.shared.media.TimeShiftControl;
import org.ocap.shared.dvr.navigation.RecordingList;
import com.alticast.illico.pvr.hn.HomeNetManager;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.popup.PastRecordingPopup;
import com.alticast.illico.pvr.data.AppData;
import com.alticast.illico.pvr.list.*;
import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.popup.Popup;

import com.videotron.tvi.illico.ixc.pvr.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.ixc.monitor.*;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.companion.LogStateListener;
//->Kenneth[2015.7.4] Tank
import com.alticast.illico.pvr.ui.*;
//<-

public class PvrServiceImpl implements PvrService {

    private static final boolean CHECK_REPEAT = false;
    private Popup savePopup = null;

    XletContext xletContext;
    private static PvrServiceImpl instance = new PvrServiceImpl();

    public static PvrServiceImpl getInstance() {
        return instance;
    }

    public void start(XletContext context) {
        this.xletContext = context;
        try {
            Log.printInfo("PvrServiceImpl: bind to IxcRegistry");
            IxcRegistry.bind(xletContext, IXC_NAME, this);
        } catch (Exception ex) {
            Log.print(ex);
        }
//        if (Environment.EMULATOR) {
//            try {
//                Thread.sleep(10000L);
//            } catch (Exception ex) {
//            }
//            requestRepeatRecording(6, "haha", System.currentTimeMillis() + Constants.MS_PER_HOUR,
//                System.currentTimeMillis() + 2 * Constants.MS_PER_HOUR,
//                System.currentTimeMillis() + Constants.MS_PER_HOUR / 2,
//                "0011000", 5);
//        }
    }

    public void dispose() {
        try {
            IxcRegistry.unbind(xletContext, IXC_NAME);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    ///////////////////////////////////////////////////////////////////////
    // PvrService methods
    ///////////////////////////////////////////////////////////////////////

    public String getDeviceName() throws RemoteException {
        return HomeNetManager.getInstance().getDeviceName();
    }

    public void setDeviceName(String name) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.setDeviceName("+name+")");
        HomeNetManager.getInstance().setDeviceName(name);
    }

    public String[] getRecordedContents() throws RemoteException {
        return RecordingListManager.getInstance().getRecordedContents();
    }

    public String[] getContents() throws RemoteException {
        return RecordingListManager.getInstance().getAllContents();
    }

    public PvrContent getContent(String id) throws RemoteException {
        return RecordingListManager.getInstance().getContent(id);
    }

    public void addContentsChangedListener(ContentsChangedListener l) throws RemoteException {
        RecordingListManager.getInstance().addContentsChangedListener(l);
    }

    public void removeContentsChangedListener(ContentsChangedListener l) throws RemoteException {
        RecordingListManager.getInstance().removeContentsChangedListener(l);
    }

    public void showMenu(final int menuId) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showMenu("+menuId+")");
        Thread t = new Thread("PvrService.showMenu") {
            public void run() {
                //->Kenneth[2016.1.18] VDTRMASTER-5753 포털로부터 오는 경우 PVR 컨텐츠 stop 안시키고 있었음.
                // 레코딩리스트, upcoming 리스트, manual recording 화면으로 이동하는 경우만 stop 시키도록 한다.
                // stop 시키는 경우 현재 media time 도 저장해 줘야 나중에 resume viewing 할 수 있다.
                if (menuId == MENU_RECORDING_LIST || menuId == MENU_CREATE_RECORDING || menuId == MENU_SCHEDULED_RECORDINGS) {
                    if (VideoPlayer.getInstance().getState() == VideoPlayer.PLAY_RECORDING) {
                        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showMenu.t.run() : PLAY_RECORDING state, so Call VideoPlayer.updateMediaTime()");
                        VideoPlayer.getInstance().updateMediaTime();
                    }
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showMenu.t.run() : Call VideoPlayer.stopPlaying(true)");
                    VideoPlayer.getInstance().stopPlaying(true);
                }
                //<-
                if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showMenu.t.run() : Call showMenu()");
                MenuController.getInstance().showMenu(menuId);
            }
        };
        t.start();
    }

    /**
     * Records the requested Program.
     */
    public void record(TvChannel ch, TvProgram p) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.record()");
        int popupType = recordImpl(ch, p);
        Log.printDebug("PvrService: record popup = " + popupType);
        PopupController.getInstance().showRecordingPopup(ch, p, popupType);
    }

    /**
     * Records the requested Program.
     */
    public void record(TvChannel ch, long from, long to, String title) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.record("+title+")");
        RecordingOption option = new RecordingOption(ch, from, to);
        option.creationMethod = AppData.BY_USER;
        if (title == null) {
            title = DataCenter.getInstance().getString("manual.ManualRecording");
        }
        option.title = title;
        RecordManager.getInstance().record(option);
    }

    private int recordImpl(TvChannel ch, TvProgram p) throws RemoteException {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.recordImpl("+p+")");
        RecordingRequest req = RecordManager.getInstance().getRequest(p);
        if (req != null) {
            Log.printInfo("PvrService.record: already recording RecordingRequest. = " + p);
            return PopupController.POPUP_DUPLICATED_RECORDING;
        }

        long cur = System.currentTimeMillis();
        long startTime = p.getStartTime();
        long endTime = p.getEndTime();
        String seriesId = p.getSeriesId();
        int type = p.getType();

        if (endTime <= cur) {
            // past
            TunerController tc = Core.getInstance().getTunerController(ch);
            if (tc == null) {
                Log.printInfo("PvrService.record: not in current TSB = " + p);
                return PopupController.POPUP_PAST_NOT_AVAILABLE;
            }
            TimeShiftControl tsc = tc.getControl();
            if (tsc == null) {
                Log.printWarning("PvrService.record: not found TimeShiftControl = " + p);
                return PopupController.POPUP_PAST_NOT_AVAILABLE;
            }
            long bb = PvrUtil.getTime(tsc.getBeginningOfBuffer());
            if (endTime <= bb) {
                Log.printWarning("PvrService.record: past program. BeginningOfBuffer = " + new Date(bb)
                                           + ", program = " + p);
                return PopupController.POPUP_PAST_NOT_AVAILABLE;
            }
            PastRecordingPopup.tunerController = tc;
            PastRecordingPopup.timeShiftControl = tsc;
            PastRecordingPopup.startTime = startTime;
            PastRecordingPopup.endTime = endTime;
            PastRecordingPopup.current = Core.epgService.getProgram(ch, System.currentTimeMillis());
            PastRecordingPopup.program = p;
            PastRecordingPopup.channel = ch;
            return PopupController.POPUP_RECORD_PAST_PROGRAM;
        } else if (AppData.isSingleProgram(type)) {
            // single (movie) - NOTE : all movies has series id so check first.
            return PopupController.POPUP_RECORD_SINGLE;
        } else if (seriesId != null && seriesId.length() > 0) {
            // series
            return PopupController.POPUP_RECORD_SERIES;
        } else {
            if (!CHECK_REPEAT) {
                return PopupController.POPUP_RECORD_REPEAT;
            }
            boolean hasMoreProgram = false;
            Object ob1 = DataCenter.getInstance().get(EpgService.IXC_NAME);
            if (ob1 != null) {
                EpgService epgService = (EpgService) ob1;
                try {
                    RemoteIterator ri = epgService.findPrograms(p.getCallLetter(), p.getId(), null, p.getEndTime(), Long.MAX_VALUE);
                    if (ri != null && ri.hasNext()) {
                        hasMoreProgram = true;
                    }
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
            if (hasMoreProgram) {
                return PopupController.POPUP_RECORD_REPEAT;
            } else {
                return PopupController.POPUP_RECORD_SINGLE;
            }
        }
    }

    public void cancel(TvProgram p) throws RemoteException {
        RecordingRequest req = RecordManager.getInstance().getRequest(p);
        if (req == null) {
            Log.printInfo("PvrService.cancel: not found RecordingRequest. = " + p);
            // TODO - 팝업 필요 ? 그냥 닫기?
            return;
        }
        cancel(p, req);
    }

    public void cancel(RecordingRequest req) {
        TvProgram p = RecordManager.getInstance().getProgram(req);
        cancel(p, req);
    }

    private void cancel(TvProgram p, RecordingRequest req) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.cancel("+p+", "+req+")");
        PopupController pc = PopupController.getInstance();
        if (RecordingListManager.isPending(req)) {
            // 예약 녹화 - 확인 팝업 필요 ok, cancel
            pc.showCancelPopup(p, req, PopupController.POPUP_FUTURE_RECORDING_CANCEL);
        } else {
            // 녹화 진행중 - 3 버튼 팝업 필요.
            pc.showCancelPopup(p, req, PopupController.POPUP_RECORDING_CANCEL_WATCH_TV);
        }
    }

    public void cancelSaveBuffer(int tunerIndex) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.cancelSaveBuffer("+tunerIndex+")");
        Popup popup = this.savePopup;
        if (popup != null) {
            PopupController.getInstance().hidePopup(popup);
        }
        this.savePopup = null;
    }

    public boolean confirmSaveBuffer(int tunerIndex, TvChannel current, TvChannel next) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.confirmSaveBuffer("+next+")");
        ChannelChangeRequestor requestor = new ChannelChangeRequestor(tunerIndex, current, next);
        Popup popup = PopupController.getInstance().showSaveBuffer(requestor);
        if (popup == null) {
            return false;
        }
        this.savePopup = popup;
        try {
            synchronized (requestor) {
                requestor.wait();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        this.savePopup = null;
        Log.printInfo("PvrService.confirmSaveBuffer: returns " + requestor.result);
        return requestor.result;
    }

    public boolean confirmChangeChannel(int videoIndex, TvChannel current, TvChannel next) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.confirmChangeChannel("+next+")");

        if (App.MANY_TUNER) {
            Log.printDebug("PvrService.confirmChangeChannel : with gateway");
            TunerRequestor requestor = new TunerRequestor(TunerRequestor.MANY_TUNER_BY_CHANNEL, videoIndex);
            IssueManager.getInstance().showTunerRequest(requestor);
            try {
                synchronized (requestor) {
                    requestor.wait();
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
            Log.printDebug("PvrService.confirmChangeChannel : returns " + requestor.result);
            return requestor.result;

        } else {
            LocalRecording[] twoRecording = checkTwoRecording();
            if (twoRecording != null && Core.getInstance().getMonitorState() != MonitorService.PIP_STATE) {
                Log.printDebug("PvrService.confirmChangeChannel : with two recordings");
                TunerRequestor requestor = new TunerRequestor(TunerRequestor.DUAL_TUNER_NO_CONFIRM, videoIndex);
                PopupController.getInstance().showTunerRequest(twoRecording, requestor);
                try {
                    synchronized (requestor) {
                        requestor.wait();
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
                Log.printDebug("PvrService.confirmChangeChannel : returns " + requestor.result);
                return requestor.result;

            } else {
                Log.printDebug("PvrService.confirmChangeChannel : with current recording");
                ChannelChangeRequestor requestor = new ChannelChangeRequestor(videoIndex, current, next);
                PopupController.getInstance().showChannelChangeNotice(requestor);
                try {
                    synchronized (requestor) {
                        requestor.wait();
                    }
                } catch (Exception ex) {
                    Log.print(ex);
                }
                Log.printDebug("PvrService.confirmChangeChannel : returns " + requestor.result);
                return requestor.result;
            }
        }
    }

    private LocalRecording[] checkTwoRecording() {
        RecordingList list = RecordingListManager.getInstance().getInProgressWithoutErrorList();
        if (list == null) {
            return null;
        }
        int size = list.size();
        Log.printDebug("PvrService.requestTuner : recordings = " + size);
        if (size != 2) {
            return null;
        }
        LocalRecording[] array = new LocalRecording[size];
        for (int i = 0; i < size; i++) {
            LeafRecordingRequest req = (LeafRecordingRequest) list.getRecordingRequest(i);
            array[i] = new LocalRecording(req);
        }
        return array;
    }

    public void showChannelAutoTuneNotice(int tunerIndex, int recordingId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showChannelAutoTuneNotice("+tunerIndex+", "+recordingId+")");
        RecordingRequest req = RecordManager.getInstance().getRecordingRequest(recordingId);
        if (req != null) {
            PopupController.getInstance().showRecordingNotice(req, tunerIndex);
        }
    }

    public boolean checkTuner(int sourceId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.checkTuner("+sourceId+")");
        RecordingList list = RecordingListManager.getInstance().getInProgressWithoutErrorList();
        if (list == null) {
            return true;
        }
        int size = list.size();
        Log.printDebug("PvrService.checkTuner : recordings = " + size);
        if (size < Environment.TUNER_COUNT) {
            return true;
        }
        return false;
    }

    public boolean requestTuner() {
        return requestTuner(0);
    }

    public boolean requestTuner(int sourceId) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.requestTuner("+sourceId+")");
        RecordingList list = RecordingListManager.getInstance().getInProgressWithoutErrorList();
        if (list == null) {
            return true;
        }
        int size = list.size();
        Log.printDebug("PvrService.requestTuner : recordings = " + size);
        if (size < Environment.TUNER_COUNT) {
            return true;
        }
        TunerRequestor requestor;
        if (App.MANY_TUNER) {
            requestor = new TunerRequestor(TunerRequestor.MANY_TUNER_BY_VOD, 0);
            IssueManager.getInstance().showTunerRequest(requestor);
        } else {
            LocalRecording[] array = new LocalRecording[size];
            for (int i = 0; i < size; i++) {
                LeafRecordingRequest req = (LeafRecordingRequest) list.getRecordingRequest(i);
                if (sourceId != 0 && sourceId == AppData.getInteger(req, AppData.SOURCE_ID)) {
                    return true;
                }
                array[i] = new LocalRecording(req);
            }
            requestor = new TunerRequestor(TunerRequestor.DUAL_TUNER_WITH_CONFIRM, 0);
            PopupController.getInstance().showTunerRequest(array, requestor);
        }
        try {
            synchronized (requestor) {
                requestor.wait();
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        Log.printDebug("PvrService.requestTuner : returns " + requestor.result);
        return requestor.result;
    }

    /** Called by Migration module of Wizard app. */
    public String requestRepeatRecording(final int channel, final String title,
                    final long from, final long to, final long programStartTime,
                    final String repeatOption, final int keepOption) {
        Log.printDebug(App.LOG_HEADER+"PvrService.requestRepeatRecording: " + channel + ", " + title
                + ", " + new Date(from) + ", " + new Date(to) + ", " + new Date(programStartTime)
                + ", " + repeatOption +  ", " + keepOption);

        EpgService es;
        while ((es = Core.epgService) == null) {
            Log.printDebug("PvrService.requestRepeatRecording: waiting for EpgService");
            try {
                Thread.sleep(2000L);
            } catch (Exception ex) {
            }
        }
        try {
            TvChannel ch = null;
            for (int i = 0; i < 20 && ch == null; i++) {
                ch = es.getChannelByNumber(channel);
                Log.printDebug("PvrService.requestRepeatRecording: channel = " + ch);
                if (ch == null) {
                    try {
                        Thread.sleep(2000L);
                    } catch (Exception ex) {
                    }
                }
            }
            if (ch == null) {
                Log.printWarning("PvrService.requestRepeatRecording: channel not found. : " + channel);
                return null;
            }
            int chId = ch.getId();
            TvProgram p = null;
            if (programStartTime > 0) {
                for (int i = 0; i < 20 && p == null; i++) {
                    p = es.getProgram(chId, programStartTime);
                    Log.printDebug("PvrService.requestRepeatRecording: program = " + p);
                    if (p == null || p.getType() == TvProgram.TYPE_UNKNOWN) {
                        p = null;
                        try {
                            Thread.sleep(2000L);
                        } catch (Exception ex) {
                        }
                    }
                }
                if (p == null || p.getType() == TvProgram.TYPE_UNKNOWN) {
                    p = null;
                    Log.printWarning("PvrService.requestRepeatRecording: program not found");
                }
            }
            RecordingScheduler sch = RecordingScheduler.getInstance();
            if (p != null) {
                long buffer = RecordManager.getInstance().getRecordingBuffer();
                return sch.setForcedRepeat(p, -buffer, buffer, repeatOption, keepOption, false, true);
            } else {
                return sch.setManualRepeat(ch, title, from, to, repeatOption, keepOption, false, true);
            }
        } catch (Exception ex) {
            Log.print(ex);
            return null;
        }
    }

    //->Kenneth[2015.7.4] Tank
    public void portalVisibilityChanged(boolean visible) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.portalVisibilityChanged("+visible+")");
        FlipBarWindow.isPortalVisible = visible;
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.portalVisibilityChanged() : Call FlipBarWindow.enableFlipBar("+!visible+")");
        FlipBarWindow.getInstance().enableFlipBar(!visible);
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.portalVisibilityChanged() : Call VideoFeedback.enableFeedback("+!visible+")");
        VideoFeedback.getInstance().enableFeedback(!visible);
    }
    //<-

    ////////////////////////////////////////////////////////////////
    // Companion APIs
    ////////////////////////////////////////////////////////////////

    public boolean pvrPlayControl(int keyCode) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.pvrPlayControl("+keyCode+")");
        return VideoPlayer.getInstance().processUserEvent(keyCode);
    }

    public boolean showHideFlipBar() {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.showHideFlipBar()");
        return VideoPlayer.getInstance().showHideFlipBar();
    }

    //->Kenneth[2015.6.25] DDC-107
    public int playContent(int id, int startPoint, String udn) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.playContent("+id+", "+startPoint+", "+udn+")");
        MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (ms == null) {
            if (udn == null) return -111;
            return -112;
        }
        AbstractRecording rec = AbstractRecording.find(id, udn);
        Log.printDebug("PvrService.playContent: rec = " + rec);
        if (rec == null) {
            if (udn == null) return -111;
            return -112;
        }
        int definition = rec.getDefinition();
        Log.printDebug("PvrService.playContent: definition = " + definition);
        Log.printDebug("PvrService.playContent: SUPPORT_UHD = " + Environment.SUPPORT_UHD);
        if (definition == 2 && !Environment.SUPPORT_UHD) {
            Log.printDebug("PvrService.playContent: returns -133");
            return -133;
        }
        boolean canPlayUhd = VideoOutputUtil.getInstance().canPlayUHDContent();
        Log.printDebug("PvrService.playContent: canPlayUhd = " + canPlayUhd);
        if (definition == 2 && Environment.SUPPORT_UHD && !canPlayUhd) {
            Log.printDebug("PvrService.playContent: returns -134");
            return -134;
        }
        //->Kenneth[2015.7.29] HDCP2.2 체크도 추가
        if (definition == 2 && Environment.SUPPORT_UHD && VideoOutputUtil.getInstance().existHdcpApi()) {
            boolean hdcp22Supported = VideoOutputUtil.getInstance().isSupportedHdcp();
            if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.playContent() : hdcp22Supported = "+hdcp22Supported);
            if (!hdcp22Supported) {
                Log.printDebug("PvrService.playContent: returns -134 -- HDCP2.2");
                return -134;
            }
        }
        //<-
        short stateGroup = rec.getStateGroup();
        if (stateGroup != RecordingListManager.COMPLETED && stateGroup != RecordingListManager.IN_PROGRESS) {
            if (udn == null) return -111;
            return -112;
        }
        try {
            String videoContext = ms.getVideoContextName();
            if ("PVR".equals(videoContext)) {
                Core.getInstance().stopOverlappingApplication();
                VideoPlayer.getInstance().updateMediaTime();
                VideoPlayer.getInstance().exitToList();
                MenuController.getInstance().playRecording(rec, startPoint);
            } else {
                if (App.NAME.equals(ms.getCurrentActivatedApplicationName())) {
                    MenuController.getInstance().playRecording(rec, startPoint);
                } else {
                    ms.startUnboundApplication(App.NAME, new String[] { "PLAY", String.valueOf(id), String.valueOf(startPoint), udn });
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
            if (udn == null) return -111;
            return -112;
        }
        if (ParentalControl.isAccessBlocked(rec)) {
            throw new IllegalStateException("PIN required");
        } else {
            return 0;
        }
    }
    /*
    public boolean playContent(int id, int startPoint, String udn) {
        if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PvrServiceImpl.playContent("+id+", "+startPoint+", "+udn+")");
        MonitorService ms = (MonitorService) DataCenter.getInstance().get(MonitorService.IXC_NAME);
        if (ms == null) {
            return false;
        }
        AbstractRecording rec = AbstractRecording.find(id, udn);
        Log.printDebug("PvrService.playContent: rec = " + rec);
        if (rec == null) {
            return false;
        }
        short stateGroup = rec.getStateGroup();
        if (stateGroup != RecordingListManager.COMPLETED && stateGroup != RecordingListManager.IN_PROGRESS) {
            return false;
        }
        try {
            String videoContext = ms.getVideoContextName();
            if ("PVR".equals(videoContext)) {
                Core.getInstance().stopOverlappingApplication();
                VideoPlayer.getInstance().updateMediaTime();
                VideoPlayer.getInstance().exitToList();
                MenuController.getInstance().playRecording(rec, startPoint);
            } else {
                if (App.NAME.equals(ms.getCurrentActivatedApplicationName())) {
                    MenuController.getInstance().playRecording(rec, startPoint);
                } else {
                    ms.startUnboundApplication(App.NAME, new String[] { "PLAY", String.valueOf(id), String.valueOf(startPoint), udn });
                }
            }
        } catch (Exception ex) {
            Log.print(ex);
            return false;
        }
        if (ParentalControl.isAccessBlocked(rec)) {
            throw new IllegalStateException("PIN required");
        } else {
            return true;
        }
    }
    */
    //<-

    public void setLogStateListener(LogStateListener l) {
        VideoPlayer.getInstance().setLogStateListener(l);
    }

    //->Kenneth[2017.8.31] R7.4 : PVR715 팝업의 경우 Error Message 로부터 호출될 수 있다
    public void removeAllUpcomingRecordings(String callLetter) throws RemoteException {
        RecordManager.getInstance().removeAllUpcomingRecordings(callLetter);
    }
    //<-
}

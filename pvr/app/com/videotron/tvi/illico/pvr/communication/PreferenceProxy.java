package com.videotron.tvi.illico.pvr.communication;

import java.rmi.RemoteException;
import java.util.Hashtable;
import com.alticast.illico.pvr.issue.IssueManager;
import com.alticast.illico.pvr.util.PvrUtil;
import com.alticast.illico.pvr.gui.GraphicsResource;
import com.alticast.illico.pvr.ui.*;
import com.alticast.illico.pvr.*;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.TvProgram;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;

public class PreferenceProxy implements PreferenceListener {

    private static PreferenceProxy instance = new PreferenceProxy();
    private PreferenceService preferenceService;
    public Hashtable ratingHash = new Hashtable();

    public static boolean hideAdult = false;

    /** Key set. */
    public static String[] preferenceKeys;

    private PreferenceProxy() {
    }

    public static PreferenceProxy getInstance() {
        return instance;
    }

    public void showPinEnabler(PinEnablerListener listener) {
        showPinEnabler(DataCenter.getInstance().getString("pvr.Enter Your PIN"), listener);
    }

    public void showPinEnabler(String str, PinEnablerListener listener) {
        if (str == null) {
            str = DataCenter.getInstance().getString("pvr.Enter Your PIN");
        }
        showPinEnabler(TextUtil.split(str, GraphicsResource.FM17, 300, "|"), listener);
    }

    private void showPinEnabler(String[] msg, PinEnablerListener listener) {
        Log.printDebug("PreferenceProxy.showPinEnabler");
        preferenceService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        try {
            preferenceService.showPinEnabler(listener, msg);
        } catch (Exception e) {
            Log.print(e);
            if (listener != null) {
                try {
                    listener.receivePinEnablerResult(PreferenceService.RESPONSE_CANCEL, "");
                } catch (Exception ex) {
                }
            }
        }
    }

    public void checkRightFilter(RightFilterListener listener) {
        try {
            preferenceService.checkRightFilter(listener, FrameworkMain.getInstance().getApplicationName(),
                    new String[] { RightFilter.PARENTAL_CONTROL }, null, null);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public void showRightFilterPinEnabler(RightFilterListener listener) {
        try {
            preferenceService.checkRightFilter(listener, FrameworkMain.getInstance().getApplicationName(),
                    new String[] {RightFilter.ALLOW_ADULT_CATEGORY}, null, Definitions.RATING_G);
        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public void hidePinEnabler() {
        PreferenceService preferenceService = (PreferenceService) DataCenter.getInstance().get(
                PreferenceService.IXC_NAME);
        if (preferenceService != null) {
            try {
                preferenceService.hidePinEnabler();
            } catch (RemoteException e) {
                Log.print(e);
            }
        }
    }

    /**
     * register listener to UPP.
     */
    public void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy.addPreferenceListener");
        }
        if (preferenceService == null) {
            preferenceService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        }
        if (preferenceService == null)
            return;
        try {
            String[] keys = {
                    PreferenceNames.LANGUAGE,
                    RightFilter.PARENTAL_CONTROL,
                    RightFilter.BLOCK_BY_RATINGS,
                    RightFilter.PROTECT_PVR_RECORDINGS,
                    RightFilter.SUSPEND_DEFAULT_TIME,
                    RightFilter.HIDE_ADULT_CONTENT,
                    PreferenceNames.RECORD_BUFFER,
                    PreferenceNames.DEFAULT_SAVE_TIME,
                    PreferenceNames.DEFAULT_SAVE_TIME_REPEAT,
                    PreferenceNames.TERMINAL_NAME,
//                    PreferenceNames.MULTIROOM_SUPPORT,
                    PreferenceNames.TERMINAL_OTHER_NAME,
                    RightFilter.CHANNEL_RESTRICTION,
                    RightFilter.PIN_CODE_UNBLOCKS_CHANNEL,
                    RecordedList.getInstance().getSortingPreferenceName(),
                    UpcomingList.getInstance().getSortingPreferenceName(),
                    //->Kenneth[2015.2.16] 4K 우정 추가
                    MonitorService.SUPPORT_UHD,
                    //->Kenneth[2016.3.8] R7 
                    PreferenceNames.SD_HD_CHANNELS_GROUPING,

                    //->Kenneth [2016.5.27] Log capture tool 때문에 추가
                    // Log capture 하려는 모든 App 은 아래의 preference 를 등록하여야 한다.
                    // 그에 관해 event 받는 것은 framework 의 Log.java 에 구현되어 있다.
                    Log.LOG_CAPTURE_APP_NAME,
                    //<-

            };
            String[] values = new String[keys.length];

            String[] result = preferenceService.addPreferenceListener(this, FrameworkMain.getInstance()
                    .getApplicationName(), keys, values);

            if (result != null) {
                for (int a = 0; a < result.length; a++) {
                    Log.printDebug("PreferenceProxy: key[" + a + "] = " + keys[a] + " values = " + result[a]);
                    DataCenter.getInstance().put(keys[a], result[a]);

                    if (keys[a].equals(RightFilter.HIDE_ADULT_CONTENT)) {
                        if (result[a].equals(Definitions.OPTION_VALUE_YES)) {
                            hideAdult = true;
                        } else {
                            hideAdult = false;
                        }
                    }
                    //->Kenneth[2016.3.8] R7
                    if (keys[a].equals(PreferenceNames.SD_HD_CHANNELS_GROUPING)) {
                        if (result[a].equals(Definitions.SD_HD_CHANNEL_GROUPED)) {
                            if (Log.DEBUG_ON) Log.printDebug("PVR : PreferenceProxy: set isChannelGrouped true");
                            Core.isChannelGrouped = true;
                        } else {
                            if (Log.DEBUG_ON) Log.printDebug("PVR : PreferenceProxy: set isChannelGrouped false");
                            Core.isChannelGrouped = false;
                        }
                    }
                    //<-
                }
            }

        } catch (RemoteException e) {
            Log.print(e);
        }
    }

    public void removePreferenceListener() {

        if (preferenceService == null) {
            preferenceService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        }
        try {
            preferenceService.removePreferenceListener(FrameworkMain.getInstance().getApplicationName(), this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public boolean setPreferenceData(String key, String value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, getPreferenceData" + key + "  " + value);
        }
        preferenceService = (PreferenceService) DataCenter.getInstance().get(PreferenceService.IXC_NAME);
        boolean flag = false;
        try {
            flag = preferenceService.setPreferenceValue(key, value);
        } catch (RemoteException e) {
            Log.print(e);
        }
        return flag;
    }

    /**
     * values are received update Listener from UPP.
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        Log.printDebug("receiveUpdatedPreference name = " + name + " value = " + value);
        if (name != null && value != null) {
            DataCenter.getInstance().put(name, value);

            if (name.equals(RightFilter.PARENTAL_CONTROL)) {
                if (value.equals(Definitions.OPTION_VALUE_ON)) {
                    String check = (String) DataCenter.getInstance().get(RightFilter.HIDE_ADULT_CONTENT);
                    if (check.equals(Definitions.OPTION_VALUE_YES)) {
                        hideAdult = true;
                    } else {
                        hideAdult = false;
                    }
                } else {
                    hideAdult = false;
                }
            } else if (name.equals(RightFilter.HIDE_ADULT_CONTENT)) {
                if (value.equals(Definitions.OPTION_VALUE_YES)) {
                    hideAdult = true;
                } else {
                    hideAdult = false;
                }
            //->Kenneth [2015.2.21] 4K
            } else if ("SUPPORT_UHD".equals(name)) {
                if ("true".equals(value)) {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PreferenceProxy.receiveUpdatedPreference() : set SUPPORT_UHD = true");
                    Environment.SUPPORT_UHD = true;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug(App.LOG_HEADER+"PreferenceProxy.receiveUpdatedPreference() : set SUPPORT_UHD = false");
                    Environment.SUPPORT_UHD = false;
                }
                //<-

            //->Kenneth[2016.3.8] R7
            } else if (name.equals(PreferenceNames.SD_HD_CHANNELS_GROUPING)) {
                if (value.equals(Definitions.SD_HD_CHANNEL_GROUPED)) {
                    if (Log.DEBUG_ON) Log.printDebug("PVR : PreferenceProxy: set isChannelGrouped true");
                    Core.isChannelGrouped = true;
                } else {
                    if (Log.DEBUG_ON) Log.printDebug("PVR : PreferenceProxy: set isChannelGrouped false");
                    Core.isChannelGrouped = false;
                }
                //<-

            }
        }
    }

    public void setBlocked(String key, String b) {
        ratingHash.put(key, b);
    }

    public boolean getBlocked(String key) {
        if (!DataCenter.getInstance().getString(RightFilter.PARENTAL_CONTROL).equals(Definitions.OPTION_VALUE_ON)) {
            return false;
        }

        String bRating = (String) ratingHash.get(key);
        if (bRating == null) {
            return false;
        }
        return bRating.equals("true");
    }
}

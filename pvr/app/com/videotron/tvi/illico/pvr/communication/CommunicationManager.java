package com.videotron.tvi.illico.pvr.communication;

import java.rmi.Remote;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.daemon.DaemonService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.IxcLookupWorker;
import com.alticast.illico.pvr.App;
//->Kenneth[2015.7.6] Tank 
import com.videotron.tvi.illico.ixc.mainmenu.*;
//<-

public class CommunicationManager {

    private XletContext xletContext;
    private static CommunicationManager instance = new CommunicationManager();

    public static CommunicationManager getInstance() {
        return instance;
    }

    IxcLookupWorker lookupWorker;

    public void start(XletContext xletContext, DataUpdateListener l) {
        this.xletContext = xletContext;
        lookupWorker = new IxcLookupWorker(xletContext);
        lookupWorker.lookup("/1/1/", MonitorService.IXC_NAME, l);
        lookupWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, l);
        lookupWorker.lookup("/1/2026/", EpgService.IXC_NAME, l);
        lookupWorker.lookup("/1/2050/", KeyboardService.IXC_NAME, l);
        lookupWorker.lookup("/1/2080/", SearchService.IXC_NAME, l);
        lookupWorker.lookup("/1/2110/", NotificationService.IXC_NAME, l);
        lookupWorker.lookup("/1/2075/", LoadingAnimationService.IXC_NAME, l);
        lookupWorker.lookup("/1/2100/", StcService.IXC_NAME, l);
        lookupWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, l);
        lookupWorker.lookup("/1/2040/", DaemonService.IXC_NAME, l);
        if (App.R3_TARGET) {
            lookupWorker.lookup("/1/2085/", VbmService.IXC_NAME, l);
        }
        //->Kenneth[2015.7.6] Tank 
        lookupWorker.lookup("/1/2090/", MainMenuService.IXC_NAME, l);
        //<-
    }

    public void dispose() {
    }

}

package com.videotron.tvi.illico.pvr.communication;

import javax.tv.xlet.XletContext;

import com.alticast.illico.pvr.impl.KeyboardOptionImpl;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardListener;
import com.videotron.tvi.illico.ixc.keyboard.KeyboardService;

public class KeyboardListenerAction  {

    private static KeyboardListenerAction instance;
    XletContext xletContext;
    KeyboardListener listener;

    /** The keyboard service. */
    private KeyboardService keyboardSvc;

    private KeyboardListenerAction() {
    }

    public static synchronized KeyboardListenerAction getInstance() {
        if (instance == null) {
            instance = new KeyboardListenerAction();
        }
        return instance;
    }

    public void start(XletContext xlet) {
        xletContext = xlet;
    }

    /**
     * Open virtual keyboard.
     */
    public synchronized void openVirtualKeyboard(int kbType, int x, int y, String defaultString, boolean tempStr, KeyboardListener listener) {
        this.listener = listener;
        KeyboardOptionImpl keyboardOptionImpl = new KeyboardOptionImpl();

        keyboardOptionImpl.setKeyboardPosition(x, y);
        keyboardOptionImpl.setType(kbType);
        if(defaultString != null) {
            keyboardOptionImpl.setTempDefaultText(tempStr);
            keyboardOptionImpl.setDefaultText(defaultString);
        } else {
            keyboardOptionImpl.setDefaultText("");
        }

        KeyboardService keyboardService = getKeyboardService();
        if (keyboardService != null) {
            try {
                keyboardService.showPopupKeyboard(keyboardOptionImpl, listener);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Close virtual keyboard.
     */
    public void closeVirtualKeyboard(KeyboardListener listener) {
        KeyboardService keyboardService = getKeyboardService();
        if (keyboardService != null) {
            try {
                keyboardService.hidePopupKeyboard(listener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            keyboardService = null;
        }
    }

    public void currentSceneCloseVirtualKeyboard() {
        KeyboardService keyboardService = getKeyboardService();
        if (keyboardService != null) {
            try {
                keyboardService.hidePopupKeyboard(listener);
            } catch (Exception e) {
                e.printStackTrace();
            }
            keyboardService = null;
        }
    }

    public KeyboardService getKeyboardService() {
        keyboardSvc = (KeyboardService) DataCenter.getInstance().get(KeyboardService.IXC_NAME);
        return keyboardSvc;
    }
}
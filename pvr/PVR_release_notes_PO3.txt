﻿This is PVR App Release Notes.

GENERAL RELEASE INFORMATION
- Release Details
    - Release name
      : version 1.2.10.1_PO3
    - Release App file Name
      : PVR_PO3.zip
    - Release type (official, debug, test,...)
      : official
    - Release date
      : 2013.12.13

***
version 1.2.10.1_PO3 - 2013.12.13
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  

***
version 1.2.9.2_PO3 - 2013.12.09
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4970  [CQ][R4][PVR][GUI]:Rec LIST: Long titles should not scroll over icons. 
        + VDTRMASTER-4971  [CQ][R4][PVR]: Upcoming scheduled recordings should not appear in the list of recordings

***
version 1.2.8.4_PO3 - 2013.12.05
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  
        + VDTRMASTER-4963  [CQ][PO3][Compagnon] Error code is not correct and STB goes to black screen when play a local pvr content of upcoming list.  
        + VDTRMASTER-4727  [CQ][R3][MR]:Deleting ongoing recordings on Client while the conflict notfication is on the Server  
        + VDTRMASTER-4964  PVR does not send VBM trigger logs.  

***
version 1.2.7.5_PO3 - 2013.11.27
    - New Feature
        N/A
    - Resolve
        + VDTRMASTER-4786  PVR - Add the PVR801 code to the message "viewing failed" when it occured  
        + VDTRMASTER-4946  [PO3 Companion] RPVR editSchedule doesn't return conflict recording when one is created.  
        + VDTRMASTER-4718  [CQ][R3][PVR]: a pending scheduled recording should start once the tuner has been released  
        + VDTRMASTER-4951  [PO3 Companion] UDN should be equal to 0 for STBs that are not in multiroom.  
        + VDTRMASTER-4799  [CQ][R3][PVR]: Wrong error code «PVR706 instead of PVR 710» displays when the disk is full  

***
version 1.2.6.0_PO3 - 2013.11.15
    - New Feature
        + Merged from R3 HEAD
    - Resolve
        N/A

***
version 1.2.5.1_PO3 - 2013.11.12
    - New Feature
        + Shows PIN popup automatically when there is a limitation on the PR.
    - Resolved Issues
        + VDTRMASTER-4940  [PO3 Companion][CQ][R4] - Application should not allow to attempt recording an unsubscribed channel  

***
version 1.2.4.2_PO3 - 2013.11.08
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4925  [PO3 Companion][CQ][R4] - Can't resume viewing a PVR content 
        + VDTRMASTER-4935  [PO3 Companion] RPVR getRecordingsMetadata of a manual recording doesn't return the category parameter

***
version 1.2.3.2_PO3 - 2013.10.31
    - New Feature
        N/A
    - Resolved Issues
        + VDTRMASTER-4913  [PO3 Companion] RPVR getRecordingsList returns scheduled records even if they are skipped.  
        + VDTRMASTER-4910  [PO3 Companion] PVR getRecordingsList does not return UDN when type=1 or type=3  

***
version 1.2.2.0_PO3 - 2013.10.16
    - New Feature
        N/A
    - Resolved Issues
        + RPVR version will be updated when recording is partially viewed.

***
version 1.2.1.0_PO3 - 2013.10.02
    - New Feature
        + Applied "api" parameter to the remote PVR.

***
version 1.2.0.0_PO3 - 2013.09.27
    - New Feature
        + First release including the followings
        + UC-PVR01B - Schedule a recording
        + UC-PVR02B - Delete a scheduled recording
        + UC-PVR03B - Retrieve a list of recordings and scheduled recordings
        + UC-PVR04B - Start a PVR playback
        + UC-REM01B - Apply remote control subset to STB
        + UC-STA01B - Receive the current state of the STB
    - Resolved Issues
        + N/A 


package com.videotron.tvi.illico.mainmenu;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;
import com.videotron.tvi.illico.mainmenu.communication.MainMenuServiceImpl;
import com.videotron.tvi.illico.mainmenu.communication.PreferenceProxy;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;
import com.videotron.tvi.illico.mainmenu.controller.MainController;
import com.videotron.tvi.illico.mainmenu.controller.BackgroundStateManager;
import com.videotron.tvi.illico.mainmenu.controller.NewDataManager;

/**
 * The initial class of Main Menu.
 * @author Sangjoon Kwon
 */
public class App implements Xlet, ApplicationConfig {
    /** XletContext instance. */
    public static XletContext xletContext;
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        Log.setApplication(this);
        FrameworkMain.getInstance().init(this);
        BackgroundStateManager.getInstance();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
        MainMenuServiceImpl.getInstance().start();
        CommunicationManager.getInstance().start();
        PreferenceProxy.getInstance().start();
        BackgroundStateManager.getInstance();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional
     *            If unconditional is true when this method is called,
     *            requests by the Xlet to not enter the destroyed state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        MainController.getInstance().dispose();
        DataManager.getInstance().dispose();
        PreferenceProxy.getInstance().dispose();
        CommunicationManager.getInstance().dispose();
        MainMenuServiceImpl.getInstance().dispose();
        FrameworkMain.getInstance().destroy();
        xletContext= null;
    }
    
    /*********************************************************************************
     * ApplicationConfig-implemented
     *********************************************************************************/
	public void init() {
        MainMenuServiceImpl.getInstance().init();
        CommunicationManager.getInstance().init();
        PreferenceProxy.getInstance().init();
        DataManager.getInstance().init();
        MainController.getInstance().init();
        NewDataManager.getInstance().init();
	}
	
    /**
     * Gets the version of this application.
     * @return the version of this application.
     */
    public String getVersion() {
        return Rs.APP_VERSION;
    }
    /**
     * Gets the name of this application.
     * @return the name of this application.
     */
    public String getApplicationName() {
        return Rs.APP_NAME;
    }
    /**
     * Gets the XletContext instance of this application.
     * @return XletContext instance of this application.
     */
    public XletContext getXletContext() {
        return xletContext;
    }
}

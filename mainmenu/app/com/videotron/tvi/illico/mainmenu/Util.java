package com.videotron.tvi.illico.mainmenu;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import com.videotron.tvi.illico.log.Log;

/**
 * The Class Util.
 */
public class Util {
    /** The Constant ZIP_FULE_BYTE_BUFFER.*/
    public static final int ZIP_FILE_BYTE_BUFFER = 1024;

    /**
     * Two bytes to int.
     *
     * @param data the data
     * @param offset the offset
     * @param byNegative the by negative
     * @return the int
     */
    public static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
        return (((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF));
    }

    /**
     * Int to2 byte.
     *
     * @param data the data
     * @return the byte[]
     */
    public static byte[] intTo2Byte(int data) {
        byte[] byteData = new byte[2];
        byteData[0] = (byte) ((data >> 8) & 0xff);
        byteData[1] = (byte) (data & 0xff);
        return byteData;
    }

    /**
     * String to2 byte.
     *
     * @param data the data
     * @return the byte[]
     */
    public static byte[] stringToNByte(String data, int targetByte) {
        if (data == null) {
            data = "";
        }
        int dataLth = data.length();
        if (targetByte>dataLth) {
            for (int i=0; i<targetByte-dataLth; i++) {
                data += " ";
            }
        }
        byte[] dataBytes = data.getBytes();
        byte[] byteData = new byte[targetByte];
        System.arraycopy(dataBytes, 0, byteData, 0, targetByte);
        return byteData;
    }

    /**
     * Flush zip file.
     *
     * @param target Zip File
     */
    public static void flushZipFile(ZipFile target) {
        if (target != null) {
            try {
                target.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            target = null;
        }
    }

    /**
     * Creates the zip file.
     *
     * @param source File
     * @return the ZipFile object
     */
    public static ZipFile createZipFile(File source) {
        if (source == null) {
            return null;
        }
        ZipFile result = null;
        try {
            result = new ZipFile(source);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Gets the bytes from zip.
     *
     * @param zipFile the zip file
     * @param entryName the entry name
     * @return the bytes from zip
     */
    public static byte[] getBytesFromZip(ZipFile zipFile, String entryName) {
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }
        if (entryName == null) {
            entryName = "";
        } else {
            entryName = entryName.trim();
        }
        if (entryName.length() <= 0) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Entry Name is invalid.");
            }
            return null;
        }
        return getBytesFromZip(zipFile, zipFile.getEntry(entryName));
    }
    /**
     * Gets the bytes from zip.
     *
     * @param zipFile the zip file
     * @param zipEntry the zip entry
     * @return the bytes from zip
     */
    public static byte[] getBytesFromZip(ZipFile zipFile, ZipEntry zipEntry) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[Util.getBytesFromZip]Param - Zip entry : " + zipEntry);
        }
        if (zipFile == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[Util.getBytesFromZip]Zip File is null.");
            }
            return null;
        }
        if (zipEntry == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(zipFile.getInputStream(zipEntry));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (Exception ioe) {
            ioe.printStackTrace();
        } finally {
            if (bout != null) {
                bout.reset();
                try{
                    bout.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                bout = null;
            }
            if (in != null) {
                try {
                    in.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                in = null;
            }
        }
        return result;
    }

    /**
     * Gets the bytes from zip.
     *
     * @param file the file
     * @return the bytes from zip
     * @throws ZipException the zip exception
     */
    public static byte[] getByteArrayFromFile(File file) {
        if (file == null) {
            return null;
        }
        byte[] result = null;
        InputStream in = null;
        ByteArrayOutputStream bout = null;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            bout = new ByteArrayOutputStream();
            byte[] buffer = new byte[ZIP_FILE_BYTE_BUFFER];
            while (true) {
                int read = in.read(buffer);
                if (read == -1) {
                    break;
                }
                bout.write(buffer, 0, read);
            }
            result = bout.toByteArray();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            if (bout != null) {
                bout.reset();
                try{
                    bout.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                bout = null;
            }
            if (in != null) {
                try {
                    in.close();
                }catch(IOException ioe) {
                    ioe.printStackTrace();
                }
                in = null;
            }
        }
        return result;
    }

    /**
     * Gets the uRL connect.
     *
     * @param data the data
     * @return the uRL connect
     */
    public static byte[] getURLConnect(String data) {
        BufferedInputStream bis = null;
        HttpURLConnection con = null;
        byte[] b = null;
        try {
            URL url = new URL(data);
            con = (HttpURLConnection) url.openConnection();
            
            // VDTRMASTER-5846
            int code = con.getResponseCode();
            if (Log.DEBUG_ON) {
            	Log.printDebug("Util.getURLConnect, responseCode=" + code);
            }
            bis = new BufferedInputStream(con.getInputStream());
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[ZIP_FILE_BYTE_BUFFER];
            int c;
            while ((c = bis.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }
            b = baos.toByteArray();
        } catch (MalformedURLException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bis = null;
            }
            if (con != null) {
                con.disconnect();
                con = null;
            }
        }
        return b;
    }

    /**
     * Load properties using by file name.
     * @param reqFileName file name
     * @return the properties
     */
    public static Properties loadProperties(String reqFileName) {
        if (reqFileName == null) {
            return null;
        }
        File file = new File(reqFileName);
        return loadProperties(file);
    }

    /**
     * Load properties using by file.
     * @param reqFile file
     * @return the properties
     */
    public static Properties loadProperties(File reqFile) {
        if (reqFile == null) {
            return null;
        }
        Properties prop = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(reqFile);
            prop.load(fis);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally{
            if (fis != null) {
                try{
                    fis.close();
                }catch(Exception ignore) {
                }
                fis = null;
            }
        }
        return prop;
    }
}

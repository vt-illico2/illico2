/*
 *  RendererNewMenu.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.gui;

import java.awt.Graphics;
import java.awt.Rectangle;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.util.Environment;

/**
 * <code>RendererNewMenu</code>
 * 
 * renderer class but do nothing except draw background image for test on the emulator
 */
public class RendererNewMenu extends Renderer {
    private static final Rectangle BOUND = new Rectangle(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);

    public Rectangle getPreferredBounds(UIComponent c) {
        return BOUND;
    }

    public void prepare(UIComponent c) {
    }

    protected void paint(Graphics g, UIComponent c) {
        if (Environment.EMULATOR) {
            g.drawImage(DataCenter.getInstance().getImage("img10.jpg"), 0, 0, null);
        }
    }
}

/*
 *  FirstMenu.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.DVBColor;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.ui.NewMenuUI;
import com.videotron.tvi.illico.util.FontResource;

/**
 * <code>FirstMenu</code>
 * 
 * 1st menu component
 */
public class FirstMenu extends Component {
    private NewMenuUI ui;

    public static final int W = 143; // focused/non-focused box's width
    public static final int H = 70; // non-focused box's height
    public static final int H2 = 130; // focused box's height
    public static final int Y2 = 5; // focused box's Y-coords, because of inclined box
    public static final int Y = 35; // non-focused box's Y-coords
    public static final int GAP = 1; // gap between the boxes

    private Font font1 = FontResource.BLENDER.getBoldFont(16);
    private Font font2 = FontResource.BLENDER.getBoldFont(18);
    private MenuEffect effect; // own effect for left/right
    private boolean inAni; // flag for animation, don't draw the yellow box in the animation 
    private int width; // this comp's width for convenient use
    private DVBBufferedImage buf; // buffered image for boxes for performance
    private boolean focused; // flag for focused on main menu
    private int srchBtnX, srchBtnY; // coords for search button

    private Image bgIllico, bgNormal; // menu box image, ILLICO has different color
    private Image rectL, rectR, rectFoc, rectSel; // focus image - left/right-inclined, normal yellow, gray one 
    private Image[] iconB, iconW; // menu icons - black and white, TODO integrate with MS
    private Image btnSrch, iconSrch, iconSrchFoc; // for search button

    Color cBlack = new DVBColor(5, 5, 5, 255);
    Color cWhite = Color.white;
    Color cYellow = new DVBColor(249, 194, 0, 255);
    Color cDim = new DVBColor(0, 0, 0, 150);

    int LEN;
    String[][] MENU;
    String searchStr;

    public void dispose() {
        if (buf != null) {
            buf.flush();
        }
    }

    public void setFocused(boolean focused) {
        this.focused = focused;
        repaint();
    }
    
    public void setInAni(boolean inAni) {
        this.inAni = inAni;
    }

    // makes a buffer with 6 boxes, and it used for paint()
    public void readyBuffer(NewMenuUI ui) {
        this.ui = ui;
        this.LEN = NewMenuUI.LEN;
        this.MENU = NewMenuUI.MENU;
        bgIllico = DataCenter.getInstance().getImage("bg_1st_lv_my_illico_box.png");
        bgNormal = DataCenter.getInstance().getImage("bg_1st_lv_n.png");
        rectL = DataCenter.getInstance().getImage("in_focus_l.png");
        rectR = DataCenter.getInstance().getImage("in_focus_r.png");
        rectFoc = DataCenter.getInstance().getImage("in_focus.png");
        rectSel = DataCenter.getInstance().getImage("in_focus_s.png");
        iconB = new Image[] {
            DataCenter.getInstance().getImage("icon_my_f.png"),
            DataCenter.getInstance().getImage("icon_recordings_f.png"),
            DataCenter.getInstance().getImage("icon_movie_f.png"),
            DataCenter.getInstance().getImage("icon_club_f.png"),
            DataCenter.getInstance().getImage("icon_channels_on_demand_f.png"),
            DataCenter.getInstance().getImage("icon_live_f.png"),
        };
        iconW = new Image[] {
            DataCenter.getInstance().getImage("icon_my_n.png"),
            DataCenter.getInstance().getImage("icon_recordings_n.png"),
            DataCenter.getInstance().getImage("icon_movie_n.png"),
            DataCenter.getInstance().getImage("icon_club_n.png"),
            DataCenter.getInstance().getImage("icon_channels_on_demand_n.png"),
            DataCenter.getInstance().getImage("icon_live_n.png"),
        };
        btnSrch = DataCenter.getInstance().getImage("bg_2nd_lv_search.png");
        iconSrch = DataCenter.getInstance().getImage("icon_search.png");
        iconSrchFoc = DataCenter.getInstance().getImage("icon_search_f.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();

        effect = new MenuEffect(this, 6);
        effect.setRequestor(ui);

        width = getWidth();
        buf = new DVBBufferedImage(width, getHeight());
        Graphics g = buf.getGraphics();
        g.setFont(font1);
        FontMetrics fm = g.getFontMetrics();
        int idx, x;
        for (int i = 0; i < LEN; i++) {
            idx = (ui.getOffset() + i) % LEN;
            x = i * (W + GAP);
            g.drawImage(idx == 0 ? bgIllico : bgNormal, x, Y, null);

            g.drawImage(iconW[idx], x + W / 2 - 20, Y + 3, null);

            g.setColor(cWhite);
            for (int j = 0; j < MENU[idx].length; j++) {
                int w = fm.stringWidth(MENU[idx][j]);
                int y = Y + (MENU[idx].length == 1 ? 58 : 50);
                g.drawString(MENU[idx][j], x + W / 2 - w / 2, y + j * 13);
            }
        }
        focused = true;
        
        searchStr = DataCenter.getInstance().getString("TxtMiniMenu.Search");
    }

    public void paint(Graphics g) {
        paintWithBuffer(g);
//        if (Log.EXTRA_ON) {
//            g.setColor(Color.red);
//            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
//        }
        super.paint(g);
    }

    private void paintWithBuffer(Graphics g) {
        int idx;
        int dx = 0, sx;
        g.setColor(cDim);
        for (int i = 0; i < LEN; i++) {
            idx = (ui.getOffset() + i) % LEN;
            if (idx == 1)
                continue; // focused position, not need to draw
            
            if (idx == 0) {
            	dx = idx * (W + GAP) + 2;
            } else if (idx >= 2) {
            	dx = idx * (W + GAP) - 2;
            }
            sx = i * (W + GAP);
            // TODO it can be done with only 2 draws not 6 times
            g.drawImage(buf, dx, Y, dx + W, Y + H, sx, Y, sx + W, Y + H, null);
            if (focused == false && ui.getFocus() != NewMenuUI.FOC_SEARCH) {
                g.fillRect(dx, Y, W, H); // draw dimmed if the focus on the 2nd menu
            }
        }
        
        // draw search button below
        boolean focSrch = ui.getFocus() == NewMenuUI.FOC_SEARCH;
        srchBtnX = W + GAP + (W - SecondMenu.W) / 2;
        srchBtnY = Y2 + H2 - 1;
        if (focSrch) {
            g.setColor(cYellow);
            g.fillRect(srchBtnX, srchBtnY, SecondMenu.W, SecondMenu.H);    
        } else {
            g.drawImage(btnSrch, srchBtnX, srchBtnY, null);
        }
        g.setColor(focSrch ? cBlack : cWhite);
        g.setFont(font1);
        srchBtnX = srchBtnX + (SecondMenu.W/2) - 12 - g.getFontMetrics().stringWidth(searchStr) / 2;
        g.drawImage(focSrch ? iconSrchFoc : iconSrch, srchBtnX, srchBtnY + 11, null);
        g.drawString(searchStr, srchBtnX + 24, srchBtnY + 25);
        srchBtnX = W + GAP + (W - SecondMenu.W) / 2; // to restore for animation.
        
        g.setFont(font1);
        
        int x = W + GAP;

        // draw the main yellow box
        if (inAni == false) {
            g.drawImage(focused ? rectFoc : rectSel, x, Y2, null);
            idx = (LEN - ui.getOffset() + 1) % LEN;
            g.drawImage(focused ? iconB[idx] : iconW[idx], x + 51, Y2 + 31 + 5, null);
            g.setColor(focused ? cBlack : cWhite);
            g.setFont(focused ? font2 : font1);
            FontMetrics fm = g.getFontMetrics();
            for (int j = 0; j < MENU[idx].length; j++) {
                int w = fm.stringWidth(MENU[idx][j]);
                int y = Y2 + (MENU[idx].length == 1 ? 102 : 94) + 4;
                g.drawString(MENU[idx][j], x + W / 2 - w / 2, y + j * 13);
            }
        }
    }

    public class MenuEffect extends Effect {
        int nextOffset;
        boolean left;
        DVBAlphaComposite[] nextAlpha;

        public MenuEffect(final Component c, final int frameCount) {
            super(c, frameCount);

            this.alpha = new DVBAlphaComposite[frameCount];
            nextAlpha = new DVBAlphaComposite[frameCount];
            
            int out = frameCount / 2;
            for (int i = 0; i < out; i++) { // fade-out the current menu status for 1/2 periods
                float v = (float) (frameCount - i) / (frameCount + 1);
                this.alpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, v);
            } 
            for (int i = out; i < frameCount; i++) { // fade-in the next menu status for next 1/2 periods
                float v = 1 - (float) (frameCount - i) / (frameCount + 1);
                nextAlpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, v);
                this.alpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0);
            }
        }

        protected void animate(Graphics g, DVBBufferedImage image, int frame) {
            int idx;
            int dx = 0, sx;
            if (nextAlpha[frame] == null) { // no need to draw the next menu status
                g.drawImage(image, 0, 0, width, Y2 + H2, 0, 0, width, Y2 + H2, null);
            } else {
                try {
                    ((DVBGraphics) g).setDVBComposite(nextAlpha[frame]);
                } catch (UnsupportedDrawingOperationException e) {
                }
    
                // TODO it can be done with only 2 draws not 6 times
                for (int i = 0; i < LEN; i++) { // draw next menu status from the buffer
                    idx = (nextOffset + i) % LEN;
                    if (idx == 0) {
                    	dx = idx * (W + GAP) + 2;
                    } else if (idx >= 2) {
                    	dx = idx * (W + GAP) - 2;
                    } else if (idx == 1) {
                    	continue;
                    }
                    sx = i * (W + GAP);
                    g.drawImage(buf, dx, Y, dx + W, Y + H, sx, Y, sx + W, Y + H, null);
                }
            }

            try {
                ((DVBGraphics) g).setDVBComposite(DVBAlphaComposite.SrcOver);
            } catch (UnsupportedDrawingOperationException e) {
            }
            // and search button below
            g.drawImage(image, srchBtnX, srchBtnY, srchBtnX + SecondMenu.W, srchBtnY + SecondMenu.H, 
                srchBtnX, srchBtnY, srchBtnX + SecondMenu.W, srchBtnY + SecondMenu.H, null);
            
            // and draw the inclined focus box during the animation
            g.drawImage(left ? rectL : rectR, W + GAP, 0, W, H2 + Y2 * 2, null);
        }

        public int getNextOffset() {
            return nextOffset;
        }
    }

    // start the animation left/right
    public void leftRight(boolean left, int nextOffset) {
        effect.nextOffset = nextOffset;
        effect.left = left;
        effect.start();
    }
}

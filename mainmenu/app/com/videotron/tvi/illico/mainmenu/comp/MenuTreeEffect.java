package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Point;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.effect.MovingEffect;

public class MenuTreeEffect extends MovingEffect {
    public MenuTreeEffect(Component c, int frameCount, Point from, Point to, int fade, short acceleration) {
        super(c, frameCount, from, to, fade, acceleration);
        // FRAME_DEALY = 40;
    }
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        int dx1 = pointX[frame] + targetBounds.x - relativeClipBounds.x;
        int dy1 = pointY[frame] + targetBounds.y - relativeClipBounds.y;
        if (fade == FADE_OUT) {
            // title area
            int sx = 53 - 51;
            int sy = 72 - 55;
            int w = 293;
            int h = 27;
            if (to.x < 0) { // left out
                g.drawImage(image, (sx - to.x), sy, (sx - to.x) + w, sy + h, sx, sy, sx + w, sy + h, null);
            } else {
                g.drawImage(image, sx, sy, sx + w, sy + h, sx, sy, sx + w, sy + h, null);
            }
            // menu area
            sy = 43;
            h = 390;
            g.drawImage(image, dx1 + sx, dy1 + sy, dx1 + sx + w, dy1 + sy + h, sx, sy, sx + w, sy + h, null);
            // content area
            sx = 347 - 51;
            sy = 72 - 55;
            w = 610;
            h = 35;
            g.drawImage(image, dx1 + sx, dy1 + sy, dx1 + sx + w, dy1 + sy + h, sx, sy, sx + w, sy + h, null);
        } else { // fade in
            // title area
            int sx = 53 - 51;
            int sy = 72 - 55;
            int w = 293;
            int h = 27;
            if (from.x < 0) { // left in
                g.drawImage(image, (sx - from.x), sy, (sx - from.x) + w, sy + h, sx, sy, sx + w, sy + h, null);
            } else {
                g.drawImage(image, sx, sy, sx + w, sy + h, sx, sy, sx + w, sy + h, null);
            }
            // menu area
            sy = 43;
            h = 390;
            g.drawImage(image, dx1 + sx, dy1 + sy, dx1 + sx + w, dy1 + sy + h, sx, sy, sx + w, sy + h, null);
            // content area
            sx = 347 - 51;
            sy = 72 - 55;
            w = 610;
            h = 35;
            g.drawImage(image, dx1 + sx, dy1 + sy, dx1 + sx + w, dy1 + sy + h, sx, sy, sx + w, sy + h, null);
        }
    }
}
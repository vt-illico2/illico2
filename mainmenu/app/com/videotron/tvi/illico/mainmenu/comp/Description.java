/*
 *  Background.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>Background</code>
 * 
 * background component that contains top/bottom gradient shadow also fade in at
 * first
 */

// TODO clock, footer
public class Description extends Component {
	private Image bgImg;
	private String desc;

	public Description() {
		super();

		bgImg = DataCenter.getInstance().getImage("bg_banner_2nd.png");

		FrameworkMain.getInstance().getImagePool().waitForAll();
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void paint(Graphics g) {
		if (desc != null && !desc.equals(TextUtil.EMPTY_STRING)) {
			g.drawImage(bgImg, 0, 0, this);
			g.setFont(FontResource.BLENDER.getFont(15));
			g.setColor(Color.white);
			g.drawString(desc, 7, 15);
		}
	}
}

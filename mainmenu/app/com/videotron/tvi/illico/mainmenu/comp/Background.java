/*
 *  Background.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;
import com.videotron.tvi.illico.mainmenu.controller.BackgroundStateManager;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * <code>Background</code>
 * 
 * background component that contains top/bottom gradient shadow
 * also fade in at first
 */

// TODO clock, footer
public class Background extends Component {
    private Image bgDimT, bgDimB;
    private Image logo, clockBg, clock;
    private boolean darker = false;
    
    Color cDim30 = new DVBColor(0, 0, 0, 77);
    Color cDim80 = new DVBColor(0, 0, 0, 204);
    
    Color cWhite80 = new DVBColor(255, 255, 255, 204);
    
    public Background() {
        super();
        
        bgDimT = DataCenter.getInstance().getImage("bg_dim_t.png");
        bgDimB = DataCenter.getInstance().getImage("bg_dim_b.png");
        logo = DataManager.getInstance().getBrandImage();
        clock = DataCenter.getInstance().getImage("clock.png");
        clockBg = DataCenter.getInstance().getImage("clockBg.png");
        
        FrameworkMain.getInstance().getImagePool().waitForAll();
        BackgroundStateManager.getInstance().setBackground(this);
    }

    public void setBgType(boolean darker) {
        if (this.darker == darker) return;
        this.darker = darker;
        repaint();
    }
    
    public void paint(Graphics g) {
        g.drawImage(bgDimT, 0, 0, 960, 87, null);
        //g.drawImage(bgDimB, 0, 371, 960, 169, null);
//    	g.setColor(cWhite80);
//    	g.fillRect(0, 0, 960, 540);
    	
        if (darker) {
            g.setColor(cDim80);
        } else {
            g.setColor(cDim30);
        }
    	
    	g.fillRect(0, 0, 960, 540);
        g.drawImage(logo, 53, 21, null); // 로고 좌표 EPG 랑 맞춤
        
        // clock
        g.drawImage(clockBg, 749, 37, null);
        g.setFont(FontResource.BLENDER.getFont(17));
        g.setColor(Color.WHITE);
        String clockStr = Formatter.getCurrent().getLongDate();
        if (clockStr != null) {
//            g.drawImage(clockBgImg, 749, 37, c);
            GraphicUtil.drawStringRight(g, clockStr, 910, 57);
            g.drawImage(clock, 910 - g.getFontMetrics().stringWidth(clockStr) - 24, 44, null);
        }
    }
}

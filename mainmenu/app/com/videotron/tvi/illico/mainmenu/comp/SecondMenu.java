/*
 *  SecondMenu.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.ui.NewMenuUI;
import com.videotron.tvi.illico.util.FontResource;

/**
 * <code>SecondMenu</code>
 * 
 * 2nd menu component
 * 
 */
public class SecondMenu extends Component {
    private NewMenuUI ui;

    public static final int W = 130; // width of a menu box
    public static final int H = 42; // height of a menu box
    public static final int GAP = 1; // gap between the menus
    public static final int X_MARGIN = 30; // margin on the left for extra info. ex) badge
    private Font font2 = FontResource.BLENDER.getBoldFont(16);
    
    private int focus;

    DVBColor cBlack = new DVBColor(0, 0, 0, 255);
    DVBColor cGray = new DVBColor(150, 150, 150, 200);
    DVBColor cWhite = new DVBColor(255, 255, 255, 240);
    DVBColor cYellow = new DVBColor(240, 200, 0, 255);
    
    int LEN;
    int x, base_y; // coords, y depends on number of menu especially
    String[][][] SUB;
    Image bg, bgFoc;
    
    public void prepare(int x, int base_y) {
        this.x = x;
        this.base_y = base_y;
        bg = DataCenter.getInstance().getImage("bg_2nd_lv.png");
        bgFoc = DataCenter.getInstance().getImage("bg_2nd_lv_f.png");
    }

    public void setMenuUI(NewMenuUI ui) {
        this.ui = ui;
        this.LEN = NewMenuUI.LEN;
        this.SUB = NewMenuUI.SUB;
    }

    int _offset; // keeps temporary

    public void updateBounds() {
        _offset = ui.getOffset();
        int idx = (LEN - ui.getOffset() + 1) % LEN; 
        int cnt = SUB[idx].length;
        int h = (H + GAP) * cnt;
        setBounds(x, base_y - h, W + X_MARGIN, h);
    }
    
    public void setFocus(int focus) {
        this.focus = focus;
        repaint();
    }

    public void paint(Graphics g) {
//        if (Log.EXTRA_ON) {
//            g.setColor(Color.red);
//            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
//        }
    	
    	if (!ui.isStarted()) {
    		Log.printDebug("SecondMenu, paint(Graphic), ui.isStarted=" + ui.isStarted());
    		return;
    	}
    	
        int idx = (LEN - _offset + 1) % LEN; 
        String[][] sub = SUB[idx];

        int h = getHeight();
        int x = X_MARGIN, y;
        g.setFont(font2);
        FontMetrics fm = g.getFontMetrics();
        for (int i = 0; i < sub.length; i++) {
        	y = h - H - 1 - i * (H + GAP);
        	
            g.drawImage(focus == i ? bgFoc : bg, x, y, null);
            g.setColor(focus == i ? cBlack : cWhite);
            y = h - H - i * (H + GAP);
            if (sub[i].length == 1) {
                int w = fm.stringWidth(sub[i][0]);
                g.drawString(sub[i][0], x + W / 2 - w / 2, y + 23);
            } else {
                for (int j = 0; j < sub[i].length; j++) {
                    int w = fm.stringWidth(sub[i][j]);
                    g.drawString(sub[i][j], x + W / 2 - w / 2, y + 17 + j * 13);
                }
            }
        }
    }
}

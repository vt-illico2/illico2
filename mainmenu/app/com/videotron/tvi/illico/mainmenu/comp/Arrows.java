/*
 *  Arrows.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.comp;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.ui.NewMenuUI;

/**
 * <code>Arrows</code>
 * 
 * 4 ways arrow around the yellow box on main menu
 * it comes after 2nd menu slide up
 * it draws 4 arrows closely to its bounds, thus UI should call setBounds() correctly
 */
public class Arrows extends Component {
    private Image up, down, left, right;
    private NewMenuUI ui;
    
    public Arrows() {
        up = DataCenter.getInstance().getImage("02_ars_t.png");
        down = DataCenter.getInstance().getImage("02_ars_b.png");
        left = DataCenter.getInstance().getImage("02_ars_l.png");
        right = DataCenter.getInstance().getImage("02_ars_r.png");
    }
    
    public void setMenuUI(NewMenuUI ui) {
        this.ui = ui;
    }
    
    public void paint(Graphics g) {
//        if (Log.EXTRA_ON) {
//            g.setColor(Color.red);
//            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
//        }
    	
    	if (!ui.isStarted()) {
    		Log.printDebug("SecondMenu, paint(Graphic), ui.isStarted=" + ui.isStarted());
    		return;
    	}
    	
        int gap = 4;
        g.drawImage(up, getWidth() / 2 - up.getWidth(null) / 2, 0+gap, null);
        g.drawImage(down, getWidth() / 2 - down.getWidth(null) / 2, getHeight() - down.getHeight(null)-gap, null);
        
        g.drawImage(left, 0+gap, getHeight() / 2 - left.getHeight(null) / 2, null);
        g.drawImage(right, getWidth() - right.getWidth(null)-gap, getHeight() / 2 - right.getHeight(null) / 2, null);
        
//        super.paint(g);
    }
}

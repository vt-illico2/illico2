package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.log.Log;

public class InbandDataListenerImpl implements InbandDataListener{
    public void receiveInbandData(String locator) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]locator : " + locator);
        }
        DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]called InbandAdapter.load(locator).");
        }
        CommunicationManager.getInstance().requestCompleteReceivingData();
        if (Log.DEBUG_ON) {
            Log.printDebug("[InbandDataListenerImpl.receiveInbandData]called completeReceivingData.");
        }
    }
}

package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;

public class RightFilterListenerImpl implements RightFilterListener {
    private String reqAppName;
    private String[] reqParamList;

    public void setRequestApplicationName(String reqAppName) {
        this.reqAppName = reqAppName;
    }
    public void setRequestExtendedParams(String[] reqExtParams) {
        this.reqParamList = reqExtParams;
    }
    public void receiveCheckRightFilter(int response) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[receiveCheckRightFilter]response : "+response);
        }
        PreferenceProxy.getInstance().setCheckingRight(false);
        switch (response) {
            case PreferenceService.RESPONSE_SUCCESS:
                if (Log.DEBUG_ON) {
                    Log.printDebug("[receiveCheckRightFilter]reqAppName : "+reqAppName);
                    Log.printDebug("[receiveCheckRightFilter]reqExtParams : "+reqParamList);
                }
                if (reqAppName == null) {
                    return;
                }
                CommunicationManager.getInstance().requestStartUnboundApplication(reqAppName, reqParamList);
                break;
            case PreferenceService.RESPONSE_ERROR:
                break;
            case PreferenceService.RESPONSE_FAIL:
                break;
            case PreferenceService.RESPONSE_CANCEL:
                break;
        }
    }
    public String[] getPinEnablerExplain() throws RemoteException {
        return new String[]{
                DataCenter.getInstance().getString("TxtFullMenu.pin_enabler_explain_0"),
                DataCenter.getInstance().getString("TxtFullMenu.pin_enabler_explain_1")
        };
    }
}

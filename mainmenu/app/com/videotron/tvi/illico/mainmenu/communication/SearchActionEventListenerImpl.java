package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.search.SearchActionEventListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.controller.MainController;

public class SearchActionEventListenerImpl implements SearchActionEventListener {
    public void actionRequested(String platform, String[] action) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[actionRequested]action : "+action);
        }
        if (action == null || action.length==0){
            if (Log.DEBUG_ON) {
                Log.printDebug("[actionRequested]action value is null. so return.");
            }
            return;
        }
        if (action[0] != null && action[0].equals(SearchActionEventListener.SEARCH_CLOSED)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[actionRequested]SEARCH_CLOSED.");
            }
            CommunicationManager.getInstance().requestHideSearchService();
            MainController.getInstance().resetInvisibleTimer(true);
            MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, true, true);
        }
    }
}

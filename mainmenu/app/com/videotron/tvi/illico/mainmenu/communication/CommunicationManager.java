package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;
import java.util.Hashtable;

import org.dvb.io.ixc.IxcRegistry;

import com.alticast.util.SharedMemory;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.dashboard.DashboardService;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessage;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.App;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.controller.MainMenuVbmController;
import com.videotron.tvi.illico.mainmenu.controller.VbmController;

public class CommunicationManager {
    private static CommunicationManager instance;
    /** Retry Look up Millis - Monitor. */
    public static final int RETRY_LOOKUP_MILLIS = 2000;

    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    /** The monitor service. */
    private MonitorService mSvc;
    private InbandDataListenerImpl iImpl;
    private EIDMappingTableUpdateListenerImpl eImpl;

    /*********************************************************************************
     * Epg Service-related
     *********************************************************************************/
    /** The epg service. */
    private EpgService eSvc;
    private VideoController vCnt;
    private VideoResizeListenerImpl vImpl;

    /*********************************************************************************
     * Dashboard Service-related
     *********************************************************************************/
    /** The SearchService service. */
    private DashboardService dSvc;

    /*********************************************************************************
     * Search Service-related
     *********************************************************************************/
    /** The SearchService service. */
    private SearchService sSvc;
    private SearchActionEventListenerImpl sImpl;
    private boolean activatedSearchService;

    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    /** The ErrorMessage service. */
    private ErrorMessageService emSvc;

    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private StcService stcSvc;
    private LogLevelChangeListenerImpl llclImpl;
    
    /*********************************************************************************
     * VBM Service-related
     *********************************************************************************/
    private VbmService vSvc;

    private PvrService pvrService;
    private VODService vodService;

    private CommunicationManager() {
    }

    public static synchronized CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }

    public void init() {
        iImpl = new InbandDataListenerImpl();
        eImpl = new EIDMappingTableUpdateListenerImpl();
        lookupMonitorService();
        lookupVbmService();
        lookupPvrService();
        lookupVodService();

        llclImpl = new LogLevelChangeListenerImpl();
        if (!Rs.IS_EMULATOR) {
            lookupStcService();
        }

        vImpl = new VideoResizeListenerImpl();

        sImpl = new SearchActionEventListenerImpl();
        activatedSearchService = false;
    }
    public void dispose() {
        sImpl = null;
        sSvc = null;

        dSvc = null;

        if (vCnt != null) {
            try{
                vCnt.removeVideoResizeListener(vImpl);
            }catch(Exception e) {
                e.printStackTrace();
            }
            vCnt = null;
        }
        vImpl = null;
        eSvc = null;
        if (stcSvc != null) {
            try{
                stcSvc.removeLogLevelChangeListener(Rs.APP_NAME);
            }catch(Exception e) {
                e.printStackTrace();
            }
            stcSvc = null;
        }
        llclImpl = null;
        disposeMonitorService();
        iImpl = null;
        eImpl = null;
        if (vSvc!=null) {
        	MainMenuVbmController.getInstance().dispose();
            vSvc=null;
        }
        mSvc=null;
    }
    public void start() {
    }
    public void stop() {
    }

    public void notifyPortalVisibilityChanged(boolean visible) {
        if (Log.DEBUG_ON) Log.printDebug("CommunicationManager.notifyPortalVisibilityChanged("+visible+")");
        if (Log.DEBUG_ON) Log.printDebug("CommunicationManager.notifyPortalVisibilityChanged() : pvrService = "+pvrService);
        if (Log.DEBUG_ON) Log.printDebug("CommunicationManager.notifyPortalVisibilityChanged() : vodService = "+vodService);
        if (pvrService != null) {
            try {
                pvrService.portalVisibilityChanged(visible);
            } catch (Exception e) {
                Log.print(e);
            }
        }
        if (vodService != null) {
            try {
                vodService.portalVisibilityChanged(visible);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /*********************************************************************************
     * Monitor Service-related
     *********************************************************************************/
    public MonitorService getMonitorService() {
        return mSvc;
    }
    private void lookupMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupMonitorService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (mSvc == null) {
                            try {
                                String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                                mSvc = (MonitorService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupMonitorService]not bound - " + MonitorService.IXC_NAME);
                                }
                            }
                        }
                        if (mSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupMonitorService]looked up.");
                            }
                            initMonitorService();
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupMonitorService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupVbmService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupVbmService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (vSvc == null) {
                            try {
                                String lookupName = "/1/2085/" + VbmService.IXC_NAME;
                                vSvc = (VbmService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupVbmService]not bound - " + VbmService.IXC_NAME);
                                }
                            }
                        }
                        if (vSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupVbmService]looked up.");
                            }
                            MainMenuVbmController.getInstance().init(vSvc);
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupVbmService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupPvrService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupPvrService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (pvrService == null) {
                            try {
                                String lookupName = "/1/20A0/" + PvrService.IXC_NAME;
                                pvrService = (PvrService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupPvrService]not bound - " + PvrService.IXC_NAME);
                                }
                            }
                        }
                        if (pvrService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupPvrService]looked up.");
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupPvrService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void lookupVodService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupVodService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (vodService == null) {
                            try {
                                String lookupName = "/1/1020/" + VODService.IXC_NAME;
                                vodService = (VODService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupVodService]not bound - " + VODService.IXC_NAME);
                                }
                            }
                        }
                        if (vodService != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupVodService]looked up.");
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupVodService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.initMonitorService]start");
        }
        try {
            mSvc.addInbandDataListener(iImpl, Rs.APP_NAME);
        } catch (Exception ignore) {
        }
        try{
            mSvc.addEIDMappingTableUpdateListener(eImpl, Rs.APP_NAME);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    private void disposeMonitorService() {
        try {
            mSvc.removeInbandDataListener(iImpl, Rs.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try{
            mSvc.removeEIDMappingTableUpdateListener(eImpl, Rs.APP_NAME);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
    public boolean requestCompleteReceivingData() {
        try {
            mSvc.completeReceivingData(Rs.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestExitApplication() {
        if (Log.DEBUG_ON) {
            Log.printInfo("[CommMgr.requestExitApplication]Called.");
            Log.printInfo("[CommMgr.requestExitApplication]Monitor service : " + mSvc);
        }
        if (mSvc == null) {
            return false;
        }
        try {
//            boolean isSpecialVideoState = requestIsSpecialVideoState();
//            if (Log.DEBUG_ON) {
//                Log.printInfo("[CommMgr.requestExitApplication]Is special video state : " + isSpecialVideoState);
//            }
//            if (isSpecialVideoState) {
//                mSvc.stopOverlappingApplication();
//            } else {
//                mSvc.exitToChannel();
//            }
            mSvc.exitToChannel();
            return true;
        }catch(Exception e) {
            e.printStackTrace();
        }
        return false;
    }
    public boolean requestIsSpecialVideoState() {
        if (Rs.IS_EMULATOR) {
            return false;
        }
        boolean result = false;
        try {
            if (mSvc != null && mSvc.isSpecialVideoState()) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
    public boolean requestIsTvViewingState() {
        if (Rs.IS_EMULATOR) {
            return false;
        }
        boolean result = false;
        try {
            if (mSvc != null && mSvc.isTvViewingState()) {
                result = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    } 
    public String requestGetVideoContextName() {
        if (Rs.IS_EMULATOR) {
            return null;
        }
        String result = null;
        try {
            if (mSvc != null) {
                result = mSvc.getVideoContextName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    } 
    
    
    
    public boolean requestStartUnboundApplication(String appName, String[] appParamList) {
        if (Log.DEBUG_ON) {
            Log.printInfo("[CommMgr.requestStartUnboundApplication]Request app name : "+appName);
            Log.printInfo("[CommMgr.requestStartUnboundApplication]Request app parameter list : "+appParamList);
            if (appParamList != null) {
                for (int i=0; i<appParamList.length; i++) {
                    Log.printInfo("[CommMgr.requestStartUnboundApplication]Request app parameter["+i+"] : "+appParamList[i]);
                }
            }
        }
        try {
            mSvc.startUnboundApplication(appName, appParamList);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
//    public boolean requestLoadAODApplication(String aodAppName) {
//        if (Log.DEBUG_ON) {
//            Log.printInfo("[CommMgr.requestLoadAODApplication]Request app name : "+aodAppName);
//        }
//        try {
//            mSvc.loadAODApplication(aodAppName, null);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
//    public boolean requestStartAODApplication(String aodAppName) {
//        if (Log.DEBUG_ON) {
//            Log.printInfo("[CommMgr.requestStartAODApplication]Request app name : "+aodAppName);
//        }
//        try {
//            mSvc.startAODApplication(aodAppName, null);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
    public boolean requestRegisterUnboundApplication(String xaitFilePath) {
        try {
            mSvc.registerUnboundApplication(xaitFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestAddScreenSaverConfirmListener(ScreenSaverConfirmationListener l) {
        try {
            mSvc.addScreenSaverConfirmListener(l, Rs.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestRemoveScreenSaverConfirmListener(ScreenSaverConfirmationListener l) {
        try {
            mSvc.removeScreenSaverConfirmListener(l, Rs.APP_NAME);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public int requestCheckResourceAuthorization(String pkgName) {
        try {
            return mSvc.checkResourceAuthorization(pkgName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return MonitorService.NOT_AUTHORIZED;
    }
    
    /*********************************************************************************
     * STC Service-related
     *********************************************************************************/
    private void lookupStcService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.lookupStcService]start");
        }
        try {
            new Thread() {
                public void run() {
                    while (true) {
                        if (stcSvc == null) {
                            try {
                                String lookupName = "/1/2100/" + StcService.IXC_NAME;
                                stcSvc = (StcService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[CommMgr.lookupStcService]not bound - " + StcService.IXC_NAME);
                                }
                            }
                        }
                        if (stcSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[CommMgr.lookupStcService]looked up.");
                            }
                            try {
                                int currentLevel = stcSvc.registerApp(Rs.APP_NAME);
                                Log.printDebug("stc log level = " + currentLevel);
                                Log.setStcLevel(currentLevel);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            Object o = SharedMemory.getInstance().get("stc-" + Rs.APP_NAME);
                            if (o != null) {
                                DataCenter.getInstance().put("LogCache", o);
                            }
                            try {
                                stcSvc.addLogLevelChangeListener(Rs.APP_NAME, llclImpl);
                            } catch (RemoteException ex) {
                                Log.print(ex);
                            }
                            break;
                        }
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[CommMgr.lookupStcService]retry lookup.");
                        }
                        try {
                            Thread.sleep(RETRY_LOOKUP_MILLIS);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*********************************************************************************
     * Epg Service-related
     *********************************************************************************/
    public EpgService getEpgService() {
        if (eSvc == null) {
            try {
                String lookupName = "/1/2026/" + EpgService.IXC_NAME;
                eSvc = (EpgService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getEpgService]not bound - " + EpgService.IXC_NAME);
                }
            }
        }
        return eSvc;
    }
    public VideoController getVideoController() {
        if (vCnt == null) {
            EpgService svc = getEpgService();
            if (svc != null) {
                try{
                    vCnt = svc.getVideoController();
                    if (vCnt != null) {
                        vCnt.addVideoResizeListener(vImpl);
                    }
                }catch(Exception e) {
                    vCnt = null;
                    e.printStackTrace();
                }
            }
        }
        return vCnt;
    }
    public boolean requestChangeChannel(int id) {
        if (requestIsSpecialVideoState()) {
            return true;
        }
        try {
            getEpgService().getChannelContext(0).changeChannel(id);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestChangeChannel(String callLetter) {
        try{
            EpgService svc = getEpgService();
            TvChannel tvChannel = svc.getChannel(callLetter);
            svc.getChannelContext(0).changeChannel(tvChannel);
        }catch(Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestResizeScreen(int x, int y, int w, int h) {
        VideoController ctl = getVideoController();
        if (ctl != null) {
            try {
                ctl.resize(x, y, w, h, VideoController.SHOW);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /*********************************************************************************
     * Dashboard Service-related
     *********************************************************************************/
    public DashboardService getDashboardService() {
        if (dSvc == null) {
            try {
                String lookupName = "/1/2081/" + DashboardService.IXC_NAME;
                dSvc = (DashboardService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getDashboardService]not bound - " + DashboardService.IXC_NAME);
                }
            }
        }
        return dSvc;
    }
    public boolean requestShowDashboardService() {
        try {
            getDashboardService().showDashboard();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean requestHideDashboardService() {
        try {
            getDashboardService().hideDashboard();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /*********************************************************************************
     * Search Service-related
     *********************************************************************************/
    public SearchService getSearchService() {
        if (sSvc == null) {
            try {
                String lookupName = "/1/2080/" + SearchService.IXC_NAME;
                sSvc = (SearchService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getSearchService]not bound - " + SearchService.IXC_NAME);
                }
            }
        }
        return sSvc;
    }
    public void requestShowSearchService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestShowSearchService]start");
        }
        SearchService svc = getSearchService();
        if (svc != null) {
            try {
                svc.addSearchActionEventListener(Rs.APP_NAME, sImpl);
                svc.launchSearchApplication(Rs.APP_NAME, true);
                activatedSearchService = true;
            } catch (Exception e) {
                e.printStackTrace();
                activatedSearchService = false;
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestShowSearchService]end");
        }
    }
    public void requestHideSearchService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[CommMgr.requestHideSearchService]start");
        }
        SearchService svc = getSearchService();
        if (sSvc != null) {
            try {
                svc.removeSearchAcitonEventListener(Rs.APP_NAME);
                svc.stopSearchApplication();
            } catch (Exception ignore) {
                ignore.printStackTrace();
            } finally {
                activatedSearchService = false;
            }
        }
    }
    public boolean requestActivatedSearchService() {
        return activatedSearchService;
    }
    /*********************************************************************************
     * ErrorMessage Service-related
     *********************************************************************************/
    public ErrorMessageService getErrorMessageService() {
        if (emSvc == null) {
            try {
                String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                emSvc = (ErrorMessageService) IxcRegistry.lookup(App.xletContext, lookupName);
            } catch (Exception e) {
                if (Log.INFO_ON) {
                    Log.printInfo("[CommMgr.getErrorMessageService]not bound - " + ErrorMessageService.IXC_NAME);
                }
            }
        }
        return emSvc;
    }
    public void requestShowErrorMessage(String errorCode, ErrorMessageListener l) {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.showErrorMessage(errorCode, l);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void requestShowErrorMessage(String errorCode, ErrorMessageListener l, Hashtable dt) {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.showErrorMessage(errorCode, l, dt);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public void requestHideErrorMessage() {
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                svc.hideErrorMessage();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public ErrorMessage requestGetErrorMessage(String errorCode) {
        ErrorMessage res = null;
        ErrorMessageService svc = getErrorMessageService();
        if (svc != null) {
            try {
                res = svc.getErrorMessage(errorCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
}

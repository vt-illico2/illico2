package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.App;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;

/**
 * The Class PreferenceProxy manage User Profile & Preference data.<p>
 * Try to look up UPP Service at the time of initializing this class.
 * If successful, upp data get from the PreferenceService interface.<br>
 * If upp data changed, this class get Event from PreferenceListener.
 *
 * @author Sangjoon Kwon
 */
public class PreferenceProxy implements PreferenceListener {
    private static final int UPP_INDEX_COUNT = 7;
    private static final int UPP_INDEX_SHORTCUT_DISPLAY = 0;
    private static final int UPP_INDEX_SHORTCUT_CONTENTS = 1;
    private static final int UPP_INDEX_LANGUAGE = 2;
    private static final int UPP_INDEX_DISPLAY_MENU_AT_POWER_ON = 3;
    private static final int UPP_INDEX_SUSPEND_DEFAULT_TIME = 4;
    private static final int UPP_INDEX_PARENTAL_CONTROL = 5;
    private static final int UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS = 6;

    private String[] prefNames;
    private String[] prefValues;
    private DataManager dataMgr = DataManager.getInstance();
    private RightFilterListenerImpl rImpl;
    private boolean isCheckingRight;
    /** The instance - PreferenceProxy. */
    private static PreferenceProxy instance;
    /** The preference service. */
    private PreferenceService uppSvc;
    /** The Constant TEST_SHORTCUT_MENU_ITEM_ID. */
    public static final int TEST_SHORTCUT_MENU_ITEM_ID = 10;

    /**
     * Gets the single instance of PreferenceProxy.
     * @return single instance of PreferenceProxy
     */
    public static synchronized PreferenceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceProxy();
        }
        return instance;
    }

    /**
     * Instantiates a new preference proxy.
     */
    private PreferenceProxy() {
        prefNames = new String[UPP_INDEX_COUNT];
        prefNames[UPP_INDEX_SHORTCUT_DISPLAY] = PreferenceNames.SHORTCUT_MENU_DISPLAY;
        prefNames[UPP_INDEX_SHORTCUT_CONTENTS] = PreferenceNames.SHORTCUT_MENU_LIST;
        prefNames[UPP_INDEX_LANGUAGE] = PreferenceNames.LANGUAGE;
        prefNames[UPP_INDEX_DISPLAY_MENU_AT_POWER_ON] = PreferenceNames.DISPLAY_MENU_AT_POWER_ON;
        prefNames[UPP_INDEX_SUSPEND_DEFAULT_TIME] = RightFilter.SUSPEND_DEFAULT_TIME;
        prefNames[UPP_INDEX_PARENTAL_CONTROL] = RightFilter.PARENTAL_CONTROL;
        prefNames[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = RightFilter.PROTECT_SETTINGS_MODIFICATIONS;

        prefValues = new String[UPP_INDEX_COUNT];
        prefValues[UPP_INDEX_SHORTCUT_DISPLAY] = Definitions.OPTION_VALUE_DISABLE;
        prefValues[UPP_INDEX_SHORTCUT_CONTENTS] = "";
        prefValues[UPP_INDEX_LANGUAGE] = Definitions.LANGUAGE_ENGLISH;
        prefValues[UPP_INDEX_DISPLAY_MENU_AT_POWER_ON] = Definitions.OPTION_VALUE_YES;
        prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME] = Definitions.OPTION_VALUE_SUSPENDED_FOR_4_HOURS;
        prefValues[UPP_INDEX_PARENTAL_CONTROL] = Definitions.OPTION_VALUE_OFF;
        prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = Definitions.OPTION_VALUE_NO;
    }

    /**
     * Inits PreferenceProxy.
     * @param xCtx
     *            the xlet context
     */
    public void init() {
        rImpl = new RightFilterListenerImpl();
        lookUPPService();
    }

    /**
     * Dispose PreferenceProxy.
     */
    public void dispose() {
        prefNames = null;
        prefValues = null;
        rImpl = null;
        if (uppSvc != null) {
            try{
                uppSvc.removePreferenceListener(Rs.APP_NAME, this);
            }catch(Exception ignore) {
            }
            uppSvc = null;
        }
        instance = null;
    }

    /**
     * Start PreferenceProxy.
     */
    public void start() {
    }

    /**
     * Stop PreferenceProxy.
     */
    public void stop() {
    }

    /**
     * Look up User Profile and Preference.
     */
    private void lookUPPService() {
        /** IXC Binding */
        try {
            new Thread() {
                public void run() {
                    String lookupName = "/1/2030/" + PreferenceService.IXC_NAME;
                    while (true) {
                        if (uppSvc == null) {
                            try {
                                uppSvc = (PreferenceService) IxcRegistry.lookup(App.xletContext, lookupName);
                            } catch (Exception e) {
                                if (Log.INFO_ON) {
                                    Log.printInfo("[PreferenceProxy.lookUPPService]not bound - " + PreferenceService.IXC_NAME);
                                }
                            }
                        }
                        if (uppSvc != null) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[PreferenceProxy.lookUpService]looked up PreferenceService. ");
                            }
                            prepare();
                            break;
                        }
                        try {
                            Thread.sleep(Rs.RETRY_LOOKUP_MILLIS_UPP);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            } .start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void prepare() {
        String[] uppData = null;
        try {
            uppData = uppSvc.addPreferenceListener(this, Rs.APP_NAME, prefNames, prefValues) ;
        } catch (Exception ignore) {
            ignore.printStackTrace();
        }
        if (uppData != null) {
            try {
                receiveUpdatedPreference(PreferenceNames.SHORTCUT_MENU_DISPLAY, uppData[UPP_INDEX_SHORTCUT_DISPLAY]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(PreferenceNames.SHORTCUT_MENU_LIST, uppData[UPP_INDEX_SHORTCUT_CONTENTS]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(PreferenceNames.LANGUAGE, uppData[UPP_INDEX_LANGUAGE]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(PreferenceNames.DISPLAY_MENU_AT_POWER_ON, uppData[UPP_INDEX_DISPLAY_MENU_AT_POWER_ON]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(RightFilter.SUSPEND_DEFAULT_TIME, uppData[UPP_INDEX_SUSPEND_DEFAULT_TIME]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(RightFilter.PARENTAL_CONTROL, uppData[UPP_INDEX_PARENTAL_CONTROL]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                receiveUpdatedPreference(RightFilter.PROTECT_SETTINGS_MODIFICATIONS, uppData[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS]);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public PreferenceService getPreferenceService() {
        return uppSvc;
    }
    /**
     * invoked when UPP application has been finished to check Rights and Filters.
     * @param result result checks ring filter
     * @throws RemoteException the remote exception
     */
    public void receiveCheckRightFilter(String result) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy.receiveCheckRightFilter: result-" + result);
        }
    }
    /**
     * invoked when the value of the preference has changed.
     *
     * @param name Updated Preference Name
     * @param value Updated value of Preference
     * @throws RemoteException the remote exception
     */
    public void receiveUpdatedPreference(String name, String value) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PreferenceProxy.receiveUpdatedPreference]name:[" + name + "]-value:[" + value
                    + "]");
        }
        if (name == null || value == null) {
            return;
        }
        if (name.equals(PreferenceNames.LANGUAGE)) {
            DataCenter.getInstance().put(PreferenceNames.LANGUAGE , value);
            dataMgr.changeBrandImage(value);
            prefValues[UPP_INDEX_LANGUAGE] = value;
        } else if (name.equals(PreferenceNames.DISPLAY_MENU_AT_POWER_ON)) {
            prefValues[UPP_INDEX_DISPLAY_MENU_AT_POWER_ON] = value;
        } else if (name.equals(RightFilter.SUSPEND_DEFAULT_TIME)) {
            prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME] = value;
        } else if (name.equals(RightFilter.PARENTAL_CONTROL)) {
            prefValues[UPP_INDEX_PARENTAL_CONTROL] = value;
        } else if (name.equals(RightFilter.PROTECT_SETTINGS_MODIFICATIONS)) {
            prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] = value;
        }
    }
    /*********************************************************************************
     * CheckRight-related
     *********************************************************************************/
    public boolean requestCheckRightFilter(String appName, String[] extParams) {
        setCheckingRight(true);
        rImpl.setRequestApplicationName(appName);
        rImpl.setRequestExtendedParams(extParams);
        try {
            getPreferenceService().checkRightFilter(rImpl, appName,
                    new String[] { RightFilter.PROTECT_SETTINGS_MODIFICATIONS }, null, null);
        } catch (Exception e) {
            setCheckingRight(false);
            e.printStackTrace();
            return false;
        }
        return true;
    }
    public void setCheckingRight(boolean isCheckingRight) {
        this.isCheckingRight = isCheckingRight;
    }
    public boolean isCheckingRight() {
        return isCheckingRight;
    }
    public boolean isActivatedMenu() {
        if (prefValues[UPP_INDEX_DISPLAY_MENU_AT_POWER_ON].equals(Definitions.OPTION_VALUE_YES)) {
            return true;
        }
        return false;
    }
    public boolean isActivatedMiniMenu() {
        if (prefValues[UPP_INDEX_SHORTCUT_DISPLAY].equals(Definitions.OPTION_VALUE_ENABLE)) {
            return true;
        }
        return false;
    }
    /*********************************************************************************
     * Language-related
     *********************************************************************************/
    public String getCurrentLanguage() {
        return prefValues[UPP_INDEX_LANGUAGE];
    }
    /*********************************************************************************
     * Parental control-related
     *********************************************************************************/
    public boolean setParentalControl(String reqValue) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setParentalControl]Request value : " + reqValue);
            Log.printDebug("[PrefProxy.setParentalControl]Current parental control : " + prefValues[UPP_INDEX_PARENTAL_CONTROL]);
        }
        if (reqValue == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.setParentalControl]Request value is invalid.");
            }
            return false;
        }
        boolean result = false;
        if (reqValue.equals(prefValues[UPP_INDEX_PARENTAL_CONTROL])) {
            result = true;
        } else {
            if (uppSvc != null) {
                try{
                    result = uppSvc.setPreferenceValue(RightFilter.PARENTAL_CONTROL, reqValue);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.setParentalControl]result : " + result);
        }
        return result;
    }
    public String getParentalControl() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.getParentalControl]Parental control : " + prefValues[UPP_INDEX_PARENTAL_CONTROL]);
        }
        return prefValues[UPP_INDEX_PARENTAL_CONTROL];
    }
    public String getCurrentSuspendDefaultTime() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.getCurrentSuspendDefaultTime]Suspend default time : " + prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME]);
        }
        return prefValues[UPP_INDEX_SUSPEND_DEFAULT_TIME];
    }

    public boolean isProtectSettingsModifications() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isProtectSettingsModifications]Called.");
        }
        if (prefValues == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Preference value list is null return false.");
            }
            return false;
        }
        if (prefValues[UPP_INDEX_PARENTAL_CONTROL] == null
                || !prefValues[UPP_INDEX_PARENTAL_CONTROL].equals(Definitions.OPTION_VALUE_ON)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Parental control value is not on. return false.");
            }
            return false;
        }
        if (prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS] == null
                || prefValues[UPP_INDEX_PROTECT_SETTINGS_MODIFICATIONS].equals(Definitions.OPTION_VALUE_NO)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[PrefProxy.isProtectSettingsModifications]Protect settings modification value is not yes. return false.");
            }
            return false;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[PrefProxy.isProtectSettingsModifications]return true.");
        }
        return true;
    }
    /*********************************************************************************
     * PIN Enabler-related
     *********************************************************************************/
    public boolean showPinEnabler(PinEnablerListener l, String[] msg) {
        if (uppSvc != null) {
            try {
                uppSvc.showPinEnabler(l, msg);
                return true;
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        return false;
    }
}

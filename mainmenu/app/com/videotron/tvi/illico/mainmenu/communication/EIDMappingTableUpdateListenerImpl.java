package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.monitor.EIDMappingTableUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;

public class EIDMappingTableUpdateListenerImpl implements EIDMappingTableUpdateListener{
    private DataManager dMgr = DataManager.getInstance();

    public void tableUpdated() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EIDMappingTableUpdateListenerImpl.tableUpdated]called");
        }
//        boolean isValidFmData = dMgr.authorizeFullMenuItem();
//        if (isValidFmData) {
//            dMgr.storeToLatestSuccessFullMenuItemData();
//            dMgr.putShortcutDataToSharedMemory();
//            dMgr.resetShortcutMenuItem();
////            dMgr.sendDataManagerUpdateEvent(DataManagerListener.DATA_KEY_FULL_MENU_ITEM);
//        } else {
//            dMgr.restoreFromLatestSuccessFullMenuItemData();
//        }
//        boolean isValidMmData = dMgr.authorizeMiniMenuItem();
//        if (isValidMmData) {
//            dMgr.storeToLatestSuccessMiniMenuItemData();
//            dMgr.putFixedDataToSharedMemory();
////            dMgr.sendDataManagerUpdateEvent(DataManagerListener.DATA_KEY_MINI_MENU_ITEM);
//        } else {
//            dMgr.restoreFromLatestSuccessMiniMenuItemData();
//        }
    }

    public void addEIDMappingTableUpdateListener(MonitorService mSvc) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EIDMappingTableUpdateListenerImpl.addEIDMappingTableUpdateListener]called");
        }
        try{
            mSvc.addEIDMappingTableUpdateListener(this, Rs.APP_NAME);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void removeEIDMappingTableUpdateListener(MonitorService mSvc) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[EIDMappingTableUpdateListenerImpl.removeEIDMappingTableUpdateListener]called");
        }
        try{
            mSvc.removeEIDMappingTableUpdateListener(this, Rs.APP_NAME);
        }catch(Exception e) {
            e.printStackTrace();
        }
    }
}

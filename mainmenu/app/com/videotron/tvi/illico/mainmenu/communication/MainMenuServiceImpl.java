package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import org.dvb.io.ixc.IxcRegistry;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;

import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.App;
import com.videotron.tvi.illico.mainmenu.controller.MainController;

/**
 * This class implemented MainMenuService interface. <p>
 * Other application execute Main Menu by MainMenuService interface.<br>
 * When booting time or Stand-by on, Main Menu execute by Manageement System display config value.<br>
 * When press RCU-Key, Main Menu execute appropriate Main Menu.<br>
 * <li> Activated Main Menu -> Deactivate Main Menu.
 * <li> Deactivated Main Menu and Has 1 more shortcut menu -> Activate Mini Menu.
 * <li> Deactivated Main Menu and Has no shortcut menu -> Activate Full Menu.
 * <li> Activated Mini Menu -> Activate Full Menu.
 * @author Sangjoon Kwon
 */
public class MainMenuServiceImpl implements MainMenuService, PowerModeChangeListener {
    /** MainMenuServiceImpl instance - Singleton. */
    private static MainMenuServiceImpl instance;
    /** because Management system display config vlaue just use booting time and stand-by on. */
    private boolean isBootingTime = true;

    private boolean isDeactivatedSTB;

    private MainMenuServiceImpl() {
    }
    /**
     * Gets the single instance of MainMenuServiceImpl.
     * @return single instance of MainMenuServiceImpl
     */
    public static synchronized MainMenuServiceImpl getInstance() {
        if (instance == null) {
            instance = new MainMenuServiceImpl();
        }
        return instance;
    }

    public void init() {
        Host.getInstance().addPowerModeChangeListener(this);
        bindToIxc();
    }
    /**
     * Dispose MainMenuServiceImpl.
     */
    public void dispose() {
        try {
            IxcRegistry.unbind(App.xletContext, MainMenuService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Host.getInstance().removePowerModeChangeListener(this);
        instance = null;
    }
    /**
     * Start MainMenuServiceImpl.
     * @param xCtx
     *            the xlet ctx
     */
    public void start() {
    }
    public void stop() {
    }

    /**
     * Bind to IxcRegistry.
     */
    private void bindToIxc() {
        try {
            IxcRegistry.bind(App.xletContext, MainMenuService.IXC_NAME, this);
            if (Log.INFO_ON) {
                Log.printInfo("[MainMenuServiceImpl.bindToIxc]Bind success : "+MainMenuService.IXC_NAME);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * requests to show main menu.
     * @param mainMenuState
     *            the state of the main menu
     * @param parameters
     *            Specific Parameters.
     * @throws RemoteException
     *             the remote exception
     */
    public void showMenu(int mainMenuState, String[] parameters) throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainMenuServiceImpl.showMenu]called changeMainMenuState [" + mainMenuState + "]");
        }
        boolean resetData = true;
        if (parameters != null && parameters.length > 0) {
            String keyString = parameters[0];
            
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainMenuServiceImpl.showMenu]keyString:" + keyString);
            }
            
            if (keyString.equals(MonitorService.REQUEST_APPLICATION_HOT_KEY)) {
                resetData = true;
                MainController.getInstance().changeMenuState(mainMenuState, resetData, true);
            } else if (keyString.equals(MonitorService.REQUEST_APPLICATION_MENU)) {
                resetData = true;
                MainController.getInstance().changeMenuState(mainMenuState, resetData, false);
            }
        } else {
        	MainController.getInstance().changeMenuState(mainMenuState, resetData, false);
        }
        
    }

    /**
     * Gets the main menu state.
     *
     * @return the main menu state
     * @throws RemoteException
     *             the remote exception
     */
    public int getMainMenuState() throws RemoteException {
        return MainController.getInstance().getMainMenuState();
    }

    /**
     * Resumes menu.
     *
     * @throws RemoteException
     *             the remote exception
     */
    public void resumeMenu() throws RemoteException {
    }

    /**
     * Hide menu.
     *
     * @throws RemoteException
     *             the remote exception
     * @return
     */
    public void hideMenu() throws RemoteException {
        if (Log.DEBUG_ON) {
            Log.printDebug("MainMenuServiceImpl.hideMenu: ");
        }
        MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false, true);
    }

    /**
     * Gets target menu state.
     *
     * @return int
     * @throws RemoteException
     *             the remote exception
     */
    public int getTargetMenuState() throws RemoteException {
        if (Host.getInstance().getPowerMode()==Host.LOW_POWER) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]Current power mode is low power. return MAIN_MENU_CLOSE_ALL");
            }
            return MAIN_MENU_CLOSE_ALL;
        }
        
        int targetMainMenuState = MainMenuService.MAIN_MENU_CLOSE_ALL;
        if (isBootingTime) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]isActiveManagementSystemDisplayConfig is true.");
            }
            isBootingTime = false;
            boolean isActivatedMenu = PreferenceProxy.getInstance().isActivatedMenu();
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]isActivatedMenu : "+isActivatedMenu);
            }
            if (isActivatedMenu) {
                return MAIN_MENU_STATE_MINI_MENU;
            }
            return MAIN_MENU_CLOSE_ALL;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]isBootingTime is false.");
        }
        int mainMenuState = MainController.getInstance().getMainMenuState();
        switch (mainMenuState) {
            case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
                if (Log.DEBUG_ON) {
                    Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]Main menu state is mini menu.");
                }
                targetMainMenuState = MainMenuService.MAIN_MENU_CLOSE_ALL;
                break;
            case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
            case MainMenuService.MAIN_MENU_CLOSE_ALL:
                if (Log.DEBUG_ON) {
                    Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]Main menu state is dash board or closed.");
                }
                boolean isActivatedMiniMenu = PreferenceProxy.getInstance().isActivatedMiniMenu();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]isActivatedMiniMenu : "+isActivatedMiniMenu);
                }
                if (isActivatedMiniMenu) {
                    return MainMenuService.MAIN_MENU_STATE_MINI_MENU;
                }
                return MainMenuService.MAIN_MENU_STATE_MINI_MENU;
            default:
                break;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainMenuServiceImpl.getTargetMenuState]targetMainMenuState : "+targetMainMenuState);
        }
        return targetMainMenuState;
    }

    /**
     * Called when the power mode changes (for example from full to low power).
     *
     * @param reqPowerMode
     *            Host.FULL_POWER, Host.LOW_POWER
     */
    public void powerModeChanged(int reqPowerMode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("MainMenuServiceImpl.powerModeChanged: reqPowerMode-" + reqPowerMode);
        }
        if (reqPowerMode == Host.LOW_POWER) {
            isBootingTime = true;
        }
    }

    public void setDeactivatedSTB(boolean isDeactivatedSTB) throws RemoteException {
        this.isDeactivatedSTB = isDeactivatedSTB;
    }

    public boolean isDeactivatedSTB() {
        return isDeactivatedSTB;
    }
}

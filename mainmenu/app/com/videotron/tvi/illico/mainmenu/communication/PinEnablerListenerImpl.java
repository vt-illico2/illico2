package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.upp.PinEnablerListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.mainmenu.controller.MainController;

public class PinEnablerListenerImpl implements PinEnablerListener {
    private String targetAppName;
    private String[] targetAppParams;
    
    public PinEnablerListenerImpl(String targetAppName, String[] targetAppParams) {
        this.targetAppName = targetAppName;
        this.targetAppParams = targetAppParams;
    }
    public void receivePinEnablerResult(int response, String detail) throws RemoteException {
        if (response == PreferenceService.RESPONSE_SUCCESS) {
            MainController.getInstance().resetInvisibleTimer(true);
            //Specifical case - Settings
            if (MainController.getInstance().checkSettingsRelated(targetAppName, targetAppParams)) {
                return;
            }
            //Check whether Search app or not
            if (MainController.getInstance().checkSearchRalated(targetAppName)) {
                return;
            }
            CommunicationManager.getInstance().requestStartUnboundApplication(targetAppName, targetAppParams);
        }
    }
}

package com.videotron.tvi.illico.mainmenu.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.log.Log;

public class LogLevelChangeListenerImpl implements LogLevelChangeListener {
    public void logLevelChanged(int logLevel) throws RemoteException {
        Log.setStcLevel(logLevel);
    }
}

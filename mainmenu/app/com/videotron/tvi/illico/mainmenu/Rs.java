package com.videotron.tvi.illico.mainmenu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

import org.dvb.ui.DVBColor;
import org.havi.ui.event.HRcEvent;
import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This class is the Resource class of main menu.
 * @version $Revision: 1.112.2.1 $ $Date: 2017/03/30 15:43:38 $
 * @author Sangjoon Kwon
 */
public class Rs {
    /*********************************************************************************
     * Application Test-related
     *********************************************************************************/
    /** Application Version. */ 
    public static final String APP_VERSION = "(R7.5).0.2";
    /** Application Name. */
    public static final String APP_NAME = "Menu";
    /** This constant is flag whether emulator test or not. */
    public static final boolean IS_EMULATOR = Environment.EMULATOR;
    public static final boolean USE_LOCAL_SERVER_FILE = IS_EMULATOR && true;
    public static final boolean INSTEAD_OF_IB_OOB = IS_EMULATOR && true;
    public static final boolean USE_PROP_DATA_INSTEAD_OF_IB_OOB = true; //MS_DATA or USER_PROP_DATA
    
    /*********************************************************************************
     * Font-related
     **********************************************************************************/
    /** The Constant Font Size 13. */
    public static final Font BLENDER13 = FontResource.BLENDER.getFont(13, true);
    /** The Constant Font Size 15. */
    public static final Font BLENDER15 = FontResource.BLENDER.getFont(15, true);
    /** The Constant Font Size 16. */
    public static final Font BLENDER16 = FontResource.BLENDER.getFont(16, true);
    /** The Constant Font Size 17. */
    public static final Font BLENDER17 = FontResource.BLENDER.getFont(17, true);
    /** The Constant Font Size 18. */
    public static final Font BLENDER18 = FontResource.BLENDER.getFont(18, true);
    /** The Constant Font Size 20. */
    public static final Font BLENDER20 = FontResource.BLENDER.getFont(20, true);
    /** The Constant Font Size 21. */
    public static final Font BLENDER21 = FontResource.BLENDER.getFont(21, true);
    /** The Constant Font Size 23. */
    public static final Font BLENDER23 = FontResource.BLENDER.getFont(23, true);
    /** The Constant Font Size 32. */
    public static final Font BLENDER32 = FontResource.BLENDER.getFont(32, true);
    /** The Constant FontMetrics Size 16. */
    public static final FontMetrics FM16 = FontResource.getFontMetrics(BLENDER16);
    /** The Constant FontMetrics Size 17. */
    public static final FontMetrics FM17 = FontResource.getFontMetrics(BLENDER17);
    /** The Constant FontMetrics Size 18. */
    public static final FontMetrics FM18 = FontResource.getFontMetrics(BLENDER18);
    /** The Constant FontMetrics Size 21. */
    public static final FontMetrics FM21 = FontResource.getFontMetrics(BLENDER21);
    /** The Constant FontMetrics Size 23. */
    public static final FontMetrics FM23 = FontResource.getFontMetrics(BLENDER23);
    /*********************************************************************************
     * Color-related
     *********************************************************************************/
    /** The Constant Color 003 003 003. */
    public static final Color C003003003 = new Color(3, 3, 3);
    /** The Constant Color 046 046 045. */
    public static final Color C046046045 = new Color(46, 46, 45);
    public static final Color C050050050 = new Color(50, 50, 50);
    public static final Color C086086086 = new Color(86, 86, 86);
    /** The Constant Color 124 124 124. */
    public static final Color C110110110 = new Color(110, 110, 110);
    public static final Color C124124124 = new Color(124, 124, 124);
    /** The Constant Color 199 159 052. */
    public static final Color C199159052 = new Color(199, 159, 52);
    /** The Constant Color 214 182 061. */
    public static final Color C214182061 = new Color(214, 182, 61);
    public static final Color C251217089 = new Color(251, 217, 89);
    /** The Constant Color 219 219 219. */
    public static final Color C219219219 = new Color(219, 219, 219);
    /** The Constant Color 202 174 097. */
    public static final Color C202174097 = new Color(202, 174, 97);
    /** The Constant Color  230 230 230. */
    public static final Color C230230230 = new Color(230, 230, 230);
    /** The Constant Color 236 211 143. */
    public static final Color C236211143 = new Color(236, 211, 143);
    /** The Constant Color 241 241 241. */
    public static final Color C241241241 = new Color(241, 241, 241);
    /** The Constant Color 255 255 255. */
    public static final Color C255255255 = new Color(255, 255, 255);
    /** The Constant DVB Color 012 012 012 204. */
    public static final  Color DVB012012012204 = new DVBColor(12, 12, 12, 204);
    /*********************************************************************************
     * Key Code-related
     *********************************************************************************/
    /** The Constant KEY_OK. */
    public static final int KEY_OK = HRcEvent.VK_ENTER;
    /** The Constant KEY_EXIT. */
    public static final int KEY_EXIT = OCRcEvent.VK_EXIT;
    /** The Constant KEY_SETTINGS. */
    public static final int KEY_SETTINGS = OCRcEvent.VK_SETTINGS;
    /** The Constant KEY_UP. */
    public static final int KEY_UP = HRcEvent.VK_UP;
    /** The Constant KEY_DOWN. */
    public static final int KEY_DOWN = HRcEvent.VK_DOWN;
    /** The Constant KEY_LEFT. */
    public static final int KEY_LEFT = HRcEvent.VK_LEFT;
    /** The Constant KEY_RIGHT. */
    public static final int KEY_RIGHT = HRcEvent.VK_RIGHT;
    /*********************************************************************************
     * Data Center Key-related
     *********************************************************************************/
    /** Data Center Key - Mini Menu Fixed Item Data. */
    public static final String DCKEY_MINI_MENU_FIXED_ITEM = "DCKEY_MINI_MENU_FIXED_ITEM";
    /** Data Center Key - Full Menu Promotion Data. */
    public static final String DCKEY_FULL_MENU_PROMOTION = "DCKEY_FULL_MENU_PROMOTION";
    /** Data Center Key - Display Menu. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_MENU = "DCKEY_MENU_CONFIG_DISPLAY_MENU";
    /** Data Center Key - dashboard display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_DASHBOARD = "DCKEY_MENU_CONFIG_DISPLAY_SEC_DASHBOARD";
    /** Data Center Key - full menu display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_FULL_MENU = "DCKEY_MENU_CONFIG_DISPLAY_SEC_FULL_MENU";
    /** Data Center Key - mini menu display sec. */
    public static final String DCKEY_MENU_CONFIG_DISPLAY_SEC_MINI_MENU = "DCKEY_MENU_CONFIG_DISPLAY_SEC_MINI_MENU";
    /** Data Center Key - Fixed Item Data. */
    public static final String DCKEY_FILE_MINI_MENU_IMAGES = "tempData/mini_menu_images.zip";
    /*********************************************************************************
     * Look up Milis-related
     *********************************************************************************/
    /** Retry Look up Millis - User Profile and Preference. */
    public static final int RETRY_LOOKUP_MILLIS_UPP = 1000;

    /*********************************************************************************
     * Screen size-related
     *********************************************************************************/
    /** Screen size - Width. */
    public static final int SCREEN_SIZE_WIDTH = 960; 
    /** Screen size - Height. */
    public static final int SCREEN_SIZE_HEIGHT = 540;

    /*********************************************************************************
     * etc Constant-related
     *********************************************************************************/
    /** The Constant MILLIS_TO_SECOND. */
    public static final int MILLIS_TO_SECOND = 1000;
    /** The Constant Default Separator. */
    public static final String DEFAULT_SEPARATOR = "_";
    
    public static final String APP_NAME_SEARCH = "Search";
    /*********************************************************************************
     * Error code-related
     **********************************************************************************/
//    public static final String ERROR_CODE_INVALID_MINI_MENU = "MEN500";
//    public static final String ERROR_CODE_INVALID_FULL_MENU = "MEN501";
//    public static final String ERROR_CODE_NOT_READY_MENU_DATA = "MEN502";
//    public static final String ERROR_CODE_NO_AUTHORIZED_APP = "MEN503";
    public static final String ERROR_CODE_CONFIG_RECEIPT_PROBLEM = "MEN500";
    public static final String ERROR_CODE_CONFIG_PARSING_PROBLEM = "MEN501";
}

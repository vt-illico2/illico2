package com.videotron.tvi.illico.mainmenu.ui;

import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;

/**
 * This class is super class about FullMenu and MiniMenu and DashBoardMenu.
 *
 * UIComponent did not have start(boolean isResetData) and keyAction(int keyCode).
 * so Menu class extend UICompoent class for no exist methods.
 * @author Sangjoon Kwon
 */
public abstract class MenuUI extends UIComponent {
    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    public abstract void dispose();
    /**
     * Start Menu.
     * @param isResetData reset data or not when component start
     */
    public abstract void start(boolean isResetData);

    protected boolean isBlockedBySpecialVideoState(String reqAppName) {
//    	return true;
    	return (reqAppName.equalsIgnoreCase(Rs.APP_NAME_SEARCH) && CommunicationManager.getInstance().requestIsSpecialVideoState());
    }
}

/*
 *  NewMenuUI.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.ui;

import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Date;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.havi.ui.event.HRcEvent;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.framework.effect.MovingEffect;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;
import com.videotron.tvi.illico.mainmenu.comp.Arrows;
import com.videotron.tvi.illico.mainmenu.comp.Background;
import com.videotron.tvi.illico.mainmenu.comp.Description;
import com.videotron.tvi.illico.mainmenu.comp.FirstMenu;
import com.videotron.tvi.illico.mainmenu.comp.FirstMenu.MenuEffect;
import com.videotron.tvi.illico.mainmenu.comp.SecondMenu;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;
import com.videotron.tvi.illico.mainmenu.controller.MainController;
import com.videotron.tvi.illico.mainmenu.controller.MainMenuVbmController;
import com.videotron.tvi.illico.mainmenu.controller.NewDataManager;
import com.videotron.tvi.illico.mainmenu.gui.RendererNewMenu;
import com.videotron.tvi.illico.ui.Footer;
import com.videotron.tvi.illico.util.Clock;
import com.videotron.tvi.illico.util.ClockListener;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.KeyCodes;

/**
 * <code>NewMenuUI</code>
 * 
 * @since 2015. 6. 17.
 */
public class NewMenuUI extends MenuUI implements ClockListener, TVTimerWentOffListener {
    public static final int LEN = 6; // number of menus, it's FIXED!
    public static final int FOC_MAIN = -1; // focus on 1st menu
    public static final int FOC_SEARCH = -2; // focus on search button
    public static final int BASE_X = 51; // base coords from the GUI doc
    
    public static String[][] MENU = new String[LEN][]; // menu labels, TODO integrate with MS
//    static {
//        MENU[0] = new String[] { "MY ILLICO" };
//        MENU[1] = new String[] { "RECORDINGS" };
//        MENU[2] = new String[] { "CHANNELS", "ON DEMAND" };
//        MENU[3] = new String[] { "CLUB ILLICO" };
//        MENU[4] = new String[] { "MOVIES AND SERIES", "ON DEMAND" };
//        MENU[5] = new String[] { "LIVE TV", "AND GUIDE" };
//    }

    public static String[][][] SUB = new String[LEN][][]; // menu labels, TODO integrate with MS
//    static {
//        SUB[0] = new String[][] { 
//            new String[] { "My Wishlist" }, 
//            new String[] { "My Resume viewing" },
//            new String[] { "My favorite", "channels" }, 
//            new String[] { "Settings", "and Apps" },
//            new String[] { "What's new ?" }, };
//        SUB[1] = new String[][] { 
//            new String[] { "My recordings" }, 
//            new String[] { "Scheduled", "Recordings" },
//            new String[] { "Manual Recording" }, 
//            new String[] {"More info..."}, };
//        SUB[2] = new String[][] { 
//            new String[] { "All channels" }, };
//        SUB[3] = new String[][] { 
//            new String[] { "New arrivals" }, 
//            new String[] { "All titles" }, };
//        SUB[4] = new String[][] { 
//            new String[] { "Most popular" }, 
//            new String[] { "All titles" }, };
//        SUB[5] = new String[][] { 
//            new String[] { "Package", "channels" }, 
//            new String[] { "All channels" },
//            new String[] { "Free Previews" }, 
//            new String[] { "Change", "your channels" }, };
//    }

//    public static final String[][] APP = new String[LEN][]; // app names for 2nd menus, TODO integrate with MS
//    static {
//        APP[0] = new String[] { "VOD", "VOD", "EPG", "Dashboard", "Help" };
//        APP[1] = new String[] { "PVR", "PVR", "PVR", "PVR" };
//        APP[2] = new String[] { "VOD" };
//        APP[3] = new String[] { "VOD", "VOD" };
//        APP[4] = new String[] { "VOD", "VOD" };
//        APP[5] = new String[] { "EPG", "EPG", "Preview", "Options" };
//    }
    
    /*
    public static final String[][][] PARAM = new String[LEN][][]; // parameters for 2nd menus, TODO integrate with MS
    static {
        // my illico
        PARAM[0] = new String[][] {
            new String[] { "Menu", "WISHLIST" },
            new String[] { "Menu", "RESUMEVIEWING" },
        };
        
        // recordings
        PARAM[1] = new String[][] {};
        
        // channels on demand
        PARAM[2] = new String[][] {
            new String[] { "Menu", "TOPIC", "Environnement des Chaines", "Videotron" },
        };
        
        // club illico
        PARAM[3] = new String[][] {
            new String[] { "Menu", "TOPIC", "ClubNouvelArrivage", "VSD" },
            new String[] { "Menu", "TOPIC", "ClubIllico", "VSD" },
        };
        
        // movies and series
        PARAM[4] = new String[][] {
            new String[] { "Menu", "TOPIC", "AccueilPlusPopulaire", "VSD" },
            new String[] { "Menu" },
        };
        
        // live tv
        PARAM[5] = new String[][] {};
    }
    */

    private int offset = 0; // main menu's offset - how far from 0-th menu MY ILLICO

    public int getOffset() { // return the offset value to draw in GUI class
        return offset;
    }

    private Background bg; // background component, because of its animation
    private AlphaEffect bgIn; // animation effect for background
    private FirstMenu menu; // main menu component, includes own animation effect for left/right
    private AlphaEffect menuIn; // animation effect for main menu appearing
    private SecondMenu second; // 2nd menu component
    private MovingEffect movDown, movUp; // animation effect for 2nd menu
    private Arrows arrows; // 4-way arrows component
    private AlphaEffect arrowsIn; // animation effect for arrows
    private TVTimerSpec secondTimer, arrowsTimer; // timer specs for appearing
    
    private final int DELAY_SECOND = 100; // delay for second menu appearing
    private final int DELAY_ARROWS = DELAY_SECOND + 3000; // delay for arrows after the main menu animation
    private final int DELAY_ARROWS_INIT = DELAY_SECOND + 1500; // delay for arrows but after the menu app launching
    private final int DELAY_CANCEL = 0; // to cancel TVTimer
    
    private Footer footer = new Footer(Footer.ALIGN_RIGHT, true, Footer.STATIC_KEY);
    
    private Description desc = new Description();
    
    private boolean isStarted;
    
    public NewMenuUI() {
        renderer = new RendererNewMenu();
        setRenderer(renderer);
    }

    public void start(boolean isResetData) {
        if (Log.DEBUG_ON) {
            Log.printDebug("NewMenuUI.start(), isResetData = " + isResetData);
        }
        isStarted = true;
        prepare();
        Clock.getInstance().addClockListener(this);
        
        int lang = FrameworkMain.getInstance().getCurrentLanguage() == Constants.LANGUAGE_ENGLISH ? 0 : 1;
        NewDataManager.getInstance().readyMainMenu();
        MENU = NewDataManager.getInstance().getMainMenuLabels(lang, FirstMenu.W - 15);
        SUB = NewDataManager.getInstance().getSubMenuLabels(lang, SecondMenu.W - 15);

        Image menuImg = DataManager.getInstance().getFooterImage(PreferenceService.BTN_MENU);
        
        footer.setFont(FontResource.BLENDER.getBoldFont(17));
        footer.reset();
        footer.addButton(menuImg, "TxtMiniMenu.closeMenu");
        add(footer);
        
        // TODO is it good that creating components here? think about DATA ...
        
        menu = new FirstMenu();
        menu.setBounds(BASE_X, 333, (FirstMenu.W + FirstMenu.GAP) * LEN, 135 + SecondMenu.H);
        offset = 0; // set 0 to draw from 0-th menu to the buffer
        menu.readyBuffer(this); // draw in here
        menu.setVisible(false);
        this.add(menu);
        offset = 1; // and then set default focus
        
        second = new SecondMenu();
        second.prepare(BASE_X + FirstMenu.W + FirstMenu.GAP + (FirstMenu.W - SecondMenu.W) / 2 - SecondMenu.X_MARGIN, 
            menu.getY() + FirstMenu.Y2 + 1);
        second.setFocus(focus = FOC_MAIN); // focus is on the main
        second.setMenuUI(this);
        second.setVisible(false); // not visible at first
        this.add(second);

        secondTimer = new TVTimerSpec();
        secondTimer.addTVTimerWentOffListener(this);
//        secondTimer(DELAY_SECOND);

        arrows = new Arrows();
        arrows.setMenuUI(this);
        int arrowsSize = 15;
        arrows.setBounds(BASE_X + FirstMenu.W + FirstMenu.GAP - arrowsSize, menu.getY() + FirstMenu.Y2 - arrowsSize, 
            FirstMenu.W + arrowsSize * 2, FirstMenu.H2 + arrowsSize * 2);
        arrows.setVisible(false); // not visible at first
        this.add(arrows, 0);
        arrowsIn = new AlphaEffect(arrows, 10, AlphaEffect.FADE_IN);
        arrowsIn.setRequestor(this);
        arrowsTimer = new TVTimerSpec();
        arrowsTimer.addTVTimerWentOffListener(this);
//        arrowsTimer(DELAY_ARROWS_INIT);
        
        this.add(desc);

        bg = new Background();
        bg.setBounds(0, 0, 960, 540);
        bg.setVisible(false);
        this.add(bg);
        
        FrameworkMain.getInstance().getImagePool().waitForAll();

        menuIn = new AlphaEffect(menu, 8, AlphaEffect.FADE_IN);
        menuIn.setRequestor(this);

        bgIn = new AlphaEffect(bg, 8, AlphaEffect.FADE_IN);
        bgIn.setRequestor(this);
        bgIn.startLater();
//        menuIn.startLater();
        
        setVisible(true);
    }

    private void secondTimer(int delay) {
        TVTimer.getTimer().deschedule(secondTimer);
        if (delay == DELAY_CANCEL) {
            return;
        }
        secondTimer.setDelayTime(delay);
        try {
            secondTimer = TVTimer.getTimer().scheduleTimerSpec(secondTimer);
        } catch (TVTimerScheduleFailedException e) {
            e.printStackTrace();
        }
    }
    private void arrowsTimer(int delay) {
        TVTimer.getTimer().deschedule(arrowsTimer);
        if (delay == DELAY_CANCEL) {
            return;
        }
        arrowsTimer.setDelayTime(delay);
        try {
            arrowsTimer = TVTimer.getTimer().scheduleTimerSpec(arrowsTimer);
        } catch (TVTimerScheduleFailedException e) {
            e.printStackTrace();
        }
    }

    public void dispose() {
        if (Log.DEBUG_ON) {
            Log.printDebug("NewMenuUI.dispose()");
        }
        stop();
    }

    public void stop() {
        if (Log.DEBUG_ON) {
            Log.printDebug("NewMenuUI.stop()");
        }
        isStarted = false;
        Clock.getInstance().removeClockListener(this);
        removeAll();

        if (menu != null) {
            menu.dispose(); // to release the buffer
            menu = null;
        }
        second = null;
        arrows = null;
        bg = null;

        secondTimer.removeTVTimerWentOffListener(this);
        secondTimer(DELAY_CANCEL);
        arrowsTimer.removeTVTimerWentOffListener(this);
        arrowsTimer(DELAY_CANCEL);

        if (desc != null) desc.setDesc(null);
    }

    public boolean handleKey(int keyCode) {
        if (Log.DEBUG_ON) {
            Log.printDebug("NewMenuUI.handKey(), key = " + keyCode);
        }
        if (Log.DEBUG_ON) Log.printDebug("MainController.resetInvisibleTimer()");
        MainController.getInstance().resetInvisibleTimer(true);
        switch (keyCode) {
            case OCRcEvent.VK_MENU :
            	if (!MainController.getInstance().isSearchVisible() 
            			&& !MainController.getInstance().isListBehaviorVisible()) {
            		if (Log.DEBUG_ON) Log.printDebug("MainController.handleKey(), footer animation");
            		footer.clickAnimation(0);
            	}
                //CommunicationManager.getInstance().requestExitApplication();
                MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, true, true);
                return true;
            case KeyCodes.COLOR_A:
            	MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_STATE_DASHBOARD, true, true);
            	return true;
            case KeyEvent.VK_LEFT :
                focus = FOC_MAIN;
                updateDescription();
                second.setVisible(false); // no ani
                secondTimer(DELAY_CANCEL);
                arrows.setVisible(false);
                arrowsTimer(DELAY_CANCEL);
                menu.leftRight(true, (offset + 1) % LEN);
                return true;
            case KeyEvent.VK_RIGHT :
                focus = FOC_MAIN;
                updateDescription();
                second.setVisible(false); // no ani
                secondTimer(DELAY_CANCEL);
                arrows.setVisible(false);
                arrowsTimer(DELAY_CANCEL);
                menu.leftRight(false, (offset - 1 + LEN) % LEN);
                return true;
            case KeyEvent.VK_UP :
                arrows.setVisible(false);
                arrowsTimer(DELAY_CANCEL);
                switch (focus) {
                    case FOC_SEARCH: // search to main
                        focus = FOC_MAIN;
                        menu.setFocused(true);
                        repaint();
                        secondTimer(DELAY_CANCEL);
                        timerWentOff(null); // to slide up the second menu
                        arrowsTimer(DELAY_ARROWS);
                        return true;
                    case FOC_MAIN: // main to second
                        menu.setFocused(false);
                        focus = 0;
                        break;
                    default: // move in second
                        int idx = (LEN - offset + 1) % LEN;
                        focus = Math.min(focus + 1, SUB[idx].length - 1);
                        break;
                }
                second.setFocus(focus);
                if (focus == 0 && second.isVisible() == false) { // need to appear now
                    secondTimer(DELAY_CANCEL); // de-schedule
                    timerWentOff(null); // to show the second
                }
                updateDescription();
                return true;
            case KeyEvent.VK_DOWN :
                arrows.setVisible(false);
                arrowsTimer(DELAY_CANCEL);
                switch (focus) {
                    case FOC_MAIN: // main to search
                        focus = FOC_SEARCH;
                        second.setVisible(false);
                        menu.setFocused(false);
                        repaint();
                        return true;
                    case FOC_SEARCH:
                    	// bottom just consumed key.
                    	return true;
                    case 0: // second to main
                        menu.setFocused(true);
                        focus = FOC_MAIN;
                        arrowsTimer(DELAY_ARROWS);
                        break;
                    default: // move in second
                        focus--;
                        break;
                }
                second.setFocus(focus);
                updateDescription();
                return true;
                
            case KeyEvent.VK_ENTER:
                switch (focus) {
                    case FOC_SEARCH:
                        launchSearch(true);
                        break;
                    case FOC_MAIN:
                        launchOnMenu();
                        break;
                    default:
                        launchOnSecond();
                        break;
                }
                return true;
            case HRcEvent.VK_INFO:
            	if (CommunicationManager.getInstance().requestIsTvViewingState()) {
            		String vCtxName = CommunicationManager.getInstance().requestGetVideoContextName();     		
            		if (vCtxName != null && !vCtxName.equalsIgnoreCase(MonitorService.DEFAULT_VIDEO_NAME)) {
            			MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false, true);
            		}
            	}
            	return false;
            case KeyCodes.COLOR_D:
            case KeyCodes.LITTLE_BOY:
            case OCRcEvent.VK_PAGE_UP: // VDTRMASTER-5568
            case OCRcEvent.VK_PAGE_DOWN: // VDTRMASTER-5568
            	return true;
            case KeyCodes.SEARCH:
            	launchSearch(true);
            	return true;
			case KeyCodes.ASPECT: // R7.2
				return true;
        }
        return false;
    }
    
    private void launchOnSecond() {
        int idx = (LEN - offset + 1) % LEN;
        String app = NewDataManager.getInstance().getSubMenuAppName(idx, focus);
        String[] param = null;
        
        // R7
        String menuId = NewDataManager.getInstance().getSubMenuId(idx, focus);
        MainMenuVbmController.getInstance().writeMenuItemSelection(menuId);
        
        if ("VOD".equals(app)) { // only for phase1 that uses fixed menu data
            JSONObject vodParam = NewDataManager.getInstance().readyVodParam();
            Object p = null;
            switch (idx) {
                case 0: // my illico
                    switch (focus) {
                        case 0: p = vodParam.get("myWishlist"); break;
                        case 1: p = vodParam.get("resumeViewing"); break;
                    }
                    break;
                case 2: // channel on demand
                	switch (focus) {
                    case 0: p = ((JSONObject) vodParam.get("moviesAndSeries")).get("mostPopular"); break;
                    case 1: p = ((JSONObject) vodParam.get("moviesAndSeries")).get("allTitles"); break;
                }
                    break;
                case 3: // club illico
                    switch (focus) {
                        case 0: p = ((JSONObject) vodParam.get("clubIllico")).get("newArrivals"); break;
                        case 1: p = ((JSONObject) vodParam.get("clubIllico")).get("allTitles"); break;
                    }
                    break;
                case 4: // movies
                    p = ((JSONObject) vodParam.get("channelOnDemand")).get("allChannels");
                    break;
            }   
            if (p instanceof JSONArray) {
                JSONArray array = (JSONArray) p;
                param = new String[array.size()];
                for (int i = 0; i < param.length; i++) {
                    param[i] = array.get(i).toString();
                }
            }
        } else if ("PVR".equals(app)) {
        	// VDTRMASTER-5730
        	boolean isMultiroomEnabled = false;

    		try {
    			isMultiroomEnabled = CommunicationManager.getInstance().getMonitorService().isMultiroomEnabled();
    		} catch (RemoteException e) {
    			e.printStackTrace();
    		}
    		
    		if (Environment.SUPPORT_DVR || isMultiroomEnabled) {
    			param = NewDataManager.getInstance().getBuildSubMenuParams(idx, focus);
    		} else {
    			MonitorService mService = CommunicationManager.getInstance().getMonitorService();
        		
        		try {
					mService.showListPressedInNonPvrPopup();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
        		return;
    		}
//        	if (focus < 3) {
//        		param = NewDataManager.getInstance().getBuildSubMenuParams(idx, focus);
//        	} else if (focus == 3) {
//        		MonitorService mService = CommunicationManager.getInstance().getMonitorService();
//        		
//        		try {
//					mService.showListPressedInNonPvrPopup();
//				} catch (RemoteException e) {
//					e.printStackTrace();
//				}
//        		return;
//        	}
        } else if ("EPG".equals(app)) {
        	param = NewDataManager.getInstance().getBuildSubMenuParams(idx, focus);
        } else if ("Dashboard".equals(app)) {
        	MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_STATE_DASHBOARD, true, false);
            return;
        } else {
            param = NewDataManager.getInstance().getBuildSubMenuParams(idx, focus);
        }
        MainController.getInstance().startApplication(app, param, null);
        //CommunicationManager.getInstance().requestStartUnboundApplication(app, param);
    }
    
    private void launchOnMenu() {
    	
    	String[] param = null;
    	JSONObject vodParam = NewDataManager.getInstance().readyVodParam();
    	Object p = null;
    	
        int idx = (LEN - offset + 1) % LEN;
        String appName = NewDataManager.getInstance().getMainMenuApp(idx);
        
     	// R7
        String menuId = NewDataManager.getInstance().getMainMenuId(idx);
        MainMenuVbmController.getInstance().writeMenuItemSelection(menuId);
        
        switch (idx) {
            case 0: // my illico
            	String[] params = NewDataManager.getInstance().getMainMenuParams(idx);
                CommunicationManager.getInstance().requestStartUnboundApplication(appName, params);
                break;
            case 1: // recordings
                // pvr
            	
            	// VDTRMASTER-5730
            	boolean isMultiroomEnabled = false;

        		try {
        			isMultiroomEnabled = CommunicationManager.getInstance().getMonitorService().isMultiroomEnabled();
        		} catch (RemoteException e) {
        			e.printStackTrace();
        		}
        		
        		if (Log.DEBUG_ON) {
        			Log.printDebug("NewMenuUI, launchOnMenu, isMultiroomEnabled=" + isMultiroomEnabled 
        					+ ", SUPPORT_DVR=" + Environment.SUPPORT_DVR);
        		}
            	
            	if (Environment.SUPPORT_DVR) {
            		params = NewDataManager.getInstance().getMainMenuParams(idx);
                    CommunicationManager.getInstance().requestStartUnboundApplication(appName, params);
            	} else if (isMultiroomEnabled) {
            		params = NewDataManager.getInstance().getBuildSubMenuParams(idx, 0);
                    CommunicationManager.getInstance().requestStartUnboundApplication(appName, params);
            	} else {
            		MonitorService mService = CommunicationManager.getInstance().getMonitorService();
            		
            		try {
    					mService.showListPressedInNonPvrPopup();
    				} catch (RemoteException e) {
    					e.printStackTrace();
    				}
            		return;
            	}
            	
                break;
            case 2: // movies
            	// vod
            	p = ((JSONObject) vodParam.get("moviesAndSeries")).get("rootParams");
            	if (p instanceof JSONArray) {
                    JSONArray array = (JSONArray) p;
                    param = new String[array.size()];
                    for (int i = 0; i < param.length; i++) {
                        param[i] = array.get(i).toString();
                    }
                }
                CommunicationManager.getInstance().requestStartUnboundApplication(appName, param);
                break;
            case 3: // club illico
                // vod
            	p = ((JSONObject) vodParam.get("clubIllico")).get("rootParams");
            	if (p instanceof JSONArray) {
                    JSONArray array = (JSONArray) p;
                    param = new String[array.size()];
                    for (int i = 0; i < param.length; i++) {
                        param[i] = array.get(i).toString();
                    }
                }
                CommunicationManager.getInstance().requestStartUnboundApplication(appName, param);
                break;
            case 4: // channels on demand
                // vod
            	p = ((JSONObject) vodParam.get("channelOnDemand")).get("rootParams");
            	if (p instanceof JSONArray) {
                    JSONArray array = (JSONArray) p;
                    param = new String[array.size()];
                    for (int i = 0; i < param.length; i++) {
                        param[i] = array.get(i).toString();
                    }
                }
                CommunicationManager.getInstance().requestStartUnboundApplication(appName, param);
                break;
            case 5: // live tv
            	params = NewDataManager.getInstance().getMainMenuParams(idx);
            	CommunicationManager.getInstance().requestStartUnboundApplication(appName, params);
                break;
        }
    }
    
    private void launchSearch(boolean reset) {
//        try {
//            SearchService ss = CommunicationManager.getInstance().getSearchService();
//            if (ss != null) {
//                ss.launchSearchApplication(SearchService.PLATFORM_EPG, reset);
//            }
//        } catch (Exception ex) {
//            Log.print(ex);
//        }
    	CommunicationManager.getInstance().requestShowSearchService();
    }
    
    private void updateDescription() {
    	if (focus > -1) {
	    	int idx = (LEN - offset + 1) % LEN;
	        desc.setBounds(331, menu.getY() + FirstMenu.Y2 - 42 - focus * 43+2, 374, 20);
	        desc.setDesc(NewDataManager.getInstance().getBuildSubMenuDesc(idx, focus));
    	} else {
    		desc.setDesc(null);
    	}
    }
    
    public boolean isStarted() {
    	return isStarted;
    }
    
    public void animationStarted(Effect src) {
    	if (MainController.getInstance().isSearchVisible() 
    			|| MainController.getInstance().isListBehaviorVisible()) {
    		
    		resetAnimation();
    		if (Log.DEBUG_ON) {
    			Log.printDebug("NewMenuUI, animationStarted, cancel animation.");
    		}
    		return;
    	}
    	
        if (src instanceof MenuEffect && menu != null) {
            menu.setInAni(true);
        }
    }

    public void animationEnded(Effect src) {
    	
    	if (MainController.getInstance().isSearchVisible() 
    			|| MainController.getInstance().isListBehaviorVisible()) {
    		
    		resetAnimation();
    		if (Log.DEBUG_ON) {
    			Log.printDebug("NewMenuUI, animationEnded, cancel animation.");
    		}
    		return;
    	}
    	
//        Point from, to;
//        int frame, div = 20;
        if (src instanceof MenuEffect && menu != null) {
            offset = ((MenuEffect) src).getNextOffset();
            menu.setFocused(true);
            menu.setInAni(false);
            second.setFocus(focus = -1);

//            if (second.isVisible()) {
//                second.setVisible(false);
//                from = new Point(0, 0);
//                to = new Point(0, second.getHeight());
//                frame = to.y / div;
//                movDown = new MovingEffect(second, frame, from, to, MovingEffect.DO_NOT_FADE, MovingEffect.ANTI_GRAVITY);
//                movDown.updateBackgroundBeforeStart(false);
//                movDown.setClipBounds(0, 0, second.getWidth(), second.getHeight());
//                movDown.setRequestor(this);
//                movDown.startLater();
//            }
            secondTimer(DELAY_SECOND);
            arrowsTimer(DELAY_ARROWS);

        } else if (src == movDown && second != null) {
            second.setVisible(false);
        } else if (src == movUp && second != null) {
            second.setVisible(true);
        } else if (src == arrowsIn && arrows != null) {
            arrows.setVisible(true);
        } else if (src == menuIn && menu != null) {
            menu.setVisible(true);
            secondTimer(DELAY_SECOND);
            arrowsTimer(DELAY_ARROWS_INIT);
        } else if (src == bgIn && bg != null) {
            bg.setVisible(true);
            menuIn.startLater();
        }
    }
    
    private void resetAnimation() {
    	secondTimer(DELAY_CANCEL);
        arrowsTimer(DELAY_CANCEL);
        bg.setVisible(true);
        menu.setVisible(true);
        second.updateBounds();
        second.setVisible(true);
        arrows.setVisible(true);
    }
    
    public void clockUpdated(Date date) {
        repaint();
    }

    public void timerWentOff(TVTimerWentOffEvent evt) {
    	
    	if (MainController.getInstance().isSearchVisible() 
    			|| MainController.getInstance().isListBehaviorVisible()) {
    		
    		resetAnimation();
    		if (Log.DEBUG_ON) {
    			Log.printDebug("NewMenuUI, timerWentOff, cancel animation.");
    		}
    		return;
    	}
    	
        if ((evt == null || evt.getTimerSpec().equals(secondTimer)) && second != null && focus != FOC_SEARCH) {
            if (Log.DEBUG_ON) Log.printDebug("NewMenuUI.timerWentOff() --- 1");
            Point from, to;
            int frame, div = 20;

            second.updateBounds();
            from = new Point(0, second.getHeight());
            to = new Point(0, 0);
            frame = from.y / div;
            movUp = new MovingEffect(second, frame, from, to, MovingEffect.DO_NOT_FADE, MovingEffect.ANTI_GRAVITY);
            movUp.updateBackgroundBeforeStart(false); // to fast start, but if there's some side-effect, needs turn on
            movUp.setClipBounds(0, 0, second.getWidth(), second.getHeight()-1);//-1 을 해서 main menu 와 겹치는 부분 없앰
            //movUp.setClipBounds(0, 0, second.getWidth(), second.getHeight());
            movUp.setRequestor(this);
            movUp.startLater();
            /* 아래 중복된 조건문 지움. 이것 때문에 movUp 이 두번 발생
        } else if (evt.getTimerSpec().equals(arrowsTimer) && arrows != null) {
            if (Log.DEBUG_ON) Log.printDebug("NewMenuUI.timerWentOff() --- 2");
            if (focus == FOC_MAIN) {
                arrowsIn.startLater();
            }
            movUp.setRequestor(this);
            movUp.startLater();
            */
        } else if (evt.getTimerSpec().equals(arrowsTimer) && arrows != null) {
            if (Log.DEBUG_ON) Log.printDebug("NewMenuUI.timerWentOff() --- 3");
            if (focus == FOC_MAIN) {
                arrowsIn.startLater();
            }
        }
    }
}

/*
 *  BackgroundStateManager.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.controller;

import javax.tv.service.*;
import javax.tv.service.selection.*;
import org.ocap.service.*;
import java.rmi.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.*;
import com.videotron.tvi.illico.ixc.monitor.*;
import com.videotron.tvi.illico.ixc.epg.*;
import com.videotron.tvi.illico.mainmenu.communication.*;
import com.videotron.tvi.illico.mainmenu.comp.*;

public class BackgroundStateManager implements ServiceContextListener, ChannelEventListener {
    private static BackgroundStateManager instance;
    private ServiceContext serviceContext;
    private Background bg;
    private boolean lastBgType;
    private BackgroundStateManager() {
        serviceContext = Environment.getServiceContext(0);
        if (serviceContext != null) {
            serviceContext.addListener(this);
        }
        try {
            CommunicationManager.getInstance().getEpgService().addChannelEventListener(this);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static BackgroundStateManager getInstance() {
        if (instance == null) {
            instance = new BackgroundStateManager();
        }
        return instance;
    }

    public void setBackground(Background bg) {
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.setBackground("+bg+")");
        this.bg = bg;
        this.bg.setBgType(lastBgType);
    }

    private String getMonitorStateStr(int state) {
        String stateStr = "";
        if (state == MonitorService.UNKNOWN_STATE) {
            stateStr = "UNKNOWN_STATE";
        } else if (state == MonitorService.TV_VIEWING_STATE) {
            stateStr = "TV_VIEWING_STATE";
        } else if (state == MonitorService.FULL_SCREEN_APP_STATE) {
            stateStr = "FULL_SCREEN_APP_STATE";
        } else if (state == MonitorService.PIP_STATE) {
            stateStr = "PIP_STATE";
        }
        return stateStr;
    }

    private void resetBackground(boolean enforcedDark) {
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground("+enforcedDark+")");
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground() : bg = "+bg);
        if (enforcedDark) {
            // enforcedDark 면 무조건 어둡게
            lastBgType = true;
            if (bg != null) bg.setBgType(true);
        } else {
            int monitorState = MonitorService.UNKNOWN_STATE;
            try {
                monitorState = CommunicationManager.getInstance().getMonitorService().getState();
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground() : monitorState = "+getMonitorStateStr(monitorState));
            // 부팅시에 UNKNOWN_STATE 임. 따라서 UNKNOWN 이면 AV 가 터질 것으로 보고 밝게 처리
            // VDTRMASTER-5531
            if (monitorState == MonitorService.UNKNOWN_STATE || monitorState == MonitorService.FULL_SCREEN_APP_STATE) {
                lastBgType = false;
                if (bg != null) bg.setBgType(false);
                return;
            }
            // TV_VIEWING_STATE 가 아니면 무조건 어둡게
            if (monitorState != MonitorService.TV_VIEWING_STATE) {
                lastBgType = true;
                if (bg != null) bg.setBgType(true);
                return;
            }
            TvChannel curCh = null;
            try {
                curCh = CommunicationManager.getInstance().getEpgService().getCurrentChannel();
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground() : curCh = "+curCh);
            if (curCh == null) return;
            int chType = TvChannel.TYPE_UNKNOWN;
            try {
                chType = curCh.getType();
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground() : chType = "+chType);
            if (chType == TvChannel.TYPE_NORMAL || chType == TvChannel.TYPE_SDV) {
                // normal 채널이나 혹은 sdv 채널은 밝게
                lastBgType = false;
                if (bg != null) bg.setBgType(false);
            } else {
                boolean isVodOrPvrPlaying = false;
                try {
                    isVodOrPvrPlaying = CommunicationManager.getInstance().getMonitorService().isSpecialVideoState();
                } catch (Exception ex) {
                    Log.print(ex);
                }
                if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.resetBackground() : isVodOrPvrPlaying = "+isVodOrPvrPlaying);
                if (isVodOrPvrPlaying) {
                    // normal/sdv 채널이 아니더라도 VOD/PVR 플레이 중이면 밝게
                    lastBgType = false;
                    if (bg != null) bg.setBgType(false);
                } else {
                    // normal/sdv 채널도 아니고 VOD/PVR 플레이 중도 아니면 어둡게
                    lastBgType = true;
                    if (bg != null) bg.setBgType(true);
                }
            }
        }
    }

    // implement of ServiceContextListener
    public void receiveServiceContextEvent(ServiceContextEvent e) {
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.receiveServiceContextEvent("+e+")");
        if (e instanceof NormalContentEvent) {
            resetBackground(false);
        } else if (e instanceof PresentationTerminatedEvent) {
            resetBackground(true);
        } else if (e instanceof SelectionFailedEvent) {
            resetBackground(true);
        } else if (e instanceof AlternativeContentErrorEvent) {
            resetBackground(true);
        }
    }

    // implement of ChannelEventListener 
    public void selectionRequested(int id, short type) throws RemoteException {
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.selectionRequested() : id = "+id+", type = "+type+"");
    }

    // implement of ChannelEventListener 
    public void selectionBlocked(int id, short type) throws RemoteException {
        if (Log.DEBUG_ON)Log.printDebug("BackgroundStateManager.selectionBlocked() : id = "+id+", type = "+type+"");
        resetBackground(true);
    }
}

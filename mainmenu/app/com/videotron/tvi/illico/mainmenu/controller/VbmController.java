package com.videotron.tvi.illico.mainmenu.controller;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;
import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.util.SharedMemory;

public class VbmController implements LogCommandTriggerListener {

    public static boolean ENABLED = false;

    public static final String DEF_MEASUREMENT_GROUP = "-1";
    public static final String VALUE_SEPARATOR = "|";

    protected VbmService vbmService;
    protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;

    protected VbmController() {
    }

    public void init(VbmService service) {
        Log.printInfo("VbmController.init");
        vbmService = service;
        try {
            vbmService.addLogCommandChangeListener(Rs.APP_NAME, this);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        //VBM ID related to Menu and Dashboard
        long[] idsMenu = null;
        try {
        	idsMenu = vbmService.checkLogCommand(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
        synchronized(idSet) {
    		if (idsMenu!=null){
    			for (int i=0; i<idsMenu.length; i++){
    				if (Log.DEBUG_ON){
    					Log.printDebug("VbmController: Menu-ids["+i+"] = "+idsMenu[i]);
    				}
    				idSet.add(new Long(idsMenu[i]));
    			}
    		}
        }
        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (vbmLogBuffer != null) {
            Log.printDebug("VbmController: found vbmLogBuffer");
        } else {
            Log.printDebug("VbmController: not found vbmLogBuffer.");
        }
    }

    public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(Rs.APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

    protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    public boolean write(long id) {
        return write(id, DEF_MEASUREMENT_GROUP, (String) null);
    }

//    public boolean write(long id, String value) {
//        return write(id, DEF_MEASUREMENT_GROUP, value);
//    }

    public boolean write(long id, String group, String value) {
        if (!ENABLED) {
            return false;
        }
        if (!idSet.contains(new Long(id))) {
            return false;
        }
        if (group == null) {
            group = DEF_MEASUREMENT_GROUP;
        }
        StringBuffer sb = new StringBuffer(80);
        sb.append(id);
        sb.append(VbmService.SEPARATOR);
        sb.append(group);
        if (value != null) {
            sb.append(VbmService.SEPARATOR);
            sb.append(value);
        }
        String str = sb.toString();
        if (Log.DEBUG_ON) {
            Log.printDebug("VbmController.write: " + str);
        }
        try {
            vbmLogBuffer.addElement(str);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }

    public boolean write(long id, String group, String[] values) {
        if (!ENABLED) {
            return false;
        }
        if (!idSet.contains(new Long(id))) {
            return false;
        }
        if (group == null) {
            group = DEF_MEASUREMENT_GROUP;
        }
        StringBuffer sb = new StringBuffer(80);
        sb.append(id);
        sb.append(VbmService.SEPARATOR);
        sb.append(group);

        if (values != null && values.length > 0) {
            sb.append(VbmService.SEPARATOR);
            sb.append(values[0]);
            for (int i = 1; i < values.length; i++) {
                sb.append(VALUE_SEPARATOR);
                sb.append(values[i]);
            }
        }
        String str = sb.toString();
        if (Log.DEBUG_ON) {
            Log.printDebug("VbmController.write: " + str);
        }
        try {
            vbmLogBuffer.addElement(str);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return true;
    }


	/**
	 * event invoked when logLevel data updated.
	 * it is only applied to action measurements
	 *
	 *
	 * @param Measurement to notify measurement an frequencies
	 * @param command - true to start logging, false to stop logging
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
        Log.printInfo("VbmController.logCommandChanged: " + measurementId + ", " + command);
        synchronized(idSet) {
            if (command) {
                idSet.add(new Long(measurementId));
            } else {
                idSet.remove(new Long(measurementId));
            }
            checkEmpty();
        }
    }

	/**
	 * event invoked when log of trigger measurement should be collected
	 *
	 * @param measurementId trigger measurement
	 *
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void triggerLogGathering(long measurementId) {
        Log.printInfo("VbmController.triggerLogGathering: " + measurementId);
    }
}


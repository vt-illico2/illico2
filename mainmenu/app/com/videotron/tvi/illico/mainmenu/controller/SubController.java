package com.videotron.tvi.illico.mainmenu.controller;


/**
 * This abstract class is to be generalize Sub controller.
 * Each sub cotroller module extends this super class.
 * 
 * @author Sangjoon Kwon
 */
public abstract class SubController {
    /**
     * Initializes SubController.
     */
    public void init() {
    }

    /**
     * Dispose SubController.
     */
    public abstract void dispose();

    /**
     * Start SubController.
     * 
     * @param isResetData Whether the data will be Reset
     *            
     */
    public abstract void start(boolean isResetData);

    /**
     * Stop SubController.
     */
    public abstract void stop();

    /**
     * Handles Key codes.
     * 
     * @param keyCode
     *            the key code
     * @return true, if key code handled.
     */
    protected abstract boolean keyAction(int keyCode);
}

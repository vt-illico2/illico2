package com.videotron.tvi.illico.mainmenu.controller;

import java.awt.event.KeyEvent;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.ui.MenuUI;
import com.videotron.tvi.illico.mainmenu.ui.NewMenuUI;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * The Class SubControllerMiniMenu.
 *
 * @author Sangjoon Kwon
 */
public class SubControllerMiniMenu extends SubController implements LayeredKeyHandler{
    /** The instance. */
    private static SubControllerMiniMenu instance;
    /*****************************************************************
     * variables - Layered-related
     *****************************************************************/
    private LayeredUI lwMiniMenuMgr;
    public LayeredWindow lwMiniMenu;

    /** The current ui component. */
    private MenuUI miniMenuUI;
    /**
     * Gets the single instance of SubControllerMiniMenu.
     * @return single instance of SubControllerMiniMenu
     */
    public static synchronized SubControllerMiniMenu getInstance() {
        if (instance == null) {
            instance = new SubControllerMiniMenu();
        }
        return instance;
    }

    /**
     * Instantiates a new sub controller mini menu.
     */
    private SubControllerMiniMenu() {
        init();
    }

    /**
     * Inits SubController.
     */
    public void init() {
        if (miniMenuUI == null) {
            miniMenuUI = new NewMenuUI(); // NewMenuUI replace FullMenuUI
        }
        if (lwMiniMenu == null) {
            lwMiniMenu = new LayeredWindowMiniMenu();
        }
        lwMiniMenu.add(miniMenuUI);
        lwMiniMenu.setVisible(true);

        lwMiniMenuMgr = WindowProperty.MINI_MENU.createLayeredDialog(lwMiniMenu, lwMiniMenu.getBounds(), this);
        lwMiniMenuMgr.deactivate();
    }

    /**
     * Dispose SubController.
     */
    public void dispose() {
        stop();
        if (lwMiniMenu != null) {
            lwMiniMenu.setVisible(false);
            lwMiniMenu.removeAll();
            lwMiniMenu = null;
        }
        if (miniMenuUI != null) {
            miniMenuUI.setVisible(false);
            miniMenuUI.removeAll();
            miniMenuUI = null;
        }
        if (lwMiniMenuMgr != null) {
            lwMiniMenuMgr.deactivate();
            lwMiniMenuMgr = null;
        }
    }

    /**
     * Start SubController.
     * @param isResetData Whether the data will be Reset
     */
    public void start(boolean isResetData) {
        if (Log.DEBUG_ON) {
            Log.printDebug("SubControllerMiniMenu.start :");
        }
//        EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                if (lwMiniMenuMgr != null) {
//                    lwMiniMenuMgr.activate();
//                }
//                if (miniMenuUI != null) {
//                    miniMenuUI.start(true);
//                }
//            }
//        });
        if (miniMenuUI != null) {
            miniMenuUI.start(true);
        }
        if (lwMiniMenuMgr != null) {
            lwMiniMenuMgr.activate();
        }
    }

    /**
     * Stop SubController.
     */
    public void stop() {
        if (Log.INFO_ON) {
            Log.printInfo("[SubControllerMiniMenu.stop]start");
        }
//        EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                if (miniMenuUI != null) {
//                    miniMenuUI.stop();
//                    miniMenuUI.setVisible(false);
//                }
//                if (lwMiniMenuMgr != null) {
//                    lwMiniMenuMgr.deactivate();
//                }
//            }
//        });
        if (miniMenuUI != null) {
            miniMenuUI.stop();
            miniMenuUI.setVisible(false);
        }
        if (lwMiniMenuMgr != null) {
            lwMiniMenuMgr.deactivate();
        }
        
//        ImagePool ip=FrameworkMain.getInstance().getImagePool();
//        Enumeration enu = ip.keys();
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[SubControllerMiniMenu.stop]enu : "+enu);
//            while(enu.hasMoreElements()) {
//                String imageName=(String)enu.nextElement();
//                Log.printDebug("[SubControllerMiniMenu.stop]Image name : "+imageName);
//                Log.printDebug("[SubControllerMiniMenu.stop]Image object : "+ip.get(imageName));
//            }
//        }
    }

    /**
     * Key action.
     *
     * @param keyCode
     *            the key code
     * @return true, if successful
     */
    protected boolean keyAction(int keyCode) {
        return false;
    }

    private boolean isOldEvent(UserEvent e) {
        long curTime = System.currentTimeMillis();
        long eventTime = e.getWhen();
        long gap = curTime - eventTime;
        if (gap > 500L) return true;
        return false;
    }

    public boolean handleKeyEvent(UserEvent userEvent) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyMiniMenu.handleKeyEvent]start.");
        }
        if (userEvent == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LayeredKeyMiniMenu.handleKeyEvent]User Event is null.");
            }
            return false;
        }
        if (isOldEvent(userEvent)) return true;
        if (userEvent.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int keyCode = userEvent.getCode();
        if (Log.DEBUG_ON) {
            Log.printDebug("[LayeredKeyMiniMenu.handleKeyEvent]keyCode : "+keyCode);
        }
        switch(keyCode) {
            case Rs.KEY_EXIT:
                MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false, true);
                return true;
            case KeyCodes.COLOR_B:
            case KeyCodes.COLOR_C:
            // DDC-113 Visually Impaired : “STAR” key is blocked.
            case KeyCodes.STAR:
                return true;
        }
        return miniMenuUI.handleKey(keyCode);
    }

    /**
     * Gets the current ui component main menu.
     *
     * @return the current ui component main menu
     */
    public MenuUI getCurrentUIComponentMainMenu() {
        return miniMenuUI;
    }
    /*****************************************************************
     * classes - Layered-related
     *****************************************************************/
    class LayeredWindowMiniMenu extends LayeredWindow {
        private static final long serialVersionUID = 1L;
        public LayeredWindowMiniMenu() {
            setBounds(0, 0, Rs.SCREEN_SIZE_WIDTH, Rs.SCREEN_SIZE_HEIGHT);
        }
        public void notifyShadowed() {
            if (Log.DEBUG_ON) {
                Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]start.");
            }
            LayeredUIInfo[] layeredUIInfos = LayeredUIManager.getInstance().getAllLayeredUIInfos();
            if (layeredUIInfos != null) {
                int mmPriority = WindowProperty.MINI_MENU.getPriority();
                String mmName = WindowProperty.MINI_MENU.getName();
                if (Log.DEBUG_ON) {
                    Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]Name : "+mmName+" - Prior : "+mmPriority);
                }
//                String recPopName = WindowProperty.PVR_POPUP.getName();
                int layeredUIInfosLth = layeredUIInfos.length;
                for (int i=0; i<layeredUIInfosLth; i++) {
                    LayeredUIInfo layeredUIInfo = layeredUIInfos[i];
                    if (layeredUIInfo == null) {
                        continue;
                    }
                    String lName = layeredUIInfo.getName();
                    if (lName == null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]Layered UI Name is null. continue.");
                        }
                        continue;
                    }
                    int lPrior = layeredUIInfo.getPriority();
                    if (Log.DEBUG_ON) {
                        Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]LName : "+lName+" - LPrior : "+lPrior);
                    }
//                    if (recPopName.equals(lName)) {
//                        if (miniMenuUI != null) {
//                            miniMenuUI.setActivatedRecPopup(layeredUIInfo.isActive());
//                        }
//                    }
                    if (!mmName.equals(lName) &&  lPrior == mmPriority) {
                        boolean isActive = layeredUIInfo.isActive();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]"+lName+" - isActive : "+isActive);
                        }
                        if (isActive) {
                            if (Log.DEBUG_ON) {
                                Log.printDebug("[LayeredWindowMiniMenu.notifyShadowed]Mini Menu stopped for activating "+lName);
                            }
                            MainController.getInstance().changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false, true);
                            break;
                        }
                    }
                }
            }
        }
    }
}

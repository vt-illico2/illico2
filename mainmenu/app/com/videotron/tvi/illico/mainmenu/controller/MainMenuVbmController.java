package com.videotron.tvi.illico.mainmenu.controller;

import com.videotron.tvi.illico.log.Log;


public class MainMenuVbmController extends VbmController {
    
    public static final long MENU_ITEM_SELECTION = 1005000001L;
    
    protected static MainMenuVbmController instance = new MainMenuVbmController();

    public static MainMenuVbmController getInstance() {
        return instance;
    }
    
    protected MainMenuVbmController() {
    }
    
    public void writeMenuItemSelection(String menuId) {
        Log.printInfo("[MainMenuVbmController.writeMenuItemSelection] start.");
        Log.printInfo("[MainMenuVbmController.writeMenuItemSelection] Param - Menu ID : " + menuId);
        write(MENU_ITEM_SELECTION, DEF_MEASUREMENT_GROUP, menuId);
        Log.printInfo("[MainMenuVbmController.writeMenuItemSelection] end.");
    }
}

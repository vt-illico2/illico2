package com.videotron.tvi.illico.mainmenu.controller;

import java.awt.Image;
import java.io.File;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.Util;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * Main feature of MainDataManager is to parse data.
 * @author Sangjoon Kwon
 */

public class DataManager implements DataUpdateListener {
    private static DataManager instance;
    /*****************************************************************
     * Framework-related
     *****************************************************************/
    private DataCenter dCenter = DataCenter.getInstance();
    private FrameworkMain fwMain = FrameworkMain.getInstance();
    private CommunicationManager cMgr = CommunicationManager.getInstance();
    /*****************************************************************
     * File Name-related
     *****************************************************************/
    public static final String IB_BRAND_IMAGE_EN = "IB_BRAND_IMAGE_EN";
    public static final String IB_BRAND_IMAGE_FR = "IB_BRAND_IMAGE_FR";
    /*****************************************************************
     * Parcing Data-related
     *****************************************************************/
    public static final String MENU_LANGUAGE_TYPE_ENGLISH = "en";
    public static final String MENU_LANGUAGE_TYPE_FRENCH = "fr";
    public static final String DATA_SEPARATOR = "|";
    /*****************************************************************
     * Brand Image-related
     *****************************************************************/
    private static final int BRAND_IMG_COUNT = 2;
    private static final int BRAND_IMG_EN = 0;
    private static final int BRAND_IMG_FR = 1;
    private static final String[] BRAND_IMAGE_KEY_LIST = {"BRAND_IMAGE_EN", "BRAND_IMAGE_FR"};
    private Image[] imgBrandList;
    private int curBrandImgIdx;
    private String[] authorizeExceptAppNameList;
    
    /*****************************************************************
     * Footer Image-related
     *****************************************************************/
    private Hashtable footerHash;
    
    // in the future, it will be used.
//    private CheckingPackageTimer checkingPackageTimer;
    
//    private boolean isTested; 
    /**
     * Instantiates a new main data manager.
     */
    private DataManager() {
    }
    /**
     * Gets the single instance of MainDataManager.
     * @return single instance of MainDataManager
     */
    public static synchronized DataManager getInstance() {
        if (instance == null) {
            instance = new DataManager();
        }
        return instance;
    }
    /**
     * init DataManager.
     */
    public void init() {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.init]start");
        }
        //init Brand Image
        initBrandImage();
        if (!Rs.INSTEAD_OF_IB_OOB) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.init]use real data.(Inband, OOB)");
            }
            File fileIbBrandImageEn = (File)dCenter.get(IB_BRAND_IMAGE_EN);
            if (fileIbBrandImageEn != null) {
                Log.printDebug("[DataMgr.init]Brand image en file is exist.");
                dataUpdated(IB_BRAND_IMAGE_EN, null, fileIbBrandImageEn);
            }
            dCenter.addDataUpdateListener(IB_BRAND_IMAGE_EN, this);

            File fileIbBrandImageFr = (File)dCenter.get(IB_BRAND_IMAGE_FR);
            if (fileIbBrandImageFr != null) {
                Log.printDebug("[DataMgr.init]Brand image fr file is exist.");
                dataUpdated(IB_BRAND_IMAGE_FR, null, fileIbBrandImageFr);
            }
            dCenter.addDataUpdateListener(IB_BRAND_IMAGE_FR, this);

        }
        
        // TODO handle package
//        String millisCheckingPackageV = DataCenter.getInstance().getString("MILLIS_CHECKING_PACKAGE");
//        long millisCheckingPackage = 120000L;
//        if (millisCheckingPackageV != null) {
//            millisCheckingPackage = Long.parseLong(millisCheckingPackageV);
//        }
//        if (millisCheckingPackage > 0) {
////            checkingPackageTimer = new CheckingPackageTimer(millisCheckingPackage);
////            checkingPackageTimer.start();
//        	new CheckingPackageThread(millisCheckingPackage).start();
//        }
    }
    public void dispose() {
        if (Log.INFO_ON) {
            Log.printInfo("[DataMgr.dispose]Start.");
        }
        
        // TODO handle package
//        if (checkingPackageTimer != null) {
//            checkingPackageTimer.stop();
//            checkingPackageTimer.dispose();
//            checkingPackageTimer = null;
//        }
        footerHash = null;
        if (dCenter != null) {
            dCenter.removeImage(BRAND_IMAGE_KEY_LIST[BRAND_IMG_EN]);
            dCenter.removeImage(BRAND_IMAGE_KEY_LIST[BRAND_IMG_FR]);
        }
        instance = null;
        if (Log.INFO_ON) {
            Log.printInfo("[DataMgr.dispose]End.");
        }
    }
    
    // TODO handle package
//    /*****************************************************************
//     * Methods - Full Menu-related
//     *****************************************************************/
//    public boolean authorizeFullMenuItem() {
//        if (Log.INFO_ON) {
//            Log.printInfo("[DataMgr.authorizeFullMenuItem]Start.");
//        }
//        if (fmItemList == null || fmItemList.length == 0) {
//            if (Log.WARNING_ON) {
//                Log.printWarning("[DataMgr.authorizeFullMenuItem]Invalid full menu item list. return.");
//            }
//            return false;
//        }
//        if (authorizedFmItemFactorVec == null) {
//            authorizedFmItemFactorVec = new Vector();
//        } else {
//            authorizedFmItemFactorVec.clear();
//        }
//        Vector authorizedFmItemVector = new Vector();
//        for (int i=0; i<fmItemList.length; i++) {
//            MenuItem item = fmItemList[i];
//            if (item == null) {
//                if (Log.WARNING_ON) {
//                    Log.printWarning("[DataMgr.authorizeFullSubMenuItem]"+i+"-th item is null. continue.");
//                }
//                continue;
//            }
//            int id = item.getMenuID();
//            boolean isGroup = item.isGroupMenuItem();
//            try{
//                if (isGroup) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]This menu is group item.");
//                    }
//                    MenuItem[] initSubItems = item.getInitSubMenuItems();
//                    Vector authorizedFmSubItemVec = authorizeFullMenuSubItem(initSubItems);
//                    int authorizedFmSubItemVecSize = 0;
//                    if (authorizedFmSubItemVec == null || (authorizedFmSubItemVecSize = authorizedFmSubItemVec.size()) == 0) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Menu item is invalid.(No sub menu item)");
//                        }
//                        continue;
//                    }
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Menu item is valid. Sub menu item count : " + authorizedFmSubItemVecSize);
//                    }
//                    MenuItem[] authorizedFmSubItemList = new MenuItem[authorizedFmSubItemVecSize];
//                    authorizedFmSubItemList = (MenuItem[])authorizedFmSubItemVec.toArray(authorizedFmSubItemList);
//                    item.setValidSubMenuItems(authorizedFmSubItemList);
//                } else {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]This menu is menu item.");
//                    }
//                    //Check package name
//                    String packageName = item.getPackageName();
//                    if (Log.DEBUG_ON) {
//                        String title = item.getMenuTitleEn();
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Title : "+title);
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Package name : "+packageName);
//                    }
//                    if (packageName == null) {
//                        packageName= "";
//                    }
//                    //if packageName is null, then show menu item.
//                    int resultAuthorization = MonitorService.AUTHORIZED;
//                    if (Rs.IS_EMULATOR) {
//                        if (packageName.equals("SKIP_PACKAGE")) {
//                            resultAuthorization = MonitorService.NOT_AUTHORIZED;
//                        }
////                        if (packageName.equals("PACKAGE_TEST")) {
////                            isTested = !isTested;
////                            if (isTested) {
////                                if (Log.DEBUG_ON) {
////                                    Log.developer.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]PACKAGE_TEST true");
////                                }
////                                resultAuthorization = MonitorService.NOT_AUTHORIZED;
////                            }
////                        }
//                    }else {
//                        if (packageName.trim().length() > 0) {
//                            resultAuthorization = cMgr.requestCheckResourceAuthorization(packageName);
//                        }
//                    }
//                    if (resultAuthorization == MonitorService.NOT_AUTHORIZED) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Menu item is not authorized.(Check resource authorization by monitor)");
//                        }
//                        continue;
//                    }
//                    //Check app name
//                    //if box did not support DVR, then except PVR menu item.
//                    // 
////                    if (!Environment.SUPPORT_DVR) {
////                        String targetAppName = item.getMenuTargetAppName();
////                        if (targetAppName != null && targetAppName.equals(MenuItem.ITEM_NAME_PVR)){
////                            if (Log.DEBUG_ON) {
////                                Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Menu item is invalid.(PVR-related at non-PVR box)");
////                            }
////                            continue;
////                        }
////                    }
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuItem]["+id+"]Menu item is valid.");
//                    }
//                    authorizedFmItemFactorVec.addElement(item);
//                }
//                authorizedFmItemVector.addElement(item);
//            }catch(Exception e) {
//                if (Log.INFO_ON) {
//                    Log.printInfo("[DataMgr.authorizeFullMenuItem]["+id+"]Exception occurred.");
//                }
//            }
//        }
//        int authorizedMenuItemVectorSize = authorizedFmItemVector.size();
//        if (authorizedMenuItemVectorSize == 0) {
//            if (Log.WARNING_ON) {
//                Log.printWarning("[DataMgr.authorizeFullMenuItem]Total authorized menu item count is zero. return false.");
//            }
//            return false;
//        }
//        if (Log.DEBUG_ON) {
//            Log.printDebug("[DataMgr.authorizeFullMenuItem]Total authorized menu item count : " + authorizedMenuItemVectorSize);
//        }
//        authorizedFmItemList = new MenuItem[authorizedMenuItemVectorSize];
//        authorizedFmItemList = (MenuItem[])authorizedFmItemVector.toArray(authorizedFmItemList);
//        if (Log.INFO_ON) {
//            Log.printInfo("[DataMgr.authorizeFullMenuItem]End. return true.");
//        }
//        return true;
//    }
//    private Vector authorizeFullMenuSubItem(MenuItem[] reqFmSubItemList) {
//        if (Log.INFO_ON) {
//            Log.printInfo("[DataMgr.authorizeFullMenuSubItem]Start.");
//        }
//        if (reqFmSubItemList == null || reqFmSubItemList.length == 0) {
//            if (Log.WARNING_ON) {
//                Log.printWarning("[DataMgr.authorizeFullMenuSubItem]Invalid full menu sub item list. return null.");
//            }
//            return null;
//        }
//        Vector authorizedFmSubItemsVec = new Vector();
//        for (int i=0; i<reqFmSubItemList.length; i++) {
//            MenuItem subItem = reqFmSubItemList[i];
//            if (subItem == null) {
//                if (Log.WARNING_ON) {
//                    Log.printWarning("[DataMgr.authorizeFullMenuSubItem]"+i+"-th sub item is null. continue.");
//                }
//                continue;
//            }
//            int id = subItem.getMenuID();
//            boolean isGroupMenuItem = subItem.isGroupMenuItem();
//            try{
//                if (isGroupMenuItem) {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]This sub menu is group item.");
//                    }
//                    MenuItem[] initSubItems = subItem.getInitSubMenuItems();
//                    Vector authorizedSubMenuItemVec = authorizeFullMenuSubItem(initSubItems);
//                    int authorizedFmSubItemVecSize = 0;
//                    if (authorizedSubMenuItemVec == null || (authorizedFmSubItemVecSize = authorizedSubMenuItemVec.size()) == 0) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub menu item is invalid.(No sub menu item)");
//                        }
//                        continue;
//                    }
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub menu item is valid. Sub menu item count : " + authorizedFmSubItemVecSize);
//                    }
//                    MenuItem[] authorizedSubMenuItems = new MenuItem[authorizedFmSubItemVecSize];
//                    authorizedSubMenuItems = (MenuItem[])authorizedSubMenuItemVec.toArray(authorizedSubMenuItems);
//                    subItem.setValidSubMenuItems(authorizedSubMenuItems);
//                } else {
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]This sub menu is menu item.");
//                    }
//                    String packageName = subItem.getPackageName();
//                    if (Log.DEBUG_ON) {
//                        String title = subItem.getMenuTitleEn();
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub title : "+title);
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub package name : "+packageName);
//                    }
//                    if (packageName == null) {
//                        packageName= "";
//                    }
//                    //if packageName is null, then show menu item.
//                    int resultAuthorization = MonitorService.AUTHORIZED;
//                    if (Rs.IS_EMULATOR) {
//                        if (packageName.equals("SKIP_PACKAGE")) {
//                            resultAuthorization = MonitorService.NOT_AUTHORIZED;
//                        }
////                        if (packageName.equals("PACKAGE_TEST")) {
////                            isTested = !isTested;
////                            if (isTested) {
////                                if (Log.DEBUG_ON) {
////                                    Log.developer.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]PACKAGE_TEST true");
////                                }
////                                resultAuthorization = MonitorService.NOT_AUTHORIZED;
////                            }
////                        }
//                    }else {
//                        if (packageName.trim().length() > 0) {
//                            resultAuthorization = cMgr.requestCheckResourceAuthorization(packageName);
//                        }
//                    }
//                    if (resultAuthorization == MonitorService.NOT_AUTHORIZED) {
//                        if (Log.DEBUG_ON) {
//                            Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub menu item is not authorized.(Check resource authorization by monitor)");
//                        }
//                        continue;
//                    }
//                    //if box did not support DVR, then except PVR menu item.
//                    if (!Environment.SUPPORT_DVR) {
//                        String targetAppName = subItem.getMenuTargetAppName();
//                        if (targetAppName != null && targetAppName.equals(MenuItem.ITEM_NAME_PVR)){
//                            if (Log.DEBUG_ON) {
//                                Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub menu item is invalid.(PVR-related at non-PVR box)");
//                            }
//                            continue;
//                        }
//                    }
//                    if (Log.DEBUG_ON) {
//                        Log.printDebug("[DataMgr.authorizeFullMenuSubItem]["+id+"]Sub menu item is valid.");
//                    }
//                    authorizedFmItemFactorVec.addElement(subItem);
//                }
//                authorizedFmSubItemsVec.addElement(subItem);
//            }catch(Exception e) {
//                if (Log.INFO_ON) {
//                    Log.printInfo("[DataMgr.authorizeFullMenuSubItem]["+id+"]Exception occurred.");
//                }
//            }
//        }
//        if (Log.INFO_ON) {
//            Log.printInfo("[DataMgr.authorizeFullMenuSubItem]End.");
//        }
//        return authorizedFmSubItemsVec;
//    }
    
    /*****************************************************************
     * Methods - Footer Image-related
     *****************************************************************/
    public Image getFooterImage(String btKey) {
        if (btKey == null) {
            return null;
        }
        footerHash = (Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG);
        Image imgFooter = null;
        if (footerHash != null) {
            imgFooter = (Image) footerHash.get(btKey);
        }
        return imgFooter;
    }
    /*****************************************************************
     * Methods - DataUpdateListener-implemented
     *****************************************************************/
    public void dataRemoved(String key) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataRemoved]key : " + key);
        }
    }
    public void dataUpdated(String key, Object old, Object value) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[DataMgr.dataUpdated]key : " + key);
            Log.printDebug("[DataMgr.dataUpdated]old : " + old);
            Log.printDebug("[DataMgr.dataUpdated]value : " + value);
        }
        if (key == null) {
            return;
        }
        if (key.equals(IB_BRAND_IMAGE_EN)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated][IB]updated Brand Image-En");
            }
            File imgBrandFile = (File) value;
            Image imgBrandEn = getImageByFile(imgBrandFile, BRAND_IMAGE_KEY_LIST[BRAND_IMG_EN]);
            if (imgBrandEn != null) {
                imgBrandList[BRAND_IMG_EN] = null;
                dCenter.removeImage(BRAND_IMAGE_KEY_LIST[BRAND_IMG_EN]);
                imgBrandList[BRAND_IMG_EN] = imgBrandEn;
                if (curBrandImgIdx == BRAND_IMG_EN) {
                    if ((Image)SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY) != null) {
                        SharedMemory.getInstance().remove(MainMenuService.BRAND_IMAGE_KEY);
                    }
                    SharedMemory.getInstance().put(MainMenuService.BRAND_IMAGE_KEY, imgBrandList[BRAND_IMG_EN]);
                }
//                sendDataManagerUpdateEvent(DataManagerListener.DATA_KEY_BRAND_IMAGE);
            }
        } else if (key.equals(IB_BRAND_IMAGE_FR)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[DataMgr.dataUpdated][IB]updated Brand Image-Fr");
            }
            File imgBrandFile = (File) value;
            Image imgBrandFr = getImageByFile(imgBrandFile, BRAND_IMAGE_KEY_LIST[BRAND_IMG_FR]);
            if (imgBrandFr != null) {
                imgBrandList[BRAND_IMG_FR] = null;
                dCenter.removeImage(BRAND_IMAGE_KEY_LIST[BRAND_IMG_FR]);
                imgBrandList[BRAND_IMG_FR] = imgBrandFr;
                if (curBrandImgIdx == BRAND_IMG_FR) {
                    if ((Image)SharedMemory.getInstance().get(MainMenuService.BRAND_IMAGE_KEY) != null) {
                        SharedMemory.getInstance().remove(MainMenuService.BRAND_IMAGE_KEY);
                    }
                    SharedMemory.getInstance().put(MainMenuService.BRAND_IMAGE_KEY, imgBrandList[BRAND_IMG_FR]);
                }
//                sendDataManagerUpdateEvent(DataManagerListener.DATA_KEY_BRAND_IMAGE);
            }
        }
    }
    
    /*****************************************************************
     * Methods - BrandImage-related
     *****************************************************************/
    private void initBrandImage() {
        imgBrandList = new Image[BRAND_IMG_COUNT];
        File fileBrandEn = new File("resource/default_data/brand_image/brand_image_en.png");
        imgBrandList[BRAND_IMG_EN] = getImageByFile(fileBrandEn, BRAND_IMAGE_KEY_LIST[BRAND_IMG_EN]);
        File fileBrandFr = new File("resource/default_data/brand_image/brand_image_fr.png");
        imgBrandList[BRAND_IMG_FR] = getImageByFile(fileBrandFr, BRAND_IMAGE_KEY_LIST[BRAND_IMG_FR]);
        curBrandImgIdx = BRAND_IMG_EN;
        SharedMemory.getInstance().put(MainMenuService.BRAND_IMAGE_KEY, imgBrandList[BRAND_IMG_EN]);
    }
    private Image getImageByFile(File reqFile, String reqImgKey) {
        if (reqFile == null || reqImgKey ==null) {
            Log.printInfo("[DataMgr.getImageByFile]Param is invalid. return null.");
            return null;
        }
        byte[] reqFileSrc = Util.getByteArrayFromFile(reqFile);
        if (reqFileSrc == null) {
            Log.printInfo("[DataMgr.getImageByFile]Image source is invalid. return null.");
            return null;
        }
        Image img = fwMain.getImagePool().createImage(reqFileSrc, reqImgKey);
        fwMain.getImagePool().waitForAll();
        return img;
    }
    
    /*****************************************************************
     * Methods Brand Image-related
     *****************************************************************/
    public Image getBrandImage() {
        return imgBrandList[curBrandImgIdx];
    }
    public void changeBrandImage(String lang){
        int tempBrandImage = BRAND_IMG_EN;
        if (lang != null && lang.equalsIgnoreCase(Definitions.LANGUAGE_FRENCH)) {
            tempBrandImage = BRAND_IMG_FR;
        }
        if (curBrandImgIdx != tempBrandImage) {
            curBrandImgIdx = tempBrandImage;
            SharedMemory.getInstance().put(MainMenuService.BRAND_IMAGE_KEY, imgBrandList[curBrandImgIdx]);
//            sendDataManagerUpdateEvent(DataManagerListener.DATA_KEY_BRAND_IMAGE);
        }
    }
    
    public boolean isExceptAppName(String reqAppName) {
        if (reqAppName == null) {
            return false;
        }
//        if (Rs.IS_EMULATOR && reqAppName.equals("Profile")) {
//            return true;
//        }
        if (authorizeExceptAppNameList == null) {
            String exceptAppName = dCenter.getString("AUTHORIZE_EXCEPT_APP_NAMES");
            if (exceptAppName != null) {
                authorizeExceptAppNameList = TextUtil.tokenize(exceptAppName, DATA_SEPARATOR);
            }
        }
        if (authorizeExceptAppNameList != null) {
            for (int i = 0; i<authorizeExceptAppNameList.length; i++) {
                if (authorizeExceptAppNameList[i] != null && authorizeExceptAppNameList[i].equals(reqAppName)) {
                    return true;
                }
            }
        }
        return false;
    }
}

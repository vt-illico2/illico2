package com.videotron.tvi.illico.mainmenu.controller;

import javax.tv.util.*;

import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.Rs;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;
import com.videotron.tvi.illico.mainmenu.communication.PinEnablerListenerImpl;
import com.videotron.tvi.illico.mainmenu.communication.PreferenceProxy;
import com.videotron.tvi.illico.mainmenu.controller.DataManager;
import com.videotron.tvi.illico.mainmenu.controller.SubController;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class handles almost main menu features.
 * <p>
 * Handling Main menu Features <ui>
 * <li>
 * Change main menu state.
 * <li>
 * Go to next ui component.
 * <li>
 * Try to look up binded Services.
 * <li>
 * Visible / Invisible Main menu.
 * <li>
 * Listen Channel-related Event. </ui>
 *
 * @author Sangjoon Kwon
 */
public class MainController implements TVTimerWentOffListener{
	
	public static final String APP_NAME_PROFILE = "Profile";
	public static final String APP_NAME_SEARCH = "Search";
	
    /** The instance. */
    private static MainController instance;
    private CommunicationManager cMgr = CommunicationManager.getInstance();
    /** The sub controller. */
    private SubController subController;
    /** The current main menu state. */
    private int currentMainMenuState;
    /** The default main menu state. */
//    private int defaultMainMenuState = -1;
    /*****************************************************************
     * PIN Enabler-related
     *****************************************************************/
    private String[] pinEnablerAppNames;


    public static long invisibleTimeoutByUserLaunch;
    public static long invisibleTimeoutByBootingLaunch;
    private TVTimerSpec invisibleTimer;
    private boolean fromHotkey;
    
    /**
     * Instantiates a new main controller.
     */
    private MainController() {
        try {
            invisibleTimeoutByUserLaunch = DataCenter.getInstance().getLong("INVISIBLE_TIMEOUT_BY_USER_LAUNCH");
            invisibleTimeoutByBootingLaunch = DataCenter.getInstance().getLong("INVISIBLE_TIMEOUT_BY_BOOTING_LAUNCH");
            if (Log.DEBUG_ON) Log.printDebug("MainController : invisibleTimeoutByUserLaunch = "+invisibleTimeoutByUserLaunch);
            if (Log.DEBUG_ON) Log.printDebug("MainController : invisibleTimeoutByBootingLaunch = "+invisibleTimeoutByBootingLaunch);
        } catch (Exception e) {
            Log.print(e);
        }
        invisibleTimer = new TVTimerSpec();
        invisibleTimer.addTVTimerWentOffListener(this);
    }

    public void resetInvisibleTimer(boolean hotKey) {
        if (Log.DEBUG_ON) Log.printDebug("MainController.resetInvisibleTimer()");
        try {
            descheduleInvisibleTimer();
            invisibleTimer.setRepeat(false);
            invisibleTimer.setRegular(true);
            if (hotKey) {
            	invisibleTimer.setDelayTime(invisibleTimeoutByUserLaunch);
            } else {
            	invisibleTimer.setDelayTime(invisibleTimeoutByBootingLaunch);
            }
            TVTimer.getTimer().scheduleTimerSpec(invisibleTimer);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void descheduleInvisibleTimer() {
        try {
            TVTimer.getTimer().deschedule(invisibleTimer);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        if (Log.DEBUG_ON) Log.printDebug("MainController.timerWentOff() : Call changeMenuState(CLOSE)");
        changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, true, true);
    }

    public void notifyPortalVisibleOverIxc(boolean visible) {
        if (Log.DEBUG_ON) Log.printDebug("MainController.notifyPortalVisibleOverIxc("+visible+")");
        CommunicationManager.getInstance().notifyPortalVisibilityChanged(visible);
    }

    /**
     * Gets the single instance of MainController.
     * @return single instance of MainController
     */
    public static synchronized MainController getInstance() {
        if (instance == null) {
            instance = new MainController();
        }
        return instance;
    }
    /**
     * Inits MainController.
     *
     * @param xCtx
     *            the xlet context
     */
    public void init() {
    }
    /**
     * Dispose MainController.
     */
    public void dispose() {
        if (subController !=null) {
            subController.dispose();
            subController = null;
        }
        cMgr.requestHideSearchService();
        instance = null;
    }
    
    public boolean changeMenuState(int reqMainMenuState, boolean isResetData, boolean hotkey) {
    	fromHotkey = hotkey;
    	return changeMenuState(reqMainMenuState, isResetData);
    }
    
    /**
     * Change main menu state.
     * @param reqMainMenuState
     *            the main menu state
     * @param isResetData
     *            the is reset data
     * @return true, if successful
     */
    private boolean changeMenuState(int reqMainMenuState, boolean isResetData) {
        if (Log.INFO_ON) {
            Log.printInfo("[MainCtrl.changeMenuState]Param - Request menu state : " + getStateName(reqMainMenuState));
            Log.printInfo("[MainCtrl.changeMenuState]Param - Request reset data : " + isResetData);
            Log.printInfo("[MainCtrl.changeMenuState]Current menu state : "+getStateName(currentMainMenuState));
        }
        if (reqMainMenuState == MainMenuService.MAIN_MENU_STATE_DASHBOARD
                && currentMainMenuState == MainMenuService.MAIN_MENU_STATE_DASHBOARD) {
            if (Log.INFO_ON) {
                Log.printInfo("[MainCtrl.changeMenuState]Close all.");
            }
            changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, true, true);
            return true;
        }
        if (reqMainMenuState == currentMainMenuState) {
            if (Log.INFO_ON) {
                Log.printInfo("[MainCtrl.changeMenuState]Reqeust State and current State is same. end.");
            }
            return true;
        }
        if (cMgr.getMonitorService() == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("[MainCtrl.changeMenuState]MonitorService is null.");
            }
//            CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_NOT_READY_MENU_DATA, null);
            changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false);
            return false;
        }
        // flush Before Controller
        switch (currentMainMenuState) {
            case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
                stopApplication();
                if (subController != null) {
                    subController.stop();
                    subController = null;
                }
                break;
            case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
                cMgr.requestHideDashboardService();
                break;
            case MainMenuService.MAIN_MENU_CLOSE_ALL:
                break;
        }
        // create New Controller
        switch (reqMainMenuState) {
            // NewMenu replace Full/MiniMenu
            case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
                subController = SubControllerMiniMenu.getInstance();
                subController.start(isResetData);
                currentMainMenuState = MainMenuService.MAIN_MENU_STATE_MINI_MENU;
                notifyPortalVisibleOverIxc(true);
                resetInvisibleTimer(fromHotkey);
                return true;
                
//                MenuItem[] fmItems = DataManager.getInstance().getFullMenuItemList();
//                if (fmItems != null && fmItems.length > 0) {
//                    subController = SubControllerFullMenu.getInstance();
//                    boolean result = subController.isValidSubController();
//                    if (Log.INFO_ON) {
//                        Log.printInfo("[MainCtrl.changeMenuState]check validation about subcontroller of full menu. : "+result);
//                    }
//                    if (result) {
//                        subController.start(isResetData);
//                        if (currentMainMenuState!=MainMenuService.MAIN_MENU_STATE_FULL_MENU) {
//                        	MainMenuVbmController.getInstance().writeFullMenuStart();
//                        }
//                        currentMainMenuState = MainMenuService.MAIN_MENU_STATE_FULL_MENU;
//                        return true;
//                    }
//                }
//                if (Log.ERROR_ON) {
//                    Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
//                }
//                CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
//                changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false);
//                return false;
//            case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
//                MenuItem[] mmItems = DataManager.getInstance().getMiniMenuItems();
//                if (mmItems != null && mmItems.length > 0) {
//                    subController = SubControllerMiniMenu.getInstance();
//                    boolean resultSubControllerMiniMenu = subController.isValidSubController();
//                    if (Log.INFO_ON) {
//                        Log.printInfo("[MainCtrl.changeMenuState]check validation about subcontroller of mini menu. : "+resultSubControllerMiniMenu);
//                    }
//                    if (resultSubControllerMiniMenu) {
//                        subController.start(isResetData);
//                        if (currentMainMenuState!=MainMenuService.MAIN_MENU_STATE_MINI_MENU) {
//                        	MainMenuVbmController.getInstance().miniMenuShown();
//                        }
//                        currentMainMenuState = MainMenuService.MAIN_MENU_STATE_MINI_MENU;
//                        return true;
//                    }
//                }
//                if (Log.ERROR_ON) {
//                    Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
//                }
//                CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
//                changeMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL, false);
//                return false;
            case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
            	// R5 - TANK
                cMgr.requestShowDashboardService();
                currentMainMenuState = MainMenuService.MAIN_MENU_STATE_DASHBOARD;
                break;
            case MainMenuService.MAIN_MENU_CLOSE_ALL:
                descheduleInvisibleTimer();
                currentMainMenuState = MainMenuService.MAIN_MENU_CLOSE_ALL;
                notifyPortalVisibleOverIxc(false);
                break;
        }
        return true;
    }
    /**
     * Gets the main menu state.
     * @return the main menu state
     */
    public int getMainMenuState() {
        return currentMainMenuState;
    }
    /**
     * Gets the sub controller.
     *
     * @return the sub controller
     */
    public SubController getSubController() {
        return subController;
    }
    /*****************************************************************
     * methods - Timer-implemented
     *****************************************************************/
    /*
    public void setTimer(int secs, TVTimerWentOffListener llistener) {
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainCtrl.setTimer]secs : "+secs);
        }
        this.llistener = llistener;
        stopMenuTimer();
        menuTimer = new TVTimerSpec();
        menuTimer.setDelayTime(secs * Rs.MILLIS_TO_SECOND);
        menuTimer.addTVTimerWentOffListener(llistener);
    }
    */
    /*****************************************************************
     * methods - Other Application-related
     *****************************************************************/   
    public void startApplication(String appName, String[] appParamList, String packageName) {
        if (Log.INFO_ON) {
            Log.printInfo("[MainCtrl.startApplication]Request app name : "+appName);
            Log.printInfo("[MainCtrl.startApplication]Request app parameter list : "+appParamList);
            if (appParamList != null) {
                for (int i=0; i<appParamList.length; i++) {
                    Log.printInfo("[MainCtrl.startApplication]Request app parameter["+i+"] : "+appParamList[i]);
                }
            }
            Log.printInfo("[MainCtrl.startApplication]Request package name : "+packageName);
        }
        boolean isExceptAppName = DataManager.getInstance().isExceptAppName(packageName);
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainCtrl.startApplication]"+appName+" - isExceptAppName : "+isExceptAppName);
        }
        if (!isExceptAppName) {
            if (packageName == null) {
                packageName = "";
            }
            if (CommunicationManager.getInstance().requestCheckResourceAuthorization(packageName) == MonitorService.NOT_AUTHORIZED) {
                if (Log.ERROR_ON) {
                    Log.printError("["+Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM+"] Cannot parse configuration files received from the Management System.");
                }
                CommunicationManager.getInstance().requestShowErrorMessage(Rs.ERROR_CODE_CONFIG_PARSING_PROBLEM, null);
                return;
            }
        }
        //PIN Enabler
        boolean isActivePinEnabler = isActivePinEnabler(appName);
        if (isActivePinEnabler) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainCtrl.startApplication]This menu item must open PIN Enabler.");
            }
            PinEnablerListenerImpl pel = new PinEnablerListenerImpl(appName, appParamList);
            String[] msg = new String[]{
                DataCenter.getInstance().getString("TxtMiniMenu.msgUnblockPIN1"),
                DataCenter.getInstance().getString("TxtMiniMenu.msgUnblockPIN2")
            };
            PreferenceProxy.getInstance().showPinEnabler(pel, msg);
            return;
        }
        //Specifical case - Settings
        if (checkSettingsRelated(appName, appParamList)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainCtrl.startApplication]This menu item checked by settings confirmation module.");
            }
            return;
        }
        //Check whether Search app or not
        if (checkSearchRalated(appName)) {
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainCtrl.startApplication]This menu item checked by search confirmation module.");
            }
            return;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("[MainCtrl.startApplication]This menu item request start application by monitor.");
        }
        cMgr.requestStartUnboundApplication(appName, appParamList);
    }
    public boolean checkSettingsRelated(String appName, String[] extParams) {
        if (appName.equalsIgnoreCase(APP_NAME_PROFILE)) {
            boolean isProtectSettingsModifications = PreferenceProxy.getInstance().isProtectSettingsModifications();
            if (Log.DEBUG_ON) {
                Log.printDebug("[MainCtrl.startApplication]isProtectSettingsModifications : "+isProtectSettingsModifications);
            }
            if (isProtectSettingsModifications) {
                PreferenceProxy.getInstance().requestCheckRightFilter(appName, extParams);
                return true;
            }
        }
        return false;
    }
    public boolean checkSearchRalated(String appName) {
        if (appName.equalsIgnoreCase(APP_NAME_SEARCH)) {
            cMgr.requestShowSearchService();
            return true;
        }
        return false;
    }
    public void stopApplication() {
        PreferenceProxy.getInstance().setCheckingRight(false);
        if (cMgr.requestActivatedSearchService()) {
            cMgr.requestHideSearchService();
        }
    }
    
    public boolean isActivatedSearchService() {
        return cMgr.requestActivatedSearchService();
    }
    
    private boolean isActivePinEnabler(String reqAppName) {
        if (Log.INFO_ON) {
            Log.printInfo("[MainCtrl.isActivePinEnabler]Called");
            Log.printInfo("[MainCtrl.isActivePinEnabler]Request app parameter : "+reqAppName);
        }
        if (reqAppName == null) {
            if (Log.INFO_ON) {
                Log.printInfo("[MainCtrl.isActivePinEnabler]Request app name is null. return false.");
            }
            return false;
        }
        if (pinEnablerAppNames == null) {
            String restrictedAppNames = DataCenter.getInstance().getString("CONFIRM_PIN_APP_NAMES");
            if (restrictedAppNames != null) {
                pinEnablerAppNames = TextUtil.tokenize(restrictedAppNames, '|');
            } else {
                pinEnablerAppNames = new String[0];
            }
        }
        for (int i=0; i<pinEnablerAppNames.length; i++) {
            if (pinEnablerAppNames[i] == null) {
                continue;
            }
            if (reqAppName.indexOf(pinEnablerAppNames[i]) != -1) {
                if (Log.INFO_ON) {
                    Log.printInfo("[MainCtrl.isActivePinEnabler]Have to open PIN enabler. return true.");
                }
                return true;
            }
        }
        if (Log.INFO_ON) {
            Log.printInfo("[MainCtrl.isActivePinEnabler]No need to open PIN enabler. return false");
        }
        return false;
    }

    private String getStateName(int reqState) {
        String stateName = "Unknown";
        switch(reqState) {
            case MainMenuService.MAIN_MENU_CLOSE_ALL:
                stateName = "MAIN_MENU_CLOSE_ALL";
                break;
            case MainMenuService.MAIN_MENU_STATE_MANAGEMENT_SYSTEM_VALUE:
                stateName = "MAIN_MENU_STATE_MANAGEMENT_SYSTEM_VALUE";
                break;
            case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
                stateName = "MAIN_MENU_STATE_MINI_MENU";
                break;
            case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
                stateName = "MAIN_MENU_STATE_DASHBOARD";
                break;
        }
        return stateName;
    }
    
    public boolean isSearchVisible() {
    	LayeredUI[] arr = LayeredUIManager.getInstance().getAllLayeredUIs();
        if (arr == null) return false;
        for (int i = 0 ; i < arr.length ; i++) {
            if (arr[i].getName().equals("Search")) {
                return arr[i].isActive();
            }
        }
        return false;
    }
    
    public boolean isListBehaviorVisible() {
    	LayeredUI[] arr = LayeredUIManager.getInstance().getAllLayeredUIs();
        if (arr == null) return false;
        for (int i = 0 ; i < arr.length ; i++) {
            if (arr[i].getName().equals("LIST_PRESSED_IN_NON_PVR_POPUP")) {
                return arr[i].isActive();
            }
        }
        return false;
    }
}

/*
 *  NewDataManager.java
 *
 *  Copyright (c) 2015 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.mainmenu.controller;

import java.awt.FontMetrics;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.rmi.RemoteException;
import java.util.Vector;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.mainmenu.communication.CommunicationManager;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * <code>NewDataManager</code>
 * 
 * data manager for new menu, it parses JSON
 */

public class NewDataManager implements DataUpdateListener {
	static NewDataManager instance = new NewDataManager();

	static final String OOB_PORTAL = "OOB_PORTAL";

	private JSONArray mainMenu;
	private JSONObject vodParam; // only for phase1, it should be replaced on
									// phase2

	private String[][][] subParamArray;
	private String[][] subDescArray;
	private String[][] subAppName;

	private NewDataManager() {
	}

	public static NewDataManager getInstance() {
		return instance;
	}

	public void init() {
		DataCenter.getInstance().addDataUpdateListener(OOB_PORTAL, this);
	}

	public boolean readyMainMenu() {
		JSONParser parser = new JSONParser();
		try {
			byte[] src = BinaryReader.read("resource/default_data/ms_data/menuData.json");
			String dataStr = new String(src, "UTF-8");
			JSONObject obj = (JSONObject) parser.parse(dataStr);
			JSONArray mainMenu = (JSONArray) obj.get("mainMenu");

			this.mainMenu = mainMenu;
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String[][] getMainMenuLabels(int lang, int width) {
		FontMetrics fm = FontResource.getFontMetrics(FontResource.BLENDER.getBoldFont(16));
		String[][] ret = new String[mainMenu.size()][];
		for (int i = 0; i < ret.length; i++) {
			JSONObject menu = (JSONObject) mainMenu.get(i);
			JSONArray label = (JSONArray) menu.get("label");
			String str = label.get(lang).toString();
			ret[i] = TextUtil.split(str, fm, width, "|");
		}
		return ret;
	}

	public String[][][] getSubMenuLabels(int lang, int width) {
		FontMetrics fm = FontResource.getFontMetrics(FontResource.BLENDER.getBoldFont(15));
		String[][][] ret = new String[mainMenu.size()][][];
		subParamArray = new String[mainMenu.size()][][];
		subDescArray = new String[mainMenu.size()][];
		subAppName = new String[mainMenu.size()][];
		boolean isMultiroomEnabled = false;

		try {
			isMultiroomEnabled = CommunicationManager.getInstance().getMonitorService().isMultiroomEnabled();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("NewDataManager.getSubMenuLabels() : isMultiroomEnabled = " + isMultiroomEnabled);
			Log.printDebug("NewDataManager.getSubMenuLabels() : SUPPORT_DVR = " + Environment.SUPPORT_DVR);
		}

		for (int i = 0; i < ret.length; i++) {
			JSONObject menu = (JSONObject) mainMenu.get(i);

			if (menu.get("menuId").equals("200")) {
				JSONArray subMenu = (JSONArray) menu.get("subMenu");

				Vector retVec = new Vector();
				Vector parmVec = new Vector();
				Vector descVec = new Vector();
				Vector appNameVec = new Vector();

				for (int j = 0; j < subMenu.size(); j++) {
					JSONObject sub = (JSONObject) subMenu.get(j);
					if (j == 0 && (Environment.SUPPORT_DVR || isMultiroomEnabled)) {
						JSONArray label = (JSONArray) sub.get("label");
						String str = label.get(lang).toString();
						retVec.add(TextUtil.split(str, fm, width, "|"));
						parmVec.add(getSubMenuParams(i, j));
						JSONArray desc = (JSONArray) sub.get("desc");
						if (desc != null) {
							descVec.add(desc.get(lang).toString());
						}
						appNameVec.add(sub.get("app"));
					} else if (j < 3 && Environment.SUPPORT_DVR) {
						JSONArray label = (JSONArray) sub.get("label");
						String str = label.get(lang).toString();
						retVec.add(TextUtil.split(str, fm, width, "|"));
						parmVec.add(getSubMenuParams(i, j));
						JSONArray desc = (JSONArray) sub.get("desc");
						if (desc != null) {
							descVec.add(desc.get(lang).toString());
						}
						appNameVec.add(sub.get("app"));
					} else if (j == 3 && !Environment.SUPPORT_DVR && !isMultiroomEnabled) {
						JSONArray label = (JSONArray) sub.get("label");
						String str = label.get(lang).toString();
						retVec.add(TextUtil.split(str, fm, width, "|"));
						parmVec.add(getSubMenuParams(i, j));
						JSONArray desc = (JSONArray) sub.get("desc");
						if (desc != null) {
							descVec.add(desc.get(lang).toString());
						}
						appNameVec.add(sub.get("app"));
					}
				}

				ret[i] = new String[retVec.size()][];
				subParamArray[i] = new String[retVec.size()][];
				subDescArray[i] = new String[retVec.size()];
				subAppName[i] = new String[retVec.size()];
				for (int j = 0; j < retVec.size(); j++) {
					ret[i][j] = (String[]) retVec.get(j);
					subParamArray[i][j] = (String[]) parmVec.get(j);
					subDescArray[i][j] = (String) descVec.get(j);
					subAppName[i][j] = (String) appNameVec.get(j);
				}
				
				

			} else {
				JSONArray subMenu = (JSONArray) menu.get("subMenu");
				ret[i] = new String[subMenu.size()][];
				subParamArray[i] = new String[subMenu.size()][];
				subDescArray[i] = new String[subMenu.size()];
				subAppName[i] = new String[subMenu.size()];
				for (int j = 0; j < ret[i].length; j++) {
					JSONObject sub = (JSONObject) subMenu.get(j);
					JSONArray label = (JSONArray) sub.get("label");
					String str = label.get(lang).toString();
					ret[i][j] = TextUtil.split(str, fm, width, "|");

					subParamArray[i][j] = getSubMenuParams(i, j);

					JSONArray desc = (JSONArray) sub.get("desc");
					if (desc != null) {
						subDescArray[i][j] = desc.get(lang).toString();
					}
					subAppName[i][j] = (String) sub.get("app");
				}
			}
		}
		return ret;
	}
	
	public String getMainMenuApp(int idx) {
		JSONObject menu = (JSONObject) mainMenu.get(idx);
		String appName = (String) menu.get("app");
		
		return appName;
	}
	
	public String getSubMenuAppName(int rootIdx, int subIdx) {
		return subAppName[rootIdx][subIdx];
	}

	public String[] getMainMenuParams(int idx) {
		JSONObject menu = (JSONObject) mainMenu.get(idx);
		JSONArray params = (JSONArray) menu.get("param");

		Vector paramVec = new Vector();
		for (int i = 0; i < params.size(); i++) {
			paramVec.add(params.get(i));
		}

		String[] retParams = new String[paramVec.size()];
		paramVec.toArray(retParams);

		return retParams;
	}

	private String[] getSubMenuParams(int rootIdx, int subIdx) {
		JSONObject menu = (JSONObject) mainMenu.get(rootIdx);
		JSONArray subMenuArray = (JSONArray) menu.get("subMenu");
		JSONObject subMenu = (JSONObject) subMenuArray.get(subIdx);
		JSONArray params = (JSONArray) subMenu.get("param");

		Vector paramVec = new Vector();
		for (int i = 0; i < params.size(); i++) {
			paramVec.add(params.get(i));
		}

		String[] retParams = new String[paramVec.size()];
		paramVec.toArray(retParams);

		return retParams;
	}
	
	// R7
	public String getMainMenuId(int idx) {
		JSONObject menu = (JSONObject) mainMenu.get(idx);
		String menuId = (String) menu.get("menuId");
		
		return menuId;
	}
	
	// R7
	public String getSubMenuId(int rootIdx, int subIdx) {
		JSONObject menu = (JSONObject) mainMenu.get(rootIdx);
		JSONArray subMenuArray = (JSONArray) menu.get("subMenu");
		JSONObject subMenu = (JSONObject) subMenuArray.get(subIdx);
		String menuId = (String) subMenu.get("menuId");
		
		return menuId;
	}

	public String[] getBuildSubMenuParams(int rootIdx, int subIdx) {

		return subParamArray[rootIdx][subIdx];
	}

	public String getBuildSubMenuDesc(int rootIdx, int subIdx) {
		return subDescArray[rootIdx][subIdx];
	}

	public JSONObject readyVodParam() {

		if (vodParam != null) {
			return vodParam;
		}

		JSONParser parser = new JSONParser();
		try {
			// TODO integrate with MS, DataCenter
			FileReader fr = new FileReader("resource/default_data/ms_data/vodParam.json");
			JSONObject obj = (JSONObject) parser.parse(fr);
			JSONObject vodParam = (JSONObject) obj.get("vodParam");
			this.vodParam = vodParam;
			return vodParam;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void updateVodParam(File f) {
		if (Log.DEBUG_ON) {
			Log.printDebug("NewDataManager.updateVodParam() : file = " + f);
			Log.printDebug("NewDataManager.updateVodParam() : SUPPORT_DVR = " + Environment.SUPPORT_DVR);
		}

		byte[] data = BinaryReader.read(f);
		JSONParser parser = new JSONParser();
		try {
			JSONObject rootJObj = (JSONObject) parser.parse(new String(data, "UTF-8"));
			String versionStr = (String) rootJObj.get("version");

			if (Log.DEBUG_ON) {
				Log.printDebug("NewDataManager.updateVodParam() : version = " + versionStr);
			}

			JSONObject newVodParam = (JSONObject) rootJObj.get("vodParam");
			readyVodParam();

			// COD
			JSONArray codParam = (JSONArray) newVodParam.get("channelOnDemand");
			if (Log.DEBUG_ON) {
				Log.printDebug("NewDataManager.updateVodParam() : newVodParam.channelOnDemand = " + codParam.toString());
			}
			codParam.add(0, "Menu");
			((JSONObject) vodParam.get("channelOnDemand")).put("rootParams", codParam);
			((JSONObject) vodParam.get("channelOnDemand")).put("allChannels", codParam);

			// Club illico
			JSONArray clubIllicoParam = (JSONArray) newVodParam.get("clubIllico");
			clubIllicoParam.add(0, "Menu");
			if (Log.DEBUG_ON) {
				Log.printDebug("NewDataManager.updateVodParam() : newVodParam.clubIllico = "
						+ clubIllicoParam.toString());
			}
			((JSONObject) vodParam.get("clubIllico")).put("rootParams", clubIllicoParam);
			((JSONObject) vodParam.get("clubIllico")).put("allTitles", clubIllicoParam);

			// Club illico - New Arrival
			JSONArray newArrivalParam = (JSONArray) newVodParam.get("newArrival");
			newArrivalParam.add(0, "Menu");
			if (Log.DEBUG_ON) {
				Log.printDebug("NewDataManager.updateVodParam() : with add menu, newVodParam.newArrival = "
						+ newArrivalParam.toString());
			}
			((JSONObject) vodParam.get("clubIllico")).put("newArrivals", newArrivalParam);

			// Most Popular
			JSONArray mostPopularParam = (JSONArray) newVodParam.get("mostPopular");
			if (Log.DEBUG_ON) {
				Log.printDebug("NewDataManager.updateVodParam() : newVodParam.mostPopularParam = "
						+ mostPopularParam.toString());
			}
			mostPopularParam.add(0, "Menu");
			((JSONObject) vodParam.get("moviesAndSeries")).put("mostPopular", mostPopularParam);

		} catch (UnsupportedEncodingException e) {
			Log.print(e);
		} catch (ParseException e) {
			Log.print(e);
		}
	}

	public void dataRemoved(String key) {
	}

	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("NewDataManager.dataUpdated() : key=" + key + ", old=" + old + ", value=" + value);
		}
		if (key.equals(OOB_PORTAL)) {
			updateVodParam((File) value);
		}
	}
}

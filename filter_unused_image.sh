#!/bin/sh
filter()
{
    cp ./UnusedImageFilter.class $@
    echo ' '
    echo ' '
    echo ' '
    echo 'move to ----------------> ['$@']'
    echo ' '
    cd $@
    java UnusedImageFilter $@
    rm UnusedImageFilter.class
    cd ..
}

filter_all ()
{
    rm -rf unused_image_filtering_result
    mkdir unused_image_filtering_result
    filter callerid;
    filter cinema;
    filter cyo;
    filter daemon;
    filter dashboard;
    filter errormessage;
    filter flipbar;
    filter framework;
    filter galaxie;
    filter help;
    filter ixc;
    filter isa;
    filter keyboard;
    filter loading;
    filter lottery;
    filter mainmenu;
    filter monitor;
    filter notification;
    filter pastille;
    filter pcs;
    filter preview;
    filter screensaver;
    filter search;
    filter upp;
    filter vod;
    filter weather;
    filter wizard;
    filter epg;
    filter pvr;
}

filter_all

package com.videotron.tvi.illico.util;

import java.util.Vector;
import java.util.Enumeration;
import java.awt.Color;
import org.dvb.ui.DVBColor;
/**
 * Utility class to get Color object easily
 *
 * @author Kenneth
 */
public class ColorUtil {
    private static Vector colorCache = new Vector();
    public static Color getColor(int r, int g, int b) {
        return getColor(r, g, b, 255);
    }

    public static Color getColor(int r, int g, int b, int a) {
        if (!colorCache.isEmpty()) {
            Enumeration enu = colorCache.elements();
            while (enu.hasMoreElements()) {
                DVBColor tmp = (DVBColor)enu.nextElement();
                if (tmp.getRed() == r
                        && tmp.getGreen() == g
                        && tmp.getBlue() == b
                        && tmp.getAlpha() == a) {
                    return tmp;
                }
            }
        }
        Color color = new DVBColor(r, g, b, a);
        colorCache.addElement(color);
        return color;
    }

    public static void dispose() {
        if (colorCache != null) {
            colorCache.removeAllElements();
            colorCache = null;
        }
    }
}

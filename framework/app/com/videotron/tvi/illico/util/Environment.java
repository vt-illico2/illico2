package com.videotron.tvi.illico.util;

import javax.tv.service.Service;
import javax.tv.service.selection.InsufficientResourcesException;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextFactory;
import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.havi.ui.HScreen;
import org.havi.ui.HVideoDevice;
import org.havi.ui.HVideoConfiguration;
import org.ocap.service.AbstractService;

import com.videotron.tvi.illico.log.Log;

/**
 * This class is the utility class that contains Environment fields.
 *
 * @version $Revision: 1.21 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public final class Environment {

    /** vendor id for SAMSUNG. */
    public static final int HW_VENDOR_SAMSUNG = 8524;
    /** vendor id for CISCO. */
    public static final int HW_VENDOR_CISCO = 734;
    /** vendor id for Emulator. */
    public static final int HW_VENDOR_EMULATOR = Integer.MIN_VALUE;

    /** hardware vendor id. */
    public static final int HW_VENDOR;

    public static final String VENDOR_NAME;

    /** true, if current runtime is running under the Emulator. */
    public static final boolean EMULATOR;

    /** the number of tuners provided by this system. */
    public static final int TUNER_COUNT;

    /** the number of video devices provided by this system. */
    public static final int VIDEO_DEVICE_COUNT;

    /** true, if the PIP functionality can be supported by this system. */
    public static final boolean SUPPORT_PIP;

    /** true, if the DVR functionality is supported by this system. */
    public static final boolean SUPPORT_DVR;

	//->Kenneth : by June
    public static final int VIDEO_HEIGHT;

    //->Kenneth[2015.1.30] 4K
    public static boolean SUPPORT_UHD = false;

    static {
        // HW_VENDOR
        String vendorString = System.getProperty("ocap.hardware.vendor_id");
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.vendorString = "+vendorString); 
        } catch (Throwable e) {
        }
        int vendorId = 0;
        try {
            vendorId = Integer.parseInt(vendorString);
        } catch (NumberFormatException ex) {
            if ("alticast".equals(vendorString)) {
                vendorId = HW_VENDOR_EMULATOR;
            } else if ("CISCO".equalsIgnoreCase(vendorString)) {
                vendorId = HW_VENDOR_CISCO;
            }
        }
        HW_VENDOR = vendorId;
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.HW_VENDOR = "+HW_VENDOR); 
        } catch (Throwable e) {
        }

        switch (Environment.HW_VENDOR) {
            case Environment.HW_VENDOR_CISCO:
                VENDOR_NAME = "CISCO";
                break;
            case Environment.HW_VENDOR_SAMSUNG:
                VENDOR_NAME = "SAMSUNG";
                break;
            case Environment.HW_VENDOR_EMULATOR:
                VENDOR_NAME = "EMULATOR";
                break;
            default:
                VENDOR_NAME = "UNKNOWN";
                break;
        }
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.VENDOR_NAME = "+VENDOR_NAME); 
        } catch (Throwable e) {
        }

        // EMULATOR
        EMULATOR = HW_VENDOR == HW_VENDOR_EMULATOR;

        // TUNER_COUNT
        int tunerCount = 1;
        try {
            NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
            if (nis != null && nis.length > 0) {
                tunerCount = nis.length;
            } else {
                tunerCount = 1;
            }
        } catch (Exception ex) {
        }
        TUNER_COUNT = tunerCount;
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.TUNER_COUNT = "+TUNER_COUNT); 
        } catch (Throwable e) {
        }

        int videoCount = 1;
		//->Kenneth : by June
        int videoHeight = 0;
        try {
            // VIDEO_DEVICE_COUNT
            HVideoDevice[] videos = HScreen.getDefaultHScreen().getHVideoDevices();
            if (videos != null && videos.length > 0) {
                videoCount = videos.length;
				//->Kenneth : by June
                int height = 0;
                for (int i = 0; i < videoCount; i++) {
                    HVideoConfiguration[] configs = videos[i].getConfigurations();
                    if (configs != null) {
                        for (int j = 0; j < configs.length; j++) {
                            videoHeight = Math.max(videoHeight, configs[j].getPixelResolution().height);
                        }
                    }
                }
            } else {
                videoCount = 1;
            }
        } catch (Exception ex) {
        }
        VIDEO_DEVICE_COUNT = videoCount;
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.VIDEO_DEVICE_COUNT = "+VIDEO_DEVICE_COUNT); 
        } catch (Throwable e) {
        }
		//->Kenneth : by June
        VIDEO_HEIGHT = Math.max(1080, videoHeight);
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.VIDEO_HEIGHT = "+VIDEO_HEIGHT); 
        } catch (Throwable e) {
        }

        // SUPPORT_PIP
        SUPPORT_PIP = Math.min(TUNER_COUNT, VIDEO_DEVICE_COUNT) >= 2;
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.SUPPORT_PIP = "+SUPPORT_PIP); 
        } catch (Throwable e) {
        }

        // SUPPORT_DVR
        String str = System.getProperty("ocap.api.option.dvr");
        SUPPORT_DVR = (str != null && !str.startsWith("0.")) || EMULATOR;
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.SUPPORT_DVR = "+SUPPORT_DVR); 
        } catch (Throwable e) {
        }

//        MODEL_ID = System.getProperty("ocap.hardware.model_id");

        //->Kenneth[2015.1.30] 이명구책임과 havi 에서 1080 보다 크면 UHD 인 것으로 결정
        // 테스트를 위한 hard coding
        //[2015.2.5] UHDAvailabilityPackage 를 통해서 UHD 인지 판단하는 것으로 해야함
        //SUPPORT_UHD = false;
//        if (VIDEO_HEIGHT > 1080) {
//            SUPPORT_UHD = true;
//        } else {
//            SUPPORT_UHD = false;
//        }
        //->Kenneth[2015.4.13] 빌드할때 write version 할 때 얘땜에 에러남
        try {
            if (Log.DEBUG_ON) Log.printDebug("Environment.SUPPORT_UHD = "+SUPPORT_UHD); 
        } catch (Throwable e) {
        }

    }

    /** ServiceContexts can be used by application. */
    private static ServiceContext[] serviceContexts;

    /** private constructor for utility class. */
    private Environment() {
    }

    /**
     * Returns the service context for the specified tuner index.
     *
     * @param tunerIndex index of tuner.
     * @return ServiceContext.
     */
    public static ServiceContext getServiceContext(int tunerIndex) {
        if (tunerIndex < 0 || tunerIndex >= TUNER_COUNT) {
            if (Log.DEBUG_ON) {
                Log.printDebug("Environment: tunerIndex is out of range = " + tunerIndex);
            }
            return null;
        }
        if (serviceContexts == null) {
            createServiceContexts();
        }
        return serviceContexts[tunerIndex];
    }

    public static int getIndex(ServiceContext sc) {
        if (sc == null) {
            return -1;
        }
        for (int i = 0; i < TUNER_COUNT; i++) {
            if (getServiceContext(i) == sc) {
                return i;
            }
        }
        return -1;
    }

    /** Creates the service contexts. */
    private static void createServiceContexts() {
        Object o = SharedMemory.getInstance().get("ServiceContexts");
        if (o != null && o instanceof ServiceContext[]) {
            serviceContexts = (ServiceContext[]) o;
            return;
        }

        serviceContexts = new ServiceContext[TUNER_COUNT];
        int index = 0;

        ServiceContextFactory scf = ServiceContextFactory.getInstance();
        ServiceContext[] contexts = scf.getServiceContexts();
        if (contexts != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("contexts.length = " + contexts.length);
            }
            for (int i = 0; i < contexts.length && index < TUNER_COUNT; i++) {
                if (contexts[i] == null) {
                    continue;
                }
                Service svc = contexts[i].getService();
                if (svc != null && svc instanceof AbstractService) {
                    continue;
                }
                serviceContexts[index] = contexts[i];
                if (Log.DEBUG_ON) {
                    Log.printDebug("Environment: ServiceContext saved " + index + " <- #" + i);
                }
                index++;
            }
        }

        for (int i = index; i < serviceContexts.length; i++) {
            try {
                serviceContexts[i] = scf.createServiceContext();
            } catch (InsufficientResourcesException ex) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Environment: can't create ServiceContext. index=" + index);
                }
                break;
            }
        }
        SharedMemory.getInstance().put("ServiceContexts", serviceContexts);
    }


}

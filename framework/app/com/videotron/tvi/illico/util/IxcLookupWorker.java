/*
 * $RCSfile: IxcLookupWorker.java,v $ $Revision: 1.5 $ $Date: 2017/01/12 19:20:15 $
 *
 * Copyright 1999-2002 by Alticast Corp. All rights reserved.
 */
package com.videotron.tvi.illico.util;

import com.videotron.tvi.illico.log.Log;
import java.util.*;
import java.rmi.Remote;
import java.rmi.NotBoundException;
import javax.tv.xlet.XletContext;
import org.dvb.io.ixc.IxcRegistry;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;

/**
 * This class is a worker class to lookup IXC services.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class IxcLookupWorker extends Vector implements Runnable {

    private static final long LOOK_UP_RETRY_DELAY = 2000L;

    private XletContext xletContext;
    private Thread thread;

    public IxcLookupWorker(XletContext xletContext) {
        this.xletContext = xletContext;
    }

    private synchronized void startNewThread() {
        Thread t = new Thread(this, FrameworkMain.getInstance().getApplicationName() + ".IxcLookupWorker");
        t.start();
        thread = t;
    }

    public synchronized void lookup(String objectPath, String objectName) {
        lookup(objectPath, objectName, null);
    }

    public synchronized void lookup(String objectPath, String objectName, DataUpdateListener l) {
        if (l != null) {
            DataCenter.getInstance().addDataUpdateListener(objectName, l);
        }
        LookupItem item = new LookupItem(objectPath, objectName);
        addElement(item);
        if (Log.DEBUG_ON) {
            Log.printDebug("IxcLookupWorker: added = " + objectName + ", size = " + size());
        }
        if (thread == null) {
            startNewThread();
        }
    }

    public void run() {
        while (true) {
            int size;
            synchronized (this) {
                size = size();
                if (Log.DEBUG_ON) {
                    Log.printDebug("IxcLookupWorker: running, size = " + size);
                }
                if (size <= 0) {
                    thread = null;
                    return;
                }
            }
            for (int i = size - 1; i >= 0; i--) {
                LookupItem item = (LookupItem) elementAt(i);
                boolean success = item.lookup();
                if (success) {
                    removeElementAt(i);
                }
            }
            try {
                Thread.sleep(LOOK_UP_RETRY_DELAY);
            } catch (InterruptedException e) {
            }
        }
    }

    class LookupItem {
        String fullName;
        String objectName;

        public LookupItem(String objectPath, String objectName) {
            this.fullName = objectPath + objectName;
            this.objectName = objectName;
        }

        public boolean lookup() {
            Remote remote = null;
            try {
                remote = IxcRegistry.lookup(xletContext, fullName);
            } catch (Exception ex) {
                if (Log.INFO_ON) {
                    Log.printInfo("IxcLookupWorker: not bound - " + objectName);
                }
            }
            if (remote == null) {
                return false;
            }
            if (Log.INFO_ON) {
                Log.printInfo("IxcLookupWorker: found - " + objectName);
            }
            try {
                DataCenter.getInstance().put(objectName, remote);
            } catch (Throwable t) {
                Log.print(t);
            }
            return true;
        }
    }

}



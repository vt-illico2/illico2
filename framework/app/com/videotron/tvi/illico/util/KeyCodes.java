package com.videotron.tvi.illico.util;

import org.ocap.ui.event.OCRcEvent;

/**
 * This class contains the constant key code variables.
 *
 * @version $Revision: 1.18 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public final class KeyCodes {
    /** Color A (yellow) key code. */
    public static final int COLOR_A     = OCRcEvent.VK_COLORED_KEY_3;
    /** Color B (blue) key code. */
    public static final int COLOR_B     = OCRcEvent.VK_COLORED_KEY_2;
    /** Color C (red) key code. */
    public static final int COLOR_C     = OCRcEvent.VK_COLORED_KEY_0;
    /** Color C (green) key code. */
    public static final int COLOR_D     = OCRcEvent.VK_COLORED_KEY_1;

    /** 'LAST' key code. */
    public static final int LAST        = OCRcEvent.VK_LAST;
    /** 'REPLAY' key code. */
    public static final int REPLAY      = OCRcEvent.VK_PREVIOUS_CANDIDATE;

    /** 'VOD' key code. */
    public static final int VOD         = OCRcEvent.VK_ON_DEMAND;
    /** 'MENU' key code. */
    public static final int MENU        = OCRcEvent.VK_MENU;
    /** 'FAV' key code. */
    public static final int FAV         = OCRcEvent.VK_NEXT_FAVORITE_CHANNEL;

    /** 'REWIND' key code. */
    public static final int REWIND      = OCRcEvent.VK_REWIND;
    /** 'FAST_FWD' key code. */
    public static final int FAST_FWD    = OCRcEvent.VK_FAST_FWD;
    /** 'PLAY' key code. */
    public static final int PLAY        = OCRcEvent.VK_PLAY;
    /** 'PAUSE' key code. */
    public static final int PAUSE       = OCRcEvent.VK_PAUSE;
    /** 'STOP' key code. */
    public static final int STOP        = OCRcEvent.VK_STOP;
    /** 'RECORD' key code. */
    public static final int RECORD      = OCRcEvent.VK_RECORD;
    /** 'LIVE' key code. */
    public static final int LIVE        = OCRcEvent.VK_LIVE;

    /** 'FORWARD' key code. */
    public static final int FORWARD     = OCRcEvent.VK_FORWARD;
    /** 'BACK' key code. */
    public static final int BACK        = OCRcEvent.VK_BACK;

    /** 'LIVE' key code. */
    public static final int LIST        = OCRcEvent.VK_LIST;

    /** 'PIP' key code. */
    public static final int PIP         = OCRcEvent.VK_PINP_TOGGLE;
    /** 'PIP MOVE' key code. */
    public static final int PIP_MOVE    = OCRcEvent.VK_PINP_MOVE;

    /** 'Widget' key code. */
    public static final int WIDGET      = OCRcEvent.VK_RESERVE_1;
    /** 'Search' key code. */
    public static final int SEARCH      = OCRcEvent.VK_RESERVE_3;
    /** 'SETTINGS' key code. */
    public static final int SETTINGS    = OCRcEvent.VK_SETTINGS;

    public static final int NEXT_DAY    = OCRcEvent.VK_NEXT_DAY;

    public static final int ASPECT      = 520;
    
    public static final int LITTLE_BOY 	= OCRcEvent.VK_RESERVE_2;

    //->Kenneth[2015.8.18] DDC-113
    public static final int STAR        = OCRcEvent.VK_ASTERISK;
    //<-

    /** private constructor for utility class. */
    private KeyCodes() {
    }


}

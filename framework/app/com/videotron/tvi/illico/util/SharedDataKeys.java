package com.videotron.tvi.illico.util;

import java.lang.reflect.Method;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is the Wrapper class of com.alticast.util.SharedMemory
 * to prevent NoClassDefFoundError.
 * If the class is not supported by system, it does nothing.
 *
 * @author  June Park
 */
public final class SharedDataKeys {

    /** value = int[]. (01:13) */
    public static final String OOB_RELOAD_TIME = "OOB_RELOAD_TIME";
    /** value = Hashtable. */
    public static final String OOB_ADAPTER_TABLE = "OOB_ADAPTER_TABLE";

    public static final String MS_URL = "MS_URL";
    public static final String VOD_POSTER_IMAGE = "VOD_POSTER_IMAGE";
    public static final String IMAGE_POOL_STATUS = "ImagePoolStatus";
    public static final String VBM_DEBUG_LOGS = "VBM_DEBUG_LOGS";
    public static final String COMPANION_DEBUG_LOGS = "COMPANION_DEBUG_LOGS";

    /**
     * SharedMemory key for channels. (value = Object[][6])
     * number (Integer), name (String), logo (Image), recordable (Boolean),
     * type (Integer), channel id (Integer).
     */
    public static final String EPG_CHANNELS = "Epg.Channels";

    /**
     * SharedMemory key for Channel CA Map. (value = Hashtable<Integer, Boolean>)
     */
    public static final String EPG_CA_MAP = "Epg.CaMap";

    public static final String EPG_MC_FREQ = "Epg.MC_FREQ";


    /** Program name of VOD or PVR.  (value = String) */
    public static final String VIDEO_PROGRAM_NAME = ".ProgramName";
    /** Video resolution of VOD or PVR.  (value = Boolean) */
    public static final String VIDEO_RESOLUTION = ".Resolution";
	//->Kenneth : by June
    /** Video definition of VOD or PVR.  (value = 0, 1, 2 for SD, HD, 4K) */
    public static final String VIDEO_DEFINITION = ".Definition";

	/**
     * VOD Image URL
     */
    public static final String VOD_POSTER_URL = "vod.image.url";

    /** private constructor for utility class. */
    private SharedDataKeys() {
    }

}

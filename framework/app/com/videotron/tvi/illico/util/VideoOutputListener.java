/**
 * 
 */
package com.videotron.tvi.illico.util;

/**
 * @author zestyman
 *
 */
public interface VideoOutputListener {
	void updateVideoOutputStatus();
}

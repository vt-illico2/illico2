package com.videotron.tvi.illico.util;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

/**
 * This class manages the system clock and provies the listener interface to receive
 * the changes of clock in every minutes. And also, manages the current clock texts.
 *
 * @version $Revision: 1.23 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public class Clock implements TVTimerWentOffListener {
    /** TVTimer. */
    private TVTimerSpec timer;
    /** ClockListeners. */
    private Vector listeners = new Vector();
    /** Singleton instance. */
    private static Clock instance = new Clock();

    /**
     * Returns the singleton instance of Clock.
     * @return Clock object.
     */
    public static Clock getInstance() {
        return instance;
    }

    /** Constuctor. */
    protected Clock() {
        timer = new TVTimerSpec();
        timer.setRepeat(false);
        timer.setAbsolute(true);
        timer.addTVTimerWentOffListener(this);
        startTimer(System.currentTimeMillis());
    }


    /** Starts the Clock timer. */
    private void startTimer(long current) {
        long time = current;
        // caculating next minute. (0.1 second as a margin of error)
//        time = (time / Constants.MS_PER_MINUTE + 1) * Constants.MS_PER_MINUTE
//                                    + Constants.MS_PER_SECOND / 10;
        time = (time / Constants.MS_PER_MINUTE + 1) * Constants.MS_PER_MINUTE;
        timer.setTime(time);

        synchronized (timer) {
            // to prevent multiple schedule.
            TVTimer.getTimer().deschedule(timer);
            try {
                TVTimer.getTimer().scheduleTimerSpec(timer);
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Registers a clock listener to notify update of clock.
     * @param l listener to add.
     */
    public void addClockListener(ClockListener l) {
        synchronized (listeners) {
            if (!listeners.contains(l)) {
                listeners.addElement(l);
            }
        }
    }

    /**
     * Removes a clock listener.
     * @param l listener to remvoe.
     */
    public void removeClockListener(ClockListener l) {
        synchronized (listeners) {
            listeners.removeElement(l);
        }
    }

    /**
     * Send an event to registered clock listeners.
     * @param date current date.
     */
    private void fireClockListener(Date date) {
        Enumeration en = listeners.elements();
        while (en.hasMoreElements()) {
            try {
                ((ClockListener) en.nextElement()).clockUpdated(date);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    public static boolean isMidnight(long time) {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(time));
        return c.get(Calendar.MINUTE) == 0 && c.get(Calendar.HOUR_OF_DAY) == 0;
    }

    public static long getRoundedTime(long time, long unit) {
        return Math.round(((double) time) / unit) * unit;
    }

    public static long getRoundedMillis() {
        return getRoundedTime(System.currentTimeMillis(), Constants.MS_PER_SECOND);
    }

    /**
     * TVTimerWentOffListener implementation. Called in every minute.
     * @param e TVTimerWentOffEvent.
     */
    public void timerWentOff(TVTimerWentOffEvent e) {
        long time = getRoundedTime(System.currentTimeMillis(), Constants.MS_PER_MINUTE);
        Date date = new Date(time);
        if (Log.INFO_ON) {
            Log.printInfo("Clock: timer hit = " + date + " ");
        }
        fireClockListener(date);
        // waits util next minutes.
        startTimer(time);
    }

}

package com.videotron.tvi.illico.util;

import com.videotron.tvi.illico.log.Log;
import java.util.*;

public class Worker extends Vector implements Runnable {

    private Thread thread;
    private boolean keepThread;
    protected String threadName;

    private boolean alive = true;

    public Worker(String name) {
        this(name, true);
    }

    public Worker(String name, boolean keepThread) {
        super(20);
        this.threadName = name;
        this.keepThread = keepThread;
        if (keepThread) {
            startNewThread();
        }
    }

    private synchronized void startNewThread() {
        Thread t = new Thread(this, threadName);
        t.start();
        thread = t;
    }

    public synchronized void push(Runnable r) {
        if (!alive) {
            if (Log.WARNING_ON) {
                Log.printWarning(threadName + ": is disposed. not added = " + r);
            }
            return;
        }
        if (contains(r)) {
            if (Log.INFO_ON) {
                Log.printInfo(threadName + ": already added = " + r);
            }
            return;
        }
        addElement(r);
        if (Log.INFO_ON) {
            Log.printInfo(threadName + ": added = " + r + " : size = " + size());
        }
        if (keepThread) {
            notify();
        } else {
            if (thread == null) {
                startNewThread();
            }
        }
    }

    public synchronized void dispose() {
        this.alive = false;
        if (keepThread) {
            notify();
        }
        clear();
    }

    public void run() {
        process();
        if (Log.INFO_ON) {
            Log.printInfo(threadName + ": is being terminated");
        }
    }

    private void process() {
        Runnable r = null;
        while (alive) {
            synchronized (this) {
                while (size() <= 0) {
                    if (!alive) {
                        if (Log.INFO_ON) {
                            Log.printInfo(threadName + ": will be disposed");
                        }
                        return;
                    } else if (keepThread) {
                        try {
                            this.wait();
                        } catch (InterruptedException ignored) {
                        }
                    } else {
                        thread = null;
                        if (Log.INFO_ON) {
                            Log.printInfo(threadName + ": end of thread");
                        }
                        return;
                    }
                }
                if (!alive) {
                    if (Log.INFO_ON) {
                        Log.printInfo(threadName + ": will be disposed");
                    }
                    return;
                }
                try {
                    r = (Runnable) remove(0);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
            if (Log.INFO_ON) {
                Log.printInfo(threadName + ": processing " + r);
            }
            try {
                r.run();
            } catch (Throwable t) {
                Log.print(t);
            }
            r = null;
        }
    }
}

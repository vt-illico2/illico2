package com.videotron.tvi.illico.util;

import java.util.*;

//->Kenneth[2017.8.22] R7.4 요구 사항에 의해서 FR accents character 를 EN character 로 변경하는 클래스
public class FrTranscoder {
    private static final String FR_CHARS = "AÀÁÂÃÄÅCÇEÈÉÊËIÌÍÎÏDÐNÑOÒÓÔÕÖØUÙÚÛÜYÝaàáâãäåcçeèéêëiìíîïnñoòóôõöøuùúûüyýÿÆæŒœ";
    // Æ -> AE, æ -> ae, Œ -> OE, œ -> oe : 이처럼 하나의 char 가 2 개로 바뀌는 경우 있음. 얘들은 맨 뒤로 보내서 init()에서 special 처리함
    private static final String EN_CHARS = "AAAAAAACCEEEEEIIIIIDDNNOOOOOOOUUUUUYYaaaaaaacceeeeeiiiiinnooooooouuuuuyyyAEaeOEoe";
    private Vector frVector = new Vector();
    private Vector enVector = new Vector();
    private static FrTranscoder transcoder = null;

    public static FrTranscoder getInstance() {
        if (transcoder == null) {
            transcoder = new FrTranscoder();
        }
        return transcoder;
    }

    private FrTranscoder() {
        init();
    }

    private void init() {
        int size = FR_CHARS.length();
        for (int i = 0 ; i < size ; i++) {
            char aChar = FR_CHARS.charAt(i);
            frVector.addElement(Character.toString(aChar));

            if (aChar == 'Æ') {
                enVector.addElement("AE");
            } else if (aChar == 'æ') {
                enVector.addElement("ae");
            } else if (aChar == 'Œ') {
                enVector.addElement("OE");
            } else if (aChar == 'œ') {
                enVector.addElement("oe");
            } else {
                enVector.addElement(Character.toString(EN_CHARS.charAt(i)));
            }
        }
    }

    public String transcode(String src) {
        if (src == null) return null;
        char[] srcChars = src.toCharArray();
        StringBuffer dstChars = new StringBuffer();
        for (int i = 0 ; i < srcChars.length ; i++) {
            String aChar = Character.toString(srcChars[i]);
            if (frVector.contains(aChar)) {
                int index = frVector.indexOf(aChar);
                dstChars.append((String)enVector.elementAt(index));
            } else {
                dstChars.append(srcChars[i]);
            }
        }
        return dstChars.toString();
    }
}

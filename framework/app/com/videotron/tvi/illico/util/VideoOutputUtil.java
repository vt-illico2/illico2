/**
 * 
 */
package com.videotron.tvi.illico.util;

import java.awt.Dimension;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Vector;

import org.ocap.hardware.Host;
import org.ocap.hardware.VideoOutputPort;
import org.ocap.hardware.device.FixedVideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputConfiguration;
import org.ocap.hardware.device.VideoOutputPortListener;
import org.ocap.hardware.device.VideoOutputSettings;
import org.ocap.hardware.device.VideoResolution;

import com.videotron.tvi.illico.log.Log;

/**
 * @author zestyman
 *
 */
public class VideoOutputUtil implements VideoOutputPortListener {
	
	private static VideoOutputUtil instance;
	
	private VideoOutputPort hdmiOutputPort;
	private VideoOutputPort componentOutputPort;
	private VideoOutputPort sVideoOutputPort;
	private VideoOutputPort bbOutputPort;
	
	private boolean isSupportedUHD = false;
	
	private boolean connectedHDMI = false;
	
	private Vector listenerList = new Vector();
    
    private boolean allPortsBlocked = false;
    
    // HDCP2.2
    private boolean existHdcpApi = false;
    private boolean isSupportedHdcp22 = false;
    Class hdcpClass;
    Method hdcpMethod;
    
    
	
	private VideoOutputUtil() {
	}
	
	public static VideoOutputUtil getInstance() {
		
		if (instance == null) {
			instance = new VideoOutputUtil();
			instance.init();
		}
		
		return instance;
	}

    /*
    private boolean isCisco() {
        if (Log.DEBUG_ON) Log.printDebug("VideoOutputUtil.isCisco() : VENDOR_NAME = "+Environment.VENDOR_NAME); 
        if ("CISCO".equals(Environment.VENDOR_NAME)) {
            return true;
        }
        return false;
    }
    */
	
	
	/**
	 * This API should be called before lookup PreferenceService to update a value of TV picture format
	 */
	private void init() {
		Log.printInfo("VideoOutputUtil, init()");
		
		Enumeration e = Host.getInstance().getVideoOutputPorts();
		
		while (e.hasMoreElements()) {
            try {
                VideoOutputPort videoPort = (VideoOutputPort) e.nextElement();
                
                if (videoPort.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
                    Log.printDebug("VideoOutputUtil, init(), found HDMI port");
                    hdmiOutputPort = videoPort;
                    VideoOutputSettings setting = (VideoOutputSettings) hdmiOutputPort;
                    if (videoPort.status() == false) {
                        Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is disabled");
                        connectedHDMI = false;
                        isSupportedUHD = false;
                    } else if (setting.isDisplayConnected() == false) {
                        Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is disconnected");
                        connectedHDMI = false;
                        isSupportedUHD = false;
                    } else {
                        connectedHDMI = true;
                        Log.printDebug("VideoOutputUtil, init(), HDMI port is connected");
                        
                        VideoOutputConfiguration[] config = setting.getSupportedConfigurations();
                        for (int i = 0; i < config.length; i++) {
                            FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
                            VideoResolution res = fixed.getVideoResolution();
                            Dimension dim = res.getPixelResolution();
                            if (Log.DEBUG_ON) {
                                Log.printDebug("VideoOutputUtil, init(), config[" + i + "] = " + fixed);
                                Log.printDebug("VideoOutputUtil, init(), config[" + i + "], resolution = " + res);
                                Log.printDebug("VideoOutputUtil, init(), config[" + i + "], dimension = " + dim);
                                Log.printDebug("VideoOutputUtil, init(), config[" + i + "], scanmode = " + res.getScanMode());
                                Log.printDebug("VideoOutputUtil, init(), config[" + i + "], rate = " + res.getRate()
                                        + ", rounded = " + Math.round(res.getRate()));
                            }
                            if (res.getScanMode() == VideoResolution.SCANMODE_PROGRESSIVE && dim.height == 2160) {
                                Log.printDebug("VideoOutputUtil, connectionStatusChanged(), UHD OK, found config =" 
                                        + config[i]); 
                                isSupportedUHD = true;
                                break;
                            }
                        }
                    }
                    
                    ((VideoOutputSettings) hdmiOutputPort).addListener(this);
                } else if (videoPort.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_COMPONENT_VIDEO) {
                    Log.printDebug("VideoOutputUtil, found Component port");
                    componentOutputPort = videoPort;
                } else if (videoPort.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_SVIDEO) {
                    Log.printDebug("VideoOutputUtil, found SVIDEO port");
                    sVideoOutputPort = videoPort;
                } else if (videoPort.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_BB) {
                    Log.printDebug("VideoOutputUtil, found BB Port");
                    bbOutputPort = videoPort;
                }
            } catch (Exception ex) {
                Log.print(ex);
            }
		}
		
		// HDCP 2.2
		try {
			hdcpClass = Class.forName("com.alticast.util.HDCPCapability");
			Class[] types = {int.class};
			hdcpMethod = hdcpClass.getMethod("requestHDCPCapability", types);
			if (hdcpMethod != null) {
				existHdcpApi = true;
			} else {
				existHdcpApi = false;
			}
		} catch (Exception ex) {
			Log.print(ex);
			existHdcpApi = false;
		}
		
		Log.printDebug("VideoOutputUtil, existHDCPAPI=" + existHdcpApi);
	}
	
	public boolean canPlayUHDContent() {
		Log.printInfo("VideoOutputUtil, canPlayUHDContent()");
		
		if (Log.DEBUG_ON) {
			Log.printDebug("VideoOutputUtil, canPlayUHDContent(), connectedHDMI=" + connectedHDMI
					+ ", isSupportedUHD=" + isSupportedUHD);
		}
		
		if (connectedHDMI && isSupportedUHD) {
			return true;
		}
		
		return false;
	}

    public boolean isAnalogPortsBlocked() {
        return allPortsBlocked;
    }
	
	public boolean isConnectedHDMI() {
		Log.printInfo("VideoOutputUtil, isConnectedHDMI(), connectedHDMI=" + connectedHDMI);
		return connectedHDMI;
	}
	
	public boolean isSupportedUHD() {
		Log.printInfo("VideoOutputUtil, isSupportedUHD(), isSupportedUHD=" + isSupportedUHD);
		return isSupportedUHD;
	}
	
	public boolean enableOfComponentOutputPort() {
		Log.printInfo("VideoOutputUtil, enableOfComponentOutputPort(), componentOutputPort=" + componentOutputPort);
		Log.printInfo("VideoOutputUtil, enableOfComponentOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
        if (!Environment.SUPPORT_UHD) return false;
		if (componentOutputPort != null) {
            try {
                return componentOutputPort.status();
            } catch (Exception e) {
                Log.print(e);
            }
		}
		
		return false;
	}
	
	public void setEnabledOfComponentOutputPort(boolean enable) {
		Log.printInfo("VideoOutputUtil, setEnabledOfComponentOutputPort(boolean), componentOutputPort=" 
				+ componentOutputPort + ", enable=" + enable);
        try {
            Log.printInfo("VideoOutputUtil, setEnabledOfComponentOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
            if (!Environment.SUPPORT_UHD) return ;
            if (componentOutputPort == null) return;
            boolean currentlyEnabled = componentOutputPort.status();
            Log.printInfo("VideoOutputUtil, setEnabledOfComponentOutputPort() : currentlyEnabled = "+currentlyEnabled); 
            if (componentOutputPort != null) {
                if (enable) {
                    if (!currentlyEnabled) {
                        componentOutputPort.enable();
                    }
                } else {
                    if (currentlyEnabled) {
                        componentOutputPort.disable();
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
	}
	
	public boolean enableOfSViewOutputPort() {
		Log.printInfo("VideoOutputUtil, enableOfSViewOutputPort(), sVideoOutputPort=" + sVideoOutputPort);
        Log.printInfo("VideoOutputUtil, enableOfSViewOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
        if (!Environment.SUPPORT_UHD) return false;
		if (sVideoOutputPort != null) {
            try {
                return sVideoOutputPort.status();
            } catch (Exception e) {
                Log.print(e);
            }
		}
		
		return false;
	}
	
	public void setEnabledOfSVideoOutputPort(boolean enable) {
		Log.printInfo("VideoOutputUtil, setEnabledOfSVideoOutputPort(), sVideoOutputPort=" + sVideoOutputPort 
				+ ", enable=" + enable);
        try {
            Log.printInfo("VideoOutputUtil, setEnabledOfSVideoOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
            if (!Environment.SUPPORT_UHD) return;
            if (sVideoOutputPort == null) return;
            boolean currentlyEnabled = sVideoOutputPort.status();
            Log.printInfo("VideoOutputUtil, setEnabledOfSVideoOutputPort() : currentlyEnabled = "+currentlyEnabled); 
            if (sVideoOutputPort != null) {
                if (enable) {
                    if (!currentlyEnabled) {
                        sVideoOutputPort.enable();
                    }
                } else {
                    if (currentlyEnabled) {
                        sVideoOutputPort.disable();
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
	}
	
	public boolean enableOfBBOutputPort() {
		Log.printInfo("VideoOutputUtil, enableOfBBOutputPort(), bbOutputPort=" + bbOutputPort);
        Log.printInfo("VideoOutputUtil, enableOfBBOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
        if (!Environment.SUPPORT_UHD) return false;
		if (bbOutputPort != null) {
            try {
                return bbOutputPort.status();
            } catch (Exception e) {
                Log.print(e);
            }
		}
		
		return false;
	}
	
	public void setEnabledOfBBOutputPort(boolean enable) {
		Log.printInfo("VideoOutputUtil, setEnabledOfBBOutputPort(), bbOutputPort=" + bbOutputPort 
				+ ", enable=" + enable);
        try {
            Log.printInfo("VideoOutputUtil, setEnabledOfBBOutputPort(), SUPPORT_UHD =" + Environment.SUPPORT_UHD);
            if (!Environment.SUPPORT_UHD) return;
            if (bbOutputPort == null) return;
            boolean currentlyEnabled = bbOutputPort.status();
            Log.printInfo("VideoOutputUtil, setEnabledOfBBOutputPort() : currentlyEnabled = "+currentlyEnabled); 
            if (bbOutputPort != null) {
                if (enable) {
                    if (!currentlyEnabled) {
                        bbOutputPort.enable();
                    }
                } else {
                    if (currentlyEnabled) {
                        bbOutputPort.disable();
                    }
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
	}
	
	public void setEnableOfAnalogueOutputPorts(boolean enable) {
		Log.printInfo("VideoOutputUtil, setEnableOfAnalogueOutputPorts(), enable=" + enable);
        allPortsBlocked = !enable;
        //->Kenneth[2015.6.29] Cisco 버그 때문에 cisco 인 경우 setting 안함.
        //if (isCisco()) return;
        //<-
		
		setEnabledOfComponentOutputPort(enable);
		setEnabledOfBBOutputPort(enable);
		setEnabledOfSVideoOutputPort(enable);
	}
	
	// HDCP2.2
	public boolean existHdcpApi() {
		Log.printInfo("VideoOutputUtil, existHdcpApi(), current existHDCPAPI=" + existHdcpApi);
		
		return existHdcpApi;
	}
	
	// HDCP2.2
	public boolean isSupportedHdcp() {
		Log.printInfo("VideoOutputUtil, isSupportedHdcp(), current isSupportedHdcp22" + isSupportedHdcp22);
		
		Log.printDebug("VideoOutputUtil, isSupportedHdcp(), existHdcpApi=" + existHdcpApi + ", hdcpClass=" + hdcpClass
				+ ", hdcpMethod=" + hdcpMethod);

        //->Kenneth[2015.7.28] : OCAP API 로 먼저 체크해 본다
        boolean hdcpResult = false;
        if (hdmiOutputPort != null) {
            try {
                Boolean hdcpResultObj = (Boolean)hdmiOutputPort.queryCapability(VideoOutputPort.CAPABILITY_TYPE_HDCP);
                hdcpResult = hdcpResultObj.booleanValue();
            } catch (Exception e){
                Log.print(e);
            }
        }
		Log.printDebug("VideoOutputUtil, isSupportedHdcp(), queryCapability(HDCP) returns " + hdcpResult);
        //<-
		
		if (existHdcpApi && hdcpClass != null && hdcpMethod != null) {
			try {
				isSupportedHdcp22 = ((Boolean)hdcpMethod.invoke(hdcpClass.newInstance(), new Object[] {new Integer(22)})).booleanValue();
			} catch (Exception e) {
				Log.print(e);
				isSupportedHdcp22 = false;
			}
		}
		
		Log.printDebug("VideoOutputUtil, isSupportedHdcp(), return isSupportedHdcp22" + isSupportedHdcp22);
		
		return isSupportedHdcp22;
	}
	
	public void addVideoOutputListener(VideoOutputListener l) {
		Log.printInfo("VideoOutputUtil, addVideoOutputListener(), l=" + l);
		if (listenerList.contains(l)) {
			listenerList.remove(l);
		}
		listenerList.add(l);
	}
	
	public void removeVideoOutputListener(VideoOutputListener l) {
		Log.printInfo("VideoOutputUtil, removeVideoOutputListener(), l=" + l);
		listenerList.remove(l);
	}


	public void configurationChanged(VideoOutputPort source, VideoOutputConfiguration oldConfig, VideoOutputConfiguration newConfig) {
	}


	public void connectionStatusChanged(VideoOutputPort source, boolean status) {
		Log.printInfo("VideoOutputUtil, connectionStatusChanged()");
        try {
            if (source.getType() == VideoOutputPort.AV_OUTPUT_PORT_TYPE_HDMI) {
                Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port status changed");
                VideoOutputSettings setting = (VideoOutputSettings) source;
                if (source.status() == false) {
                    Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is disabled");
                    connectedHDMI = false;
                    isSupportedUHD = false;
                } else if (setting.isDisplayConnected() == false) {
                    Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is disconnected");
                    connectedHDMI = false;
                    isSupportedUHD = false;
                } else {
                    Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is connected");
                    connectedHDMI = true;
                    
                    VideoOutputConfiguration[] config = setting.getSupportedConfigurations();
                    for (int i = 0; i < config.length; i++) {
                        FixedVideoOutputConfiguration fixed = (FixedVideoOutputConfiguration) config[i];
                        VideoResolution res = fixed.getVideoResolution();
                        Dimension dim = res.getPixelResolution();
                        if (Log.DEBUG_ON) {
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), config[" + i + "] = " + fixed);
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), config[" + i + "], resolution = "
                                    + res);
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), config[" + i + "], dimension = "
                                    + dim);
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), config[" + i + "], scanmode = "
                                    + res.getScanMode());
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), config[" + i + "], rate = "
                                    + res.getRate() + ", rounded = " + Math.round(res.getRate()));
                        }
                        if (res.getScanMode() == VideoResolution.SCANMODE_PROGRESSIVE && dim.height == 2160) {
                            Log.printDebug("VideoOutputUtil, connectionStatusChanged(), UHD OK, found config =" 
                                    + config[i]); 
                            isSupportedUHD = true;
                            break;
                        }
                    }
                    if (isSupportedUHD == false) {
                        Log.printDebug("VideoOutputUtil, connectionStatusChanged(), HDMI port is ok, but 2160P is not "
                                + "supported");
                    }
                }
                
                for (int i = 0; i < listenerList.size(); i++) {
                    VideoOutputListener l = (VideoOutputListener) listenerList.get(i);
                    l.updateVideoOutputStatus();
                }
            }
        } catch (Exception e) {
            Log.print(e);
        }
	}

	public void enabledStatusChanged(VideoOutputPort source, boolean status) {
		Log.printInfo("VideoOutputUtil, connectionStatusChanged(), source.type=" + source.getType() + ", status=" + status);
	}
}

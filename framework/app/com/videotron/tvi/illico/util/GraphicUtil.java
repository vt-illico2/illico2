package com.videotron.tvi.illico.util;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.FontMetrics;
import java.awt.Image;
import java.awt.image.ImageObserver;

/**
 * This class contains ulitily methods which is related with Graphics and drawing.
 *
 * @version $Revision: 1.12 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class GraphicUtil {

    /** Constructor. */
    private GraphicUtil() {
    }

    /**
     * Draws the center aligned string.
     * @param g graphics context.
     * @param s the string to be drawn.
     * @param x the center x coordinate.
     * @param y the baseline y coordinate.
     * @return calculated x coordinate.
     */
    public static int drawStringCenter(Graphics g, String s, int x, int y) {
        return drawStringCenter(g, s, x, y, g.getFontMetrics());
    }

    /**
     * Draws the center aligned string.
     * @param g graphics context.
     * @param s the string to be drawn.
     * @param x the center x coordinate.
     * @param y the baseline y coordinate.
     * @param fm FontMetrics to calculate with of string.
     * @return calculated x coordinate.
     */
    public static int drawStringCenter(Graphics g, String s, int x, int y,
                                                            FontMetrics fm) {
        if (s != null) {
        	x = x - fm.stringWidth(s) / 2;
            g.drawString(s, x, y);
        }
        return x;
    }

    /**
     * Draws the right aligned string.
     * @param g graphics context.
     * @param s the string to be drawn.
     * @param x the right x coordinate.
     * @param y the baseline y coordinate.
     * @return calculated x coordinate.
     */
    public static int drawStringRight(Graphics g, String s, int x, int y) {
        return drawStringRight(g, s, x, y, g.getFontMetrics());
    }

    /**
     * Draws the right aligned string.
     * @param g graphics context.
     * @param s the string to be drawn.
     * @param x the right x coordinate.
     * @param y the baseline y coordinate.
     * @param fm FontMetrics to calculate with of string.
     * @return calculated x coordinate.
     */
    public static int drawStringRight(Graphics g, String s, int x, int y,
                                                            FontMetrics fm) {
        return drawStringRight(g, s, x, y, Integer.MIN_VALUE, fm);
    }

    /**
     * Draws the right aligned string.
     * @param g graphics context.
     * @param s the string to be drawn.
     * @param x the right x coordinate.
     * @param y the baseline y coordinate.
     * @param limit the most-left x coordinate.
     * @param fm FontMetrics to calculate with of string.
     * @return calculated x coordinate.
     */
    public static int drawStringRight(Graphics g, String s, int x, int y, int limit,
                                                            FontMetrics fm) {
        if (s != null) {
        	x = Math.max(limit, x - fm.stringWidth(s));
            g.drawString(s, x, y);
        }
        return x;
    }

    /**
     * Draws the right aligned string.
     * @param g graphics context.
     * @param im the image to be drawn.
     * @param x the center x coordinate.
     * @param y the center y coordinate.
     * @param o object to be notified as more of the image is converted.
     */
    public static void drawImageCenter(Graphics g, Image im, int x, int y, ImageObserver o) {
        int w = im.getWidth(o);
        int h = im.getHeight(o);
        g.drawImage(im, x - w/2, y - h/2, o);
    }


    public static void drawHighlightedText(Graphics g, String s, int x, int y,
                        Color normal, Color[] highlight, String[] replaceText) {
        drawHighlightedTextImpl(g, s, x, y, normal, highlight, replaceText, false);
    }

    public static void drawHighlightedTextCenter(Graphics g, String s, int x, int y,
                        Color normal, Color[] highlight, String[] replaceText) {
        drawHighlightedTextImpl(g, s, x, y, normal, highlight, replaceText, true);
    }

    public static void drawHighlightedTextImpl(Graphics g, String s, int x, int y,
                        Color normal, Color[] highlight, String[] replaceText, boolean center) {
        FontMetrics fm = g.getFontMetrics();
        String[] tokens = TextUtil.tokenize(s, TextUtil.TEXT_REPLACE_MARKER);
        StringBuffer sb = new StringBuffer(100);
        for (int i = 0; i < tokens.length; i++) {
            sb.append(tokens[i]);
            if (i < replaceText.length) {
                sb.append(replaceText[i]);
            }
        }
        int sx;
        if (center) {
            sx = x - fm.stringWidth(sb.toString()) / 2;
        } else {
            sx = x;
        }
        String temp;
        for (int i = 0; i < tokens.length; i++) {
            temp = tokens[i];
            g.setColor(normal);
            g.drawString(temp, sx, y);
            sx = sx + fm.stringWidth(temp);
            if (i < replaceText.length) {
                temp = replaceText[i];
                g.setColor(highlight[i]);
                g.drawString(temp, sx, y);
                sx = sx + fm.stringWidth(temp);
            }
        }
    }

}

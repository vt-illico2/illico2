package com.videotron.tvi.illico.util;

import org.dvb.user.GeneralPreference;
import org.dvb.user.UserPreferenceManager;
import com.videotron.tvi.illico.log.Log;

/**
 * This class contains ulitily methods which controls GeneralPreference.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class GeneralPreferenceUtil {

    /** Constructor. */
    private GeneralPreferenceUtil() {
    }

    /**
     * Returns the most favourite value for given GeneralPreference name.
     * @param name the general preference name.
     * @return a String representing the most favourite value for this preference.
     *         Returns null if no value is defined for this preference.
     */
    public static String read(String name) {
        String value = null;
        try {
            GeneralPreference pref = new GeneralPreference(name);
            UserPreferenceManager.getInstance().read(pref);
            value = pref.getMostFavourite();
        } catch (IllegalArgumentException ex) {
            Log.print(ex);
        }
        if (Log.INFO_ON) {
            Log.printInfo("GeneralPreferenceUtil.read : name = " + name
                                                    + ", value = " + value);
        }
        return value;
    }

    /**
     * Sets the most favourite value for this preference.
     * @param name the general preference name.
     * @param value the most favourite value.
     */
    public static void write(String name, String value) {
        if (Log.INFO_ON) {
            Log.printInfo("GeneralPreferenceUtil.write : name = " + name
                                                    + ", value = " + value);
        }
        try {
            GeneralPreference pref = new GeneralPreference(name);
            pref.setMostFavourite(value);
            UserPreferenceManager.getInstance().write(pref);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

}

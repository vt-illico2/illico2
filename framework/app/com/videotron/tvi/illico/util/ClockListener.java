package com.videotron.tvi.illico.util;

import java.util.Date;

/**
 * Listener interface to receive clock update event.
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public interface ClockListener {

    /**
     * Called when clock is updated in every minutes.
     *
     * @param date Date instance which represents current clock.
     */
    void clockUpdated(Date date);
}

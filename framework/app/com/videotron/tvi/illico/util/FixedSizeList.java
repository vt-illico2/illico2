package com.videotron.tvi.illico.util;

import java.util.LinkedList;

public class FixedSizeList extends LinkedList {

    private int maxSize;

    public FixedSizeList(int size) {
        maxSize = size;
    }

    public void addFirst(Object o) {
        super.addFirst(o);
        if (size() > maxSize) {
            removeLast();
        }
    }
}

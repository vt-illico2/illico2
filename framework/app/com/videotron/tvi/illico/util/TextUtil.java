package com.videotron.tvi.illico.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.net.URLEncoder;
import java.net.URLDecoder;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * This class contains ulitily methods which is related with Text and String.
 *
 * @version $Revision: 1.25 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class TextUtil {

    /** Constructor. */
    private TextUtil() {
    }

    /** Zero-length String. */
    public static final String EMPTY_STRING = "";
    /** A String that represents ellipsis. */
    private static final String ELLIPSIS = "...";

    /** Marker string to replace text. */
    public static final String TEXT_REPLACE_MARKER = "%%";

    /**
     * Shorten the given string to specified width.
     *
     * @param src String
     * @param fm FontMetrics
     * @param width int
     * @return String
     */
    public static String shorten(String src, FontMetrics fm, int width) {
        if (src == null || src.length() <= 0) {
            return EMPTY_STRING;
        }
        if (fm.stringWidth(src) <= width) {
            return src;
        }
        int ew = fm.stringWidth(ELLIPSIS);
        int len;
        do {
            len = src.length();
            if (len <= 1) {
                if (width >= ew) {
                    return ELLIPSIS;
                } else {
                    return EMPTY_STRING;
                }
            }
            src = src.substring(0, len - 1);
        } while (fm.stringWidth(src) + ew > width);
        return src + ELLIPSIS;
    }

    /**
     * Splits the text into multiple lines with given width.
     *
     * @param src original string.
     * @param fm current font metrics
     * @param width width
     * @return splited lines
     */
    public static String[] split(String src, FontMetrics fm, int width) {
        return split(src, fm, width, "\n");
    }

    public static String[] split(String src, FontMetrics fm, int width, String newLineStr) {
    	if (src == null) {
            return new String[] { "" };
        }
    	ArrayList list = new ArrayList();
    	StringTokenizer lineTok = new StringTokenizer(src, newLineStr);
    	StringBuffer buf = new StringBuffer(200);

    	while (lineTok.hasMoreTokens()) {
    		StringTokenizer tok = new StringTokenizer(lineTok.nextToken(), " ");
        	String word = "";
        	buf.setLength(0);
        	try {
                int len;
                while (true) {
                    word = tok.nextToken();
                    while (fm.stringWidth(word) > width) {
                        // long word
                        if (buf.length() > 0) {
                            list.add(buf.toString());
                            buf.setLength(0);
                        }
                        len = word.length();
                        do {
                            String part = word.substring(0, len);
                            if (fm.stringWidth(part) < width) {
                                list.add(part);
                                word = word.substring(len);
                                len = word.length();
                                break;
                            } else {
                                len--;
                            }
                        } while (len > 0);
                    }
                    len = buf.length();
                    if (len > 0) {
                        buf.append(' ');
                    }
                    buf.append(word);
                    if (width <= fm.stringWidth(buf.toString())) {
                        list.add(buf.substring(0, len));
                        buf.setLength(0);
                        buf.append(word);
                    }
                    len = buf.length();
                }
        	} catch (NoSuchElementException e) {
        		list.add(buf.toString());
        	}
    	}

    	String[] ret = new String[list.size()];
    	list.toArray(ret);
    	return ret;
    }

    /**
     * Splits the text into multiple lines with given width.
     *
     * @param src original string.
     * @param fm current font metrics
     * @param width width
     * @return splited lines
     *
//     * @deprecated <leeksnet> new split() is faster :)
     */
//    public static String[] split(String src, FontMetrics fm, int width) {
//        if (src == null) {
//            return new String[0];
//        }
//        boolean byWord = true;
//        String temp;
//        Vector lines = new Vector();
//
//        StringBuffer strbuf = new StringBuffer();
//        for (int i = 0; i < src.length(); i++) {
//            if (src.charAt(i) == '\n') {
//                addLineAfterWrap(lines, strbuf.toString(), fm, width, byWord);
//                strbuf = new StringBuffer();
//            } else {
//                strbuf.append(src.charAt(i));
//            }
//        }
//        addLineAfterWrap(lines, strbuf.toString(), fm, width, byWord);
//
//        String[] dummy = new String[lines.size()];
//        for (int i = 0; i < lines.size(); i++) {
//            dummy[i] = (String) lines.elementAt(i);
//        }
//        return dummy;
//    }

    /**
     * Splits the text into multiple lines with given width.
     *
     * @param src original string.
     * @param fm current font metrics
     * @param width width
     * @param delim delimiter to be used.
     * @return splited lines
     */
    public static String[] split(String src, FontMetrics fm, int width, char delim) {
        if (src == null) {
            return new String[0];
        }
        boolean byWord = true;
        String temp;
        Vector lines = new Vector();

        StringBuffer strbuf = new StringBuffer();
        for (int i = 0; i < src.length(); i++) {
            if (src.charAt(i) == delim) {
                addLineAfterWrap(lines, strbuf.toString(), fm, width, byWord);
                strbuf = new StringBuffer();
            } else {
                strbuf.append(src.charAt(i));
            }
        }
        addLineAfterWrap(lines, strbuf.toString(), fm, width, byWord);

        String[] dummy = new String[lines.size()];
        for (int i = 0; i < lines.size(); i++) {
            dummy[i] = (String) lines.elementAt(i);
        }
        return dummy;
    }

    /**
     * Splits the text into multiple lines with given width.
     *
     * @param lines contains splited lines.
     * @param string lines to be added.
     * @param fm current font metrics
     * @param width width
     * @param byWord If true, the line will be splited by white space.
     */
    private static void addLineAfterWrap(Vector lines, String string,
                             FontMetrics fm, int width, boolean byWord) {
        StringBuffer org = new StringBuffer(string);

        StringBuffer sb = new StringBuffer();

        int firstIndex = 0;
        int lastBlank = -1;
        int fw;
        char c;
        int strlen = org.length();
        for (int i = 0; i < strlen; i++) {
            c = org.charAt(i);
            sb = sb.append(c);
            fw = fm.stringWidth(sb.toString());

            if (byWord && (c == '\t' || c == ' ')) {
                lastBlank = i;
            }
            if (fw > width) {
                if (lastBlank >= firstIndex) {
                    // to the next line
                    lines.addElement(string.substring(firstIndex, lastBlank));
                    firstIndex = lastBlank + 1;
                    sb = new StringBuffer(string.substring(firstIndex, i + 1));
                } else if (i > 0) {
                    // split word
                    lines.addElement(string.substring(firstIndex, i));
                    firstIndex = i;
                    sb = new StringBuffer();
                    // skip first blank char
                    if (c != '\t' && c != ' ') {
                        sb.append(c);
                    }
                }
            }
        }
        String lastStr = string.substring(firstIndex, strlen);
        if (fm.stringWidth(lastStr) > width) {
            lines.addElement(string.substring(firstIndex, strlen - 1));
            lines.addElement(string.substring(strlen - 1));
        } else {
            lines.addElement(lastStr);
        }
    }

    /**
     * Splits the text into multiple lines with given width and max lines.
     *
     * @param src original string.
     * @param fm current font metrics
     * @param width width
     * @param lines maximum line number
     * @return splited strings.
     */
    public static String[] split(String src, FontMetrics fm, int width, int lines) {
        String[] s = split(src, fm, width);
        if (s.length <= lines) {
            return s;
        }
        s[lines - 1] = shorten(s[lines - 1] + ELLIPSIS, fm, width);
        String[] ret = new String[lines];
        System.arraycopy(s, 0, ret, 0, lines);
        return ret;
    }

    /**
     * Returns right aligned text filled with blank character on the right side
     * if the original string is less than the specified size.
     * <p>
     * If the original string is null, it returns string filled with all blank
     * characters. If the original string is equal to or longer than the
     * designated size, it returns the original string itself.
     *
     * @param str   the original string
     * @param size  the size of text displaying the original string
     * @param blankChar the character representing blank such as ' '
     * @return  the right aligned text filled with blank character
     */
    public static String getRightAlignedText(String str, int size, char blankChar) {
        if (str == null) {
            char[] ret = new char[size];
            for (int i = 0; i < size; i++) {
                ret[i] = blankChar;
            }
            return new String(ret);
        }

        int strLen = str.length();
        if (strLen >= size) {
            return str;
        }

        int spaceCount = size - strLen;
        char[] ret = new char[size];
        for (int i = 0; i < spaceCount; i++) {
            ret[i] = blankChar;
        }
        System.arraycopy(str.toCharArray(), 0, ret, spaceCount, strLen);

        return new String(ret);
    }

    /**
     * Splits this string with the delimiters.
     *
     * @param s original string.
     * @param delim the delimiters.
     * @return splited tokens.
     */
    public static String[] tokenize(String s, char delim) {
        Vector v = new Vector();
        int l = 0;
        int size = s.length();
        int p;
        while (l < size) {
            p = s.indexOf(delim, l);
            if (p == -1) {
                if (l > 0) {
                    v.addElement(s.substring(l));
                } else {
                    v.addElement(s);
                }
                break;
            } else {
                v.addElement(s.substring(l, p));
                l = p + 1;
            }
            if (l == size) {
                v.addElement("");
                break;
            }
        }
        String[] ret = new String[v.size()];
        v.copyInto(ret);
        return ret;
    }

    /**
     * Splits this string with the delimiters.
     *
     * @param s original string.
     * @param delim the delimiters.
     * @return splited tokens.
     */
    public static String[] tokenize(String s, String delim) {
        Vector v = new Vector();
        int l = 0;
        int size = s.length();
        int p;
        int dLen = delim.length();
        while (l < size) {
            p = s.indexOf(delim, l);
            if (p == -1) {
                if (l > 0) {
                    v.addElement(s.substring(l));
                } else {
                    v.addElement(s);
                }
                break;
            } else {
                v.addElement(s.substring(l, p));
                l = p + dLen;
            }
            if (l == size) {
                v.addElement("");
                break;
            }
        }
        String[] ret = new String[v.size()];
        v.copyInto(ret);
        return ret;
    }

    /**
     * Returns the basename of given filename. For example, this method will return 'abc'
     * if the given filename is 'abc.txt'.
     *
     * @param filename filename.
     * @return basename.
     */
    public static String getBaseName(String filename) {
        int p = filename.lastIndexOf('.');
        if (p > 0) {
            return filename.substring(0, p);
        } else {
            return filename;
        }
    }

    /**
     * Returns a new string resulting from replacing all occurrences of oldStr in the
     * given string with newStr.
     *
     * @param s the original string.
     * @param oldStr the old string.
     * @param newStr the new string.
     * @return the new string.
     */
    public static String replace(String s, String oldStr, String newStr) {
        char[] ch = s.toCharArray();
        int l = 0;
        int r = 0;
        int oldLen = oldStr.length();
        StringBuffer sb = new StringBuffer(s.length() * 2);
        while ((r = s.indexOf(oldStr, l)) != -1) {
            sb.append(ch, l, r - l);
            sb.append(newStr);
            l = r + oldLen;
        }
        sb.append(ch, l, ch.length - l);
        return sb.toString();
    }

    public static String urlEncode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return s;
        }
    }

    public static String urlDecode(String s) {
        if (s == null) {
            return s;
        }
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            return s;
        }
    }

}

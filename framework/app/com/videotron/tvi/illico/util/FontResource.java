package com.videotron.tvi.illico.util;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Toolkit;
import java.io.File;
import java.net.URL;
import org.dvb.ui.FontFactory;
import com.videotron.tvi.illico.log.Log;

/**
 * FontResource manages the font factories and caches the Font objects.
 *
 * @version $Revision: 1.13 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class FontResource {
    /** Font directory. */
    private static final String FONT_DIRECTORY = "./resource/font/";

    /** FontResource for BLENDER font. */
    public static final FontResource BLENDER = new FontResource(
                                "Blender Pro Bold", "BlenderPro-Bold.ttf");
    /** FontResource for DINMED font. */
    public static final FontResource DINMED = new FontResource(
                                "Dinmed", "DINMed.TTF");

    /** Font factory to create Font. */
    private FontFactory factory;
    /** Font name of this FontResource. */
    private String fontName;

    /** Array that containing fonts. */
    private Font[] array;
    /** initial array size for the font resoruce. */
    private static final int INITIAL_ARRAY_SIZE = 40;

    /**
     * Constructor.
     *
     * @param name font face name.
     * @param file font file name.
     */
    public FontResource(final String name, final String file) {
        this.fontName = name;
        factory = (FontFactory) SharedMemory.getInstance().get(fontName);
        if (factory == null) {
            try {
                factory = new FontFactory(new URL("file", "", new File(FONT_DIRECTORY + file).getAbsolutePath()));
                SharedMemory.getInstance().put(fontName, factory);
            } catch (Exception ex) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("FontResource: fail to create font factory : " + file);
                }
                Log.print(ex);
            }
        }
        array = new Font[INITIAL_ARRAY_SIZE];
    }

    /**
     * Returns the Font with specified size.
     *
     * @param size size of Font.
     * @return Font object.
     */
    public Font getFont(int size) {
        return getFont(size, false);
    }

    /**
     * Returns the Font with specified size.
     *
     * @param size size of Font.
     * @param shareCreatedFont true to store a created font into SharedMemory.
     * @return Font object.
     */
    public Font getFont(int size, boolean shareCreatedFont) {
        if (factory == null) {
            Log.printWarning("FontResource: no font factory.");
            return null;
        }
        if (size >= array.length) {
            if (Log.DEBUG_ON) {
                Log.printDebug("FontResource: ensure array size. " + array.length + " -> " + size);
            }
            Font[] newArray = new Font[size + 2];
            System.arraycopy(array, 0, newArray, 0, array.length);
            array = newArray;
        }
        Font font = array[size];
        if (font == null) {
            String smKey = fontName + " size=" + size;
            font = (Font) SharedMemory.getInstance().get(smKey);
            if (font == null) {
                try {
                    font = factory.createFont(fontName, Font.PLAIN, size);
                    if (Log.DEBUG_ON) {
                        Log.printDebug("FontResource: created = " + font);
                    }
                    if (shareCreatedFont) {
                        SharedMemory.getInstance().put(smKey, font);
                    }
                } catch (Exception ex) {
                    Log.printWarning("FontResource: fail to create font : " + fontName + ", " + size);
                    Log.print(ex);
                }
            }
            array[size] = font;
        }
        return font;
    }

    public Font getBoldFont(int size) {
        if (factory == null) {
            Log.printWarning("FontResource: no font factory.");
            return null;
        }
        Font font = null;
        try {
            font = factory.createFont(fontName, Font.BOLD, size);
        } catch (Exception ex) {
            Log.print(ex);
        }
        return font;
    }

    /**
     * Gets the font metrics of the given font.
     *
     * @param font font.
     * @return FontMetrics.
     */
    public static FontMetrics getFontMetrics(Font font) {
        return Toolkit.getDefaultToolkit().getFontMetrics(font);
    }
}

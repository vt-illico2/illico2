package com.videotron.tvi.illico.util;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.Locale;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.framework.DataCenter;

/**
 * Formatter.
 *
 * @version $Revision: 1.28 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public abstract class Formatter implements ClockListener {
    public static final int TIME_HOUR_MIN     = 0;
    public static final int TIME_HOUR_MIN_SEC = 1;


    private static TimeZone currentTimeZone;
    private static Locale currentLocale;
    private static TimeZoneUpdater timeZoneUpdater = new TimeZoneUpdater();

    protected String[] days;
    protected String[] fullDays;
    protected String[] months;
    protected String[] shortMonths;

    static {
        timeZoneUpdater.clockUpdated(new Date());
        Clock.getInstance().addClockListener(timeZoneUpdater);
    }

    public static Formatter get(String language) {
        if (language.equals(Constants.LANGUAGE_FRENCH)) {
            return frenchFormatter;
        } else {
            return frenchFormatter;
        }
    }

    // TODO - fix.
    private static Formatter frenchFormatter = new FrenchFormatter();
    private static Formatter englishFormatter = new EnglishFormatter();

    private static Formatter currentFormatter;

    public static Formatter getCurrent() {
        return currentFormatter;
    }

    static {
        frenchFormatter.init();
        englishFormatter.init();
    }

    protected String currentTimeString;
    protected String currentShortDateString;
    protected String currentLongDateString;

    protected void init() {
        Clock.getInstance().addClockListener(this);
        clockUpdated(new Date());
    }

    public static void setLanguage(String language) {
        if (language.equals(Constants.LANGUAGE_FRENCH)) {
            currentFormatter = frenchFormatter;
        } else {
            currentFormatter = englishFormatter;
        }
    }

    /** Returns "01:23:45". */
    public abstract String getDuration(long duration);

    /** Returns "5 d 1 h 23 min". */
    public abstract String getDurationText(long duration);

    /** Returns "24 h, 3 days or 3 jours". */
    public abstract String getDurationText2(long duration);

    /** Returns "24 h, 3 d. or 3 j.". */
    public abstract String getDurationText3(long duration);

    /** Returns "24 h 2 min". */
    public String getDurationText4(long duration) {
        int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return (int) ((duration + 500L) / Constants.MS_PER_SECOND) + " sec";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " min";
            } else {
                int hour = min / 60;
                min = min % 60;
                StringBuffer sb = new StringBuffer();
                sb.append(hour);
                sb.append(" h ");
                if (min < 10) {
                    sb.append('0');
                }
                sb.append(min);
                sb.append(" min");
                return sb.toString();
            }
        }
    }

    /** Returns "HH:MM:SS, 3 days or 3 jours". */
    public abstract String getCountDownDuration(long duration);

    /** Returns "12:00". */
    public String getTime(long time) {
        return getTime(new Date(time));
    }

    /** Returns "12:00". */
    public String getTime(Date date) {
        return getTime(date, TIME_HOUR_MIN);
    }

    /** Returns "12:00". */
    public String getTime(Calendar cal) {
        if (cal == null) {
            return "";
        }
        return getTime(cal.getTime(), TIME_HOUR_MIN);
    }

    /** Returns "12:00" or "12:00:59". */
    public String getTime(long time, int timeFormat) {
        return new String(getTimeChars(new Date(time), timeFormat));
    }

    /** Returns "12:00" or "12:00:59". */
    public String getTime(Date date, int timeFormat) {
        return new String(getTimeChars(date, timeFormat));
    }

    /** Returns "12:00" or "12:00:59". */
    public abstract char[] getTimeChars(Date date, int timeFormat);

    /** Returns "Sept. 25". */
    public String getDayText(long time) {
        return getDayText(new Date(time));
    }

    /** Returns "Sept. 25". */
    public String getShortDayText(long time) {
        Calendar cal = getCalendar();
        cal.setTime(new Date(time));
        return getShortText(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    }

    /** Returns "Sept. 25". */
    public String getDayText(Date date) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        return getDayText(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    }

    /** Returns "Sept. 25". */
    public String getDayText(Calendar cal) {
        if (cal == null) {
            return "";
        }
        return getDayText(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
    }

    /** Returns "Fri. Sept. 12" */
    public String getLongDayText(long time) {
        return getLongDayText(new Date(time));
    }

    /** Returns "Fri. Sept. 12" */
    public String getLongDayText(Date date) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        return getDayText(cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH),
                            cal.get(Calendar.DAY_OF_WEEK) - 1);
    }

    /** Returns "Sept. 12" */
    protected abstract String getDayText(int month, int day, int dow);

    protected abstract String getShortText(int month, int day);

    protected abstract String getDayText(int month, int day);

    public String getDay(int dow) {
        return days[dow];
    }

    public String getFullDay(int dow) {
        return fullDays[dow];
    }

    public String getMonth(int month) {
        return months[month];
    }

    public String getShortDate(long time) {
        return getShortDate(new Date(time));
    }

    public String getLongDate(long time) {
        return getLongDate(new Date(time));
    }

    /** Returns "Fri. 25 - 12:35". */
    public String getShortDate(Date date) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        int dow = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuffer sb = new StringBuffer(30);
        sb.append(getDay(dow));
        sb.append(' ');
        if (day < 10) {
            sb.append('0');
        }
        sb.append(day);
        sb.append(" - ");
        sb.append(getTimeChars(date, TIME_HOUR_MIN));
        return sb.toString();
    }

    /** Returns "Fri. Sept. 16 - 12:35". */
    public abstract String getLongDate(Date date);

    /** Returns current time string. */
    public String getTime() {
        return currentTimeString;
    }

    /** Returns current shot date string. */
    public String getShortDate() {
        return currentShortDateString;
    }

    /** Returns current shot date string. */
    public String getLongDate() {
        return currentLongDateString;
    }

    /** Convert "1.23" to "$1.23" or "1,23 $". */
    public abstract String getPrice(float price);

    /** Convert "1.23" to "$1.23" or "1,23 $". */
    public abstract String getPrice(String price);

    public abstract boolean is24HourFormat();

    /** ClockListener. */
    public void clockUpdated(Date date) {
        this.currentTimeString = getTime(date);
        this.currentLongDateString = getLongDate(date);
        this.currentShortDateString = getShortDate(date);
    }

    public static Calendar getCalendar() {
        return Calendar.getInstance(currentTimeZone, currentLocale);
    }

    static class TimeZoneUpdater implements ClockListener {
        public void clockUpdated(Date date) {
            currentTimeZone = TimeZone.getDefault();
            currentLocale = Locale.getDefault();
        }
    }
}


class EnglishFormatter extends Formatter {

    public EnglishFormatter() {
        /** Texts for English days. */
        days = new String[] { "Sun.", "Mon.", "Tues.", "Wed.", "Thurs.", "Fri.", "Sat." };
        fullDays = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
        /** Texts for English months. */
        months = new String[] { "Jan.", "Feb.", "March", "April", "May", "June",
                                "July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec." };
        shortMonths = new String[] { "Jan.", "Feb.", "Mar.", "April", "May", "June",
                                "July", "Aug.", "Sept.", "Oct.", "Nov.", "Dec." };
    }

    public boolean is24HourFormat() {
        return false;
    }

    public String getDayText(int month, int day, int dow) {
        return days[dow] + " " + months[month] + " " + day;
    }

    public String getDayText(int month, int day) {
        return months[month] + " " + day;
    }

    public String getShortText(int month, int day) {
        return shortMonths[month] + " " + day;
    }

    /** Returns "01:23:45". */
    public String getDuration(long duration) {
        StringBuffer sb = new StringBuffer(10);
        int sec = (int) ((duration + 500L) / Constants.MS_PER_SECOND);
        int min = sec / 60;
        sec = sec % 60;
        int hour = min / 60;
        min = min % 60;

        if (hour > 0) {
            sb.append(hour);
            sb.append(':');
        }
        if (min < 10) {
            sb.append('0');
        }
        sb.append(min);
        sb.append(':');
        if (sec < 10) {
            sb.append('0');
        }
        sb.append(sec);
        return sb.toString();
    }

    /** Returns "5 d 1 h 23 min". */
    public String getDurationText(long duration) {
        int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return (int) ((duration + 500L) / Constants.MS_PER_SECOND) + " sec";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " min";
            } else {
                int hour = min / 60;
                min = min % 60;
                StringBuffer sb = new StringBuffer();
                if (hour < 24) {
                    sb.append(hour);
                } else {
                    sb.append(hour / 24);
                    sb.append(" d ");
                    sb.append(hour % 24);
                }
                sb.append(" h ");
                if (min < 10) {
                    sb.append('0');
                }
                sb.append(min);
                sb.append(" min");
                return sb.toString();
            }
        }
    }

    public String getDurationText2(long duration) {
    	int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return (duration / Constants.MS_PER_SECOND) + " sec";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " min";
            } else {
                int hour = min / 60;
                if (hour <= 24) {
                    return hour + " h";
                } else {
                    int d = (int) Math.round(((double) duration) / Constants.MS_PER_DAY);
                    return d + (d == 1 ? " day" : " days");
                }
            }
        }
    }

    public String getDurationText3(long duration) {
    	int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return Math.max(0, duration / Constants.MS_PER_SECOND) + " s";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " m";
            } else {
                int hour = min / 60;
                if (hour <= 24) {
                    return hour + " h";
                } else {
                    int d = (int) Math.round(((double) duration) / Constants.MS_PER_DAY);
                    return d + " d.";
                }
            }
        }
    }

    public String getCountDownDuration(long duration) {
        if (duration > Constants.MS_PER_DAY) {
            int day = (int) (duration / Constants.MS_PER_DAY);
            if (day <= 1) {
                return day + " day";
            } else {
                return day + " days";
            }
        } else {
            return getDuration(duration);
        }
    }

    /** Returns "$1.23". */
    public String getPrice(float price) {
        int p = Math.round(price * 100);
        return "$" + (p / 100) + "." + String.valueOf(p % 100 + 100).substring(1);
    }

    /** Returns "$1.23". */
    public String getPrice(String price) {
    	return "$" + price;
    }

    /** Returns "Fri. Sept. 16 - 12:35 pm". */
    public String getLongDate(Date date) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        int dow = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuffer sb = new StringBuffer(40);
        sb.append(getDay(dow));
        sb.append(' ');
        sb.append(getMonth(month));
        sb.append(' ');
        if (day < 10) {
            sb.append('0');
        }
        sb.append(day);
        sb.append(" - ");
        sb.append(getTimeChars(date, TIME_HOUR_MIN));
        return sb.toString();
    }

    /** Returns "12:00" or "12:00:59". */
    public char[] getTimeChars(Date date, int timeFormat) {
        Calendar calendar = getCalendar();
        calendar.setTime(date);

        int hour = calendar.get(Calendar.HOUR);
        int min = calendar.get(Calendar.MINUTE);
        int ampm = calendar.get(Calendar.AM_PM);
        if (hour == 0) {
            hour = 12;
        }

        int cSize = (hour / 10) + (timeFormat == TIME_HOUR_MIN ? 7 : 10);
        char[] c = new char[cSize];

        int i = 0;
        if (hour >= 10) {
            c[i++] = (char) ((hour / 10) % 10 + '0');
        }
        c[i++] = (char) (hour % 10 + '0');
        c[i++] = ':';
        c[i++] = (char) ((min / 10) % 10 + '0');
        c[i++] = (char) (min % 10 + '0');
        if (timeFormat == TIME_HOUR_MIN_SEC) {
            int sec = calendar.get(Calendar.SECOND);
            c[i++] = ':';
            c[i++] = (char) ((sec / 10) % 10 + '0');
            c[i++] = (char) (sec % 10 + '0');
        }
        c[i++] = ' ';
        c[i++] = ampm == Calendar.AM ? 'a' : 'p';
        c[i++] = 'm';
        return c;
    }

}

class FrenchFormatter extends Formatter {
    public FrenchFormatter() {
        /** Texts for French days. */
        days = new String[] { "dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam." };
        fullDays = new String[] { "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" };
        /** Texts for French months. */
        months = new String[] { "janv.", "févr.", "mars", "avr.", "mai", "juin",
                                "juill.", "août", "sept.", "oct.", "nov.", "déc." };
        shortMonths = months;
    }

    public boolean is24HourFormat() {
        return true;
    }

    public String getDayText(int month, int day, int dow) {
        return days[dow] + " " + day + " " + months[month];
    }

    public String getDayText(int month, int day) {
        return day + " " + months[month];
    }

    public String getShortText(int month, int day) {
        return day + " " + shortMonths[month];
    }

    /** Returns "01:23:45". */
    public String getDuration(long duration) {
        StringBuffer sb = new StringBuffer(10);
        int sec = (int) ((duration + 500L) / Constants.MS_PER_SECOND);
        int min = sec / 60;
        sec = sec % 60;
        int hour = min / 60;
        min = min % 60;

        if (hour > 0) {
            if (hour < 10) {
                sb.append('0');
            }
            sb.append(hour);
            sb.append(':');
        }
        if (min < 10) {
            sb.append('0');
        }
        sb.append(min);
        sb.append(':');
        if (sec < 10) {
            sb.append('0');
        }
        sb.append(sec);
        return sb.toString();
    }

    /** Returns "5 d 1 h 23 min". */
    public String getDurationText(long duration) {
        int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return (int) ((duration + 500L) / Constants.MS_PER_SECOND) + " sec";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " min";
            } else {
                int hour = min / 60;
                min = min % 60;
                StringBuffer sb = new StringBuffer();
                if (hour < 24) {
                    sb.append(hour);
                } else {
                    sb.append(hour / 24);
                    sb.append(" j ");
                    sb.append(hour % 24);
                }
                sb.append(" h ");
                if (min < 10) {
                    sb.append('0');
                }
                sb.append(min);
                sb.append(" min");
                return sb.toString();
            }
        }
    }

    public String getDurationText2(long duration) {
    	int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return (duration / Constants.MS_PER_SECOND) + " sec";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " min";
            } else {
                int hour = min / 60;
                if (hour <= 24) {
                    return hour + " h";
                } else {
                    int d = (int) Math.round(((double) duration) / Constants.MS_PER_DAY);
                    return d + (d == 1 ? " jour" : " jours");
                }
            }
        }
    }

    public String getDurationText3(long duration) {
    	int min = (int) (duration / Constants.MS_PER_MINUTE);
        if (min < 1) {
            return Math.max(0, duration / Constants.MS_PER_SECOND) + " s";
        } else {
            min = (int) Math.round(((double) duration) / Constants.MS_PER_MINUTE);
            if (min < 60) {
                return min + " m";
            } else {
                int hour = min / 60;
                if (hour <= 24) {
                    return hour + " h";
                } else {
                    int d = (int) Math.round(((double) duration) / Constants.MS_PER_DAY);
                    return d + " j.";
                }
            }
        }
    }

    public String getCountDownDuration(long duration) {
        if (duration > Constants.MS_PER_DAY) {
            int day = (int) (duration / Constants.MS_PER_DAY);
            if (day <= 1) {
                return day + " jour";
            } else {
                return day + " jours";
            }
        } else {
            return getDuration(duration);
        }
    }

    /** Returns "1,23 $". */
    public String getPrice(float price) {
        int p = Math.round(price * 100);
        return (p / 100) + "," + String.valueOf(p % 100 + 100).substring(1) + " $";
    }

    /** Returns "1,23 $". */
    public String getPrice(String price) {
    	return price.replace('.', ',') + " $";
    }

    /** Returns "ven. 16 sept. - 12:35". */
    public String getLongDate(Date date) {
        Calendar cal = getCalendar();
        cal.setTime(date);
        int dow = cal.get(Calendar.DAY_OF_WEEK) - 1;
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        StringBuffer sb = new StringBuffer(40);
        sb.append(getDay(dow));
        sb.append(' ');
        if (day < 10) {
            sb.append('0');
        }
        sb.append(day);
        sb.append(' ');
        sb.append(getMonth(month));
        sb.append(" - ");
        sb.append(getTimeChars(date, TIME_HOUR_MIN));
        return sb.toString();
    }

    /** Returns "12:00" or "12:00:59". */
    public char[] getTimeChars(Date date, int timeFormat) {
        Calendar calendar = getCalendar();
        char[] c = new char[timeFormat == TIME_HOUR_MIN ? 5 : 8];
        calendar.setTime(date);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        c[0] = (char) ((hour / 10) % 10 + '0');
        c[1] = (char) (hour % 10 + '0');
        c[2] = ':';
        c[3] = (char) ((min / 10) % 10 + '0');
        c[4] = (char) (min % 10 + '0');
        if (timeFormat == TIME_HOUR_MIN_SEC) {
            int sec = calendar.get(Calendar.SECOND);
            c[5] = ':';
            c[6] = (char) ((sec / 10) % 10 + '0');
            c[7] = (char) (sec % 10 + '0');
        }
        return c;
    }

}


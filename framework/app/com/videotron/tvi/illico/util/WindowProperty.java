package com.videotron.tvi.illico.util;

import java.awt.Rectangle;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUIManager;

/**
 * This class represents priority and window of layered UI.
 *
 * @version $Revision: 1.66.2.1 $ $Date: 2017/03/30 12:58:18 $
 * @author June Park
 */
public class WindowProperty {
    /** WindowProperty for Key block handler. */
    public static final WindowProperty LOW_POWER_KEY_BLOCKER = new WindowProperty("LowPowerKeyBlocker", 400000);
    /** WindowProperty for SDV User Activity Check. */
    public static final WindowProperty SDV_USER_ACTIVITY  = new WindowProperty("Sdv.UserActivity", 300000);
    /** WindowProperty for OC Debug screen. */
    public static final WindowProperty DEBUG_SCREEN  = new WindowProperty("DebugScreen", 200000);
    /** WindowProperty for key block while Monitor shows application loading. */
    public static final WindowProperty APP_LOADING_KEY_BLOCKER = new WindowProperty("AppLoadingKeyBlocker", 100000);
    /** WindowProperty for blocked popup. */
    public static final WindowProperty AUTHORITY_BLOCKED_POPUP = new WindowProperty("AuthorityBlocked", 6845);
    /** WindowProperty for ScreenSaver. */
    public static final WindowProperty SCREEN_SAVER = new WindowProperty("ScreenSaver", 6900);
    /** WindowProperty for Error message. */
    public static final WindowProperty ERROR_MESSAGE  = new WindowProperty("ErrorMessage", 6850);
    /** WindowProperty for the unbound application update pupup. */
    //->Kenneth : Corrent misspelling : UPDTAE -> UPDATE
    public static final WindowProperty UNBOUND_APP_UPDATE_POPUP = new WindowProperty("UnboundAppUpdate", 6800);
    /** WindowProperty for weak signal popup. */
    public static final WindowProperty WEAK_SIGNAL_POPUP = new WindowProperty("WeakSignal", 6780);

    /** WindowProperty for PVR's popup for conflict. */
    public static final WindowProperty PVR_CONFLICT_POPUP      = new WindowProperty("Pvr.ConflictPopup", 6705);
    /** WindowProperty for PVR's popup. */
    public static final WindowProperty PVR_POPUP      = new WindowProperty("Pvr.Popup", 6700);
    /** WindowProperty for EPG's popup. */
    public static final WindowProperty EPG_POPUP      = new WindowProperty("Epg.Popup", 6700);
    /** WindowProperty for EPG volume bar. */
    public static final WindowProperty VOLUME_BAR     = new WindowProperty("Epg.VolumeBar",     6700);

    /** WindowProperty for 'D' option menu. */
    public static final WindowProperty OPTION_MENU    = new WindowProperty("OptionMenu",        6650);
    /** WindowProperty for Caller ID. */
    public static final WindowProperty CALLERID         = new WindowProperty("CallerID",          3000);
    /** WindowProperty for EPG channel banner. */
    public static final WindowProperty CHANNEL_BANNER = new WindowProperty("Epg.ChannelBanner", 5030);
    /** WindowProperty for Mini EPG. */
    public static final WindowProperty MINI_EPG       = new WindowProperty("Epg.MiniEpg",       5020);
    /** WindowProperty for Full Menu. */
    public static final WindowProperty FULL_MENU      = new WindowProperty("Menu.FullMenu",     3000);
    /** WindowProperty for Mini Menu. */
    public static final WindowProperty MINI_MENU      = new WindowProperty("Menu.MiniMenu",     5020);
    /** WindowProperty for Dashboard. */
    public static final WindowProperty DASHBOARD      = new WindowProperty("Dashboard",         5020);
    /** WindowProperty for Full Menu Option. */
    public static final WindowProperty FULL_MENU_OPTION = new WindowProperty("Menu.FullMenuOption", 4500);
    /** WindowProperty for EPG's popup. */
    public static final WindowProperty MENU_POPUP      = new WindowProperty("Menu.Popup", 5030);
    /** WindowProperty for Loading. */
    public static final WindowProperty LOADING        = new WindowProperty("Loading", 5900);
    /** WindowProperty for Notification. */
    public static final WindowProperty NOTIFICATION       = new WindowProperty("Notification", 5800);

    /** WindowProperty for PIN Enabler. */
    public static final WindowProperty PINENABLER         = new WindowProperty("Profile.PinEnabler", 6750);
    /** WindowProperty for the terminal blocked time. */
    public static final WindowProperty TERMINAL_BLOCKED_TIME = new WindowProperty("TerminalBlockedTime", 6730);

    /** WindowProperty for Keyboard. */
    public static final WindowProperty KEYBOARD       = new WindowProperty("keyboard", 5700);
    /** WindowProperty for Search. */
    public static final WindowProperty SEARCH         = new WindowProperty("Search", 5710);

    /** WindowProperty for PVR FlipBar. */
    public static final WindowProperty PVR_FLIP_BAR   = new WindowProperty("PVR.FlipBar", 5010);
    /** WindowProperty for PVR Play Info. */
    public static final WindowProperty PVR_PLAY_INFO   = new WindowProperty("PVR.PlayInfo", 5000);

    /** WindowProperty for EPG's Scaled Video UI. */
    public static final WindowProperty EPG_SCALED_VIDEO   = new WindowProperty("Epg.ScaledVideo", 4600);
    /** WindowProperty for EPG's PIP. */
    public static final WindowProperty EPG_PIP        = new WindowProperty("Epg.Pip", 4500);

    /** WindowProperty for VOD's popup. */
    public static final WindowProperty VOD_CHANGE_VIEW = new WindowProperty("VOD.ChangeView", 4000);
    /** WindowProperty for VOD's trailer viewer. */
    public static final WindowProperty VOD_TRAILER = new WindowProperty("VOD.TrailerViewer", 4000);
    /** WindowProperty for VOD's main screen. */
    public static final WindowProperty VOD_MAIN = new WindowProperty("VOD.Main", 3000);
    /** WindowProperty for EPG. */
    public static final WindowProperty EPG_DEFAULT        = new WindowProperty("Epg.Default", 3000);
    /** WindowProperty for PVR Menu. */
    public static final WindowProperty PVR_MENU           = new WindowProperty("Pvr.Menu", 3000);
    /** WindowProperty for SDV Barker. */
    public static final WindowProperty SDV_BARKER         = new WindowProperty("Sdv.Barker", 2510);
    /** WindowProperty for PPV. */
    public static final WindowProperty PPV_CHANNEL        = new WindowProperty("Ppv.Channel", 2500);
    /** WindowProperty for Pastille. */
    public static final WindowProperty PASTILLE           = new WindowProperty("Pastille", 2000);
    /** WindowProperty for Wizard. */
    public static final WindowProperty WIZARD           = new WindowProperty("Wizard", 2000);
    /** WindowProperty for Wizard. */
    public static final WindowProperty WEATHER           = new WindowProperty("Weather", 2000);
    /** WindowProperty for Wizard. */
    public static final WindowProperty LOTTERY           = new WindowProperty("Lottery", 2000);
    /** WindowProperty for Cinema. */
    public static final WindowProperty CINEMA           = new WindowProperty("Cinema", 2000);
    /** WindowProperty for Preview. */
    public static final WindowProperty PREVIEW           = new WindowProperty("Preview", 2000);
    /** WindowProperty for PCS. */
    public static final WindowProperty PCS           = new WindowProperty("PCS", 2000);
    /** WindowProperty for PVR's Recording Icon. */
    public static final WindowProperty PVR_RECORDING_ICON = new WindowProperty("Pvr.RecordingIcon", 1200);
    /** WindowProperty for PVR's Video feedback icon. */
    public static final WindowProperty PVR_VIDEO_FEEDBACK = new WindowProperty("Pvr.VideoFeedback", 1100);
    /** WindowProperty for EPG Background. */
    public static final WindowProperty EPG_BACKGROUND     = new WindowProperty("Epg.Background", 1000);

    /** WindowProperty for Parental Wizard. */
    public static final WindowProperty PARENTAL_WIZARD           = new WindowProperty("Parental", 2500);

    /** WindowProperty for iTV-Application Popup. */
    public static final WindowProperty ITV_POPUP           = new WindowProperty("ITV.Popup", 5022);

    /** WindowProperty for ISA. */
    public static final WindowProperty ISA	= new WindowProperty("ISA", 5890);
    
    /** WindowProperty for Common Message. */
    public static final WindowProperty COMMON_MESSAGE = new WindowProperty("CommonMessage", 6770);
    
    /** WindowProperty for SimilarView in EPG. */
    public static final WindowProperty SIMILARE_VIEW      = new WindowProperty("SimilarView", 5020);

    /** WindowProperty for VOD's popup. */
    public static final WindowProperty VOD_POPUP      = new WindowProperty("VOD.Popup", 6700);

    /** Window name. */
    private String windowName;
    /** Window priority. */
    private int windowPriority;
    /** Adaptable. */
    private boolean isAdaptable;

    public WindowProperty(final String name, final int priority) {
        this(name, priority, true);
    }

    public WindowProperty(final String name, final int priority, final boolean adaptable) {
        this.windowName = name;
        this.windowPriority = priority;
        this.isAdaptable = adaptable;
    }

    /** Returns the name of this Window. */
    public String getName() {
        return windowName;
    }

    /** Returns the priority of this Window. */
    public int getPriority() {
        return windowPriority;
    }

    /** Shortcut API to create LayeredDialog. */
    public LayeredUI createLayeredDialog(LayeredWindow window, Rectangle bounds,
                                              LayeredKeyHandler keyHandler) {
        return LayeredUIManager.getInstance().createLayeredDialog(
                windowName, windowPriority, isAdaptable,
                window, bounds, keyHandler);
    }

    /** Shortcut API to create LayeredKeyHandler. */
    public LayeredUI createLayeredKeyHandler(LayeredKeyHandler keyHandler) {
        return LayeredUIManager.getInstance().createLayeredKeyHandler(
                windowName, windowPriority, keyHandler);
    }

    /** Shortcut API to create LayeredWindow. */
    public LayeredUI createLayeredWindow(LayeredWindow window, Rectangle bounds) {
        return LayeredUIManager.getInstance().createLayeredWindow(
                windowName, windowPriority, isAdaptable, window, bounds);
    }

    public static void dispose(LayeredUI ui) {
        LayeredUIManager.getInstance().disposeLayeredUI(ui);
    }

}

package com.videotron.tvi.illico.util;

import java.lang.reflect.Method;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is the Wrapper class of com.alticast.util.GraphicsMemory
 * to prevent NoClassDefFoundError.
 *
 * <code>GraphicsMemory</code>provides the function that can extract current
 * using amount of graphics memory for optimize application
 *
 * @version $Revision: 1.4 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public final class GraphicsMemory {

    /** Method instance. */
    private static Method method;

    static {
        try {
            Class c = Class.forName("com.alticast.util.GraphicsMemory");
            method = c.getMethod("getUsingGraphicsMemory", null);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    public static int getUsingGraphicsMemory() {
        if (method != null) {
            try {
                return ((Integer) method.invoke(null, null)).intValue();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return 0;
    }

}

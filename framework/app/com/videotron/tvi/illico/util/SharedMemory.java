package com.videotron.tvi.illico.util;

import java.lang.reflect.Method;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is the Wrapper class of com.alticast.util.SharedMemory
 * to prevent NoClassDefFoundError.
 * If the class is not supported by system, it does nothing.
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public final class SharedMemory {

    /** true, if the SharedMemory is supported by system. */
    private static final boolean SUPPORT;

    /** Get method of com.alticast.util.SharedMemory. */
    private static Method get;
    /** Put method of com.alticast.util.SharedMemory. */
    private static Method put;
    /** Remove method of com.alticast.util.SharedMemory. */
    private static Method remove;
    /** Singleton instance of com.alticast.util.SharedMemory. */
    private static Object smInstance;

    static {
        boolean support = false;
        try {
            Class c = Class.forName("com.alticast.util.SharedMemory");
            Method m = c.getMethod("getInstance", null);
            get = c.getMethod("get", new Class[] {Object.class});
            put = c.getMethod("put", new Class[] {Object.class, Object.class});
            remove = c.getMethod("remove", new Class[] {Object.class});
            smInstance = m.invoke(null, null);
            support = true;
        } catch (Exception ex) {
            Log.print(ex);
        }
        SUPPORT = support;
        if (Log.INFO_ON) {
            Log.printInfo("SharedMemory.SUPPORT = " + SUPPORT);
        }
    }

    /** Singleton instance of this SharedMemory. */
    private static SharedMemory instance = new SharedMemory();

    /**
     * Returns the singleton instance of this SharedMemory.
     *
     * @return the singleton instance.
     */
    public static SharedMemory getInstance() {
        return instance;
    }

    /** Constructor. */
    private SharedMemory() {
    }

    /**
     * Returns the value to which the specified key is mapped in the SharedMemory.
     *
     * @param key a key in the SharedMemory.
     * @return the value to which the key is mapped in this hashtable;
     *         null if the key is not mapped to any value in this hashtable
     *         or the SharedMemory is not supported in this system.
     */
    public Object get(Object key) {
        Object ret = null;
        if (SUPPORT) {
            try {
                ret = get.invoke(smInstance, new Object[] {key});
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return ret;
    }

    /**
     * Maps the specified key in this hashtable.
     *
     * @param key a key in the SharedMemory.
     * @param value the value.
     */
    public Object put(Object key, Object value) {
        if (SUPPORT) {
            try {
                return put.invoke(smInstance, new Object[] {key, value});
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return null;
    }

    /**
     * Removes the key and its corresponding value from the SharedMemory.
     *
     * @param key a key in the SharedMemory.
     * @return the value to which the key had been mapped in this hashtable,
     *             or null if the key did not have a mapping.
     */
    public Object remove(Object key) {
        Object ret = null;
        if (SUPPORT) {
            try {
                ret = remove.invoke(smInstance, new Object[] {key});
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        return ret;
    }


}

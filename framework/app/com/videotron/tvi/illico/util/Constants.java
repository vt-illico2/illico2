package com.videotron.tvi.illico.util;

import java.awt.Rectangle;

/**
 * This class is the utility class that contains Constant variables.
 *
 * @version $Revision: 1.15 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public final class Constants {

    //////////////////////////////////////////////////////////
    // screen related
    //////////////////////////////////////////////////////////

    /** width of application screen. */
    public static final int SCREEN_WIDTH = 960;
    /** height of application screen. */
    public static final int SCREEN_HEIGHT = 540;
    /** screen dimension. */
    public static final Rectangle SCREEN_BOUNDS = new Rectangle(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

    /** Language code for French. */
    public static final String LANGUAGE_FRENCH = "fr";
    /** Language code for English. */
    public static final String LANGUAGE_ENGLISH = "en";
    /** Language code for International. */
    public static final String LANGUAGE_INTERNATIONAL = "it";

    /** Language codes. */
    public static final String[] LANGUAGES = {
        LANGUAGE_FRENCH, LANGUAGE_ENGLISH
    };

    /** Rating index for all. */
    public static final int RATING_INDEX_ALL = 0;
    /** Rating index for G. */
    public static final int RATING_INDEX_G   = 1;
    /** Rating index for 8+. */
    public static final int RATING_INDEX_8   = 2;
    /** Rating index for 13+. */
    public static final int RATING_INDEX_13  = 3;
    /** Rating index for 16+. */
    public static final int RATING_INDEX_16  = 4;
    /** Rating index for 18+. */
    public static final int RATING_INDEX_18  = 5;

    /** Parental Rating names. */
    public static final String[] PARENTAL_RATING_NAMES = {
        "", "G", "8+", "13+", "16+", "18+"
    };

    public static final int DEFINITION_SD = 0;
    public static final int DEFINITION_HD = 1;
    public static final int DEFINITION_4K = 2;

    //////////////////////////////////////////////////////////
    // time related
    //////////////////////////////////////////////////////////

    /** milliseconds per one second. */
    public static final long MS_PER_SECOND  = 1000L;
    /** milliseconds per one minute. */
    public static final long MS_PER_MINUTE  = 60 * MS_PER_SECOND;
    /** milliseconds per one hour. */
    public static final long MS_PER_HOUR    = 60 * MS_PER_MINUTE;
    /** milliseconds per one day. */
    public static final long MS_PER_DAY     = 24 * MS_PER_HOUR;
    /** milliseconds per one week. */
    public static final long MS_PER_WEEK    =  7 * MS_PER_DAY;


    /** private constructor for utility class. */
    private Constants() {
    }

}

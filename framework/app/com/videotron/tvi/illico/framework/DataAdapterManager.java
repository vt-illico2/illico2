package com.videotron.tvi.illico.framework;

import com.videotron.tvi.illico.framework.adapter.InbandAdapter;
import com.videotron.tvi.illico.framework.adapter.OobAdapter;
import com.videotron.tvi.illico.framework.adapter.BuiltInFileAdapter;
import com.videotron.tvi.illico.framework.io.PropertiesReader;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.framework.io.FieldedTextReader;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.TextUtil;
import com.videotron.tvi.illico.log.Log;
import java.io.File;
import java.util.Enumeration;
import java.util.Properties;

/**
 * DataAdapterManager manages basic Data Adapters based on application's
 * configuration.
 *
 * @version $Revision: 1.23 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class DataAdapterManager {

    /** Default in-band carousel ID. */
    private static final int DEFAULT_CAROUSEL_ID = 28;

    /** root path of resource. */
    public static final String RESOURCE_ROOT = "resource/";
    /** path of resource text. */
    public static final String TEXT_RESOURCE_PATH = RESOURCE_ROOT + "text/";
    /** path of resource text. */
    public static final String IMAGE_RESOURCE_PATH = RESOURCE_ROOT + "image/";
    /** built-in list file. */
    public static final String BUILT_IN_LIST = "built_in.prop";
    /** inband list file. */
    public static final String IN_BAND_LIST = "ib.prop";
    /** oob list file. */
    public static final String OUT_OF_BAND_LIST = "oob.prop";
    /** oob nsap file. */
    public static final String OUT_OF_BAND_NSAP = "oob_nsap.dat";
    /** application config file. */
    public static final String APPLICATION_CONFIG = "application.prop";

    /** singleton instance. */
    private static DataAdapterManager instance = new DataAdapterManager();

    private BuiltInFileAdapter builtInFileAdapter;
    private OobAdapter oobAdapter;
    private InbandAdapter inbandAdapter;

    /** Constructor. */
    private DataAdapterManager() {
    }

    /**
     * Returns the singleton instance of this class.
     * @return DataAdapterManager
     */
    public static DataAdapterManager getInstance() {
        return instance;
    }

    /** Initalizes this DataAdapterManager. */
    public void init() {
        Log.printInfo("DataAdapterManager.init");
        readConfig();
        loadResources();

        builtInFileAdapter = new BuiltInFileAdapter(RESOURCE_ROOT + BUILT_IN_LIST);
        oobAdapter = new OobAdapter(RESOURCE_ROOT + OUT_OF_BAND_LIST, RESOURCE_ROOT + OUT_OF_BAND_NSAP);

        int cid = DataCenter.getInstance().getInt("DATA_CAROUSEL_ID", DEFAULT_CAROUSEL_ID);
        inbandAdapter = new InbandAdapter(RESOURCE_ROOT + IN_BAND_LIST, cid);
    }

    /** Starts adapters. */
    public void start() {
        Log.printInfo("DataAdapterManager.start");
        oobAdapter.start();
        builtInFileAdapter.start();
        inbandAdapter.start();
    }

    /** Disposes this DataAdapterManager. */
    public void dispose() {
    }

    /**
     * Returns the inband adapter.
     * @return InbandAdapter
     */
    public InbandAdapter getInbandAdapter() {
        return inbandAdapter;
    }

    /**
     * Returns the OOB adapter.
     * @return OobAdapter
     */
    public OobAdapter getOobAdapter() {
        return oobAdapter;
    }

    /**
     * Returns the Built-in file adapter.
     * @return BuiltInFileAdapter
     */
    public BuiltInFileAdapter getBuiltInFileAdapter() {
        return builtInFileAdapter;
    }

    /** Reads application config file and put the contents into DataCenter. */
    private void readConfig() {
        // reading application config.
        Properties p = PropertiesReader.read(RESOURCE_ROOT + APPLICATION_CONFIG);
        if (p != null) {
            Enumeration en = p.propertyNames();
            while (en.hasMoreElements()) {
                Object key = en.nextElement();
                DataCenter.getInstance().put(key.toString(), p.get(key));
            }
        }
    }

    /**
     * Reads built-in data files and put the File instance into DataCenter.
     * Then reads all text resource files in the <code>TEXT_RESOURCE_PATH</code>
     * and stores into the DataCenter.
     */
    private void loadResources() {
        // load text resources
        File textDir = new File(TEXT_RESOURCE_PATH);
        if (textDir.exists() && textDir.isDirectory()) {
            String[] list = textDir.list();
            if (list != null && list.length > 0) {
                FieldedTextReader reader = new FieldedTextReader(FieldedTextReader.TAB_DELIMITER);
                for (int i = 0; i < list.length; i++) {
                    String name = list[i];
                    storeTextResources(TextUtil.getBaseName(name), reader.read(new File(textDir, name)));
                }
            }
        }
    }

    /**
     * Stores the text data with parsed table data.
     *
     * @param baseName the name of text resource.
     * @param values parsed data by FieldedTextReader.
     * @see FieldedTextReader
     */
    private void storeTextResources(String baseName, String[][] values) {
        if (values == null) {
            return;
        }
        DataCenter dc = DataCenter.getInstance();
        String key = baseName + '.';
        for (int i = 0; i < values.length; i++) {
            String[] items = values[i];
            if (items != null && items.length > 0) {
                for (int j = 1; j < items.length && j <= Constants.LANGUAGES.length; j++) {
                    if (items[0] != null && items[0].length() > 0) {
                        dc.put(key + items[0] + '_' + Constants.LANGUAGES[j - 1], items[j]);
                    }
                }
            }
        }
    }

}

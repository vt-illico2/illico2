package com.videotron.tvi.illico.framework;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.SharedDataKeys;
import java.awt.Component;
import java.awt.Container;
import java.awt.MediaTracker;
import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Hashtable;

import java.net.URL;

/**
 * An image pool. This class caches images by name.
 *
 *
 * @version $Revision: 1.16 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class ImagePool extends Hashtable {

    private Hashtable flushed = new Hashtable();

    /** Default media tracker id for image data. */
    public static final int DEFAULT_ID = 1;

    /** Memory limit of ImagePool. */
    private final int maxMemory;
    /** Base directory of images. */
    private String baseDirectory;
    /** Toolkit to decode image. */
    private Toolkit toolkit = Toolkit.getDefaultToolkit();
    /** MediaTracker. */
//    private MediaTrackerWrapper tracker = new MediaTrackerWrapper();
    private Component component = new Container();
    private MediaTracker tracker = new MediaTracker(component);

    private MemorySizeMonitor status = new MemorySizeMonitor();

    private Class thisClass = getClass();

    /** Creates an ImagePool instance without memory limit. */
    public ImagePool() {
        this(Integer.MAX_VALUE);
    }

    /**
     * Creates an ImagePool instance with maximum memory size (in bytes).
     *
     * @param maxMemoryLimit the maximum memory limit in bytes.
     */
    public ImagePool(final int maxMemoryLimit) {
        // TODO - handling maximum memory size is not implemented yet.
        //        The only problem is that the image must be decoded.
        this.maxMemory = maxMemoryLimit;

        SharedMemory.getInstance().put(FrameworkMain.getInstance().getApplicationName() + "."
                        + SharedDataKeys.IMAGE_POOL_STATUS, status);
    }

    /**
     * Sets the base directory of this ImagePool.
     *
     * @param directory path of base directory.
     */
    public void setBaseDirectory(String directory) {
        this.baseDirectory = directory;
    }

    public Image get(String key) {
        return (Image) super.get(key);
    }

    /**
     * Returns the image with specified key. If the image is not in ImagePool,
     * this method will create and store Image.
     *
     * @param key image file name.
     * @return Image instance.
     */
    public Image getImage(String key) {
        Object value = super.get(key);
        if (value == null) {
            value = flushed.remove(key);
            if (value != null) {
                put(key, (Image) value);
                return (Image) value;
            } else {
                return createImage(key);
            }
        } else {
            return (Image) value;
        }
    }

    /**
     * Removes and flush the image from ImagePool.
     *
     * @param key image file name.
     * @return Image instance.
     */
    public Object remove(Object key) {
        Object value = super.remove(key);
        if (value != null && value instanceof Image) {
            Image image = (Image) value;
            tracker.removeImage(image);
            image.flush();

            flushed.put(key, image);
        }
        return value;
    }

    public Object removeAbsolutely(Object key) {
        Object value = super.remove(key);
        if (value != null && value instanceof Image) {
            Image image = (Image) value;
            tracker.removeImage(image);
            image.flush();
            image = null;
        }
        return value;
    }

    public void put(String key, Image image) {
        tracker.addImage(image, DEFAULT_ID);
        Image old = (Image) super.put(key, image);
        if (old != null) {
            tracker.removeImage(old);
            old.flush();
        }
    }

    /**
     * Creates and store Image.
     *
     * @param name image file name.
     * @return Image instance.
     */
    public Image createImage(String name) {
        URL url = thisClass.getResource("/" + baseDirectory + name);
        if (url == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ImagePool: can't find file : " + name);
            }
            return null;
        }

//        File file = new File(baseDirectory, name);
//        if (!file.exists()) {
//            if (Log.DEBUG_ON) {
//                Log.printDebug("ImagePool: can't find file : " + file);
//            }
//            return null;
//        }
//        Image img = toolkit.createImage(baseDirectory + name);
        Image img = toolkit.createImage(url);
        if (img == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ImagePool: fail to create image : " + name);
            }
            return null;
        }
        put(name, img);
        return img;
    }

    /**
     * Creates Image with bytes source and store Image with specified key.
     *
     * @param data source bytes of image file.
     * @param key hashtable key for the this Image instance.
     * @return Image instance.
     */
    public Image createImage(byte[] data, String key) {
        Image img = toolkit.createImage(data);
        if (img == null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("ImagePool fail to create image : " + key);
            }
            return null;
        }
        put(key, img);
        return img;
    }

    /**
     * Starts loading all images tracked by this ImagePool. This method waits
     * until all the images being tracked have finished loading.
     *
     * @see MediaTracker#waitForAll
     */
    public void waitForAll() {
        long started = System.currentTimeMillis();
        try {
            tracker.waitForAll();
        } catch (InterruptedException e) {
            Log.print(e);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("ImagePool.waitForAll: takes "
                + (System.currentTimeMillis() - started) + " ms: " + toString());
        }
    }

    /**
     * Starts loading all images tracked by this ImagePool with specified id.
     * This method waits until all the images with the specified identifier have
     * finished loading.
     *
     * @param id the identifier of the images to check
     * @see MediaTracker#waitForID(int)
     */
    public void waitForID(int id) {
        try {
            tracker.waitForID(id);
        } catch (InterruptedException e) {
            Log.print(e);
        }
    }

    /**
     * Flushes all image and clear this ImagePool.
     */
    public void clear() {
        // flush all images
        Enumeration keys = super.keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            remove(key);
        }
        // actually this is not meaningful.
        super.clear();
        if (Log.INFO_ON) {
            Log.printInfo("ImagePool.clear: " + toString());
        }
    }

    public String toString() {
        return "ImagePool[cached=" + size() + ", flushed=" + flushed.size() + "]";
    }

/*
    class MediaTrackerWrapper {
        MediaTracker mediaTracker = new MediaTracker(new Container());
        Hashtable idTable = new Hashtable();

        public synchronized void addImage(Image image, int id) {
            mediaTracker.addImage(image, id);
            Integer old = (Integer) idTable.get(image);
            int newId;
            if (old != null) {
                newId = Math.max(old.intValue(), id);
            } else {
                newId = id;
            }
            idTable.put(image, new Integer(id));
        }

        public synchronized void removeImage(Image image) {
            mediaTracker.removeImage(image);
            idTable.remove(image);
        }

        public void waitForID(int id) throws InterruptedException {
            mediaTracker.waitForID(id);
        }

        public void waitForAll() throws InterruptedException {
            mediaTracker.waitForAll();
        }

        public void flush(int maxId) {

        }
    }
*/

    class MemorySizeMonitor implements Runnable {

        private long currentSize = 0;

        public void run() {
            Log.printDebug("MemorySizeMonitor.run");
            long size = 0;
            for (Iterator it = values().iterator(); it.hasNext();) {
                Image im = (Image) it.next();
                int w = im.getWidth(component);
                if (w > 0) {
                    int h = im.getHeight(component);
                    if (h > 0) {
                        size += h * w * 4;
                    }
                }
            }
            currentSize = size;
            Log.printDebug("MemorySizeMonitor.currentSize = " + currentSize);
        }

        public String toString() {
            return (currentSize / 1024) + " KB [cached=" + size() + ", flushed=" + flushed.size() + "]";
        }
    }

}


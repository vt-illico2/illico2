package com.videotron.tvi.illico.framework.effect;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This effect provides the sweep effect to provide bounds.
 *
 */
public class ImageMovingEffect extends Effect {
	private int[] pointX;
	private int mode;
	private Image sweep;
    private int imageWidth;

     /*
     * @param c AnimationRequestor.
     * @param frameCount total frame count.
     * @param fadeOption fade option.
     */
    public ImageMovingEffect(Component c, Image image, int frameCount, boolean toRight, Rectangle r) {
        super(c, frameCount);

        pointX = new int[frameCount];

        sweep = image;

    	int diffX = r.width;
        for (int i = 0; i < frameCount; i++) {
            if (toRight) {
                pointX[i] = Math.round((float) (diffX * i) / (frameCount-1));
            } else {
                pointX[i] = diffX - Math.round((float) (diffX * i) / (frameCount-1));
            }
        }
    	targetBounds = new Rectangle(r);
        relativeClipBounds = new Rectangle(r);
        frameDelay = 20L;
        updateBackgroundBeforeStart(false);
    }

    protected void start(boolean now) {
        imageWidth = sweep.getWidth(component);
        super.start(now);
    }

    /**
     * Paints contents.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
    	g.drawImage(image, 0, 0, null);
        g.drawImage(sweep, pointX[frame], 0, imageWidth, targetBounds.height, null);

    }
}


package com.videotron.tvi.illico.framework.effect;

/**
 * This is a listener interface that represents Animation target.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public interface AnimationRequestor {

    /**
     * Determines the effect should be start or not.
     *
     * @param effect Effect object.
     * @return true if the effect should be started.
     */
    boolean skipAnimation(Effect effect);

    /**
     * Called when the specified effect starts.
     *
     * @param effect Effect object.
     */
    void animationStarted(Effect effect);

    /**
     * Called when the specified effect ends.
     *
     * @param effect Effect object.
     */
    void animationEnded(Effect effect);

}

package com.videotron.tvi.illico.framework.effect;

import java.awt.Rectangle;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Container;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Toolkit;
import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.UnsupportedDrawingOperationException;
import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBGraphics;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.alticast.util.SharedMemory;

/**
 * This class is an abstract class that represent Animation Effect.
 * One instance is generated for every moving target.
 *
 * @version $Revision: 1.34 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public abstract class Effect implements Runnable, AnimationRequestor {

    /** Time delay for the effect frame. This is related with FPS. */
    public static final long DEFAULT_FRAME_DEALY = 40L;

    /** Used for check time for LOG. */
    private static long lastReadyTime = 0;

    /** True if the Effect will be enabled. */
    private static boolean enabled = true;

    /** Target Component. */
    protected Component component;
    /** Requestor. */
    protected AnimationRequestor requestor;
    /** inner bounds of target component */
    protected Rectangle targetBounds;
    /** animation clip bounds. */
    protected Rectangle relativeClipBounds;
    /** animation clip bounds of screen graphics. */
    protected Rectangle absoluteClipBounds;

    /** Total frame count of this Effect. */
    protected int frameCount;

    protected long frameDelay = DEFAULT_FRAME_DEALY;

    /** True if the application need to update background before run this Effect. */
    protected boolean updateBackground = true;

    protected boolean includeTargetToBackground = false;

    /** Alpha composites for every frames. By default, it will be <code>SrcOver</code>. */
    protected DVBAlphaComposite[] alpha;
    /** Root window of this system. */
    private static Container root;
    /** <code>Toolkit</code> to synchronize graphics. */
    private static Toolkit toolkit = Toolkit.getDefaultToolkit();

    /** SharedMemory key to store root window. */
    private static final String KEY_ROOT_WINDOW = "Effect.rootWindow";

    private static DVBBufferedImage background;
    private static DVBBufferedImage image;
    private static DVBBufferedImage offScreen;

    /**
     * Sets the root window of this application.
     *
     * @param rootWindow root windows of this application.
     */
    public static void init() {
        if (Log.DEBUG_ON) {
            //Log.printDebug("Effect init.");
        }
        SharedMemory sm = SharedMemory.getInstance();
        synchronized (sm) {
            background = getFullScreenImage(sm, "DVBBufferedImage.1");
            image      = getFullScreenImage(sm, "DVBBufferedImage.2");
            offScreen  = getFullScreenImage(sm, "DVBBufferedImage.3");
        }
    }

    private static DVBBufferedImage getFullScreenImage(SharedMemory sm, String key) {
        DVBBufferedImage buf = (DVBBufferedImage) sm.get(key);
        if (buf == null) {
            buf = new DVBBufferedImage(Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            sm.put(key, buf);
        }
        return buf;
    }

    public static DVBBufferedImage getOffScreenImage() {
        return offScreen;
    }

    public static Container getRoot(Component component) {
        root = (Container) SharedMemory.getInstance().get(KEY_ROOT_WINDOW);
        if (root != null) {
            return root;
        }
        if (component == null) {
            return null;
        }
        Container c = component.getParent();
        while (c != null) {
            root = c;
            c = c.getParent();
        }
        if (root != null) {
            SharedMemory.getInstance().put(KEY_ROOT_WINDOW, root);
        }
        return root;
    }

    /**
     * Enables or disables Effect.
     *
     * @param b If true, Effect is enabled; otherwise disabled.
     */
    public static void setEnabled(boolean b) {
        enabled = b;
    }

    /**
     * Constructor.
     *
     * @param c Target component to apply this Effect.
     * @param frameCount frame count of this Effect.
     */
    public Effect(final Component c, final int frameCount) {
        this.component = c;
        if (c instanceof AnimationRequestor) {
            this.requestor = (AnimationRequestor) c;
        } else {
            this.requestor = this;
        }

        this.frameCount = frameCount;
        this.targetBounds = new Rectangle(0, 0, c.getWidth(), c.getHeight());
        this.relativeClipBounds = new Rectangle(targetBounds);

        alpha = new DVBAlphaComposite[frameCount];
        for (int i = 0; i < frameCount; i++) {
            alpha[i] = DVBAlphaComposite.SrcOver;
        }
    }

    /**
     * Sets the AnimationRequestor.
     *
     * @param r AnimationRequestor.
     */
    public void setRequestor(AnimationRequestor r) {
        this.requestor = r;
    }

    /**
     * Sets the frameDelay of this Effect.
     *
     * @param delay the frameDelay to set
     */
    public void setFrameDelay(long delay) {
        this.frameDelay = delay;
    }

    /**
     * Updates background before run this Effect. Due to AWT paint mechanism,
     * outer background (outside of animation target) shall be mismatched with
     * inner background (inside of animation target). It may happens when application's
     * background is updated just before running Effect. In this case the change of
     * background may not be applied in the screen, so application can enable this method.
     *
     * @param update If true, Effect will update background before run this Effect.
     */
    public void updateBackgroundBeforeStart(boolean update) {
        this.updateBackground = update;
    }

    /**
     * Sets the target bounds of animation target. This coordinate should be relative to
     * target's locatation.
     *
     * @param x the x coordinate of this target bounds.
     * @param y the y coordinate of this target bounds.
     * @param w the width of this target bounds.
     * @param h the height of this target bounds.
     */
    public void setTargetBounds(int x, int y, int w, int h) {
        setTargetBounds(new Rectangle(x, y, w, h));
    }

    /**
     * Sets the target bounds of animation target. This coordinate should be relative to
     * target's locatation.
     *
     * @param r the target bounds.
     */
    public void setTargetBounds(Rectangle r) {
        this.targetBounds = r;
    }

    /**
     * Sets the clip bounds of this Effect.
     *
     * @param x the x coordinate of this clip bounds.
     * @param y the y coordinate of this clip bounds.
     * @param w the width of this clip bounds.
     * @param h the height of this clip bounds.
     */
    public final void setClipBounds(int x, int y, int w, int h) {
        setClipBounds(new Rectangle(x, y, w, h));
    }

    /**
     * Sets the clip bounds of this Effect.
     *
     * @param r the clip bounds.
     */
    public final void setClipBounds(Rectangle r) {
        this.relativeClipBounds = r;
    }

    /**
     * Returns the target bounds of this Effect.
     *
     * @return the target bounds.
     */
    public Rectangle getTargetBounds() {
        return targetBounds;
    }

    /**
     * Returns the clip bounds of this Effect.
     *
     * @return the clip bounds.
     */
    public Rectangle getClipBounds() {
        return relativeClipBounds;
    }

    /** Starts this Effect. */
    public final void start() {
        start(true);
    }

    /**
     * Starts this Effect after all pending events in event queue are processed.
     */
    public final void startLater() {
        start(false);
    }

    /**
     * Starts this Effect.
     * @param now if true, effect will start immediately.
     */
    protected void start(boolean now) {
        if (Log.DEBUG_ON) {
            //Log.printDebug("Effect.start");
        }
        if (background == null) {
            init();
        }
        if (root == null) {
            getRoot(component);
        }
        requestor.animationStarted(this);
        if (!enabled || requestor.skipAnimation(this) || root == null) {
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect.start : skip animation");
            }
            requestor.animationEnded(this);
        } else {
            if (now && EventQueue.isDispatchThread()) {
                if (Log.DEBUG_ON) {
                    //Log.printDebug("Effect.start : direct run in EDT");
                }
                run();
            } else {
                if (Log.DEBUG_ON) {
                    //Log.printDebug("Effect.start : invokeLater");
                }
                EventQueue.invokeLater(this);
            }
        }
        if (Log.DEBUG_ON) {
            //Log.printDebug("Effect.start : end");
        }
    }

    private void clear(DVBGraphics g) {
        g.setClip(null);
        try {
            g.setDVBComposite(DVBAlphaComposite.Src);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }
        g.clearRect(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        try {
            g.setDVBComposite(DVBAlphaComposite.SrcOver);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }
    }

    /**
     * This method will be run as a separated Thread or in EventDispatchThread.
     * @see Runnable#run
     */
    public void run() {
        DVBGraphics rGraphics = (DVBGraphics) root.getGraphics();
        if (rGraphics == null) {
            Log.printWarning("Effect.run : failed to create root graphics");
            requestor.animationEnded(this);
            root = null;
            SharedMemory.getInstance().remove(KEY_ROOT_WINDOW);
            return;
        }

        DVBGraphics bGraphics = background.createGraphics();
        DVBGraphics iGraphics = image.createGraphics();
        DVBGraphics oGraphics = offScreen.createGraphics();

        long before = System.currentTimeMillis();

        Point p = getAbsoluteLocation();
        int px = p.x;
        int py = p.y;
        int cbx = relativeClipBounds.x + px;
        int cby = relativeClipBounds.y + py;
        int cbw = relativeClipBounds.width;
        int cbh = relativeClipBounds.height;
        int cbx2 = cbx + cbw;
        int cby2 = cby + cbh;

        this.absoluteClipBounds = Constants.SCREEN_BOUNDS.intersection(
                                            new Rectangle(cbx, cby, cbw, cbh));
        int tbx = targetBounds.x;
        int tby = targetBounds.y;
        int tbw = targetBounds.width;
        int tbh = targetBounds.height;

        int ix = tbx + px;
        int iy = tby + py;

        try {
            rGraphics.setDVBComposite(DVBAlphaComposite.Src);
        } catch (UnsupportedDrawingOperationException ex) {
            Log.print(ex);
        }

        boolean oldVisible = component.isVisible();
        // prepare background image
        if (updateBackground) {
            bGraphics.setClip(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
            // update background
            paintBackground(bGraphics);
            rGraphics.drawImage(background, 0, 0, null);
            // prepare background
            component.setVisible(includeTargetToBackground);
            clear(bGraphics);
            paintBackground(bGraphics);
        } else {
            bGraphics.setClip(absoluteClipBounds);
            component.setVisible(includeTargetToBackground);
            paintBackground(bGraphics);
        }

        if (Log.DEBUG_ON) {
            lastReadyTime = System.currentTimeMillis() - before;
            //Log.printDebug("Effect: bGraphics ready = " + lastReadyTime);
        }
        // prepare animation image
        if (Log.DEBUG_ON) {
            //Log.printDebug("Effect: relativeClipBounds = " + relativeClipBounds);
            //Log.printDebug("Effect: absoluteClipBounds = " + absoluteClipBounds);
            //Log.printDebug("Effect: targetBounds       = " + targetBounds);
        }

        component.setVisible(true);
        iGraphics.setClip(0, 0, tbw, tbh);
        iGraphics.translate(-tbx, -tby);
        try {
            component.paint(iGraphics);
        } catch (Exception ex) {
        }
        iGraphics.translate(tbx, tby);

        Rectangle oldClip = rGraphics.getClipBounds();
        rGraphics.setClip(absoluteClipBounds);
        if (Log.DEBUG_ON) {
            lastReadyTime = System.currentTimeMillis() - before;
            //Log.printDebug("Effect: __before_____loop____ " + lastReadyTime);
            //Log.printDebug("Effect: oldClip = " + oldClip);
        }

        // animation loop
        long last;
        long time;
        int i = 0;
        while (true) {
            last = System.currentTimeMillis();
            try {
                oGraphics.setDVBComposite(DVBAlphaComposite.Src);
            } catch (UnsupportedDrawingOperationException ex) {
                Log.print(ex);
            }
            oGraphics.drawImage(background, 0, 0, cbw, cbh, cbx, cby, cbx2, cby2, null);

            try {
                oGraphics.setDVBComposite(alpha[i]);
            } catch (UnsupportedDrawingOperationException ex) {
                Log.print(ex);
            }
            animate(oGraphics, image, i);

            rGraphics.drawImage(offScreen, cbx, cby, cbx2, cby2, 0, 0, cbw, cbh, null);
            toolkit.sync();
            i++;
            if (i == frameCount) {
                break;
            }

            time = System.currentTimeMillis();
            long diff = time - last;
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect: __in_____loop____ " + diff);
            }
            if (diff < frameDelay) {
                try {
                    Thread.sleep(frameDelay - diff);
                } catch (Exception ex) {
                    Log.print(ex);
                }
            }
        }
        if (Log.DEBUG_ON) {
            //Log.printDebug("Effect: __after_____loop____");
        }
        component.setVisible(oldVisible);
        rGraphics.setClip(oldClip);
        rGraphics.dispose();

        clear(bGraphics);
        clear(iGraphics);
        clear(oGraphics);

        bGraphics.dispose();
        iGraphics.dispose();
        oGraphics.dispose();

        requestor.animationEnded(this);
    }

    /**
     * Paints contents of the specified frame to the graphics context.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected abstract void animate(Graphics g, DVBBufferedImage image, int frame);

    /**
     * Paints background components to the graphics.
     *
     * @param g graphics context
     */
    private void paintBackground(Graphics g) {
        try {
            Component[] child = root.getComponents();

            int i = child.length - 1;
            Component c;
            while (i >= 0) {
                c = child[i];
                if (c != null && c.isVisible()) {
                    Point p = c.getLocation();
                    Graphics g1 = g.create();
                    g1.translate(p.x, p.y);
                    try {
                        c.paint(g1);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                    g1.dispose();
                }
                i--;
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /**
     * Returns the absolute location of this component.
     *
     * @return the absolute location of this component.
     */
    private Point getAbsoluteLocation() {
        return getAbsoluteLocation(this.component);
    }

    /**
     * Returns the absolute location of the speficied component.
     *
     * @return the absolute location of the speficied component.
     */
    protected static Point getAbsoluteLocation(Component comp) {
        Point p = comp.getBounds().getLocation();
        Container c = comp.getParent();
        while (c != null) {
            Point np = c.getLocation();
            p.translate(np.x, np.y);
            c = c.getParent();
        }
        return p;
    }

    /**
     * Determines the effect should be start or not.
     *
     * @param effect Effect object.
     * @return true if the effect should be started.
     */
    public boolean skipAnimation(Effect effect) {
        return false;
    }

    /**
     * Called when the specified effect starts.
     *
     * @param effect Effect object.
     */
    public void animationStarted(Effect effect) {
    }

    /**
     * Called when the specified effect ends.
     *
     * @param effect Effect object.
     */
    public void animationEnded(Effect effect) {
    }

}


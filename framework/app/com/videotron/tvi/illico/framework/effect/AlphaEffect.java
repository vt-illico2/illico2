package com.videotron.tvi.illico.framework.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;

import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBBufferedImage;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;

/**
 * This effect provides the fade effect to the component.
 *
 * @version $Revision: 1.12 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public class AlphaEffect extends Effect {
    /** Do not fade. */
    public static final int DO_NOT_FADE = 0;
    /** Fade in. */
    public static final int FADE_IN  = 1;
    /** Fade out. */
    public static final int FADE_OUT = 2;
    /** Dissolve. */
    public static final int DISSOLVE = 3;

    /** Fade option. */
    protected int fade;
    
    private Image nextImage;
    private Point offset = new Point();
    
    /**
     * Creates an AlphaEffect object.
     *
     * @param c AnimationRequestor.
     * @param frameCount total frame count.
     * @param fadeOption fade option.
     */
    public AlphaEffect(final Component c, final int frameCount,
                                                   final int fadeOption) {
        super(c, frameCount);
        this.fade = fadeOption;
        this.alpha = new DVBAlphaComposite[frameCount];

        for (int i = 0; i < frameCount; i++) {
            switch (fade) {
                case FADE_IN :
                    alpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER,
                                                            (float) (i+1) / (frameCount + 1));
                    break;
                case FADE_OUT :
                case DISSOLVE :
                    alpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER,
                                         (float) (frameCount - i) / (frameCount + 1));
                    break;
                default :
                    alpha[i] = DVBAlphaComposite.SrcOver;
                    break;
            }
        }
    }
    
    public void setNextImage(Image nextImage) {
    	this.nextImage = nextImage;
    }
    
    public void setOffset(int x, int y) {
    	offset.x = x;
    	offset.y = y;
    }

    /**
     * Paints contents.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        g.drawImage(image, 0, 0, null);
        if (nextImage != null) {
        	DVBAlphaComposite opp = DVBAlphaComposite.getInstance(
        			DVBAlphaComposite.SRC_OVER, 1 - alpha[frame].getAlpha());
        	try {
				((DVBGraphics) g).setDVBComposite(opp);
			} catch (UnsupportedDrawingOperationException e) {}
			g.drawImage(nextImage, offset.x, offset.y, null);
        }
    }

}


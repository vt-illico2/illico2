package com.videotron.tvi.illico.framework.effect;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This effect provides the sweep effect to provide bounds.
 *
 */
public class RefreshEffect extends Effect {
    private static final int RIGHT_MARGIN = 0;

	public static final int MODE_EXPAND = 0;
	public static final int MODE_COLLAPSE = 1;
	private int[] pointX;
	private int[] width;
	private Image sweep;

    private DVBBufferedImage beforeImage;

     /*
     * @param c AnimationRequestor.
     * @param frameCount total frame count.
     * @param fadeOption fade option.
     */
    public RefreshEffect(Component c, int frameCount, Image sweep) {
        super(c, frameCount);
        this.sweep = sweep;

        pointX = new int[frameCount];
        width = new int[frameCount];
    }

    public void readyToStart(int x, int y, int w, int h) {
        targetBounds = new Rectangle(x, y, w, h);
        relativeClipBounds = new Rectangle(x, y, w, h);
        synchronized (this) {
            if (beforeImage != null) {
                beforeImage.dispose();
            }
            beforeImage = new DVBBufferedImage(w, h);
        }
        Graphics g = beforeImage.getGraphics();
        g.translate(-x, -y);
        component.paint(g);
        g.dispose();

        int middle = frameCount / 2;
        int right = w - RIGHT_MARGIN;

        for (int i = 0; i <= middle; i++) {
            pointX[i] = 0;
            width[i] = Math.round((float) (right * i) / middle) + RIGHT_MARGIN;
        }
        for (int i = middle + 1; i < frameCount; i++) {
            pointX[i] = Math.round((float) (right * (i - middle)) / (frameCount - middle - 1));
            width[i] = w - pointX[i];
        }
    }

    public void run() {
        super.run();
        synchronized (this) {
            if (beforeImage != null) {
                beforeImage.dispose();
                beforeImage = null;
            }
        }
    }

    private void start(int x, int y, int width, int height) {
    	int diffX = width;
        int fcs = (frameCount - 1) * (frameCount - 1);
        for (int i = 0; i < frameCount; i++) {
        	int k = frameCount - 1 - i;
            pointX[i] = diffX - Math.round((float) diffX * k * k / fcs);
        }
    	targetBounds = new Rectangle(x, y, width, height);
        relativeClipBounds = new Rectangle(x, y, width, height);
    	start(true);
    }

    /**
     * Paints contents.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        int x = pointX[frame];
        int w = width[frame];
        int r = x + w;
        int h = targetBounds.height;
        g.drawImage(image, 0, 0, r, h,
                           0, 0, r, h, null);
        g.drawImage(beforeImage, r, 0, targetBounds.width, h,
                                 r, 0, targetBounds.width, h, null);

        g.drawImage(sweep, x, 0, w, h, null);
    }
}


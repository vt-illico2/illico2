package com.videotron.tvi.illico.framework.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

/**
 * This effect provides the page flip effect between 2 images.
 */
public class FlipEffect extends Effect {
    public static final int FLIP_FROM_TOP = 3;
    public static final int FLIP_FROM_BOTTOM = 4;
    public static final int FLIP_FROM_LEFT = 5;
    public static final int FLIP_FROM_RIGHT = 6;
    
    protected int flip;

    private Image nextImage;
    private Point offset = new Point();
    
    private Rectangle[] rect1;
    private Rectangle[] rect2;
    
    /**
     * Creates an FlipEffect object.
     *
     * @param c AnimationRequestor.
     * @param frameCount total frame count.
     * @param flipOption fade option.
     */
    public FlipEffect(final Component c, final int frameCount,
                                                   final int flipOption) {
        super(c, frameCount);
        this.flip = flipOption;

        rect1 = new Rectangle[frameCount];
        rect2 = new Rectangle[frameCount];
        for (int i = 0; i < frameCount; i++) {
            rect1[i] = c.getBounds();
            rect2[i] = c.getBounds();
            rect1[i].x = 0;
            rect1[i].y = 0;
            rect2[i].x = 0;
            rect2[i].y = 0;
            switch (flip) {
                case FLIP_FROM_TOP :
                    rect2[i].height = (int) (rect2[i].height * ((float) (i+1) / (frameCount + 1)));
                    rect1[i].y = rect2[i].height;
                    break;
                case FLIP_FROM_BOTTOM :
                    rect2[i].y = rect2[i].height - (int) (rect2[i].height * ((float) (i+1) / (frameCount + 1)));
                    rect1[i].height = rect2[i].y;
                    break;
                case FLIP_FROM_LEFT :
                    rect2[i].width = (int) (rect2[i].width * ((float) (i+1) / (frameCount + 1)));
                    rect1[i].x = rect2[i].width;
                    break;
                case FLIP_FROM_RIGHT :
                    rect2[i].x = rect2[i].width - (int) (rect2[i].width * ((float) (i+1) / (frameCount + 1)));
                    rect1[i].width = rect2[i].x;
                    break;
            }
        }
    }
    
    public void setNextImage(Image nextImage) {
        this.nextImage = nextImage;
    }
    
    public void setOffset(int x, int y) {
        offset.x = x;
        offset.y = y;
    }

    /**
     * Paints contents.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
//        g.drawImage(image, rect1[frame].x, rect1[frame].y, rect1[frame].width, rect1[frame].height,
//              rect1[frame].x, rect1[frame].y, rect1[frame].width, rect1[frame].height, null);
//        if (nextImage != null) {
//          g.drawImage(nextImage, rect2[frame].x + offset.x, rect2[frame].y + offset.y, 
//                  rect2[frame].width + offset.x, rect2[frame].height + offset.y,
//                  rect2[frame].x, rect2[frame].y, rect2[frame].width, rect2[frame].height, null);
//        }
        Rectangle ori = g.getClipBounds();
        g.clipRect(rect1[frame].x, rect1[frame].y, 
                rect1[frame].width - rect1[frame].x, 
                rect1[frame].height - rect1[frame].y);  
        g.drawImage(image, 0, 0, null);
        g.setClip(ori);
        if (nextImage != null) {
            g.clipRect(rect2[frame].x, rect2[frame].y, 
                    rect2[frame].width - rect2[frame].x, 
                    rect2[frame].height - rect2[frame].y);  
            g.drawImage(nextImage, offset.x, offset.y, null);
            
        }
        g.setClip(ori);
        
        
    }

}


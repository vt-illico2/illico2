package com.videotron.tvi.illico.framework.effect;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.FontResource;

/**
 * This effect provides the sweep effect to provide bounds.
 *
 */
public class SweepEffect extends Effect {
	public static final int MODE_EXPAND = 0;
	public static final int MODE_COLLAPSE = 1;
	private int[] pointX;
	private int mode;
	private Image sweep;

     /*
     * @param c AnimationRequestor.
     * @param frameCount total frame count.
     * @param fadeOption fade option.
     */
    public SweepEffect(Component c, int frameCount) {
        super(c, frameCount);

        pointX = new int[frameCount];

        sweep = DataCenter.getInstance().getImage("sweep.png");
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }
    public int getMode() {
    	return mode;
    }
    public void startExpand(int x, int y, int width, int height) {
    	mode = MODE_EXPAND;
    	start(x, y, width, height);
    }
    public void startCollapse() {
    	mode = MODE_COLLAPSE;
    	start(targetBounds.x, targetBounds.y, targetBounds.width, targetBounds.height);
    }
    private void start(int x, int y, int width, int height) {
    	int diffX = width;
        int fcs = (frameCount - 1) * (frameCount - 1);
        for (int i = 0; i < frameCount; i++) {
        	int k = frameCount - 1 - i;
            pointX[i] = diffX - Math.round((float) diffX * k * k / fcs);
        }
    	targetBounds = new Rectangle(x, y, width, height);
        relativeClipBounds = new Rectangle(x, y, width, height);
    	start(true);
    }

    /**
     * Paints contents.
     *
     * @param g graphics context.
     * @param image buffered image of target component.
     * @param frame frame to be painted.
     */
    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
    	g.drawImage(image, 0, 0, null);
    	if (mode == MODE_EXPAND) {
    		g.drawImage(sweep, 0, 0, pointX[frame], targetBounds.height, null);
    		g.setFont(FontResource.BLENDER.getFont(16));
    		g.setColor(Color.white);
    	} else { // collapse
    		g.drawImage(sweep, pointX[frame], 0, targetBounds.width-pointX[frame], targetBounds.height, null);
    	}

    }
}


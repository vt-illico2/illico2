package com.videotron.tvi.illico.framework.effect;

import java.awt.*;
import org.dvb.ui.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * IconHighlightEffect
 *
 * @version $Revision: 1.9 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public class ScaleEffect extends Effect {

    protected double[] scale;

    protected int[] pointX;
    protected int[] pointY;
    protected int[] widths;
    protected int[] heights;

    protected int imageWidth;
    protected int imageHeight;

    protected int maxWidth;
    protected int maxHeight;

    protected ScaleFactor factor;

    protected int clipBoundsXOffset;
    protected int clipBoundsYOffset;

    public ScaleEffect(Component c, int frameCount, ScaleFactor f) {
        super(c, frameCount);
        this.factor = f;

        this.pointX = new int[frameCount];
        this.pointY = new int[frameCount];
        this.widths = new int[frameCount];
        this.heights = new int[frameCount];

        this.scale = new double[frameCount];

        Rectangle r = c.getBounds();
        this.imageWidth = r.width;
        this.imageHeight = r.height;

        for (int i = 0; i < frameCount; i++) {
            double step = (double) i / (frameCount - 1);
            scale[i] = factor.getScale(step);
            alpha[i] = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, (float) factor.getAlpha(step));
            widths[i] = (int) (imageWidth * scale[i]);
            heights[i] = (int) (imageHeight * scale[i]);
            maxWidth = Math.max(maxWidth, widths[i]);
            maxHeight = Math.max(maxHeight, heights[i]);
        }
        for (int i = 0; i < frameCount; i++) {
            pointX[i] = (maxWidth - widths[i]) / 2;
            pointY[i] = (maxHeight - heights[i]) / 2;
        }
        setTargetBounds(0, 0, imageWidth, imageHeight);
        clipBoundsXOffset = (imageWidth - maxWidth) / 2;
        clipBoundsYOffset = (imageHeight - maxHeight) / 2;
        setClipBounds(clipBoundsXOffset, clipBoundsYOffset, maxWidth, maxHeight);
    }

    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        g.drawImage(image, pointX[frame], pointY[frame], pointX[frame] + widths[frame], pointY[frame] + heights[frame],
                           0, 0, imageWidth, imageHeight, null);
    }

}


package com.videotron.tvi.illico.framework.effect;

import java.awt.*;
import org.dvb.ui.*;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * MovingEffect
 *
 * @version $Revision: 1.11 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public class MovingEffect extends AlphaEffect {

    /** 등가속 운동 */
    public static final short LINEAR       = 10;
    /** 중력 가속도 (점점 빨라짐) */
    public static final short GRAVITY      = 11;
    /** 중력 가속도 (점점 느려짐) */
    public static final short ANTI_GRAVITY = 12;

    protected int[] pointX;
    protected int[] pointY;

    protected Point from;
    protected Point to;

    public MovingEffect(Component c, int frameCount, Point from, Point to, int fade) {
        this(c, frameCount, from, to, fade, LINEAR);
    }

    public MovingEffect(Component c, int frameCount, Point from, Point to,
            int fade, short acceleration) {
        super(c, frameCount, fade);
        this.from = from;
        this.to = to;
        resetClipBounds();

        this.pointX = new int[frameCount];
        this.pointY = new int[frameCount];

        int diffX = to.x - from.x;
        int diffY = to.y - from.y;

        int fcs = (frameCount - 1) * (frameCount - 1);

        for (int i = 0; i < frameCount; i++) {
            switch (acceleration) {
                case LINEAR:
                    pointX[i] = from.x + Math.round((float) (diffX * i) / (frameCount-1));
                    pointY[i] = from.y + Math.round((float) (diffY * i) / (frameCount-1));
                    break;
                case GRAVITY:
                    pointX[i] = from.x + Math.round((float) diffX * i * i / fcs);
                    pointY[i] = from.y + Math.round((float) diffY * i * i / fcs);
                    break;
                case ANTI_GRAVITY:
                    int k = frameCount - 1 - i;
                    pointX[i] = to.x - Math.round((float) diffX * k * k / fcs);
                    pointY[i] = to.y - Math.round((float) diffY * k * k / fcs);
                    break;
            }
        }
    }

    public void setTargetBounds(int x, int y, int w, int h) {
        super.setTargetBounds(x, y, w, h);
        resetClipBounds();
    }

    private void resetClipBounds() {
        Rectangle r1 = new Rectangle(targetBounds);
        Rectangle r2 = new Rectangle(targetBounds);
        r1.translate(from.x, from.y);
        r2.translate(to.x, to.y);
        setClipBounds(r1.union(r2));
    }

    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        int sx1 = 0;
        int sy1 = 0;
        int sx2 = targetBounds.width;
        int sy2 = targetBounds.height;
        int dx1 = pointX[frame] + targetBounds.x - relativeClipBounds.x;
        int dy1 = pointY[frame] + targetBounds.y - relativeClipBounds.y;
        int dx2 = dx1 + sx2;
        int dy2 = dy1 + sy2;

        if (dx1 < 0) {
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect - dx1 range over = " + dx1);
            }
            sx1 = sx1 - dx1;
            dx1 = 0;
        }
        if (dy1 < 0) {
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect - dy1 range over = " + dy1);
            }
            sy1 = sy1 - dy1;
            dy1 = 0;
        }
        if (dx2 > Constants.SCREEN_WIDTH) {
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect - dx2 range over = " + dx2);
            }
            sx2 = sx2 - dx2 + Constants.SCREEN_WIDTH;
            dx2 = Constants.SCREEN_WIDTH;
        }
        if (dy2 > Constants.SCREEN_HEIGHT) {
            if (Log.DEBUG_ON) {
                //Log.printDebug("Effect - dy2 range over = " + dy2);
            }
            sy2 = sy2 - dy2 + Constants.SCREEN_HEIGHT;
            dy2 = Constants.SCREEN_HEIGHT;
        }
        g.drawImage(image, dx1, dy1, dx2, dy2,
                           sx1, sy1, sx2, sy2, null);
    }

}


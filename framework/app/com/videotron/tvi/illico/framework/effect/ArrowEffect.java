package com.videotron.tvi.illico.framework.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;

public class ArrowEffect {
	public static final int UP = 0;
	public static final int DOWN = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 3;

	Image[] arrows;
    Component component;
    Component root;
    Point absoluteLocation = new Point(0, 0);

	public ArrowEffect(Component c, int frameCount) {
        this(c);
    }

	public ArrowEffect(Component c) {
        component = c;
        DataCenter dataCenter = DataCenter.getInstance();
		arrows = new Image[] {
			dataCenter.getImage("02_ars_t_m.png"),
			dataCenter.getImage("02_ars_b_m.png"),
			dataCenter.getImage("02_ars_l_m.png"),
			dataCenter.getImage("02_ars_r_m.png"),
			dataCenter.getImage("02_ars_t_l.png"),
			dataCenter.getImage("02_ars_b_l.png"),
			dataCenter.getImage("02_ars_l_l.png"),
			dataCenter.getImage("02_ars_r_l.png"),
		};
	}

    public void readyAbsoluteLocation() {
        root = Effect.getRoot(component);
        absoluteLocation = Effect.getAbsoluteLocation(component);
    }

	public void startFromRoot(Point point, int direction) {
        start(root.getGraphics(), point.x + absoluteLocation.x, point.y + absoluteLocation.y, direction);
    }

	public void start(Rectangle bounds, int direction) {
        start(component.getGraphics(), bounds.x, bounds.y, direction);
    }

	private void start(Graphics g, int x, int y, int direction) {
		if (direction == UP) {
			x -= 6;
			y -= 2;
		} else if (direction == DOWN) {
			x -= 6;
			y -= 1;
		} else {
			x -= 1;
			y -= 5;
		}

		Rectangle ori = g.getClipBounds();
		g.clipRect(x, y, 34, 34);
		g.drawImage(arrows[direction], x, y, component);
		Toolkit.getDefaultToolkit().sync();
		try {
			Thread.sleep(25);
		} catch (InterruptedException e) {
        }
		g.drawImage(arrows[direction+4], x, y, component);
		Toolkit.getDefaultToolkit().sync();
		g.setClip(ori);
		try {
			Thread.sleep(25);
		} catch (InterruptedException e) {
        }
	}

}

package com.videotron.tvi.illico.framework.effect;

public class ScaleFactor {
    public double startScale;
    public double maxScale;
    public double endScale;

    public double scaleUpFrom;
    public double scaleUpUntil;
    public double scaleDownFrom;
    public double scaleDownUntil;

    public double keepAlphaFrom;
    public double keepAlphaUntil;

    public ScaleFactor() {
    }

    public ScaleFactor(double ss, double ms, double es,
                       double suf, double suu, double sdf, double sdu,
                       double kaf, double kau) {
        startScale = ss;
        maxScale = ms;
        endScale = es;
        scaleUpFrom = suf;
        scaleUpUntil = suu;
        scaleDownFrom = sdf;
        scaleDownUntil = sdu;
        keepAlphaFrom = kaf;
        keepAlphaUntil = kau;
    }

    public static final ScaleFactor SIMPLE_SCALE_UP = new ScaleFactor(
        0d, 2d, 2d,  0d, 1d, 1d, 1d,  0d, 1d);

    public static final ScaleFactor ICON_SHOWING = new ScaleFactor(
        0.2d, 1.8d, 1d,  0d, 0.6d, 0.75d, 0.95d,  0.4d, 1d);

    public static final ScaleFactor ICON_HIDING = new ScaleFactor(
        1d, 1.8d, 0.2d,  0.2d, 0.75d, 0.8d, 1d,  0d, 0.7d);


    /** x = [0 to 1], return scale. */
    public double getScale(double x) {
        if (x < scaleUpFrom) {
            return startScale;
        }
        if (x < scaleUpUntil) {
            return startScale + (maxScale - startScale) * (x - scaleUpFrom) / (scaleUpUntil - scaleUpFrom);
        }
        if (x < scaleDownFrom) {
            return maxScale;
        }
        if (x < scaleDownUntil) {
            return maxScale - (maxScale - endScale) * (x - scaleDownFrom) / (scaleDownUntil - scaleDownFrom);
        }
        return endScale;
//        return (Math.cos( Math.PI * (x * X_PROJECTION_FACTOR - 1)) + 1) * (maxScale - 1) / 2.0d + 1;
    }

    /** x = [0 to 1], return alpha factor. */
    public double getAlpha(double x) {
        if (x < keepAlphaFrom) {
            return x / keepAlphaFrom;
        }
        if (x >= keepAlphaUntil) {
            return 1 - (x - keepAlphaUntil) / (1 - keepAlphaUntil);
        }
        return 1;
    }

}


package com.videotron.tvi.illico.framework.effect;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;

/**
 * ClickingEffect
 *
 * @version $Revision: 1.13 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */

public class ClickingEffect extends Effect {

    Rectangle effectArea;
	int[] leftPoints;
	int[] rightPoints;

	int margin;

	public ClickingEffect(Component c) {
        this(c, 5);
    }

	public ClickingEffect(Component c, int frameCount) {
		super(c, frameCount);
        updateBackground = false;

		this.margin = (frameCount - 1) / 2;

		leftPoints = new int[frameCount];
		rightPoints = new int[frameCount];

		int i = 0;
		int j = frameCount - 1;
		while (i <= j) {
			leftPoints[i] = margin - i;
			rightPoints[i] = margin + i;

			if (i == j) {
				break;
			}
			leftPoints[j] = margin - i;
			rightPoints[j] = margin + i;

			i++;
			j--;
			if (j < i) {
				return;
			}
		}
	}

	public void start(int x, int y, int width, int height, int inset) {
		start(x + inset, y + inset, width - inset * 2, height - inset * 2);
	}

	public void start(int x, int y, int width, int height) {
        start(new Rectangle(x, y, width, height));
	}

	public void start(Rectangle r) {
        if (r != null) {
            effectArea = r;
            includeTargetToBackground = true;
            updateBackground = false;
            setTargetBounds(r);
            setClipBounds(r.x - margin, r.y - margin, r.width + margin * 2, r.height + margin * 2);
            this.start();
        }
    }

    protected void animate(Graphics g, DVBBufferedImage image, int frame) {
        g.drawImage(image, leftPoints[frame], leftPoints[frame],
                           effectArea.width + rightPoints[frame],
                           effectArea.height + rightPoints[frame],
                           0, 0, effectArea.width, effectArea.height, null);
    }


}

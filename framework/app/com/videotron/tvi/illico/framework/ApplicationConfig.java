/*
 * $RCSfile: ApplicationConfig.java,v $ $Revision: 1.6 $ $Date: 2017/01/12 19:20:15 $
 *
 * Copyright 1999-2002 by Alticast Corp. All rights reserved.
 */

package com.videotron.tvi.illico.framework;

import javax.tv.xlet.XletContext;

/**
 * This interface used for all applications that uses this framework.
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public interface ApplicationConfig {

    /**
     * Returns name of application. The name can be used as a unique key to find
     * <code>AppProxy</code> from <code>AppsDatabase</code>.
     *
     * @return name of application
     */
    String getApplicationName();

    /**
     * Returns version of application.
     *
     * @return version of application
     */
    String getVersion();

    /**
     * Returns the XletContext of application.
     *
     * @return XletContext
     */
    XletContext getXletContext();


    /**
     * Initialize application.
     */
    void init();

}

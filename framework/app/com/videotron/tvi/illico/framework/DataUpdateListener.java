package com.videotron.tvi.illico.framework;

/**
 * Listener interface to receive notification when the data has been added, removed or
 * updated.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:20:15 $
 * @author  June Park
 */
public interface DataUpdateListener {

    /**
     * Called when data has been removed.
     *
     * @param key key of data.
     */
    void dataRemoved(String key);

    /**
     * Called when data has been updated.
     *
     * @param key key of data.
     * @param old old value of data.
     * @param value new value of data.
     */
    void dataUpdated(String key, Object old, Object value);
}

package com.videotron.tvi.illico.framework.io;

import com.videotron.tvi.illico.log.Log;
import java.io.BufferedReader;
import java.io.UnsupportedEncodingException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.FileNotFoundException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.Vector;
import java.util.zip.*;

/**
 * This class is the utility class that reads the zip file and returns the data of entries.
 *
 * @version $Revision: 1.7 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class ZipFileReader {

	/** The buffer size. */
	private static final int BUFFER_SIZE = 1024;

    /** Constructor. */
    protected ZipFileReader() {
    }

    /**
     * Returns the contents of zip file.
     *
     * @param path path of source file.
     * @return hashtable that contains entry name as a key and byte array as a value.
     */
    public static Hashtable read(String path) {
        return read(new File(path));
    }

    /**
     * Returns the contents of zip file.
     *
     * @param data bytes data of zip file.
     * @return hashtable that contains entry name as a key and byte array as a value.
     */
    public static Hashtable read(byte[] data) {
        return read(new ByteArrayInputStream(data));
    }

    /**
     * Returns the contents of zip file.
     *
     * @param file source file.
     * @return hashtable that contains entry name as a key and byte array as a value.
     */
    public static Hashtable read(File file) {
        try {
            return read(new FileInputStream(file));
        } catch (FileNotFoundException ex) {
            Log.print(ex);
            return null;
        }

    }

    /**
     * Returns the contents of zip file.
     *
     * @param is InputStream of zip file.
     * @return hashtable that contains entry name as a key and byte array as a value.
     */
    private static Hashtable read(InputStream is) {
		int cnt = 0;
		byte[] buffer = new byte[BUFFER_SIZE];

		ZipInputStream zis = new ZipInputStream(is);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

		ZipEntry entry = null;
        Hashtable table = new Hashtable();
		try {
			while ((entry = zis.getNextEntry()) != null) {
				try {
					while ((cnt = zis.read(buffer)) != -1) {
						baos.write(buffer, 0, cnt);
					}
				} catch (Exception e) {
        			Log.print(e);
				}
                if (Log.DEBUG_ON) {
                    Log.printDebug("ZipData : " + entry.getName() + ", bytes=" + baos.size());
                }
                table.put(entry.getName(), baos.toByteArray());
                baos.reset();
			}
		} catch (Exception e) {
			Log.print(e);
		} finally {
			try {
				zis.close();
			} catch (Exception e) {
				Log.print(e);
			}
			try {
				is.close();
			} catch (Exception e) {
				Log.print(e);
			}
		}
        baos = null;
        return table;
    }

}


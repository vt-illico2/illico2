package com.videotron.tvi.illico.framework.io;

import com.videotron.tvi.illico.log.Log;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * This class is the utility class that reads the property file and returns a
 * Properties instance.
 *
 * @version $Revision: 1.7 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class PropertiesReader {

    /** Constructor. */
    protected PropertiesReader() {
    }

    /**
     * Returns the contents of text file.
     *
     * @param path path of source file.
     * @return an array of each line, or null of file not found.
     */
    public static Properties read(String path) {
        return read(new File(path));
    }

    /**
     * Returns the contents of text file.
     *
     * @param file source file
     * @return an array of each line, or null of file not found.
     */
    public static Properties read(File file) {
        Properties p = new Properties();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            p.load(fis);
        } catch (IOException e) {
            Log.print(e);
            return null;
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("read " + p.size() + " properties.");
        }
        return p;
    }

}


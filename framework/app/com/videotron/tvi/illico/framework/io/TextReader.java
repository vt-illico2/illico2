package com.videotron.tvi.illico.framework.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;

/**
 * This class is the utility class that reads the text file and returns the
 * array of string.
 *
 * @version $Revision: 1.7 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class TextReader {

	/** Default encoding of text file. */
	private static final String	DEFAULT_ENCODING	= "UTF-8";

	/** Constructor. */
	protected TextReader() {
	}

	/**
	 * Returns the contents of text file.
	 *
	 * @param path path of source file.
	 * @return an array of each line, or null of file not found.
	 */
	public static String[] read(String path) {
		return read(new File(path));
	}

	/**
	 * Returns the contents of text file.
	 *
	 * @param file source file
	 * @return an array of each line, or null of file not found.
	 */
	public static String[] read(File file) {
		try {
			return read(new InputStreamReader(new FileInputStream(file), DEFAULT_ENCODING));
		}
		catch (IOException e) {
			if (Log.WARNING_ON) {
				Log.print(e);
			}
			return null;
		}
	}

	/**
	 * Returns the contents of text file. <strong>This method is not part of the Alticast framework. It was added as a convenience method by Videotron.</strong>>
	 *
	 * @param file source file
	 * @return an array of each line, or null of file not found.
	 */
	public static String[] read(File file, String encoding) {
		try {
			return read(new InputStreamReader(new FileInputStream(file), encoding));
		}
		catch (IOException e) {
			if (Log.WARNING_ON) {
				Log.print(e);
			}
			return null;
		}
	}

	/**
	 * Returns the contents of text file.
	 *
	 * @param reader Reader of source file
	 * @return an array of each line, or null of file not found.
	 */
	public static String[] read(Reader reader) {
		try {
			Vector vector = getLines(reader);
			String[] ret = new String[vector.size()];
			vector.copyInto(ret);
			if (Log.DEBUG_ON) {
				Log.printDebug("TextReader: read " + ret.length + " lines.");
			}
			return ret;
		}
		catch (IOException e) {
			if (Log.WARNING_ON) {
				Log.print(e);
			}
		}
		return null;
	}

	/**
	 * Gets the contents from the reader.
	 *
	 * @param reader Reader of source file
	 * @return Vector that contains lines.
	 * @throws IOException If an I/O error occurs.
	 */
	public static Vector getLines(Reader reader) throws IOException {
		BufferedReader r = new BufferedReader(reader);
		Vector vector = new Vector();
		String str;
		while ((str = r.readLine()) != null) {
			vector.addElement(str);
		}
		r.close();
		return vector;
	}

}

package com.videotron.tvi.illico.framework.io;

import com.videotron.tvi.illico.log.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This class is the utility class that reads the file and returns the contents
 * as a byte array.
 *
 * @version $Revision: 1.5 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class BinaryReader {

    private static final int MIN_ARRAY_SIZE = 1024;

    /** The buffer size. */
	private static final int BUFFER_SIZE = 1024;


    /** Constructor. */
    protected BinaryReader() {
    }

    /**
     * Returns the contents of binary file.
     *
     * @param path path of source file.
     * @return bytes, or null of file not found.
     */
    public static byte[] read(String path) {
        return read(new File(path));
    }

    /**
     * Returns the contents of binary file.
     *
     * @param file source file
     * @return bytes, or null of file not found.
     */
    public static byte[] read(File file) {
        try {
            return read(new FileInputStream(file), (int) file.length());
        } catch (IOException e) {
            if (Log.WARNING_ON) {
                Log.print(e);
            }
            return null;
        }
    }

    public static byte[] read(InputStream is) {
        return read(is, 0);
    }

    public static byte[] read(InputStream is, int fileLength) {
        int arraySize = Math.max(fileLength, MIN_ARRAY_SIZE);
        ByteArrayOutputStream baos ;
        if (fileLength < MIN_ARRAY_SIZE) {
            baos = new ByteArrayOutputStream(MIN_ARRAY_SIZE);
        } else {
            baos = new DirectUseByteArrayOutputStream(fileLength);
        }
        BufferedInputStream bis = null;
        byte[] ret = null;
        byte[] buf = new byte[1024];
        try {
            bis = new BufferedInputStream(is);
            int c;
            while ((c = bis.read(buf)) != -1) {
                baos.write(buf, 0, c);
            }
            ret = baos.toByteArray();
            if (Log.DEBUG_ON) {
                Log.printDebug("BinaryReader : read " + ret.length + " bytes, fileLength=" + fileLength);
            }
        } catch (IOException e) {
            if (Log.WARNING_ON) {
                Log.print(e);
            }
        } finally {
            try {
                bis.close();
            } catch (Exception ex) {
            }
        }
        return ret;
    }

    static class DirectUseByteArrayOutputStream extends ByteArrayOutputStream {
        public DirectUseByteArrayOutputStream(int size) {
            super(size);
        }

        public byte[] toByteArray() {
            if (count == buf.length) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DirectUseByteArrayOutputStream : return current buffer.");
                }
                return buf;
            } else {
                if (Log.DEBUG_ON) {
                    Log.printDebug("DirectUseByteArrayOutputStream : count = " + count + ", len = " + buf.length);
                }
                return super.toByteArray();
            }
        }
    }

}


package com.videotron.tvi.illico.framework.io;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;

/**
 * This class is the utility class to get data from back-end server.
 *
 * @version $Revision: 1.15.6.1 $ $Date: 2019/08/12 15:47:07 $
 * @author June Park
 */
public class URLRequestor {

    private static final int READ_BUFFER_SIZE = 2048;

    private static Hashtable alive = new Hashtable();

    /** Constructor. */
    private URLRequestor() {
    }

    /**
     * Gets bytes from URL.
     *
     * @param proxyHost IP or DNS of proxy server, if null don't use proxy
     * @param proxyPort port of proxy server
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public static byte[] getBytes(String proxyHost, int proxyPort, String locator, Hashtable param) throws IOException {
        String path = getFullPath(locator, param);
        if (Log.INFO_ON) {
            Log.printInfo("URLRequestor.getBytes path = " + path);
        }
        URL url = null;
        if (proxyHost != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("URLRequestor.getBytes proxy:port = " + proxyHost + ":" + proxyPort);
            }
        	url = new URL("HTTP", proxyHost, proxyPort, path);
        } else {
        	url = new URL(path);
        }
        URLConnection con = url.openConnection();
        if (Log.INFO_ON) {
            if (con instanceof HttpURLConnection) {
                HttpURLConnection hc = (HttpURLConnection) con;
                Log.printInfo("URLRequestor.getBytes response = " + hc.getResponseCode());
                Log.printInfo("URLRequestor.getBytes : " + hc.getResponseMessage());
            }
        }

        BufferedInputStream bis = new BufferedInputStream(con.getInputStream());
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buf = new byte[READ_BUFFER_SIZE];
        int c;
        while ((c = bis.read(buf)) != -1) {
            baos.write(buf, 0, c);
        }
        if (Log.DEBUG_ON) {
            Log.printDebug("URLRequestor.getBytes read bytes = " + baos.size());
        }
        if (con instanceof HttpURLConnection) {
            ((HttpURLConnection) con).disconnect();
        }
        bis.close();
        return baos.toByteArray();
    }

    /**
     * Gets bytes from URL.
     *
     * @param proxyHosts IP or DNS of proxy server, if null don't use proxy
     * @param proxyPorts port of proxy server
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public static byte[] getBytes(String[] proxyHosts, int[] proxyPorts, String locator, Hashtable param) throws IOException {
        if (proxyHosts == null || proxyHosts.length == 0) {
            return getBytes(null, 0, locator, param);
        }
        for (int i = 0; i < proxyHosts.length; i++) {
            try {
                return getBytes(proxyHosts[i], proxyPorts[i], locator, param);
            } catch (IOException ex) {
                Log.printWarning(ex.getClass() + " / " + proxyHosts[i] + ":" + proxyPorts[i]);
            }
        }
        throw new IOException("no avaliable proxy");
    }

    /**
     * Gets bytes from URL.
     *
     * @param locator path of source file
     * @param param parameter to transfer collection
     * @return an array of byte[] of return, or null of file not found.
     */
    public static byte[] getBytes(String locator, Hashtable param) throws IOException {
    	return getBytes(null, 0, locator, param);
    }

    /**
     * Gets String array from URL.
     *
     * @param proxyHost IP or DNS of proxy server, if null don't use proxy
     * @param proxyPort port of proxy server
     * @param locator path of source file.
     * @return an array of each line, or null of file not found.
     */
    public static Vector getLines(String proxyHost, int proxyPort, String locator, Hashtable param) throws IOException {
    	return getLines(proxyHost, proxyPort, locator, param, "UTF-8");
    }

    //->Kenneth[2016.7.19] R7.2 On-Screen debug 
    public static String lastURLStr = null;
    //<-
    
    //VDTRMASTER-6231, R7.4 freelife - Thread가 자기자신에 의해 interrupt 되는 경우 무조건 error(TIMEOUT) 처리되는 오류를 수정함 
    public static Vector getLines(String proxyHost, int proxyPort, String locator, Hashtable param, final String charset) throws IOException {
        final String path = getFullPath(locator, param);
        if (Log.INFO_ON) {
            Log.printInfo("URLRequestor.getLines path = " + path);
        }
        URL url = null;
        if (proxyHost != null) {
            if (Log.DEBUG_ON) {
                Log.printDebug("URLRequestor.getLines proxy:port = " + proxyHost + ":" + proxyPort);
            }
        	url = new URL("HTTP", proxyHost, proxyPort, path);
        } else {
        	url = new URL(path);
        }
        //->Kenneth[2016.7.19] R7.2 On-Screen debug 
        lastURLStr = url.toString();
        //<-
        final URLConnection con = url.openConnection();
        final Object lock = new Object();
        new Thread("URLRequestor, url="+url) {
        	public void run() {
        		Log.printInfo("URLRequestor.run() started Thread" + System.currentTimeMillis());
        		try {
					Vector ret = TextReader.getLines(charset == null
							? new InputStreamReader(con.getInputStream())
							: new InputStreamReader(con.getInputStream(), "UTF-8"));
					alive.put(con, ret);
				} catch (UnsupportedEncodingException e) {
					Log.printInfo("URLRequestor.getLines UnsupportedEncodingException "+e.getMessage());
					e.printStackTrace();
					
				} catch (IOException e) {
					Log.printInfo("URLRequestor.getLines IOException "+e.getMessage());
					e.printStackTrace();
					
					alive.put(con, e);
				} catch (Exception e) {
					Log.printInfo("URLRequestor.getLines Exception "+e.getMessage());
					e.printStackTrace();
				}
				synchronized (lock) {
					Log.printInfo("URLRequestor.getLines notify lock thread " + System.currentTimeMillis());
        			lock.notifyAll();
        		}
        		if (con instanceof HttpURLConnection) {
                    ((HttpURLConnection) con).disconnect();
                }

        		Log.printInfo("URLRequestor.getLines disconnected " + System.currentTimeMillis());
        		// discard result and link if "Timeout"
        		try {
					Thread.sleep(500);
				} catch (InterruptedException e) {}
				Object obj = alive.remove(con); // obj will be null in normal case
				if (obj != null && obj instanceof Vector) {
					((Vector) obj).clear();
				}
				
				Log.printInfo("URLRequestor.getLines ended Thread " + System.currentTimeMillis());
        	}
        }.start();
        synchronized (lock) {
        	boolean interrupted = false;
        	try {        		
        		Log.printInfo("URLRequestor.getLines waiting lock thread " + System.currentTimeMillis());
				lock.wait(10000); // wait 10 secs for timeout						
				Log.printInfo("URLRequestor.getLines notified waiting thread " + System.currentTimeMillis());
				
			} catch (InterruptedException e) {
				interrupted = true;
				Log.printInfo("URLRequestor.getLines lock thread Interrupted !! " + System.currentTimeMillis());							
			}
        	if (interrupted) {
	        	try {        		
	        		Log.printInfo("URLRequestor.getLines waiting lock thread : in Interrupted" + System.currentTimeMillis());
					lock.wait(10000); // wait 10 secs for timeout						
					Log.printInfo("URLRequestor.getLines notified waiting thread : in Interrupted" + System.currentTimeMillis());
					
				} catch (InterruptedException ie) {
					Log.printInfo("URLRequestor.getLines lock thread Interrupted !! : in Interrupted" + System.currentTimeMillis());
				}
        	}
        }
        if (alive.containsKey(con)) {        	
        	Object obj = alive.remove(con);
        	if (obj instanceof IOException) {
        		throw (IOException) obj;
        	}
        	Log.printInfo("URLRequestor.return " + System.currentTimeMillis());
        	return (Vector) obj;
        }
        Log.printInfo("URLRequestor.getLines throw IOException(Timeout) " + System.currentTimeMillis());
        
        throw new IOException("Timeout");
    }

    /**
     * Gets String array from URL.
     *
     * @param proxyHosts IP or DNS of proxy server, if null don't use proxy
     * @param proxyPorts port of proxy server
     * @param locator path of source file.
     * @return an array of each line, or null of file not found.
     */
    public static Vector getLines(String[] proxyHosts, int[] proxyPorts, String locator, Hashtable param) throws IOException {
        if (proxyHosts == null || proxyHosts.length == 0) {
            return getLines(null, 0, locator, param);
        }
        for (int i = 0; i < proxyHosts.length; i++) {
            try {
                return getLines(proxyHosts[i], proxyPorts[i], locator, param);
            } catch (IOException ex) {
                Log.printWarning(ex.getClass() + " / " + proxyHosts[i] + ":" + proxyPorts[i]);
            }
        }
        throw new IOException("no avaliable proxy");
    }


    /**
     * Gets String array from URL.
     *
     * @param path path of source file.
     * @return an array of each line, or null of file not found.
     */
    public static Vector getLines(String locator, Hashtable param) throws IOException {
    	return getLines(null, 0, locator, param);
    }

    public static Vector getLines(String locator, Hashtable param, String charset) throws IOException {
    	return getLines(null, 0, locator, param, charset);
    }

    /**
     */
    public static String[] getLinesWithPost(String locator, String postValue) throws IOException {
        String path = getFullPath(locator, null);
        if (Log.INFO_ON) {
            Log.printInfo("URLRequestor.getLinesWithPost path = " + path);
        }
        URL url = new URL(path);
        URLConnection con = url.openConnection();
        if (!(con instanceof HttpURLConnection)) {
            return null;
        }
        HttpURLConnection hc = (HttpURLConnection) con;

        hc.setDoOutput(true);
        hc.setRequestMethod("POST");

        OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
        wr.write(postValue);
        wr.flush();

        int rc = hc.getResponseCode();
        String rm = hc.getResponseMessage();

        if (Log.DEBUG_ON) {
            Log.printDebug("URLRequestor.getLinesWithPost response = " + rc);
            Log.printDebug("URLRequestor.getLinesWithPost : " + rm);
        }

        InputStreamReader isr = new InputStreamReader(con.getInputStream(), "UTF-8");
        String[] ret = TextReader.read(isr);
        hc.disconnect();
        return ret;
    }

    private static String getFullPath(String locator, Hashtable param) {
        if (param == null || param.size() <= 0) {
            return locator;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(locator);
        sb.append('?');

        Enumeration en = param.keys();
        Object key = en.nextElement();
        sb.append(key);
        sb.append('=');
        sb.append(param.get(key));
        while (en.hasMoreElements()) {
            sb.append('&');
            key = en.nextElement();
            sb.append(key);
            sb.append('=');
            sb.append(param.get(key));
        }
        return sb.toString();
    }
}


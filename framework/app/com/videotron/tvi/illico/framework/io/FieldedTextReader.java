package com.videotron.tvi.illico.framework.io;

import java.io.CharConversionException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.TextUtil;

/**
 * This class is the utility class that reads the text file and returns the
 * array of string.
 *
 * @version $Revision: 1.11 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class FieldedTextReader {

	/** Default encoding of text file. */
	private static final String	DEFAULT_ENCODING	= "UTF-16";
	/** A tab delimiter. */
	public static final char	TAB_DELIMITER		= '\t';
	/** A comma delimiter. */
	public static final char	COMMA_DELIMITER		= ',';
	/** A space delimiter. */
	public static final char	SPACE_DELIMITER		= ' ';

	/** A delimiter. */
	private char				delim;

	private String				encoding;

	/**
	 * Constructor.
	 *
	 * @param delimiter a delimiter to use
	 */
	public FieldedTextReader(char delimiter) {
		delim = delimiter;
		encoding = DEFAULT_ENCODING;
	}

	/**
	 * Construct a FieldedTextReader which uses a (possibly) different encoding format than the default (which is UTF-16).
	 * @param delimiter
	 * @param anEncoding
	 */
	public FieldedTextReader(char delimiter, String anEncoding) {
		delim = delimiter;
		encoding = anEncoding;
	}

	/**
	 * Returns the contents of text file.
	 *
	 * @param path path of source file.
	 * @return an array of each line, or null of file not found.
	 */
	public String[][] read(String path) {
		return read(new File(path));
	}

	/**
	 * Returns the contents of text file.
	 *
	 * @param file source file
	 * @return an array of each line, or null of file not found.
	 */
	public String[][] read(File file) {
		if (Log.DEBUG_ON) {
			Log.printDebug("FieldedTextReader.read : " + file);
		}
		Vector vector = null;
		try {
			vector = TextReader.getLines(new InputStreamReader(new FileInputStream(file), encoding));
		}
		catch (UnsupportedEncodingException ex) {
			// try with TextReader's default encoding (UTF-8)
		}
		catch (CharConversionException ex) {
			// try with TextReader's default encoding (UTF-8)
		}
		catch (Exception ex) {
			Log.print(ex);
			return null;
		}
		String[] lines;
		if (vector == null) {
			if (Log.WARNING_ON) {
				Log.printWarning("FieldedTextReader.read : retry with another encoding = " + file);
			}
			lines = TextReader.read(file);
		}
		else {
			int size = vector.size();
			if (size <= 0) {
				return null;
			}
			lines = new String[size];
			vector.copyInto(lines);
		}
		if (lines == null) {
			return null;
		}
		String[][] ret = new String[lines.length][];
		for (int i = 0; i < lines.length; i++) {
			String[] tokens = TextUtil.tokenize(lines[i], delim);
			for (int j = 0; j < tokens.length; j++) {
				tokens[j] = removeQuotes(tokens[j]);
			}
			ret[i] = tokens;
		}
		return ret;
	}

	private String removeQuotes(String s) {
		if (s != null) {
			int len = s.length();
			if (len > 2 && s.charAt(0) == '\"' && s.charAt(len - 1) == '\"') {
				String ret = TextUtil.replace(s.substring(1, len - 1), "\"\"", "\"");
				return TextUtil.replace(ret, "\\n", "\n"); // if it has '\','n', replace to '\n' for multi-line string
			}
		}
		return s;
	}

}

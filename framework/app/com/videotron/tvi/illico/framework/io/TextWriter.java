package com.videotron.tvi.illico.framework.io;

import com.videotron.tvi.illico.log.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.IOException;
import java.util.Vector;

/**
 * This class is the utility class that writes contents to  the text file.
 *
 * @version $Revision: 1.6 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class TextWriter {

    /** Default encoding of text file. */
    private static final String DEFAULT_ENCODING = "UTF-8";

    /** Constructor. */
    protected TextWriter() {
    }

    /**
     * Writes the contents to the text file.
     *
     * @param contents contents.
     * @param path path of the target file.
     */
    public static boolean write(Object[] contents, String path) {
        return write(contents, new File(path));
    }

    /**
     * Writes the contents to the text file.
     *
     * @param contents contents.
     * @param file target file.
     */
    public static boolean write(Object[] contents, File file) {
        try {
            return write(contents, new OutputStreamWriter(new FileOutputStream(file),
                                                      DEFAULT_ENCODING));
        } catch (IOException e) {
            if (Log.WARNING_ON) {
                Log.print(e);
            }
            return false;
        }
    }

    /**
     * Writes the contents to the text file.
     *
     * @param contents contents.
     * @param writer writer of the target file.
     */
    public static boolean write(Object[] contents, Writer writer) {
        try {
            BufferedWriter w = new BufferedWriter(writer);
            int count = 0;
            for (int i = 0; i < contents.length; i++) {
                Log.printDebug("TextWriter: contents[" + i + "] = " + contents[i]);
                if (contents[i] != null) {
                    w.write(contents[i].toString());
                    w.newLine();
                    count++;
                }
            }
            w.flush();
            w.close();
            if (Log.DEBUG_ON) {
                Log.printDebug("TextWriter: write " + count + " lines.");
            }
            return true;
        } catch (IOException e) {
            if (Log.WARNING_ON) {
                Log.print(e);
            }
        }
        return false;
    }

}


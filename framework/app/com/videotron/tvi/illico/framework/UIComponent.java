package com.videotron.tvi.illico.framework;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Image;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is a extended class of java.awt.Container that display UI.
 * Location and Size are specified by application. Drawing is handled by Renderer class.
 *
 * @version $Revision: 1.20 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public abstract class UIComponent extends Container implements AnimationRequestor {

    /** current state of UI. */
    protected int state;

    /** current focus position of UI. */
    protected int focus;

    /** GUI Renderer of this UI. */
    protected Renderer renderer;

    public void prepare() {
        if (renderer != null) {
            renderer.prepare(this);
        }
    }

    public void start() {
        prepare();
    }

    public void stop() {
    }

    /**
     * Sets the renderer of UI.
     */
    public void setRenderer(Renderer r) {
        this.renderer = r;
        if (r != null) {
            Rectangle rect = r.getPreferredBounds(this);
            if (rect != null && !rect.isEmpty()) {
                setBounds(rect);
            }
        }
    }

    /**
     * Returns the state of UI.
     */
    public int getState() {
        return state;
    }

    /**
     * Sets the state of UI.
     */
    public void setState(int newState) {
        this.state = newState;
    }

    /**
     * Returns the focus position of UI.
     */
    public int getFocus() {
        return focus;
    }

    /**
     * Sets the focus of UI.
     */
    public void setFocus(int newFocus) {
        this.focus = newFocus;
    }

    public final void paint(Graphics g) {
        try {
            if (renderer != null) {
                renderer.paint(g, this);
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        super.paint(g);
    }

    public boolean handleKey(int code) {
        return false;
    }

    public boolean skipAnimation(Effect effect) {
        return false;
    }

    public void animationStarted(Effect effect) {
    }

    public void animationEnded(Effect effect) {
    }

    /** Overriden.
    public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
        if (Log.DEBUG_ON) {
            Log.printDebug("imageUpdate = " + img + ", " + infoflags);
        }
        return super.imageUpdate(img, infoflags, x, y, width, height);
    }
     */

}

package com.videotron.tvi.illico.framework;

import java.awt.Graphics;
import java.awt.Rectangle;

/**
 * A class that handles paint of UIComponent.
 *
 * @version $Revision: 1.8 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 * @see UIComponent
 */
public abstract class Renderer {

    /**
     * Returns the preferred bounds of this renderer. If preferred bounds is
     * non-null, the UIComponent's bounds will be set by this method.
     *
     * @param c UIComponent to
     * @return The Preferred Bounds for the this Renderer.
     * @see UIComponent#setRenderer
     */
    public abstract Rectangle getPreferredBounds(UIComponent c);

    /**
     * This will be called before the related UIComponent is shown.
     *
     * @param c UIComponent to be painted by this renderer.
     * @see UIComponent#prepare
     */
    public abstract void prepare(UIComponent c);

    /**
     * Paints the UIComponent.
     *
     * @param g The graphics context to use for painting.
     * @param c UIComponent to be painted by this renderer.
     */
    protected abstract void paint(Graphics g, UIComponent c);

}

/*
 * $RCSfile: FrameworkMain.java,v $ $Revision: 1.48 $ $Date: 2017/01/12 19:20:15 $
 *
 * Copyright 1999-2002 by Alticast Corp. All rights reserved.
 */
package com.videotron.tvi.illico.framework;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Formatter;

import java.awt.Component;
import java.awt.Container;

import org.havi.ui.HScene;
import org.havi.ui.HSceneFactory;
import org.havi.ui.HSceneTemplate;
import org.havi.ui.HScreenDimension;
import org.havi.ui.HScreenPoint;

import javax.tv.xlet.XletContext;
import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextFactory;
import javax.tv.service.selection.ServiceContextListener;
import javax.tv.service.selection.ServiceContentHandler;
import javax.tv.service.selection.InsufficientResourcesException;

import org.ocap.service.AbstractService;

/**
 * This is the main class of framework. Application must call
 * {@link init(ApplicationConfig)} in the initXlet().
 *
 * @version $Revision: 1.48 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class FrameworkMain implements DataUpdateListener {

    /** version of framework. */
    //public static final String VERSION = "2014.10.08.Q4";
    //->Kenneth[2015.1.30] 4K
    public static final String VERSION = "2015.1.30.4K"; // add Environment.SUPPORT_UHD

    /** singleton instance. */
    private static FrameworkMain instance = new FrameworkMain();
    /** application's configuration. */
    private ApplicationConfig config;

    /** HScene for the UI application. */
    private HScene scene;
    /** ImagePool instance. */
    private ImagePool imagePool;
    /** Root container of this application. */
    private Container rootContainer;

    /** Current language. */
    private String currentLanguage = Constants.LANGUAGE_FRENCH;

    /**
     * Returns the singleton instance of FramworkMain.
     *
     * @return FrameworkMain instance.
     */
    public static FrameworkMain getInstance() {
        return instance;
    }

    /** Constructor. */
    private FrameworkMain() {
        DataCenter.getInstance().addDataUpdateListener("language", this);
        setCurrentLanguage(Constants.LANGUAGE_FRENCH);
        DataCenter.getInstance().addDataUpdateListener("SUPPORT_UHD", this);
    }

    /**
     * Initializes the Framework. Application must call this method.
     *
     * @param appConfig application configuration.
     */
    public void init(ApplicationConfig appConfig) {
        this.config = appConfig;
        Log.setApplication(config);
        if (Log.INFO_ON) {
            Log.printInfo("FrameworkMain.init : " + VERSION);
        }
        imagePool = new ImagePool();
        imagePool.setBaseDirectory(DataAdapterManager.IMAGE_RESOURCE_PATH);
        DataAdapterManager.getInstance().init();
        appConfig.init();
        DataAdapterManager.getInstance().start();
    }

    /**
     * Application must call this in when startXlet() called.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo("FrameworkMain.start : " + VERSION);
        }
    }

    /**
     * Application must call this in when pauseXlet() called.
     */
    public void pause() {
        if (Log.INFO_ON) {
            Log.printInfo("FrameworkMain.pause : " + VERSION);
        }
        DataCenter.getInstance().flush(DataCenter.WEAK_REFERENCE);
    }

    /**
     * Application must call this in when destroyXlet() called.
     */
    public void destroy() {
        if (Log.INFO_ON) {
            Log.printInfo("FrameworkMain.destroy : " + VERSION);
        }
        if (scene != null) {
            scene.removeAll();
            scene.dispose();
        }
        DataCenter.getInstance().dispose();
    }

    /**
     * Gets the application's name from <code>ApplicationConfig</code>.
     *
     * @return the application's name
     */
    public String getApplicationName() {
        return config.getApplicationName();
    }

    /**
     * Gets the application's version from <code>ApplicationConfig</code>.
     *
     * @return the application's version
     */
    public String getApplicationVersion() {
        return config.getVersion();
    }

    /**
     * Gets the XletContext from <code>ApplicationConfig</code>.
     *
     * @return the application's version
     */
    public XletContext getXletContext() {
        return config.getXletContext();
    }

    /**
     * Returns the ImagePool instance.
     *
     * @return ImagePool instance.
     */
    public ImagePool getImagePool() {
        return imagePool;
    }

    /**
     * Returns the HScene of this application.
     *
     * @return HScene
     */
    public HScene getHScene() {
        if (scene == null) {
            scene = createScene();
        }
        return scene;
    }

    /**
     * Creates a new HScene.
     *
     * @return new Hscene
     */
    private HScene createScene() {
        HSceneTemplate req = new HSceneTemplate();
        req.setPreference(
                HSceneTemplate.SCENE_SCREEN_LOCATION,
                new HScreenPoint(0.0f, 0.0f),
                HSceneTemplate.PREFERRED
        );
        req.setPreference(
                HSceneTemplate.SCENE_SCREEN_DIMENSION,
                new HScreenDimension(1.0f, 1.0f),
                HSceneTemplate.PREFERRED
        );
        HScene newScene = HSceneFactory.getInstance().getBestScene(req);
        return newScene;
    }

    /**
     * Addes a component to the top of HScene.
     *
     * @param c a component to be added.
     */
    public void addComponent(Component c) {
        addComponent(c, 0);
    }

    /**
     * Addes the component to the specified position of HScene.
     *
     * @param c the component to be added.
     * @param pos the position at which to insert the component, or -1 to insert the
     *            component at the end.
     */
    public void addComponent(Component c, int pos) {
        HScene hScene = getHScene();
        c.setVisible(true);
        hScene.add(c, pos);
        hScene.setVisible(true);
    }

    /**
     * Removes the added component.
     *
     * @param c the component to be removed.
     */
    public void removeComponent(Component c) {
        HScene hScene = getHScene();
        hScene.remove(c);
        c.setVisible(false);
        hScene.repaint();
        if (hScene.getComponentCount() <= 0) {
            hScene.setVisible(false);
        }
    }

    /**
     * Prints a simple log for this application. This log will store into
     * SharedMemory and can be displayed in Diagnostics screen.
     *
     * @param str log.
     */
    public void printSimpleLog(String str) {
        printSimpleLog(Log.getPrefix(), str);
    }

    /**
     * Prints a simple log for this application. This log will store into
     * SharedMemory and can be displayed in Diagnostics screen.
     *
     * @param str log.
     */
    public void printSimpleLog(String tag, String str) {
        DataAdapterManager.getInstance().getBuiltInFileAdapter().updateLog(tag, 1, str);
    }

    /**
     * Sets the current language.
     *
     * @param language language.
     */
    private void setCurrentLanguage(String language) {
        if (language.equals(Constants.LANGUAGE_ENGLISH)) {
            currentLanguage = Constants.LANGUAGE_ENGLISH;
        } else if (language.equals(Constants.LANGUAGE_FRENCH)) {
            currentLanguage = Constants.LANGUAGE_FRENCH;
        } else {
            return;
        }
        DataCenter.getInstance().setLanguagePostfix(currentLanguage);
        Formatter.setLanguage(currentLanguage);
    }

    /**
     * Returns the current language.
     *
     * @returns String current language.
     */
    public String getCurrentLanguage() {
        return currentLanguage;
    }

    /** Called when Language setting is changed. */
    public void dataUpdated(String key, Object old, Object value) {
    	if (key.equals("language")) {
	        try {
	            setCurrentLanguage(value.toString().substring(0, 2).toLowerCase());
	        } catch (Exception ex) {
	            Log.print(ex);
	        }
    	} else if (key.equals("SUPPORT_UHD")) {
    		if (value.equals("true")) {
    			Environment.SUPPORT_UHD = true;
    		} else {
    			Environment.SUPPORT_UHD = false;
    		}
    		
    	}
    }

    public void dataRemoved(String key) {
    }

}

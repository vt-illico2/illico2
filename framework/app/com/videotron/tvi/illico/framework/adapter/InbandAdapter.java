package com.videotron.tvi.illico.framework.adapter;

import java.util.*;
import java.io.File;
import org.ocap.net.OcapLocator;
import org.davic.net.Locator;
import org.dvb.dsmcc.ServiceDomain;
import org.dvb.dsmcc.DSMCCObject;
import org.dvb.dsmcc.SuccessEvent;
import org.dvb.dsmcc.AsynchronousLoadingEvent;
import org.dvb.dsmcc.AsynchronousLoadingEventListener;
import org.dvb.dsmcc.InvalidPathNameException;
import org.dvb.dsmcc.NotLoadedException;
import org.davic.net.InvalidLocatorException;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;

public class InbandAdapter extends AbstractFileAdapter {

    private static final int ATTACH_RETRY_COUNT = 5;
    private static final long ATTACH_RETRY_DELAY = 1000L;

    private String[] files;
    private int carouselId;

    private ServiceDomain currentDomain;

    public InbandAdapter(String configPath, int cid) {
        super(configPath);
        this.carouselId = cid;
        this.files = new String[keyList.size()];
        for (int i = 0; i < files.length; i++) {
            files[i] = (String) table.get(keyList.elementAt(i));
        }
    }

    protected String getDebugScreenLogKey() {
        return "InbandAdapter.updateStatusTable";
    }

    public void start() {
    }

    public void asynchronousLoad(String locatorUrl) {
        load(locatorUrl, false, true);
    }

    public void asynchronousLoad(String locatorUrl, boolean detach) {
        load(locatorUrl, false, detach);
    }

    public void synchronousLoad(String locatorUrl) {
        load(locatorUrl, true, true);
    }

    private void load(String locatorUrl, boolean sync, boolean detach) {
        if (files.length == 0) {
            return;
        }
        if (locatorUrl == null || Environment.EMULATOR) {
            loadFromLocal();
            return;
        }
        OcapLocator locator = null;
        try {
            locator = new OcapLocator(locatorUrl);
        } catch (InvalidLocatorException ex) {
            Log.print(ex);
        }
        if (locator == null) {
            return;
        }
        ServiceDomain serviceDomain = attach(locator, carouselId, ATTACH_RETRY_COUNT, ATTACH_RETRY_DELAY);
        if (serviceDomain == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("InbandAdapter: attach failed. return.");
            }
            return;
        }
        this.currentDomain = serviceDomain;
        DSMCCObject mountPoint = serviceDomain.getMountPoint();
        if (mountPoint == null) {
            if (Log.WARNING_ON) {
                Log.printWarning("InbandAdapter: mountPoint is null.");
            }
            updateLog(Log.getPrefix(), 0, "failed to get mount point");
            return;
        }
        removeLog(Log.getPrefix());

        DataCenter dataCenter = DataCenter.getInstance();

        InbandReceiveChecker checker = new InbandReceiveChecker(files.length);

        synchronized (checker) {
            boolean wait = false;
            for (int i = 0; i < files.length; i++) {
//                DSMCCObject object = new DSMCCObject(mountPoint, files[i]);
                DSMCCObject object = new DSMCCObject(mountPoint, getConvertedPath(files[i]));
                String key = (String) keyList.elementAt(i);
                String objectName = object.getName();
                if (sync) {
                    try {
                        updateLog(objectName, 0, "loading...");
                        object.synchronousLoad();
                        dataCenter.put(key, object);
                        updateLog(objectName, 1, "synchronousLoad completed.");
                    } catch (Throwable ex) {
                        Log.print(ex);
                        updateLog(objectName, 1, ex.toString());
                    }
                    unload(object);
                } else {
                    checker.put(objectName, key);
                    updateLog(objectName, 0, "loading...");
                    try {
                        object.asynchronousLoad(checker);
                        wait = true;
                    } catch (InvalidPathNameException ex) {
                        Log.print(ex);
                        checker.check();
                        updateLog(objectName, 1, ex.toString());
                    }
                }
            }
            if (wait) {
                try {
                    checker.wait();
                } catch (InterruptedException ex) {
                    Log.print(ex);
                }
            }
        }
        if (detach) {
            detach();
        }
    }

    public void detach() {
        if (currentDomain != null) {
            Log.printInfo("InbandAdapter: detach");
            try {
                currentDomain.detach();
            } catch (NotLoadedException ex) {
                Log.print(ex);
            }
            currentDomain = null;
        }
    }

    private void loadFromLocal() {
        for (int i = 0; i < files.length; i++) {
            File f = new File(getConvertedPath(files[i]));
            String key = (String) keyList.elementAt(i);
            try {
                if (f.exists()) {
                    if (Log.INFO_ON) {
                        Log.printInfo("InbandAdapter: read files from app : " + f);
                    }
                    DataCenter.getInstance().put(key, f);
                    updateLog(f.getName(), 1, f.toString());
                } else {
                    updateLog(f.getName(), 1, "InvalidPath : " + f.toString());
                }
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }

    public ServiceDomain attach(Locator locator, int carouselId, int maxRetry, long delay) {
        ServiceDomain serviceDomain = new ServiceDomain();
        for (int i = 0; i < maxRetry; i++) {
            updateLog(Log.getPrefix(), 0, "trying to attach " + i);
            try {
                serviceDomain.attach(locator, carouselId);

                if (serviceDomain.isAttached()) {
                    return serviceDomain;
                }
                if (Log.INFO_ON) {
                    Log.printInfo("InbandAdapter: attach failed. retry " + i);
                }
                Thread.sleep(delay);
            } catch (Throwable ex) {
                Log.print(ex);
            }
        }
        updateLog(Log.getPrefix(), 0, "failed to attach");
        return null;
    }

    private String getConvertedPath(String name) {
        String fullPath = name;
        int pos = name.indexOf('%');
        if (pos > 0) {
            int r = name.indexOf('%', pos + 1);
            if (r > 0) {
                String waitKey = name.substring(pos + 1, r);
                updateLog(name, 0, "waiting data : " + waitKey);
                Object value = DataCenter.getInstance().waitForData(waitKey, Constants.MS_PER_MINUTE);
                removeLog(name);
                fullPath = name.substring(0, pos) + value + name.substring(r + 1);
            }
        }
        return fullPath;
    }

    /////////////////////////////////////////////////////////////////////////
    // AsynchronousLoadingEventListener
    /////////////////////////////////////////////////////////////////////////

    class InbandReceiveChecker extends Hashtable implements AsynchronousLoadingEventListener {
        private static final long serialVersionUID = 234567;

        private int count;

        public InbandReceiveChecker(int size) {
            count = size;
        }

        private synchronized void check() {
            count--;
            if (count == 0) {
                notifyAll();
                clear();
            }
        }

        public void receiveEvent(AsynchronousLoadingEvent e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("InbandReceiveChecker: AsynchronousLoadingEvent = " + e);
            }
            DSMCCObject o = (DSMCCObject) e.getSource();
            String objectName = o.getName();
            if (e instanceof SuccessEvent) {
                try {
                    DataCenter.getInstance().put((String) get(objectName), o);
                } catch (Throwable t) {
                    Log.print(t);
                }
            }
            check();
            updateLog(objectName, 1, e.toString());
            unload(o);
        }
    }

}


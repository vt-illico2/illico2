package com.videotron.tvi.illico.framework.adapter;

import java.awt.*;
import java.io.File;
import java.util.Vector;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Date;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.io.TextReader;
import com.videotron.tvi.illico.log.Log;
import javax.tv.util.*;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.SharedMemory;
import org.dvb.dsmcc.DSMCCObject;
import java.io.*;

/**
 * This class represents the abstract file adapter.
 *
 * @version $Revision: 1.23 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public abstract class AbstractFileAdapter {

    /** contains [key, filename] map. */
    protected Hashtable table = new Hashtable();
    /** contains key list. */
    protected Vector keyList = new Vector();

    protected Hashtable updateStatusTable;

    /**
     * Construct this adapter with config file path.
     * @param configPath path of config file.
     */
    public AbstractFileAdapter(String configPath) {
        // Not using Properties, because the order of list may be important
        //  for some applications.
        File file = new File(configPath);
        if (file.exists()) {
            String[] lines = TextReader.read(file);
            for (int i = 0; lines != null && i < lines.length; i++) {
                String s = lines[i];
                if (s == null || s.startsWith("#") || s.startsWith("!")) {
                    continue;
                }
                int pos = s.indexOf('=');
                if (pos < 0) {
                    continue;
                }
                String key = s.substring(0, pos).trim();
                String value = s.substring(pos + 1).trim();
                table.put(key, value);
                keyList.addElement(key);
            }
        } else {
            Log.printWarning("FileNotFound: " + configPath);
        }
        String logKey = getDebugScreenLogKey();
        updateStatusTable = (Hashtable) SharedMemory.getInstance().get(logKey);
        if (updateStatusTable == null) {
            updateStatusTable = new Hashtable();
            SharedMemory.getInstance().put(logKey, updateStatusTable);
        }
    }

    protected abstract String getDebugScreenLogKey();

    /** starts this adapter. */
    public abstract void start();

    /////////////////////////////////////////////////////////////////////////
    // Logs for Debug Screen
    /////////////////////////////////////////////////////////////////////////

    private Formatter timeFormatter = Formatter.get(Constants.LANGUAGE_FRENCH);

    public void removeLog(String name) {
        updateStatusTable.remove(name);
    }

    public void updateLog(String name, int inc, String log) {
        Object[] newStatus = new Object[4];
        Object[] oldStatus = (Object[]) updateStatusTable.get(name);
        newStatus[0] = name;
        if (oldStatus == null) {
            newStatus[1] = new Integer(0);
        } else {
            newStatus[1] = new Integer(((Integer) oldStatus[1]).intValue() + inc);
        }
        newStatus[2] = Log.DEBUG_DATE_FORMAT.format(new Date());
        if (log == null) {
            log = "null";
        } else if (log.startsWith("org.dvb.dsmcc.")) {
            log = log.substring("org.dvb.dsmcc.".length());
        }
        newStatus[3] = log;
        updateStatusTable.put(name, newStatus);
    }

    protected void unload(DSMCCObject obj) {
        if (obj.isLoaded()) {
            try {
                obj.unload();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
    }

    //->Kenneth[2016.5.2] OOB 뿐 아니라 Inband 에도 사용하기 위해서 여기로 옮김
    public boolean dumpNeeded = false;
    public void needDump(boolean dumpNeeded) {
        this.dumpNeeded = dumpNeeded;
    }

    protected void dumpFile(String name, File src, boolean isOOB) {
        if (Log.DEBUG_ON) Log.printDebug("AbstractFileAdapter.dumpFile("+name+", "+src.toString()+")");
        try {
            String path = Log.getLogicalStorageVolume()+"/dump_OOB/";
            if (!isOOB) {
                path = Log.getLogicalStorageVolume()+"/dump_IB/";
            }
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdir();
            }
            File dst = new File(dir, name);
            if (dst.exists()) {
                dst.delete();
            }
            dst.createNewFile();
            copy(src, dst);
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
    }

    private void copy(File src, File dst) throws IOException {
        InputStream in = new FileInputStream(src);
        try {
            OutputStream out = new FileOutputStream(dst);
            try {
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
            } finally {
                out.close();
            }
        } finally {
            in.close();
        }
    }
    //<-

}


package com.videotron.tvi.illico.framework.adapter;

import java.io.File;
import java.util.Enumeration;
import java.util.Date;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

public class BuiltInFileAdapter extends AbstractFileAdapter {

    public BuiltInFileAdapter(String configPath) {
        super(configPath);
    }

    protected String getDebugScreenLogKey() {
        return "BuiltInFileAdapter.updateStatusTable";
    }

    public void start() {
        int count = 0;
        Enumeration en = keyList.elements();
        while (en.hasMoreElements()) {
            String key = (String) en.nextElement();
            Object value = table.get(key);
            File f = new File(DataAdapterManager.RESOURCE_ROOT, (String) value);
            if (!f.exists()) {
                if (Log.WARNING_ON) {
                    Log.printWarning("BuiltInFileAdapter: file not found : " + f);
                }
            } else if (f.isDirectory()) {
                if (Log.WARNING_ON) {
                    Log.printWarning("BuiltInFileAdapter: file is directory : " + f);
                }
            } else {
                DataCenter.getInstance().put(key, f);
                count++;
            }
        }
        updateLog(Log.getPrefix(), 0, "Loaded " + count + " files. Framework = " + FrameworkMain.VERSION);
    }

}


package com.videotron.tvi.illico.framework.adapter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.io.*;
import org.davic.net.InvalidLocatorException;
import org.dvb.dsmcc.AsynchronousLoadingEvent;
import org.dvb.dsmcc.AsynchronousLoadingEventListener;
import org.dvb.dsmcc.DSMCCObject;
import org.dvb.dsmcc.InvalidFormatException;
import org.dvb.dsmcc.InvalidPathNameException;
import org.dvb.dsmcc.NotLoadedException;
import org.dvb.dsmcc.ObjectChangeEvent;
import org.dvb.dsmcc.ObjectChangeEventListener;
import org.dvb.dsmcc.ServiceDomain;
import org.dvb.dsmcc.SuccessEvent;
import org.ocap.net.OcapLocator;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.SharedDataKeys;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import javax.tv.util.TVTimerScheduleFailedException;

public class OobAdapter extends AbstractFileAdapter implements Runnable, TVTimerWentOffListener, ActionListener {

    public static final String COMMAND_RESET = "command_reset";
    public static final String COMMAND_SET_TIMER = "command_set_timer";
    //->Kenneth[2015.10.19]
    public static final String COMMAND_DUMP = "command_dump";
    public static final String COMMAND_LOAD_FROM_DUMP = "command_load_from_dump";
    //<-

    private static final int ATTACH_RETRY_COUNT = 500;
    private static final long ATTACH_RETRY_DELAY = 2000L;

    private byte[] nsap;
    private DSMCCObject mountPoint;
    private ServiceDomain serviceDomain;
    private OobUpdateChecker checker;

    private int updateHour = 1;
    private int updateMinute = 0;
    private TVTimerSpec updateTimer = new TVTimerSpec();

    public OobAdapter(String configPath, String nsapPath) {
        super(configPath);
        this.nsap = readNSAP(nsapPath);

        updateTimer = new TVTimerSpec();
        updateTimer.setRepeat(false);
        updateTimer.setAbsolute(true);
        updateTimer.addTVTimerWentOffListener(this);

        if (keyList.size() > 0 && table.size() > 0) {
            Hashtable oobAdapterTable;
            synchronized (Runtime.getRuntime()) {
                oobAdapterTable = (Hashtable) SharedMemory.getInstance().get(SharedDataKeys.OOB_ADAPTER_TABLE);
                if (oobAdapterTable == null) {
                    oobAdapterTable = new Hashtable();
                    SharedMemory.getInstance().put(SharedDataKeys.OOB_ADAPTER_TABLE, oobAdapterTable);
                }
            }
            oobAdapterTable.put(FrameworkMain.getInstance().getApplicationName(), this);
        }
    }

    protected String getDebugScreenLogKey() {
        return "OobAdapter.updateStatusTable";
    }

    //->Kenneth[2016.5.2] inband 에도 쓰기 위해서 AbstractFileAdapter 로 옮김
    /*
    //->Kenneth [2015.10.19]
    private boolean dumpNeeded = false;
    public void start(boolean dumpNeeded) {
        this.dumpNeeded = dumpNeeded;
        start();
    }
    //<-
    */

    public void start() {
        if (keyList.size() > 0 && table.size() > 0) {
            new Thread(this, "OobAdapter.start").start();
        } else {
            if (Log.INFO_ON) {
                Log.printInfo("OobAdapter: skip to start.");
            }
        }
    }

    public DSMCCObject getMountPoint() {
        return mountPoint;
    }

    private void disposeAll() {
        if (serviceDomain != null) {
            try {
                serviceDomain.detach();
            } catch (Exception ex) {
                Log.print(ex);
            }
            serviceDomain = null;
        }
        if (checker != null) {
            checker.dispose();
            checker = null;
        }
    }

    private void startTimer() {
        try {
            int[] time = (int[]) SharedMemory.getInstance().get(SharedDataKeys.OOB_RELOAD_TIME);
            if (time != null && time.length >= 2) {
                this.updateHour = time[0];
                this.updateMinute = time[1];
            }
        } catch (Exception ex) {
            Log.print(ex);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, updateHour);
        cal.set(Calendar.MINUTE, updateMinute);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long nextTime = cal.getTimeInMillis();
        if (nextTime < System.currentTimeMillis()) {
            nextTime += Constants.MS_PER_DAY;
        }
        startTimer(nextTime);
    }


    private synchronized void startTimer(long time) {
        if (Log.INFO_ON) {
            Log.printInfo("OobAdapter.startTimer = " + new Date(time));
        }
        updateTimer.setTime(time);
        TVTimer.getTimer().deschedule(updateTimer);
        try {
            updateTimer = TVTimer.getTimer().scheduleTimerSpec(updateTimer);
        } catch (TVTimerScheduleFailedException ex) {
            Log.print(ex);
        }
    }

    /**
     * TVTimerWentOffListener implementation. Called when re-attach is needed.
     * @param e TVTimerWentOffEvent.
     */
    public void timerWentOff(TVTimerWentOffEvent e) {
        start();
    }

    public void run() {
        disposeAll();
        attachAndLoad();
        startTimer();
    }

    private void attachAndLoad() {
        if (nsap != null) {
            this.serviceDomain = attach(nsap);
            if (serviceDomain == null) {
                if (Log.WARNING_ON) {
                    Log.printWarning("OobAdapter: attach failed. return.");
                }
                return;
            }
            updateLog(Log.getPrefix(), 0, "trying to get mount point");
            this.mountPoint = serviceDomain.getMountPoint();
            if (mountPoint == null) {
                if (Log.WARNING_ON) {
                    Log.printWarning("OobAdapter: mountPoint is null.");
                }
                updateLog(Log.getPrefix(), 0, "failed to get mount point");
                return;
            }
            removeLog(Log.getPrefix());
        } else {
            if (Log.WARNING_ON) {
                Log.printWarning("OobAdapter: can't read nsap.");
            }
        }

        this.checker = new OobUpdateChecker();

        for (int i = 0; i < keyList.size(); i++) {
            String key = (String) keyList.elementAt(i);
            String file = (String) table.get(key);
            if (nsap != null) {
                DSMCCObject object = new DSMCCObject(mountPoint, file);
                try {
                    checker.load(object, key);
                } catch (InvalidPathNameException ex) {
                    Log.print(ex);
                    readLocalFile(key, file);
                }
            } else {
                readLocalFile(key, file);
            }
        }
    }

    private boolean readLocalFile(String key, String file) {
        File f = new File(file);
        try {
            if (f.exists()) {
                if (Log.INFO_ON) {
                    Log.printInfo("OobAdapter: read files from app : " + f);
                }
                DataCenter.getInstance().put(key, f);
                updateLog(f.getName(), 1, f.toString());
                return true;
            }
        } catch (Throwable t) {
            Log.print(t);
        }
        updateLog(f.getName(), 1, "InvalidPath : " + f.toString());
        return false;
    }


    /** For testing with Emulator. */
    public void reloadLocalFiles() {
        Thread t = new Thread() {
            public void run() {
                for (int i = 0; i < keyList.size(); i++) {
                    String key = (String) keyList.elementAt(i);
                    String file = (String) table.get(key);
                    readLocalFile(key, file);
                }
            }
        };
        t.start();
    }

    private static byte[] readNSAP(String nsapPath) {
        if (Environment.EMULATOR) {
            // OOB is not working with Emulator, files need to read from local directory.
            return null;
        }
        byte[] nsap = null;
        try {
            FileInputStream fis = new FileInputStream(nsapPath);
            BufferedInputStream bis = new BufferedInputStream(fis, 1024);
            nsap = new byte[20];
            bis.read(nsap);
            bis.close();
            fis.close();
        } catch (Exception ex) {
            Log.print(ex);
        }
        return nsap;
    }


    private ServiceDomain attach(byte[] nsap) {
        ServiceDomain domain = new ServiceDomain();
        for (int i = 0; i < ATTACH_RETRY_COUNT; i++) {
            updateLog(Log.getPrefix(), 0, "trying to attach " + i);
            try {
                domain.attach(nsap);
            } catch (Throwable ex) {
                Log.print(ex);
            }
            if (domain.isAttached()) {
                return domain;
            }
            if (Log.INFO_ON) {
                Log.printInfo("OobAdapter: attach failed. retry " + i);
            }
            try {
                Thread.sleep(ATTACH_RETRY_DELAY);
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
        updateLog(Log.getPrefix(), 0, "failed to attach");
        return null;
    }

    /////////////////////////////////////////////////////////////////////////
    // ObjectChangeEventListener
    /////////////////////////////////////////////////////////////////////////

    class OobUpdateChecker extends Hashtable implements AsynchronousLoadingEventListener, ObjectChangeEventListener {
        private static final long serialVersionUID = 123456;
        private Vector objectList = new Vector();

        void load(DSMCCObject object, String key) throws InvalidPathNameException {
            objectList.addElement(object);
            String name = object.getName();
            put(name, key);
            updateLog(name, 0, "loading...");
            object.asynchronousLoad(this);
        }

        void dispose() {
            Enumeration en = objectList.elements();
            while (en.hasMoreElements()) {
                DSMCCObject object = (DSMCCObject) en.nextElement();
                object.removeObjectChangeEventListener(this);
                unload(object);
            }
            objectList.clear();
        }

        //->Kenneth[2016.5.2] Inband 에도 사용하기 위해 AbstractFileAdapter 로 옮김
        /*
        //->Kenneth[2015.10.19]
        private void dumpFile(String name, File src) {
            if (Log.DEBUG_ON) Log.printDebug("OobUpdateChecker.dumpFile("+name+", "+src.toString()+")");
            try {
                //String path = System.getProperty("dvb.persistent.root")+"/dump_OOB/";
                String path = Log.getLogicalStorageVolume()+"/dump_OOB/";
                File dir = new File(path);
                if (!dir.exists()) {
                    dir.mkdir();
                }
                File dst = new File(dir, name);
                if (dst.exists()) {
                    dst.delete();
                }
                dst.createNewFile();
                copy(src, dst);
            } catch (Exception e) {
                if (Log.DEBUG_ON) Log.print(e);
            }
        }
        //<-

        //->Kenneth[2015.10.19]
        private void copy(File src, File dst) throws IOException {
            InputStream in = new FileInputStream(src);
            try {
                OutputStream out = new FileOutputStream(dst);
                try {
                    byte[] buf = new byte[1024];
                    int len;
                    while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                    }
                } finally {
                    out.close();
                }
            } finally {
                in.close();
            }
        }
        //<-
        */

        public void receiveEvent(AsynchronousLoadingEvent e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("OobUpdateChecker: AsynchronousLoadingEvent = " + e);
            }
            DSMCCObject o = (DSMCCObject) e.getSource();
            String objectName = o.getName();
            if (e instanceof SuccessEvent) {
                try {
                    o.addObjectChangeEventListener(this);
                } catch (Exception ex) {
                    Log.print(ex);
                }
                try {
                    DataCenter.getInstance().put((String) get(objectName), o);
                } catch (Throwable t) {
                    Log.print(t);
                }
                //->Kenneth[2015.10.19]
                if (dumpNeeded) {
                    dumpFile(objectName, o, true);
                }
                //<-
                unload(o);
            } else {
                try {
                    o.asynchronousLoad(this);
                } catch (InvalidPathNameException ex) {
                    Log.print(ex);
                }
            }
            updateLog(objectName, 1, e.toString());
        }

        public void receiveObjectChangeEvent(ObjectChangeEvent e) {
            if (Log.DEBUG_ON) {
                Log.printDebug("OobUpdateChecker: ObjectChangeEvent = " + e);
            }
            DSMCCObject o = (DSMCCObject) e.getSource();
            if (o == null) {
                Log.printWarning("OobUpdateChecker: DSMCCObject is null");
                return;
            }
            String objectName = o.getName();
            unload(o);
            try {
                o.synchronousLoad();
            } catch (InvalidFormatException t) {
                Log.print(t);
                updateLog(objectName, 1, t.toString());
                startTimer(System.currentTimeMillis() + Constants.MS_PER_MINUTE);
                return;
            } catch (Exception t) {
                Log.print(t);
                updateLog(objectName, 1, t.toString());
                return;
            }
            try {
                DataCenter.getInstance().put((String) get(objectName), o);
            } catch (Throwable t) {
                Log.print(t);
            }
            updateLog(objectName, 1, e.toString());
            unload(o);
        }
    }

    //->Kenneth[2015.10.19]
    private void loadFromDump() {
        if (Log.DEBUG_ON) Log.printDebug("OobAdapter.loadFromDump()");
        try {
            //String path = System.getProperty("dvb.persistent.root")+"/dump_OOB/";
            String path = Log.getLogicalStorageVolume()+"/dump_OOB/";
            File dir = new File(path);
            if (!dir.exists()) {
                if (Log.DEBUG_ON) Log.printDebug("OobAdapter.loadFromDump() : folder doesn't exist.");
                return;
            }
            File[] files = dir.listFiles();
            if (files == null || files.length == 0) {
                if (Log.DEBUG_ON) Log.printDebug("OobAdapter.loadFromDump() : files don't exist.");
                return;
            }
            if (checker == null) {
                if (Log.DEBUG_ON) Log.printDebug("OobAdapter.loadFromDump() : checker is null.");
                return;
            }
            for (int i = 0 ; i < files.length ; i++) {
                try {
                    if (Log.DEBUG_ON) Log.printDebug("OobAdapter.loadFromDump() : files["+i+"] = "+files[i].getName());
                    DataCenter.getInstance().put((String)checker.get(files[i].getName()), files[i]);
                    updateLog(files[i].getName(), 1, "From Dump file : "+files[i].toString());
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        } catch (Exception e) {
            if (Log.DEBUG_ON) Log.print(e);
        }
    }
    //<-

    /////////////////////////////////////////////////////////////////////////
    // ActionListener
    /////////////////////////////////////////////////////////////////////////

    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if (COMMAND_RESET.equals(command)) {
            start();
        //->Kenneth [2015.10.19]
        } else if (COMMAND_DUMP.equals(command)) {
            needDump(true);
            start();
        } else if (COMMAND_LOAD_FROM_DUMP.equals(command)) {
            loadFromDump();
        //<-
        } else if (COMMAND_SET_TIMER.equals(command)) {
            startTimer();
        }
    }
}


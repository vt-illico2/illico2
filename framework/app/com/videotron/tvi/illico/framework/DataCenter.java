package com.videotron.tvi.illico.framework;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;

/**
 * DataCenter is the repository of Application Data. It has two Hashtable
 * internally. Data cache is managed in two levels. When application state is
 * paused, content of WeakRepository is flushed. Data classification in each
 * Repository is determined by Application.
 *
 * @version $Revision: 1.30 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public final class DataCenter {

	public static final boolean	ALWAYS_FIND_LANGUAGE_RELATED_IMAGE	= false;

	/** Repository id that represents STRONG_REFERENCE. */
	public static final int		STRONG_REFERENCE					= 7;
	/** Repository id that represents WEAK_REFERENCE. */
	public static final int		WEAK_REFERENCE						= 3;

	/** StrongRepository instance. */
	private Hashtable			strongRepository					= new Hashtable();
	/** WeakRepository instance. */
	private Hashtable			weakRepository						= new Hashtable();

	/**
	 * Contains the <code>DataUpdateListener</code>.
	 * Key of this Hashtable should be same with DataCenter's, and the values should be
	 * s <code>Vector</code> instance which conatins <code>DataUpdateListener</code>.
	 */
	private Hashtable			listeners							= new Hashtable();

	/** Current language postfix. */
	private String				languagePostfix						= "en";

	/** Singleton instance of this class. */
	private static DataCenter	instance							= new DataCenter();

	/** Constructor. */
	private DataCenter() {
	}

	/**
	 * Returns the singleton instance of DataCenter.
	 *
	 * @return DataCenter
	 */
	public static DataCenter getInstance() {
		return instance;
	}

	/**
	 * Puts the specified key with the specified value to this DataCenter.
	 * By default the data will be stored into strong repository.
	 *
	 * @param key the hashtable key.
	 * @param value the value.
	 */
	public void put(String key, Object value) {
		put(key, value, STRONG_REFERENCE);
	}

	/**
	 * Puts the specified key with the specified value to this DataCenter.
	 *
	 * @param key the hashtable key.
	 * @param value the value.
	 * @param level the level represents repository which this data should be stored.
	 */
	public void put(String key, Object value, int level) {
		Object oldValue = get(key);
		switch (level) {
			case STRONG_REFERENCE:
				strongRepository.put(key, value);
				break;
			case WEAK_REFERENCE:
				weakRepository.put(key, value);
				break;
			default:
				return;
		}
		fireDataUpdatedEvent(key, oldValue, value);
	}

	/**
	 * Returns the value to which the specified key is mapped in this DataCenter.
	 *
	 * @param key a key in the DataCenter.
	 * @return the value.
	 */
	public Object get(String key) {
		Object o = find(key);
		if (o != null) {
			return o;
		}
		else {
			return find(key + '_' + languagePostfix);
		}
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to <code>String</code> and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @return String object which is converted from value.
	 */
	public String getString(String key) {
		Object o = get(key);
		if (o == null) {
			return "";
		}
		else if (o instanceof String) {
			return (String) o;
		}
		else {
			return o.toString();
		}
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to integer and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @return a integer value.
	 */
	public int getInt(String key) {
		return getInt(key, 0);
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to integer and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @param defaultValue default value
	 * @return a integer value.
	 */
	public int getInt(String key, int defaultValue) {
		try {
			return Integer.parseInt(getString(key));
		}
		catch (NumberFormatException ex) {
			return defaultValue;
		}
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to long and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @return a long value.
	 */
	public long getLong(String key) {
		return getLong(key, 0);
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to long and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @param defaultValue default value
	 * @return a long value.
	 */
	public long getLong(String key, long defaultValue) {
		try {
			return Long.parseLong(getString(key));
		}
		catch (NumberFormatException ex) {
			return defaultValue;
		}
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Color and return it. The string representation of such
	 * a color in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>RR,GG,BB&lt;,AA&gt;</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>RR</b> is the red value (between 0 and 255)
	 *	<li><b>GG</b> is the green value (between 0 and 255)
	 *	<li><b>BB</b> is the blue value (between 0 and 255)
	 *	<li><b>AA</b> is the alpha value (between 0 and 255). This parameter is optional.
	 *		If it is not provided, the default value is 255 (i.e. 100% opaque).
	 *</ul>
	 *<p>Note: There is minimal validation of the RGBA values.
	 * @param key a key in the DataCenter.
	 * @return a Color built from the string representation. null is returned if the key is not found.
	 */
	public Color getColor(String key) {
		return getColor(key, null);
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Color and return it. The string representation of such
	 * a color in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>RR,GG,BB&lt;,AA&gt;</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>RR</b> is the red value (between 0 and 255)
	 *	<li><b>GG</b> is the green value (between 0 and 255)
	 *	<li><b>BB</b> is the blue value (between 0 and 255)
	 *	<li><b>AA</b> is the alpha value (between 0 and 255). This parameter is optional.
	 *		If it is not provided, the default value is 255 (i.e. 100% opaque).
	 *</ul>
	 *<p>Note: There is minimal validation of the RGBA values.
	 * @param key a key in the DataCenter.
	 * @return a Color built from the string representation.
	 * <tt>defaultValue</tt> is returned if the key is not found or if the key does not
	 * return a valid string.
	 */
	public Color getColor(String key, Color defaultValue) {
		Object o = get(key);
		Color returnValue = defaultValue;

		if (o instanceof String) {

			returnValue = parseColor((String) o);
		}
		if (returnValue == null)
			return defaultValue;
		return returnValue;
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Font and returns it. The string representation of such
	 * a font in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>Font Name,Size&lt;,Style&rt;</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>Font Name</b> is the name of the font. It can contain spaces but no a comma (,).
	 *	<li><b>Size</b> is the size of the font
	 *	<li><b>Style</b> is the style desired. Possible values are <strong>B</strong>,
	 *		<strong>I</strong>, <strong>BI</strong>, and <strong>P</strong>. These represent,
	 *		respectively, <b>bold</b>, <em>italics</em>, <b><em>bold italics</em></b> and plain.
	 *		This parameter is optional and defaults to P.
	 *</ul>
	 *<p>Note: There is minimal validation of the values.
	 * @param key a key in the DataCenter.
	 * @return a Font built from the string representation. null is returned if the key is not found.
	 */
	public Font getFont(String key) {
		return getFont(key, null);
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Font and returns it. The string representation of such
	 * a font in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>Font Name,Size&lt;,Style&rt;</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>Font Name</b> is the name of the font. It can contain spaces but no a comma (,).
	 *	<li><b>Size</b> is the size of the font
	 *	<li><b>Style</b> is the style desired. Possible values are <strong>B</strong>,
	 *		<strong>I</strong>, <strong>BI</strong>, and <strong>P</strong>. These represent,
	 *		respectively, <b>bold</b>, <em>italics</em>, <b><em>bold italics</em></b> and plain.
	 *		This parameter is optional and defaults to P.
	 *</ul>
	 *<p>Note: There is minimal validation of the values.
	 * @param key a key in the DataCenter.
	 * @return a Font built from the string representation. <code>defaultValue</code> is returned
	 * if the key is not found or if the string representation is invalid.
	 */
	public Font getFont(String key, Font defaultValue) {
		Object o = get(key);
		Font returnValue = defaultValue;
		if (o instanceof String) {

			returnValue = parseFont((String) o);
		}
		if (returnValue == null)
			return defaultValue;
		return returnValue;
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Font and returns it. The string representation of such
	 * a rectangle in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>X,Y,W,H</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>X</b> is the X position
	 *	<li><b>Y</b> is the Y position
	 *	<li><b>W</b> is the width of the rectangle
	 *	<li><b>H</b> is the height of the rectangle
	 *</ul>
	 * @param key a key in the DataCenter.
	 * @return a Rectangle built from the string representation or null if the key does not exist
	 */
	public Rectangle getRectangle(String key) {
		return getRectangle(key, null);
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to a Font and returns it. The string representation of such
	 * a rectangle in the properties file is as follows:
	 *
	 *<p><tt><b>myKey</b>=<em>X,Y,W,H</em></tt>
	 *<p>where
	 *<ul>
	 *	<li><b>X</b> is the X position
	 *	<li><b>Y</b> is the Y position
	 *	<li><b>W</b> is the width of the rectangle
	 *	<li><b>H</b> is the height of the rectangle
	 *</ul>
	 * @param key a key in the DataCenter.
	 * @return a Rectangle built from the string representation.
	 * <tt>defaultValue</tt> is returned if the key is not found or if the key does not
	 * return a valid string.
	 */
	public Rectangle getRectangle(String key, Rectangle defaultValue) {
		Object o = get(key);
		Rectangle returnValue = defaultValue;

		if (o instanceof String) {

			returnValue = parseRectangle((String) o);
		}
		if (returnValue == null)
			return defaultValue;
		return returnValue;
	}

	/**
	 * Parse a string representation of a color and return a {@link Color} object.
	 * @param colorValue
	 * @return
	 */
	private Color parseColor(String colorValue) {
		Color returnValue = null;
		StringTokenizer st = new StringTokenizer(colorValue, ",", false);
		try {
			int red = -1;
			int green = -1;
			int blue = -1;
			int alpha = 255;

			if (st.hasMoreElements())
				red = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements())
				green = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements())
				blue = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements()) {
				alpha = Integer.parseInt(st.nextToken());
			}

			if (red != -1 && blue != -1 && green != -1 && alpha != -1) {
				returnValue = new Color(red, green, blue, alpha);
			}

		}
		catch (NumberFormatException e) {
			// ignore
		}
		return returnValue;
	}

	/**
	 * Parse a string representation of a font and return a {@link Font} object.
	 * @param colorValue
	 * @return
	 */
	private Font parseFont(String fontValue) {
		Font returnValue = null;
		StringTokenizer st = new StringTokenizer(fontValue, ",", false);
		try {
			String family = null;
			int size = -1;
			int style = Font.PLAIN;

			if (st.hasMoreElements())
				family = st.nextToken();
			if (st.hasMoreElements())
				size = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements()) {
				String tok = st.nextToken().toUpperCase();
				if (tok.startsWith("P")) {
					// do nothing
				}
				else if (tok.startsWith("BI")) {
					style = Font.BOLD | Font.ITALIC;
				}
				else if (tok.startsWith("B")) {
					style = Font.BOLD;
				}
				else if (tok.startsWith("I")) {
					style = Font.ITALIC;
				}
				else {
					style = -1;
				}
			}
			if (family != null && size != -1 && style != -1) {
				returnValue = new Font(family, style, size);
			}

		}
		catch (NumberFormatException e) {
			// ignore
		}
		return returnValue;
	}

	/**
	 * Parse a string representation of a rectangle and return a {@link Rectangle} object.
	 * @param colorValue
	 * @return
	 */
	private Rectangle parseRectangle(String rectValue) {
		Rectangle returnValue = null;
		StringTokenizer st = new StringTokenizer(rectValue, ",", false);
		try {
			int x = -1;
			int y = -1;
			int width = -1;
			int height = 255;

			if (st.hasMoreElements())
				x = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements())
				y = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements())
				width = Integer.parseInt(st.nextToken());
			if (st.hasMoreElements()) {
				height = Integer.parseInt(st.nextToken());
			}

			if (x != -1 && width != -1 && y != -1 && height != -1) {
				returnValue = new Rectangle(x, y, width, height);
			}

		}
		catch (NumberFormatException e) {
			// ignore
		}
		return returnValue;
	}

	/**
	 * Gets the value to which the specified key is mapped in this DataCenter,
	 * converts it to boolean and return it.
	 *
	 * @param key a key in the DataCenter.
	 * @return a boolean value.
	 */
	public boolean getBoolean(String key) {
		return "true".equalsIgnoreCase(getString(key));
	}

	/**
	 * Finds the value to which the specified key is mapped in this DataCenter.
	 *
	 * @param key a key in the DataCenter.
	 * @return the value.
	 */
	private Object find(String key) {
		Object o = strongRepository.get(key);
		if (o == null) {
			o = weakRepository.get(key);
		}
		return o;
	}

	/**
	 * Return the <code>Image</code> object which the specified key is mapped in the
	 * ImagePool.
	 *
	 * @param key a key in the ImagePool.
	 * @return the Image object.
	 * @see ImagePool
	 */
	public Image getImageByLanguage(String key) {
		ImagePool ip = FrameworkMain.getInstance().getImagePool();
		Image image = ip.get(key);
		if (image != null) {
			return image;
		}
		int c = key.lastIndexOf('.');
		if (c < 0) {
			return ip.getImage(key);
		}
		String newKey = key.substring(0, c) + '_' + languagePostfix + key.substring(c);
		image = ip.get(newKey);
		if (image != null) {
			return image;
		}
		image = ip.getImage(key);
		if (image != null) {
			return image;
		}
		return ip.getImage(newKey);
	}

	public Image getImage(String key) {
		if (ALWAYS_FIND_LANGUAGE_RELATED_IMAGE) {
			return getImageByLanguage(key);
		}
		else {
			return FrameworkMain.getInstance().getImagePool().getImage(key);
		}
	}

	/**
	 * Removes the <code>Image</code> object which the specified key is mapped in
	 * ImagePool.
	 *
	 * @param key a key in the ImagePool.
	 */
	public void removeImage(String key) {
		ImagePool ip = FrameworkMain.getInstance().getImagePool();
		ip.remove(key);
		int c = key.lastIndexOf('.');
		if (c >= 0) {
			String base = key.substring(0, c) + '_';
			String ext = key.substring(c);
			for (int i = 0; i < Constants.LANGUAGES.length; i++) {
				ip.remove(base + Constants.LANGUAGES[i] + ext);
			}
		}
	}

	/**
	 * Removes the data to which the specified key is mapped in this DataCenter.
	 *
	 * @param key a key in the DataCenter.
	 */
	public void remove(String key) {
		Object o1 = weakRepository.remove(key);
		Object o2 = strongRepository.remove(key);
		if (o1 != null || o2 != null) {
			fireDataRemovedEvent(key);
		}
	}

	/**
	 * Sets the level of data.
	 *
	 * @param key key of data.
	 * @param level STRONG_REFERENCE or WEAK_REFERENCE.
	 */
	public void setLevel(String key, int level) {
		Object o;
		switch (level) {
			case STRONG_REFERENCE:
				o = weakRepository.get(key);
				if (o != null) {
					strongRepository.put(key, o);
				}
				break;
			case WEAK_REFERENCE:
				o = strongRepository.get(key);
				if (o != null) {
					weakRepository.put(key, o);
				}
				break;
			default:
				break;
		}
	}

	/**
	 * Sets the current language's postfix.
	 *
	 * @param id current language's postfix.
	 */
	public void setLanguagePostfix(String id) {
		this.languagePostfix = id;
	}

	/**
	 * Removes all data in the specified repository.
	 *
	 * @param level level of repository to be flushed
	 */
	public void flush(int level) {
		switch (level) {
			case STRONG_REFERENCE:
				strongRepository.clear();
				break;
			case WEAK_REFERENCE:
				weakRepository.clear();
				break;
			default:
				break;
		}
	}

	/** Called when application is destroyed. */
	public void dispose() {
		flush(STRONG_REFERENCE);
		flush(WEAK_REFERENCE);
		listeners.clear();
	}

	/**
	 * Called when some data has been removed from this DataCenter.
	 *
	 * @param key key of data.
	 */
	private void fireDataRemovedEvent(String key) {
		if (Log.DEBUG_ON) {
			Log.printDebug("DataCenter.dataRemoved : " + key);
		}
		Vector v = (Vector) listeners.get(key);
		if (v != null) {
			DataUpdateListener[] array = new DataUpdateListener[v.size()];
			v.copyInto(array);
			for (int i = 0; i < array.length; i++) {
				try {
					array[i].dataRemoved(key);
				}
				catch (Exception t) {
					Log.print(t);
				}
			}
		}
	}

	/**
	 * Called when some data in thid DataCenter has been updated.
	 *
	 * @param key key of data.
	 * @param old value of data.
	 * @param value value of data.
	 */
	private void fireDataUpdatedEvent(String key, Object old, Object value) {
		if (old == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("DataCenter.dataUpdated (added) : " + key);
			}
		}
		else {
			if (Log.DEBUG_ON) {
				Log.printDebug("DataCenter.dataUpdated (update) : " + key);
			}
		}
		Vector v = (Vector) listeners.get(key);
		if (v != null) {
			DataUpdateListener[] array = new DataUpdateListener[v.size()];
			v.copyInto(array);
			for (int i = 0; i < array.length; i++) {
				try {
					array[i].dataUpdated(key, old, value);
				}
				catch (Exception t) {
					Log.print(t);
				}
			}
		}
	}

	/**
	 * Adds a new <code>DataUpdateListener</code> for specified key.
	 *
	 * @param key key of data.
	 * @param l DataUpdateListener.
	 */
	public void addDataUpdateListener(String key, DataUpdateListener l) {
		Vector v = (Vector) listeners.get(key);
		if (v == null) {
			v = new Vector(2);
			listeners.put(key, v);
		}
		else {
			v.removeElement(l);
		}
		v.addElement(l);
	}

	/**
	 * Removes a <code>DataUpdateListener</code> for specified key.
	 *
	 * @param key key of data.
	 * @param l DataUpdateListener.
	 */
	public void removeDataUpdateListener(String key, DataUpdateListener l) {
		Vector v = (Vector) listeners.get(key);
		if (v != null) {
			v.removeElement(l);
			if (v.size() == 0) {
				listeners.remove(key);
			}
		}
	}

	public Object waitForData(String key) {
		return waitForData(key, 0);
	}

	public Object waitForData(String key, long timeout) {
		DataUpdateChecker checker = new DataUpdateChecker();
		synchronized (checker) {
			synchronized (this) {
				Object value = get(key);
				if (value != null) {
					return value;
				}
				addDataUpdateListener(key, checker);
			}
			try {
				if (timeout > 0) {
					checker.wait(timeout);
				}
				else {
					checker.wait();
				}
			}
			catch (InterruptedException ex) {
				Log.print(ex);
			}
		}
		removeDataUpdateListener(key, checker);
		return checker.data;
	}

	class DataUpdateChecker implements DataUpdateListener {
		Object	data;

		public synchronized void dataUpdated(String key, Object old, Object value) {
			data = value;
			DataUpdateChecker.this.notifyAll();
		}

		public void dataRemoved(String key) {
		}
	}

	public String toString() {
		return "DataCenter [strongRepository=" + strongRepository.toString() + ", weakRepository="
				+ weakRepository.toString() + ", languagePostfix=" + languagePostfix + "]";
	}

}

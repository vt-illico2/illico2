package com.videotron.tvi.illico.ui;

import java.awt.Font;
import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.TextUtil;

public class ScrollTexts extends UIComponent implements Scrollable {
    private static final long serialVersionUID = 8197519571951L;

    private DataCenter dataCenter = DataCenter.getInstance();

    private FontMetrics fontMetrics;
    private int rowCount = 3;
    private int rowHeight = 20;

//    private int currentPage = 0;
//    private int totalPage = 1;

    private int lines = 0;
    private int offset = 0;

    private String[] texts;

    private Image topMaskImage;
    private Point topMaskImagePosition;
    private Image bottomMaskImage;
    private Point bottomMaskImagePosition;

    private Insets insets = new Insets(0, 0, 0, 0);
    private boolean buttonIcon;
    private boolean centerAlign;

    private boolean scrollable;

    public ScrollTexts() {
        this(true);
    }

    public ScrollTexts(boolean scrollable) {
        this.scrollable = scrollable;
        setRenderer(new ScrollTextsRenderer());
        fontMetrics = FontResource.getFontMetrics(getFont());
    }

    public void setFont(Font f) {
        super.setFont(f);
        fontMetrics = FontResource.getFontMetrics(f);
    }

    public void setTopMaskImage(Image image) {
        topMaskImage = image;
    }

    public void setTopMaskImagePosition(int x, int y) {
        topMaskImagePosition = new Point(x, y);
    }

    public void setBottomMaskImage(Image image) {
        bottomMaskImage = image;
    }

    public void setBottomMaskImagePosition(int x, int y) {
        bottomMaskImagePosition = new Point(x, y);
    }

    public void setInsets(int top, int left, int bottom, int right) {
        insets = new Insets(top, left, bottom, right);
    }

    public void setButtonIcon(boolean show) {
        buttonIcon = show;
    }
    
    public void setCenterAlign(boolean align) {
    	centerAlign = align;
    }

    /** Sets the number of rows for this ScrollTexts. */
    public void setRows(int rows) {
        rowCount = rows;
    }

    public void setRowHeight(int height) {
        rowHeight = height;
    }

    public void showFirstPage() {
        if (offset > 0) {
            offset = 0;
            repaint();
        }
    }

    /** return true if it reaches last page */
    public boolean showNextPage() {
        if (offset + rowCount < lines) {
            offset++;
            repaint();
        }
        return offset + rowCount >= lines;
    }

    /** return true if it reaches first page */
    public boolean showPreviousPage() {
        if (offset > 0) {
            offset--;
            repaint();
        }
        return offset <= 0;
    }

    public int getRowCount() {
        return rowCount;
    }

    public int getPageCount() {
        return Math.max(0, lines - rowCount) + 1;
    }

    public int getLineCount() {
        return lines;
    }

    public boolean hasMultiplePages() {
        return getPageCount() > 1;
    }

    //->Kenneth[2015.6.11] R5 
    private String header = null;
    public void setHeader(String header) {
        this.header = header;
    }

    public void setContents(String str) {
        //->Kenneth[2015.6.11] R5 : setContents 가 불리우면 header 를 지움 
        // 쓸데 없는 header 가 남아 있는 것을 방지하기 위함
        header = null;

        if (str == null) {
            str = " ";
        }
        if (scrollable) {
            texts = TextUtil.split(str, fontMetrics, getWidth() - insets.right - insets.left - 18);
        } else {
            texts = TextUtil.split(str, fontMetrics, getWidth() - insets.right - insets.left - 18, rowCount);
        }
        lines = texts.length;
        offset = 0;
//        totalPage = ((texts.length - 1) / rowCount) + 1;
//        currentPage = 0;
    }

    private class ScrollTextsRenderer extends Renderer {
        private int BAR_HEIGHT = 28;

        private Image[] iBackground = {
            dataCenter.getImage("scrbg_up.png"),
            dataCenter.getImage("scr_m.png"),
            dataCenter.getImage("scrbg_dn.png")
        };

        private Image iBar = dataCenter.getImage("scr_bar.png");

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        public void prepare(UIComponent c) {
        }

        public void paint(Graphics g, UIComponent c) {
            String[] texts = ScrollTexts.this.texts;
            if (texts == null) {
                return;
            }
            g.setColor(getForeground());
            Font font = getFont();
            g.setFont(font);
            int index = offset;
            int x = 1 + insets.left;
            int y = font.getSize() + insets.top;

            if (buttonIcon) {
            	Image imgD = (Image) ((Hashtable) SharedMemory.getInstance().get(PreferenceService.FOOTER_IMG)).get(PreferenceService.BTN_D);
                int width;
                for (int i = 0; i < rowCount && index < texts.length; i++) {
                        int idxD = texts[index].indexOf("[D]");
                        if (idxD != -1) {
                                String line = texts[index].substring(0, idxD);
                                g.drawString(line, x, y);
                                width = g.getFontMetrics().stringWidth(line);
                                g.drawImage(imgD, x + width, y - 16, c);
                                width += imgD.getWidth(c);
                                g.drawString(texts[index].substring(idxD+3), x + width, y);
                        } else {
                                g.drawString(texts[index], x, y);
                        }
                        index++;
                        y = y + rowHeight + 5;
                    }
            } else {
            	
            		if (centerAlign) {
            			
            			if (texts.length <= rowCount) {
                            //->Kenneth [2014.10.7] : 계산식이 잘못 되어서 수정함
            				//x = (c.getWidth() - insets.left - insets.right) / 2;
            				x = (c.getWidth() - insets.left - insets.right) / 2 + insets.left;
            				y = y + c.getHeight() / 2 - (rowHeight / 2) * texts.length;
            			} else {
            				//x = (c.getWidth() - insets.left - insets.right - 18) / 2;
            				x = (c.getWidth() - insets.left - insets.right - 18) / 2 + insets.left;
            			}
            		}
            	
                    for (int i = 0; i < rowCount && index < texts.length; i++) {
                    	
                    	if (centerAlign) {
                    		GraphicUtil.drawStringCenter(g, texts[index], x, y);
                    	} else {
                            g.setColor(getForeground());
                    		g.drawString(texts[index], 1 + insets.left, y);
                            //->Kenneth[2015.6.11] R5 : header 의 구현은 범용적으로 만들지 않음.
                            // D 아이콘 없고 center 정렬 아닌 여기에서만 header 구현
                            // 색상도 white 를 사용
                            if (header != null && index == 0) {
                                g.setColor(Color.white);
                                g.drawString(header, 1 + insets.left, y);
                            }
                    	}
                        index++;
                        y = y + rowHeight;
                    }
            }

            if (texts.length > rowCount) {
                if (bottomMaskImage != null && offset + rowCount < texts.length) {
                    int mix;
                    int miy;
                    if (bottomMaskImagePosition != null) {
                        mix = bottomMaskImagePosition.x;
                        miy = bottomMaskImagePosition.y;
                    } else {
                        mix = 0;
                        miy = c.getHeight() - bottomMaskImage.getHeight(c);
                    }
                    g.drawImage(bottomMaskImage, mix, miy, c);
                }
                if (topMaskImage != null && offset > 0) {
                    int mix;
                    int miy;
                    if (topMaskImagePosition != null) {
                        mix = topMaskImagePosition.x;
                        miy = topMaskImagePosition.y;
                    } else {
                        mix = 0;
                        miy = 0;
                    }
                    g.drawImage(topMaskImage, mix, miy, c);
                }

                int width = c.getWidth() - insets.left - insets.right;
                int height = c.getHeight() - insets.top - insets.bottom;
                x = width - 10 + insets.left;
                y = insets.top;

                g.drawImage(iBackground[0], x, y, c);
                g.drawImage(iBackground[1], x, y + 10, 7, height - 20, c);
                g.drawImage(iBackground[2], x, y + height - 10, c);

                int totalPage = texts.length - rowCount + 1;
                x = x - 3;
                //tklee
                //y = insets.top + ((height + 2 - totalPage * BAR_HEIGHT) / (totalPage - 1) + BAR_HEIGHT) * offset;
                int gap = (height + 2 - BAR_HEIGHT) / (totalPage - 1);
                int rest = (height + 2 - BAR_HEIGHT) % (totalPage - 1);
                y = insets.top + gap * offset;
                if (rest > 0) { y += rest * offset / totalPage; }
                int cal = (y + BAR_HEIGHT) - (insets.top + height + 2);
                if (cal > 0 || (cal < 0 && offset == totalPage - 1)) { y -= cal; }
                //
                g.drawImage(iBar, x, y, c);
            }
        }

    }

}

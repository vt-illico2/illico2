package com.videotron.tvi.illico.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Stack;
import java.util.EmptyStackException;

import org.ocap.ui.event.OCRcEvent;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.TextUtil;

public class ScaledUpMenu extends UIComponent {

    private static final int Y_GAP = 34;
    private static final int MAX_ROW = 5;

    private MenuItem root;
    private MenuItem current;
    private Stack menuDepthStack = new Stack();

    private MenuListener listener;
    private MenuOpenListener openListener;

    private int rowCount;
    private int focus;
    private int offset;
    private Stack focusStack = new Stack();
    private Stack offsetStack = new Stack();
    private ClickingEffect clickEffect;

    public ScaledUpMenu(MenuItem menu) {
        setRenderer(new ScaledUpMenuRenderer());
        root = menu;

        rowCount = getRowCountForSize(menu.size());
        setSize(285, 10 * Y_GAP);

        clickEffect = new ClickingEffect(this, 5);
    }

    private static int getRowCountForSize(int size) {
    	return Math.min(MAX_ROW, size);
    }

    public void start(MenuListener l) {
        listener = l;
        current = root;

        focus = 0;
        offset = 0;

        menuDepthStack.clear();
        focusStack.clear();
        offsetStack.clear();

        prepare();
        setVisible(true);
        if (openListener != null) {
            openListener.opened(root);
        }
    }

    public void setMenuOpenListener(MenuOpenListener l) {
        openListener = l;
    }

    public void stop() {
        setVisible(false);
    }

    public int getRowCount() {
        return rowCount;
    }

    public boolean handleKey(int code) {
        switch (code) {
            case OCRcEvent.VK_UP:
            	if (focus > 0) {
            		focus--;
            	} else if (offset > 0) {
            		offset--;
            	}
                repaint();
                break;
            case OCRcEvent.VK_DOWN:
            	if (focus < rowCount - 1) {
            		focus++;
            	} else if (offset < current.size() - MAX_ROW) {
            		offset++;
            	}
                repaint();
                break;
            case OCRcEvent.VK_LEFT:
                moveLeft();
                break;
            case OCRcEvent.VK_RIGHT:
                MenuItem item = current.getItemAt(offset + focus);
                if (item.hasChild()) {
                    if (openListener != null) {
                        openListener.opened(item);
                    }
                    moveRight();
                }
                break;
            case OCRcEvent.VK_ENTER:
            	clickEffect.start(16, getHeight() - 23 + ((-rowCount + focus) * Y_GAP), 251, 35);
                MenuItem item2 = current.getItemAt(offset + focus);
                if (item2.hasChild()) {
                    if (openListener != null) {
                        openListener.opened(item2);
                    }
                    moveRight();
                } else {
                    if (listener != null) {
                        listener.selected(item2);
                    }
                }
                break;
            case OCRcEvent.VK_LAST:
                if (!moveLeft()) {
                    if (listener != null) {
                        listener.canceled();
                    }
                }
                break;
            case OCRcEvent.VK_EXIT:
                if (listener != null) {
                    listener.canceled();
                }
                break;
            default:
                return false;
        }
        return true;
    }

    private boolean moveLeft() {
        if (current == root) {
            return false;
        }
        try {
            MenuItem parent = (MenuItem) menuDepthStack.pop();
            if (parent != null) {
                current = parent;
                focus = ((Integer) focusStack.pop()).intValue();
                offset = ((Integer) offsetStack.pop()).intValue();
                rowCount = getRowCountForSize(current.size());
                repaint();
                return true;
            }
        } catch (EmptyStackException ex) {
        }
        return false;
    }

    private void moveRight() {
        menuDepthStack.push(current);
        current = current.getItemAt(offset + focus);
        focusStack.push(new Integer(focus));
        offsetStack.push(new Integer(offset));
        rowCount = getRowCountForSize(current.size());
        focus = 0;
        offset = 0;
        repaint();
    }

    /** Default Renderer for ScaledUpMenu. */
    class ScaledUpMenuRenderer extends Renderer {
        private DataCenter dataCenter = DataCenter.getInstance();

//        private Image i_sortsh_t = dataCenter.getImage("sortsh_t.png");
//        private Image i_sortsh_b = dataCenter.getImage("sortsh_b.png");

        private Image i_02_ars_t = dataCenter.getImage("02_ars_t.png");
        private Image i_02_ars_b = dataCenter.getImage("02_ars_b.png");
        private Image i_02_ars_l = dataCenter.getImage("02_ars_l.png");
        private Image i_02_ars_in = dataCenter.getImage("02_ars_in.png");
        private Image i_02_ars_in_foc = dataCenter.getImage("02_ars_in_foc.png");

        private Image i_d_bread = dataCenter.getImage("d_bread.png");
        private Image i_sorttitlebg_t = dataCenter.getImage("sorttitlebg_t.png");
        private Image i_sortbg_m = dataCenter.getImage("sortbg_m.png");
        private Image i_sortbg_b = dataCenter.getImage("sortbg_b.png");
        private Image i_sort_foc = dataCenter.getImage("sort_foc.png");

        private Image i_check = dataCenter.getImage("check.png");
        private Image i_check_foc = dataCenter.getImage("check_foc.png");

        private Color cTitle = new Color(251, 217, 89);
        private Color cText = Color.white;
        private Color cFocusedText = Color.black;

        private Font fText = FontResource.BLENDER.getFont(18);
        private Font fTitle = FontResource.BLENDER.getFont(20);
        private FontMetrics fmText = FontResource.getFontMetrics(fText);
        private FontMetrics fmTitle = FontResource.getFontMetrics(fTitle);

        public void prepare(UIComponent c) {
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void paint(Graphics g, UIComponent c) {
            boolean isRoot = (current == root);

            g.translate(-getX(), -getY() + Y_GAP * (MAX_ROW - rowCount + 1));

            g.drawImage(i_sorttitlebg_t, 660, 229, c);
            g.setFont(fTitle);
//            g.setColor(cFocusedText);
            String header = dataCenter.getString(current.getKey());
            if (isRoot && header.length() == 0) {
                header = dataCenter.getString("_options");
            }
            header = TextUtil.shorten(header, g.getFontMetrics(), 220);
//            g.drawString(header, 675, 251);
            g.setColor(cTitle);
            g.drawString(header, 674, 250);

            // draw menu list
            g.setFont(fText);
            g.setColor(cText);
            MenuItem item = null;
            boolean isChecked;
            boolean hasChild;
            int stringWidth;
            for (int i = 0; i < rowCount; i++) {
            	item = current.getItemAt(offset + i);

                hasChild = item.hasChild();
                isChecked = item.isChecked();
                if (hasChild) {
                    stringWidth = 210;
                } else if (isChecked) {
                    stringWidth = 200;
                } else {
                    stringWidth = 220;
                }

            	g.drawImage(i == rowCount-1 ? i_sortbg_b : i_sortbg_m, 660, 260 + i * Y_GAP, c);
            	String s = TextUtil.shorten(dataCenter.getString(item.getKey()), fmText, stringWidth);
            	g.drawString(s, 677, 282 + i * Y_GAP);

            	if (hasChild) {
            		g.drawImage(i_02_ars_in, 891, 269 + i * Y_GAP, c);
            	} else if (isChecked) {
                    g.drawImage(i_check, 881, 269 + i * Y_GAP, c);
                }
            }

            // arrows
            if (offset > 0) {
//            	g.drawImage(i_sortsh_t, 662, 260, c);
            	g.drawImage(i_02_ars_t, 774, 214, c);
            }
            if (current.size() - MAX_ROW > offset) {
//            	g.drawImage(i_sortsh_b, 662, 230 + MAX_ROW * Y_GAP, c);
            	g.drawImage(i_02_ars_b, 774, 259 + MAX_ROW * Y_GAP, c);
            }
            if (!isRoot) {
            	g.drawImage(i_02_ars_l, 643, 214 + (rowCount + 1) * Y_GAP / 2, c);
            }

            // overwrite draw focus menu
            item = current.getItemAt(offset + focus);
            hasChild = item.hasChild();
            isChecked = item.isChecked();
            if (hasChild) {
                stringWidth = 210;
            } else if (isChecked) {
                stringWidth = 200;
            } else {
                stringWidth = 220;
            }
            g.drawImage(i_sort_foc, 659, 258 + focus * Y_GAP, c);
            g.setColor(cFocusedText);
            String s = TextUtil.shorten(dataCenter.getString(item.getKey()), fmText, stringWidth);
        	g.drawString(s, 677, 282 + focus * Y_GAP);
        	if (hasChild) {
        		g.drawImage(i_02_ars_in_foc, 891, 269 + focus * Y_GAP, c);
            } else if (isChecked) {
                g.drawImage(i_check_foc, 881, 269 + focus * Y_GAP, c);
            }
        } // end of paint()
    }

}

package com.videotron.tvi.illico.ui;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.dsmcc.DSMCCObject;
import org.dvb.ui.DVBBufferedImage;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.io.BinaryReader;
import com.videotron.tvi.illico.framework.io.URLRequestor;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedDataKeys;
import com.videotron.tvi.illico.util.SharedMemory;

/**
 * This component shows promotion banner from MS. It also cache banner, parse
 * data and handle key event. It offered by Singleton pattern because it can be
 * placed on one or more screens and it doesn't need different each other.
 */
public class PromotionBanner extends UIComponent implements
		TVTimerWentOffListener, DataUpdateListener, Runnable {

	/**
	 * Name of data.
	 */
	private static final String BANNER_KEY = "PROMOTION_BANNER_CONFIG";
	private static final String BANNER_DATA = "PROMOTION_BANNER_CONFIG.data";

	/**
	 * Default boundary.
	 */
	private static final Rectangle BOUNDS = new Rectangle(496, 31, 230, 40);

	/**
	 * Instance of itself.
	 */
	private static final PromotionBanner instance = new PromotionBanner();

	/**
	 * Images for display.
	 */
	private Image[] banner;

	/**
	 * Data for banner;
	 */
	private BannerData[] bannerData;

	/**
	 * Index for current banner.
	 */
	private int idx;

	/**
	 * Timer instance.
	 */
	private TVTimerSpec timer;

	/**
	 * Monitor IXC instance for application launching.
	 */
	private MonitorService monitorSvc;

	/**
	 * EPG IXC instance for change channel.
	 */
	private EpgService epgSvc;

	/**
	 * Instance getter.
	 *
	 * @return instance
	 */
	public static PromotionBanner getInstance() {
		return instance;
	}

	/**
	 * Hidden constructor.
	 */
	private PromotionBanner() {
		setRenderer(new PromotionBannerRenderer());
		setVisible(true);
		DataCenter.getInstance().addDataUpdateListener(BANNER_KEY, this);
		DataCenter.getInstance().addDataUpdateListener(BANNER_DATA, this);
        Object old = DataCenter.getInstance().get(BANNER_KEY);
        if (old != null) {
            dataUpdated(BANNER_KEY, null, old);
        }
	}

	/**
	 * Turn on/off rotation timer.
	 *
	 * @param turnOn
	 *            on/off
	 */
	public void setRotationTimer(boolean turnOn) {
		if (turnOn) {
			scheduleTimer();
		} else {
			if (timer != null) {
				TVTimer.getTimer().deschedule(timer);
				timer = null;
			}
		}
	}

	/**
	 * Sets Monitor IXC.
	 *
	 * @param svc
	 *            IXC instance
	 */
	public void setMonitorService(MonitorService svc) {
		monitorSvc = svc;
	}

	/**
	 * Sets EPG IXC.
	 *
	 * @param svc
	 *            IXC instance
	 */
	public void setEpgService(EpgService svc) {
		epgSvc = svc;
	}

	/**
	 * Add listener to start data monitoring.
	 */
	public void startMonitoring() {
//		DataCenter.getInstance().addDataUpdateListener(BANNER_DATA, this);
		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner, startMonitoring()");
		}

		// data is already cached
//        Object data = DataCenter.getInstance().get(BANNER_DATA);
//		if (data != null) {
//            parse((byte[]) data);
//		}
	}

	/**
	 * Remove listener to stop data monitoring.
	 */
	public void stopMonitoring() {
//		DataCenter.getInstance().removeDataUpdateListener(BANNER_DATA, this);
		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner, stopMonitoring()");
		}
		flush();
	}

	/**
	 * Clean up images.
	 */
	public synchronized void flush() {
		int cnt = 0;
		for (int i = 0; banner != null && i < banner.length; i++) {
			if (banner[i] != null) {
				banner[i].flush();
				cnt++;
			}
			banner[i] = null;
		}
		idx = 0;
		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner, image flushed = " + cnt);
		}
	}

	/**
	 * Key handler method.
	 *
	 * @return true if key event is used
	 */
	public boolean processKeyEvent() {
        int idx = this.idx;
		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner, processKeyEvent = "
					+ (bannerData == null ? "NULL"
							: (bannerData.length > idx ? bannerData[idx]
									.toString() : "IDX")));
		}
		if (bannerData == null) {
			return false;
		}
		switch (bannerData[idx].getLinkType()) {
		case 1: // no action
			return false;
		case 2: // tv channel
			if (epgSvc != null) {
				try {
					TvChannel ch = epgSvc
							.getChannel(bannerData[idx].callLetter);
					if (monitorSvc != null) {
						monitorSvc.exitToChannel(ch.getSourceId());
					}
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
			return true;
		case 3: // tv subscription
			return true;
		case 4: // PPV
			if (epgSvc != null) {
				try {
					TvChannel ch = epgSvc
							.getChannel(bannerData[idx].callLetter);
					if (monitorSvc != null) {
						monitorSvc.exitToChannel(ch.getSourceId());
					}
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
			return true;
		case 5: // itv application
			if (monitorSvc != null) {
				try {
					monitorSvc.startUnboundApplication(bannerData[idx].appName,
							new String[] { bannerData[idx].parameter });
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
			return true;
		case 6: // VOD, category
			if (monitorSvc != null) {
				try {
					monitorSvc.startUnboundApplication("VOD", new String[] {
							"banner_category", bannerData[idx].categoryID });
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
			return true;
		case 7: // VOD, content
			if (monitorSvc != null) {
				try {
					monitorSvc.startUnboundApplication("VOD", new String[] {
							"banner_content", bannerData[idx].contentsID });
				} catch (RemoteException e) {
					Log.printWarning(e);
				}
			}
			return true;
		}
		return false;
	}

	/**
	 * Returns current banner data.
	 *
	 * @return banner data
	 */
	public BannerData getBannerData() {
        try {
            return bannerData[idx];
        } catch (NullPointerException ex) {
            return null;
        } catch (ArrayIndexOutOfBoundsException ex) {
            return null;
        }
    }

	/**
	 * Method for retrieve banner image via HTTP on another thread.
	 */
	public void run() {
        int idx = this.idx;
        synchronized (this) {
            if (banner[idx] != null) {
                return;
            }
            banner[idx] = new DVBBufferedImage(1, 1);
        }
        String path = bannerData[idx].getUrlName()
                    + bannerData[idx].getPromoImageName(
                            DataCenter.getInstance().getString(PreferenceNames.LANGUAGE) );
        Log.printDebug("PromotionBanner, idx = " + idx + ", url = " + path);
		try {
            byte[] b = URLRequestor.getBytes(path, null);
            Image newImage = Toolkit.getDefaultToolkit().createImage(b);
            MediaTracker tracker = new MediaTracker(this);
            tracker.addImage(newImage, 0);
            try {
                tracker.waitForAll();
            } catch (InterruptedException ie) {
    			Log.print(ie);
            }
            tracker.removeImage(newImage);
            synchronized (this) {
                if (banner[idx] != null) {
                    banner[idx].flush();
                }
                banner[idx] = newImage;
            }
            Log.printDebug("PromotionBanner, image = " + newImage);
		} catch (IOException e) {
			Log.print(e);
		}
		repaint();
	}

	/**
	 * Banner image getter.
	 *
	 * @return banner image
	 */
	public Image getBannerImage() {
        int index = idx;
		if (banner != null && banner.length > index) {
			if (banner[index] != null) {
				return banner[index];
			}
			// to prevent EDT blocking
			new Thread(this, "PromotionBanner, idx = " + idx).start();
		}
		return null;
	}

	/**
	 * Banner image will be changed when timer event is occur.
	 */
	public void timerWentOff(TVTimerWentOffEvent evt) {
		if (bannerData == null) {
			return;
		}
		idx = (idx + 1) % bannerData.length;
		repaint();

		scheduleTimer();
	}

	/**
	 * Schedule the timer for banner changing
	 */
	private void scheduleTimer() {
		if (bannerData == null || bannerData.length <= 1
				|| idx >= bannerData.length) {
			return;
		}
		setRotationTimer(false);
		timer = new TVTimerSpec();
		timer.setDelayTime(bannerData[idx].duration * Constants.MS_PER_SECOND);
		timer.addTVTimerWentOffListener(this);
		try {
			TVTimer.getTimer().scheduleTimerSpec(timer);
			if (Log.DEBUG_ON) {
				Log.printDebug("PromotionBanner, setup timer, delay = "
								+ bannerData[idx].duration);
			}
		} catch (TVTimerScheduleFailedException e) {

		}
	}

	/**
	 * Method for data removed.
	 *
	 * @param key
	 *            name of data
	 */
	public void dataRemoved(String key) {
	}


	/**
	 * Method for data updated. It will parse the data.
	 *
	 * @param key
	 *            name of data
	 * @param old
	 *            old data
	 * @param value
	 *            new data
	 */
	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner.dataUpdated: key = " + key + ", value = " + value);
        }
        if (BANNER_KEY.equals(key) && value instanceof DSMCCObject) {
        	DSMCCObject file = (DSMCCObject) value;
        	if (file.isLoaded() == false) {
        		Log.printDebug("PromotionBanner.dataUpdated: not loaded, " + file);
        		try {
					file.synchronousLoad();
					Log.printDebug("PromotionBanner.dataUpdated: loading done");
				} catch (Exception e) {
					Log.printWarning(e);
				}
        	}
            byte[] data = BinaryReader.read((File) value);
            if (Log.DEBUG_ON) {
                Log.printDebug("PromotionBanner.dataUpdated: lenth = " + (data == null ? -1 : data.length));
            }
            if (data != null && data.length > 0) {
                DataCenter.getInstance().put(BANNER_DATA, data);
            }
        } else if (BANNER_DATA.equals(key)) {
            parse((byte[]) value);
        }
    }

    private void parse(byte[] bytes) {
        int index = 0;
		BannerData[] newData = null;
		StringBuffer detailInfo = new StringBuffer();

        try {
            byte version = bytes[index++];
            detailInfo.append("version:").append(version).append("\n");

            int len = bytes[index++];
            String urlName = new String(bytes, index, len);
            detailInfo.append("urlName:").append(urlName).append("\n");
            index += len;

            int langTypeSize = ((int) bytes[index++]) & 0xFF;
            detailInfo.append("langTypeSize:").append(langTypeSize).append("\n");

            int promoListSize = ((int) bytes[index++]) & 0xFF;
            detailInfo.append("promoListSize:").append(promoListSize).append("\n");

            newData = new BannerData[promoListSize];
            for (int i = 0; i < promoListSize; i++) {
                newData[i] = new BannerData();
                newData[i].setUrlName(urlName);

                newData[i].setPromotionID((((int) bytes[index++]) & 0xFF) << 8 + (((int) bytes[index++]) & 0xFF));

                newData[i].setStatus((char) (((int) bytes[index++]) & 0xFF));
                newData[i].setDuration(((int) bytes[index++]) & 0xFF);
                for (int j = 0; j < langTypeSize; j++) {
                    newData[i].addLanguageType(new String(bytes, index, 2));
                    index += 2;
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].addPromoImageName(new String(bytes, index, len));
                    index += len;
                } // end of for (langTypeSize)
                newData[i].setLinkType(((int) bytes[index++]) & 0xFF);
                switch (newData[i].getLinkType()) {
                case 2:
                case 4:
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setCallLetter(new String(bytes, index, len));
                    index += len;
                    break;
                case 3:
                    newData[i].setSubscriptionType(((int) bytes[index++]) & 0xFF);
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setCallLetter(new String(bytes, index, len));
                    index += len;
                    break;
                case 5:
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setAppName(new String(bytes, index, len));
                    index += len;
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setParameter(new String(bytes, index, len));
                    index += len;
                    break;
                case 6:
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setCategoryID(new String(bytes, index, len));
                    index += len;
                    break;
                case 7:
                    len = ((int) bytes[index++]) & 0xFF;
                    newData[i].setContentsID(new String(bytes, index, len));
                    index += len;
                    break;
                } // end of switch (linkType)
            } // end of for (promoListSize)
            if (index < bytes.length) {
                len = ((int) bytes[index++]) & 0xFF;
                String str = new String(bytes, index, len);
                detailInfo.append("default:").append(str).append("\n");
                for (int i = 0; i < newData.length; i++) {
                    newData[i].setDefaultHolderImgName(str);
                }
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            Log.print(ex);
        }

		if (Log.DEBUG_ON) {
			Log.printDebug("PromotionBanner, " + detailInfo.toString());
		}
		if (newData != null) { // parsing OK.
			if (Log.DEBUG_ON) {
				for (int i = 0; i < newData.length; i++) {
					Log.printDebug("PromotionBanner[" + i + "/"
							+ newData.length + "] = " + newData[i]);
				}
			}
			bannerData = newData;
			flush();
			banner = new Image[bannerData.length];
		} else { // parsing fail.
			Log.printWarning("PromotionBanner parsing fail.");
		}
	}

	/**
	 * Simple structure class for banner data.
	 */
	public class BannerData {
		private String urlName;
		private int promotionID;
		private char status;
		private int duration;
		private String[] languageType = new String[0];
		private String[] promoImageName = new String[0];
		private int linkType;
		private String callLetter;
		private int subscriptionType;
		private String appName;
		private String parameter;
		private String categoryID;
		private String contentsID;
		private String defaultHolderImgName;

		public String toString() {
			String img = urlName + ";";
			if (languageType != null && languageType.length > 0) {
				for (int i = 0; i < languageType.length; i++) {
					img += languageType[i] + "," + promoImageName[i] + ";";
				}
			} else {
				img = "NULL";
			}
			String link = "";
			switch (linkType) {
			case 2:
			case 4:
				link = callLetter;
				break;
			case 3:
				link = subscriptionType + "-" + callLetter;
				break;
			case 5:
				link = appName + "-" + parameter;
				break;
			case 6:
				link = categoryID;
				break;
			case 7:
				link = contentsID;
				break;
			default:
				link = "UNKNOWN";
			}
			return promotionID + "|" + status + "|" + duration + "|" + img
					+ "|" + linkType + "|" + link + "|" + defaultHolderImgName;
		}

		public String getUrlName() {
			return urlName;
		}

		public void setUrlName(String urlName) {
			this.urlName = urlName;
//            String url = (String) SharedMemory.getInstance().get(SharedDataKeys.MS_URL);
//            if (url == null) {
            if (!urlName.startsWith("http://")) {
                return;
            }
            try {
                String url = urlName.substring(0, urlName.indexOf('/', 7));
                if (Log.DEBUG_ON) {
                    Log.printDebug("PromotionBanner: MS_URL = " + url);
                }
                SharedMemory.getInstance().put(SharedDataKeys.MS_URL, url);
            } catch (Exception ex) {
                Log.print(ex);
            }
//            }
		}

		public int getPromotionID() {
			return promotionID;
		}

		public void setPromotionID(int promotionID) {
			this.promotionID = promotionID;
		}

		public char getStatus() {
			return status;
		}

		public void setStatus(char status) {
			this.status = status;
		}

		public int getDuration() {
			return duration;
		}

		public void setDuration(int duration) {
			this.duration = duration;
		}

		public String[] getLanguageType() {
			return languageType;
		}

		public void addLanguageType(String str) {
			int size = languageType.length + 1;
			String[] temp = new String[size];
			if (languageType != null && languageType.length > 0) {
				System.arraycopy(languageType, 0, temp, 0, languageType.length);
			}
			temp[size - 1] = str;
			languageType = temp;
		}

		public String getPromoImageName(String lang) {
			// compare 2 chars (ex: Fr, En)
			lang = lang.substring(0, 2);
			for (int i = 0; languageType != null && i < languageType.length; i++) {
				if (languageType[i].equalsIgnoreCase(lang)) {
					return promoImageName[i];
				}
			}
			return "";
		}

		public void addPromoImageName(String str) {
			int size = promoImageName.length + 1;
			String[] temp = new String[size];
			if (promoImageName != null && promoImageName.length > 0) {
				System.arraycopy(promoImageName, 0, temp, 0,
						promoImageName.length);
			}
			temp[size - 1] = str;
			promoImageName = temp;
		}

		public int getLinkType() {
			return linkType;
		}

		public void setLinkType(int linkType) {
			this.linkType = linkType;
		}

		public String getCallLetter() {
			return callLetter;
		}

		public void setCallLetter(String callLetter) {
			this.callLetter = callLetter;
		}

		public int getSubscriptionType() {
			return subscriptionType;
		}

		public void setSubscriptionType(int subscriptionType) {
			this.subscriptionType = subscriptionType;
		}

		public String getAppName() {
			return appName;
		}

		public void setAppName(String appName) {
			this.appName = appName;
		}

		public String getParameter() {
			return parameter;
		}

		public void setParameter(String parameter) {
			this.parameter = parameter;
		}

		public String getCategoryID() {
			return categoryID;
		}

		public void setCategoryID(String categoryID) {
			this.categoryID = categoryID;
		}

		public String getContentsID() {
			return contentsID;
		}

		public void setContentsID(String contentsID) {
			this.contentsID = contentsID;
		}

		public String getDefaultHolderImgName() {
			return defaultHolderImgName;
		}

		public void setDefaultHolderImgName(String defaultHolderImgName) {
			this.defaultHolderImgName = defaultHolderImgName;
		}
	}

	/**
	 * Renderer class, it draws banner image only.
	 */
	private class PromotionBannerRenderer extends Renderer {

		public Rectangle getPreferredBounds(UIComponent c) {
			return BOUNDS;
		}

		protected void paint(Graphics g, UIComponent c) {
			PromotionBanner ui = (PromotionBanner) c;
			Image banner = ui.getBannerImage();
			if (banner != null) {
				g.drawImage(banner, 0, 0, BOUNDS.width, BOUNDS.height, c);
			}
		}

		public void prepare(UIComponent c) {
		}
	}
}

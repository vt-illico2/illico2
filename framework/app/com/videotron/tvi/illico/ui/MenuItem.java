package com.videotron.tvi.illico.ui;

import java.util.Vector;
import java.util.Enumeration;
import java.awt.*;

/**
 * This class represents the menu item. Each item may have a child items.
 *
 * @version $Revision: 1.11.2.1 $ $Date: 2017/03/23 16:26:39 $
 * @author June Park
 */
public class MenuItem {

    /** menu key. */
    protected String key;

    /** children menu items. */
    protected Vector items = new Vector();
    /** the data that specific on menu */
    protected Object menuData;

    protected boolean checked;

    protected boolean enabled = true;

    /** Create menu item with specified key. */
    public MenuItem(String key) {
        this.key = key;
    }

    /** Create menu item with specified key and children. */
    public MenuItem(String key, Vector children) {
        this.key = key;
        Enumeration en = children.elements();
        while (en.hasMoreElements()) {
            add((MenuItem) en.nextElement());
        }
    }

    /** Create menu item with specified key and children. */
    public MenuItem(String key, MenuItem[] children) {
        this.key = key;
        for (int i = 0; i < children.length; i++) {
            add(children[i]);
        }
    }

    //->Kenneth[2017.3.15] R7.3 
    // PVR 의 Recorded List 에서 resume viewing 의 경우 MenuItem 에서 TOC 가 아닌 남은 시간을
    // toggle 로 보여줘야 한다.
    // 따라서 MenuItem 에 displayableText 를 추가한다.
    // displayableText 가 null 이 아니면 displayableText 를 그리고
    // null 인 경우 기존대로 key 를 가지고 TOC 로 그리는 코드를 OptionPanelRenderer 에 구현한다.
    // 또한 focused/unfocused 상태에 따른 icon 을 추가한다.
    private String displayableText;

    public void setDisplayableText(String displayableText) {
        this.displayableText = displayableText;
    }

    public String getDisplayableText() {
        return displayableText;
    }

    public Image focusedIcon;
    public Image unfocusedIcon;
    //<-

    /** Return the key of this menu item. */
    public String getKey() {
        return key;
    }

    /** Clear all child menu items. */
    public void clear() {
    	items.clear();
    }

    /** Adds the child menu item. */
    public void add(MenuItem item) {
        if (item != null) {
            items.addElement(item);
        }
    }

    public void removeItemAt(int index) {
        try {
            items.removeElementAt(index);
        } catch (ArrayIndexOutOfBoundsException ex) {
        }
    }

    /** Adds the child menu item. */
    public void setItemAt(MenuItem item, int index) {
        items.setElementAt(item, index);
    }

    /** Returns the child menu item. */
    public MenuItem getItemAt(int index) {
        return (MenuItem) items.elementAt(index);
    }

    /** Returns the index of child menu item. */
    public int indexOf(MenuItem item) {
        return items.indexOf(item);
    }

    /** Returns the size of child menu item. */
    public int size() {
        return items.size();
    }

    /** Checks this item has child or not. */
    public boolean hasChild() {
        return items.size() > 0;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean check) {
        this.checked = check;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enable) {
        this.enabled = enable;
    }

    /** Set the menu data. */
    public void setData(Object data) {
    	menuData = data;
    }

    /** Returns the menu data. */
    public Object getData() {
    	return menuData;
    }

    public MenuItem find(String key) {
        int size = items.size();
        for (int i = 0; i < size(); i++) {
            MenuItem item = (MenuItem) items.elementAt(i);
            if (item.hasChild()) {
                MenuItem fromChild = item.find(key);
                if (fromChild != null) {
                    return fromChild;
                }
            } else {
                if (item.key.equals(key)) {
                    return item;
                }
            }
        }
        return null;
    }

}

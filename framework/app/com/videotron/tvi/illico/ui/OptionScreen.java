package com.videotron.tvi.illico.ui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.dvb.ui.DVBColor;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.Renderer;
import com.videotron.tvi.illico.framework.UIComponent;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.ClickingEffect;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;


/**
 * This class represents the option screen for 'D' key.
 *
 * @version $Revision: 1.25 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public class OptionScreen extends UIComponent implements LayeredKeyHandler,
                                        MenuListener, TVTimerWentOffListener {

    private static DataCenter dataCenter = DataCenter.getInstance();

    private static final Color DEFAULT_BACKGROUND = new DVBColor(12, 12, 12, 204);
    //->Kenneth[2015.7.23] : Flat design
    private static final Color AV_BACKGROUND = new DVBColor(0, 0, 0, 166);
    private static final Color APP_BACKGROUND = new DVBColor(17, 17, 17, 166);
    //<-

    static {
        dataCenter.put("close_options_fr", "Fermer");
        dataCenter.put("close_options_en", "Close");
        dataCenter.put("_options_fr", "Options");
        dataCenter.put("_options_en", "Options");
    }

    private ScaledUpMenu menu;
    private LayeredUI ui;
    private MenuListener listener;

    private TVTimerSpec autoCloseTimer;
    private long autoCloseDelay = 0;

    private Color background = DEFAULT_BACKGROUND;

    private int cancelKeyCode;
    private String closeIconKey;
    private String closeTextKey;

    private Footer footer = new Footer();

    private ClickingEffect click = new ClickingEffect(this, 5);

    //->Kenneth[2015.8.19] DDC-113 Visually Impaired
    private boolean started = false;
    //<-

    public OptionScreen(int cancelKeyCode, String closeIconKey, String closeTextKey) {
        this.cancelKeyCode = cancelKeyCode;
        this.closeIconKey = closeIconKey;
        this.closeTextKey = closeTextKey;
        setRenderer(new OptionScreenRenderer());
        autoCloseTimer = new TVTimerSpec();
        autoCloseTimer.addTVTimerWentOffListener(this);
    }

    public OptionScreen() {
        this(KeyCodes.COLOR_D, PreferenceService.BTN_D, "close_options");
    }

    public void setBackground(Color c) {
        background = c;
    }

    public void setAutoCloseDelay(long delay) {
        autoCloseDelay = delay;
        if (delay > 0) {
            autoCloseTimer.setDelayTime(autoCloseDelay);
        }
    }

    public void start(MenuItem root, MenuListener l) {
        start(root, null, l, null);
    }

    public void start(MenuItem root, Point point, MenuListener l) {
        start(root, point, l, null);
    }

    public synchronized void start(MenuItem root, Point point, MenuListener l, MenuOpenListener ol) {
        listener = l;
        menu = new ScaledUpMenu(root);
        menu.setMenuOpenListener(ol);
        LayeredWindow window = new LayeredWindow();
        window.setBounds(Constants.SCREEN_BOUNDS);
        window.add(this);
        this.add(menu);

        footer.reset();
        footer.addButton(closeIconKey, closeTextKey);
        this.add(footer);

        menu.start(this);
        ui = WindowProperty.OPTION_MENU.createLayeredDialog(window, Constants.SCREEN_BOUNDS, this);
        if (point == null) {
            menu.setLocation(643, 146);
        } else {
            menu.setLocation(point);
        }
        prepare();
        int menuX = menu.getX();
        //->Kenneth[2015.7.23] : Flat design
        footer.setBounds(menuX-10, menu.getY() + menu.getHeight() + 2, menu.getWidth() - 10, 25);
        //footer.setBounds(menuX, menu.getY() + menu.getHeight() + 2, menu.getWidth() - 10, 25);
        //<-
        ui.activate();
        startTimer();

        this.setVisible(false);
        new AlphaEffect(this, 5, AlphaEffect.FADE_IN).start();
        //->Kenneth[2015.8.19] DDC-113 Visually Impaired
        if (Log.DEBUG_ON) Log.printDebug("OptionScreen : started = true"+this);
        started = true;
        //<-
    }

    public void animationEnded(Effect effect) {
    	this.setVisible(true);
    }

    private synchronized void startTimer() {
        if (autoCloseDelay <= 0) {
            return;
        }
        TVTimer timer = TVTimer.getTimer();
        timer.deschedule(autoCloseTimer);
        try {
            autoCloseTimer = timer.scheduleTimerSpec(autoCloseTimer);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
    }

    public synchronized void stop() {
        TVTimer.getTimer().deschedule(autoCloseTimer);
        if (ui != null) {
            ui.deactivate();
            LayeredUIManager.getInstance().disposeLayeredUI(ui);
            ui = null;
        }
        listener = null;
        removeAll();
        //->Kenneth[2015.8.19] DDC-113 Visually Impaired
        if (Log.DEBUG_ON) Log.printDebug("OptionScreen : started = false"+this);
        started = false;
        //<-
    }

    //->Kenneth[2015.8.19] DDC-113 Visually Impaired
    public boolean isStarted() {
        if (Log.DEBUG_ON) Log.printDebug("OptionScreen : isStarted returns "+started+this);
        return started;
    }
    //<-

    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        startTimer();
        if (code == cancelKeyCode) {
            footer.clickAnimation(0);
            canceled();
            return true;
        }
        return menu.handleKey(code);
    }

    public void selected(MenuItem item) {
        menu.stop();
        MenuListener l = listener;
        stop();
        if (l != null) {
            l.selected(item);
        }
    }

    public void canceled() {
        MenuListener l = listener;
        stop();
        if (l != null) {
            l.canceled();
        }
    }


    class OptionScreenRenderer extends Renderer {
        private Image iWatchBg;
        private Image iNormalBg;

        public void prepare(UIComponent c) {
            if (background == null) {
                iWatchBg = dataCenter.getImage("05_pop_rmdbg.png");
            } else {
                iNormalBg = dataCenter.getImage("02_d_shadow.png");
            }
            FrameworkMain.getInstance().getImagePool().waitForAll();
        }

        public Rectangle getPreferredBounds(UIComponent c) {
            return Constants.SCREEN_BOUNDS;
        }

        public void paint(Graphics g, UIComponent c) {
            if (background != null) {
                g.setColor(background);
                g.fillRect(0, 0, 960, 540);
                if (iNormalBg != null) {
                    //->Kenneth[2015.7.23] : Flat design
                    g.setColor(APP_BACKGROUND);
                    g.fillRect(660, 465, 251, 540-465);
                    //g.fillRect(660, 465, 960-660, 540-465);
                    //g.drawImage(iNormalBg, 568, 196, c);
                    //<-
                }
            } else {
                //->Kenneth[2015.7.23] : Flat design
                g.setColor(AV_BACKGROUND);
                g.fillRect(660, 465, 251, 540-465);
                //<-
                if (iWatchBg != null) {
                    g.drawImage(iWatchBg, 429, 82, c);
                }
            }
        }

    }

    public void timerWentOff(TVTimerWentOffEvent e) {
        canceled();
    }

}

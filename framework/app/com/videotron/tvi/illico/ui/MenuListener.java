package com.videotron.tvi.illico.ui;

/**
 * Defines a listener for menu selection.
 *
 * @version $Revision: 1.3 $ $Date: 2017/01/12 19:20:15 $
 * @author June Park
 */
public interface MenuListener {

    void selected(MenuItem item);

    void canceled();

}

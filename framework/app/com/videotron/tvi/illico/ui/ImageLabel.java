package com.videotron.tvi.illico.ui;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.Toolkit;
import java.net.MalformedURLException;
import java.net.URL;

import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.FlipEffect;

public class ImageLabel extends Component {
    public static final int NONE_EFFECT = 0;
    public static final int DISSOLVE_EFFECT = 1;
    public static final int FADE_EFFECT = 2;
    public static final int FLIP_FROM_TOP = 3;
    public static final int FLIP_FROM_BOTTOM = 4;
    public static final int FLIP_FROM_LEFT = 5;
    public static final int FLIP_FROM_RIGHT = 6;
    
    private AlphaEffect fadeOut;
    private AlphaEffect fadeIn;
    private AlphaEffect dissolve;
    private FlipEffect flipTop;
    private FlipEffect flipBottom;
    private FlipEffect flipLeft;
    private FlipEffect flipRight;
    
    protected Image image;
    protected Image highlight;
    protected String imgUrl;
    protected boolean centerAlign;

    public ImageLabel(Image im) {
        this.image = im;
        if (image != null) {
            int w = image.getWidth(this);
            int h = image.getHeight(this);
            setSize(w, h);
        }
    }
    
    // some case, it needs extra image on original image
    public void setHighlight(Image high) {
        highlight = high;
    }
    
    public void setAlign(boolean center) {
        centerAlign = center;
    }
    
    public void replaceImage(Image nextImage, int animationOption) {
        if (nextImage == null) {
            return;
        }
        switch (animationOption) {
        case FADE_EFFECT:
            if (fadeOut == null) {
                //tklee frmaecount : 5 -> 15 -> 30 -> 60
                fadeOut = new AlphaEffect(this, 60, AlphaEffect.FADE_OUT);
                fadeOut.setFrameDelay(20L);
            }
            if (fadeIn == null) {
                fadeIn = new AlphaEffect(this, 60, AlphaEffect.FADE_IN);
                fadeIn.setFrameDelay(20L);
            }
            fadeOut.start();
            image = nextImage;
            fadeIn.start();
            break;
        case DISSOLVE_EFFECT:
            if (dissolve == null) {
              //tklee frmaecount : 10 -> 20 -> 30 -> 60
                dissolve = new AlphaEffect(this, 60, AlphaEffect.DISSOLVE);
                dissolve.setFrameDelay(20L);
            }
            dissolve.setNextImage(nextImage);
            if (centerAlign) {
                dissolve.setOffset(getWidth() / 2 - nextImage.getWidth(null) / 2, 
                        getHeight() / 2 - nextImage.getHeight(null) / 2);
            } else {
                dissolve.setOffset(0, 0);
            }
            dissolve.start();
            dissolve.setNextImage(null);
            image = nextImage;
            break;
        case FLIP_FROM_TOP:
            if (flipTop == null) {
                flipTop = new FlipEffect(this, 50, FlipEffect.FLIP_FROM_TOP);
                flipTop.setFrameDelay(20L);
            }
            flipTop.setNextImage(nextImage);
            if (centerAlign) {
                flipTop.setOffset(getWidth() / 2 - nextImage.getWidth(null) / 2, 
                        getHeight() / 2 - nextImage.getHeight(null) / 2);
            } else {
                flipTop.setOffset(0, 0);
            }
            flipTop.start();
            flipTop.setNextImage(null);
            image = nextImage;
            break;
        case FLIP_FROM_BOTTOM:
            if (flipBottom == null) {
                flipBottom = new FlipEffect(this, 50, FlipEffect.FLIP_FROM_BOTTOM);
                flipBottom.setFrameDelay(20L);
            }
            flipBottom.setNextImage(nextImage);
            if (centerAlign) {
                flipBottom.setOffset(getWidth() / 2 - nextImage.getWidth(null) / 2, 
                        getHeight() / 2 - nextImage.getHeight(null) / 2);
            } else {
                flipBottom.setOffset(0, 0);
            }
            flipBottom.start();
            flipBottom.setNextImage(null);
            image = nextImage;
            break;
        case FLIP_FROM_LEFT:
            if (flipLeft == null) {
                flipLeft = new FlipEffect(this, 60, FlipEffect.FLIP_FROM_LEFT);
                flipLeft.setFrameDelay(20L);
            }
            flipLeft.setNextImage(nextImage);
            if (centerAlign) {
                flipLeft.setOffset(getWidth() / 2 - nextImage.getWidth(null) / 2, 
                        getHeight() / 2 - nextImage.getHeight(null) / 2);
            } else {
                flipLeft.setOffset(0, 0);
            }
            flipLeft.start();
            flipLeft.setNextImage(null);
            image = nextImage;
            break;
        case FLIP_FROM_RIGHT:
            if (flipRight == null) {
                flipRight = new FlipEffect(this, 60, FlipEffect.FLIP_FROM_RIGHT);
                flipRight.setFrameDelay(20L);
            }
            flipRight.setNextImage(nextImage);
            if (centerAlign) {
                flipRight.setOffset(getWidth() / 2 - nextImage.getWidth(null) / 2, 
                        getHeight() / 2 - nextImage.getHeight(null) / 2);
            } else {
                flipRight.setOffset(0, 0);
            }
            flipRight.start();
            flipRight.setNextImage(null);
            image = nextImage;
            break;
        default:
            image = nextImage;
            break;
        }
    }
    
    public Image getImage() {
        return image;
    }
    
    public void paint(Graphics g) {
        if (image != null) {
            if (centerAlign) {
                g.drawImage(image, getWidth() / 2 - image.getWidth(null) / 2, 
                        getHeight() / 2 - image.getHeight(null) / 2, this);
            } else {
                g.drawImage(image, 0, 0, this);
            }
        }
        if (highlight != null) {
            int w = Math.min(getWidth(), highlight.getWidth(this));
            int h = Math.min(getHeight(), highlight.getHeight(this));
            g.drawImage(highlight, 0, 0, w, h, 0, 0, w, h, this);
        }
    }

    synchronized public void flush() {
        if (image != null) {
            image.flush();
            image = null;
        }
        imgUrl = null;
    }
    
    Thread imgLoading;
    
    public void setImage(final String url) {
        if (url != null && url.equals(imgUrl)) {
            return;
        }
        flush();
        final Component thisComp = this;
        if (imgLoading != null) {
            imgLoading.interrupt();
        }
        imgLoading = new Thread("ImageLabel, " + url) {
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e2) {
                    return;
                }
                Image tmp = null;
                try {
                    tmp = Toolkit.getDefaultToolkit().createImage(new URL(url));
                } catch (MalformedURLException e1) {
                    return;
                }
                MediaTracker tracker = new MediaTracker(thisComp);
                tracker.addImage(tmp, 99);
                try {
                    tracker.waitForID(99);
                    flush();
                    image = tmp;
                    imgUrl = url;
                    repaint();
                } catch (InterruptedException e) {
                    flush();
                    return;
                } finally {
                    tracker.removeImage(tmp);
                    imgLoading = null;
                }
            }
        };
        
        imgLoading.start();
    }
}


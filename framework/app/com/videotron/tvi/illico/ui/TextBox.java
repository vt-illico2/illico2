package com.videotron.tvi.illico.ui;

import com.videotron.tvi.illico.framework.*;
import com.videotron.tvi.illico.util.*;
import java.awt.*;
import java.util.Vector;
import java.util.Hashtable;

public class TextBox extends UIComponent {

    /** The key name of Hashtable for Footer images in SharedMemory. */
    private static final String FOOTER_IMG = "Footer Img";

    public static final short ALIGN_CENTER = 0;
    public static final short ALIGN_LEFT   = 1;
    public static final short ALIGN_RIGHT  = 2;

    private short align = ALIGN_LEFT;

    private DataCenter dataCenter = DataCenter.getInstance();

    private FontMetrics fontMetrics;
    private int rowHeight = 20;

//    private String originalData;
    private RenderingObject[][] objects;

    public TextBox(short align) {
        setRenderer(new TextBoxRenderer());
        this.align = align;
        fontMetrics = FontResource.getFontMetrics(getFont());
    }

    public void setContents(String data) {
        String[] lines = TextUtil.tokenize(data, "|");
        RenderingObject[][] array = new RenderingObject[lines.length][];
        for (int i = 0; i < lines.length; i++) {
            array[i] = convert(lines[i]);
        }
        resetPositions(array);
        objects = array;
    }

    private RenderingObject[] convert(String line) {
        Vector list = new Vector();
        int len = line.length();
        int l = 0;
        int r;
        while (l < len && (r = line.indexOf('<', l)) >= 0) {
            if (l < r) {
                list.add(new TextRenderingObject(line.substring(l, r)));
            }
            int pos = line.indexOf("\"/>", r);
            if (pos < 0) {
                break;
            }
            if (line.startsWith("button name=\"", r + 1)) {
                Hashtable table = (Hashtable) SharedMemory.getInstance().get(FOOTER_IMG);
                if (table != null) {
                    String name = line.substring(r + 14, pos);
                    Image im = (Image) table.get(name);
                    if (im != null) {
                        list.addElement(new ImageRenderingObject(im));
                    }
                }
            } else if (line.startsWith("image name=\"", r + 1)) {
                String name = line.substring(r + 13, pos);
                Image im = FrameworkMain.getInstance().getImagePool().getImage(name);
                if (im != null) {
                    list.addElement(new ImageRenderingObject(im));
                }
            }
            l = pos + 3;
        }
        if (l < len) {
            list.add(new TextRenderingObject(line.substring(l)));
        }
        RenderingObject[] ret = new RenderingObject[list.size()];
        list.copyInto(ret);
        return ret;
    }

    public void setFont(Font f) {
        super.setFont(f);
        fontMetrics = FontResource.getFontMetrics(f);
        resetPositions(objects);
    }

    private void resetPositions(RenderingObject[][] array) {
        if (array == null) {
            return;
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
        Font f = getFont();
        int fontHeight = fontMetrics.getHeight();
        int maxAscent = fontMetrics.getMaxAscent();
        int maxDescent = fontMetrics.getMaxDescent();

        for (int i = 0; i < array.length; i++) {
            int width = 0;
            for (int j = 0; j < array[i].length; j++) {
                array[i][j].setFontMetrics(fontHeight, maxAscent, maxDescent);
                array[i][j].x = width;
                width += array[i][j].getWidth();
            }

            int offset;
            if (align == ALIGN_RIGHT) {
                offset = -width;
            } else if (align == ALIGN_CENTER) {
                offset = -width / 2;
            } else {
                continue;
            }
            for (int j = 0; j < array[i].length; j++) {
                array[i][j].x += offset;
            }
        }
    }

    public void setRowHeight(int height) {
        rowHeight = height;
    }

    private class TextBoxRenderer extends Renderer {

        public Rectangle getPreferredBounds(UIComponent c) {
            return null;
        }

        public void prepare(UIComponent c) {
        }

        public void paint(Graphics g, UIComponent c) {
            RenderingObject[][] array = objects;
            if (array == null) {
                return;
            }
            int offset = 0;
            if (align == ALIGN_RIGHT) {
                offset = getWidth();
            } else if (align == ALIGN_CENTER) {
                offset = getWidth() / 2;
            }
            g.translate(offset, 0);
            g.setColor(getForeground());
            g.setFont(getFont());
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    array[i][j].render(g);
                }
                g.translate(0, rowHeight);
            }
            g.translate(-offset, -rowHeight * array.length);
        }
    }

    abstract class RenderingObject {
        int x, y;
        public abstract void setFontMetrics(int height, int ascent, int descent);
        public abstract int getWidth();
        public abstract void render(Graphics g);
    }

    class TextRenderingObject extends RenderingObject {
        String text;

        public TextRenderingObject(String text) {
            this.text = text;
        }

        public void setFontMetrics(int height, int ascent, int descent) {
            y = ascent + descent;
        }

        public int getWidth() {
            return fontMetrics.stringWidth(text);
        }

        public void render(Graphics g) {
            g.drawString(text, x, y);
        }
    }

    class ImageRenderingObject extends RenderingObject {
        Image image;

        public ImageRenderingObject(Image image) {
            this.image = image;
        }

        public void setFontMetrics(int height, int ascent, int descent) {
            y = (ascent + descent - image.getHeight(TextBox.this)) / 2 + descent;
        }

        public int getWidth() {
            return image.getWidth(TextBox.this);
        }

        public void render(Graphics g) {
            g.drawImage(image, x, y, TextBox.this);
        }
    }


}

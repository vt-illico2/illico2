package com.videotron.tvi.illico.ui;

public interface Scrollable {

    boolean hasMultiplePages();

}

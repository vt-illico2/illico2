/*
 * $RCSfile: Log.java,v $ $Revision: 1.28.2.1 $ $Date: 2017/03/30 16:03:04 $
 *
 * Copyright 1999-2002 by Alticast Corp. All rights reserved.
 */
package com.videotron.tvi.illico.log;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.GeneralPreferenceUtil;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.Formatter;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import org.dvb.user.GeneralPreference;
import org.dvb.user.UserPreferenceManager;
import org.dvb.user.UserPreferenceChangeListener;
import org.dvb.user.UserPreferenceChangeEvent;
import java.util.Iterator;
import java.util.Collection;
import java.util.Date;
import java.util.Vector;
import java.text.SimpleDateFormat;
import java.io.*;
import org.ocap.ui.event.OCRcEvent;
import org.ocap.storage.*;
import org.ocap.dvr.storage.*;
import java.lang.reflect.*;

public class Log {
    public static final SimpleDateFormat DEBUG_DATE_FORMAT = new SimpleDateFormat("MM/dd HH:mm:ss");

    public static final String LAST_ERROR_LOG = "last_error_log";
    /** Using unused GeneralPreference as a console log. */
    public static final String UP_NAME_CONSOLE_LEVEL = "Default Font Size";

    public static final String CONSOLE_LOG_LEVEL_DATA_KEY = "Log.console_level";

    public static final int LEVEL_OFF       = 0;
    public static final int LEVEL_ERROR     = 3;
    public static final int LEVEL_WARNING   = 4;
    public static final int LEVEL_INFO      = 6;
    public static final int LEVEL_DEBUG     = 7;
    public static final int LEVEL_EXTRA     = 8;
    public static final int LEVEL_ALL       = 9;    // include CANH

    /** Level marker. This is compatible with Alticast M/W. */
    public static final char[] LEVEL_MARKER = { ' ', '!', '!', '!', '?', '@', '@', '#', '*', '*' };

    protected static String prefix = "";

    protected static ApplicationConfig config;
    protected static String userName;

    protected static int logLevel;

    protected static int stcLevel;
    protected static int consoleLevel;

    // Note that these fields are not constant.
    // But use capital because it is easier to read.
    public static boolean ERROR_ON;
    public static boolean WARNING_ON;
    public static boolean INFO_ON;
    public static boolean DEBUG_ON;
    public static boolean EXTRA_ON;
    public static boolean ALL_ON;

    private static Vector logCache;

    //->Kenneth[2016.5.27] : Log Capture
    public static String LOG_CAPTURE_APP_NAME = "LOG_CAPTURE_APP_NAME";
    //<-

    //->Kenneth[2017.3.29] : VDTRMASTER-5703 이슈 해결을 위해 FDR 로그 print 추가
	private static Object logObj;
	private static Method method;
    //<-

    static {
        consoleLevel = LEVEL_DEBUG;
        setStcLevel(LEVEL_OFF);
        readConsoleLevel();
        UserPreferenceManager.getInstance().addUserPreferenceChangeListener(
                                        new ConsoleLevelChangedListener() );

        DataCenter.getInstance().addDataUpdateListener("LogCache", new LogCacheUpdater());
        //->Kenneth[2016.5.27] : Log Capture
        DataCenter.getInstance().addDataUpdateListener(LOG_CAPTURE_APP_NAME, new AppNameUpdater());
        //<-

        //->Kenneth[2017.3.29] : VDTRMASTER-5703 이슈 해결을 위해 FDR 로그 print 추가
        String className = "com.alticast.debug.Log";
        String methodName = "printDebug";
        Class clazz;
        try {
            clazz = Class.forName(className);
            logObj = clazz.newInstance();
            Class[] types = new Class[1];
            types[0] = String.class;
            method = clazz.getMethod(methodName, types);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //<-
    }

    //->Kenneth[2017.3.29] : VDTRMASTER-5703 이슈 해결을 위해 FDR 로그 print 추가
	public static void printFDR(String str) {
		if (method != null) {
	    	try {
				method.invoke(logObj, new String[] {"[APP_FDR] "+str});
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
    //<-

    public static void setApplication(ApplicationConfig c) {
        config = c;
        resetPrefix();
    }

    public static void setUserName(String s) {
        userName = s;
        resetPrefix();
    }

    private static void resetPrefix() {
        prefix = "<" + config.getApplicationName()
               + " " + config.getVersion() + "> ";
    }

    public static String getPrefix() {
        return prefix;
    }

    public static int getStcLevel() {
        return stcLevel;
    }

    public static int getConsoleLevel() {
        return consoleLevel;
    }

    public static void setStcLevel(int newLevel) {
        stcLevel = newLevel;
        setLevel(Math.max(consoleLevel, stcLevel));
    }

    public static void setConsoleLevel(int newLevel) {
        consoleLevel = newLevel;
        setLevel(Math.max(consoleLevel, stcLevel));
        DataCenter.getInstance().put(CONSOLE_LOG_LEVEL_DATA_KEY, new Integer(newLevel));
    }

    protected static void setLevel(int newLevel) {
        logLevel = newLevel;
        EXTRA_ON = (logLevel >= LEVEL_EXTRA);
        DEBUG_ON = (logLevel >= LEVEL_DEBUG);
        INFO_ON = (logLevel >= LEVEL_INFO);
        WARNING_ON = (logLevel >= LEVEL_WARNING);
        ERROR_ON = (logLevel >= LEVEL_ERROR);
        ALL_ON  = (logLevel >= LEVEL_ALL);
    }

    public static String toLevelString(int level) {
        if (level >= LEVEL_ALL) {
            return "ALL";
        } else if (level >= LEVEL_EXTRA) {
            return "EXTRA";
        } else if (level >= LEVEL_DEBUG) {
            return "DEBUG";
        } else if (level >= LEVEL_INFO) {
            return "INFO";
        } else if (level >= LEVEL_WARNING) {
            return "WARNING";
        } else if (level >= LEVEL_ERROR) {
            return "ERROR";
        } else {
            return "OFF";
        }
    }

    public static void print(Throwable t) {
        if (t instanceof Error) {
            printLine(LEVEL_ERROR, "caught error : " + t);
        } else {
            printLine(LEVEL_WARNING, "caught exception : " + t);
        }
        if (DEBUG_ON) {
            t.printStackTrace();
        }
    }

    public static void printDebug(Object o) {
        print(LEVEL_DEBUG, o);
    }

    public static void printInfo(Object o) {
        print(LEVEL_INFO, o);
    }

    public static void printWarning(Object o) {
        print(LEVEL_WARNING, o);
    }

    public static void printError(Object o) {
        print(LEVEL_ERROR, o);

        if (o == null || config == null) {
            return;
        }
        Object[] newStatus = new Object[4];
        Object[] oldStatus = (Object[]) SharedMemory.getInstance().get(LAST_ERROR_LOG);
        newStatus[0] = "Error Log from " + config.getApplicationName();
        if (oldStatus == null) {
            newStatus[1] = new Integer(1);
        } else {
            newStatus[1] = new Integer(((Integer) oldStatus[1]).intValue() + 1);
        }
        newStatus[2] = DEBUG_DATE_FORMAT.format(new Date());
        newStatus[3] = o.toString();
        SharedMemory.getInstance().put(LAST_ERROR_LOG, newStatus);
    }

    public static void print(int level, Object o) {
        if (logLevel >= level) {
            printLine(level, o);
        }
    }

    public static void print(int level, Collection c) {
        if (logLevel >= level) {
            printLine(level, "collection size = " + c.size());
            int index = 0;
            Iterator it = c.iterator();
            while (it.hasNext()) {
                printLine(level, index + "] = " + it.next());
                index++;
            }
        }
    }

    public static void print(int level, Object[] o) {
        if (logLevel >= level) {
            if (o == null) {
                printLine(level, "array is null");
            } else if (o.length <= 0) {
                printLine(level, "array is empty");
            } else {
                printLine(level, "array size = " + o.length);
                for (int i = 0; i < o.length; i++) {
                    printLine(level, i + "] = " + o[i]);
                }
            }
        }
    }

    /** Overriden. */
    protected static void printLine(int level, Object o) {
        String s = LEVEL_MARKER[level] + prefix + o;
        if (consoleLevel >= level) {
            System.out.println(s);
        }
        if (logCache != null && stcLevel >= level) {
            logCache.addElement(s);
        }
        //->Kenneth[2015.10.14]
        if (logLevel >= level) {
            writeInFile(s);
        }
        //<-
    }

    private static void readConsoleLevel() {
        String value = GeneralPreferenceUtil.read(UP_NAME_CONSOLE_LEVEL);
        if (DEBUG_ON) {
            printDebug("readConsoleLevel = " + value);
        }
        try {
            if (Environment.EMULATOR) {
                setConsoleLevel(Math.max(Integer.parseInt(value) - 8, LEVEL_DEBUG));
            } else {
                setConsoleLevel(Integer.parseInt(value) - 8);
            }
        } catch (NumberFormatException ex) {
        }
    }

    static class ConsoleLevelChangedListener implements UserPreferenceChangeListener {
        public void receiveUserPreferenceChangeEvent(UserPreferenceChangeEvent e) {
            if (UP_NAME_CONSOLE_LEVEL.equals(e.getName())) {
                readConsoleLevel();
            }
        }
    }

    static class LogCacheUpdater implements DataUpdateListener {
        /** Called when LogCache is ready to use. */
        public void dataUpdated(String key, Object old, Object value) {
            logCache = (Vector) DataCenter.getInstance().get("LogCache");
        }

        public void dataRemoved(String key) {
        }
    }


    //->Kenneth[2015.10.14]
    private static BufferedWriter bw;
    private static int offset;
    private static final int MAX_OFFSET = 900000;//0.9MB
    // 부팅시에 이벤트가 한 번 올라옴. 그것을 무시해야 임의로 log capture 가 켜지는 것을 막는다.
    private static boolean ignoreFirstUpdate = true;
    static class AppNameUpdater implements DataUpdateListener {
        public void dataUpdated(String key, Object old, Object value) {
            try {
                if (ignoreFirstUpdate) {
                    ignoreFirstUpdate = false;
                    return;
                }
                String appNameforLogging = (String)value;
                String myName = FrameworkMain.getInstance().getApplicationName();
                if (myName.equals(appNameforLogging)) {
                    turnOnLogCache(myName);
                } else {
                    turnOffLogCache();
                }
            } catch (Exception e) {
                print(e);
            }
        }

        public void dataRemoved(String key) {
        }
    }

    public static void turnOnLogCache(String appName) {
        System.out.println("--------------> turnOnLogCache("+appName+")");
        turnOffLogCache();
        try {
            String root = getLogicalStorageVolume();
            System.out.println("--------------> root = "+root);
            if (root == null) return;
            String path = root+"/log/";
            //String path = System.getProperty("dvb.persistent.root")+"/log/";
            File dir = new File(path);
            if (!dir.exists()) {
                dir.mkdir();
            }
            File file = new File(dir, appName+".txt");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            bw = new BufferedWriter(new FileWriter(file.getAbsoluteFile()));
            bw.write(appName+" Log\n");
        } catch (Exception e) {
            if (DEBUG_ON) print(e);
        }
    }

    public static void turnOffLogCache() {
        System.out.println("--------------> turnOffLogCache()");
        try {
            if (bw != null) {
                bw.close();
            }
            bw = null;
        } catch (Exception e) {
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (Exception ex) {
                }
            }
            bw = null;
        }
        offset = 0;
    }

    private static void writeInFile(Object log) {
        if (log == null || !(log instanceof String)) return;
        if (bw == null) return;
        if (offset > MAX_OFFSET) return;
        try {
            String aLine = (String)log;
            aLine = aLine+"\n";
            int length = aLine.length();
            bw.write(aLine);
            bw.flush();
            offset += length;
        } catch (Exception e) {
            System.out.println("----------> writeInFile : e = "+e);
        }
    }

    public static String getLogicalStorageVolume() {
        LogicalStorageVolume lsv = null;
        try {
            StorageManager sm = StorageManager.getInstance();
            if (sm == null) {
                return null;
            }
            StorageProxy[] sps = sm.getStorageProxies();
            if (sps == null || sps.length <= 0) {
                return null;
            }
            for (int i = 0; i < sps.length; i++) {
                LogicalStorageVolume[] lsvs = sps[i].getVolumes();
                if (lsvs == null || lsvs.length <= 0) {
                    continue;
                }
                for (int j = 0; j < lsvs.length; j++) {
                    if (lsvs[j] == null || lsvs[j] instanceof MediaStorageVolume) {
                        continue;
                    }
                    lsv = lsvs[j];
                    break;
                }
            }

            if (lsv == null) {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (lsv == null) return null;
        return lsv.getPath();
    }

    //static final int[] logKey = {KeyCodes.ASPECT, KeyCodes.ASPECT, KeyCodes.ASPECT};
    // log on/off 시키는 키 : Up, Down, Up, Down, D
    /*
    static final int[] logKey = {OCRcEvent.VK_UP, OCRcEvent.VK_DOWN, OCRcEvent.VK_UP, OCRcEvent.VK_DOWN
        , KeyCodes.COLOR_D};
    static int logKeyStep = 0;
    public static boolean checkLogOnOff(int keyCode, String appName) {
        if (!DEBUG_ON) return false;
        if (true) return false;//->Kenneth[2015.10.16] 이번 릴리즈에서는 막기로 함.

        if (keyCode == logKey[logKeyStep]) {
            logKeyStep++;
            if (logKeyStep >= logKey.length) {
                logKeyStep = 0;
                if (bw == null) {
                    if (DEBUG_ON) printDebug("Log.turnOnLogCache()");
                    turnOnLogCache(appName);
                } else {
                    if (DEBUG_ON) printDebug("Log.turnOffLogCache()");
                    turnOffLogCache();
                }
                return true;
            }
        } else {
            logKeyStep = 0;
        }
        return false;
    }
    */
    /*
    // log on 시키는 키 : Up, Down, Up, Down, D
    //static final int[] logOnKey = {KeyCodes.COLOR_D, KeyCodes.COLOR_D, OCRcEvent.VK_MUTE, OCRcEvent.VK_MUTE};
    static final int[] logOnKey = {OCRcEvent.VK_UP, OCRcEvent.VK_DOWN, OCRcEvent.VK_UP, OCRcEvent.VK_DOWN, KeyCodes.COLOR_D};
    static int logOnKeyStep = 0;
    // log off 시키는 키 : Down, Up, Down, Up, D
    //static final int[] logOffKey = {OCRcEvent.VK_MUTE, OCRcEvent.VK_MUTE, KeyCodes.COLOR_D, KeyCodes.COLOR_D};
    static final int[] logOffKey = {OCRcEvent.VK_DOWN, OCRcEvent.VK_UP, OCRcEvent.VK_DOWN, OCRcEvent.VK_UP, KeyCodes.COLOR_D};
    static int logOffKeyStep = 0;

    public static boolean checkLogOnOff(int keyCode, String appName) {
        if (!Log.DEBUG_ON) return false;

        if (keyCode == logOnKey[logOnKeyStep]) {
            logOnKeyStep++;
            if (logOnKeyStep >= logOnKey.length) {
                logOnKeyStep = 0;
                if (Log.DEBUG_ON) Log.printDebug("Log.turnOnLogCache()");
                Log.turnOnLogCache(appName);
                return true;
            }
        } else {
            logOnKeyStep = 0;
        }

        if (keyCode == logOffKey[logOffKeyStep]) {
            logOffKeyStep++;
            if (logOffKeyStep >= logOffKey.length) {
                logOffKeyStep = 0;
                if (Log.DEBUG_ON) Log.printDebug("Log.turnOffLogCache()");
                Log.turnOffLogCache();
                return true;
            }
        } else {
            logOffKeyStep = 0;
        }
        return false;
    }
    //<-
    */
}

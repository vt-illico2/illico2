package com.videotron.tvi.illico.screensaver.data;

import java.io.File;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.screensaver.communication.ScreenSaverServiceImpl;

/**
 * This class does parser of InabandData.
 * @author pellos
 */
public final class DataManager implements DataUpdateListener {

    /** instance of DataManager. */
    private static DataManager dataManager;

    /** Screen Saver Data. */
    private static ScreenSaverData screenSaverData;

    /** IB_DATA_OC parameter. */
    public static String IB_DATA_OC = "Scrs_DataIB/";
    /** FILE_NAME parameter. */
    public static String FILE_NAME = "screen_saver_images";
    /** FILE_TYPE_ZIP parameter. */
    public static String FILE_TYPE_ZIP = ".zip";
    /** FILE_TYPE_TXT parameter. */
    public static final String FILE_TYPE_TXT = ".txt";
    /** IMAGE_TYPE parameter. */
    public static final String IMAGE_TYPE = ".img";

    String name = "Scrs_DataIB/screen_saver_images.zip";

    /**
     * Constructor. register InbandData into DataCenter.
     */
    private DataManager() {
        // String name = DataCenter.getInstance().getString("INBAND1");
        Log.printDebug("inband = " + name);
        try {
            IB_DATA_OC = name.substring(0, name.indexOf('/') + 1);
            FILE_NAME = name.substring(name.indexOf('/') + 1, name.indexOf('.'));
            FILE_TYPE_ZIP = name.substring(name.indexOf('.'), name.length());

            Log.printDebug(IB_DATA_OC + " " + FILE_NAME + " " + FILE_TYPE_ZIP);
            DataCenter.getInstance().addDataUpdateListener("INBAND1", this);
        } catch (Exception e) {
            ScreenSaverServiceImpl.getInstance().showErrorMessage("SSV500");
        }
    }

    /**
     * get Instance DataManager.
     * @return dataManager DataManager
     */
    public static DataManager getInstance() {
        if (dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    /**
     * get ScreenSaverData.
     * @return screenSaverData ScreenSaverData
     */
    public static ScreenSaverData getScreenSaverData() {
        return screenSaverData;
    }

    /**
     * It separates data from ZipFile.
     * @param data inbandData
     */
    public void readZipFile(Object data) throws Exception {
        Hashtable table = ZipFileReader.read((File) data);
        if (table != null) {

            byte[] config = (byte[]) table.get(FILE_NAME + FILE_TYPE_TXT);
            parserText(config);

            String type = screenSaverData.getDataType();
            if (type.equals("I")) {
                String[] fileName = screenSaverData.getFileName();
                for (int b = 0; b < fileName.length; b++) {
                    byte[] imageByte = (byte[]) table.get(fileName[b]);

                    if (b == ScreenSaverData.FRENCH) {
                        FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                screenSaverData.getApplicationName() + "_fr" + IMAGE_TYPE);
                    } else if (b == ScreenSaverData.ENGLISH) {
                        FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                screenSaverData.getApplicationName() + "_en" + IMAGE_TYPE);
                    }
                }
            } else if (type.equals("T")) {
                String[] text = screenSaverData.getText();
                DataCenter.getInstance()
                        .put(screenSaverData.getApplicationName() + "_fr", text[ScreenSaverData.FRENCH]);
                DataCenter.getInstance().put(screenSaverData.getApplicationName() + "_en",
                        text[ScreenSaverData.ENGLISH]);
            }
        }
        FrameworkMain.getInstance().getImagePool().waitForAll();
    }

    /**
     * parser Text data.
     * @param data byte
     */
    public void parserText(byte[] data) {
        int index = -1;
        int version = oneByteToInt(data, ++index);
        Log.printDebug(" version = " + version);
        ScreenSaverData.setVersion(version);

        int languageTypeSize = oneByteToInt(data, ++index);
        Log.printDebug(" languageTypeSize = " + languageTypeSize);

        int speedType = oneByteToInt(data, ++index);
        Log.printDebug(" speedType = " + speedType);
        ScreenSaverData.setSpeedType(speedType);

        screenSaverData = new ScreenSaverData();
        screenSaverData.setApplicationName(FrameworkMain.getInstance().getApplicationName());

        String dataType = byteArrayToString(data, ++index, 1);
        Log.printDebug(" dataType = " + dataType);
        screenSaverData.setDataType(dataType);

        if (ScreenSaverData.IMAGE.equals(dataType)) {

            for (int b = 0; b < languageTypeSize; b++) {
                String languageType = byteArrayToString(data, ++index, 2);
                Log.printDebug(" languageType = " + languageType);
                ++index;

                int imageNameLength = oneByteToInt(data, ++index);
                Log.printDebug(" imageNameLength = " + imageNameLength);

                String imageName = byteArrayToString(data, ++index, imageNameLength);
                Log.printDebug(" imageName = " + imageName);
                index += imageNameLength - 1;

                if (languageType.equals("fr")) {
                    screenSaverData.setFileName(ScreenSaverData.FRENCH, imageName);
                } else if (languageType.equals("en")) {
                    screenSaverData.setFileName(ScreenSaverData.ENGLISH, imageName);
                }
            }
        } else if (ScreenSaverData.TEXT.equals(dataType)) {

            for (int b = 0; b < languageTypeSize; b++) {
                String languageType = byteArrayToString(data, ++index, 2);
                ++index;
                Log.printDebug(" languageType = " + languageType);

                int textLength = oneByteToInt(data, ++index);
                Log.printDebug(" textLength = " + textLength);

                String textName = byteArrayToString(data, ++index, textLength);
                Log.printDebug(" textName = " + textName);
                index += textLength - 1;

                if (languageType.equals("fr")) {
                    screenSaverData.setText(ScreenSaverData.FRENCH, textName);
                } else if (languageType.equals("en")) {
                    screenSaverData.setText(ScreenSaverData.ENGLISH, textName);
                }
            }
        }
    }

    /**
     * convert oneByte to int.
     * @param data byte
     * @param offset length
     * @return int
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]));
    }

    /**
     * convert byteArray To string.
     * @param data byte
     * @param offset start point
     * @param length length
     * @return result String
     */
    public static String byteArrayToString(byte[] data, int offset, int length) {
        String result = null;

        try {
            result = new String(data, offset, length);
        } catch (Exception uee) {
            uee.printStackTrace();
            return null;
        }
        return result;
    }

    /**
     * data Removed.
     * @param key file name.
     */
    public void dataRemoved(String key) {
    }

    /**
     * data Updated.
     * @param key file name.
     * @param old previous data.
     * @param value new data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        Log.printDebug("dataUpdated key =" + key + "old = " + old + "value = " + value);
        try {
            readZipFile(value);
        } catch (Exception e) {
            ScreenSaverServiceImpl.getInstance().showErrorMessage("SSV501");
        }

    }
}

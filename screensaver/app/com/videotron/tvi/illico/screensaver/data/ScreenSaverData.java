package com.videotron.tvi.illico.screensaver.data;

import java.util.Arrays;

import com.videotron.tvi.illico.framework.FrameworkMain;

/**
 * This class saves ScreenSaverData.
 * @author pellos
 *
 */
public class ScreenSaverData {

    /** French type. */
    public static final int FRENCH = 0;
    /** English type. */    
    public static final int ENGLISH = 1;

    /** Image type. */
    public static final String IMAGE = "I";
    /** Text type. */
    public static final String TEXT = "T";

    /** version. */
    private static int version;
    
    /** speedType. */
    private static int speedType;

    /** default Image name. */
    private static String[] defaultImageName = new String[2];
    /** is saved default Image name into DataCenter. */
    private static String defaultName = "illico.png";

    /** screen saver ID. */
    private String applicationName;

    /** Image or Text data Type. */
    private String dataType;

    /** file name. */
    private String[] fileName = new String[2];
    /** text. */
    private String[] text = new String[2];

    /**
     * Constructor.
     */
    public ScreenSaverData() {
    }

    /**
     * get defaultName.
     * @return defaultName
     */
    public static String getDefaultName() {
        return defaultName;
    }

    /**
     * get Version.
     * @return version
     */
    public static int getVersion() {
        return version;
    }

    /**
     * set Version.
     * @param ver data version
     */
    public static void setVersion(int ver) {
        ScreenSaverData.version = ver;
    }

    /**
     * get SpeedType.
     * @return speedType
     */
    public static int getSpeedType() {
        return speedType;
    }

    /**
     * set SpeedType.
     * @param speed speedType
     */
    public static void setSpeedType(int speed) {
        ScreenSaverData.speedType = speed;
    }

    /**
     * set Default Image Name.
     * @param num id
     * @param name fileName
     */
    public static void setDefaultImageName(int num, String name) {
        defaultImageName[num] = name;
    }

    /**
     * get DefaultImageName.
     * @return defaultImageName
     */
    public static String[] getDefaultImageName() {
        return defaultImageName;
    }

    /**
     * get ApplicastionName.
     * @return applicastionName
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * set ApplicastionName.
     * @param id application.
     */
    public void setApplicationName(String id) {
        this.applicationName = id;
    }

    /**
     * get DataType.
     * @return dataType
     */
    public String getDataType() {
        return dataType;
    }

    /**
     * set DataType.
     * @param data dataType
     */
    public void setDataType(String data) {
        this.dataType = data;
    }

    /**
     * get FileName.
     * @return fileName
     */
    public String[] getFileName() {
        return fileName;
    }

    /**
     * set FileName.
     * @param num key
     * @param name file name.
     */
    public void setFileName(int num, String name) {
        fileName[num] = name;
    }

    /**
     * get text.
     * @return text
     */
    public String[] getText() {
        return text;
    }

    /**
     * set Text.
     * @param num key.
     * @param str String.
     */
    public void setText(int num, String str) {
        text[num] = str;
    }

    /**
     * toString.
     * @return toString
     */
    public String toString() {
        return "ScreenSaverData [applicastionName=" + applicationName + ", dataType=" + dataType + ", fileName="
                + (fileName != null ? Arrays.asList(fileName) : null) + ", text="
                + (text != null ? Arrays.asList(text) : null) + "]";
    }

}

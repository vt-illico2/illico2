/**
 *
 */
package com.videotron.tvi.illico.screensaver;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.screensaver.communication.ScreenSaverServiceImpl;

/**
 * The main class of screen-saver application.
 * @author Woosik Lee
 */
public class App implements Xlet, ApplicationConfig {
    /** The singleton instance of App. */
    private XletContext xletContext;

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
        Log.setApplication(this);
        if (Log.INFO_ON) {
            Log.printInfo("App: initXlet");
        }

        // DataManager2.getInstance();
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();
        ScreenSaverServiceImpl.getInstance().dispose();
    }

    // //////////////////////////////////////////////////////////////////////////
    // ApplicationConfig implementation
    // //////////////////////////////////////////////////////////////////////////
    /**
     * Returns the version of Application.
     * @return version string.
     */
    public String getVersion() {
        //->Kenneth[2015.7.6] R5 Tank
        // illico.png 를 바꿈. IB 데이터 못 가져왔을 때 보이는 default image
        return "R5.1.1";//VDTRMASTER-5505(로고이미지교체)
        //return "1.0.2.0";
    }

    /**
     * Returns the name of Application.
     * @return application name.
     */
    public String getApplicationName() {
        return "screensaver";
    }

    /**
     * Returns the Application's XletContext.
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
		ScreenSaverServiceImpl.getInstance().start(xletContext);
	}
}

package com.videotron.tvi.illico.screensaver.controller;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.effect.AlphaEffect;
import com.videotron.tvi.illico.framework.effect.AnimationRequestor;
import com.videotron.tvi.illico.framework.effect.Effect;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.screensaver.data.DataManager;
import com.videotron.tvi.illico.screensaver.data.ScreenSaverData;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class is GUI.
 * @author pellos
 */
public final class ScreenSaver extends Container implements Runnable {

    /**
     *
     */
    private static final long serialVersionUID = 20103100000L;

    /** is Emul. */
    private boolean isEmul = Environment.EMULATOR;

    private ImageContainer imageContainer;

    /** Instance of ScreenSaver. */
    private static ScreenSaver instance;

    /** Screen Saver Image Name. */
    private String imageName;

    /** Screen Saver Image. */
    private Image screenSaverImage;

    /** Image X-coordinates. */
    private int imageX;
    /** Image Y-coordinates. */
    private int imageY;

    /** screen saver message. */
    private String message;

    /** Instance of LayeredUIManager. */
    private LayeredUIManager layeredUIManager;

    private AlphaEffect fadeInEffect;
    private AlphaEffect fadeOutEffect;

    /** Instance of LayeredUI. */
    private LayeredUI layerui;

    /** Instance of layeredWindos. */
    private LayeredWindow layeredWindos = new LayeredWindow();

    /** Instance of TVTimer. */
    private TVTimer timer = TVTimer.getTimer();

    /** Instance of TVTimerSpec. */
    private TVTimerSpec timerSpec = new TVTimerSpec();

    /** Width of image. */
    private int imageWidth = 0;
    /** Height of Image. */
    private int imageHeight = 0;

    /** Animation type. */
    private int animationType = 0;

    /** kind of Animation. */

    /** CENTER Type. */
    private static final int CENTER = 0;
    /** RANDOM Type. */
    private static final int RANDOM = 1;
    /** ACROSS Type. */
    private static final int ACROSS = 2;
    /** RIGHT_TO_LEFT Type. */
    private static final int RIGHT_TO_LEFT = 3;
    /** LEFT_TO_RIGHT Type. */
    private static final int LEFT_TO_RIGHT = 4;

    /** X-Coordinates for animation. */
    private int xCoordinates;

    /** Temp X-Coordinates for animation. */
    private int tempX;
    /** Temp Y-Coordinates for animation. */
    private int tempY;

    /** Random instance. */
    private Random ra = new Random();

    /** Direction of across animation. */
    private int imageCourse = 0;

    /** LEFT_DOWN Direction of across animation. */
    private static final int LEFT_DOWN = 0;
    /** DOWN_RIGHT Direction of across animation. */
    private static final int DOWN_RIGHT = 1;
    /** RIGHT_UP Direction of across animation. */
    private static final int RIGHT_UP = 2;
    /** UP_LEFT Direction of across animation. */
    private static final int UP_LEFT = 3;
    /** UP_RIGHT Direction of across animation. */
    private static final int UP_RIGHT = 4;
    /** LEFT_UP Direction of across animation. */
    private static final int LEFT_UP = 5;
    /** RIGHT_DOWN Direction of across animation. */
    private static final int RIGHT_DOWN = 6;
    /** DOWN_LEFT Direction of across animation. */
    private static final int DOWN_LEFT = 7;

    /** BOUNDS. */
    private static final Rectangle BOUNDS = new Rectangle(0, 0, 960, 540);

    private static final Font F20 = FontResource.BLENDER.getFont(20);
    private static final FontMetrics FM20 = FontResource.getFontMetrics(F20);

    /** 1 Scend. */
    private static final long SECOND = 1000L;

    /**
     * Constructor. set LayeredUIManager, LayeredUI.
     */
    private ScreenSaver() {
        super();
        layeredUIManager = LayeredUIManager.getInstance();
        setBounds(BOUNDS);
        setVisible(false);
        layeredWindos.setBounds(BOUNDS);
        layerui = WindowProperty.SCREEN_SAVER.createLayeredWindow(layeredWindos, BOUNDS);

        if (imageContainer == null) {
            imageContainer = new ImageContainer();
            this.add(imageContainer);
            imageContainer.setVisible(false);
        }
    }

    /**
     * getInstance.
     * @return instance ScreenSaver
     */
    public static synchronized ScreenSaver getInstance() {
        if (instance == null) {
            instance = new ScreenSaver();
        }
        return instance;
    }

    /**
     * show screen saver.
     * @param id id.
     */
    public void showScreenSaver(String id) {
        if (Log.INFO_ON) {
            Log.printInfo("ScreenSaver: showScreenSaver = " + id);
        }
        String context;
        if (id == null) {
            context = "TV";
        } else {
            context = id;
        }
        FrameworkMain.getInstance().printSimpleLog("started in " + context + " context");
        ScreenSaverData screenSaverData = DataManager.getScreenSaverData();
        if (screenSaverData != null) {
            String type = screenSaverData.getDataType();

            if (ScreenSaverData.IMAGE.equals(type)) {
                screenSaverImage = DataCenter.getInstance().getImageByLanguage(
                        screenSaverData.getApplicationName() + DataManager.IMAGE_TYPE);
                animationType = RANDOM;
            } else if (ScreenSaverData.TEXT.equals(type)) {
                message = (String) DataCenter.getInstance().get(screenSaverData.getApplicationName());
                animationType = RANDOM;
            }

            if (screenSaverImage == null && message == null) {
                screenSaverImage = DataCenter.getInstance().getImage(ScreenSaverData.getDefaultName());
                animationType = RANDOM;
            }
        } else {
             imageName = "illico.png";
             animationType = RANDOM;
             screenSaverImage = DataCenter.getInstance().getImage(imageName);
        }

        if (screenSaverImage != null) {
            imageWidth = screenSaverImage.getWidth(this);
            imageHeight = screenSaverImage.getHeight(this);
        } else if (message != null) {
            imageWidth = FM20.stringWidth(message);
            imageHeight = FM20.getHeight();

            Log.printInfo("imageWidth = " + imageWidth);
        }
        layeredWindos.add(this);
        layeredWindos.setVisible(true);
        this.setVisible(true);
        layerui.activate();
        super.setVisible(true);
        imageContainer.setVisible(true);
        startTimer();
    }

    public void fadeInEffect() {
        Log.printDebug("fadeInEffect");

        if (fadeInEffect == null) {
            fadeInEffect = new AlphaEffect(imageContainer, 50, AlphaEffect.FADE_IN);
        }
        fadeInEffect.start();
    }

    public void fadeOutEffect() {
        Log.printDebug("fadeOutEffect");

        if (fadeOutEffect == null) {
            fadeOutEffect = new AlphaEffect(imageContainer, 30, AlphaEffect.FADE_OUT);
        }
        fadeOutEffect.start();
    }

    /**
     * hide Screen Saver.
     */
    public void hideScreenSaver() {
        if (Log.INFO_ON) {
            Log.printInfo("ScreenSaver: hideScreenSaver()");
        }
        screenSaverImage = null;
        message = null;
        stopTimer();
        layeredWindos.remove(this);
        layeredWindos.setVisible(false);
        super.setVisible(false);
        this.setVisible(false);
        layerui.deactivate();
        if (imageName != null) {
            DataCenter.getInstance().remove(imageName);
        }
    }

    Graphics gg;

    /**
     * draw Image.
     * @param g Graphics
     */
    public void paint(Graphics g) {
        gg = g;
        g.setColor(Color.black);
        // g.setColor(Color.blue);
        g.fillRect(0, 0, 960, 540);
        super.paint(g);
    }

    public void runnig() {
        switch (animationType) {
        case CENTER:
            imageX = ((960 - imageWidth) / 2);
            imageY = ((540 - imageHeight) / 2);
            break;
        case RANDOM:
            imageX = (int) (Math.random() * (960 - (2 * imageWidth)) - 60);
            imageY = (int) (Math.random() * (540 - imageHeight));

            if (imageX < 60) {
                imageX = 60;
            }

            if (screenSaverImage != null) {
                imageContainer.setBounds(imageX, imageY, imageWidth, imageHeight);
            } else {
                imageContainer.setBounds(imageX, imageY, imageWidth, imageHeight);
            }
             break;
        case ACROSS:
            switch (imageCourse) {
            case LEFT_DOWN:
            case UP_RIGHT:
                imageX = imageX + 15 + tempX;
                imageY = imageY + 10 + tempY;

                if (imageX > 960 - imageWidth) {
                    imageCourse = RIGHT_DOWN;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                } else if (imageY > 540 - imageHeight) {
                    imageCourse = DOWN_RIGHT;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                }
                break;
            case DOWN_RIGHT:
            case LEFT_UP:
                imageX = imageX + 15 + tempX;
                imageY = imageY - 10 + tempY;

                if (imageX > 960 - imageWidth) {
                    imageCourse = RIGHT_UP;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                } else if (imageY < 0) {
                    imageCourse = UP_RIGHT;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                }
                break;
            case RIGHT_UP:
            case DOWN_LEFT:
                imageX = imageX - 15 + tempX;
                imageY = imageY - 10 + tempY;

                if (imageX < 0) {
                    imageCourse = LEFT_UP;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                } else if (imageY < 0) {
                    imageCourse = UP_LEFT;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                }
                break;
            case UP_LEFT:
            case RIGHT_DOWN:

                imageX = imageX - 15 + tempX;
                imageY = imageY + 10 + tempY;

                if (imageX < 0) {
                    imageCourse = LEFT_DOWN;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                } else if (imageY > 540 - imageHeight) {
                    imageCourse = DOWN_LEFT;
                    tempX = ra.nextInt(5);
                    tempY = ra.nextInt(5);
                }
                break;
            default:
                break;
            }
            break;
        case RIGHT_TO_LEFT:
            xCoordinates = xCoordinates + 15;
            imageX = (960 - (imageWidth / 4) - xCoordinates);
            imageY = ((540 - imageHeight) / 2);

            if (imageX < (-imageWidth + (imageWidth / 4))) {
                xCoordinates = 0;
            }

            break;
        case LEFT_TO_RIGHT:
            xCoordinates = xCoordinates + 15;
            imageX = -((3 * imageWidth) / 4) + xCoordinates;
            imageY = ((540 - imageHeight) / 2);

            if (imageX > (960 - (imageWidth / 4))) {
                xCoordinates = 0;
            }
            break;
        default:
            break;
        }
        repaint();
    }

    Thread th;

    /**
     * Start Timer, Animation.
     */
    private void startTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("ScreenSaver: startTimer");
        }
        th = null;
        th = new Thread(instance);
        threadRunnig = false;
        th.start();
    }

    /**
     * stop Timer, Animation.
     */
    private void stopTimer() {
        if (Log.INFO_ON) {
            Log.printInfo("ScreenSaver: stopTimer");
        }
        threadRunnig = false;
        th = null;
    }

    class ImageContainer extends Container implements AnimationRequestor {
        public void paint(Graphics g) {
            if (screenSaverImage != null) {
                g.drawImage(screenSaverImage, 0, 0, null);
            } else if (message != null) {
                g.setColor(Color.white);
                g.setFont(F20);
                g.drawString(message, 0, imageHeight);
            }
        }

        public void animationEnded(Effect effect) {
            if (fadein) {
                // imageContainer.setVisible(true);
                imageContainer.setVisible(false);
            }
        }

        public void animationStarted(Effect effect) {
            if (!fadein) {
                imageContainer.setVisible(false);
            }
        }

        public boolean skipAnimation(Effect effect) {
            // TODO Auto-generated method stub
            return false;
        }
    }

    boolean fadein = true;
    boolean threadRunnig = false;

    public void run() {
        Log.printDebug("start Thread");

        if (threadRunnig) {
            return;
        }
        threadRunnig = true;

        while (threadRunnig) {
            runnig();
            fadein = true;
            // fadeInEffect();
            // try {
            // Thread.sleep(1 * SECOND);
            // } catch (InterruptedException e) {
            // e.printStackTrace();
            // }

            fadein = false;
            // fadeOutEffect();
            try {
                Thread.sleep(2 * SECOND);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}

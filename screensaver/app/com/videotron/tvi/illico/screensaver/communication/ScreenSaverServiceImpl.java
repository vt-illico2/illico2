package com.videotron.tvi.illico.screensaver.communication;

import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.davic.resources.ResourceStatusEvent;
import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.screensaver.controller.ScreenSaver;
import com.videotron.tvi.illico.screensaver.data.DataManager;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * This class is Controller of ScreenSaver.
 * @author pellos
 */
public final class ScreenSaverServiceImpl implements ScreenSaverService, DataUpdateListener {

    /** Instance of XletContext. */
    private XletContext xletContext;

    /** Instance of ScreenSaverServiceImpl. */
    private static ScreenSaverServiceImpl instance;

    /** The remote object of MonitorService. */
    private MonitorService monitorService;

    /** turning channel listener for receiving inbandData. */
    private InbandListener inbandListener = new InbandListener();

    /** Is visible time of ScreenSaver. */
    private long maximumInactivityTime;

    private int version = 0;

    private StcService stcService;
    private ErrorMessageService errorMgsService;

    private IxcLookupWorker ixcWorker;

    /**
     * Get instance of ScreenSaverServiceImpl.
     * @return instance
     */
    public static synchronized ScreenSaverServiceImpl getInstance() {
        if (instance == null) {
            instance = new ScreenSaverServiceImpl();
        }
        return instance;
    }

    /**
     * Constructor.
     */
    private ScreenSaverServiceImpl() {
        DataManager.getInstance();
    }

    /**
     * ScreenSaver Start.
     * @param xlet XletContext.
     */
    public void start(XletContext xlet) {
        this.xletContext = xlet;
        ixcWorker = new IxcLookupWorker(xlet);

        maximumInactivityTime = Long.parseLong((String) DataCenter.getInstance().get("MaximumInactivityTime"));
        Log.printDebug("MaximumInactivityTime = " + maximumInactivityTime);
        FrameworkMain.getInstance().printSimpleLog("MaximumInactivityTime = " + maximumInactivityTime);
        bindToIxc();

        ixcWorker.lookup("/1/1/", MonitorService.IXC_NAME, this);
        ixcWorker.lookup("/1/2100/", StcService.IXC_NAME, this);
        ixcWorker.lookup("/1/2025/", ErrorMessageService.IXC_NAME, this);
        ixcWorker.lookup("/1/2030/", PreferenceService.IXC_NAME, this);
    }

    /**
     * dispose.
     */
    public void dispose() {
        try {
            IxcRegistry.unbind(xletContext, ScreenSaverService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * ScreenSaver does bind.
     */
    private void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("ScreenSaverService: bind to IxcRegistry");
            }
            IxcRegistry.bind(xletContext, ScreenSaverService.IXC_NAME, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dataUpdated(String key, Object old, Object value) {
        if (key.equals(MonitorService.IXC_NAME)) {
            monitorService = (MonitorService) value;
            try {
                String appName = FrameworkMain.getInstance().getApplicationName();
                monitorService.addInbandDataListener(inbandListener, appName);
            } catch (Throwable e) {
                Log.print(e);
            }
        } else if (key.equals(StcService.IXC_NAME)) {
            stcService = (StcService) value;
            try {
                int currentLevel = stcService.registerApp("screensaver");
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);

                Object o = SharedMemory.getInstance().get("stc-" + "screensaver");
                if (o != null) {
                    DataCenter.getInstance().put("LogCache", o);
                }
                try {
                    stcService.addLogLevelChangeListener("screensaver", new LogLevelAdapter());
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
            } catch (Throwable e) {
                Log.print(e);
            }
        } else if (key.equals(ErrorMessageService.IXC_NAME)) {
            errorMgsService = (ErrorMessageService) value;
        } else if (key.equals(PreferenceService.IXC_NAME)) {
            PreferenceProxy.getInstance().setPreferenceService((PreferenceService) value);
        }
    }

    public void dataRemoved(String key)  {
    }

    /**
     * Shows the screen saver.
     * @param id Application Name
     * @throws RemoteException RemoteException
     */
    public void showScreenSaver(String id) throws RemoteException {
        ScreenSaver.getInstance().showScreenSaver(id);
    }

    /**
     * Hides the screen saver.
     * @throws RemoteException RemoteException
     */
    public void hideScreenSaver() throws RemoteException {
        ScreenSaver.getInstance().hideScreenSaver();
    }

    /**
     * Gets the maximun inactivity time.
     * @return inactivity time (unit is second).
     * @throws RemoteException RemoteException
     */
    public long getMaximulInactivityTime() throws RemoteException {
        return maximumInactivityTime;
    }

    public void showErrorMessage(String code) {
        if (errorMgsService != null) {
            try {
                errorMgsService.showErrorMessage(code, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This class is turning channel listener for receiving inbandData.
     * @author pellos
     */
    class InbandListener implements InbandDataListener {

        /**
         * channel turn Listener.
         * @param locator file name
         */
        public void receiveInbandData(String locator) {
            if (Log.DEBUG_ON) {
                Log.printDebug("receiveInbandData = " + locator);
            }

            int newVersion = 0;
            try {
                newVersion = monitorService.getInbandDataVersion(MonitorService.screen_saver_image);
                Log.printDebug("receiveInbandData newVersion = " + newVersion);
            } catch (RemoteException e1) {
                Log.printError(e1);
            }

            if(newVersion != version){
                version = newVersion;
                DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
            }
            try {
                monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
            } catch (Exception e) {
                Log.printError(e);
            }
        }
    }

    class LogLevelAdapter implements LogLevelChangeListener {
        public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }
}

package com.videotron.tvi.illico.screensaver.communication;

import java.rmi.RemoteException;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.log.Log;

/**
 * PreferenceProxy integrated with User Profile Application to apply the setting values of Search Application.
 * @author
 */
public class PreferenceProxy implements PreferenceListener {
    /** This class instance. */
    private static PreferenceProxy instance = new PreferenceProxy();
    /** PreferenceService instance. */
    private PreferenceService preferenceService;
    /** Key set. */
    public static final String[] preferenceKeys = new String[]{ PreferenceNames.LANGUAGE };

    /**
     * Get instance.
     * @return PreferenceProxy
     */
    public static PreferenceProxy getInstance() {
        return instance;
    }

    void setPreferenceService(PreferenceService service) {
        this.preferenceService = service;
        addPreferenceListener();
    }

    /**
     * register listener to UPP.
     */
    private void addPreferenceListener() {
        if (Log.DEBUG_ON) {
            Log.printDebug("PreferenceProxy, addPreferenceListener");
        }
        try {
            String[] values = preferenceService.addPreferenceListener(this, FrameworkMain.getInstance().getApplicationName(),
                    preferenceKeys, new String[]{""});

            DataCenter.getInstance().put(PreferenceNames.LANGUAGE, values[0]);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * values are received update Listener from UPP.
     */
    public void receiveUpdatedPreference(String preferenceName, String value) throws RemoteException {
        Log.printInfo("preferenceName = " + preferenceName + " value = " + value);

        if (preferenceName.equals(PreferenceNames.LANGUAGE)) {
            DataCenter.getInstance().put(preferenceName, value);
        }
    }
}

/**
 * @(#)LoadingController.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.loading;

import javax.tv.xlet.XletContext;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.loading.ixc.IXCManager;
import com.videotron.tvi.illico.loading.ixc.PreferenceProxy;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;

/**
 * The Main class of LoadingAnimation Enabler. To communicate with other application bind a LoadingAnimation for IXC.
 * Manages to show and hide LoadingAnimation.
 * @author Pellos
 * @version 1.1
 */

public final class LoadingController implements DataUpdateListener {

    /** is Emul. */
    public static final boolean ISEMUL = Environment.EMULATOR;
    /** The instance of Controller. */
    private static LoadingController instance;

    /** loaded inband data. */
    private static boolean isLoadingdata = false;
    /** file Update Check. */
    private boolean configFileUpdateCheck = false;
    /** file Update Check. */
    private boolean imageFileUpdateCheck = false;

    /** booting file Update Check. */
    private boolean bootingConfigFileUpdateCheck = false;
    /** booting file Update Check. */
    private boolean bootingImageFileUpdateCheck = false;

    /** booting data version. */
    private int bootingVersion = -2;

    /** Booting config data. */
    private Object bootingConfig;
    /** Booting data. */
    private Object bootingData;

    /** Inband data Names. */
    private String inband1 = "INBAND1";
    private String inband2 = "INBAND2";
    private String inband3 = "INBAND3";
    private String inband4 = "INBAND4";

    /**
     * Instantiates a LoadingController for singleton.
     */
    private LoadingController() {
    }

    /**
     * Gets the singleton instance of LoadingController.
     * @return single instance of LoadingController
     */
    public static LoadingController getInstance() {
        if (instance == null) {
            instance = new LoadingController();
        }
        return instance;
    }

    /**
     * Inits the Controller. creates {@link #uiTable}.
     * @param xletContext the xlet context
     */
    public void init(XletContext xletContext) {
        Log.printInfo("init");
        DataCenter.getInstance().addDataUpdateListener(inband1, this);
        DataCenter.getInstance().addDataUpdateListener(inband2, this);
        DataCenter.getInstance().addDataUpdateListener(inband3, this);
        DataCenter.getInstance().addDataUpdateListener(inband4, this);

        DataManager.getInstance();
        IXCManager.getInstance().init(xletContext);
        PreferenceProxy.getInstance().init(xletContext);
    }
    
    public void start(){
        LoadingAnimation.getInstance();
    }

    /**
     * data Removed.
     * @param key file name.
     */
    public void dataRemoved(String key) {

    }

    /**
     * InbandData is updated and does load.
     * @param key file name
     * @param old previous data.
     * @param value new data.
     */
    public void dataUpdated(String key, Object old, Object value) {
        Log.printDebug("dataUpdated key =[" + key + "]old = " + old + "value = " + value);

        if (inband1.equals(key)) {
            Log.printDebug("readTextFile1");
            try {
                DataManager.getInstance().readTextFile(value, DataManager.LOADING_TYPE);
                configFileUpdateCheck = true;
            } catch (Exception e) {
                configFileUpdateCheck = false;
            }
        } else if (inband2.equals(key)) {
            Log.printDebug("readTextFile2");
            if (value != null) {
                try {
                    DataManager.getInstance().readZipFile(value, DataManager.LOADING_TYPE);
                    imageFileUpdateCheck = true;
                } catch (Exception e) {
                    imageFileUpdateCheck = false;
                }
            }
        } else if (inband3.equals(key)) {
            Log.printDebug("readTextFile3");
            try {
                DataManager.getInstance().readTextFile(value, DataManager.BOOTING_TYPE);
                bootingConfigFileUpdateCheck = true;
            } catch (Exception e) {
                bootingConfigFileUpdateCheck = false;
            }
            bootingConfig = null;
            bootingConfig = value;
        } else if (inband4.equals(key)) {
            Log.printDebug("readTextFile4");
            if (value != null) {
                bootingData = null;
                bootingData = value;
                bootingImageFileUpdateCheck = true;
            }
        }

        if ((inband3.equals(key) || inband4.equals(key))
                && bootingConfigFileUpdateCheck && bootingImageFileUpdateCheck
                && DataManager.getInstance().getBootingVersion() != bootingVersion) {
            bootingVersion = DataManager.getInstance().getBootingVersion();

            Thread th = new Thread() {
                public void run() {
                    DataManager.cashWriteData(bootingConfig, DataManager.CONFIG_FILE);
                    DataManager.cashWriteData(bootingData, DataManager.BOOTING_FILE);
                    bootingImageFileUpdateCheck = false;
                    bootingConfigFileUpdateCheck = false;
                }
            };
            th.start();
        }

        if ((inband1.equals(key) || inband2.equals(key)) && configFileUpdateCheck && imageFileUpdateCheck) {
            setRoadingdata(true);
            configFileUpdateCheck = false;
            imageFileUpdateCheck = false;
            Log.printDebug("fileUpdateCheck did");
        }
    }

    /**
     * inbandData is received or doesn't be received.
     * @return isLoadingdata isLoadingdata
     */
    public static boolean isLoadingdata() {
        return ISEMUL ? true : isLoadingdata;
    }

    /**
     * set in case received inbandData.
     * @param data isLoadingdata
     */
    public static void setRoadingdata(boolean data) {
        LoadingController.isLoadingdata = data;
    }
}

/**
 * @(#)LoadingAnimation.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.loading;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.loading.ixc.IXCManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * This class is Loading Animation GUI.
 * @author pellos
 */
public final class LoadingAnimation extends LayeredWindow implements TVTimerWentOffListener, LayeredKeyHandler {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /** The instance of TVTimer. */
    private TVTimerSpec timer;
    
    /** The instance of TVTimer. */
    private TVTimerSpec limitTimeTimer;

    /** The instance. */
    private static LoadingAnimation loadingAnimation = new LoadingAnimation();

    /** Animation rolling index. */
    private int index = 0;

    /** Loading Image. */
    private Image[][] image;

    /** background Image. */
    private Image bgImage;

    private Image loadingBg;

    /** time is repeated to TVTimer. */
    private static long WAITTING_TIME = 500L;

    /** Renderer size. */
    private final Rectangle BOUNDS = new Rectangle(0, 0, 960, 540);

    /** Instance of LayeredUI. */
    private static LayeredUI layerui;
    /** Instance of LayeredWindow. */
    private LayeredWindow layeredWindos = new LayeredWindow();

    /** In case STB booting */
    private int isVisibleBgImage = 0;

    /** Default image X coordinates. */
    private int imageX = 430;
    /** Default image Y coordinates. */
    private int imageY = 220;

    private String bgName;

    /** Is showing. */
    private boolean isShowing = false;

    private long delayTime;

    private Vector defaultImgName1 = new Vector();
    private Vector defaultImgName2 = new Vector();

    private Vector imageName = new Vector();
    
    private int languageIndex = 0;

    private boolean delay;
    private long delayCount = 0;
    
    private long limitTime = 60000;

    private boolean isKeyMoving = false;

    Object ob = new Object();
    
    private int imgesGapX = 0;
    private int imgesGapY = 0;

    /**
     * Gets the single instance of LoadingAnimation.
     * @return single instance of LoadingAnimation
     */
    public synchronized static LoadingAnimation getInstance() {
        return loadingAnimation;
    }

    /**
     * Instantiates a new renderer LoadingAnimation.
     */
    private LoadingAnimation() {
        setBounds(BOUNDS);
        setVisible(false);

        layeredWindos.setBounds(BOUNDS);
        layeredWindos.add(this);

        layerui = WindowProperty.LOADING.createLayeredDialog(layeredWindos, BOUNDS, this);
        timer = new TVTimerSpec();
        long waitTime = Long.parseLong((String) DataCenter.getInstance().get("ANIMATION_DELAY_TIME"));
        delayTime = Long.parseLong((String) DataCenter.getInstance().get("DELAY_TIME")) * 1000;
        limitTime = DataCenter.getInstance().getLong("LIMIT_TIME");
        
        Log.printDebug("waitTime = " + waitTime);
        Log.printDebug("delayTime = " + delayTime);
        Log.printDebug("delayTime = " + limitTime);

        if (waitTime > 0) {
            WAITTING_TIME = waitTime;
        }

        String temp = (String) DataCenter.getInstance().get("LOADING_IMG_NAME1");
        StringTokenizer st = new StringTokenizer(temp, ",");

        while (st.hasMoreTokens()) {
            String aToken = st.nextToken();
            defaultImgName1.add(aToken);
        }

        String temp2 = (String) DataCenter.getInstance().get("LOADING_IMG_NAME2");
        StringTokenizer st2 = new StringTokenizer(temp2, ",");

        while (st2.hasMoreTokens()) {
            String aToken = st2.nextToken();
            defaultImgName2.add(aToken);
        }

        timer.setDelayTime(WAITTING_TIME);
        timer.setRepeat(true);
        timer.addTVTimerWentOffListener(this);
        timer.setRegular(false);
        
        limitTimeTimer = new TVTimerSpec();
        limitTimeTimer.setDelayTime(limitTime);
        limitTimeTimer.addTVTimerWentOffListener(this);
    }

    /**
     * @param flag
     */
    public void isKeyMoving(boolean flag) {
        isKeyMoving = flag;
    }

    /**
     * showLoadingAnimation makes images, starts TVTimer.
     */
    public void showLoadingAnimation(Point p, final boolean delay) {
        Log.printInfo("showLoadingAnimation = " + isShowing);
        Log.printInfo("showLoadingAnimation delay = " + delay);
        if (isShowing) {
            return;
        }

        synchronized (ob) {

            isShowing = true;
            this.delay = delay;
            if (p != null) {
                imageX = p.x;
                imageY = p.y;
            }
            delayCount = 0;

            Log.printDebug("imageX = " + imageX);
            Log.printDebug("imageY " + imageY);
            imageName.clear();
            loadingBg = DataCenter.getInstance().getImage("loading_bg.png");
//            imageName.add("loading_bg.png");
            if (!LoadingController.isLoadingdata() || LoadingController.ISEMUL) {
                isVisibleBgImage++;
                if (isVisibleBgImage > 2) {
                    isVisibleBgImage = 2;
                }

                image = null;
                String language = (String) DataCenter.getInstance().get(PreferenceNames.LANGUAGE);
                Log.printDebug("language = " + language);
                setLanguage(language);

                String[] str = DataManager.getInstance().getBootingSaveImageName();
                if (str != null) {
                    image = new Image[2][str.length];
                    for (int a = 0; a < str.length; a++) {
                        Log.printDebug("str[" + a + "]" + str[a]);
                        image[0][a] = DataCenter.getInstance().getImage(str[a] + "_fr" + ".img");
                        image[1][a] = DataCenter.getInstance().getImage(str[a] + "_en" + ".img");
                        
                        imageName.add(str[a] + "_fr" + ".img");
                        imageName.add(str[a] + "_en" + ".img");
                    }
                } else {
                    image = new Image[2][defaultImgName1.size()];

                    for (int a = 0; a < defaultImgName1.size(); a++) {
                        image[0][a] = DataCenter.getInstance().getImage((String) defaultImgName1.elementAt(a));
                        image[1][a] = DataCenter.getInstance().getImage((String) defaultImgName2.elementAt(a));
                        
                        imageName.add((String) defaultImgName1.elementAt(a));
                        imageName.add((String) defaultImgName2.elementAt(a));
                    }
                }

                String bgStr = DataManager.getInstance().getBackGroundImageName();
                Log.printDebug("bgStr = " + bgStr);
                if (bgStr != null) {
                    bgName = bgStr;
                    bgImage = DataCenter.getInstance().getImage(bgName);
                             
                } else {
                    bgName = "bg.png";
                    bgImage = DataCenter.getInstance().getImage(bgName);
                }
                imageName.add(bgName);           
                FrameworkMain.getInstance().getImagePool().waitForAll();
            } else {
                isVisibleBgImage = 2;
                int size = DataManager.getInstance().getLoadingImageLength();
                String[] str = DataManager.getInstance().getSaveImageName();

                image = null;
                image = new Image[2][size];

                for (int a = 0; a < str.length; a++) {
                    image[0][a] = DataCenter.getInstance().getImage(str[a] + "_fr" + ".img");
                    image[1][a] = DataCenter.getInstance().getImage(str[a] + "_en" + ".img");
//                    imageName.add(str[a] + "_fr" + ".img");
//                    imageName.add(str[a] + "_en" + ".img");
                }
                
                FrameworkMain.getInstance().getImagePool().waitForAll();
            }

            imageX = imageX - image[0][0].getWidth(this) / 2;
            imageY = imageY - image[0][0].getHeight(this) / 2;
            Log.printDebug("imageX2 = " + imageX);
            Log.printDebug("imageY2 = " + imageY);
            
            imgesGapY = (loadingBg.getHeight(this) - image[0][0].getHeight(this)) / 2;
            imgesGapX = (loadingBg.getWidth(this) - image[0][0].getWidth(this) ) / 2 - 1;
            
            if (isShowing) {
                Log.printDebug("Loding is visible");
                layeredWindos.setVisible(true);
                loadingAnimation.setVisible(true);
                layerui.activate();

                try {
                    timer = TVTimer.getTimer().scheduleTimerSpec(timer);
                    Log.printDebug("timer = " + timer);
                } catch (Exception e) {
                    Log.print(e);
                }
                
                MonitorService mService = IXCManager.getInstance().getMonitorService();
                
                try {
					if (mService != null && mService.getSTBMode() != MonitorService.STB_MODE_BOOTING) {
						if (Log.DEBUG_ON) {
							Log.printDebug("LoadingAnimation: start limitTime Timer.");
						}
					    try {
					    	TVTimer.getTimer().deschedule(limitTimeTimer);
					    	limitTimeTimer = TVTimer.getTimer().scheduleTimerSpec(limitTimeTimer);
					        Log.printDebug("limitTimeTimer = " + limitTimeTimer);
					    } catch (Exception e) {
					        Log.print(e);
					    }
					} else {
						if (Log.DEBUG_ON) {
							Log.printDebug("LoadingAnimation: Don''t start limitTime Timer.");
						}
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
            }
        }
    }

    public void setLanguage(String lang) {
        Log.printInfo("language = " + lang);
        if (lang == null || lang.equals(Definitions.LANGUAGE_FRENCH)) {
            languageIndex = 0;
        } else {
            languageIndex = 1;
        }
    }

    /**
     * hideLoadingAnimation does unvisible, stops TVTimer.
     */
    public void hideLoadingAnimation() {
        Log.printInfo("ui hideLoadingAnimation");
        synchronized (ob) {
            isShowing = false;
            layerui.deactivate();
            if (bgImage != null) {
                DataCenter.getInstance().removeImage(bgName);
                bgImage = null;
            }

            if(imageName.size() > 0) {
                for(int a = 0; a < imageName.size(); a++) {
                    DataCenter.getInstance().removeImage((String)imageName.elementAt(a));
                }
            }
            imageName.clear();
            
            isKeyMoving = false;
            TVTimer.getTimer().deschedule(timer);
        }
    }

    /**
     * Timer Event.
     * @param event is TVTimerEvent.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
    	TVTimerSpec timer = event.getTimerSpec();
    	
//    	if (Log.DEBUG_ON) {
//			Log.printDebug("LoadingAnimation: timerWentOff: timer = " + timer);
//    	}
    	
    	if (timer.equals(this.timer)) {
    	
	        if (LoadingController.isLoadingdata() && delay && delayTime > (WAITTING_TIME * delayCount)) {
	            delayCount++;
	        } else {
	            if (image != null) {
	                index = (index + 1) % image[0].length;
	            }
	            repaint();
	        }
    	} else {
    		if (Log.DEBUG_ON) {
    			Log.printDebug("LoadingAnimation: timerWentOff: limit time's is fired.");
    			Log.printDebug("LoadingAnimation: timerWentOff: layerui active = " + layerui.isActive());
    		}
    		
    		if (layerui.isActive()) {
    			hideLoadingAnimation();
    		}
    	}
    }

    /**
     * paint.
     * @param g Graphics
     */
    public void paint(Graphics g) {
        if (LoadingController.isLoadingdata() && delay && delayTime > (WAITTING_TIME * delayCount)) {
            return;
        }

        if (isVisibleBgImage == 1) {
            g.drawImage(bgImage, 0, 0, this);
        }

        if (image != null) {
            g.drawImage(loadingBg, imageX - imgesGapX, imageY - imgesGapY - 5, this);
            g.drawImage(image[languageIndex][index], imageX, imageY, this);
        }
    }

    /**
     * update.
     * @param g Graphics
     */
    public void update(Graphics g) {
        paint(g);
    }

    /**
     * Keyevent handler.
     */
    public boolean handleKeyEvent(UserEvent e) {
        if (e.getType() != KeyEvent.KEY_PRESSED) {
            return false;
        }
        int code = e.getCode();
        switch (code) {
        case OCRcEvent.VK_ENTER:
        case KeyEvent.VK_UP:
        case KeyEvent.VK_DOWN:
        case KeyEvent.VK_LEFT:
        case KeyEvent.VK_RIGHT:
            if (isKeyMoving) {
                Log.printDebug("handleKeyEvent = false");
                return false;
            }
        case KeyCodes.LAST:
        case KeyEvent.VK_0:
        case KeyEvent.VK_1:
        case KeyEvent.VK_2:
        case KeyEvent.VK_3:
        case KeyEvent.VK_4:
        case KeyEvent.VK_5:
        case KeyEvent.VK_6:
        case KeyEvent.VK_7:
        case KeyEvent.VK_8:
        case KeyEvent.VK_9:
            Log.printDebug("handleKeyEvent = true");
            return true;
        default:
            Log.printDebug("handleKeyEvent = false");
            return false;
        }
    }
    
    /**
     * Another application isn't start.
     */
    public void notifyClean() {
        Log.printInfo("Loading notifyClean");
    }

    /**
     * Another application is started.
     */
    public void notifyShadowed() {
        Log.printInfo("Loading notifyShadowed");
        hideLoadingAnimation();
    }
}

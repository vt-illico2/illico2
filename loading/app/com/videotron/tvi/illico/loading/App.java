/**
 * @(#)DataManager.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.loading;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
/**
 * The initial class of LoadingAnimation.
 *
 * @author pellos
 *
 */
public class App implements Xlet, ApplicationConfig {
    /** The singleton instance of App. */
    private XletContext xletContext;

    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     *
     * @param ctx
     *            The XletContext of this Xlet.
     * @throws XletStateChangeException
     *             If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
        Log.setApplication(this);
        if (Log.INFO_ON) {
            Log.printInfo("App: initXlet");
        }
        
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     *
     * @throws XletStateChangeException
     *             is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        FrameworkMain.getInstance().start();
        if (Log.INFO_ON) {
            Log.printInfo("App: startXlet");
        }
        LoadingController.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        FrameworkMain.getInstance().pause();
        if (Log.INFO_ON) {
            Log.printInfo("App: pauseXlet");
        }
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     *
     * @param unconditional
     *            If unconditional is true when this method is called, requests by the Xlet to not enter the destroyed
     *            state will be ignored.
     * @throws XletStateChangeException
     *             is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed state). This exception
     *             is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean arg0) throws XletStateChangeException {
        FrameworkMain.getInstance().destroy();

        if (Log.INFO_ON) {
            Log.printInfo("App: destroyXlet");
        }
    }

    /**
     * Returns the name of Application.
     *
     * @return application name.
     */
    public String getApplicationName() {
        return "Loading";
    }

    /**
     * Returns the version of Application.
     *
     * @return version string.
     */
    public String getVersion() {
        return "R5.2.0";
    }

    /**
     * Returns the Application's XletContext.
     *
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

	public void init() {
		LoadingController.getInstance().init(xletContext);
	}

}

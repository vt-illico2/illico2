/**
 * @(#)DataManager.java
 *
 * Copyright $year Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.loading;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;

import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.framework.io.ZipFileReader;
import com.videotron.tvi.illico.loading.ixc.IXCManager;
import com.videotron.tvi.illico.log.Log;

/**
 * This class is that data is converted to config, images from inbandData.
 * @author pellos
 */
public final class DataManager {

    /** The repository root. */
    private static final String REPOSITORY_ROOT = System.getProperty("dvb.persistent.root");
    private static final String DIRECTORY = REPOSITORY_ROOT + "/loading/";
    public static final String CONFIG_FILE = DIRECTORY + "config.txt";
    public static final String BOOTING_FILE = DIRECTORY + "booting.img";

    /** instance of DataManager. */
    private static DataManager dataManager;

    /** inbandData version. */
    private static int version = -1;

    /** booting inbandData version. */
    private static int bootingVersion = -1;

    /** inbandData Image count. */
    private static int loadingImageSize;

    /** Language Type. */
    private String[] languageType;

    /** Booting Language Type. */
    private String[] bootingLanguageType;

    /** Background Image Name. */
    private String backGroundImage;

    /** Booting Image Name. */
    private String[][] bootingImageName;

    /** Image name of InbandData. */
    private String[][] loadingImageName;

    /** is saved image name to DataCenter. */
    private String[] saveImageName;

    /** is saved booting image name to DataCenter. */
    private String[] bootingSaveImageName;

    /** image of first name. */
    private static final String LOADING_NAME = "LOADING";

    /** booting image of first name. */
    private static final String BOOTING_NAME = "BOOTING";

    /** background image of first name. */
    private static final String BACKGROUND_NAME = "BACKGROUND";

    private String backGroundImageName;

    /** Booting data File type. */
    public static final int BOOTING_TYPE = 0;
    /** Loading data File type. */
    public static final int LOADING_TYPE = 1;

    /**
     * Constructor.
     */
    private DataManager() {
        File file = new File(DIRECTORY);
        if (!file.exists()) {
            if (Log.DEBUG_ON) {
                Log.printDebug(file + "is not exist, so mkdirs");
            }
            file.mkdirs();
        }

        try {
            File f1 = new File(CONFIG_FILE);
            File f = new File(BOOTING_FILE);

            readTextFile(f1, DataManager.BOOTING_TYPE);
            readZipFile(f, DataManager.BOOTING_TYPE);
        } catch (Exception ex) {
            Log.print(ex);
        }
    }

    /**
     * Gets the singleton instance of DataManager.
     * @return dataManager
     */
    public static DataManager getInstance() {
        if (dataManager == null) {
            dataManager = new DataManager();
        }
        return dataManager;
    }

    public int getBootingVersion() {
        return bootingVersion;
    }

    /**
     * get saving image name in the DataCenter.
     * @return saveImageName
     */
    public String[] getSaveImageName() {
        return saveImageName;
    }

    /**
     * get booting image name in the DataCenter.
     * @return saveImageName
     */
    public String[] getBootingSaveImageName() {
        return bootingSaveImageName;
    }

    /**
     * get saving booting image name in the DataCenter
     * @return bootingImageName
     */
    public String[][] getBootingImageName() {
        return bootingImageName;
    }

    /**
     * get saving background image name in the DataCenter
     * @return backGroundImage
     */
    public String getBackGroundImageName() {
        return backGroundImageName;
    }

    /**
     * Image zip file read and parser.
     * @param data is data received from inbandData
     */
    public void readZipFile(Object data, int type) throws Exception{
        Log.printInfo("ReadZipFile");
        if (data == null) {
            IXCManager.getInstance().showErrorMessage("LAM500");
            return;
        }
        Hashtable table = ZipFileReader.read((File) data);
        saveImageName = null;

        if (table != null) {
            try {
                flushImage();
                if (type == LOADING_TYPE) {
                    saveImageName = new String[loadingImageName[0].length];
                    for (int a = 0; a < loadingImageName.length; a++) {
                        for (int b = 0; b < loadingImageName[a].length; b++) {
                            Log.printDebug("loadingImageName[" + a + "][" + b + "] = "
                                    + loadingImageName[a][b]);
                            byte[] imageByte = (byte[]) table.get(loadingImageName[a][b]);
                            saveImageName[b] = LOADING_NAME + String.valueOf(b);

                            Log.printDebug("saveImageName[a] = " + saveImageName[b]);

                            if (a == 0) {
                                FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                        saveImageName[b] + "_fr.img");
                            } else {
                                FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                        saveImageName[b] + "_en.img");
                            }
                        }
                    }
                } else {
                    bootingSaveImageName = new String[bootingImageName[0].length];
                    for (int a = 0; a < bootingImageName.length; a++) {
                        for (int b = 0; b < bootingImageName[a].length; b++) {
                            Log.printDebug("bootingImageName[" + a + "][" + b + "] = "
                                    + bootingImageName[a][b]);
                            byte[] imageByte = (byte[]) table.get(bootingImageName[a][b]);
                            bootingSaveImageName[b] = BOOTING_NAME + String.valueOf(b);

                            if (imageByte != null) {
                                if (a == 0) {
                                    FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                            bootingSaveImageName[b] + "_fr.img");
                                } else {
                                    FrameworkMain.getInstance().getImagePool().createImage(imageByte,
                                            bootingSaveImageName[b] + "_en.img");
                                }

                                Log.printDebug("bootingSaveImageName = " + bootingSaveImageName[b]);
                            }
                        }
                    }

                    byte[] imageByte = (byte[]) table.get(backGroundImage);
                    if (imageByte != null) {
                        backGroundImageName = BACKGROUND_NAME + ".img";
                        FrameworkMain.getInstance().getImagePool().createImage(imageByte, backGroundImageName);
                    }
                }
            } catch (Exception e) {
                IXCManager.getInstance().showErrorMessage("LAM502");
                throw e;
            }
        }

        FrameworkMain.getInstance().getImagePool().waitForAll();
        Log.printInfo("readZipFile end ");
    }

    /**
     * flush Image.
     */
    public void flushImage() {
        if (saveImageName != null) {
            for (int b = 0; b < saveImageName.length; b++) {
                FrameworkMain.getInstance().getImagePool().remove(saveImageName[b] + "_fr.img");
                FrameworkMain.getInstance().getImagePool().remove(saveImageName[b] + "_en.img");
            }
        }
    }

    /**
     * read and parser LoadingAnimation config file.
     * @param data is InbandData.
     */
    public void readTextFile(Object data, int type) throws Exception {
        Log.printInfo("readTextFile");
        if (data == null) {
            IXCManager.getInstance().showErrorMessage("LAM500");
            return;
        }

        try {
            byte[] textData = readFile(data);

            if (type == LOADING_TYPE) {
                if (textData != null) {
                    decodeConfigData(textData);
                }
            } else {
                if (textData != null) {
                    decodeBootingConfigData(textData);
                }
            }
        } catch (Exception e) {
            IXCManager.getInstance().showErrorMessage("LAM502");
            throw e;
        }
    }

    /**
     * read config file from data.
     * @param data is InbandData
     * @return buf byte is read from inbandData
     */
    public synchronized static byte[] readFile(Object data) throws Exception {

        File file = (File) data;
        BufferedInputStream bis = null;

        byte[] buf = null;

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        try {
            bis = new BufferedInputStream(new FileInputStream(file));
            int c;
            while ((c = bis.read()) != -1) {
                baos.write(c);
            }

            buf = baos.toByteArray();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                bis.close();
            } catch (IOException e) {
                Log.print(e);
            }
        }

        return buf;

    }

    /**
     * parser config file from byte data.
     * @param data is byte data of inbandData
     */
    public void decodeConfigData(byte[] data) throws Exception{
        int index = -1;

        version = oneByteToInt(data, ++index);
        Log.printDebug("version = " + version);

        int languageTypeSize = oneByteToInt(data, ++index);
        Log.printDebug("languageTypeSize = " + languageTypeSize);

        loadingImageSize = oneByteToInt(data, ++index);
        Log.printDebug("loadingImageSize = " + loadingImageSize);
        loadingImageName = new String[2][loadingImageSize];

        languageType = new String[languageTypeSize];
        int enImageCount = 0;
        int frImageCount = 0;

        for (int a = 0; a < loadingImageSize; a++) {
            int loadingAnimationID = oneByteToInt(data, ++index);
            Log.printDebug(" loadingAnimationID = " + loadingAnimationID);

            for (int b = 0; b < languageTypeSize; b++) {

                languageType[b] = byteArrayToString(data, ++index, 2);
                Log.printDebug(" languageType = " + languageType[b]);
                index++;

                if (languageType[b].equals("fr")) {
                    int loadingImageNameLength = oneByteToInt(data, ++index);
                    loadingImageName[0][frImageCount] = byteArrayToString(data, ++index, loadingImageNameLength);
                    Log.printDebug(" loadingImageName[0][" + frImageCount + "] = "
                            + loadingImageName[0][frImageCount]);
                    index += loadingImageNameLength - 1;
                    frImageCount++;
                } else {
                    int loadingImageNameLength = oneByteToInt(data, ++index);
                    loadingImageName[1][enImageCount] = byteArrayToString(data, ++index, loadingImageNameLength);
                    Log.printDebug(" loadingImageName[1][" + enImageCount + "] = "
                            + loadingImageName[1][enImageCount]);
                    index += loadingImageNameLength - 1;
                    enImageCount++;
                }
            }
        }
    }

    /**
     * parser booting config file from byte data.
     * @param data is byte data of inbandData
     */
    public void decodeBootingConfigData(byte[] data) throws Exception {
        int index = -1;

        bootingVersion = oneByteToInt(data, ++index);
        Log.printDebug("version = " + version);

        int languageTypeSize = oneByteToInt(data, ++index);
        Log.printDebug("languageTypeSize = " + languageTypeSize);

        int backGroundImageLength = oneByteToInt(data, ++index);
        backGroundImage = byteArrayToString(data, ++index, backGroundImageLength);
        Log.printDebug("backGroundImage = " + backGroundImage);
        index += backGroundImageLength - 1;

        int bootImageSize = oneByteToInt(data, ++index);
        Log.printDebug("bootImageSize = " + bootImageSize);
        bootingLanguageType = new String[languageTypeSize];
        bootingImageName = new String[bootingLanguageType.length][bootImageSize];

        int enImageCount = 0;
        int frImageCount = 0;
        for (int a = 0; a < bootImageSize; a++) {
            int bootLoadingID = oneByteToInt(data, ++index);
            Log.printDebug(" bootLoadingID = " + bootLoadingID);
            for (int b = 0; b < languageTypeSize; b++) {

                bootingLanguageType[b] = byteArrayToString(data, ++index, 2);
                Log.printDebug(" bootingLanguageType = " + bootingLanguageType[b]);
                index++;
                if (bootingLanguageType[b].equals("fr")) {
                    int loadingImageNameLength = oneByteToInt(data, ++index);
                    bootingImageName[0][frImageCount] = byteArrayToString(data, ++index, loadingImageNameLength);
                    index += loadingImageNameLength - 1;
                    Log.printDebug(" bootingImageName[0][" + frImageCount + "] = "
                            + bootingImageName[0][frImageCount]);
                    frImageCount++;
                } else {
                    int loadingImageNameLength = oneByteToInt(data, ++index);
                    bootingImageName[1][enImageCount] = byteArrayToString(data, ++index, loadingImageNameLength);
                    index += loadingImageNameLength - 1;
                    Log.printDebug(" bootingImageName[1][" + enImageCount + "] = "
                            + bootingImageName[1][enImageCount]);
                    enImageCount++;
                }
            }
        }
    }

    /**
     * convert byte to int.
     * @param data is all byte date.
     * @param offset is offset byte.
     * @return int read byte
     */
    public static int oneByteToInt(byte[] data, int offset) {
        return (((int) data[offset]));
    }

    /**
     * convert byte to String.
     * @param data is all byte date.
     * @param offset is offset byte.
     * @param length is read byte length.
     * @return result is String that converting byte
     */
    public static String byteArrayToString(byte[] data, int offset, int length) {
        String result = null;
        try {
            result = new String(data, offset, length);
        } catch (Exception uee) {
            Log.print(uee);
            return null;
        }
        return result;
    }

    /**
     * get Image count.
     * @return image count.
     */
    public int getLoadingImageLength() {
        if (loadingImageName == null) {
            return 0;
        }
        return loadingImageName[0].length;
    }

    /**
     * It write a data in flash memory.
     * @param data data.
     * @param fileName path/fileName.
     */
    public static void cashWriteData(Object data, String fileName) {
        Log.printInfo("WriteData fileName = " + fileName);

        if (data == null) {
            return;
        }
        FileOutputStream fos = null;
        File file = null;
        try {
            file = new File(DIRECTORY);
            if (!file.exists()) {
                file.mkdirs();
            }
            byte[] b = readFile(data);
            fos = new FileOutputStream(fileName);
            fos.write(b);
            Log.printInfo("WriteData End");
        } catch (FileNotFoundException e) {
            Log.print(e);
        } catch (IOException e) {
            Log.print(e);
        } catch (Exception e) {
            Log.print(e);
        } finally {
            try {
                file = null;
                fos.close();
            } catch (IOException e1) {
                Log.print(e1);
            }
        }
    }
}

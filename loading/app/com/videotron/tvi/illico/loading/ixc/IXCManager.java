/**
 * @(#)IXCManager.java
 *
 * Copyright 1999-2010 Alticast Corp, Inc. All rights reserved.
 */
package com.videotron.tvi.illico.loading.ixc;

import java.awt.Point;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.davic.resources.ResourceStatusEvent;
import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.loading.App;
import com.videotron.tvi.illico.loading.LoadingAnimation;
import com.videotron.tvi.illico.log.Log;

/**
 * This class does IXC. Let's starts other applications.
 * @author pellos
 */
public final class IXCManager implements LoadingAnimationService {

    /** The instance of IXCManager. */
    private static IXCManager ixcManager;

    /** The xlet context. */
    private XletContext xletContext;

    /** The remote object of MonitorService. */
    private MonitorService monitorService;
    private ErrorMessageService errorMgsService;

    /** Retry waiting Time for IXC Connecting. */
    private static final long RETRY_LOOKUP_MILLIS = 3000;

    /** turning channel listener for receiving inbandData. */
    private InbandListener inbandListener = new InbandListener();

    private int version = 0;
    private int bootingVersion = 0;

    /**
     * Gets the singleton instance of IXCManager.
     * @return single instance of IXCManager
     */
    public static IXCManager getInstance() {

        if (ixcManager == null) {
            ixcManager = new IXCManager();
        }
        return ixcManager;
    }

    /** Constructor. */
    private IXCManager() {

    }

    /**
     * is called before IXC.
     * @param xlet is XletContext.
     */
    public void init(XletContext xlet) {
        this.xletContext = xlet;
        bindToIxc();
        lookupMonitorService();
    }

    /**
     * Binds a remote Object of LoadingAnimation to communicate with application which want to use LoadingAnimation.
     */

    public void bindToIxc() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo("LoadingAnimationService: bind to IxcRegistry");
            }
            IxcRegistry.bind(xletContext, LoadingAnimationService.IXC_NAME, this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lookup monitor service to receive Status Event.
     * @see {@link #statusChanged(ResourceStatusEvent)}
     */
    public void lookupMonitorService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("lookupMonitorService: called");
        }

       Thread th = new Thread("LoadingMonitor") {
            public void run() {
                int count = 0;
                while (true) {
                    try {
                        /** The path of MonitorService. */
                        String lookupName = "/1/1/" + MonitorService.IXC_NAME;
                        monitorService = (MonitorService) IxcRegistry.lookup(xletContext, lookupName);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (monitorService != null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("IXCManager.lookupmonitorService: monitorService is looked up.");
                        }

                        try {
                            String appName = FrameworkMain.getInstance().getApplicationName();
                            monitorService.addInbandDataListener(inbandListener, "Loading");

                            Log.printInfo("monitorService Lookup completed = " + appName);
                            Log.printInfo("monitorService Lookup completed inbandListener = " + inbandListener);

                            if (Log.INFO_ON) {
                                Log.printInfo("monitorService Lookup completed");
                            }
                            break;
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    }
                    if (Log.DEBUG_ON) {
                        Log.printDebug("monitorService Lookup fail. sleep & try again ... count = " + count);
                    }
                    try {
                        Thread.sleep(RETRY_LOOKUP_MILLIS);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                    count++;
                }
            }
        };
        th.start();
    }

    public void lookupErrorMessageService() {
        if (Log.DEBUG_ON) {
            Log.printDebug("lookupErrorervice: called");
        }

        Thread th = new Thread("PastilleSTCIXC") {
            public void run() {
                int count = 0;
                while (true) {
                    try {
                        String lookupName = "/1/2025/" + ErrorMessageService.IXC_NAME;
                        errorMgsService = (ErrorMessageService) IxcRegistry.lookup(xletContext, lookupName);
                    } catch (Exception e) {
                        // e.printStackTrace();
                    }

                    if (errorMgsService != null) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug("IXCManager.lookupErrorService: errorService is looked up.");
                        }
                        break;
                    }

                    try {
                        Thread.sleep(RETRY_LOOKUP_MILLIS);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                    count++;
                }
            }

        };
        th.start();
    }

    /**
     * get MonitorService.
     * @return monitorService
     */
    public MonitorService getMonitorService() {
        return monitorService;
    }

    /**
     * Called by {@link App#destroyXlet(boolean)} when LoadingAnimation Application destroy.
     */
    public void destroy() {
        try {
            IxcRegistry.unbind(xletContext, LoadingAnimationService.IXC_NAME);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Implements {@link LoadingAnimationService#hideLoadingAnimation()}. LoadingAnimaition is used Data loaded or
     * Application loaded
     * @throws RemoteException If a remote stub class cannot be called.
     * @see com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService#hideLoadingAnimation()
     */
    public void hideLoadingAnimation() throws RemoteException {
        Log.printInfo("ixc hideLoadingAnimation");
        LoadingAnimation.getInstance().hideLoadingAnimation();
    }

    /**
     * Implements {@link LoadingAnimationService#showLoadingAnimation()}. LoadingAnimaition is used Data loading or
     * Application loading
     * @throws RemoteException If a remote stub class cannot be called.
     * @see com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService#showLoadingAnimation()
     */
    public void showLoadingAnimation(Point p) throws RemoteException {
        Log.printInfo("showLoadingAnimation");
        LoadingAnimation.getInstance().showLoadingAnimation(p, true);
    }

    /**
     * This class is turning channel listener for receiving inbandData.
     * @author pellos
     */
    class InbandListener implements InbandDataListener {

        /**
         * channel turn Listener.
         * @param locator InbandData Name.
         */
        public void receiveInbandData(String locator) {
            if (Log.DEBUG_ON) {
                Log.printDebug("receiveInbandData = " + locator);
            }
            int newVersion = 0;
            int newBootingVersion = 0;
            try {
                newVersion = monitorService.getInbandDataVersion(MonitorService.loading_animation_config);
                newBootingVersion = monitorService.getInbandDataVersion(MonitorService.booting_animation_config);
                Log.printDebug("receiveInbandData newVersion = " + newVersion);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }

            if((newVersion != version) || (bootingVersion != newBootingVersion)){
                version = newVersion;
                bootingVersion = newBootingVersion;
                DataAdapterManager.getInstance().getInbandAdapter().synchronousLoad(locator);
            }

            try {
                monitorService.completeReceivingData(FrameworkMain.getInstance().getApplicationName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void showErrorMessage(String code) {
        if (errorMgsService != null) {
            try {
                errorMgsService.showErrorMessage(code, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void showNotDelayLoadingAnimation(Point p) throws RemoteException {
        Log.printInfo("showLoadingAnimation");
        LoadingAnimation.getInstance().showLoadingAnimation(p, false);
    }
    
    public void isKeyMoving(boolean flag) throws RemoteException { 
        Log.printInfo("isKeyMoving = " + flag);
        LoadingAnimation.getInstance().isKeyMoving(flag);
    }

}

﻿This is Monitor App Release Notes.
GENERAL RELEASE INFORMATION
 - Release Details
    - Release version
      : version 1.2.0.0
    - Release file name
      : R3_Monitor.zip
    - Release type (official, debug, test,...)
      : official       
    - Release date
      : 2013.11.15

***
version 1.2.0.0
    - New Feature
        + Added a new unbound application - ISA
    - Resolved Issues
		+ N/A


package com.videotron.tvi.illico.monitor.main;

import java.lang.reflect.Method;

import javax.tv.xlet.XletContext;

import org.ocap.hardware.Host;

import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.communication.MonitorServiceImpl;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxy;
import com.videotron.tvi.illico.monitor.core.AodManager2;
import com.videotron.tvi.illico.monitor.core.AppLauncher;
import com.videotron.tvi.illico.monitor.core.AuthorizationManager;
import com.videotron.tvi.illico.monitor.core.AutoStandbyManager;
import com.videotron.tvi.illico.monitor.core.InbandDataManager;
import com.videotron.tvi.illico.monitor.core.InbandDataUpdateController;
import com.videotron.tvi.illico.monitor.core.InternalInbandDataListener;
import com.videotron.tvi.illico.monitor.core.MonitorKeyHandler;
import com.videotron.tvi.illico.monitor.core.STBModeManager;
import com.videotron.tvi.illico.monitor.core.ScreenSaverManager;
import com.videotron.tvi.illico.monitor.core.ServiceStateManager;
import com.videotron.tvi.illico.monitor.core.StandaloneModeManager;
import com.videotron.tvi.illico.monitor.core.StandbyAndRunningModeManager;
import com.videotron.tvi.illico.monitor.core.StateManager;
import com.videotron.tvi.illico.monitor.core.TransitionEvent;
import com.videotron.tvi.illico.monitor.core.UnboundAppProxy;
import com.videotron.tvi.illico.monitor.core.UnboundAppsManager;
import com.videotron.tvi.illico.monitor.core.WatchingTimeRestrictionManager;
import com.videotron.tvi.illico.monitor.core.WeakSignalController;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.DataVersionManager;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;
import com.videotron.tvi.illico.monitor.data.MonitorConfigurationManager;
import com.videotron.tvi.illico.monitor.system.CANHManager;
import com.videotron.tvi.illico.monitor.system.DeferredDownloadManager;
import com.videotron.tvi.illico.monitor.system.UnboundAppUpdateManager;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.Environment;
import org.ocap.system.RegisteredApiManager;
import java.io.*;

/**
 * The main class of monitor application.
 *
 * @author Woosik Lee
 */
public class MonitorMain implements InternalInbandDataListener {

	/** The singleton instance of MonitorMain. */
	private static MonitorMain instance;
	/** The XletContext that use IXC communication. */
	private XletContext xletContext;
	/** The instance of UserEventManager. */
	private MonitorKeyHandler monitorKeyHandler;
	/** The instance of UnboundAppUpdateManager. */
	private UnboundAppUpdateManager unboundAppUpdateManager;
	/** The flag of the Menu initialization state. */
	private boolean readyMenu;
	/** The flag of the EPG initialization state. */
	private boolean readyEpg;
	/** The flag of the VOD initialization state. */
	private boolean readyVOD;
	/** The retry delay time for lookup IXC. */
	private static final long IXC_LOOKUP_RETRY_TIME = 2000L;
	/** The retry count for lookup IXC. */
	private static final int IXC_LOOKUP_RETRY_COUNT = 20;

	public static final int BOOT_STATE_UNKNOWN = 1;
	public static final int BOOT_STATE_NOW = 2;
	public static final int BOOT_STATE_FINISHED = 100;

	private int currentBootState = BOOT_STATE_UNKNOWN;

    public static final boolean IB_EMULATION = Environment.EMULATOR;
    public static final boolean CA_EMULATION = Environment.EMULATOR;

	/** The log header message. */
	private static final String LOG_HEADER = "[MonitorMain] ";

    //->Kenneth[2015.5.26] R5 : JSON parser 공유
	private static final String REGISTERED_API_NAME = "reg_api_json_parser";
	private static final short REGISTERED_API_STORAGE_PRIOR = 123;

	/**
	 * Gets the singleton instance of MonitorMain.
	 *
	 * @return instance of MonitorMain.
	 */
	public static synchronized MonitorMain getInstance() {
		if (instance == null) {
			instance = new MonitorMain();
		}
		return instance;
	}

	/**
	 * Initials this class.
	 *
	 * @param xls
	 *            The instance of XletContext.
	 */
	public void init(XletContext xls) {
		this.xletContext = xls;
        //->Kenneth[2015.3.25] 4K : EID OOB update 가 listener 걸기 전에 완료되는 경우
        // SUPPORT_UHD 를 UP setting 해서 다른 app 에게 전파하는 logic 동작하지 않음.
        // 따라서 EIDDataManager instance 를 먼저 생성해서 그 안에 있는 listener 가 빨리
        // 등록되도록 한다.
        // 일단 없애미
        //if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER+"MonitorMain.init() : Call EIDDataManager.getInstance() earlier");
        //EIDDataManager.getInstance();

        //->Kenneth[2015.5.26] R5 : JSON parser 공유
		try {
			RegisteredApiManager rm = RegisteredApiManager.getInstance();
			rm.register(REGISTERED_API_NAME, MonitorXlet.VERSION, new File(
					"./scdf.xml").getAbsoluteFile(),
					REGISTERED_API_STORAGE_PRIOR);
			String[] rAPINames = rm.getNames();
			if (rAPINames != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("[Monitor.registerAPI]Registered API Name Count : "
									+ rAPINames.length);
				}
				for (int i = 0; i < rAPINames.length; i++) {
					String rAPIName = rAPINames[i];
					if (rAPIName == null) {
						continue;
					}
					String rAPIVersion = rm.getVersion(rAPIName);
					if (Log.DEBUG_ON) {
						Log.printDebug("[Monitor.registerAPI]" + i
								+ "-Name[" + rAPIName + "], Versiont["
								+ rAPIVersion + "]");
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			if (Log.DEBUG_ON) {
				Log.printDebug("[FlipBar.registerAPI]Exception occurred");
			}
		}
	}

	public int getCurrentBootState() {
		return currentBootState;
	}

	public MonitorKeyHandler getMonitorKeyHandler() {
		return monitorKeyHandler;
	}

	/**
	 * Starts this class. It performs the booting process.
	 */
	public void start() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "start");
		}
		try {
			org.ocap.OcapSystem.monitorConfiguringSignal(0, 0);
		} catch (Throwable e) {
			Log.print(e);
		}
		// Initialize LayeredUI.
		LayeredUIManager.getInstance();

		// Make font.
		makeFont();

		CANHManager.getInstance().start(xletContext);

		currentBootState = BOOT_STATE_NOW;

		MonitorServiceImpl.getInstance().start(xletContext);
		// Start Daemon, Semi-Daemon Application.
		UnboundAppsManager.getInstance().start();
		DataManager.getInstance();
		// IXC Lookup
		CommunicationManager.getInstance().start(xletContext);
		unboundAppUpdateManager = new UnboundAppUpdateManager();
		unboundAppUpdateManager.start();

		// Test for Terminal blocked time ////////////////////
		// String stTime = WatchingTimeRestrictionManager.makeHHMM12Hour(13,
		// 25);
		// String endTime = WatchingTimeRestrictionManager.makeHHMM12Hour(3,
		// 55);
		// WatchingTimeRestrictionManager.getInstance().showTerminalBlockedScene(stTime,
		// endTime);
		// /////////////////////////////////

		StandbyAndRunningModeManager.getInstance();
	}

	/**
	 * Checks the activation package until activated.
	 */
	/*
	 * private void checkActivationPackage() { int activationAuthority =
	 * AuthorizationManager.getInstance().checkActivationPackage(); if
	 * (Log.INFO_ON) {
	 * Log.printInfo(LOG_HEADER + "activateionAuthority = " +
	 * AuthorizationManager.getResultString(activationAuthority)); }
	 *
	 * if (activationAuthority == AuthorizationManager.CHECK_RESULT_AUTHORIZED)
	 * { try { MainMenuService menuService =
	 * CommunicationManager.getInstance().getMainMenuService();
	 * menuService.setDeactivatedSTB(false); } catch (Exception e) {
	 * Log.print(e); } } else { if (activationAuthority ==
	 * AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED) { try { MainMenuService
	 * menuService = CommunicationManager.getInstance().getMainMenuService();
	 * menuService.setDeactivatedSTB(true); } catch (Exception e) {
	 * Log.print(e); } } } }
	 */

	private void checkPackageAuthority() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkPackageAuthority()");
		}

		// 1. Checks the Mandatory and Activation package.
		AuthorizationManager.getInstance().decideStbStateByAuthority();
		/*
		 * // 1. Checks the activation package //checkActivationPackage();
		 *
		 * // 2. Checks the mandatory package. int mandatoryAuthority =
		 * AuthorizationManager.getInstance().checkMandatoryPackage(); if
		 * (Log.INFO_ON) {
		 * Log.printInfo(LOG_HEADER + "mandatoryAuthority = " +
		 * AuthorizationManager.getResultString(mandatoryAuthority)); } switch
		 * (mandatoryAuthority) { case
		 * AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED: // Wow! this STB is
		 * not authorized. All service will be blocked! if (Log.INFO_ON) {
		 * Log.printInfo(
		 * LOG_HEADER + "This STB is not authorized. All service should blocked!"
		 * ); }
		 *
		 * AuthorizationManager.getInstance().startMandatoryBlocked(); return
		 * false; }
		 */
		// 3. Checks the Standalone package
		int standaloneAuthority = AuthorizationManager.getInstance().checkStandalonePackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "standaloneAuthority = "
					+ AuthorizationManager.getResultString(standaloneAuthority));
		}

		// 4. Checks the iPPV Deactivation package
		AuthorizationManager.getInstance().decideIPPVDeactivationPackage();

	}

	/**
	 * Calls when all daemon, semi-daemon applications started completely.
	 */
	public void completedAllDaemonApplicationLauching() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "completedAllDaemonApplicationLauching");
		}
		InbandDataManager.getInstance().status.updateLog("completedAllDaemonApplicationLauching");

		// Reads the version.txt in the OOB.
		DataVersionManager.getInstance();

		MonitorConfigurationManager.getInstance();
		// The last step in this method. Start receiving inband data.
		InbandDataManager.getInstance().startReceivingData(this);
	}

	/**
	 * Implementation of InternalInbandDataListener.
	 */
	public void finishReceivingData() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "finishReceivingData()");
		}
		completeInbandDataReceive();
	}

	/**
	 * If complete receiving Inband data, this method is called and performs the
	 * next step.
	 */
	private void completeInbandDataReceive() {
		startVODTSScan();
	}

	/**
	 * Inband 수신을 모두 다 했으니 VOD TS Scan을 수행 한다. 만약 VOD IXC 를 Lookup 하지 못 했다면 일정
	 * 시간 동안 시도 한다. 일정 시간 이후에도 Lookup 하지 못하면 readyApplication(VOD) 를 수행 해서 부팅을
	 * 진행 하게 하자.
	 */
	private void startVODTSScan() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "completeInbandDataReceive");
		}
		InbandDataManager.getInstance().status.updateLog("startVODTSScan");

		String vodName = DataManager.getInstance().getVodName();
		UnboundAppProxy vodProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(vodName);
		if (vodProxy == null) {
			if (Log.ERROR_ON) {
				Log.printError(LOG_HEADER + "Not found VOD application.");
			}
			readyApplication(DataManager.getInstance().getVodName(), false);
			return;
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "try to VOD TS Scan start!");
		}
		VODService vodService = CommunicationManager.getInstance().getVODService();
		if (vodService == null) {
			if (Log.ERROR_ON) {
				Log.printError(LOG_HEADER + "ERROR VODService is null!");
			}
			boolean readyVodIXC = false;
			int lookupVODCount = 0;
			while (!readyVodIXC) {
				if (Log.WARNING_ON) {
					Log.printWarning(LOG_HEADER + "VODService lookup loop wait 2 sec.");
				}
				try {
					Thread.sleep(IXC_LOOKUP_RETRY_TIME);
				} catch (Exception e) {
					Log.print(e);
				}
				vodService = CommunicationManager.getInstance().getVODService();
				if (vodService != null) {
					readyVodIXC = true;
					break;
				}
				lookupVODCount++;
				if (lookupVODCount > IXC_LOOKUP_RETRY_COUNT) {
					if (Log.ERROR_ON) {
						Log.printError(LOG_HEADER + "ERROR giveup to lookup VOD IXC.");
					}
					// VOD Lookup 에 실패 했다. Ready 처리 하고 부팅을 계속 하자.
					// Fail to lookup VOD's IXC so process next stop.
					readyApplication(DataManager.getInstance().getVodName(), false);
					return;
				}
			}
		}
		try {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "vodService.startAutoDiscovery()");
			}
			if (vodService != null) {
				vodService.startAutoDiscovery();
			}
		} catch (Exception e) {
			Log.print(e);
		}

	}

	/**
	 * It performs the last step of booting process. It is called when other
	 * applications have been initialized.
	 */
	private void finishBootingProcess() {
		monitorConfiguredSignal();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "finishBootingProcess() STB mode is "
					+ STBModeManager.getInstance().getMode());
		}
		InbandDataManager.getInstance().status.updateLog("finishBootingProcess");

		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if (!preManager.isReadyPreferenceService()) {
			InbandDataManager.getInstance().status.updateLog("wait preference service");
			int retryCount = 0;
			while (true) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "WARNING!! Wait preference service ready. retry count="
							+ retryCount);
				}
				try {
					Thread.sleep(1000L);
					retryCount++;
					if (preManager.isReadyPreferenceService()) {
						break;
					}
					if (retryCount > 30) {
						break;
					}
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}


		if (STBModeManager.getInstance().getMode() == MonitorService.STB_MODE_STANDALONE) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Standalone mode start.");
			}

			currentBootState = BOOT_STATE_FINISHED;

			InbandDataManager.getInstance().status.stop();
			CommunicationManager.getInstance().stopLoadingAnimation();

			StandaloneModeManager.getInstance().start();
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);

			monitorKeyHandler = new MonitorKeyHandler();
			monitorKeyHandler.start();

			ScreenSaverManager.getInstance().start(xletContext);
			AutoStandbyManager.getInstance().bootComplete();

			WeakSignalController.getInstance();
			// In the Standalone mode, need not StandbyAndRunningModeManager.
			// To control power state is controlled by StandaloneModeManager.
			StandbyAndRunningModeManager.getInstance().dispose();

			return;
		} else if (STBModeManager.getInstance().getMode() == MonitorService.STB_MODE_NO_SIGNAL) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "No signal mode start.");
			}

			currentBootState = BOOT_STATE_FINISHED;

			InbandDataManager.getInstance().status.stop();
			CommunicationManager.getInstance().stopLoadingAnimation();

			// Shows popup.
			// CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_506, null);

			UnboundAppsManager.getInstance().destroyAllUnboundApplicationWithoutErrorApplication();

			WeakSignalController.getInstance().showNoSignalPopup();

			return;
		}

		InbandDataManager.getInstance().status.stop();

		int currentPowerMode = Host.getInstance().getPowerMode();
		if(Log.DEBUG_ON) {
			Log.printDebug("current power mode is " + currentPowerMode);
		}

		if(currentPowerMode == Host.LOW_POWER) {
			CommunicationManager.getInstance().stopLoadingAnimation();
		}
		else {
			if (STBModeManager.getInstance().getMode() != MonitorService.STB_MODE_MANDATORY_BLOCKED) {
				doInitialAction();
			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Do not tune to channel because this STB blocked by Mandatory package.");
				}
			}
		}
		
		if(STBModeManager.getInstance().getMode() == MonitorService.STB_MODE_BOOTING) {
			STBModeManager.getInstance().setMode(MonitorService.STB_MODE_NORMAL);
		}
		
		WatchingTimeRestrictionManager.getInstance().bootComplete();

		monitorKeyHandler = new MonitorKeyHandler();
		monitorKeyHandler.start();
		ScreenSaverManager.getInstance().start(xletContext);

		currentBootState = BOOT_STATE_FINISHED;
		AutoStandbyManager.getInstance().bootComplete();

		WeakSignalController.getInstance();

		InbandDataUpdateController.init();

		new DeferredDownloadManager();
		
		AodManager2.getInstance();
	}

	private void doInitialAction() {
		if(Log.INFO_ON) {
			Log.printInfo("doInitialAction()");
		}

		if(AuthorizationManager.getInstance().checkWizardNeeded()) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "wizard app is started so do not select channel and Menu");
			}
			// Set flag for Menu' initial state.
			if(CommunicationManager.getInstance().getMainMenuService() != null) {
	            try {
	            	if(Log.DEBUG_ON) {
	            		Log.printDebug(LOG_HEADER + "call Menu getTargetMenuState()");
	            	}
	                CommunicationManager.getInstance().getMainMenuService().getTargetMenuState();
	            } catch (Exception ex) {
	                Log.print(ex);
	            }
            }
			
			// Should wait until wizard start.
			try {
				Thread.sleep(3000L);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return;
		}
		
		// Blocked time check - It is synchronized call.
		boolean isBlockedTime = WatchingTimeRestrictionManager.getInstance().isBlockedTime();
		if(isBlockedTime) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Now blocked time so don't select channel and Menu");
			}
			// Set flag for Menu' initial state.
			if(CommunicationManager.getInstance().getMainMenuService() != null) {
	            try {
	            	if(Log.DEBUG_ON) {
	            		Log.printDebug(LOG_HEADER + "call Menu getTargetMenuState()");
	            	}
	                CommunicationManager.getInstance().getMainMenuService().getTargetMenuState();
	            } catch (Exception ex) {
	                Log.print(ex);
	            }
            }
			return;
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "change STB mode STB_MODE_NORMAL and start Menu");
		}
		try {
			CommunicationManager.getInstance().getEpgService().changeChannel(TransitionEvent.LCW_SOURCE_ID);
		} catch (Exception e) {
			Log.print(e);
		}

		boolean menuAuth = AppLauncher.getInstance().startUnboundApplication(
				DataManager.getInstance().getMenuName(), null, false);
		if (!menuAuth) {
			AppLauncher.getInstance().exitToChannelWithoutMenu(TransitionEvent.LCW_SOURCE_ID);
		}

		CommunicationManager.getInstance().stopLoadingAnimation();
	}

	public XletContext getXletContext() {
		return xletContext;
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "dispose()");
		}

		try {
			CANHManager.getInstance().dispose();
			if (monitorKeyHandler != null) {
				monitorKeyHandler.dispose();
			}
			StateManager.getInstance().dispose();
			CommunicationManager.getInstance().dispose();
			if (unboundAppUpdateManager != null) {
				unboundAppUpdateManager.dispose();
			}
			EIDDataManager.getInstance().dispose();
			AutoStandbyManager.getInstance().dispose();
			InbandDataManager.getInstance().dispose();
			ScreenSaverManager.getInstance().dispose();
			ServiceStateManager.getInstance().dispose();
			StandbyAndRunningModeManager.getInstance().dispose();
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * During the STB boot, this method checks the applications finished the
	 * initialization.
	 *
	 * @param appName
	 *            the application name to notify
	 * @param isReady
	 *            if true ready the application, false application has some
	 *            error or exception.
	 */
	public synchronized void readyApplication(String appName, boolean isReady) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "readyApplication appName=" + appName + ", isReady=" + isReady);
		}

		/*
		if (appName.equalsIgnoreCase(DataManager.getInstance().getWizardName())) {
			synchronized (this) {
				UnboundAppProxy wizard = UnboundAppsManager.getInstance().getUnboundAppProxy(
						DataManager.getInstance().getWizardName());
				wizard.stop();
				this.notifyAll();
			}
		}
		*/

		if (readyMenu && readyEpg && readyVOD) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "already finished boot. so ignore this request.");
			}
			return;
		}

		if (appName.equals(DataManager.getInstance().getVodName()) && isReady && readyVOD) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Already ready VOD so no action");
			}
			return;
		}
		if (appName.equals(DataManager.getInstance().getEPGName()) && isReady && readyEpg) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Already ready Epg so no action");
			}
			return;
		}
		if (appName.equals(DataManager.getInstance().getMenuName()) && isReady && readyMenu) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Already ready Menu so no action");
			}
			return;
		}

		if (appName.equals(DataManager.getInstance().getVodName())) {
			readyVOD = true;
			// Next step is SDV channel selection by EPG.
			Thread sdvSelectThread = new Thread() {
				public void run() {
					checkAuthrity();
				}
			};
			sdvSelectThread.start();

		} else if (appName.equals(DataManager.getInstance().getEPGName())) {
			if (!isReady) {
				readyEpg = false;
				// TODO go to the watch TV mode.
			} else {
				readyEpg = true;
			}
		} else if (appName.equals(DataManager.getInstance().getMenuName())) {
			readyMenu = true;
		} else {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "No action. Why call this method? Who are you?");
			}
			return;
		}


		if (readyMenu && readyEpg && readyVOD) {
			Thread finishingBootThread = new Thread("finishingBootThread") {
				public void run() {
					finishBootingProcess();
				}
			};
			finishingBootThread.start();
		}
	}

	private void checkAuthrity() {
		InbandDataManager.getInstance().status.updateLog("checkAuthrity");
		EIDDataManager.init();
		if (!CA_EMULATION) {
			checkPackageAuthority();
		}

		startSDVChannelSelectionByEPG();
	}

	/**
	 * Calls the EPG function for select SDV channel by EPG.
	 */
	private void startSDVChannelSelectionByEPG() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startSDVChannelSelectionByEPG()");
		}
		InbandDataManager.getInstance().status.updateLog("startSDVChannelSelectionByEPG");

		EpgService epgService = CommunicationManager.getInstance().getEpgService();
		if (epgService == null) {
			while (true) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "WARNING EPGService is null!");
				}
				boolean readyEpgIXC = false;
				int lookupEpgCount = 0;
				while (!readyEpgIXC) {
					if (Log.WARNING_ON) {
						Log.printWarning(LOG_HEADER + "EPGService lookup loop wait 2 sec.");
					}
					try {
						Thread.sleep(IXC_LOOKUP_RETRY_TIME);
					} catch (Exception e) {
						Log.print(e);
					}
					epgService = CommunicationManager.getInstance().getEpgService();
					if (epgService != null) {
						readyEpgIXC = true;
						break;
					}
					lookupEpgCount++;
					if (lookupEpgCount > IXC_LOOKUP_RETRY_COUNT) {
						if (Log.ERROR_ON) {
							Log.printError(LOG_HEADER + "ERROR giveup to lookup EPG IXC.");
						}
						// Fail to lookup EPG's IXC so process next stop.
						// readyApplication(DataManager.getInstance().getVodName(),
						// false);
						// TODO Go to the Watch TV Mode. How to?
						return;
					}
				}
			}
		}

		try {
			epgService.selectSdvHomeChannel();
		} catch (Exception e) {
			Log.print(e);
		}

	}

	/**
	 * Calls the OcapSystem#monitorConfiguredSignal().
	 */
	private void monitorConfiguredSignal() {
		try {
			org.ocap.OcapSystem.monitorConfiguredSignal();
		} catch (Throwable t) {
			// It is the code for the emulator.
			// Before the OCAP specification 1.1.1, monitorConfiguredSignal()
			// has two parameters.
			// In the OCAP specification 1.1.1, monitorConfiguredSignal() has no
			// parameter.
			// Unfortunately the emulator I have not supports the OCAP
			// specification 1.1.1.
			Log.print(t);
			try {
				Class ocapSystemClass = Class.forName("org.ocap.OcapSystem");
				Class[] param = { Integer.TYPE, Integer.TYPE };
				Method m1 = ocapSystemClass.getMethod("monitorConfiguredSignal", param);

				Object[] parameters = { new Integer(0), new Integer(0) };
				m1.invoke(ocapSystemClass, parameters);
			} catch (Exception e) {
				Log.print(e);
			}
		}

	}

	private void makeFont() {
		for (int i = 14; i < 33; i++) {
			FontResource.BLENDER.getFont(i, true);
		}
		FontResource.DINMED.getFont(14, true);
		FontResource.DINMED.getFont(15, true);
	}

}

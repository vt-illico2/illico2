package com.videotron.tvi.illico.monitor.main;

import javax.tv.xlet.Xlet;
import javax.tv.xlet.XletContext;
import javax.tv.xlet.XletStateChangeException;

import com.videotron.tvi.illico.framework.ApplicationConfig;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * The Xlet class of Monitor.
 * @author Woosik Lee
 */
public class MonitorXlet implements Xlet, ApplicationConfig {

    /** The XletContext. */
    private XletContext xletContext;
    //->Kenneth[2015.5.26] R5 : MonitorMain 에서 써야 해서 VERSION 을 추가
    //public static final String VERSION = "R5.1.0_01";//JSON parser registered 로 공유
    //public static final String VERSION = "R5.1.0_beta";//6.1 초기 릴리즈|6.6 VDTR-5428쥴리 요청반영
    //public static final String VERSION = "R5.1.x_beta01";//6.9 VDTRMASTER-5301
    //public static final String VERSION = "R5.1.2";//6.17 R5 첫 공식 릴리즈|VDTRMASTER-5428, 5301
    //public static final String VERSION = "R5.2.x";//6.29 Tank : showListPressedInNonPvrPopup() 추가
    //public static final String VERSION = "R5.2.x_01";//7.2 Tank : A 키도 위젯띄우게 함
    //public static final String VERSION = "R5.2.x_02";//7.8 Tank : MAIN_MENU_STATE_FULL_MENU 없앰
    //public static final String VERSION = "R5.2.x_03";//7.15 Tank : version.txt 에 portal_brand_images 추가됨
    //public static final String VERSION = "R5.2.x_04";//7.17 version.txt 에 업데이트 된 놈 없어로 로그 찍게함.
    //public static final String VERSION = "R5.2.x_05";//7.17 version.txt 에 있는 내용과 맞춤
    //public static final String VERSION = "R5.2.0";//7.20 VT 두번째 공식 릴리즈
    //public static final String VERSION = "R5.3.x_01";//8.4 : VDTRMASTER-5542
    //public static final String VERSION = "R5.3.1";//8.10 : VT 세번째 공식 릴리즈 
    //public static final String VERSION = "R5.4.x_01";//8.26 : VDTRMASTER-5594 
    //public static final String VERSION = "R5.4.1";//8.28 : VT 네번째 공식 릴리즈
    //public static final String VERSION = "R5.5.x_01";//8.31 : VDTRMASTER-5528
    //public static final String VERSION = "R5.5.1";//9.10 : VT 5번째 공식 릴리즈
    //public static final String VERSION = "R7_2.1.x";// 2016.7.15 : Zoom 키 처리 하지 않도록 함. (EPG 가 처리할 것임)
    //public static final String VERSION = "(R7.3).1.x.1";//2017.03.11 APD관련 수정 
    //public static final String VERSION = "(R7.3).1.1";//2017.03.27 VDTRMASTER-6030
    //public static final String VERSION = "(R7.3).2.1";//2017.04.11 VDTRMASTER-6030
    //public static final String VERSION = "(R7.3).3.1";//2017.04.26 VDTRMASTER-6125 : VBM 로그를 남기기 위해서 Search키를 monitor 에서는 더이상 처리하지 않음. EPG 가 처리함.
    //public static final String VERSION = "(R7.4).1.0";//2017.08.31 Block Admin PIN : TOC 변경 from unblock terminal
    //public static final String VERSION = "(R7.4).2.1";//2017.10.18 VDTRMASTER-6218 : R7.4 두번째 릴리즈. JIRA improvement 때문임.
    public static final String VERSION = "(R7.5).0.1";
    /**
     * Signals the Xlet to initialize itself and enter the Paused state.
     * @param ctx The XletContext of this Xlet.
     * @throws XletStateChangeException If the Xlet cannot be initialized.
     */
    public void initXlet(XletContext ctx) throws XletStateChangeException {
        xletContext = ctx;
        FrameworkMain.getInstance().init(this);
    }

    public void init() {
    	MonitorMain.getInstance().init(xletContext);
    }

    /**
     * Signals the Xlet to start providing service and enter the Active state.
     * @throws XletStateChangeException is thrown if the Xlet cannot start providing service.
     */
    public void startXlet() throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("MonitorXlet: startXlet");
        }
        FrameworkMain.getInstance().start();
        MonitorMain.getInstance().start();
    }

    /**
     * Signals the Xlet to stop providing service and enter the Paused state.
     */
    public void pauseXlet() {
        if (Log.INFO_ON) {
            Log.printInfo("MonitorXlet: pauseXlet");
        }
        FrameworkMain.getInstance().pause();
    }

    /**
     * Signals the Xlet to terminate and enter the Destroyed state.
     * @param unconditional If unconditional is true when this method is called, requests by the Xlet to not enter the
     *            destroyed state will be ignored.
     * @throws XletStateChangeException is thrown if the Xlet wishes to continue to execute (Not enter the Destroyed
     *             state). This exception is ignored if unconditional is equal to true.
     */
    public void destroyXlet(boolean unconditional) throws XletStateChangeException {
        if (Log.INFO_ON) {
            Log.printInfo("MonitorXlet: destroyXlet");
        }
        FrameworkMain.getInstance().destroy();
        MonitorMain.getInstance().dispose();
    }

    // //////////////////////////////////////////////////////////////////////////
    // ApplicationConfig implementation
    // //////////////////////////////////////////////////////////////////////////

    /**
     * Returns the version of Application.
     * @return version string.
     */
    public String getVersion() {
        //->Kenneth : Version on Monitor Application for R4.1
        //return "1.4.3.1";
        //return "R4_1.1.0";
        //return "R4_1.2.1";//VDTRMASTER-5319 수정 및 APD 우정이 코드 추가
        //return "R4_1.3.1"e//Energy Star + ScreenSaver + VDTRMASTER5333
        return VERSION;
    }

    /**
     * Returns the name of Application.
     * @return application name.
     */
    public String getApplicationName() {
        return "Monitor";
    }

    /**
     * Returns the Application's XletContext.
     * @return application's XletContext.
     */
    public XletContext getXletContext() {
        return xletContext;
    }

}

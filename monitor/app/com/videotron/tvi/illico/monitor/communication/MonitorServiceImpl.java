package com.videotron.tvi.illico.monitor.communication;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;

import javax.tv.xlet.XletContext;

import org.dvb.io.ixc.IxcRegistry;
import org.ocap.application.AppManagerProxy;
import org.ocap.diagnostics.MIBDefinition;
import org.ocap.diagnostics.MIBManager;
import org.ocap.diagnostics.MIBObject;
import org.ocap.hardware.Host;

import com.opencable.handler.cahandler.CAAuthorization;
import com.opencable.handler.cahandler.CAHandler;
import com.videotron.tvi.illico.ixc.monitor.AODEventListener;
import com.videotron.tvi.illico.ixc.monitor.AODRemoveListener;
import com.videotron.tvi.illico.ixc.monitor.BlockedTimeListener;
import com.videotron.tvi.illico.ixc.monitor.EIDMappingTableUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorCAAuthorizationListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverStateListener;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.ixc.monitor.VideoContextListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.core.AodManager2;
import com.videotron.tvi.illico.monitor.core.AppLauncher;
import com.videotron.tvi.illico.monitor.core.AuthorizationManager;
import com.videotron.tvi.illico.monitor.core.AutoStandbyManager;
import com.videotron.tvi.illico.monitor.core.CAResourceAuthorizationListenerImpl;
import com.videotron.tvi.illico.monitor.core.InbandDataManager;
import com.videotron.tvi.illico.monitor.core.MonitorStateManager;
import com.videotron.tvi.illico.monitor.core.STBModeManager;
import com.videotron.tvi.illico.monitor.core.ScreenSaverManager;
import com.videotron.tvi.illico.monitor.core.ServiceStateManager;
import com.videotron.tvi.illico.monitor.core.StateManager;
import com.videotron.tvi.illico.monitor.core.TransitionEvent;
import com.videotron.tvi.illico.monitor.core.WatchingTimeRestrictionManager;
import com.videotron.tvi.illico.monitor.data.ActivationPackage;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.DataVersionManager;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;
import com.videotron.tvi.illico.monitor.data.MandatoryPackage;
import com.videotron.tvi.illico.monitor.data.MonitorConfigurationManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;
import com.videotron.tvi.illico.monitor.system.CANHManager;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.TextUtil;
//->Kenneth[2015.6.29] Tank
import com.videotron.tvi.illico.monitor.ui.popup.*;
//<-

/**
 * Implementation of MonitorService.
 *
 * @author Woosik Lee
 */
public class MonitorServiceImpl implements MonitorService {

	/** The XletContext instance. */
	private XletContext xletContext;
	/** The singleton instance. */
	private static MonitorServiceImpl instance = null;
	/** The cable card mac address. */
	private byte[] cableCardMacAddress = null;

	private static final String LOG_HEADER = "[MonitorServiceImpl]";

	private CAResourceAuthorizationListenerImpl caListener = CAResourceAuthorizationListenerImpl.getInstance();

	/**
	 * Gets the singleton instance of MontiorServiceImpl.
	 *
	 * @return instance.
	 */
	public static synchronized MonitorServiceImpl getInstance() {
		if (instance == null) {
			instance = new MonitorServiceImpl();
		}
		return instance;
	}

	/**
	 * Starts this class. try to bind Monitor's IXC.
	 *
	 * @param pXletContext
	 *            the XletContext instance.
	 */
	public void start(XletContext pXletContext) {
		this.xletContext = pXletContext;
		bindToIxc();
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		try {
			IxcRegistry.unbind(xletContext, MonitorService.IXC_NAME);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * Binds the Monitor's IXC.
	 */
	private void bindToIxc() {
		try {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "bind to IxcRegistry");
			}
			IxcRegistry.bind(xletContext, MonitorService.IXC_NAME, this);
		} catch (Exception e) {
			Log.print(e);
		}
	}

	public void loadAODApplication(String aodAppName, AODEventListener listener) throws RemoteException {
		if(!AodManager2.AOD_SUPPORTED) {
			throw new RemoteException();
		}
		AodManager2.getInstance().loadAODApplication(aodAppName, listener);
	}

	public void startAODApplication(String aodAppName, AODEventListener listener) throws RemoteException {
		if(!AodManager2.AOD_SUPPORTED) {
			throw new RemoteException();
		}
		AodManager2.getInstance().startAODApplication(aodAppName, listener);
	}

	public void closeAODApplication(String aodAppName, AODEventListener listener) throws RemoteException {
		if(!AodManager2.AOD_SUPPORTED) {
			throw new RemoteException();
		}
		AodManager2.getInstance().closeAODApplication(aodAppName, listener);
	}

	public void addAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) throws RemoteException {
		if(!AodManager2.AOD_SUPPORTED) {
			throw new RemoteException();
		}
		AodManager2.getInstance().addAODApplicationRemoveListener(listener, applicationName);
	}

	public void removeAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) throws RemoteException {
		if(!AodManager2.AOD_SUPPORTED) {
			throw new RemoteException();
		}
		AodManager2.getInstance().removeAODApplicationRemoveListener(listener, applicationName);
	}


	/**
	 * Gets the CableCard MAC address.
	 *
	 * @return the mac address.
	 * @throws RemoteException
	 *             the exception.
	 */
	public byte[] getCableCardMacAddress() throws RemoteException {
		if (Log.DEBUG_ON) {
			Log.printDebug("getCableCardMacAddress()");
		}
		if (cableCardMacAddress == null) {
			findCableCardMacAddress();
		}
		return cableCardMacAddress;
	}

	/**
	 * Gets the host's MAC address.
	 * @return MAC address string.
	 * @throws RemoteException the exception.
	 */
	public String getMacAddress() throws RemoteException {
		String macAddress = null;

		if (Environment.HW_VENDOR == Environment.HW_VENDOR_SAMSUNG) {
			byte[] addr = getCableCardMacAddress();
			macAddress = getStringBYTEArray(addr);
		}
		else {
			macAddress = Host.getInstance().getReverseChannelMAC();
            String[] token = TextUtil.tokenize(macAddress, ":");
            StringBuffer address = new StringBuffer();
            for (int i = 0; i < token.length; i++) {
                address.append(token[i]);
            }
            macAddress = address.toString();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug("getMacAddress() : " + macAddress);
		}
		return macAddress;
	}

	/**
     * Returns a String from the byte array.
     * @param data byte array
     * @return String
     */
    private String getStringBYTEArray(byte[] data) {
        if (data == null) {
            return "";
        }
        return getStringBYTEArrayOffset(data, 0);
    }

    /**
     * Returns a String from the byte array.
     * @param data byte array
     * @param offset offset
     * @return String
     */
    private String getStringBYTEArrayOffset(byte[] data, int offset) {
        if (data == null) {
            return "";
        }

        StringBuffer buff = new StringBuffer(4096);
        for (int i = offset; i < offset + data.length; i++) {
            buff.append(getHexStringBYTE(data[i]));
            if (i != data.length - 1) {
                buff.append("");
            }
        }
        return buff.toString();
    }

    /**
     * Returns a String from the byte array.
     * @param b byte
     * @return String
     */
    private String getHexStringBYTE(byte b) {
        String str = "";

        // handle the case of -1
        if (b == -1) {
            str = "FF";
            return str;
        }

        int value = (((int) b) & 0xFF);

        if (value < 16) {
            // pad out string to make it look nice
            str = "0";
        }
        str += (Integer.toHexString(value)).toUpperCase();
        return str;
    }

	/**
	 * During the boot process, the unbound applications call available this
	 * method.
	 *
	 * @param applicationName
	 *            the application unique name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void iamReady(String applicationName) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "iamReady : " + applicationName);
		}
		MonitorMain.getInstance().readyApplication(applicationName, true);
	}

	/**
	 * Checks The User has the authorization to execute the application.
	 *
	 * @param applicationName
	 *            the application unique name.
	 * @return return true if authorized, return false if not authorized.
	 * @throws RemoteException
	 *             the exception.
	 */
	public int checkAppAuthorization(String applicationName) throws RemoteException {
		int result = AuthorizationManager.getInstance().checkResourceAuthorization(applicationName);
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkAppAuthorization : " + applicationName + ", result="+ result);
		}
		switch (result) {
		case AuthorizationManager.CHECK_RESULT_AUTHORIZED:
			return MonitorService.AUTHORIZED;
		case AuthorizationManager.CHECK_RESULT_ERROR_PACKAGE_FILE:
		case AuthorizationManager.CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
		case AuthorizationManager.CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			return MonitorService.CHECK_ERROR;
		case AuthorizationManager.CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST:
		case AuthorizationManager.CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE:
			return MonitorService.NOT_FOUND_ENTITLEMENT_ID;
		case AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED:
			return MonitorService.NOT_AUTHORIZED;
		}

		return MonitorService.AUTHORIZED;
	}

	/**
	 * Gets the iPPV Deactivation package's authority.
	 *
	 * @return {@link MonitorService#NOT_AUTHORIZED},
	 *         {@link MonitorService#AUTHORIZED},
	 *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID},
	 *         {@link MonitorService#CHECK_ERROR}.
	 * @throws RemoteException
	 *             the exception.
	 */
	public int getIPPVDeactivationPackageAuthority() throws RemoteException {
		return AuthorizationManager.getInstance().getIPPVDeactivationPackageAuthority();
	}

	/**
	 * Requests the parameters. It returns the parameters that is
	 * {@link MonitorService#startUnboundApplication(String, String[])} 's
	 * parameter
	 * <P>
	 * The first index of parameter indicates the start event. Following is the
	 * start event variables.
	 *
	 * @link {@link MonitorService#REQUEST_APPLICATION_HOT_KEY},
	 * @link {@link MonitorService#REQUEST_APPLICATION_LAST_KEY},
	 * @link {@link MonitorService#REQUEST_APPLICATION_MENU}.
	 * @param appName
	 *            Application name to request the parameter.
	 * @return parameters it is the parameters by
	 *         {@link MonitorService#startUnboundApplication(String, String[])}
	 * @throws RemoteException
	 *             the exception.
	 */
	public String[] getParameter(String appName) throws RemoteException {
		return AppLauncher.getInstance().getParameter(appName);
	}

	/**
	 * Request to start unbound application. When this method is called, current
	 * running application is destroyed (or paused), and the requested
	 * application is started. All these action is performed asynchronously.
	 *
	 * @param appName
	 *            unbound application name to be started.
	 * @param parameters
	 *            the arguments to be transfered. It can be retrieved by
	 *            {@link MonitorService#getParameter(String)}.
	 * @return if successfully launched the application returns true, otherwise
	 *         returns false.
	 * @throws RemoteException
	 *             the exception.
	 */
	public boolean startUnboundApplication(String appName, String[] parameters) throws RemoteException {
		return AppLauncher.getInstance().startUnboundApplication(appName, parameters);
	}

	/**
	 * Request to start unbound application.
	 *
	 * @param code
	 *            hot key code
	 * @throws RemoteException
	 *             the exception.
	 */
	public void startUnboundAppWithHotKey(int keyCode) throws RemoteException {
		MonitorMain.getInstance().getMonitorKeyHandler().handleKeyEvent(keyCode);
	}

	/**
	 * In the special video state, this method stops current overlapping application.
	 * @throws RemoteException
	 */
	public void stopOverlappingApplication() throws RemoteException {
		MonitorStateManager.getInstance().stopOverlappingApplication(true);
	}

	/**
	 * Enter to special video mode. It is called by PVR, VOD, PCS.
	 *
	 * @param videoName
	 *            the video name.
	 * @param endingMessage
	 *            Monitor can stop special video mode and shows confirm popup.
	 *            This parameter is confirm popup message. If null, Monitor does
	 *            not show confirm popoup.
	 * @throws RemoteException the exception.
	 */
	public void reserveVideoContext(String videoName, String endingMessage) throws RemoteException {
		MonitorStateManager.getInstance().reserveVideoContext(videoName, endingMessage);
	}

	/**
	 * Exit to normal video mode.
	 * @throws RemoteException the exception.
	 */
	public void releaseVideoContext() throws RemoteException {
		MonitorStateManager.getInstance().releaseVideoContext(true);
	}

	/**
	 * Whether special video mode or not.
	 * @return If special video mode now, return true otherwise return false.
	 * @throws RemoteException the exception.
	 */
	public boolean isSpecialVideoState() throws RemoteException {
		return MonitorStateManager.getInstance().isSpecialVideoState();
	}

	/**
	 * Gets the current video context name.
	 * @return video name.
	 * @throws RemoteException the exception.
	 */
	public String getVideoContextName() throws RemoteException {
		return MonitorStateManager.getInstance().getCurrentVideoName();
	}

	/**
	 * Returns current overlapping application name.
	 * Return null, if overlapping application is not exist.
	 * @return
	 * @throws RemoteException
	 */
	public String getCurrentOverlappingApplicationName() throws RemoteException {
		return MonitorStateManager.getInstance().getCurrentOverlappingApplicationName();
	}


	/**
	 * Adds the {@link VideoContextListener} .
	 * @param l	{@link VideoContextListener}.
	 * @throws RemoteException	the exception.
	 */
	public void addVideoContextListener(VideoContextListener l, String appName) throws RemoteException {
		MonitorStateManager.getInstance().addVideoContextListener(l, appName);
	}


	public void removeVideoContextListener(VideoContextListener l, String appName) throws RemoteException {
		MonitorStateManager.getInstance().removeVideoContextListener(l, appName);
	}

	/**
	 * Requests to destroy the current running application When this method is
	 * called, MonitorApplication destroys (or pause) the current running
	 * application, and selects the LCW service. All these action is performed
	 * asynchronously.
	 *
	 * @throws RemoteException
	 *             the exception.
	 */
	public void exitToChannel() throws RemoteException {
		AppLauncher.getInstance().exitToChannel();
	}

	/**
	 * Requests to destroy the current running application and moves to the
	 * specified service by sourceId. When this method is called, Monitor
	 * destroys (or pause) the current running application, and selects the
	 * specified service. All these action is performed asynchronously.
	 *
	 * @param sourceId
	 *            The target channel's source id.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void exitToChannel(int sourceId) throws RemoteException {
		AppLauncher.getInstance().exitToChannel(sourceId);
	}

	/**
	 * Gets the service state.
	 *
	 * @return current service state.
	 * @throws RemoteException
	 *             the exception
	 */
	public int getState() throws RemoteException {
		return ServiceStateManager.getInstance().getState();
	}

	/**
	 * Weather TV_VIEWING_STATE or not.
	 *
	 * @return if TV_VIEWING_STATE, returns true.
	 * @throws RemoteException
	 *             the exception.
	 */
	public boolean isTvViewingState() throws RemoteException {
		return ServiceStateManager.getInstance().isTvViewingState();
	}

	/**
	 * Adds the service state listener.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            The application name that requests listener.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void addStateListener(ServiceStateListener l, String applicationName) throws RemoteException {
		ServiceStateManager.getInstance().addStateListener(l, applicationName);
	}

	/**
	 * Removes the service state listener.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            the application name that requests listener.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void removeStateListener(ServiceStateListener l, String applicationName) throws RemoteException {
		ServiceStateManager.getInstance().removeStateListener(l, applicationName);
	}

	/**
	 * Gets the STB mode.
	 *
	 * @return currrent STB mode.
	 * @throws RemoteException
	 *             the exception.
	 */
	public int getSTBMode() throws RemoteException {
		return STBModeManager.getInstance().getMode();
	}

	/**
	 * Adds the STB mode changed listener.
	 *
	 * @param l
	 *            The {@link STBModeChangeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void addSTBModeChangeListener(STBModeChangeListener l, String applicationName) throws RemoteException {
		STBModeManager.getInstance().addModeChangeListener(l, applicationName);
	}

	/**
	 * Removes the STB mode changed listener.
	 *
	 * @param l
	 *            The {@link STBModeChangeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void removeSTBModeChangeListener(STBModeChangeListener l, String applicationName) throws RemoteException {
		STBModeManager.getInstance().removeModeChangeListener(l, applicationName);
	}

	/**
	 * Resets screen saver timer. It is called when the software action occur.
	 *
	 * @throws RemoteException
	 *             the exception.
	 */
	public void resetScreenSaverTimer() throws RemoteException {
		ScreenSaverManager.getInstance().resetTimer();
	}

	/**
     * Requests to start screen saver.
     * @throws RemoteException
     */
    public void startScreenSaver() throws RemoteException {
    	ScreenSaverManager.getInstance().startScreenSaver(true);
    }

	/**
	 * Adds the screen saver confirmation listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverConfirmationListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void addScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String applicationName)
			throws RemoteException {
		ScreenSaverManager.getInstance().addScreenSaverConfirmListener(l, applicationName);
	}

	/**
	 * Removes the screen saver confirmation listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverConfirmationListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void removeScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String applicationName)
			throws RemoteException {
		ScreenSaverManager.getInstance().removeScreenSaverConfirmListener(l, applicationName);
	}

	/**
	 * Adds the screen saver state listener
	 *
	 * @param l
	 *            the {@link ScreenSaverStateListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void addScreenSaverStateListener(ScreenSaverStateListener l, String applicationName) throws RemoteException {
		ScreenSaverManager.getInstance().addScreenSaverStateListener(l, applicationName);
	}

	/**
	 * Removes the screen saver state listener.
	 *
	 * @param l
	 *            The {@link ScreenSaverStateListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void removeScreenSaverStateListener(ScreenSaverStateListener l, String applicationName)
			throws RemoteException {
		ScreenSaverManager.getInstance().removeScreenSaverStateListener(l, applicationName);
	}

	/**
	 * Adds the Inband data listener.
	 *
	 * @param l
	 *            The {@link InbandDataListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	public void addInbandDataListener(InbandDataListener l, String applicationName) throws RemoteException {
		InbandDataManager.getInstance().addInbandDataListener(l, applicationName);
	}

	/**
	 * Removes the Inband data listener.
	 *
	 * @param l
	 *            The {@link InbandDataListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	public void removeInbandDataListener(InbandDataListener l, String applicationName) throws RemoteException {
		InbandDataManager.getInstance().removeInbandDataListener(l, applicationName);
	}

	/**
	 * The applications must call this interface when application completes
	 * receiving the Inband data. When all applicatios that receiving the Inband
	 * data call this interface, the Monitor application can perform next
	 * process in boot state.
	 *
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	public void completeReceivingData(String applicationName) throws RemoteException {
		InbandDataManager.getInstance().completeReceivingData(applicationName);
	}

	/**
	 * Checks the authorization using packageID.
	 *
	 * @param packageID
	 *            the package ID
	 * @return if authorized package ID, returns
	 *         {@link MonitorService.AUTHORIZED}. Otherwise return
	 *         {@link MonitorService.NOT_AUHORIZED}. if not found the
	 *         entitlement id using packageID, returns
	 *         {@link MonitorService.NOT_FOUND_ENTITLEMENT_ID}. if error occurs,
	 *         return {@link MonitorService.CHECK_ERROR}.
	 * @throws RemoteException
	 *             the exception.
	 */
	public int checkResourceAuthorization(String packageID) throws RemoteException {
		short entitlementID = EIDDataManager.getInstance().findEidByPackageId(packageID);
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkResourceAuthorization requested packageId=" + packageID
					+ ", entitlementID=" + entitlementID);
		}
		if (entitlementID == Short.MIN_VALUE) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "checkResourceAuthorization() Not found EID");
			}
			return NOT_FOUND_ENTITLEMENT_ID;
		}
		try {
			CAHandler caHandler = CANHManager.getInstance().getCAHandler();
			long id = AuthorizationManager.EID_PREFIX | (long) entitlementID;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "checkResourceAuthorization Prefixed id=" + id);
			}
			CAAuthorization caResult = caHandler.getResourceAuthorization(id, caListener);
			boolean result = caResult.isAuthorized();

			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "checkResourceAuthorization result=" + result);
			}

			if (result) {
				//AuthrorizationResultManager.getInstance().checkResult(entitlementID,
				//		AuthorizationManager.CHECK_RESULT_AUTHORIZED);
				return AUTHORIZED;
			} else {
				//AuthrorizationResultManager.getInstance().checkResult(entitlementID,
				//		AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED);
				return NOT_AUTHORIZED;
			}
		} catch (Exception e) {
			//AuthrorizationResultManager.getInstance().checkResult(entitlementID,
			//		AuthorizationManager.CHECK_RESULT_ERROR_CANH_COMMUNICATION);
			Log.print(e);
		}
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkResourceAuthorization() CHECK ERROR");
		}
		return CHECK_ERROR;
	}

	/**
	 * Adds the CAAuthorizationListener.
	 * @param l instance of CAAuthorizationListener.
	 * @throws RemoteException the exception.
	 */
	public void addCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName) throws RemoteException {
		CAResourceAuthorizationListenerImpl ca = CAResourceAuthorizationListenerImpl.getInstance();
		ca.addCAResourceAuthorizationListener(l, applicationName);
	}

	public void removeCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName) throws RemoteException {
		CAResourceAuthorizationListenerImpl ca = CAResourceAuthorizationListenerImpl.getInstance();
		ca.removeCAResourceAuthorizationListener(l, applicationName);
	}

	/**
	 * Adds the EIDMappingTableUpdateListener. If any application want to
	 * getting the event that package id/entitlement id changed, can register
	 * event.
	 *
	 * @param l
	 *            EIDMappingTableUpdateListener instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             The exception.
	 */
	public void addEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String applicationName)
			throws RemoteException {
		EIDDataManager.getInstance().addEIDMappingTableUpdateListener(l, applicationName);
	}

	/**
	 * Removes the EIDMappingTableUpdateListener.
	 *
	 * @param l
	 * @param applicationName
	 * @throws RemoteException
	 */
	public void removeEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String applicationName)
			throws RemoteException {
		EIDDataManager.getInstance().removeEIDMappingTableUpdateListener(l, applicationName);
	}

	/**
	 * Starts the monitoring key event to go standby mode automatically.
	 *
	 * @param inactivityTime
	 *            the waiting time for auto standby mode.
	 * @throws RemoteException
	 *             The exception.
	 */
	public void startAutoStandby(long inactivityTime) throws RemoteException {
		if (inactivityTime < 1) {
			AutoStandbyManager.getInstance().setInactivityTime(0);
		} else {
			AutoStandbyManager.getInstance().setInactivityTime(inactivityTime);
		}
	}

	/**
	 * Stops the monitoring key event to go standby mode automatically.
	 *
	 * @throws RemoteException
	 *             The exception.
	 */
	public void stopAutoStandby() throws RemoteException {
		AutoStandbyManager.getInstance().setInactivityTime(0);
	}

	/**
	 * Returns the version of the Inband data file.
	 *
	 * @param fileName
	 *            the file name.
	 * @return version. if not found the file name, returns
	 *         {@link MonitorService#VERSION_NOT_FOUND}
	 * @throws RemoteException
	 *             RemoteException The exception.
	 */
	public int getInbandDataVersion(String fileName) throws RemoteException {
		return DataVersionManager.getInstance().getInbandDataVersion(fileName);
	}

	/**
	 * Returns the entitlement id in the package_name_eid.txt file.
	 *
	 * @param packageName
	 *            the package name.
	 * @return entitlement id, if not found the package id, returns NULL.
	 * @throws RemoteException
	 *             The exception.
	 */
	public String getEidAtPackageNameEidMappingFile(String packageName) throws RemoteException {
		return EIDDataManager.getInstance().findEidStringByPackageId(packageName);
	}

	/**
	 * Adds the blocked-time state listener
	 *
	 * @param l
	 *            the {@link BlockedTimeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void addBlockedTimeListener(BlockedTimeListener l, String applicationName) throws RemoteException {
		WatchingTimeRestrictionManager.getInstance().addBlockedListener(l, applicationName);
	}

	/**
	 * Removes the blocked-time state listener.
	 *
	 * @param l
	 *            The {@link BlockedTimeListener} instance.
	 * @param applicationName
	 *            The application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public void removeBlockedTimeListener(BlockedTimeListener l, String applicationName) throws RemoteException {
		WatchingTimeRestrictionManager.getInstance().removeBlockedTimeListener(l, applicationName);
	}

	/**
	 * Gets the current activated application name in the
	 * {@link MonitorService#FULL_SCREEN_APP_STATE} only. If this method is
	 * called in the {@link MonitorService#TV_VIEWING_STATE}, returns null;
	 *
	 * @return Application name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public String getCurrentActivatedApplicationName() throws RemoteException {
		if (getState() == MonitorService.FULL_SCREEN_APP_STATE) {
			TransitionEvent curEvent = StateManager.getInstance().getCurrentEvent();
			if (curEvent == null) {
				return null;
			}
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "current app name=" + curEvent.getApplicationName());
			}
			return curEvent.getApplicationName();
		} else {
			return null;
		}
	}

	/**
	 * Gets the DNCS name in OOB's Moni_DataOOB/package_name_eid.txt file.
	 *
	 * @return DNCS name.
	 * @throws RemoteException
	 *             the exception.
	 */
	public String getDncsName() throws RemoteException {
		return EIDDataManager.getInstance().getDncsName();
	}

	public void registerUnboundApplication(String xaitFilePath) throws RemoteException {
		AppManagerProxy proxy = AppManagerProxy.getInstance();
		String TestForAOD = DataManager.getInstance().getTestForAOD();

		if (Log.INFO_ON) {
			Log.printInfo("registerUnboundApplication() " + xaitFilePath + ", isTest? " + TestForAOD);
		}

		try {
			URL url;

			if (TestForAOD.equals("YES")) {
				String host = "10.247.191.165";
				int port = 8080;
				url = new URL("http://" + host + ":" + port + "/appstore/xait.xml/");
			} else {
				String host = MonitorConfigurationManager.getInstance().getAodServerIP();
				int port = MonitorConfigurationManager.getInstance().getAodServerPort();
				String contextRoot = MonitorConfigurationManager.getInstance().getAodContextRoot();
				String urlPath = "http://" + host + ":" + port + contextRoot + xaitFilePath;
				url = new URL(urlPath);
				if(Log.INFO_ON) {
					Log.printInfo("registerUnboundApplication() : " + urlPath);
				}
			}

			URLConnection conn = url.openConnection();

			conn.setDoOutput(true);

			InputStream xait = conn.getInputStream();

			/*
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line;
			String displayText = "";
			while ((line = rd.readLine()) != null) {
				displayText += line;
			}
			if (Log.INFO_ON) {
				Log.printInfo("registerUnboundApplication() result = " + displayText);
			}
			*/

			// If xaitFilePath don't start with '/', Monitor put '/'.
			if(!xaitFilePath.startsWith("/")) {
				xaitFilePath = "/" + xaitFilePath;
			}

			try {
				if(Log.INFO_ON) {
					Log.printInfo("registerUnboundApplication() inputStream: " + xait);
				}
				proxy.registerUnboundApp(xait);
				if(Log.INFO_ON) {
					Log.printInfo("registerUnboundApplication() finish registerUnboundApp()");
				}
			} catch (IOException e) {
				Log.print(e);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * Gets the Mandatory package name array.
	 * If the Mandatory package is not present, returns null.
	 * If the Mandatory package is empty, returns empty array (Array is not null and size is 0).
	 * @return Mandatory package name array.
	 * @throws RemoteException The remote exception
	 */
	public String[] getMandatoryPackageNames() throws RemoteException {
		MandatoryPackage[] mPackages = EIDDataManager.getInstance().getMandatoryPackages();
		if(mPackages == null) {
			return null;
		}
		if(mPackages.length < 1) {
			return new String[]{};
		}

		int size = mPackages.length;
		String[] mNames = new String[size];
		for(int i=0; i<size; i++) {
			mNames[i] = mPackages[i].getPackageName();
		}
		return mNames;
	}

	/**
	 * Gets the authorization of Multiroom package.
	 * @return User has authorization of Multiroom return true.
	 * @throws RemoteException The remote exception
	 */
	public boolean isMultiroomEnabled() throws RemoteException {
		int authResult = AuthorizationManager.getInstance().checkPvrMultiroomPackage();
		if (authResult == AuthorizationManager.CHECK_RESULT_AUTHORIZED) {
			return true;
		}
		return false;
	}

	/**
	 * Checkes the STB is blocked by "terminal blocked time".
	 *
	 * @return if the STB is blocked, returns true.
	 * @throws RemoteException the exception.
	 */
    public boolean isTerminalBlockedTime() throws RemoteException {
        return WatchingTimeRestrictionManager.getInstance().isBlockedTime();
    }

	/**
	 * Gets the Activation package name array.
	 * If the Activation package is not present, returns null.
	 * If the Activation package is empty, returns empty array (Array is not null and size is 0).
	 * @return Activation package name array.
	 * @throws RemoteException The remote exception
	 */
	public String[] getActivationPackageNames() throws RemoteException {
		ActivationPackage[] mPackages = EIDDataManager.getInstance().getActivationPackages();
		if(mPackages == null) {
			return null;
		}
		if(mPackages.length < 1) {
			return new String[]{};
		}

		int size = mPackages.length;
		String[] mNames = new String[size];
		for(int i=0; i<size; i++) {
			mNames[i] = mPackages[i].getPackageName();
		}
		return mNames;
	}

	/**
	 * Finds the CableCard Mac address using the MIB.
	 */
	private void findCableCardMacAddress() {
		final String oid = ".1.3.6.1.4.1.4491.2.3.1.1.4.4.1.0";

		if (Log.DEBUG_ON) {
			Log.printDebug("findCableCardMacAddress() oid=" + oid);
		}

		MIBManager mgr = MIBManager.getInstance();
		MIBDefinition[] defs = mgr.queryMibs(oid);
		if (defs == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug("findCableCardMacAddress() defs is null.");
			}
			return;
		}
		int defsLth = defs.length;
		if (Log.DEBUG_ON) {
			Log.printDebug("findCableCardMacAddress() defsLth : <" + defsLth + ">");
		}

		for (int i = 0; i < defs.length; i++) {
			MIBDefinition def = defs[i];
			if (def == null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("findCableCardMacAddress() def-" + i + " is null.");
				}
				continue;
			}
			MIBObject obj = def.getMIBObject();
			if (obj == null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("findCableCardMacAddress() obj-" + i + " is null.");
				}
				continue;
			}
			byte[] mibObjectData = obj.getData();
			if (mibObjectData == null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("findCableCardMacAddress() mibObjectData-" + i + " is null.");
				}
				continue;
			}

			if (Log.DEBUG_ON) {
				Log.printDebug("findCableCardMacAddress() result-" + i + " : <" + toHexString(mibObjectData)
						+ ">");
			}

			boolean isValidAddress = false;
			for(int t=0; t<mibObjectData.length; t++) {
				if(mibObjectData[t] != 0) {
					isValidAddress = true;
				}
			}

			if(!isValidAddress) {
				if(Log.INFO_ON) {
					Log.printInfo("Found address is invalid. so continue~");
				}
				continue;
			}

			cableCardMacAddress = mibObjectData;
			break;
		}
	}

	/**
	 * Converts byte array to the Java String object.
	 *
	 * @param bytes
	 *            byte array.
	 * @return String object.
	 */
	private String toHexString(byte[] bytes) {
		if (bytes == null) {
			return "";
		}

		StringBuffer buf = new StringBuffer(bytes.length);
		for (int i = 0; i < bytes.length; i++) {
			String b = Integer.toHexString(bytes[i] & 0xFF);
			if (b.length() == 1) {
				buf.append("0");
			}
			buf.append(b);
			buf.append(",");
		}
		int n = buf.length();
		if (n > 0) {
			buf.deleteCharAt(n - 1); // remove last ','
		}
		return buf.toString();
	}

    //->Kenneth[2015.6.29] Tank
	/**
	 * Show list behavior popup
	 *
	 * @throws RemoteException the exception.
	 */
    public void showListPressedInNonPvrPopup() throws RemoteException {
        Log.printDebug("Kenneth : MonitorServiceImpl.showListPressedInNonPvrPopup()");
        ListPressedInNonPvrPopup.getInstance().showPopup();
    }
    //<-

    /**
     * Shares YearShift to the other applications
     *
     * @return YearShift
	 * @throws RemoteException the exception.
     */
    public int getYearShift() {
        int yearShift = 0;
        try {
            String yearShiftStr = DataManager.getInstance().getYearShift();
            yearShift = Integer.parseInt(yearShiftStr);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Log.DEBUG_ON) Log.printDebug("getYearShift() returns "+yearShift);
        return yearShift;
    }
}

package com.videotron.tvi.illico.monitor.communication;

public interface NotificationCenterProxyListener {

    public void action(String id);

    public void cancel(String id);
}

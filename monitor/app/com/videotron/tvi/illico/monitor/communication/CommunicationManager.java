package com.videotron.tvi.illico.monitor.communication;

import java.awt.Point;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Enumeration;

import javax.tv.xlet.XletContext;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.CurrentServiceFilter;
import org.dvb.io.ixc.IxcRegistry;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageListener;
import com.videotron.tvi.illico.ixc.errormessage.ErrorMessageService;
import com.videotron.tvi.illico.ixc.loadinganimation.LoadingAnimationService;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.ixc.pvr.PvrService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.stc.LogLevelChangeListener;
import com.videotron.tvi.illico.ixc.stc.StcService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.isa.ISAService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.core.InbandDataManager;
import com.videotron.tvi.illico.monitor.core.MonitorVbmController;
import com.videotron.tvi.illico.monitor.core.WatchingTimeRestrictionManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.SharedMemory;
import com.videotron.tvi.illico.util.IxcLookupWorker;

/**
 * This class provides the interface instances that is other application's IXC.
 * @author Woosik Lee
 */
public class CommunicationManager implements DataUpdateListener {

    /** The XletContext instance. */
    private XletContext xletContext;
    /** Indicates the singleton instance of CommunicationManager. */
    private static CommunicationManager instance;

    /** Indicates the Menu application's IXC interface instance. */
    private MainMenuService mainMenuService;
    /** Indicates the EPG application's IXC interface instance. */
    private EpgService epgService;
    /** Indicates the VOD application's IXC interface instance. */
    private VODService vodService;
    /** Indicates the User Profile Preference application's IXC interface instance. */
    private PreferenceService preferenceService;
    /** Indicates the Loading animation application's IXC interface instance. */
    private LoadingAnimationService loadingService;
    /** Indicates the Search application's IXC interface. */
    private SearchService searchService;
    /** Indicates the ErrorMessage application's IXC interface. */
    private ErrorMessageService errorMessageService;
    /** Indicates the Notification application's IXC interface. */
    private NotificationService notificationService;
    /** Indicates the STC application's IXC interface. */
    private StcService stcService;
    /** Indicates the VBM application's IXC interface. */
    private VbmService vbmService;
    /** indicates the PVR application's IXC interface. */
    private PvrService pvrService;
    /** indicates the ISA application's IXC interface. */
    private ISAService isaService;

    /** Weak or no signal error. */
    public static final String ERROR_500 = "MOT500";
    /** No permission to launch application. */
    public static final String ERROR_501 = "MOT501";
    /** No mandatory permission. */
    public static final String ERROR_502 = "MOT502";
    /** Request to start PVR application int the non PVR STB. */
    public static final String ERROR_503 = "MOT503";
    /** Fail to start unbound application. */
    public static final String ERROR_504 = "MOT504";
    /** Can not use the standalone mode for more 30 days at PVR STB. */
    public static final String ERROR_505 = "MOT505";
    /** No signal and have not standalone authority. */
    public static final String ERROR_506 = "MOT506";
    /** AOD error code - XAIT server not reachable. */
    public static final String ERROR_AOD_001 = "AOD001";
    /** AOD error code - Application missing file error. */
    public static final String ERROR_AOD_002 = "AOD002";
    /** AOD error code - AOD parsing error. */
    public static final String ERROR_AOD_003 = "AOD003";
    /** AOD error code - AOD application processing error (Timeout). */
    public static final String ERROR_AOD_004 = "AOD004";
    /** AOD error code - AOD application information not found. */
    public static final String ERROR_AOD_005 = "AOD005";
    
    //Kenneth : R4.1 : VOD Deactivation Package error code
    //public static final String ERROR_507 = "MOT507";
    
    private long lastTimeRequestAPD = 0;

    private LogLevelAdapter logListener = new LogLevelAdapter();

    private IxcLookupWorker ixcWorker;

    /** Log header. */
    private static final String LOG_HEADER = "[CommunicationManager] ";

    /**
     * Gets the singleton instance of CommunicationManager.
     * @return the singleton instance.
     */
    public static synchronized CommunicationManager getInstance() {
        if (instance == null) {
            instance = new CommunicationManager();
        }
        return instance;
    }

    /**
     * Starts this class. It performs finding IXC interfaces.
     * @param xc the XletContext's instance.
     */
    public void start(final XletContext xc) {
        xletContext = xc;
        ixcWorker = new IxcLookupWorker(xc);

        DataManager dm = DataManager.getInstance();

        findAndLookup(dm.getLoadingName(), LoadingAnimationService.IXC_NAME, false);
        findAndLookup(dm.getMenuName(), MainMenuService.IXC_NAME, true);
        findAndLookup(dm.getEPGName(), EpgService.IXC_NAME, true);
        findAndLookup(dm.getVodName(), VODService.IXC_NAME, true);
        findAndLookup(dm.getUPPName(), PreferenceService.IXC_NAME, true);
        findAndLookup(dm.getSearchName(), SearchService.IXC_NAME, false);
        findAndLookup(dm.getErrorMessageName(), ErrorMessageService.IXC_NAME, false);
        findAndLookup(dm.getNotificationName(), NotificationService.IXC_NAME, false);
        findAndLookup(dm.getStcName(), StcService.IXC_NAME, false);
        findAndLookup(dm.getVbmName(), VbmService.IXC_NAME, false);
        findAndLookup(dm.getPVRName(), PvrService.IXC_NAME, false);
        findAndLookup(dm.getIsaName(), ISAService.IXC_NAME, false);
    }

    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {

    }

    private void findAndLookup(String appName, String serviceName, boolean readyWhenFail) {
        AppID appId = findApplicationId(appName);
        if (appId == null) {
            Log.printError(LOG_HEADER + "ERROR - Not found AppID for " + appName);
            if (readyWhenFail && appName != null) {
                MonitorMain.getInstance().readyApplication(appName, false);
            }
            return;
        }
        String path = "/" + Integer.toHexString(appId.getOID()) + "/" + Integer.toHexString(appId.getAID()) + "/";
        ixcWorker.lookup(path, serviceName, this);
    }


    public void dataUpdated(String key, Object old, Object value) {
        if (key.equals(LoadingAnimationService.IXC_NAME)) {
            loadingService = (LoadingAnimationService) value;
            Thread startLoadingThread = new Thread() {
                public void run() {
                    if (Log.INFO_ON) {
                        Log.printInfo(LOG_HEADER + "start loading animation.");
                    }
                    try {
                        Point p =  new Point(Constants.SCREEN_WIDTH/2, Constants.SCREEN_HEIGHT/2);
                        loadingService.showLoadingAnimation(p);
                    } catch (Exception e) {
                        Log.print(e);
                    }
                }
            };
            startLoadingThread.start();

        } else if (key.equals(MainMenuService.IXC_NAME)) {
            mainMenuService = (MainMenuService) value;
            MonitorMain.getInstance().readyApplication(DataManager.getInstance().getMenuName(), true);

        } else if (key.equals(EpgService.IXC_NAME)) {
            epgService = (EpgService) value;

        } else if (key.equals(VODService.IXC_NAME)) {
            vodService = (VODService) value;

        } else if (key.equals(PreferenceService.IXC_NAME)) {
            preferenceService = (PreferenceService) value;
            PreferenceServiceProxy.getInstance().readyPreferenceService(preferenceService);

        } else if (key.equals(SearchService.IXC_NAME)) {
            searchService = (SearchService) value;

        } else if (key.equals(ErrorMessageService.IXC_NAME)) {
            errorMessageService = (ErrorMessageService) value;

        } else if (key.equals(NotificationService.IXC_NAME)) {
            notificationService = (NotificationService) value;
            NotificationCenterProxy.getInstance().setNotificationService(notificationService);
            try {
                WatchingTimeRestrictionManager.getInstance().setNotificationPopupTime(notificationService.getDisplayTime());
            } catch (Exception e) {
                Log.print(e);
            }

        } else if (key.equals(StcService.IXC_NAME)) {
            stcService = (StcService) value;
            try {
                int currentLevel = stcService.registerApp(FrameworkMain.getInstance().getApplicationName());
                Log.printDebug("stc log level = " + currentLevel);
                Log.setStcLevel(currentLevel);
            } catch (RemoteException ex) {
                Log.print(ex);
            }
            Object o = SharedMemory.getInstance().get("stc-" + FrameworkMain.getInstance().getApplicationName());
            if (o != null) {
                DataCenter.getInstance().put("LogCache", o);
            }
            try {
                stcService.addLogLevelChangeListener(FrameworkMain.getInstance().getApplicationName(), logListener);
            } catch (RemoteException ex) {
                Log.print(ex);
            }

        } else if (key.equals(VbmService.IXC_NAME)) {
            vbmService = (VbmService) value;
            MonitorVbmController.getInstance().init(vbmService);

        } else if (key.equals(PvrService.IXC_NAME)) {
            pvrService = (PvrService) value;

        } else if (key.equals(ISAService.IXC_NAME)) {
            isaService = (ISAService) value;
        }
    }

    public void dataRemoved(String key)  {

    }

    /**
     * Gets the Menu application's IXC interface instance.
     * @return the IXC interface instance of Menu.
     */
    public MainMenuService getMainMenuService() {
        return mainMenuService;
    }

    /**
     * Gets the EPG application's IXC interface instance.
     * @return the IXC interface instance of EPG.
     */
    public EpgService getEpgService() {
        return epgService;
    }

    /**
     * Gets the VOD application's IXC interface instance.
     * @return the IXC interface.
     */
    public VODService getVODService() {
        return vodService;
    }

    /**
     * Gets the UPP application's IXC interface instance.
     * @return the IXC interface.
     */
    public PreferenceService getPreferenceService() {
        return preferenceService;
    }

    /**
     * Gets the Loading animation's IXC interface instance.
     * @return the IXC interface.
     */
    public LoadingAnimationService getLoadingService() {
        return loadingService;
    }

    /**
     * Gets the Search's IXC interface instance.
     * @return the IXC interface.
     */
    public SearchService getSearchService() {
        return searchService;
    }
    /**
     * Gets the ErrorMessage's IXC interface instance.
     * @return the IXC interface.
     */
    public ErrorMessageService getErrorMessageService() {
        return errorMessageService;
    }

    /**
     * Gets the Notification's IXC interface instance.
     * @return
     */
    public NotificationService getNotificationService() {
        return notificationService;
    }

    /**
     * Gets the ISA's IXC interface instance.
     * @return
     */
    public ISAService getIsaService() {
        return isaService;
    }

    /**
     * Finds the application id by application name in the AppsDatabase.
     * @param applicationName the application name.
     * @return AppID's instance.
     */
    private AppID findApplicationId(String applicationName) {
        if (applicationName == null) {
            return null;
        }
        AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                AppAttributes attr = appsDB.getAppAttributes(appID);
                String name = attr.getName();
                if (applicationName.equals(name)) {
                    return appID;
                }
            }
        }
        return null;
    }

    public void startLoadingAnimation() {
        if (loadingService != null) {
            try {
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "start loading animation");
                }
                loadingService.showLoadingAnimation(new Point(480, 270));
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    public void stopLoadingAnimation() {
        InbandDataManager.getInstance().status.stop();
        if (loadingService != null) {
            try {
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "stop loading animation");
                }
                loadingService.hideLoadingAnimation();
            } catch (Exception e) {
                Log.print(e);
            }
        }
    }

    /**
     * Interface to communication with Error Message.
     * @param errorCode
     * @param l
     */
    public void showErrorMessage(String errorCode, ErrorMessageListener l) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "showErrorMessage : " + errorCode);
        }
        if(errorMessageService == null) {
            if (Log.ERROR_ON) {
                Log.printError(LOG_HEADER + "Not bound ErrorMessageService");
            }
            return;
        }
        try {
            errorMessageService.showErrorMessage(errorCode, l);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void hideErrorMessage() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "hideErrorMessage");
        }
        if(errorMessageService == null) {
            if (Log.ERROR_ON) {
                Log.printError(LOG_HEADER + "Not bound ErrorMessageService");
            }
            return;
        }
        try {
            errorMessageService.hideErrorMessage();
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void setDeactivatedSTB(boolean deactivate) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "setDeactivatedSTB() " + deactivate);
        }
        try {
            if(mainMenuService != null) {
                mainMenuService.setDeactivatedSTB(deactivate);
            }
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void startPvrMainMenu() {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startPvrMainMenu : ");
        }
    	try {
    		if(pvrService != null) {
    			pvrService.showMenu(PvrService.MENU_PVR_PORTAL);
    		}
    	} catch (Exception e) {
            Log.print(e);
        }
    }

    public void stopPVRMain() {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "stopPvrMainMenu : ");
        }
    	try {
    		if(pvrService != null) {
    			pvrService.showMenu(PvrService.MENU_PLAY_RECORDING);
    		}
    	} catch (Exception e) {
            Log.print(e);
        }
    }

    public void startVodMainMenu() {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startVodMainMenu : ");
        }
    	try {
    		if(vodService != null) {
    			vodService.requestToStop();
    		}
    	} catch (Exception e) {
            Log.print(e);
        }
    }
    
    public boolean resetAPDTimer(boolean force) {
    	if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "resetAPDTimer");
		}
    	// for repeat key
    	if (!force) {
    		if (lastTimeRequestAPD > System.currentTimeMillis() - 10000) {
    			if (Log.INFO_ON) {
    				Log.printInfo(LOG_HEADER + "resetAPDTimer, ignore a key event in range under 10 sec");
    			}
    			return false;
    		}
    	}
    	
    	lastTimeRequestAPD = System.currentTimeMillis();
    	PreferenceService pService = CommunicationManager.getInstance().getPreferenceService();
        
    	if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "resetAPDTimer, pService=" + pService);
		}
    	
    	if (pService != null) {
        	try {
				return pService.resetAPDTimer();
			} catch (RemoteException e) {
				Log.print(e);
			}
        }
        return false;
    }
    
    public boolean resetAPDTimerWithKeycode(int keycode) {
    	if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "resetAPDTimerWithKeycode");
		}
    	// for repeat key
		if (lastTimeRequestAPD > System.currentTimeMillis() - 10000) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "resetAPDTimerWithKeycode, ignore a key event in range under 10 sec");
			}
			
			return false;
		}
    	
    	lastTimeRequestAPD = System.currentTimeMillis();
    	PreferenceService pService = CommunicationManager.getInstance().getPreferenceService();
        
    	if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "resetAPDTimerWithKeycode, pService=" + pService);
		}
    	
    	if (pService != null) {
        	try {
        		return pService.resetAPDTimerWithKeycode(keycode);
			} catch (RemoteException e) {
				Log.print(e);
			}
        }
    	return false;
    }

    class LogLevelAdapter implements LogLevelChangeListener {
    	public void logLevelChanged(int logLevel) throws RemoteException {
            Log.setStcLevel(logLevel);
        }
    }

}

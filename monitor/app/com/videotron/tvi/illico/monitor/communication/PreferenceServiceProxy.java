package com.videotron.tvi.illico.monitor.communication;

import java.util.ArrayList;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceListener;
import com.videotron.tvi.illico.ixc.upp.PreferenceNames;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.core.WatchingTimeRestrictionManager;

/**
 * @author Woosik Lee
 */
public class PreferenceServiceProxy {

    private PreferenceService preService;

    private boolean isReadyPreferenceService = false;

    private ArrayList listeners;

    private static PreferenceServiceProxy instance;

    private String[] preferenceValues;

    public static final String PNAME_COMPLETE_WIZARD = "COMPLETE_WIZARD";
    public static final String PDEFAULT_COMPLETE_WIZARD = "false";

    public static final String RUNNING_STATE_FLAG_FOR_XAIT_UPDATE = "RUNNING_STATE_FLAG_FOR_XAIT_UPDATE";
    public static final String RUNNING_STATE_FLAG_RUNNING = "R";
    public static final String RUNNING_STATE_FLAG_STANDBY = "S";

    public static final String PNAME_LANGUAGE = PreferenceNames.LANGUAGE;
    public static final String PDEFAULT_LANGUGAGE = Definitions.LANGUAGE_FRENCH;

    public static final String PNAME_STANDALONE_AUTHORITY_RESULT_NAME = "STANDALONE_AUTHORITY_RESULT_NAME";
    public static final String PDEFAULT_STANDALONE_AUTHORITY_RESULT_NAME = "-1";

    public static final String PNAME_STANDALONE_AUTHORITY_EID_NAME = "STANDALONE_AUTHORITY_EID_NAME";
    public static final String PDEFAULT_STANDALONE_AUTHORITY_EID_NAME = "-1";

    public static final String PNAME_PARENTAL_CONTROL = RightFilter.PARENTAL_CONTROL;
    public static final String PDEFAULT_PARENTAL_CONTROL = Definitions.OPTION_VALUE_ON;

    private static final String[] PREFERENCE_NAMES = {PNAME_COMPLETE_WIZARD, RUNNING_STATE_FLAG_FOR_XAIT_UPDATE,
            PNAME_LANGUAGE, PNAME_STANDALONE_AUTHORITY_RESULT_NAME, PNAME_STANDALONE_AUTHORITY_EID_NAME, PNAME_PARENTAL_CONTROL};

    private static final String[] PREFERENCE_DEFAULT_VALUES = {PDEFAULT_COMPLETE_WIZARD, RUNNING_STATE_FLAG_RUNNING,
            PDEFAULT_LANGUGAGE, PDEFAULT_STANDALONE_AUTHORITY_RESULT_NAME, PDEFAULT_STANDALONE_AUTHORITY_EID_NAME, PDEFAULT_PARENTAL_CONTROL};

    private static final String LOG_HEADER = "[PreferenceServiceProxy] ";

    public synchronized static PreferenceServiceProxy getInstance() {
        if (instance == null) {
            instance = new PreferenceServiceProxy();
        }
        return instance;
    }

    public void readyPreferenceService(PreferenceService pService) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "readyPreferenceService()");
        }
        this.preService = pService;
        try {
            preferenceValues = preService.addPreferenceListener(preListener, FrameworkMain.getInstance()
                    .getApplicationName(), PREFERENCE_NAMES, PREFERENCE_DEFAULT_VALUES);
            printPreferenceValue();
        } catch (Exception e) {
            Log.print(e);
        }

        // Set Language
        if(Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "current language is " + preferenceValues[2]);
        }
        String value = preferenceValues[2];
        DataCenter.getInstance().put(PNAME_LANGUAGE, value);

        isReadyPreferenceService = true;
        WatchingTimeRestrictionManager.getInstance().readyPreferenceService();
    }

    public boolean isReadyPreferenceService() {
        return isReadyPreferenceService;
    }

    public String getPreferenceValue(String name) {
        if (preferenceValues != null) {
            for (int i = 0; i < PREFERENCE_NAMES.length; i++) {
                if (name.equals(PREFERENCE_NAMES[i])) {
                    return preferenceValues[i];
                }
            }
        }
        return null;
    }

    public void setPreferenceValue(String name, String value) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "setPreferenceValue() name=" + name + ", value=" + value);
        }
        try {
            preService.setPreferenceValue(name, value);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    private PreferenceListener preListener = new PreferenceListener() {
        public void receiveUpdatedPreference(String preferenceName, String value) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "receiveUpdatedPreference() " + preferenceName + ", "
                        + value);
            }

            if (preferenceValues != null) {
            	if(preferenceName.equals(PNAME_LANGUAGE)) {
                	if(Log.INFO_ON) {
                		Log.printInfo(LOG_HEADER + "Set to data center language is" + value);
                	}
                	DataCenter.getInstance().put(PNAME_LANGUAGE, value);
                }
            	
                for (int i = 0; i < PREFERENCE_NAMES.length; i++) {
                    if (preferenceName.equals(PREFERENCE_NAMES[i])) {
                        preferenceValues[i] = value;
                        notifyUpdatePreferenceValue(i);
                        break;
                    }
                }                
            }
        }
    };

    private PreferenceServiceProxy() {
        listeners = new ArrayList();
    }

    public void addPreferenceServiceListener(PreferenceServiceProxyListener l) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "addPreferenceListener()");
        }
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public void removePreferenceServiceListener(PreferenceServiceProxyListener l) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "removePreferenceListener()");
        }
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    private void notifyUpdatePreferenceValue(int preferenceIndex) {
        Object[] lsnrs;
        synchronized (listeners) {
            lsnrs = listeners.toArray();
        }

        for (int i = 0; i < lsnrs.length; i++) {
            PreferenceServiceProxyListener listener = (PreferenceServiceProxyListener) lsnrs[i];
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "notifyUpdatePreferenceValue() to " + listener);
            }
            listener.notifyUpdatePreferenceValue(PREFERENCE_NAMES[preferenceIndex], preferenceValues[preferenceIndex]);
        }
    }

    private void printPreferenceValue() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + " ::: Print Preference Values::::::::::::::::::::");
            for (int i = 0; i < PREFERENCE_NAMES.length; i++) {
                Log.printInfo(PREFERENCE_NAMES[i] + " : " + preferenceValues[i] + " (Default:"
                        + PREFERENCE_DEFAULT_VALUES[i] + ")");
            }
            Log.printInfo("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
        }
    }

}

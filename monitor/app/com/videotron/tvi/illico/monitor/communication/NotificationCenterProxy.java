package com.videotron.tvi.illico.monitor.communication;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.videotron.tvi.illico.ixc.notification.NotificationListener;
import com.videotron.tvi.illico.ixc.notification.NotificationOption;
import com.videotron.tvi.illico.ixc.notification.NotificationService;
import com.videotron.tvi.illico.log.Log;

public class NotificationCenterProxy implements NotificationListener {

    private NotificationService service;

    private ArrayList listeners;

    private static NotificationCenterProxy instance;

    private WTRMPrepareOption wtrmPrepareOption = new WTRMPrepareOption();

    private static final String LOG_HEADER = "[NotificationCenterProxy] ";

    public synchronized static NotificationCenterProxy getInstance() {
        if (instance == null) {
            instance = new NotificationCenterProxy();
        }
        return instance;
    }

    private NotificationCenterProxy() {
        listeners = new ArrayList();
    }

    public NotificationOption getWTRMPrepareOption() {
        return wtrmPrepareOption;
    }

    public void setNotificationService(NotificationService s) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "setNotificationService()");
        }
        this.service = s;
        try {
            service.setRegisterNotificaiton("Monitor", this);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    public void addNotificationListener(NotificationCenterProxyListener l) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "addNotificationListener()");
        }
        synchronized (listeners) {
            listeners.add(l);
        }
    }

    public void removeNotificationListener(NotificationCenterProxyListener l) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "removeNotificationListener()");
        }
        synchronized (listeners) {
            listeners.remove(l);
        }
    }

    public String setNotify(NotificationOption option) {
        if (service == null) {
            return null;
        }
        try {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "setNotify() " + option.getNotificationPopupType());
            }
            String messageId = service.setNotify(option);
            return messageId;
        } catch (Exception e) {
            Log.print(e);
        }
        return null;
    }

    public String setNotifyAndTime(NotificationOption option, long time) {
        if (service == null) {
            return null;
        }
        try {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "setNotifyAndTime() " + option.getNotificationPopupType() + ", time=" + time);
            }
            String messageId = service.setNotifyAndTime(option, time);
            return messageId;
        } catch (Exception e) {
            Log.print(e);
        }
        return null;
    }

    public void removeNotify(String messageId) {
        if (service == null) {
            return;
        }
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "removeNotify() " + messageId);
        }
        try {
            service.removeNotify(messageId);
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     *
     */
    public void action(String id) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "action() " + id);
        }

        Object[] lsnrs;
        synchronized (listeners) {
            lsnrs = listeners.toArray();
        }

        for(int i=0; i<lsnrs.length; i++) {
            NotificationCenterProxyListener listener = (NotificationCenterProxyListener) lsnrs[i];
            listener.action(id);
        }
    }

    public void cancel(String id) throws RemoteException {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "cancel() " + id);
        }

        Object[] lsnrs;
        synchronized (listeners) {
            lsnrs = listeners.toArray();
        }

        for(int i=0; i<lsnrs.length; i++) {
            NotificationCenterProxyListener listener = (NotificationCenterProxyListener) lsnrs[i];
            listener.cancel(id);
        }
    }

    public void directShowRequested(String platform, String[] action) throws RemoteException {

    }

    // //////////////////////////////////////////////
    // Following is the NotificationOption classes.
    // //////////////////////////////////////////////

    /**
     * WatchingTimeRestrictionManager
     */
    public class WTRMPrepareOption implements NotificationOption {
        /**
         * Gets the ApllicationName
         * @return
         * @throws RemoteException
         */
        public String getApplicationName() throws RemoteException {
            return "Monitor";
        }

        /**
         * Gets the create date of Notification message.
         * @return
         * @throws RemoteException
         */
        public long getCreateDate() throws RemoteException {
            return 0;
        }

        /**
         * Whether or not.
         * @return
         * @throws RemoteException
         */
        public boolean isViewed() throws RemoteException {
            return false;
        }

        /**
         * Gets display count.
         * @return count.
         * @throws RemoteException
         */
        public int getDisplayCount() throws RemoteException {
            return 0;
        }

        /**
         * Gets a message ID.
         * @return message.
         * @throws RemoteException the remote exception
         */
        public String getMessageID() throws RemoteException {
            return null;
        }

        /**
         * Gets the categories.
         * @return the categories
         * @throws RemoteException the remote exception
         */
        public int getCategories() throws RemoteException {
            return 0;
        }

        /**
         * Gets the display time.
         * @return the display time.
         * @throws RemoteException the remote exception
         */
        public long getDisplayTime() throws RemoteException {
            return System.currentTimeMillis();
        }

        /**
         * Gets the reminder viewing Sum.
         * @return the reminder time. count.
         * @throws RemoteException the remote exception
         */
        public int getRemainderTime() throws RemoteException {
            return 0;
        }

        /**
         * Gets the reminder interval time.
         * @return the reminder delay. Value is a milliseconds.
         * @throws RemoteException the remote exception
         */
        public long getRemainderDelay() throws RemoteException {
            return 0;
        }

        /**
         * Is the countDwon.
         * @return
         * @throws RemoteException
         */
        public boolean isCountDown() throws RemoteException {
            return true;
        }

        /**
         * Gets the message.
         * @return the message
         * @throws RemoteException the remote exception
         */
        public String getMessage() throws RemoteException {
            return null;
        }

        /**
         * Gets the sub_message.
         * @return the message
         * @throws RemoteException the remote exception
         */
        public String getSubMessage() throws RemoteException {
            return null;
        }

        /**
         * Gets the notification popup type.
         * @return the notification status
         * @throws RemoteException the remote exception
         */
        public int getNotificationPopupType() throws RemoteException {
            return NotificationOption.TIMEBASED_RESTRICTIONS_POPUP;
        }

        /**
         * Gets the notification action.
         * @return the notification action
         * @throws RemoteException the remote exception
         */
        public int getNotificationAction() throws RemoteException {
            return NotificationOption.ACTION_PIN_POPUP;
        }

        /**
         * Gets the notification action data. [0] is application name or channel name, [1] is data. The parameters of
         * action.
         * @return the notification action data
         * @throws RemoteException the remote exception
         */
        public String[] getNotificationActionData() throws RemoteException {
            return null;
        }

        /**
         * Gets Large Popup Message.
         * @return message.
         * @throws RemoteException
         */
        public String getLargePopupMessage() throws RemoteException {
            return null;
        }

        /**
         * Gets Large Popup sub Message.
         * @return message.
         * @throws RemoteException
         */
        public String getLargePopupSubMessage() throws RemoteException {
            return null;
        }

        /**
         * Gets large button Info. [0][0] is a button Name. [0][1] is ActionType. [0][2] is application name or channel
         * name, [0][n] is data.
         * @return
         * @throws RemoteException
         */
        public String[][] getButtonNameAction() throws RemoteException {
            return null;
        }
    }

}

package com.videotron.tvi.illico.monitor.communication;

/**
 * Internal listener for the PreferenceListener.
 * @author Woosik Lee
 */
public interface PreferenceServiceProxyListener {
    public void notifyUpdatePreferenceValue(String name, String value);
}

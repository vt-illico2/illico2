package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import org.dvb.ui.DVBColor;
import org.dvb.event.UserEvent;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.WindowProperty;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.ColorUtil;
import com.videotron.tvi.illico.ui.ScrollTexts;
import com.videotron.tvi.illico.log.Log;

import com.videotron.tvi.illico.monitor.data.ListBehavior;
/**
 * Popup when "LIST" button pressed in non PVR STB
 *
 * @author Kenneth
 *
 */
public class ListPressedInNonPvrPopup extends LayeredWindow implements LayeredKeyHandler {
    private static final String POPUP_NAME = "LIST_PRESSED_IN_NON_PVR_POPUP";
    private static ListPressedInNonPvrPopup instance;
    private LayeredUI rootWindow;
    ScrollTexts scroll;

    public static ListPressedInNonPvrPopup getInstance() {
        if (instance == null) {
            instance = new ListPressedInNonPvrPopup();
        }
        return instance;
    }

    protected ListPressedInNonPvrPopup() {
        setBounds(0, 0, Constants.SCREEN_WIDTH, Constants.SCREEN_HEIGHT);
        rootWindow = LayeredUIManager.getInstance().createLayeredDialog(POPUP_NAME,
                WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), false, this, Constants.SCREEN_BOUNDS, this);

        //->Kenneth[2015.1.8] : 5331 완전 수정 안됨. 8라인만 있을 경우 소문자 하단 잘림.
        // 따라서 ScrollTexts 의 하단을 5 pixel 늘리고 대신 setBottomMaskImagePosition(0, 126) 을 해서 위치 고정함
        scroll = new ScrollTexts();
        int height = 144+5;
        scroll.setBounds(new Rectangle(189, 261, 588 ,height));
        // top, left, bottom, right insets
        scroll.setInsets(0, 25, 0, 25);
        scroll.setForeground(ColorUtil.getColor(190, 190, 190));
        scroll.setFont(FontResource.DINMED.getFont(16));
        scroll.setRows(8);
        //->Kenneth[2014.12.11] : VDTRMASTER-5331 height 를 19 -> 17
        scroll.setRowHeight(17);
        scroll.setCenterAlign(true);
        scroll.setBottomMaskImage(DataCenter.getInstance().getImage("mr_pop_sh_b.png"));
        scroll.setBottomMaskImagePosition(0, 126);
        scroll.setTopMaskImage(DataCenter.getInstance().getImage("mr_pop_sh_t.png"));
        scroll.setVisible(true);
        this.add(scroll);

        setVisible(true);
    }

    public void showPopup() {
        if (Log.DEBUG_ON) Log.printDebug("ListPressedInNonPvrPopup.showPopup()");
        resetData();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        rootWindow.activate();
    }

    private void resetData() {
        scroll.setContents(ListBehavior.getInstance().getDescription());
    }

    public void hidePopup() {
        if (Log.DEBUG_ON) Log.printDebug("ListPressedInNonPvrPopup.hidePopup()");
    	PopupImageManager.getInstance().flushImages();
        rootWindow.deactivate();
    }

    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(rootWindow);
    }

	public boolean handleKeyEvent(UserEvent e) {
        int keyCode = e.getCode();
		if (e.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
        switch (keyCode) {
            case KeyEvent.VK_ENTER :
                hidePopup();
                return true;
            case OCRcEvent.VK_PAGE_UP:
                scroll.showPreviousPage();
                return true;
            case OCRcEvent.VK_PAGE_DOWN:
                scroll.showNextPage();
                return true;
            //case KeyCodes.LIST:
                //hidePopup();
                //return true;
            case OCRcEvent.VK_EXIT:
                hidePopup();
                return true;
        }
        //return false;
        return true;
    }

    public void paint(Graphics g) {
        g.setColor(ColorUtil.getColor(0, 0, 0, 204));
        g.fillRect(0, 0, getBounds().width, getBounds().height);
        g.setColor(ColorUtil.getColor(33, 33, 33));
        g.fillRect(170, 51, 625, 435);
        g.drawImage(DataCenter.getInstance().getImage("12_pop_high_625.png"), 170, 51, 625, 378, this);
        g.drawImage(DataCenter.getInstance().getImage("07_respop_title.png"), 197, 89, this);
        g.drawImage(DataCenter.getInstance().getImage("pop_sep_589.png"), 196, 426, this);
        g.drawImage(DataCenter.getInstance().getImage("05_focus.png"), 403, 440, this);
        g.drawImage(DataCenter.getInstance().getImage("12_ppop_sha.png"), 170, 483, 625, 57, this);

        Image banner = ListBehavior.getInstance().getBannerImage();
        if (Log.EXTRA_ON) Log.printDebug("ListPressedInNonPvrPopup.paint() : banner = "+banner);
        if (banner != null) {
            g.drawImage(banner, 189, 109, 588, 98, this);
        }
        g.setFont(FontResource.BLENDER.getFont(24));
        g.setColor(ColorUtil.getColor(250, 202, 0));
        if (Log.EXTRA_ON) Log.printDebug("ListPressedInNonPvrPopup.paint() : title = "+ListBehavior.getInstance().getTitle());
        GraphicUtil.drawStringCenter(g, ListBehavior.getInstance().getTitle(), 482, 78);
        g.setFont(FontResource.BLENDER.getFont(20));
        g.setColor(ColorUtil.getColor(254, 234, 160));
        GraphicUtil.drawStringCenter(g, ListBehavior.getInstance().getFunctionalText(), 479, 238);
        g.setFont(FontResource.BLENDER.getFont(18));
        g.setColor(ColorUtil.getColor(3, 3, 3));
        if (Log.EXTRA_ON) Log.printDebug("ListPressedInNonPvrPopup.paint() : buttonName = "+ListBehavior.getInstance().getButtonName());
        GraphicUtil.drawStringCenter(g, ListBehavior.getInstance().getButtonName(), 481, 461);
        super.paint(g);
    }
}

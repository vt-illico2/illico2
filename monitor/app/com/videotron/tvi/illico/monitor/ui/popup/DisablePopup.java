package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.alticast.ui.LayeredKeyHandler;

/**
 * 
 * @author WSLEE1
 *
 */
public class DisablePopup extends Popup {
    /** The serial version uid. */
    public static final long serialVersionUID = 2010071000L;
    /** Size of screen. */
    private static final Rectangle BOUNDS = new Rectangle(0,0,960,540);
    
    /**
     * Constructor.
     * @param priority
     * @param keyHandler
     */
    public DisablePopup(int priority, LayeredKeyHandler keyHandler) {
        super("", "DisablePopup", priority, new String[] {""}, BOUNDS, keyHandler);
    }
    
    public void paint(Graphics g) {
        g.setColor(Color.black);
        g.fillRect(BOUNDS.x, BOUNDS.y, BOUNDS.width, BOUNDS.height);
    }
}

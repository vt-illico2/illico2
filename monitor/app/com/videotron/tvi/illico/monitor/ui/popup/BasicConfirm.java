package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Graphics;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * The popup that has "OK" button.
 *
 * @author Woosik Lee
 *
 */
public class BasicConfirm extends BasicPopup {

    /** The serial version uid. */
    public static final long serialVersionUID = 2010061000L;
    
    private String MSG_OK;

    /**
     * Constructor with LayeredKeyHandler.
     *
     * @param title
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param message
     *            the popup message.
     * @param keyHandler
     *            the key handler.
     */
    public BasicConfirm(final String title, final String name, final int priority, final String[] message,
            final LayeredKeyHandler keyHandler) {
        super(title, name, priority, message, keyHandler);
        MSG_OK = DataCenter.getInstance().getString(KeyNames.BTN_OK);
    }

    /**
     * Implements the Component class's paint method.
     *
     * @param g
     *            the instance of Graphics.
     */
    public void paint(Graphics g) {
        super.paint(g);
        g.drawImage(PopupImageManager.getInstance().img_05_focus, 403, 318, this);
        g.setColor(COLOR_BUTTON);
        g.setFont(BLENDER18);
        GraphicUtil.drawStringCenter(g, MSG_OK, 481, 339);
    }
}

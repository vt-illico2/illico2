package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.videotron.tvi.illico.util.FontResource;

public class TestPopup extends Popup {

    /** The serial version uid. */
    public static final long serialVersionUID = 2010061000L;

    private String message = "Not loaded";
    private String message2 = "";
    private String time = "";

    public TestPopup() {
        super("", "", 10000, new String[] { "", "" }, new Rectangle(100, 100, 500, 100), null);
    }

    public void setMessage(String msg, String msg2) {
        this.message = msg;
        this.message2 = msg2;
        repaint();
    }

    public void setTime(String msg) {
        this.time = msg;
        repaint();
    }

    /** The instance of Color for the popup. */
    public static final Color COLOR_MESSAGE = new Color(255, 255, 255);
    private static final DVBColor BACKGROUND_COLOR = new DVBColor(12, 12, 12, 90);
    /** The instance of font for the popup. */
    public static final Font BLENDER20 = FontResource.BLENDER.getFont(20);

    public void paint(Graphics g) {
        g.setColor(BACKGROUND_COLOR);
        g.fillRect(0, 0, getBounds().width, getBounds().height);
        g.setColor(COLOR_MESSAGE);
        g.setFont(BLENDER20);
        g.drawString("OOB OC test message by Monitor(2)", 10, 20);
        g.drawString(message, 10, 40);
        g.drawString(message2, 10, 60);
        g.drawString("Time : " + time, 10, 80);
    }

}

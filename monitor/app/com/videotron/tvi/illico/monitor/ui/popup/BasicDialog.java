package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Graphics;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * The popup include two button.
 *
 * @author Woosik Lee
 *
 */
public class BasicDialog extends BasicPopup {

    /** The serial version uid. */
    public static final long serialVersionUID = 2010061000L;
    
    private String MSG_OK;
    private String MSG_CANCEL;

    /**
     * Constructor with LayeredKeyHandler.
     *
     * @param title
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param message
     *            the popup message.
     * @param keyHandler
     *            the key handler.
     */
    public BasicDialog(final String title, final String name, final int priority, final String[] message,
            final LayeredKeyHandler keyHandler) {
        super(title, name, priority, message, keyHandler);
        
        MSG_OK = DataCenter.getInstance().getString(KeyNames.BTN_OK);
        MSG_CANCEL = DataCenter.getInstance().getString(KeyNames.BTN_CANCEL);
    }

    //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
    public BasicDialog(final String title, final String name, final int priority, final String[] message,
            final LayeredKeyHandler keyHandler, String leftButtonName, String rightButtonName) {
        super(title, name, priority, message, keyHandler);
        
        MSG_OK = leftButtonName;
        MSG_CANCEL = rightButtonName;
    }
    //<-

    /**
     * Implements the Component class's paint method.
     *
     * @param g
     *            the instance of Graphics.
     */
    public void paint(Graphics g) {
        super.paint(g);

        if (getFocusBtn()) {
            g.drawImage(PopupImageManager.getInstance().img_05_focus, 321, 318, this);
            g.drawImage(PopupImageManager.getInstance().img_05_focus_dim, 485, 318, this);
        } else {
            g.drawImage(PopupImageManager.getInstance().img_05_focus_dim, 321, 318, this);
            g.drawImage(PopupImageManager.getInstance().img_05_focus, 485, 318, this);
        }

        g.setColor(COLOR_BUTTON);
        g.setFont(BLENDER18);
        GraphicUtil.drawStringCenter(g, MSG_OK, 403, 339);
        GraphicUtil.drawStringCenter(g, MSG_CANCEL, 561, 339);
    }
}

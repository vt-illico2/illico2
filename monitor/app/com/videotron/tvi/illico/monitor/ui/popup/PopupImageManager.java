package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Image;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;

/**
 * Image loader for Monitor popup.
 * @author WSLEE1
 *
 */
public class PopupImageManager {

	/**
	 * Log header message.
	 */
	private static final String LOG_HEADER = "[PopupImageManager] ";
	
	public Image img_05_focus;
	public Image img_05_focus_dim;
	public Image img_pop_sha;
    public Image img_05_pop_glow_m;
    public Image img_05_pop_glow_b;
    public Image img_05_pop_glow_t;
    public Image img_pop_gap_379;
    public Image img_pop_high_350;
    
    private static PopupImageManager instance;
    
    private int popupCount = 0;
    
    /**
     * Gets the singleton instance.
     * @return PoopuImageManager.
     */
    public synchronized static PopupImageManager getInstance() {
    	if(instance == null) {
    		instance = new PopupImageManager();
    	}
    	return instance;
    }
    
    /**
     * Constructor.
     */
    private PopupImageManager() {
    	popupCount = 0;
    }
    
    /**
     * Load images.
     */
    public synchronized void loadImages() {
    	popupCount ++;
    	
    	if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "loadImages() popupCount= " + popupCount);
        }
    	
    	if(popupCount == 1) {
    		img_05_focus = DataCenter.getInstance().getImage("05_focus.png");
    	    img_05_focus_dim = DataCenter.getInstance().getImage("05_focus_dim.png");
    	    img_pop_sha = DataCenter.getInstance().getImage("pop_sha.png");
    	    img_05_pop_glow_m = DataCenter.getInstance().getImage("05_pop_glow_m.png");
    	    img_05_pop_glow_b = DataCenter.getInstance().getImage("05_pop_glow_b.png");
    	    img_05_pop_glow_t = DataCenter.getInstance().getImage("05_pop_glow_t.png");
    	    img_pop_gap_379 = DataCenter.getInstance().getImage("pop_gap_379.png");
    	    img_pop_high_350 = DataCenter.getInstance().getImage("pop_high_350.png");
    	    FrameworkMain.getInstance().getImagePool().waitForAll();
    	}
    	
    }
    
    /**
     * Remove loaded images.
     */
    public synchronized void flushImages() {
    	popupCount --;
    	
    	if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "flushImages() popupCount= " + popupCount);
        }
    	
    	if(popupCount == 0) {
    		DataCenter.getInstance().removeImage("05_focus.png");
    	    DataCenter.getInstance().removeImage("05_focus_dim.png");
    	    DataCenter.getInstance().removeImage("pop_sha.png");
    	    DataCenter.getInstance().removeImage("05_pop_glow_m.png");
    	    DataCenter.getInstance().removeImage("05_pop_glow_b.png");
    	    DataCenter.getInstance().removeImage("05_pop_glow_t.png");
    	    DataCenter.getInstance().removeImage("pop_gap_379.png");
    	    DataCenter.getInstance().removeImage("pop_high_350.png");
    	}
    }
	
}

package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import org.dvb.ui.DVBColor;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;

/**
 * The basic popup. This popup haven't button.
 *
 * @author Woosik Lee
 *
 */
public class BasicPopup extends Popup {

    /** The serial version uid. */
    public static final long serialVersionUID = 2010061000L;

    /** The instance of Rectangle for the popup. */
    private static final Rectangle BOUNDS = new Rectangle(0, 0, 960, 540);
    /** The background color for the popup. */
    private static final DVBColor BACKGROUND_COLOR = new DVBColor(12, 12, 12, 90);
    
    private static final Color POPUP_BG_COLOR = new Color(35, 35, 35);
    /** The instance of font for the popup. */
    public static final Font BLENDER18 = FontResource.BLENDER.getFont(18);
    /** The instance of font for the popup. */
    public static final Font BLENDER20 = FontResource.BLENDER.getFont(20);
    /** The instance of font for the popup. */
    public static final Font BLENDER24 = FontResource.BLENDER.getFont(24);
    /** The instance of Color for the popup. */
    public static final Color COLOR_TITLE = new Color(252, 202, 4);
    /** The instance of Color for the popup. */
    public static final Color COLOR_MESSAGE = new Color(255, 255, 255);
    /** The instance of Color for the popup. */
    public static final Color COLOR_BUTTON = new Color(3, 3, 3);
    
    /**
     * Constructor with LayeredKeyHandler.
     *
     * @param title
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param message
     *            the popup message.
     * @param keyHandler
     *            the key handler.
     */
    public BasicPopup(final String title, final String name, final int priority, final String[] message,
            final LayeredKeyHandler keyHandler) {
        super(title, name, priority, message, BOUNDS, keyHandler);
    }

    /**
     * Constructor without LayeredKeyHandler.
     *
     * @param title
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param pMessages
     *            the popup message.
     */
    public BasicPopup(final String title, final String name, final int priority, final String[] pMessages) {
        super(title, name, priority, pMessages, BOUNDS, null);
    }

    /**
     * Implements the Component class's paint method.
     *
     * @param g
     *            the instance of Graphics.
     */
    public void paint(Graphics g) {
        g.setColor(BACKGROUND_COLOR);
        g.fillRect(0, 0, getBounds().width, getBounds().height);
        
        g.setColor(POPUP_BG_COLOR);
        g.fillRect(306, 143, 350, 224);
        g.drawImage(PopupImageManager.getInstance().img_pop_high_350, 306, 143, this);
        g.drawImage(PopupImageManager.getInstance().img_pop_gap_379, 285, 181, this);
        g.drawImage(PopupImageManager.getInstance().img_05_pop_glow_t, 276, 113, this);
        
        g.drawImage(PopupImageManager.getInstance().img_05_pop_glow_b, 276, 317, this);
        g.drawImage(PopupImageManager.getInstance().img_05_pop_glow_m, 276, 193, 410, 124, this);
        
        g.drawImage(PopupImageManager.getInstance().img_pop_sha, 306, 364, 350, 79, this);

        g.setColor(COLOR_TITLE);
        g.setFont(BLENDER24);
        GraphicUtil.drawStringCenter(g, title, 481, 169);
        
        paintMessage(g);
    }

    /**
     * Draws the messages.
     *
     * @param g
     *            Graphics's instance.
     */
    public void paintMessage(Graphics g) {
        if (messages == null) {
            return;
        }
        g.setColor(COLOR_MESSAGE);
        g.setFont(BLENDER20);
        switch (messages.length) {
        case 1:
            GraphicUtil.drawStringCenter(g, messages[0], 482, 259);
            break;
        case 2:
            GraphicUtil.drawStringCenter(g, messages[0], 482, 239);
            GraphicUtil.drawStringCenter(g, messages[1], 482, 279);
            break;
        case 3:
            GraphicUtil.drawStringCenter(g, messages[0], 482, 219);
            GraphicUtil.drawStringCenter(g, messages[1], 482, 259);
            GraphicUtil.drawStringCenter(g, messages[2], 482, 299);
            break;
        case 4:
            GraphicUtil.drawStringCenter(g, messages[0], 482, 209);
            GraphicUtil.drawStringCenter(g, messages[1], 482, 239);
            GraphicUtil.drawStringCenter(g, messages[2], 482, 269);
            GraphicUtil.drawStringCenter(g, messages[3], 482, 299);
            break;
        default:
            break;
        }
    }
}

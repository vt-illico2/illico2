package com.videotron.tvi.illico.monitor.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.ui.TextBox;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * Scene for blocked time.
 * @author Woosik Lee
 */
public class TerminalBlockedScene extends LayeredWindow {

    public static final long serialVersionUID = 2011010400L;

    /** The root windows made by Layered UI. */
    private LayeredUI rootWindow;

    private String startTime;
    private String endTime;

    private Image av;
    private Image block_bg;
    private Image block_lockimg;
    private Image btn_b;
    
    private TextBox unblockMessage = new TextBox(TextBox.ALIGN_LEFT);    

    private String MSG_TITLE;

    private String MSG_BODY;
    
    private String MSG_TO;
    
    private String MSG_BTN;

    /**
     * Constructor.
     * @param startTime block start time.
     * @param endTime end time.
     * @param keyHandler ui key handler.
     */
    public TerminalBlockedScene(String startTime, String endTime, LayeredKeyHandler keyHandler) {
        this.startTime = startTime;
        this.endTime = endTime;

        setBounds(Constants.SCREEN_BOUNDS);
        rootWindow = LayeredUIManager.getInstance().createLayeredDialog(WindowProperty.TERMINAL_BLOCKED_TIME.getName(),
                WindowProperty.TERMINAL_BLOCKED_TIME.getPriority(), false, this, Constants.SCREEN_BOUNDS, keyHandler);
        
        setLayout(null);
        
        unblockMessage.setFont(FontResource.BLENDER.getFont(17));
        unblockMessage.setForeground(Color.white);
        unblockMessage.setBounds(493, 243, 460, 30);
        add(unblockMessage);
        unblockMessage.setVisible(true);
        
        setVisible(true);
    }

    /**
     * Shows the popup.
     */
    public void showScene() {    	
    	String timeUnblockMsg = DataCenter.getInstance().getString(KeyNames.TIMEBLOCKED_UNBLOCKED_MSG);
    	unblockMessage.setContents(timeUnblockMsg);
    	
    	if(Log.INFO_ON) {
    		Log.printInfo("[TerminalBlockedScene] showScene(), timeUnblockMsg=" + timeUnblockMsg);
    	}
    	MSG_TITLE = DataCenter.getInstance().getString(KeyNames.TIMEBLOCKED_TITLE);
    	MSG_BODY = DataCenter.getInstance().getString(KeyNames.TIMEBLOCKED_MESSAGE);
    	MSG_TO = DataCenter.getInstance().getString(KeyNames.TIMEBLOCKED_MESSAGETO);
    	MSG_BTN = DataCenter.getInstance().getString(KeyNames.TIMEBLOCKED_UNLOCK);
    	
    	av = DataCenter.getInstance().getImage("av.png");
        block_bg = DataCenter.getInstance().getImage("block_bg.png");
        block_lockimg = DataCenter.getInstance().getImage("block_lockimg.png");
        btn_b = DataCenter.getInstance().getImage("btn_b.png");
    	
        FrameworkMain.getInstance().getImagePool().waitForAll();
        rootWindow.activate();
    }

    /**
     * Hides the popup.
     */
    public void hideScene() {
        rootWindow.deactivate();
        DataCenter.getInstance().removeImage("av.png");
        DataCenter.getInstance().removeImage("block_bg.png");
        DataCenter.getInstance().removeImage("block_lockimg.png");
        DataCenter.getInstance().removeImage("btn_b.png");
    }

    /**
     * Disposes the popup.
     */
    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(rootWindow);
    }

    /** The color for font. */
    private static final Color C_216_216_216 = new Color(216, 216, 216);
    /** The color for font. */
    private static final Color C_241_241_241 = new Color(241, 241, 241);
    /** The color for font. */
    private static final Color C_255_207_3 = new Color(255, 207, 3);
    /** The instance of font. */
    private static final Font BLENDER17 = FontResource.BLENDER.getFont(17);
    /** The instance of font. */
    private static final Font BLENDER24 = FontResource.BLENDER.getFont(24);
    /** The instance of font. */
    private static final Font BLENDER27 = FontResource.BLENDER.getFont(27);

    public void paint(Graphics g) {
        g.drawImage(av, 0, 0, null);
        g.drawImage(block_bg, -84, 75, null);

        g.setColor(C_216_216_216);
        g.setFont(BLENDER27);
        g.drawString(MSG_TITLE, 92, 138);

        g.setColor(Color.white);
        g.setFont(BLENDER24);

        // Start drawing message body.
        int txtX = 340;
        g.drawString(MSG_BODY, txtX, 208);
        FontMetrics fm = g.getFontMetrics();
        int addX = fm.stringWidth(MSG_BODY) + 10;
        txtX += addX;

        g.setColor(C_255_207_3);
        g.drawString(startTime, txtX, 208);
        addX = fm.stringWidth(startTime) + 10;
        txtX += addX;

        g.setColor(Color.white);
        g.drawString(MSG_TO, txtX, 208);
        addX = fm.stringWidth(MSG_TO) + 10;
        txtX += addX;

        g.setColor(C_255_207_3);
        g.drawString(endTime, txtX, 208);
        // End drawing message body

        g.drawImage(block_lockimg, 113, 180, null);
        
        g.drawImage(btn_b, 688 + 40, 488, null);
        g.setColor(C_241_241_241);
        g.setFont(BLENDER17);
        g.drawString(MSG_BTN, 688 + 40 + 30, 503);
        
        super.paint(g);
    }
}

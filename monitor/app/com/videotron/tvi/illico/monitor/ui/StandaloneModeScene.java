package com.videotron.tvi.illico.monitor.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import org.dvb.ui.DVBAlphaComposite;
import org.dvb.ui.DVBGraphics;
import org.dvb.ui.UnsupportedDrawingOperationException;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxy;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.util.Constants;
import com.videotron.tvi.illico.util.FontResource;
import com.videotron.tvi.illico.util.GraphicUtil;
import com.videotron.tvi.illico.util.WindowProperty;
/**
 * Scene for standalone mode.
 * @author WSLEE1
 *
 */
public class StandaloneModeScene extends LayeredWindow {

	public static final long serialVersionUID = 2011010400L;

	/** The root windows made by Layered UI. */
	private LayeredUI rootWindow;

	private String currentLanguage = Definitions.LANGUAGE_FRENCH;

	private Image pop_sha;
	private Image img_05_pop_glow_04;
	private Image pop_high_402;
	private Image pop_gap_379;
	private Image error_txt_bg;
	private Image img_05_focus;
	
	private String MSG_TITLE;
	private String MSG_BODY_1;
	private String MSG_BODY_2;
	private String MSG_BODY_3;
	private String MSG_BODY_4;

	private String MSG_BUTTON;

	/**
	 * Constructor.
	 * @param keyHandler LayeredUI's key handler.
	 */
	public StandaloneModeScene(LayeredKeyHandler keyHandler) {
		currentLanguage = getLanguage();

		setBounds(Constants.SCREEN_BOUNDS);
		rootWindow = LayeredUIManager.getInstance().createLayeredDialog(WindowProperty.TERMINAL_BLOCKED_TIME.getName(),
				WindowProperty.TERMINAL_BLOCKED_TIME.getPriority(), false, this, Constants.SCREEN_BOUNDS, keyHandler);

		setVisible(true);
	}

	/**
	 * Shows the popup.
	 */
	public void showScene() {
		
		MSG_TITLE = DataCenter.getInstance().getString(KeyNames.STANDALONE_TITLE);
		MSG_BODY_1 = DataCenter.getInstance().getString(KeyNames.STANDALONE_MESSAGE1);
		MSG_BODY_2 = DataCenter.getInstance().getString(KeyNames.STANDALONE_MESSAGE2);
		MSG_BODY_3 = DataCenter.getInstance().getString(KeyNames.STANDALONE_MESSAGE3);
		MSG_BODY_4 = DataCenter.getInstance().getString(KeyNames.STANDALONE_MESSAGE4);
		MSG_BUTTON = DataCenter.getInstance().getString(KeyNames.STANDALONE_BUTTON);
		
		pop_sha = DataCenter.getInstance().getImage("pop_sha.png");
		img_05_pop_glow_04 = DataCenter.getInstance().getImage("05_pop_glow_04.png");
		pop_high_402 = DataCenter.getInstance().getImage("pop_high_402.png");
		pop_gap_379 = DataCenter.getInstance().getImage("pop_gap_379.png");
		error_txt_bg = DataCenter.getInstance().getImage("error_txt_bg.png");
		img_05_focus = DataCenter.getInstance().getImage("05_focus.png");
		
		FrameworkMain.getInstance().getImagePool().waitForAll();
		rootWindow.activate();
	}

	/**
	 * Hides the popup.
	 */
	public void hideScene() {
		rootWindow.deactivate();
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_TITLE);
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_MESSAGE1);
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_MESSAGE2);
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_MESSAGE3);
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_MESSAGE4);
		DataCenter.getInstance().removeImage(KeyNames.STANDALONE_BUTTON);
	}

	/**
	 * Disposes the popup.
	 */
	public void dispose() {
		LayeredUIManager.getInstance().disposeLayeredUI(rootWindow);
	}

	/** The color for font. */
	private static final Color C_252_202_4 = new Color(252, 202, 4);
	/** The color for font. */
	private static final Color C_3_3_3 = new Color(3, 3, 3);
	/** The instance of font. */
	private static final Font BLENDER18 = FontResource.BLENDER.getFont(18);
	/** The instance of font. */
	private static final Font BLENDER24 = FontResource.BLENDER.getFont(24);
	
	private static final Color C_0_0_0 = new Color(0, 0, 0);
	private DVBAlphaComposite alpha = DVBAlphaComposite.getInstance(DVBAlphaComposite.SRC_OVER, 0.8f);
	
	private static final Color C_35_35_35 = new Color(35, 35, 35);

	public void paint(Graphics g) {
		DVBGraphics dg = (DVBGraphics)g;
		DVBAlphaComposite backAlpha = dg.getDVBComposite();
		try {
            dg.setDVBComposite(alpha);
        } catch (UnsupportedDrawingOperationException e) {
            e.printStackTrace();
        }
        
		g.setColor(C_0_0_0);
		g.fillRect(0,0,960,540);
		
		try {
            dg.setDVBComposite(backAlpha);
        } catch (UnsupportedDrawingOperationException e) {
            e.printStackTrace();
        }
        
        g.drawImage(img_05_pop_glow_04, 250, 107, null);
		g.drawImage(pop_sha, 280, 407, 404, 79, null);
		
		g.setColor(C_35_35_35);
		g.fillRect(280, 135, 402, 276);

		g.drawImage(pop_high_402, 280, 135, null);
		g.drawImage(pop_gap_379, 292, 173, null);

		g.drawImage(error_txt_bg, 293, 187, null);

		g.setFont(BLENDER24);
		g.setColor(C_252_202_4);
		GraphicUtil.drawStringCenter(g, MSG_TITLE, 482, 161);

		// Body
		g.setFont(BLENDER18);
		g.setColor(Color.white);
		GraphicUtil.drawStringCenter(g, MSG_BODY_1, 480, 218);
		GraphicUtil.drawStringCenter(g, MSG_BODY_2, 480, 240);
		g.drawImage(img_05_focus, 402, 360, null);
		GraphicUtil.drawStringCenter(g, MSG_BODY_3, 480, 278);
		GraphicUtil.drawStringCenter(g, MSG_BODY_4, 480, 300);
		// Button
		g.setFont(BLENDER18);
		g.setColor(C_3_3_3);
		GraphicUtil.drawStringCenter(g, MSG_BUTTON, 480, 381);
	}

	/**
	 * Find current language.
	 * @return language string.
	 */
	private String getLanguage() {

		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if (preManager.isReadyPreferenceService()) {
			String result = preManager.getPreferenceValue(PreferenceServiceProxy.PNAME_LANGUAGE);
			return result;
		}
		return Definitions.LANGUAGE_FRENCH;
	}

}

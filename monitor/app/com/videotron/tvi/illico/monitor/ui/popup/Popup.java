package com.videotron.tvi.illico.monitor.ui.popup;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.FrameworkMain;

/**
 * The root popup class.
 *
 * @author Woosik Lee
 *
 */
public abstract class Popup extends LayeredWindow {
    /** The serial version uid. */
    public static final long serialVersionUID = 2010061000L;
    /** The flag for focus. */
    private boolean focusBtn = true;
    /** The focus value. */
    public static final boolean FOCUS_BUTTON_OK = true;
    /** The focus value. */
    public static final boolean FOCUS_BUTTON_CANCEL = false;
    /** The root windows made by Layered UI. */
    private LayeredUI rootWindow;
    /** The popup's title. */
    public String title;
    /** The popup's messages. */
    public String[] messages;
    
    

    /**
     * Constructor with LayeredKeyHandler.
     *
     * @param pTitle
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param pMessages
     *            the popup message.
     * @param bounds
     *            the popup area in the screen.
     * @param keyHandler
     *            the key handler.
     */
    public Popup(String pTitle, String name, int priority, String[] pMessages, Rectangle bounds,
            LayeredKeyHandler keyHandler) {
        this.title = pTitle;
        this.messages = pMessages;

        // create component
        setBounds(0, 0, bounds.width, bounds.height);
        if (keyHandler != null) {
            rootWindow = LayeredUIManager.getInstance().createLayeredDialog(name, priority, false, this, bounds,
                    keyHandler);
        } else {
            rootWindow = LayeredUIManager.getInstance().createLayeredWindow(name, priority, false, this, bounds);
        }
        setVisible(true);
    }

    /**
     * Constructor without LayeredKeyHandler.
     *
     * @param pTitle
     *            the popup title.
     * @param name
     *            the popup name. It will use Layered UI name.
     * @param priority
     *            the Layered UI's priority.
     * @param lines
     *            the popup message.
     * @param bounds
     *            the popup area in the screen.
     */
    public Popup(final String pTitle, final String name, final int priority, final String[] lines,
            final Rectangle bounds) {
        this(pTitle, name, priority, lines, bounds, null);
    }

    /**
     * Shows the popup.
     */
    public void showPopup() {
    	PopupImageManager.getInstance().loadImages();
        FrameworkMain.getInstance().getImagePool().waitForAll();
        rootWindow.activate();
    }

    /**
     * Hides the popup.
     */
    public void hidePopup() {
    	PopupImageManager.getInstance().flushImages();
        rootWindow.deactivate();
    }

    /**
     * Disposes the popup.
     */
    public void dispose() {
        LayeredUIManager.getInstance().disposeLayeredUI(rootWindow);
    }

    /**
     * Sets the focus.
     *
     * @param focus
     *            the button foucs.
     */
    public void setFocusBtn(boolean focus) {
        if (this.focusBtn != focus) {
            this.focusBtn = focus;
            repaint();
        }
    }

    /**
     * Gets the focus.
     *
     * @return focus.
     */
    public boolean getFocusBtn() {
        return this.focusBtn;
    }

    /**
     * Implements the Component class's paint method.
     *
     * @param g
     *            the instance of Graphics.
     */
    public abstract void paint(Graphics g);
    
}

package com.videotron.tvi.illico.monitor.util;
/**
 * This interface notifies alarm.
 * @author WSLEE1
 *
 */
public interface SimpleAlarmListener {

	/**
	 * Notify alarm.
	 */
	public void alarmUpdated();
}

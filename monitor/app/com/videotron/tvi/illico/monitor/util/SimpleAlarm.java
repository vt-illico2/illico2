package com.videotron.tvi.illico.monitor.util;

import java.util.Calendar;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import com.videotron.tvi.illico.log.Log;
/**
 * Simple alarm class.
 * @author WSLEE1
 *
 */
public class SimpleAlarm implements TVTimerWentOffListener {

	/**
	 * Alarm hour.
	 */
	private int HH;
	/**
	 * Alarm minute.
	 */
	private int MM;
	/**
	 * Alarm call back listener.
	 */
	private SimpleAlarmListener listener;

	/**
	 * Next alarm time.
	 */
	private long nextAlarmTime;

	/** TimerSpec for next alarm. */
	private TVTimerSpec timerSpec = new TVTimerSpec();

	/**
	 * Log header name.
	 */
	private static final String LOG_TAG = "[SimpleAlarm] ";

	/**
	 * Constructor.
	 * @param hh alarm hour.
	 * @param mm alarm minute.
	 * @param l call back listener.
	 */
	public SimpleAlarm(int hh, int mm, SimpleAlarmListener l) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_TAG + "SimpleAlarm() " + hh + ":" + mm + "," + l);
		}
		this.HH = hh;
		this.MM = mm;
		this.listener = l;
	}

	/**
	 * Starts alarm.
	 */
	public synchronized void start() {
		long currentTime = System.currentTimeMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(currentTime);
		cal.set(Calendar.MILLISECOND, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MINUTE, MM);
		cal.set(Calendar.HOUR_OF_DAY, HH);

		nextAlarmTime = cal.getTimeInMillis();

		if (nextAlarmTime > currentTime) {
			// Start first timer by targetTime
		} else {
			// Adds a day.
			nextAlarmTime += 1000 * 60 * 60 * 24;
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_TAG + "start() currentTime=" + currentTime + ", nextAlarmTime=" + nextAlarmTime);
		}

		startTimer(nextAlarmTime);
	}

	/**
	 * Stops alarm.
	 */
	public synchronized void stop() {
		if(Log.INFO_ON){
			Log.printInfo(LOG_TAG + "stop()");
		}
		endTimer();
	}

	/**
	 * Destroys alarm.
	 */
	public void dispose() {
		if(Log.INFO_ON){
			Log.printInfo(LOG_TAG + "dispose()");
		}
		stop();
		listener = null;
	}

	/**
	 * Starts timer for next restriction time.
	 *
	 * @param absTime
	 *            next restriction time.
	 */
	private synchronized void startTimer(long absTime) {
		endTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_TAG + "startTimer() absTime=" + absTime);
		}
		try {
			timerSpec.addTVTimerWentOffListener(this);
			timerSpec.setAbsoluteTime(absTime);
			timerSpec.setRegular(true);
			TVTimer.getTimer().scheduleTimerSpec(timerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	/**
	 * Stops next restriction timer.
	 */
	private synchronized void endTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_TAG + "endTimer()");
		}
		timerSpec.removeTVTimerWentOffListener(this);
		TVTimer.getTimer().deschedule(timerSpec);
	}

	/**
	 * Timer callback.
	 */
	public void timerWentOff(TVTimerWentOffEvent e) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_TAG + "timerWentOff()");
		}

		try {
			listener.alarmUpdated();
		} catch (Exception ex) {
			Log.printError(ex);
		}

		endTimer();

		start();
	}
}

package com.videotron.tvi.illico.monitor.core;

import java.rmi.RemoteException;
import java.util.Hashtable;

import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.util.Environment;
//->Kenneth : Popup added
import com.videotron.tvi.illico.monitor.ui.popup.ListPressedInNonPvrPopup;

/**
 * This class provides the API for service state tranistion.
 * <P>
 * @author Woosik Lee
 */
public final class AppLauncher {
    /** The singleton instance of AppLauncher. */
    private static AppLauncher instance = null;
    /** The transition event for the EPG guide. */
    private TransitionEvent GUIDE_EVENT;
    /** The transition event for the VOD. */
    private TransitionEvent VOD_EVENT;
    /** The transition event for the PIP. */
    private TransitionEvent PIP_EVENT;
    /** The transition event for the Menu. */
    private TransitionEvent MENU_EVENT;
    /** The transition event for exit. this event is used to exit from application. */
    private TransitionEvent LCW_EVENT;
    /** The transition event for TV_VIEWING_STATE without Menu. */
    private TransitionEvent CHANNEL_EVENT_WITHOUT_MENU;
    /** The transition evnet for TV_VIEWING_STATE without Menu and video. Watching time restriction used it. */
    private TransitionEvent CHANNEL_EVENT_WITHOUT_MENU_AND_AV;
    /** The transition event for FULL_SCREEN_APP_STATE without any application. It just change state. */
    private TransitionEvent FULL_SCREEN_APP_WITHOUT_APPLICATION;
    /** The transition event for channel change. */
    private TransitionEvent CHANNEL_EVENT;
    /** The transition event for the Settings. */
    private TransitionEvent SETTINGS_EVENT;
    /** The transition event for the PVR. */
    private TransitionEvent PVR_EVENT;
    /** The transition event for the ISA. */
    private TransitionEvent ISA_EVENT;
    /** The special menu's state. */
    //public static final String DASHBOARD_NAME = "DASHBOARD";
    /** The settings name included User Profile application. */
    public static final String SETTINGS_NAME = "SETTINGS";
    /** The Full menu application name. */
    public static final String FULL_MENU = "FULL_MENU";
    /** The PIP name. */
    public static final String PIP_NAME = "PIP";
    /** The PPV name. */
    public static final String PPV_NAME = "PPV";
    /** The hashtable for the parameters. */
    private Hashtable parameterTable = new Hashtable();

    private static final String LOG_HEADER = "[AppLauncher] ";

    /** Constructor. */
    private AppLauncher() {
        initEvents();
    }

    /**
     * Gets the singleton instance of AppLauncher.
     * @return instance.
     */
    public static synchronized AppLauncher getInstance() {
        if (instance == null) {
            instance = new AppLauncher();
        }
        return instance;
    }

    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {

    }

    /**
     * Initializes the static events.
     */
    private void initEvents() {
        String vodName = DataManager.getInstance().getVodName();
        UnboundAppProxy vodProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(vodName);
        VOD_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.VOD, vodName, vodProxy, null);

        String menuName = DataManager.getInstance().getMenuName();
        UnboundAppProxy menuProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(menuName);
        MENU_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.MAIN_MENU, menuName, menuProxy,
                null);

        String epgName = DataManager.getInstance().getEPGName();
        UnboundAppProxy epgProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(epgName);
        GUIDE_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.GUIDE, epgName, epgProxy, null);

        String uppName = DataManager.getInstance().getUPPName();
        SETTINGS_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.SETTINGS, uppName, null, null);

        String pvrName = DataManager.getInstance().getPVRName();
        UnboundAppProxy pvrProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(pvrName);
        PVR_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.PVR, pvrName, pvrProxy, null);

        PIP_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.PIP, PIP_NAME, epgProxy, null);

        LCW_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.STATE_TV_VIEWING, null, null, null);
        CHANNEL_EVENT_WITHOUT_MENU = new TransitionEvent(TransitionEvent.TRANSITION,
                TransitionEvent.STATE_TV_VIEWING_WITHOUT_MENU, null, null, null);
        CHANNEL_EVENT_WITHOUT_MENU_AND_AV = new TransitionEvent(TransitionEvent.TRANSITION,
                TransitionEvent.STATE_TV_VIEWING_WITHOUT_MENU_AND_AV, null, null, null);
        FULL_SCREEN_APP_WITHOUT_APPLICATION = new TransitionEvent(TransitionEvent.TRANSITION,
                TransitionEvent.STATE_FULL_SCREEN_APP_WITHOUT_APPLICATION, null, null, null);
        CHANNEL_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.STATE_TV_VIEWING, null, null,
                null);
        CHANNEL_EVENT.setMenuState(MainMenuService.MAIN_MENU_CLOSE_ALL);

        String isaName = DataManager.getInstance().getIsaName();
        UnboundAppProxy isaProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(isaName);
        ISA_EVENT = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.ISA, isaName, isaProxy, null);
    }

    /**
     * Gets the application parameters.
     * @param key the application name.
     * @return If exist the parameters for given key, returns parameters.
     * @throws RemoteException the RemoteException.
     */
    public String[] getParameter(String key) throws RemoteException {
        String[] parameter = (String[]) parameterTable.get(key);
        return parameter;
    }

    /**
     * Sets the application parameters.
     * @param appName the application name.
     * @param value the parameter (String[]).
     */
    public void setParameter(String appName, String[] value) {
        parameterTable.put(appName, value);
    }

    /**
     * Request to start unbound application. When this method is called, current running application is destroyed (or
     * paused), and the requested application is started. All these action is performed asynchronously.
     * @param appName unbound application name to be started.
     * @param args the arguments to be transfered. It can be retrieved by {@link MonitorService#getParameter(String)}.
     * @return if successfully launched the application returns true, otherwise returns false.
     */
    public boolean startUnboundApplication(String appName, String[] args) {
        return startUnboundApplication(appName, args, true);
    }

    /**
     * Request to start unbound application. When this method is called, current running application is destroyed (or
     * paused), and the requested application is started. All these action is performed asynchronously.
     * @param appName unbound application name to be started.
     * @param args the arguments to be transfered. It can be retrieved by {@link MonitorService#getParameter(String)}.
     * @param showAuthorityPopup If true, shows authority popup if necessary.
     * @return if successfully launched the application returns true, otherwise returns false.
     */
    public boolean startUnboundApplication(String appName, String[] args, boolean showAuthorityPopup) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startUnboundApplication() " + appName + ", showAuthorityPopup="
                    + showAuthorityPopup);
        }

        // Checks the application's authority.
        int applicationAuthority = AuthorizationManager.CHECK_RESULT_AUTHORIZED;
        boolean needToCheckAuthority = true;
        String[] skipAppList = DataManager.getInstance().getNoNeedToCheckAuthAppList();
        if(skipAppList != null) {
        	for(int i=0; i<skipAppList.length; i++) {
        		if(skipAppList[i].equalsIgnoreCase(appName)) {
        			needToCheckAuthority = false;
        			if(Log.INFO_ON) {
        				Log.printInfo(LOG_HEADER + "No need to check authoriztion for " + appName);
        			}
        			break;
        		}
        	}
        }

        if(!needToCheckAuthority) {
            applicationAuthority = AuthorizationManager.CHECK_RESULT_AUTHORIZED;
        }
        else {
        	if(appName.equals(FULL_MENU)) {
        		applicationAuthority = AuthorizationManager.getInstance().checkResourceAuthorization(DataManager.getInstance().getMenuName());
        	}
        	else {
        		applicationAuthority = AuthorizationManager.getInstance().checkResourceAuthorization(appName);
        	}
        }

        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "applicationAuthority = "
                    + AuthorizationManager.getResultString(applicationAuthority));
        }
        if (applicationAuthority == AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED) {
            if (showAuthorityPopup) {
                //->Kenneth : R4.1 VOD Deactivation Package 체크해서 MOT501 대신 다른 팝업 띄워야함.
                /* 봉인 : R4.1 에서 없어지고 추후 DDC 로
                if (appName.equals(DataManager.getInstance().getVodName())) {
                    int vodDeactivationCheck = AuthorizationManager.getInstance().getVODDeactivationPackageAuthority();
                    Log.printDebug("Kenneth [AppLauncher] : VODDeactivationPackage = "+vodDeactivationCheck);
                    if (vodDeactivationCheck == AuthorizationManager.CHECK_RESULT_AUTHORIZED) {
                        Log.printDebug("Kenneth [AppLauncher] : Call showErrorMessage(MOT507)");
                        CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_507, null);
                        return false;
                    }
                }
                */
                //<-
                if(Log.ERROR_ON) {
                    Log.printError(LOG_HEADER + "MOT501 [" + appName + "] is not authorized");
                }
                AuthorizationManager.getInstance().startApplicationBlocked(appName);
            }
            return false;
        }

        if (args == null) {
            parameterTable.remove(appName);
        } else {
            parameterTable.put(appName, args);
        }

        if (appName.equals(DataManager.getInstance().getVodName())) {
            UnboundAppProxy vodProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(DataManager.getInstance().getVodName());
            if(vodProxy == null) {
                CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                return false;
            }
            StateManager.getInstance().requestStateTransition(VOD_EVENT);
        } else if (appName.equals(DataManager.getInstance().getEPGName())) {
            StateManager.getInstance().requestStateTransition(GUIDE_EVENT);
        } else if (appName.equals(PPV_NAME)) {
            // PPV는 Guide의 특정 Page로 가는 것이다.
            // PPV event will go to the Guide's page. The page is indicated by parameterTable.
            StateManager.getInstance().requestStateTransition(GUIDE_EVENT);
        } else if (appName.equals(DataManager.getInstance().getPVRName())) {
            UnboundAppProxy pvrProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(DataManager.getInstance().getPVRName());
            if(pvrProxy == null) {
                CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                return false;
            }

            if (!Environment.SUPPORT_DVR) {
				// Non PVR STB can't launch PVR.
				// Nevertheless if Non PVR STB has a authority about PVR Multiroom
				// package, PVR is available.
				int authResult = AuthorizationManager.getInstance().checkPvrMultiroomPackage();
				if (authResult == AuthorizationManager.CHECK_RESULT_AUTHORIZED) {
					if (Log.DEBUG_ON) {
						Log.printDebug("UserEventManager: This is Non PVR STB but this STB has authority of PVR multiroom package so LIST button is available.");
					}
				} else {
                    //->Kenneth [R4.1] : LIST BEHAVIOR 
					//CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_503, null);
                    boolean isPopupVisible = MonitorKeyHandler.decideListPopupDisplay();
                    if (isPopupVisible) {
                        Log.printDebug("Kenneth : show popup from AppLauncher.");
                        ListPressedInNonPvrPopup.getInstance().showPopup();
                    }
					return false;
				}
			}

            StateManager.getInstance().requestStateTransition(PVR_EVENT);
        } else if (appName.equals(PIP_NAME)) {
            StateManager.getInstance().requestStateTransition(PIP_EVENT);
            /*->Kenneth[2015.7.8] : Tank 
        } else if(appName.equals(FULL_MENU)) {
            int targetMainMenuState = MainMenuService.MAIN_MENU_STATE_FULL_MENU;
            MENU_EVENT.setMenuState(targetMainMenuState);
            MENU_EVENT.setParameter(args);
            StateManager.getInstance().requestStateTransition(MENU_EVENT);
            */
        } else if (appName.equals(DataManager.getInstance().getMenuName())) {
            if(CommunicationManager.getInstance().getMainMenuService() == null) {
                CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                return false;
            }
            int targetMainMenuState = MainMenuService.MAIN_MENU_CLOSE_ALL;
            try {
                targetMainMenuState = CommunicationManager.getInstance().getMainMenuService().getTargetMenuState();
            } catch (Exception ex) {
                Log.print(ex);
            }
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "target main menu state is " + targetMainMenuState);
            }

            MENU_EVENT.setMenuState(targetMainMenuState);
            MENU_EVENT.setParameter(args);
            StateManager.getInstance().requestStateTransition(MENU_EVENT);
        } else if (appName.equals(DataManager.getInstance().getDashboardName())) {
            if(CommunicationManager.getInstance().getMainMenuService() == null) {
                CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                return false;
            }
            int targetMainMenuState = MainMenuService.MAIN_MENU_STATE_DASHBOARD;
            MENU_EVENT.setMenuState(targetMainMenuState);
            MENU_EVENT.setParameter(args);
            StateManager.getInstance().requestStateTransition(MENU_EVENT);
        } else if (appName.equals(SETTINGS_NAME) || appName.equals(DataManager.getInstance().getUPPName())) {
            if(CommunicationManager.getInstance().getPreferenceService() == null) {
                CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                return false;
            }
            SETTINGS_EVENT.setParameter(args);
            StateManager.getInstance().requestStateTransition(SETTINGS_EVENT);
        } else if (appName.equals(DataManager.getInstance().getSearchName())) {
            // Do not use state transition event.
            try {
                SearchService searchService = CommunicationManager.getInstance().getSearchService();
                if (searchService != null) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(LOG_HEADER + "launchSearchApplication() ");
                    }
                    searchService.launchSearchApplication(SearchService.PLATFORM_MONITOR, true);
                    return true;
                } else {
                    if (Log.WARNING_ON) {
                        Log.printWarning(LOG_HEADER + "ERROR not found Search IXC ");
                    }
                    CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                    return false;
                }
            } catch (Exception e) {
                Log.print(e);
            }
        } else if (appName.equals(DataManager.getInstance().getIsaName())) {
            ISA_EVENT.setParameter(args);
            StateManager.getInstance().requestStateTransition(ISA_EVENT);

        } else {
            UnboundAppProxy appProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(appName);
            if (appProxy != null) {
                TransitionEvent appEvent = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.UNBOUND_APP,
                        appName, appProxy, null);
                StateManager.getInstance().requestStateTransition(appEvent);
            } else {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "Not found unbound app name in unbound app list [" + appName + "]");
                }

                // Find AppProxy in AODManager
                UnboundAppProxy aodProxy = AodManager2.getInstance().getAodAppProxy(appName);

                if(aodProxy == null) {
                	// AOD App 이면 AOD Process 를 수행 한다.
                	boolean isAodApp = AodManager2.getInstance().isAODApplication(appName);
                	if(isAodApp) {
                		if(Log.INFO_ON) {
                			Log.printInfo(LOG_HEADER + "This is AOD application so do process AOD");
                		}
                		AodManager2.getInstance().startAODApplication(appName, null);
                		return true;
                	}
                	else {
                		if (Log.WARNING_ON) {
                            Log.printWarning(LOG_HEADER + "ERROR not found unbound app name " + appName);
                        }
                		CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
                		return false;
                	}
                }
                else {
                	AodManager2.getInstance().startAODApplication(appName, null);
                	return true;
                }
            }
        }

        return true;
    }

    /**
     * Requests to destroy the current running application. When this method is called, MonitorApplication destroys (or
     * pause) the current running application, and selects the LCW service. All these action is performed
     * asynchronously.
     */
    public void exitToChannel() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "exitToChannel");
        }

        if(STBModeManager.getInstance().isStandaloneMode()) {
            StandaloneModeManager.getInstance().start();
            return;
        }

        StateManager.getInstance().requestStateTransition(LCW_EVENT);
    }

    /**
     * Requests to destroy the current running application and moves to the specified service by sourceId. When this
     * method is called, Monitor destroys (or pause) the current running application, and selects the specified service.
     * All these action is performed asynchronously.
     * @param sourceId The target channel's source id.
     */
    public void exitToChannel(int sourceId) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "exitToChannel(" + sourceId + ")");
        }

        if(STBModeManager.getInstance().isStandaloneMode()) {
            StandaloneModeManager.getInstance().start();
            return;
        }

        CHANNEL_EVENT.setSourceId(sourceId);
        StateManager.getInstance().requestStateTransition(CHANNEL_EVENT);
    }

    /**
     * Requests to destroy the current running application and moves to the specified service by sourceId. When this
     * method is called, Monitor destroys (or pause) the current running application, and selects the specified service.
     * All these action is performed asynchronously.
     * @param sourceId The target channel's source id.
     */
    public void exitToChannelWithoutMenu(int sourceId) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "exitToChannelWithoutMenu(" + sourceId + ")");
        }
        CHANNEL_EVENT_WITHOUT_MENU.setSourceId(sourceId);
        StateManager.getInstance().requestStateTransition(CHANNEL_EVENT_WITHOUT_MENU);
    }

    /**
     * Requests to destroy the current running application. When this
     * method is called, Monitor destroys (or pause) the current running application, and do not select the AV service.
     * All these action is performed asynchronously.
     */
    public void exitToChannelWithoutMenuAndAV() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "exitToChannelWithoutMenuAndAV()");
        }
        CHANNEL_EVENT_WITHOUT_MENU_AND_AV.setSourceId(-1);
        StateManager.getInstance().requestStateTransition(CHANNEL_EVENT_WITHOUT_MENU_AND_AV);
    }

    /**
     * Go to FULL_SCREEN_APP_STATE but does not start any application.
     */
    public void goToFullScreenAppStateWithoutApplication() {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "goToFullScreenAppStateWithoutApplication()");
        }
    	StateManager.getInstance().requestStateTransition(FULL_SCREEN_APP_WITHOUT_APPLICATION);
    }
}

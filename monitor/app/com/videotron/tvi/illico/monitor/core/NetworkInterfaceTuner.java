package com.videotron.tvi.illico.monitor.core;

import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceController;
import org.davic.net.tuning.NetworkInterfaceEvent;
import org.davic.net.tuning.NetworkInterfaceListener;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterfaceReleasedEvent;
import org.davic.net.tuning.NetworkInterfaceTuningEvent;
import org.davic.net.tuning.NetworkInterfaceTuningOverEvent;
import org.davic.net.tuning.NoFreeInterfaceException;
import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.davic.resources.ResourceStatusEvent;
import org.davic.resources.ResourceStatusListener;
import org.ocap.net.OcapLocator;

import com.videotron.tvi.illico.log.Log;

public class NetworkInterfaceTuner implements NetworkInterfaceListener, ResourceClient, ResourceStatusListener {

	private static int tuningTimeOut = 3000; // Milliseconds to wait for a

	/** The place to obtain interfaces. */
	private NetworkInterfaceManager netManager;

	/** A Controller object for the NetworkInterface being used. */
	private NetworkInterfaceController netController;

	/** The NetworkInterface to be used. */
	private NetworkInterface netInterface;

	/** The object used for tuner event synchronization. */
	private Object tuningEvent = new Object();

	/** Set to true if the tuner has been setup. */
	private boolean isInitialized = false;

	/** Set to true when a network interface tune over event is received. */
	private boolean receivedNIEvent = false;

	private Object statusChangedEvent = new Object();
	private boolean receivedStatusChangedEvent;

	/** The status of the tuner. */
	private int tuneStatus = NetworkInterfaceTuningOverEvent.FAILED;
	
	private NetworkInterfaceTuningFailListener listener = null;

	private static final String LOG_HEADER = "[NetworkInterfaceTuner] ";

	public NetworkInterfaceTuner(NetworkInterfaceTuningFailListener l) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "Constructor()");
		}
		listener = l;
		
		isInitialized = true;
		netManager = NetworkInterfaceManager.getInstance();

		if (netManager == null) {
			isInitialized = false;
		}
		netController = new NetworkInterfaceController(this);
	}
	
	public void dispose() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "dispose()");
		}
		listener = null;
		
		NetworkInterfaceManager.getInstance().removeResourceStatusEventListener(this);
		
		NetworkInterface ni = null;
        if (netController != null) {
            ni = netController.getNetworkInterface();
        }
        if (ni != null && ni.isReserved()) {
            try {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Tuner.cleanup- Releasing network interface");
                }
                netController.release();
            } catch (Throwable t) {
                Log.printDebug("Tuner.cleanup - Got error when releasing");
            }
        }
        
		if (netInterface != null) {
            netInterface.removeNetworkInterfaceListener(this);
            netInterface = null;
        }
	}

	public boolean tune(OcapLocator locator) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "tune() " + locator.toString() + ", isInitialized=" + isInitialized);
		}

		if (!isInitialized) {
			return false;
		}

		NetworkInterfaceManager.getInstance().addResourceStatusEventListener(this);
		receivedStatusChangedEvent = false;

		// Request the asynchronous tune.
		int tryCount = 0;
		while (tryCount < 2) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Tuner.tune - See if Net interface available. Attempt (" + tryCount + ")");
			}
			try {

				netController.reserveFor(locator, null);
				netInterface = netController.getNetworkInterface();
				netInterface.addNetworkInterfaceListener(this);

				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Tune the tuner");
				}
				// Ensure we start with a false indication of a
				// receivee Network Interface Event;
				receivedNIEvent = false;
				netController.tune(locator);

				break; /* out of the while loop */
			} catch (Exception ex) {
				if (ex instanceof NoFreeInterfaceException) {
					// fall through and wait for a NetworkInterface to
					// become available
					Log.printDebug(LOG_HEADER + "Tuner.tune - No Free Interface, wait for a free one.");
				} else {
					Log.print(ex);
					ex.printStackTrace();
					return false;
				}
			}

			/* else, no tuner free */
			/* Wait for a tuner to become free */
			synchronized (statusChangedEvent) {
				try {
					if (!receivedStatusChangedEvent) {
						statusChangedEvent.wait(tuningTimeOut);
					}

					if (Log.DEBUG_ON) {
						/* Timed out. */
						if (!receivedStatusChangedEvent) {
							Log.printDebug(LOG_HEADER
									+ "Tuner.tune - Timedout waiting for network interface to become available");
						} else {
							Log.printDebug(LOG_HEADER + "Tuner.tune - Received the proper event");
						}
					}
				} catch (Exception ex) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Waiting for tuner exception: " + ex.getMessage());
					ex.printStackTrace();
					/* keep trying */
				}
			}
			tryCount++;
		} // end of while loop

		/* Check to see if the interface was gotten. */
		if (netInterface == null) {
			Log.printDebug(LOG_HEADER + "Tuner.tune - Failed to attain a Network Interface. Tune failed.");
			return false;
		}
		synchronized (tuningEvent) {
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Wait for tuning event");
				}
				// receivedNIEvent is used to signal whether the tune() has
				// completed or not.
				if (!receivedNIEvent) {
					tuningEvent.wait(tuningTimeOut);
				}
				// At this point check again whether the tune completed or not.
				if (!receivedNIEvent) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Didn't receive a tuning event");

					return false;
				}

				if (tuneStatus != NetworkInterfaceTuningOverEvent.SUCCEEDED) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Failed: status = " + tuneStatus);
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "Tuner.tune - adding cache entry for freq = "
								+ locator.getFrequency() + ", tsid = 0xFFFF");
					}
					return false;
				}
			} catch (Exception ex) {
				// See if we simply missed the event.
				if (!receivedNIEvent) {
					Log.printDebug(LOG_HEADER + "Tuner.tune - Exception and no event");
					ex.printStackTrace();
					return false;
				}
				receivedNIEvent = false;
			}
		}

		return true;
	}

	public void statusChanged(ResourceStatusEvent rse) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "statusChanged(): Event received " + rse.toString());
		}

		if (rse instanceof NetworkInterfaceReleasedEvent) {
			synchronized (statusChangedEvent) {
				receivedStatusChangedEvent = true;
				/* wake up blocked thread */
				statusChangedEvent.notify();
			}
		}
	}

	/**
	 * Receives an event from the NetworkInterface indicating that tuning is
	 * complete.
	 */
	public void receiveNIEvent(NetworkInterfaceEvent ev) {
		if (ev instanceof NetworkInterfaceTuningEvent) {
			if (Log.DEBUG_ON) {
				NetworkInterfaceTuningEvent nite = (NetworkInterfaceTuningEvent) ev;
				Log.printDebug(LOG_HEADER + "NetworkInterfaceTuningEvent - Event=" + nite.toString());
			}
		}

		if (ev instanceof NetworkInterfaceTuningOverEvent) {
			if (Log.DEBUG_ON) {
				NetworkInterfaceTuningOverEvent nite = (NetworkInterfaceTuningOverEvent) ev;
				Log.printDebug(LOG_HEADER + "NetworkInterfaceTuningOverEvent - Event=" + nite.toString());
			}
			if (ev instanceof NetworkInterfaceTuningOverEvent) {
				NetworkInterfaceTuningOverEvent nitoe = (NetworkInterfaceTuningOverEvent) ev;
				tuneStatus = nitoe.getStatus();
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Tuner.receiveNIEvent - Tuning Over Event status = " + tuneStatus);
				}
				receivedNIEvent = true;

				synchronized (tuningEvent) {
					tuningEvent.notify();
				}
			}
		}
	}

	// ResourceClient requirements
	public boolean requestRelease(ResourceProxy px, Object data) {
		// ServiceGroupDiscovery.getInstance().stopSGD();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "requestRelease : ");
		}
		if(listener != null) {
			listener.tuningFailed();
		}
		else {
			dispose();
		}
		return true;
	}

	public void notifyRelease(ResourceProxy px) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "notifyRelease : " + px);
		}
	}

	public void release(ResourceProxy px) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "release : " + px);
		}
	}
}

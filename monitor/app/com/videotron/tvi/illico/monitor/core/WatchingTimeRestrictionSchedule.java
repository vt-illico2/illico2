package com.videotron.tvi.illico.monitor.core;

import java.util.Calendar;
import java.util.StringTokenizer;

import com.videotron.tvi.illico.log.Log;
/**
 * 이 클래스는 시청 시간제한을 하나만 할 수 있을 경우에 사용되는 것이다.
 * 20100915 에 5개로 늘려 달라는 요청이 있어 이 클래스는 더이상 사용되지 않을 것 이다.
 * @author Woosik Lee
 *
 */
public class WatchingTimeRestrictionSchedule {

    private int startHH;
    private int startMM;
    private long startTimeMillis;

    private int endHH;
    private int endMM;
    private long endTimeMillis;

    /** The week informations, [0]=Sun, [1]=Mon, [2]=Tue ... [6]=Sat. */
    private boolean[] repeatWeek;

    /** The log header message. */
	private static final String LOG_HEADER = "[WTRM-Schedule] ";

    public WatchingTimeRestrictionSchedule() {

    }

    /**
     * The main method for test.
     * @param args
     */
    public static void main(String[] args) {
        WatchingTimeRestrictionSchedule wtrs = new WatchingTimeRestrictionSchedule();
        String sHHMM = "2205";
        String eHHMM = "0210";
        String week = "NO|NO|NO|NO|NO|NO|NO";
        wtrs.setData(sHHMM, eHHMM, week);

        long[] result = wtrs.getNextSchedule(System.currentTimeMillis(), true);

        printTime(result[0], "RESULT START");
        printTime(result[1], "RESULT END");
    }

    public void setData(String startHHMM, String endHHMM, String week) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "setData() startHHMM=" + startHHMM + ",endHHMM=" + endHHMM
                    + ", week=" + week);
        }
        String hh = startHHMM.substring(0, 2);
        String mm = startHHMM.substring(2, 4);
        startHH = Integer.parseInt(hh);
        startMM = Integer.parseInt(mm);
        startTimeMillis = addTime(0, 0, startHH, startMM);

        hh = endHHMM.substring(0, 2);
        mm = endHHMM.substring(2, 4);
        endHH = Integer.parseInt(hh);
        endMM = Integer.parseInt(mm);
        endTimeMillis = addTime(0, 0, endHH, endMM);

        repeatWeek = new boolean[7];
        StringTokenizer st = new StringTokenizer(week, "|");
        int count = 0;
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            if ("Yes".equalsIgnoreCase(token)) {
                repeatWeek[count] = true;
            } else {
                repeatWeek[count] = false;
            }
            count++;
        }
    }

    /**
     * Returns the next schedule. 시간을 전달 하면 다음 스케줄을 리턴 한다.
     * @param requestedTime The requested time. (unit is millisecond).
     * @param includeCurrentTime if true, include current time. True 이면 requested time이 포함된 스케줄이 있으면 그것을 리턴한다. False 이면
     *            requested time 이 포함된 스케줄이 있다 하더라도 다음 스케줄을 리턴한다.
     * @return The schedule time. [0] is startTime and [1] is endTime. unit is millisecond.
     */
    public long[] getNextSchedule(long requestedTime, boolean includeCurrentTime) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "getNextSchedule() requestedTime=" + requestedTime
                    + ",includeCurrentTime=" + includeCurrentTime);
        }
        // requestedTime 기준으로 가장 가까운 혹은 requestedTime 이 포함 된 스케줄을 찾는다.
        Calendar reqTime = Calendar.getInstance();
        reqTime.setTimeInMillis(requestedTime);
        // int requestedWeek = reqTime.get(Calendar.DAY_OF_WEEK) - 1;
        // SUN : 1, MON : 2, TUE : 3, WED : 4, THU : 5, FRI : 6, SAT : 7
        // http://download.oracle.com/javase/1.4.2/docs/api/constant-values.html#java.util.Calendar.SUNDAY

        // requestedTime 기준으로 금주의 시작인 일요일 00시 00 분을 먼저 계산 한다.
        long weekStartTime = findWeekStartTime(reqTime);

        long[] startTimes = new long[7];
        long[] endTimes = new long[7];
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                // Do not compute time if not checked day of week.
                startTimes[i] = addTime(weekStartTime, i, startHH, startMM);
                if(endTimeMillis > startTimeMillis) {
                    // 24시간 이내에 start / end 가 있는 경우.
                    endTimes[i] = addTime(weekStartTime, i, endHH, endMM);
                }
                else {
                    // 이틀에 걸쳐서 start / end 가 있는 경우. 이 경우 end 를 다음날로 설정 해야 한다.
                    endTimes[i] = addTime(weekStartTime, i+1, endHH, endMM);
                }
            }
        }

        if (Log.DEBUG_ON) {
            for (int i = 0; i < 7; i++) {
                if (repeatWeek[i]) {
                    printTime(startTimes[i], "SCHEDULE[" + i + "] Start Time : ");
                    printTime(endTimes[i], "SCHEDULE[" + i + "] End Time : ");
                }
            }
        }

        // Finds the nearest schedule using requestTime.
        int findSchedule = -1;
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                if (requestedTime < endTimes[i]) {
                    if (includeCurrentTime) {
                        findSchedule = i;
                        break;
                    } else {
                        if (requestedTime < startTimes[i]) {
                            findSchedule = i;
                            break;
                        }
                    }
                }
            }
        }

        if (findSchedule > -1) {
            long[] returnValue = new long[2];
            returnValue[0] = startTimes[findSchedule];
            returnValue[1] = endTimes[findSchedule];
            return returnValue;
        }

        // 금주내에서 Schedule 을 찾지 못했다. 그렇다면 다음주의 가장 첫 번째 스케줄을 리턴 하면 된다.
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                long[] returnValue = new long[2];
                returnValue[0] = addTime(startTimes[i], 7, 0, 0);
                returnValue[1] = addTime(endTimes[i], 7, 0, 0);
                return returnValue;
            }
        }

        return null;
    }

    /**
     * Returns the start time of this week.
     * @param calendar The Calendar instance.
     * @return the start time of this week. unit is millisecond.
     */
    private long findWeekStartTime(Calendar calendar) {
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTimeInMillis(calendar.getTimeInMillis());
        tempCalendar.set(Calendar.MILLISECOND, 0);
        tempCalendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
        tempCalendar.set(Calendar.MINUTE, 0);
        tempCalendar.set(Calendar.SECOND, 0);
        return tempCalendar.getTimeInMillis();
    }

    /**
     * Returns the new time.
     * @param startTime the start time.
     * @param day to be add day.
     * @param hour to be add hour.
     * @param min to be add min
     * @return new time. added day, hour, min.
     */
    private long addTime(long startTime, int day, int hour, int min) {
        int day1 = 1000 * 60 * 60 * 24 * day;
        int hour1 = 1000 * 60 * 60 * hour;
        int min1 = 1000 * 60 * min;
        return startTime + day1 + hour1 + min1;
    }

    public static void printTime(long timeMillis, String title) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeMillis);
        printTime(cal, title);
    }

    private static void printTime(Calendar cal, String title) {

        if (Log.DEBUG_ON) {
            StringBuffer buf = new StringBuffer();
            buf.append(LOG_HEADER + "" + title + " " + cal.getTimeInMillis() + " ==============\n");
            buf.append("YEAR=" + cal.get(Calendar.YEAR));
            buf.append(",MONTH=" + (cal.get(Calendar.MONTH) + 1));
            buf.append(",DATE=" + cal.get(Calendar.DATE));
            int week = cal.get(Calendar.DAY_OF_WEEK);
            switch (week) {
            case 1:
                buf.append(",WEEK=Sun");
                break;
            case 2:
                buf.append(",WEEK=Mon");
                break;
            case 3:
                buf.append(",WEEK=Tue");
                break;
            case 4:
                buf.append(",WEEK=Wed");
                break;
            case 5:
                buf.append(",WEEK=Thu");
                break;
            case 6:
                buf.append(",WEEK=Fri");
                break;
            case 7:
                buf.append(",WEEK=Sat");
                break;
            }

            buf.append(",HOUR=" + cal.get(Calendar.HOUR_OF_DAY));
            buf.append(",MIN=" + cal.get(Calendar.MINUTE));
            buf.append(",SEC=" + cal.get(Calendar.SECOND));

            buf.append("\n===========================================");

            if (Log.DEBUG_ON) {
                Log.printDebug(buf.toString());
            }
        }
    }

}

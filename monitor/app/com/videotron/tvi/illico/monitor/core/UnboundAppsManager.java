package com.videotron.tvi.illico.monitor.core;

import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppStateChangeEvent;
import org.dvb.application.AppStateChangeEventListener;
import org.dvb.application.AppsDatabase;
import org.dvb.application.CurrentServiceFilter;
import org.dvb.application.DVBJProxy;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;

/**
 * Manages the unbound applications.
 * <P>
 * This class makes the unbound application list and provides the {@link UnboundAppProxy}.
 * @author Woosik Lee
 */
public class UnboundAppsManager implements AppStateChangeEventListener, TVTimerWentOffListener {

    /** The singleton instance of UnboundAppsManager. */
    private static UnboundAppsManager instance;
    /** The unbound application's proxy class. */
    private UnboundAppProxy[] unboundAppProxy;
    /** The monitor application's priority. It is defined at XAIT. */
    private static final int MONITOR_PRORITY = 255;
    /**
     * The flag for application ready. If application started completely, application's appID will be removed in vector.
     */
    private Vector flagOfReadyApplicationId;
    /** The flag for wait timer went off. */
    private boolean waitTimerWentOff;
    /** The timer instance. */
    private final TVTimer timer = TVTimer.getTimer();
    /** The waiting time timer. */
    private TVTimerSpec checkTimerSpec = new TVTimerSpec();
    /** The waiting time that start unbound application. */
    private static final long WAIT_TIME = 60000L;

    private static final int MODE_STARTED_REGISTERED_API_DAEMON = 1;
    private static final int MODE_STARTED_NORMAL_DAEMON = 2;
    private static final int MODE_NONE = 0;

    private int currentMode = MODE_NONE;

    /** The log header message. */
	private static final String LOG_HEADER = "[UnboundAppsManager] ";

    /**
     * Gets the singleton instance.
     * @return instance of UnboundAppsManager.
     */
    public static synchronized UnboundAppsManager getInstance() {
        if (instance == null) {
            instance = new UnboundAppsManager();
        }
        return instance;
    }

    /**
     * Starts this class.
     * <P>
     * Makes the {@link UnboundAppProxy} list and starts daemon-applications.
     */
    public void start() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "start");
        }
        buildUnboundAppDB();
        startRegisteredAPIDaemon();
    }

    /**
     * Destroy all unbound application without ErrorMessage.
     */
    public void destroyAllUnboundApplicationWithoutErrorApplication() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "destroyAllUnboundApplicationWithoutErrorApplication");
        }
        String errorAppName = DataManager.getInstance().getErrorMessageName();
        if (unboundAppProxy != null) {
            for (int i = 0; i < unboundAppProxy.length; i++) {
                if (unboundAppProxy[i].getPriority() != MONITOR_PRORITY && !unboundAppProxy[i].getName().equalsIgnoreCase(errorAppName)) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(LOG_HEADER + "stop app " + unboundAppProxy[i]);
                    }
                    unboundAppProxy[i].destroy();
                }
            }
        }
    }

    /**
     * Gets the all unbound application.
     * @return UnboundAppProxy.
     */
    public UnboundAppProxy[] getAllUnboundAppProxies() {
        return unboundAppProxy;
    }
    
    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "dispose");
        }
    }

    /**
     * Returns the {@link UnboundAppProxy}.
     * @param applicationName applicationName is application name in the Application Property File. It was matched the
     *            application name in XAIT.
     * @return {@link UnboundAppProxy}
     */
    public UnboundAppProxy getUnboundAppProxy(String applicationName) {
        if (unboundAppProxy == null) {
            return null;
        }
        for (int i = 0; i < unboundAppProxy.length; i++) {
            if (unboundAppProxy[i].getName().equalsIgnoreCase(applicationName)) {
                return unboundAppProxy[i];
            }
        }

        return null;
    }

    private void startRegisteredAPIDaemon() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startRegisteredAPIDaemon");
        }
        currentMode = MODE_STARTED_REGISTERED_API_DAEMON;

        flagOfReadyApplicationId = new Vector();
        for (int i = 0; i < unboundAppProxy.length; i++) {
            if (unboundAppProxy[i].getPriority() != MONITOR_PRORITY
                    && unboundAppProxy[i].getAppType() == UnboundAppProxy.DAEMON
                    && unboundAppProxy[i].hasRegisteredAPI()) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "startDaemon (REG API) " + unboundAppProxy[i].getName()
                            + " start");
                }
                // Sets the application id into Vector.
                flagOfReadyApplicationId.add(unboundAppProxy[i].getAppID());
                DVBJProxy appProxy = unboundAppProxy[i].getDVBJProxy();
                appProxy.addAppStateChangeEventListener(this);
                unboundAppProxy[i].start(null);

            } else if (unboundAppProxy[i].getPriority() != MONITOR_PRORITY
                    && unboundAppProxy[i].getAppType() == UnboundAppProxy.SEMI_DAEMON
                    && unboundAppProxy[i].hasRegisteredAPI()) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "start Semi-Daemon (REG API)"
                            + unboundAppProxy[i].getName() + " initXlet()");
                }
                // Sets the application id into Vector.
                flagOfReadyApplicationId.add(unboundAppProxy[i].getAppID());
                DVBJProxy appProxy = unboundAppProxy[i].getDVBJProxy();
                appProxy.addAppStateChangeEventListener(this);
                unboundAppProxy[i].init();
            }
        }
    }

    /**
     * Starts daemon (semi-daemon) applications.
     */
    private void startNormalDaemon() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startNormalDaemon");
        }
        currentMode = MODE_STARTED_NORMAL_DAEMON;
        flagOfReadyApplicationId = new Vector();

        for (int i = 0; i < unboundAppProxy.length; i++) {
            if (unboundAppProxy[i].getPriority() != MONITOR_PRORITY
                    && unboundAppProxy[i].getAppType() == UnboundAppProxy.DAEMON
                    && !unboundAppProxy[i].hasRegisteredAPI()) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "startDaemon " + unboundAppProxy[i].getName()
                            + " start");
                }
                // Sets the application id into Vector.
                flagOfReadyApplicationId.add(unboundAppProxy[i].getAppID());
                DVBJProxy appProxy = unboundAppProxy[i].getDVBJProxy();
                appProxy.addAppStateChangeEventListener(this);
                unboundAppProxy[i].start(null);

            } else if (unboundAppProxy[i].getPriority() != MONITOR_PRORITY
                    && unboundAppProxy[i].getAppType() == UnboundAppProxy.SEMI_DAEMON
                    && !unboundAppProxy[i].hasRegisteredAPI()) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "start Semi-Daemon " + unboundAppProxy[i].getName()
                            + " initXlet()");
                }
                // Sets the application id into Vector.
                flagOfReadyApplicationId.add(unboundAppProxy[i].getAppID());
                DVBJProxy appProxy = unboundAppProxy[i].getDVBJProxy();
                appProxy.addAppStateChangeEventListener(this);
                unboundAppProxy[i].init();
            }
        }

        // 모든 App들이 정상적으로 시작 하지 못하는 경우에도 부팅이 되도록 30초 시간이 경과 하면
        // 기다리는 것을 중단 하겠다.
        
        long timeout = WAIT_TIME;
        try {
        	String timeOutS = DataManager.getInstance().getUnboundApplicationsInitializeTimeout();
        	int tempTime = Integer.parseInt(timeOutS.trim());
        	timeout = tempTime * 1000L;
        } catch (Exception e) {
        	Log.print(e);
        }
        
        try {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "start timer time=" + timeout);
            }
            waitTimerWentOff = false;
            checkTimerSpec.addTVTimerWentOffListener(this);
            checkTimerSpec.setDelayTime(timeout);
            checkTimerSpec.setRepeat(false);
            timer.scheduleTimerSpec(checkTimerSpec);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }

    }

    /**
     * Builds the unbound application database.
     */
    private void buildUnboundAppDB() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "buildUnboundAppDB");
        }
        AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        Vector tempProxy = new Vector();
        Vector tempAttr = new Vector();
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();

                boolean alreadyAdd = false;
                for (int i = 0; i < tempAttr.size(); i++) {
                    AppAttributes at = (AppAttributes) tempAttr.get(i);
                    if (at.getIdentifier().equals(appID)) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(LOG_HEADER + "Already added app proxy=" + appID);
                        }
                        alreadyAdd = true;
                        break;
                    }
                }
                if (alreadyAdd) {
                    continue;
                }

                DVBJProxy proxy = (DVBJProxy) appsDB.getAppProxy(appID);
                tempProxy.addElement(proxy);
                AppAttributes attr = appsDB.getAppAttributes(appID);
                tempAttr.addElement(attr);



            }
        }

        unboundAppProxy = new UnboundAppProxy[tempProxy.size()];

        for (int i = 0; i < tempProxy.size(); i++) {
            DVBJProxy proxy = (DVBJProxy) tempProxy.elementAt(i);
            AppAttributes attr = (AppAttributes) tempAttr.elementAt(i);

            int appVersion = -1;
            int storagePriority = -1;
            try {
	            Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
	            if (Log.INFO_ON) {
	            	Log.printInfo("class name is " + cls.toString());
	            }
	            Method method = cls.getMethod("getVersion", null);
	            if (Log.INFO_ON) {
	            	Log.printInfo("method name is " + method.toString());
	            }
	            if(!method.isAccessible()) {
	            	method.setAccessible(true);
	            }
	            Object obj = method.invoke(attr, null);
	            Integer integer = (Integer)obj;
	            appVersion = integer.intValue();
	            if (Log.INFO_ON) {
	            	Log.printInfo("version = " + appVersion);
	            }
	            
	            // It is debugging code.
	            Method method0 = cls.getMethod("getStoragePriority", null);
	            if(!method0.isAccessible()) {
	            	method0.setAccessible(true);
	            }
	            Object obj0 = method0.invoke(attr, null);
	            Integer integer0 = (Integer)obj0;
	            storagePriority = integer0.intValue();
	            if (Log.INFO_ON) {
	    			Log.printInfo(LOG_HEADER + "storagePriority() " + storagePriority);
	    		}
	            
            } catch (Exception e) {
            	e.printStackTrace();
            }

            unboundAppProxy[i] = new UnboundAppProxy(proxy, attr, appVersion, storagePriority, false);

            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "Unbound App Proxy is added: "
                        + unboundAppProxy[i].getAppID() + ":" + unboundAppProxy[i].getName() + ":"
                        + unboundAppProxy[i].getDVBJProxy() + ": Priority = " + attr.getPriority() + ", version=" + appVersion);
            }
        }
    }

    /**
     * Implementation of AppStateChangeEventListener.
     * @param ev the instance of AppStateChangeEvent.
     */
    public void stateChange(AppStateChangeEvent ev) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "stateChange() waitTimerWentOff=" + waitTimerWentOff);
            Log.printDebug(LOG_HEADER + "stateChange() " + ev + ", App=" + ev.getAppID());
            Log.printDebug(LOG_HEADER + "flagOfReadyApplicationId's size="
                    + flagOfReadyApplicationId.size());
            for (int i = 0; i < flagOfReadyApplicationId.size(); i++) {
                AppID sAppID = (AppID) flagOfReadyApplicationId.get(i);
                Log.printDebug(LOG_HEADER + "Old Not ready app = " + sAppID);
            }
        }
        synchronized (this) {
            if (waitTimerWentOff) {
                // Timer 에 의해서 모든 Resource를 다 정리 한 상태이기 때문에 아래 기능이 동작할 필요가 없다.
                return;
            }

            AppID id = ev.getAppID();
            UnboundAppProxy ubProxy = findUnboundAppProxy(id);
            ubProxy.getDVBJProxy().removeAppStateChangeEventListener(this);
            flagOfReadyApplicationId.remove(id);
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "New flagOfReadyApplicationId size="
                        + flagOfReadyApplicationId.size());
                for (int i = 0; i < flagOfReadyApplicationId.size(); i++) {
                    AppID sAppID = (AppID) flagOfReadyApplicationId.get(i);
                    Log.printDebug(LOG_HEADER + "New Not ready app = " + sAppID);
                }
            }
            if (flagOfReadyApplicationId.size() < 1) {
                if(Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "currentMode=" + currentMode);
                }
                if (currentMode == MODE_STARTED_REGISTERED_API_DAEMON) {
                    startNormalDaemon();
                } else {
                    Thread finishAppLaunchingThread = new Thread("FinishAppLaunchingThread") {
                        public void run() {
                            MonitorMain.getInstance().completedAllDaemonApplicationLauching();
                        }
                    };
                    finishAppLaunchingThread.start();
                }
            }
        }

    }

    /**
     * Finds the unbound application proxy using application id.
     * @param appID the application id.
     * @return the UnboundAppProxy instance.
     */
    private UnboundAppProxy findUnboundAppProxy(AppID appID) {
        for (int i = 0; i < unboundAppProxy.length; i++) {
            if (unboundAppProxy[i].equals(appID, null)) {
                return unboundAppProxy[i];
            }
        }
        return null;

    }

    /**
     * The interface for timer event.
     * @param event The timer event.
     */
    public void timerWentOff(TVTimerWentOffEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "timerWentOff(waitTimerSpec)");
        }
        synchronized (this) {
            waitTimerWentOff = true;
        }
        checkTimerSpec.removeTVTimerWentOffListener(this);

        int size = flagOfReadyApplicationId.size();
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "flagOfReadyApplicationId's size=" + size);
        }
        if (size > 0) {
            for (int i = 0; i < flagOfReadyApplicationId.size(); i++) {
                AppID sAppID = (AppID) flagOfReadyApplicationId.get(i);
                if(Log.DEBUG_ON) {
                    Log.printInfo(LOG_HEADER + "Current Not ready app = " + sAppID);
                }
            }

            for (int i = 0; i < size; i++) {
                AppID id = (AppID) flagOfReadyApplicationId.get(i);
                UnboundAppProxy ubProxy = findUnboundAppProxy(id);
                ubProxy.getDVBJProxy().removeAppStateChangeEventListener(this);
            }
            flagOfReadyApplicationId.removeAllElements();

            Thread finishAppLaunchingThread = new Thread("FinishAppLaunchingThread") {
                public void run() {
                    MonitorMain.getInstance().completedAllDaemonApplicationLauching();
                }
            };
            finishAppLaunchingThread.start();
        }

    }

}

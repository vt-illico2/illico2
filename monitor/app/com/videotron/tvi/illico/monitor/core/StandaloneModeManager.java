package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;

import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;
import com.videotron.tvi.illico.monitor.ui.StandaloneModeScene;

public class StandaloneModeManager implements LayeredKeyHandler, ScreenSaverConfirmationListener, PowerModeChangeListener {

	private StandaloneModeScene standaloneModeScene;

	private static StandaloneModeManager instance;

	private static final String APP_NAME_FOR_SCREEN_SAVER = "StandaloneModePopup";
	/** The log header message. */
	private static final String LOG_HEADER = "[SALMM] ";

	public synchronized static StandaloneModeManager getInstance() {
		if (instance == null) {
			instance = new StandaloneModeManager();
		}
		return instance;
	}
	
	private StandaloneModeManager() {
		Host.getInstance().addPowerModeChangeListener(this);
	}

	public void start() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "start()");
		}

		/*
		 * // Check authority short standaloneEntitlementId =
		 * AuthorizationManager
		 * .getInstance().getStoredStandaloneEntitlementId();
		 *
		 * int authResult =
		 * AuthorizationManager.getInstance().checkSourceAuthorization
		 * (standaloneEntitlementId); if (Log.DEBUG_ON) {
		 * Log.printDebug(LOG_HEADER + "standaloneEntitlementId=" +
		 * standaloneEntitlementId + ", authResult=" + authResult); }
		 *
		 * if(authResult == AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED) {
		 * isValidStandalone = false; } else { isValidStandalone = true; }
		 */

		String pvrName = DataManager.getInstance().getPVRName();
		UnboundAppProxy pvrProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(pvrName);
		pvrProxy.stop();

		if (standaloneModeScene == null) {
			standaloneModeScene = new StandaloneModeScene(this);
		}
		standaloneModeScene.showScene();
		ScreenSaverManager.getInstance().addScreenSaverConfirmListener(this, APP_NAME_FOR_SCREEN_SAVER);
	}

	public void pause() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "pause()");
		}
		standaloneModeScene.hideScene();

		ScreenSaverManager.getInstance().removeScreenSaverConfirmListener(this, APP_NAME_FOR_SCREEN_SAVER);
		
		Thread pvrThread = new Thread("PvrStartThread") {
			public void run() {
				String pvrName = DataManager.getInstance().getPVRName();
				UnboundAppProxy pvrProxy = UnboundAppsManager.getInstance().getUnboundAppProxy(pvrName);
				pvrProxy.start(null);
			}
		};
		pvrThread.start();
		
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "handleKeyEvent() event=" + event);
		}
		
		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = event.getCode();

		if (keyCode == OCRcEvent.VK_POWER) {
			return false;
		}

		if (keyCode == HRcEvent.VK_ENTER) {
			pause();
		}
		return true;
	}
	
	public boolean confirmScreenSaver() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "confirmScreenSaver() return true");
		}
		return true;
	}
	
	/**
	 * Implements the org.ocap.hardware.PowerModeChangeListener.
	 *
	 * @param newPowerMode
	 *            the new power mode.
	 */
	public synchronized void powerModeChanged(int newPowerMode) {
		
		int bootState = MonitorMain.getInstance().getCurrentBootState();
		
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "newPowerMode=" + newPowerMode + ", bootState=" + bootState);
		}

		if (newPowerMode == Host.FULL_POWER) {
			start();
		} else {
			start();
			try {
				CommunicationManager.getInstance().getEpgService().stopChannel();
			} catch (Exception e) {
				Log.print(e);
			}
		}

	}
}

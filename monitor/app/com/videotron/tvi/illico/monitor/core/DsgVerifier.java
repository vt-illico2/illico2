package com.videotron.tvi.illico.monitor.core;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.DataManager;

public class DsgVerifier {

	private static DsgVerifier instance;

	private String checkUrl;

	private static final String LOG_HEADER = "[DsgVerifier] ";

	public synchronized static DsgVerifier getInstance() {
		if (instance == null) {
			instance = new DsgVerifier();
		}
		return instance;
	}

	private DsgVerifier() {
		checkUrl = DataManager.getInstance().getDsgCheckingUrl();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkUrl=" + checkUrl);
		}
	}
	
	public boolean checkDsgState() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkDsgState() " + checkUrl);
		}
		OutputStreamWriter wr = null;
		BufferedReader rd = null;

		try {
			URL url = new URL(checkUrl);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			wr = new OutputStreamWriter(conn.getOutputStream());
			wr.write("");
			wr.flush();

			// Get the response
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String line;
			while (true) {
				line = rd.readLine();
				if (line == null) {
					break;
				}
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "check result : " + line);
				}
			}

			wr.close();
			rd.close();
			return true;

		} catch (Exception e) {
			Log.printError(e);
		} finally {
			try {
				if (wr != null) {
					wr.close();
				}
				if (rd != null) {
					rd.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkDsgState() return false");
		}
		return false;
	}

}

package com.videotron.tvi.illico.monitor.core;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.VideoContextListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;

/**
 * Monitor internal state manager.
 * This class manages the video state and overlapping app (minor app) state.
 * @author WSLEE1
 * 
 */
public class MonitorStateManager {
	/** Minor application state none. */
	public static final int OVERLAPPING_APP_NONE = 0;
	/** Minor application state EPG. */
	public static final int OVERLAPPING_APP_EPG = 100;
	/** Minor application state PIP. */ 
	public static final int OVERLAPPING_APP_PIP = 101;
	/** Minor application state Full Menu. */
	public static final int OVERLAPPING_APP_FULL_MENU = 102;
	/** Minor application state Mini Menu. */
	public static final int OVERLAPPING_APP_MINI_MENU = 201;
	/** Minor application state Dashboard. */
	public static final int OVERLAPPING_APP_DASHBOARD = 202;
	/** Minor application state PVR main. It is called by Menu. */
	public static final int OVERLAPPING_APP_PVR_MAIN = 301;
	/** Minor application state VOD main. It is called by Menu. */
	//public static final int OVERLAPPING_APP_VOD_MAIN = 302;
	
	/** Current minor application state. */
	private int mOverlappingAppState = OVERLAPPING_APP_NONE;

	/** indicates the normal video state. */
	public static final int VIDEO_STATE_NORMAL = 1000;
	/** Indicates the special video state. */
	public static final int VIDEO_STATE_SPECIAL = 10001;
	/** Current video state. */
	private int mVideoState = VIDEO_STATE_NORMAL;

	private String mVideoContextName = MonitorService.DEFAULT_VIDEO_NAME;
	private String mVideoEndingMessage;

	/** This list has the listeners that notifies of video state. */
    private ArrayList listeners = null;
    
	private static MonitorStateManager instance;
	
	private VideoController videoController;

	/** The log header string. */
	private static final String LOG_HEADER = "[MonitorStateManager] ";

	/**
	 * Singleton instance.
	 * @return instance of MonitorStateManager.
	 */
	public synchronized static MonitorStateManager getInstance() {
		if (instance == null) {
			instance = new MonitorStateManager();
		}
		return instance;
	}

	/**
	 * Constructor.
	 */
	private MonitorStateManager() {
		listeners = new ArrayList();
	}

	/**
	 * Gets the current overlapping application state.
	 * {@see MonitorStateManager#OVERLAPPING_APP_STATE_FULL_MENU}, {@see MonitorStateManager#MINOR_APP_STATE_DASHBOARD},
	 * {@see MonitorStateManager#MINOR_APP_STATE_NONE}, {@see MonitorStateManager#MINOR_APP_STATE_EPG},
	 * {@see MonitorStateManager#MINOR_APP_STATE_PIP}, {@see MonitorStateManager#MINOR_APP_STATE_MINI_MENU}.
	 * 
	 * @return current overlapping state
	 */
	public int getOverlappingAppState() {
		return mOverlappingAppState;
	}

	/**
	 * Gets the current video state.
	 * {@see MonitorStateManager#VIDEO_STATE_NORMAL}, {@see MonitorStateManager#VIDEO_STATE_SPECIAL}.
	 * @return current vidoe state.
	 */
	public int getVideoState() {
		return mVideoState;
	}
	
	/**
	 * Returns weather special video state or not.
	 * @return If current video state is special, return true otherwise return false.
	 */
	public boolean isSpecialVideoState() {
		if(mVideoState == VIDEO_STATE_SPECIAL) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the current video context name.
	 * @return video name.
	 */
	public String getCurrentVideoName() {
		return mVideoContextName;
	}
	
	public String getCurrentOverlappingApplicationName() {
		if(isSpecialVideoState() == false) {
			return null;
		}
		
		switch (mOverlappingAppState) {
		case OVERLAPPING_APP_NONE:
			return null;
		case OVERLAPPING_APP_EPG:
			return "EPG";
		case OVERLAPPING_APP_PIP:
			return "PIP";
		case OVERLAPPING_APP_FULL_MENU:
			return "FULL MENU";
		case OVERLAPPING_APP_MINI_MENU:
			return "MINI MENU";
		case OVERLAPPING_APP_DASHBOARD:
			return "Dashboard";
		case OVERLAPPING_APP_PVR_MAIN:
			return "PVR MAIN";
		}
		return null;
	}

	/**
	 * Enter to special video mode. It is called by PVR, VOD, PCS.
	 * 
	 * @param videoName
	 *            the video name.
	 * @param endingMessage
	 *            Monitor can stop special video mode and shows confirm popup.
	 *            This parameter is confirm popup message. If null, Monitor does
	 *            not show confirm popoup.
	 * @throws RemoteException the exception.
	 */
	public void reserveVideoContext(String videoName, String endingMessage) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "reserveVideoContext() " + videoName + ", " + endingMessage);
		}
		synchronized (this) {
			this.mVideoContextName = videoName;
			this.mVideoEndingMessage = endingMessage;
			mVideoState = VIDEO_STATE_SPECIAL;
		}
		
		notifyVideoModeChanged(videoName);
		
		ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
	}

	/**
	 * Exit to normal video mode.
	 * @param doNotifyStateChanged whether do notify or not.
	 * @throws RemoteException the exception.
	 */
	public void releaseVideoContext(boolean doNotifyStateChanged) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "releaseVideoContext() " + mVideoState + ", " + doNotifyStateChanged);
		}
		synchronized (this) {
			if (mVideoState == VIDEO_STATE_NORMAL) {
				return;
			}
			this.mVideoContextName = null;
			this.mVideoEndingMessage = null;
			mVideoState = VIDEO_STATE_NORMAL;
			mVideoContextName = MonitorService.DEFAULT_VIDEO_NAME;
		}
		
		// PVR Special scenario .. 
		// PVR playback 중에 PVR Main 이 Overlapping app 으로 뜬 상태에서 releaseVideoContext() 가 오면 이때는 
		// stopOverlappingApplication(false)를 하면 안 된다.
		boolean doNotStopOverlappingApp = false;
		TransitionEvent prevEvent = StateManager.getInstance().getCurrentEvent();
		if(prevEvent != null) {
			if(prevEvent.getAppType() == TransitionEvent.PVR) {
				if(mOverlappingAppState == MonitorStateManager.OVERLAPPING_APP_PVR_MAIN) {
					doNotStopOverlappingApp = true;
				}
			}
		}
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "releaseVideoContext() doNotStopOverlappingApp : " + doNotStopOverlappingApp);
		}
		
		if(!doNotStopOverlappingApp) {
			stopOverlappingApplication(false);
		}
		else {
			mOverlappingAppState = OVERLAPPING_APP_NONE;;
		}
		
		notifyVideoModeChanged(mVideoContextName);
		
		if (doNotifyStateChanged) {
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
		}
	}

	/**
	 * Sets the minor (overlapping) app state.
	 * @param minorApp Minor application.
	 */
	public void startOverlappingApplication(int overlappingApp) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startOverlappingApplication() " + mOverlappingAppState + ", currentOverlapping");
		}
		
		if ((overlappingApp == mOverlappingAppState)
				&& (overlappingApp == MonitorStateManager.OVERLAPPING_APP_DASHBOARD)) {
			if (Log.INFO_ON) {
				Log
						.printInfo("LOG_HEADER + startOverlappingApplication() ignore this event because current ovlapping app equals requested app");
			}
			return;
		}

		stopOverlappingApplication(false);
		mOverlappingAppState = overlappingApp;
	}
	
	/**
	 * Stops the current minor application.
	 * @param doNotifyStateChanged whether do notify or not.
	 */
	public void stopOverlappingApplication(boolean doNotifyStateChanged) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "stopOverlappingApplication() " + mOverlappingAppState + ", "
					+ doNotifyStateChanged);
		}
		boolean releaseScaledVideo = false;
		try {
			switch (mOverlappingAppState) {
			case OVERLAPPING_APP_EPG:
			case OVERLAPPING_APP_PIP:
				releaseScaledVideo = true;
				CommunicationManager.getInstance().getEpgService().changeState(EpgService.STATE_NONE);
				if (doNotifyStateChanged) {
					ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
				}
				break;
			case OVERLAPPING_APP_FULL_MENU:
				releaseScaledVideo = true;
				CommunicationManager.getInstance().getMainMenuService().hideMenu();
				if (doNotifyStateChanged) {
					ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
				}
				break;
			case OVERLAPPING_APP_MINI_MENU:
			case OVERLAPPING_APP_DASHBOARD:
				CommunicationManager.getInstance().getMainMenuService().hideMenu();
				break;
			case OVERLAPPING_APP_PVR_MAIN :
				CommunicationManager.getInstance().stopPVRMain();
				break;
			//case OVERLAPPING_APP_VOD_MAIN :
			//	break;
			}
			
			if(releaseScaledVideo) {
				if (videoController == null) {
					videoController = CommunicationManager.getInstance().getEpgService().getVideoController();
				}
				videoController.maximize();
			}
			
		} catch (Exception e) {
			Log.print(e);
		}
		mOverlappingAppState = OVERLAPPING_APP_NONE;

	}
	
	public String getEndingMessage() {
		return mVideoEndingMessage;
	}

	/**
	 * Notify changed mode to the listeners.
     * @param state new state.
     */
    private void notifyVideoModeChanged(String videoName) {
        Object[] lsnrs;
        synchronized (listeners) {
            lsnrs = listeners.toArray();
        }
        for (int i = 0; i < lsnrs.length; i++) {
            try {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "notifyVideoModeChanged to "
                            + ((ListenerItem) lsnrs[i]).listener);
                }
                ((ListenerItem) lsnrs[i]).listener.videoModeChanged(videoName);
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }

    /**
     * Adds the listener
     * @param l The {@link VideoContextListener} instance.
     * @param applicationName The application name that requests listener.
     */
    public void addVideoContextListener(VideoContextListener l, String applicationName) {
        if (Log.INFO_ON) {
            Log.printInfo("ServiceStateManager: addVideoContextListener=" + applicationName);
        }
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            if (!listeners.contains(item)) {
                listeners.add(item);
            } else {
                // Removes a previous listener.
                listeners.remove(item);
                // Adds a new listener.
                listeners.add(item);
            }
        }
    }

    /**
     * Removes a listener.
     * @param l the listener instance.
     * @param applicationName the requested application name.
     */
    public void removeVideoContextListener(VideoContextListener l, String applicationName) {
    	if (Log.INFO_ON) {
            Log.printInfo("ServiceStateManager: removeVideoContextListener=" + applicationName);
        }
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            listeners.remove(item);
        }
    }

    /**
     * This class has the unique application id and the instance of VideoContextListener.
     * @author Woosik Lee
     */
    private class ListenerItem {
        /** The instance of listener. */
        private VideoContextListener listener;
        /** The requested application name. */
        private String applicationName;

        /**
         * Constructor.
         * @param l listener.
         * @param appName application name.
         */
        public ListenerItem(final VideoContextListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItem) {
                String appName = ((ListenerItem) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }

}

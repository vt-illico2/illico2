package com.videotron.tvi.illico.monitor.core;

import java.rmi.RemoteException;
import java.util.ArrayList;

import com.opencable.handler.cahandler.CAAuthorizationEvent;
import com.opencable.handler.cahandler.CAAuthorizationListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorCAAuthorizationListener;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;

public class CAResourceAuthorizationListenerImpl implements CAAuthorizationListener {

	private static CAResourceAuthorizationListenerImpl instance;

	private static final String LOG_HEADER = "[CAResourceListenerImpl] ";

	/** CAAuthorization listeners. */
	private ArrayList listeners = null;

	private CAResourceAuthorizationListenerImpl() {
		listeners = new ArrayList();
	}

	public synchronized static CAResourceAuthorizationListenerImpl getInstance() {
		if(instance == null) {
			instance = new CAResourceAuthorizationListenerImpl();
		}
		return instance;
	}

	/**
	 * Callback from CANH
	 */
	public void deliverEvent(CAAuthorizationEvent event) throws RemoteException {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "deliverEvent=" + event.isAuthorized() + "," + event.getTargetId() + "," + event.getTargetType());
		}
		notifyCAAuthorizationListener(event);
	}


	/**
	 * Notifies listeners.
	 *
	 * @param state
	 */
	private void notifyCAAuthorizationListener(CAAuthorizationEvent event) {
		Object[] lsnrs;
		synchronized (listeners) {
			lsnrs = listeners.toArray();
		}

		short tId = (short) event.getTargetId();
		String packageId = EIDDataManager.getInstance().findPackageIdByEid(tId);
		if(Log.INFO_ON) {
			Log.printDebug("notifyCAListener found package id = " + packageId);
		}

		for (int i = 0; i < lsnrs.length; i++) {
			try {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "notify to "
							+ ((ListenerItem) lsnrs[i]).listener);
				}
				((ListenerItem) lsnrs[i]).listener.deliverEvent(event.getTargetId(), event.getTargetType(), event.isAuthorized(), packageId);
			} catch (Throwable t) {
				Log.print(t);
			}
		}
	}

	/**
	 * Adds the listener for the service state transition.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            The application name that requests listener.
	 */
	public void addCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "addCAResourceAuthorizationListener=" + applicationName);
		}
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			if (!listeners.contains(item)) {
				listeners.add(item);
			} else {
				// Removes a previous listener.
				listeners.remove(item);
				// Adds a new listener.
				listeners.add(item);
			}
		}
	}

	/**
	 * Removes a listener.
	 *
	 * @param l
	 *            the listener instance.
	 * @param applicationName
	 *            the requested application name.
	 */
	public void removeCAResourceAuthorizationListener(MonitorCAAuthorizationListener l, String applicationName) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "removeCAResourceAuthorizationListener=" + applicationName);
		}
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			listeners.remove(item);
		}
	}

	/**
	 * This class has the unique application id and the instance of
	 * ServiceStateListener.
	 *
	 * @author Woosik Lee
	 */
	private class ListenerItem {
		/** The instance of listener. */
		private MonitorCAAuthorizationListener listener;
		/** The requested application name. */
		private String applicationName;

		/**
		 * Constructor.
		 *
		 * @param l
		 *            listener.
		 * @param appName
		 *            application name.
		 */
		public ListenerItem(final MonitorCAAuthorizationListener l, final String appName) {
			this.listener = l;
			this.applicationName = appName;
		}

		/**
		 * Overrides the equals() by Object class.
		 *
		 * @param o
		 *            the object to compare with this.
		 * @return if equals instance return true, otherwise return false.
		 */
		public boolean equals(Object o) {
			if (o instanceof ListenerItem) {
				String appName = ((ListenerItem) o).applicationName;
				if (appName.equals(applicationName)) {
					return true;
				}
				return false;
			} else {
				return false;
			}
		}
	}


}

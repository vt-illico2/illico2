package com.videotron.tvi.illico.monitor.core;

import org.dvb.event.UserEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.WindowProperty;

public class MonitorKeyBlocker implements LayeredKeyHandler {

	private boolean doBlock;

	private LayeredUI handler;

	private static final String LOG_HEADER = "[MonitorKeyBlocker] ";
	private Object owner;

    public synchronized void setBlock(boolean block) {
    	if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "[" + owner.toString() + "] setBlock:" + block);
		}
    	doBlock = block;
        if (block) {
            handler.activate();
        } else {
            handler.deactivate();
        }
    }

    public MonitorKeyBlocker(Object owner) {
    	if(Log.INFO_ON) {
    		Log.printInfo(LOG_HEADER + "MonitorKeyBlocker : owner=" + owner.toString());
    	}
    	this.owner = owner;
        handler = WindowProperty.APP_LOADING_KEY_BLOCKER.createLayeredKeyHandler(this);
        handler.deactivate();
        doBlock = false;
    }

    public boolean handleKeyEvent(UserEvent e) {
        Log.printWarning(LOG_HEADER + "MonitorKeyBlocker : key blocked by owner=" + owner.toString());
        return true;
    }


}

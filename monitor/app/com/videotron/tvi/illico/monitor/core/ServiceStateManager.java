package com.videotron.tvi.illico.monitor.core;

import java.util.ArrayList;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.log.Log;

/**
 * This class manages the service state of STB. Service state id defined at
 * {@link MonitorService}. If the state change, this class notifies the event to
 * the listeners.
 *
 * @author Woosik Lee
 */
public final class ServiceStateManager implements AppsDatabaseEventListener {

	/** A Singleton instance of ServiceStateManager. */
	private static ServiceStateManager instance;
	/** This list has the listeners that notifies of service transition. */
	private ArrayList listeners = null;
	/** This is the current service state. */
	private int currentState = MonitorService.UNKNOWN_STATE;

	/** The log header message. */
	private static final String LOG_HEADER = "[ServiceStateManager] ";

	/**
	 * Constructor.
	 */
	private ServiceStateManager() {
		listeners = new ArrayList();
		AppsDatabase.getAppsDatabase().addListener(this);
	}

	/**
	 * Gets a singleton instance.
	 *
	 * @return instance of ServiceStateManager.
	 */
	public static synchronized ServiceStateManager getInstance() {
		if (instance == null) {
			instance = new ServiceStateManager();
		}
		return instance;
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "dispose");
		}
		if (listeners != null) {
			listeners.clear();
		}
		AppsDatabase.getAppsDatabase().removeListener(this);
	}

	/**
	 * Sets the service state. If change service state, in this method notify to
	 * the listeners about new state.
	 *
	 * @param newState
	 *            new state of service state.
	 */
	public void setState(int newState) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "setState() newState=" + getServiceStateString(newState)
					+ ", curState=" + getServiceStateString(currentState));
		}
		currentState = newState;
		/*
		 * if (currentState != newState) { currentState = newState; } else {
		 * return; }
		 */
		notifyServiceStateListeners(newState);
	}

	/**
	 * Gets the current service state.
	 *
	 * @return current state.
	 */
	public int getState() {
		return currentState;
	}

	public boolean isTvViewingState() {
		if (currentState == MonitorService.TV_VIEWING_STATE) {
			return true;
		}

		return false;
	}

	/**
	 * Notifies the listeners of the service state transition.
	 *
	 * @param state
	 *            new state.
	 */
	private void notifyServiceStateListeners(int state) {
		Object[] lsnrs;
		synchronized (listeners) {
			lsnrs = listeners.toArray();
		}
		for (int i = 0; i < lsnrs.length; i++) {
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "stateChanged to "
							+ ((ListenerItem) lsnrs[i]).listener);
				}
				((ListenerItem) lsnrs[i]).listener.stateChanged(state);
			} catch (Throwable t) {
				Log.print(t);
			}
		}
	}

	/**
	 * Adds the listener for the service state transition.
	 *
	 * @param l
	 *            The {@link ServiceStateListener} instance.
	 * @param applicationName
	 *            The application name that requests listener.
	 */
	public void addStateListener(ServiceStateListener l, String applicationName) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "addStateListener=" + applicationName);
		}
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			if (!listeners.contains(item)) {
				listeners.add(item);
			} else {
				// Removes a previous listener.
				listeners.remove(item);
				// Adds a new listener.
				listeners.add(item);
			}
		}
	}

	/**
	 * Removes a listener.
	 *
	 * @param l
	 *            the listener instance.
	 * @param applicationName
	 *            the requested application name.
	 */
	public void removeStateListener(ServiceStateListener l, String applicationName) {
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			listeners.remove(item);
		}
	}

	/**
	 * This class has the unique application id and the instance of
	 * ServiceStateListener.
	 *
	 * @author Woosik Lee
	 */
	private class ListenerItem {
		/** The instance of listener. */
		private ServiceStateListener listener;
		/** The requested application name. */
		private String applicationName;

		/**
		 * Constructor.
		 *
		 * @param l
		 *            listener.
		 * @param appName
		 *            application name.
		 */
		public ListenerItem(final ServiceStateListener l, final String appName) {
			this.listener = l;
			this.applicationName = appName;
		}

		/**
		 * Overrides the equals() by Object class.
		 *
		 * @param o
		 *            the object to compare with this.
		 * @return if equals instance return true, otherwise return false.
		 */
		public boolean equals(Object o) {
			if (o instanceof ListenerItem) {
				String appName = ((ListenerItem) o).applicationName;
				if (appName.equals(applicationName)) {
					return true;
				}
				return false;
			} else {
				return false;
			}
		}
	}

	/**
	 * Implements the AppsDatabaseEventListener's method.
	 *
	 * @param paramAppsDatabaseEvent
	 *            the event.
	 */
	public void newDatabase(AppsDatabaseEvent paramAppsDatabaseEvent) {

	}

	/**
	 * Implements the AppsDatabaseEventListener's method.
	 *
	 * @param paramAppsDatabaseEvent
	 *            the event.
	 */
	public void entryAdded(AppsDatabaseEvent paramAppsDatabaseEvent) {

	}

	/**
	 * This method is called when the bound application is deleted at the
	 * AppsDatabase. If the {@link ServiceStateManager} has the bound
	 * application's listener, it must delete.
	 *
	 * @param paramAppsDatabaseEvent
	 *            the event.
	 */
	public void entryRemoved(AppsDatabaseEvent paramAppsDatabaseEvent) {
		synchronized (listeners) {
			AppID appId = paramAppsDatabaseEvent.getAppID();
			AppsDatabase db = AppsDatabase.getAppsDatabase();
			AppAttributes appAtt = db.getAppAttributes(appId);
			String appName = appAtt.getName();
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "entryRemoved() appName=" + appName);
			}

			ListenerItem item = new ListenerItem(null, appName);
			listeners.remove(item);
		}
	}

	/**
	 * Implements the AppsDatabaseEventListener's method.
	 *
	 * @param paramAppsDatabaseEvent
	 *            the event.
	 */
	public void entryChanged(AppsDatabaseEvent paramAppsDatabaseEvent) {

	}

	public static String getServiceStateString(int state) {
		switch (state) {
		case MonitorService.TV_VIEWING_STATE:
			return "[TV_VIEWING_STATE]";
		case MonitorService.PIP_STATE:
			return "[PIP_STATE]";
		case MonitorService.FULL_SCREEN_APP_STATE:
			return "[APP_STATE]";
		default:
			return "[UNKNOWN]";
		}
	}

}

package com.videotron.tvi.illico.monitor.core;

import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;

/**
 * The event for service state transition and the STB state transition. This class has 3 type of major event. 1. To
 * standby mode (STB state). 2. To running mode (STB state). 3. To other service state. (Service state)
 * <P>
 * And has many type of minor event. That is part of service state. 1. To STATE_TV_VIEWING state. 2. To the EPG guide.
 * 3. To the VOD. 4. To the Menu (Full mode, Mini mode, Dash board) 5. and so on....
 * @author Woosik Lee
 */
public class TransitionEvent {
    /** The major event of service state. */
    public static final int TRANSITION = 0;
    /** The major event of STB state. */
    public static final int STANDBY = 1;
    /** The major event of STB state. */
    public static final int RUN = 2;

    /** The minor event of service state. */
    public static final int STATE_TV_VIEWING = MonitorService.TV_VIEWING_STATE;
    /** The event for TV_VIEWING_SAAE without Menu. */
    public static final int STATE_TV_VIEWING_WITHOUT_MENU = 99;
    /** The event for TV_VIEWING_STATE without Menu and Video. */
    public static final int STATE_TV_VIEWING_WITHOUT_MENU_AND_AV = 98;
    /** The event for FULL_SCREEN_APP_STATE without application. */
    public static final int STATE_FULL_SCREEN_APP_WITHOUT_APPLICATION = 990;
    /** The event to guide. */
    public static final int GUIDE = 100;
    /** The event to menu. */
    public static final int MAIN_MENU = 101;
    /** The event to vod. */
    public static final int VOD = 102;
    /** The event to settings. */
    public static final int SETTINGS = 103;
    /** The event to PVR. */
    public static final int PVR = 104;
    /** The event to PIP. */
    public static final int PIP = 105;
    /** The event to ISA. */
    public static final int ISA = 106;
    /** The normal unbound application. */
    public static final int UNBOUND_APP = 200;


    /** Indicates the major event. */
    private int type; // TRANSITION, STANDBY, RUN
    /** Indicates the minor event. */
    private int appType; // MiniEPG, GRID, VOD...
    /** specific state for the menu. */
    private int menuState; // MainMenu's scene type.
    /** The application name. */
    private String applicationName;

    /** Instance of {@link UnboundAppProxy}. */
    private UnboundAppProxy appProxy;
    /** The parameters. */
    private String[] parameters;
    /** The channel source id. if -1 indicates the LCW. */
    private int sourceId = -1;
    /** Indicates the LCW source id. */
    public static final int LCW_SOURCE_ID = -1;

    /**
     * Constructor.
     * @param pType the major event.
     */
    public TransitionEvent(final int pType) {
        this.type = pType;
    }

    /**
     * Constructor.
     * @param pType the major event.
     * @param pAppType the minor event.
     * @param pAppName the application name.
     * @param pAppProxy the proxy of unbound application.
     * @param pArgs the arguments for unbound application.
     */
    public TransitionEvent(final int pType, final int pAppType, final String pAppName, final UnboundAppProxy pAppProxy,
            final String[] pArgs) {
        this.type = pType;
        this.appType = pAppType;
        this.appProxy = pAppProxy;
        this.parameters = pArgs;
        this.applicationName = pAppName;
    }

    public boolean isTVViewingState() {
        switch(appType) {
        case STATE_TV_VIEWING:
        case STATE_TV_VIEWING_WITHOUT_MENU:
        case STATE_TV_VIEWING_WITHOUT_MENU_AND_AV:
            return true;
        case MAIN_MENU :
            //->Kenneth[2015.7.8] Tank : MAIN_MENU_STATE_FULL_MENU 는 더이상 존재하지 않음.
            return true;
            /*
            if(menuState != MainMenuService.MAIN_MENU_STATE_FULL_MENU) {
                return true;
            }
            */
            //<-
        }
        return false;
    }

    /**
     * Gets the major event type.
     * @return major event.
     */
    public int getType() {
        return type;
    }

    /**
     * Gets the minor event type.
     * @return minor event.
     */
    public int getAppType() {
        return appType;
    }

    /**
     * Gets the parameters of event.
     * @return parameters.
     */
    public String[] getParameter() {
        return parameters;
    }

    /**
     * Sets the parameters of event.
     * @param args parameters.
     */
    public void setParameter(String[] args) {
        this.parameters = args;
    }

    /**
     * Gets the proxy for unbound application.
     * @return proxy.
     */
    public UnboundAppProxy getUnboundAppProxy() {
        return appProxy;
    }

    /**
     * Sets the channel source id.
     * @param pSourceId the channel source id.
     */
    public void setSourceId(int pSourceId) {
        this.sourceId = pSourceId;
    }

    /**
     * Gets the channel source id.
     * @return sourceId the channel source id.
     */
    public int getSourceId() {
        return sourceId;
    }

    /**
     * Sets the Menu's state.
     * @param pMenuState Menu's state.
     */
    public void setMenuState(int pMenuState) {
        this.menuState = pMenuState;
    }

    /**
     * Gets the Menu's state.
     * @return The Menu's state.
     */
    public int getMenuState() {
        return menuState;
    }

    /**
     * Gets the application name.
     * @return the application name.
     */
    public String getApplicationName() {
        return applicationName;
    }

    /**
     * Override the toString() in Object class.
     * @see toString in class java.lang.Object#toString()
     * @return class message.
     */
    public String toString() {
        StringBuffer buf = new StringBuffer();
        buf.append("TYPE = " + type);
        buf.append(", appType : " + appType);
        buf.append(", appName=" + applicationName);
        buf.append("\n");
        buf.append("appProxy : " + (appProxy == null ? "NULL" : "PROXY is not null"));
        buf.append("\n");
        buf.append("args[] : ");
        if (parameters != null) {
            for (int i = 0; i < parameters.length; i++) {
                buf.append(parameters[i] + "\t");
            }
        } else {
            buf.append(" is NULL");
        }
        buf.append("\n");
        return buf.toString();
    }
}

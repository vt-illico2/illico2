package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;

import org.dvb.event.EventManager;
import org.dvb.event.UserEvent;
import org.dvb.event.UserEventListener;
import org.dvb.event.UserEventRepository;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.dvb.application.DVBJProxy;

import com.videotron.tvi.illico.ixc.epg.ChannelContext;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.TvChannel;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.KeyCodes;
//->Kenneth : Popup added
import com.videotron.tvi.illico.monitor.ui.popup.ListPressedInNonPvrPopup;
import com.videotron.tvi.illico.monitor.core.UnboundAppsManager;
import com.videotron.tvi.illico.monitor.core.UnboundAppProxy;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIInfo;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.monitor.communication.MonitorServiceImpl;

/**
 * This class handles the hot key that is inputted by user.
 *
 * @author Woosik Lee
 */
public class MonitorKeyHandler implements UserEventListener {

	/** This is a repository for register key event. */
	private UserEventRepository repository = new UserEventRepository("Monitor");
	/** The parameters about how to start applications. */
	private static final String[] APP_PARAMETERS_HOT_KEY = { MonitorService.REQUEST_APPLICATION_HOT_KEY };

	private static final String[] APP_PARAMETERS_SETTING_ASPECT_SCENE = { MonitorService.REQUEST_APPLICATION_HOT_KEY,
			"ASPECT" };
	/** The flag. UPP 응답을 기다리는지 여부를 확인 하기 위해서. */
	private boolean checkingUPP;
	/** The filter interface from User Profile application. */
	private RightFilterListenerImpl listener = new RightFilterListenerImpl();

	private static final String LOG_HEADER = "[UserEventManager] ";

	/**
	 * Constructor.
	 */
	public MonitorKeyHandler() {
		init();
	}

	/**
	 * Initializes the class.
	 */
	private void init() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "init()");
		}
		checkingUPP = false;
		repository.addKey(KeyCodes.VOD);
		repository.addKey(KeyCodes.MENU);
		repository.addKey(KeyCodes.COLOR_A);
		repository.addKey(HRcEvent.VK_GUIDE);
		// It just test for dashboard.
		repository.addKey(KeyCodes.WIDGET);

        //->Kenneth[2017.4.26] VDTRMASTER-6125 : VBM 로그 남기는 이유로 더이상 모니터에서 키 처리 안함.
		//repository.addKey(KeyCodes.SEARCH);
        //<-

		// PIP
		repository.addKey(KeyCodes.PIP);

		repository.addKey(KeyCodes.SETTINGS);

		// PVR
		repository.addKey(KeyCodes.LIST);

        //->Kenneth[2017.7.15] : Zoom 키 동작이 바뀌면서 EPG 가 처리할 것임
		// #
		//repository.addKey(KeyCodes.ASPECT);
        //<-

		repository.addKey(HRcEvent.VK_ESCAPE);
		if (Environment.EMULATOR) {
			repository.addKey(HRcEvent.VK_POWER);
		}
	}

	/**
	 * Starts the class.
	 */
	public void start() {
		EventManager.getInstance().addUserEventListener(this, repository);
	}

	/**
	 * Stops the class.
	 */
	public void stop() {
		EventManager.getInstance().removeUserEventListener(this);
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		stop();
	}

	/**
	 * Implements the UserEventListener interface. It receive the user event and
	 * process the hot key.
	 *
	 * @param e
	 *            inputed key event.
	 */
	public synchronized void userEventReceived(UserEvent e) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "userEventReceived event is " + e);
		}
		if (e.getType() != KeyEvent.KEY_PRESSED) {
			return;
		}

		// TEST
		// /if(e.getCode() == KeyCodes.COLOR_A) {
		// InbandDataUpdateController.getInstance().dataUpdated();
		// AppLauncher.getInstance().startUnboundApplication(AppLauncher.PIP_NAME,
		// APP_PARAMETERS_HOT_KEY);
		// return;
		// }
		// //////////

		if (Environment.EMULATOR) {
			if (e.getCode() == HRcEvent.VK_POWER) {
				Log.printInfo("WSLEE : Power key is inputed");

				int currentPowerMode = Host.getInstance().getPowerMode();
				if (currentPowerMode == Host.FULL_POWER) {
					Host.getInstance().setPowerMode(Host.LOW_POWER);
				} else {
					Host.getInstance().setPowerMode(Host.FULL_POWER);
				}
			}
		}

		handleKeyEvent(e.getCode());
	}

	public void handleKeyEvent(int keyCode) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "handleKeyEvent keyCode is " + keyCode);
		}
		if (checkingUPP) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Checking by UPP now so ignore this event.");
			}
			return;
		}

		if (STBModeManager.getInstance().isStandaloneMode()) {
			switch (keyCode) {
			case HRcEvent.VK_ESCAPE:
				StandaloneModeManager.getInstance().start();
				break;
			default:
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore event because current STB mode is Stanalone.");
				}
				return;
			}
		}

		int previousEventType = Integer.MIN_VALUE;
		TransitionEvent prevEvent = StateManager.getInstance().getCurrentEvent();
		if (prevEvent != null) {
			previousEventType = prevEvent.getAppType();
		}

		switch (keyCode) {
		case KeyCodes.VOD:
			if (previousEventType == TransitionEvent.VOD) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore event because this event same previous event");
				}
				return;
			}
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
							+ DataManager.getInstance().getVodName());
				}
				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					// RightFilterListenerImpl listener = new
					// RightFilterListenerImpl(DataManager.getInstance()
					// .getVodName());
					listener.setAppName(DataManager.getInstance().getVodName(), null);
					uppService.checkRight(listener, DataManager.getInstance().getVodName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getVodName(),
							APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			break;

		case KeyCodes.MENU:
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
							+ DataManager.getInstance().getMenuName());
				}

				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					// RightFilterListenerImpl listener = new
					// RightFilterListenerImpl(DataManager.getInstance()
					// .getMenuName());
					listener.setAppName(DataManager.getInstance().getMenuName(), null);
					uppService.checkRight(listener, DataManager.getInstance().getMenuName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getMenuName(),
							APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			break;
        //->Kenneth[2015.7.2] Tank : A 키도 Widget 띄워야 함.
        /*
		case KeyCodes.COLOR_A:
			if (ServiceStateManager.getInstance().isTvViewingState()) {
				try {
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
								+ DataManager.getInstance().getMenuName());
					}

					PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
					if (uppService != null) {
						checkingUPP = true;
						// RightFilterListenerImpl listener = new
						// RightFilterListenerImpl(DataManager.getInstance()
						// .getMenuName());
						listener.setAppName(DataManager.getInstance().getMenuName(), AppLauncher.FULL_MENU);
						uppService.checkRight(listener, DataManager.getInstance().getMenuName());
					} else {
						AppLauncher.getInstance()
								.startUnboundApplication(AppLauncher.FULL_MENU, APP_PARAMETERS_HOT_KEY);
					}
				} catch (Exception ex) {
					Log.print(ex);
				}

			}
			break;

            */
		case HRcEvent.VK_GUIDE:
			if (previousEventType == TransitionEvent.GUIDE) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore event because this event same previous event");
				}
				return;
			}
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
							+ DataManager.getInstance().getEPGName());
				}

				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					// RightFilterListenerImpl listener = new
					// RightFilterListenerImpl(DataManager.getInstance()
					// .getEPGName());
					listener.setAppName(DataManager.getInstance().getEPGName(), null);
					uppService.checkRight(listener, DataManager.getInstance().getEPGName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getEPGName(),
							APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			break;

		case KeyCodes.PIP:
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "PIP support ? " + Environment.SUPPORT_PIP);
			}
			if (!Environment.SUPPORT_PIP) {
				return;
			}

			if (previousEventType == TransitionEvent.PIP) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore event because this event same previous event");
				}
				return;
			}

			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
							+ DataManager.getInstance().getEPGName());
				}

				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					listener.setAppName(DataManager.getInstance().getEPGName(), AppLauncher.PIP_NAME);
					uppService.checkRight(listener, DataManager.getInstance().getEPGName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(AppLauncher.PIP_NAME, APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			break;

        //->Kenneth[2015.7.2] Tank : A 키도 Widget 띄워야 함.
		case KeyCodes.COLOR_A:
		case KeyCodes.WIDGET:
            //->Kenneth[2015.8.4] VDTRMASTER-5542 : cyo 등에서 처리가 안되서리 여기서 함
			if (keyCode == KeyCodes.COLOR_A && !ServiceStateManager.getInstance().isTvViewingState()) {
                Log.printDebug(LOG_HEADER+"A key but not TV_VIEWING. So return.");
                return;
            } 
            //<-
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
							+ DataManager.getInstance().getDashboardName());
				}

				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					listener.setAppName(DataManager.getInstance().getDashboardName(), null);
					uppService.checkRight(listener, DataManager.getInstance().getMenuName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getDashboardName(),
							APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}
			break;


        //->Kenneth[2017.4.26] VDTRMASTER-6125 : VBM 로그 남기는 이유로 더이상 모니터에서 키 처리 안함.
        // EPG 에서 처리함.
        /*
		case KeyCodes.SEARCH:
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Do not requests checking user profile to UPP "
						+ DataManager.getInstance().getSearchName());
			}

			// If PPV channel, search key should launch PPV application.
			if (ServiceStateManager.getInstance().isTvViewingState()) {
				EpgService epgService = CommunicationManager.getInstance().getEpgService();
				try {
					ChannelContext chContext = epgService.getChannelContext(0);
					if (chContext != null) {
						TvChannel tvChannel = chContext.getCurrentChannel();
						if (tvChannel != null) {
							short chType = tvChannel.getType();
							if (chType == TvChannel.TYPE_PPV) {
								if (Log.DEBUG_ON) {
									Log.printDebug(LOG_HEADER + "Go to PPV Search because current channel is ppv");
								}
								try {
									SearchService searchService = CommunicationManager.getInstance().getSearchService();
									if (searchService != null) {
										if (Log.DEBUG_ON) {
											Log.printDebug(LOG_HEADER + "launchSearchApplication() ");
										}
										searchService.launchSearchApplication(SearchService.PLATFORM_PPV, true);
									} else {
										if (Log.WARNING_ON) {
											Log.printWarning(LOG_HEADER + "ERROR not found Search IXC ");
										}
										CommunicationManager.getInstance().showErrorMessage(
												CommunicationManager.ERROR_504, null);
									}
								} catch (Exception e) {
									Log.print(e);
								}
								return;
							}
						}
					}
				} catch (Exception e) {
					Log.print(e);
				}
			}

			// Search 는 스스로 권한 체크를 한다.
			AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getSearchName(),
					APP_PARAMETERS_HOT_KEY);
			break;
            */
            //<-
		case KeyCodes.ASPECT:
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Requests checking user profile(ASPECT) to UPP "
						+ AppLauncher.SETTINGS_NAME);
			}
			try {
				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					listener.setAppName(AppLauncher.SETTINGS_NAME, String.valueOf(KeyCodes.ASPECT));
					uppService.checkRight(listener, AppLauncher.SETTINGS_NAME);
				}
				else {
					if (Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "call setting directly by #");
					}
					CommunicationManager.getInstance().getPreferenceService().showSettingsMenu(APP_PARAMETERS_SETTING_ASPECT_SCENE);
				}
			} catch(Exception ex) {
				Log.print(ex);
			}

			break;
		case KeyCodes.SETTINGS:
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Requests checking user profile to UPP "
						+ AppLauncher.SETTINGS_NAME);
			}
			try {
				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					listener.setAppName(AppLauncher.SETTINGS_NAME, String.valueOf(KeyCodes.SETTINGS));
					uppService.checkRight(listener, AppLauncher.SETTINGS_NAME);
				} else {
					AppLauncher.getInstance().startUnboundApplication(AppLauncher.SETTINGS_NAME,
						APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}
			break;
		case KeyCodes.LIST:
            //->Kenneth : LIST button pressed for test
            //if (true) {
            //    handleListPopup();
            //    return;
            //}
			if (!Environment.SUPPORT_DVR) {
				// Non PVR STB can't use the LIST button.
				// Nevertheless if Non PVR STB has a authority for PVR Multiroom
				// package, LIST button is available.
				int authResult = AuthorizationManager.getInstance().checkPvrMultiroomPackage();
				if (authResult == AuthorizationManager.CHECK_RESULT_AUTHORIZED) {
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "This is Non PVR STB but this STB has authority of PVR multiroom package so LIST button is available.");
					}
				} else {
                    //->Kenneth [R4.1] : LIST BEHAVIOR 
					//CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_503, null);
                    boolean isPopupVisible = decideListPopupDisplay();
                    if (isPopupVisible) {
                        Log.printDebug("Kenneth : show popup from MonitorKeyHandler.");
                        ListPressedInNonPvrPopup.getInstance().showPopup();
                    }
					return;
				}
				
			}
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Requests checking user profile to PVR "
						+ DataManager.getInstance().getPVRName());
			}
			try {
				PreferenceService uppService = CommunicationManager.getInstance().getPreferenceService();
				if (uppService != null) {
					checkingUPP = true;
					listener.setAppName(DataManager.getInstance().getPVRName(), null);
					uppService.checkRight(listener, DataManager.getInstance().getPVRName());
				} else {
					AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getPVRName(),
							APP_PARAMETERS_HOT_KEY);
				}
			} catch (Exception ex) {
				Log.print(ex);
			}

			break;
		default:
			break;
		}
	}

    //->Kenneth : non-pvr 이고 multi-room 패키지가 아닌 상태에서 이 메쏘드가 호출된다.
    // 여기서는 확인할 사항은 다음과 같다.
    // 1. error message 가 떠 있는 상황에서는 ignore 하고 팝업 띄우지 않음.
    // 2. full-mode 시에는 help 앱이 떠 있는 상태에서만 팝업 띄우고 나머지는 ignore
    // 3. watchtv-mode 시에는 팝업 띄움

    // boolean 을 리턴하는 것으로 수정. 왜냐하면 팝업 안띄우면 그 외의 process 를 계속 진행해야 하기 때문.
    // true 면 팝업 띄움, false 면 안띄움

    // 2014-10.14 : ScreenSaverManager 에서도 호출될 수 있으므로 static 으로 바꿈
    public static boolean decideListPopupDisplay() {
        Log.printDebug("Kenneth : Call decideListPopupDisplay()");
        // 1. error message 떠 있는지 여부 체크
        if (isErrorPopupVisible()) {
            Log.printDebug("Kenneth : isErrorPopupVisible() returns true. So return false.");
            return false;
        }
        // 2. help app 떠 있으면 팝업 띄움
        if (isHelpVisible()) {
            Log.printDebug("Kenneth : isHelpVisible() returns true. So show popup.");
            return true;
        }
        // 3. watch tv 모드이면 팝업 띄움
        // -1 = UNKNOWN_STATE, 0 = TV_VIEWING_STATE, 1 = FULL_SCREEN_APP_STATE, 2 = PIP_STATE
        try {
            int state = MonitorServiceImpl.getInstance().getState();
            Log.printDebug("Kenneth : Monitor state  = "+state);
            if (state == MonitorService.TV_VIEWING_STATE || state == MonitorService.PIP_STATE) {
                //Kenneth[JIRA](VDTRMASTER-5319) : VOD 가 play 중이면 (help video 든 일반 비디오든) 팝업 띄우지 않는다
                String videoOwner = MonitorStateManager.getInstance().getCurrentVideoName();
                Log.printDebug("Kenneth : videoOwner = ["+videoOwner+"]");
                if ("VOD".equals(videoOwner)) {
                    return false;
                }
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //->Kenneth : 에러팝업이 떠 있는지 확인한 후에 알려준다.
    private static boolean isErrorPopupVisible() {
        LayeredUI[] arr = LayeredUIManager.getInstance().getAllLayeredUIs();
        if (arr == null) return false;
        for (int i = 0 ; i < arr.length ; i++) {
            if (arr[i].getName().equals("ErrorMessage")) {
                return arr[i].isActive();
            }
        }
        return false;
    }

    //->Kenneth : Help app 이 떠 있는지 확인해서 알려준다.
    public static boolean isHelpVisible() {
        UnboundAppProxy[] apps = UnboundAppsManager.getInstance().getAllUnboundAppProxies();
        if (apps == null) return false;
        for (int i = 0 ; i < apps.length ; i++) {
            if (apps[i].getName().equals("Help")) {
                DVBJProxy proxy = apps[i].getDVBJProxy();
                Log.printDebug("Kenneth : Help App State : "+proxy.getState());
                if (proxy.getState() == DVBJProxy.STARTED) {
                    return true;
                }
            }
        }
        return false;
    }

	/**
	 * Implements the UPP's RightFilterListener. It is received the state by
	 * user profile authorizer in the UPP. If result is
	 * {@link PreferenceService.RESPONSE_SUCCESS}, can launch the unbound
	 * application.
	 *
	 * @author Woosik Lee
	 */
	private class RightFilterListenerImpl implements RightFilterListener {
		/** the application will be launching. */
		private String applicationName;

		private String realapplicationName;

		/**
		 * the constructor.
		 */
		public RightFilterListenerImpl() {
		}

		/**
		 * @param appName
		 */
		public void setAppName(String appName, String realAppName) {
			applicationName = appName;
			realapplicationName = realAppName;
		}

/**
         * Implements the {@link RightFilterListener#receiveCheckRightFilter(int).
         * @param response the response from UPP.
         * @throws the exception.
         */
		public void receiveCheckRightFilter(int response) throws RemoteException {
			synchronized (MonitorKeyHandler.this) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "receiveCheckRightFilter() " + applicationName
							+ ", response=" + response);
				}
				checkingUPP = false;
				switch (response) {
				case PreferenceService.RESPONSE_SUCCESS:
					if (applicationName.equals(AppLauncher.SETTINGS_NAME)) {
						int keyCode = Integer.parseInt(realapplicationName);
						if (keyCode == KeyCodes.SETTINGS) {
							AppLauncher.getInstance().startUnboundApplication(AppLauncher.SETTINGS_NAME,
									APP_PARAMETERS_HOT_KEY);
						} else {
							if (Log.INFO_ON) {
								Log.printInfo(LOG_HEADER + "call setting directly by #");
							}
							CommunicationManager.getInstance().getPreferenceService().showSettingsMenu(APP_PARAMETERS_SETTING_ASPECT_SCENE);
						}
						return;
					}

					if (realapplicationName != null) {
						AppLauncher.getInstance().startUnboundApplication(realapplicationName, APP_PARAMETERS_HOT_KEY);
					} else {
						AppLauncher.getInstance().startUnboundApplication(applicationName, APP_PARAMETERS_HOT_KEY);
					}
					break;
				case PreferenceService.RESPONSE_ERROR:
					// TODO Error popup
					break;
				case PreferenceService.RESPONSE_FAIL:
					break;
				case PreferenceService.RESPONSE_CANCEL:
					break;
				}

			}
		}

/**
         * Implements the {@link RightFilterListener#getPinEnablerExplain().
         * @return null.
         * @throws the exception.
         */
		public String[] getPinEnablerExplain() throws RemoteException {
			return null;
		}
	}
}

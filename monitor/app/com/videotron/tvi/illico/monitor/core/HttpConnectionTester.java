// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/core/HttpConnectionTester.java,v 1.7 2017/01/09 21:12:13 zestyman Exp $

/*
 *  HttpConnectionTester.java	$Revision: 1.7 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.core;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.videotron.tvi.illico.log.Log;

public class HttpConnectionTester {

	private String urlString;

	/** File not found. */
	public static final int HTTP_NOT_FOUND = 1;
	public static final int CONNECTION_SUCCESS = 0;
	/** Server is down. */
	public static final int IOEXCEPTION = 2;

	private int result;

	private static final String LOG_HEADER = "[HttpConnectionTester] ";

	public HttpConnectionTester(String url) {
		this.urlString = url;
		connect();
	}

	public int getConnectionResult() {
		return result;
	}

	private void connect() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + " connect() url=" + urlString);
		}
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			int responseCode = connection.getResponseCode();
			if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " HTTP_NOT_FOUND occurs");
				}
				result = HTTP_NOT_FOUND;
			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " success");
				}
				result = CONNECTION_SUCCESS;
			}

		} catch (IOException e) {
			Log.printError(e);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + " IOException occurs");
			}
			result = IOEXCEPTION;
		} catch (Exception e) {
			Log.printError(e);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + " IOException occurs");
			}
			result = IOEXCEPTION;
		}
	}

}

// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/core/AodManager2.java,v 1.39 2017/01/09 21:12:13 zestyman Exp $

/*
 *  AodManager2.java	$Revision: 1.39 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.core;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.service.Service;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextFactory;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppProxy;
import org.dvb.application.AppStateChangeEvent;
import org.dvb.application.AppStateChangeEventListener;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;
import org.dvb.application.CurrentServiceFilter;
import org.dvb.application.DVBJProxy;
import org.ocap.application.AppManagerProxy;
import org.ocap.application.OcapAppAttributes;
import org.ocap.net.OcapLocator;
import org.ocap.service.AbstractService;
import org.ocap.service.AbstractServiceType;

import com.videotron.tvi.illico.ixc.monitor.AODEventListener;
import com.videotron.tvi.illico.ixc.monitor.AODRemoveListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.AodApplication;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;
import com.videotron.tvi.illico.monitor.data.MonitorConfigurationManager;

public class AodManager2 implements AppsDatabaseEventListener {

	public static final boolean AOD_SUPPORTED = false;

	private static AodManager2 instance;

	/** Key=requested AOD app name, value={@link AodRequestResource}.*/
	private Hashtable requestedAppTable;

	/** Key=remove AOD app name, value={@link AodRemoveResource}.*/
	private Hashtable removeAppTable;

	/** Key=Loaded AOD app name, value={@link UnboundAppProxy}. */
	private Hashtable aodList;

	/** Error log history for debug viwer. */
	private ArrayList errorLogs;

    private ArrayList removeListeners = null;

    private MonitorKeyBlocker keyBlocker;

	private static final String LOG_HEADER = "[AODM] ";

	private static final int AOD_TYPE_UNKNOWN = 0;
	private static final int AOD_TYPE_LOAD_NORMAL = 1;
	private static final int AOD_TYPE_LOAD_DAEMON = 2;
	private static final int AOD_TYPE_START_NORMAL = 3;
	private static final int AOD_TYPE_START_DAEMON = 4;

	public synchronized static AodManager2 getInstance() {
		if(instance == null) {
			instance = new AodManager2();
		}
		return instance;
	}

	private AodManager2() {
		requestedAppTable = new Hashtable();
		aodList = new Hashtable();
		removeAppTable = new Hashtable();
		errorLogs = new ArrayList();

		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
		appsDB.addListener(this);

		keyBlocker = new MonitorKeyBlocker("AODManager");
	}

	public Hashtable getLoadedAodList() {
		return aodList;
	}

	public Hashtable getLoadingAodList() {
		return requestedAppTable;
	}

	/**
	 * Gets the AOD application proxy.
	 * @param appName AOD name.
	 * @return
	 */
	public UnboundAppProxy getAodAppProxy(String appName) {
		if(!AodManager2.AOD_SUPPORTED) {
			return null;
		}
		UnboundAppProxy proxy = (UnboundAppProxy) aodList.get(appName);
		return proxy;
	}

	public void addAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) {
		if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "addAODApplicationRemoveListener : " + applicationName);
        }
        synchronized (removeListeners) {
            ListenerItem item = new ListenerItem(listener, applicationName);
            if (!removeListeners.contains(item)) {
            	removeListeners.add(item);
            } else {
                // Removes a previous listener.
            	removeListeners.remove(item);
                // Adds a new listener.
            	removeListeners.add(item);
            }
        }
	}

	public void removeAODApplicationRemoveListener(AODRemoveListener listener, String applicationName) {
		if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "removeAODApplicationRemoveListener : " + applicationName);
        }
		synchronized (removeListeners) {
            ListenerItem item = new ListenerItem(listener, applicationName);
            removeListeners.remove(item);
        }
	}

	public synchronized void closeAODApplication(String aodAppName, AODEventListener listener) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "]");
			printAodStatus();
		}
		UnboundAppProxy aodAppProxy = (UnboundAppProxy) aodList.get(aodAppName);
		if(aodAppProxy == null) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] not found in aodList");
			}

			notifyResultNewThread(aodAppName, listener, MonitorService.AOD_REQUEST_SUCCESS);
			return;
		}

		if(removeAppTable.containsKey(aodAppName)) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] already removing process is running, so ignore this request.");
			}
			return;
		}

		// Debugging ...
		// AOD Application state....
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] state = " + getAppStateName(aodAppProxy.getDVBJProxy().getState()));
		}

		// 1. 현재 어플이 실행 중이라면 종료 하고 진행 한다.
		TransitionEvent currentEvent = StateManager.getInstance().getCurrentEvent();
		if(currentEvent != null) {
			UnboundAppProxy currentProxy = currentEvent.getUnboundAppProxy();
			if(currentProxy != null) {
				if(aodAppName.equals(currentProxy.getName())) {
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] is running on the screen.");
					}
					// AOD 어플이 Running 중이기 때문에 여기서 종료 시키는데, AppState 결과 후에 종료 한다.
					// Add AppstateChangeListener to table
					AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(aodAppName, APP_STATE_REQUEST_CLOSE);
					DVBJProxy appProxy = aodAppProxy.getDVBJProxy();
					appProxy.addAppStateChangeEventListener(appStateListener);

					AodRemoveResource resource = new AodRemoveResource(listener, aodAppProxy, appStateListener);
					removeAppTable.put(aodAppName, resource);
					AppLauncher.getInstance().exitToChannel();

					return;
				}
			}
		}

		// 2. 현재 실행 중은 아니지만 SEMI-DAEMON 이라면 destroyXlet 까지 하고 진행 한다.
		if(aodAppProxy.getAppType() != UnboundAppProxy.NORMAL) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] is SEMI-DAEMON.");
			}
			// Add AppstateChangeListener to table
			AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(aodAppName, APP_STATE_REQUEST_CLOSE);
			DVBJProxy appProxy = aodAppProxy.getDVBJProxy();
			appProxy.addAppStateChangeEventListener(appStateListener);

			AodRemoveResource resource = new AodRemoveResource(listener, aodAppProxy, appStateListener);
			removeAppTable.put(aodAppName, resource);

			aodAppProxy.destroy();

			return;
		}

		// 3. 현재 실행 중도 아니고, NORMAL 이다. 바로 종료 한다.
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "] is NORMAL.");
		}
		AppID appId = aodAppProxy.getAppID();
		int abstractServiceId = getAbstractServiceId(appId);
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "closeAODApplication() [" + aodAppName + "], abstractServiceId=" + abstractServiceId);
		}

		AodRemoveResource resource = new AodRemoveResource(listener, aodAppProxy, null);
		removeAppTable.put(aodAppName, resource);
		// App 이 속한 abstract service id 를 찾아서 지운다.
		AppManagerProxy proxy = AppManagerProxy.getInstance();
		proxy.unregisterUnboundApp(abstractServiceId, appId);
		// notify 는 AppsDatabase listener call back 받고 한다.
	}

	public boolean isAODApplication(String appName) {
		AodApplication aodApplication = findAodApplicationInMS(appName);
		if(aodApplication == null) {
			return false;
		}
		return true;
	}

	public synchronized void startAODApplication(final String appName, final AODEventListener listener) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startAODApplication() [" + appName + "]");
			printAodStatus();
		}

		if(!checkAodConfiguration()) {
			int error = MonitorService.AOD_ERROR_CONFIGURATION_FILE;
			//if(EIDDataManager.getInstance().getIsAODParsingError() == true) {
			//	error = MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING;
			//}
			notifyResultNewThread(appName, listener, error);
			return;
		}

		final AodApplication aodApplication = findAodApplicationInMS(appName);

		if(aodApplication == null) {
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_APP_NAME_NOT_FOUND);
			return;
		}

		if(requestedAppTable.containsKey(appName) && !aodList.containsKey(appName)) {
			notifyResultNewThread(appName, listener, MonitorService.AOD_ALREADY_LOADDED);
			return;
		}


		if(aodList.containsKey(appName)) {
			// 이미 Loading 된 어플인 경우
			if(requestedAppTable.containsKey(appName)) {
				// 이미  startAODApplication() 에 의해서 재 호출 된 경우 이다.
				// startAODapplication() 이 처음 호출 되고 나서 registerComplete() 이 되면 AOD를 시작 시키기 위해서
				// 다시 여기가 호출 된다. 이경우에는 그냥 어플을 시작만 시키면 된다. Timer 나 Listener를 등록할 필요가 없다.
				UnboundAppProxy aodAppProxy = (UnboundAppProxy) aodList.get(appName);
				TransitionEvent appEvent = new TransitionEvent(TransitionEvent.TRANSITION, TransitionEvent.UNBOUND_APP,
                        appName, aodAppProxy, null);
                StateManager.getInstance().requestStateTransition(appEvent);
			}
			else {
				restartAODApplication(appName, listener, aodApplication);
			}

			return;
		}


		// 여기서 부터는 startAODApplication() 이 처음 호출 된 경우에 올 수 있다.
		// EDT 타고 들어오면 Loading 이 즉시 보이지 않기 때문에 새 Thread 로 돌린다.
		CommunicationManager.getInstance().startLoadingAnimation();
		keyBlocker.setBlock(true);

		Thread thread = new Thread() {
			public void run() {
				synchronized (AodManager2.this) {
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "[" + appName + "] try register!");
					}
					// Add timer resource to table
					TVTimerSpec timerSpec = new TVTimerSpec();
					TimeOutListener timeListener = new TimeOutListener(appName);
					// Add AppstateChangeListener to table
					AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(appName, APP_STATE_REQUEST_START);

					AodRequestResource resource = new AodRequestResource(timerSpec, timeListener, true, listener, appStateListener);

					requestedAppTable.put(appName, resource);

					// Time out
					startTimer(appName, resource);
					// Request
					boolean regResult = registerUnboundApp(aodApplication, listener);
					if(!regResult) {
						// 실패 하는 경우 Timer 정리 하고, requestedAppTable 도 비운다.
						stopTimer(appName, resource);
						requestedAppTable.remove(appName);
						CommunicationManager.getInstance().stopLoadingAnimation();
						keyBlocker.setBlock(false);
						return;
					}

					// 성공 하면... AppsDatabase 에 등록이 된다 !!!!!!!
					UnboundAppProxy unboundAppProxy = getAppProxyInAppsDB(appName);
					if(unboundAppProxy == null) {
						if(Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "[" + appName + "] appProxy is null. ERROR!");
						}
						// 이 상황이 벌어지면 안 되지만 혹시 몰라서 ....
						stopTimer(appName, resource);
						requestedAppTable.remove(appName);
						CommunicationManager.getInstance().stopLoadingAnimation();
						keyBlocker.setBlock(false);
						return;
					}

					// 등록이 끝났다. 이때 Storage Priority 가 0 이면 STC 로그를 남긴다.
					// 메일 ([VT_AOD] Storage Priority 테스트 - 11/06) 를 참조
					if (unboundAppProxy.getStoragePriority() == 0) {
						if (Log.ERROR_ON) {
							//STC Log.
							Log.printError("Storage priority is 0 so it will not be store [" + appName + "]");
						}
					}

					// App 등록은 완료 되었다! 시작 한다.
					aodList.put(appName, unboundAppProxy);

					AodAppStateChangeEventListener appListener = resource.getAodAppStateChangeEventListener();
					DVBJProxy appProxy = unboundAppProxy.getDVBJProxy();
					appProxy.addAppStateChangeEventListener(appListener);

					if(unboundAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
		        		// 1. NORMAL
		        		if(Log.INFO_ON) {
		        			Log.printInfo(LOG_HEADER + "[" + appName + "] 1. Start & Normal");
		        		}
		        		resource.setAodType(AOD_TYPE_START_NORMAL);
		        		// Normal 이기 때문에 State transition 하도록 한다.
		        		AppLauncher.getInstance().startUnboundApplication(appName, null);
		        	}
		        	else {
		        		// 2. SEMI-DAEMON
		        		if(Log.INFO_ON) {
		        			Log.printInfo(LOG_HEADER + "[" + appName + "] 1. Start & SEMI-DAEMON ");
		        		}
		        		resource.setAodType(AOD_TYPE_START_DAEMON);
		        		// initXlet() 이 완료된 이후에 {@link AodManager#appStateChange()} 에서 startXlet() 을 state transition 으로 처리 한다.
		        		unboundAppProxy.init();
		        	}

					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "[" + appName + "] END of register.");
					}
				}
			}
		};
		// IXC Thread 를 통해서 오기 때문에 EDT 여부를 판단 할 수가 없다. 무조건 Loading bar를 띄운다.
		thread.start();
	}

	/**
	 * 이미 로딩된 App 의 Update 확인.
	 * @param appName
	 * @param listener
	 * @param aodApplication
	 */
	private synchronized boolean reloadAodApplication(String appName, AODEventListener listener, AodApplication aodApplication) {
		// 업데이트 확인!
		UnboundAppProxy oldAodAppProxy = (UnboundAppProxy) aodList.get(appName);
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "[" + appName + "] reloadAodApplication() old version = " + oldAodAppProxy.getAppVersion() + ", type=" + oldAodAppProxy.getAppType());
		}
		boolean isOldAppProxyNORMAL = (oldAodAppProxy.getAppType() == UnboundAppProxy.NORMAL);
		int oldAodAppStoragePriority = oldAodAppProxy.getStoragePriority();
		boolean success = registerUnboundApp(aodApplication, null);
		if(!success) {
			return false;
		}
		// Normal App 이 업데이트 되었다면 여기서 새로운  AppProxy 가 등록 되어 있다.
		// Daemon App 은 업데이트가 되었다 하더라도 여기에서 AppProxy 가 등록 되지 않는다.
		// 어플이 완전 종료 되었을 경우에만 AppsDatabase 에 새 AppProxy 가 등록 되기 때문이다.

		UnboundAppProxy newAodAppProxy = getAppProxyInAppsDB(appName);
		if(newAodAppProxy == null) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "[" + appName + "] reloadAodApplicationappProxy() is null. ERROR!");
			}
			// 어플이 삭제된 경우 이렇게 될 수 있다. 에러 팝업을 보여주고 리턴 한다.
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE);
			return false;
		}

		// 등록이 끝났다. 이때 Storage Priority 가 0 이면 STC 로그를 남긴다.
		// 메일 ([VT_AOD] Storage Priority 테스트 - 11/06) 를 참조
		if (oldAodAppStoragePriority > 0 && newAodAppProxy.getStoragePriority() == 0) {
			if (Log.ERROR_ON) {
				//STC Log.
				Log.printError("Storage priority is 0 so it will not be store [" + appName + "]");
			}
		}

		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "[" + appName + "] check update end new version = " + newAodAppProxy.getAppVersion() + ", type=" + newAodAppProxy.getAppType());
		}

		if(isOldAppProxyNORMAL) {
			// NORMAL 은 업데이트 되었을 수도 있으니 새로운 AppProxy 로 교체 한다.
			// Daemon 은 AppsDatabase 가 업데이트 될 수가 없어서 업데이트 하지 않는다.
			aodList.put(appName, newAodAppProxy);
		}

		return true;
	}

	/**
	 * 이미 로딩 된 AOD App 을 실행 하는 경우.
	 * @param appName
	 * @param listener
	 * @param aodApplication
	 */
	private void restartAODApplication(final String appName, final AODEventListener listener, final AodApplication aodApplication) {

		// 이미 Loading 이 완료되고 한번 실행 되었다가 종료된 어플을 다시 시작 요청 하는 경우
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "[" + appName + "] is already loaded so start application with loading bar");
		}
		CommunicationManager.getInstance().startLoadingAnimation();
		keyBlocker.setBlock(true);

		Thread thread = new Thread() {
			public void run() {
				synchronized (AodManager2.this) {
					// 업데이트 확인!
					UnboundAppProxy oldAodAppProxy = (UnboundAppProxy) aodList.get(appName);
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "[" + appName + "] check update! version = " + oldAodAppProxy.getAppVersion() + ", type=" + oldAodAppProxy.getAppType());
					}
					boolean isOldAppProxyNORMAL = (oldAodAppProxy.getAppType() == UnboundAppProxy.NORMAL);
					int oldAodAppStoragePriority = oldAodAppProxy.getStoragePriority();

					boolean regSuccess = reloadAodApplication(appName, listener, aodApplication);
					if(!regSuccess) {
						if(Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "[" + appName + "] check update! Error! ignore this request.");
						}
						CommunicationManager.getInstance().stopLoadingAnimation();
						keyBlocker.setBlock(false);
						return;
					}

					UnboundAppProxy newAodAppProxy = (UnboundAppProxy) aodList.get(appName);
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "[" + appName + "] check update end new version = " + newAodAppProxy.getAppVersion() + ", type=" + newAodAppProxy.getAppType());
					}

					// 등록이 끝났다. 이때 Storage Priority 가 0 이면 STC 로그를 남긴다.
					// 메일 ([VT_AOD] Storage Priority 테스트 - 11/06) 를 참조
					if (oldAodAppStoragePriority > 0 && newAodAppProxy.getStoragePriority() == 0) {
						if (Log.ERROR_ON) {
							//STC Log.
							Log.printError("Storage priority is 0 so it will not be store [" + appName + "]");
						}
					}

					boolean isNewAppProxyDAEMON = (newAodAppProxy.getAppType() != UnboundAppProxy.NORMAL);

					// Add timer resource to table
					TVTimerSpec timerSpec = new TVTimerSpec();
					TimeOutListener timeListener = new TimeOutListener(appName);
					// Add AppstateChangeListener to table
					AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(
							appName, APP_STATE_REQUEST_START);

					DVBJProxy appProxy = newAodAppProxy.getDVBJProxy();
					appProxy.addAppStateChangeEventListener(appStateListener);

					AodRequestResource resource = new AodRequestResource(timerSpec, timeListener,
							appStateListener);
					if (newAodAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
						resource.setAodType(AOD_TYPE_START_NORMAL);
					} else {
						resource.setAodType(AOD_TYPE_START_DAEMON);
					}

					requestedAppTable.put(appName, resource);
					// Time out
					startTimer(appName, resource);

					// Loading 은 이미 되어 있으니 어플을 시작 하고, AppState listener
					// 를 통해서 뜨는 것 확인 하고 notify 하자.
					if(isOldAppProxyNORMAL && isNewAppProxyDAEMON) {
						if(Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "[" + appName + "] is updated NORMAL to DAEMON so call initXlet() ");
						}
						// NORMAL App 이 SEMI-DAEMON 으로 변경 된 케이스 이다. init() 을 먼저 호출 하고, AppStateChanged 에서 init 이 완료 되면
						// AppLauncher#start() 한다.
						newAodAppProxy.init();
					}
					else {
						TransitionEvent appEvent = new TransitionEvent(TransitionEvent.TRANSITION,
								TransitionEvent.UNBOUND_APP, appName, newAodAppProxy, null);
						StateManager.getInstance().requestStateTransition(appEvent);
					}
				}
			}
		};
		thread.start();

	}

	public synchronized void loadAODApplication(String appName, AODEventListener listener) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "loadAODApplication() [" + appName + "]");
			printAodStatus();
		}

		if(!checkAodConfiguration()) {
			int error = MonitorService.AOD_ERROR_CONFIGURATION_FILE;
			//if(EIDDataManager.getInstance().getIsAODParsingError() == true) {
			//	error = MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING;
			//}
			notifyResultNewThread(appName, listener, error);
			return;
		}

		AodApplication aodApplication = findAodApplicationInMS(appName);

		if(aodApplication == null) {
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_APP_NAME_NOT_FOUND);
			return;
		}

		if(requestedAppTable.containsKey(appName)) {
			notifyResultNewThread(appName, listener, MonitorService.AOD_ALREADY_LOADDED);
			return;
		}

		if(aodList.containsKey(appName)) {
			// 업데이트를 위해서 register 는 한번 해야 한다. 결과 callback 이 없을 수도 있기 때문에 Callback 하지 않도록 listener는 null 을 던진다.
			reloadAodApplication(appName, listener, aodApplication);
			return;
		}

		// Request
		boolean regResult = registerUnboundApp(aodApplication, listener);
		if(!regResult) {
			return;
		}

		// 성공 하면... AppsDatabase 에 등록이 된다 !!!!!!!
		UnboundAppProxy unboundAppProxy = getAppProxyInAppsDB(appName);
		if(unboundAppProxy == null) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "[" + appName + "] appProxy is null. ERROR!");
			}
			// 이 상황이 벌어지면 안 되지만 혹시 몰라서 ....
			return;
		}

		if (unboundAppProxy.getStoragePriority() == 0) {
			if (Log.ERROR_ON) {
				//STC Log.
				Log.printError("Storage priority is 0 so it will not be store [" + appName + "]");
			}
		}

		// App 등록 한다!
		aodList.put(appName, unboundAppProxy);

		if(unboundAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
    		// 1. NORMAL
    		if(Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "[" + appName + "] 1. Load & Normal");
    		}
    		//resource.setAodType(AOD_TYPE_LOAD_NORMAL);
    		//finishAodRequest(appName, resource);
    	}
    	else {
    		// 2. SEMI-DAEMON
    		if(Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "[" + appName + "] 1. Load & SEMI-DAEMON start initXlet()");
    		}

    		// Add timer resource to table
    		TVTimerSpec timerSpec = new TVTimerSpec();
    		TimeOutListener timeListener = new TimeOutListener(appName);

    		// Add AppstateChangeListener to table
    		AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(appName, APP_STATE_REQUEST_START);
    		AodRequestResource resource = new AodRequestResource(timerSpec, timeListener, false, listener, appStateListener);
    		requestedAppTable.put(appName, resource);

    		// Time out
    		startTimer(appName, resource);

    		DVBJProxy appProxy = unboundAppProxy.getDVBJProxy();
    		appProxy.addAppStateChangeEventListener(appStateListener);

    		resource.setAodType(AOD_TYPE_LOAD_DAEMON);
    		unboundAppProxy.init();

    		// Do you want to see next step? go to {@link AodManager2#appStateChange()}
    	}

		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "[" + appName + "] END of loadAODApplication");
		}

		/*

		// Add timer resource to table
		TVTimerSpec timerSpec = new TVTimerSpec();
		TimeOutListener timeListener = new TimeOutListener(appName);

		// Add AppstateChangeListener to table
		AodAppStateChangeEventListener appStateListener = new AodAppStateChangeEventListener(appName, APP_STATE_REQUEST_START);

		AodRequestResource resource = new AodRequestResource(timerSpec, timeListener, false, listener, appStateListener);

		requestedAppTable.put(appName, resource);

		// Time out
		startTimer(appName, resource);
		*/
	}

	/**
	 * AppsDatabase 에서 App Name 으로 AppProxy 를 찾는다.
	 * @param appName
	 * @return
	 */
	private UnboundAppProxy getAppProxyInAppsDB(String appName) {
		// 1. AppsDatabase 를 뒤져서 requestedAppTable 에 있는 AppName 과 같은게 있다면 AOD Request 이다.
		AppID foundAppId = null;

		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                AppAttributes attr = appsDB.getAppAttributes(appID);
            	String foundAppName = attr.getName();
            	if(appName.equals(foundAppName)) {
            		foundAppId = appID;
            		break;
            	}
            }
        }

        if(foundAppId == null) {
        	// AppsDatabase 에 없으므로 무시 한다.
        	if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "registerComplete() Error! Not found [" + appName + "] AOD in AppsDatabase");
    		}
        	return null;
        }

        DVBJProxy proxy = (DVBJProxy) appsDB.getAppProxy(foundAppId);
        AppAttributes attr = appsDB.getAppAttributes(foundAppId);
        int appVersion = getAppVersion(attr);
        OcapAppAttributes ocapApp = ((OcapAppAttributes) attr);
        int storagePriority = ocapApp.getStoragePriority();
    	UnboundAppProxy newAppProxy = new UnboundAppProxy(proxy, attr, appVersion, storagePriority, true);

    	return newAppProxy;

	}

	/**
	 * 사용하지 않음....
	 * It is called by AppsDatabaseChangeListener.
	 * @param appEvent
	 */
	private synchronized void registerComplete(AppsDatabaseEvent appEvent) {
		AppID newAppId = appEvent.getAppID();

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "registerComplete() : " + newAppId.toString());
			printAodStatus();
		}

		// 0. requestedAppTable 이 비었으면 아무것도 하지 말자.
		if(requestedAppTable.size() < 1) {
			return;
		}

		// 1. AppsDatabase 를 뒤져서 requestedAppTable 에 있는 AppName 과 같은게 있다면 AOD Request 이다.
		boolean findAppId = false;
		String foundAppName = null;

		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                if(appID.equals(newAppId)) {
                	if (Log.INFO_ON) {
            			Log.printInfo(LOG_HEADER + "registerComplete() find AOD application");
            		}
                	findAppId = true;
                    AppAttributes attr = appsDB.getAppAttributes(appID);
                	foundAppName = attr.getName();
                	break;
                }
            }
        }

        if(!findAppId) {
        	// AppsDatabase 에 없으므로 무시 한다.
        	if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "registerComplete() Error! Not found AOD in AppsDatabase");
    		}
        	return;
        }

        if(Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "registerComplete() Found AOD name : [" + foundAppName + "]");
        }

        AodRequestResource resource = (AodRequestResource) requestedAppTable.get(foundAppName);
        if(resource == null) {
        	// 새로 추가된 App 이 AOD Request 된 것이 아니다! 무시 한다.
        	if(Log.INFO_ON) {
            	Log.printInfo(LOG_HEADER + "Error! registerComplete()[" + foundAppName + "] Not found AOD name in requestedAppTable or maybe timeout occurs");
            }
        	return;
        }

		// 2. aodList 에 UnboundAppProxy 로 만들어서 저장 한다.
        if(Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "[" + foundAppName + "] Store AOD application to list (aodList)");
        }
        DVBJProxy proxy = (DVBJProxy) appsDB.getAppProxy(newAppId);
        AppAttributes attr = appsDB.getAppAttributes(newAppId);
        int appVersion = getAppVersion(attr);
        OcapAppAttributes ocapApp = ((OcapAppAttributes) attr);
        int storagePriority = ocapApp.getStoragePriority();
    	UnboundAppProxy newAppProxy = new UnboundAppProxy(proxy, attr, appVersion, storagePriority, true);
    	// 로딩이 끝났으면 AppList 에 넣는다.
    	aodList.put(foundAppName, newAppProxy);


        // 1. Load 요청에 Normal App : loading 끝나면 바로 notify 하고 끝
        // 2. Load 요청에 Semi-daemon App : loading 끝났으니 initXlet() 호출 하고 initXlet() 이 끝나면 notify 하고 끝
        // 3. Start 요청에 Normal App : loading 끝나면 기존 App (launcher) 를 종료 하고 initXlet(), startXlet() 호출 하고 startXlet() 호출이 끝나면 notify 함.
        // 4. Start 요청에 Smei-daemon App : loading 끝나면 기존 App (launcher) 를 종료 하고 initXlet(), startXlet() 호출 하고 startXlet() 호출이 끝나면 notify 함.
        // 5. Start 요청은 Loading Animation 도 종료 해야 함.

    	AodAppStateChangeEventListener appListener = resource.getAodAppStateChangeEventListener();
		DVBJProxy appProxy = newAppProxy.getDVBJProxy();
		appProxy.addAppStateChangeEventListener(appListener);

        if(resource.isStartRequest()) {
        	// Start 요청 임.
        	if(newAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
        		// 1. NORMAL
        		if(Log.INFO_ON) {
        			Log.printInfo(LOG_HEADER + "[" + foundAppName + "] 1. Start & Normal");
        		}
        		resource.setAodType(AOD_TYPE_START_NORMAL);
        		// Normal 이기 때문에 State transition 하도록 한다.
        		AppLauncher.getInstance().startUnboundApplication(foundAppName, null);
        	}
        	else {
        		// 2. SEMI-DAEMON
        		if(Log.INFO_ON) {
        			Log.printInfo(LOG_HEADER + "[" + foundAppName + "] 1. Start & SEMI-DAEMON ");
        		}
        		resource.setAodType(AOD_TYPE_START_DAEMON);
        		// initXlet() 이 완료된 이후에 {@link AodManager#appStateChange()} 에서 startXlet() 을 state transition 으로 처리 한다.
        		newAppProxy.init();
        	}
        }
        else {
        	// Load 요청 임.
        	if(newAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
        		// 1. NORMAL
        		if(Log.INFO_ON) {
        			Log.printInfo(LOG_HEADER + "[" + foundAppName + "] 1. Load & Normal");
        		}
        		resource.setAodType(AOD_TYPE_LOAD_NORMAL);
        		// 끝!
        		finishAodRequest(foundAppName, resource);
        	}
        	else {
        		// 2. SEMI-DAEMON
        		if(Log.INFO_ON) {
        			Log.printInfo(LOG_HEADER + "[" + foundAppName + "] 1. Load & SEMI-DAEMON start initXlet()");
        		}
        		resource.setAodType(AOD_TYPE_LOAD_DAEMON);
        		newAppProxy.init();

        		// Do you want to see next step? go to {@link AodManager2#appStateChange()}
        	}
        }

        if (Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "registerComplete [" + foundAppName + "] END");
		}
	}

	/**
	 * AOD app 을 close 하기 위해서 exitToService() 혹은 destroyXlet() 을 한 결과
	 * @param appName
	 * @param event
	 */
	private synchronized void closeAppStateChange(String appName, AppStateChangeEvent event) {
		int state = event.getToState();
		boolean hasFailed = event.hasFailed();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "closeAppStateChange : [" + appName + "], contain? " + removeAppTable.containsKey(appName));
			Log.printInfo(LOG_HEADER + "closeAppStateChange [" + appName + "] state=" + getAppStateName(state) + ", hasFailed=" + hasFailed);
		}

		AodRemoveResource resource = (AodRemoveResource) removeAppTable.get(appName);
		UnboundAppProxy aodAppProxy = (UnboundAppProxy) aodList.get(appName);
		if(resource == null || aodAppProxy == null) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Warnin! Not found [" + appName + "] at aodList (" + resource + "," + aodAppProxy + ")");
			}
			return;
		}
		AppID appId = aodAppProxy.getAppID();

		// Normal App
		if(aodAppProxy.getAppType() == UnboundAppProxy.NORMAL) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "closeAppStateChange : [" + appName + "] is NORMAL");
			}
			if(state == AppProxy.DESTROYED || state == AppProxy.NOT_LOADED) {
				// Normal App 의 종료가 완료 되었다.
				int abstractServiceId = getAbstractServiceId(appId);
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "closeAppStateChange() [" + appName + "], abstractServiceId=" + abstractServiceId);
				}
				// 아래와 같이 unregister 를 호출 하고.. entryRemoved 가 올라오면 리소스를 정리 한다.
				AppManagerProxy proxy = AppManagerProxy.getInstance();
				proxy.unregisterUnboundApp(abstractServiceId, appId);
			}
		}
		else {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "closeAppStateChange : [" + appName + "] is SEMI-DAEMON");
			}

			// Daemon App 도 Paused 만 되면 unregisterUnboundApp() 을 호출 하면 된다. - destroy 하고 entryRemoved 가 올라온다.

			if(state == AppProxy.PAUSED) {
				// StateManager 에 의해서는 PAUSE 만 시킬 수 있다.
				// paused 되었으면 destroy 한다.
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "closeAppStateChange : [" + appName + "] try to destroyXlet()");
				}
				aodAppProxy.destroy();
			}
			else if(state == AppProxy.DESTROYED || state == AppProxy.NOT_LOADED) {
				// Daemon App 의 종료가 완료 되었다.
				int abstractServiceId = getAbstractServiceId(appId);
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "closeAppStateChange() [" + appName + "], abstractServiceId=" + abstractServiceId);
				}
				// 아래와 같이 unregister 를 호출 하고.. entryRemoved 가 올라오면 리소스를 정리 한다.
				AppManagerProxy proxy = AppManagerProxy.getInstance();
				proxy.unregisterUnboundApp(abstractServiceId, appId);
			}
		}
	}

	/**
	 * AOD app 을 Start 한 결과
	 * @param appName
	 * @param event
	 */
	private synchronized void appStateChange(String appName, AppStateChangeEvent event) {

		int state = event.getToState();
		boolean hasFailed = event.hasFailed();

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "appStateChange : [" + appName + "], contain? " + requestedAppTable.containsKey(appName));
			Log.printInfo(LOG_HEADER + "appStateChange [" + appName + "] state=" + getAppStateName(state) + ", hasFailed=" + hasFailed);
		}

		AodRequestResource resource = (AodRequestResource) requestedAppTable.get(appName);
		if(resource == null) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "appStateChange [" + appName + "] Error occurs. Not found application request Maybe timeout....");
			}
			return;
		}

		int aodType = resource.getAodType();

		switch (aodType) {
		case AOD_TYPE_LOAD_NORMAL :
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "appStateChange : [" + appName + "] It is not reachable");
			}
			break;
		case AOD_TYPE_LOAD_DAEMON :
			// initXlet() 이 완료가 된 것이다. 마무리 하자. 끝!
			finishAodRequest(appName, resource);
			break;
		case AOD_TYPE_START_NORMAL :
			// startXlet() 이 완료 되었는지 확인 하고 마무리 한다. 이때 Loading bar 도 없애야 한다.
			if(state == AppProxy.STARTED) {
				finishAodRequest(appName, resource);
				CommunicationManager.getInstance().stopLoadingAnimation();
				keyBlocker.setBlock(false);
				if(hasFailed) {
					AppLauncher.getInstance().exitToChannel();
				}
			}
			break;
		case AOD_TYPE_START_DAEMON :
			// initXlet() 이 완료 되면 startXlet() 을 호출 한다. 왜냐면 daemon 이기 때문에 initXlet() 은 state transition 타지 않는다.
			// startXlet() 이 완료 되면 Loading bar 없애고 마무리 한다.
			if(state == AppProxy.PAUSED) {
				// initXlet() 이 종료 되었다.
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "appStateChange : [" + appName + "] finish initXlet() so start application");
				}
				AppLauncher.getInstance().startUnboundApplication(appName, null);
			}
			else if(state == AppProxy.STARTED) {
				// startXlet() 까지 종료가 되었다.
				finishAodRequest(appName, resource);
				CommunicationManager.getInstance().stopLoadingAnimation();
				keyBlocker.setBlock(false);
				if(hasFailed) {
					AppLauncher.getInstance().exitToChannel();
				}
			}
			break;
		}

		/*
		if(resource.isRestartRequest()) {
			// 이미 로딩 된 AOD를 다시 로딩 해서 성공한 경우!
			// 업데이트를 위해서 register 는 한번 해야 한다. 결과 callback 이 없을 수도 있기 때문에 Callback 하지 않도록 listener는 null 을 던진다.
			AodApplication aodApplication = findAodApplicationInMS(appName);
			registerUnboundApp(aodApplication, null);
		}
		*/
	}

	private synchronized void updateComplete(AppsDatabaseEvent appEvent) {
		// XAIT update 가 있는 경우에 호출 된다.
		// 여기서는 SEMI-DAEMON AOD 의 업데이트만 처리 한다. NORMAL 의 업데이트는 registerUnboundApp() 이 리턴 되면 처리가 된다.
		// SEMI-DAEMON 은 entryChanged() 가 호출이 되어도 AppsDatabse 의 AppProxy 는 이전 버전을 리턴한다. 즉 어플이 완전 종료
		// 되기 전까지는 새 버전의 AppProxy를 얻을 방법이 없다.
		// 여기서는  SEMI-DAEMON App 인 경우 AOD Launcher 에게 알리기 위한 작업을 한다.
		// AOD Launcher 는 업데이트를 인지 하고 closeAODApplication() --> startAODApplication() 으로 SEMI-DAEMON 을 업데이트 할 수 있다.
		AppID newAppId = appEvent.getAppID();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "updateComplete() : " + newAppId.toString());
			printAodStatus();
		}

		// 1. AppsDatabase 에서 업데이트 된 AppProxy 를 찾는다.
		String appName = null;
		boolean findAppId = false;
		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                if(appID.equals(newAppId)) {
                	findAppId = true;
                    AppAttributes attr = appsDB.getAppAttributes(appID);
                    appName = attr.getName();
                    if (Log.INFO_ON) {
            			Log.printInfo(LOG_HEADER + "updateComplete() found updated AOD application name is [" + appName + "]");
            		}
                	break;
                }
            }
        }

        if(findAppId == false) {
        	if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "updateComplete() not found updated AOD application name.");
    		}
        	return;
        }

        UnboundAppProxy appProxy = (UnboundAppProxy)aodList.get(appName);
        if(appProxy == null) {
        	if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "updateComplete() not found updated AOD application in aodList.");
    		}
        	return;
        }

        boolean isOldProxyIsNormalApp = (appProxy.getAppType() == UnboundAppProxy.NORMAL);
        if(isOldProxyIsNormalApp) {
        	if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "updateComplete() This is normal AOD so ignore.");
    		}
        	return;
        }

        // 기존 App PRoxy 가 DAEMON 인 경우
		// DAEMON 은 AOD Launcher 에게 업데이트를 알린다. AppProxy 를 변경 하지 않는다.
		notifyAppChanged(appName);


		/*
        // Normal 인 경우 즉시 aodList 에 new AppProxy 를 업데이트 한다.
        // Daemon 인 경우 aodList 에 new AppProxy 를 업데이트 할 수 없다. 왜냐면 NON_LOADED 가 아니므로 AppsDatabase 에는 여전히
        // 이전 버전의 App proxy 가 들어있다.

    	if(isOldProxyIsNormalApp) {
    		// 기존 AppProxy 가 NORMAL 인 경우.
            aodList.remove(appName);

            // 2. aodList 에 UnboundAppProxy 로 만들어서 저장 한다.
            if(Log.INFO_ON) {
            	Log.printInfo(LOG_HEADER + "[" + appName + "] Store AOD application to list (aodList) in updateComplete(), isOldProxyIsNormalApp=" + isOldProxyIsNormalApp);
            }
            DVBJProxy proxy = (DVBJProxy) appsDB.getAppProxy(newAppId);
            AppAttributes attr = appsDB.getAppAttributes(newAppId);
            int appVersion = getAppVersion(attr);
        	UnboundAppProxy newAppProxy = new UnboundAppProxy(proxy, attr, appVersion, true);
        	//boolean isNewProxyIsDaemonApp = (appProxy.getAppType() != UnboundAppProxy.NORMAL);
        	// 로딩이 끝났으면 AppList 에 넣는다.
        	aodList.put(appName, newAppProxy);
    	}
    	else {
    		// 기존 App PRoxy 가 DAEMON 인 경우
    		// DAEMON 은 AOD Launcher 에게 업데이트를 알린다. AppProxy 를 변경 하지 않는다.
    		notifyAppChanged(appName);
    	}
    	*/

    	// 3. 끝!
    	if (Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "updateComplete [" + appName + "] END");
        	printAodStatus();
		}
	}

	/**
	 * aodList 에서 appID 를 이용해서 App Name 을 찾는다. 없으면 NULL
	 */
	private String findAppNameByAppId(AppID newAppId) {
		Enumeration enumer = aodList.keys();
		while(enumer.hasMoreElements()) {
			String appName = (String) enumer.nextElement();
			UnboundAppProxy appProxy = (UnboundAppProxy) aodList.get(appName);
			AppID appId = appProxy.getAppID();
			if(newAppId.equals(appId)) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "findAppNameByAppId() : [" + appName + "] found AOD application in aodList");
				}
				return appName;
			}
		}
		return null;
	}

	private synchronized void appRemoved(AppsDatabaseEvent appEvent) {
		AppID newAppId = appEvent.getAppID();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "appRemoved() : " + newAppId.toString());
			printAodStatus();
		}

		// newAppId 를 이용해서 removeAppTable 의 Key 인 AppName 와 비교해서 찾아야 한다.
		String appName = findAppNameByAppId(newAppId);

		if(appName == null) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "appRemoved() : fail - Not found AppID in aodList");
			}
			return;
		}

		UnboundAppProxy appProxy = (UnboundAppProxy) aodList.get(appName);
		// removeAppTable 에 있는 AppID 라면 unregisterUnboundApp() 에 의해 지워진 것이다.
		AodRemoveResource resource = (AodRemoveResource) removeAppTable.get(appName);


		if(resource != null) {
			// closeAODApplication() 요청에 의해 삭제 될  AOD App 이다.
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "appRemoved() : [" + appName + "] found at removeAppTable !");
			}

			// AppState remove.
			AodAppStateChangeEventListener appStateListener = resource.getAodAppStateChangeEventListener();
			if(appStateListener != null) {
				DVBJProxy jProxy = appProxy.getDVBJProxy();
				jProxy.removeAppStateChangeEventListener(appStateListener);
			}

			AODEventListener listener = resource.getCallbackListener();
			notifyResultNewThread(appName, listener, MonitorService.AOD_REQUEST_SUCCESS);
			aodList.remove(appName);
			//
			removeAppTable.remove(appName);
		}
		else {
			// closeAODApplication() 이 아닌 어떤 이유로 사라진 어플이다.. Flash 용량이 모자라거나 하면 사라질 수 있다...
			// aodList 에 있는 것이라면 삭제 하고 notify 하자...	aodList 에 없는 것이라면... 그냥 냅둔다..
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "appRemoved() : [" + appName + "] not found at removeAppTable !");
			}
			notifyAppRemove(appName);
			aodList.remove(appName);
		}


		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "appRemoved() result : ");
			printAodStatus();
		}
	}


	/**
	 * 모든 것이 성공해서 마무리 한다.
	 * @param appName
	 * @param resource
	 */
	private synchronized void finishAodRequest(String appName, AodRequestResource resource) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "finishAodRequest : [" + appName + "] !!!!!!!!");
		}
		AODEventListener listener = resource.getCallbackListener();
		notifyResultNewThread(appName, listener, MonitorService.AOD_REQUEST_SUCCESS);
		// clear timer
		stopTimer(appName, resource);

		// clear app state listener
		AodAppStateChangeEventListener appstateListener = resource.getAodAppStateChangeEventListener();
		UnboundAppProxy appProxy = (UnboundAppProxy) aodList.get(appName);
		DVBJProxy jProxy = appProxy.getDVBJProxy();
		jProxy.removeAppStateChangeEventListener(appstateListener);

		// clear resource
		requestedAppTable.remove(appName);

		if(Log.INFO_ON) {
        	printAodStatus();
		}
	}


	private synchronized void timeoutOccurs(String appName) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "timeout occurs! : [" + appName + "], contain? " + requestedAppTable.containsKey(appName));
		}

		if(requestedAppTable.containsKey(appName)) {
			AodRequestResource resource = (AodRequestResource) requestedAppTable.get(appName);

			AODEventListener listener = resource.getCallbackListener();
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_TIME_OUT_DELAY_EXPIRED);

			stopTimer(appName, resource);

			// clear app state listener
			AodAppStateChangeEventListener appstateListener = resource.getAodAppStateChangeEventListener();
			if(aodList.containsKey(appName)) {
				UnboundAppProxy appProxy = (UnboundAppProxy) aodList.get(appName);
				if(appProxy != null) {
					if (Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "timeout occurs! : [" + appName + "] removeAppStateChangeEventListener()");
					}
					DVBJProxy jProxy = appProxy.getDVBJProxy();
					jProxy.removeAppStateChangeEventListener(appstateListener);
				}
			}

			if(resource.isStartRequest()) {
				CommunicationManager.getInstance().stopLoadingAnimation();
				keyBlocker.setBlock(false);
				AppLauncher.getInstance().exitToChannel();
			}

			requestedAppTable.remove(appName);
		}

		printAodStatus();
	}

	private void startTimer(String appName, AodRequestResource resource) {

		long timeOutDelay = (long)MonitorConfigurationManager.getInstance().getAodTimeoutDelay() * 1000L;

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startTimer() : [" + appName + "], timeout=" + timeOutDelay);
		}
		TVTimer timer = TVTimer.getTimer();

		TVTimerSpec timerSpec = resource.getTimerSpec();
		TimeOutListener timeListener = resource.getTimerListener();

		try {
            timerSpec.setDelayTime(timeOutDelay);
            timerSpec.setRepeat(false);
            timerSpec.addTVTimerWentOffListener(timeListener);
            timer.scheduleTimerSpec(timerSpec);
        } catch (TVTimerScheduleFailedException e) {
            Log.print(e);
        }
	}


	/**
	 * 타이머를 종료 시킨다.
	 * @param appName
	 * @param resource
	 */
	private void stopTimer(String appName, AodRequestResource resource) {

		if(Log.INFO_ON) {
        	Log.printInfo(LOG_HEADER + "[" + appName + "] stopTimer() appName=" + appName);
        }

    	TVTimerSpec timerSpec = resource.getTimerSpec();
		TimeOutListener timeListener = resource.getTimerListener();

		timerSpec.removeTVTimerWentOffListener(timeListener);
		TVTimer timer = TVTimer.getTimer();
        timer.deschedule(timerSpec);
	}

	private boolean registerUnboundApp(AodApplication aodApplication, AODEventListener listener) {

		String appName = aodApplication.getAppName();
		AppManagerProxy proxy = AppManagerProxy.getInstance();
		InputStream xait = null;
		InputStream xait2 = null;

		try {

			String ip = MonitorConfigurationManager.getInstance().getAodServerIP();
			String root = MonitorConfigurationManager.getInstance().getAodContextRoot();
			int port = MonitorConfigurationManager.getInstance().getAodServerPort();

			String appPath = aodApplication.getAppPath();

			String fullURL = "http://" + ip + ":" + port +  root + appPath + "/" + appName + ".xait";
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "[" + appName + "] registerUnboundApp() " + fullURL);
			}

			////////// XAIT Server down check ///////////////////
			HttpConnectionTester connTest = new HttpConnectionTester(fullURL);
			int connTestResult = connTest.getConnectionResult();
			switch (connTestResult) {
			case HttpConnectionTester.HTTP_NOT_FOUND :
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " Error HttpConnectionTester.HTTP_NOT_FOUND ");
				}
				notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_FILE_NOT_FOUND);
				return false;
			case HttpConnectionTester.IOEXCEPTION :
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " Error HttpConnectionTester.IOEXCEPTION  ");
				}
				notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE);
				return false;
			}
			/////////// End of checking XAIT server ///////////////////////
			URL url = new URL(fullURL);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			xait = conn.getInputStream();

			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "registerUnboundApplication() inputStream: " + xait);
			}
			proxy.registerUnboundApp(xait);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "registerUnboundApplication() finish registerUnboundApp()");
			}
			xait.close();
			/////////// check zip file /////////////////////////////////////////

			URL url2 = new URL(fullURL);
			URLConnection conn2 = url2.openConnection();
			conn2.setDoOutput(true);
			xait2 = conn2.getInputStream();

			XAITParser xaitParser = new XAITParser(xait2);
			String zipFileUrl = xaitParser.getApplicationUrl();
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "zip file url : " + zipFileUrl);
			}
			xait2.close();
			HttpConnectionTester zipConnTest = new HttpConnectionTester(zipFileUrl);
			int zipConnTestResult = zipConnTest.getConnectionResult();
			switch (zipConnTestResult) {
			case HttpConnectionTester.HTTP_NOT_FOUND :
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " Zip File Error HttpConnectionTester.HTTP_NOT_FOUND ");
				}
				notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_ZIP_FILE_NOT_FOUND);
				return false;
			case HttpConnectionTester.IOEXCEPTION :
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + " Zip File Error HttpConnectionTester.IOEXCEPTION  ");
				}
				notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_ZIP_FILE_SERVER_NOT_REACHABLE);
				return false;
			}
			////////// End of checking zip file /////////////////

		} catch (UnknownHostException e) {
			Log.print(e);
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE);
			return false;
		} catch (IOException e) {
			Log.print(e);
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE);
			return false;
		} catch (IllegalArgumentException e) {
			Log.print(e);
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_PARSING);
			return false;
		} catch (Exception e) {
			Log.print(e);
			notifyResultNewThread(appName, listener, MonitorService.AOD_ERROR_XAIT_FILE_NOT_FOUND);
			return false;
		} finally {
			if(xait != null) {
				try {
					xait.close();
				} catch (Exception e) {
					Log.printError(e);
				}
			}
			if(xait2 != null) {
				try {
					xait2.close();
				} catch (Exception e) {
					Log.printError(e);
				}
			}
		}

		return true;
	}



	private boolean checkAodConfiguration() {
		String serverIp = MonitorConfigurationManager.getInstance().getAodServerIP();
		if(serverIp == null) {
			return false;
		}
		return true;
	}

	private AodApplication findAodApplicationInMS(String name) {
		AodApplication[] aodAppList = EIDDataManager.getInstance().getAodApplicationList();
		if(aodAppList == null || aodAppList.length < 1) {
			return null;
		}

		for(int i=0; i<aodAppList.length; i++) {
			if(name.equalsIgnoreCase(aodAppList[i].getAppName())) {
				return aodAppList[i];
			}
		}
		return null;
	}


	/**
     * Implements the TVTimerWentOffListener.
     * @author Woosik Lee
     */
    private class TimeOutListener implements TVTimerWentOffListener {

    	private String appName;

    	public TimeOutListener(String app) {
    		appName = app;
    	}

        /**
         * This is timer call back.
         * @param event TVTimer event.
         */
        public void timerWentOff(TVTimerWentOffEvent event) {
        	timeoutOccurs(appName);
        }
    };


    private static final int APP_STATE_REQUEST_START = 1;
    private static final int APP_STATE_REQUEST_CLOSE = 2;

    private class AodAppStateChangeEventListener implements AppStateChangeEventListener {

    	private String appName = null;

    	private int requestType = APP_STATE_REQUEST_START;

    	public AodAppStateChangeEventListener(String requestAppName, int pRequestType) {
    		appName = requestAppName;
    		requestType = pRequestType;
    	}

    	/**
         * Implementation of AppStateChangeEventListener.
         * @param ev the instance of AppStateChangeEvent.
         */
        public void stateChange(AppStateChangeEvent ev) {
            int state = ev.getToState();
            boolean hasFailed = ev.hasFailed();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "AodAppStateChange [" + appName + "] requestType=" + getRequestTypeString() + ", state=" + getAppStateName(state) + ", hasFailed=" + hasFailed);
            }
            if(requestType == APP_STATE_REQUEST_START) {
            	appStateChange(appName, ev);
            }
            else if(requestType == APP_STATE_REQUEST_CLOSE) {
            	closeAppStateChange(appName, ev);
            }
        }

        private String getRequestTypeString() {
        	switch (requestType) {
        	case APP_STATE_REQUEST_START :
        		return "(START)";
        	case APP_STATE_REQUEST_CLOSE :
        		return "(CLOSE)";
        	}
        	return "(KNOWN)";
        }
    }


	/////////////////////////////////////////////
	// AppsDatabaseEventListener
	/////////////////////////////////////////////
	public void newDatabase(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "AppsDatabaseEventListener newDatabase() : " + getAppsDatabaseEventInfo(event));
		}
		//registerComplete(event);
	}

	public void entryAdded(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "AppsDatabaseEventListener entryAdded() : " + getAppsDatabaseEventInfo(event));
			// entryAdded 가 필요 없다! registerUnboundApp() 리턴 되면 AppDatabase 를 뒤지면 된다.
			//registerComplete(event);
		}
	}

	public void entryRemoved(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "AppsDatabaseEventListener entryRemoved() : " + getAppsDatabaseEventInfo(event));
		}
		// unregisterUnboundApp() 의 결과 혹은 용량 부족등의 이유로 Flash 에서 지워졌을 때 혹은 어떤 이유로 AM이 지운 경우이다.
		appRemoved(event);
	}

	public void entryChanged(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "AppsDatabaseEventListener entryChanged() : " + getAppsDatabaseEventInfo(event));
		}
		// XAIT update 가 있을 경우에 호출 된다.
		updateComplete(event);
	}

	/**
	 * Information for AppsDatabaseEvent.
	 * @param event AppsDatabaseEvent.
	 * @return Information message.
	 */
	private String getAppsDatabaseEventInfo(AppsDatabaseEvent event) {
		StringBuffer buffer = new StringBuffer("EventID=");
		switch(event.getEventId()) {
		case AppsDatabaseEvent.NEW_DATABASE:
			buffer.append("NEW_DATABASE");
			break;
		case AppsDatabaseEvent.APP_ADDED:
			buffer.append("APP_ADDED");
			break;
		case AppsDatabaseEvent.APP_CHANGED:
			buffer.append("APP_CHANGED");
			break;
		case AppsDatabaseEvent.APP_DELETED:
			buffer.append("APP_DELETED");
			break;
		}
		buffer.append(", AppID=" + event.getAppID());
		return buffer.toString();
	}

	private String makeErrorLog(int result, String appName) {
		String LOG = null;
		switch (result) {
		case MonitorService.AOD_ERROR_APP_NAME_NOT_FOUND :
			LOG = "AOD application name not found [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_CONFIGURATION_FILE :
			LOG = "Error AOD applications information not found [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING :
			LOG = "Error parsing AOD configuration file [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_STARTING_TIMER :
			LOG = "Error starting AOD timer [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_STOPPING_TIMER :
			LOG = "Error stopping AOD timer [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_TIME_OUT_DELAY_EXPIRED :
			LOG = "AOD loading/launching timeout delay has expired [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_UNABLE_TO_UNZIP_FILE :
			LOG = "Unable to unzip file [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_XAIT_FILE_NOT_FOUND :
			LOG = "XAIT file not found [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_XAIT_PARSING :
			LOG = "XAIT parsing error [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE :
			LOG = "XAIT Server not reachable [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_ZIP_FILE_NOT_FOUND :
			LOG = "AOD application ZIP file not found [" + appName + "]";
			break;
		case MonitorService.AOD_ERROR_ZIP_FILE_SERVER_NOT_REACHABLE :
			LOG = "Zip file server not reachable [" + appName + "]";
			break;
		}

		return LOG;
	}

	/**
     * Get date and time in form of yyyy/MM/dd/ HH:mm:ss.
     * @return yyyy/MM/dd/ HH:mm:ss
     */
    public String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd/ HH:mm:ss");
        return sdf.format(new Date());
    }

	private void writeStcLog(int result, String appName) {
		if(result == MonitorService.AOD_REQUEST_SUCCESS) {
			if(Log.DEBUG_ON) {
				Log.printDebug("AOD request Success [" + appName + "]");
			}
		}
		else {
			String LOG = makeErrorLog(result, appName);
			if(LOG != null) {
				// For debug viewer
				errorLogs.add(LOG + " | TIME : " + getDateTime());
			}
			if(Log.ERROR_ON) {
				if(LOG != null) {
					// STC Log.
					Log.printError(LOG);
				}
			}
		}
	}

	public ArrayList getErrorLogHistory() {
		return errorLogs;
	}

	public void notifyResultNewThread(final String appName, final AODEventListener listener, final int result) {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "notifyResultNewThread() [" + appName + "] result=" + result);
		}
		writeStcLog(result, appName);

		// Error message popup
		if(result != MonitorService.AOD_REQUEST_SUCCESS) {
			String errorCode = null;
			switch (result) {
			case MonitorService.AOD_ERROR_ZIP_FILE_NOT_FOUND :
				errorCode = CommunicationManager.ERROR_AOD_002;
				break;
			case MonitorService.AOD_ERROR_APP_NAME_NOT_FOUND :
				errorCode = CommunicationManager.ERROR_AOD_005;
				break;
			case MonitorService.AOD_ERROR_CONFIGURATION_FILE :
				errorCode = CommunicationManager.ERROR_AOD_005;
				break;
			case MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING :
				errorCode = CommunicationManager.ERROR_AOD_003;
				break;
			case MonitorService.AOD_ERROR_STARTING_TIMER :
				errorCode = CommunicationManager.ERROR_AOD_004;
				break;
			case MonitorService.AOD_ERROR_STOPPING_TIMER :
				errorCode = CommunicationManager.ERROR_AOD_004;
				break;
			case MonitorService.AOD_ERROR_TIME_OUT_DELAY_EXPIRED :
				errorCode = CommunicationManager.ERROR_AOD_004;
				break;
			case MonitorService.AOD_ERROR_UNABLE_TO_UNZIP_FILE :
				errorCode = CommunicationManager.ERROR_AOD_003;
				break;
			case MonitorService.AOD_ERROR_XAIT_FILE_NOT_FOUND :
				errorCode = CommunicationManager.ERROR_AOD_002;
				break;
			case MonitorService.AOD_ERROR_XAIT_PARSING :
				errorCode = CommunicationManager.ERROR_AOD_003;
				break;
			case MonitorService.AOD_ERROR_ZIP_FILE_SERVER_NOT_REACHABLE :
			case MonitorService.AOD_ERROR_XAIT_SERVER_NOT_REACHABLE :
				errorCode = CommunicationManager.ERROR_AOD_001;
				break;
			case MonitorService.AOD_ALREADY_LOADDED :
				break;
			}

			if(errorCode != null) {
				CommunicationManager.getInstance().showErrorMessage(errorCode, null);
			}
		}

		if(listener == null)
		{
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "notifyResultNewThread() listener is null so do not notify result.");
			}
			return;
		}

		Thread thread = new Thread() {
			public void run() {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "notifyResultNewThread() [" + appName + "] result=" + result + ", to listener=" + listener);
				}
				try {
					listener.notifyAodEvent(result);
				} catch (Exception e) {
					Log.print(e);
				}
			}
		};
		thread.start();
	}

	private void notifyAppRemove(final String appName) {
		if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "notifyAppRemove [" + appName + "]");
        }
		Thread thread = new Thread() {
			public void run() {

				Object[] lsnrs;
		        synchronized (removeListeners) {
		            lsnrs = removeListeners.toArray();
		        }
		        for (int i = 0; i < lsnrs.length; i++) {
		            try {
		                if (Log.INFO_ON) {
		                    Log.printInfo(LOG_HEADER + "notifyAppRemove to "
		                            + ((ListenerItem) lsnrs[i]).listener);
		                }
		                ((ListenerItem) lsnrs[i]).listener.notifyAODRemoved(appName);
		            } catch (Throwable t) {
		                Log.print(t);
		            }
		        }

			}
		};
		thread.start();
	}

	private void notifyAppChanged(final String appName) {
		if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "notifyAppChanged [" + appName + "]");
        }
		Thread thread = new Thread() {
			public void run() {

				Object[] lsnrs;
		        synchronized (removeListeners) {
		            lsnrs = removeListeners.toArray();
		        }
		        for (int i = 0; i < lsnrs.length; i++) {
		            try {
		                if (Log.INFO_ON) {
		                    Log.printInfo(LOG_HEADER + "notifyAODChanged to "
		                            + ((ListenerItem) lsnrs[i]).listener);
		                }
		                ((ListenerItem) lsnrs[i]).listener.notifyAODChanged(appName);
		            } catch (Throwable t) {
		                Log.print(t);
		            }
		        }

			}
		};
		thread.start();

	}

	private void printAodStatus() {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "================= AOD Status ==================");
			Log.printInfo(LOG_HEADER + "======= Now loading application ===============");
			Enumeration keys = requestedAppTable.keys();
			while(keys.hasMoreElements()) {
				String key = (String)keys.nextElement();
				AodRequestResource resource = (AodRequestResource) requestedAppTable.get(key);
				Log.printInfo(LOG_HEADER + "AOD Name (Loading) : " + key + ", R=" + resource.toString());
			}
			Log.printInfo(LOG_HEADER + "======= Now loading application End ============");

			Log.printInfo(LOG_HEADER + "======= Loaded applications ====================");
			keys = aodList.keys();
			while(keys.hasMoreElements()) {
				String key = (String)keys.nextElement();
				UnboundAppProxy proxy = (UnboundAppProxy) aodList.get(key);
				Log.printInfo(LOG_HEADER + "AOD Name (Loaded) : " + key + ":" + proxy.toString());
			}
			Log.printInfo(LOG_HEADER + "======= Loaded applications End ================");

			Log.printInfo(LOG_HEADER + "======= To be remove applications ====================");
			keys = removeAppTable.keys();
			while(keys.hasMoreElements()) {
				String appName = (String) keys.nextElement();
				//AppID appId = aodAppProxy.getAppID();
				//AodRemoveResource resource = (AodRemoveResource) removeAppTable.get(aodAppProxy);
				//String appName = aodAppProxy.getName();
				Log.printInfo(LOG_HEADER + "AOD Name (Removing) : " + appName + ":");
			}
			Log.printInfo(LOG_HEADER + "======= To be remove applications End ================");
		}
	}

	private String getAppStateName(int appState) {
		switch(appState) {
		case AppProxy.DESTROYED :
			return "{DESTORYED}";
		case AppProxy.NOT_LOADED :
			return "{NOT_LOADED}";
		case AppProxy.PAUSED :
			return "{PAUSED}";
		case AppProxy.STARTED :
			return "{STARTED}";
		}
		return "UNKNOWN";
	}

	private String getAodTypeName(int type) {
		switch (type) {
		case AOD_TYPE_LOAD_NORMAL:
			return "LOAD/NORMAL";
		case AOD_TYPE_LOAD_DAEMON:
			return "LOAD/DAEMON";
		case AOD_TYPE_START_NORMAL:
			return "START/NORMAL";
		case AOD_TYPE_START_DAEMON:
			return "START/DAEMON";
		case AOD_TYPE_UNKNOWN:
			return "UNKNOWN";
		}
		return "NULL";
	}

	private class AodRemoveResource {
		private AODEventListener callbackListener;
		private UnboundAppProxy appProxy;
		private AodAppStateChangeEventListener appStateListener;

		public AodRemoveResource(AODEventListener callbackListener, UnboundAppProxy appProxy, AodAppStateChangeEventListener appStateListener) {
			super();
			this.callbackListener = callbackListener;
			this.appProxy = appProxy;
			this.appStateListener = appStateListener;
		}

		public AODEventListener getCallbackListener() {
			return callbackListener;
		}

		public UnboundAppProxy getUnboundAppProxy() {
			return appProxy;
		}

		public AodAppStateChangeEventListener getAodAppStateChangeEventListener() {
			return appStateListener;
		}
	}

	private class AodRequestResource {
		private TVTimerSpec timerSpec;
		private TimeOutListener timerListener;
		// true:start request, false : load request
		private boolean startRequest;

		private AODEventListener callbackListener;
		private AodAppStateChangeEventListener appStateListener;

		/** 이미 AppsDatabase 에 등록 된 App에 대해 다시 startAODApplication() 이 호출 된 경우  TRUE 임. */
		private boolean restartRequest;

		/**
		 * l
		 */
		private int aodType = AOD_TYPE_UNKNOWN;

		/**
		 * Constructor for start, load AOD application.
		 * @param timerSpec
		 * @param timerListener
		 * @param startRequest
		 * @param callbackListener
		 * @param appStateListener
		 */
		public AodRequestResource(TVTimerSpec timerSpec, TimeOutListener timerListener, boolean startRequest,
				AODEventListener callbackListener, AodAppStateChangeEventListener appStateListener) {
			super();
			this.timerSpec = timerSpec;
			this.timerListener = timerListener;
			this.startRequest = startRequest;
			this.callbackListener = callbackListener;
			this.appStateListener = appStateListener;
			restartRequest = false;
		}

		/**
		 * Constructor for restarting loaded AOD application only.
		 * @param timerSpec
		 * @param timerListener
		 * @param appStateListener
		 */
		public AodRequestResource(TVTimerSpec timerSpec, TimeOutListener timerListener,
				AodAppStateChangeEventListener appStateListener) {
			this.timerSpec = timerSpec;
			this.timerListener = timerListener;
			this.appStateListener = appStateListener;
			startRequest = true;
			restartRequest = true;
		}

		public String toString() {
			return "startRequest=" + startRequest + ", aodType=" + getAodTypeName(aodType) + ", restart? : " + restartRequest;
		}

		public boolean isRestartRequest() {
			return restartRequest;
		}

		public TVTimerSpec getTimerSpec() {
			return timerSpec;
		}

		public TimeOutListener getTimerListener() {
			return timerListener;
		}

		public boolean isStartRequest() {
			return startRequest;
		}

		public AODEventListener getCallbackListener() {
			return callbackListener;
		}

		public AodAppStateChangeEventListener getAodAppStateChangeEventListener() {
			return appStateListener;
		}

		public void setAodType(int type) {
			this.aodType = type;
		}

		public int getAodType() {
			return aodType;
		}


	}


	private int getAppVersion(AppAttributes attr) {
        try {
            Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
            Method method = cls.getMethod("getVersion", null);
            if(!method.isAccessible()) {
            	method.setAccessible(true);
            }
            Object obj = method.invoke(attr, null);
            Integer integer = (Integer)obj;
            return integer.intValue();

            /*
            // It is debugging code.
            Method method0 = cls.getMethod("getStoragePriority", null);
            if(!method0.isAccessible()) {
            	method0.setAccessible(true);
            }
            Object obj0 = method0.invoke(attr, null);
            Integer integer0 = (Integer)obj0;
            int storagePriority = integer0.intValue();
            if (Log.INFO_ON) {
    			Log.printInfo(LOG_HEADER + "storagePriority() " + storagePriority);
    		}
            result[1] = storagePriority;
            */
        } catch (Exception e) {
        	e.printStackTrace();

        }
        return -1;
	}

	/**
	 * Returns OOB Abstract service id.
	 * @return
	 */
	private int getAbstractServiceId(AppID appId) {

		ServiceContext[] serviceContexts = ServiceContextFactory.getInstance().getServiceContexts();
		int serviceId = 0;
		if(serviceContexts != null) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "getAbstractServiceId() appId=" + appId.toString() + ", serviceContexts.length = " + serviceContexts.length);
			}
			for(int i=0; i<serviceContexts.length; i++) {
				if(serviceContexts[i] != null) {
					Service service = serviceContexts[i].getService();
					if(service != null) {
						if(Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "], type=" + service.getServiceType() + ", AbstractServiceType=" + AbstractServiceType.OCAP_ABSTRACT_SERVICE);
						}

						try {

							if(service.getServiceType() == AbstractServiceType.OCAP_ABSTRACT_SERVICE) {
								if(Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "] this service is OCAP_ABSTRACT_SERVICE");
								}

								Enumeration en = ((AbstractService)service).getAppIDs();
								while(en.hasMoreElements()) {
									AppID tempAppId = (AppID) en.nextElement();
									if(Log.INFO_ON) {
										Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "] AppId=" + tempAppId.toString());
									}
									if(appId.equals(tempAppId)) {
										// Found Abstract Service
										// Abstract service id is source id of Locator
										// See OCAP Spec. Table 16–2 - OCAP URI Universally Resolvable Constructs
										// The source_id parameter matches either the source_id of a program
										// stream or the service_id of an abstract service.
										OcapLocator ocapLocator = (OcapLocator) service.getLocator();
										serviceId = ocapLocator.getSourceID();
										if(Log.INFO_ON) {
											Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "] found service id=" + serviceId);
										}
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return serviceId;

		/*

		PODExtendedChannel podC = PODExtendedChannel.getInstance();
		org.davic.mpeg.Service[] services = podC.retrieveServices();
		int serviceId = 0;

		for(int i=0; i<services.length; i++) {
			if(services[i] instanceof AbstractService) {
				serviceId = services[i].getServiceId();
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "] AbstractService id=" + serviceId);
				}
			}
			else {
				serviceId = services[i].getServiceId();
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "getAbstractServiceId() [" + i + "] Service id=" + serviceId);
				}
			}
		}

		return serviceId;
		*/
	}


	/**
     * This class has the unique application id and the instance of ServiceStateListener.
     * @author Woosik Lee
     */
    private class ListenerItem {
        /** The instance of listener. */
        private AODRemoveListener listener;
        /** The requested application name. */
        private String applicationName;

        /**
         * Constructor.
         * @param l listener.
         * @param appName application name.
         */
        public ListenerItem(final AODRemoveListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItem) {
                String appName = ((ListenerItem) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }
}


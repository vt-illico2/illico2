package com.videotron.tvi.illico.monitor.core;

public interface InternalInbandDataListener {

    public void finishReceivingData();
    
}

package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.rmi.RemoteException;
import java.util.Vector;

import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.ixc.epg.EpgService;
import com.videotron.tvi.illico.ixc.epg.VideoController;
import com.videotron.tvi.illico.ixc.mainmenu.MainMenuService;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.ui.popup.BasicDialog;
import com.videotron.tvi.illico.monitor.ui.popup.BasicPopup;
import com.videotron.tvi.illico.util.WindowProperty;

/**
 * Manages the {@link TransitionEvent} for service state transition or STB state
 * transition.
 * <P>
 * The transition event is operated by single thread. Single thread never die
 * while the Monitor application alive.
 * <P>
 *
 * @author Woosik Lee
 */
public final class StateManager {

	/** The event queue for {@link TransitionEvent}. */
	private Vector eventQ;
	/** The previous event. */
	private TransitionEvent prevEvt;
	/** The single thread state. */
	private boolean running = true;
	/** The single thread. */
	private StateManagerThread mainThread;
	/** The singleton instance. */
	private static StateManager instance = null;

	private VideoController videoController = null;

	private boolean nextEventStartOverlappingApplicationOnSpecialVideo;
	private boolean nextEventStopOverlappingApplicationOnSpecialVideo;

	private MonitorKeyBlocker keyBlocker;

	/** The log header message. */
	private static final String LOG_HEADER = "[StateManager] ";

	/**
	 * Constructor.
	 */
	private StateManager() {
		init();
	}

	/**
	 * Gets the singleton instance.
	 *
	 * @return instance of StateManager.
	 */
	public static synchronized StateManager getInstance() {
		if (instance == null) {
			instance = new StateManager();
		}
		return instance;
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		running = false;
		try {
			mainThread.interrupt();
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * Initializes the class.
	 */
	private void init() {
		keyBlocker = new MonitorKeyBlocker("StateManager");
		eventQ = new Vector();
		prevEvt = null;
		mainThread = new StateManagerThread();
		mainThread.start();
	}

	/**
	 * Starts the class.
	 */
	public void start() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "start");
		}
	}

	/**
	 * Requests the new state transition.
	 *
	 * @param event
	 *            new event.
	 */
	public void requestStateTransition(TransitionEvent event) {
		synchronized (eventQ) {
			eventQ.addElement(event);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "request state transition");
			}
			eventQ.notifyAll();
		}
	}

	/**
	 * Gets the last executed event.
	 *
	 * @return the last event.
	 */
	public TransitionEvent getCurrentEvent() {
		return prevEvt;
	}

	/**
	 * Stops the previous executed application or state.
	 *
	 * @param event
	 *            new event.
	 */
	private void stopPreviousApplication(TransitionEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "stopPreviousApplication() event=" + event);
		}
		if (prevEvt == null) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "prevEvt is null");
			}
			return;
		}

		if(nextEventStopOverlappingApplicationOnSpecialVideo) {
			// This is stop overlapping event so do not stop prevEvt.
			// Just do stop overlapping application and maximize video.
			MonitorStateManager.getInstance().stopOverlappingApplication(false);
            if (event.getType() != TransitionEvent.STANDBY) {
                // need to stop prevEvent when the STB goes to standby mode.
			    return;
            }
		}

		if (nextEventStartOverlappingApplicationOnSpecialVideo) {
			// This event is overlapping application so do not stop prevEvt.
			// And maximize video if next event is TV_VIEWING_STATE
			boolean maximizeVideo = false;
            //->Kenneth[2015.7.8] Tank
			if(event.getAppType() == TransitionEvent.MAIN_MENU) {
			//if(event.getAppType() == TransitionEvent.MAIN_MENU && event.getMenuState() != MainMenuService.MAIN_MENU_STATE_FULL_MENU) {
            //<-
				maximizeVideo = true;
			}
			if(maximizeVideo) {
				try {
					if (videoController == null) {
						videoController = CommunicationManager.getInstance().getEpgService().getVideoController();
					}
					videoController.maximize();
				} catch (Exception e) {
					Log.print(e);
				}
			}
			return;
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "stopPreviousApplication prevEvent=" + prevEvt);
		}
        if (prevEvt != null) {
            switch (prevEvt.getAppType()) {
            case TransitionEvent.GUIDE:
            case TransitionEvent.PIP:
                try {
                    CommunicationManager.getInstance().getEpgService().changeState(EpgService.STATE_NONE);
                } catch (RemoteException e) {
                    Log.print(e);
                }
                break;
            case TransitionEvent.MAIN_MENU:
                if (event.getAppType() != TransitionEvent.MAIN_MENU) {
                    try {
                        CommunicationManager.getInstance().getMainMenuService().hideMenu();
                    } catch (RemoteException e) {
                        Log.print(e);
                    }
                }
                break;
            case TransitionEvent.SETTINGS:
                try {
                    CommunicationManager.getInstance().getPreferenceService().hideSettingsMenu();
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
                break;
            case TransitionEvent.ISA:
                try {
                    CommunicationManager.getInstance().getIsaService().hideSubscriptionPopup();
                } catch (RemoteException ex) {
                    Log.print(ex);
                }
                break;
            default:
                if (prevEvt.getUnboundAppProxy() != null) {
                    prevEvt.getUnboundAppProxy().stop();
                }
                break;
            }
        }

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "Maximize the video screen");
		}
		try {
			if (videoController == null) {
				videoController = CommunicationManager.getInstance().getEpgService().getVideoController();
			}
			videoController.maximize();
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * Starts the new application or state.
	 *
	 * @param event
	 *            new event.
	 */
	private void startNewApplication(TransitionEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startNewApplication event=" + event);
		}

		switch (event.getAppType()) {
		case TransitionEvent.STATE_TV_VIEWING:
			ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
			if (nextEventStopOverlappingApplicationOnSpecialVideo) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER
							+ "nextEventStopOverlappingApplicationOnSpecialVideo is true so do not anything.");
				}
				return;
			}
			try {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "request changing state to STATE_TV_VIEWING. menu state="
							+ event.getMenuState() + ", sourceId=" + event.getSourceId());
				}
				CommunicationManager.getInstance().getMainMenuService().showMenu(event.getMenuState(),
						event.getParameter());
				int sourceId = event.getSourceId();
				CommunicationManager.getInstance().getEpgService().changeChannel(sourceId);
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.STATE_TV_VIEWING_WITHOUT_MENU:
			ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
			try {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "request changing state to STATE_TV_VIEWING_WITHOUT_MENU. sourceId="
							+ event.getSourceId());
				}
				int sourceId = event.getSourceId();
				CommunicationManager.getInstance().getEpgService().changeChannel(sourceId);
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.STATE_TV_VIEWING_WITHOUT_MENU_AND_AV:
			ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "request changing state to STATE_TV_VIEWING_WITHOUT_MENU_AND_AV. ");
			}
			stopChannel();
			break;
		case TransitionEvent.STATE_FULL_SCREEN_APP_WITHOUT_APPLICATION:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "request changing state to STATE_FULL_SCREEN_APP_WITHOUT_APPLICATION. ");
			}
			// This event is used by Inband data receiving module only.
			// Inband data receiving module conducts tuning to data channel so need not stop channel.
			//stopChannel();
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
			break;
		case TransitionEvent.MAIN_MENU:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Main Menu target state=" + event.getMenuState() + ", sourceId="
						+ event.getSourceId());
			}
			try {
                //->Kenneth[2015.7.8] Tank : MAIN_MENU_STATE_FULL_MENU 는 더이상 존재하지 않음.
                ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);

                if (MonitorStateManager.getInstance().isSpecialVideoState()) {
                    switch (event.getMenuState()) {
                    case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
                        MonitorStateManager.getInstance().startOverlappingApplication(
                                MonitorStateManager.OVERLAPPING_APP_MINI_MENU);
                        break;
                    case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
                        MonitorStateManager.getInstance().startOverlappingApplication(
                                MonitorStateManager.OVERLAPPING_APP_DASHBOARD);
                        break;
                    }
                } else {
                    int sourceId = event.getSourceId();
                    CommunicationManager.getInstance().getEpgService().changeChannel(sourceId);
                }
                /*
				if (event.getMenuState() == MainMenuService.MAIN_MENU_STATE_FULL_MENU) {
					ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
					if(MonitorStateManager.getInstance().isSpecialVideoState()) {
						MonitorStateManager.getInstance().startOverlappingApplication(
								MonitorStateManager.OVERLAPPING_APP_FULL_MENU);
					}
				} else {
					ServiceStateManager.getInstance().setState(MonitorService.TV_VIEWING_STATE);

					if (MonitorStateManager.getInstance().isSpecialVideoState()) {
						switch (event.getMenuState()) {
						case MainMenuService.MAIN_MENU_STATE_MINI_MENU:
							MonitorStateManager.getInstance().startOverlappingApplication(
									MonitorStateManager.OVERLAPPING_APP_MINI_MENU);
							break;
						case MainMenuService.MAIN_MENU_STATE_DASHBOARD:
							MonitorStateManager.getInstance().startOverlappingApplication(
									MonitorStateManager.OVERLAPPING_APP_DASHBOARD);
							break;
						}
					} else {
						int sourceId = event.getSourceId();
						CommunicationManager.getInstance().getEpgService().changeChannel(sourceId);
					}
				}
                */
                //<-

				CommunicationManager.getInstance().getMainMenuService().showMenu(event.getMenuState(),
						event.getParameter());
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.GUIDE:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start Guide");
			}
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
			if (MonitorStateManager.getInstance().isSpecialVideoState()) {
				MonitorStateManager.getInstance().startOverlappingApplication(MonitorStateManager.OVERLAPPING_APP_EPG);
			}
			try {
				CommunicationManager.getInstance().getEpgService().changeState(EpgService.STATE_GUIDE);
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.PIP:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start PIP");
			}
			ServiceStateManager.getInstance().setState(MonitorService.PIP_STATE);
			if (MonitorStateManager.getInstance().isSpecialVideoState()) {
				MonitorStateManager.getInstance().startOverlappingApplication(MonitorStateManager.OVERLAPPING_APP_PIP);
			}

			try {
				if (!MonitorStateManager.getInstance().isSpecialVideoState()) {
					CommunicationManager.getInstance().getEpgService().changeChannel(-1);
				}
				CommunicationManager.getInstance().getEpgService().changeState(EpgService.STATE_PIP);
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.SETTINGS:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start SETTINGS");
			}
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
			stopChannel();
			try {
				CommunicationManager.getInstance().getPreferenceService().showSettingsMenu(event.getParameter());
			} catch (RemoteException e) {
				Log.print(e);
			}
			break;
		case TransitionEvent.PVR:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start PVR - Does not stop channel! and special state is " + nextEventStartOverlappingApplicationOnSpecialVideo);
			}

			if(nextEventStartOverlappingApplicationOnSpecialVideo) {
				// PVR Play 중에 PVR 요청이 들어온 것임. PVR Main 으로 감.
				ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
				MonitorStateManager.getInstance().startOverlappingApplication(
						MonitorStateManager.OVERLAPPING_APP_PVR_MAIN);
				CommunicationManager.getInstance().startPvrMainMenu();
			}
			else {
				ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
				event.getUnboundAppProxy().start(null);
			}
			break;
			/*
		case TransitionEvent.VOD:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start VOD special state is " + nextEventStartOverlappingApplicationOnSpecialVideo);
			}

			if(nextEventStartOverlappingApplicationOnSpecialVideo) {
				ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
				MonitorStateManager.getInstance().startOverlappingApplication(
						MonitorStateManager.OVERLAPPING_APP_VOD_MAIN);
				CommunicationManager.getInstance().startVodMainMenu();
			}
			else {
				ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
				event.getUnboundAppProxy().start(null);
			}
			*/
		case TransitionEvent.ISA:
            String[] params = event.getParameter();
            if (params != null && params.length >= 3) {
                try {
                    CommunicationManager.getInstance().getIsaService().showSubscriptionPopup(params[1], params[2]);
                } catch (RemoteException e) {
                    Log.print(e);
                }
            }
            break;
		default:
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "start " + event.getApplicationName());
			}
			stopChannel();
			ServiceStateManager.getInstance().setState(MonitorService.FULL_SCREEN_APP_STATE);
			event.getUnboundAppProxy().start(null);
			break;
		}

	}

	/**
	 * Stops the channel service.
	 */
	private void stopChannel() {
		try {
			CommunicationManager.getInstance().getEpgService().stopChannel();
		} catch (Exception e) {
			Log.print(e);
		}
	}

	/**
	 * Checks the validation of requested event.
	 *
	 * @param event
	 *            the new event.
	 * @return if this event is valid return true, otherwise return false.
	 */
	private boolean checkIfValid(TransitionEvent event) {
		if (prevEvt == null) {
			return true;
		}

		switch (event.getAppType()) {
		case TransitionEvent.STATE_TV_VIEWING:
			return true;
		case TransitionEvent.GUIDE:
		case TransitionEvent.VOD:
		case TransitionEvent.PVR:
			if (event == prevEvt) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore event because this event is same previous event");
				}
				return false;
			}
		default:
			return true;
		}
	}

	/**
	 * Gets the next event at the event queue.
	 *
	 * @return the next event.
	 */
	private TransitionEvent getNext() {
		TransitionEvent evt = null;
		while (!eventQ.isEmpty()) {
			evt = (TransitionEvent) eventQ.firstElement();
			eventQ.removeElementAt(0);
			if (evt.getType() != TransitionEvent.TRANSITION) {
				break;
			}
		}
		return evt;
	}


	/** The update notification popup. */
	private BasicPopup dialog = null;
	private LayeredKeyHandlerImpl layeredKeyHandler = new LayeredKeyHandlerImpl();
	private boolean popupResult;

	private boolean showConfirmExitSpecialVideo() {
		String endingMessage = MonitorStateManager.getInstance().getEndingMessage();
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "showConfirmExitSpecialVideo() " + endingMessage);
		}
		if(endingMessage == null) {
			return true;
		}
		synchronized(layeredKeyHandler) {
			dialog = new BasicDialog("Notification", WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
					WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), new String[]{endingMessage}, layeredKeyHandler);
			dialog.showPopup();

			try {
				layeredKeyHandler.wait();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "End of showConfirmExitSpecialVideo() popupResult=" + popupResult);
		}

		return popupResult;
	}

	public void hideConfirmExitSpecialVideo() {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "hideConfirmExitSpecialVideo()");
		}
		synchronized(layeredKeyHandler) {
			layeredKeyHandler.notifyAll();
			if(dialog != null) {
				dialog.hidePopup();
				dialog.dispose();
				dialog = null;
			}
		}

	}

	private class LayeredKeyHandlerImpl implements LayeredKeyHandler {
		public boolean handleKeyEvent(UserEvent event) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "handleKeyEvent");
			}
			if (event.getType() != KeyEvent.KEY_PRESSED) {
				return true;
			}

			int keyCode = event.getCode();

			if (keyCode == HRcEvent.VK_POWER) {
				return false;
			}

			switch (keyCode) {
			case OCRcEvent.VK_LEFT:
				dialog.setFocusBtn(true);
				break;
			case OCRcEvent.VK_RIGHT:
				dialog.setFocusBtn(false);
				break;
			case OCRcEvent.VK_ENTER:
				if (dialog.getFocusBtn()) {
					popupResult = true;
				} else {
					popupResult = false;
				}

				hideConfirmExitSpecialVideo();
				break;
			default:
			}

			return true;
		}
	};

	/**
	 * Sets the STB running state standby and running.
	 *
	 * @param runningState
	 */
	public void setRunningState(int runningState) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "setRunningState " + runningState);
		}
		mainThread.setRunningState(runningState);
	}

	/**
	 * The thread class for state transition.
	 *
	 * @author Woosik Lee
	 */
	private class StateManagerThread extends Thread {
		/** The initial state of this thread. */
		private static final int INITIAL = -1;
		/** The standby state of this thread. */
		private static final int STANDBY = 1;
		/** The running state of this thread. */
		private static final int RUNNING = 0;
		/** The running state. */
		private int state = INITIAL;

		/**
		 * Constructor.
		 */
		public StateManagerThread() {
			super("StateManagerThread");
		}

		public void setRunningState(int rState) {
			if (rState == Host.FULL_POWER) {
				state = RUNNING;
			} else if (rState == Host.LOW_POWER) {
				state = STANDBY;
			}
		}

		/**
		 * The running method. It override from Runnable class.
		 */
		public void run() {
			while (running) {
				keyBlocker.setBlock(false);
				try {
					TransitionEvent event = null;
					synchronized (eventQ) {
						if (eventQ.isEmpty()) {
							try {
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + "wait next command");
								}
								eventQ.wait();
							} catch (InterruptedException e) {
								Log.print(e);
								return;
							} catch (Exception e) {
								Log.print(e);
							}
						}
						event = getNext();
					}

					// 일반 transition 이외의 type 처리 필요.

					if (event == null || !checkIfValid(event)) {
						if (Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + " is special video : " + MonitorStateManager.getInstance().isSpecialVideoState());
						}
						boolean isSpecialVideoOverlappingAppRequest = false;
						if(event != null && event.getAppType() == TransitionEvent.PVR) {
							if(MonitorStateManager.getInstance().isSpecialVideoState()) {
								// PVR 실행 중에 Menu 에서 다시 PVR 을 선택 한 경우다.
								isSpecialVideoOverlappingAppRequest = true;
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + " startPvrMainMenu");
								}
								/*
								// PVR 실행 중에 Menu 에서 다시 PVR 을 선택 한 경우다. 이때는 State transition 대신 PVR 메뉴를 직접 호출 한다.
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + " startPvrMainMenu");
								}
								// 1. 기존 Overlapping App 을 닫는다.
								MonitorStateManager.getInstance().stopOverlappingApplication(true);
								// 2. PVR 의 메뉴를 호출 한다.
								CommunicationManager.getInstance().startPvrMainMenu();
								*/
							}
						}
						if(event != null && event.getAppType() == TransitionEvent.VOD) {
							if(MonitorStateManager.getInstance().isSpecialVideoState()) {
								//isSpecialVideoOverlappingAppRequest = true;
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + " startVODMainMenu");
								}

								// VOD 실행 중에 Menu 에서 다시 VOD 을 선택 한 경우다. 이때는 State transition 대신 VOD 메뉴를 직접 호출 한다.
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + " startVODMainMenu");
								}
								// 1. 기존 Overlapping App 을 닫는다.
								MonitorStateManager.getInstance().stopOverlappingApplication(true);
								// 2. VODstartVodMainMenu 의 메뉴를 호출 한다.
								CommunicationManager.getInstance().startVodMainMenu();
								continue;
							}
						}

						if(!isSpecialVideoOverlappingAppRequest) {
							continue;
						}
					}

					nextEventStartOverlappingApplicationOnSpecialVideo = false;
					nextEventStopOverlappingApplicationOnSpecialVideo = false;
					if (MonitorStateManager.getInstance().isSpecialVideoState()) {
						// If special video state and target app is Menu,EPG,PIP
						// does not stop previous app.
						switch (event.getAppType()) {
						case TransitionEvent.MAIN_MENU:
						case TransitionEvent.GUIDE:
						case TransitionEvent.PIP:
							if (Log.INFO_ON) {
								Log.printInfo(LOG_HEADER + "Now special video state and special app");
							}
							nextEventStartOverlappingApplicationOnSpecialVideo = true;
							break;
						case TransitionEvent.PVR:
						case TransitionEvent.VOD:
							if (event == prevEvt) {
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + "PVR, VOD same event in special video.");
								}
								nextEventStartOverlappingApplicationOnSpecialVideo = true;
							}
							break;

						case TransitionEvent.STATE_TV_VIEWING:
							if (event.getSourceId() == TransitionEvent.LCW_SOURCE_ID) {
								int overlappingApp = MonitorStateManager.getInstance().getOverlappingAppState();
								switch (overlappingApp) {
								case MonitorStateManager.OVERLAPPING_APP_FULL_MENU:
								case MonitorStateManager.OVERLAPPING_APP_EPG:
								case MonitorStateManager.OVERLAPPING_APP_PIP:
								case MonitorStateManager.OVERLAPPING_APP_PVR_MAIN:
								//case MonitorStateManager.OVERLAPPING_APP_VOD_MAIN:
									nextEventStopOverlappingApplicationOnSpecialVideo = true;
								}
							}
							break;
						}
					}

					if (!nextEventStartOverlappingApplicationOnSpecialVideo
							&& MonitorStateManager.getInstance().isSpecialVideoState()
							&& !nextEventStopOverlappingApplicationOnSpecialVideo) {
						// 현재 Special video state 이면서 non-Special app 이 뜨는 경우
						// (혹은 AV) 이다.
						// 이 경우에는 Major,Minor 모두 종료 해야 한다.
						// 0. Popup 으로 Confirm 받는다.
						// 1. Special video state 를 먼저 Normal 로 변경 하고
						// 2. MinorApp 을 stop 한다.
						boolean confirmResult = showConfirmExitSpecialVideo();
						if(!confirmResult) {
							continue;
						}
						MonitorStateManager.getInstance().releaseVideoContext(false);
					}

					if (Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "get command state=" + state
								+ ", nextEventStartOverlappingApplicationOnSpecialVideo="
								+ nextEventStartOverlappingApplicationOnSpecialVideo);
						Log.printInfo(LOG_HEADER + "get command state=" + state
								+ ", nextEventStopOverlappingApplicationOnSpecialVideo="
								+ nextEventStopOverlappingApplicationOnSpecialVideo);
					}

					switch (event.getType()) {
					case TransitionEvent.TRANSITION:
						if (state == STANDBY) {
							break;
						}
						keyBlocker.setBlock(true);
						stopPreviousApplication(event);
						//prevEvt = null;
						// PAUSE 된 상태이므로,
						// 혹시 밀린 요청이 있으면 무시해도 좋다.
						if (!eventQ.isEmpty()) {
							continue;
						}
						startNewApplication(event);

						if (nextEventStartOverlappingApplicationOnSpecialVideo || nextEventStopOverlappingApplicationOnSpecialVideo) {
							// Do not apply new event to prevEvt.
						} else {
							prevEvt = event;
						}
						break;

					case TransitionEvent.RUN:
						if (state == RUNNING) {
							break;
						}
						// Resume applications
						state = RUNNING;
						if (Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "start running action");
						}

						// 시청 시간 제한이면 AV 및 Menu 시작 하면 안된다.
						int wtrmMode = WatchingTimeRestrictionManager.getInstance().getCurrentMode();
						if (wtrmMode == WatchingTimeRestrictionManager.MODE_RESTRICTION_TIME) {
							if (Log.INFO_ON) {
								Log.printInfo(LOG_HEADER + "Now watcing time restriction occurs. ");
							}
							resetMenuTargetState();
							break;
						}

						// Inband update 중이면 AV 및 Menu 시작 하면 안 된다.
						InbandDataUpdateController ibUpdater = InbandDataUpdateController.getInstance();
						if (ibUpdater != null) {
							if (ibUpdater.isUpdating()) {
								if (Log.INFO_ON) {
									Log.printInfo(LOG_HEADER + "IB Update is starting so do not select channel.");
								}
								resetMenuTargetState();
								break;
							}
						}

						boolean menuAuth = AppLauncher.getInstance().startUnboundApplication(
								DataManager.getInstance().getMenuName(), null, false);
						if (!menuAuth) {
							AppLauncher.getInstance().exitToChannelWithoutMenu(TransitionEvent.LCW_SOURCE_ID);
							resetMenuTargetState();
						}
						try {
                            //->Kenneth[2017.10.13] VDTRMASTER-6218 : StandBy->Running 이라는 메시지 같이 보낼 수 있도록 함.
                            if (Log.DEBUG_ON) Log.printDebug("StateManager : Call changeChannel(MONITOR:RUNNING)");
							CommunicationManager.getInstance().getEpgService().changeChannel(
							    TransitionEvent.LCW_SOURCE_ID, "MONITOR:RUNNING");
							//CommunicationManager.getInstance().getEpgService().changeChannel(
						   	//    TransitionEvent.LCW_SOURCE_ID);
                            //<-
						} catch (RemoteException e) {
							Log.print(e);
						}
						break;

					case TransitionEvent.STANDBY:
						if (state == STANDBY) {
							break;
						}

						if(MonitorStateManager.getInstance().isSpecialVideoState()) {
							if (Log.INFO_ON) {
								Log.printInfo(LOG_HEADER + " To standby - stop special video state!");
							}
							MonitorStateManager.getInstance().releaseVideoContext(false);
						}

						stopPreviousApplication(event);
						state = STANDBY;
						prevEvt = null;
						if (Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "stop service");
						}
						stopChannel();
						if (Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "clear background color");
						}
						break;
					default:
						break;
					}

				} catch (Exception e) {
					Log.print(e);
				} catch (Throwable t) {
					Log.print(t);
				}
			}
		}
	}

	private void resetMenuTargetState() {
		// Set flag for Menu' initial state.
		if(CommunicationManager.getInstance().getMainMenuService() != null) {
            try {
            	if(Log.DEBUG_ON) {
            		Log.printDebug(LOG_HEADER + "call Menu getTargetMenuState()");
            	}
                CommunicationManager.getInstance().getMainMenuService().getTargetMenuState();
            } catch (Exception ex) {
                Log.print(ex);
            }
        }
	}
}

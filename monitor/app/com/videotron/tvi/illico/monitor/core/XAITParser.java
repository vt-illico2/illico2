// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/core/XAITParser.java,v 1.6 2017/01/09 21:12:13 zestyman Exp $

/*
 *  XAITParser.java	$Revision: 1.6 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import com.videotron.tvi.illico.log.Log;

public class XAITParser {

	private InputStream stream;
	private String applicationUrl;
	private int offset;
	private byte[] data;

	private static final String LOG_HEADER = "[XAIT Parser] ";

	public XAITParser(InputStream stream) {
		this.stream = stream;
		try {
			compute();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void printLogInfo(String arg) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + arg);
		}
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	private void compute() throws Exception {

		data = new byte[1024 * 2];
		int result = stream.read(data, 0, data.length);

		printLogInfo("read result=" + result);

		offset = 0;

		int tableId = oneByteToInt(data, offset);
		printLogInfo("table_id=" + tableId);
		offset++;
		offset++; // section_syntax_indicator, reserved_future_use, reserved,
					// section_length (1)
		offset++; // section_length (2)
		offset++; // test_application_flag, application_type(1)
		offset++; // application_type (2)
		offset++; // reserved, version_number, current_next_indicator
		offset++; // section_number
		int section_number = oneByteToInt(data, offset);
		printLogInfo("section_number=" + section_number);

		offset++; // last_section_number

		int common_descriptor_length = 0; // 12bit
		// 앞의 4bit (reserved_future_use) 를 0 으로 채운다. 2byte 에서 앞의 4bit 는
		// reserved_future_use 이다.
		data[offset] = (byte) (data[offset] & 0x0F);
		common_descriptor_length = twoBytesToInt(data, offset, false);
		printLogInfo("common_descriptor_length=" + common_descriptor_length);
		offset++;
		offset++;
		String common_descriptor = byteArrayToString(data, offset, common_descriptor_length);
		printLogInfo("common_descriptor=" + common_descriptor);

		offset += common_descriptor_length;

		//
		int application_loop_length = 0; // 12bit
		// 앞의 4bit (reserved_future_use) 는 0 으로 채운다.
		data[offset] = (byte) (data[offset] & 0x0F);
		application_loop_length = twoBytesToInt(data, offset, false);
		printLogInfo("application_loop_length=" + application_loop_length + ", offset=" + offset);
		offset++;
		offset++;

		offset += 6; // application_identifier

		int application_control_code = oneByteToInt(data, offset);
		printLogInfo("application_control_code=" + application_control_code + ", offset=" + offset);

		offset++;

		data[offset] = (byte) (data[offset] & 0x0F);
		int application_descriptors_loop_length = twoBytesToInt(data, offset, false);
		printLogInfo("application_descriptors_loop_length=" + application_descriptors_loop_length + ", offset="
				+ offset);

		offset++;
		offset++;

		// //////// 여기서 부터 Application descriptor
		// application descriptor byte 개수 카운트용
		int descriptorUsage = 0;
		int count = 0;
		while (true) {
			int descriptor_tag = oneByteToInt(data, offset);
			printLogInfo("[" + count + "] descriptor_tag=" + descriptor_tag);
			int usage = processApplicationDescriptorOnlyUrl(descriptor_tag);
			descriptorUsage += usage;
			printLogInfo("[" + count + "] application descriptor usage =" + usage + ", total=" + descriptorUsage
					+ ", application_descriptors_loop_length=" + application_descriptors_loop_length);

			if (descriptorUsage >= application_descriptors_loop_length) {
				break;
			}
			count++;
		}

	}

	/**
	 * URL 만을 얻기 위한 Application descriptor parsing.
	 * @param descriptor_tag
	 * @return
	 */
	private int processApplicationDescriptorOnlyUrl(int descriptor_tag) {
		if (descriptor_tag == 2) {
			// Table 90: transport protocol descriptor syntax
			printLogInfo("===== Table 90: transport protocol descriptor syntax =====");

			offset++;
			int descriptor_length = oneByteToInt(data, offset);
			printLogInfo("descriptor_length=" + descriptor_length);
			offset++;

			int protocol_id = twoBytesToInt(data, offset, false);
			printLogInfo("protocol_id=" + protocol_id);
			offset++;
			offset++;

			int transport_protocol_label = oneByteToInt(data, offset);
			printLogInfo("transport_protocol_label=" + transport_protocol_label);
			offset++;

			// 11.2.1.7 Transport via Interaction Channel (OCAP)
			int url_length = oneByteToInt(data, offset);
			printLogInfo("url_length=" + url_length);
			offset++;

			int section_byte_length = descriptor_length - 4;
			String selector_byte = byteArrayToString(data, offset, section_byte_length);
			printLogInfo("selector_byte=" + selector_byte);
			offset += section_byte_length;

			this.applicationUrl = selector_byte;

			return descriptor_length + 2;
		} else {
			// 모르는 descriptor 가 오면 pass 한다.
			printLogInfo("==== Unknown descriptor ====");
			offset++;
			int descriptor_length = oneByteToInt(data, offset);
			printLogInfo("descriptor_length=" + descriptor_length);
			offset++;

			offset += descriptor_length;
			return 2 + descriptor_length;
		}
	}

	private static int oneByteToInt(byte[] data, int offset) {
		return ((int) data[offset]) & 0xFF;
	}

	private static int twoBytesToInt(byte[] data, int offset, boolean byNegative) {
		return ((((int) data[offset]) & (byNegative ? 0x7F : 0xFF)) << 8) + (((int) data[offset + 1]) & 0xFF);
	}

	private static int threeBytesToInt(byte[] data, int offset) {

		int x1 = (((int) data[offset]) & 0xFF) << 16;
		int x2 = (((int) data[offset + 1]) & 0xFF) << 8;
		int x3 = (((int) data[offset + 2]) & 0xFF);

		return x1 + x2 + x3;
	}

	private static int fourBuytesToInt(byte[] data, int offset) {
		int x0 = (((int) data[offset]) & 0xFF) << 32;
		int x1 = (((int) data[offset]) & 0xFF) << 16;
		int x2 = (((int) data[offset + 1]) & 0xFF) << 8;
		int x3 = (((int) data[offset + 2]) & 0xFF);

		return x0 + x1 + x2 + x3;
	}

	private static String byteArrayToString(byte[] data, int offset, int length) {
		return new String(data, offset, length);
	}

	public static void main(String[] args) {
		try {
			File file = new File("./AOD_Test_App.xait");
			InputStream stream = new FileInputStream(file);
			// printLogInfo("File is " + file.getAbsolutePath());
			XAITParser parser = new XAITParser(stream);

			// printLogInfo("URL is " + parser.getApplicationUrl());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

/*
 * $Log: XAITParser.java,v $
 * Revision 1.6  2017/01/09 21:12:13  zestyman
 * commit after replace with source of CYO Bundling.
 *
 * Revision 1.4  2014/02/04 18:33:09  june
 * [videotron] encoding.
 *
 * Revision 1.3  2014/02/04 18:32:45  june
 * [videotron] encoding.
 *
 */
package com.videotron.tvi.illico.monitor.core;

import java.util.ArrayList;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.Environment;

/**
 * This class manages the STB mode. {@see com.videotron.tvi.illico.ixc.monitor.MonitorService#STB_MODE_BOOTING}, {@see
 * com.videotron.tvi.illico.ixc.monitor.MonitorService#STB_MODE_NORMAL}, {@see
 * com.videotron.tvi.illico.ixc.monitor.MonitorService#STB_MODE_STANDALONE}.
 * @author Woosik Lee
 */
public class STBModeManager implements AppsDatabaseEventListener {

    /** A Singleton instance of ServiceStateManager. */
    private static STBModeManager instance;
    /** This list has the listeners that notifies of service transition. */
    private ArrayList listeners = null;
    /** This is the current service state. */
    private int currentMode = MonitorService.STB_MODE_BOOTING;
    /** The log header message. */
	private static final String LOG_HEADER = "[STBModeManager] ";
    /**
     * Constructor.
     */
    private STBModeManager() {
        listeners = new ArrayList();
        AppsDatabase.getAppsDatabase().addListener(this);
    }

    /**
     * Gets a singleton instance.
     * @return instance of ServiceStateManager.
     */
    public static synchronized STBModeManager getInstance() {
        if (instance == null) {
            instance = new STBModeManager();
        }
        return instance;
    }

    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "dispose");
        }
        if (listeners != null) {
            listeners.clear();
        }
        AppsDatabase.getAppsDatabase().removeListener(this);
    }

    /**
     * Sets the STB mode. If changed STB mode, in this method notify mode changed to listeners.
     * @param newMode new mode of STB.
     */
    public synchronized void setMode(int newMode) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "setState() newState=" + newMode + ", curState="
                    + currentMode);
        }

        if(newMode == MonitorService.STB_MODE_STANDALONE) {
            // Check is PVR STB and standalone package's authority.
            int authority = AuthorizationManager.getInstance().getStoredStandaloneAuthority();

            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "authority=" + authority + ", DVR="
                        + Environment.SUPPORT_DVR);
            }

            if(authority != AuthorizationManager.CHECK_RESULT_AUTHORIZED || !Environment.SUPPORT_DVR) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "can't change mode to Standalone so go to the NO_SIGNAL Mode.");
                }
                currentMode = MonitorService.STB_MODE_NO_SIGNAL;
                notifyServiceStateListeners(currentMode);
                return;
            }

            // DNCS-F0285 : A PVR enabled DVR cannot be used in standalone mode for more than 30 days.
            // Hot to implement?

        }

        if (currentMode != newMode) {
            currentMode = newMode;
        } else {
            return;
        }
        notifyServiceStateListeners(currentMode);
    }

    /**
     * Gets the current STB mode.
     * @return current mode.
     */
    public int getMode() {
        return currentMode;
    }

    public boolean isStandaloneMode() {
        if(currentMode == MonitorService.STB_MODE_STANDALONE) {
            return true;
        }
        return false;
    }

    /**
     * Notify changed mode to the listeners.
     * @param state new state.
     */
    private void notifyServiceStateListeners(int state) {
        Object[] lsnrs;
        synchronized (listeners) {
            lsnrs = listeners.toArray();
        }
        for (int i = 0; i < lsnrs.length; i++) {
            try {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "modeChanged to "
                            + ((ListenerItem) lsnrs[i]).listener);
                }
                ((ListenerItem) lsnrs[i]).listener.modeChanged(state);
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }

    /**
     * Adds the listener
     * @param l The {@link STBModeChangeListener} instance.
     * @param applicationName The application name that requests listener.
     */
    public void addModeChangeListener(STBModeChangeListener l, String applicationName) {
        if (Log.INFO_ON) {
            Log.printInfo("ServiceStateManager: addStateListener=" + applicationName);
        }
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            if (!listeners.contains(item)) {
                listeners.add(item);
            } else {
                // Removes a previous listener.
                listeners.remove(item);
                // Adds a new listener.
                listeners.add(item);
            }
        }
    }

    /**
     * Removes a listener.
     * @param l the listener instance.
     * @param applicationName the requested application name.
     */
    public void removeModeChangeListener(STBModeChangeListener l, String applicationName) {
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            listeners.remove(item);
        }
    }

    /**
     * This class has the unique application id and the instance of ServiceStateListener.
     * @author Woosik Lee
     */
    private class ListenerItem {
        /** The instance of listener. */
        private STBModeChangeListener listener;
        /** The requested application name. */
        private String applicationName;

        /**
         * Constructor.
         * @param l listener.
         * @param appName application name.
         */
        public ListenerItem(final STBModeChangeListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItem) {
                String appName = ((ListenerItem) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void newDatabase(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryAdded(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * This method is called when the bound application is deleted at the AppsDatabase. If the
     * {@link STBModeManager} has the bound application's listener, it must delete.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryRemoved(AppsDatabaseEvent paramAppsDatabaseEvent) {
        synchronized (listeners) {
            AppID appId = paramAppsDatabaseEvent.getAppID();
            AppsDatabase db = AppsDatabase.getAppsDatabase();
            AppAttributes appAtt = db.getAppAttributes(appId);
            String appName = appAtt.getName();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "RentryRemoved() appName=" + appName);
            }

            ListenerItem item = new ListenerItem(null, appName);
            listeners.remove(item);
        }
    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryChanged(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }



}

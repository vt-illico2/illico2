package com.videotron.tvi.illico.monitor.core;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppProxy;
import org.dvb.application.AppStateChangeEvent;
import org.dvb.application.AppStateChangeEventListener;
import org.dvb.application.DVBJProxy;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;

/**
 * The proxy class for unbouind application.
 * <P>
 * @author Woosik Lee
 */
public class UnboundAppProxy implements AppStateChangeEventListener {
    /** It is proxy of the application. */
    private DVBJProxy proxy;

    /** Indicates the daemon application type. */
    public static final int DAEMON = 0;
    /** Indicates the semi daemon application type. */
    public static final int SEMI_DAEMON = 1;
    /** Indicates the normal application type. */
    public static final int NORMAL = 2;
    /** Indicates the daemon application priority. if application priority is over 200, it defines the DAEMON. */
    private static final int DAEMON_PRIORITY = 200;
    /**
     * Indicates the daemon application priority.
     * if application priority is over 100 and under 200, it defines the SIMI_DAEMON.
     */
    private static final int SEMI_DAEMON_PRIORITY = 100;
    /** Indicates the application that has registered API and It is daemon. */
    private static final int DAEMON_REGISTERED_API_PRIORITY = 240;
    /** Indicates the application that has registered API and It is semi-daemon. */
    private static final int SEMI_DAEMON_REGISTERED_API_PRIORITY = 180;

    /** Indicates the application Type. */
    private int appType;
    /** Indicates the application priority in the XAIT. */
    private int priority;
    /** Registered API. */
    private boolean hasRegisteredAPI;

    /** Application name. */
    private String appName;
    /** Application Id. */
    private AppID appId;

    /** Indicates that a running state of application. */
    private boolean running = false;

    /** The flag determines which API to call that applicable to semi-daemon application. */
    private boolean isFirstStart = true;

    /** The application version in the XAIT. */
    private int appVersion;
    
    private int storagePriority;
    
    /** AOD application or not. */
    private boolean isRegisteredUnboundApplication = false;

    /** The log header message. */
	private static final String LOG_HEADER = "[UnboundAppProxy] ";

    /**
     * Create a UnboundAppProxy instance that includes the DVBJProxy and AppAttributes.
     * @param pProxy instance of DVBJProxy.
     * @param pAttr instance of AppAttributes.
     */
    public UnboundAppProxy(final DVBJProxy pProxy, final AppAttributes pAttr, int appVersion, int storagePriority, boolean isRegisteredUnboundApp) {
        this.proxy = pProxy;
        appName = pAttr.getName();
        appId = pAttr.getIdentifier();
        this.priority = pAttr.getPriority();
        this.appVersion = appVersion;
        this.storagePriority = storagePriority;
        this.isRegisteredUnboundApplication = isRegisteredUnboundApp;

        if (priority >= DAEMON_PRIORITY) {
            appType = DAEMON;
            if(priority >= DAEMON_REGISTERED_API_PRIORITY) {
                hasRegisteredAPI = true;
            }
        } else if (priority >= SEMI_DAEMON_PRIORITY) {
            appType = SEMI_DAEMON;
            if(priority >= SEMI_DAEMON_REGISTERED_API_PRIORITY) {
                hasRegisteredAPI = true;
            }
        } else {
            appType = NORMAL;
        }
    }

    /**
     * Gets the application type.
     * @return the application type
     * @see UnboundAppProxy#DAEMON
     * @see UnboundAppProxy#SEMI_DAEMON
     * @see UnboundAppProxy#NORMAL
     */
    public int getAppType() {
        return appType;
    }

    /**
     * Gets the application priority.
     * @return the application priority.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Gets the application has registered api.
     * @return true application has registered api.
     */
    public boolean hasRegisteredAPI() {
        return hasRegisteredAPI;
    }

    /**
     * Starts the application.
     * @param args the parameters.
     * @return Returns true if the application started successfully, if not returns false .
     */
    public synchronized boolean start(String[] args) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "start(" + appName + ")");
        }
        boolean retv = true;
        if (appType == DAEMON) {
            if (proxy.getState() == AppProxy.PAUSED) {
                proxy.resume();
            } else {
                proxy.start(args);
            }
        } else if (appType == SEMI_DAEMON) {
            if (proxy.getState() != AppProxy.PAUSED) {
                if (Log.WARNING_ON) {
                    Log.printWarning(LOG_HEADER + "" + appName + " is not PAUSED state so return");
                }
                return true;
            }
            synchronized (proxy) {
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "start loading animation. isFirstStart=" + isFirstStart);
                }
                CommunicationManager.getInstance().startLoadingAnimation();
                proxy.addAppStateChangeEventListener(this);
                if (isFirstStart) {
                    proxy.start();
                } else {
                    proxy.resume();
                }
                isFirstStart = false;

                try {
                    proxy.wait();
                } catch (InterruptedException e) {
                    Log.print(e);
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "stop loading animation");
                }
                CommunicationManager.getInstance().stopLoadingAnimation();
            }
        } else {
            if (args == null) {
                proxy.start();
            } else {
                proxy.start(args);
            }
        }
        running = true;

        return retv;
    }

    /**
     * It call a Xlet#initXlet() of the application.
     */
    public synchronized void init() {
        proxy.init();
    }

    /**
     * Stops the application.
     */
    public synchronized void stop() {
        if (!running && appType != SEMI_DAEMON) {
            if (Log.WARNING_ON) {
                Log.printWarning(LOG_HEADER + "" + appName + " already stopped");
            }
            return;
        }

        if (appType == DAEMON) {
            running = false;
            proxy.pause();
        } else if (appType == SEMI_DAEMON) {
            synchronized (proxy) {
                running = false;
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "pause SEMI-DAEMON ");
                }
                proxy.addAppStateChangeEventListener(this);
                proxy.pause();
                try {
                    proxy.wait();
                } catch (InterruptedException e) {
                    Log.print(e);
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "end of pause semi-daemon");
                }
            }
        } else {
            proxy.stop(true);
        }
    }

    /**
     * Destroys the application.
     */
    public void destroy() {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "destroy(" + appName + ")");
        }
        isFirstStart = true;
        proxy.stop(true);
    }

    /**
     * It compares this class and other class.
     * <P>
     * First it compares by id, if id is null compares by application name.
     * @param id the AppId.
     * @param name the application name.
     * @return if same instance of class returns true, or else returns false.
     */
    public boolean equals(AppID id, String name) {
        if (id != null) {
            if (appId.equals(id)) {
                return true;
            } else {
                return false;
            }
        }
        if (name != null) {
            if (this.appName.equals(name)) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    
    public boolean equals(Object obj) {
    	if(obj instanceof UnboundAppProxy) {
    		UnboundAppProxy compare = (UnboundAppProxy) obj;
    		AppID cId = compare.getAppID();
    		String cName = compare.getName();
    		return equals(cId, cName);
    	}
    	return false;
    }

    /**
     * Returns the application id.
     * @return AppID Instance.
     */
    public AppID getAppID() {
        return appId;
    }

    /**
     * Returns the application proxy.
     * @return DVBJProxy instance.
     */
    public DVBJProxy getDVBJProxy() {
        return proxy;
    }

    /**
     * Returns the application name.
     * @return the application name.
     */
    public String getName() {
        return appName;
    }

    public int getAppVersion() {
    	return appVersion;
    }
    
    public int getStoragePriority() {
    	return storagePriority;
    }

    /**
     * Implementation of AppStateChangeEventListener.
     * @param ev the instance of AppStateChangeEvent.
     */
    public void stateChange(AppStateChangeEvent ev) {
        int state = ev.getToState();
        boolean hasFailed = ev.hasFailed();
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "stateChange state=" + state + ", hasFailed=" + hasFailed);
        }
        if (state == AppProxy.STARTED || state == AppProxy.PAUSED) {
            proxy.removeAppStateChangeEventListener(this);
            synchronized (proxy) {
                proxy.notifyAll();
            }
        }
    }

    /**
     * Override the toString() by Object class.
     * @see toString in class java.lang.Object#toString().
     * @return class message.
     */
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("UnboundAppProxy : ");
        sb.append("NAME:" + appName);
        sb.append(" ID : " + appId);
        sb.append(" Priority : " + priority);
        sb.append(" AppVersion : " + appVersion);
        sb.append(" isRegisteredUnboundApplication : " + isRegisteredUnboundApplication);
        return sb.toString();
    }
}

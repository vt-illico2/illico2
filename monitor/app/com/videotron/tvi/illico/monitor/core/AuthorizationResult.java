package com.videotron.tvi.illico.monitor.core;

public class AuthorizationResult {

    private short eid;
    
    private int result;
    
    
    public AuthorizationResult(short id, int r) {
        this.eid = id;
        this.result = r;
    }
    
    public short getEid() {
        return eid;
    }
    
    public void setResult(int r) {
        this.result = r;
    }
    
    public int getResult() {
        return result;
    }
    
    public boolean equals(Object o) {
        AuthorizationResult obj = (AuthorizationResult)o;
        if(obj.getEid() == eid) {
            return true;
        }
        return false;
    }
}

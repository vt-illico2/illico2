package com.videotron.tvi.illico.monitor.core;

import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;
import org.dvb.application.CurrentServiceFilter;
import org.dvb.application.DVBJProxy;
import org.ocap.application.AppManagerProxy;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.MonitorConfigurationManager;

/**
 * Application On Demand Manager.
 * 
 * 여기서는 registerUnboundApp 을 한다. 그리고 unregisterUnboundApp 은 일단 하지 않는다.
 * 그래서 XAIT 에 App 이 추가 된 이후에 삭제 될 것에 대해서는 고민 하지 않는다. 단 삭제를 해야 한다고 하면 UnboundAppsManager 에 
 * 삭제 하는 코드도 추가 해야 한다.
 * 
 * 기본적으로 AOD는 storage priority 가 0 일 가능성이 크다. 일일이 다 storage 에 저장 하기 힘들테니..
 * 
 * @author WSLEE1
 * 
 */
public class AodManager implements AppsDatabaseEventListener {
	
	private Hashtable aodAppList;
	
	/** Running state. */
	private boolean runningState = false;
	
	private String currentRequestedAppName;

	/** AOD server ip. */
	private String host;
	/** AOD server port. */
	private int port;
	/** AOD server context root. */
	private String contextRoot;
	/** AOD Server url. */
	private String urlPath;

	/** The log header message. */
	private static final String LOG_HEADER = "[UnboundAppUpdateManager] ";
	
	private static AodManager instance = null;
	
	public synchronized static AodManager getInstance() {
		if(instance == null) {
			instance = new AodManager();
		}
		return instance;
	}

	/**
	 * Constructor.
	 */
	private AodManager() {
		
		aodAppList = new Hashtable();
		
		host = MonitorConfigurationManager.getInstance().getAodServerIP();
		port = MonitorConfigurationManager.getInstance().getAodServerPort();
		contextRoot = MonitorConfigurationManager.getInstance().getAodContextRoot();
		urlPath = "http://" + host + ":" + port + contextRoot + "/";
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "AodManager server info : " + urlPath);
		}
	}
	
	public UnboundAppProxy getAodAppProxy(String app) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "getAodAppProxy() appName :" + app);
			Enumeration keys = aodAppList.keys();
			while(keys.hasMoreElements()) {
				String name = (String) keys.nextElement();
				UnboundAppProxy proxy = (UnboundAppProxy) aodAppList.get(name);
				Log.printInfo(LOG_HEADER + " **** " + name + ":" + proxy.toString());
			}
		}
		
		UnboundAppProxy appProxy = (UnboundAppProxy) aodAppList.get(app);
		return appProxy;
	}

	public synchronized void startAodApplication(String appName) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startAodApplication() appName :" + appName);
		}
		
		runningState = true;
		this.currentRequestedAppName = appName;
		
		CommunicationManager.getInstance().startLoadingAnimation();
		
		// Add AppsDatabaseUpdateListener
		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
		appsDB.addListener(this);
		
		// TODO Timeout ??
		
		
		AppManagerProxy proxy = AppManagerProxy.getInstance();
		try {
			URL url = new URL(urlPath + appName);
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			InputStream xait = conn.getInputStream();

			if (Log.INFO_ON) {
				Log.printInfo("registerUnboundApplication() inputStream: " + xait);
			}
			proxy.registerUnboundApp(xait);
			if (Log.INFO_ON) {
				Log.printInfo("registerUnboundApplication() finish registerUnboundApp()");
			}

		} catch (Exception e) {
			e.printStackTrace();
			CommunicationManager.getInstance().stopLoadingAnimation();
			appsDB.removeListener(this);
			runningState = false;
			
			CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
		}
	}
	
	private synchronized void registerComplete() {
		if (Log.INFO_ON) {
			Log.printInfo("registerComplete() appName: " + currentRequestedAppName + ",runningState=" + runningState);
		}
		
		if(!runningState) {
			return;
		}
		
		// Find requested application in the AppsDatabase
		AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                
                DVBJProxy proxy = (DVBJProxy) appsDB.getAppProxy(appID);
                AppAttributes attr = appsDB.getAppAttributes(appID);
                
                String name = attr.getName();
                if(currentRequestedAppName.equals(name)) {
                	// Find target application!!!!
                	if (Log.INFO_ON) {
                		int priority = attr.getPriority();
            			Log.printInfo("registerComplete() found app ! priority = " + priority);
            		}
                	int appVersion = getAppVersion(attr);
                	UnboundAppProxy newAppProxy = new UnboundAppProxy(proxy, attr, appVersion, -1, true);
                	aodAppList.put(currentRequestedAppName, newAppProxy);
                	break;
                }
            }
        }
        
        CommunicationManager.getInstance().stopLoadingAnimation();
		appsDB.removeListener(this);
		runningState = false;
		
		// start unbound application !
		
		UnboundAppProxy appProxy = getAodAppProxy(currentRequestedAppName);
		if(appProxy == null) {
			if(Log.INFO_ON) {
				Log.printInfo("Error! not found application in the app database");
			}
			CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_504, null);
		}
		else {
			if(Log.INFO_ON) {
				Log.printInfo("start !!!!!!!");
			}
			AppLauncher.getInstance().startUnboundApplication(currentRequestedAppName, null);
		}
		
		if (Log.INFO_ON) {
			Log.printInfo("registerComplete() END appName: " + currentRequestedAppName);
		}
	}
	
	private int getAppVersion(AppAttributes attr) {
		int appVersion = -1;
        try {
            Class cls = Class.forName("com.alticast.am.AMOCAPJProxy");
            Method method = cls.getMethod("getVersion", null);
            if(!method.isAccessible()) {
            	method.setAccessible(true);
            }
            Object obj = method.invoke(attr, null);
            Integer integer = (Integer)obj;
            appVersion = integer.intValue();
            
            // It is debugging code.
            Method method0 = cls.getMethod("getStoragePriority", null);
            if(!method0.isAccessible()) {
            	method0.setAccessible(true);
            }
            Object obj0 = method0.invoke(attr, null);
            Integer integer0 = (Integer)obj0;
            int storagePriority = integer0.intValue();
            if (Log.INFO_ON) {
    			Log.printInfo("storagePriority() " + storagePriority);
    		}
            
        } catch (Exception e) {
        	e.printStackTrace();
        }
        
        return appVersion;
	}
	
	
	/////////////////////////////////////////////
	// AppsDatabaseEventListener 
	/////////////////////////////////////////////
	public void newDatabase(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo("AppsDatabaseEventListener newDatabase() : " + getAppsDatabaseEventInfo(event));
		}
		registerComplete();
	}

	public void entryAdded(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo("AppsDatabaseEventListener entryAdded() : " + getAppsDatabaseEventInfo(event));
		}
	}

	public void entryRemoved(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo("AppsDatabaseEventListener entryRemoved() : " + getAppsDatabaseEventInfo(event));
		}
	}

	public void entryChanged(AppsDatabaseEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo("AppsDatabaseEventListener entryChanged() : " + getAppsDatabaseEventInfo(event));
		}
	}
	
	/**
	 * Information for AppsDatabaseEvent.
	 * @param event AppsDatabaseEvent.
	 * @return Information message.
	 */
	private String getAppsDatabaseEventInfo(AppsDatabaseEvent event) {
		StringBuffer buffer = new StringBuffer("EventID=");
		switch(event.getEventId()) {
		case AppsDatabaseEvent.NEW_DATABASE:
			buffer.append("NEW_DATABASE");
			break;
		case AppsDatabaseEvent.APP_ADDED:
			buffer.append("APP_ADDED");
			break;
		case AppsDatabaseEvent.APP_CHANGED:
			buffer.append("APP_CHANGED");
			break;
		case AppsDatabaseEvent.APP_DELETED:
			buffer.append("APP_DELETED");
			break;
		}
		buffer.append(", AppID=" + event.getAppID());
		return buffer.toString();
	}
}

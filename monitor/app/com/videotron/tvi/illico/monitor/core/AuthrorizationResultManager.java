package com.videotron.tvi.illico.monitor.core;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;

/**
 * TODO 이 클래스를 사용하지 말자~~~~
 * Manages the entitlement id's authority result.
 * @author Woosik Lee
 *
 */
public class AuthrorizationResultManager {

    private boolean checkingAllAuthority;

    private ArrayList resultList;

    private static AuthrorizationResultManager instance;

    private static final String LOG_HEADER = "[AuthrorizationResultManager] ";

    public synchronized static AuthrorizationResultManager getInstance() {
        if (instance == null) {
            instance = new AuthrorizationResultManager();
        }
        return instance;
    }

    private AuthrorizationResultManager() {
        resultList = new ArrayList();
        checkingAllAuthority = false;
    }

    public synchronized void setNewEidList(Hashtable table) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + ": setNewEidList()");
        }
        checkingAllAuthority = true;
        // 리스트가 변경이 되었기 때문에 이미 Notify 가 간다. 그래서 여기서는 새로운 리스트와 결과를 저장해 놓으면 된다.
        resultList.clear();
        Enumeration keys = table.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            Short value = (Short) table.get(key);
            short eid = value.shortValue();
            int checkResult = AuthorizationManager.getInstance().checkSourceAuthorization(eid);
            AuthorizationResult result = new AuthorizationResult(eid, checkResult);
            resultList.add(result);
        }

        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + ": setNewEidList ============");
            for (int i = 0; i < resultList.size(); i++) {
                AuthorizationResult re = (AuthorizationResult) resultList.get(i);
                Log.printDebug("\t[" + i + "] EID(" + re.getEid() + "), result(" + re.getResult() + ")");
            }
            Log.printDebug(LOG_HEADER + ": ===============================");
        }
        checkingAllAuthority = false;
    }

    public void checkResult(final short eid, final int result) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + ": checkResult() checkingAllAuthority="
                    + checkingAllAuthority);
        }
        if (checkingAllAuthority) {
            return;
        }
        Thread checkThread = new Thread() {
            public void run() {
                synchronized (AuthrorizationResultManager.this) {
                    // 권한 체크 할 때 마다 여기서 결과를 받아서 변경 된 것이 있다면 바로 반영하고 노티를 주자.
                    for (int i = 0; i < resultList.size(); i++) {
                        AuthorizationResult re = (AuthorizationResult) resultList.get(i);
                        if (re.getEid() == eid) {
                            if (Log.INFO_ON) {
                                Log.printInfo(LOG_HEADER + ": checkResult eid=" + eid
                                        + ", result=" + result + ", stored result=" + re.getResult());
                            }

                            if (result != re.getResult()) {
                                re.setResult(result);
                                notifyEIDMappingTableUpdated();
                            }
                        }
                    }
                }
            }
        };
        checkThread.start();

    }

    /**
     * Checks the all entitlement id.
     */
    public void checkAllEIDsAuthority() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + ": checkAllEIDsAuthority() checkingAllAuthority="
                    + checkingAllAuthority);
        }
        if (checkingAllAuthority) {
            return;
        }

        synchronized (this) {
            checkingAllAuthority = true;
            boolean changed = false;

            for (int i = 0; i < resultList.size(); i++) {
                AuthorizationResult re = (AuthorizationResult) resultList.get(i);
                short eid = re.getEid();
                int result = re.getResult();
                int checkResult = AuthorizationManager.getInstance().checkSourceAuthorization(eid);
                if (result != checkResult) {
                    if(Log.INFO_ON) {
                        Log.printInfo(LOG_HEADER + ": Found changed authority eid is " + eid
                                + ", result is " + checkResult);
                    }
                    changed = true;
                    re.setResult(checkResult);
                }
            }

            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + ": checkAllEIDsAuthority changed=" + changed);
                for (int i = 0; i < resultList.size(); i++) {
                    AuthorizationResult re = (AuthorizationResult) resultList.get(i);
                    Log.printDebug("\t[" + i + "] EID(" + re.getEid() + "), result(" + re.getResult() + ")");
                }
                Log.printDebug(LOG_HEADER + ": ===============================");
            }

            if (changed) {
                notifyEIDMappingTableUpdated();
            }

            checkingAllAuthority = false;
        }
    }

    private void notifyEIDMappingTableUpdated() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + ": notifyEIDMappingTableUpdated()");
        }

        Thread t = new Thread() {
            public void run() {
                EIDDataManager.getInstance().notifyEIDMappingTableUpdated();
            }
        };
        t.start();
    }
}

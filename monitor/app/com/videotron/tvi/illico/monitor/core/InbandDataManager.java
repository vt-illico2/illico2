package com.videotron.tvi.illico.monitor.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import javax.tv.service.SIManager;
import javax.tv.service.Service;
import javax.tv.service.navigation.ServiceList;
import javax.tv.service.selection.NormalContentEvent;
import javax.tv.service.selection.ServiceContext;
import javax.tv.service.selection.ServiceContextEvent;
import javax.tv.service.selection.ServiceContextListener;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;
import org.ocap.net.OcapLocator;

import com.videotron.tvi.illico.ixc.monitor.InbandDataListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.DataVersionManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.framework.DataAdapterManager;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.monitor.data.ListBehavior;

import com.videotron.tvi.illico.framework.FrameworkMain;


/**
 * This class manages the receiving inband data.
 * @author Woosik Lee
 */
public final class InbandDataManager implements AppsDatabaseEventListener, ServiceContextListener, TVTimerWentOffListener {

    /** The no action state of receiving data. */
    private static final int STATE_PAUSED = 0;
    /** Indicates the receiving data now. */
    private static final int STATE_RECEIVEING_DATA = 1;
    /** The current state. */
    private int currentState = STATE_PAUSED;
    /** The ServiceContext instance for tune to inband channel. */
    private ServiceContext serviceContext;

    /** The wait time of check SI data validation. */
    private static final long WAIT_TIME_FOR_CHECK_SI_DATA = 2000L;
    /** The max count of check SI data validation. */
    private static final int MAX_COUNT_FIND_SI_DATA = 20;

    /** The wait time of next service selection when fail to service selection. */
    private static final long WAIT_TIME_FOR_SERVICE_SELECTION = 1000L;
    /** The max count of service selection when fail to service selection. */
    private static final int MAX_COUNT_SERVICE_SELECTION = 2;
    /** The count of service selection. */
    private int countForServiceSelection = 0;

    /** A Singleton instance of InbandDataManager. */
    private static InbandDataManager instance;

    /** This list has the listeners that notifies of available data. */
    private ArrayList listeners = null;

    private InternalInbandDataListener internalInbandDataListener = null;

    /** The flag for receiving data. If true, all applications receive data at once. */
    private boolean receivingInbandDataAtOnce = false;

    /** The instance of TVTimer. It is used to update timeout. */
	private TVTimer timer = TVTimer.getTimer();
	/**
	 * The instance of TVTimerSpec. It is used to timeout.
	 */
	private TVTimerSpec timerSpec = new TVTimerSpec();

	private int updateTryCount = 0;

	private static final String LOG_HEADER = "[InbandDataManager] ";

    /**
     * Constructor.
     */
    private InbandDataManager() {
        String dataFlag = DataManager.getInstance().getReceivingInbandDataAtOnce();
        if(dataFlag.equalsIgnoreCase("YES")) {
            receivingInbandDataAtOnce = true;
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "receivingInbandDataAtOnce=" + receivingInbandDataAtOnce);
        }
        listeners = new ArrayList();
        AppsDatabase.getAppsDatabase().addListener(this);
        updateTryCount = 0;

        //->Kenneth : 2015.1.8 : VDTRMASTER-5329 : 다른 app 처럼 업데이트 되게 만듦
        Log.printDebug("Kenneth : addInbandDataListener for Monitor IB");
        InbandDataListener ibListener = new InbandDataListenerImpl();
        addInbandDataListener(ibListener, FrameworkMain.getInstance().getApplicationName());
    }

    /**
     * Gets a singleton instance.
     * @return instance of ServiceStateManager.
     */
    public static synchronized InbandDataManager getInstance() {
        if (instance == null) {
            instance = new InbandDataManager();
        }
        return instance;
    }

    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "dispose");
        }
        if (listeners != null) {
            listeners.clear();
        }
        AppsDatabase.getAppsDatabase().removeListener(this);
    }

    /**
     * Adds the listener for the receiving inband data.
     * @param l The {@link InbandDataListener} instance.
     * @param applicationName The application name that requests listener.
     */
    public void addInbandDataListener(final InbandDataListener l, String applicationName) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "addInbandDataListener=" + applicationName + ", receivingInbandDataAtOnce=" + receivingInbandDataAtOnce);
        }
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            if (!listeners.contains(item)) {
                listeners.add(item);
            } else {
                // Removes a previous listener.
                listeners.remove(item);
                // Adds a new listener.
                listeners.add(item);
            }

            if(receivingInbandDataAtOnce) {
                if (currentState == STATE_RECEIVEING_DATA) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug(LOG_HEADER + "Already started receiving data so requests to "
                                + "download directly");
                    }
                    // Already receiving data.
                    Thread directlyNotifyThread = new Thread("directlyNotifyThread") {
                        public void run() {
                            String sourceId = DataManager.getInstance().getSourceIdForInbandData();
                            try {
                                String url = MonitorMain.IB_EMULATION ? null : ("ocap://0x" + Integer.toHexString(Integer.parseInt(sourceId)));
                                l.receiveInbandData(url);
                            } catch (Exception e) {
                                Log.print(e);
                            }
                        }
                    };
                    directlyNotifyThread.start();
                }
            }

        }
        status.repaint();
    }

    /**
     * Removes a listener.
     * @param l the listener instance.
     * @param applicationName the requested application name.
     */
    public void removeInbandDataListener(InbandDataListener l, String applicationName) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "removeInbandDataListener=" + applicationName);
        }
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            listeners.remove(item);
        }
        status.repaint();
    }

    /**
     * The applications must call this interface when application completes receiving the Inband data. When all
     * applications that receiving the Inband data call this interface, the Monitor application can perform next process
     * in boot state.
     * @param applicationName The application name.
     */
    public synchronized void completeReceivingData(String applicationName) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "completeReceivingData() " + applicationName + ", receivingInbandDataAtOnce=" + receivingInbandDataAtOnce
                    + "currentState=" + currentState);
        }

        if(currentState == STATE_PAUSED) {
        	// finishJob() 이후의 호출 들은 무시 한다.
        	return;
        }

        boolean allComplete = true;     // for receivingInbandDataAtOnce is true only.
        synchronized (listeners) {
        Iterator iterator = listeners.iterator();
            while(iterator.hasNext()) {
                ListenerItem item = (ListenerItem) iterator.next();
                if (item.getApplicationName().equals(applicationName)) {
                    item.setReceiveComplete(true);
                }
                if (!item.isReceiveComplete()) {
                    allComplete = false;
                }

                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "appName=" + item.applicationName + "isComplete="
                            + item.isReceiveComplete());
                }
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "==========================================");
        }

        status.repaint();

        if(!receivingInbandDataAtOnce) {
            nextNotification();
        }
        else {
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "allComplete " + allComplete);
            }

            if (allComplete) {
                finishJob(true);
            }
        }

    }

    /**
     * Finishs the job in pvr.
     * This method is called only PVR.
     * When NetworkInterface tuner is stolen by other application, InbandDataUpdateController call this method.
     */
    public synchronized void finishJobPvr() {
    	// PVR STB 에서 Network Interface Tuning 성공해서 받다가, Tuner를 뺏긴 경우
    	// 여기가 호출 된다. 이 때는 조용히 중단 하면 된다.
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "finishJobPvr() currentState=" + currentState);
        }
    	stopTimer();
    	currentState = STATE_PAUSED;
    }

    /**
     * Finishs the job.
     * @param success true if finished success.
     */
    private synchronized void finishJob(boolean success) {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "finishJob() currentState=" + currentState + ", PVR:" + Environment.SUPPORT_DVR);
        }

    	stopTimer();

    	//if(currentState != STATE_RECEIVEING_DATA) {
    	//	return;
    	//}

        currentState = STATE_PAUSED;
        if(!Environment.SUPPORT_DVR) {
            stopServiceContext();
        } else {
            //->Kenneth[E* : 2014.12.5] : booting sequence 이면 SC stop 을 호출한다. 그래야 tuner 를 릴리즈해서
            // standby 시 부팅되는 경우 tuner 를 turn off 할 수 있음.
            int stbMode = STBModeManager.getInstance().getMode();
            Log.printDebug(LOG_HEADER+"stbMode = "+stbMode);
            if(InbandDataUpdateController.getInstance() != null && 
                    InbandDataUpdateController.getInstance().isUpdateUsingServiceContext()) {
                Log.printDebug(LOG_HEADER+"isUpdateUsingServiceContext() returns true, so call SC.stop()");
                stopServiceContext();
            } else if (stbMode == MonitorService.STB_MODE_BOOTING) {
                // 부팅중 IB 데이터 받은 것이면 SC 를 stop 해준다.
                Log.printDebug(LOG_HEADER+"STB Mode is BOOTING. So call SC.stop() for E*");
                stopServiceContext();
            }
        }

        if(internalInbandDataListener != null) {
            internalInbandDataListener.finishReceivingData();
        }
    }
    
    private void stopServiceContext() {
        if(serviceContext != null) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "serviceContext#stop()");
            }
            serviceContext.stop();
        }
    }

    /**
     * Checks validation of current SI data.
     * @param channelSourceId the source id for inband data channel.
     * @return true if exist service given source id.
     */
    private boolean checkSIValidation(String channelSourceId) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "checkSIValidation() sourceId=" + channelSourceId);
        }
        status.updateLog("checkSIValidation : " + channelSourceId);
        int sourceId = Integer.MAX_VALUE;
        try {
            sourceId = Integer.parseInt(channelSourceId);
        } catch (NumberFormatException nfe) {
            Log.print(nfe);
            return false;
        }
        SIManager siManager = SIManager.createInstance();
        ServiceList serviceList = siManager.filterServices(null);

        if (serviceList == null || serviceList.size() < 1) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            Service service = serviceList.getService(i);
            int sId = ((OcapLocator) service.getLocator()).getSourceID();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "service(" + i + ") sourceId=" + sId);
            }
            if (sourceId == sId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Starts receiving data.
     * @param l
     */
    public synchronized void startReceivingData(InternalInbandDataListener l) {
    	updateTryCount ++;
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startReceivingData() l=" + l + ", updateTryCount=" + updateTryCount);
        }
        status.updateLog("startReceivingData.");
        this.internalInbandDataListener = l;
        // Data 를 수신 할 것이기 때문에 flag를 reset 한다.
        DataVersionManager.getInstance().clearHasUpdatedFile();
        tuneToDataChannel();
    }

    public synchronized void startNotificationPVR(InternalInbandDataListener l) {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startNotificationPVR() l=" + l);
        }
    	this.internalInbandDataListener = l;
    	startNotification();
    }

    private synchronized void startNotification() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startNotification() receivingInbandDataAtOnce=" + receivingInbandDataAtOnce);
        }

        // Listener 가 하나도 없다면 즉시 중단 한다.
        if(listeners.size() < 1) {
        	finishJob(false);
        }

        // Set timer
        startTimer();

        if(!receivingInbandDataAtOnce) {
            // Reset all application's state
            synchronized (listeners) {
                Iterator iterator = listeners.iterator();
                while(iterator.hasNext()) {
                    ListenerItem item = (ListenerItem) iterator.next();
                    item.setReceiveComplete(false);
                }
            }

            currentState = STATE_RECEIVEING_DATA;
            status.time = System.currentTimeMillis();
            status.updateLog("tuned to data channel");

            nextNotification();
        }
        else {
            Object[] lsnrs;
            // Reset all application's state
            synchronized (listeners) {
                Iterator iterator = listeners.iterator();
                while(iterator.hasNext()) {
                    ListenerItem item = (ListenerItem) iterator.next();
                    item.setReceiveComplete(false);
                }
                lsnrs = listeners.toArray();
            }
            currentState = STATE_RECEIVEING_DATA;
            String sourceId = DataManager.getInstance().getSourceIdForInbandData();
            int iSourceId = Integer.MAX_VALUE;
            try {
                iSourceId = Integer.parseInt(sourceId);
            }
            catch (NumberFormatException nfe) {
                Log.print(nfe);
            }
            final String url = MonitorMain.IB_EMULATION ? null : ("ocap://0x" + Integer.toHexString(iSourceId));
            status.time = System.currentTimeMillis();
            status.updateLog("tuned to data channel");
            for (int i = 0; i < lsnrs.length; i++) {
                final Object tempListener = lsnrs[i];
                Thread t = new Thread("InbandDataReceive" + i) {
                    public void run() {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(LOG_HEADER + "notify to "
                                    + ((ListenerItem) tempListener).listener);
                        }
                        try {
                            ((ListenerItem) tempListener).listener.receiveInbandData(url);
                        } catch (Throwable th) {
                            Log.print(th);
                        }
                    }
                };
                t.start();
            }
            //->Kenneth : 2015.1.8 : VDTRMASTER-5329 : 가끔 업데이트 안되는 문제가 발생
            // 데이터 받기 전에 finishJob 이 불려서 tune 으로 인해 데이터 못받는 경우 있음.
            // 따라서 list_behavior 관련 데이터도 다른 App 이 하는 mechanism 에 넣기로 함.
            // Kenneth : 2014.9.25 : R4.1 에 의해서 Monitor 도 Inband 데이터를 받을 필요가 있음.
            // list_behavior.txt 와 list_behavior_images.zip 이 그것임
            /*
            Thread t2 = new Thread("Monitor IB data receiver") {
                public void run() {
                    Log.printDebug(LOG_HEADER + "Load Monitor IB data");
                    try {
                        loadMyIBData(url);
                    } catch (Throwable th) {
                        Log.print(th);
                    }
                }
            };
            t2.start();
            */
        }
    }

    //->Kenneth : 2015.1.8 : VDTRMASTER-5329 : 가끔 업데이트 안되는 문제가 발생
    class InbandDataListenerImpl implements InbandDataListener {
        public void receiveInbandData(String locator) {
            Log.printDebug("Kenneth : receivedInbandData("+locator+") called");
            Log.printDebug("Kenneth : call loadMyIBData()");
            loadMyIBData(locator);
            Log.printDebug("Kenneth : call completeReceivingData()");
            completeReceivingData(FrameworkMain.getInstance().getApplicationName());
        }
    }

    private DataUpdateListenerImpl updateListenerImpl;
    // Kenneth : IB 데이터를 로드한다
    private void loadMyIBData(String locator) {
        Log.printDebug("Kenneth : Before asynchrousLoad("+locator+")");
        if (updateListenerImpl == null) {
            updateListenerImpl = new DataUpdateListenerImpl();
        }
        // LIST_BEHAVIOR_TXT 나 LIST_BEHAVIOR_IMG 는 ib.prop 에서 사용하고 있는 key 값임
        DataCenter.getInstance().addDataUpdateListener("LIST_BEHAVIOR_TXT", updateListenerImpl);
        DataCenter.getInstance().addDataUpdateListener("LIST_BEHAVIOR_IMG", updateListenerImpl);
	    DataAdapterManager.getInstance().getInbandAdapter().asynchronousLoad(locator);
        Log.printDebug("Kenneth : After asynchrousLoad("+locator+")");
    }

    // Kenneth : IB 데이터가 로드 혹은 업데이트 되면 DataManager 에 의해서 호출된다.
    class DataUpdateListenerImpl implements DataUpdateListener {
        public void dataUpdated(String key, Object old, Object value) {
            Log.printDebug("Kenneth : dataUpdated("+key+")");
            if (key.equals("LIST_BEHAVIOR_TXT")) {
                ListBehavior.getInstance().resetTexts(value);
            } else if (key.equals("LIST_BEHAVIOR_IMG")) {
                ListBehavior.getInstance().resetImage(value);
            }
        }

        public void dataRemoved(String key) {
        }
    }

    /**
     *
     */
    private synchronized void nextNotification() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "nextNotification()");
            synchronized (listeners) {
                Iterator iterator = listeners.iterator();
                while(iterator.hasNext()) {
                    ListenerItem item = (ListenerItem) iterator.next();
                    if (Log.DEBUG_ON) {
                        Log.printDebug(LOG_HEADER + "appName=" + item.applicationName + "isComplete="
                                + item.isReceiveComplete());
                    }
                }
            }

            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "==========================================");
            }
        }

        String sourceId = DataManager.getInstance().getSourceIdForInbandData();
        final String url = "ocap://0x" + Integer.toHexString(Integer.parseInt(sourceId));

        boolean allComplete = true;
        synchronized (listeners) {
            Iterator iterator = listeners.iterator();
            while(iterator.hasNext()) {
                final ListenerItem nextListener = (ListenerItem)iterator.next();
                if(!nextListener.isReceiveComplete()) {
                    allComplete = false;
                    Thread t = new Thread("InbandDataReceive" + nextListener.applicationName) {
                        public void run() {
                            if (Log.DEBUG_ON) {
                                Log.printDebug(LOG_HEADER + "notify to "
                                        + nextListener.listener);
                            }
                            try {
                                nextListener.listener.receiveInbandData(url);
                            } catch (Throwable th) {
                                Log.print(th);
                            }
                        }
                    };
                    t.start();
                    break;
                }
            }
        }

        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "allComplete=" + allComplete);
        }
        if(allComplete) {
            finishJob(true);
        }
    }

    /**
     * Starts the receiving data.
     */
    private synchronized void tuneToDataChannel() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "tuneToDataChannel()");
        }
        if (MonitorMain.IB_EMULATION) {
            Log.printInfo(LOG_HEADER + "tuneToDataChannel() : EMULATOR : Go directly startNotification()");
            startNotification();
            return;
        }
        String sourceId = DataManager.getInstance().getSourceIdForInbandData();

        int retryCount = 0;
        while (true) {
            boolean existSI = checkSIValidation(sourceId);
            retryCount++;
            if (MAX_COUNT_FIND_SI_DATA < retryCount) {
                if (Log.ERROR_ON) {
                    Log.printError("ERROR : Giveup finding service for inband data channel. updateTryCount=" + updateTryCount);
                }
                if(updateTryCount == 1) {
                	STBModeManager.getInstance().setMode(MonitorService.STB_MODE_STANDALONE);
                }
                finishJob(false);
                return;
            }

            if (existSI) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("Found service for inband data channel so try service selection.");
                }
                break;
            } else {
                if (Log.WARNING_ON) {
                    Log.printWarning(LOG_HEADER + "SI Data is not complete so wait and retry "
                            + "SI Data's validation.");
                }

                try {
                    Thread.sleep(WAIT_TIME_FOR_CHECK_SI_DATA);
                } catch (Exception e) {
                    Log.print(e);
                }
            }
        }

        // Tune to data channel.
        if (serviceContext == null) {
            serviceContext = Environment.getServiceContext(0);
        }

        OcapLocator[] locators = new OcapLocator[1];
        try {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "select service sourceId=" + sourceId);
            }
            serviceContext.addListener(this);
            OcapLocator locator = new OcapLocator(Integer.parseInt(sourceId));
            locators[0] = locator;
            serviceContext.select(locators);
        } catch (Throwable t) {
        	if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "Error occurs while service selection.");
            }
            Log.print(t);
            try {
            	serviceContext.removeListener(this);
            } catch (Exception e) {
            	Log.print(e);
            }
            finishJob(false);
            return;
        }
    }

    /**
     * Implementation of ServiceContextListener.
     * @param paramServiceContextEvent the ServiceContextEvent instance.
     */
    public void receiveServiceContextEvent(ServiceContextEvent paramServiceContextEvent) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "receiveServiceContextEvent() " + paramServiceContextEvent);
        }
        status.updateLog("ServiceContextEvent : " + countForServiceSelection + " : " + paramServiceContextEvent);
        synchronized (this) {
            serviceContext.removeListener(this);
            if (paramServiceContextEvent instanceof NormalContentEvent) {
                startNotification();
            } else {
                // This case is fail to service selection so retry service selection.
                if (Log.WARNING_ON) {
                    Log.printWarning("ERROR : InbandDataManager: fail to service selection. countForServiceSelection="
                                    + countForServiceSelection);
                }
                countForServiceSelection++;
                if (countForServiceSelection > MAX_COUNT_SERVICE_SELECTION || MonitorMain.IB_EMULATION) {
                    if (Log.ERROR_ON) {
                        Log.printError("ERROR : InbandDataManager: give up service selection. "
                                + "countForServiceSelection=" + countForServiceSelection + ",updateTryCount=" + updateTryCount);
                    }
                    if(updateTryCount == 1) {
                    	STBModeManager.getInstance().setMode(MonitorService.STB_MODE_STANDALONE);
                    }
                    finishJob(false);
                } else {
                    if (Log.WARNING_ON) {
                        Log.printWarning("ERROR : InbandDataManager: Retry service selection.");
                    }

                    Thread retryThread = new Thread("Retry Inband tune") {
                        public void run() {
                            try {
                                Thread.sleep(WAIT_TIME_FOR_SERVICE_SELECTION);
                                tuneToDataChannel();
                            } catch (Exception e) {
                                Log.print(e);
                            }
                        }
                    };
                    retryThread.start();
                }

            }
        }
    }

    /**
     * Starts timeout timer.
     */
    private void startTimer() {
		stopTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "startTimer() ");
		}
		try {
			String timeoutS = DataManager.getInstance().getIBUpdateTimeOut();
			long timeOut = Integer.parseInt(timeoutS) * 1000L;
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "waitTime=" + timeOut);
			}
			timerSpec.setDelayTime(timeOut);
			timerSpec.setRepeat(false);
			timerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(timerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

    /**
     * Stops timout timer.
     */
	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "stopTimer() ");
		}
		timerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(timerSpec);
	}

	/**
	 * This is timer call back.
	 *
	 * @param event
	 *            TVTimer event.
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "timerWentOff() ");
		}
        // 정상 상황은 아니지만 start vod scan에서 block되어 timer가 stuck 되는 경우를 방지
        new Thread("finishJob.timeout") {
            public void run() {
		        finishJob(false);
            }
        }.start();
	}

    /**
     * This class has the unique application id and the instance of ServiceStateListener.
     * @author Woosik Lee
     */
    private class ListenerItem {
        /** The instance of listener. */
        private InbandDataListener listener;
        /** The requested application name. */
        private String applicationName;
        /** The flag for receiving state. */
        private boolean receiveComplete;

        private long time;
        /**
         * Constructor.
         * @param l listener.
         * @param appName application name.
         */
        public ListenerItem(final InbandDataListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItem) {
                String appName = ((ListenerItem) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }

        /**
         * Returns the flag that is state of receiving data.
         * @return the flag.
         */
        public boolean isReceiveComplete() {
            return receiveComplete;
        }

        /**
         * Sets the state of receiving data.
         * @param state the flag.
         */
        public void setReceiveComplete(boolean state) {
            receiveComplete = state;
            if (receiveComplete) {
                time = System.currentTimeMillis();
            }
        }

        /**
         * Returns the application name.
         * @return application name.
         */
        public String getApplicationName() {
            return applicationName;
        }

        /**
         * Override the toString() in Object class.
         * @see toString in class java.lang.Object#toString()
         * @return class message.
         */
        public String toString() {
            return "[" + applicationName + ", " + receiveComplete;
        }
    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void newDatabase(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryAdded(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * This method is called when the bound application is deleted at the AppsDatabase. If the
     * {@link ServiceStateManager} has the bound application's listener, it must delete.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryRemoved(AppsDatabaseEvent paramAppsDatabaseEvent) {
        synchronized (listeners) {
            AppID appId = paramAppsDatabaseEvent.getAppID();
            AppsDatabase db = AppsDatabase.getAppsDatabase();
            AppAttributes appAtt = db.getAppAttributes(appId);
            String appName = appAtt.getName();
            if (Log.INFO_ON) {
                Log.printInfo("ServiceStateManager: entryRemoved() appName=" + appName);
            }

            ListenerItem item = new ListenerItem(null, appName);
            listeners.remove(item);
            status.repaint();
        }
    }

    /**
     * Implements the AppsDatabaseEventListener's method.
     * @param paramAppsDatabaseEvent the event.
     */
    public void entryChanged(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    public class Status extends java.awt.Component {

        com.alticast.ui.LayeredUI ui;
        long time = System.currentTimeMillis();
        javax.tv.xlet.XletContext xc = com.videotron.tvi.illico.framework.FrameworkMain.getInstance().getXletContext();
        String log = "waiting....";

        public Status() {
            com.alticast.ui.LayeredWindow window = new com.alticast.ui.LayeredWindow();
            window.setBounds(0, 0, 960, 540);
            window.setVisible(true);
            window.add(this);
            ui = com.alticast.ui.LayeredUIManager.getInstance().createLayeredWindow("Monitor,Debug", 20000, false, window, new java.awt.Rectangle(0, 0, 960, 540));
            this.setVisible(true);
            this.setBounds(0, 0, 960, 540);
        }

        public void start() {
            this.setVisible(true);
            ui.activate();
        }

        public void stop() {
            if (ui.isActive()) {
                this.setVisible(false);
                ui.deactivate();
                long sec = (System.currentTimeMillis() - time) / 1000L;
                com.videotron.tvi.illico.framework.DataAdapterManager.getInstance().getBuiltInFileAdapter().updateLog(
                            Log.getPrefix(), 0, "In-band data loading & booting time = " + sec + " s");
            }
        }

        public void updateLog(String log) {
            this.log = log;
            repaint();
        }

        java.awt.Color bg = new org.dvb.ui.DVBColor(0, 0, 255, 70);
        java.awt.Font f = com.videotron.tvi.illico.util.FontResource.BLENDER.getFont(18);
        public void paint(java.awt.Graphics g) {
            if (!Log.INFO_ON) {
                return;
            }
            g.setColor(bg);
            g.fillRect(0, 0, 960, 540);
            g.setFont(f);
            g.setColor(java.awt.Color.green);
            long sec = (System.currentTimeMillis() - time) / 1000L;
            g.drawString("This is the debug infomation of Monitor App. <version = "
                + com.videotron.tvi.illico.framework.FrameworkMain.getInstance().getApplicationVersion() + ">", 90, 50);
            g.drawString(new java.util.Date() + " : " + log, 90, 70);
            g.drawString(sec + " s", 90, 90);
            int y = 120;
            for (int i = 0; i < listeners.size(); i++) {
                ListenerItem item = (ListenerItem) listeners.get(i);
                boolean b = item.isReceiveComplete();
                if (b) {
                    g.setColor(java.awt.Color.magenta);
                    g.drawString("COMPLETE :  " + item.applicationName + " : time = " + ((item.time - time) / 1000L + " s"), 100, y);
                } else {
                    g.setColor(java.awt.Color.yellow);
                    g.drawString("receiving : " + item.applicationName + " : time = " + sec, 100, y);
                }
                y = y + 19;
            }

            g.setColor(java.awt.Color.white);
            String[] list = org.dvb.io.ixc.IxcRegistry.list(xc);
            if (list != null) {
                y = 120;
                for (int i = 0; i < list.length; i++) {
                    g.drawString(list[i], 550, y);
                    y += 19;
                }
            }
        }
    }

    public Status status = new Status();
    {
        status.start();
    }
}

package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.util.StringTokenizer;
import java.util.Vector;

import org.davic.net.tuning.NetworkInterface;
import org.davic.net.tuning.NetworkInterfaceEvent;
import org.davic.net.tuning.NetworkInterfaceListener;
import org.davic.net.tuning.NetworkInterfaceManager;
import org.davic.net.tuning.NetworkInterfaceTuningOverEvent;
import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.monitor.ui.popup.*;
import com.videotron.tvi.illico.util.WindowProperty;
//->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;

/**
 * Weak signal controller.
 * This class checks RF Signal's status and provides the popup.
 * @author WSLEE1
 *
 */
public class WeakSignalController implements NetworkInterfaceListener, LayeredKeyHandler {

	/** The singleton instance of WeakSignalController. */
	private static WeakSignalController instance;

	/** Popup. */
	private BasicPopup popup;
	/** The flag popup's visibility. */
	private boolean isShownErrorPopup;

	/** The log header message. */
	private static final String LOG_HEADER = "[WeakSignalController] ";

    //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
	private BasicDialog noSignalPopup;
	/**
	 * Returns the singleton instance of WeakSignalController.
	 * @return instance of WeakSignalController.
	 */
	public synchronized static WeakSignalController getInstance() {
		if (instance == null) {
			instance = new WeakSignalController();
		}
		return instance;
	}

	/**
	 * The constructor.
	 */
	private WeakSignalController() {
		NetworkInterface[] nis = NetworkInterfaceManager.getInstance().getNetworkInterfaces();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "monitorNetworkInterface() # of network interfaces="
					+ nis.length);
		}
		for (int i = 0; i < nis.length; i++) {
			nis[i].addNetworkInterfaceListener(this);
		}
		isShownErrorPopup = false;
	}

	public void receiveNIEvent(NetworkInterfaceEvent anEvent) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "receiveNIEvent() is called. isShownErrorPopup = " + isShownErrorPopup);
		}

		if (anEvent instanceof NetworkInterfaceTuningOverEvent) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "STB mode is " + STBModeManager.getInstance().getMode());
			}

			if (((NetworkInterfaceTuningOverEvent) anEvent).getStatus() == NetworkInterfaceTuningOverEvent.FAILED) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "receiveNIEvent() NetworkInterfaceTuningOverEvent.FAILED");
				}

				switch (STBModeManager.getInstance().getMode()) {
				case MonitorService.STB_MODE_STANDALONE:
					hidePopup();
					break;
				case MonitorService.STB_MODE_NO_SIGNAL:
					showNoSignalPopup();
					break;
				case MonitorService.STB_MODE_MANDATORY_BLOCKED :
					// In the Mandatory blocked, does not show error message because Mandatory blocked error message is already appeared.
					break;
//				default:
//					switch(ServiceStateManager.getInstance().getState()) {
//					case MonitorService.PIP_STATE:
//					case MonitorService.TV_VIEWING_STATE:
//						boolean isSpecialVideoState = MonitorStateManager.getInstance().isSpecialVideoState();
//						if (Log.INFO_ON) {
//							Log.printInfo(LOG_HEADER + "isSpecialVideoState: " + isSpecialVideoState);
//						}
//						// Dose not show popup in special video state, because that state is PVR or VOD.
//						if(!isSpecialVideoState) {
//                            if (Host.getInstance().getPowerMode() != Host.LOW_POWER) {
//                                showErrorMessage();
//                            }
//						}
//						break;
//					case MonitorService.FULL_SCREEN_APP_STATE:
//						// No action
//						break;
//					}
				}

			} else if (((NetworkInterfaceTuningOverEvent) anEvent).getStatus() == NetworkInterfaceTuningOverEvent.SUCCEEDED) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "receiveNIEvent() NetworkInterfaceTuningOverEvent.SUCCEEDED");

				}
				switch (STBModeManager.getInstance().getMode()) {
				case MonitorService.STB_MODE_STANDALONE:
					showRebootPopup();
					break;
				case MonitorService.STB_MODE_NO_SIGNAL:
					showRebootPopup();
					break;
				case MonitorService.STB_MODE_MANDATORY_BLOCKED:
					// In the Mandatory blocked, does not show error message because Mandatory blocked error message is already appeared.
					break;
//				default:
//					hideErrorMessage();
				}
			}
		}
	}

//	/**
//	 * Shows the message popup using the ErrorMessage.
//	 */
//	private void showErrorMessage() {
//		isShownErrorPopup = true;
//		CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_500, null);
//	}
//
//	/**
//	 * Hides the popup.
//	 */
//	private void hideErrorMessage() {
//		if(isShownErrorPopup) {
//			isShownErrorPopup = false;
//			CommunicationManager.getInstance().hideErrorMessage();
//		}
//	}

	/**
	 * For only the No Signal mode.
	 * It is called when RF signal is connected in No Signal mode.
	 */
	private void showRebootPopup() {
		hidePopup();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "showRebootPopup()");
		}

		String msg = DataCenter.getInstance().getString(KeyNames.POPUP_REBOOT_AT_STANDALONE_MODE);
		StringTokenizer stk = new StringTokenizer(msg, "|");
		Vector v = new Vector();
		while (stk.hasMoreElements()) {
			v.add(stk.nextToken());
		}
		String[] message = new String[v.size()];
		for (int i = 0; i < message.length; i++) {
			message[i] = (String) v.get(i);
		}

		String title = DataCenter.getInstance().getString(KeyNames.POPUP_TITLE_NOTIFICATION);

		synchronized (this) {
			WindowProperty wp = WindowProperty.WEAK_SIGNAL_POPUP;
			popup = new BasicConfirm(title, wp.getName(), wp.getPriority(), message, this);
			popup.showPopup();
		}
	}

	/**
	 * Shows the No Signal popup without via ErrorMessage(MOT-500).
	 * It is called when STB enter No Signal Mode.
	 */
	public void showNoSignalPopup() {
        //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
		hidePopup();
	    if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup()");
		String msg = DataCenter.getInstance().getString(KeyNames.NOSIGNAL_MSG);
		StringTokenizer stk = new StringTokenizer(msg, "|");
		Vector v = new Vector();
		while (stk.hasMoreElements()) {
			v.add(stk.nextToken());
		}
		String[] message = new String[v.size()];
		for (int i = 0; i < message.length; i++) {
			message[i] = (String) v.get(i);
            if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup() : msg : "+message[i]);
		}
		String title = DataCenter.getInstance().getString(KeyNames.NOSIGNAL_TITLE);
        if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup() : title : "+title);
		String close = DataCenter.getInstance().getString(KeyNames.NOSIGNAL_CLOSE);
        if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup() : close : "+close);
		String restart = DataCenter.getInstance().getString(KeyNames.NOSIGNAL_RESTART);
        if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup() : restart : "+restart);
        LayeredKeyHandlerImpl layeredKeyHandler = new LayeredKeyHandlerImpl();
		synchronized (this) {
			WindowProperty wp = WindowProperty.WEAK_SIGNAL_POPUP;
			noSignalPopup = new BasicDialog(title, wp.getName(), wp.getPriority(), message, layeredKeyHandler, close, restart);
			noSignalPopup.showPopup();
		}
        //<-

        /*
		hidePopup();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "showNoSignalPopup()");
		}

		String msg = DataCenter.getInstance().getString(KeyNames.POPUP_NO_SIGNAL_MODE);
		StringTokenizer stk = new StringTokenizer(msg, "|");
		Vector v = new Vector();
		while (stk.hasMoreElements()) {
			v.add(stk.nextToken());
		}
		String[] message = new String[v.size()];
		for (int i = 0; i < message.length; i++) {
			message[i] = (String) v.get(i);
		}

		String title = DataCenter.getInstance().getString(KeyNames.POPUP_TITLE_NOTIFICATION);

		synchronized (this) {
			WindowProperty wp = WindowProperty.WEAK_SIGNAL_POPUP;
			popup = new BasicPopup(title, wp.getName(), wp.getPriority(), message);
			popup.showPopup();
		}
        */
	}

    //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
	private class LayeredKeyHandlerImpl implements LayeredKeyHandler {
		public boolean handleKeyEvent(UserEvent event) {
			if (event.getType() != KeyEvent.KEY_PRESSED) {
				return true;
			}

			int keyCode = event.getCode();

			if (keyCode == HRcEvent.VK_POWER) {
				return false;
			}

			switch (keyCode) {
                case OCRcEvent.VK_LEFT:
                    noSignalPopup.setFocusBtn(true);
                    break;
                case OCRcEvent.VK_RIGHT:
                    noSignalPopup.setFocusBtn(false);
                    break;
                case OCRcEvent.VK_ENTER:
                    if (noSignalPopup.getFocusBtn()) {
                        hidePopup();
                    } else {
                        hidePopup();
                        if (Log.DEBUG_ON) Log.printDebug(LOG_HEADER + "showNoSignalPopup() : Call reboot()");
                        Host.getInstance().reboot();
                    }
                    break;
                default:
			}
			return true;
		}
	}
    //<-

	/**
	 * Hides the popup in No Signal mode.
	 */
	private void hidePopup() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "hidePopup()");
		}
		synchronized (this) {
			if (popup != null) {
				popup.hidePopup();
				popup.dispose();
				popup = null;
			}
            //->Kenneth[2015.6.6] VDTRMASTER-5428 쥴리 요구에 의한 improvement
			if (noSignalPopup != null) {
				noSignalPopup.hidePopup();
				noSignalPopup.dispose();
				noSignalPopup= null;
			}
            //<-
		}
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "userEventReceived() event=" + event);
		}

		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = event.getCode();
		switch (keyCode) {
		case OCRcEvent.VK_ENTER:
			Host.getInstance().reboot();
			return true;
		default:
			break;
		}

		return true;
	}

}

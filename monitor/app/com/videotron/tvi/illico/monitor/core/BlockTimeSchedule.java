package com.videotron.tvi.illico.monitor.core;

import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.TimeZone;

import com.videotron.tvi.illico.log.Log;

public class BlockTimeSchedule {

    private int scheduleId;

    private int startHH;
    private int startMM;

    private int endHH;
    private int endMM;

    /** The week informations, [0]=Sun, [1]=Mon, [2]=Tue ... [6]=Sat. */
    private boolean[] repeatWeek;

    private long startTimeMillis;

    private long endTimeMillis;

    private long currentScheduleStartTime;

    private long currentScheduleEndTime;

    /**
     * 다음 Schedule 계산시 무시해야할 시간. 화면에 한번 보여 줬거나 Notification 에서 취소한 경우 해당 시간대에 속하는 schedule
     * 의 startTimeMillis 를 세팅 해서 다음 계산시 무시 하도록 한다.
     * 주의 : Standby 갔다가 오면 다시 Reset 해 줘야 한다.
     */
    private long lastShownScheduleTime;

    private static final String LOG_HEADER = "[WTRM-Schedule] ";

    public BlockTimeSchedule(int id, String startHHMM, String endHHMM, String repeat) {
        this.scheduleId = id;

        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "startHHMM=" + startHHMM + ",endHHMM=" + endHHMM + ", repeat="
                    + repeat);
        }

        String hh = startHHMM.substring(0, 2);
        String mm = startHHMM.substring(2, 4);
        startHH = Integer.parseInt(hh);
        startMM = Integer.parseInt(mm);
        startTimeMillis = addTime(0, 0, startHH, startMM);

        hh = endHHMM.substring(0, 2);
        mm = endHHMM.substring(2, 4);
        endHH = Integer.parseInt(hh);
        endMM = Integer.parseInt(mm);
        endTimeMillis = addTime(0, 0, endHH, endMM);

        repeatWeek = new boolean[7];
        StringTokenizer st = new StringTokenizer(repeat, "|");
        int count = 0;
        while (st.hasMoreElements()) {
            String token = st.nextToken();
            if ("Yes".equalsIgnoreCase(token)) {
                repeatWeek[count] = true;
            } else {
                repeatWeek[count] = false;
            }
            count++;
        }
    }

    /**
     * 1. Notification 에서 Cancel 했을 때 해당 되는 Schedule 의 시간.
     * 2. Time Blocked 에서 PIN 입력 해서 Free된 시간.
     * 3. Standby 로 가게 되면 -1 을 세팅.
     * @param pLastShownScheduleTime
     */
    public void setLastShownScheduleTime(long pLastShownScheduleTime) {
    	if (Log.INFO_ON) {
    		Log.printInfo(LOG_HEADER + "setLastShownScheduleTime() : " + pLastShownScheduleTime);
    	}
    	this.lastShownScheduleTime = pLastShownScheduleTime;
    }

    /**
     * Computes the next schedule. 시간을 전달 하면 다음 스케줄을 리턴 한다.
     * @param requestedTime The requested time. (unit is millisecond).
     * @param ignoreNearestSchedule 가장 가까운 스케줄 하나를 무조건 무시 한다.
     *          다음의 경우 ignoreNearestSchedule 가 true 이다.
     *          1. 제한 시간 들어가기 전에 Notification 에서 Cancel해 버리는 경우 해당 스케줄은 무조건 무시 되어야 하기 때문이다.
     *          2. 제한 시간 중에 PIN (UPP PIN Enabler) 을 풀면 다시 Schedule 을 계산 한다. 이때도 무시 되어야 한다.
     *          다음의 경우 ignoreNearestSchedule 가 -1 이다.
     *          1. 스케줄 설정값이 변경 되었을 경우.
     *          2. 부팅후 최초로 계산 하는 경우.
     *          3. 제한 시간이 모두 경과 해서 다시 schedule 을 계산 하는 경우.
     * @return weak start time for day light saving.
     */
    public long findNextSchedule(long requestedTime) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "getNextSchedule() requestedTime=" + requestedTime
                    + ",lastShownScheduleTime=" + lastShownScheduleTime);
        }
        // requestedTime 기준으로 가장 가까운 혹은 requestedTime 이 포함 된 스케줄을 찾는다.
        Calendar reqTime = Calendar.getInstance();
        reqTime.setTimeInMillis(requestedTime);
        // int requestedWeek = reqTime.get(Calendar.DAY_OF_WEEK) - 1;
        // SUN : 1, MON : 2, TUE : 3, WED : 4, THU : 5, FRI : 6, SAT : 7
        // http://download.oracle.com/javase/1.4.2/docs/api/constant-values.html#java.util.Calendar.SUNDAY

        // requestedTime 기준으로 금주의 시작인 일요일 00시 00 분을 먼저 계산 한다.
        long weekStartTime = findWeekStartTime(reqTime);

        long[] startTimes = new long[7];
        long[] endTimes = new long[7];
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                // Do not compute time if not checked day of week.
                startTimes[i] = addTime(weekStartTime, i, startHH, startMM);
                if (endTimeMillis > startTimeMillis) {
                    // 24시간 이내에 start / end 가 있는 경우.
                    endTimes[i] = addTime(weekStartTime, i, endHH, endMM);
                } else {
                    // 이틀에 걸쳐서 start / end 가 있는 경우. 이 경우 end 를 다음날로 설정 해야 한다.
                    endTimes[i] = addTime(weekStartTime, i + 1, endHH, endMM);
                }
            }
        }

        if (Log.DEBUG_ON) {
            for (int i = 0; i < 7; i++) {
                if (repeatWeek[i]) {
                    printTime(startTimes[i], "SCHEDULE[" + i + "] Start Time : ");
                    printTime(endTimes[i], "SCHEDULE[" + i + "] End Time : ");
                }
            }
        }



        // Finds the nearest schedule using requestTime without lastShownScheduleTime.
        int findSchedule = -1;
        for(int i=0; i<7; i++) {
        	if(repeatWeek[i]) {
        		if(requestedTime < endTimes[i]) {
        			// 현재 시간 기준 가장 가까운 스케줄이다.

        			// lastShownScheduleTime 를 판단해서 무시 할지 판단 한다.
        			if(startTimes[i] <= lastShownScheduleTime && endTimes[i] >= lastShownScheduleTime) {
        				// 한번 보여줬던 스케줄이다. 무시 하자.
        			}
        			else {
        				findSchedule = i;
        				break;
        			}
        		}
        	}
        }


        /*
        boolean alreadyIgnoreNearestSchedule = false;
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                if (requestedTime < endTimes[i]) {
                    if (ignoreNearestSchedule) {
                        // ignoreNearestSchedule 이라면 가장 처음 나오는 스케줄을 무시해야 한다.
                        if(alreadyIgnoreNearestSchedule) {
                            findSchedule = i;
                            break;
                        }
                        else {
                            alreadyIgnoreNearestSchedule = true;
                        }
                    }
                    else {
                        // ignoreNearestSchedule 이 아니라면 가장 가까운 스케줄을 리턴 하면 된다.
                        findSchedule = i;
                        break;
                    }
                }
            }
        }
        */

        if (findSchedule > -1) {
            currentScheduleStartTime = startTimes[findSchedule];
            currentScheduleEndTime = endTimes[findSchedule];
            return weekStartTime;
        }

        // 금주내에서 Schedule 을 찾지 못했다. 그렇다면 다음주의 가장 첫 번째 스케줄을 리턴 하면 된다.
        for (int i = 0; i < 7; i++) {
            if (repeatWeek[i]) {
                // startTimes[i], endTimes[i] 가 이미 세팅 되어 있다. 그래서 이 값에다가 7일만 더하면 된다.
                currentScheduleStartTime = addTime(startTimes[i], 7, 0, 0);
                currentScheduleEndTime = addTime(endTimes[i], 7, 0, 0);
                // 하나를 찾았으면 바로 중단해야 한다. 최초로 찾은 것이 가장 가까운 스케줄이다.
                break;
            }
        }
        return weekStartTime;
    }

    public long getCurrentScheduleStartTime() {
        return currentScheduleStartTime;
    }

    public long getCurrentScheduleEndTime() {
        return currentScheduleEndTime;
    }

    /**
     * Returns the new time.
     * @param startTime the start time.
     * @param day to be add day.
     * @param hour to be add hour.
     * @param min to be add min
     * @return new time. added day, hour, min.
     */
    private long addTime(long startTime, int day, int hour, int min) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(startTime);
        cal.add(Calendar.DAY_OF_MONTH, day);
        cal.add(Calendar.HOUR_OF_DAY, hour);
        cal.add(Calendar.MINUTE, min);
        return cal.getTimeInMillis();

//        int day1 = 1000 * 60 * 60 * 24 * day;
//        int hour1 = 1000 * 60 * 60 * hour;
//        int min1 = 1000 * 60 * min;
//        return startTime + day1 + hour1 + min1;
    }

    /**
     * Returns the start time of this week.
     * @param calendar The Calendar instance.
     * @return the start time of this week. unit is millisecond.
     */
    private long findWeekStartTime(Calendar calendar) {
        Calendar tempCalendar = Calendar.getInstance();
        tempCalendar.setTimeInMillis(calendar.getTimeInMillis());
        tempCalendar.set(Calendar.MILLISECOND, 0);
        tempCalendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        tempCalendar.set(Calendar.HOUR_OF_DAY, 0);
        tempCalendar.set(Calendar.MINUTE, 0);
        tempCalendar.set(Calendar.SECOND, 0);
        return tempCalendar.getTimeInMillis();
    }

    public void printTime(long timeMillis, String title) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(timeMillis);
        printTime(cal, title);
    }

    public int getStartHH() {
        return startHH;
    }

    public int getStartMM() {
        return startMM;
    }

    public int getEndHH() {
        return endHH;
    }

    public int getEndMM() {
        return endMM;
    }

    private void printTime(Calendar cal, String title) {

        if (Log.DEBUG_ON) {
            StringBuffer buf = new StringBuffer();
            buf.append(LOG_HEADER + "ID= " + scheduleId + "," + title + " " + cal.getTimeInMillis()
                    + " ==============\n");
            buf.append("YEAR=" + cal.get(Calendar.YEAR));
            buf.append(",MONTH=" + (cal.get(Calendar.MONTH) + 1));
            buf.append(",DATE=" + cal.get(Calendar.DATE));
            int week = cal.get(Calendar.DAY_OF_WEEK);
            switch (week) {
            case 1:
                buf.append(",WEEK=Sun");
                break;
            case 2:
                buf.append(",WEEK=Mon");
                break;
            case 3:
                buf.append(",WEEK=Tue");
                break;
            case 4:
                buf.append(",WEEK=Wed");
                break;
            case 5:
                buf.append(",WEEK=Thu");
                break;
            case 6:
                buf.append(",WEEK=Fri");
                break;
            case 7:
                buf.append(",WEEK=Sat");
                break;
            }

            buf.append(",HOUR=" + cal.get(Calendar.HOUR_OF_DAY));
            buf.append(",MIN=" + cal.get(Calendar.MINUTE));
            buf.append(",SEC=" + cal.get(Calendar.SECOND));

            buf.append("\n===========================================");

            if (Log.DEBUG_ON) {
                Log.printDebug(buf.toString());
            }
        }
    }

}

package com.videotron.tvi.illico.monitor.core;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Vector;

import com.videotron.tvi.illico.ixc.vbm.LogCommandTriggerListener;
import com.videotron.tvi.illico.ixc.vbm.VbmService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.util.GraphicsMemory;
import com.videotron.tvi.illico.util.SharedMemory;

public class MonitorVbmController implements LogCommandTriggerListener {

	private VbmService vbmService;

	protected HashSet idSet = new HashSet();
    protected Vector vbmLogBuffer;

    public static boolean ENABLED = false;

	private static MonitorVbmController instance;

	private static final String LOG_HEADER = "[VbmController] ";

	public static final long ID_POWER_EVENT = 1001000001L;
	public static final long ID_HEAP_USAGE = 1000001L;
	public static final long ID_GRAPHICS_USAGE = 1000002L;

	public static final String EVENT_POWER_ON = "POWER_ON";
	public static final String EVENT_POWER_OFF = "POWER_OFF";

	public static final String DEF_MEASUREMENT_GROUP = "-1";

	public static final String APP_NAME = "Monitor";

	public synchronized static MonitorVbmController getInstance() {
		if (instance == null) {
			instance = new MonitorVbmController();
		}
		return instance;
	}

	private MonitorVbmController() {

	}

	public void init(VbmService vbm) {
		if(Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "init()");
		}
		this.vbmService = vbm;

		long[] ids = null;
        try {
            ids = vbmService.checkLogCommand(APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }

		try {
			vbmService.addLogCommandChangeListener(APP_NAME, this);
		} catch (Exception e) {
			Log.printError(e);
		}

		if (ids != null) {
            for (int i = 0; i < ids.length; i++) {
                if (Log.DEBUG_ON) {
                    Log.printDebug("VbmController: ids[" + i + "] = " + ids[i]);
                }
                idSet.add(new Long(ids[i]));
            }
        }

        vbmLogBuffer = (Vector) SharedMemory.getInstance().get(VbmService.VBM_SM_INTF);
        checkEmpty();
        if (vbmLogBuffer != null) {
        	if(Log.DEBUG_ON) {
        		Log.printDebug(LOG_HEADER + "found vbmLogBuffer");
        	}
        } else {
        	if(Log.DEBUG_ON) {
        		Log.printDebug(LOG_HEADER + "not found vbmLogBuffer.");
        	}
        }

	}

	protected void checkEmpty() {
        ENABLED = vbmLogBuffer != null && !idSet.isEmpty();
    }

    public boolean write(long id, String group, String[] value, boolean trigger) {
        if (!ENABLED) {
            return false;
        }
        if (!trigger && !idSet.contains(new Long(id))) {
            return false;
        }
        if(group == null) {
        	group = DEF_MEASUREMENT_GROUP;
        }
        String str = Long.toString(id);
        if (value != null) {
            StringBuffer buffer = new StringBuffer(100);
            buffer.append(str);
            buffer.append(VbmService.SEPARATOR);
            buffer.append(group);
            for (int i = 0; i < value.length; i++) {
                buffer.append(VbmService.SEPARATOR);
                buffer.append(value[i]);
            }
            str = buffer.toString();
        }
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "write: " + str);
        }
        vbmLogBuffer.addElement(str);
        return true;
    }

	public void dispose() {
        try {
            vbmService.removeLogCommandChangeListener(APP_NAME);
        } catch (RemoteException ex) {
            Log.print(ex);
        }
    }

	/**
	 * event invoked when logLevel data updated.
	 * it is only applied to action measurements
	 *
	 *
	 * @param Measurement to notify measurement an frequencies
	 * @param command - true to start logging, false to stop logging
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void logCommandChanged(long measurementId, boolean command) throws RemoteException {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "logCommandChanged: " + measurementId + ", " + command);
		}
        if (command) {
            idSet.add(new Long(measurementId));
        } else {
            idSet.remove(new Long(measurementId));
        }
        checkEmpty();
    }

	/**
	 * event invoked when log of trigger measurement should be collected
	 *
	 * @param measurementId trigger measurement
	 *
	 * @throws RemoteException - occur during the execution of a remote method call
	 */
	public void triggerLogGathering(long measurementId) {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "triggerLogGathering: " + measurementId);
		}

        if(measurementId == ID_HEAP_USAGE) {
        	Runtime rt = Runtime.getRuntime();
        	long totalMemory = rt.totalMemory();
        	long freeMemory = rt.freeMemory();
        	long memoryBytes = totalMemory - freeMemory;
        	int memoryMbytes = (int) (memoryBytes / (1024 * 1024));
        	String[] usage = {String.valueOf(memoryMbytes)};

        	if(Log.DEBUG_ON) {
        		Log.printDebug(LOG_HEADER + " total:" + totalMemory + ",free:" + freeMemory + ",memoryMbytes:" + memoryMbytes);
        	}

        	write(ID_HEAP_USAGE, null, usage, true);
        }
        else if(measurementId == ID_GRAPHICS_USAGE) {
        	int memory = GraphicsMemory.getUsingGraphicsMemory() / (1024 * 1024);
        	String[] usage = {String.valueOf(memory)};
        	if(Log.DEBUG_ON) {
        		Log.printDebug(LOG_HEADER + " Graphics memory usage is " + memory);
        	}

        	write(ID_GRAPHICS_USAGE, null, usage, true);
        }
    }


}

package com.videotron.tvi.illico.monitor.core;

import org.davic.resources.ResourceClient;
import org.davic.resources.ResourceProxy;
import org.ocap.hardware.frontpanel.FrontPanelManager;
import org.ocap.hardware.frontpanel.TextDisplay;

import com.videotron.tvi.illico.log.Log;

/**
 * <code>FrontPanelDisplay</code>
 *
 * @author Charles CW Kwak
 * @since 2007. 1. 23
 * @version $Revision: 1.5 $ $Date: 2017/01/09 21:12:13 $
 */
public class FrontPanelDisplayer implements ResourceClient {
	private FrontPanelManager fpm = FrontPanelManager.getInstance();

	private static FrontPanelDisplayer instance = null;

	public static synchronized FrontPanelDisplayer getInstance() {
		if (instance == null) {
			instance = new FrontPanelDisplayer();
		}
		return instance;
	}

	private FrontPanelDisplayer() {
	}

	public void showText(String str) {
		displayText(str);
	}

	public void clear() {
		displayText("    ");
	}

	// all synchronized methods call this method
	private synchronized void displayText(String str) {
		if (Log.DEBUG_ON) {
			Log.printDebug("displayText(\"" + str + "\")");
		}

		if (fpm.reserveTextDisplay(this)) {
			TextDisplay led = fpm.getTextDisplay();
			try {
				led.setTextDisplay(new String[] { str }, led.getBlinkSpec(), led.getScrollSpec(), led.getBrightSpec(),
						led.getColorSpec());
			} catch (Throwable ex) {
				ex.printStackTrace();
			}
		} else {
			if (Log.DEBUG_ON) {
				Log.printDebug("displayText() fail to reserve front panel");
			}
		}
	}

	/** Resoure Client */
	public void notifyRelease(ResourceProxy proxy) {
	}

	public void release(ResourceProxy proxy) {
	}

	public boolean requestRelease(ResourceProxy proxy, Object requestData) {
		return true;
	}
}

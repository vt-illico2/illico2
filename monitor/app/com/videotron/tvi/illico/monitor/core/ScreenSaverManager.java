package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.awt.Rectangle;
import java.rmi.NotBoundException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;
import javax.tv.xlet.XletContext;

import org.dvb.application.AppAttributes;
import org.dvb.application.AppID;
import org.dvb.application.AppsDatabase;
import org.dvb.application.AppsDatabaseEvent;
import org.dvb.application.AppsDatabaseEventListener;
import org.dvb.application.CurrentServiceFilter;
import org.dvb.event.UserEvent;
import org.dvb.io.ixc.IxcRegistry;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.AODEventListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverConfirmationListener;
import com.videotron.tvi.illico.ixc.monitor.ScreenSaverStateListener;
import com.videotron.tvi.illico.ixc.monitor.ServiceStateListener;
import com.videotron.tvi.illico.ixc.screensaver.ScreenSaverService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.util.WindowProperty;
//Kenneth : list button 처리 때문에 추가됨
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.monitor.communication.MonitorServiceImpl;


/**
 * This class manages the screen-saver's lifecycle.
 * <P>
 * The screen saver application can draw graphics of screen-saver and this class determines the screen-saver turns on or
 * off.
 * <P>
 * If there is not inputed key event or software event that requested from other applications during maximum inactivity
 * time, this class request to screen-saver tunes on.
 * <P>
 * If key event or software event input during screen-saver on, this class request to screen-saver tunes off.
 * <P>
 * Before request to screen-saver turns on, the call back listener is provied for help that the actived application able
 * to determine that screen-saver turns on by other applications.
 * <P>
 * @author WSLEE1
 */
public class ScreenSaverManager implements AppsDatabaseEventListener, LayeredKeyHandler {

    /** The list of ScreenSaverConfirmationListener. */
    private ArrayList listeners = new ArrayList();
    /** The list of ScreenSaverStateListener. */
    private ArrayList stateListeners = new ArrayList();
    /** The maximum inactivity time. Unit is millisecond. */
    private long maxInactivityTime;
    /** The time that inputed lastest. */
    private long lastInputedKeyTime = 0;
    /** Indicates the state that screen-saver is tuned on. */
    private static final int STATE_STARTED_SCREEN_SAVER = 2;
    /** Indicates the state that screen-saver is tuned off and not monitor event. */
    private static final int STATE_STOPED = 3;
    /** Indicates the stat that screen-saver is tuned off and monitor event. */
    private static final int STATE_MONITORING = 4;
    /** The current state of the screen-saver state. */
    private int currentState = STATE_STOPED;

    /** Retry count if not found the IXC interface. */
    private static final int RETRY_COUNT_FOR_FOUND_IXC = 30;
    /** Sleep time for finding IXC interface. */
    private static final long SLEEP_TIME = 2000L;

    /** The instance of TVTimer. It is used to monitoring key event. */
    private TVTimer timer = TVTimer.getTimer();
    /** The instance of TVTimerSpec. It is used to monitoring key event. */
    private TVTimerSpec timerSpec = new TVTimerSpec();
    /** Implements the TVTimerWentOffListener. It is used to monitoring key event. */
    private TimerListener timerListener = new TimerListener();
    /** The time for checking inactivity time. */
    private static final long CHECK_TIME_INACTIVITY_TIME = 20 * 1000L;

    /** The singleton instance of ScreenSaverManager. */
    private static ScreenSaverManager instance = null;

    /** The Screen-saver application's IXC interface. */
    private ScreenSaverService screenSaverService = null;

    /** Implements the ServiceStateListener. */
    // private ServiceStateListenerImpl serviceStateListener = new ServiceStateListenerImpl();
    private PowerModeChangeListenerImpl powerModeChangeListenerImpl = new PowerModeChangeListenerImpl();

    private LayeredUI layeredUI;

    private static final String LOG_HEADER = "[ScreenSaverManager] ";
    /**
     * Initializes this class.
     */
    private void init() {
        try {
            maxInactivityTime = screenSaverService.getMaximulInactivityTime();
        } catch (Exception e) {
            Log.print(e);
        }
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "init() maxInactivityTime=" + maxInactivityTime);
        }
        startMonitoring();
        // Service state 에 관계 없이 무조건 Monitoring 한다. TV_VIEWING_STATE 에서도 Search 같은 것이 있으면
        // 동작 해야 한다.
        // Start monitoring service state changed.
        // ServiceStateManager.getInstance().addStateListener(serviceStateListener, this.getClass().getName());
        Host.getInstance().addPowerModeChangeListener(powerModeChangeListenerImpl);

        AppsDatabase.getAppsDatabase().addListener(this);
    }

    /**
     * Starts this class.
     * @param xletContext instance of XletContext.
     */
    public void start(XletContext xletContext) {
        findScreenSaver(xletContext);
    }

    /**
     * Gets the singleton instance.
     * @return instance of ScreenSaverManager.
     */
    public static synchronized ScreenSaverManager getInstance() {
        if (instance == null) {
            instance = new ScreenSaverManager();
        }
        return instance;
    }

    /**
     * Finds the Screen-saver application's IXC.
     * @param xletContext the instance of XletContext. It used to find IXC.
     */
    private void findScreenSaver(final XletContext xletContext) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "findScreenSaver()");
        }
        Thread thread = new Thread("IXCLookup") {
            public void run() {

                String ssName = DataManager.getInstance().getScreenSaverName();
                AppID ssAppId = findApplicationId(ssName);
                if (ssAppId == null) {
                    if (Log.ERROR_ON) {
                        Log.printError(LOG_HEADER + "ERROR - Not found the ScreenSaver.");
                    }
                    return;
                }
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "Found the ScreenSaver id=" + ssAppId);
                }

                String name = null;
                name = "/" + Integer.toHexString(ssAppId.getOID()) + "/" + Integer.toHexString(ssAppId.getAID()) + "/"
                        + ScreenSaverService.IXC_NAME;

                for (int i = 0; i < RETRY_COUNT_FOR_FOUND_IXC; i++) {
                    try {
                        screenSaverService = (ScreenSaverService) IxcRegistry.lookup(xletContext, name);
                        if (Log.INFO_ON) {
                            Log.printInfo(LOG_HEADER + "Founds the EnablersService");
                        }
                        ScreenSaverManager.this.init();
                        return;
                    } catch (NotBoundException notbound) {
                        if (Log.DEBUG_ON) {
                            Log.printDebug(LOG_HEADER + "EnablersService is not bound. Wait...");
                        }
                        try {
                            Thread.sleep(SLEEP_TIME);
                        } catch (Exception e) {
                            Log.print(e);
                        }
                    } catch (Exception e) {
                        Log.print(e);
                        break;
                    }
                }
                if (Log.ERROR_ON) {
                    Log.printError(LOG_HEADER + "Give up to find enablerService");
                }
            }
        };
        thread.start();
    }

    /**
     * Finds the application id by application name in the AppsDatabase.
     * @param applicationName the application name.
     * @return If found the application, return AppID, otherwise return null.
     */
    private AppID findApplicationId(String applicationName) {
        AppsDatabase appsDB = AppsDatabase.getAppsDatabase();
        Enumeration enumeration = appsDB.getAppIDs(new CurrentServiceFilter());
        if (enumeration != null) {
            while (enumeration.hasMoreElements()) {
                AppID appID = (AppID) enumeration.nextElement();
                AppAttributes attr = appsDB.getAppAttributes(appID);
                String name = attr.getName();
                if (applicationName.equals(name)) {
                    return appID;
                }
            }
        }
        return null;
    }

    /**
     * Start monitoring for key event.
     */
    private void startMonitoring() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startMonitoring() currentState=" + currentState);
        }
        currentState = STATE_MONITORING;
        lastInputedKeyTime = System.currentTimeMillis();
        startTimer();
        registerKeyHandler();
    }

    /**
     * Stops the monitoring key event.
     */

    private void stopMonitoring() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "stopMonitoring");
        }
        currentState = STATE_STOPED;
        lastInputedKeyTime = 0;
        stopTimer();
        unregisterKeyHandler();
    }

    /**
     * Starts the screen-saver.
     * Screen-saver 에 App name 을 넘기는 방법
     * 1.  Confirm 을 한 App 이 있다면 해당 APP 이름을 전달한다.
     * 2.  Listener 를 아무도 걸지 않았다면
     *   A.  FULL_SCREEN_APP_STATE 라면 현재 실행 중인 APP 이름을 넘긴다.
     *   B.  TV_VIEWING_STATE 라면 null 을 던진다.
     *   C.  TV_VIEWING_STATE 인데 Special Video State 라면
     *    i.  Special video mode owner 를 넘긴다. 
     * 3.  IXC를 통해서 Screen saver 요청을 받은 경우 : 위의 2번과 동일.
     */
    public void startScreenSaver(boolean forced) {
    	if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startScreenSaver() forced=" + forced);
        }
        // IXC 로 ScreenSaver가 동작해도 되는지 물어본다.
        // 이때 동작하면 안 된다는 답이 온 경우, 다시 모니터링 할 필요가 없다.
        // 다음 상태 변경이 일어나면 그때 모니터링 시작 하면 된다. 그래서 일단 stop()
        stopMonitoring();
        currentState = STATE_STOPED;
        // 어플들에게 확인 받자.

        // 1. Listener 를 아무도 등록하지 않은 경우 Screen-saver 가 동작 하면 안 된다. (TV_VIEWING_STATE)
        int wtrmMode = WatchingTimeRestrictionManager.getInstance().getCurrentMode();
        boolean isBlockedTime = false;
        if(wtrmMode == WatchingTimeRestrictionManager.MODE_RESTRICTION_TIME) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "Blocked time now");
            }
            isBlockedTime = true;
        }

        if (listeners.size() < 1 && !isBlockedTime) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "No listener. so do not show screen-saver");
            }
            startMonitoring();
            return;
        }

        ScreenSaverConfirmResult confirm = null;
        if(!forced) {
        	confirm = confirmScreenSaverActivity();
        }
        else {
            confirm = new ScreenSaverConfirmResult(true, null);
        }

        if(isBlockedTime) {
            confirm = new ScreenSaverConfirmResult(true, null);
        }
        
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "ScreenSaverConfirmResult=" + confirm);
        }

        if (!confirm.isConfirm()) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "Can't confirm showing the screen saver so no action");
            }
            startMonitoring();
            return;
        }
        try {
        	if(STBModeManager.getInstance().isStandaloneMode()) {
        		if(Log.INFO_ON) {
        			Log.printInfo(LOG_HEADER + "Now standalone mode. ");
        		}
        		screenSaverService.showScreenSaver(DataManager.getInstance().getPVRName());
        		notifyScreenSaverStarted();
        	}
        	else {
        	    
        	    if(confirm.getConfirmAppName() != null) {
        	        // Requests start screen-saver to the ScreenSaver application.
        	        if (Log.INFO_ON) {
                        Log.printInfo(LOG_HEADER + "Starts screen saver. confirm app ="
                                + confirm.getConfirmAppName());
                    }
                    screenSaverService.showScreenSaver(confirm.getConfirmAppName());
        	    }
        	    else {
        	        String currentAppName = getCurrentActivatedApplicationName();
        	        if(isBlockedTime) {
        	            currentAppName = null;
        	        }
        	        if (Log.INFO_ON) {
                        Log.printInfo(LOG_HEADER + "Starts screen saver. Current app ="
                                + currentAppName);
                    }
        	        screenSaverService.showScreenSaver(currentAppName);
        	    }
        	    
        	    // Notify that the screen-saver is started.
                notifyScreenSaverStarted();
        	    
        	}
        } catch (Exception e) {
            Log.print(e);
        }
        currentState = STATE_STARTED_SCREEN_SAVER;
        // registerExclusiveUserEvent();
        registerKeyHandler();
    }
    
    private String getCurrentActivatedApplicationName() {
        if(ServiceStateManager.getInstance().isTvViewingState()) {
            if(MonitorStateManager.getInstance().isSpecialVideoState()) {
                // Special video state
                return MonitorStateManager.getInstance().getCurrentVideoName();
            }
            else {
                // TV_VIEWING_STATE
                return null;
            }
        }
        else {
            // FULL_SCREEN_APP_STATE or PIP_STATE
            TransitionEvent curEvent = StateManager.getInstance().getCurrentEvent();
            if(curEvent == null) {
                return null;
            }
            return curEvent.getApplicationName();
        }
    }

    /**
     * Stops the screen-saver.
     */
    private void stopScreenSaver() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "Hide screen saver");
        }
        try {
            screenSaverService.hideScreenSaver();
            notifyScreenSaverStoped();
        } catch (Exception e) {
            Log.print(e);
        }
    }

    /**
     * Disposes this class.
     * <P>
     * When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "dispose()");
        }
        if (powerModeChangeListenerImpl != null) {
            Host.getInstance().removePowerModeChangeListener(powerModeChangeListenerImpl);
        }
        if (listeners != null) {
            listeners.clear();
        }
        AppsDatabase.getAppsDatabase().removeListener(this);
        stopMonitoring();
    }

    /**
     * Starts the timer for checking inactivity time.
     */
    private void startTimer() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "startTimer");
        }
        synchronized (timerSpec) {
            stopTimer();
            try {
                timerSpec.setDelayTime(CHECK_TIME_INACTIVITY_TIME);
                timerSpec.setRepeat(true);
                timerSpec.addTVTimerWentOffListener(timerListener);
                timer.scheduleTimerSpec(timerSpec);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    /**
     * Stop the timer for checking inactivity time.
     */
    private void stopTimer() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "stopTimer");
        }
        synchronized (timerSpec) {
            timerSpec.removeTVTimerWentOffListener(timerListener);
            timer.deschedule(timerSpec);
        }
    }

    /**
     * Implements the TVTimerWentOffListener.
     * @author Woosik Lee
     */
    private class TimerListener implements TVTimerWentOffListener {
        /**
         * This is timer call back. It will check the maximum inactivity time.
         * @param event TVTimer event.
         */
        public void timerWentOff(TVTimerWentOffEvent event) {
            long currentTime = System.currentTimeMillis();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "timerWentOff() currentTime=" + currentTime
                        + ", lastInputedKeyTime=" + lastInputedKeyTime);
            }
            if (currentTime - lastInputedKeyTime > maxInactivityTime) {
                startScreenSaver(false);
            }
        }
    };

    /**
     * Implements the ServiceStateListener.
     * @author Woosik Lee
     */
    private class ServiceStateListenerImpl implements ServiceStateListener {
        /**
         * It is notified the service state changed.
         * @param newState new service state.
         */
        public void stateChanged(int newState) {
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "stateChanged() newState=" + newState);
            }
            if (ServiceStateManager.getInstance().isTvViewingState()) {
                if (currentState == STATE_STARTED_SCREEN_SAVER) {
                    if (Log.DEBUG_ON) {
                        Log.printDebug("WARNING : stop screen-saver!");
                    }
                    stopScreenSaver();
                }
                stopMonitoring();
            } else {
                startMonitoring();
            }
        }
    }

    /**
     * Implements the PowerModeChangeListener.
     * @author Woosik Lee
     */
    private class PowerModeChangeListenerImpl implements PowerModeChangeListener {
        public void powerModeChanged(int powerMode) {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "powerModeChanged() powerMode=" + powerMode);
            }
            if (powerMode == Host.FULL_POWER) {
                startMonitoring();
            } else if (powerMode == Host.LOW_POWER) {
                if (currentState == STATE_STARTED_SCREEN_SAVER) {
                    if (Log.INFO_ON) {
                        Log.printInfo("stop screen-saver!");
                    }
                    stopScreenSaver();
                }
                stopMonitoring();
            }
        }
    };

    // ///////////////////////////////////////
    // Implementation of ScreenSaverListener..
    // //////////////////////////////////////

    /**
     * Stops the monitoring timer or launched screen-saver.
     * <P>
     * If any application want to stop screen-saver, can call this method by
     * {@link MonitorService#resetScreenSaverTimer()}.
     */
    public void resetTimer() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "resetTimer() currentState=" + currentState);
        }

        if (currentState == STATE_STARTED_SCREEN_SAVER) {
            stopScreenSaver();
            startMonitoring();
        } else {
            lastInputedKeyTime = System.currentTimeMillis();
        }
    }

    /**
     * Adds the listener.
     * @param l instance of ScreenSaverConfirmationListener.
     * @param appName requested application name.
     */
    public void addScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String appName) {
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, appName);
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "addScreenSaverConfirmListener():" + item + ", appName="
                        + appName);
            }
            if (!listeners.contains(item)) {
                listeners.add(item);
            } else {
                // Removes a previous listener.
                listeners.remove(item);
                // Adds a new listener.
                listeners.add(item);
            }
        }
    }

    /**
     * Removes a listener.
     * @param l instance of ScreenSaverConfirmationListener.
     * @param applicationName requested application name.
     */
    public void removeScreenSaverConfirmListener(ScreenSaverConfirmationListener l, String applicationName) {
        synchronized (listeners) {
            ListenerItem item = new ListenerItem(l, applicationName);
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "removeScreenSaverConfirmListener():" + item + ", appName="
                        + applicationName);
            }
            listeners.remove(item);
        }
    }

    /**
     * Adds the ScreenSaverStateListener.
     * @param l instance of ScreenSaverStateListener.
     * @param appName requested application name.
     */
    public void addScreenSaverStateListener(ScreenSaverStateListener l, String applicationName) {
        synchronized (stateListeners) {
            ListenerItemForState item = new ListenerItemForState(l, applicationName);
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "addScreenSaverStateListener():" + item + ", appName="
                        + applicationName);
            }
            if (!stateListeners.contains(item)) {
                stateListeners.add(item);
            } else {
                // Removes a previous listener.
                stateListeners.remove(item);
                // Adds a new listener.
                stateListeners.add(item);
            }
        }
    }

    /**
     * Removes a ScreenSaverStateListener.
     * @param l instance of ScreenSaverStateListener.
     * @param applicationName requested application name.
     */
    public void removeScreenSaverStateListener(ScreenSaverStateListener l, String applicationName) {
        synchronized (stateListeners) {
            ListenerItemForState item = new ListenerItemForState(l, applicationName);
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "removeScreenSaverStateListener():" + item + ", appName="
                        + applicationName);
            }
            stateListeners.remove(item);
        }
    }
    
    /**
     * Screen-saver confirmation result.
     * @author woosiiik
     *
     */
    private class ScreenSaverConfirmResult {
        private boolean confirm;
        private String confirmAppName;

        public ScreenSaverConfirmResult(boolean confirm, String confirmAppName) {
            super();
            this.confirm = confirm;
            this.confirmAppName = confirmAppName;
        }

        public boolean isConfirm() {
            return confirm;
        }

        public String getConfirmAppName() {
            return confirmAppName;
        }

        public String toString() {
            return "confirm=" + confirm + ",appName=" + confirmAppName;
        }
    }

    /**
     * Confirm to listeners. If haven't any listener, return true. In the TV_VIEWING_STATE, returns false if one or more
     * listeners reject this question, otherwise return true. In the FULL_SCREEN_APP_STATE, it should ask to only one
     * listener that is last registered.
     * @return execute the screen-saver or not.
     */
    private ScreenSaverConfirmResult confirmScreenSaverActivity() {
        if (listeners.size() < 1) {
            ScreenSaverConfirmResult result = new ScreenSaverConfirmResult(true, null);
            return result;
        }

        // 원래 FULL_SCREEN_APP_STATE 인 경우에는 last added listener 에게 물어보고
        // TV_VIEWING_STATE 인 경우에는 모두 물어 봤는데~
        // 상태 관계 없이 항상 last added listener 에게 물어보는 것으로 변경 하겠다.

        Object lastListener;
        synchronized (listeners) {
            lastListener = listeners.get(listeners.size() - 1);
        }
        try {
            ListenerItem ssl = (ListenerItem) lastListener;
            boolean result = ssl.listener.confirmScreenSaver();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "(2)result=" + result + ", listener=" + ssl.toString()
                        + ", appName=" + ssl.applicationName);
            }
            FrameworkMain.getInstance().printSimpleLog("ScreenSaver confirm result is " + result + " by " + ssl.applicationName);
            ScreenSaverConfirmResult resultConfirm = new ScreenSaverConfirmResult(result, ssl.applicationName);
            return resultConfirm;
            
        } catch (Throwable t) {
            Log.print(t);
        }
        
        ScreenSaverConfirmResult result = new ScreenSaverConfirmResult(true, null);
        return result;        

        /*
        int curState = ServiceStateManager.getInstance().getState();
        if (curState == MonitorService.TV_VIEWING_STATE) {
            Object[] lsnrs;
            synchronized (listeners) {
                lsnrs = listeners.toArray();
            }
            for (int i = 0; i < lsnrs.length; i++) {
                try {
                    ListenerItem ssl = (ListenerItem) lsnrs[i];
                    boolean result = ssl.listener.confirmScreenSaver();
                    if (Log.INFO_ON) {
                        Log.printInfo(LOG_HEADER + "(1)result=" + result + ", listener=" + ssl.toString()
                                + ", appName=" + ssl.applicationName);
                    }
                    if (!result) {
                        return false;
                    }
                } catch (Throwable t) {
                    Log.print(t);
                }
            }
        }
        else {
            // This is FULL_SCREEN_APP_STATE.
            // Should ask to last registered listener only.
            Object lastListener;
            synchronized (listeners) {
                lastListener = listeners.get(listeners.size() - 1);
            }
            try {
                ListenerItem ssl = (ListenerItem) lastListener;
                boolean result = ssl.listener.confirmScreenSaver();
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "(2)result=" + result + ", listener=" + ssl.toString()
                            + ", appName=" + ssl.applicationName);
                }
                return result;
            } catch (Throwable t) {
                Log.print(t);
            }
        }
        */
    }

    /**
     * Notify that the screen-saver is started.
     */
    private void notifyScreenSaverStarted() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "notifyScreenSaverStarted()");
        }
        if (stateListeners.size() < 1) {
            return;
        }
        Object[] lsnrs;
        synchronized (stateListeners) {
            lsnrs = stateListeners.toArray();
        }
        for (int i = 0; i < lsnrs.length; i++) {
            try {
                ListenerItemForState lifs = (ListenerItemForState) lsnrs[i];
                lifs.listener.startScreenSaver();
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "notifyScreenSaverStarted() to " + lifs.listener);
                }
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }

    /**
     * Notify that the screen-saver is stoped.
     */
    private void notifyScreenSaverStoped() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "notifyScreenSaverStoped()");
        }
        if (stateListeners.size() < 1) {
            return;
        }
        Object[] lsnrs;
        synchronized (stateListeners) {
            lsnrs = stateListeners.toArray();
        }
        for (int i = 0; i < lsnrs.length; i++) {
            try {
                ListenerItemForState lifs = (ListenerItemForState) lsnrs[i];
                lifs.listener.stopScrenSaver();
                if (Log.DEBUG_ON) {
                    Log.printDebug(LOG_HEADER + "notifyScreenSaverStoped() to " + lifs.listener);
                }
            } catch (Throwable t) {
                Log.print(t);
            }
        }
    }

    /**
     * This class has the unique application id and the instance of ScreenSaverConfirmationListener.
     * @author Woosik Lee
     */
    private class ListenerItem {
        /** Instance of ScreenSaverConfirmationListener. */
        private ScreenSaverConfirmationListener listener;
        /** The requested application name. */
        private String applicationName;

        /**
         * Constructor.
         * @param l ScreenSaverConfirmationListener.
         * @param appName requested application name.
         */
        public ListenerItem(final ScreenSaverConfirmationListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItem) {
                String appName = ((ListenerItem) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }

    /**
     * This class has the unique application id and the instance of ScreenSaverStateListener.
     * @author Woosik Lee
     */
    private class ListenerItemForState {
        /** Instance of ScreenSaverStateListener. */
        private ScreenSaverStateListener listener;
        /** The requested application name. */
        private String applicationName;

        /**
         * Constructor.
         * @param l ScreenSaverConfirmationListener.
         * @param appName requested application name.
         */
        public ListenerItemForState(final ScreenSaverStateListener l, final String appName) {
            this.listener = l;
            this.applicationName = appName;
        }

        /**
         * Overrides the equals() by Object class.
         * @param o the object to compare with this.
         * @return if equals instance return true, otherwise return false.
         */
        public boolean equals(Object o) {
            if (o instanceof ListenerItemForState) {
                String appName = ((ListenerItemForState) o).applicationName;
                if (appName.equals(applicationName)) {
                    return true;
                }
                return false;
            } else {
                return false;
            }
        }
    }

    /**
     * Registers the Layered UI key handler.
     */
    private void registerKeyHandler() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "registerKeyHandler()");
        }
        synchronized (this) {
            if (layeredUI == null) {
                layeredUI = LayeredUIManager.getInstance().createLayeredKeyHandler(
                        WindowProperty.SCREEN_SAVER.getName(), WindowProperty.SCREEN_SAVER.getPriority(), this);
                layeredUI.activate();
            }
        }
    }

    /**
     * Unregisters the Layered UI key handler.
     */
    private void unregisterKeyHandler() {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "unregisterKeyHandler()");
        }
        synchronized (this) {
            if (layeredUI != null) {
                LayeredUIManager.getInstance().disposeLayeredUI(layeredUI);
                layeredUI = null;
            }
        }
    }


    
    private static final String HIDDEN_KEY_FOR_AOD = "123699999";
    private String hiddenKeyForAod = "";
    private AodStateViewer aodViewer = null;
    /**
     *
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "handleKeyEvent() event=" + event);
        }

        if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}
		
		// Hidden Key - AOD Status Popup
        int code = event.getCode();
        if(code == KeyEvent.VK_1) {
        	hiddenKeyForAod = "1";
        }
        else if(code == OCRcEvent.VK_STOP) {
        	if(aodViewer != null) {
        		aodViewer.stop();
        		aodViewer = null; 
        	}
        }
        else if(code == KeyCodes.COLOR_C) {
        	if(aodViewer != null) {
        		// RED button - AOD Test - remove aod application
        		Hashtable table = AodManager2.getInstance().getLoadedAodList();
        		Enumeration enumer = table.keys();
        		while(enumer.hasMoreElements()) {
        			final String appName = (String) enumer.nextElement();
        			AodManager2.getInstance().closeAODApplication(appName, new AODEventListener() {
        				public void notifyAodEvent(int result) {
        					if(Log.INFO_ON) {
        						Log.printInfo(LOG_HEADER + "[AODM] closeAODApplication() [" + appName + "] result is " + result);
        					}
        				}
        			});
        			break;
        		}
        		
        	}
        }
        else {
        	hiddenKeyForAod += String.valueOf(code - KeyEvent.VK_0);
        	if(HIDDEN_KEY_FOR_AOD.equals(hiddenKeyForAod)) {
        		// Hidden debug popup !!!!!!!!
        		if(aodViewer == null) {
        			aodViewer = new AodStateViewer();
        			aodViewer.start();
        		}
        	}
        	if(hiddenKeyForAod.length() > HIDDEN_KEY_FOR_AOD.length()) {
        		hiddenKeyForAod = "";
        	}
        }
        
        
        ////////// End of Hidden key //////////////////
        
        // R7.3 Sangyong : for APD
        boolean consumed = CommunicationManager.getInstance().resetAPDTimerWithKeycode(code);
        
        if (code == KeyCodes.COLOR_D && consumed) {
        	if (Log.DEBUG_ON) {
              Log.printDebug(LOG_HEADER + "APD module in Profile consumed a green key");
        	}
        	return true;
        }

        if (currentState == STATE_MONITORING) {
            lastInputedKeyTime = System.currentTimeMillis();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "User event received time=" + lastInputedKeyTime);
            }
            //->Kenneth[2014.12.5] : 가끔 screen saver 가 안 사라지는 문제 발생
            // 이런 경우 복구할 방법이 없으므로 exit 키에 한해서 실제 screen saver ui 가 떠 있는지 확인하고
            // 만약 떠 있다면 에러 상황으로 판단하고 스크린 세이버를 해제하도록 한다.
            // exit 키로 한정한 것은 모든 키 눌릴 때마다 이런 체크를 하게 되면 performance 에 영향을 줄 수 있기 때문임.
            // screen saver 를 stop 하고 return true 하지 않는 이유는 혹 뭔가의 이유로 exit 를 
            // 제대로 전달하지 못하는 사태가 벌어질까 우려해서임.
            //->Kenneth[2015.6.9] : VDTRMASTER-5301
            // Gabriel 요청에 따라서 R5 에 이 기능을 넣기로 함
            if (code == OCRcEvent.VK_EXIT) {
                if (isScreenSaverVisible()) {
                    Log.printDebug("[ScreenSaverManager] STATE_MONITORING but isScreenSaverVisible returns TRUE. So strange state!");
                    Log.printDebug("Call stopScreenSaver()");
                    stopScreenSaver();
                }
            }
            //<-Kenneth

            //->Kenneth [R4.1] : List button 눌렸을 때 Search app 이 스스로 사라지는 경우 있음 발견
            //->Kenneth : 그래서 Search 보다 높은 ScreenSaverManager 에서 필터링하는 것임
            try {
                if (code == KeyCodes.LIST) {
                    Log.printDebug("Kenneth [R4.1] ScreenSaverManager : condition -- 1");
                    if (!Environment.SUPPORT_DVR) {
                        Log.printDebug("Kenneth [R4.1] ScreenSaverManager : condition -- 2");
                        if (AuthorizationManager.getInstance().checkPvrMultiroomPackage() !=
                                AuthorizationManager.CHECK_RESULT_AUTHORIZED) {
                            Log.printDebug("Kenneth [R4.1] ScreenSaverManager : condition -- 3");
                            if (!MonitorKeyHandler.isHelpVisible()) {
                                Log.printDebug("Kenneth [R4.1] ScreenSaverManager : condition -- 4");
                                int state = MonitorServiceImpl.getInstance().getState();
                                if (state == MonitorService.FULL_SCREEN_APP_STATE){
                                    Log.printDebug("Kenneth [R4.1] ScreenSaverManager : LIST key : NON-PVR : No Multiroom package : Help app Not visible : FULL_SCREEN_APP_STATE => Not to dispatch this List Key Event");
                                    return true;
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
                Log.print(e);
            }
            return false;

        } else if (currentState == STATE_STARTED_SCREEN_SAVER) {
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "Received exclusive event");
            }
            // Screen Saver 제거
            stopScreenSaver();
            // Monitoring 시작.
            startMonitoring();
            return true;
        }
        return false;
    }

    //->Kenneth[2014.12.5] : 진짜 screen saver 가 화면에 보이고 있는지 확인하는 코드임
    private boolean isScreenSaverVisible() {
        LayeredUI[] arr = LayeredUIManager.getInstance().getAllLayeredUIs();
        if (arr == null) return false;
        for (int i = 0 ; i < arr.length ; i++) {
            // ScreenSaver 같은 경우 rectangle 이 없는 애가 항상 layeredUI 로 하나 잡혀 있으므로
            // 얘를 거르기 위한 필터링임
            Rectangle r = arr[i].getBounds();
            if (r == null || r.isEmpty()) {
                continue;
            }
            if (arr[i].getName().equals("ScreenSaver")) {
                return arr[i].isActive();
            }
        }
        return false;
    }

    // ////////////////////////////////////////
    // Implements AppsDatabaseEventListener
    // ////////////////////////////////////////
    /**
     * Implements the AppsDatabaseEventListener.
     * <P>
     * No need to implementation.
     * @param paramAppsDatabaseEvent event.
     */
    public void newDatabase(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * Implements the AppsDatabaseEventListener.
     * <P>
     * No need to implementation.
     * @param paramAppsDatabaseEvent event.
     */
    public void entryAdded(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }

    /**
     * This method is called when the bound application is deleted at the AppsDatabase. If the
     * {@link ServiceStateManager} has the bound application's listener, it must delete.
     * @param paramAppsDatabaseEvent event.
     */
    public void entryRemoved(AppsDatabaseEvent paramAppsDatabaseEvent) {
        synchronized (listeners) {
            AppID appId = paramAppsDatabaseEvent.getAppID();
            AppsDatabase db = AppsDatabase.getAppsDatabase();
            AppAttributes appAtt = db.getAppAttributes(appId);
            String appName = appAtt.getName();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "entryRemoved() appName=" + appName);
            }

            ListenerItem item = new ListenerItem(null, appName);
            listeners.remove(item);
        }
    }

    /**
     * Implements the AppsDatabaseEventListener.
     * <P>
     * No need to implementation.
     * @param paramAppsDatabaseEvent event.
     */
    public void entryChanged(AppsDatabaseEvent paramAppsDatabaseEvent) {

    }
}

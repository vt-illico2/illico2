// $Header: /home/cvs/illico2/monitor/app/com/videotron/tvi/illico/monitor/core/AodStateViewer.java,v 1.11 2017/01/09 21:12:13 zestyman Exp $

/*
 *  AodStateViewer.java	$Revision: 1.11 $ $Date: 2017/01/09 21:12:13 $
 *
 *  Copyright (c) 2001-2009 Alticast Corp.
 *  All rights reserved. http://www.alticast.com/
 *
 *  This software is the confidential and proprietary information of
 *  Alticast Corp. ("Confidential Information"). You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Alticast.
 */
package com.videotron.tvi.illico.monitor.core;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.application.AppProxy;

import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.alticast.ui.LayeredWindow;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.data.AodApplication;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;
import com.videotron.tvi.illico.monitor.data.MonitorConfigurationManager;
import com.videotron.tvi.illico.util.FontResource;

public class AodStateViewer extends Component implements TVTimerWentOffListener {

	private LayeredUI ui;

	private static final Color bg = new org.dvb.ui.DVBColor(0, 0, 255, 70);
    private static final Font f = FontResource.BLENDER.getFont(18);


    private String serverInfo;

    private String[] aodListFromMS;

    private String[] aodLoadedList;

    private String[] aodLoadingList;

    private ArrayList errorLogs;

	private TVTimer timer = TVTimer.getTimer();
	private TVTimerSpec timerSpec = new TVTimerSpec();

	private static final String LOG_HEADER = "[AodStateViewer] ";

	public AodStateViewer() {
		LayeredWindow window = new com.alticast.ui.LayeredWindow();
        window.setBounds(0, 0, 960, 540);
        window.setVisible(true);
        window.add(this);
        ui = LayeredUIManager.getInstance().createLayeredWindow("Monitor,AOD,Debug", 20001, false, window, new Rectangle(0, 0, 960, 540));
        this.setVisible(true);
        this.setBounds(0, 0, 960, 540);
	}

	public synchronized void start() {
		init();
		this.setVisible(true);
        ui.activate();
        repaint();

        startTimer();
	}

	private void init() {
        String serverIp = MonitorConfigurationManager.getInstance().getAodServerIP();
        int port = MonitorConfigurationManager.getInstance().getAodServerPort();
        String rootContext = MonitorConfigurationManager.getInstance().getAodContextRoot();
        int timeoutdelay = MonitorConfigurationManager.getInstance().getAodTimeoutDelay();
        if(serverIp == null) {
        	serverInfo = "[AOD server info] Monitor couldn't receive AOD server information from MS";
        }
        else {
        	serverInfo = "[AOD server info] " + serverIp + ":" + String.valueOf(port) + rootContext + ", Timeout delay is " + String.valueOf(timeoutdelay);
		}

        AodApplication[] aodAppList = EIDDataManager.getInstance().getAodApplicationList();
        if(aodAppList == null || aodAppList.length < 1) {
        	aodListFromMS = new String[] {"[AOD app list] Monitor couldn't receive AOD application name from MS"};
        }
        else {
        	aodListFromMS = new String[aodAppList.length + 1];
        	aodListFromMS[0] = "[AOD app list] from MS";
        	for(int i=0; i<aodAppList.length; i++) {
        		aodListFromMS[i+1] = "++++ name=[" + aodAppList[i].getAppName() + "], path=" + aodAppList[i].getAppPath();
        	}
        }


		Hashtable aodList = AodManager2.getInstance().getLoadedAodList();
		if (aodList.size() < 1) {
			aodLoadedList = new String[] { "[AOD loaded list] - No list" };
		} else {
			aodLoadedList = new String[aodList.size() + 1];
			aodLoadedList[0] = "[AOD loaded list]";
			int count = 1;
			Enumeration enum1 = aodList.keys();
			while (enum1.hasMoreElements()) {
				String key = (String) enum1.nextElement();
				UnboundAppProxy appProxy = (UnboundAppProxy) aodList.get(key);

				aodLoadedList[count] = "++ [" + key + "] ID=" + appProxy.getAppID() + ", Type="
						+ makeAppTypeString(appProxy.getAppType()) + ", P=" + appProxy.getPriority() + ", v="
						+ appProxy.getAppVersion() + ", storage=" + appProxy.getStoragePriority() + ", state="
						+ getAppStateName(appProxy.getDVBJProxy().getState());

				count++;
			}

		}

        aodList = AodManager2.getInstance().getLoadingAodList();
        if(aodList.size() < 1) {
        	aodLoadingList = new String[] {"[AOD loading list] - No list"};
        }
        else {
        	aodLoadingList = new String[aodList.size() + 1];
        	aodLoadingList[0] = "[AOD loading list]";
        	int count = 1;
        	Enumeration enum1 = aodList.keys();
        	while(enum1.hasMoreElements()) {
        		String key = (String) enum1.nextElement();
        		aodLoadingList[count] = "++++ [" + key + "] now loading .......";
				count ++;
        	}
        }

        errorLogs = AodManager2.getInstance().getErrorLogHistory();
    }

	private String getAppStateName(int appState) {
		switch(appState) {
		case AppProxy.DESTROYED :
			return "{DESTORYED}";
		case AppProxy.NOT_LOADED :
			return "{NOT_LOADED}";
		case AppProxy.PAUSED :
			return "{PAUSED}";
		case AppProxy.STARTED :
			return "{STARTED}";
		}
		return "UNKNOWN";
	}

	private String makeAppTypeString(int type) {
		switch (type) {
		case UnboundAppProxy.DAEMON :
			return "DAEMON";
		case UnboundAppProxy.SEMI_DAEMON :
			return "SEMI-DAEMON";
		case UnboundAppProxy.NORMAL :
			return "NORMAL";
		}
		return "UNKNOWN";
	}

    public synchronized void stop() {
        if (ui.isActive()) {
            this.setVisible(false);
            ui.deactivate();
        }
        stopTimer();
    }


    public void paint(java.awt.Graphics g) {
    	g.setColor(bg);
        g.fillRect(0, 0, 960, 540);
        g.setFont(f);

        g.setColor(Color.green);
        g.drawString("This is the AOD debug infomation of Monitor App. <version = "
                + FrameworkMain.getInstance().getApplicationVersion() + "> ", 70, 40);
        g.drawString("    (You can remove loaded AOD application using 'RED' key ) ", 70, 65);

        int y = 90;
        g.setColor(Color.magenta);
        g.drawString(serverInfo, 80, y);

        y += 25;
        g.setColor(Color.yellow);
        for(int i=0; i<aodListFromMS.length; i++) {
        	g.drawString(aodListFromMS[i], 80, y);
        	y += 19;
        }

        y += 10;
        g.setColor(java.awt.Color.green);
        for(int i=0; i<aodLoadedList.length; i++) {
        	g.drawString(aodLoadedList[i], 80, y);
        	y += 19;
        }

        y += 10;
        g.setColor(Color.white);
        for(int i=0; i<aodLoadingList.length; i++) {
        	g.drawString(aodLoadingList[i], 80, y);
        	y += 19;
        }

        y += 10;

        g.setColor(Color.green);
        if(errorLogs != null) {
        	int size = errorLogs.size();
        	g.drawString("[AOD Error Logs] Error count is " + size, 80, y);
        	y += 19;
        	for(int i=size - 1; i >= 0; i--) {
        		String log = (String) errorLogs.get(i);
        		g.drawString(log, 80, y);
        		y += 19;
        	}
        }

        y += 10;
    }

    /**
     * Starts timeout timer.
     */
    private void startTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "startTimer() ");
		}
		try {
			timerSpec.setDelayTime(3000L);
			timerSpec.setRepeat(true);
			timerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(timerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

    /**
     * Stops timout timer.
     */
	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "stopTimer() ");
		}
		timerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(timerSpec);
	}

	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "timerWentOff() ");
		}
		init();
		repaint();
	}

}

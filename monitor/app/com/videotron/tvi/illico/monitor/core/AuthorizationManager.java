package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.util.Vector;

import javax.tv.service.selection.ServiceContext;
import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.opencable.handler.cahandler.CAAuthorization;
import com.opencable.handler.cahandler.CAHandler;
import com.videotron.tvi.illico.framework.FrameworkMain;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.vod.VODEvents;
import com.videotron.tvi.illico.ixc.vod.VODService;
import com.videotron.tvi.illico.ixc.vod.VODServiceListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxy;
import com.videotron.tvi.illico.monitor.data.ActivationPackage;
import com.videotron.tvi.illico.monitor.data.ApplicationPackage;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.EIDDataManager;
import com.videotron.tvi.illico.monitor.data.IPPVDeactivationPackage;
import com.videotron.tvi.illico.monitor.data.MandatoryPackage;
import com.videotron.tvi.illico.monitor.data.PvrMultiroomPackage;
import com.videotron.tvi.illico.monitor.data.PvrStandalonePackage;
import com.videotron.tvi.illico.monitor.system.CANHManager;
import com.videotron.tvi.illico.monitor.ui.popup.DisablePopup;
import com.videotron.tvi.illico.monitor.ui.popup.Popup;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.GeneralPreferenceUtil;
import com.videotron.tvi.illico.util.WindowProperty;
//->Kenneth : R4.1
import com.videotron.tvi.illico.monitor.data.VODDeactivationPackage;
//->Kenneth[2015.2.4] 4K
import com.videotron.tvi.illico.monitor.data.UhdAvailabilityPackage;

/**
 * The manager for authorization. It supports APIs for checking the mandatory
 * package, activation package and application package.
 *
 * @author Woosik Lee
 */
public final class AuthorizationManager implements LayeredKeyHandler, TVTimerWentOffListener {

	/** The popup. */
	private Popup popup;
	/** The popup for disable all screen. */
	private Popup disablePopup;
	/** Indicates the no popup. */
	private static final int POPUP_NONE = 0;
	/** Indicates the mandatory bloced popup. */
	private static final int POPUP_MANDATORY_BLOCKED = 1;
	/**
	 * Indicates the application blocked popup is shown at application launching
	 * time.
	 */
	private static final int POPUP_APPLICATION_BLOCKED = 2;

	/** Indicates the kind of popup. */
	private int popupState = POPUP_NONE;

	/** The authorized result of checking authorization. */
	public static final int CHECK_RESULT_AUTHORIZED = 1;
	/** The not authorized result of checking authorization. */
	public static final int CHECK_RESULT_NOT_AUTHORIZED = 2;
	/** The package id file error result of checking authorization. */
	public static final int CHECK_RESULT_ERROR_PACKAGE_FILE = 3;
	/** The eid mapping table error error result of checking authorization. */
	public static final int CHECK_RESULT_ERROR_EID_MAPPING_TABLE = 4;
	/** The CNNH communication error result of checking authorization. */
	public static final int CHECK_RESULT_ERROR_CANH_COMMUNICATION = 5;

	/**
	 * The result that not found package id using the application name in
	 * package file.
	 */
	public static final int CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST = 10;
	/**
	 * The result that not found entitlement id using package id in mapping
	 * table.
	 */
	public static final int CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE = 11;

	/** The instance of TVTimer. It is used to check authorization 1 hour. */
	private TVTimer timer = TVTimer.getTimer();
	/**
	 * The instance of TVTimerSpec. It is used to check mandatory and activation
	 * authorization.
	 */
	private TVTimerSpec timerSpecForMandatoryActivation = new TVTimerSpec();
	/** The instance of TVTimerSpec . It is used to check activation package. */
	// private TVTimerSpec timerSpecForActivation = new TVTimerSpec();
	/** The repeat time for checking mandatory package. */
	private static final long CHECK_TIME_MANDATORY_ACTIVATION = 5 * 60 * 1000L;

	/** The singleton instance of AuthorizationManager. */
	private static AuthorizationManager instance;

	//private AuthrorizationResultManager authResultManager;

	private int iPPVDeactivationAutority;
    
    //Kenneth : 봉인 R4.1 에서 삭제, 추후 DDC 로
	//private int vodDeactivationAutority;

    //->Kenneth[2015.2.4] 4K
	private int uhdAvailabilityAutority;

	public static final long EID_PREFIX = 0x0000000100000000L;

	/**
	 * Mandatory 권한이 없어서 block 된 상태를 나타낸다. 다시 체크 했을 때 권한이 있다면 block 상태를 해지 해야
	 * 한다.
	 */
	private boolean isBlockedSystemByMandatory = false;

	/** To prevent overlapping registration to VOD listener.*/
	private boolean isRegisterVodListener = false;
	private VODServiceListenerImpl vodServiceListenerImpl = new VODServiceListenerImpl();

	private CAResourceAuthorizationListenerImpl caListener = CAResourceAuthorizationListenerImpl.getInstance();

	private static final String LOG_HEADER = "[AuthorizationManager] ";

	/** Flag for test in Alticast lab.*/
	private boolean isTestStb = false;

	/**
	 * Gets the singleton instance of AuthorizationManager.
	 *
	 * @return AuthorizationManager's instance.
	 */
	public static synchronized AuthorizationManager getInstance() {
		if (instance == null) {
			instance = new AuthorizationManager();
		}
		return instance;
	}

	/**
	 * The constructor.
	 */
	private AuthorizationManager() {
		//authResultManager = AuthrorizationResultManager.getInstance();
		long repeatTime = CHECK_TIME_MANDATORY_ACTIVATION;
		String checkTime = DataManager.getInstance().getStbAuthorityCheckRepeatTime();
		if (checkTime != null) {
			try {
				repeatTime = Integer.parseInt(checkTime) * 1000L;
			} catch (Exception e) {
				Log.print(e);
			}
		}
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "start Timer repeat time for mandatory=" + repeatTime);
		}
		try {
			timerSpecForMandatoryActivation.setDelayTime(repeatTime);
			timerSpecForMandatoryActivation.setRepeat(true);
			timerSpecForMandatoryActivation.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(timerSpecForMandatoryActivation);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}

		String testInKorea = DataManager.getInstance().getTestInKorea();
		if(!testInKorea.equalsIgnoreCase("NO")) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "This STB is running in Alticast's lab");
			}
			isTestStb = true;
		}
		else {
			isTestStb = false;
		}

		/*
		 * checkTime =
		 * DataManager.getInstance().getActivationAuthorityCheckRepeatTime(); if
		 * (checkTime != null) { try { repeatTime = Integer.parseInt(checkTime)
		 * * 1000L; } catch (Exception e) { Log.print(e); } }
		 *
		 *
		 * if (Log.DEBUG_ON) {Log.printDebug(
		 * LOG_HEADER + "start Timer repeat time for activation=" +
		 * repeatTime); } try { timerSpecForActivation.setDelayTime(repeatTime);
		 * timerSpecForActivation.setRepeat(true);
		 * timerSpecForActivation.addTVTimerWentOffListener(this);
		 * timer.scheduleTimerSpec(timerSpecForActivation); } catch
		 * (TVTimerScheduleFailedException e) { Log.print(e); }
		 */
	}

	/**
	 * Gets the iPPV Deactivation package's authority.
	 *
	 * @return {@link MonitorService#NOT_AUTHORIZED},
	 *         {@link MonitorService#AUTHORIZED},
	 *         {@link MonitorService#NOT_FOUND_ENTITLEMENT_ID},
	 *         {@link MonitorService#CHECK_ERROR}.
	 */
	public int getIPPVDeactivationPackageAuthority() {
		decideIPPVDeactivationPackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "getIPPVDeactivationPackageAuthority="
					+ iPPVDeactivationAutority);
		}
		return iPPVDeactivationAutority;
	}

    //->Kenneth : 봉인 : R4.1 에서 삭제, 추후 DDC 로
    /*
	public int getVODDeactivationPackageAuthority() {
		decideVODDeactivationPackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "getVODDeactivationPackageAuthority="
					+ vodDeactivationAutority);
		}
		return vodDeactivationAutority;
	}
    */

    //->Kenneth[2015.2.4] 4K
	public int getUhdAvailabilityPackageAuthority() {
		decideUhdAvailabilityPackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "getUhdAvailabilityPackageAuthority="
					+ uhdAvailabilityAutority);
		}
		return uhdAvailabilityAutority;
	}

	/**
	 * If changed the files that are relative authorization, should check
	 * mandatory package and current running application's authority.
	 */
	public void changedAuthorizationFile() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "changedAuthorizationFile ()");
		}

		// 0. Checks the Standalone package
		checkStandalonePackage();

		// 1. Checks the mandatory package and Activation package.
		decideStbStateByAuthority();

		// 2. Checks the iPPV Deactivation package.
		decideIPPVDeactivationPackage();

		// 2-1. Kenneth : Checks the VOD Deactivation package.
        // 봉인 : R4.1 에서 삭제, 추후 DDC 로
		//decideVODDeactivationPackage();

        //->Kenneth[2015.2.4] 4K
		decideUhdAvailabilityPackage();

		// 3. Checks the current running application.
		TransitionEvent event = StateManager.getInstance().getCurrentEvent();
		if (event != null) {
			String appName = event.getApplicationName();
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "current event=" + appName);
			}

			if (appName == null) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "Checking current application's authority is not necessary");
				}
				return;
			}

			int applicationAuthority = AuthorizationManager.getInstance().checkResourceAuthorization(appName);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "applicationAuthority = "
						+ AuthorizationManager.getResultString(applicationAuthority));
			}

			if (applicationAuthority == AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED) {
				// TODO 일반 App 인 경우 해당 앱을 죽이고 팝업을 띄워야 한다. 이 때 팝업 메세지는 어플 띄울 때 뜨는
				// 것과 다를 수 있다.
				// 하지만 VOD 시청 중이라면 VOD 가 종료 될 때 까지 기다려야 한다. ㅡ.ㅡ;;
				// If VOD content is playing, wait end of content.
				if (appName.equals(DataManager.getInstance().getVodName())) {
					try {
						VODService vodService = CommunicationManager.getInstance().getVODService();
						int vodState = vodService.getVODState();
						if(Log.INFO_ON) {
							Log.printInfo(LOG_HEADER + "vod state is " + vodState);
						}
						if(vodState == VODService.APP_WATCHING) {
							// VOD content playing now!
							if(Log.INFO_ON) {
								Log.printInfo("Now VOD content is playing so wait. isRegisterVodListener=" + isRegisterVodListener);
							}
							if(!isRegisterVodListener) {
								vodService.addVODServiceListener(vodServiceListenerImpl);
								isRegisterVodListener = true;
							}
							return;
						}
					} catch (Exception e) {
						Log.print(e);
					}
				}

				// 1. Shows popup
				String[] message = { appName + " is not authorized", "the client must contact a SAC",
						"clerk immediately" };
				showApplicationBlockedPopup(appName, message);
				// 2. Stop current application and exit to channel.
				AppLauncher.getInstance().exitToChannel();
			}
		}

		// Checks all eids.
		// TODO 아래 내용 호출 하지 말자.
		/*
		Thread checkThread = new Thread() {
			public void run() {
				authResultManager.checkAllEIDsAuthority();
			}
		};
		checkThread.start();
		*/
	}

	/**
	 * This function decide STB's authority (Mandatory and Activation). Refer to
	 * Application Implementation Guide's chapter 15 Mandatory and Activation
	 * authority. Mandatory Activation Behavior Display “Demo Profile” or Not
	 * Available STB or Not AUTHORIZED AUTHORIZED X O AUTHORIZED NOT AUTHORIZED
	 * X O NOT AUTHORIZED AUTHORIZED O O NOT AUTHORIZED NOT AUTHORIZED X X
	 *
	 */
	public void decideStbStateByAuthority() {
		boolean isMandatoryAuthorized = true;
		boolean isActivationAuthorized = false;

		if(isTestStb) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "This STB is running in Alticast's lab so do not check Mandatory package");
			}
			return;
		}

		int mandatoryAuthResult = checkMandatoryPackage1();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "decideStbStateByAuthority() result=" + mandatoryAuthResult);
		}
		switch (mandatoryAuthResult) {
		case AuthorizationManager.CHECK_RESULT_AUTHORIZED:
		case AuthorizationManager.CHECK_RESULT_ERROR_PACKAGE_FILE:
		case AuthorizationManager.CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
		case AuthorizationManager.CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			isMandatoryAuthorized = true;
			break;
		case AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED:
			isMandatoryAuthorized = false;
			break;
		}

		int activationAuthResult = checkActivationPackage();
		switch (activationAuthResult) {
		case AuthorizationManager.CHECK_RESULT_AUTHORIZED:
			isActivationAuthorized = true;
			break;
		case AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED:
		case AuthorizationManager.CHECK_RESULT_ERROR_PACKAGE_FILE:
		case AuthorizationManager.CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
		case AuthorizationManager.CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			isActivationAuthorized = false;
			break;
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "decideStbStateByAuthority()" + isMandatoryAuthorized + ", "
					+ isActivationAuthorized + ", isBlockedSystemByMandatory=" + isBlockedSystemByMandatory);
		}

		if (isMandatoryAuthorized && isActivationAuthorized) {
			CommunicationManager.getInstance().setDeactivatedSTB(false);
			if (isBlockedSystemByMandatory) {
				stopMandatoryBlocked();
			}
		} else if (isMandatoryAuthorized && !isActivationAuthorized) {
			CommunicationManager.getInstance().setDeactivatedSTB(false);
			if (isBlockedSystemByMandatory) {
				stopMandatoryBlocked();
			}
		} else if (!isMandatoryAuthorized && isActivationAuthorized) {
			CommunicationManager.getInstance().setDeactivatedSTB(true);
			if (isBlockedSystemByMandatory) {
				stopMandatoryBlocked();
			}
		} else {
			// !isMandatoryAuthorized && !isActivationAuthorized
			CommunicationManager.getInstance().setDeactivatedSTB(false);
			if (isBlockedSystemByMandatory == false) {
				startMandatoryBlocked();
			}
		}
	}

	/**
	 * Returns the weather STB is available or not by Mandatory package.
	 *
	 * @return
	 */
	public boolean isBlockedSystemByMandatory() {
		return isBlockedSystemByMandatory;
	}

	/**
	 * This function is called when the STB have not mandatory permission.
	 */
	public void startMandatoryBlocked() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "startMandatoryBlocked()");
		}

		// If VOD content is playing, wait end of content.
		try {
			VODService vodService = CommunicationManager.getInstance().getVODService();
			int vodState = vodService.getVODState();
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "vod state is " + vodState);
			}
			if(vodState == VODService.APP_WATCHING) {
				// VOD content is playing.
				if(Log.INFO_ON) {
					Log.printInfo("Now VOD content is playing so wait - by Mandatory isRegisterVodListener=" + isRegisterVodListener);
				}
				if(!isRegisterVodListener) {
					vodService.addVODServiceListener(vodServiceListenerImpl);
					isRegisterVodListener = true;
				}
				return;
			}
		} catch (Exception e) {
			Log.print(e);
		}

		ScreenSaverManager.getInstance().resetTimer();

		int curMode = STBModeManager.getInstance().getMode();
		switch (curMode) {
		case MonitorService.STB_MODE_NO_SIGNAL:
		case MonitorService.STB_MODE_STANDALONE :
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Current STB mode is No signal or Standalone so ignore mandatory blocked");
			}
			return;
		}

		isBlockedSystemByMandatory = true;

		FrontPanelDisplayer.getInstance().showText("----");

		STBModeManager.getInstance().setMode(MonitorService.STB_MODE_MANDATORY_BLOCKED);

		// 0. Stops the Loading animation if launched.
		CommunicationManager.getInstance().stopLoadingAnimation();

		// 1. Show popup first and blocking screen by black popup.
		showBlockingAllServicePopup();

		// 2 Stops current unbound application.
		AppLauncher.getInstance().exitToChannelWithoutMenuAndAV();

		// 3. Stop AV Service.
		ServiceContext serviceContext = Environment.getServiceContext(0);
		serviceContext.stop();
	}

	private void stopMandatoryBlocked() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "stopMandatoryBlocked()");
		}

		int curMode = STBModeManager.getInstance().getMode();
		switch (curMode) {
		case MonitorService.STB_MODE_NO_SIGNAL:
		case MonitorService.STB_MODE_STANDALONE :
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Current STB mode is No signal or Standalone so ignore mandatory blocked");
			}
			return;
		}

		STBModeManager.getInstance().setMode(MonitorService.STB_MODE_NORMAL);

		isBlockedSystemByMandatory = false;
		hidePopup();

		// Check Wizard is needed... and launch wizard or exitToChannel()
		if(!checkWizardNeeded())  {
		// Start AV.
			AppLauncher.getInstance().exitToChannel();
		}
	}

	public boolean checkWizardNeeded() {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkWizardNeeded()");
		}
		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if(!preManager.isReadyPreferenceService()) {
			return false;
		}

		InbandDataManager.getInstance().status.updateLog("check completeWizard");
		String completeWizard = null;

		String result = preManager.getPreferenceValue(PreferenceServiceProxy.PNAME_COMPLETE_WIZARD);
		if (result != null) {
			completeWizard = result;
		}

		if ((completeWizard == null || !completeWizard.equalsIgnoreCase("true")) && !Environment.EMULATOR) {
			// Launches the Auto activation application(Wizard).
			String wizardName = DataManager.getInstance().getWizardName();
			UnboundAppProxy wizard = UnboundAppsManager.getInstance().getUnboundAppProxy(wizardName);
			if (wizard == null) {
				if (Log.ERROR_ON) {
					Log.printError(LOG_HEADER + "ERROR Not found wizard application");
				}
				return false;
			}
			// Standby 상태에서 여기가 실행 될 수도 있는데, 이경우 StateManager 가 STANDBY 라면 App실행이 안 되기 때문에 강제로 RUN 으로 해 준다.
			StateManager.getInstance().setRunningState(Host.FULL_POWER);

			// stops the loading animation for wizard.
			CommunicationManager.getInstance().stopLoadingAnimation();
			String[] parameter = { FrameworkMain.getInstance().getApplicationName() };
			AppLauncher.getInstance().startUnboundApplication(DataManager.getInstance().getWizardName(), parameter);
			FrontPanelDisplayer.getInstance().showText("ALLO");
			GeneralPreferenceUtil.write(Log.UP_NAME_CONSOLE_LEVEL, String.valueOf(Log.LEVEL_OFF + 8));

			Thread frontPanelThread = new Thread() {
				public void run() {
					boolean dsgOk = DsgVerifier.getInstance().checkDsgState();
					if(!dsgOk) {
						try {
							Thread.sleep(2000L);
						} catch (Exception e) {
							e.printStackTrace();
						}
						dsgOk = DsgVerifier.getInstance().checkDsgState();
						if(!dsgOk) {
							FrontPanelDisplayer.getInstance().showText("nBid");
						}
					}

				}
			};
			frontPanelThread.start();

			return true;
		}
		return false;
	}

	/**
	 * Stops the blocked application.
	 *
	 * @param appName
	 *            the blocked application name.
	 */
	public void startApplicationBlocked(String appName) {
		String[] message = { appName + " is not authorized", "the client must contact a SAC", "clerk immediately" };
		showApplicationBlockedPopup(appName, message);
	}

	/**
	 * Checks authorization using entitlement id.
	 *
	 * @param entitlementID
	 *            the Eid.
	 * @return if result is authorized, return true. otherwise return false.
	 */
	public synchronized int checkSourceAuthorization(short entitlementID) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkSourceAuthorization EID=" + entitlementID);
		}
		try {
			CAHandler caHandler = CANHManager.getInstance().getCAHandler();

			long id = EID_PREFIX | (long) entitlementID;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "checkSourceAuthorization Prefixed id=" + id);
			}
			CAAuthorization caResult = caHandler.getResourceAuthorization(id, caListener);
			boolean result = caResult.isAuthorized();
			if (result) {
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "checkSourceAuthorization result is Authorized");
				}
				return CHECK_RESULT_AUTHORIZED;
			} else {
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "checkSourceAuthorization result is Not Authorized");
				}
				return CHECK_RESULT_NOT_AUTHORIZED;
			}
		} catch (Exception e) {
			Log.print(e);
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

	}

	/**
	 * Checks the Standalone packages authorization.
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	public int checkStandalonePackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkStandalonePackage()");
		}

		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Standalone package ids.
		PvrStandalonePackage[] stdPackages = EIDDataManager.getInstance().getStandalonePackage();
		if (stdPackages == null || stdPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < stdPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(stdPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any one package id was authorized, the standalne
					// package is authorized!
					storeStandaloneAuthorityResult(CHECK_RESULT_AUTHORIZED, entitlementID);
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.WARNING_ON) {
					Log.printWarning(LOG_HEADER + "not found EID finding PackageID="
							+ stdPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}
		storeStandaloneAuthorityResult(CHECK_RESULT_NOT_AUTHORIZED, (short) -1);
		return CHECK_RESULT_NOT_AUTHORIZED;

	}

	/**
	 * Checks the PVR Multiroom packages authorization.
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	public int checkPvrMultiroomPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkPvrMultiroomPackage()");
		}

		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the PVR multiroom package ids.
		PvrMultiroomPackage[] multiroomPackages = EIDDataManager.getInstance().getMultiroomPackage();
		if (multiroomPackages == null || multiroomPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < multiroomPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance()
					.findEidByPackageId(multiroomPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any one package id was authorized, the pvr multiroom
					// package is authorized!
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.WARNING_ON) {
					Log.printWarning(LOG_HEADER + "not found EID finding PackageID="
							+ multiroomPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}
		return CHECK_RESULT_NOT_AUTHORIZED;
	}

	/**
	 * Stores the result of Standlone's authority.
	 *
	 * @param authorityResult
	 *            the result.
	 */
	private void storeStandaloneAuthorityResult(int authorityResult, short entitlementID) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "storeStandaloneAuthorityResult(" + authorityResult + ")");
		}

		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if (preManager.isReadyPreferenceService()) {
			preManager.setPreferenceValue(PreferenceServiceProxy.PNAME_STANDALONE_AUTHORITY_RESULT_NAME, String
					.valueOf(authorityResult));
			preManager.setPreferenceValue(PreferenceServiceProxy.PNAME_STANDALONE_AUTHORITY_EID_NAME, String
					.valueOf(entitlementID));
		}
	}

	/**
	 * Returns the stored Standalone package's authority.
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	public int getStoredStandaloneAuthority() {
		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if (preManager.isReadyPreferenceService()) {
			String result = preManager
					.getPreferenceValue(PreferenceServiceProxy.PNAME_STANDALONE_AUTHORITY_RESULT_NAME);
			try {
				int value = Integer.parseInt(result);
				return value;
			} catch (NumberFormatException e) {
				Log.print(e);
			}
		}
		return -1;
	}

	public short getStoredStandaloneEntitlementId() {
		PreferenceServiceProxy preManager = PreferenceServiceProxy.getInstance();
		if (preManager.isReadyPreferenceService()) {
			String result = preManager.getPreferenceValue(PreferenceServiceProxy.PNAME_STANDALONE_AUTHORITY_EID_NAME);
			try {
				short value = Short.parseShort(result);
				return value;
			} catch (NumberFormatException e) {
				Log.print(e);
			}
		}
		return -1;
	}

	/**
	 * It is not used since 2011.06.13
	 * Checks the mandatory package's authorization.
	 * If all package ids are authorized, Mandatory is authorized.
	 * {@see AuthorizationManager#checkMandatoryPackage1()};
	 *
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	private int checkMandatoryPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkMandatoryPackage()");
		}
		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		MandatoryPackage[] manPackages = EIDDataManager.getInstance().getMandatoryPackages();
		if (manPackages == null || manPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		for (int i = 0; i < manPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(manPackages[i].getPackageName());
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "package[" + i + "]=" + manPackages[i].getPackageName()
						+ ", EID=" + entitlementID);
			}
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "result = " + result);
				}
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If all package ids are authorized, the mandatory package
					// is authorized! (Mandatory package only)

				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					// If any package id was not authorized, the mandatory
					// package is not authorized!
					return CHECK_RESULT_NOT_AUTHORIZED;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}
			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ manPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_AUTHORIZED;
	}

	/**
	 * Checks the mandatory package's authorization.
	 * If any package id is authorized, Mandatory is authorized.
	 * {@see AuthorizationManager#checkMandatoryPackage()};
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	private int checkMandatoryPackage1() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkMandatoryPackage1()");
		}
		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		MandatoryPackage[] manPackages = EIDDataManager.getInstance().getMandatoryPackages();
		if (manPackages == null || manPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		for (int i = 0; i < manPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(manPackages[i].getPackageName());
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "package[" + i + "]=" + manPackages[i].getPackageName()
						+ ", EID=" + entitlementID);
			}
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "result = " + result);
				}
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any package id authorized, the mandatory package
					// is authorized!
					return CHECK_RESULT_AUTHORIZED;

				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					//return CHECK_RESULT_NOT_AUTHORIZED;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "CANH Error occurs");
					}
					errorCount++;
				}
			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ manPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}

	/**
	 * Checks the activation packages authorization.
	 *
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHEKC_RESULT_ERROR_CANH_COMMUNICATION}
	 *         .
	 */
	private int checkActivationPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkActivationPackage()");
		}

		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		ActivationPackage[] actPackages = EIDDataManager.getInstance().getActivationPackages();
		if (actPackages == null || actPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < actPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(actPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if(Log.INFO_ON) {
					Log.printInfo("AuthorizationManager : [" + i + "] packageId=" + actPackages[i].getPackageName() + ",result = " + result);
				}
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any package id was authorized, the activation package
					// is authorized!
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ actPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}

	/**
	 * Checks the application's authority. Finds package names including
	 * applicatioName from EIDDataManager and finds EID by package names in
	 * mapping table. And checks authority via CANH API by EIDs. if not found
	 * package id using given application name, returns
	 * {@link AuthorizationManager#CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST}
	 * . if not found entitlement id using found package id, returns
	 * {@link AuthorizationManager#CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE}.
	 *
	 * @param applicationName
	 *            the requested application name.
	 * @return {@link AuthorizationManager#CHECK_RESULT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_AUTHORIZED},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_PACKAGE_FILE},
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_EID_MAPPING_TABLE}
	 *         ,
	 *         {@link AuthorizationManager#CHECK_RESULT_ERROR_CANH_COMMUNICATION}
	 *         ,
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST}
	 *         .
	 *         {@link AuthorizationManager#CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE}
	 *         .
	 */
	public int checkResourceAuthorization(String applicationName) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkResourceAuthorization() application=" + applicationName);
		}

		if(isTestStb) {
			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "This STB is running in Alticast's lab so return CHECK_RESULT_AUTHORIZED");
			}
			return CHECK_RESULT_AUTHORIZED;
		}

		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// Step 1. finds package ids using application name.
		ApplicationPackage[] appPackages = EIDDataManager.getInstance().getApplicationPackages();
		if (appPackages == null || appPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}
		String[] foundPackageIds = null;
		for (int i = 0; i < appPackages.length; i++) {
			if (appPackages[i].getPackageName().equals(applicationName)) {
				foundPackageIds = appPackages[i].getPackageIDs();
				break;
			}
		}

		if (foundPackageIds == null || foundPackageIds.length < 1) {
			return CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST;
		}

		if (Log.INFO_ON) {
			for (int i = 0; i < foundPackageIds.length; i++) {
				Log.printInfo(LOG_HEADER + "found package id=" + foundPackageIds[i]);
			}
		}

		// Step 2. find entitlement ids using found package id.
		Vector foundEId = new Vector();
		for (int i = 0; i < foundPackageIds.length; i++) {
			short id = EIDDataManager.getInstance().findEidByPackageId(foundPackageIds[i]);
			if (id != Short.MIN_VALUE) {
				foundEId.add(new Short(id));
			}
		}

		if (foundEId.size() < 1) {
			return CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE;
		}

		if (Log.INFO_ON) {
			for (int i = 0; i < foundEId.size(); i++) {
				Log.printInfo(LOG_HEADER + "found eid id=" + foundEId.get(i));
			}
		}

		int countOfCommunicationError = 0;
		for (int i = 0; i < foundEId.size(); i++) {
			short eid = ((Short) foundEId.get(i)).shortValue();
			int result = checkSourceAuthorization(eid);
			switch (result) {
			case CHECK_RESULT_AUTHORIZED:
				// If provisioned at least one of packages, it is authorized.
				return CHECK_RESULT_AUTHORIZED;
			case CHECK_RESULT_NOT_AUTHORIZED:
				break;
			case CHECK_RESULT_ERROR_CANH_COMMUNICATION:
				countOfCommunicationError++;
				break;
			}

			// Updates the result cache.
			//authResultManager.checkResult(eid, result);
		}
		// If all communication has been error via CHNH APIs, returns CANH
		// error.
		if (countOfCommunicationError == foundEId.size()) {
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}

	public void decideIPPVDeactivationPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "decideIPPVDeactivationPackage()");
		}
		int tempIPPV = checkIPPVDeactivationPackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "tempIPPV=" + tempIPPV);
		}
		switch (tempIPPV) {
		case CHECK_RESULT_ERROR_PACKAGE_FILE:
		case CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
			this.iPPVDeactivationAutority = MonitorService.NOT_FOUND_ENTITLEMENT_ID;
			break;
		case CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			this.iPPVDeactivationAutority = MonitorService.CHECK_ERROR;
			break;
		case CHECK_RESULT_AUTHORIZED:
			this.iPPVDeactivationAutority = MonitorService.AUTHORIZED;
			break;
		case CHECK_RESULT_NOT_AUTHORIZED:
			this.iPPVDeactivationAutority = MonitorService.NOT_AUTHORIZED;
			break;
		}
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "iPPVDeactivationAutority=" + iPPVDeactivationAutority);
		}
	}

    //->Kenneth : VOD Deactivation package 가 authorized 되었는지 판단한다
    // 봉인 : R4.1 에서 삭제 추후 DDC 로
    /*
	public void decideVODDeactivationPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "Kenneth : decideVODDeactivationPackage()");
		}
		int tempVOD = checkVODDeactivationPackage();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "tempVOD=" + tempVOD);
		}
		switch (tempVOD) {
		case CHECK_RESULT_ERROR_PACKAGE_FILE:
		case CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
			this.vodDeactivationAutority = MonitorService.NOT_FOUND_ENTITLEMENT_ID;
			break;
		case CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			this.vodDeactivationAutority = MonitorService.CHECK_ERROR;
			break;
		case CHECK_RESULT_AUTHORIZED:
			this.vodDeactivationAutority = MonitorService.AUTHORIZED;
			break;
		case CHECK_RESULT_NOT_AUTHORIZED:
			this.vodDeactivationAutority = MonitorService.NOT_AUTHORIZED;
			break;
		}
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "vodDeactivationAutority=" + vodDeactivationAutority);
		}
	}
    */

    //->Kenneth[2015.2.4] 4K
	public void decideUhdAvailabilityPackage() {
        try {
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "Kenneth : decideUhdAvailabilityPackage()");
            }
            int tempUhd = checkUhdAvailabilityPackage();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "tempUhd=" + tempUhd);
            }
            switch (tempUhd) {
            case CHECK_RESULT_ERROR_PACKAGE_FILE:
            case CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
                this.uhdAvailabilityAutority = MonitorService.NOT_FOUND_ENTITLEMENT_ID;
                break;
            case CHECK_RESULT_ERROR_CANH_COMMUNICATION:
                this.uhdAvailabilityAutority = MonitorService.CHECK_ERROR;
                break;
            case CHECK_RESULT_AUTHORIZED:
                this.uhdAvailabilityAutority = MonitorService.AUTHORIZED;
                break;
            case CHECK_RESULT_NOT_AUTHORIZED:
                this.uhdAvailabilityAutority = MonitorService.NOT_AUTHORIZED;
                break;
            }
            
            if (this.uhdAvailabilityAutority == MonitorService.AUTHORIZED) {
            	PreferenceServiceProxy.getInstance().setPreferenceValue(MonitorService.SUPPORT_UHD, Boolean.toString(true));
            } else {
            	PreferenceServiceProxy.getInstance().setPreferenceValue(MonitorService.SUPPORT_UHD, Boolean.toString(false));
            }
            
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "uhdAvailabilityAutority=" + uhdAvailabilityAutority);
            }
        } catch (Exception e) {
            Log.print(e);
        }
	}

	private int checkIPPVDeactivationPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkIPPVDeactivationPackage()");
		}
		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		IPPVDeactivationPackage[] iPPVPackages = EIDDataManager.getInstance().getIppvDeactivationPackage();
		if (iPPVPackages == null || iPPVPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < iPPVPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(iPPVPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any package id was authorized, the ippv package is
					// authorized!
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ iPPVPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}

    //->Kenneth : 내부 함수. 실제 여기서 VOD Deactivation 을 체크한다
    // 봉인 : R4.1 에서 삭제, 추후 DDC 로
    /*
	private int checkVODDeactivationPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkVODDeactivationPackage()");
		}
		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		VODDeactivationPackage[] vodPackages = EIDDataManager.getInstance().getVODDeactivationPackage();
		if (vodPackages == null || vodPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < vodPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(vodPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any package id was authorized, the vod package is
					// authorized!
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ vodPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}
    */

    //->Kenneth[2015.2.4] 4K
    //->Kenneth : 내부 함수. 실제 여기서 UHD availability 를 체크한다
	private int checkUhdAvailabilityPackage() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "checkUhdAvailabilityPackage()");
		}

		if (!EIDDataManager.getInstance().hasPackageFile()) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		if (!EIDDataManager.getInstance().hasPackageIdEIDMappingTableFile()) {
			return CHECK_RESULT_ERROR_EID_MAPPING_TABLE;
		}

		// First, reads the Mandatory package ids.
		UhdAvailabilityPackage[] uhdPackages = EIDDataManager.getInstance().getUhdAvailabilityPackage();
		if (uhdPackages == null || uhdPackages.length < 1) {
			return CHECK_RESULT_ERROR_PACKAGE_FILE;
		}

		int checkCount = 0;
		int errorCount = 0;
		int notAuthorizedCount = 0;
		for (int i = 0; i < uhdPackages.length; i++) {
			// Second, find the entitlement id using mapping table.
			short entitlementID = EIDDataManager.getInstance().findEidByPackageId(uhdPackages[i].getPackageName());
			if (entitlementID != Short.MIN_VALUE) {
				checkCount++;
				int result = checkSourceAuthorization(entitlementID);
				if (result == CHECK_RESULT_AUTHORIZED) {
					// If any package id was authorized, the vod package is
					// authorized!
					return CHECK_RESULT_AUTHORIZED;
				} else if (result == CHECK_RESULT_NOT_AUTHORIZED) {
					notAuthorizedCount++;
				} else if (result == CHECK_RESULT_ERROR_CANH_COMMUNICATION) {
					errorCount++;
				}

			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "not found EID finding PackageID="
							+ uhdPackages[i].getPackageName());
				}
			}
		}

		if (errorCount == checkCount) {
			// If all request from CANH was failed, returns error.
			return CHECK_RESULT_ERROR_CANH_COMMUNICATION;
		}

		return CHECK_RESULT_NOT_AUTHORIZED;
	}

	/**
	 * Returns the result message.
	 *
	 * @param result
	 *            .
	 * @return result the message of given result.
	 */
	public static String getResultString(int result) {
		switch (result) {
		case CHECK_RESULT_AUTHORIZED:
			return "CHECK_RESULT_AUTHORIZED";
		case CHECK_RESULT_NOT_AUTHORIZED:
			return "CHECK_RESULT_NOT_AUTHORIZED";
		case CHECK_RESULT_ERROR_PACKAGE_FILE:
			return "CHECK_RESULT_ERROR_PACKAGE_FILE";
		case CHECK_RESULT_ERROR_EID_MAPPING_TABLE:
			return "CHECK_RESULT_ERROR_EID_MAPPING_TABLE";
		case CHECK_RESULT_ERROR_CANH_COMMUNICATION:
			return "CHECK_RESULT_ERROR_CANH_COMMUNICATION";
		case CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST:
			return "CHECK_RESULT_NOT_FOUND_APPLICAION_ID_IN_PACKAGE_LIST";
		case CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE:
			return "CHECK_RESULT_NOT_FOUND_EID_IN_MAPPING_TABLE";
		}
		return "UNKNOWN RESULT";
	}

	// //////////////////////////
	// Popup Implementation.
	// //////////////////////////

	/**
	 * All service block.
	 *
	 * @param message
	 *            the popup message.
	 */
	private void showBlockingAllServicePopup() {
		CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_502, null);

		// hidePopup();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "showMandatoryBlockedPopup()");
		}
		synchronized (this) {
			popupState = POPUP_MANDATORY_BLOCKED;
			WindowProperty wp = WindowProperty.AUTHORITY_BLOCKED_POPUP;
			disablePopup = new DisablePopup(wp.getPriority(), this);
			disablePopup.showPopup();
		}

	}

	/**
	 * Shows the blocked popup to user by application blocked at application
	 * launching time.
	 *
	 * @param appName
	 *            the blocked application name.
	 * @param message
	 *            the popup message.
	 */
	private void showApplicationBlockedPopup(String appName, String[] message) {
		CommunicationManager.getInstance().showErrorMessage(CommunicationManager.ERROR_501, null);
		/*
		 * hidePopup(); if (Log.INFO_ON) {Log.printInfo(
		 * LOG_HEADER + "showApplicationBlockedPopup()"); }
		 * synchronized (this) { popupState = POPUP_APPLICATION_BLOCKED;
		 * WindowProperty wp = WindowProperty.AUTHORITY_BLOCKED_POPUP; popup =
		 * new BasicConfirm("WARNING", wp.getName(), wp.getPriority(), message,
		 * this); popup.showPopup(); }
		 */
	}

	/**
	 * Hides the blocked popup.
	 */
	private void hidePopup() {
		CommunicationManager.getInstance().hideErrorMessage();

		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "hidePopup()");
		}
		synchronized (this) {
			if (disablePopup != null) {
				disablePopup.hidePopup();
				disablePopup.dispose();
				disablePopup = null;
			}
			popupState = POPUP_NONE;
		}
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "userEventReceived popupState=" + popupState + ", event="
					+ event);
		}

		if (popupState == POPUP_MANDATORY_BLOCKED) {
			// Now mandatory blocked. so all key is blocked.
			return true;
		}

		if (event.getType() != KeyEvent.KEY_PRESSED) {
			return false;
		}

		int keyCode = event.getCode();
		switch (keyCode) {
		case OCRcEvent.VK_ENTER:
			hidePopup();
			break;
		default:
			break;
		}

		return true;
	}

	/**
	 * This class receives the VOD event. If vod application enters to the not
	 * authorized state, the monitor disposes the vod application. But vod
	 * application playing content now, we wait the end of content.
	 *
	 * @author Woosik Lee
	 */
	private class VODServiceListenerImpl implements VODServiceListener {
		/**
         *
         */
		public void receiveVODEvent(int event) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "receiveVODEvent=" + event);
			}
			if (event == VODEvents.STOP) {
				// 1. Shows popup
				//String[] message = { "VOD is not authorized", "the client must contact a SAC", "clerk immediately" };
				//showApplicationBlockedPopup(DataManager.getInstance().getVodName(), message);
				// 2. Stop current application and exit to channel.
				//AppLauncher.getInstance().exitToChannel();
				// Check VOD authority and Mandatory authority

				try {
					VODService vodService = CommunicationManager.getInstance().getVODService();
					if(Log.INFO_ON) {
						Log.printInfo(LOG_HEADER + "Remove VOD service listener");
					}
					vodService.removeVODServiceListener(vodServiceListenerImpl);
					isRegisterVodListener = false;
				} catch (Exception e) {
					Log.print(e);
				}

				changedAuthorizationFile();
			}
		}
	}

	/**
	 * This is timer call back. It will check the authorization.
	 *
	 * @param event
	 *            TVTimer event.
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "timerWentOff() ");
		}

		TVTimerSpec timerSpec = event.getTimerSpec();
		if (timerSpec.equals(timerSpecForMandatoryActivation)) {
			// Mandatory
			// Checks the mandatory package
			decideStbStateByAuthority();
			/*
			 * int mandatoryAuthority = checkMandatoryPackage(); if
			 * (Log.INFO_ON) {
			 * Log.printInfo(LOG_HEADER + "mandatoryAuthority = "
			 * + AuthorizationManager.getResultString(mandatoryAuthority)); }
			 *
			 * switch (mandatoryAuthority) { case
			 * AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED: // Wow! this
			 * STB is not authorized. All service will be blocked! if
			 * (Log.INFO_ON) { Log.printInfo(
			 * LOG_HEADER + "This STB is not authorized. All service should blocked!"
			 * ); } startMandatoryBlocked(); break; case
			 * AuthorizationManager.CHECK_RESULT_AUTHORIZED: if (Log.INFO_ON) {
			 * Log.printInfo(
			 * LOG_HEADER + "This STB is authorized. isBlockedSystemByMandatory="
			 * + isBlockedSystemByMandatory); } if (isBlockedSystemByMandatory
			 * == true) { stopMandatoryBlocked(); } break; }
			 */
		}
		/*
		 * else if (timerSpec.equals(timerSpecForActivation)) { // Activation
		 * int activationAuthority = checkActivationPackage(); if (Log.INFO_ON)
		 * {
		 * Log.printInfo(LOG_HEADER + "activateionAuthority = "
		 * + AuthorizationManager.getResultString(activationAuthority)); }
		 *
		 * if (activationAuthority ==
		 * AuthorizationManager.CHECK_RESULT_AUTHORIZED) { try { MainMenuService
		 * menuService =
		 * CommunicationManager.getInstance().getMainMenuService();
		 * menuService.setDeactivatedSTB(false); } catch (Exception e) {
		 * Log.print(e); } } else { if (activationAuthority ==
		 * AuthorizationManager.CHECK_RESULT_NOT_AUTHORIZED) { try {
		 * MainMenuService menuService =
		 * CommunicationManager.getInstance().getMainMenuService(); // Demo
		 * Profile 을 Menu 에 보인다. Activation 권한이 없는 경우이니...
		 * menuService.setDeactivatedSTB(true); } catch (Exception e) {
		 * Log.print(e); } } }
		 *
		 * }
		 */

	}
}

package com.videotron.tvi.illico.monitor.core;

import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;

import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;

public class StandbyAndRunningModeManager implements PowerModeChangeListener {

	private static StandbyAndRunningModeManager instance;

	/** The log header message. */
	private static final String LOG_HEADER = "[StandbyAndRunningModeManager] ";

	public synchronized static StandbyAndRunningModeManager getInstance() {
		if (instance == null) {
			instance = new StandbyAndRunningModeManager();
		}
		return instance;
	}

	private StandbyAndRunningModeManager() {
		Host.getInstance().addPowerModeChangeListener(this);
	}

	public void dispose() {
		Host.getInstance().removePowerModeChangeListener(this);
	}

	/**
	 * Enter to running mode.
	 */
	private synchronized void enterRunningMode() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "enterRunningMode() cur mode="
					+ STBModeManager.getInstance().getMode());
		}
		
		String[] vbmEvents = {MonitorVbmController.EVENT_POWER_ON};
		MonitorVbmController.getInstance().write(MonitorVbmController.ID_POWER_EVENT, null, vbmEvents, false);
		
		WatchingTimeRestrictionManager.getInstance().powerModeChanged(Host.FULL_POWER);
		
		AuthorizationManager.getInstance().decideStbStateByAuthority();
		boolean isBlockedByMandatoryPermission = AuthorizationManager.getInstance().isBlockedSystemByMandatory();
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "" + ", isBlockedByMandatoryPermission="
					+ isBlockedByMandatoryPermission);
		}
		// If STB is blocked by Mandatory package, Do not tune to LCW.
		if (!isBlockedByMandatoryPermission) {
			if (STBModeManager.getInstance().getMode() == MonitorService.STB_MODE_BOOTING) {
				STBModeManager.getInstance().setMode(MonitorService.STB_MODE_NORMAL);
			}
			
			// Check Wizard is needed...
			if(!AuthorizationManager.getInstance().checkWizardNeeded())  {
				// Wizard 가 필요 없는 경우에 RUN 으로 간다.
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Wizard is not needed so process to normal running mode");
				}
				TransitionEvent event = new TransitionEvent(TransitionEvent.RUN);
				StateManager.getInstance().requestStateTransition(event);
			}
			else {
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Wizard is started");
				}
			}
		}
		else {
			// Sets the flag for StateManager.
			StateManager.getInstance().setRunningState(Host.FULL_POWER);
		}
	}

	/**
	 * Enter to standby mode.
	 */
	private synchronized void enterStandbyMode() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "enterStandbyMode()");
		}
		
		String[] vbmEvents = {MonitorVbmController.EVENT_POWER_OFF};
		MonitorVbmController.getInstance().write(MonitorVbmController.ID_POWER_EVENT, null, vbmEvents, false);

		WatchingTimeRestrictionManager.getInstance().powerModeChanged(Host.LOW_POWER);
		
		try {
			SearchService searchService = CommunicationManager.getInstance().getSearchService();
			if (searchService != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug("StandbyAndRunningModeManager: stop search ");
				}
				searchService.stopSearchApplication();
			}
		} catch (Exception e) {
			Log.print(e);
		}
		
		StateManager.getInstance().hideConfirmExitSpecialVideo();

		TransitionEvent event = new TransitionEvent(TransitionEvent.STANDBY);
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "go to standby!");
		}
		StateManager.getInstance().requestStateTransition(event);

	}

	/**
	 * Implements the org.ocap.hardware.PowerModeChangeListener.
	 *
	 * @param newPowerMode
	 *            the new power mode.
	 */
	public synchronized void powerModeChanged(int newPowerMode) {
		
		int bootState = MonitorMain.getInstance().getCurrentBootState();
		
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "newPowerMode=" + newPowerMode + ", bootState=" + bootState);
		}
		
		if(bootState != MonitorMain.BOOT_STATE_FINISHED) {
			return;
		}

		if (newPowerMode == Host.FULL_POWER) {
			enterRunningMode();
		} else {
			// Stops the Search application
			enterStandbyMode();
		}

	}

}

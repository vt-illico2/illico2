package com.videotron.tvi.illico.monitor.core;

import java.util.StringTokenizer;
import java.util.Vector;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.havi.ui.event.HRcEvent;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;
import org.ocap.net.OcapLocator;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.ixc.search.SearchService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.data.DataManager;
import com.videotron.tvi.illico.monitor.data.KeyNames;
import com.videotron.tvi.illico.monitor.ui.popup.BasicConfirm;
import com.videotron.tvi.illico.monitor.ui.popup.BasicDialog;
import com.videotron.tvi.illico.monitor.ui.popup.BasicPopup;
import com.videotron.tvi.illico.monitor.util.SimpleAlarm;
import com.videotron.tvi.illico.monitor.util.SimpleAlarmListener;
import com.videotron.tvi.illico.util.Environment;
import com.videotron.tvi.illico.util.WindowProperty;

public class InbandDataUpdateController implements PowerModeChangeListener, InternalInbandDataListener,
		LayeredKeyHandler, TVTimerWentOffListener, SimpleAlarmListener {

	private static InbandDataUpdateController instance = null;

	/** Waiting update signal. */
	private static final int STATE_WAITING = 0;
	/** Canceled update by user. */
	private static final int STATE_UPDATE_DELAYED = 1;
	/** Update dialog is showing. */
	private static final int STATE_POPUP_DIALOG = 2;
	/** Update popup that user can't cancel is showing. */
	private static final int STATE_POPUP_CONFIRM = 3;
	/** Update now. */
	private static final int STATE_UPDATING = 4;
	/** Current state. */
	private int currentState = STATE_WAITING;

	/** Update try count. 0 보다 크면 업데이트 시그널이 왔다는 뜻이다. */
	private int updateRetryCount = 0;

	/** Allowed delay time. */
	private static String MAX_RETRY_COUNT = "6";
	/**
	 * If IB update is canceled, the next IB update popup will be appearing
	 * after following delay time.
	 */
	private static String RETRY_TIME = "14400"; // 60*60*4 (4hour)
	/** Popup continues time. */
	private static String POPUP_COUNT_DOWN_TIME = "120"; // 2 min

	private static String IGNORE_TIME_NEXT_UPDATE = "300"; // 5min

	private static String IB_UPDATE_TIME = "03:00";

	private static final String LOG_HEADER = "[InbandUpdateControl] ";

	/** The dialog popup */
	private BasicPopup dialog = null;
	/** The instance of TVTimer. It is used to waiting popup's continual time. */
	private TVTimer timer = TVTimer.getTimer();
	/**
	 * The instance of TVTimerSpec. It is used to waiting popup's continual
	 * time.
	 */
	private TVTimerSpec timerSpec = new TVTimerSpec();

	/** Retry timer every 4 hour. */
	private TVTimerSpec retryTimerSpec = new TVTimerSpec();

	// Cancel 한 이후에 특정 시간 안에 올라오는 dataUpdate() 에 대해 무시 하기 위해 cancel 한 시간을 저장 한 값이다.
	private long lastCanceledTime = -1;
	/** The alarm class instance. */
	private SimpleAlarm alarm;
	
	/** PVR에서 강제로 업데이트 된 경우 ServiceSelection 하기 때문에 끝난 다음에 LCW Tune 해 줘야 한다. */
	/** In PVR if update occurs with service selection, must tune to LCW after update. */
	private boolean forceLcwInPvr = false;
	
	public synchronized static void init() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " init()");
		}
		if (instance == null) {
			instance = new InbandDataUpdateController();
		}
	}

	public static InbandDataUpdateController getInstance() {
		return instance;
	}
	
	/**
	 * PVR은 기본적으로 Tuner를 직접 이용하는데 24시간 동안 tuner가 여유가 없는 경우에는 ServiceContext를 이용해서 강제로 업데이트 하게 한다.
	 * 현재 업데이트가 ServiceContext를 활용해서 tune 했는지 여부를 확인 한다.  
	 * @return
	 */
	public boolean isUpdateUsingServiceContext() {
	    return forceLcwInPvr;
	}

	public void dispose() {
		Host.getInstance().removePowerModeChangeListener(this);
		hidePopup();
		alarm.dispose();
	}

	public boolean isUpdating() {
		if (currentState == STATE_UPDATING) {
			return true;
		}
		return false;
	}

	private InbandDataUpdateController() {

		MAX_RETRY_COUNT = DataManager.getInstance().getIbUpdateMaxDelayCount();
		RETRY_TIME = DataManager.getInstance().getIbUpdateDelayTime();
		POPUP_COUNT_DOWN_TIME = DataManager.getInstance().getInbandDataDialogPopupTime();
		IGNORE_TIME_NEXT_UPDATE = DataManager.getInstance().getIbUpdateIgnoreTimeNextUpdate();
		IB_UPDATE_TIME = DataManager.getInstance().getIbUpdateTime();

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "MAX_RETRY_COUNT=" + MAX_RETRY_COUNT);
			Log.printInfo(LOG_HEADER + "RETRY_TIME=" + RETRY_TIME);
			Log.printInfo(LOG_HEADER + "POPUP_COUNT_DOWN_TIME=" + POPUP_COUNT_DOWN_TIME);
			Log.printInfo(LOG_HEADER + "IGNORE_TIME_NEXT_UPDATE=" + IGNORE_TIME_NEXT_UPDATE);
			Log.printInfo(LOG_HEADER + "IB_UPDATE_TIME=" + IB_UPDATE_TIME);
		}
		Host.getInstance().addPowerModeChangeListener(this);

		StringTokenizer st = new StringTokenizer(IB_UPDATE_TIME, ":");
		String hh = st.nextToken().trim();
		String mm = st.nextToken().trim();
		
		int iHH = 3;
		int iMM = 0;
		try {
			iHH = Integer.parseInt(hh);
			iMM = Integer.parseInt(mm);
		} catch (NumberFormatException e) {
			Log.printError(e);
		}

		alarm = new SimpleAlarm(iHH, iMM, this);
		alarm.start();
	}

	/**
	 * SimpleAlarm callback listener.
	 */
	public void alarmUpdated() {
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "alarmUpdated()");
		}
		dataUpdated(true);
	}

	/**
	 * It is called by DataVersionManager.
	 * @param realUpdated If version.txt in OOB is updated true, otherwise false.
	 */
	public synchronized void dataUpdated(boolean realUpdated) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " dateUpdated() currentState:" + getStateString(currentState) + ", "
					+ Host.getInstance().getPowerMode() + ", updateRetryCount=" + updateRetryCount + ", realUpdated=" + realUpdated);
		}

		if (currentState == STATE_UPDATING) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + " dateUpdated() Update is processing now.");
			}
			return;
		}

		if(!Environment.SUPPORT_DVR) {
			// In the non-PVR STB only have to update in standby mode.
			if (Host.getInstance().getPowerMode() == Host.LOW_POWER) {
				// If standby mode, immediately do update.
				startUpdate();
				return;
			}
		}		

		if (currentState == STATE_POPUP_DIALOG || currentState == STATE_POPUP_CONFIRM) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Already update popup is appearing!");
			}
			return;
		}

		if(lastCanceledTime > 0 && realUpdated) {
			// 한번 이상 Cancel 한 상태이면 lastCanceledTime 가 -1 이 아니다. Timer 에 의한 update 때는 시간 체크 하지 않는다.
			long currentTime = System.currentTimeMillis();
			long tempTime = Long.parseLong(IGNORE_TIME_NEXT_UPDATE) * 1000L;
			long sumTime = tempTime + lastCanceledTime;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "lastCanceledTime=" + lastCanceledTime + ", sumTime=" + sumTime);
			}
			if(currentTime < sumTime) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Ignore this update because time is not enough from canceled time");
				}
				return;
			}
		}

		// Increase count.
		updateRetryCount++;

		int maxRetryCount = Integer.parseInt(MAX_RETRY_COUNT);
		if (updateRetryCount >= maxRetryCount) {
			// 24시간까지 Delay를 했기 때문에 강제로 업데이트가 되어야 한다.
			forceLcwInPvr = true;
			showUpdatePopup(STATE_POPUP_CONFIRM);
			startTimer();
		} else {
			forceLcwInPvr = false;
			if(Environment.SUPPORT_DVR) {
				updateInPvr();
			}
			else {
				// Show dialog.
				showUpdatePopup(STATE_POPUP_DIALOG);
				startTimer();
			}
		}
	}

	private synchronized void cancelUpdate() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " cancelUpdate() updateRetryCount=" + updateRetryCount);
		}
		currentState = STATE_UPDATE_DELAYED;

		lastCanceledTime = System.currentTimeMillis();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " cancel update time=" + lastCanceledTime);
		}

		hidePopup();
		stopTimer();
		startRetryTimer();
	}
	
	/**
	 * Starts update at non-PVR only.
	 */
	private synchronized void startUpdate() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " startUpdate() ");
		}
		// Reset count.
		updateRetryCount = 0;

		stopTimer();
		stopRetryTimer();
		currentState = STATE_UPDATING;
		hidePopup();

		// Go to TV_VIEWING_STATE without LCW
		if (Host.getInstance().getPowerMode() == Host.FULL_POWER) {
			// AV STTOP 및 TV_VIEWING_STATE에서 Graphics를 가진 어플을 죽이기 위해 ..
			AppLauncher.getInstance().goToFullScreenAppStateWithoutApplication();
			
			// Search 가 떠 있다면 종료~
			SearchService searchService = CommunicationManager.getInstance().getSearchService();
			if (searchService != null) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "stopSearchApplication() ");
				}
				try {
					searchService.stopSearchApplication();
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}

		showUpdatePopup(STATE_UPDATING);

		Thread ibUpdateThread = new Thread("IB Update") {
			public void run() {
				InbandDataManager.getInstance().startReceivingData(InbandDataUpdateController.this);
			}
		};
		ibUpdateThread.start();
	}
	
	private NetworkInterfaceTuner pvrTuner = null;
	/**
	 * Starts update in PVR.
	 */
	private synchronized void updateInPvr() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " updateInPvr() updateRetryCount=" + updateRetryCount);
		}
		
		currentState = STATE_UPDATING;
		
		// 실패 하면 타이머를 걸어서 4시간 후에 다시 하도록 해야 한다. startRetryTimer();
		// currentState = STATE_UPDATING 으로 했다가 실패 하면 다시 STATE_UPDATE_DELAYED 변경 해야 한다.
		pvrTuner = new NetworkInterfaceTuner(new NetworkInterfaceTuningFailListener() {
			public void tuningFailed() {
				// Tuning 성공해서 받고 있는 도중에 Tuner 를 뺏긴 경우이다. 이때는 Timer를 걸어 4시간 후를 기다린다.
				if(Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + " tuningFailed() event occurs");
				}
                // lock 때문에 이 updateFailPvr이 block이 되어 release를 하지 못하는 deadlock이 발생하여 수정.
                new Thread("NetworkInterfaceTuningFailListener") {
                    public void run() {
                        InbandDataManager.getInstance().finishJobPvr();
                        updateFailPvr();
                    }
                }.start();
			}
		});
		
		try {
			String sourceId = DataManager.getInstance().getSourceIdForInbandData();
			OcapLocator locator = new OcapLocator(Integer.parseInt(sourceId));
			boolean tuningResult = pvrTuner.tune(locator);
			if(!tuningResult) {
				// NetworkInterface 를 이용해서 Tuning 실패 했기 때문에 정리 하고 4시간 기다린다.
				updateFailPvr();
				pvrTuner.dispose();
				pvrTuner = null;
			}
			else {
				// NetworkInterface 를 이용해서 Tuning 성공했기 때문에 Notification 한다.
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + " updateInPvr() tuning success! ");
				}
				Thread ibUpdateThread = new Thread("IB Update") {
					public void run() {
						InbandDataManager.getInstance().startNotificationPVR(InbandDataUpdateController.this);
					}
				};
				ibUpdateThread.start();
			}
		} catch (Exception e) {
			Log.print(e);
		}
		
	}
	
	/**
	 * Only PVR.
	 */
	private synchronized void updateFailPvr() {
		// 업데이트 실패 했기 때문에 4시간 기다린다.
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " updateFailPvr() updateRetryCount=" + updateRetryCount);
		}
		currentState = STATE_UPDATE_DELAYED;
		lastCanceledTime = System.currentTimeMillis();
		startRetryTimer();
	}

	/**
	 * Implementation of InternalInbandDataListener. InbandDataManager 로 부터 데이터
	 * 수신이 끝났다는 Call back을 받은 것이다.
	 */
	public synchronized void finishReceivingData() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " finishReceivingData() " + Environment.SUPPORT_DVR + ", forceLcwInPvr=" + forceLcwInPvr);
		}
		
		currentState = STATE_WAITING;
		updateRetryCount = 0;
		lastCanceledTime = -1;

		if(Environment.SUPPORT_DVR) {
			// PVR 은 NetworkInterface로 Tuning 했기 때문에 아래와 같이 pvrTuner 를 정리 해야 한다.
			if(pvrTuner != null) {
				pvrTuner.dispose();
				pvrTuner = null;
			}
			if(forceLcwInPvr) {
				// This case is forced update in PVR with service selection so tune to LCW.
				remakeStbState();
			}
		}
		else {
			remakeStbState();
		}
		
		hidePopup();
	}
	
	private void remakeStbState() {
		if (Host.getInstance().getPowerMode() == Host.FULL_POWER) {
			if(!AuthorizationManager.getInstance().checkWizardNeeded())  {
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Wizard is not needed so process to normal running mode");
				}
				AppLauncher.getInstance().exitToChannelWithoutMenu(-1);
			}
			else {
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "Wizard is started");
				}
			}
		}
	}

	/**
	 *
	 * @param popupType
	 */
	private void showUpdatePopup(int popupType) {
		ScreenSaverManager.getInstance().resetTimer();
		synchronized (this) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + " showUpdateDialog() " + getStateString(popupType));
			}
			hidePopup();
			currentState = popupType;

			String msg;
			if (popupType == STATE_POPUP_DIALOG) {
				// 확인/취소 버튼 있는 팝업.
				msg = DataCenter.getInstance().getString(KeyNames.POPUP_IBUPDATE_DIALOG_MSG);
			} else if (popupType == STATE_UPDATING) {
				// 업데이트 중이라는 팝업.
				msg = DataCenter.getInstance().getString(KeyNames.POPUP_IBUPDATE_POPUP_MSG);
			} else {
				// 확인 버튼만 있는 팝업.
				msg = DataCenter.getInstance().getString(KeyNames.POPUP_IBUPDATE_DIALOG_MSG);
			}

			if(Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "Message=" + msg);
			}

			StringTokenizer stk = new StringTokenizer(msg, "|");
			Vector v = new Vector();
			while (stk.hasMoreElements()) {
				v.add(stk.nextToken());
			}
			String[] message = new String[v.size()];
			for (int i = 0; i < message.length; i++) {
				message[i] = (String) v.get(i);
			}

			String title = DataCenter.getInstance().getString(KeyNames.POPUP_IBUPDATE_TITLE);

			if (popupType == STATE_POPUP_DIALOG) {
				dialog = new BasicDialog(title, WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
						WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), message, this);
			} else if (popupType == STATE_UPDATING) {
				dialog = new BasicPopup(title, WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
						WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), message, this);
			} else {
				dialog = new BasicConfirm(title, WindowProperty.UNBOUND_APP_UPDATE_POPUP.getName(),
						WindowProperty.UNBOUND_APP_UPDATE_POPUP.getPriority(), message, this);
			}
			dialog.showPopup();
		}
	}

	/**
	 * Hides the popup.
	 */
	public void hidePopup() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " hidePopup() " + getStateString(currentState));
		}
		synchronized (this) {
			if (dialog != null) {
				dialog.hidePopup();
				dialog.dispose();
				dialog = null;
			}
		}
	}

	private void startTimer() {
		stopTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " startTimer() " + getStateString(currentState));
		}
		try {
			String waitTimeConfig = DataManager.getInstance().getInbandDataDialogPopupTime();
			long waitTime = Integer.parseInt(waitTimeConfig) * 1000L;
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + " waitTime=" + waitTime);
			}
			timerSpec.setDelayTime(waitTime);
			timerSpec.setRepeat(false);
			timerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(timerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	private void stopTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " stopTimer() " + getStateString(currentState));
		}
		timerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(timerSpec);
	}

	private void startRetryTimer() {
		stopRetryTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "startRetryTimer() " + RETRY_TIME);
		}
		try {
			long delayTime = Integer.parseInt(RETRY_TIME) * 1000L;
			retryTimerSpec.setDelayTime(delayTime);
			retryTimerSpec.setRepeat(false);
			retryTimerSpec.addTVTimerWentOffListener(this);
			timer.scheduleTimerSpec(retryTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	private void stopRetryTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "stopRetryTimer()");
		}
		retryTimerSpec.removeTVTimerWentOffListener(this);
		timer.deschedule(retryTimerSpec);
	}

	/**
	 * This is timer call back.
	 *
	 * @param event
	 *            TVTimer event.
	 */
	public void timerWentOff(TVTimerWentOffEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + ": timerWentOff() " + event.getTimerSpec() + "currentState=" + getStateString(currentState));
		}

		synchronized (this) {
			if (event.getTimerSpec().equals(timerSpec)) {
				startUpdate();
			} else {
				// Rety timer.
				dataUpdated(false);
			}
		}
	}

	/**
	 * Callback function of PowerModeChangeListenr.
	 */
	public synchronized void powerModeChanged(int newPowerMode) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + ": powerModeChanged() " + newPowerMode + ", currentState" + getStateString(currentState)
					+ ",updateRetryCount=" + updateRetryCount);
		}
		if (newPowerMode == Host.LOW_POWER) {
			if (currentState == STATE_WAITING || currentState == STATE_UPDATING) {

			} else {
				startUpdate();
			}

		} else {

		}
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + " handleKeyEvent() event=" + event);
		}

		int keyCode = event.getCode();
		if(keyCode == HRcEvent.VK_POWER) {
			return false;
		}

		if (currentState == STATE_UPDATING) {
			// Inband data 수신 중이기 때문에 Power Key를 제외하고는 모두 막는다.
			if (keyCode == OCRcEvent.VK_POWER) {
				return false;
			}
			return true;
		} else {

			if (currentState == STATE_POPUP_DIALOG) {
				// Dialog popup
				switch (keyCode) {
				case OCRcEvent.VK_LEFT:
					dialog.setFocusBtn(true);
					break;
				case OCRcEvent.VK_RIGHT:
					dialog.setFocusBtn(false);
					break;
				case OCRcEvent.VK_ENTER:
					if (dialog.getFocusBtn()) {
						// Update 시작.
						startUpdate();
					} else {
						cancelUpdate();
					}
					break;
				}
			} else {
				// OK 버튼만 있는 팝업.
				if (keyCode == OCRcEvent.VK_ENTER) {
					startUpdate();
				}
				return true;
			}

			return true;
		}
	}

	private static final String getStateString(int state) {
		switch (state) {
		case STATE_WAITING:
			return "[STATE_WAITING]";
		case STATE_UPDATE_DELAYED:
			return "[STATE_UPDATE_DELAYED]";
		case STATE_POPUP_DIALOG:
			return "[STATE_POPUP_DIALOG]";
		case STATE_POPUP_CONFIRM:
			return "[STATE_POPUP_CONFIRM]";
		case STATE_UPDATING:
			return "[STATE_UPDATING]";
		}
		return "UNKNOWN";
	}

}

package com.videotron.tvi.illico.monitor.core;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;
import org.ocap.ui.event.OCRcEvent;

import com.alticast.ui.LayeredKeyHandler;
import com.videotron.tvi.illico.ixc.monitor.BlockedTimeListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.ixc.monitor.STBModeChangeListener;
import com.videotron.tvi.illico.ixc.upp.BlockTime;
import com.videotron.tvi.illico.ixc.upp.BlockTimeUpdateListener;
import com.videotron.tvi.illico.ixc.upp.Definitions;
import com.videotron.tvi.illico.ixc.upp.PreferenceService;
import com.videotron.tvi.illico.ixc.upp.RightFilter;
import com.videotron.tvi.illico.ixc.upp.RightFilterListener;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.communication.CommunicationManager;
import com.videotron.tvi.illico.monitor.communication.NotificationCenterProxy;
import com.videotron.tvi.illico.monitor.communication.NotificationCenterProxyListener;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxy;
import com.videotron.tvi.illico.monitor.communication.PreferenceServiceProxyListener;
import com.videotron.tvi.illico.monitor.ui.TerminalBlockedScene;
import com.videotron.tvi.illico.util.KeyCodes;
import com.videotron.tvi.illico.framework.DataCenter;

/**
 * Manages the watching time restriction.
 *
 * @author Woosik Lee
 */
public class WatchingTimeRestrictionManager implements LayeredKeyHandler,
		STBModeChangeListener {

	/** Indicates the stop mode. */
	public static final int MODE_STOP = 0;
	/** Indicates the mode wating restriction time. */
	public static final int MODE_WAITING_RESTRICTION_TIME = 1;
	/** Indicates the mode restrication now. */
	public static final int MODE_RESTRICTION_TIME = 2;
	/** The current mode. */
	private int currentMode = MODE_STOP;
	/** Before enter restriction time, should show notification popup. */
	private static long PREPARE_POPUP_TIME = 1000L * 60L;
	/** The message id from the notification center. */
	private String notificationMessageId;

	/** The IXC service of Preference. */
	private PreferenceService preferenceService;
	/** The schedules. */
	private BlockTimeSchedule[] schedule = null;
	/** Current selected schedule's index. */
	private int currentBlockTimeScheduleIndex = -1;
	/** Ready Notification application or not. */
	private boolean readyNotification;
	private boolean readyPreference;
	/** Is boot up is complete?. */
	private boolean completeBooting;

	/** TimerSpec for entering restriction. */
	private TVTimerSpec startTimerSpec = new TVTimerSpec();
	/** TimerSpec for exiting restriction. */
	private TVTimerSpec endTimerSpec = new TVTimerSpec();
	/** TimerSpec for prepare popup. */
	private TVTimerSpec preparePopupTimerSpec = new TVTimerSpec();

	/** The preference callback listener. */
	private BlockTimeUpdateListenerImpl updateListener = new BlockTimeUpdateListenerImpl();

	private NotificationCenterListenerImpl notificationListener = new NotificationCenterListenerImpl();

	private RightFilterListenerImpl rightFilterListenerImpl = new RightFilterListenerImpl();

	/** The singleton instance of WatchingTimeRestrictionManager. */
	private static WatchingTimeRestrictionManager instance;

	/** The list of BlockedTimeListener. */
	private ArrayList stateListeners = new ArrayList();

	private TerminalBlockedScene terminalBlockedScene;

	/** The log header message. */
	private static final String LOG_HEADER = "[WTRM] ";

	/**
	 * Returns the singleton instance.
	 *
	 * @return instance of WatchingTimeRestrictionManager.
	 */
	public static synchronized WatchingTimeRestrictionManager getInstance() {
		if (instance == null) {
			instance = new WatchingTimeRestrictionManager();
		}
		return instance;
	}

	/**
	 * Returns current WTRM mode.
	 *
	 * @return current WTRM mode.
	 */
	public int getCurrentMode() {
		return currentMode;
	}

	public boolean isBlockedTime() {
		if(currentMode == MODE_RESTRICTION_TIME) {
			return true;
		}
		return false;
	}

	/**
	 * Constructor.
	 */
	private WatchingTimeRestrictionManager() {
		readyNotification = false;
		readyPreference = false;
		//Host.getInstance().addPowerModeChangeListener(this);
	}

	public synchronized void bootComplete() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "bootComplete() readyPreference=" + readyPreference
					+ ", readyNotification=" + readyNotification);
		}
		completeBooting = true;
		start();
		STBModeManager.getInstance().addModeChangeListener(this, "WTRM");
	}

	private synchronized void start() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "modeChanged() completeBooting=" + completeBooting + ", readyPreference=" + readyPreference
					+ ", readyNotification=" + readyNotification);
		}
		if (readyPreference && readyNotification) {
			refreshRestriction(null);
		}
	}

	public synchronized void setNotificationPopupTime(long displayTime) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "setNotificationPopupTime()" + displayTime + ", readyPreference="
					+ readyPreference + ", completeBooting=" + completeBooting);
		}
		PREPARE_POPUP_TIME = displayTime;
		readyNotification = true;
		if (readyPreference && completeBooting) {
			refreshRestriction(null);
		}
	}

	public synchronized void readyPreferenceService() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "readyPreferenceService() readyNotification=" + readyNotification
					+ ",completeBooting=" + completeBooting);
		}
		readyPreference = true;
		if (readyNotification && completeBooting) {
			refreshRestriction(null);
		}

		PreferenceServiceProxy.getInstance().addPreferenceServiceListener(preserviceListener);
	}

	private PreferenceServiceProxyListener preserviceListener = new PreferenceServiceProxyListener() {
		public void notifyUpdatePreferenceValue(String name, String value) {
			if (PreferenceServiceProxy.PNAME_PARENTAL_CONTROL.equals(name)) {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "refreshRestrication() notifyUpdatePreferenceValue()" + value
							+ ", currentMode=" + currentMode);
				}
				synchronized (WatchingTimeRestrictionManager.this) {
					if (currentMode == MODE_STOP) {
						// WTRM 이 STOP 상태이다. 여기서 PARENTAL_CONTROL 이 ON 되면 다시 실행
						// 해야 하는지 여부를
						// 판단 해야 한다.
						if (Definitions.OPTION_VALUE_ON.equals(value)) {
							refreshRestriction(null);
						}
					} else {
						// WTRM이 실행(혹은대기)상태에서 PARENTAL_CONTROL이 변경 되었다.
						// 이때 PARENTAL_CONTROL 이 OFF 가 된다면 WTRM을 중단 해야 한다.
						// Following is the PARENTAL_CONTROL value.
						// ON = Definitions.OPTION_VALUE_ON
						// OFF = Definitions.OPTION_VALUE_OFF
						// OFF = OPTION_VALUE_SUSPENDED_FOR_4_HOURS
						// OFF = OPTION_VALUE_SUSPENDED_FOR_8_HOURS
						if (!Definitions.OPTION_VALUE_ON.equals(value)) {
							refreshRestriction(null);
						}
					}
				}
			}
		}
	};

	/**
	 * Compute restriction time schedule.
	 *
	 * @param times
	 *            {@link com.videotron.tvi.illico.ixc.upp.BlockTime}.
	 */
	private synchronized void refreshRestriction(BlockTime[] times) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "refreshRestrication() currentMode=" + currentMode);
		}
		resetMode();

		BlockTime[] blockTimes = times;
		try {
			if (blockTimes == null && preferenceService == null) {
				preferenceService = CommunicationManager.getInstance().getPreferenceService();
				if (preferenceService == null) {
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "refreshRestrication() preferenceService is null");
					}
					// resetMode();
					return;
				}
			}
			blockTimes = preferenceService.addBlockTimeUpdateListener(updateListener);

			// //////////////// // TEST Start ////////////////
			/*
			 * Log.printInfo("WTRM:TEST " +
			 * makeYYYYMMDDhhmmss(System.currentTimeMillis())); // 에뮬레이터에서는 아래
			 * //[0],[1] 으로 테스트 하면 됨. 에뮬 시작 시간이 11:18분경임. blockTimes = new
			 * BlockTime[2]; blockTimes[0] = new BlockTime();
			 * blockTimes[0].setStartTime("1119");
			 * blockTimes[0].setEndTime("1121");
			 * blockTimes[0].setRepeat("Yes|Yes|Yes|Yes|Yes|Yes|Yes");
			 *
			 *
			 * blockTimes[1] = new BlockTime();
			 * blockTimes[1].setStartTime("0155");
			 * blockTimes[1].setEndTime("0157");
			 * blockTimes[1].setRepeat("Yes|Yes|Yes|Yes|Yes|Yes|Yes");
			 */
			/*
			 * blockTimes[2] = new BlockTime();
			 * blockTimes[2].setStartTime("0155");
			 * blockTimes[2].setEndTime("0158");
			 * blockTimes[2].setRepeat("Yes|Yes|Yes|Yes|Yes|Yes|Yes");
			 *
			 * blockTimes[3] = new BlockTime();
			 * blockTimes[3].setStartTime("0200");
			 * blockTimes[3].setEndTime("0202");
			 * blockTimes[3].setRepeat("Yes|Yes|Yes|Yes|Yes|Yes|Yes");
			 *
			 * blockTimes[4] = new BlockTime();
			 * blockTimes[4].setStartTime("0205");
			 * blockTimes[4].setEndTime("0210");
			 * blockTimes[4].setRepeat("Yes|Yes|Yes|Yes|Yes|Yes|Yes");
			 */
			// ////////////////

			if (blockTimes == null || blockTimes.length < 1) {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "refreshRestrication() blockTimes is null so clear schedule");
				}
				resetMode();
				schedule = null;
				return;
			}

			String parentalControlValue = PreferenceServiceProxy.getInstance().getPreferenceValue(
					PreferenceServiceProxy.PNAME_PARENTAL_CONTROL);
			if (!Definitions.OPTION_VALUE_ON.equals(parentalControlValue)) {
				// PARENTAL_CONTROL 이 ON 이 아니면 실행 하면 안 됨.
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "refreshRestrication() Parental control off !");
				}
				resetMode();
				return;
			}

			int length = blockTimes.length;
			schedule = new BlockTimeSchedule[length];
			for (int i = 0; i < length; i++) {
				String startTime = blockTimes[i].getStartTime();
				String endTime = blockTimes[i].getEndTime();
				String repeat = blockTimes[i].getRepeat();

				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "refreshRestrication() [" + i + "] startTime=" + startTime
							+ ",endTime=" + endTime + ",repeat=" + repeat);
				}

				schedule[i] = new BlockTimeSchedule(i, startTime, endTime, repeat);
			}

			computeSchedule();

		} catch (Exception e) {
			Log.print(e);
			resetMode();
		} catch (Throwable t) {
			Log.print(t);
			resetMode();
		}
	}

	/**
     *
     */
	private void computeSchedule() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "computeSchedule()");
		}

		long currentTime = System.currentTimeMillis();
		if (Log.DEBUG_ON) {
			printTime(currentTime, "Current time is : ");
		}

		if (schedule == null || schedule.length < 1) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "computeSchedule() schedule is null");
			}
			return;
		}

		int nearestScheduleIndex = 0;
		long tempStartTime = Long.MAX_VALUE;

		int scheduleLength = schedule.length;

		long weekStartTime = 0;
		for (int i = 0; i < scheduleLength; i++) {
			weekStartTime = schedule[i].findNextSchedule(currentTime);

			if (schedule[i].getCurrentScheduleStartTime() < tempStartTime) {
				tempStartTime = schedule[i].getCurrentScheduleStartTime();
				nearestScheduleIndex = i;
			}
		}

		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "Following is the found Schedules ====================");
			Log.printInfo(LOG_HEADER + "nearestScheduleIndex : " + nearestScheduleIndex + ", weekStartTime=" + weekStartTime);
			for (int i = 0; i < scheduleLength; i++) {
				Log.printInfo(LOG_HEADER + "SCHEDULE[" + i + "] StartTime="
						+ schedule[i].getCurrentScheduleStartTime() + ", "
						+ new Date(schedule[i].getCurrentScheduleStartTime()));
				Log.printInfo(LOG_HEADER + "SCHEDULE[" + i + "] ENDTime=" + schedule[i].getCurrentScheduleEndTime()
						+ ", " + new Date(schedule[i].getCurrentScheduleEndTime()));
			}
			Log.printInfo(LOG_HEADER + "End found Schedules ====================");
		}

		currentBlockTimeScheduleIndex = nearestScheduleIndex;
		long startTime = schedule[currentBlockTimeScheduleIndex].getCurrentScheduleStartTime();

/*
		// compute daylight saving.
		Date weakStartTimeDate = new Date(weekStartTime);

		TimeZone timezone = TimeZone.getDefault();
		boolean weekTimeDaylight = timezone.inDaylightTime(weakStartTimeDate);

		Date startTimeDate = new Date(startTime);
		boolean startTimeDaylight = timezone.inDaylightTime(startTimeDate);

		if(weekTimeDaylight == false && startTimeDaylight == true) {
			// weekTime 은 daylight saving 아니고, startTime 은 daylight saving 이다. 1시간을 뺀다.
			int hour1 = 1000 * 60 * 60;
			startTime = (startTime - hour1);
		}
		else if(weekTimeDaylight == true && startTimeDaylight == false){
			// weekTime 은 daylight saving 이고, startTime 은 아니다. 1시간을 더한다.
			int hour1 = 1000 * 60 * 60;
			startTime = (startTime + hour1);
		}
		if(Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "Day light saving : weekTimeDaylight=" + weekTimeDaylight + ", startTimeDaylight=" + startTimeDaylight);
			Log.printInfo(LOG_HEADER + "startTime=" + startTime + ", " + new Date(startTime).toString());
		}
		// end of compute day light saving.
*/

		if (currentTime > startTime) {
			// 이미 시정 제한이 시작 되었다.
			// Already Restriction time has been started.
			enterRestrictionMode();
		} else {
			currentMode = MODE_WAITING_RESTRICTION_TIME;

			long preparePopupTime = startTime - PREPARE_POPUP_TIME;
			if (preparePopupTime < currentTime) {
				showPreparePopup(startTime - currentTime);
			} else {
				schedulePreparePopupTimer(preparePopupTime);
			}
			scheduleStartTimer(startTime);
		}
	}

	/**
	 * @param currentTime
	 *            the current time.
	 * @param endTime
	 *            the end time.
	 */
	private synchronized void enterRestrictionMode() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "enterRestricationMode() ");
		}

		// LastShownScheduleTime 을 세팅 한다. BlockTimeSchedule 에서 같은 시간대를 중복 선택 하지
		// 않게 하기 위해서.
		long startTime = schedule[currentBlockTimeScheduleIndex].getCurrentScheduleStartTime();
		schedule[currentBlockTimeScheduleIndex].setLastShownScheduleTime(startTime);

		// ScreenSaverManager.getInstance().resetTimer();
		currentMode = MODE_RESTRICTION_TIME;
		long endTime = schedule[currentBlockTimeScheduleIndex].getCurrentScheduleEndTime();
		scheduleEndTimer(endTime);

		String startTimeString = makeHHMM12Hour(schedule[currentBlockTimeScheduleIndex].getStartHH(),
				schedule[currentBlockTimeScheduleIndex].getStartMM());
		String endTimeString = makeHHMM12Hour(schedule[currentBlockTimeScheduleIndex].getEndHH(),
				schedule[currentBlockTimeScheduleIndex].getEndMM());

		hidePreparePopup();

		if (Host.getInstance().getPowerMode() == Host.FULL_POWER) {
			AppLauncher.getInstance().exitToChannelWithoutMenuAndAV();
		}

		showTerminalBlockedScene(startTimeString, endTimeString);

		notifyBlockedTimeStarted();
	}

	public static String makeHHMM12Hour(int hh, int mm) {
		StringBuffer result = new StringBuffer();
		if (hh > 12) {
			result.append(hh - 12);
		} else {
			result.append(hh);
		}
		result.append(":");
		if (mm < 10) {
			result.append("0").append(mm);
		} else {
			result.append(mm);
		}
		result.append(" ");
		if (hh > 11) {
			result.append("pm");
		} else {
			result.append("am");
		}
		return result.toString();
	}

	/**
	 * Exits the restriction.
	 *
	 * @param endByTimer
	 */
	private synchronized void exitRestrictionMode(boolean endByTimer) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "exitRestrictionMode() endByTimer=" + endByTimer);
		}

		currentMode = MODE_STOP;

		// ScreenSaverManager.getInstance().resetTimer();
		hideTerminalBlockedScene();
		descheduleEndTimer();

		// 1. Hide Popup
		PreferenceService preService = CommunicationManager.getInstance().getPreferenceService();
		try {
			preService.hidePinEnabler();
		} catch (Exception e) {
			Log.print(e);
		}

		// 2. If screen-saver is activated, stop screen-saver.
		ScreenSaverManager.getInstance().resetTimer();

		computeSchedule();

		// 3. Select AV
		if (Host.getInstance().getPowerMode() == Host.FULL_POWER) {
			AppLauncher.getInstance().exitToChannelWithoutMenu(-1);
		}

		notifyBlockedTimeStoped();
	}

	/**
	 * Requests popup to Notification center.
	 */
	private void showPreparePopup(long time) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "showPreparePopup() ");
		}
		NotificationCenterProxy.getInstance().addNotificationListener(notificationListener);
		notificationMessageId = NotificationCenterProxy.getInstance().setNotifyAndTime(
				NotificationCenterProxy.getInstance().getWTRMPrepareOption(), time);
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "notificationMessageId=" + notificationMessageId);
		}
	}

	/**
     *
     */
	private void hidePreparePopup() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "hidePreparePopup() notificationMessageId=" + notificationMessageId);
		}
		NotificationCenterProxy.getInstance().removeNotificationListener(notificationListener);
		NotificationCenterProxy.getInstance().removeNotify(notificationMessageId);
	}

	/**
	 * The TimerListener for entering restriction.
	 */
	private TVTimerWentOffListener startTimerListener = new TVTimerWentOffListener() {
		public void timerWentOff(TVTimerWentOffEvent e) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Start Timer timerWentOff()");
			}

			enterRestrictionMode();
		}
	};

	private TVTimerWentOffListener preparePopupTimerListener = new TVTimerWentOffListener() {
		public void timerWentOff(TVTimerWentOffEvent e) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "Prepare Popup Timer timerWentOff()");
			}
			showPreparePopup(PREPARE_POPUP_TIME);
		}
	};

	/**
	 * The TimerListener for exiting restriction.
	 */
	private TVTimerWentOffListener endTimerListener = new TVTimerWentOffListener() {
		public void timerWentOff(TVTimerWentOffEvent e) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "End Timer timerWentOff()");
			}

			exitRestrictionMode(true);
		}
	};

	/**
	 * Starts timer for next restriction time.
	 *
	 * @param absTime
	 *            next restriction time.
	 */
	private synchronized void scheduleStartTimer(long absTime) {
		descheduleStartTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "scheduleStartTimer() absTime=" + absTime);
		}
		try {
			startTimerSpec.addTVTimerWentOffListener(startTimerListener);
			startTimerSpec.setAbsoluteTime(absTime);
			startTimerSpec.setRegular(true);
			TVTimer.getTimer().scheduleTimerSpec(startTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	/**
	 * Stops next restriction timer.
	 */
	private synchronized void descheduleStartTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "descheduleStartTimer()");
		}
		startTimerSpec.removeTVTimerWentOffListener(startTimerListener);
		TVTimer.getTimer().deschedule(startTimerSpec);
	}

	/**
	 * Starts timer for next restriction time's popup.
	 *
	 * @param absTime
	 *            next restriction popup time.
	 */
	private synchronized void schedulePreparePopupTimer(long absTime) {
		deschedulePreparePopupTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "schedulePreparePopupTimer() absTime=" + absTime);
		}
		try {
			preparePopupTimerSpec.addTVTimerWentOffListener(preparePopupTimerListener);
			preparePopupTimerSpec.setAbsoluteTime(absTime);
			preparePopupTimerSpec.setRegular(true);
			TVTimer.getTimer().scheduleTimerSpec(preparePopupTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	/**
	 * Stops next restriction popup timer.
	 */
	private synchronized void deschedulePreparePopupTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "deschedulePreparePopupTimer()");
		}
		preparePopupTimerSpec.removeTVTimerWentOffListener(preparePopupTimerListener);
		TVTimer.getTimer().deschedule(preparePopupTimerSpec);
	}

	/**
	 * Starts timer for end of current restriction time.
	 *
	 * @param absTime
	 *            end time.
	 */
	private synchronized void scheduleEndTimer(long absTime) {
		descheduleEndTimer();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "scheduleEndTimer() absTime=" + absTime);
		}
		try {
			endTimerSpec.addTVTimerWentOffListener(endTimerListener);
			endTimerSpec.setAbsoluteTime(absTime);
			endTimerSpec.setRegular(true);
			TVTimer.getTimer().scheduleTimerSpec(endTimerSpec);
		} catch (TVTimerScheduleFailedException e) {
			Log.print(e);
		}
	}

	/**
	 * Stops timer for end of current restriction time.
	 */
	private synchronized void descheduleEndTimer() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "descheduleEndTimer()");
		}
		endTimerSpec.removeTVTimerWentOffListener(endTimerListener);
		TVTimer.getTimer().deschedule(endTimerSpec);
	}

	/**
	 * Resets the mode.
	 */
	private synchronized void resetMode() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "resetMode() currentMode=" + currentMode);
		}
		currentMode = MODE_STOP;
		// 1. reset timer. (TIMER)
		descheduleStartTimer();
		descheduleEndTimer();
		hidePreparePopup();
	}

	/**
	 * Implementation of Notification center listener.
	 *
	 * @author Woosik Lee
	 */
	private class NotificationCenterListenerImpl implements NotificationCenterProxyListener {
		public void action(String id) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "NotificationCenterListenerImpl action() id=" + id);
			}
			synchronized (WatchingTimeRestrictionManager.this) {
				descheduleStartTimer();
				// Notification 에서 곧 실행 될 제한 시간을 취소 했기 때문에
				// currentBlockTimeScheduleIndex 를 세팅 한다.
				// BlockTimeSchedule 에서 취소된 시간이 다시 선택 되지 않게 하기 위해서
				// LastShownScheduleTime 을 세팅 한다. BlockTimeSchedule 에서 같은 시간대를
				// 중복 선택 하지 않게 하기 위해서.
				long startTime = schedule[currentBlockTimeScheduleIndex].getCurrentScheduleStartTime();
				schedule[currentBlockTimeScheduleIndex].setLastShownScheduleTime(startTime);
				computeSchedule();
			}
		}

		public void cancel(String id) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "NotificationCenterListenerImpl cancel() id=" + id);
			}

			hidePreparePopup();
		}
	}

	private class BlockTimeUpdateListenerImpl implements BlockTimeUpdateListener {
		public void updateBlockTimeData(BlockTime[] times) {
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "updateBlockTimeData()");
			}
			refreshRestriction(times);
		}
	}

	private class RightFilterListenerImpl implements RightFilterListener {
		public void receiveCheckRightFilter(int response) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "receiveCheckRightFilter() response = " + response);
			}
			if (response == PreferenceService.RESPONSE_SUCCESS) {
				Thread newThread = new Thread() {
					public void run() {
						exitRestrictionMode(false);
					}
				};
				newThread.start();
			}
		}

		public String[] getPinEnablerExplain() {
            //->Kenneth[2017.3.27] R7.3 VDTRMASTER-6030
            return new String[]{DataCenter.getInstance().getString("popup.unblock_msg")};
			//return null;
            //<-
		}
	}

	// ///////////////////////////////////
	// BlockedTimeListener
	// ///////////////////////////////////
	/**
	 * Adds the BlockedTimeListener.
	 *
	 * @param l
	 *            instance of BlockedTimeListener.
	 * @param appName
	 *            requested application name.
	 */
	public void addBlockedListener(BlockedTimeListener l, String applicationName) {
		synchronized (stateListeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "addBlockedListener():" + item + ", appName=" + applicationName);
			}
			if (!stateListeners.contains(item)) {
				stateListeners.add(item);
			} else {
				// Removes a previous listener.
				stateListeners.remove(item);
				// Adds a new listener.
				stateListeners.add(item);
			}
		}
	}

	/**
	 * Removes a BlockedTimeListener.
	 *
	 * @param l
	 *            instance of BlockedTimeListener.
	 * @param applicationName
	 *            requested application name.
	 */
	public void removeBlockedTimeListener(BlockedTimeListener l, String applicationName) {
		synchronized (stateListeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "removeBlockedTimeListener():" + item + ", appName=" + applicationName);
			}
			stateListeners.remove(item);
		}
	}

	/**
	 * Notify that the bloced-time is started.
	 */
	private void notifyBlockedTimeStarted() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "notifyBlockedTimeStarted()");
		}
		if (stateListeners.size() < 1) {
			return;
		}
		Object[] lsnrs;
		synchronized (stateListeners) {
			lsnrs = stateListeners.toArray();
		}
		for (int i = 0; i < lsnrs.length; i++) {
			try {
				ListenerItem lifs = (ListenerItem) lsnrs[i];
				lifs.listener.startBlockedTime();
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "notifyBlockedTimeStarted() to " + lifs.listener);
				}
			} catch (Throwable t) {
				Log.print(t);
			}
		}
	}

	/**
	 * Notify that the bloced-time is stoped.
	 */
	private void notifyBlockedTimeStoped() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "notifyBlockedTimeStoped()");
		}
		if (stateListeners.size() < 1) {
			return;
		}
		Object[] lsnrs;
		synchronized (stateListeners) {
			lsnrs = stateListeners.toArray();
		}
		for (int i = 0; i < lsnrs.length; i++) {
			try {
				ListenerItem lifs = (ListenerItem) lsnrs[i];
				lifs.listener.stopBlockedTime();
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "notifyBlockedTimeStoped() to " + lifs.listener);
				}
			} catch (Throwable t) {
				Log.print(t);
			}
		}
	}

	/**
	 * This class has the unique application id and the instance of
	 * BlockedTimeListener.
	 *
	 * @author Woosik Lee
	 */
	private class ListenerItem {
		/** Instance of BlockedTimeListener. */
		private BlockedTimeListener listener;
		/** The requested application name. */
		private String applicationName;

		/**
		 * Constructor.
		 *
		 * @param l
		 *            BlockedTimeListener.
		 * @param appName
		 *            requested application name.
		 */
		public ListenerItem(final BlockedTimeListener l, final String appName) {
			this.listener = l;
			this.applicationName = appName;
		}

		/**
		 * Overrides the equals() by Object class.
		 *
		 * @param o
		 *            the object to compare with this.
		 * @return if equals instance return true, otherwise return false.
		 */
		public boolean equals(Object o) {
			if (o instanceof ListenerItem) {
				String appName = ((ListenerItem) o).applicationName;
				if (appName.equals(applicationName)) {
					return true;
				}
				return false;
			} else {
				return false;
			}
		}
	}

	/**
	 * Just for debuging. It print schedule.
	 *
	 * @param time
	 *            current time.
	 * @param title
	 *            title.
	 */
	private void printTime(long time, String title) {

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		if (Log.DEBUG_ON) {
			StringBuffer buf = new StringBuffer();
			buf.append("WTRM[Schedule]: title=" + title + " " + cal.getTimeInMillis() + " ==============\n");
			buf.append("YEAR=" + cal.get(Calendar.YEAR));
			buf.append(",MONTH=" + (cal.get(Calendar.MONTH) + 1));
			buf.append(",DATE=" + cal.get(Calendar.DATE));
			int week = cal.get(Calendar.DAY_OF_WEEK);
			switch (week) {
			case 1:
				buf.append(",WEEK=Sun");
				break;
			case 2:
				buf.append(",WEEK=Mon");
				break;
			case 3:
				buf.append(",WEEK=Tue");
				break;
			case 4:
				buf.append(",WEEK=Wed");
				break;
			case 5:
				buf.append(",WEEK=Thu");
				break;
			case 6:
				buf.append(",WEEK=Fri");
				break;
			case 7:
				buf.append(",WEEK=Sat");
				break;
			}

			buf.append(",HOUR=" + cal.get(Calendar.HOUR_OF_DAY));
			buf.append(",MIN=" + cal.get(Calendar.MINUTE));
			buf.append(",SEC=" + cal.get(Calendar.SECOND));

			buf.append("\n===========================================");

			if (Log.DEBUG_ON) {
				Log.printDebug(buf.toString());
			}
		}
	}

	// //////////////////////////////////
	// Terminal Blocked Scene
	// //////////////////////////////////

	/**
	 * Shows the blocked scene.
	 */
	public synchronized void showTerminalBlockedScene(String startTime, String endTime) {
		hideTerminalBlockedScene();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "showTerminalBlockedScene()");
		}

		terminalBlockedScene = new TerminalBlockedScene(startTime, endTime, this);
		terminalBlockedScene.showScene();
	}

	/**
	 * Hides the blocked scene.
	 */
	private synchronized void hideTerminalBlockedScene() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "hideTerminalBlockedScene()");
		}

		if (terminalBlockedScene != null) {
			terminalBlockedScene.hideScene();
			terminalBlockedScene.dispose();
			terminalBlockedScene = null;
		}
	}

	/**
	 * Implements the Layered UI's key event handler.
	 *
	 * @param event
	 *            the key event.
	 * @return Always return true because this is system popup.
	 */
	public boolean handleKeyEvent(UserEvent event) {
		if (Log.DEBUG_ON) {
			Log.printDebug("WTRM handleKeyEvent() event=" + event);
		}

		int keyCode = event.getCode();

		if (keyCode == OCRcEvent.VK_POWER) {
			return false;
		}

		if (keyCode == KeyCodes.COLOR_B && event.getType() == KeyEvent.KEY_PRESSED) {
			PreferenceService preService = CommunicationManager.getInstance().getPreferenceService();
			try {
				preService.checkRightFilter(rightFilterListenerImpl, "Monitor",
						new String[] { RightFilter.ALLOW_BYPASS_BLOCKED_TIME_PERIOD }, null, null);
			} catch (Exception e) {
				Log.print(e);
			}
		}

		return true;
	}

	private static String makeYYYYMMDDhhmmss(long time) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(time);
		StringBuffer buf = new StringBuffer();
		buf.append(cal.get(Calendar.YEAR)).append(":");
		buf.append(cal.get(Calendar.MONTH) + 1).append(":");
		buf.append(cal.get(Calendar.DATE)).append("__");

		buf.append(cal.get(Calendar.HOUR_OF_DAY)).append(":");
		buf.append(cal.get(Calendar.MINUTE)).append(":");
		buf.append(cal.get(Calendar.SECOND));

		int ampm = cal.get(Calendar.AM_PM);
		if (ampm == Calendar.AM) {
			buf.append("(AM)");
		} else {
			buf.append("(PM)");
		}

		return buf.toString();
	}

	/**
	 * This method is called by StandbyAndRunningModeManager.
	 * @param newPowerMode
	 *            the new power mode.
	 */
	public synchronized void powerModeChanged(int newPowerMode) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "newPowerMode=" + newPowerMode);
		}

		if (newPowerMode == Host.FULL_POWER) {

			String parentalControlValue = PreferenceServiceProxy.getInstance().getPreferenceValue(
					PreferenceServiceProxy.PNAME_PARENTAL_CONTROL);
			if (!Definitions.OPTION_VALUE_ON.equals(parentalControlValue)) {
				// PARENTAL_CONTROL 이 ON 이 아니면 실행 하면 안 됨.
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "refreshRestrication() Parental control off !");
				}
				resetMode();
				return;
			}

			/**
			 * 대기모드를 다녀왔기 때문에 PIN을 입력 했는지 여부와 관계 없이 LastShownScheduleTime 을
			 * Reset 한다.
			 */
			if (schedule != null && schedule.length > 0) {
				for (int i = 0; i < schedule.length; i++) {
					schedule[i].setLastShownScheduleTime(-1);
				}
				computeSchedule();
			} else {
				if (Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "schedule is null so do not anything. ");
				}
			}

		} else {

		}
	}

	public void modeChanged(int newMode) {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "modeChanged=" + newMode + ", completeBooting=" + completeBooting);
		}
		switch(newMode) {
		case MonitorService.STB_MODE_MANDATORY_BLOCKED :
		case MonitorService.STB_MODE_NO_SIGNAL :
		case MonitorService.STB_MODE_STANDALONE :
		case MonitorService.STB_MODE_BOOTING :
			this.resetMode();
			break;
		case MonitorService.STB_MODE_NORMAL :
			// 이 경우는 STB_MODE_MANDATORY_BLOCKED -->  STB_MODE_NORMAL 인 경우이다.
			start();
			break;
		}

	}
}

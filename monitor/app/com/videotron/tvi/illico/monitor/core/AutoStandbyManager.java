package com.videotron.tvi.illico.monitor.core;

import javax.tv.util.TVTimer;
import javax.tv.util.TVTimerScheduleFailedException;
import javax.tv.util.TVTimerSpec;
import javax.tv.util.TVTimerWentOffEvent;
import javax.tv.util.TVTimerWentOffListener;

import org.dvb.event.UserEvent;
import org.ocap.hardware.Host;
import org.ocap.hardware.PowerModeChangeListener;

import com.alticast.ui.LayeredKeyHandler;
import com.alticast.ui.LayeredUI;
import com.alticast.ui.LayeredUIManager;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.main.MonitorMain;

public class AutoStandbyManager implements LayeredKeyHandler, PowerModeChangeListener {
    /** The current inactivity time. */
    private long inactivityTime;
    /** The time that inputed lastest. */
    private long lastInputedKeyTime = 0;

    /** Indicates the state of not monitoring. */
    private static final int STATE_NOT_MONITORING = 0;
    /** Indicates the state of monitoring. */
    private static final int STATE_MONITORING = 1;
    /** The current monitoring state flag. */
    private int currentState = STATE_NOT_MONITORING;

    /** The instance of TVTimer. It is used to monitoring key event. */
    private TVTimer timer = TVTimer.getTimer();
    /** The instance of TVTimerSpec. It is used to monitoring key event. */
    private TVTimerSpec timerSpec = new TVTimerSpec();
    /** Implements the TVTimerWentOffListener. It is used to monitoring key event. */
    private TimerListener timerListener = new TimerListener();
    /** The time for checking inactivity time. */
    private static final long CHECK_TIME_INACTIVITY_TIME = 20 * 1000L;

    /** The time for comparing to STT (Sat Jan 01 00:00:00 KST 2000). */
    private static final long TIME_20000101_00_00_00 = 946652400000L;

    /** The singleton instance. */
    private static AutoStandbyManager instance;

    /** The Layered UI. */
    private LayeredUI layeredUI;

    private static final String LOG_HEADER = "[AutoStandbyManager] ";

    public synchronized static AutoStandbyManager getInstance() {
        if (instance == null) {
            instance = new AutoStandbyManager();
        }
        return instance;
    }

    private AutoStandbyManager() {
        Host.getInstance().addPowerModeChangeListener(this);
    }

    public synchronized void bootComplete() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "bootComplete() " + inactivityTime);
        }
        if (inactivityTime > 0) {
            startMonitoring();
        }
    }

    /**
     * Starts monitoring key event.
     * @param pInactivityTime the inactivity time. unit is millisecond.
     */
    private synchronized void startMonitoring() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startMonitoring (" + inactivityTime + ")");
        }
        currentState = STATE_MONITORING;

        startTimer();
        lastInputedKeyTime = System.currentTimeMillis();
        if (layeredUI == null) {
            layeredUI = LayeredUIManager.getInstance().createLayeredKeyHandler("AutoStandbyManager", 6900, this);
            layeredUI.activate();
        }
    }

    /**
     * Sets the inactivity time.
     * @param pInactivityTime
     */
    public synchronized void setInactivityTime(long pInactivityTime) {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "setInactivityTime (" + pInactivityTime + ")");
        }
        this.inactivityTime = pInactivityTime;
        if (inactivityTime > 0) {
            int bootState = MonitorMain.getInstance().getCurrentBootState();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "bootState = " + bootState);
            }
            if(bootState != MonitorMain.BOOT_STATE_FINISHED) {
                return;
            }

            startMonitoring();
        } else {
            stopMonitoring();
        }
    }

    /**
     * Stops monitoring.
     */
    private synchronized void stopMonitoring() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "stopMonitoring()");
        }
        currentState = STATE_NOT_MONITORING;
        stopTimer();
        if (layeredUI != null) {
            LayeredUIManager.getInstance().disposeLayeredUI(layeredUI);
            layeredUI = null;
        }
    }

    /**
     * Disposes this class. When the application's Xlet#destroyXlet() is called, It is called.
     */
    public void dispose() {
        stopMonitoring();
    }

    // ////////////////////////////////
    // Timer
    // ////////////////////////////////
    /**
     * Starts the timer for checking inactivity time.
     */
    private void startTimer() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "startTimer");
        }
        synchronized (timerSpec) {
            stopTimer();
            try {
                timerSpec.setDelayTime(CHECK_TIME_INACTIVITY_TIME);
                timerSpec.setRepeat(true);
                timerSpec.addTVTimerWentOffListener(timerListener);
                timer.scheduleTimerSpec(timerSpec);
            } catch (TVTimerScheduleFailedException e) {
                Log.print(e);
            }
        }
    }

    /**
     * Stop the timer for checking inactivity time.
     */
    private void stopTimer() {
        if (Log.INFO_ON) {
            Log.printInfo(LOG_HEADER + "stopTimer");
        }
        synchronized (timerSpec) {
            timerSpec.removeTVTimerWentOffListener(timerListener);
            timer.deschedule(timerSpec);
        }
    }

    /**
     * Implements the TVTimerWentOffListener.
     * @author Woosik Lee
     */
    private class TimerListener implements TVTimerWentOffListener {
        /**
         * This is timer call back. It will check the maximum inactivity time.
         * @param event TVTimer event.
         */
        public void timerWentOff(TVTimerWentOffEvent event) {
            long currentTime = System.currentTimeMillis();
            if (Log.INFO_ON) {
                Log.printInfo(LOG_HEADER + "timerWentOff() currentTime=" + currentTime
                        + ", lastInputedKeyTime=" + lastInputedKeyTime);
            }
            // Before receive the STT (System time table), ignore the lastInputedKeyTime
            if(lastInputedKeyTime < TIME_20000101_00_00_00) {
                lastInputedKeyTime = System.currentTimeMillis();
                return;
            }

            if (currentTime - lastInputedKeyTime > inactivityTime) {
                if (Log.INFO_ON) {
                    Log.printInfo(LOG_HEADER + "go to standby !!!");
                }
                Host.getInstance().setPowerMode(Host.LOW_POWER);
            }
        }
    };

    /**
     *
     */
    public boolean handleKeyEvent(UserEvent event) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "handleKeyEvent() event=" + event);
        }
        if (currentState == STATE_MONITORING) {
            lastInputedKeyTime = System.currentTimeMillis();
            if (Log.DEBUG_ON) {
                Log.printDebug(LOG_HEADER + "User event received time=" + lastInputedKeyTime);
            }
        }
        return false;
    }

    public synchronized void powerModeChanged(int newPowerMode) {
        if (Log.DEBUG_ON) {
            Log.printDebug(LOG_HEADER + "powerModeChanged() mode=" + newPowerMode + ", inavtivityTime="
                    + inactivityTime);
        }
        if (newPowerMode == Host.FULL_POWER) {
            if (inactivityTime > 0) {
                startMonitoring();
            }
        } else if (newPowerMode == Host.LOW_POWER) {
            stopMonitoring();
        }
    }

}

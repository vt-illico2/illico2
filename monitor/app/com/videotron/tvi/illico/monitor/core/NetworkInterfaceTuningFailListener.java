package com.videotron.tvi.illico.monitor.core;

public interface NetworkInterfaceTuningFailListener {

	public void tuningFailed();
}

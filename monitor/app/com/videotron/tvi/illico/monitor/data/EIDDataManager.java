package com.videotron.tvi.illico.monitor.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

import com.videotron.tvi.illico.framework.DataCenter;
import com.videotron.tvi.illico.framework.DataUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.EIDMappingTableUpdateListener;
import com.videotron.tvi.illico.ixc.monitor.MonitorService;
import com.videotron.tvi.illico.log.Log;
import com.videotron.tvi.illico.monitor.core.AodManager2;
import com.videotron.tvi.illico.monitor.core.AuthorizationManager;
import com.videotron.tvi.illico.monitor.main.MonitorMain;

/**
 * Reads the package_configuration.txt and packageslab.txt files in the OOB OC.
 *
 * @author Woosik Lee
 */
public final class EIDDataManager implements DataUpdateListener {
	/** The signleton instance of DataManager. */
	private static EIDDataManager instance;
	/** The flag of exist package_configuration.txt. */
	private boolean hasPackageFile;
	/** The EID (PackageID) - AppID mapping information. */
	private ApplicationPackage[] applicationIdPackages;
	/** The mandatory package list. */
	private MandatoryPackage[] mandatoryPackages;
	/** The activation package list. */
	private ActivationPackage[] activateionPackages;
	/** The IPPV deactivation package list. */
	private IPPVDeactivationPackage[] ippvDeactivationPackage;
	/** The Standalone package list. */
	private PvrStandalonePackage[] standalonePackage;
	/** The PVR mulitroom pckage list. */
	private PvrMultiroomPackage[] multiroomPackage;
	/** The MOCA package list. */
	private MocaPackage[] mocaPackage;
	/** Kenneth : The VOD deactivation package list. */
    // 봉인 : R4.1 에서 삭제, 추후 DDC 로
	//private VODDeactivationPackage[] vodDeactivationPackage;

    //->Kenneth[2015.2.4] 4K
	private UhdAvailabilityPackage[] uhdAvailabilityPackage;



	/** The version of package_configuration.txt. */
	private int version;
	
	private AodApplication[] aodApplications;

	/** The flag of exist packageslab.txt. */
	private boolean hasPackageIdEIDMappingTableFile;
	/**
	 * The package id/entitlement id mapping table. It is included at the
	 * packageslab.txt.
	 */
	private Hashtable packageID_EID_Mapping_table;
	/**
	 * The entitlement id / package id mapping table.
	 */
	private Hashtable eid_packageid_Mapping_table;
	/** The DNCS name. */
	private String DNCS_NAME = null;

	/** The list of EIDMappingTableUpdateListener. */
	private ArrayList listeners = new ArrayList();
	/** The package definition file path and name. */
	private static final String PACKAGE_CONFIG_FILE_NAME = "PACKAGE_CONFIG_FILE_NAME";
	/** The mapping table definition file path and name. */
	private static final String PACKAGE_EID_MAPPING_FILE = "PACKAGE_EID_MAPPING_FILE";

	private boolean ready_PACKAGE_CONFIG_FILE_NAME = false;
	private boolean ready_PACKAGE_EID_MAPPING_FILE = false;
	
	/** flag for checking AOD error. **/
	boolean isAODParsingError = false;

	/** The log header message. */
	private static final String LOG_HEADER = "[EIDDataManager] ";

	/**
	 * Gets the singleton instance.
	 *
	 * @return instance of DataManager.
	 */
	public static synchronized EIDDataManager getInstance() {
		if (instance == null) {
			instance = new EIDDataManager();
		}
		return instance;
	}

	public static synchronized void init() {
		if (instance == null) {
			instance = new EIDDataManager();
		}
	}
	
	public boolean getIsAODParsingError() {
		return isAODParsingError;
	}

	/**
	 * The constructor.
	 */
	private EIDDataManager() {
		packageID_EID_Mapping_table = new Hashtable();
		eid_packageid_Mapping_table = new Hashtable();

		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "start monitoring " + PACKAGE_CONFIG_FILE_NAME);
			Log.printDebug(LOG_HEADER + "start monitoring " + PACKAGE_EID_MAPPING_FILE);
		}

		synchronized (this) {

			DataCenter.getInstance().addDataUpdateListener(PACKAGE_CONFIG_FILE_NAME, this);
			DataCenter.getInstance().addDataUpdateListener(PACKAGE_EID_MAPPING_FILE, this);

			// 만약 현재 파일을 아직 읽지 못했다면 기다리겠다.
			File file1 = (File) DataCenter.getInstance().get(PACKAGE_CONFIG_FILE_NAME);
			if (file1 == null) {
				ready_PACKAGE_CONFIG_FILE_NAME = false;
			} else {
				ready_PACKAGE_CONFIG_FILE_NAME = true;
			}

			File file2 = (File) DataCenter.getInstance().get(PACKAGE_EID_MAPPING_FILE);
			if (file2 == null) {
				ready_PACKAGE_EID_MAPPING_FILE = false;
			} else {
				ready_PACKAGE_EID_MAPPING_FILE = true;
			}

			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "ready_PACKAGE_CONFIG_FILE_NAME="
						+ ready_PACKAGE_CONFIG_FILE_NAME);
				Log.printDebug(LOG_HEADER + "ready_PACKAGE_EID_MAPPING_FILE="
						+ ready_PACKAGE_EID_MAPPING_FILE);
			}

			// 현재 Booting 상황이다. 그런데 두 파일을 획득하지 못하면 다음 Process를 진행 할수가 없기 때문에
			// 일정시간동안 파일이 올라오도록 기다리겠다.
			if (!ready_PACKAGE_CONFIG_FILE_NAME || !ready_PACKAGE_EID_MAPPING_FILE) {
				try {
					if (Log.WARNING_ON) {
						Log.printWarning(LOG_HEADER + "Not found file so wait until 1 min");
					}
					// 여기서 기다린다.
					long waitTime = 60 * 1000L;
					if (MonitorMain.CA_EMULATION) {
						waitTime = 1L;
					}
					this.wait(waitTime);
					// Flag를 Reset 해서 다음 FileUpdate Callback 에 동작 하지 않도록 한다.
					ready_PACKAGE_CONFIG_FILE_NAME = true;
					ready_PACKAGE_EID_MAPPING_FILE = true;
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "End of wait");
					}
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}

		refreshMappingTable();
		refreshPackageID();
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "EID Information=" + toString());
		}

		//refreshAuthorizationResult();

        //->Kenneth[2015.3.25] 4K : EID OOB update 가 listener 걸기 전에 완료되는 경우
        // SUPPORT_UHD 를 UP setting 해서 다른 app 에게 전파하는 logic 동작하지 않음.
        // 그래서 생성자에서 함 호출해준다.
        // thread 로 안 했더니 계속 무한 반복하더군. 왜냐면 constructor 가 다 진행이 되어야 instance 가 
        // null 이 아니게 되는데 decideUhdAvailabilityPackage() 내에서 다시 EIDDataManager 를 찾게 되고
        // instance 가 null 이니 다시 new 로 creation 하는 일이 반복됨.
		Thread thread = new Thread() {
			public void run() {
                Log.printDebug(LOG_HEADER + "Contructor.thread.run() : Call decideUhdAvailabilityPackage() earlier");
                AuthorizationManager.getInstance().decideUhdAvailabilityPackage();
			}
		};
		thread.start();
	}

	/**
	 * Gets the Mapping table.
	 *
	 * @return Mapping table.
	 */
	public Hashtable getPackageID_EID_Mapping_table() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return packageID_EID_Mapping_table;
	}

	/**
	 * Gets the DNCS name.
	 *
	 * @return DNCS name.
	 */
	public String getDncsName() {
		return DNCS_NAME;
	}

	/**
	 * Disposes this class.
	 * <P>
	 * When the application's Xlet#destroyXlet() is called, It is called.
	 */
	public void dispose() {
		if (listeners != null) {
			listeners.clear();
		}
		DataCenter.getInstance().removeDataUpdateListener(PACKAGE_CONFIG_FILE_NAME, this);
		DataCenter.getInstance().removeDataUpdateListener(PACKAGE_EID_MAPPING_FILE, this);
	}

	/**
	 * If exists package file (package_configuration.txt) return true. Otherwise
	 * return false.
	 *
	 * @return hasPackageFile.
	 */
	public boolean hasPackageFile() {
		return hasPackageFile;
	}

	/**
	 * If exists package id/entitlement id mapping file (packageslab.txt) return
	 * true. Otherwise return false.
	 *
	 * @return hasPackageFile.
	 */
	public boolean hasPackageIdEIDMappingTableFile() {
		return hasPackageIdEIDMappingTableFile;
	}

	/**
	 * Gets the EID Packages.
	 *
	 * @return the list of EID packages.
	 */
	public ApplicationPackage[] getApplicationPackages() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return applicationIdPackages;
	}

	/**
	 * Gets the mandatory package ids.
	 *
	 * @return the list of {@link MandatoryPackage}.
	 */
	public MandatoryPackage[] getMandatoryPackages() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return mandatoryPackages;
	}

	/**
	 * Gets the activation package ids.
	 *
	 * @return the list of {@link ActivationPackage}.
	 */
	public ActivationPackage[] getActivationPackages() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return activateionPackages;
	}

	/**
	 * Gets the IPPV deactivation package ids.
	 *
	 * @return the list of {@link IPPVDeactivationPackage}.
	 */
	public IPPVDeactivationPackage[] getIppvDeactivationPackage() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return ippvDeactivationPackage;
	}

	/**
	 * Kenneth : Gets the VOD deactivation package ids.
     * 봉인 : R4.1 에서 삭제, 추후 DDC 로
	 *
	 * @return the list of {@link VODDeactivationPackage}.
	 */
    /*
	public VODDeactivationPackage[] getVODDeactivationPackage() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return vodDeactivationPackage;
	}
    */

    //->Kenneth[2015.2.4] 4K
	public UhdAvailabilityPackage[] getUhdAvailabilityPackage() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return uhdAvailabilityPackage;
	}

	/**
	 * Gets the Standalone package ids.
	 *
	 * @return the list of {@link PvrStandalonePackage}.
	 */
	public PvrStandalonePackage[] getStandalonePackage() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return standalonePackage;
	}

	public PvrMultiroomPackage[] getMultiroomPackage() {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return multiroomPackage;
	}

	public String findPackageIdByEid(short eid) {
		Object id = eid_packageid_Mapping_table.get(new Short(eid));
		if(Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "findPackageIdByEid() eid=" + eid + ", packageId=" + id);
		}
		if(id == null) {
			return null;
		}
		return (String)id;
	}

	/**
	 * Gets the AOD application list.
	 * @return
	 */
	public AodApplication[] getAodApplicationList() {
		if (!hasPackageIdEIDMappingTableFile) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if (!hasPackageFile) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}
		return aodApplications;
	}

	/**
	 * Returns the entitlement id.
	 *
	 * @param packageId
	 *            the package id.
	 * @return the entitlement id. if not found entitlement, returns the
	 *         Short.MIN_VALUE.
	 */
	public short findEidByPackageId(String packageId) {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return Short.MIN_VALUE;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return Short.MIN_VALUE;
		}

		Object id = packageID_EID_Mapping_table.get(packageId);
		if (id == null) {
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "findEidByPackageId() packageId=" + packageId
						+ ", found id is null");
			}
			return Short.MIN_VALUE;
		}
		if (id instanceof Short) {
			return ((Short) id).shortValue();
		}
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "findEidByPackageId() packageId=" + packageId
					+ ", found id is not Short value, id=" + id);
		}
		return Short.MIN_VALUE;
	}

	/**
	 * Returns the entitlement id.
	 *
	 * @param packageId
	 *            the package id.
	 * @return the entitlement id. if not found entitlement, returns the NULL.
	 */
	public String findEidStringByPackageId(String packageId) {
		if(!hasPackageIdEIDMappingTableFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageIdEIDMappingTableFile is false so return null");
			}
			return null;
		}
		if(!hasPackageFile) {
			if(Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "hasPackageFile is false so return null");
			}
			return null;
		}

		Object id = packageID_EID_Mapping_table.get(packageId);
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "findEidStringByPackageId() packageId=" + packageId
					+ ", found id=" + id);
		}
		if (id == null) {
			return null;
		}
		if (id instanceof String) {
			return (String) id;
		} else if (id instanceof Short) {
			return String.valueOf(id);
		}
		return null;
	}

	/**
	 * Reads the Package Id/Entitlement Id table from OOB. The file name is
	 * packageslab.txt.
	 */
	private void refreshMappingTable() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "refreshMappingTable() clear mapping table");
		}

		packageID_EID_Mapping_table.clear();
		eid_packageid_Mapping_table.clear();

		File file = (File) DataCenter.getInstance().get(PACKAGE_EID_MAPPING_FILE);
		if (file == null || !file.exists()) {
			if (Log.ERROR_ON) {
				Log.printError(LOG_HEADER + "refreshMappingTable() Not found packageslab.txt");
			}
			hasPackageIdEIDMappingTableFile = false;
			return;
		}

		Properties props = new Properties();
		FileInputStream inputStream = null;

		try {
			inputStream = new FileInputStream(file);
			props.load(inputStream);

			Enumeration keyList = props.propertyNames();
			while (keyList.hasMoreElements()) {
				String key = ((String) keyList.nextElement()).trim();
				String value = (String) props.getProperty(key);
				if(Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "key key is '" + key + "', value is '" + value + "'");
				}
				if (key.equalsIgnoreCase("DNCS")) {
					DNCS_NAME = value.trim();
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "found DNCS name is " + DNCS_NAME);
					}
				} else {
					if(value != null && !value.trim().equals("") && key != null && !key.equals("{") && !key.equals("}")) {
						try {
							Short shortV = Short.valueOf(value.trim());
							packageID_EID_Mapping_table.put(key, shortV);
							eid_packageid_Mapping_table.put(shortV, key);
						} catch (NumberFormatException nfe) {
							//Log.printError(LOG_HEADER + "Exception key is '" + key + "', value is '" + value + "'");
							//Log.print(nfe);
						}
					}
				}
			}

			hasPackageIdEIDMappingTableFile = true;
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "finish refreshMappingTable()");
			}

		} catch (Exception e) {
			Log.print(e);
			props = null;
		} finally {
			try {
				inputStream.close();
			} catch (Exception ex) {
				Log.print(ex);
			}
		}

	}

	/**
	 * Reads the package_configuration.txt file and parses the entitlement id
	 * and application id.
	 */
	private void refreshPackageID() {
		if (Log.INFO_ON) {
			Log.printInfo(LOG_HEADER + "refreshPackageID()");
		}

		File file = (File) DataCenter.getInstance().get(PACKAGE_CONFIG_FILE_NAME);
		if (file == null || !file.exists()) {
			if (Log.ERROR_ON) {
				Log.printError(LOG_HEADER + "readfreshEID() Not found package_configuration.txt");
			}
			hasPackageFile = false;
			return;
		}
		FileInputStream inputStream = null;

		isAODParsingError = false;
		
		try {
			inputStream = new FileInputStream(file);
			byte[] data = new byte[1024 * 10];
			int readLength = inputStream.read(data);
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "readfreshEID() readLength=" + readLength);
			}

			int version = data[0];

			int applicationListSize = data[1];
			int index = 2;
			if (Log.INFO_ON) {
				Log.printInfo(LOG_HEADER + "readfreshEID() version=" + version);
				Log.printDebug(LOG_HEADER + "readfreshEID() applicationListSize=" + applicationListSize);
			}

			// 1. Finds the application name VS package names
			ApplicationPackage[] applicationPackages = new ApplicationPackage[applicationListSize];
			for (int i = 0; i < applicationListSize; i++) {
				int applicationNameLength = data[index];
				index++;
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "readfreshApplicationID() applicationNameLength="
							+ applicationNameLength);
				}
				String applicationName = new String(data, index, applicationNameLength);
				index += applicationNameLength;

				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "readfreshApplicationID() applicationName[" + i + "]="
							+ applicationName);
				}

				int packageListSize = data[index];
				index++;
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "readfreshApplicationID() packageListSize="
							+ packageListSize);
				}

				String[] packageList = new String[packageListSize];
				for (int k = 0; k < packageListSize; k++) {
					int packageIdSize = data[index];
					index++;
					packageList[k] = new String(data, index, packageIdSize).trim();
					if (Log.DEBUG_ON) {
						Log.printDebug(LOG_HEADER + "readfreshApplicationID() packageList[" + k + "]="
								+ packageList[k]);
					}
					index += packageIdSize;
				}
				applicationPackages[i] = new ApplicationPackage(applicationName, packageList);
			}
			this.applicationIdPackages = applicationPackages;

			// 2. Reads the mandatory packages.
			int mandatoryPackageSize = data[index];
			index++;
			MandatoryPackage[] manPackages = new MandatoryPackage[mandatoryPackageSize];
			for (int i = 0; i < mandatoryPackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				manPackages[i] = new MandatoryPackage(name);
			}
			this.mandatoryPackages = manPackages;

			// 3. Reads the activation packages.
			int activationPackageSize = data[index];
			index++;
			ActivationPackage[] actiPackages = new ActivationPackage[activationPackageSize];
			for (int i = 0; i < activationPackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				actiPackages[i] = new ActivationPackage(name);
			}
			this.activateionPackages = actiPackages;

			// 4. Reads the IPPV deactivation packages.
			int ippvPackageSize = data[index];
			index++;
			IPPVDeactivationPackage[] ippvPackages = new IPPVDeactivationPackage[ippvPackageSize];
			for (int i = 0; i < ippvPackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				ippvPackages[i] = new IPPVDeactivationPackage(name);
			}
			this.ippvDeactivationPackage = ippvPackages;

			// 5. Reads the Standalone packages.
			int standalonePackageSize = data[index];
			index++;
			PvrStandalonePackage[] standPackages = new PvrStandalonePackage[standalonePackageSize];
			for (int i = 0; i < standalonePackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				standPackages[i] = new PvrStandalonePackage(name);
			}
			this.standalonePackage = standPackages;

			// 6. Reads the PVR Multiroom packages.
			int multiroomPackageSize = data[index];
			index++;
			PvrMultiroomPackage[] multiroomPackages = new PvrMultiroomPackage[multiroomPackageSize];
			for (int i = 0; i < multiroomPackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				multiroomPackages[i] = new PvrMultiroomPackage(name);
			}
			this.multiroomPackage = multiroomPackages;
			
			// 7. Moca package.
			int mocaPackageSize = data[index];
			index++;
			MocaPackage[] mocaPack = new MocaPackage[mocaPackageSize];
			for (int i = 0; i < mocaPackageSize; i++) {
				int nameLength = data[index];
				index++;
				String name = new String(data, index, nameLength).trim();
				index += nameLength;
				mocaPack[i] = new MocaPackage(name);
			}
			this.mocaPackage = mocaPack;
			
			
			isAODParsingError = true;
			
			// 8. AOD Application list
			int aodAppSize = data[index];
			index++;
			AodApplication[] aodApps = new AodApplication[aodAppSize];
			for(int i=0; i<aodAppSize; i++) {
				int appNameLength = data[index];
				index++;
				String aodAppName = new String(data, index, appNameLength).trim();
				index += appNameLength;
				
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "AOD App name : " + aodAppName);
				}
				if (aodAppName == null || aodAppName.equals("")) {
					AodManager2.getInstance().notifyResultNewThread("", null,
							MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING);
				}
				
				int appPathLength = data[index];
				index++;
				String aodAppPath = new String(data, index, appPathLength).trim();
				index += appPathLength;
				
				aodApps[i] = new AodApplication(aodAppName, aodAppPath);
				
				if(Log.INFO_ON) {
					Log.printInfo(LOG_HEADER + "AOD App path : " + aodAppPath);
				}
				if (aodAppPath == null || aodAppPath.equals("")) {
					AodManager2.getInstance().notifyResultNewThread("", null,
							MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING);
				}
			}

            //->Kenneth : R4.1 : 9. Reads the VOD deactivation packages.
            // 봉인 : R4.1 에서 삭제, 추후 DDC 로
            /*
            try {
                int vodPackageSize = data[index];
                index++;
                VODDeactivationPackage[] vodPackages = new VODDeactivationPackage[vodPackageSize];
                for (int i = 0; i < vodPackageSize; i++) {
                    int nameLength = data[index];
                    index++;
                    String name = new String(data, index, nameLength).trim();
                    Log.printDebug("Kenneth : VOD Deactivation Package Name = "+name);
                    index += nameLength;
                    vodPackages[i] = new VODDeactivationPackage(name);
                }
                this.vodDeactivationPackage = vodPackages;
            } catch (Exception e) {
                Log.print(e);
            }
            */
            //->Kenneth[2015.2.4] 4K
            try {
                int uhdPackageSize = data[index];
                index++;
                UhdAvailabilityPackage[] uhdPackages = new UhdAvailabilityPackage[uhdPackageSize];
                for (int i = 0; i < uhdPackageSize ; i++) {
                    int nameLength = data[index];
                    index++;
                    String name = new String(data, index, nameLength).trim();
                    Log.printDebug("Kenneth : Uhd Availability Package Name = "+name);
                    index += nameLength;
                    uhdPackages[i] = new UhdAvailabilityPackage(name);
                }
                this.uhdAvailabilityPackage = uhdPackages;
            } catch (Exception e) {
                Log.print(e);
            }



			this.aodApplications = aodApps;
			hasPackageFile = true;
			isAODParsingError = false;
		} catch (Exception e) {
			Log.print(e);
			if(isAODParsingError) {
				AodManager2.getInstance().notifyResultNewThread("", null, MonitorService.AOD_ERROR_CONFIGURATION_FILE_PARSING);
			}
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception e) {
					Log.print(e);
				}
			}
		}

	}

	/**
	 * Override the toString() in Object class.
	 *
	 * @see toString in class java.lang.Object#toString()
	 * @return class message.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer("\n====================================\n");
		buf.append("EIDDataManager version=" + version + "\n");
		buf.append("\t Application Package Info \n");
		if (applicationIdPackages != null) {
			buf.append("\t\t Application Package length = " + applicationIdPackages.length + "\n");
			for (int i = 0; i < applicationIdPackages.length; i++) {
				buf.append("\t\t Application Package[" + i + "] Application Name = "
						+ applicationIdPackages[i].getPackageName() + "\n");
				String[] packageIDList = applicationIdPackages[i].getPackageIDs();
				for (int j = 0; j < packageIDList.length; j++) {
					buf.append("\t\t\t Package ID = " + packageIDList[j] + "\n");
				}
			}
		}

		if (mandatoryPackages != null) {
			buf.append("\t Mandatory Package length = " + mandatoryPackages.length + "\n");
			for (int i = 0; i < mandatoryPackages.length; i++) {
				buf.append("\t\t MandatoryPackages ID = " + mandatoryPackages[i].getPackageName() + "\n");
			}
		}
		if (activateionPackages != null) {
			buf.append("\t Activateion Package length = " + activateionPackages.length + "\n");
			for (int i = 0; i < activateionPackages.length; i++) {
				buf.append("\t\t ActivateionPackages ID = " + activateionPackages[i].getPackageName() + "\n");
			}
		}
		if (ippvDeactivationPackage != null) {
			buf.append("\t ippvDeactivation Package length = " + ippvDeactivationPackage.length + "\n");
			for (int i = 0; i < ippvDeactivationPackage.length; i++) {
				buf.append("\t\t IppvDeactivationPackage ID = " + ippvDeactivationPackage[i].getPackageName() + "\n");
			}
		}
		if (standalonePackage != null) {
			buf.append("\t standalonePackage length = " + standalonePackage.length + "\n");
			for (int i = 0; i < standalonePackage.length; i++) {
				buf.append("\t\t standalonePackage ID = " + standalonePackage[i].getPackageName() + "\n");
			}
		}
		if (multiroomPackage != null) {
			buf.append("\t multiroomPackage length = " + multiroomPackage.length + "\n");
			for (int i = 0; i < multiroomPackage.length; i++) {
				buf.append("\t\t multiroomPackage ID = " + multiroomPackage[i].getPackageName() + "\n");
			}
		}
		
		if (mocaPackage != null) {
			buf.append("\t mocaPackage length = " + mocaPackage.length + "\n");
			for (int i = 0; i < mocaPackage.length; i++) {
				buf.append("\t\t mocaPackage ID = " + mocaPackage[i].getPackageName() + "\n");
			}
		}
		
		if (aodApplications != null) {
			buf.append("\t aodApplications length = " + aodApplications.length + "\n");
			for (int i = 0; i < aodApplications.length; i++) {
				buf.append("\t\t aodApplications name = " + aodApplications[i].getAppName() + ", path=" + aodApplications[i].getAppPath() + "\n");
			}
		}

        //->Kenneth
        // 봉인 : R4.1 에서 삭제, 추후 DDC 로
        /*
		if (vodDeactivationPackage != null) {
			buf.append("\t vodDeactivation Package length = " + vodDeactivationPackage.length + "\n");
			for (int i = 0; i < vodDeactivationPackage.length; i++) {
				buf.append("\t\t VODDeactivationPackage ID = " + vodDeactivationPackage[i].getPackageName() + "\n");
			}
		}
        */
        //<-
        //->Kenneth[2015.2.4] 4K
		if (uhdAvailabilityPackage != null) {
			buf.append("\t uhdAvailability Package length = " + uhdAvailabilityPackage.length + "\n");
			for (int i = 0; i < uhdAvailabilityPackage.length; i++) {
				buf.append("\t\t UhdAvailabilityPackage ID = " + uhdAvailabilityPackage[i].getPackageName() + "\n");
			}
		}

		buf.append("\t Package ID / EID Mapping table is following");
		Enumeration keyList = packageID_EID_Mapping_table.keys();
		while (keyList.hasMoreElements()) {
			String key = (String) keyList.nextElement();
			Object valueTemp = packageID_EID_Mapping_table.get(key);
			buf.append("\t\t " + key + "=" + valueTemp + "\n");
		}

		buf.append("=======================================\n");

		return buf.toString();
	}

	/**
	 * Adds the listener.
	 *
	 * @param l
	 *            instance of EIDMappingTableUpdateListener.
	 * @param appName
	 *            requested application name.
	 */
	public void addEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String appName) {
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, appName);
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "addEIDMappingTableUpdateListener():" + item + ", appName="
						+ appName);
			}
			if (!listeners.contains(item)) {
				listeners.add(item);
			} else {
				// Removes a previous listener.
				listeners.remove(item);
				// Adds a new listener.
				listeners.add(item);
			}
		}
	}

	/**
	 * Removes a listener.
	 *
	 * @param l
	 *            instance of EIDMappingTableUpdateListener.
	 * @param applicationName
	 *            requested application name.
	 */
	public void removeEIDMappingTableUpdateListener(EIDMappingTableUpdateListener l, String applicationName) {
		synchronized (listeners) {
			ListenerItem item = new ListenerItem(l, applicationName);
			if (Log.DEBUG_ON) {
				Log.printDebug(LOG_HEADER + "removeEIDMappingTableUpdateListener():" + item + ", appName="
						+ applicationName);
			}
			listeners.remove(item);
		}
	}

	/**
	 * Notifies to listeners. If haven't any listener, return true. Returns
	 * false if one or more listeners reject this question, otherwise return
	 * true.
	 */
	public void notifyEIDMappingTableUpdated() {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "notifyEIDMappingTableUpdated()");
		}
		Object[] lsnrs;
		synchronized (listeners) {
			lsnrs = listeners.toArray();
		}
		for (int i = 0; i < lsnrs.length; i++) {
			try {
				if (Log.DEBUG_ON) {
					Log.printDebug(LOG_HEADER + "notifyEIDMappingTableUpdated() to " + lsnrs[i]);
				}
				ListenerItem ssl = (ListenerItem) lsnrs[i];
				ssl.listener.tableUpdated();
			} catch (Throwable t) {
				Log.print(t);
			}
		}
	}

	/**
	 * This class has the unique application id and the instance of
	 * EIDMappingTableUpdateListener.
	 *
	 * @author Woosik Lee
	 */
	private class ListenerItem {
		/** Instance of EIDMappingTableUpdateListener. */
		private EIDMappingTableUpdateListener listener;
		/** The requested application name. */
		private String applicationName;

		/**
		 * Constructor.
		 *
		 * @param l
		 *            EIDMappingTableUpdateListener.
		 * @param appName
		 *            requested application name.
		 */
		public ListenerItem(final EIDMappingTableUpdateListener l, final String appName) {
			this.listener = l;
			this.applicationName = appName;
		}

		/**
		 * Overrides the equals() by Object class.
		 *
		 * @param o
		 *            the object to compare with this.
		 * @return if equals instance return true, otherwise return false.
		 */
		public boolean equals(Object o) {
			if (o instanceof ListenerItem) {
				String appName = ((ListenerItem) o).applicationName;
				if (appName.equals(applicationName)) {
					return true;
				}
				return false;
			} else {
				return false;
			}
		}
	}

	/**
	 * Called when data has been removed.
	 *
	 * @param key
	 *            key of data.
	 */
	public void dataRemoved(String key) {

	}

	/**
	 * Called when data has been updated.
	 *
	 * @param key
	 *            key of data.
	 * @param old
	 *            old value of data.
	 * @param value
	 *            new value of data.
	 */
	public void dataUpdated(String key, Object old, Object value) {
		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "dataUpdated() key=" + key);
			Log.printDebug(LOG_HEADER + "ready_PACKAGE_CONFIG_FILE_NAME=" + ready_PACKAGE_CONFIG_FILE_NAME);
			Log.printDebug(LOG_HEADER + "ready_PACKAGE_EID_MAPPING_FILE=" + ready_PACKAGE_EID_MAPPING_FILE);
		}

		synchronized (this) {
			if (!ready_PACKAGE_CONFIG_FILE_NAME || !ready_PACKAGE_EID_MAPPING_FILE) {
				// 한번도 파일을 읽지 않았고생성자에서 wait(60000) 하고 있는 상태이다.
				// It is waiting state in constructor (wait(6000)).
				if (PACKAGE_CONFIG_FILE_NAME.equals(key)) {
					if (!ready_PACKAGE_CONFIG_FILE_NAME) {
						ready_PACKAGE_CONFIG_FILE_NAME = true;
					}
				}

				if (PACKAGE_EID_MAPPING_FILE.equals(key)) {
					if (!ready_PACKAGE_EID_MAPPING_FILE) {
						ready_PACKAGE_EID_MAPPING_FILE = true;
					}
				}
				// 둘다 Ready 되었으면 Lock 을 풀고 그냥 리턴한다. 생성자에서 파일을 처리 할 것이다.
				if (ready_PACKAGE_CONFIG_FILE_NAME && ready_PACKAGE_EID_MAPPING_FILE) {
					this.notifyAll();
				}
				if (Log.INFO_ON) {
					Log.printDebug(LOG_HEADER + "(after) ready_PACKAGE_CONFIG_FILE_NAME="
							+ ready_PACKAGE_CONFIG_FILE_NAME);
					Log.printDebug(LOG_HEADER + "(after) ready_PACKAGE_EID_MAPPING_FILE="
							+ ready_PACKAGE_EID_MAPPING_FILE);
				}
				return;
			}
		}

		if (PACKAGE_CONFIG_FILE_NAME.equals(key)) {
			refreshPackageID();
			AuthorizationManager.getInstance().changedAuthorizationFile();
			// notifyEIDMappingTableUpdated();
		} else if (PACKAGE_EID_MAPPING_FILE.equals(key)) {
			refreshMappingTable();
			notifyEIDMappingTableUpdated();
			AuthorizationManager.getInstance().changedAuthorizationFile();
			// TODO 아래 내용 호출 하지 말자..
			//refreshAuthorizationResult();
		}

		if (Log.DEBUG_ON) {
			Log.printDebug(LOG_HEADER + "EID Information==" + toString());
		}
	}

	/*
	private void refreshAuthorizationResult() {
		// Refresh the authorization result.
		Thread newListThread = new Thread("NewListThread") {
			public void run() {
				AuthrorizationResultManager.getInstance().setNewEidList(packageID_EID_Mapping_table);
			}
		};
		newListThread.start();
	}
	*/

}
